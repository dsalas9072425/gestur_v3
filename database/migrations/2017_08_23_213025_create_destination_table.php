<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDestinationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('destination', function(Blueprint $table)
		{
			$table->integer('destination_id', true);
			$table->integer('destinationdtp_id')->default(0);
			$table->integer('infoprovider_id')->default(0);
			$table->string('destination_code', 10)->default('XDBD');
			$table->string('destination_name', 70)->default('XDBD');
			$table->string('destination_type', 20)->default('XDBD');
			$table->integer('destination_super')->default(0);
			$table->datetimetz('destination_timeimport')->default('0001-12-31 20:09:20-03:50:40 BC');
			$table->datetimetz('destination_timelastimport')->default('0001-12-31 20:09:20-03:50:40 BC');
			$table->boolean('destination_tempscript')->default(0);
			$table->string('destination_continent', 70)->default('XDBD');
			$table->string('destination_region', 70)->default('XDBD');
			$table->string('destination_country', 70)->default('XDBD');
			$table->string('destination_department', 70)->default('XDBD');
			$table->string('destination_city', 70)->default('XDBD');
			$table->string('destination_zone', 70)->default('XDBD');
			$table->string('destination_state', 20)->default('XDBD');
			$table->string('destination_doc', 30)->default('XDBD');
			$table->string('destination_countrycode')->nullable()->default('XDBD');
			$table->string('destination_statecode')->nullable()->default('XDBD');
			$table->string('destination_latitude')->nullable()->default('XDBD');
			$table->string('destination_longitude')->nullable()->default('XDBD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('destination');
	}

}
