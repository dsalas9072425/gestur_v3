<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentmethodTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('paymentmethod', function(Blueprint $table)
		{
			$table->integer('paymentmethod_id', true);
			$table->string('paymentmethod_description', 70)->default('XDBD')->unique('paymentmethod_paymentmethod_description_key');
			$table->string('paymentmethod_state', 20)->default('XDBD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('paymentmethod');
	}

}
