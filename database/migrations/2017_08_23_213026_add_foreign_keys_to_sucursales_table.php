<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSucursalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sucursales', function(Blueprint $table)
		{
			$table->foreign('id_agencia', 'sucursales_fk_01')->references('id_agencia')->on('agencias')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sucursales', function(Blueprint $table)
		{
			$table->dropForeign('sucursales_fk_01');
		});
	}

}
