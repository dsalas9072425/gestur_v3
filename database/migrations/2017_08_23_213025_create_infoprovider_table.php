<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInfoproviderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('infoprovider', function(Blueprint $table)
		{
			$table->integer('infoprovider_id', true);
			$table->string('infoprovider_code', 10)->default('XDBD')->unique('infoprovider_infoprovider_code_key');
			$table->string('infoprovider_name', 70)->default('XDBD')->unique('infoprovider_infoprovider_name_key');
			$table->string('infoprovider_description', 250)->default('XDBD');
			$table->string('infoprovider_web', 100)->default('XDBD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('infoprovider');
	}

}
