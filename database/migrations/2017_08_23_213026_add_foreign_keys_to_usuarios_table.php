<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuarios', function(Blueprint $table)
		{
			$table->foreign('id_agencia', 'usuarios_fk_01')->references('id_agencia')->on('sucursales')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_perfil', 'usuarios_fk_02')->references('id_perfil')->on('perfiles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuarios', function(Blueprint $table)
		{
			$table->dropForeign('usuarios_fk_01');
			$table->dropForeign('usuarios_fk_02');
		});
	}

}
