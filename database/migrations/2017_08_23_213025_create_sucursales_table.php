<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSucursalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sucursales', function(Blueprint $table)
		{
			$table->string('descripcion_agencia');
			$table->integer('id_sucursal_agencia', true);
			$table->integer('id_agencia');
			$table->string('telefono')->nullable();
			$table->string('email')->nullable();
			$table->string('latitud')->nullable();
			$table->string('longitud')->nullable();
			$table->char('activo', 1)->default('S');
			$table->bigInteger('id_sistema_facturacion')->nullable()->unique('sucursales_id_sistema_facturacion_uk');
			$table->primary(['id_agencia','id_sucursal_agencia'], 'pk_suc_agencia');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sucursales');
	}

}
