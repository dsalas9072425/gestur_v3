<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProveedorPorDestinoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('proveedor_por_destino', function(Blueprint $table)
		{
			$table->foreign('destino', 'proveedor_por_destino_destino_fk')->references('destination_id')->on('destination')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('proveedor', 'proveedor_por_destino_proveedor_fk')->references('infoprovider_id')->on('infoprovider')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('proveedor_por_destino', function(Blueprint $table)
		{
			$table->dropForeign('proveedor_por_destino_destino_fk');
			$table->dropForeign('proveedor_por_destino_proveedor_fk');
		});
	}

}
