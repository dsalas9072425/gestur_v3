<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table)
		{
			$table->integer('id_usuario', true);
			$table->integer('id_agencia');
			$table->string('usuario')->unique('usuarios_usuario_uk');
			$table->string('password')->nullable();
			$table->string('email');
			$table->char('activo', 1);
			$table->date('fecha_alta')->nullable()->default('now()');
			$table->date('fecha_validez')->nullable();
			$table->integer('id_perfil')->nullable()->index('fki_perfil');
			$table->integer('id_sucursal_agencia');
			$table->string('nombre_apellido')->nullable();
			$table->boolean('cambiarpass')->nullable();
			$table->string('tokenemail')->nullable();
			$table->integer('codigo_agente')->nullable();
			$table->text('primer_pass')->nullable();
			$table->boolean('pass_notificado')->default(0);
			$table->integer('id_sistema_facturacion')->nullable()->unique('usuarios_id_sistema_facturacion_uk');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}
