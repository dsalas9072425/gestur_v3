<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFileReservaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('file_reserva', function(Blueprint $table)
		{
			$table->integer('id_file_reserva', true);
			$table->integer('id_usuario')->default(0);
			$table->datetimetz('fecha_creacion')->default('0002-12-31 20:09:20-03:50:40 BC');
			$table->string('cliente')->default('XDBD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('file_reserva');
	}

}
