<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('currency', function(Blueprint $table)
		{
			$table->integer('currency_id')->primary('currency_pkey');
			$table->text('currency_code')->unique('currency_currency_code_uk');
			$table->text('hb_desc')->nullable();
			$table->text('to_desc')->nullable();
			$table->integer('currency_num_code')->nullable();
			$table->float('rate_actual', 10, 0);
			$table->dateTime('fecha_actualizacion')->nullable()->default('now()');
			$table->boolean('actualizar_auto')->default(1);
			$table->float('multiplicador', 10, 0);
			$table->float('rate_webservice', 10, 0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('currency');
	}

}
