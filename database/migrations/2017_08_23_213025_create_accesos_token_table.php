<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccesosTokenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accesos_token', function(Blueprint $table)
		{
			$table->integer('id_auditoria', true);
			$table->datetimetz('fecha_hora');
			$table->datetimetz('validez')->nullable();
			$table->string('usuario')->nullable();
			$table->string('ip')->nullable();
			$table->integer('id_estado_acceso')->nullable();
			$table->string('token')->nullable();
			$table->integer('id_usuario')->nullable();
			$table->integer('id_agencia')->nullable();
			$table->integer('id_sucursal_agencia')->nullable();
			$table->integer('id_perfil')->nullable();
			$table->char('activo', 1)->default('S');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accesos_token');
	}

}
