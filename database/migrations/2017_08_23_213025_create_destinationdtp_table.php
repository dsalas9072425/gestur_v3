<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDestinationdtpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('destinationdtp', function(Blueprint $table)
		{
			$table->integer('destinationdtp_id', true);
			$table->string('destinationdtp_code', 20)->default('XDBD');
			$table->string('destinationdtp_name', 70)->default('XDBD');
			$table->string('destinationdtp_type', 20)->default('XDBD');
			$table->integer('destinationdtp_super')->default(0);
			$table->string('destinationdtp_cin', 20)->default('XDBD');
			$table->string('destinationdtp_state', 20)->default('XDBD');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('destinationdtp');
	}

}
