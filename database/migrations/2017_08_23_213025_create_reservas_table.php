<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_file_reserva')->default(0);
			$table->integer('id_usuario')->default(0);
			$table->text('codigo')->nullable();
			$table->date('fecha_reserva')->default('now()')->comment('utilizado para guardar la descripción de retorno del proveedor al realizar la reserva');
			$table->integer('id_agencia');
			$table->text('nombre_agente_dtp');
			$table->text('codigo_hotel');
			$table->integer('id_proveedor');
			$table->integer('id_destino_dtp');
			$table->text('pasajero_principal_nombre');
			$table->text('pasajero_principal_apellido');
			$table->integer('cantidad_habitaciones');
			$table->date('fecha_checkin');
			$table->date('fecha_checkout');
			$table->float('monto_con_comision', 10, 0);
			$table->float('monto_sin_comision', 10, 0);
			$table->float('monto_cobrado', 10, 0);
			$table->text('nombre_hotel')->nullable();
			$table->text('direccion_hotel')->nullable();
			$table->text('telefono_hotel')->nullable();
			$table->date('cancelacion_desde')->nullable()->comment('la fecha a partir de la cual se incurre en gastos si se cancela la reserva');
			$table->float('cancelacion_monto', 10, 0)->nullable();
			$table->integer('state_id')->default(1);
			$table->integer('id_moneda');
			$table->integer('id_medio_pago');
			$table->integer('cantidad_noches');
			$table->dateTime('fecha_cancelacion')->nullable();
			$table->integer('id_usuario_cancelacion')->nullable();
			$table->text('info_adicional')->nullable();
			$table->text('request')->nullable()->comment('request de la reserva');
			$table->text('response')->nullable()->comment('response de la reserva');
			$table->dateTime('fecha_hora_rq')->nullable();
			$table->dateTime('fecha_hora_rs')->nullable();
			$table->text('comentario_pasajero')->nullable();
			$table->text('referencia_cliente')->nullable()->comment('código ingresado por la agencia como referencia de la reserva');
			$table->string('codigo_cancelacion', 50)->nullable();
			$table->text('info_adicional_hotel')->nullable();
			$table->float('monto_pagado', 10, 0)->nullable();
			$table->string('cancelacion_notificada', 1)->nullable();
			$table->string('enviado_proforma', 1)->nullable();
			$table->dateTime('fechahora_proforma')->nullable();
			$table->string('codret_proforma', 50)->nullable();
			$table->text('desret_proforma')->nullable();
			$table->text('rq_proforma')->nullable();
			$table->text('rs_proforma')->nullable();
			$table->integer('ocupancia');
			$table->string('hora_checkin', 20)->nullable();
			$table->string('hora_checkout', 20)->nullable();
			$table->string('id_request', 50)->unique('reservas_id_request_uk_01')->comment('el id de la búsqueda realizada utilizado para asociar los resultados encontrados');
			$table->integer('id_agente_dtp');
			$table->integer('id_moneda_origen');
			$table->float('monto_moneda_origen', 10, 0);
			$table->float('rate_concretado', 10, 0);
			$table->unique(['codigo','id_proveedor']);
			$table->unique(['id_proveedor','codigo'], 'reservas_uk');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reservas');
	}

}
