<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agencias', function(Blueprint $table)
		{
			$table->integer('id_agencia', true);
			$table->decimal('linea_credito', 15, 4)->nullable();
			$table->decimal('comision', 12)->nullable();
			$table->string('razon_social');
			$table->string('telefono')->nullable();
			$table->string('latitud')->nullable();
			$table->string('longitud')->nullable();
			$table->string('email')->nullable();
			$table->string('logo')->nullable();
			$table->char('activo', 1)->default('S');
			$table->date('fecha_alta')->default('now()');
			$table->bigInteger('id_sistema_facturacion')->nullable()->unique('agencias_id_sistema_facturacion_uk');
			$table->bigInteger('vencimiento_factura')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agencias');
	}

}
