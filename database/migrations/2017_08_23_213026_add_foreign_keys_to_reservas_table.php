<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReservasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservas', function(Blueprint $table)
		{
			$table->foreign('id_file_reserva', 'reservas_fk_01')->references('id_file_reserva')->on('file_reserva')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_proveedor', 'reservas_fk_02')->references('infoprovider_id')->on('infoprovider')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_moneda', 'reservas_fk_currency')->references('currency_id')->on('currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_medio_pago', 'reservas_fk_paymentmethod')->references('paymentmethod_id')->on('paymentmethod')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_usuario_cancelacion', 'reservas_fk_usuarios_id_usuario_cancelacion')->references('id_usuario')->on('usuarios')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('state_id', 'reservas_fk_states_state_id')->references('state_id')->on('states')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_usuario', 'reservas_fk_usuarios_id_usuario')->references('id_usuario')->on('usuarios')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_destino_dtp', 'reservas_fk_id_destino_dtp')->references('destinationdtp_id')->on('destinationdtp')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_moneda_origen', 'reservas_fk_currency_id_moneda_origen')->references('currency_id')->on('currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_agente_dtp', 'reservas_fk_id_agente_dtp')->references('id_usuario')->on('usuarios')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservas', function(Blueprint $table)
		{
			$table->dropForeign('reservas_fk_01');
			$table->dropForeign('reservas_fk_02');
			$table->dropForeign('reservas_fk_currency');
			$table->dropForeign('reservas_fk_paymentmethod');
			$table->dropForeign('reservas_fk_usuarios_id_usuario_cancelacion');
			$table->dropForeign('reservas_fk_states_state_id');
			$table->dropForeign('reservas_fk_usuarios_id_usuario');
			$table->dropForeign('reservas_fk_id_destino_dtp');
			$table->dropForeign('reservas_fk_currency_id_moneda_origen');
			$table->dropForeign('reservas_fk_id_agente_dtp');
		});
	}

}
