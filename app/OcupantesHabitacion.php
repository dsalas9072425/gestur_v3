<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OcupantesHabitacion extends Model
{
	protected $table = 'ocupantes_habitacion';
	
    public function ocupantesHabitacion()
    {
        return $this->belongsTo('App\Reserva','id_reserva','habitacion');
    }

}
