<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    protected $table = 'notificaciones';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
    	public function usuario()
    {
        return $this->belongsTo('App\Usuario','id_usuario','id_usuario');
    }
	public function agencia()
    {
        return $this->belongsTo('App\Agencias','id_agencia','id_agencia');
    }

        public function tipos()
    {
        return $this->belongsTo('App\TipoNotificacion','tipo','id');
    }


}