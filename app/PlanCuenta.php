<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanCuenta extends Model
{
    protected $table = 'plan_cuentas';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;

    public function currency(){
		return $this->belongsTo('App\Currency', 'id_moneda');
	}

    
}

