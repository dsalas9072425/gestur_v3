<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentaGasto extends Model
{
    protected $table = 'cuentas_gastos';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
   
    public function usuario()
    {
        return $this->belongsTo('App\Persona','id_usuario');
    }

    public function planCuentas()
    {
        return $this->belongsTo('App\PlanCuenta','id_cuenta_padre');
    }

    public function cuentasPadre()
    {
        return $this->belongsTo('App\CuentaGasto','id_cuenta_padre');
    }


}