<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrigenAsiento extends Model
{
    protected $table = 'origen_asiento_contable';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
}
