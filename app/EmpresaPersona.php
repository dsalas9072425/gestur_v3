<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpresaPersona extends Model
{
   protected $table = 'empresa_persona';
   public $timestamps = false;
   protected $primaryKey = 'id';

 

   public function sucursal()
   {
       return $this->belongsTo('App\Persona','id_sucursal_empresa','id');
   }


	public function empresas()
    {
        return $this->belongsTo('App\Empresa','id_empresa','id');
    }


}


