<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepositoBancarioDetalle extends Model
{
    protected $table = 'deposito_bancario_detalle';
    protected $primaryKey = 'id';
    public $timestamps = false;




    public function forma_cobro_recibo_detalle()
    {
        return $this->belongsTo('App\FormaCobroReciboDetalle','id_forma_cobro');
    }

}


