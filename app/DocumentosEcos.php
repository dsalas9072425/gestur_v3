<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentosEcos extends Model
{
    protected $table = 'documentos_electronicos';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


	public function factura()
    {
      return $this->belongsTo('App\Factura','id_documento','id');
    }


}