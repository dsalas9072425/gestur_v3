<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agencias extends Model
{
    protected $table = 'agencias';
    public $timestamps = false;
	protected $primaryKey = 'id_agencia';
    public function sucursal()
    {
        return $this->belongsTo('App\Sucursal','id_sucursal_agencia','id_sucursal_agencia');
    }

}