<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiquidacionComisionDetalle extends Model
{
  	protected $table = 'liquidaciones_comisiones_vendedores_detalle';

    public function liquidaciones()
    {
      return $this->belongsTo('App\LiquidacionComision','id_liquidacion','id');
    }

    public function facturas()
    {
      return $this->belongsTo('App\Factura','id_documento','id');
    }

    public function notasCredito()
    {
      return $this->belongsTo('App\NotaCredito','id_documento','id');
    }
}