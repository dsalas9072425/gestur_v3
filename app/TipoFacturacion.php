<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoFacturacion extends Model
{
    protected $table = 'tipo_facturacion';
    protected $primaryKey = 'id';  

}