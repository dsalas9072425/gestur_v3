<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaCobroCliente extends Model
{
    protected $table = 'forma_cobro_cliente';
    protected $primaryKey = 'id';  

    //Define que formas de cobro se puede utilizar para selccionar cuenta y banco, tambien sirve como listado 
    public const SELECTCUENTA = [
        'EFECT' => false,
        'CHEQ' => false,
        'CHEQ_DIF' => false,
        'TRANSF' => false,
        'RETEN' => true,
        'DEP' => false,
        'FACT' => true,
        'TC' => true,
        'TD' => true,
        'VO' => true
    ];


    public const CUENTA_DETALLE = [
        'EFECT' => ['depositable' => true, 'tipo' => 'VALORES'],
        'CHEQ' => ['depositable' => true, 'tipo' => 'VALORES'],
        'CHEQ_DIF' => ['depositable' => true, 'tipo' => 'VALORES'],
        'TRANSF' => ['depositable' => false, 'tipo' => 'VALORES'],
        'RETEN' => ['depositable' => false, 'tipo' => 'DOCUMENTO'],
        'DEP' => ['depositable' => false, 'tipo' => 'VALORES'],
        'FACT' => ['depositable' => false, 'tipo' => 'DOCUMENTO'],
        'TC' => ['depositable' => false, 'tipo' => 'VALORES'],
        'TD' => ['depositable' => false, 'tipo' => 'VALORES'],
        'VO' => ['depositable' => false, 'tipo' => 'DOCUMENTO'],
    ];




    public function ctaCtb()
    {
        return $this->hasMany('App\CtaCtb','id_forma_cobro','id');
    }

}