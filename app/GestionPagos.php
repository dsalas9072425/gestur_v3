<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GestionPagos extends Model
{
    protected $table = 'gestion_pagos';
    protected $primaryKey = 'id'; 

   	public function usuario()
    {
        return $this->belongsTo('App\Usuario','usuario_id','id_usuario');
    }
	public function agencia()
    {
        return $this->belongsTo('App\Agencias','id_agencia','id_agencia');
    }

    public function banco()
    {
        return $this->belongsTo('App\Banco','banco_id','id');
    }
    public function paymentmethod()
    {
        return $this->belongsTo('App\Paymentmethod','paymentmethod_id','paymentmethod_id');
    }
    public function currency()
    {
        return $this->belongsTo('App\Currency','currency_id','currency_id');
    }
    public function detallePago()
    {
       return $this->hasMany('App\DetallePagoOnline','id_gestion_pago','id'); 
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoFactour','estado_id','id');
    }

}