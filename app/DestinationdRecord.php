<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DestinationdRecord extends Model
{
	protected $table = 'destination_record';
	public $primarykey = 'destination_id';

}
