<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $table = 'tickets_offices';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
   

}