<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrasladoCangoroo extends Model
{
    protected $table = 'traslados_cangoroo';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


}