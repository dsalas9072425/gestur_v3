<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaNemoActividad extends Model
{
    protected $table = 'reservas_nemo_actividades';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
}