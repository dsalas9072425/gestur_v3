<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GastoOp extends Model
{
    protected $table = 'gastos_op';
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


    public function op_cabecera()
    {
      return $this->belongsTo('App\OpCabecera','id_op','id');
    }

    public function cuenta_contable()
    {
      return $this->belongsTo('App\PlanCuenta','id_cuenta_contable','id');
    }
    
    public function moneda()
    {
      return $this->belongsTo('App\Currency','id_moneda_gasto','currency_id');
    }

    
}
