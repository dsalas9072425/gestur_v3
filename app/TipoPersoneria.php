<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPersoneria extends Model
{
    protected $table = 'tipo_personeria';
    protected $primaryKey = 'id';  

}