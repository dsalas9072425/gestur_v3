<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DestinationdMappedProvider extends Model
{
	protected $table = 'destination_mapped_provider';
	public $primarykey = 'id_destination';

	public function reserva()
    {
       // return $this->belongsTo('App\Reserva', 'id_destino_dtp');
    }
}
