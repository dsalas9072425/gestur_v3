<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retencion extends Model
{
    protected $table = 'retencion';
    protected $primaryKey = 'id'; 
    public $timestamps = false;

    public function usuario()
    {
    	 return $this->belongsTo('App\Persona', 'id_usuario', 'id');
    }

    public function persona()
    {
    	 return $this->belongsTo('App\Persona', 'id_persona', 'id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Divisas', 'id_moneda', 'currency_id');
    }

    public function empresa()
    {
        return $this->belongsTo('App\Empresa', 'id_empresa', 'id');
    }

    public function libroVenta()
    {
        return $this->belongsTo('App\LibroVenta', 'id_libro_venta', 'id');
    }



    


}