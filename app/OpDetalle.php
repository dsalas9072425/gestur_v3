<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpDetalle extends Model
{
    protected $table = 'op_detalle';
    protected $primaryKey = 'id'; 
	public $timestamps = false;
	protected $dateFormat = 'Y-m-d H:i:s';

    public function opCabecera()
    {
        return $this->belongsTo('App\OpCabecera', 'id_cabecera','id');
    }

    public function libroCompra()
    {
        return $this->belongsTo('App\LibroCompra', 'id_libro_compra','id');
    }

}