<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerfilPaymentmethod extends Model
{
	protected $table = 'perfil_paymentmethod';
	public $primarykey = 'perfil_paymentmethod';

}
