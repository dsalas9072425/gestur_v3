<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaNemoHabitacion extends Model
{
    protected $table = 'reservas_nemo_habitaciones';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
}