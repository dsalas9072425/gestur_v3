<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TarjetaPersona extends Model
{
    protected $table = 'tarjeta_personas';
    public $timestamps = false;
    protected $fillable = ['id_banco_detalle','id_persona','created_at','updated_at'];
	protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get all of the banco_detalle for the TarjetaPersona
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function banco_detalle()
    {
        return $this->hasOne(BancoDetalle::class,'id','id_banco_detalle');
    }
}
