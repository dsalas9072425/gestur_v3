<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibroCompra extends Model
{
	protected $table = 'libros_compras';
	public $primarykey = 'id';
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';

	public function cliente()
    {
        return $this->belongsTo('App\Persona', 'id_persona','id');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Persona', 'id_usuario','id');
    }
    public function usuarioAnulacion()
    {
        return $this->belongsTo('App\Persona', 'id_usuario_anulacion','id');
    }

    public function tipoFactura()
    {
        return $this->belongsTo('App\TipoFactura', 'id_tipo_factura','id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Divisas', 'id_moneda','currency_id');
    }

    public function timbrado()
    {
       return $this->belongsTo('App\Timbrado', 'id_timbrado','id');
    }

    public function factura()
    {
        return $this->belongsTo('App\Factura', 'id_documento');
    }

    public function facturaDetalle()
    {
        return $this->belongsTo('App\FacturaDetalle', 'id_documento_detalle');
    }

    public function producto()
    {
        return $this->belongsTo('App\Producto', 'id_producto','id');
    }

    public function opDetalle()
    {
        return $this->hasMany('App\OpDetalle', 'id_libro_compra','id');
    }

    public function asiento()
    {
        return $this->belongsTo('App\Asiento', 'id_asiento','id');
    }

    public function notaCredito()
    {
        return $this->hasMany('App\LibroCompra', 'id_documento','id');
    }

    public function notaCreditoDetalle()
    {
        return $this->belongsTo('App\NotaCreditoDetalle', 'id_documento_detalle');
    }

    public function tipoDocumento()
    {
        return $this->belongsTo('App\TipoDocumento', 'id_tipo_documento');
    }

    public function sucursal()
    {
        return $this->belongsTo('App\Persona', 'id_sucursal','id');
    }

    public function centroCosto()
    {
        return $this->belongsTo('App\CentroCosto', 'id_centro_costo');
    }

    public function OpTimbrado(){
        return $this->belongsTo('App\OpTimbrado', 'id_timbrado');
    }

    public function cuentaContableIva10(){
        return $this->belongsTo('App\PlanCuenta', 'id_cuenta_gravada_10');
    }

    public function cuentaContableIva5(){
        return $this->belongsTo('App\PlanCuenta', 'id_cuenta_gravada_5');
    }

    public function cuentaContableExenta(){
        return $this->belongsTo('App\PlanCuenta', 'id_cuenta_exenta');
    }

    public function cuentaContableRetencionIva(){
        return $this->belongsTo('App\PlanCuenta', 'id_cuenta_contable_ret_iva');
    }

    public function cuentaContableRetencionRenta(){
        return $this->belongsTo('App\PlanCuenta', 'id_cuenta_contable_ret_renta');
    }

    public function tipoHechauka()
    {
        return $this->belongsTo('App\TipoDocumentoHechauka', 'id_tipo_documento_hechauka','id');
    }

    public function sucursal_empresa()
    {
        return $this->belongsTo('App\SucursalEmpresa', 'id_sucursal','id');
    }

}
