<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
    protected $table = 'cheques';
    protected $primaryKey = 'id'; 
    public $timestamps = false;

	public function banco()
    {
        return $this->belongsTo('App\Banco','banco_id','id');
    }

	public function currency()
    {
        return $this->belongsTo('App\Currency','id','currency_id');
    }

	public function tipo_cheque()
    {
        return $this->belongsTo('App\TipoCheque','tipo_cheques_id','id');
    }

	public function usuario()
    {
        return $this->belongsTo('App\Usuario','usuario_id','id_usuario');
    }

}