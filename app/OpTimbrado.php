<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpTimbrado extends Model
{
    protected $table = 'op_timbrados';
    protected $primaryKey = 'id'; 
	public $timestamps = false;
	// protected $dateFormat = 'Y-m-d H:i:s';

    public function proveedor()
    {
       return $this->belongsTo('App\Persona','id_persona','id');
    }


}