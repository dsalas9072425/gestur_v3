<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtaCtb extends Model
{
    protected $table = 'cta_ctb_forma_cobro';
    protected $primaryKey = 'id';  

    public function formaCobroCliente()
    {
        return $this->belongsTo('App\FormaCobroCliente','id_forma_cobro','id');
    }

    public function planCuenta()
    {
        return $this->belongsTo('App\PlanCuenta','id_cuenta_contable','id');
    }

    public function currency()
    {
        return  $this->belongsTo('App\Divisas', 'id_moneda', 'currency_id');
    }


}