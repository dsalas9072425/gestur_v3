<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BancoCabecera extends Model
{
    protected $table = 'banco_cabecera';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


    public function banco_detalle()
    {
      return $this->hasMany('App\BancoDetalle','id_banco');
    }

    public function usuario()
    {
      return $this->hasOne('App\Persona','id','id_usuario');
    }
   
    
}
