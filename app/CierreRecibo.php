<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CierreRecibo extends Model
{
    protected $table = 'cierre_recibos';
    public $timestamps = false;
	protected $primaryKey = 'cierre_recibos';

	public function recibo()
    {
       return $this->belongsTo('App\Recibo','id_recibo','id');
    }

    //relacion con cierre de caja
    public function cierreCaja()
    {
        return $this->belongsTo('App\Cierre','id_cierre','id');
    }


}