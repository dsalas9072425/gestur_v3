<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonaSet extends Model
{
    protected $table = 'personas_set';
    public $timestamps = false;

}