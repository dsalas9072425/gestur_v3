<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoTimbrado extends Model
{
    protected $table = 'tipo_timbrado';
    protected $primaryKey = 'id';  

}