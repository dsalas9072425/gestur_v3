<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoTicket extends Model
{
    protected $table = 'tipos_ticket';
    protected $primaryKey = 'id';  
    protected $fillable = [
            'descripcion',
            'id_empresa',
            
    ];
    public function empresa()
    {
        return $this->hasOne('App\Empresa', 'id','id_empresa');
    }
}