<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoWoo extends Model
{
    protected $table = 'pedidos_woo';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


    // public function cuenta_contable()
    // {
    //   return $this->belongsTo('App\PlanCuenta','id_cuenta_contable');
    // }

    // public function banco_cabecera()
    // {
    //   return $this->belongsTo('App\BancoCabecera','id_banco');
    // }

    // public function banco_cab()
    // {
    //   return $this->hasOne('App\BancoCabecera','id','id_banco');
    // }

    // public function currency()
    // {
    //   return $this->hasOne('App\Divisas','currency_id','id_moneda');
    // }
    
    public function status()
    {
      return $this->belongsTo('App\EstadoPedidoWoo','status','name');
    }
    
}
