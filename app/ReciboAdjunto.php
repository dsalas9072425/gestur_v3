<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReciboAdjunto extends Model
{
    protected $table = 'recibo_comprobantes';
    protected $primaryKey = 'id';  
    public $timestamps = false;
}