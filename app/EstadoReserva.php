<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoReserva extends Model
{
    protected $table = 'estado_reserva';
    public $timestamps = false;
}