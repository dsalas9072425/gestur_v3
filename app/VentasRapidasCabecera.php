<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentasRapidasCabecera extends Model
{
    protected $table = 'ventas_rapidas_cabecera';
    protected $primaryKey = 'id'; 
    public $timestamps = false;

    public function cliente()
    {
        return $this->hasOne('App\Persona','id','id_cliente');
    }

    public function currency()
    {
        return $this->hasOne('App\Currency','currency_id','id_moneda');
    }

    public function vendedor()
    {
        return $this->hasOne('App\Persona','id','id_vendedor');
    }

    public function vendedor_agencia()
    {
        return $this->hasOne('App\Persona','id','id_vendedor_agencia');
    }

    public function ventasDetalle()
    {
        return $this->hasMany('App\VentasRapidasDetalle','id_venta_cabecera','id');
    }
    
    public function factura()
    {
        return $this->belongsTo('App\Factura', 'id_factura');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoFactour', 'id_estado');
    }
    public function pedido()
    {
        return $this->belongsTo('App\PedidoWoo', 'id_pedido');
    }
    public function zona()
    {
        return $this->belongsTo('App\Zona', 'zona_id');
    }
    public function formaPago()
    {
        return $this->belongsTo('App\FormaCobroCliente', 'id_forma_pago');
    }
    public function comercio()
    {
        return $this->belongsTo('App\ComercioPersona', 'id_comercio_empresa');
    }

    public function tipoVenta()
    {
        return $this->belongsTo('App\TipoVenta', 'id_tipo_venta_rapida');
    }

}