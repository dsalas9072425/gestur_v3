<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';
    protected $primaryKey = 'id';
    public $timestamps = false; 
    protected $dateFormat = 'Y-m-d H:i:s';

   public function personas() {
    	return $this->belongsTo('App\Persona', 'id_persona');
    	//return $this->belongsToMany('App\Persona', 'producto_persona');
   }

  public function tipoImpuesto() {
    return $this->belongsTo('App\TipoImpuesto', 'id_tipo_impuesto');
  }
  
  public function sucursales() {
    return $this->belongsTo('App\SucursalEmpresa', 'id_sucursal', 'id_persona');
  }

}