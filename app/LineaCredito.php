<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineaCredito extends Model
{
    protected $table = 'linea_de_credito';

    protected $primaryKey = 'id';  
    public $timestamps = false;



     public function persona()
    {
       return $this->belongsTo('App\Persona','id_persona','id');
    }




}