<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuTipoPersona extends Model
{
	public function menu()
    {
		return $this->belongsTo('App\Menu', 'id_menu', 'id')->orderBy('orden');
	}

	public function tipoPersonas()
    {
		return $this->belongsTo('App\TipoPersona', 'id_tipo_persona');
	}
 
}