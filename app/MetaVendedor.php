<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class MetaVendedor extends Model
{
    protected $table = 'meta_vendedores';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;

    protected $fillable = [
      'id_agencia',
      'id_vendedor',
      'id_moneda',
      'meta',
      'id_usuario',
      'meta',
      'activo',
      'fecha',
      'anho_mes_desde',
      'anho_mes_hasta'
  ];

    
    public function vendedor()
    {
      return $this->belongsTo('App\Persona','id_vendedor','id');
    }
    
}
