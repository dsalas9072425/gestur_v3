<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursales';
    protected $primaryKey = 'id_sucursal_agencia'; 
    public $timestamps = false;
    public function agencia()
    {
        return $this->belongsTo('App\Agencias','id_agencia','id_agencia');
    }


}