<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservasVuelos extends Model
{
    protected $table = 'reservas_vuelos';
	protected $primarykey = 'id';
	//protected $fillable = ['id_reserva', 'codigo_reserva_proveedor','id_file_reserva'];

	   public function usuario()
    {
        return $this->belongsTo('App\Usuario','usuario_id','id_usuario');
    }

        public function agencia()
    {
        return $this->belongsTo('App\Agencias','agencia_id','id_agencia');
    }

        public function itinerarios()
    {
        return $this->hasMany('App\Itinerarios','reservas_vuelo_id', 'id');
    }

        public function pasajeros()
    {
        return $this->hasMany('App\Pasajeros','reserva_id', 'id');
    }

}
