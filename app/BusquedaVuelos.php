<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusquedaVuelos extends Model
{
    protected $table = 'busqueda_vuelos';

    public function usuario()
    {
        return $this->belongsTo('App\Usuario','id_usuario','id_usuario');
    }

}