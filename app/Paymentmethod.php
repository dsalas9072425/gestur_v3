<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paymentmethod extends Model
{
	protected $table = 'paymentmethod';
	public $primarykey = 'paymentmethod_id';

}
