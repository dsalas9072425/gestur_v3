<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parametros extends Model
{
    protected $table = 'parametros';
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
}