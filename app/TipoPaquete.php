<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPaquete extends Model
{
    protected $table = 'tipo_paquete';
    protected $primaryKey = 'id'; 

}