<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaPagoCliente extends Model
{
    protected $table = 'forma_pago_cliente';
    protected $primaryKey = 'id'; 
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
    
       //Define que formas de cobro se puede utilizar para selccionar cuenta y banco, tambien sirve como listado 
       public const SELECTCUENTA = [
        1 => true, //Efectivo
        2 => false, //Tarjeta de Credito Empresa
        3 => false, //Tarjeta de Credito Aerolinea
        4 => false, //Cheque
        5 => false, //Deposito
        6 => false, //Transferencia
        7 => false, //Retención 
        8 => false, //Factura 
        9 => false, //Anticipo 
        10 => true, //Canje 
        11 => false, //Tarjeta de Crédito 
        12 => true, //Canje 2
    ];

}