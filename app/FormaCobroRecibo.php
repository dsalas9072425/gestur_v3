<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaCobroRecibo extends Model
{
    protected $table = 'forma_cobro_recibo_cabecera';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


    public function fp_detalle()
    {
      return  $this->hasMany('App\FormaCobroReciboDetalle','id_cabecera');
    }

    public function recibo()
    {
        return $this->hasOne('App\Recibo','id','id_recibo');
    }

    
}
