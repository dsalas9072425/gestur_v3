<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpCabecera extends Model
{
    protected $table = 'op_cabecera';
    protected $primaryKey = 'id'; 
	public $timestamps = false;
	protected $dateFormat = 'Y-m-d H:i:s';


    public function currency()
    {
        return $this->belongsTo('App\Divisas', 'id_moneda','currency_id');
    }

    public function proveedor()
    {
        return $this->belongsTo('App\Persona', 'id_proveedor','id');
    }

     public function opDetalle()
    {
        return $this->hasMany('App\OpDetalle', 'id_cabecera','id');
    }





}