<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoComentariosProforma extends Model
{
    protected $table = 'historico_comentarios_proforma';
    protected $primaryKey = 'id'; 
    protected $dateFormat = 'Y-m-d H:i:s';
	public $timestamps = false;


	 public function persona()
    {
       return $this->belongsTo('App\Persona','id_usuario','id');
    }

    public function motivoAutorizacion()
    {
        
        return $this->belongsTo(MotivoAutorizacion::class, 'id_motivo_autorizacion', 'id');
    }



}