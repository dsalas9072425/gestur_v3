<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotivoAutorizacion extends Model
{
    protected $table = 'motivos_autorizacion_proforma';
}
