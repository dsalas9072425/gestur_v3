<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentaCorrienteEmpresa extends Model
{
	protected $table = 'cuentas_contables_empresa';
	public $primarykey = 'id';
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';
 }   