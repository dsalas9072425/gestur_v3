<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroCosto extends Model
{
    protected $table = 'centro_costo';
    protected $primaryKey = 'id'; 
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;

    public function usuario()
    {
      return $this->hasOne('App\Persona','id','id_usuario');
    }


}