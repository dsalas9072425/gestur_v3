<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaNemo extends Model
{
    protected $table = 'reservas_nemo';
    protected $primaryKey = 'id'; 
    public $timestamps = false;

	public function proveedor(){
		return $this->belongsTo('App\Persona', 'supplier_id', 'id');
	}

	public function prestador(){
		return $this->belongsTo('App\Persona', 'lender_id', 'id');
	}


}