<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EscalaComision extends Model
{
    protected $table = 'escala_comision_cabecera';
    public $timestamps = false;
	protected $primaryKey = 'id';

	public function vendedor()
    {
      return $this->belongsTo('App\Persona','id_vendedor','id');
    }

    public function escalaDetalle()
    {
      return $this->hasMany('App\EscalaComisionDetalle', 'id_cabecera', 'id');
    }



}