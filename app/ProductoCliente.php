<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoCliente extends Model
{
    protected $table = 'productos_clientes';
    protected $primaryKey = 'id'; 
    public $timestamps = false;



public function persona()
    {
       return $this->belongsTo('App\Persona','id_cliente','id');
    }

public function producto()
    {
       return $this->belongsTo('App\Producto','id_producto','id');
    }

}