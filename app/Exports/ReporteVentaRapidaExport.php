<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteVentaRapidaExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte Venta Rapida';
    }

    public function headings(): array
    {
        return [
            'NRO. FACTURA',
            'VENDEDOR',
            'NRO. VTA. RAPIDA',
            'PROVEEDOR',
            'DESCRIPCION ITEM',
            'CANTIDAD',
            'COSTO',
            'VENTA',
            'RENTA',
            'CLIENTE',
            'FECHA FACTURA',
            'ESTADO'
        ];
    }
}
