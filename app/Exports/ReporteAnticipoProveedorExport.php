<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteAnticipoProveedorExport implements FromArray, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function headings(): array
    {
        return [
            'N° Anticipo',
            'OP',
            'Fecha Anticipo',
            'OP Aplicado',
            'Proveedor',
            'Venta/Grupo/Proforma',
            'Importe',
            'Moneda',
            'Saldo',
            'Estado',
            'Fecha Vencimiento'
        ];
    }
}
