<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PagoProveedorExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Pago Proveedor';
    }

    public function headings(): array
    {
        return [
            'Provedor',
            'Fecha Proveedor',
            'Fecha Gasto',
            'Factura/Documento',
            'Checkin',
            'Tipo',
            'Pagado',
            'Pasajero',
            'Moneda',
            'Saldo',
            'Codigo RSVA',
            'Producto',
            'Monto TTL RSVA',
            'Proforma',
            'Fecha Facturacion',
            'Vendedor',
            'Origen',
        ];
    }
}
