<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteBSPExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Reporte BSP';
    }

    public function headings(): array
    {
        return [
            'CIA', 'TRNC', 'DOCUMENTO', 'EMISION', 'FOP', 'VALOR TRANSACCION', 'VALOR TARIFA', 'IMP', 'YQ', 'PY', 'PEN', '%', 'COMISION', 'COM', 'NETO A PAGAR'
        ];
    }
}
