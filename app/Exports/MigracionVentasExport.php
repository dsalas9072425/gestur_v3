<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MigracionVentasExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Ventas';
    }

    public function headings(): array
    {
        return [
            'ven_tipimp', 'ven_gra05', 'ven_iva05', 'ven_disg05', 'cta_iva05', 'ven_rubgra', 'ven_rubg05', 'ven_disexe', 'ven_numero', 'ven_imputa', 'ven_sucursal',
            'generar', 'form_pag', 'ven_centro', 'ven_provee', 'ven_cuenta', 'ven_prvnom', 'cta_tipofa', 'ven_fecha', 'ven_totfac', 'ven_exenta', 'ven_gravad', 'ven_iva',
            'ven_retenc', 'ven_aux', 'ven_ctrl', 'ven_con', 'ven_cuota', 'ven_fecven', 'cant_dias', 'origen', 'cambio', 'valor', 'moneda', 'exen_dolar', 'concepto', 'cta_iva',
            'cta_caja', 'tkdesde', 'tkhasta', 'caja', 'ven_disgra', 'forma_devo', 'ven_cuense', 'anular', 'reproceso', 'cuenta_exe', 'usu_ide', 'timbrado', 'clieasi',
            'ventirptip', 'ventirpgra', 'ventimporteirp', 'ventirpexe', 'irpc', 'ivasimplificado', 'ven_gdc', 'venrubrogdc', 'ven_gdccosto', 'ven_gdcventa', 'ven_gdcori',
            'en_nronc', 'ven_nctim', 'ventipcli', 'ven_banctacte', 'ven_bannom'
        ];
    }
}
