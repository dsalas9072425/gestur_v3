<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ListadoNotaDeCreditoExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Listado Nota de Credito';
    }

    public function headings(): array
    {
        return [
            'Nro', 'Fecha', 'Factura', 'Monto', 'Saldo', 'Moneda', 'Cliente', 'Vendedor', 'Estado', 'Tipo'
        ];
    }
}
