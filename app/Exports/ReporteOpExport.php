<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteOpExport implements FromArray, WithHeadings
{
    use Exportable;

    protected $cuerpo;

    public function __construct($cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        $data = [['OP', 'Tipo', 'Fecha Creación', 'Proveedor', 'Moneda', 'Forma Pago', 'Nro Comprobantes', 'Cuenta Fondo', 'Monto', 'Estado']];
        return array_merge($data, $this->cuerpo);
    }

    public function headings(): array
    {
        return [];
    }
}
