<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteVentaPrestadorExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte Venta Prestador';
    }

    public function headings(): array
    {
        return [
            'PROVEEDOR',
            'PRESTADOR',
            'FECHA FACT.',
            'NUMERO FACT.',
            'CLIENTE',
            'TELEFONO',
            'EMAIL',
            'NUMERO PROFORMA',
            'ITEM/DESCRIPCION',
            'PRODUCTO',
            'COD. RESERVA',
            'VENDEDOR EMPRESA',
            'VENDEDOR AGENCIA',
            'FECHA IN',
            'DESTINO FACTURA',
            'MONEDA VENTA',
            'VENTA',
            'MONEDA COSTO',
            'COSTO',
            'RENTA',
            'FECHA GASTO'
        ];
    }
}
