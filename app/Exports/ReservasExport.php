<?php

namespace App\Exports;

use App\Reserva; // Asegúrate de importar el modelo correcto en caso de ser necesario
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ListadoReservasExport implements FromCollection, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function title(): string
    {
        return 'Listado Reservas';
    }

    public function headings(): array
    {
        return [
            'Booking ID',
            'Servicio ID',
            'Codigo',
            'Proforma',
            'Estado Gestur',
            'Estado Cangoroo',
            'Producto',
            'Proveedor',
            'Moneda C',
            'Costo',
            'Moneda V',
            'Venta',
            'Cliente',
            'Vendedor',
            'Usuario',
            'Check-in',
            'Pasajero',
        ];
    }
}
