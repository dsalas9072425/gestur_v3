<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteFacturasPendientesExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Facturas Pendientes';
    }

    public function headings(): array
    {
        return [
            'descripcion', 'cliente', 'fecha_emision', 'vencimiento', 'check_in', 'nro_factura', 'pasajero', 'usuario', 'moneda', 'saldo', 'dias', 'estado', 'comercio'
        ];
    }
}
