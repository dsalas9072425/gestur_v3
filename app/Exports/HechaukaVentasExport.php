<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class HechaukaVentasExport implements FromCollection, WithHeadings, WithTitle
{
    protected $cuerpo;
    protected $cabeceras;

    public function __construct($cuerpo, $cabeceras)
    {
        $this->cuerpo = $cuerpo;
        $this->cabeceras = $cabeceras;
    }

    public function collection()
    {
        return collect($this->cuerpo);
    }

    public function headings(): array
    {
        return $this->cabeceras;
    }

    public function title(): string
    {
        return 'Hechauka Ventas';
    }
}