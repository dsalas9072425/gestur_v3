<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MigracionAsientosExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Migración de asientos';
    }

    public function headings(): array
    {
        return [
            'asi_id', 'asi_lin', 'asi_nom', 'asi_fec', 'asi_suc', 'asi_cen', 'asi_doc', 'asi_com', 'asi_tip', 'asi_cue',
            'asi_con', 'asi_deb', 'asi_cre', 'asi_cmn', 'asi_mon', 'asi_imp', 'asi_asi', 'asi_lib', 'asi_ctl', 'asi_aux',
            'asi_origen', 'nuevo', 'asi_memo', 'comentario', 'asi_ord', 'asi_usu'
        ];
    }
}
