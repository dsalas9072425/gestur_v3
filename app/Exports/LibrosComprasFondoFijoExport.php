<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LibrosComprasFondoFijoExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;
    protected $titulo;

    public function __construct(array $cuerpo, string $titulo)
    {
        $this->cuerpo = $cuerpo;
        $this->titulo = $titulo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return $this->titulo;
    }

    public function headings(): array
    {
        return [
            'DÍA',
            'DOCUMENTO NÚMERO',
            'R. SOCIAL / APELL./NOMB',
            'R.U.C',
            'GRAV. 10%',
            'GRAV. 5%',
            'IVA 10%',
            'IVA 5%',
            'EXENTAS',
            'TOTAL',
            'TIMBRADO'
        ];
    }
}
