<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportePersonasExport implements FromArray, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function headings(): array
    {
        if ($this->getIdEmpresa() == 1) {
            return [
                'Empresa / Agencia',
                'Nombre/Razon Social',
                'Tipo Persona',
                'Correo',
                'Denominación Comercial',
                'Documento/RUC',
                'Gestur',
                'Online'
            ];
        } else {
            return [
                'Empresa / Agencia',
                'Nombre/Razon Social',
                'Tipo Persona',
                'Correo',
                'Denominación Comercial',
                'Documento/RUC',
                'Gestur'
            ];
        }
    }
}
