<?php 

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MigracionCompraExport implements FromArray, WithHeadings
{
    protected $cuerpo;
    protected $request;

    public function __construct($cuerpo, $request)
    {
        $this->cuerpo = $cuerpo;
        $this->request = $request;
    }

    public function array(): array
    {
        $cabeceras = [
            ['com_numero', 'generar', 'cta_iva', 'cta_caja', 'form_pag', 'com_imputa', 'com_sucurs', 'com_centro', 'com_provee', 'com_cuenta', 'com_prvnom', 'com_tipofa', 'com_fecha', 'com_totfac', 'com_exenta', 'com_gravad', 'com_iva', 'com_import', 'com_aux', 'com_con', 'com_cuota', 'com_fecven', 'com_ordenp', 'cant_dias', 'concepto', 'moneda', 'cambio', 'valor', 'origen', 'exen_dolar', 'com_disexe', 'com_disgra', 'forma_devo', 'retencion', 'porcentaje', 'reproceso', 'cuenta_exe', 'com_tipimp', 'com_gra05', 'com_iva05', 'com_disg05', 'cta_iva05', 'com_rubgra', 'com_rubg05', 'com_ctag05', 'com_rubexe', 'com_saldo', 'com_timbra', 'com_gasinv', 'com_monto', 'com_rubro', 'com_gdc', 'com_gdcimp', 'com_irpc', 'com_ireimp', 'no_imputa', 'no_imputa5', 'com_nro_nc', 'com_nc_tim', 'com_banctacte', 'com_bannom']
        ];

        return array_merge($cabeceras, $this->cuerpo);
    }

    public function headings(): array
    {
        return [
            'com_numero', 'generar', 'cta_iva', 'cta_caja', 'form_pag', 'com_imputa', 'com_sucurs', 'com_centro', 'com_provee', 'com_cuenta', 'com_prvnom', 'com_tipofa', 'com_fecha', 'com_totfac', 'com_exenta', 'com_gravad', 'com_iva', 'com_import', 'com_aux', 'com_con', 'com_cuota', 'com_fecven', 'com_ordenp', 'cant_dias', 'concepto', 'moneda', 'cambio', 'valor', 'origen', 'exen_dolar', 'com_disexe', 'com_disgra', 'forma_devo', 'retencion', 'porcentaje', 'reproceso', 'cuenta_exe', 'com_tipimp', 'com_gra05', 'com_iva05', 'com_disg05', 'cta_iva05', 'com_rubgra', 'com_rubg05', 'com_ctag05', 'com_rubexe', 'com_saldo', 'com_timbra', 'com_gasinv', 'com_monto', 'com_rubro', 'com_gdc', 'com_gdcimp', 'com_irpc', 'com_ireimp', 'no_imputa', 'no_imputa5', 'com_nro_nc', 'com_nc_tim', 'com_banctacte', 'com_bannom'
        ];
    }
}

