<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteCorteProveedoresExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Corte Proveedores';
    }

    public function headings(): array
    {
        return [
            'Fecha Venta', 'Hora Corte', 'Numero Pedido', 'Proveedor', 'Producto', 'Sku', 'Cantidad', 'Precio Costo',
            'Numero Venta', 'Estado Venta'
        ];
    }
}
