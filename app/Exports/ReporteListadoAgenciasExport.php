<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteListadoAgenciasExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte Listado Agencias';
    }

    public function headings(): array
    {
        return [
            'id_vendedor_agencia',
            'agencia',
            'meta_anual',
            'anho',
        ];
    }
}
