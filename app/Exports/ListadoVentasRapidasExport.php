<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ListadoVentasRapidasExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Listado Ventas Rapidas';
    }

    public function headings(): array
    {
        return [
            'Venta', 'Monto_Venta', 'Moneda', 'Estado', 'Factura', 'Fecha', 'Cliente', 'Vendedor_Empresa', 'Vendedor_Agencia', 'Tipo_Venta', 'Pedido', 'Origen'
        ];
    }
}
