<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteMetaAgenciasExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte Meta Agencias';
    }

    public function headings(): array
    {
        return [
            'COD AGENCIA',
            'AGENCIA',
            'MES',
            'OBJETIVO',
            'REALIZADO',
            'PERIODO'
        ];
    }
}
