<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteMasivoPersonasExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte Masivo Personas';
    }

    public function headings(): array
    {
        return [
            'nombre',
            'email',
            'celular',
            'fecha_nacimiento',
            'telefono',
            'id_tipo_persona',
            'id_tipo_identidad',
            'id_pais',
            'direccion',
            'denominacion_comercial',
            'apellido',
            'id_ciudad',
            'documento_identidad',
            'id_plazo_pago',
            'correo_comerciales',
            'correo_administrativo',
            'agente_retentor',
            'cheque_emisor_txt',
            'retentor',
            'porcentaje_retencion',
            'retener_iva',
            'dv',
            'aplica_iva',
            'id_empresa',
        ];
    }
}
