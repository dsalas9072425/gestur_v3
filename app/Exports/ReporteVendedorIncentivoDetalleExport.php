<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;

class ReporteVendedorIncentivoDetalleExport implements FromArray, WithTitle
{
    protected $cuerpo;
    protected $detalle;

    public function __construct(array $cuerpo, $detalle)
    {
        $this->cuerpo = $cuerpo;
        $this->detalle = $detalle;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Reporte Detalle-';
    }
}

