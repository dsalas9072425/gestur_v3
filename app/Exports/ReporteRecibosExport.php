<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteRecibosExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Recibos';
    }

    public function headings(): array
    {
        return [
            'Agencia', 'Recibo N°', 'Monto', 'Tipo', 'Estado', 'Moneda', 'Fecha', 'Concepto', 'Fecha Confirmación', 'Usuario', 'Sucursal'
        ];
    }
}
