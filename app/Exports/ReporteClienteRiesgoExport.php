<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteClienteRiesgoExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Reporte Riesgo';
    }

    public function headings(): array
    {
        return [
            'CLIENTE', 'RUC', 'CREDITO', 'VENCIDAS', 'A VENCER', 'EN GASTOS', 'SIN GASTOS', 'SALDO'
        ];
    }
}
