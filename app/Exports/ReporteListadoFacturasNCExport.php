<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteListadoFacturasNCExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte Listado Facturas -NC';
    }

    public function headings(): array
    {
        return [
            'Nro. Factura',
            'Nro. Nota Credito',
            'Fecha Facturacion',
            'Total Factura',
            'Total Nota Credito',
            'Saldo Factura',
            'Saldo Nota Credito',
            'Valor Nota Credito Aplicado',
            'Moneda',
            'Cliente',
        ];
    }
}
