<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteMovimientoCuentaExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Reporte Movimiento Cuenta';
    }

    public function headings(): array
    {
        return [
            'Fecha Emisión', 'Fecha Operación', 'Entidad', 'Nro. Cuenta', 'Moneda', 'Operación', 'Id Documento', 'Debe', 'Haber', 'Documento', 'Suc', 'Concepto'
        ];
    }
}
