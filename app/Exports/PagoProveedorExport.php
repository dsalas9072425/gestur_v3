<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PagoProveedorExport implements FromArray, WithTitle, ShouldAutoSize
{
    use Exportable;

    protected $cuerpo;
    protected $req;

    public function __construct($cuerpo, $req)
    {
        $this->cuerpo = $cuerpo;
        $this->req = $req;
    }

    public function array(): array
    {
        $cabeceras = [
            ['Proveedor', 'Fecha Proveedor', 'Fecha Gasto', 'Factura/Documento', 'Checkin', 'Tipo', 'Pagado', 'Pasajero', 'Moneda', 'Saldo', 'Codigo RSVA', 'Producto', 'Monto TTL RSVA', 'Saldo Documento', 'Proforma', 'Fecha Facturacion', 'Vendedor', 'Origen', 'Pais']
        ];

        $data = array_merge($cabeceras, $this->cuerpo);

        return $data;
    }

    public function title(): string
    {
        return 'Pago Proveedor';
    }
}
