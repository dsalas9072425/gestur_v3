<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteHistoricoProformasExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte historico proforma';
    }

    public function headings(): array
    {
        return [
            'PROFORMA',
            'CREADO POR',
            'FECHA DE CREACION',
            'FECHA DE MODIFICACION',
            'USUARIO MODIFICACION',
            'ESTADO ACTUAL',
            'DIAS DESDE LA CREACION'
        ];
    }
}
