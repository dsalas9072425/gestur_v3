<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteTicketsExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Compras';
    }

    public function headings(): array
    {
        return [
            'Ticket', 'Cliente', 'Pasajero', 'Aerolínea', 'Emisión', 'Usuario', 'Proforma', 'Check In-Out', 'Usuario Proforma', 'TC', 'Pago BSP', 'Tipo', 'Origen', 'Estado', 'Factura', 'PNR', 'Moneda', 'Costo', 'Venta', 'Pedido', 'Origen', 'Comision', '%Renta', 'R.Bruto', 'R.Extra', 'Incentivo', 'R.Neta'
        ];
    }
}
