<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteAnticiposV2Export implements FromArray, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function headings(): array
    {
        return [
            'Beneficiario',
            'Asiento N°',
            'Recibo N°',
            'Concepto',
            'Importe',
            'Moneda',
            'Estado',
            'Proforma',
            'Usuario Proforma',
            'Grupo',
            'Fecha Pago',
            'Saldo'
        ];
    }
}
