<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class CostoVentaPendientes implements FromCollection, WithHeadings, WithTitle
{
    protected $cuerpo;

    public function __construct($cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function collection()
    {
        return collect($this->cuerpo);
    }

    public function headings(): array
    {
        return ['Fecha','Día','Documento','R.U.C.','Proveedor','Gravadas 10%','Gravadas 5%','IVA 10%','IVA 5%','Exentas','Total','Timbrado','Tipo','Proforma','Origen'];
    }

    public function title(): string
    {
        return 'Compras';
    }
}