<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteTicketsPendientesExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Ventas';
    }

    public function headings(): array
    {
        return [
            'proforma', 'factura', 'fecha', 'cliente', 'moneda', 'monto', 'saldoF', 'saldoT', 'senha'
        ];
    }
}
