<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteListadoPlanCuentasExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte Listado Cuentas';
    }

    public function headings(): array
    {
        return [
            'NUMERO DE CUENTA',
            'CUENTA',
            'DENOMINACION',
            'TOTAL DEBE',
            'TOTAL HABER',
            'SALDO DEBE',
            'SALDO HABER',
        ];
    }
}
