<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class HechaukaCompraExport implements FromCollection, WithHeadings
{
    protected $cuerpo;
    protected $cabeceras;

    public function __construct($cuerpo, $cabeceras)
    {
        $this->cuerpo = $cuerpo;
        $this->cabeceras = $cabeceras;
    }

    public function collection()
    {
        return collect($this->cuerpo);
    }

    public function headings(): array
    {
        return $this->cabeceras;
    }
}