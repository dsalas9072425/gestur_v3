<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class ReporteResumenCuentaExport implements FromArray, WithHeadings, WithTitle
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function headings(): array
    {
        return [
            'Cliente', 'Moneda', 'Importe', 'Saldo'
        ];
    }

    public function title(): string
    {
        return 'Reporte Resumen Cuenta';
    }
}
