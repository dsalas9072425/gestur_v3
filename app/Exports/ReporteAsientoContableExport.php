<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteAsientoContableExport implements FromArray, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Fecha',
            'Concepto',
            'Documento',
            'Operación',
            'Debe',
            'Haber',
            'Usuario',
            'Balanceado',
            'Anulado'
        ];
    }
}
