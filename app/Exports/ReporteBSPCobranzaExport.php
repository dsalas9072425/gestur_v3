<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteBSPCobranzaExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Reporte BSP Cobranza';
    }

    public function headings(): array
    {
        return [
            'Estado Cobro', 'Nro Fact.', 'Proforma', 'Cliente Ag.', 'Nro. Tickets', 'Nombre Pax', ' Neto a Pagar', 'Usuario Pedido', 'Usuario Proforma', ' Void', 'Seña', 'Saldo Seña', 'Usuario Emision', 'Fecha Emision', 'Periodo BSP', 'Moneda', 'Tipo Cambio'
        ];
    }
}
