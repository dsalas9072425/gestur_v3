<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ListadoFacturasExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Listado Facturas';
    }

    public function headings(): array
    {
        return [
            'Id',
            'Factura',
            'Fecha',
            'Check In',
            'Vencimiento',
            'Proforma',
            'Estado',
            'Monto',
            'Saldo',
            'Moneda',
            'Cliente',
            'Pasajero',
            'Usuario',
            'Destino',
            'Comisión',
            '% Renta',
            'Renta Bruta',
            'Renta Extra',
            'Incentivo',
            'Renta Neta',
            'Vendedor',
            'Sucursal',
            'Cotizado',
            'Negocio',
            'Saldo',
            'Autorización',
            'Asistente',
            'Equipo',
            'Promoción'
        ];
    }
}
