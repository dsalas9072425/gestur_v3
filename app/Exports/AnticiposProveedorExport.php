<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AnticiposProveedorExport implements FromArray, WithHeadings
{
    protected $cuerpo;
    protected $detalle;

    public function __construct(array $cuerpo, $detalle)
    {
        $this->cuerpo = $cuerpo;
        $this->detalle = $detalle;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function headings(): array
    {
        return [
            null,
            null,
            null,
            null,
            null,
            'Nombre: ' . $this->detalle->cliente,
            null,
            null,
            null,
            'Fecha: ' . $this->detalle->fechas,
            null
        ];
    }
}
