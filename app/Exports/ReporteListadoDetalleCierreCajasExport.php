<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteListadoDetalleCierreCajasExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte Listado Detalle Cierre de Cajas';
    }

    public function headings(): array
    {
        return [
            'Nro. Recibo',
            'Cliente',
            'Usuario',
            'Forma de Cobro',
            'Estado',
            'Fecha Cierre',
            'Moneda',
            'Total',
        ];
    }
}
