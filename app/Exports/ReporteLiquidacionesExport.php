<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteLiquidacionesExport implements FromArray, WithTitle, WithHeadings
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function title(): string
    {
        return 'Reporte Liquidación Comisiones';
    }

    public function headings(): array
    {
        return [
            '',
            '',
            '',
            '',
            '',
            'Reporte Liquidación Comisiones',
            '',
            '',
            '',
            '',
            ''
        ];
    }
}
