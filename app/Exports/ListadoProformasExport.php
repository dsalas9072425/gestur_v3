<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ListadoProformasExport implements FromArray, WithTitle, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function title(): string
    {
        return 'Listado Propietarios';
    }

    public function headings(): array
    {
        return [
            'Postergación',
            'Proforma',
            'Estado',
            'Factura',
            'Fecha Proforma',
            'Check-In',
            'Cliente',
            'Vendedor',
            'Pasajero',
            'Moneda Venta',
            'Monto Total',
            '% Renta',
            'Operativo',
            'Usuario',
            'Destino',
            'Negocio',
            'Equipo',
            'Reconfirmacion'
        ];
    }
}
