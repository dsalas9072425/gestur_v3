<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReporteProductosExport implements FromArray, WithHeadings
{
    protected $cuerpo;

    public function __construct(array $cuerpo)
    {
        $this->cuerpo = $cuerpo;
    }

    public function array(): array
    {
        return $this->cuerpo;
    }

    public function headings(): array
    {
        return [
            'Denominación',
            'Grupo',
            'Precio Costo',
            'Código Producto',
            'Cuenta Contable',
            'Voucher',
            'Multiple Voucher',
            'Incentivo',
            'Comisionable',
            'Comisiona Solo',
            'Comision Base',
            'Imprimir en Fact.',
            'Visible',
            'Estado'
        ];
    }
}
