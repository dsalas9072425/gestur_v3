<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActividadesPromo extends Model
{
    protected $table = 'actividades_promo';
   public $timestamps = false;
}