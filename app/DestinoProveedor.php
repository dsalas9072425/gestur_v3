<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DestinoProveedor extends Model
{
    protected $table = 'destinos_proveedor';
	protected $primaryKey = 'id';
	
	
	public function destinosDtpmundo()
    {
        return $this->belongsToMany('App\DestinoProveedor', 'destinos_proveedor_dtpmundo', 'id_destino', 'id_destino_dtpmundo');
    }
}
