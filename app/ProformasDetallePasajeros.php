<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProformasDetallePasajeros extends Model
{
    protected $table = 'proformas_detalle_pasajeros';
    protected $primaryKey = 'id'; 
    public $timestamps = false;

}