<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CierreResumen extends Model
{
    protected $table = 'cierre_resumen';
    public $timestamps = false;
	  protected $primaryKey = 'cierre_resumen';

	public function moneda()
    {
       return $this->belongsTo('App\Currency','moneda_id','currency_id');
    }

	public function formaCobro()
    {
       return $this->belongsTo('App\FormaCobroCliente','forma_cobro_id','id');
    }

  public function cierre()
    {
       return $this->belongsTo('App\Cierre','id_cierre','id');
    }
}