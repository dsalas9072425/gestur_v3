-- FUNCTION: public.anular_op(integer, integer, integer)

-- DROP FUNCTION IF EXISTS public.anular_op(integer, integer, integer);

CREATE OR REPLACE FUNCTION public.anular_op(
	p_id_op integer,
	p_id_usuario integer,
	p_option integer)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

DECLARE
	v_id integer;
	v_total_pago double precision;
	v_id_moneda integer;
	v_id_proveedor integer;
	v_anular_permiso_persona integer;
	c_op_detalle CURSOR FOR SELECT * FROM op_detalle WHERE id_cabecera = p_id_op;
	v_id_estado integer;
	cant_libro_compra_op integer;
	v_hay_anticipo integer;
	v_importe_anticipos double precision;
	v_saldo_anticipos double precision;
	v_anticipo integer;
	
	v_importe_total_pago double precision;
	v_id_cuenta_contable_banco integer;
	v_concepto character varying;
	v_id_sucursal_contable integer;
	v_id_centro_costo integer;
	v_cotizacion_op_cabecera double precision;
	v_id_banco_detalle integer; 
	v_nro_comprobante character varying;
	v_fecha_emision date; 
	v_fecha_vencimiento date;
	v_origen character varying;
BEGIN

--MODIFICADO POR PAULO 10/03/2020
	
	--OBTENER PERMISO DEL USUARIO
     SELECT COUNT(*)
 	 INTO v_anular_permiso_persona
     FROM permisos_especiales pe,
     persona_permiso_especiales ppe
     WHERE pe.id = ppe.id_permiso
     AND ppe.id_persona = p_id_usuario
     AND pe.accion = 'anular_op';
	
 	IF v_anular_permiso_persona = 0 THEN 
 		RETURN 'No puede anular la OP. No tiene los permisos necesarios';
 	END IF;
	
	SELECT count(*) 
	INTO v_hay_anticipo
	FROM anticipos 
	WHERE id_op = p_id_op AND activo = true;
	
	IF v_hay_anticipo > 0 THEN
		SELECT id, importe, saldo
		INTO v_anticipo, v_importe_anticipos, v_saldo_anticipos
		FROM anticipos 
		WHERE id_op = p_id_op;
		IF v_importe_anticipos <> v_saldo_anticipos THEN
			RETURN 'El anticipo ya se aplicó total o parcialmente';
		END IF;
	END IF;

	SELECT id, total_pago, id_moneda, id_proveedor, id_estado
	INTO v_id, v_total_pago, v_id_moneda, v_id_proveedor, v_id_estado
	FROM op_cabecera 
	WHERE id = p_id_op;
		
	-- SI LA OP NO ESTA ANULADA	
	IF v_id_estado <> 54 THEN
	
		--SI LA OP ESTA PENDIENTE		
		IF v_id_estado = 52 OR p_option = 52 THEN
		
			--SE ANULA LA OP
			UPDATE op_cabecera
			SET fecha_hora_anulacion = (to_char(now(), 'YYYY-MM-DD HH24:MI:SS'))::timestamp,
					id_usuario_anulacion = p_id_usuario,
					activo = false,
					id_estado = 54
			WHERE id = p_id_op;
				
			--SE LIBERAN LOS ITEMS DE LA OP
			FOR detalle IN c_op_detalle LOOP
				/*UPDATE libros_compras
				SET pendiente = false,
					genero_retencion = false, --REQUIERE QUE SE AFINE LA LOGICA DE RETENCION
					total_retencion = null
				WHERE id = detalle.id_libro_compra;

				UPDATE anticipos
				SET pendiente = false
				WHERE id = detalle.id_anticipo;*/
				
				IF detalle.id_libro_compra IS NOT NULL THEN
					UPDATE libros_compras
					SET retencion_pago_parcial = false,
						pendiente = false
					WHERE id = detalle.id_libro_compra;
				END IF;
				IF detalle.id_anticipo IS NOT NULL THEN
					UPDATE anticipos
					SET saldo = saldo + (detalle.monto * -1),
						pendiente = false
					WHERE id = detalle.id_anticipo;
				END IF;
				IF detalle.id_nc IS NOT NULL THEN
					UPDATE libros_compras
					SET retencion_pago_parcial = false,
						pendiente = false
					WHERE id = detalle.id_nc;
				END IF;				
				--SE ELIMINA LA RETENCION
				DELETE FROM retencion
				WHERE id_libro_compra = detalle.id_libro_compra;
			END LOOP;
			
			IF v_hay_anticipo > 0 THEN 
					UPDATE anticipos
					SET activo = false,
						saldo = 
						id_usuario_anulacion = p_id_usuario,
						fecha_hora_anulacion = (now())::timestamp(0) without time zone
					WHERE id = v_anticipo;
			END IF;
		END IF;
		
		--SI LA OP ESTA VERIFICADA		
		IF v_id_estado = 49 OR p_option = 49 THEN
		
			--VERIFICAR SI NO ES DE ORIGEN 'AN' PARA EVITAR BORRAR FORMA PAGO DE ANTICIPO
			SELECT origen
			INTO v_origen
			FROM op_cabecera
			WHERE id = p_id_op;
			
			IF v_origen <> 'AN' THEN
				
			
				--ELIMINAR LA FORMAS DE PAGO
				DELETE 
				FROM forma_pago_op_detalle
				WHERE id IN (
					SELECT id_cabecera
					FROM forma_pago_op_cabecera 
					WHERE id_op = p_id_op
				);

				DELETE 
				FROM forma_pago_op_cabecera 
				WHERE id_op = p_id_op;

				-- ELIMINAR GASTO OP
				DELETE
				FROM gastos_op
				WHERE id_op = p_id_op;

				--PASAR A ESTADO PENDIENTE, QUITAR RELACION CON FORMA DE PAGO Y QUITAR INDICE DE COTIZACION
				UPDATE op_cabecera
				SET id_estado = 52,
					fecha_hora_verificado = null,
					id_usuario_verificado = null,
					id_forma_pago = null,
					indice_cotizacion = null,
					cotizacion_especial = null,
					id_usuario_cotizacion_especial = null,
					fecha_cotizacion_especial = null,
					total_gastos = 0,
					total_descuento = 0
				WHERE id = p_id_op;
				
			ELSE
			
				--PASAR A ESTADO PENDIENTE
				UPDATE op_cabecera
				SET id_estado = 52,
					fecha_hora_verificado = null,
					id_usuario_verificado = null
				WHERE id = p_id_op;
				
			
			END IF;
			
		END IF;
		
		--SI LA OP ESTA AUTORIZADA	
		IF v_id_estado = 50 OR p_option = 50 THEN
			--PASAR A ESTADO PENDIENTE
			UPDATE op_cabecera
			SET id_estado = 49,
				fecha_hora_autorizado = null,
				id_usuario_autorizado = null
			WHERE id = p_id_op;
		END IF;
		
		--SI LA OP ESTA PROCESADA		
		IF v_id_estado = 53 OR p_option = 53 THEN 
		
		 	FOR detalle IN c_op_detalle LOOP
			
				-- BUSCA LIBRO COMPRA QUE NO SE ENCUENTRE EN OP QUE NO SEAN PROCESADOS Y ANULADOS
				-- IGNORAR OP ACTUAL
				SELECT COUNT(*)
				INTO cant_libro_compra_op
				FROM op_cabecera oc
				JOIN op_detalle od ON od.id_cabecera = oc.id
				WHERE id_estado NOT IN(53,54) 
				AND od.id_libro_compra IS NOT NULL
				AND od.id_libro_compra = detalle.id_libro_compra;
				
				IF cant_libro_compra_op > 0 THEN
					RETURN 'No se puede anular porque el item de libro compra se encuentra en una OP pendiente de proceso.';
				END IF;
				
				cant_libro_compra_op = 0;
				
				--VERIFICAR QUE EL LIBRO COMPRA NO ESTA PENDIENTE
				SELECT COUNT(*)
				INTO cant_libro_compra_op
				FROM libros_compras
				WHERE id = detalle.id_libro_compra 
				AND pendiente = true;
				
				IF cant_libro_compra_op > 0 THEN
					RETURN 'No se puede anular porque el item de libro compra se encuentra bloqueado por otro proceso.';
				END IF;

			END LOOP;
		
			--COLOCAR LA OP EN ESTADO AUTORIZADO
			UPDATE op_cabecera
			SET id_estado = 50,
				/*fecha_hora_autorizado = (to_char(now(), 'YYYY-MM-DD HH24:MI:SS'))::timestamp,
				id_usuario_autorizado = p_id_usuario,*/
				id_usuario_procesado = null,
				fecha_hora_procesado = null
			WHERE id = p_id_op;
			
			--SE ANULAN LOS ASIENTOS CONTABLES
			UPDATE asientos_contables
			SET id_usuario_anulacion = p_id_usuario,
						fecha_hora_anulacion = (to_char(now(), 'YYYY-MM-DD HH24:MI:SS'))::timestamp,
						activo = false
			WHERE id IN (SELECT id_asiento FROM op_cabecera WHERE id = p_id_op);
			
			--SE ANULAN LOS REGISTROS DE CC
			UPDATE cuenta_corriente
			SET activo = false,
				fecha_hora_anulacion = (to_char(now(), 'YYYY-MM-DD HH24:MI:SS'))::timestamp,
				id_usuario_anulacion = p_id_usuario
			WHERE id_documento = p_id_op;	
			
			
			--OBTIENE DATOS DE LA OP
			SELECT 	fp.importe_total, 
					b.id_cuenta_contable, 
					o.concepto, 
					o.id_sucursal, 
					o.id_centro_costo,
					fp.cotizacion, 
					b.id,
					fpd.nro_comprobante, 
					fpd.fecha_vencimiento

			INTO 	v_importe_total_pago, 
					v_id_cuenta_contable_banco,
					v_concepto, 
					v_id_sucursal_contable, 
					v_id_centro_costo,
					v_cotizacion_op_cabecera, 
					v_id_banco_detalle, 
					v_nro_comprobante,
					v_fecha_vencimiento
			FROM op_cabecera o 
			JOIN forma_pago_op_cabecera fp ON fp.id_op = o.id
			JOIN forma_pago_op_detalle fpd ON fpd.id_cabecera = fp.id
			JOIN banco_detalle b ON b.id = fpd.id_banco_detalle
			WHERE o.id = p_id_op; 

			-- SE ANULA EL MOVIMIENTO DE CUENTA CORRIENTE DE LA OP INSERTANDO UN MOVIMIENTO DE AGREGAR SALDO
			v_concepto = CONCAT('ANULAR ', v_concepto);
			v_fecha_emision = (to_char(now(), 'YYYY-MM-DD'))::date;
			
			PERFORM registrar_movimiento_cta_cte_bancos
			(
				v_concepto, 
				v_importe_total_pago, 
				v_id_banco_detalle::integer,
				v_nro_comprobante,
				v_fecha_emision, 
				v_fecha_vencimiento, 
				v_id_sucursal_contable, 
				v_id_cuenta_contable_banco, 
				v_id_centro_costo, 
				v_id_proveedor, 
				v_cotizacion_op_cabecera, 
				p_id_usuario, 
				13, -- OPERACION DE ANULACION DE OP
				p_id_op
			);
			
			--SE RECORRE EL DETALLE Y SE CARGA SU SALDO Y LOS ITEMS QUEDAN PENDIENTE DE VUELTA.
			FOR detalle IN c_op_detalle LOOP
				UPDATE libros_compras
				SET saldo = saldo + detalle.monto,
					pendiente = true
				WHERE id = detalle.id_libro_compra;

				UPDATE anticipos
				SET saldo = saldo + detalle.monto,
					pendiente = true
				WHERE id = detalle.id_anticipo;
					
			END LOOP;
		END IF;
		
	ELSE 
		RETURN 'Esta OP ya esta anulada';
	END IF;
	
	RETURN 'OK';

END;
$BODY$;

ALTER FUNCTION public.anular_op(integer, integer, integer)
    OWNER TO postgres;
