-- FUNCTION: public.procesar_recibo(integer, integer)

-- DROP FUNCTION IF EXISTS public.procesar_recibo(integer, integer);

CREATE OR REPLACE FUNCTION public.procesar_recibo(
	p_id_recibo integer,
	p_id_usuario integer)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   
DECLARE
	v_id_empresa integer;
	v_id_cabecera integer;
	v_id_asiento_retencion integer;
	v_cant_items integer;
	v_id_cliente integer;
	v_id_moneda_pago integer;
	v_id_asiento integer;
	v_id_moneda_retencion integer;
	v_id_cuenta_contable_banco integer;
	v_id_cuenta_contable_proveedor integer;
	v_total_cotizacion double precision;
	v_importe_total_pago double precision;
	v_total_debe_retencion double precision;
	v_cotizacion_promedio double precision;	
	v_diferencia_anticipo double precision;
	v_diferencia_retencion double precision;
	v_cotizacion_pago double precision;
	v_total_anticipo double precision;
	v_total_pagar double precision;
	v_monto_gasto double precision;
	v_diferencia double precision;
	v_total_detalle double precision;
	v_total_gastos double precision;
	v_total_recibo double precision;
	v_total_recibo_dif double precision;
	v_total_retencion double precision;	
	v_total double precision;
	v_diferencia_centavos double precision;
	v_total_deudores double precision;
	v_origen character varying;
	v_concepto character varying;
	v_orden integer;
	v_cant_fc_cursor integer;
	v_contador integer;
	v_cant_activos integer;
	v_id_sucursal_contable integer;
	v_id_centro_costo integer;
	v_id_estado integer;
	v_cant_anticipos integer;
	v_cant_retencion integer;
	v_total_nc double precision;
	v_diferencia_real double precision;
	v_nro_recibo character varying;
	v_saldo double precision;
	c_forma_cobro_detalle CURSOR FOR --CURSOR QUE OBTIENE LAS FORMAS DE COBRO
		SELECT 	b.id_cuenta_contable AS cuenta_contable_banco, b.id AS id_banco_detalle, r.id_forma_cobro, 
				fpd.nro_comprobante, fpd.fecha_emision, r.id_sucursal, r.id_centro_costo, fpd.fecha_documento, 
				r.id_cliente AS id_cliente_recibo, r.cotizacion AS cotizacion_recibo, r.id AS id_recibo, 
				fpd.importe_pago, fpd.id_moneda, fpd.id_cta_ctb AS id_cuenta_contable, r.concepto, 
				s.id AS id_sucursal_contable, r.id_empresa, r.importe, 
				fpd.cotizacion_contable, fpd.id_tipo_pago,
				(SELECT EXTRACT (MONTH FROM fpd.fecha_documento)) AS mes_documento,
				(SELECT EXTRACT (MONTH FROM CURRENT_DATE)) AS mes_actual
		FROM recibos r 
		JOIN forma_cobro_recibo_cabecera fp ON fp.id = r.id_forma_cobro
		JOIN forma_cobro_recibo_detalle fpd ON fpd.id_cabecera = fp.id
		LEFT JOIN banco_detalle b ON b.id = fpd.id_banco_detalle
		JOIN sucursales_empresa s ON s.id = r.id_sucursal
		WHERE r.id = p_id_recibo AND fpd.activo = true AND fp.activo = true;
	
	--CURSOR QUE OBTIENE DETALLES DEL RECIBO
	c_recibo_detalle CURSOR FOR SELECT importe, id_libro_venta FROM recibos_detalle WHERE id_cabecera = p_id_recibo;
	
	--CURSOR QUE OBTIENE LAS RETENCIONES POR FECHA
	c_retenciones CURSOR FOR 
	 	SELECT 	SUM((SELECT get_monto_cotizado_asiento(fpd.importe_pago, fpd.id_moneda, 111, 
				(CASE WHEN fpd.cotizacion_contable = 0 THEN ((SELECT get_cotizacion_contable_actual(1, 'C')))
				ELSE fpd.cotizacion_contable END )))) AS total, SUM(fpd.importe_pago) AS importe_original,
				fpd.id_cta_ctb, fpd.cotizacion_contable, r.concepto, s.id AS id_sucursal_contable, 
				r.id_centro_costo, fpd.id_moneda, r.id_empresa
		FROM recibos r 
		JOIN forma_cobro_recibo_cabecera fp ON fp.id = r.id_forma_cobro
		JOIN forma_cobro_recibo_detalle fpd ON fpd.id_cabecera = fp.id
		JOIN sucursales_empresa s ON s.id = r.id_sucursal
		WHERE r.id = p_id_recibo AND fpd.activo = true AND fpd.id_tipo_pago = 5
		GROUP BY (SELECT EXTRACT(MONTH FROM fpd.fecha_documento)), fpd.id_cta_ctb, fpd.cotizacion_contable, r.concepto, s.id, 
				  r.id_centro_costo, fpd.id_moneda, r.id_empresa;
	
	--CURSOR QUE OBTIENE LOS CANJES Y SUS RETENCIONES
	c_canjes_retenciones_a_cobrar CURSOR FOR
		SELECT 
		SUM (fpd.importe_pago)/*((SELECT get_monto_cotizado_asiento(fpd.importe_pago, fpd.id_moneda, 111, 
						(CASE WHEN fpd.cotizacion_contable = 0 THEN ((SELECT get_cotizacion_contable_actual(1, 'C')))
						ELSE fpd.cotizacion_contable END )))) - COALESCE(SUM(fpd.retencion), 0)*/ AS canje, 
						SUM(fpd.retencion) AS retencion_a_cobrar,
					fpd.id_cta_ctb, fpd.cotizacion_contable, r.concepto, s.id AS id_sucursal_contable, 
					r.id_centro_costo, fpd.id_moneda, r.id_empresa			
		FROM recibos r 
			JOIN forma_cobro_recibo_cabecera fp ON fp.id = r.id_forma_cobro
			JOIN forma_cobro_recibo_detalle fpd ON fpd.id_cabecera = fp.id
			JOIN sucursales_empresa s ON s.id = r.id_sucursal
			WHERE r.id = p_id_recibo AND fpd.activo = true AND fpd.id_tipo_pago = 6
			GROUP BY fpd.id_cta_ctb, fpd.cotizacion_contable, r.concepto, 
				s.id, r.id_centro_costo, fpd.id_moneda, r.id_empresa;
		
BEGIN
	---------------------------------------------------CONTROLES---------------------------------------------------
	SELECT COUNT(fpd.id)
	INTO v_cant_activos
	FROM forma_cobro_recibo_cabecera fp
	JOIN forma_cobro_recibo_detalle fpd ON fpd.id_cabecera = fp.id
	WHERE fp.id_recibo = p_id_recibo AND fpd.activo = true;
	
	IF v_cant_activos = 0 THEN
		RETURN 'No se puede procesar el recibo. No tiene formas de cobros activo';
	END IF;
	
	--OBTIENE EL IMPORTE TOTAL, MONEDA Y COTIZACIÓN DEL RECIBO
	SELECT 	fp.importe_total, r.id_moneda, r.cotizacion, r.id_cliente, r.id_empresa, r.importe, 
			r.concepto, s.id, r.id_centro_costo, r.id_estado, r.importe, r.nro_recibo::character varying,
			r.total_anticipo, r.total_nc
	INTO 	v_importe_total_pago, v_id_moneda_pago, v_cotizacion_pago, v_id_cliente, v_id_empresa, v_total_recibo, 
			v_concepto, v_id_sucursal_contable, v_id_centro_costo, v_id_estado, v_total_recibo_dif, v_nro_recibo,
			v_total_anticipo, v_total_nc
	FROM recibos r 
	JOIN forma_cobro_recibo_cabecera fp ON fp.id = r.id_forma_cobro
	JOIN sucursales_empresa s ON s.id = r.id_sucursal
	WHERE r.id = p_id_recibo AND fp.activo = true;

	IF v_id_estado <> 40 THEN
		RETURN 'No se puede procesar el recibo. El mismo no está cobrado.';
	END IF;
	
	----------------------------------------------FIN CONTROLES----------------------------------------------------------

	--CANTIDAD DE ANTICIPOS
	SELECT COUNT(*)
	INTO v_cant_anticipos
	FROM  recibos_detalle 
	WHERE id_cabecera = p_id_recibo AND id_anticipo IS NOT NULL;
	
	--CANTIDAD DE RETENCIONES
	SELECT COUNT(*)
	INTO v_cant_retencion
	FROM  recibos_detalle 
	WHERE id_cabecera = p_id_recibo AND id_retencion IS NOT NULL;

	--CANTIDAD DE ITEMS
	SELECT COUNT(*)
	INTO v_cant_items
	FROM  recibos_detalle 
	WHERE id_cabecera = p_id_recibo;
	
	--INICIALIZACIÓN DE VARIABLES
	v_cotizacion_promedio = (SELECT get_cotizacion_promedio_recibo(p_id_recibo));
	v_total = 0;
	v_id_cabecera = 0;
	v_id_asiento = 0;
	v_contador = 0;
	v_total_deudores = 0;
	
	--POR CADA FORMA DE COBRO SE REGISTRAN MOVIMIENTOS EN CC BANCOS Y SE GENERA ASIENTO DE RECAUDACIONES
	FOR detalle IN c_forma_cobro_detalle LOOP 
	
		--SE REGISTRAN MOVIMIENTOS EN CTA CTE BANCOS
		PERFORM registrar_movimiento_cta_cte_bancos
		(
			detalle.concepto, detalle.importe_pago, detalle.id_banco_detalle::integer, 
			detalle.nro_comprobante, detalle.fecha_documento, null, detalle.id_sucursal,
			detalle.cuenta_contable_banco, detalle.id_centro_costo, detalle.id_cliente_recibo, 
			detalle.cotizacion_recibo, p_id_usuario, 23, detalle.id_recibo::integer
		);
		--ASIENTO RECAUDACIONES POR VENTA (SÓLO SI NO ES RETENCIÓN O CANJE) Y SI LA FECHA DOC NO ES DE UN MES ANTERIOR
		
		IF detalle.id_tipo_pago NOT IN (5,6) AND detalle.mes_documento = detalle.mes_actual THEN  
			SELECT insertar_asientos_cobranza(detalle.id_cuenta_contable, detalle.importe_pago, 'D'::character varying, p_id_usuario, 4, 
					detalle.id_moneda, detalle.cotizacion_contable, v_id_cabecera, detalle.concepto, detalle.id_sucursal_contable, 
					detalle.id_centro_costo, detalle.id_empresa, v_nro_recibo,detalle.id_tipo_pago)
			INTO v_id_cabecera;
		ELSE
			--SI ES TC O TRANSFERENCIA
			IF detalle.id_tipo_pago IN (4,7) THEN 
				--MONTO DEUDORES POR VENTA
				SELECT SUM(fpd.importe_pago)
				INTO v_total_deudores
				FROM recibos r 
				JOIN forma_cobro_recibo_cabecera fp ON fp.id = r.id_forma_cobro
				JOIN forma_cobro_recibo_detalle fpd ON fpd.id_cabecera = fp.id
				JOIN banco_detalle b ON b.id = fpd.id_banco_detalle
				JOIN sucursales_empresa s ON s.id = r.id_sucursal
				WHERE r.id = p_id_recibo AND fpd.activo = true AND fp.activo = true AND fpd.id_tipo_pago IN (4,7)
				AND (SELECT EXTRACT (MONTH FROM fpd.fecha_documento)) < (SELECT EXTRACT (MONTH FROM CURRENT_DATE));
				--SE GENERA ASIENTOS POR FECHA DE DOCUMENTO
				SELECT out_total, out_id_asiento 
				FROM generar_asientos_por_fecha_cobranza(detalle.id_cuenta_contable, detalle.importe_pago, detalle.importe_pago, 4, 
						detalle.id_moneda, detalle.id_moneda, detalle.cotizacion_contable, v_cotizacion_promedio, detalle.concepto,
						detalle.id_sucursal_contable, detalle.id_centro_costo, detalle.id_empresa, v_nro_recibo, p_id_usuario, 
						v_id_asiento, v_total, false, v_total_deudores) 
				INTO v_total, v_id_asiento;
				v_id_cabecera = v_id_asiento;
			END IF;
		END IF;
	END LOOP;
	--SE REGISTRAN MOVIMIENTOS EN CTA CTE CLIENTE
	PERFORM registrar_movimiento_cta_cte(v_nro_recibo::character varying, v_importe_total_pago, v_id_moneda_pago, v_id_cliente, 23,'C', p_id_recibo);
	
	--SE ACTUALIZA EL SALDO DEL LIBRO DE VENTA
	/*FOR detalle IN c_recibo_detalle LOOP 	
		RAISE NOTICE 'Valor de importe = (%)',ROUND(detalle.importe::numeric, 2);
		RAISE NOTICE 'Valor id libro venta = (%)', detalle.id_libro_venta;
		UPDATE libros_ventas
		SET saldo = ROUND(saldo::numeric, 2) - ROUND(detalle.importe::numeric, 2)
		WHERE id = detalle.id_libro_venta;
		RAISE NOTICE 'Valor saldo = (%)', (SELECT saldo FROM libros_ventas WHERE id = detalle.id_libro_venta);
	END LOOP;*/
	FOR detalle IN c_recibo_detalle LOOP 	
		RAISE NOTICE 'Valor de importe = (%)',ROUND(detalle.importe::numeric, 2);
		RAISE NOTICE 'Valor id libro venta = (%)', detalle.id_libro_venta;
		SELECT saldo INTO v_saldo FROM libros_ventas WHERE id = detalle.id_libro_venta;
		UPDATE libros_ventas
		SET saldo = ROUND(v_saldo::numeric, 2) - ROUND(detalle.importe::numeric, 2)
		WHERE id = detalle.id_libro_venta;
		RAISE NOTICE 'Valor saldo = (%)', v_saldo;

	END LOOP;
	
	----------------------------------------------------------------------------------------------------------------------------------
														--ASIENTOS CONTABLES
	----------------------------------------------------------------------------------------------------------------------------------

	--SE GENERAN NUEVOS ASIENTOS CONTABLES (CABECERA Y DETALLE) PARA RETENCIONES POR FECHA DE DOCUMENTO 
	IF c_retenciones IS NOT NULL THEN
	
		v_total_retencion = 0;
		
		FOR detalle IN c_retenciones LOOP			
			
			IF v_id_asiento is not null then
				v_id_asiento_retencion = v_id_asiento;
			ELSE
				v_id_asiento_retencion = 0;
			END IF;
			v_id_moneda_retencion = detalle.id_moneda;

			--SE GENERA ASIENTOS POR FECHA DE RETENCIÓN				
			SELECT 	out_total, out_id_asiento 
			FROM generar_asientos_por_fecha_cobranza(detalle.id_cta_ctb, detalle.total, detalle.importe_original, 4, detalle.id_moneda,
					111, detalle.cotizacion_contable, v_cotizacion_promedio, detalle.concepto, detalle.id_sucursal_contable, 
					detalle.id_centro_costo, detalle.id_empresa, v_nro_recibo, p_id_usuario, v_id_asiento_retencion, v_total_retencion,
					true, 0)
			INTO v_total_retencion, v_id_asiento_retencion;
		END LOOP;
		
		--SE ACTUALIZA EL TOTAL DEL RECIBO PARA LUEGO GENERAR EL ASIENTO DEUDORES (YA SÓLO CON LA DIFERENCIA)
		--v_total_recibo = v_total_recibo - (SELECT get_monto_cotizado_asiento(v_total_retencion, v_id_moneda_retencion, v_id_moneda_pago, v_cotizacion_promedio));

	END IF;

	--SE GENERAN ASIENTOS CONTABLES PARA CANJES Y SUS RETENCIONES  
	IF c_canjes_retenciones_a_cobrar IS NOT NULL THEN
		FOR detalle IN c_canjes_retenciones_a_cobrar LOOP
			IF v_id_asiento_retencion <> 0 THEN
				v_id_cabecera = v_id_asiento_retencion;
			END IF;
			IF detalle.retencion_a_cobrar > 0 THEN --SOLO INSERTAR ASIENTO SI TIENE RETENCIONES
				SELECT 	insertar_asientos_cobranza((SELECT get_id_cuenta_contable('RETENCIONES_A_COBRAR', detalle.id_empresa, null)),
						detalle.retencion_a_cobrar, 'D'::character varying, p_id_usuario, 4, 111, detalle.cotizacion_contable, 
						v_id_cabecera, detalle.concepto, detalle.id_sucursal_contable, detalle.id_centro_costo, detalle.id_empresa, 
						v_nro_recibo)
				INTO v_id_cabecera;	
			END IF;
			SELECT insertar_asientos_cobranza(detalle.id_cta_ctb,(SELECT get_monto_cotizado_asiento(detalle.canje, detalle.id_moneda,111,detalle.cotizacion_contable,detalle.id_empresa)), 'D'::character varying, p_id_usuario, 4, 111, 
						detalle.cotizacion_contable, v_id_cabecera, detalle.concepto, detalle.id_sucursal_contable, 
						detalle.id_centro_costo, detalle.id_empresa, v_nro_recibo)
			INTO v_id_cabecera;	
					
		END LOOP;
	END IF;
	
	--CUENTAS (MONTO DEL RECIBO) DEUDORES POR VENTA
	IF v_cant_anticipos = 1 AND v_cant_items = 1 THEN
		--SE INSERTA EL ASIENTO DEL ANTICIPO
		SELECT insertar_asientos_cobranza((SELECT get_id_cuenta_contable('ANTICIPOS', v_id_empresa, v_id_moneda_pago)), v_total_recibo,
				'H'::character varying, p_id_usuario, 4, v_id_moneda_pago, v_cotizacion_promedio, v_id_cabecera, v_concepto, 
				v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, v_nro_recibo)
		INTO v_id_cabecera;	
		
		--SE ACTUALIZAN LOS DATOS DEL ANTICIPO
		UPDATE anticipos
		SET id_asiento = v_id_cabecera,
			id_estado = 64
		WHERE id = (SELECT id_anticipo FROM recibos_detalle WHERE id_cabecera = p_id_recibo);
		
	ELSIF v_cant_retencion > 0 THEN --SI TIENE RETENCIÓN (PAULO)
		SELECT insertar_asientos_cobranza((SELECT get_id_cuenta_contable('ANTICIPOS', v_id_empresa, v_id_moneda_pago)), v_total_recibo,
				'H'::character varying, p_id_usuario, 4, v_id_moneda_pago, v_cotizacion_promedio, v_id_cabecera, v_concepto, 
				v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, v_nro_recibo)
		INTO v_id_cabecera;	

		--SE INSERTA EL ASIENTO DEL COBRO POR RETENCIÓN
		SELECT insertar_asientos_cobranza((SELECT get_id_cuenta_contable('RETENCIONES_A_COBRAR', v_id_empresa, v_id_moneda_pago)), 
				v_total_recibo,	'H'::character varying, p_id_usuario, 7, v_id_moneda_pago, v_cotizacion_promedio, 
				v_id_cabecera, v_concepto, v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, v_nro_recibo)
		INTO v_id_cabecera;	
		
		--NO SE ACTUALIZA LA TABLA RETENCION
		--PENDIENTE PARA TENER EN CUENTA (PAULO)
		
	ELSIF  v_total_nc = 0 THEN --SI NO TIENE NC
		--SE INSERTA EL ASIENTO DE DEUDORES POR VENTAS
		SELECT insertar_asientos_cobranza((SELECT get_id_cuenta_contable('DEUDORES_VENTAS', v_id_empresa, v_id_moneda_pago)), 					 
			v_total_recibo,	'H'::character varying, p_id_usuario, 4, v_id_moneda_pago, v_cotizacion_promedio, 
			v_id_cabecera, v_concepto, v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, v_nro_recibo)
		INTO v_id_cabecera;
		
		--SE ACTUALIZAN LOS DATOS DEL ANTICIPO
		UPDATE anticipos
		SET id_asiento = v_id_cabecera,
			id_estado = 64,
			saldo = saldo - (SELECT importe FROM recibos_detalle WHERE id_cabecera = p_id_recibo AND id_anticipo IS NOT NULL)
		WHERE id = (SELECT DISTINCT id_anticipo FROM recibos_detalle WHERE id_cabecera = p_id_recibo AND id_anticipo IS NOT NULL);
		
	END IF;
	
	--ASIENTO DE ANTICIPO (FACTURA + ANTICIPO)
	IF v_cant_items > 1 AND v_cant_anticipos >= 1 AND v_total_anticipo <> 0 AND v_total_anticipo IS NOT NULL THEN
		--SE INSERTA EL ASIENTO DEL ANTICIPO
		SELECT insertar_asientos_cobranza((SELECT get_id_cuenta_contable('ANTICIPOS', v_id_empresa, v_id_moneda_pago)), v_total_anticipo,
				'D'::character varying, p_id_usuario, 4, v_id_moneda_pago,  0::double precision, v_id_cabecera, v_concepto, 
				v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, v_nro_recibo)
		INTO v_id_cabecera;	
	END IF;
	
	------------------------------------------------ASIENTO NC--------------------------------------------------------
	IF v_total_nc <> 0 THEN 

		SELECT insertar_asientos_cobranza((SELECT get_id_cuenta_contable('CRE_A_FAVOR_AGE', v_id_empresa, null)), 					 
			v_total_nc,	'D'::character varying, p_id_usuario, 4, v_id_moneda_pago, v_cotizacion_promedio, 
			v_id_cabecera, v_concepto, v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, v_nro_recibo)
		INTO v_id_cabecera;	

		SELECT insertar_asientos_cobranza((SELECT get_id_cuenta_contable('CRE_A_FAVOR_CLI', v_id_empresa, null)), 					 
			v_total_recibo,	'H'::character varying, p_id_usuario, 4, v_id_moneda_pago, v_cotizacion_promedio, 
			v_id_cabecera, v_concepto, v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, v_nro_recibo)
		INTO v_id_cabecera;	
	END IF;
	--------------------------------------------------DIFERENCIA DE CENTAVOS--------------------------------------------------------
	
	--SI ES ANTICIPO (FACTURA + ANTICIPO)
	IF v_cant_items > 1 AND v_cant_anticipos >= 1 AND v_total_anticipo <> 0 AND v_total_anticipo IS NOT NULL THEN
		v_diferencia_centavos = 0;
	ELSE
		v_diferencia_centavos = ROUND((v_importe_total_pago - v_total_recibo_dif)::numeric, 0);
	END IF;
	
	-- 	SE SUMA LA COLUMNA 'DEBE' CON TODOS LOS MONTOS CALCULADOS ANTERIORMENTE
	SELECT SUM(debe)
	INTO v_total_pagar
	FROM asientos_contables_detalle
	WHERE id_asiento_contable = v_id_cabecera;
		--SE OBTIENE LA DIFERENCIA DE CAMBIO
	SELECT get_diferencia_cambio_cobro_cobranza(v_total_recibo, v_total_pagar, v_cotizacion_promedio, v_id_moneda_pago, 
									(SELECT get_tipo_cotizacion_contable(4)), v_id_empresa, p_id_recibo)
	INTO v_diferencia;
	
	v_diferencia_real = v_diferencia_centavos + v_diferencia;

	--GENERA DIFERENCIA SÓLO SI NO TIENE NC
	IF v_total_nc = 0 THEN
		
		PERFORM generar_diferencia_cambio(v_id_cabecera, p_id_usuario, v_concepto, v_id_sucursal_contable,
			v_id_centro_costo, v_id_empresa, v_diferencia, 0::double precision, 111, false, 4, v_nro_recibo);
	END IF;
	
		
	--------------------------------------------------DIFERENCIA DE CAMBIO----------------------------------------------------------
	
	
	/*IF v_diferencia > 1 THEN 
		--SE GENERA EL ASIENTO CORRESPONDIENTE A DIFERENCIA POSITIVA O NEGATIVA
		PERFORM generar_diferencia_cambio(v_id_cabecera, p_id_usuario, v_concepto, v_id_sucursal_contable,
				v_id_centro_costo, v_id_empresa, v_diferencia, 0::double precision, 111, true, 4, v_nro_recibo);
	ELSE 
		-- 	DIFERENCIA DE 1 GS	
		PERFORM redondear_asiento(v_id_cabecera);
	END IF;*/
	----------------------------------------------------DIFERENCIA REDONDEO-----------------------------------------------------

-- 	DIFERENCIA DE REDONDEO HASTA 50 GS SI EXISTE DIFERENCIA DE CAMBIO
 /*	PERFORM generar_diferencia_redondeo(v_id_cabecera, p_id_usuario, v_concepto, v_id_sucursal_contable, v_id_centro_costo, 
										v_id_empresa, v_diferencia, 4, v_nro_recibo);*/
	
	
	------------------------------------------------------------------------------------------------------------------------------
	
	--SE OBTIENE EL IMPORTE TOTAL DE LOS LV ASOCIADOS AL RECIBO
	SELECT SUM(d.importe) 
	INTO v_total_detalle
	FROM libros_ventas l
	JOIN recibos_detalle d ON d.id_libro_venta = l.id
	WHERE d.id_cabecera = p_id_recibo;     
	
	--SE ACTUALIZA EL ESTADO Y OTROS CAMPOS DEL RECIBO COBRADO
	UPDATE recibos 
	SET id_estado = 56, 
		id_usuario_aplicado = p_id_usuario, 
		fecha_hora_aplicado = (now())::timestamp(0) without time zone,
		total_recibo_detalle = v_total_detalle,
		id_asiento = v_id_cabecera
	WHERE id = p_id_recibo; 
	
	UPDATE forma_cobro_recibo_cabecera
	SET cotizacion = (SELECT get_cotizacion_contable_actual(v_id_empresa, 'V'))
	WHERE id_recibo = p_id_recibo;
	
	--UNA VEZ PROCESADA EL LV, PENDIENTE PASA A SER FALSO
	UPDATE libros_ventas
	SET pendiente = false
	WHERE id IN (SELECT id_libro_venta FROM recibos_detalle WHERE id_cabecera = p_id_recibo);

	RETURN 'OK';
END;
$BODY$;

ALTER FUNCTION public.procesar_recibo(integer, integer)
    OWNER TO postgres;
