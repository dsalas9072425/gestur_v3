<?php 

namespace App\Traits;

use App\Persona;
use App\Ticket;
use DB;
use Session;
use Log;


trait TicketsTrait {


     /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }//function


    private function getIdEmpresa(){
	
        if(!$this->id_empresa){
           $this->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
        }
       return $this->id_empresa;
      }
      

     public function insert_into_ticket_manual($data_tickets){

        //TODO: quitar debug cuando funcion ya este lista
        Log::debug('Creando Tickets => '.json_encode($data_tickets,JSON_PRETTY_PRINT));

        $fecha_emision = date('Y-m-d');
        $id_proveedor = null; //Aerolinea
        $facial = 0;
        $id_grupo = null;
        $id_currency_costo = null;
        $id_proforma = null;
        $usuario_amadeus = null; //string
        $pasajero = null; //string
        $pnr = null; //string
        $iva = 0; 
        $id_empresa = $this->getIdEmpresa();
        $tasas = 0;
        $tasas_yq = 0;
        $id_factura = null;
        $comision = 0;
        $iva_comision = 0;
        $fee = 0;
        $id_estado = null;
        $id_tipo_ticket = null;
        $fecha_alta = null; //date
        $id_usuario = null;
        $id_detalle_proforma = null;
        $nro_amadeus = null; //string
        $total_ticket = 0;
        $id_tipo_transaccion = null;
        $tipo_pago = null; //string
        $variacion_impuesto = 0;
        $senha_paquete = 0;
        $tarifa_comision_yq = true;

        $cant_redondeo = 2;
        $genera_iva = true;
        $genera_comision = true;
        $origen = 'M';// Origen ticket Manual

   
        if(isset($data_tickets['fecha_emision']) && $data_tickets['fecha_emision']){
            $fecha_emision = $this->formatoFechaEntrada($data_tickets['fecha_emision']);
        }

        if(isset($data_tickets['fecha_alta']) && $data_tickets['fecha_alta']){
            $fecha_alta = $this->formatoFechaEntrada($data_tickets['fecha_alta']);
        }

        if(isset($data_tickets['id_proveedor']) && $data_tickets['id_proveedor']){
            $id_proveedor = $data_tickets['id_proveedor'];
        } else {
            throw new \Exception("Falta parametro de proveedor al crear el ticket");
        }

        if(isset($data_tickets['facial'])){
            $facial = $data_tickets['facial'];
        } else {
            throw new \Exception("Falta parametro facial al crear el ticket");
        }

        if(isset($data_tickets['id_grupo']) && $data_tickets['id_grupo']){
            $id_grupo = $data_tickets['id_grupo'];
        }

        if(isset($data_tickets['id_currency_costo']) && $data_tickets['id_currency_costo']){
            $id_currency_costo = $data_tickets['id_currency_costo'];
            if($id_currency_costo == 111){
                $cant_redondeo = 0;
            }
        } else {
            throw new \Exception("Falta parametro id_currency_costo al crear el ticket");
        }

        if(isset($data_tickets['id_proforma']) && $data_tickets['id_proforma']){
            $id_proforma = $data_tickets['id_proforma'];
        }

        if(isset($data_tickets['usuario_amadeus']) && $data_tickets['usuario_amadeus']){
            $usuario_amadeus = $data_tickets['usuario_amadeus'];
        }

        if(isset($data_tickets['pasajero'])){
            $pasajero = $data_tickets['pasajero'];
        }

        if(isset($data_tickets['pnr']) && $data_tickets['pnr']){
            $pnr = $data_tickets['pnr'];
        }

        if(isset($data_tickets['tasas_yq']) && $data_tickets['tasas_yq']){
            $tasas_yq = $data_tickets['tasas_yq'];
        }

        if(isset($data_tickets['genera_iva'])){
            $genera_iva = $data_tickets['genera_iva'];

            if($facial > 0 && $genera_iva){
                $iva = round(((((float)$facial + $tasas_yq) * 2.5)/100), $cant_redondeo);
            }
        }

        if(isset($data_tickets['tarifa_comision_yq'])){
            $tarifa_comision_yq = $data_tickets['tarifa_comision_yq'];
        }

        if(isset($data_tickets['genera_comision'])){
            $genera_comision = $data_tickets['genera_comision'];

            if($genera_comision && $facial > 0.00){
                $com_pactada = Persona::select('comision_pactada')->where('id', $id_proveedor)->first();

                //Si su comision es 0 o null no vamos a calcular
                if($com_pactada && $com_pactada->comision_pactada != null){
                    $val_com = (float)$com_pactada->comision_pactada;
                    if($val_com > 0.00){

                        //Si tasa yq es comisionable entonces ingresa a total para comision
                        $total_comisionable = $facial;
                        if($tarifa_comision_yq){
                            $total_comisionable = $facial + $tasas_yq;
                        }

                        $comision_pactada = (float) $com_pactada->comision_pactada; 
                        $comision = round((($total_comisionable * $comision_pactada) / 100), $cant_redondeo);
    
                        if($genera_iva){
                            $iva_comision = round((($comision * 10)/100), $cant_redondeo);
                        }
                    } else {
                        Log::error('Se puso para comisionar pero su comision es 0, ticket ID => '.$id_proveedor);
                    }
                 
                   
                } else {
                    Log::error('No se encontro la comision pactada al crear un ticket ID => '.$id_proveedor);
                }
    	       
              
            }
        }


        /**
         * Excepcion para empresa OASIS
         * TODO: Parametrizar si genera IVA Ticket
         */
        if($id_empresa == 21){
            $iva = 0;
            $iva_comision = 0;
        }



        if(isset($data_tickets['variacion_impuesto']) && $data_tickets['variacion_impuesto']){
            $variacion_impuesto = $data_tickets['variacion_impuesto'];
        }

        if(isset($data_tickets['tasas']) && $data_tickets['tasas']){
            $tasas = $data_tickets['tasas'];
        }


        if(isset($data_tickets['id_tipo_transaccion']) && $data_tickets['id_tipo_transaccion']){
            $id_tipo_transaccion = $data_tickets['id_tipo_transaccion'];
        } else {
            throw new \Exception("Falta id_tipo_transaccion al crear el ticket");
        }

        if(isset($data_tickets['senha_paquete']) && $data_tickets['senha_paquete']){
            $senha_paquete = $data_tickets['senha_paquete'];
        }

        if(isset($data_tickets['id_tipo_ticket']) && $data_tickets['id_tipo_ticket']){
            $id_tipo_ticket = $data_tickets['id_tipo_ticket'];
        } else {
            throw new \Exception("Falta id_tipo_ticket al crear el ticket");
        }

        if(isset($data_tickets['id_usuario']) && $data_tickets['id_usuario']){
            $id_usuario = $data_tickets['id_usuario'];
        } else {
            throw new \Exception("Falta id_usuario al crear el ticket");
        }

        if(isset($data_tickets['nro_amadeus']) && $data_tickets['nro_amadeus']){
            $nro_amadeus = $data_tickets['nro_amadeus'];
        } else {
            throw new \Exception("Falta nro_amadeus al crear el ticket");
        }

        $ticket_repetido = Ticket::where('numero_amadeus', $nro_amadeus)->first();

        if($ticket_repetido){
            throw new \Exception("El numero de ticket ya existe");
        }
        

        $tasas = $tasas + $variacion_impuesto - $iva;
        $total_ticket = $facial + $tasas + $iva + $tasas_yq;

        //Fecha de pago BSP
        $fecha_pago_bsp = DB::select("SELECT get_fecha_bsp as v_fecha_pago FROM get_fecha_bsp('$fecha_emision')")[0]->v_fecha_pago;
        //Estado del ticket
        $id_estado = DB::select("SELECT get_ticket_estado_x_transaccion as v_id_estado FROM get_ticket_estado_x_transaccion($id_tipo_transaccion)")[0]->v_id_estado;


            $ticket = new Ticket;
            $ticket->fecha_emision = $fecha_emision;
            $ticket->id_proveedor = $id_proveedor;
            $ticket->facial = $facial;
            $ticket->id_grupo = $id_grupo;
            $ticket->id_currency_costo = $id_currency_costo;
            $ticket->id_proforma = $id_proforma;
            $ticket->fecha_pago_bsp = $fecha_pago_bsp;
            $ticket->usuario_amadeus = $usuario_amadeus;
            $ticket->pasajero = $pasajero;
            $ticket->pnr = $pnr;
            $ticket->iva = $iva;
            $ticket->id_empresa = $id_empresa;
            $ticket->tasas = $tasas;
            $ticket->tasas_yq = $tasas_yq; //Probar hasta que funcione bien 
            $ticket->id_factura = $id_factura;
            $ticket->comision = $comision;
            $ticket->iva_comision = $iva_comision;
            $ticket->fee = $fee;
            $ticket->id_estado = $id_estado;
            $ticket->id_tipo_ticket = $id_tipo_ticket;
            $ticket->fecha_alta = $fecha_alta;
            $ticket->id_usuario = $id_usuario;
            $ticket->id_detalle_proforma = null;
            $ticket->numero_amadeus = $nro_amadeus;
            $ticket->total_ticket = $total_ticket;
            $ticket->id_tipo_transaccion = $id_tipo_transaccion;
            $ticket->tipo_pago = $tipo_pago;
            $ticket->porc_iva = 2.5;
            $ticket->variacion_impuesto = $variacion_impuesto;
            $ticket->senha_paquete = $senha_paquete;
            $ticket->origen = $origen;
            $ticket->tarifa_comision_yq = $tarifa_comision_yq;
            $ticket->save();

        //Generar libro compra
        DB::select("SELECT  generar_lc_ticket(".$ticket->id.")");

        return $ticket;

     }
}