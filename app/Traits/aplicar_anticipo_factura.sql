-- FUNCTION: public.aplicar_anticipo_factura(integer, double precision, integer, double precision, integer, integer, character varying)

-- DROP FUNCTION IF EXISTS public.aplicar_anticipo_factura(integer, double precision, integer, double precision, integer, integer, character varying);

CREATE OR REPLACE FUNCTION public.aplicar_anticipo_factura(
	v_id_factura integer,
	v_monto_anticipo double precision,
	v_id_anticipo integer,
	v_monto_factura double precision,
	v_id_usuario integer,
	v_id_libro integer,
	v_tipo character varying)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE

v_id_lv_factura integer;
v_saldo_actual_factura double precision;
v_saldo_actual_anticipo double precision;
v_id_moneda_costo integer;
v_id_moneda_venta integer;
v_cotizacion double precision;
v_monto_cotizado double precision;
v_saldo_nuevo_factura double precision;
v_saldo_nuevo_anticipo double precision;
v_id_estado_cobro_factura integer;
v_id_estado_anticipo integer;
v_mensaje text;

BEGIN  

SELECT  id
INTO v_id_lv_factura
FROM libros_ventas 
WHERE id_documento = v_id_factura;

SELECT  id
INTO v_id_anticipo
FROM anticipos 
WHERE id = v_id_anticipo;

-- Obtenemos los saldos actuales
IF v_tipo = 'C' THEN
	SELECT saldo, id_moneda_venta
	FROM libros_ventas
	INTO v_saldo_actual_factura,v_id_moneda_costo
	WHERE id = v_id_lv_factura;
END IF;

IF v_tipo = 'P' THEN
	SELECT saldo, id_moneda
	FROM libros_compras
	INTO v_saldo_actual_factura,v_id_moneda_costo
	WHERE id = v_id_libro;
END IF;

SELECT saldo,id_moneda,cotizacion
FROM anticipos
INTO v_saldo_actual_anticipo,v_id_moneda_venta,v_cotizacion 
WHERE id = v_id_anticipo;

IF v_monto_anticipo > ROUND(AVG(v_saldo_actual_anticipo)::numeric,2) THEN
	v_mensaje = 'ERROR, El monto a aplicar es mayor al saldo actual del anticipo';
	return v_mensaje;
END IF;

RAISE NOTICE 'v_cotizacion: %', v_cotizacion;
RAISE NOTICE 'v_monto_anticipo: %', v_monto_anticipo;
RAISE NOTICE 'v_id_moneda_venta: %', v_id_moneda_venta;
RAISE NOTICE 'v_id_moneda_costo: %', v_id_moneda_costo;

v_monto_cotizado = (SELECT public.get_monto_cotizacion_custom(v_cotizacion,v_monto_anticipo,v_id_moneda_venta,v_id_moneda_costo));
v_saldo_nuevo_factura = v_saldo_actual_factura - v_monto_cotizado;
v_saldo_nuevo_anticipo = v_saldo_actual_anticipo - v_monto_anticipo;

IF v_saldo_nuevo_factura = 0 THEN
	v_id_estado_cobro_factura = 40;
ELSE
	v_id_estado_cobro_factura = 31;
END IF;

IF v_saldo_nuevo_anticipo = 0 THEN
	v_id_estado_anticipo = 78;
ELSE
	v_id_estado_anticipo = 64;
END IF;

-- Insertamos el registro
 
INSERT INTO anticipo_aplicacion
(id_anticipo, id_factura, monto, id_usuario, fecha_hora, anulado, monto_cotizado,cotizacion, id_moneda,id_libro,tipo)
VALUES 
(v_id_anticipo, v_id_factura, v_monto_anticipo, v_id_usuario, now(), false,v_monto_cotizado,v_cotizacion,v_id_moneda_venta,v_id_libro,v_tipo);

-- Actualizamos los saldos de la FA
RAISE NOTICE 'v_monto_cotizado: %', v_monto_cotizado;

IF v_tipo = 'C' THEN
	UPDATE libros_ventas
	SET saldo = saldo - v_monto_cotizado,
	id_estado_cobro = v_id_estado_cobro_factura
	WHERE id = v_id_factura;

	UPDATE libros_ventas
	SET saldo = saldo - v_monto_cotizado,
	id_estado_cobro = v_id_estado_cobro_factura
	WHERE id = v_id_lv_factura;
END IF;

IF v_tipo = 'P' THEN
	UPDATE libros_compras
	SET saldo = saldo - v_monto_cotizado
	WHERE id = v_id_libro;
END IF;

-- Actualizamos los saldos del anticipo
UPDATE anticipos
SET saldo = saldo - v_monto_anticipo,
id_estado = v_id_estado_anticipo
WHERE id = v_id_anticipo; 

-- PENDIENTE / Generar asiento

v_mensaje = 'OK';

return v_mensaje;
END;
$BODY$;

ALTER FUNCTION public.aplicar_anticipo_factura(integer, double precision, integer, double precision, integer, integer, character varying)
    OWNER TO postgres;
