<?php 

namespace App\Traits;

use App\LibroVenta;
use App\ReciboDetalle;
use DB;
use App\Exceptions\ExceptionCustom;



trait LibrosVentasTrait {


    public function anular_libro_venta_manual($id_libro_venta, $id_usuario){

        $puede_anualar = $this->datosPermiso('verLibroVenta',59,$id_usuario );

        if(!$puede_anualar){
            throw new ExceptionCustom('NO TIENE PERMISO PARA ANULAR LIBRO VENTA MANUAL');
        }


        //Recibo debe estar anulado
        $existe_en_recibo = ReciboDetalle::with('recibo')->where('id_libro_venta',$id_libro_venta)->whereHas('recibo',function($q){
            $q->where('id_estado','<>',30);
        })->get();

        if(count($existe_en_recibo)){
            $nro_recibo = '';
            foreach ($existe_en_recibo as $key => $value) {
                $nro_recibo .= $value->recibo->nro_recibo.',';
            }

            throw new ExceptionCustom('EL LIBRO VENTA SE ENCUENTRA EN RECIBO, DEBE ANULAR RECIBO NRO:'.$nro_recibo);
        }

        $libro_venta = LibroVenta::find($id_libro_venta);

        if(!$libro_venta->activo){
            throw new ExceptionCustom('EL ITEM DE LIBRO VENTA ESTA EN ESTADO ANULADO');
        }

        if($libro_venta->origen != 'M'){
            throw new ExceptionCustom('EL LIBRO VENTA NO ES DE ORIGEN MANUAL');
        }

        //TODO: Controlar que la anulacion sea dentro del mes

        //Anulando LV
        $libro_venta->activo = false;
        $libro_venta->id_usuario_anulacion = $id_usuario;
        $libro_venta->fecha_hora_anulacion = date('Y-m-d H:i:00');
        $libro_venta->save();

        //--SE ANULAN LOS ASIENTOS CONTABLES
        DB::select("UPDATE asientos_contables
        SET id_usuario_anulacion = ?,
            fecha_hora_anulacion = (to_char(now(), 'YYYY-MM-DD HH24:MI:SS'))::timestamp,
            activo = false
        WHERE id = ? ",[$id_usuario, $libro_venta->id_asiento]);



    }



    private function datosPermiso($vista, $permiso, $id_usuario){

            $btn = DB::select("SELECT COUNT(pe.id) as total 
                            FROM persona_permiso_especiales p, permisos_especiales pe
                            WHERE p.id_permiso = pe.id 
                            AND p.id_persona =  ? 
                            AND pe.url = ? 
                            AND pe.id = ?", [$id_usuario, $vista, $permiso])[0]->total;
        
            return $btn ? true : false;
    }//function

}