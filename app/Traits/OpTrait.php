<?php

namespace App\Traits;

use App\Persona;
use App\Ticket;
use DB;
use Session;
use Log;
use App\OpCabecera;
use App\Personas;
use App\BancoDetalle;
use App\Anticipo;
use App\LibroCompra;
use App\Retencion;
use App\FormaPagoOpCabecera;
use App\FormaPagoOpDetalle;
use App\ChequeFp;
use App\GastoOp;
use App\Asiento;
use App\CuentaCorriente;
use App\IntermediaAdjuntoOp;
use App\Exceptions\ExceptionCustom;
use App\OpDetalle;
use App\ProformasDetalle;
use App\FormaPagoCliente;
use App\SucursalEmpresa;
use App\Factura;
use App\Currency;
use App\AsientoDetalle;
use App\CuentaCorrienteBanco;
use Illuminate\Support\Facades\Storage;

/**
 * Se encuentra en el trai porque son funciones que anteriormente estaban en base de datos.
 * Tambien estan en trait porque se usa en multiples lugares
 */

trait OpTrait {

    public function procesar_op($p_id_op, $p_id_usuario, $regenerar_op = false){

        // LC CONTADO OP
        $v_id_asiento_lc = ""; // string
        $v_id_tipo_documento = 0; // integer
        $v_id_lc_manual = 0; // integer
        $v_concepto = ""; // string
        $v_nro_comprobante = ""; // string
        $v_result = 0; // integer

        // RETENCIONES
        $v_total_retencion = 0.0; // float
        $v_id_empresa = 0; // integer
        $v_id_cabecera = 0; // integer
        $v_id_cabecera_ant = 0; // integer
        $v_id_proveedor = 0; // integer
        $v_id_moneda_pago = 0; // integer
        $v_id_asiento_pago = 0; // integer
        $v_id_cuenta_contable_pago = 0; // integer
        $v_id_cabecera_diferencia = 0; // integer
        $v_id_cuenta_contable_proveedor = 0; // integer
        $v_importe_total_pago = 0.0; // float
        $v_total_gastos_adm = 0.0; // float
        $v_cotizacion_promedio = 0.0; // float
        $v_diferencia_anticipo = 0.0; // float
        $v_cotizacion_pago = 0.0; // float
        $v_total_pagar = 0.0; // float
        $v_monto_gasto = 0.0; // float
        $v_diferencia = 0.0; // float
        $v_total_detalle = 0.0; // float
        $v_total_op_con_gastos = 0.0; // float
        $v_origen = ""; // string
        $v_id_sucursal_contable = 0; // integer
        $v_id_pais = 0; // integer
        $v_id_centro_costo = 0; // integer
        $v_id_banco_detalle = 0; // integer
        $v_total_op = 0.0; // float
        $v_cotizacion_op_forma_pago = 0.0; // float
        $v_cotizacion_del_dia = 0.0; // float
        $v_diferencia_redondeo = 0.0; // float
        $v_total_retencion_cotizado = 0.0; // float
        $v_total_anticipo_cotizado = 0.0; // float
        $v_fecha_emision = NULL; // date
        $v_fecha_vencimiento = NULL; // date
        $v_moneda_op = 0; // integer
        $v_cotizacion_op = 0.0; // float
        $v_total_gastos = 0.0; // float
        $v_diferencia_asiento = 0.0; // float
        $v_diferencias = 0.0; // float
        $v_fecha_hora_autorizado = NULL; // date
        $importe_total_op = 0;

        //FECHA DE OP
        $fecha_hora_operacion = date('Y-m-d H:i:00');

        if($regenerar_op){
            $op_regnerar = OpCabecera::find($p_id_op);
            $fecha_hora_operacion = $op_regnerar->fecha_hora_procesado;
            $p_id_usuario = $op_regnerar->id_usuario_procesado;
        }




        // Obtener los registros de op_detalle con Eloquent
        $c_op_detalle = DB::table('vw_op_detalle')->where('id_op', '=', $p_id_op)->get();


        // Obtener los registros de gastos_op con Eloquent
        $c_gastos_op = DB::table('gastos_op')->select('id_cuenta_contable', 'monto', 'tipo', 'id_moneda_gasto', 'monto_moneda_original', 'cotizacion_gasto')
            ->where('id_op', '=', $p_id_op)
            ->get();

        // Obtener los registros de anticipos con Eloquent
        $c_anticipos = Anticipo::select('id_cuenta_contable', 'id_moneda', 'cotizacion', 'sucursales_empresa.id AS id_sucursal_contable', 'id_centro_costo', 'op_detalle.monto')
        ->join('op_detalle', 'op_detalle.id_anticipo', '=', 'anticipos.id')
        ->join('sucursales_empresa', 'sucursales_empresa.id', '=', 'anticipos.id_sucursal')
        ->where('op_detalle.id_cabecera', '=', $p_id_op)
        ->get();

        $op_data = OpCabecera::with('proveedor')->where('id', $p_id_op)->first();
        
        if($op_data->origen == 'NC' && !$op_data->id_nota_credito){
            throw new ExceptionCustom('Error de sistema, falta relación con nota de credito, ponerse en contacto con soporte');
        }

        // Obtener el país del proveedor para saber si es local o extranjero
        $v_id_pais = $op_data->proveedor->id_pais;

        // Obtener los datos del pago
        $resultado = DB::table('op_cabecera as o')
        ->select(
            'fp.importe_total',
            DB::raw('fp.id_moneda as id_moneda_pago'),
            DB::raw('o.cotizacion as cotizacion_pago'),
            DB::raw('fpd.id_cuenta_contable as id_cuenta_contable_pago'), 
            'o.id_proveedor',
            'o.id_cuenta_contable',
            'o.origen',
            'o.concepto',
            'o.id_empresa',
            DB::raw('s.id as id_sucursal_contable'),
            'o.id_centro_costo',
            'o.total_pago',
            DB::raw('fp.cotizacion as cotizacion_op_forma_pago'), 
            'b.id',
            'fpd.nro_comprobante',
            'fp.fecha_hora',
            'fpd.fecha_vencimiento',
            'fpd.fecha_emision',
            'o.id_moneda',
            'o.cotizacion',
            'o.fecha_hora_autorizado',
            'o.total_neto_pago',
            'o.total_op'
        )
        ->leftjoin('forma_pago_op_cabecera as fp', 'fp.id_op', '=', 'o.id')
        ->leftjoin('forma_pago_op_detalle as fpd', 'fpd.id_cabecera', '=', 'fp.id')
        ->leftjoin('banco_detalle as b', 'b.id', '=', 'fpd.id_banco_detalle')
        ->leftjoin('sucursales_empresa as s', 's.id', '=', 'o.id_sucursal')
        ->where('o.id', $p_id_op)
        ->first();


         $tiene_forma_pago = true;


        //OP no tiene forma de pago, casos de NC que cancelan la totalidad de factura
        if(!$resultado){

            $tiene_forma_pago =  false;

            $resultado = DB::table('op_cabecera as o')
            ->select(
                'o.total_neto_pago',
                'o.id_moneda',
                'o.id_proveedor',
                'o.id_cuenta_contable',
                'o.origen',
                'o.concepto',
                'o.id_empresa',
                DB::raw('s.id as id_sucursal_contable'),
                'o.id_centro_costo',
                'o.total_pago',
                'o.id_moneda',
                'o.cotizacion',
                'o.fecha_hora_autorizado'
            )
            ->join('sucursales_empresa as s', 's.id', '=', 'o.id_sucursal')
            ->where('o.id', $p_id_op)
            ->first();

               // Asignar los valores a las variables correspondientes
               $v_importe_total_pago = $resultado->total_neto_pago;
               $importe_total_op = $resultado->total_pago;
               $v_id_moneda_pago = $resultado->id_moneda;
               $v_cotizacion_pago = $resultado->cotizacion;
               $v_id_cuenta_contable_pago = null;
               $v_id_proveedor = $resultado->id_proveedor;
               $v_id_cuenta_contable_proveedor = $resultado->id_cuenta_contable;
               $v_origen = $resultado->origen;
               $v_concepto = 'APLICACION NC-OP '.$resultado->concepto;
               $v_id_empresa = $resultado->id_empresa;
               $v_id_sucursal_contable = $resultado->id_sucursal_contable;
               $v_id_centro_costo = $resultado->id_centro_costo;
               $v_total_op = $resultado->total_pago;
               $v_cotizacion_op_forma_pago = $resultado->cotizacion;
               $v_id_banco_detalle = null;
               $v_nro_comprobante = null;
               $v_fecha_emision = null;
               $v_fecha_vencimiento = null;
               $v_moneda_op = $resultado->id_moneda;
               $v_cotizacion_op = $resultado->cotizacion;
               $v_fecha_hora_autorizado = $resultado->fecha_hora_autorizado;
         
        } else {
            
            // Asignar los valores a las variables correspondientes
            $v_importe_total_pago = $resultado->importe_total;
            $v_id_moneda_pago = $resultado->id_moneda_pago;
            $v_cotizacion_pago = $resultado->cotizacion_pago;
            $v_id_cuenta_contable_pago = $resultado->id_cuenta_contable_pago;
            $v_id_proveedor = $resultado->id_proveedor;
            $v_id_cuenta_contable_proveedor = $resultado->id_cuenta_contable;
            $v_origen = $resultado->origen;
            $v_concepto = $resultado->concepto;
            $v_id_empresa = $resultado->id_empresa;
            $v_id_sucursal_contable = $resultado->id_sucursal_contable;
            $v_id_centro_costo = $resultado->id_centro_costo;
            $v_total_op = $resultado->total_neto_pago;
            $v_cotizacion_op_forma_pago = $resultado->cotizacion_op_forma_pago;
            $v_id_banco_detalle = $resultado->id;
            $v_nro_comprobante = $resultado->nro_comprobante;
            $v_fecha_emision = $resultado->fecha_emision ? $resultado->fecha_emision : $resultado->fecha_hora;
            $v_fecha_vencimiento = $resultado->fecha_vencimiento;
            $v_moneda_op = $resultado->id_moneda;
            $v_cotizacion_op = $resultado->cotizacion;
            $v_fecha_hora_autorizado = $resultado->fecha_hora_autorizado;
        }
         
        // Obtener la cotización promedio
        // $v_cotizacion_promedio = OpCabecera::get_cotizacion_promedio_op($p_id_op);
        // if ($v_cotizacion_promedio == null || $v_cotizacion_promedio == 0) {
            $v_cotizacion_promedio = $v_cotizacion_op_forma_pago;
        // }

        // POR CADA ITEM DE LC SE ACTUALIZA SU SALDO Y SE REGISTRAN MOVIMIENTOS EN CC

        $total_lcs_sin_cot_factura = 0; //monto original sin cotizar
        $total_lcs_factura = 0;

        $total_lcs_nc = 0;
        $total_lcs_sin_cot_nc = 0; //monto original sin cotizar

        $total_anticipos = 0;
        $total_anticipos_sin_cot = 0; //monto original sin cotizar

        $total_op_calculado = 0;
        $total_op_calculado_original = 0;

        //Se toma el cambio del dia para cotizar la retencion
       
        $v_cotizacion_del_dia = DB::select("SELECT * FROM get_cotizacion(?,?,?,?)",[111,$v_moneda_op,$v_id_empresa,null])[0]->get_cotizacion;
            
        $cotizaciones_total = 0;
        $contador_cotizaciones = 0;
        //RECORRER ITEMS DE LC 
        foreach ($c_op_detalle as $detalle) {

            /**
             * Aqui condicionamos que si encuentra 
             * 
             * 5 FACTURA CONTADO
             * 1 FACTURA CREDITO
             * 22 FACTURA CREDITO PROVEEDOR
             * 33 PASAJE AEREO ELECTRONICO
             * 
             * 20 ANTICIPO
             * 
             * 32 NOTA CREDITO
             * 2 NOTA CREDITO
             */

            $v_id_tipo_documento = $detalle->id_tipo_documento;
            $v_id_lc_manual = $detalle->id_documento;
            $v_total_retencion = $detalle->total_retencion;
            $importa_a_pagar = (float) $detalle->monto_op_detalle;

            //Convertir a positivo, porque las NC guarda en negativo
            if(!($importa_a_pagar > 0.00)){
                $importa_a_pagar = $importa_a_pagar * -1;
            }

            if(!$regenerar_op){
                $v_result = DB::select(DB::raw("SELECT generar_retencion_a_cobrar(:v_id_lc_manual, :p_id_usuario)"), [
                    'v_id_lc_manual' => $v_id_lc_manual,
                    'p_id_usuario' => $p_id_usuario,
                ])[0]->generar_retencion_a_cobrar;

                // SE ACTUALIZA EL GENERO_RETENCION DEL LIBRO DE COMPRA
                if ($v_total_retencion > 0 && $v_total_retencion != NULL) {
                        $lc_ret = LibroCompra::where('id', $detalle->id_documento)->where('id_empresa',$v_id_empresa)->first();
                        $lc_ret->genero_retencion =  true;
                        $lc_ret->save();
                }
            }



  
                //POR CADA ITEM DE LC vamos a tomar su valor ya cotizado
                if($detalle->id_tipo_documento == 5 || $detalle->id_tipo_documento == 1 || $detalle->id_tipo_documento  == 33 || $detalle->id_tipo_documento  == 22 ){

                    $importe_total_op += $importa_a_pagar;
                    $contador_cotizaciones++;

                    // SE ACTUALIZA EL SALDO DEL LIBRO DE COMPRA
                    $lc = LibroCompra::where('id', $detalle->id_documento)
                    ->where('id_empresa',$v_id_empresa)
                    ->first();

                    if(!$regenerar_op){
                        if($lc->saldo > 0){
                                $lc->saldo = (float) $lc->saldo - $importa_a_pagar ;
                                $lc->save();
                        } else {
                            throw new ExceptionCustom('No se puede descontar el saldo de un item LC, saldo negativo ID: '.$lc->id);
                        }
                    }

                    //Matar reprice asociados al proforma detalle
                    if($lc->id_proforma_detalle && $lc->saldo == 0){
                        if(!$regenerar_op){
                            DB::table('libros_compras')
                                ->where('id_proforma_detalle', $lc->id_proforma_detalle)
                                ->update(['saldo' => 0]);
                        }
                    }

                   
                    // Log::debug('RESTANDO saldo LC'.json_encode($lc));

                    //TODO:Falta probar con documento en PYG por eso validamos moneda, pero el TC debe ser 1
                    if($lc->id_moneda != 111){

                        if($lc->cambio <= 1){
                            throw new ExceptionCustom('Libro Compra con cotización igual o menor a 1,  ID: '.$lc->id);
                        }

                        $cotizaciones_total += $lc->cambio;
                        $total_lcs_factura += round($importa_a_pagar * $lc->cambio);
                    } else {
                        $cotizaciones_total = 1;
                        $total_lcs_factura += round($importa_a_pagar);
                    }
                    
                    

                }
                
                if($detalle->id_tipo_documento == 32 || $detalle->id_tipo_documento == 2){
                    // Notas de credito

                    $importe_total_op -= $importa_a_pagar;

                    // SE ACTUALIZA EL SALDO DEL LIBRO DE COMPRA
                    $lc = LibroCompra::where('id', $detalle->id_documento)
                    ->where('id_empresa',$v_id_empresa)
                    ->first();

                    if(!$regenerar_op){
                        if($lc->saldo > 0){
                                $lc->saldo = (float) $lc->saldo - $importa_a_pagar ;
                                $lc->save();
                        } else {
                            throw new ExceptionCustom('No se puede descontar el saldo de un item LC, saldo negativo ID: '.$lc->id);
                        }
                    }
                    
                    //Matar reprice asociados al proforma detalle
                    if($lc->id_proforma_detalle && $lc->saldo == 0){
                        if(!$regenerar_op){
                            DB::table('libros_compras')
                            ->where('id_proforma_detalle', $lc->id_proforma_detalle)
                            ->update(['saldo' => 0]);
                        }
                        
                    }

                    // Log::debug('RESTANDO saldo LC'.json_encode($lc));


                    //TODO:Falta probar con documento en PYG por eso validamos moneda, pero el TC debe ser 1
                    if($lc->id_moneda != 111){

                        if($lc->cambio <= 1){
                            throw new ExceptionCustom('Libro Compra con cotización igual o menor a 1,  ID: '.$lc->id);
                        }

                        $total_lcs_nc += round($importa_a_pagar * $lc->cambio);
                    } else {
                        $total_lcs_nc += round($importa_a_pagar);
                    }
                   
                }
               

          
            if($detalle->id_tipo_documento == 20){

                // SÓLO SE ACTUALIZA EL SALDO CUANDO ESTÁ PENDIENTE (CUANDO SE ELIGE EL ANT DESDE LA PANTALLA DE PP)
                $anticipo = Anticipo::where('id', $detalle->id_documento)->first();

                if($anticipo){

                    //SÓLO SE ACTUALIZA EL SALDO CUANDO ESTÁ PENDIENTE (CUANDO SE ELIGE EL ANT DESDE LA PANTALLA DE PP)
                    // Cuando creas tu anticipo con op este esta en estado pendiente true y no se tiene que descontar el saldo, y final del proceso
                    // se tiene que poner pendiente false
                    if(!$anticipo->pendiente){

                        if(!$regenerar_op){
                            if($anticipo->saldo > 0){
                                    $anticipo->saldo = $anticipo->saldo - $importa_a_pagar;
                                    $anticipo->save(); 
                            } else {
                                throw new ExceptionCustom('No se puede descontar el saldo de un item Anticipo, saldo negativo ID: '.$anticipo->id);
                            }
                        }

                        
                        
                    } else {
                        if(!$regenerar_op){
                            $anticipo->pendiente = false;
                            $anticipo->save(); 
                        }
                    }

                    // Log::debug('RESTANDO saldo AN'.json_encode($anticipo));

                    //TODO:Falta probar con documento en PYG por eso validamos moneda, pero el TC debe ser 1
                    if($anticipo->id_moneda != 111){

                        if($anticipo->cotizacion <= 1){
                            throw new ExceptionCustom('Ancticipo con cotización igual o menor a 1,  ID: '.$anticipo->id);
                        }

                        $total_anticipos += round($importa_a_pagar * $anticipo->cotizacion);
                    } else {
                        $total_anticipos += round($importa_a_pagar);
                    }
                 
                }
            }



        }

        if($tiene_forma_pago){
            // SE REGISTRAN MOVIMIENTOS EN CTA CTE BANCOS
            $params_bancos = [
                $v_concepto,
                $v_importe_total_pago,
                (int) $v_id_banco_detalle,
                $v_nro_comprobante,
                $v_fecha_emision,
                $v_fecha_vencimiento,
                $v_id_sucursal_contable,
                (int) $v_id_cuenta_contable_pago,
                (int) $v_id_centro_costo,
                (int) $v_id_proveedor,
                $v_cotizacion_op_forma_pago,
                $p_id_usuario,
                12,
                $p_id_op
            ];

            if(!$regenerar_op){

                DB::select('SELECT registrar_movimiento_cta_cte_bancos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_bancos);
            } else {
                //TODO: POr lo que vemos no es necesario entonces no lo hacemos
                // $this->regenerar_movimiento_cta_cte_bancos($params_bancos, $p_id_op);
            }
        }

      
        if(!$regenerar_op){
            // SE REGISTRAN MOVIMIENTOS EN CTA CTE PROVEEDOR
            $params_proveedor = [
                $p_id_op,
                $v_importe_total_pago,
                (int) $v_id_moneda_pago,
                (int) $v_id_proveedor,
                12,
                'P',
                $p_id_op
            ];
            DB::select('SELECT registrar_movimiento_cta_cte(?, ?, ?, ?, ?, ?, ?)', $params_proveedor);
        }


        /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												--ASIENTOS CONTABLES
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */
        // SI LA FACTURA ES DIFERENTE A CONTADO
        /**
         * ESTE ASIENTO DEBE SER EL VALOR TOTAL DE LA OP SIN RESTAR AUN LOS VALORES
         * AQUI PRIMERO COTIZAMOS NUESTROS VALORES SEGUN LC Y ANTICIPO, SIEMPRE TOMANDO EL MONTO A PAGAR POR LA COTIZACION DE ESE DOCUMENTO
         */

        if ($v_id_tipo_documento != 5) {

            if($tiene_forma_pago){

                if($total_lcs_factura == 0){
                    $total_op_calculado = $total_lcs_nc + $total_anticipos; //monto cotizado segun cambio de libros o Anticipo
                } else {

                    //No se resta el anticipo , porque debe formar parte del total
                    $total_op_calculado = $total_lcs_factura - $total_lcs_nc; //monto cotizado segun cambio de libros o Anticipo
                }


                //TODO: Hacer el ajuste para evitar poner cotizacion en una LV en guaranies, ahora hacemos un parche
                if($v_moneda_op != 111){

                    /**
                     * El calculo de promedio de cotizaciones de los LC y antcipos se realiza para que al momento de sumar los valores, se tenga una 
                     * cotizacion para poder migrar al siga
                    */

                    /**
                     * SI ES DE ORIGEN NC VAMOS A TOMAR LA COTIZACION DE LA CABECERA DE LA OP
                     * Y CON ESO VAMOS A CALCULAR, YA QUE NO TIENE ASOCIADO NINGUNA LC
                     */
                    if($v_origen == 'NC' || $v_origen == 'AN'){
                        $v_cotizacion_promedio = $v_cotizacion_pago;
                        $importe_total_op = $v_total_op; // No hay foreach de detalle por eso usamos el monto directo
                        $total_op_calculado = round($v_total_op * $v_cotizacion_promedio); //Cotizamos
                    } else {
                        $v_cotizacion_promedio = round($cotizaciones_total / $contador_cotizaciones);
                    }

                    
                } else {
                    $v_cotizacion_promedio = 1;    

                    /**
                     * SI ES DE ORIGEN NC VAMOS A TOMAR LA COTIZACION DE LA CABECERA DE LA OP
                     * Y CON ESO VAMOS A CALCULAR, YA QUE NO TIENE ASOCIADO NINGUNA LC
                     */
                    if($v_origen == 'NC' || $v_origen == 'AN'){
                        $importe_total_op = $v_total_op; // No hay foreach de detalle por eso usamos el monto directo
                        $total_op_calculado = $v_total_op; //Cotizamos
                    } 
                }
            } else {
                

                 //TODO: Hacer el ajuste para evitar poner cotizacion en una LV en guaranies, ahora hacemos un parche
                 if($v_moneda_op != 111){
                    $v_cotizacion_promedio = $v_cotizacion_op;    
                    $total_op_calculado = round($v_total_op * $v_cotizacion_op); //Cotizamos
                } else {
                    $total_op_calculado = $v_total_op;
                    $v_cotizacion_promedio = 1;    
                }
            }

          
            
            // PROVEEDORES DEL EXTERIOR O LOCALES SEGÚN OP CABECERA
            $params_asientos = [
                $v_id_cuenta_contable_proveedor,
                $importe_total_op, //total original
                $total_op_calculado, //total de montos cotizados con sus cotizaciones 
                'D',
                $p_id_usuario,
                3,
                $v_moneda_op,
                $v_cotizacion_promedio,
                0,
                $v_concepto,
                $v_id_sucursal_contable,
                $v_id_centro_costo,
                $v_id_empresa,
                $p_id_op,
                $fecha_hora_operacion
            ];

            $v_id_cabecera = DB::select('SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos_custom;

       
            //SI NO TIENE FORMA DE PAGO ENTONCES SE CANCELA CONTRA SI MISMO CASO DE FC - NC = 0
            //Y SI ES ANTICIPO NO INGRESAR AQUI, IR A LA LOGICA DE MAS ABAJO
            if(!$tiene_forma_pago && count($c_anticipos) == 0){

            // PROVEEDORES DEL EXTERIOR O LOCALES SEGÚN OP CABECERA
            $params_asientos = [
                $v_id_cuenta_contable_proveedor,
                $importe_total_op, //total original
                $total_op_calculado, //total de montos cotizados con sus cotizaciones 
                'H', //CONTRA PARTIDA
                $p_id_usuario,
                3,
                $v_moneda_op,
                $v_cotizacion_promedio,
                $v_id_cabecera,
                $v_concepto,
                $v_id_sucursal_contable,
                $v_id_centro_costo,
                $v_id_empresa,
                $p_id_op,
                $fecha_hora_operacion
            ];

                $v_id_cabecera = DB::select('SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos_custom;
            }

        } else {

            // SI ES FONDO FIJO, GENERA EL ASIENTO CORRESPONDIENTE A FF
            $fondo_fijo = DB::table('vw_op_detalle')->select('fondo_fijo')
                ->where('id_op', $p_id_op)
                ->distinct()
                ->first()
                ->fondo_fijo;

            if ($fondo_fijo) {
                // Si es fondo fijo, genera el asiento correspondiente a FF
                $params_ff = [
                    $p_id_op,
                    $p_id_usuario,
                    0
                ];

                $v_id_cabecera = DB::select('SELECT generar_libro_compra_manual_ff(?, ?, ?)::integer', $params_ff)[0]->generar_libro_compra_manual_ff;
            } else {
                // Si es factura contado y no es de fondo fijo vamos a generar el asiento de contrapartida de transicion

                    
                if($tiene_forma_pago){

                    if($total_lcs_factura == 0){
                        $total_op_calculado = $total_lcs_nc + $total_anticipos; //monto cotizado segun cambio de libros o Anticipo
                    } else {
                        $total_op_calculado = $total_lcs_factura - $total_lcs_nc; //monto cotizado segun cambio de libros o Anticipo
                    }
    
    
                    //TODO: Hacer el ajuste para evitar poner cotizacion en una LV en guaranies, ahora hacemos un parche
                    if($v_moneda_op != 111){
                        $v_cotizacion_promedio = round($cotizaciones_total /$contador_cotizaciones);    
                    } else {
                        $v_cotizacion_promedio = 1;    
                    }
                } else {
    
                     //TODO: Hacer el ajuste para evitar poner cotizacion en una LV en guaranies, ahora hacemos un parche
                     if($v_moneda_op != 111){
                        $total_op_calculado = round($v_total_op * $v_cotizacion_op); //Cotizamos
                    } else {
                        $total_op_calculado = $v_total_op;
                        $v_cotizacion_promedio = 1;    
                    }
                }
    

               
                // PROVEEDORES DEL EXTERIOR O LOCALES SEGÚN OP CABECERA
                $params_asientos = [
                    $v_id_cuenta_contable_proveedor,
                    $importe_total_op, //total original
                    $total_op_calculado, //total de montos cotizados con sus cotizaciones 
                    'D', //CONTRA PARTIDA
                    $p_id_usuario,
                    3,
                    $v_moneda_op,
                    $v_cotizacion_promedio,
                    0,
                    $v_concepto,
                    $v_id_sucursal_contable,
                    $v_id_centro_costo,
                    $v_id_empresa,
                    $p_id_op,
                    $fecha_hora_operacion
                ];
                $v_id_cabecera = DB::select('SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos_custom;

                if(!$tiene_forma_pago && count($c_anticipos) == 0){
                     // PROVEEDORES DEL EXTERIOR O LOCALES SEGÚN OP CABECERA
                    $params_asientos = [
                        $v_id_cuenta_contable_proveedor,
                        $importe_total_op, //total original
                        $total_op_calculado, //total de montos cotizados con sus cotizaciones 
                        'H',
                        $p_id_usuario,
                        3,
                        $v_moneda_op,
                        $v_cotizacion_promedio,
                        $v_id_cabecera,
                        $v_concepto,
                        $v_id_sucursal_contable,
                        $v_id_centro_costo,
                        $v_id_empresa,
                        $p_id_op,
                        $fecha_hora_operacion
                    ];
                    $v_id_cabecera = DB::select('SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos_custom;
                }
               
            }

        }


          /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												GASTOS ADMINISTRATIVOS
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */


        // POR CADA GASTO SE GENERA UN ASIENTO, SI ES + ENTONCES VA A AL DEBE Y SI ES - AL HABER
            $v_total_gastos = 0;
            foreach ($c_gastos_op as $gasto) {
                if ($gasto->tipo == '+') {
                    $params_asientos = [
                        $gasto->id_cuenta_contable,
                        $gasto->monto_moneda_original,
                        'D',
                        $p_id_usuario,
                        3,
                        $gasto->id_moneda_gasto,
                        $gasto->cotizacion_gasto,
                        $v_id_cabecera,
                        $v_concepto,
                        $v_id_sucursal_contable,
                        $v_id_centro_costo,
                        $v_id_empresa,
                        $p_id_op,
                        $fecha_hora_operacion
                    ];
                    $v_id_cabecera = DB::select('SELECT insertar_asientos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos;
                } else {
                    $params_asientos = [
                        $gasto->id_cuenta_contable,
                        $gasto->monto_moneda_original,
                        'H',
                        $p_id_usuario,
                        3,
                        $gasto->id_moneda_gasto,
                        $gasto->cotizacion_gasto,
                        $v_id_cabecera,
                        $v_concepto,
                        $v_id_sucursal_contable,
                        $v_id_centro_costo,
                        $v_id_empresa,
                        $p_id_op,
                        $fecha_hora_operacion
                    ];
                    $v_id_cabecera = DB::select('SELECT insertar_asientos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos;
                }
                $v_total_gastos += $gasto->monto ?? 0;
            }
        


        /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												        ANTICIPOS
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */
            // SI ES ORIGEN AUTOMÁTICO
            if ($v_origen == 'PP') {
                foreach ($c_anticipos as $anticipo) {

    

                    //TODO: Aqui vamos a quitar el valor negativo del monto de anticipo de op detalle, ver mas adelante de evitar eso
                    $monto_anticipo = $anticipo->monto;
                    if($anticipo->monto < 0){
                        $monto_anticipo = $anticipo->monto * -1;
                    }
                

                    // SI ES PROVEEDOR LOCAL
                    if ($v_id_pais == 1455) {
                        $params_asientos_proveedor = [
                            DB::select('SELECT get_id_cuenta_contable(?, ?, ?)', ['ANTICIPOS_PROV_LOCALES', $v_id_empresa, $anticipo->id_moneda])[0]->get_id_cuenta_contable,
                             $monto_anticipo,
                            'H',
                            $p_id_usuario,
                            8,
                            $anticipo->id_moneda,
                            $anticipo->cotizacion,
                            $v_id_cabecera,
                            $v_concepto,
                            $anticipo->id_sucursal_contable,
                            $anticipo->id_centro_costo,
                            $v_id_empresa,
                            $p_id_op,
                            $fecha_hora_operacion
                        ];

                       $v_id_cabecera =  DB::select('SELECT insertar_asientos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos_proveedor)[0]->insertar_asientos;

                    } else {
                        $params_asientos_proveedor = [
                            DB::select('SELECT get_id_cuenta_contable(?, ?, ?)', ['ANTICIPOS_PROV_EXT_USD', $v_id_empresa, $anticipo->id_moneda])[0]->get_id_cuenta_contable,
                             $monto_anticipo,
                            'H',
                            $p_id_usuario,
                            8,
                            $anticipo->id_moneda,
                            $anticipo->cotizacion,
                            $v_id_cabecera,
                            $v_concepto,
                            $anticipo->id_sucursal_contable,
                            $anticipo->id_centro_costo,
                            $v_id_empresa,
                            $p_id_op,
                            $fecha_hora_operacion
                        ];

                       $v_id_cabecera =  DB::select('SELECT insertar_asientos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos_proveedor)[0]->insertar_asientos;
                    }
                }
            }


        /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												        RETENCIÓN
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */

        if (!is_null($v_total_retencion) && $v_total_retencion != 0) {
            // Diferencia entre la retención y la cotización

            if($regenerar_op && $v_moneda_op == 111){
                $v_cotizacion_del_dia = 1;
            }

            if($regenerar_op && $v_moneda_op != 111 ){
                dd('No se puede regenerar porque falta logica de cotizacion retencion');
            }
            // Insertar asientos
            $v_id_cabecera = DB::select(
                DB::raw("SELECT insertar_asientos(
            (SELECT get_id_cuenta_contable('RETENCION', :v_id_empresa, null)), 
             :v_total_retencion, 
             'H', 
             :p_id_usuario, 
              3, 
              :v_moneda_op, 
              :v_cotizacion_retencion, 
              :v_id_cabecera, 
              :v_concepto, 
              :v_id_sucursal_contable, 
              :v_id_centro_costo, 
              :v_id_empresa, 
              :p_id_op,
              :fecha_hora_operacion)"), 
            [
            'v_id_empresa' => $v_id_empresa,
            'v_total_retencion' => $v_total_retencion,
            'p_id_usuario' => $p_id_usuario,
            'v_moneda_op' => $v_moneda_op,
            'v_cotizacion_retencion' => $v_cotizacion_del_dia,
            'v_id_cabecera' => $v_id_cabecera,
            'v_concepto' => $v_concepto,
            'v_id_sucursal_contable' => $v_id_sucursal_contable,
            'v_id_centro_costo' => $v_id_centro_costo,
            'v_id_empresa' => $v_id_empresa,
            'p_id_op' => $p_id_op,
            'fecha_hora_operacion' => $fecha_hora_operacion
            ])[0]->insertar_asientos;
        }


        /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												       BANCOS (CUENTAS)
         *                                                     ASIENTO DE PAGO
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */
        //CUANDO ES UNA OP QUE SE CANCELA FACTURA CONTRA NOTA DE CREDITO SEUELE SER DE VALOR CERO
         if($tiene_forma_pago){
            $v_id_cabecera =  DB::select("SELECT insertar_asientos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
                $v_id_cuenta_contable_pago,
                $v_importe_total_pago,
                'H',
                $p_id_usuario,
                3,
                $v_id_moneda_pago,
                $v_cotizacion_op_forma_pago,
                $v_id_cabecera,
                $v_concepto,
                $v_id_sucursal_contable,
                $v_id_centro_costo,
                $v_id_empresa,
                $p_id_op,
                $fecha_hora_operacion
            ])[0]->insertar_asientos;
         }

        /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												        DIFERENCIA DE CAMBIO
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */
         //CUANDO ES UNA OP QUE SE CANCELA FACTURA CONTRA NOTA DE CREDITO SEUELE SER DE VALOR CERO
         if($tiene_forma_pago){

            // Suma y resta de valores
            $v_diferencia_asiento = DB::table('asientos_contables_detalle')
            ->selectRaw('SUM(debe) - SUM(haber) as diferencia')
            ->where('id_asiento_contable', $v_id_cabecera)
            ->first()
            ->diferencia;
            

            // Ejecución de la función generar_diferencia_cambio
            DB::statement("SELECT generar_diferencia_cambio(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
                $v_id_cabecera, //p_id_asiento
                $p_id_usuario, //p_id_usuario
                $v_concepto, //p_concepto
                $v_id_sucursal_contable, //p_id_sucursal_contable
                $v_id_centro_costo, //p_id_centro_costo
                $v_id_empresa, //p_id_empresa
                $v_diferencia_asiento, //p_diferencia
                1, //p_cotizacion //La diferencia es en la moneda de GS no necesita cotizacion //Patty
                111, //p_id_moneda
                true, //p_es_diferencia_cambio
                3, //p_id_origen
                $p_id_op //p_nro_documento
            ]);
        }



        /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
         * 
         */ 
 
             // Se actualiza el estado de la OP a procesado
             DB::update("UPDATE op_cabecera SET 
                                id_estado = 53, 
                                id_usuario_procesado = ?, 
                                fecha_hora_procesado = ?, 
                                id_asiento = ?
                                WHERE id = ?", [
                 $p_id_usuario,
                 $fecha_hora_operacion,
                 $v_id_cabecera,
                 $p_id_op
             ]);
         
           

            // Si es factura contado se le asigna el mismo asiento que la OP
            if ($v_id_tipo_documento == 5) {
                DB::update("UPDATE libros_compras SET id_asiento = ? WHERE fondo_fijo = false AND id IN (SELECT id_libro_compra FROM op_detalle WHERE id_cabecera = ?)", [
                    $v_id_cabecera,
                    $p_id_op
                ]);
            }



            // Una vez procesada el LC, pendientecontinuación...

            if(!$regenerar_op){
                // pasa a ser falso
                DB::update("UPDATE libros_compras SET pendiente = false WHERE id IN (SELECT id_libro_compra FROM op_detalle WHERE id_cabecera = ? and id_nc is null)", [
                    $p_id_op
                ]);

                // Una vez procesada el LC con nota de crédito, pendiente pasa a ser falso
                DB::update("UPDATE libros_compras SET pendiente = false WHERE id IN (SELECT id_libro_compra FROM op_detalle WHERE id_cabecera = ? and id_nc is not null)", [
                    $p_id_op
                ]);

                // Una vez procesado el anticipo, pendiente pasa a ser falso
                DB::update("UPDATE anticipos SET pendiente = false WHERE id IN (SELECT id_anticipo FROM op_detalle WHERE id_cabecera = ?)", [
                    $p_id_op
                ]);
            }


            // Se actualiza el id_asiento de anticipo
            DB::update("UPDATE anticipos SET id_asiento = ? WHERE id_op = ?", [
                $v_id_cabecera,
                $p_id_op
            ]);

            if($regenerar_op){
                //Anular asiento anterior
                $asiento = Asiento::find($op_regnerar->id_asiento);
                $asiento->id_usuario_anulacion = 1;
                $asiento->fecha_hora_anulacion = date('Y-m-d H:i:00');
                $asiento->activo = false;
                $asiento->save();
            }
 

    }

    public function anular_op($p_id_op, $p_id_usuario, $p_option){

        Log::debug('Se procede a anular la op '.$p_id_op.' Usuario: '.$p_id_usuario);

        $puede_anualar = $this->datosPermiso('anular_op',$p_id_usuario );

        if(!$puede_anualar){
            throw new ExceptionCustom('No puede anular la OP. No tiene los permisos necesarios');
        }

        $anticipo_op = Anticipo::where('id_op',$p_id_op)->where('activo',true)->first();

        if($anticipo_op){ 
            if($anticipo_op->importe != $anticipo_op->saldo){
                throw new ExceptionCustom('El anticipo ya se aplicó total o parcialmente');
            }
        }

        $op = OpCabecera::with('opDetalle')->findOrFail($p_id_op);

        if($op->origen == 'NC'){
            throw new ExceptionCustom('La op es de origen Nota de credito y no puede ser anulado, ponerse en contacto con soporte');
        }

        /**
         * ---- ESTADOS -----
         * 54 Anulado
         * 53 Procesado
         * 52 Pendiente
         * 49 Verificado
         * 50 Autorizado
         */

         //Si no esta anulado
        if($op->id_estado != 54){

            //Si esta pendiente o es pasar a pendiente
            if($op->id_estado == 52 || $p_option == 52){

                $this->desactivarOp($op, $p_id_usuario, $anticipo_op);
            }

            if($op->id_estado == 49 || $p_option == 49){
                
                if($op->origen != 'AN'){

                    //Eliminar forma de pago
                    $forma_pago = FormaPagoOpCabecera::with('fp_detalle')->where('id_op',$p_id_op)->first();
                    $forma_pago->fp_detalle()->delete();
                    $forma_pago->delete();

                    //Eliminar cheque
                    ChequeFp::where('id_op',$p_id_op)->delete();

                    //Eliminar gastos y descuestos
                    GastoOp::where('id_op',$p_id_op)->delete();

                    //-PASAR A ESTADO PENDIENTE, QUITAR RELACION CON FORMA DE PAGO Y QUITAR INDICE DE COTIZACION
                     $op->id_estado = 52;
                     $op->fecha_hora_verificado = null;
                     $op->id_usuario_verificado = null;
                     $op->id_forma_pago = null;
                     $op->indice_cotizacion = null;
                     $op->cotizacion_especial = null;
                     $op->id_usuario_cotizacion_especial = null;
                     $op->fecha_cotizacion_especial = null;
                     $op->total_gastos = 0;
                     $op->total_descuento = 0;
                     $op->save();

                } else {

                     $op->id_estado = 52;
                     $op->fecha_hora_verificado = null;
                     $op->id_usuario_verificado = null;
                     $op->indice_cotizacion = null;
                     $op->cotizacion_especial = null;
                     $op->id_usuario_cotizacion_especial = null;
                     $op->fecha_cotizacion_especial = null;
                     $op->save();

                }
            }

            //SI LA OP ESTA AUTORIZADA
            if($op->id_estado == 50 || $p_option == 50){
                $op->id_estado = 49;
                $op->fecha_hora_autorizado = null;
                $op->id_usuario_autorizado = null;
                $op->save();
            }

            //SI LA OP ESTA PROCESADA
            if($op->id_estado == 53 || $p_option == 53){


                //SE LIBERAN LOS ITEMS DE LA OP
                foreach ($op->opDetalle as  $detalle) {


                    //-- BUSCA LIBRO COMPRA QUE NO SE ENCUENTRE EN OP QUE NO SEAN PROCESADOS Y ANULADOS
                    //-- IGNORAR OP ACTUAL
                    $lc_pendiente = DB::select("SELECT oc.nro_op
                        FROM op_cabecera oc
                        JOIN op_detalle od ON od.id_cabecera = oc.id
                        WHERE id_estado NOT IN(53,54) 
                        AND od.id_libro_compra IS NOT NULL
                        AND od.id_libro_compra = ?
                    ",[$detalle->id_libro_compra]);
                    
                    if(count($lc_pendiente)){
                        $lc_op = '';
                        foreach ($lc_pendiente as  $value) {
                            $lc_op .= $value->nro_op.',';
                        }

                        throw new ExceptionCustom('No se puede anular porque el item de libro compra se encuentra en una OP pendiente de proceso. OP: '.$lc_op);
                    }
                    

                    $libro_compra = LibroCompra::find($detalle->id_libro_compra);
                    if($libro_compra && $libro_compra->pendiente){
                        throw new ExceptionCustom('No se puede anular porque el item de libro compra se encuentra bloqueado por otro proceso.');
                    }

        
                }//foreach

                $forma_pago = FormaPagoOpCabecera::with('fp_detalle')->where('id_op',$p_id_op)->first();

                 //Validar si es de monto 0
                 /**
                  * Si es de monto cero vamos a anular la op al 100% y quitar todos los pasos ya realizados !!
                  */
                  
                //--COLOCAR LA OP EN ESTADO AUTORIZADO
                $op->id_estado = 50;
                $op->id_usuario_procesado = null;
                $op->fecha_hora_procesado = null;
                $op->save();
                 


                //Buscar los asientos para revertir, con un contra asiento
                $asiento = Asiento::with('asientoDetalle')->find($op->id_asiento);
                
                if($asiento){

                    $asiento_revert = new Asiento();
                    $asiento_revert->id_empresa = $asiento->id_empresa;
                    $asiento_revert->id_usuario = $asiento->id_usuario;
                    $asiento_revert->created_at = date('Y-m-d H:m:s');
                    $asiento_revert->updated_at = date('Y-m-d H:m:s');
                    $asiento_revert->fecha_hora = $op->fecha_hora_autorizado;
                    $asiento_revert->activo = true;
                    $asiento_revert->id_origen_asiento = $asiento->id_origen_asiento;
                    $asiento_revert->nro_documento = $asiento->nro_documento;
                    $asiento_revert->balancea = $asiento->balancea;
                    $asiento_revert->concepto = 'Reversion: '.$asiento->concepto;
                    $asiento_revert->debe = $asiento->haber;
                    $asiento_revert->haber = $asiento->debe;
                    $asiento_revert->numero_grupo_cabecera = $asiento->numero_grupo_cabecera;
                    $asiento_revert->save();

                    //Copiar los asientos existentes y invertir columnas
                    foreach ($asiento->asientoDetalle as $detalle) {

                        $asiento_detalle_revert = new AsientoDetalle();
                        $asiento_detalle_revert->id_asiento_contable = $asiento_revert->id;
                        $asiento_detalle_revert->id_cuenta_contable = $detalle->id_cuenta_contable;
                        $asiento_detalle_revert->monto_original = $detalle->monto_original;
                        $asiento_detalle_revert->id_moneda = $detalle->id_moneda;
                        $asiento_detalle_revert->cotizacion = $detalle->cotizacion;
                        $asiento_detalle_revert->orden = $detalle->orden;
                        $asiento_detalle_revert->concepto_detalle = $detalle->concepto_detalle;
                        $asiento_detalle_revert->numero_grupo = $detalle->numero_grupo;
                        $asiento_detalle_revert->activo = true;
                        $asiento_detalle_revert->id_sucursal = $detalle->id_sucursal;
                        $asiento_detalle_revert->id_centro_costo = $detalle->id_centro_costo;
                        $asiento_detalle_revert->created_at = date('Y-m-d H:m:s');
                        $asiento_detalle_revert->updated_at = date('Y-m-d H:m:s');
                        $asiento_detalle_revert->haber = 0;
                        $asiento_detalle_revert->debe = 0;

                        if($detalle->debe > 0){
                            $asiento_detalle_revert->haber = $detalle->debe;
                        }

                        if($detalle->haber > 0){
                            $asiento_detalle_revert->debe = $detalle->haber;
                        }

                        $asiento_detalle_revert->save();
                    }

                }

                 //--SE ANULAN LOS REGISTROS DE CC
                 $cuenta_corriente = CuentaCorriente::where('id_documento',$p_id_op)->first();
                 $cuenta_corriente->activo = false;
                 $cuenta_corriente->fecha_hora_anulacion = date('Y-m-d H:i:s');
                 $cuenta_corriente->id_usuario_anulacion = $p_id_usuario;
                 $cuenta_corriente->id_documento = $p_id_op; //TODO: ver como diferenciar por tipo de documento !
                 $cuenta_corriente->save();

              

               
                 
               
                 if($forma_pago){
                    //-- SE ANULA EL MOVIMIENTO DE CUENTA CORRIENTE DE LA OP INSERTANDO UN MOVIMIENTO DE AGREGAR SALDO
                    $forma_pago_detalle = $forma_pago->fp_detalle;
                    $v_id_banco_detalle = $forma_pago_detalle[0]->id_banco_detalle;


                    if($v_id_banco_detalle){

                        $concepto                  = 'ANULAR '.$op->concepto;
                        $v_nro_comprobante         = $forma_pago_detalle[0]->nro_comprobante;
                        $v_fecha_emision           = date('Y-m-d H:i:s');
                        $v_fecha_vencimiento       = $forma_pago_detalle[0]->fecha_vencimiento;
                        $v_id_sucursal_contable    = $forma_pago_detalle[0]->id_sucursal;
                        $v_id_centro_costo         = $forma_pago_detalle[0]->id_centro_costo;
                        $v_id_cuenta_contable_pago = $forma_pago_detalle[0]->id_cuenta_contable;
                        $v_cotizacion_op_forma_pago  = $forma_pago_detalle[0]->cotizacion;
                        $v_importe_total_pago      = $forma_pago_detalle[0]->importe_pago;

                         // SE REGISTRAN MOVIMIENTOS EN CTA CTE BANCOS
                         $params_bancos = [
                            $concepto,
                            $v_importe_total_pago,
                            $v_id_banco_detalle,
                            $v_nro_comprobante,
                            $v_fecha_emision,
                            $v_fecha_vencimiento,
                            $v_id_sucursal_contable,
                            $v_id_cuenta_contable_pago,
                            $v_id_centro_costo,
                            $op->id_proveedor,
                            $v_cotizacion_op_forma_pago,
                            $p_id_usuario,
                            13, //-- OPERACION DE ANULACION DE OP
                            $p_id_op
                        ];
                        DB::select('SELECT registrar_movimiento_cta_cte_bancos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_bancos);
                    }
                       
                 }

                 $lc_pago_tc = false;
                 //--SE RECORRE EL DETALLE Y SE CARGA SU SALDO Y LOS ITEMS QUEDAN PENDIENTE DE VUELTA.
                 foreach ($op->opDetalle as  $detalle) {
                   
                    // Log::debug('Devolviendo saldo '.json_encode($detalle));

                   
                    //Casos de NC que tiene saldo monto negativo
                    if(!($detalle->monto > 0.00)){
                        //En los casos de NC
                        $detalle->monto = $detalle->monto * -1;
                    }
                  
                    $libro_compra = LibroCompra::find($detalle->id_libro_compra);
                    if($libro_compra){
                        
                        $libro_compra->saldo = (float) $libro_compra->saldo + $detalle->monto;
                        $libro_compra->pendiente = true;
                        $libro_compra->save();

                         //Verificar si esta asociado a un pago de TC
                        $detalle_proforma = ProformasDetalle::with('proforma')
                        ->where('id_libro_compra',$libro_compra->id)
                        ->where('pagado_con_tc',true)
                        ->count();

                        if($detalle_proforma){
                            $lc_pago_tc =true;
                        }
                       

                        // Log::debug('Devolviendo saldo LC'.json_encode($libro_compra));
                    }

                    $anticipo = Anticipo::find($detalle->id_anticipo);
                    if($anticipo){
                        $anticipo->saldo = $anticipo->saldo + $detalle->monto;
                        $anticipo->pendiente = true;
                        $anticipo->save();
                        // Log::debug('Devolviendo saldo AN'.json_encode($anticipo));
                    }

                    $libro_compra = LibroCompra::find($detalle->id_nc);
                    if($libro_compra){
                        $libro_compra->saldo = (float) $libro_compra->saldo + $detalle->monto;
                        $libro_compra->pendiente = true;
                        $libro_compra->save();

                        // Log::debug('Devolviendo saldo NC'.json_encode($anticipo));
                    }
        

                 }//foreach

               

                 //Cuando no tiene forma de pago se procede a pasar inactivo la op
                 if(!$forma_pago){

                    //Anulacion total de op
                    $op->fecha_hora_autorizado = null;
                    $op->id_usuario_autorizado = null;
                    $op->fecha_hora_verificado = null;
                    $op->id_usuario_verificado = null;
                    $op->save();

                    $this->desactivarOp($op, $p_id_usuario, $anticipo_op);
                 }
	            


            }


        } else {
            throw new ExceptionCustom('Esta OP ya esta anulada');
        }
    }

    private function datosPermiso($accion, $id_usuario){

        $btn = DB::select("SELECT COUNT(pe.id) as total 
                          FROM persona_permiso_especiales p, permisos_especiales pe
                          WHERE p.id_permiso = pe.id 
                          AND p.id_persona =  ? 
                          AND pe.accion = ?", [$id_usuario, $accion])[0]->total;
    
        return $btn ? true : false;
  }//function


    /**
     * Para cuando la op debe liberar los items y y ano estar activo
     */
    private function desactivarOp($op, $p_id_usuario, $anticipo_op){

        //SE ANULA LA OP
        $op->fecha_hora_anulacion = date('Y-m-d H:i:s');
        $op->id_usuario_anulacion = $p_id_usuario;
        $op->activo = false;
        $op->id_estado = 54; //Anulado
        $op->save();

        //SE LIBERAN LOS ITEMS DE LA OP
        foreach ($op->opDetalle as  $detalle) {

            //Casos de NC que tiene saldo monto negativo
            if(!($detalle->monto > 0.00)){
                //En los casos de NC
                $detalle->monto = $detalle->monto * -1;
            }
            
            $libro_compra = LibroCompra::find($detalle->id_libro_compra);
            if($libro_compra){

                //Solo si la OP genero retencion anulamos su retencion
                if($op->total_retencion > 0){
                    $libro_compra->genero_retencion = false;
                }
                
                $libro_compra->pendiente = false;
                $libro_compra->save();

                 //Verificar si esta asociado a un pago de TC
                $detalle_proforma = ProformasDetalle::with('proforma')
                                    ->where('id_libro_compra',$libro_compra->id)
                                    ->where('pagado_con_tc',true)
                                    ->first();

                if($detalle_proforma){
                    $detalle_proforma->pagado_con_tc = false;

                    //Control para facturacion parcial
                    $facturas = Factura::where('id_estado_factura',29)->where('id_proforma',$detalle_proforma->id_proforma)->count();

                    //Si proforma no esta facturado, anulamos el LC automatico generado por el pago de TC
                    if($detalle_proforma->proforma->estado_id != 4 && $facturas == 0){
                        $detalle_proforma->id_libro_compra = null;
                        DB::select("SELECT anular_libro_compra(?,?,?,?)",[$libro_compra->id,$libro_compra->id_usuario,'A','FACTURA']);
                    }

                    $detalle_proforma->save();
                }
            }

            $anticipo = Anticipo::find($detalle->id_anticipo);
            if($anticipo){
                //TODO: Ver porque devolver el saldo al anular la op ??
                // $anticipo->saldo = $anticipo->saldo + $detalle->monto;
                $anticipo->pendiente = false;
                $anticipo->save();
            }


            $libro_compra = LibroCompra::find($detalle->id_nc);
            if($libro_compra){
                $libro_compra->pendiente = false;
                $libro_compra->save();
            }

            //--SE ELIMINA LA RETENCION
            Retencion::where('id_libro_compra',$detalle->id_libro_compra)->delete();
        }

        //Anula el anticipo
        if($anticipo_op){ 
            $anticipo_op->activo = false;
            $anticipo_op->id_usuario_anulacion = $p_id_usuario;
            $anticipo_op->fecha_hora_anulacion = date('Y-m-d H:i:s');
            $anticipo_op->save();
        }

        //Eliminar el adjunto de la op
        try {
            $adjuntos = IntermediaAdjuntoOp::where('op_cabecera_id',$op->id)
            ->get();
          
            foreach ($adjuntos as $key => $adjunto) {
                unlink('adjuntoDetalle/documentos_op/' . $adjunto->archivo); 
                $adjunto->delete(); 
            }
               
            
        } catch (\Exception $e) {
            Log::error($e);
        }

    }


     /**
    * OBTENER RETENCION
    * RETORNA MONTO, RETENCION(BOOLEAN), ERROR
    */
    public function retencion($obj)
    { 
            //REFACTORIZAR PARA RECIBIR UN LISTADO DE ID DE LIBRO COMPRA PARA INSERTAR LOS DATOS.
            $response = new \StdClass; 
            $idEmpresa = $this->getIdEmpresa();
            $response->err = true;
            $response->retencion = false;
            $response->result = 0;
            $response->monto = 0;
            $result = [];
  
            /**
             * TODO: por hacer
             * Verificar que el monto a pagar sea mayor a 0
             * Verificar que sea de paraguay
             * Verificar si se debe de retener el IVA, segun config de persona
             * Verificar si alcanzo el minimo imponible en el mes
             *  - Sumar todos los documentos LC en OP no anuladas de ese proveedor
             *  - Sumar todos los documentos NC en OP no anuladas de ese proveedor
             *  - Restar el total NC del total LC, si supera el monto imponible entonces aplicamos
             * - Sumar todos los LC de la op que no hayan generado retencion
             * - Sumar todos los NC de la op
             * - Restar los totales y validar que sea mayor a 0
             * - Restar las gravada de LC y NC , sobre este monto obtener la retencion
             * 
             * 
             * Al momento de anular tener en cuenta saber quien tiene retencion para anular la generacion de retencion del LC
             * El campo genero_retencion : es por los casos de pago parcial , para no volver a generar de nuevo sobre el documento
             * Al carga una LC contado , debe calcular retencion (crear op debe ser una funcion local)
             * Porque al procesar la op recien le pone la marca de genero_retencion..?
             */
  
             $total_pago = 0;
             $total_gravadas_nc = 0;
             $total_gravadas_factura = 0;
             $id_ncs = [];
             $id_lcs = [];
             $total_facturas_usuario = 0;
             $total_notas_credito_usuario = 0;
             $aplica_retencion = false;
             $monto_imponible = (int)DB::select("SELECT get_parametro('monto_imponible') as get_parametro")[0]->get_parametro;
             $porcentaje_retencion = (float)DB::select("SELECT get_parametro('porcentaje_retencion') as get_parametro")[0]->get_parametro ;
             $empresa_retentor = DB::select("SELECT agente_retentor FROM empresas where id = $idEmpresa")[0]->agente_retentor;
             $id_moneda = 0;
             $total_gravadas_cotizado_gs = 0;
             $total_gravada_10_original = 0;
             $total_gravada_5_original = 0;
  
             $proveedor_retencion = Persona::where('id',$obj->id_proveedor)
             ->where('retener_iva',true)
             ->where('id_pais',1455)
             ->count();
  
             //Si es cero no se realiza retencion, porque el proveedor no es PY o porque no se puede retener IVA
             //Si empresa retentor es false, entonces la empresa no debe realizar retenciones
             if($proveedor_retencion == 0 || !$empresa_retentor){
              return $response;
             }
  
  
  
             /**
              * Calculas el total de las facturas y NC de la OP a generar
              */
             foreach ($obj->data as $data) {
                $data = (Object)$data;
                $total_pago += $data->operacion['monto_pago'];
                
                /**
                 * El campo costo_gravada es la suma de la gravada 10 y 5
                 */
                if($data->operacion['id_nc']){
                  $nc_data = LibroCompra::select(
                            DB::raw('get_monto_cotizado_asiento(costo_gravada,id_moneda,111, cotizacion_contable_venta) as costo_gravada_cotizado'),
                            '*')
                  ->where('id',$data->operacion['id_nc'])->first();
                  $total_gravadas_cotizado_gs -= $nc_data->costo_gravada_cotizado;
                  $total_gravada_10_original -= $nc_data->iva10;
                  $total_gravada_5_original -= $nc_data->iva5;
                }
  
                if($data->operacion['id_compra']){
                  $fact_data = LibroCompra::select(DB::raw('get_monto_cotizado_asiento(costo_gravada,id_moneda,111, cotizacion_contable_venta) as costo_gravada_cotizado'),'*')
                  ->where('id',$data->operacion['id_compra'])->where('genero_retencion',false)->first();
                  if($fact_data){
                    $total_gravadas_cotizado_gs += $fact_data->costo_gravada_cotizado;
                    $total_gravada_10_original += $fact_data->iva10;
                    $total_gravada_5_original += $fact_data->iva5;
                  }
                  
                }
             
             }
             
             if($total_pago > 0 && $total_gravadas_cotizado_gs > $monto_imponible){
              $aplica_retencion = true;
             }
  
             //Si no aplica buscar en todos los documentos de OP ya generadas
             if(!$aplica_retencion && $total_pago > 0){
                // Tipo documento
                // FC : 1,5,22,33
                // NC : 2,32
  
                /**
                 * Total de las gravadas de los documentos pagados
                 */
                $ops_mes_actual = OpDetalle::whereHas('opCabecera',function($q) use ($obj){
                  $q->where('fecha_hora_creacion','>',date('Y-m-01 00:00:00'));
                  $q->where('id_proveedor',$obj->id_proveedor);
                  $q->where('id_estado','<>',54);
                  
                })->get();
  
                if(count($ops_mes_actual)){
  
                  $total_gravada_facturas = LibroCompra::select(DB::raw('get_monto_cotizado_asiento(costo_gravada,id_moneda,111, cotizacion_contable_venta) as costo_cotizado'))
                  ->whereIn('id',$ops_mes_actual->pluck('id_libro_compra'))->get()->sum('costo_cotizado');
                  $total_gravada_nota_credito = LibroCompra::select(DB::raw('get_monto_cotizado_asiento(costo_gravada,id_moneda,111, cotizacion_contable_venta) as costo_cotizado'))
                  ->whereIn('id',$ops_mes_actual->pluck('id_nc'))->get()->sum('costo_cotizado');
  
                  $total_gravada_mes = $total_gravada_facturas - $total_gravada_nota_credito;
                  
                  if($total_gravada_mes > $monto_imponible){
                    $aplica_retencion = true;
                  }
                }
             }
  
             if($aplica_retencion){
                $response->retencion = true;
  
                $redondeo = $obj->moneda != 111 ? 2 : 0;
                $response->monto += $total_gravada_10_original > 0 ? round(($total_gravada_10_original/100) * $porcentaje_retencion, $redondeo ) : 0;
                $response->monto += $total_gravada_5_original > 0 ? round(($total_gravada_5_original/100) * $porcentaje_retencion, $redondeo ) : 0;
             }
  
            
     
  
            return $response;
    }

    private function getIdEmpresa() {
        return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
    }

    private function getIdUsuario(){
        return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
    }

    /**
     * Funcion para realizar el pago con TC de un ticket o de una reserva de un detalle de proforma
     * recibe como parametro id del ticket para obener el LC o id_proforma_detalle para generar un LC y poner la op en autorizado y bloquear ese detalle
     */
    public function generar_pago_tc($id_ticket, $id_proforma_detalle, $request){

        $porcentaje_retencion = null;
        $base_imponible = null;
        $total_retencion = null;
        
        $id_empresa = $this->getIdEmpresa();
        $id_estado_op =  50;//AUTORIZADO  --> Debe pasar a procesado
        $tipo_operacion = 11;   

        /**
         * Si es un ticket
         * > Debemos obtener el LC del ticket para  > Vendedor es el usuario
         * > Generar la OP y dejarlo autorizado > Vendedor es el usuario
         * > Agregar forma de pago TC > Vendedor es el usuario
         * > Procesar la OP > Vendedor es el usuario
         * 
         * Si es un detalle de proforma
         * > Generar un LC Automatico > Vendedor es el usuario
         * > Generar los asientos correspondientes > Vendedor es el usuario
         * > Setear el item de detalle de proforma con un id_libro_compra y poner en una columna que pagado_con_tc = true
         * > Generar una OP y dejarlo autorizado > Vendedor es el usuario
         * > Agregar forma de pago TC
         * > Procesar la OP > Vendedor es el usuario
         */


  
       

        $tarjetas = DB::select("SELECT bd.id id_tarjeta, 
                                       bd.numero_cuenta as nro_tarjeta, 
                                       bc.nombre as banco,
                                       bd.id_moneda,
                                       bd.id_cuenta_contable
                                FROM banco_detalle bd,banco_cabecera bc, currency m
                                WHERE bd.id_tipo_cuenta = 4 
                                and bd.id_moneda = m.currency_id
                                and bc.activo = true
                                and bd.activo = true
                                and bd.id_banco  = bc.id
                                and bd.id =".$request->input('tarjeta'));
        
         $currency_code_lc = '';

         if($id_ticket){

            $tickMonto = Ticket::where('id',$id_ticket)->first(['total_ticket']);
                            //Usamos id_tipo_documento_hechauka para identificar LC de tipo pasajero aereo
            $libro_compra = LibroCompra::where('id_tipo_documento_hechauka',13)
                            ->where('id_documento',$id_ticket)->first();


            if(!$libro_compra){
                throw new ExceptionCustom('No se encontro el libro compra del ticket');
            }

            if($libro_compra->pendiente){
                throw new ExceptionCustom('El libro compra del ticket ya se encuentra en una OP');
            }

            if($libro_compra->saldo <= 0){
                throw new ExceptionCustom('El saldo del libro compra es 0');
            }

            $currency_code_lc = Currency::find($libro_compra->id_moneda)->currency_code;
            $proveedor_py = Persona::where('id',$libro_compra->id_persona)->where('id_pais',1455)->count();
            $id_usuario = $this->getIdUsuario();

         } else if($id_proforma_detalle){

            $proforma_detalle = ProformasDetalle::with('proforma','producto','proveedor')
                                ->where('id',$id_proforma_detalle)->where('activo',true)
                                ->first();

            if(!$proforma_detalle){
                throw new ExceptionCustom('El item de proforma ya no se encuentra activo');
            }

            $vendedor = Persona::where('id',$proforma_detalle->proforma->id_usuario)->first();
            $sucursal = SucursalEmpresa::where('id_persona',$vendedor->id_sucursal_empresa)->first();

            if(!$sucursal){
                throw new ExceptionCustom('Sucursal empresa no disponible para usuario de la proforma');
            }

            $ruc = $proforma_detalle->proveedor->dv != '' || $proforma_detalle->proveedor->dv != null ? $proforma_detalle->proveedor->documento .'-'.$proforma_detalle->proveedor->dv : $proforma_detalle->proveedor->documento;

            $data_lc = [];
            $data_lc['id_cliente'] = $proforma_detalle->id_proveedor;
            $data_lc['id_sucursal'] = $sucursal->id;
            $data_lc['fecha_hora_facturacion'] = date('Y-m-d H:m:s');
            $data_lc['cliente_ruc'] = $ruc;
            $data_lc['id_timbrado'] = null;
            $data_lc['id_tipo_factura'] = 1;//Credito
            $data_lc['nro_documento'] = $proforma_detalle->id.'-'.$proforma_detalle->id_proforma;
            $data_lc['id_moneda'] = $proforma_detalle->currency_costo_id;
            $data_lc['importe'] = $proforma_detalle->precio_costo;//(revisar)
            $data_lc['saldo'] = $proforma_detalle->precio_costo;//(revisar)
            $data_lc['costo_venta'] = 0;
            $data_lc['id_tipo_documento_hechauka'] = 1; //FACTURA PRE IMPRESA
            $data_lc['cotizacion'] = $proforma_detalle->cotizacion_costo;
            $data_lc['cotizacion'] = $proforma_detalle->cotizacion_costo;
            $data_lc['costo_exenta'] = $proforma_detalle->costo_proveedor; //Los costos del exterior son exentas
            $data_lc['costo_gravada'] = $proforma_detalle->costo_gravado;
            $data_lc['iva10'] = 0;
            $data_lc['iva5'] = 0;
            $data_lc['gravadas10'] = 0;
            $data_lc['gravadas5'] = 0;
            $data_lc['id_centro_costo'] =  $sucursal->id_sucursal_contable;//ver
            $data_lc['concepto'] = 'PAGO TC DETALLE DE PROFORMA '.$proforma_detalle->id_proforma.' DETALLE ID:'.$proforma_detalle->id;
            $data_lc['id_usuario'] = $proforma_detalle->proforma->id_usuario;
            $data_lc['id_empresa'] = $proforma_detalle->proforma->id_empresa;
            $data_lc['id_tipo_documento'] = 22; //FACTURA CREDITO PROVEEDOR
            $data_lc['cod_confirmacion'] = $proforma_detalle->cod_confirmacion;
            $data_lc['retencion_renta'] = null;
            

            $data_lc['id_cuenta_contable_producto'] = $proforma_detalle->producto->id_plan_cuenta;
            $data_lc['id_pais'] = $proforma_detalle->proveedor->id_pais;
            $data_lc['id_proforma_detalle'] = $proforma_detalle->id;
            $data_lc['id_proforma'] = $proforma_detalle->id_proforma;
            $data_lc['id_documento'] = $proforma_detalle->id;



            $libro_compra = $this->generar_libro_compra($data_lc);
            $proveedor_py = $proforma_detalle->proveedor->id_pais == 1455;
            $id_usuario = $proforma_detalle->proforma->id_usuario;

            $proforma_detalle->pagado_con_tc = true;
            $proforma_detalle->id_libro_compra = $libro_compra->id;
            $proforma_detalle->save();
            $currency_code_lc = Currency::find($libro_compra->id_moneda)->currency_code;

         } else {
            throw new ExceptionCustom('Error en proceso interno del sistema');
         }


            $nro_op_funct = DB::select("SELECT public.get_documento(".$id_empresa.",'OP')");
            $nro_op = $nro_op_funct[0]->get_documento;
            $total_pago = $libro_compra->saldo;
            $total_retencion = $libro_compra->total_retencion;
            $total_gravadas = $libro_compra->gravadas10 + $libro_compra->gravadas5; 
            $id_moneda =  $libro_compra->id_moneda;
            $id_sucursal = $libro_compra->id_sucursal; 
            $centro_costo = $libro_compra->id_centro_costo;
            $cotizacion = $libro_compra->cotizacion_contable_venta;
            $id_proveedor = $libro_compra->id_persona;
            $total_pago_neto = $total_pago;
            $moneda_tc = $tarjetas[0]->id_moneda;
            $total_forma_pago = DB::select('SELECT public.get_monto_cotizado('.floatval($total_pago).','.$id_moneda.','.$moneda_tc.','.$id_empresa.')');
            $total_forma_pago = $total_forma_pago[0]->get_monto_cotizado;

            if($proveedor_py){

                $obj = new \StdClass; 
                $obj->id_proveedor = $libro_compra->id_persona;
                $obj->moneda = $id_moneda;
                $obj->data[] = ['operacion' => [
                  'monto_pago' => $total_pago,
                  'id_compra' => $libro_compra->id,
                  'id_nc' => null
                ]];
                $result_retencion = $this->retencion($obj); //Funcion de OpTrait

                if($result_retencion->retencion){
                    $consulta = DB::select("SELECT get_parametro('base_imponible') as base_imponible, 
                                                          get_parametro('porcentaje_retencion') as porcentaje_retencion");                               
                    $base_imponible =  $consulta[0]->base_imponible;
                    $porcentaje_retencion = $consulta[0]->porcentaje_retencion;  
                    $total_retencion =  $result_retencion->monto;
                }
                
                $id_cuenta_contable = DB::select("SELECT get_id_cuenta_contable('PROV_LOCALES',".$id_empresa.",".$id_moneda.") AS n")[0]->n;

                if(!$id_cuenta_contable){
                    throw new ExceptionCustom('Falta configurar PROV_LOCALES');
                }

              } else {
                $id_cuenta_contable = DB::select("SELECT get_id_cuenta_contable('PROVEEDORES_EXT',".$id_empresa.",null)  AS n")[0]->n;

                if(!$id_cuenta_contable){
                    throw new ExceptionCustom('Falta configurar PROVEEDORES_EXT');
                }

              }

                  $cabecera_op = [
                                  'nro_op'=> $nro_op,
                                  'id_usuario_creacion'=> $id_usuario,
                                  'id_estado'=>$id_estado_op,
                                  'total_pago'=> $total_pago, 
                                  'total_neto_pago'=>$total_pago_neto,
                                  'total_anticipo'=>0,
                                  'total_nota_credito'=>0,
                                  'total_retencion'=>$total_retencion,
                                  'total_gravadas'=>$total_gravadas,
                                  'id_moneda'=> $id_moneda,
                                  'id_sucursal'=>$id_sucursal,
                                  'id_centro_costo'=>$centro_costo,
                                  'cotizacion'=>$cotizacion,
                                  'cotizacionoperativa'=>$cotizacion,
                                  'id_proveedor'=>$id_proveedor,
                                  'id_empresa'=>$id_empresa,
                                  'id_cuenta_contable'=> $id_cuenta_contable,
                                  'base_imponible'=>$base_imponible,
                                  'porcentaje_retencion'=>$porcentaje_retencion,
                                  'id_usuario_verificado'=> $id_usuario,
                                  'fecha_hora_verificado'=> date('Y-m-d H:i:00'),
                                  'id_usuario_autorizado'=> $id_usuario,
                                  'fecha_hora_autorizado'=> date('Y-m-d H:i:00'),
                                  'origen'=>'PP',
                                  'fondo_fijo'=>false
                                ]; 

                    $id_cabecera_op =  DB::table('op_cabecera')->insertGetId ($cabecera_op);  

                    $detalles_op[] = [
                                      'id_libro_compra'=> $libro_compra->id,
                                      'id_anticipo'=> null,
                                      'monto'=>$total_pago,
                                      'id_cabecera' => $id_cabecera_op
                                      ];

                    DB::table('op_detalle')->insert($detalles_op);   

                    $formaPagoCabecera = new FormaPagoOpCabecera;  
                    $formaPagoCabecera->id_usuario = $id_usuario;
                    $formaPagoCabecera->importe_total = $total_forma_pago;
                    $formaPagoCabecera->id_moneda = $moneda_tc;
                    $formaPagoCabecera->cotizacion = $cotizacion;
                    $formaPagoCabecera->id_op = $id_cabecera_op;
                    $formaPagoCabecera->id_empresa = $id_empresa;
                    $formaPagoCabecera->save();

                    if(!$tarjetas[0]->id_cuenta_contable){
                        throw new \ExceptionCustom('Falta configurar cuenta contable de cuenta de fondo.');
                    }

                    $formaPagoDetalle = new FormaPagoOpDetalle;
                    $formaPagoDetalle->id_cabecera = $formaPagoCabecera->id;
                    $formaPagoDetalle->id_cuenta_contable = $tarjetas[0]->id_cuenta_contable;
                    $formaPagoDetalle->id_banco_detalle = $request->input('tarjeta');
                    $formaPagoDetalle->importe_pago = $total_forma_pago;
                    $formaPagoDetalle->nro_comprobante = $request->input('comprobante');
                    $formaPagoDetalle->id_tipo_pago = $tipo_operacion; 
                    $formaPagoDetalle->save();


                    

                    $forma_pago = FormaPagoCliente::where('id', $tipo_operacion)->first(['denominacion']);
                    $proveedor_name = Persona::where('id',$id_proveedor)->first();
                    if($id_proveedor != 0){                  
                      if(!is_null($proveedor_name->getFullNameAttribute())){
                        $nombreCliente = $proveedor_name->getFullNameAttribute();
                      } else {
                        $nombreCliente = $proveedor_name->denominacion_comercial;
                      }
                    }else{
                      $nombreCliente = "";
                    }  
                    $concepto_op = 'OP '.$nro_op.' - '. date('d/mY').' - '.$nombreCliente.' - '.$request->input('comprobante').' - '. $forma_pago->denominacion.' - '.$tarjetas[0]->nro_tarjeta.'-'. $currency_code_lc.' - '.$total_pago;

                    OpCabecera::where('id', $id_cabecera_op)
                              ->update(['id_forma_pago' =>  $formaPagoCabecera->id,
                                      'concepto'=> $concepto_op]);
                    

                    $this->procesar_op($id_cabecera_op, $id_usuario); //Ver trait optrait

                    //Se agrega el adjunto, pero si dio un error no interrumpe el proceso
                    try {
                        if($request->hasFile('adjunto_op')){
                            $file = $request->adjunto_op;
                            $filename = $file->getClientOriginalName();
                            $filename=str_replace(' ','', $filename);
                            $extension = pathinfo($filename, PATHINFO_EXTENSION);
                            $imagen = in_array(strtolower($extension), ['jpg', 'jpeg', 'bmp', 'png', 'pdf', 'doc', 'docx']);
                            $fileWithRandomName = $this->getId4Log() . '_' . $filename;
                     
                            Storage::disk('adjuntoOp')->put($fileWithRandomName, \File::get($file));
                            $uploadedFiles[] = [
                                'archivo' => $fileWithRandomName,
                                'imagen' => $imagen
                            ];
                            $intermediaAdjunto = new IntermediaAdjuntoOp();
                            $intermediaAdjunto->op_cabecera_id = $id_cabecera_op;
                            $intermediaAdjunto->archivo = $fileWithRandomName;
                            $intermediaAdjunto->save();
                        }
                        
                    } catch (\Exception $e) {
                        \Log::error($e);
                    }
              
        
                 DB::table('tickets')->where('id',$request->input('ticket'))
                      ->update([
                                'id_tarjeta_pago'=>$request->input('tarjeta'),
                                'nro_voucher_comprobante'=>$request->input('comprobante'),
                                'id_tipo_pago'=>'TC'
                              ]); 




             return $nro_op;
    }


    

    public function generar_libro_compra($data_lc){
        
        $libroCompra = new LibroCompra;
        $libroCompra->id_sucursal = $data_lc['id_sucursal'];
        $libroCompra->id_persona = $data_lc['id_cliente'];
        $libroCompra->fecha_hora_facturacion = $data_lc['fecha_hora_facturacion'];
        $libroCompra->ruc = $data_lc['cliente_ruc'];
        $libroCompra->id_timbrado = $data_lc['id_timbrado'];
        $libroCompra->fecha = date('Y-m-d H:m:s');//fecha_creacion
        $libroCompra->id_tipo_factura = $data_lc['id_tipo_factura'];
        $libroCompra->nro_documento = $data_lc['nro_documento'];
        $libroCompra->id_moneda = $data_lc['id_moneda'];
        $libroCompra->importe = $data_lc['importe'];
        $libroCompra->saldo = $data_lc['importe'];
        $libroCompra->costo_venta = $data_lc['costo_venta'];
        $libroCompra->id_tipo_documento_hechauka= $data_lc['id_tipo_documento_hechauka'];
        $libroCompra->origen = 'A';
        $libroCompra->cambio = $data_lc['cotizacion'];
        $libroCompra->cotizacion_contable_venta = $data_lc['cotizacion'];
        $libroCompra->costo_exenta = $data_lc['costo_exenta'];
        $libroCompra->costo_gravada = $data_lc['costo_gravada'];
        $libroCompra->iva10 = $data_lc['iva10']; //IVA CALCULADO
        $libroCompra->iva5 =  $data_lc['iva5'];//IVA CALCULADO
        $libroCompra->gravadas10 = (float)$data_lc['gravadas10']; 
        $libroCompra->gravadas5 = (float)$data_lc['gravadas5'];
        $libroCompra->id_centro_costo = $data_lc['id_centro_costo'];
        $libroCompra->concepto = $data_lc['concepto'];
        $libroCompra->id_usuario = $data_lc['id_usuario'];
        $libroCompra->id_empresa = $data_lc['id_empresa'];
        $libroCompra->id_tipo_documento = $data_lc['id_tipo_documento'];
        $libroCompra->id_documento = $data_lc['id_documento']; 
        $libroCompra->id_proforma = $data_lc['id_proforma']; 
        // $libroCompra->diferencia = $data_lc['diferencia'];
        // $libroCompra->nro_venta_pedido = $data_lc['nro_venta_pedido'];
        // $libroCompra->diferencia = $data_lc['diferencia'];
        // $libroCompra->retencion_iva = $data_lc['ret_iva'];
        // $libroCompra->pasajero = $data_lc['pasajero'];
        $libroCompra->cod_confirmacion = $data_lc['cod_confirmacion'];
        $libroCompra->retencion_renta = $data_lc['retencion_renta'];
        $libroCompra->save();

        $v_id_cabecera = 0;
        $concepto = 'PAGO TC DETALLE '.$data_lc['id_proforma_detalle'].' PROFORMA: '.$data_lc['id_proforma'];

        //COSTO
        $params_asientos = [
            $data_lc['id_cuenta_contable_producto'],
            $data_lc['importe'],
            'D',
            $data_lc['id_usuario'],
            2,
            $data_lc['id_moneda'],
            $data_lc['cotizacion'],
            $v_id_cabecera,
            $concepto,
            $data_lc['id_sucursal'],
            $data_lc['id_centro_costo'],
            $data_lc['id_empresa'],
            $data_lc['nro_documento']
        ];
        $v_id_cabecera = DB::select('SELECT insertar_asientos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos;


        //  PROVEEDORES DEL EXTERIOR O LOCAL
        if($data_lc['id_pais'] == 1455){

            $params_asientos = [
                DB::select('SELECT get_id_cuenta_contable(?, ?, ?)', ['PROV_LOCALES', $data_lc['id_empresa'], $data_lc['id_moneda']])[0]->get_id_cuenta_contable,
                $data_lc['importe'],
                'H',
                $data_lc['id_usuario'],
                2,
                $data_lc['id_moneda'],
                $data_lc['cotizacion'],
                $v_id_cabecera,
                $concepto,
                $data_lc['id_sucursal'],
                $data_lc['id_centro_costo'],
                $data_lc['id_empresa'],
                $data_lc['nro_documento']
            ];
            $v_id_cabecera = DB::select('SELECT insertar_asientos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos;

        } else {

             
            $params_asientos = [
                DB::select('SELECT get_id_cuenta_contable(?, ?, ?)', ['PROVEEDORES_EXT', $data_lc['id_empresa'], null])[0]->get_id_cuenta_contable,
                $data_lc['importe'],
                'H',
                $data_lc['id_usuario'],
                2,
                $data_lc['id_moneda'],
                $data_lc['cotizacion'],
                $v_id_cabecera,
                $concepto,
                $data_lc['id_sucursal'],
                $data_lc['id_centro_costo'],
                $data_lc['id_empresa'],
                $data_lc['nro_documento']
            ];
            $v_id_cabecera = DB::select('SELECT insertar_asientos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos;

        }


        //Relacion de asiento a LC
        $lc_update = LibroCompra::find($libroCompra->id);
        $lc_update->id_asiento = $v_id_cabecera;
        $lc_update->save();

        return $lc_update;


    }

    private function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}


    private function regenerar_movimiento_cta_cte_bancos($params_bancos, $p_id_op){

        //Buscar la operacion de op y anular para volver a generar en la fecha
        $cuenta = CuentaCorrienteBanco::where('id_documento',$p_id_op)->where('id_tipo_documento',12)->first();
        if(!$cuenta){
            dd('Error al buscar cuenta corriente banco');
        }
        $cuenta->activo = false;
        $cuenta->id_usuario_anulacion = 1;
        $cuenta->fecha_hora_anulacion = date("Y-m-d H:i:00");
        $cuenta->save();

        DB::select('SELECT registrar_movimiento_cta_cte_bancos(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_bancos);
    }



}

