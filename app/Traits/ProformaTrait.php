<?php 

namespace App\Traits;

use App\Proforma;
use App\ProformasDetalle;
use Illuminate\Support\Facades\DB;
use Session;
use Log;


trait ProformaTrait {

    /**
     * Se encarga de insertr el producto FEE administrativo cuando la proforma supera la renta neta de 50 USD
     */
    public function calcularFeeAdministrativo($id_proforma)
    {
        $proforma = Proforma::findOrFail($id_proforma);
        $v_id_empresa = $proforma->id_empresa;
    
        if ($v_id_empresa == 1) {
            $v_renta_neta = (float)$proforma->renta_neta;
           
            $v_id_moneda_venta = $proforma->id_moneda_venta;
           
            $v_cotizacion_actual =  DB::select("SELECT get_cotizacion(?,?,?)",[143, $v_id_empresa, 0])[0]->get_cotizacion;
            $v_tiene_producto_fee = ProformasDetalle::where('activo', true)
                ->where('id_proforma',$id_proforma)
                ->where('id_producto', 38961)
                ->count();
                
            if ($v_id_moneda_venta == 111) {
                $v_renta_neta = DB::select("SELECT get_monto_cotizacion_custom(?,?,?,?)",[$v_cotizacion_actual, $v_renta_neta, 111, 143])[0]->get_monto_cotizacion_custom;
            }

            if ($v_renta_neta >= 50.00 && $v_tiene_producto_fee == 0) {
                $proforma_detalle = new ProformasDetalle;
                $proforma_detalle->id_proforma = $id_proforma;
                $proforma_detalle->item = 1;
                $proforma_detalle->cod_confirmacion = 'feedtp' . $id_proforma;
                $proforma_detalle->id_producto = 38961;
                $proforma_detalle->precio_costo = 5;
                $proforma_detalle->costo_proveedor = 5; //PRECIO ORIGINAL PARA LC
                $proforma_detalle->costo_gravado = 0;
                $proforma_detalle->precio_venta = 0;
                $proforma_detalle->id_proveedor = 3220;
                $proforma_detalle->id_prestador = 3220;
                $proforma_detalle->descripcion = 'FEE ADMINISTRATIVO';
                $proforma_detalle->currency_venta_id = $v_id_moneda_venta;
                $proforma_detalle->currency_costo_id = 143;
                $proforma_detalle->markup = 0;
                $proforma_detalle->id_empresa = $v_id_empresa;
                $proforma_detalle->porcentaje_comision_agencia = 0;
                $proforma_detalle->origen = 'A';
                $proforma_detalle->cantidad = 1;
                $proforma_detalle->cotizacion_costo = $v_cotizacion_actual;
                $proforma_detalle->save();
                
            }


            //No alcanza la renta pero tiene el producto FEE 
            if($v_renta_neta < 50.00 && $v_tiene_producto_fee > 0){

                    $detalle = ProformasDetalle::where('activo', true)
                    ->where('id_proforma',$id_proforma)
                    ->where('id_producto', 38961)
                    ->first();

                     $detalle->activo =false;
                     $detalle->id_usuario_anulacion = 1;
                     $detalle->fecha_anulacion= date('Y-m-d H:m:i');
                     $detalle->save();
                                                
                    
               DB::select('SELECT public.actualizar_proforma_cabecera('.$id_proforma.')');
            }
        }

    }

    public function calcularFeeCangoroo($id_proforma, $calculo_fee = true)
    {
        // $proforma_detalle = ProformasDetalle::with('proforma')->findOrFail($id_proforma_detalle);
        $proforma = Proforma::find($id_proforma);
        $v_id_empresa = $proforma->id_empresa;

        //============================= PRODUCTOS CONFIG =================================================================
        $productos_cangoroo = [
            38980, //translado
            38979, //Alojamiento
            38984
        ];
        $id_producto_fee = 38985;
        $costo = 2.5; 
        $moneda_costo = 143; //USD
        $venta = 3; 
        $proveedor_cangoroo = 106879;
        $prestador_cangoroo = 106879;
        //============================= PRODUCTOS CONFIG =================================================================
    
        if ($v_id_empresa == 1) {

            $v_id_moneda_venta = $proforma->id_moneda_venta;
            $v_cotizacion_actual =  DB::select("SELECT get_cotizacion(?,?,?)",[143, $v_id_empresa, 0])[0]->get_cotizacion;
            $venta = DB::select("SELECT get_monto_cotizacion_custom(?,?,?,?)",[$v_cotizacion_actual, $venta, $moneda_costo, $v_id_moneda_venta])[0]->get_monto_cotizacion_custom;

            if($calculo_fee){
    
                /**
                 * Determinar cuantos grupo de reserva existe en la proforma para calcular cuantos FEE se debe agregar
                 */
                $proformas_detalle_producto = ProformasDetalle::where('activo', true)
                    ->where('id_proforma',$id_proforma)
                    ->whereIn('id_producto', $productos_cangoroo)
                    ->get(['id_producto','id']);
                   
    
                //DETERMINAR CUANTOS FEE SE DEBEN INSERTAR
               $cuanto_grupo_reserva_existe = DB::table('vw_reservas_cangoroo')
                    ->select('bookingid')
                    ->whereIn('id_proforma_detalle',$proformas_detalle_producto->pluck('id'))
                    ->groupBy('bookingid')
                    ->get()->toArray();
    
               $cuanto_grupo_reserva_existe = count($cuanto_grupo_reserva_existe);
    
                $total_fee_creados= ProformasDetalle::where('activo', true)
                    ->where('id_proforma',$id_proforma)
                    ->where('id_producto', $id_producto_fee)
                    ->count();  
              
                   Log::info(json_encode([$cuanto_grupo_reserva_existe, $total_fee_creados]));
    
                if($cuanto_grupo_reserva_existe > $total_fee_creados){
                    $cantidad_crear = $cuanto_grupo_reserva_existe - $total_fee_creados;
    
                    for ($i=0; $i < $cantidad_crear; $i++) { 
                        $proforma_detalle = new ProformasDetalle;
                        $proforma_detalle->id_proforma = $proforma->id;
                        $proforma_detalle->item = 1;
                        $proforma_detalle->cod_confirmacion = 'FEE CANGOROO';
                        $proforma_detalle->id_producto = $id_producto_fee;
                        $proforma_detalle->precio_costo = $costo;
                        $proforma_detalle->costo_proveedor = $costo; //PRECIO ORIGINAL PARA LC
                        $proforma_detalle->costo_gravado = 0;
                        $proforma_detalle->precio_venta = $venta;
                        $proforma_detalle->id_proveedor = $proveedor_cangoroo;
                        $proforma_detalle->id_prestador = $prestador_cangoroo;
                        $proforma_detalle->descripcion = 'FEE CANGOROO';
                        $proforma_detalle->currency_venta_id = $v_id_moneda_venta;
                        $proforma_detalle->currency_costo_id = $moneda_costo;
                        $proforma_detalle->markup = 0;
                        $proforma_detalle->id_empresa = $v_id_empresa;
                        $proforma_detalle->porcentaje_comision_agencia = 0;
                        $proforma_detalle->origen = 'A';
                        $proforma_detalle->cantidad = 1;
                        $proforma_detalle->cotizacion_costo = $v_cotizacion_actual;
                        $proforma_detalle->save();
                    }
    
                } else if ($cuanto_grupo_reserva_existe < $total_fee_creados){
                    $delete_fee = $total_fee_creados - $cuanto_grupo_reserva_existe;
    
                    for ($i=0; $i < $delete_fee; $i++) {
    
                        //Busca un fee para eliminar
                        $fee = ProformasDetalle::where('activo', true)
                        ->where('id_proforma',$id_proforma)
                        ->where('id_producto', $id_producto_fee)
                        ->first();   
    
                        $fee->activo = false;
                        $fee->id_usuario_anulacion = 1;
                        $fee->fecha_anulacion= date('Y-m-d H:m:i');
                        $fee->save();
                    }
                } else if ($cuanto_grupo_reserva_existe == 0){
                    $fee = ProformasDetalle::where('activo', true)
                    ->where('id_proforma',$id_proforma)
                    ->where('id_producto', $id_producto_fee)
                    ->delete();  
                }
              
            }

          
            $fee = ProformasDetalle::where('activo', true)
            ->where('id_proforma',$id_proforma)
            ->where('id_producto', $id_producto_fee)
            ->count(); 



            //Restar FEE de cada reserva segun cantidad de grupo de reserva
            if($fee > 0){
                //Por cada fee en el detalle de proforma , buscar las reservas de cangooro en el detalle de proforma y restar el fee
               

                $proformas_detalle = ProformasDetalle::where('activo', true)
                ->where('id_proforma',$id_proforma)
                ->whereIn('id_producto', $productos_cangoroo)
                ->get(['id_producto','id','precio_venta','precio_venta_anterior']);
                
                foreach ($proformas_detalle as $key => $detalle) {

                    if($key < $fee){
                        $detalle->precio_venta =  $detalle->precio_venta_anterior - $venta;
                        $detalle->save();
                    } else {
                        $detalle->precio_venta = $detalle->precio_venta_anterior;
                        $detalle->save();
                    }
                }
            } else {
                //Poner el precio original de los productos cangoroo
                $proformas_detalle = ProformasDetalle::where('activo', true)
                ->where('id_proforma',$id_proforma)
                ->whereIn('id_producto', $productos_cangoroo)
                ->get(['id_producto','id','precio_venta','precio_venta_anterior']);

                foreach ($proformas_detalle as $key => $detalle) {
                    $detalle->precio_venta = $detalle->precio_venta_anterior;
                    $detalle->save();
                }
            }
                
            
        }

    }


}