<?php 

namespace App\Traits;

use App\Persona;
use App\Ticket;
use App\Recibo;
use App\ReciboDetalle;
use App\Anticipo;
use App\AsientoDetalle;
use App\AplicarAnticipo;
use App\LibroVenta;
use App\FormaCobroReciboCabecera;
use App\FormaCobroReciboDetalle;
use App\BancoCabecera;

use Illuminate\Support\Facades\DB;
use Session;
use Log;


trait RecibosTrait {


     /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }//function


    private function getIdEmpresa(){
	
        if(!$this->id_empresa){
           $this->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
        }
       return $this->id_empresa;
      }
      

     public function procesar_recibo($id_recibo, $id_usuario){

        try {
       

            $recibo = Recibo::with('formaCobroCabecera','detalle')->find($id_recibo);

            $detalleCobro = DB::table('recibos')
                    ->select('b.id_cuenta_contable as cuenta_contable_banco', 
                            'b.id as id_banco_detalle',
                            'recibos.id_forma_cobro', 
                            'fpd.nro_comprobante', 
                            'fpd.fecha_emision', 
                            'recibos.id_sucursal', 
                            'recibos.id_centro_costo', 
                            'fpd.fecha_documento',
                            'recibos.id_cliente as id_cliente_recibo', 
                            'recibos.cotizacion as cotizacion_recibo', 
                            'recibos.id as id_recibo', 
                            'fpd.importe_pago', 
                            'fpd.id_moneda',
                            'fpd.id_cta_ctb as id_cuenta_contable', 
                            'recibos.concepto', 
                            's.id as id_sucursal_contable', 
                            'recibos.id_empresa', 
                            'recibos.importe',
                            'fpd.cotizacion_contable', 
                            'fpd.id_tipo_pago', 
                            DB::raw('(SELECT EXTRACT(MONTH FROM fpd.fecha_documento)) as mes_documento'),
                            DB::raw('(SELECT EXTRACT(MONTH FROM CURRENT_DATE)) as mes_actual')
                        )
                    ->join('forma_cobro_recibo_cabecera as fp', 'fp.id', '=', 'recibos.id_forma_cobro')
                    ->join('forma_cobro_recibo_detalle as fpd', 'fpd.id_cabecera', '=', 'fp.id')
                    ->leftJoin('banco_detalle as b', 'b.id', '=', 'fpd.id_banco_detalle')
                    ->join('sucursales_empresa as s', 's.id', '=', 'recibos.id_sucursal')
                    ->where('recibos.id', '=', $id_recibo)
                    ->where('fpd.activo', true)
                    ->where('fp.activo', true)
                    ->get();

            $c_recibo_detalle = DB::select('SELECT importe, id_libro_venta FROM recibos_detalle WHERE id_cabecera = ?',[$id_recibo]);
      
            //TODO: get_cotizacion_contable_actual aca faltaba id_empresa
            $c_retenciones =DB::table('recibos as r')
                                    ->join('forma_cobro_recibo_cabecera as fp', 'fp.id', '=', 'r.id_forma_cobro')
                                    ->join('forma_cobro_recibo_detalle as fpd', 'fpd.id_cabecera', '=', 'fp.id')
                                    ->join('sucursales_empresa as s', 's.id', '=', 'r.id_sucursal')
                                    ->selectRaw("
                                        SUM(
                                            (SELECT get_monto_cotizado_asiento(fpd.importe_pago, fpd.id_moneda,111,
                                            (CASE WHEN fpd.cotizacion_contable = 0 THEN 
                                               ((SELECT get_cotizacion_contable_actual(r.id_empresa, 'C')))
                                            ELSE 
                                               fpd.cotizacion_contable END)))
                                            ) AS total,
                                        SUM(fpd.importe_pago) AS importe_original,
                                        fpd.id_cta_ctb,
                                        fpd.cotizacion_contable,
                                        r.concepto,
                                        s.id AS id_sucursal_contable,
                                        r.id_centro_costo,
                                        fpd.id_moneda,
                                        r.id_empresa
                                    ")
                                    ->where('r.id', $id_recibo)
                                    ->where('fpd.activo', true)
                                    ->where('fpd.id_tipo_pago', 5)
                                    ->groupBy(DB::raw('EXTRACT(MONTH FROM fpd.fecha_documento)'), 'fpd.id_cta_ctb', 'fpd.cotizacion_contable', 'r.concepto', 's.id', 'r.id_centro_costo', 'fpd.id_moneda', 'r.id_empresa')
                                    ->get();
        
        
        



            if(count($detalleCobro) == 0){
                return 'No se pude procesar el recibo. No tiene formas de cobros activo';
            }

            if($recibo->id_estado != 40){
                return 'No se puede procesar el recibo. El mismo no está cobrado.';
            }


            $c_canjes_retenciones_a_cobrar = DB::table('recibos as r')
            ->join('forma_cobro_recibo_cabecera as fp', 'fp.id', '=', 'r.id_forma_cobro')
            ->join('forma_cobro_recibo_detalle as fpd', 'fpd.id_cabecera', '=', 'fp.id')
            ->join('sucursales_empresa as s', 's.id', '=', 'r.id_sucursal')
            ->selectRaw('
                SUM(fpd.importe_pago) - COALESCE(SUM(fpd.retencion), 0) AS canje,
                SUM(fpd.retencion) AS retencion_a_cobrar,
                fpd.id_cta_ctb,
                fpd.cotizacion_contable,
                r.concepto,
                s.id AS id_sucursal_contable,
                r.id_centro_costo,
                fpd.id_moneda,
                r.id_empresa
            ')
            ->where('r.id', $id_recibo)
            ->where('fpd.activo', true)
            ->where('fpd.id_tipo_pago', 6)
            ->groupBy('fpd.id_cta_ctb', 'fpd.cotizacion_contable', 'r.concepto', 's.id', 'r.id_centro_costo', 'fpd.id_moneda', 'r.id_empresa')
            ->get();

            $countQuery = DB::select("SELECT COUNT(*) AS count FROM recibos_detalle WHERE id_cabecera = ? AND id_retencion IS NOT NULL", [$id_recibo]);
            $v_cant_retencion = $countQuery[0]->count;


            $countQuery = DB::select("SELECT COUNT(*) AS count FROM recibos_detalle WHERE id_cabecera = ? AND id_anticipo IS NOT NULL", [$id_recibo]);
            $v_cant_anticipos = $countQuery[0]->count;

            $v_cant_items = count($recibo->detalle);


            $v_id_cabecera = 0;
            $v_id_asiento = 0;
            $contador = 0;
            $total_deudores = 0;
            $v_primera_moneda = 0;
            $v_diferencia_moneda = false;
            $v_nro_recibo = $recibo->nro_recibo;
            $v_cotizacion_promedio = DB::select("SELECT get_cotizacion(?,?,?)",[$recibo->id_moneda,$recibo->id_empresa,0])[0]->get_cotizacion;
            $v_id_asiento_retencion = 0;
            $v_total = 0;
            $v_total_deudores = 0;


            DB::beginTransaction();

            foreach ($detalleCobro as $key => $forma_cobro_detalle) {


                // TODO: Permitimos no validar la cuenta de banco porque los recibos ya creados no lo tienen, pero luego tenemos que validar
                //Validar que haya id_banco_detalle
                if($forma_cobro_detalle->id_banco_detalle){
                    // return "Error interno, no se puede registrar movimiento de cuenta corriente banco";

                          //SE REGISTRAN MOVIMIENTOS EN CTA CTE BANCOS
                          DB::select("SELECT registrar_movimiento_cta_cte_bancos(
                            ?, ?, ?, ?, ?, null, ?, ?, ?, ?, ?, ?, ?, ?)"
                        , [
                            $forma_cobro_detalle->concepto,
                            $forma_cobro_detalle->importe_pago,
                            $forma_cobro_detalle->id_banco_detalle,
                            $forma_cobro_detalle->nro_comprobante,
                            $forma_cobro_detalle->fecha_documento,
                            $forma_cobro_detalle->id_sucursal,
                            $forma_cobro_detalle->cuenta_contable_banco,
                            $forma_cobro_detalle->id_centro_costo,
                            $forma_cobro_detalle->id_cliente_recibo,
                            $forma_cobro_detalle->cotizacion_recibo,
                            $id_usuario,
                            23,
                            $id_recibo
                        ]);

                }


                /**
                 * SI NO ES 
                 * 6 - FACTURA CANJE
                 */

                //Porque hay una condicion de mes de documento y mes de cobro ?
                //  && $forma_cobro_detalle->mes_documento == $forma_cobro_detalle->mes_actual

                if (!in_array($forma_cobro_detalle->id_tipo_pago, [ 6]) ) {
                    
                    $v_id_cabecera = DB::select("SELECT insertar_asientos_cobranza(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) AS v_id_cabecera", [
                        $forma_cobro_detalle->id_cuenta_contable,
                        $forma_cobro_detalle->importe_pago,
                        'D',
                        $id_usuario,
                        4, // RECIBO origen_asiento_contable
                        $forma_cobro_detalle->id_moneda,
                        $forma_cobro_detalle->cotizacion_contable,
                        $v_id_cabecera,
                        $forma_cobro_detalle->concepto,
                        $forma_cobro_detalle->id_sucursal_contable,
                        $forma_cobro_detalle->id_centro_costo,
                        $forma_cobro_detalle->id_empresa,
                        $v_nro_recibo
                    ])[0]->v_id_cabecera;

                }
                
            }//foreach

            
            // SE REGISTRAN MOVIMIENTOS EN CTA CTE CLIENTE
            DB::select("SELECT registrar_movimiento_cta_cte(?, ?, ?, ?, 23, 'C', ?)"
                , [
                    $v_nro_recibo,
                    $recibo->formaCobroCabecera->importe_total,
                    $recibo->id_moneda,
                    $recibo->id_cliente,
                    $recibo->id
                ]);

                //ACTUALIZAR EL SALDO DEL LIBRO VENTA
                foreach ($c_recibo_detalle as $detalle) {
                    $lv = LibroVenta::where('id',$detalle->id_libro_venta)->first();
                    if($lv){

                        if(!$lv->activo){
                            return 'Una de las facturas esta anulada LV: '.$lv->id;
                        }
                        
                        $saldo_calculado = round($lv->saldo,2) - round($detalle->importe,2);
                        $lv->saldo =  $saldo_calculado;
                        $lv->save();
                    }
                }
 

                /**
                 * -----------------------------------------------------------------------------------------------------------------------
                 *                                      ASIENTO SEGUN RECIBO DETALLE
                 * -----------------------------------------------------------------------------------------------------------------------
                 */


                //SE GENERAN ASIENTOS CONTABLES PARA CANJES Y SUS RETENCIONES
                if (!is_null($c_canjes_retenciones_a_cobrar)) {
                    foreach ($c_canjes_retenciones_a_cobrar as $detalle) {
                        // if ($v_id_asiento_retencion != 0) {
                        //     $v_id_cabecera = $v_id_asiento_retencion;
                        // }

                        if ($detalle->retencion_a_cobrar > 0) {
                            $cuentaContable = $this->get_id_cuenta_contable('RETENCIONES_A_COBRAR', $recibo->id_empresa, null);
                            $v_id_cabecera = DB::select("SELECT insertar_asientos_cobranza(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) AS v_id_cabecera", [
                                $cuentaContable,
                                $detalle->retencion_a_cobrar,
                                'D',
                                $id_usuario,
                                4, // RECIBO origen_asiento_contable
                                111, //mondea
                                $detalle->cotizacion_contable,
                                $v_id_cabecera,
                                $detalle->concepto,
                                $detalle->id_sucursal_contable,
                                $detalle->id_centro_costo,
                                $detalle->id_empresa,
                                $v_nro_recibo
                            ])[0]->v_id_cabecera;
                        }

                        $montoCotizado = DB::select("SELECT get_monto_cotizado_asiento(?, ?, 111, ?, ?) AS monto_cotizado", [
                            $detalle->canje,
                            $detalle->id_moneda,
                            $detalle->cotizacion_contable,
                            $detalle->id_empresa
                        ]);
                        

                        $v_id_cabecera = DB::select("SELECT insertar_asientos_cobranza(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) AS v_id_cabecera", [
                            $detalle->id_cta_ctb,
                            $montoCotizado[0]->monto_cotizado,
                            'D',
                            $id_usuario,
                            4, // RECIBO origen_asiento_contable
                            111, //mondea
                            $detalle->cotizacion_contable,
                            $v_id_cabecera,
                            $detalle->concepto,
                            $detalle->id_sucursal_contable,
                            $detalle->id_centro_costo,
                            $detalle->id_empresa,
                            $v_nro_recibo
                        ])[0]->v_id_cabecera;
                    

                    
                    }
                }

        

                if ($v_cant_anticipos === 1 && $v_cant_items === 1) {

                    //--------------------------------------------------  DIFERENCIA DE CAMBIO   --------------------------------------------------------

                    //Vamos el valor y cotizacion del anticipo para calcular cuanto es el valor a cobrar real
                    $idAnticipo = ReciboDetalle::where('id_cabecera', $id_recibo)->value('id_anticipo');
                    $anticipo =  Anticipo::where('id', $idAnticipo)->first();
                    $total_real = $recibo->importe;

                    //Si no es PYG cotizamos
                    if($anticipo->id_moneda != 111)
                        $total_real = round($recibo->importe * $anticipo->cotizacion);

                    
                    //Precalcula ya con el importe del Anticipo cotizado para conocer la diferencia y generar el asiento antes de insertar el asiento de cierre
                    $this->generar_diferencia_cambio([
                        'v_cant_anticipos'          => $v_cant_anticipos, 
                        'v_cant_items'              => $v_cant_items, 
                        'v_cotizacion_promedio'     => $v_cotizacion_promedio, 
                        'recibo'                    => $recibo, 
                        'v_id_cabecera'             => $v_id_cabecera, 
                        'id_usuario'                => $id_usuario, 
                        'v_nro_recibo'              => $v_nro_recibo, 
                    ]);


                    //--------------------------------------------------  ANTICIPOS- ASIENTO CIERRE --------------------------------------------------------
                    $cuentaContableAnticipos = $this->get_id_cuenta_contable('ANTICIPOS', $recibo->id_empresa, $recibo->id_moneda);
                    $insertQuery = DB::select("SELECT insertar_asientos_cobranza(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) as v_id_cabecera", [
                        $cuentaContableAnticipos,
                        $recibo->importe,
                        'H',
                        $id_usuario,
                        4,
                        $recibo->id_moneda,
                        $v_cotizacion_promedio,
                        $v_id_cabecera,
                        $recibo->concepto,
                        $recibo->id_sucursal,
                        $recibo->id_centro_costo,
                        $recibo->id_empresa,
                        $v_nro_recibo
                    ]);
                    
                
                     // SE ACTUALIZAN LOS DATOS DEL ANTICIPO
                    Anticipo::where('id', $idAnticipo)
                        ->update([
                            'id_asiento' => $v_id_cabecera,
                            'id_estado' => 64
                        ]);
                
                } else {

                        //Generar el total de facturas con la cotizacion de cada documento
                        // $total_facturas = $recibo->importe;
                        $total_facturas = 0;
                        $cotizacion_calculada = 1;
                        foreach ($recibo->detalle as $detalle) {
                            $lv = LibroVenta::where('id',$detalle->id_libro_venta)->first();
                            if($lv){
                                //TODO: Hacer el ajuste para evitar poner cotizacion en una LV en guaranies, ahora hacemos un parche
                                if($lv->id_moneda_venta != 111){
                                    $total_facturas += round($detalle->importe * (int) $lv->cotizacion_documento);
                                } else {
                                    $total_facturas += round($detalle->importe);
                                }
                               
                                
                            }
                           
                        }

                        //Solo si no es PYG cotizamos
                        if($recibo->id_moneda != 111){
                            //Obtener la cotizacion del total de los documentos cotizados para la referencia del asiento
                            $cotizacion_calculada = round($total_facturas / $recibo->importe);
                        }
                      


                        //--------------------------------------------------  DIFERENCIA DE CAMBIO   --------------------------------------------------------
 
                        $this->generar_diferencia_cambio([
                            'v_cant_anticipos'          => $v_cant_anticipos, 
                            'v_cant_items'              => $v_cant_items, 
                            'v_cotizacion_promedio'     => $v_cotizacion_promedio, 
                            'recibo'                    => $recibo, 
                            'v_id_cabecera'             => $v_id_cabecera, 
                            'id_usuario'                => $id_usuario, 
                            'v_nro_recibo'              => $v_nro_recibo, 
                            'total_facturas'            => $total_facturas
                        ]);
                                  
                        //--------------------------------------------------  DEUDORES POR VENTA - ASIENTO CIERRE --------------------------------------------------------


                        // SE INSERTA EL ASIENTO DE DEUDORES POR VENTAS
                        $cuentaContableDeudoresVentas = $this->get_id_cuenta_contable('DEUDORES_VENTAS', $recibo->id_empresa, $recibo->id_moneda);
                        $insertQuery = DB::select("SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) as v_id_cabecera", [
                            $cuentaContableDeudoresVentas,
                            $recibo->importe,
                            (int)$total_facturas, //importe cotizado con el cambio del documento
                            'H',
                            $id_usuario,
                            4,
                            $recibo->id_moneda,
                            $cotizacion_calculada,
                            $v_id_cabecera,
                            $recibo->concepto,
                            $recibo->id_sucursal,
                            $recibo->id_centro_costo,
                            $recibo->id_empresa,
                            $v_nro_recibo,
                            date('Y-m-d H:i:00')
                        ]);
                        
                        // SE ACTUALIZAN LOS DATOS DEL ANTICIPO
                        $idAnticipos = ReciboDetalle::where('id_cabecera', $id_recibo)
                            ->whereNotNull('id_anticipo')
                            ->distinct('id_anticipo')
                            ->pluck('id_anticipo')
                            ->toArray();
                        
                        foreach ($idAnticipos as $idAnticipo) {
                            Anticipo::where('id', $idAnticipo)
                                ->update([
                                    'id_asiento' => $v_id_cabecera,
                                    'id_estado' => 64,
                                    'saldo' => DB::raw("saldo - (SELECT importe FROM recibos_detalle WHERE id_cabecera = $v_id_cabecera AND id_anticipo IS NOT NULL)")
                                ]);
                        }
                
                   
                }



            


               

             


            $result = DB::select("SELECT SUM(d.importe) AS total_detalle 
                                  FROM libros_ventas l 
                                  JOIN recibos_detalle d ON d.id_libro_venta = l.id 
                                  WHERE d.id_cabecera = ?", [$id_recibo]);

            $v_total_detalle = $result[0]->total_detalle;

            DB::update("UPDATE recibos 
                        SET id_estado = 56, 
                        id_usuario_aplicado = ?, 
                        fecha_hora_aplicado = NOW()::timestamp(0), 
                        total_recibo_detalle = ?, id_asiento = ? 
                        WHERE id = ?", [$id_usuario, $v_total_detalle, $v_id_cabecera, $id_recibo]);

            DB::update("UPDATE forma_cobro_recibo_cabecera 
                        SET cotizacion = (SELECT get_cotizacion_contable_actual(?, 'V')) 
                        WHERE id_recibo = ?", [$recibo->id_empresa, $id_recibo]);

            DB::update("UPDATE libros_ventas 
                        SET pendiente = false 
                        WHERE id IN (
                                      SELECT id_libro_venta FROM recibos_detalle WHERE id_cabecera = ?
                                      )", [$id_recibo]);

                    //Pruebas
                    // DB::rollback();
                    // dd($v_total ,
                    // $v_id_asiento,
                    // $v_id_cabecera );
            DB::commit();
            return 'OK';


              
        } catch (\Exception $e) {
            DB::rollback();
            Log::critical($e);
            return $e->getMessage();
           
        }

    }//function


    private function get_id_cuenta_contable($cuenta, $id_empresa, $moneda = null){
        $cuentaContable = DB::select("SELECT get_id_cuenta_contable(?, ?, ?) AS cuenta_contable", [
            $cuenta,
            $id_empresa,
            $moneda
        ]);

        if(count($cuentaContable)){
            return $cuentaContable[0]->cuenta_contable;
        }

        return null;
    }


    private function generar_diferencia_cambio($data){

        Log::info(json_encode($data));

        $v_cant_anticipos = $data['v_cant_anticipos'];
        $v_cant_items = $data['v_cant_items'];
        $v_cotizacion_promedio = $data['v_cotizacion_promedio'];
        $recibo = $data['recibo'];
        $v_id_cabecera = $data['v_id_cabecera'];
        $id_usuario = $data['id_usuario'];
        $v_nro_recibo = $data['v_nro_recibo'];
        $total_deudores = 0;
        if(isset($data['total_facturas'])){
            $total_deudores = $data['total_facturas']; //ya cotizado
        } else {
                //Vamos a precotizar ya antes para obtener el valor de deudores por venta para poder insertar la diferencia
                $total_deudores = DB::select("SELECT get_monto_cotizado_asiento(?, ?, ?, ?, ?)",
                [
                    $recibo->importe,
                    $recibo->id_moneda, 
                    111, 
                    $v_cotizacion_promedio, 
                    $recibo->id_empresa 
                 ]
                 )[0]->get_monto_cotizado_asiento;
        }
    

 
            
           
            $result = DB::select("SELECT 
                                    SUM(debe) AS total_debe, 
                                    SUM(haber) AS total_haber 
                                    FROM asientos_contables_detalle 
                                    WHERE id_asiento_contable = ?", [$v_id_cabecera]);

            $v_total_debe = $result[0]->total_debe;
            $v_total_haber = $result[0]->total_haber + $total_deudores;
            $v_diferencia = $v_total_debe - $v_total_haber; //diferencia

        

            DB::select("SELECT generar_diferencia_cambio(?, ?, ?, ?, ?, ?, ?, ?, ?, false, 4, ?)", [
                $v_id_cabecera,
                $id_usuario,
                $recibo->concepto,
                $recibo->id_sucursal,
                $recibo->id_centro_costo,
                $recibo->id_empresa,
                $v_diferencia,
                1, //La diferencia es en la moneda de GS no necesita cotizacion //Patty
                111,
                $v_nro_recibo
            ]);
            
    }

    public function aplicar_recibo_anticipo($p_id_recibo, $p_id_anticipo, $p_monto_anticipo){

        // VALIDAR QUE EL MONTO NETO (TOTAL PAGO) DEL RECIBO SEA CERO PARA EJECUTAR
        $recibo = Recibo::findOrFail($p_id_recibo);
        $v_monto_neto_recibo = $recibo->total_pago;
        $v_nro_recibo = $recibo->nro_recibo;
        $v_id_empresa = $recibo->id_empresa;
        $v_id_usuario = $recibo->id_usuario_creacion;

        $anticipo = Anticipo::findOrFail($p_id_anticipo);
        $v_cotizacion_operativa = $anticipo->cotizacion;
        $v_id_moneda = $anticipo->id_moneda;

        // INSERTAR CABECERA FORMA COBRO
        $formaCobroReciboCabecera = new FormaCobroReciboCabecera();
        $formaCobroReciboCabecera->id_usuario = $v_id_usuario;
        $formaCobroReciboCabecera->importe_total = $v_monto_neto_recibo;
        $formaCobroReciboCabecera->fecha_hora = now()->format('Y-m-d H:i:s');
        $formaCobroReciboCabecera->id_recibo = $p_id_recibo;
        $formaCobroReciboCabecera->id_empresa = $v_id_empresa;
        $formaCobroReciboCabecera->activo = true;
        $formaCobroReciboCabecera->save();
        $v_id_forma_cobro_cabecera = $formaCobroReciboCabecera->id;

        $v_cotizacion_contable = DB::select("SELECT get_cotizacion_contable_actual(?,?,?)",[$recibo->id_empresa, $recibo->id_moneda, 'C'])[0]->get_cotizacion_contable_actual;
        $v_cuenta_cta_ctb = null;
        $v_cuenta_banco_detalle = null;

        //Buscar si existe la deominacion de ANTICIPO como fondo padre
		$banco = BancoCabecera::where('nombre','ANTICIPO')->where('id_empresa',$v_id_empresa)->first();
		if($banco){
		  $banco_detalle =  $banco->banco_detalle()->where('id_moneda', $v_id_moneda)->first();
		  if($banco_detalle){
			$v_cuenta_banco_detalle = $banco_detalle->id; 

			if($banco_detalle->id_cuenta_contable){
				$v_cuenta_cta_ctb = $banco_detalle->id_cuenta_contable;
			} else {
				return 'No se encuentra cuenta contable de la cuenta de ANTICIPO en la moneda seleccionada';
			}
			
		  } else {
            return 'No se encuentra la moneda seleccionada en la cuenta banco ANTICIPO';
		  }
		
		} else {
            return 'No se encuentra registrado la cuenta banco ANTICIPO';
		}

        
        $formaCobroReciboDetalle = new FormaCobroReciboDetalle();
        $formaCobroReciboDetalle->id_cabecera = $v_id_forma_cobro_cabecera;
        $formaCobroReciboDetalle->importe_pago = $p_monto_anticipo;
        $formaCobroReciboDetalle->nro_comprobante = $v_nro_recibo;
        $formaCobroReciboDetalle->cotizacion_operativa = $v_cotizacion_operativa;
        $formaCobroReciboDetalle->cotizacion_contable = $v_cotizacion_contable;
        $formaCobroReciboDetalle->id_tipo_pago = 9;
        $formaCobroReciboDetalle->id_moneda = $v_id_moneda;
        $formaCobroReciboDetalle->id_cta_ctb = $v_cuenta_cta_ctb;
        $formaCobroReciboDetalle->id_banco_detalle = $v_cuenta_banco_detalle;
        $formaCobroReciboDetalle->id_usuario_insert = $v_id_usuario;
        $formaCobroReciboDetalle->fecha_hora_insert = now()->format('Y-m-d H:i:s');
        $formaCobroReciboDetalle->activo = true;
        $formaCobroReciboDetalle->save();
        
        // PONER COBRADO POR EL USUARIO RECIBO (DE FORMA MANUAL)
        DB::select("SELECT cobrar_recibo(?,?)",[$p_id_recibo, $v_id_usuario]);

        // ASIGNAR AL RECIBO SU FORMA DE COBRO
        $recibo = Recibo::findOrFail($p_id_recibo);
        $recibo->id_forma_cobro = $v_id_forma_cobro_cabecera;
        $recibo->save();
        
        // PONER APLICADO POR EL USUARIO RECIBO (SE ENCARGA LA FUNCIÓN APLICAR RECIBO)
        $v_result_procesar_recibo = $this->procesar_recibo($p_id_recibo, $v_id_usuario);
        
        // DESCONTAR EL MONTO UTILIZADO DEL SALDO ANTICIPO
        $anticipo = Anticipo::findOrFail($p_id_anticipo);
        $anticipo->saldo = $anticipo->saldo - $p_monto_anticipo;
        $anticipo->id_estado = 64; // COBRADO
        $anticipo->pendiente = false;
        $anticipo->save();
        
        if ($v_result_procesar_recibo != 'OK') {
            return $v_result_procesar_recibo;
        }
        
        return 'OK';


    }

    //TODO: Este desarrollo queda pausado para ser utilizado de forma posterior a pedido de la Señora Sylvia 08/08/2023
    public function get_documento_recibo($p_id_empresa, $p_id_usuario){
        set_time_limit(0.0001); //maximo 2 segundos de espera
        $id_sucursal_empresa = Persona::select('id_sucursal_empresa')->find($p_id_usuario)->id_sucursal_empresa;


        $numeracion_recibo =  DB::table('numeracion_recibo')
                                ->where('id_sucursal_empresa',$id_sucursal_empresa)
                                ->where('id_empresa',$p_id_empresa)
                                ->lockForUpdate()
                                ->first();

        $numeracion_recibo->numeracion = $numeracion_recibo->numeracion + 1;
        
        $numeracion_recibo_update =  DB::table('numeracion_recibo')
                            ->where('id_sucursal_empresa',$id_sucursal_empresa)
                            ->where('id_empresa',$p_id_empresa)
                            ->update([ 'numeracion' =>  $numeracion_recibo->numeracion]);


        return str_pad($numeracion_recibo->establecimiento, 3, '0', STR_PAD_LEFT). '-' . str_pad($numeracion_recibo->expedicion, 3, '0', STR_PAD_LEFT). '-' . str_pad($numeracion_recibo->numeracion, 7, '0', STR_PAD_LEFT); 

    }


    public function aplica_anticipo_factura($v_id_factura,$v_monto_anticipo,$v_id_anticipo,$v_id_usuario, $id_aplicacion_regenerar = null) {
     
            DB::beginTransaction();

            //Validar que existen esos documentos
           
            $anticipo =Anticipo::where('id', $v_id_anticipo)->firstOrFail();
            $v_saldo_actual_factura = 0;
            $v_id_moneda_costo = 0;
            $id_empresa = $anticipo->id_empresa;
            $v_id_cabecera = 0;

            

            /**
             * Se puede aplicar anticipo de una moneda a facturas de cualquier moneda
             * Ahora aplicamos anticipo solamente desde cobranza con esta funcion
             */



            $libro_venta = LibroVenta::where('id_documento', $v_id_factura)->firstOrFail();
            $v_saldo_actual_factura = $libro_venta->saldo;
            $v_id_moneda_costo      = $libro_venta->id_moneda_venta;


            

 
            $v_saldo_actual_anticipo = (float)$anticipo->saldo;
            $v_id_moneda_venta = $anticipo->id_moneda;
            $v_cotizacion = $anticipo->cotizacion;

            //No se valida saldo si es que se regenera asiento
            if ($v_monto_anticipo > round($v_saldo_actual_anticipo, 2) &&  !$id_aplicacion_regenerar) {
                return 'ERROR, El monto a aplicar es mayor al saldo actual del anticipo';
            }
            

            /**
             * Aqui cotizamos el monto a aplicar,  pero cotizando segun la moneda del libro venta
             * POrque puedo tener un Antiicipo en GS y aplicar contra una USD
             */

             //Usamos la cotizacion del dia para aplicar el monto a la factura, entonces depende la moneda de libro venta
            $cotizacion_del_dia_libro_venta = DB::select("SELECT get_cotizacion(?,?,?)",[$libro_venta->id_moneda_venta,$id_empresa,0])[0]->get_cotizacion;

            if(!$cotizacion_del_dia_libro_venta){
                return 'ERROR, No hay cotización cargada para moneda de factura';
            }

            $parametro_cotizacion = [
                $cotizacion_del_dia_libro_venta,
                $v_monto_anticipo,
                $anticipo->id_moneda,
                $libro_venta->id_moneda_venta
            ];
            $v_monto_cotizado = DB::select("SELECT get_monto_cotizacion_custom(?,?,?,?)",$parametro_cotizacion)[0]->get_monto_cotizacion_custom;

            $v_saldo_nuevo_factura = $v_saldo_actual_factura - $v_monto_cotizado; //Restamos saldo de factura LV
            $v_saldo_nuevo_anticipo = $v_saldo_actual_anticipo - $v_monto_anticipo; //Restamos saldo de Anticipo 

            $v_id_estado_cobro_factura = ($v_saldo_nuevo_factura == 0) ? 40 : 31;
            $v_id_estado_anticipo = ($v_saldo_nuevo_anticipo == 0) ? 78 : 64;


            $fecha_operacion = date('Y-m-d H:i:00');

            //Si vamos a regenerar no vamos a insertar y tampoco actualizar los saldos
            if(!$id_aplicacion_regenerar){

                // Inserta el registro de aplicacion
                $aplicacion = new AplicarAnticipo;
                $aplicacion->id_anticipo = $v_id_anticipo;
                $aplicacion->id_factura = $v_id_factura;
                $aplicacion->monto = $v_monto_anticipo;
                $aplicacion->id_usuario = $v_id_usuario;
                $aplicacion->fecha_hora = $fecha_operacion;
                $aplicacion->anulado = false;
                $aplicacion->monto_cotizado = $v_monto_cotizado;
                $aplicacion->cotizacion = $cotizacion_del_dia_libro_venta;
                $aplicacion->id_moneda =$v_id_moneda_costo;
                $aplicacion->tipo = 'C';
                

                // Actualiza los saldos
                $libro_venta->saldo =  $libro_venta->saldo - $v_monto_cotizado;
                $libro_venta->id_estado_cobro =  $v_id_estado_cobro_factura;
                $libro_venta->save();
            

                DB::table('anticipos')
                    ->where('id', $v_id_anticipo)
                    ->update([
                        'saldo' => DB::raw('saldo - ' . $v_monto_anticipo),
                        'id_estado' => $v_id_estado_anticipo,
                    ]);
            } else {
                //Vamos a usar la fecha de aplicacion para el el nuevo asiento contable
               $aplicacion =  AplicarAnticipo::where('id',$id_aplicacion_regenerar)->first();
               $fecha_operacion = $aplicacion->fecha_hora;

                 //Obtener la cotizacion de la fecha de aplicacion, podria no ser muy preciso este metodo
               $cotizacion_del_dia_libro_venta = 0;

               if($libro_venta->id_moneda_venta === 111){
                    $cotizacion_del_dia_libro_venta = 1;
                } else {
                    $cotizacion = DB::table('currency')
                            ->join('cotizaciones', 'cotizaciones.id_currency', '=', 'currency.currency_id')
                            ->where('currency.currency_id', '=', 143)
                            ->where('cotizaciones.id_empresa', '=', $id_empresa)
                            ->whereDate('cotizaciones.fecha', '=', $fecha_operacion) // Esto se ajusta a la fecha actual, puedes reemplazarlo por la fecha deseada
                            ->orderBy('cotizaciones.id', 'desc')
                            ->limit(1)
                            ->select('cotizaciones.cotizacion_contable_compra', 'cotizaciones.fecha')
                            ->first();

                    if($cotizacion){
                        $cotizacion_del_dia_libro_venta = $cotizacion->cotizacion_contable_compra;
                    } else {
                        Log::error('REGENERAR ASIENTO APLICACACION: No se encontro cotizacion del dia');
                        return 'ERROR';
                    }
                }
              

            }
           

                
                /**
                 * ASIENTO DE APLICACION DE ANTICIPO CLIENTE
                 * El monto a aplicar sera cotizado tanto en la moneda del anticipo como la moneda de la factura, pero en el asiento 
                 * de deudores sera la cotizacion del dia
                 */

                 /**
                 *      ------------------------------- ASIENTO DEBE ---------------------------------
                 * */ 

                 //Cotizamos porque la funcion insertar_asientos_custom no lo hace
                 //Se cotiza con el cotizacion del anticipo

                $parametro_cotizacion = [
                    $anticipo->cotizacion,
                    $v_monto_anticipo,
                    $anticipo->id_moneda,
                    111
                ];
                $total_anticipo_cotizado_gs = DB::select("SELECT get_monto_cotizacion_custom(?,?,?,?)",$parametro_cotizacion)[0]->get_monto_cotizacion_custom;
               

                $cuentaContableAnticipos = $this->get_id_cuenta_contable('ANTICIPOS', $id_empresa, $v_id_moneda_venta);
                if(!$cuentaContableAnticipos){
                    return 'ERROR, parametro ANTICIPOS no esta configurado';
                }

                $v_id_cabecera = DB::select("SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) as v_id_cabecera", [
                    $cuentaContableAnticipos,
                    $v_monto_anticipo, //Monto original sin cotizar
                    (int)$total_anticipo_cotizado_gs, //Importe cotizado para asiento
                    'D', //Debe
                    $v_id_usuario,
                    4, //RECIBO origen_asiento_contable
                    $anticipo->id_moneda,
                    $anticipo->cotizacion,
                    $v_id_cabecera,
                    $anticipo->concepto,
                    $anticipo->id_sucursal,
                    $anticipo->id_centro_costo,
                    $id_empresa,
                    $anticipo->id,
                    $fecha_operacion //Fecha del asiento
                ])[0]->v_id_cabecera;

                /**
                 *      ------------------------------- ASIENTO HABER ---------------------------------
                 * */ 

                
                 $parametro_cotizacion = [
                    $cotizacion_del_dia_libro_venta,
                    $v_monto_cotizado,
                    $libro_venta->id_moneda_venta,
                    111
                ];
                $total_factura_cotizado_gs = DB::select("SELECT get_monto_cotizacion_custom(?,?,?,?)",$parametro_cotizacion)[0]->get_monto_cotizacion_custom;

                $cuentaContableDeudoresVentas = $this->get_id_cuenta_contable('DEUDORES_VENTAS', $id_empresa, $libro_venta->id_moneda_venta);

                if(!$cuentaContableDeudoresVentas){
                    return 'ERROR, parametro DEUDORES_VENTAS no esta configurado';
                }

                $v_id_cabecera = DB::select("SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) as v_id_cabecera", [
                    $cuentaContableDeudoresVentas,
                    $v_monto_cotizado, //Monto original sin cotizar
                    (int)$total_factura_cotizado_gs, //Importe cotizado para asiento
                    'H', //Haber
                    $v_id_usuario,
                    4, //RECIBO origen_asiento_contable
                    $libro_venta->id_moneda_venta,
                    $cotizacion_del_dia_libro_venta,
                    $v_id_cabecera,
                    $anticipo->concepto,
                    $anticipo->id_sucursal,
                    $anticipo->id_centro_costo,
                    $id_empresa,
                    $anticipo->id,
                    $fecha_operacion //Fecha del asiento
                ])[0]->v_id_cabecera;


                /**
                 *      ------------------------------- CALCULAR DIFERENCIA DE CAMBIO ---------------------------------
                 * */    
                $result = DB::select("SELECT 
                SUM(debe) AS total_debe, 
                SUM(haber) AS total_haber 
                FROM asientos_contables_detalle 
                WHERE id_asiento_contable = ?", [$v_id_cabecera]);

                $v_total_debe = $result[0]->total_debe;
                $v_total_haber = $result[0]->total_haber;
                $v_diferencia = $v_total_debe - $v_total_haber; //diferencia

                    // dd($total_factura_cotizado_gs,$total_anticipo_cotizado_gs );


                DB::select("SELECT generar_diferencia_cambio(?, ?, ?, ?, ?, ?, ?, ?, ?, false, 4, ?)", [
                    $v_id_cabecera,
                    $v_id_usuario,
                    $anticipo->concepto,
                    $anticipo->id_sucursal,
                    $anticipo->id_centro_costo,
                    $anticipo->id_empresa,
                    $v_diferencia,
                    1, //La diferencia es en la moneda de GS no necesita cotizacion //Patty
                    111,
                    $anticipo->id
                ]);
                   
                //DD para ver asientos generados
                // dd(\App\AsientoDetalle::where('id_asiento_contable', $v_id_cabecera)->get());

                //Agregar el id asiento a la aplicacion
                $aplicacion->id_asiento = $v_id_cabecera;
                $aplicacion->save();

                Log::debug('SE GENERO EL ASIENTO :'.$v_id_cabecera);

            DB::commit();

            return 'OK';
      
    }


    public function regenerarAsientoAplicacion($id_empresa){
        
        Log::info('INICIO PROCESO DE REGENERAR ASIENTO DE APLICACION');

        $aplicaciones = AplicarAnticipo::with('factura')
                        ->whereHas('factura',function($q) use ($id_empresa){
                            $q->where('id_empresa',$id_empresa);
                        })
                        ->where('anulado',false)
                        ->where('fecha_hora','>=','2023-01-01 00:00:00')
                        ->get();

        foreach ($aplicaciones as  $aplicacion) {
            Log::info('PROCESO AP : '.$aplicacion->id);
            $this->aplica_anticipo_factura($aplicacion->id_factura,
                                           $aplicacion->monto,
                                           $aplicacion->id_anticipo,
                                           $aplicacion->id_usuario, 
                                           $aplicacion->id);


        }

        Log::info('Finalizar proceso de regenerar asiento de aplicacion');
    }


    public function regenerar_procesar_recibo($id_recibo){

        try {
       

            $recibo = Recibo::with('formaCobroCabecera','detalle')->find($id_recibo);
            $id_usuario = $recibo->id_usuario_aplicado;

            $detalleCobro = DB::table('recibos')
                    ->select('b.id_cuenta_contable as cuenta_contable_banco', 
                            'b.id as id_banco_detalle',
                            'recibos.id_forma_cobro', 
                            'fpd.nro_comprobante', 
                            'fpd.fecha_emision', 
                            'recibos.id_sucursal', 
                            'recibos.id_centro_costo', 
                            'fpd.fecha_documento',
                            'recibos.id_cliente as id_cliente_recibo', 
                            'recibos.cotizacion as cotizacion_recibo', 
                            'recibos.id as id_recibo', 
                            'fpd.importe_pago', 
                            'fpd.id_moneda',
                            'fpd.id_cta_ctb as id_cuenta_contable', 
                            'recibos.concepto', 
                            's.id as id_sucursal_contable', 
                            'recibos.id_empresa', 
                            'recibos.importe',
                            'fpd.cotizacion_contable', 
                            'fpd.id_tipo_pago', 
                            DB::raw('(SELECT EXTRACT(MONTH FROM fpd.fecha_documento)) as mes_documento'),
                            DB::raw('(SELECT EXTRACT(MONTH FROM CURRENT_DATE)) as mes_actual')
                        )
                    ->join('forma_cobro_recibo_cabecera as fp', 'fp.id', '=', 'recibos.id_forma_cobro')
                    ->join('forma_cobro_recibo_detalle as fpd', 'fpd.id_cabecera', '=', 'fp.id')
                    ->leftJoin('banco_detalle as b', 'b.id', '=', 'fpd.id_banco_detalle')
                    ->join('sucursales_empresa as s', 's.id', '=', 'recibos.id_sucursal')
                    ->where('recibos.id', '=', $id_recibo)
                    ->where('fpd.activo', true)
                    ->where('fp.activo', true)
                    ->get();

            $c_recibo_detalle = DB::select('SELECT importe, id_libro_venta FROM recibos_detalle WHERE id_cabecera = ?',[$id_recibo]);
      
            //TODO: get_cotizacion_contable_actual aca faltaba id_empresa
            $c_retenciones =DB::table('recibos as r')
                                    ->join('forma_cobro_recibo_cabecera as fp', 'fp.id', '=', 'r.id_forma_cobro')
                                    ->join('forma_cobro_recibo_detalle as fpd', 'fpd.id_cabecera', '=', 'fp.id')
                                    ->join('sucursales_empresa as s', 's.id', '=', 'r.id_sucursal')
                                    ->selectRaw("
                                        SUM(
                                            (SELECT get_monto_cotizado_asiento(fpd.importe_pago, fpd.id_moneda,111,
                                            (CASE WHEN fpd.cotizacion_contable = 0 THEN 
                                               ((SELECT get_cotizacion_contable_actual(r.id_empresa, 'C')))
                                            ELSE 
                                               fpd.cotizacion_contable END)))
                                            ) AS total,
                                        SUM(fpd.importe_pago) AS importe_original,
                                        fpd.id_cta_ctb,
                                        fpd.cotizacion_contable,
                                        r.concepto,
                                        s.id AS id_sucursal_contable,
                                        r.id_centro_costo,
                                        fpd.id_moneda,
                                        r.id_empresa
                                    ")
                                    ->where('r.id', $id_recibo)
                                    ->where('fpd.activo', true)
                                    ->where('fpd.id_tipo_pago', 5)
                                    ->groupBy(DB::raw('EXTRACT(MONTH FROM fpd.fecha_documento)'), 'fpd.id_cta_ctb', 'fpd.cotizacion_contable', 'r.concepto', 's.id', 'r.id_centro_costo', 'fpd.id_moneda', 'r.id_empresa')
                                    ->get();
        
        
        



            if(count($detalleCobro) == 0){
                return 'No se pude procesar el recibo. No tiene formas de cobros activo';
            }

            if(!$recibo->id_estado == 56){
                return 'No se puede procesar el recibo no esta aplicado.';
            }


            $c_canjes_retenciones_a_cobrar = DB::table('recibos as r')
            ->join('forma_cobro_recibo_cabecera as fp', 'fp.id', '=', 'r.id_forma_cobro')
            ->join('forma_cobro_recibo_detalle as fpd', 'fpd.id_cabecera', '=', 'fp.id')
            ->join('sucursales_empresa as s', 's.id', '=', 'r.id_sucursal')
            ->selectRaw('
                SUM(fpd.importe_pago) - COALESCE(SUM(fpd.retencion), 0) AS canje,
                SUM(fpd.retencion) AS retencion_a_cobrar,
                fpd.id_cta_ctb,
                fpd.cotizacion_contable,
                r.concepto,
                s.id AS id_sucursal_contable,
                r.id_centro_costo,
                fpd.id_moneda,
                r.id_empresa
            ')
            ->where('r.id', $id_recibo)
            ->where('fpd.activo', true)
            ->where('fpd.id_tipo_pago', 6)
            ->groupBy('fpd.id_cta_ctb', 'fpd.cotizacion_contable', 'r.concepto', 's.id', 'r.id_centro_costo', 'fpd.id_moneda', 'r.id_empresa')
            ->get();

            $countQuery = DB::select("SELECT COUNT(*) AS count FROM recibos_detalle WHERE id_cabecera = ? AND id_retencion IS NOT NULL", [$id_recibo]);
            $v_cant_retencion = $countQuery[0]->count;


            $countQuery = DB::select("SELECT COUNT(*) AS count FROM recibos_detalle WHERE id_cabecera = ? AND id_anticipo IS NOT NULL", [$id_recibo]);
            $v_cant_anticipos = $countQuery[0]->count;

            $v_cant_items = count($recibo->detalle);


            $v_id_cabecera = 0;
            $v_id_asiento = 0;
            $contador = 0;
            $total_deudores = 0;
            $v_primera_moneda = 0;
            $v_diferencia_moneda = false;
            $v_nro_recibo = $recibo->nro_recibo;
            $v_id_asiento_retencion = 0;
            $v_total = 0;
            $v_total_deudores = 0;

            $v_cotizacion_promedio = 0;
            $fecha_aplicacion = date("Y-m-d", strtotime($recibo->fecha_hora_aplicado));

            if($recibo->id_moneda != 111){
                $cotizacion = DB::table('currency')
                ->join('cotizaciones', 'cotizaciones.id_currency', '=', 'currency.currency_id')
                ->where('currency.currency_id', '=', $recibo->id_moneda)
                ->where('cotizaciones.id_empresa', '=', $recibo->id_empresa)
                ->whereDate('cotizaciones.fecha', '=', $fecha_aplicacion) // Esto se ajusta a la fecha actual, puedes reemplazarlo por la fecha deseada
                ->orderBy('cotizaciones.id', 'desc')
                ->limit(1)
                ->select('cotizaciones.cotizacion_contable_compra', 'cotizaciones.fecha')
                ->first();
    
    
    
                if($cotizacion){
                    $v_cotizacion_promedio = $cotizacion->cotizacion_contable_compra;
                } else {
                    return "Error al obtener cotizacion para el asiento regenerar recibo ";
                    Log::error('REGENERAR ASIENTO APLICACACION: No se encontro cotizacion del dia');
                    return 'ERROR';
                }
    
            } else {
                $v_cotizacion_promedio = 1;
            }
          
          

            DB::beginTransaction();

            foreach ($detalleCobro as $key => $forma_cobro_detalle) {


                /**
                 * SI NO ES 
                 * 6 - FACTURA CANJE
                 */

                //Porque hay una condicion de mes de documento y mes de cobro ?
                //  && $forma_cobro_detalle->mes_documento == $forma_cobro_detalle->mes_actual

                if (!in_array($forma_cobro_detalle->id_tipo_pago, [ 6]) ) {
                    
                    $v_id_cabecera = DB::select("SELECT insertar_asientos_cobranza(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) AS v_id_cabecera", [
                        $forma_cobro_detalle->id_cuenta_contable,
                        $forma_cobro_detalle->importe_pago,
                        'D',
                        $id_usuario,
                        4, // RECIBO origen_asiento_contable
                        $forma_cobro_detalle->id_moneda,
                        $forma_cobro_detalle->cotizacion_contable,
                        $v_id_cabecera,
                        $forma_cobro_detalle->concepto,
                        $forma_cobro_detalle->id_sucursal_contable,
                        $forma_cobro_detalle->id_centro_costo,
                        $forma_cobro_detalle->id_empresa,
                        $v_nro_recibo
                    ])[0]->v_id_cabecera;

                }
                
            }//foreach

    

                //ACTUALIZAR EL SALDO DEL LIBRO VENTA
                foreach ($c_recibo_detalle as $detalle) {
                    $lv = LibroVenta::where('id',$detalle->id_libro_venta)->first();
                    if($lv){

                        if(!$lv->activo){
                            return 'Una de las facturas esta anulada LV: '.$lv->id;
                        }
                        
                        $saldo_calculado = round($lv->saldo,2) - round($detalle->importe,2);
                    }
                }
 

                /**
                 * -----------------------------------------------------------------------------------------------------------------------
                 *                                      ASIENTO SEGUN RECIBO DETALLE
                 * -----------------------------------------------------------------------------------------------------------------------
                 */


                //SE GENERAN ASIENTOS CONTABLES PARA CANJES Y SUS RETENCIONES
                if (!is_null($c_canjes_retenciones_a_cobrar)) {
                    foreach ($c_canjes_retenciones_a_cobrar as $detalle) {
                        // if ($v_id_asiento_retencion != 0) {
                        //     $v_id_cabecera = $v_id_asiento_retencion;
                        // }

                        if ($detalle->retencion_a_cobrar > 0) {
                            $cuentaContable = $this->get_id_cuenta_contable('RETENCIONES_A_COBRAR', $recibo->id_empresa, null);
                            $v_id_cabecera = DB::select("SELECT insertar_asientos_cobranza(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) AS v_id_cabecera", [
                                $cuentaContable,
                                $detalle->retencion_a_cobrar,
                                'D',
                                $id_usuario,
                                4, // RECIBO origen_asiento_contable
                                111, //mondea
                                $detalle->cotizacion_contable,
                                $v_id_cabecera,
                                $detalle->concepto,
                                $detalle->id_sucursal_contable,
                                $detalle->id_centro_costo,
                                $detalle->id_empresa,
                                $v_nro_recibo
                            ])[0]->v_id_cabecera;
                        }

                        $montoCotizado = DB::select("SELECT get_monto_cotizado_asiento(?, ?, 111, ?, ?) AS monto_cotizado", [
                            $detalle->canje,
                            $detalle->id_moneda,
                            $detalle->cotizacion_contable,
                            $detalle->id_empresa
                        ]);
                        

                        $v_id_cabecera = DB::select("SELECT insertar_asientos_cobranza(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) AS v_id_cabecera", [
                            $detalle->id_cta_ctb,
                            $montoCotizado[0]->monto_cotizado,
                            'D',
                            $id_usuario,
                            4, // RECIBO origen_asiento_contable
                            111, //mondea
                            $detalle->cotizacion_contable,
                            $v_id_cabecera,
                            $detalle->concepto,
                            $detalle->id_sucursal_contable,
                            $detalle->id_centro_costo,
                            $detalle->id_empresa,
                            $v_nro_recibo
                        ])[0]->v_id_cabecera;
                    

                    
                    }
                }

        

                if ($v_cant_anticipos === 1 && $v_cant_items === 1) {

                    //--------------------------------------------------  DIFERENCIA DE CAMBIO   --------------------------------------------------------

                    //Vamos el valor y cotizacion del anticipo para calcular cuanto es el valor a cobrar real
                    $idAnticipo = ReciboDetalle::where('id_cabecera', $id_recibo)->value('id_anticipo');
                    $anticipo =  Anticipo::where('id', $idAnticipo)->first();
                    $total_real = $recibo->importe;

                    //Si no es PYG cotizamos
                    if($anticipo->id_moneda != 111)
                        $total_real = round($recibo->importe * $anticipo->cotizacion);

                    
                    //Precalcula ya con el importe del Anticipo cotizado para conocer la diferencia y generar el asiento antes de insertar el asiento de cierre
                    $this->generar_diferencia_cambio([
                        'v_cant_anticipos'          => $v_cant_anticipos, 
                        'v_cant_items'              => $v_cant_items, 
                        'v_cotizacion_promedio'     => $v_cotizacion_promedio, 
                        'recibo'                    => $recibo, 
                        'v_id_cabecera'             => $v_id_cabecera, 
                        'id_usuario'                => $id_usuario, 
                        'v_nro_recibo'              => $v_nro_recibo, 
                    ]);


                    //--------------------------------------------------  ANTICIPOS- ASIENTO CIERRE --------------------------------------------------------
                    $cuentaContableAnticipos = $this->get_id_cuenta_contable('ANTICIPOS', $recibo->id_empresa, $recibo->id_moneda);
                    $insertQuery = DB::select("SELECT insertar_asientos_cobranza(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) as v_id_cabecera", [
                        $cuentaContableAnticipos,
                        $recibo->importe,
                        'H',
                        $id_usuario,
                        4,
                        $recibo->id_moneda,
                        $v_cotizacion_promedio,
                        $v_id_cabecera,
                        $recibo->concepto,
                        $recibo->id_sucursal,
                        $recibo->id_centro_costo,
                        $recibo->id_empresa,
                        $v_nro_recibo
                    ]);
                    
                
                
                } else {

                        //Generar el total de facturas con la cotizacion de cada documento
                        // $total_facturas = $recibo->importe;
                        $total_facturas = 0;
                        $cotizacion_calculada = 1;
                        foreach ($recibo->detalle as $detalle) {
                            $lv = LibroVenta::where('id',$detalle->id_libro_venta)->first();
                            if($lv){
                                //TODO: Hacer el ajuste para evitar poner cotizacion en una LV en guaranies, ahora hacemos un parche
                                if($lv->id_moneda_venta != 111){
                                    $total_facturas += round($detalle->importe * (int) $lv->cotizacion_documento);
                                } else {
                                    $total_facturas += round($detalle->importe);
                                }
                               
                                
                            }
                           
                        }

                        //Solo si no es PYG cotizamos
                        if($recibo->id_moneda != 111){
                            //Obtener la cotizacion del total de los documentos cotizados para la referencia del asiento
                            $cotizacion_calculada = round($total_facturas / $recibo->importe);
                        }
                      


                        //--------------------------------------------------  DIFERENCIA DE CAMBIO   --------------------------------------------------------
 
                        $this->generar_diferencia_cambio([
                            'v_cant_anticipos'          => $v_cant_anticipos, 
                            'v_cant_items'              => $v_cant_items, 
                            'v_cotizacion_promedio'     => $v_cotizacion_promedio, 
                            'recibo'                    => $recibo, 
                            'v_id_cabecera'             => $v_id_cabecera, 
                            'id_usuario'                => $id_usuario, 
                            'v_nro_recibo'              => $v_nro_recibo, 
                            'total_facturas'            => $total_facturas
                        ]);
                                  
                        //--------------------------------------------------  DEUDORES POR VENTA - ASIENTO CIERRE --------------------------------------------------------


                        // SE INSERTA EL ASIENTO DE DEUDORES POR VENTAS
                        $cuentaContableDeudoresVentas = $this->get_id_cuenta_contable('DEUDORES_VENTAS', $recibo->id_empresa, $recibo->id_moneda);
                        $insertQuery = DB::select("SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) as v_id_cabecera", [
                            $cuentaContableDeudoresVentas,
                            $recibo->importe,
                            (int)$total_facturas, //importe cotizado con el cambio del documento
                            'H',
                            $id_usuario,
                            4,
                            $recibo->id_moneda,
                            $cotizacion_calculada,
                            $v_id_cabecera,
                            $recibo->concepto,
                            $recibo->id_sucursal,
                            $recibo->id_centro_costo,
                            $recibo->id_empresa,
                            $v_nro_recibo,
                            date('Y-m-d H:i:00')
                        ]);
                
                   
                }



            

            Log::debug('ID_ASIENTO NUEVO: '.$v_id_cabecera);

            //Anular el asiento del recibo actual
            DB::update("UPDATE asientos_contables 
                        SET fecha_hora_anulacion = ?,
                            id_usuario_anulacion = ?,
                            activo = ?
                        WHERE id = ?",[date('Y-m-d H:i:00'), 1,false, $recibo->id_asiento]);

            //Actualizar los asientos la fecha del documento
            DB::update("UPDATE asientos_contables 
            SET fecha_hora = ? 
            WHERE id = ?",[$fecha_aplicacion, $v_id_cabecera]);

            //Asignar nuevo asiento al recibo
            DB::update("UPDATE recibos 
                        SET  id_asiento = ? 
                        WHERE id = ?", [$v_id_cabecera, $id_recibo]);

            DB::select("select  redondear_asiento(?)",[$v_id_cabecera]);
            // $asientos = AsientoDetalle::where('id_asiento_contable',$v_id_cabecera)->get();

            

                   
            DB::commit();
            return 'OK';


              
        } catch (\Exception $e) {
            DB::rollback();
            Log::critical($e);
            return $e->getMessage();
           
        }

    }//function






}//class