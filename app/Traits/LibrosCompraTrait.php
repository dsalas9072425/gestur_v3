<?php 

namespace App\Traits;

use App\LibroCompra;
use DB;
use App\Exceptions\ExceptionCustom;



trait LibrosCompraTrait {

    public function anular_libro_compra_manual($id_libro_compra, $id_usuario, $id_empresa){

        $libro_compra = LibroCompra::where('id_empresa',$id_empresa)->findOrFail($id_libro_compra);

        $puede_anualar = $this->datosPermiso('verLibroCompra',58,$id_usuario );

        if(!$puede_anualar){
            throw new ExceptionCustom('NO TIENE PERMISO PARA ANULAR LC MANUAL');
        }
        
    
        //-- EL ITEM SE ENCUENTRA EN OP Y LA OP NO ESTA ANULADO
		$validar_op = DB::select("SELECT COUNT(*) as total
                    FROM op_cabecera op
                    JOIN op_detalle opd ON opd.id_cabecera = op.id
                    WHERE opd.id_libro_compra = ? 
                    AND op.id_estado <> 54",[$id_libro_compra])[0]->total;
		
		
		if($validar_op){

            $ops = DB::select("SELECT op.nro_op
                        FROM op_cabecera op
                        JOIN op_detalle opd ON opd.id_cabecera = op.id
                        WHERE opd.id_libro_compra = ? 
                        AND op.id_estado <> 54 
                        GROUP BY op.nro_op",[$id_libro_compra]);

            $op_err = '';
            foreach ($ops as $key => $op) {
                $op_err .= $op->nro_op.',';
            }
            
            throw new ExceptionCustom("EL ITEM SE ENCUENTRA DENTRO DE UNA OP, ANULE LA OP PARA CONTINUAR NRO:".$op_err);
        }


        if($libro_compra->origen != 'M'){
            throw new ExceptionCustom('EL LIBRO COMPRA NO ES DE ORIGEN MANUAL');
        }

		if($libro_compra->pendiente){
            throw new ExceptionCustom('EL ITEM DE LIBRO COMPRA ESTA EN ESTADO PENDIENTE');
        }
		

		if(!$libro_compra->activo){
            throw new ExceptionCustom('EL ITEM DE LIBRO COMPRA ESTA EN ESTADO ANULADO');
        }

        //Anulando LC
        $libro_compra->activo = false;
        $libro_compra->id_usuario_anulacion = $id_usuario;
        $libro_compra->fecha_hora_anulacion = date('Y-m-d H:i:00');
        $libro_compra->save();

        //--SE ANULAN LOS REGISTROS CORRESPONDIENTES A ESE LIBRO DE COMPRA
        DB::select("UPDATE cuenta_corriente
                    SET activo = false,
                        fecha_hora_anulacion = (to_char(now(), 'YYYY-MM-DD HH24:MI:SS'))::timestamp
                    WHERE id_documento = ? AND tipo = 'P'",[$id_libro_compra]);

        //--SE ANULAN LOS ASIENTOS CONTABLES
        DB::select("UPDATE asientos_contables
                    SET id_usuario_anulacion = ?,
                        fecha_hora_anulacion = (to_char(now(), 'YYYY-MM-DD HH24:MI:SS'))::timestamp,
                        activo = false
                    WHERE id = ? ",[$id_usuario, $libro_compra->id_asiento]);


        //-- SE ANULA LA RELACION ENTRE LC Y FACTURA_DETALLE(COSTO DE VENTA)
        DB::select("UPDATE facturas_detalle
                    SET id_libro_compra = NULL,
                    costo_proveedor = precio_costo
                    WHERE id_libro_compra = ?",[$id_libro_compra]);

        DB::select("UPDATE proformas_detalle
                    SET id_libro_compra = NULL
                    WHERE id_libro_compra = ?",[$id_libro_compra]);
        
		
		DB::select("UPDATE ventas_rapidas_detalle
                    SET id_libro_compra = NULL
                    WHERE id_libro_compra = ?",[$id_libro_compra]);
		

    }


    private function datosPermiso($vista, $permiso, $id_usuario){

        $btn = DB::select("SELECT COUNT(pe.id) as total 
                          FROM persona_permiso_especiales p, permisos_especiales pe
                          WHERE p.id_permiso = pe.id 
                          AND p.id_persona =  ? 
                          AND pe.url = ? 
                          AND pe.id = ?", [$id_usuario, $vista, $permiso])[0]->total;
    
        return $btn ? true : false;
  }//function


}