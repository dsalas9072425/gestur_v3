-- FUNCTION: public.procesar_op(integer, integer)

-- DROP FUNCTION IF EXISTS public.procesar_op(integer, integer);

CREATE OR REPLACE FUNCTION public.procesar_op(
	p_id_op integer,
	p_id_usuario integer)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

DECLARE
   
DECLARE

	--LC CONTADO OP
	v_id_asiento_lc character varying;
	v_id_tipo_documento integer;
	v_id_lc_manual integer;
	v_concepto character varying;
	v_nro_comprobante character varying;
	v_result integer;
	--RETENCIONES--
	v_total_retencion double precision;
	v_id_empresa integer;
	v_id_cabecera integer;
	v_id_cabecera_ant integer;
	v_id_proveedor integer;
	v_id_moneda_pago integer;
	v_id_asiento_pago integer;
	v_id_cuenta_contable_banco integer;
	v_id_cabecera_diferencia integer;
	v_id_cuenta_contable_proveedor integer;
	v_importe_total_pago double precision;
	v_total_gastos_adm double precision;
	v_cotizacion_promedio double precision;	
	v_diferencia_anticipo double precision;
	v_cotizacion_pago double precision;
	v_total_pagar double precision;
	v_monto_gasto double precision;
	v_diferencia double precision;
	v_total_detalle double precision;
	v_total_op_con_gastos double precision;
	v_origen character varying;
	v_id_sucursal_contable integer;
	v_id_pais integer;
	v_id_centro_costo integer;
	v_id_banco_detalle integer;
	v_total_op double precision;
	v_cotizacion_op_cabecera double precision;
	v_cotizacion_retencion double precision;
	v_diferencia_redondeo double precision;
	v_total_retencion_cotizado double precision;
	v_total_anticipo_cotizado double precision;
	v_fecha_emision date;
  v_fecha_vencimiento date;
	v_moneda_op integer;
	v_cotizacion_op double precision;
	v_total_gastos double precision;
	v_diferencia_asiento double precision;
	v_diferencias double precision;
	v_fecha_hora_autorizado date;
	
	c_op_detalle CURSOR FOR SELECT * FROM vw_op_detalle WHERE id_op = p_id_op; --OBTIENE OP DETALLE	

	c_gastos_op CURSOR FOR --CURSOR QUE OBTIENE TODOS LOS GASTOS ADM DE LA OP
		SELECT id_cuenta_contable, monto, tipo, id_moneda_gasto, monto_moneda_original, cotizacion_gasto
		FROM gastos_op
		WHERE id_op = p_id_op;
		
	c_anticipos CURSOR FOR --OBTIENE LOS ANTICIPOS
		SELECT 	a.id_cuenta_contable, a.id_moneda, a.cotizacion, s.id AS id_sucursal_contable, 
				a.id_centro_costo, op.monto
		FROM op_detalle op
		JOIN anticipos a ON a.id = op.id_anticipo
		JOIN sucursales_empresa s ON s.id = a.id_sucursal
		WHERE op.id_cabecera = p_id_op; 
				
BEGIN
	v_total_retencion_cotizado = 0;
	v_total_anticipo_cotizado = 0;
	v_id_tipo_documento = 0;
	v_id_lc_manual = 0;
	v_total_retencion = 0;
	v_cotizacion_retencion = 0;
	
			
	--PAÍS DEL PROVEEDOR PARA SABER SI ES LOCAL O EXTRANJERO
	SELECT p.id_pais
	INTO v_id_pais
	FROM op_cabecera op
	JOIN personas p ON p.id = op.id_proveedor
	WHERE op.id = p_id_op;

	--OBTIENE DATOS DEL PAGO
	SELECT 	fp.importe_total, fp.id_moneda, o.cotizacion, b.id_cuenta_contable, o.id_proveedor, o.id_cuenta_contable, 
			o.origen, o.concepto, o.id_empresa, s.id, o.id_centro_costo, o.total_pago, fp.cotizacion, b.id,
			fpd.nro_comprobante, fp.fecha_hora, fpd.fecha_vencimiento, o.id_moneda, o.cotizacion, o.fecha_hora_autorizado
	INTO 	v_importe_total_pago, v_id_moneda_pago, v_cotizacion_pago, v_id_cuenta_contable_banco, v_id_proveedor,
			v_id_cuenta_contable_proveedor, v_origen, v_concepto, v_id_empresa, v_id_sucursal_contable, 
			v_id_centro_costo, v_total_op, v_cotizacion_op_cabecera, v_id_banco_detalle, v_nro_comprobante,
			v_fecha_emision, v_fecha_vencimiento, v_moneda_op, v_cotizacion_op, v_fecha_hora_autorizado
	FROM op_cabecera o 
	--JOIN forma_pago_op_cabecera fp ON fp.id = o.id_forma_pago
	JOIN forma_pago_op_cabecera fp ON fp.id_op = o.id
	JOIN forma_pago_op_detalle fpd ON fpd.id_cabecera = fp.id
	JOIN banco_detalle b ON b.id = fpd.id_banco_detalle
	JOIN sucursales_empresa s ON s.id = o.id_sucursal
	WHERE o.id = p_id_op; 
		
	--SE OTIENE LA COTIZACIÓN PROMEDIO
	-- v_cotizacion_promedio = (SELECT get_cotizacion_promedio_op(p_id_op));
	--RAISE NOTICE 'p_id_op_ %', p_id_op;
	--RAISE NOTICE 'v_cotizacion_promedio_ %', v_cotizacion_promedio;
	
	--PARA CONTROLAR CUANDO ES ANTICIPO Y NO TIENE COTIZACIÓN PROMEDIO, SE USA EL DE LA OP
-- 	IF v_cotizacion_promedio IS NULL OR v_cotizacion_promedio = 0 THEN
		v_cotizacion_promedio = v_cotizacion_op_cabecera;
-- 	END IF;
	
	--POR CADA ITEM DE LC SE ACTUALIZA SU SALDO Y SE REGISTRAN MOVIMIENTOS EN CC
	FOR detalle IN c_op_detalle LOOP 
		v_id_tipo_documento = detalle.id_tipo_documento;
		v_id_lc_manual = detalle.id_documento;
		v_total_retencion = detalle.total_retencion;
		--v_cotizacion_retencion = detalle.cotizacion_retencion;
		v_cotizacion_retencion = detalle.cotizacion_documento;
		
	    v_result = (SELECT generar_retencion_a_cobrar(v_id_lc_manual, p_id_usuario));

		--SE ACTUALIZA EL GENERO_RETENCION DEL LIBRO DE COMPRA
		IF v_total_retencion > 0 AND v_total_retencion IS NOT NULL THEN
			UPDATE libros_compras
			SET genero_retencion = true
			WHERE id = detalle.id_documento;
		END IF;

		--SE ACTUALIZA EL SALDO DEL LIBRO DE COMPRA
		UPDATE libros_compras
		SET saldo = saldo - detalle.monto_op_detalle
		WHERE id = detalle.id_documento;

		--SÓLO SE ACTUALIZA EL SALDO CUANDO ESTÁ PENDIENTE (CUANDO SE ELIGE EL ANT DESDE LA PANTALLA DE PP)
		UPDATE anticipos
		SET saldo = saldo - detalle.monto_op_detalle
		WHERE id = detalle.id_documento AND pendiente = true;

	END LOOP;

	--SE REGISTRAN MOVIMIENTOS EN CTA CTE BANCOS
	PERFORM registrar_movimiento_cta_cte_bancos
	(
		v_concepto, v_importe_total_pago, v_id_banco_detalle::integer, v_nro_comprobante, v_fecha_emision, 
		v_fecha_vencimiento, v_id_sucursal_contable, v_id_cuenta_contable_banco, v_id_centro_costo, 
		v_id_proveedor, v_cotizacion_op_cabecera, p_id_usuario, 12, p_id_op
	);

	--SE REGISTRAN MOVIMIENTOS EN CTA CTE PROVEEDOR
	PERFORM registrar_movimiento_cta_cte(p_id_op::character varying, v_importe_total_pago, v_id_moneda_pago, v_id_proveedor, 12,'P', p_id_op);
										 
	----------------------------------------------------------------------------------------------------------------------------------
														--ASIENTOS CONTABLES
	----------------------------------------------------------------------------------------------------------------------------------
	
	--SI LA FACTURA ES DIFERENTE A CONTADO		
	IF 	v_id_tipo_documento <> 5 THEN 
		
		--RAISE NOTICE 'v_cotizacion_promedio_2 %', v_cotizacion_promedio;
		--PROVEEDORES DEL EXTERIOR O LOCALES SEGÚN OP CABECERA
		SELECT insertar_asientos(v_id_cuenta_contable_proveedor, v_total_op, 'D', p_id_usuario, 3, v_moneda_op, 
					v_cotizacion_promedio, 0, v_concepto, v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, 
					p_id_op::character varying)
		INTO v_id_cabecera;

	ELSE
		--SI ES FONDO FIJO, GENERA EL ASIENTO CORRESPONDIENTE A FF
		IF (SELECT DISTINCT fondo_fijo FROM vw_op_detalle WHERE id_op = p_id_op) = true THEN
			SELECT generar_libro_compra_manual_ff(p_id_op, p_id_usuario, 0)::integer INTO v_id_cabecera;	
		ELSE
			SELECT generar_libro_compra_manual(v_id_lc_manual, p_id_usuario, 'OP')::integer INTO v_id_cabecera;
		END IF;
	END IF;

	--------------------------------------------------GASTOS ADMINISTRATIVOS----------------------------------------------------------	
	
	--POR CADA GASTO SE GENERA UN ASIENTO, SI ES + ENTONCES VA A AL DEBE Y SI ES - AL HABER
	IF v_id_empresa <> 2 THEN
	  v_total_gastos = 0;
		FOR gasto IN c_gastos_op LOOP
			--RAISE NOTICE 'id_cuenta_contable %', gasto.id_cuenta_contable;
			--RAISE NOTICE 'monto_moneda_original %', gasto.monto_moneda_original;
			IF gasto.tipo = '+' THEN
				SELECT insertar_asientos(gasto.id_cuenta_contable, gasto.monto_moneda_original, 'D', p_id_usuario, 3, gasto.id_moneda_gasto,gasto.cotizacion_gasto, 
						v_id_cabecera, v_concepto, v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, p_id_op::character varying)
				INTO v_id_cabecera;
			ELSE
				SELECT insertar_asientos(gasto.id_cuenta_contable, gasto.monto_moneda_original, 'H', p_id_usuario, 3, gasto.id_moneda_gasto,gasto.cotizacion_gasto,
						v_id_cabecera, v_concepto, v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, p_id_op::character varying)
				INTO v_id_cabecera;
			END IF;
			v_total_gastos = v_total_gastos + coalesce(gasto.monto, 0);
		END LOOP;
	ELSE
		FOR gasto IN c_gastos_op LOOP
			IF gasto.tipo = '+' THEN
				PERFORM generar_op_anticipo_agencia(
					p_id_usuario,
					gasto.monto,
					v_id_moneda_pago,
					v_id_sucursal_contable,
					v_cotizacion_op_cabecera,
					v_id_proveedor,
					v_id_centro_costo,
					v_concepto,
					gasto.id_cuenta_contable,
					v_id_empresa,
					p_id_op,
					v_id_banco_detalle::integer);
			
			--SE REGISTRAN MOVIMIENTOS EN CTA CTE BANCOS
			PERFORM registrar_movimiento_cta_cte_bancos
			(
				v_concepto, gasto.monto, v_id_banco_detalle::integer, v_nro_comprobante, v_fecha_hora_autorizado, 
				v_fecha_vencimiento, v_id_sucursal_contable, v_id_cuenta_contable_banco, v_id_centro_costo, 
				v_id_proveedor, v_cotizacion_op_cabecera, p_id_usuario, 12, p_id_op
			);
			END IF;
			
			SELECT insertar_asientos(v_id_cuenta_contable_proveedor, gasto.monto, 'D', p_id_usuario, 3, gasto.id_moneda_gasto, 
					v_cotizacion_op_cabecera, 0, v_concepto, v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, 
					p_id_op::character varying)
			INTO v_id_cabecera_ant;
		
			--SI ES PROVEEDOR LOCAL
			IF v_id_pais = 1455 THEN
				SELECT insertar_asientos((SELECT get_id_cuenta_contable('ANTICIPOS_PROV_LOCALES', v_id_empresa, gasto.id_moneda_gasto)), 
						gasto.monto, 'H', p_id_usuario, 8, v_id_moneda_pago, v_cotizacion_op_cabecera, v_id_cabecera_ant, v_concepto, 
						v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, p_id_op::character varying)
				INTO v_id_cabecera_ant;
			ELSE
				SELECT insertar_asientos((SELECT get_id_cuenta_contable('ANTICIPOS_PROV_EXT_USD', v_id_empresa, gasto.id_moneda_gasto)), 
						gasto.monto, 'H', p_id_usuario, 8, v_id_moneda_pago, v_cotizacion_op_cabecera, v_id_cabecera_ant, v_concepto, 
						v_id_sucursal_contable, v_id_centro_costo, v_id_empresa, p_id_op::character varying)
				INTO v_id_cabecera_ant;
			END IF;
		END LOOP;
	END IF;	
	-----------------------------------------------------ANTICIPOS-------------------------------------------------------------------
	
	--SI ES ORIGEN AUTOMÁTICO
	IF v_origen = 'PP' THEN
	
		FOR anticipo IN c_anticipos LOOP
			--DIFERENCIA QUE EXISTE ENTRE EL ANTICIPO CON SU COTIZACIÓN Y EL ANTICIPO CON LA COTIZACIÓN DE LA OP 
			v_total_anticipo_cotizado = ((SELECT get_monto_cotizado_asiento(anticipo.monto, anticipo.id_moneda, 111, 
										anticipo.cotizacion)) - (SELECT get_monto_cotizado_asiento(anticipo.monto, 
										anticipo.id_moneda, 111, v_cotizacion_op_cabecera))) * -1;

			--SI ES PROVEEDOR LOCAL
			IF v_id_pais = 1455 THEN
				SELECT insertar_asientos((SELECT get_id_cuenta_contable('ANTICIPOS_PROV_LOCALES', v_id_empresa, anticipo.id_moneda)), 
						anticipo.monto, 'H', p_id_usuario, 8, anticipo.id_moneda, anticipo.cotizacion, v_id_cabecera, v_concepto, 
						anticipo.id_sucursal_contable, anticipo.id_centro_costo, v_id_empresa, p_id_op::character varying)
				INTO v_id_cabecera;
			ELSE
				SELECT insertar_asientos((SELECT get_id_cuenta_contable('ANTICIPOS_PROV_EXT_USD', v_id_empresa, anticipo.id_moneda)), 
						anticipo.monto, 'H', p_id_usuario, 8, anticipo.id_moneda, anticipo.cotizacion, v_id_cabecera, v_concepto, 
						anticipo.id_sucursal_contable, anticipo.id_centro_costo, v_id_empresa, p_id_op::character varying)
				INTO v_id_cabecera;
			END IF;
		END LOOP;
	END IF;
	
	-----------------------------------------------------RETENCIÓN-------------------------------------------------------------
	--RAISE NOTICE 'v_total_retencion %', v_total_retencion;
	
	IF v_total_retencion IS NOT NULL AND v_total_retencion <> 0 THEN
	
		--DIFERENCIA QUE EXISTE ENTRE LA RETENCIÓN CON SU COTIZACIÓN Y LA RETENCIÓN CON LA COTIZACIÓN DE LA OP 
		v_total_retencion_cotizado = ((SELECT get_monto_cotizado_asiento(v_total_retencion, v_moneda_op, 111, 
			v_cotizacion_retencion)) - (SELECT get_monto_cotizado_asiento(v_total_retencion, v_moneda_op, 111, 
			 v_cotizacion_op_cabecera))) * -1;

		--RETENCION IMP. A LA RENTA A PAGAR
		--AQUI El CAMBIO
		SELECT insertar_asientos((SELECT get_id_cuenta_contable('RETENCION', v_id_empresa, null)), v_total_retencion, 'H', 
				p_id_usuario, 3, v_moneda_op, v_cotizacion_retencion, v_id_cabecera, v_concepto, v_id_sucursal_contable, 
				v_id_centro_costo, v_id_empresa, p_id_op::character varying)
		INTO v_id_cabecera;
		
	END IF;
	
	-----------------------------------------------------BANCOS (CUENTAS)--------------------------------------------------------
	
	SELECT insertar_asientos(v_id_cuenta_contable_banco, v_importe_total_pago, 'H', p_id_usuario, 3, v_id_moneda_pago, 
							 v_cotizacion_op_cabecera, v_id_cabecera, v_concepto, v_id_sucursal_contable, v_id_centro_costo, 
							 v_id_empresa, p_id_op::character varying)
	INTO v_id_cabecera;

	-----------------------------------------------DIFERENCIA DE CAMBIO-------------------------------------------------------------
	
	SELECT SUM(debe) - SUM(haber)
	INTO v_diferencia_asiento
	FROM asientos_contables_detalle 
	WHERE id_asiento_contable = v_id_cabecera;
	
	v_total_op_con_gastos = v_total_op + v_total_gastos;
	
	--v_diferencia = (SELECT get_diferencia_cambio_op(v_total_op, v_cotizacion_promedio, v_moneda_op, 'V', v_id_empresa, v_cotizacion_op));
	v_diferencia = (SELECT get_diferencia_cambio_op(v_total_op_con_gastos, v_cotizacion_promedio, v_moneda_op, 'V', v_id_empresa, v_cotizacion_op));
	
	--RAISE NOTICE 'v_diferencia_1: %', v_diferencia;
	v_diferencias = v_diferencia_asiento - v_diferencia;
	IF v_diferencias < 0 THEN
		v_diferencia = v_diferencia - v_diferencias;
	END IF;
	
	IF v_diferencias > 0 THEN
		v_diferencia = v_diferencia + v_diferencias;
	END IF;
												
	--RAISE NOTICE 'v_diferencia_2: %', v_diferencia;
  --RAISE NOTICE 'v_total_retencion_cotizado: %', v_total_retencion_cotizado;
	--RAISE NOTICE 'v_total_anticipo_cotizado: %', v_total_anticipo_cotizado;

	--SÓLO SI EXISTE DIFERENCIA DE CAMBIO SE LE SUMAN LOS MONTOS DE DIFERENCIA DE RETENCIÓN Y ANTICIPO
	IF v_diferencia <> 0 THEN 
		v_diferencia = v_diferencia + v_total_retencion_cotizado + v_total_anticipo_cotizado;
	END IF;
		
	--RAISE NOTICE 'v_diferencia_2%', v_diferencia;
	
	--SE GENERA EL ASIENTO CORRESPONDIENTE A DIFERENCIA POSITIVA O NEGATIVA
	PERFORM generar_diferencia_cambio(v_id_cabecera, p_id_usuario, v_concepto, v_id_sucursal_contable, v_id_centro_costo, 
									 v_id_empresa, v_diferencia, 1::double precision, 111, true, 3, p_id_op::character varying);
									 
------------------------------------------------------DIFERENCIA REDONDEO-------------------------------------------------------
	
	--DIFERENCIA DE REDONDEO HASTA 50 GS SI EXISTE DIFERENCIA DE CAMBIO

 	PERFORM generar_diferencia_redondeo(v_id_cabecera, p_id_usuario, v_concepto, v_id_sucursal_contable, v_id_centro_costo,v_id_empresa, v_diferencia, 3, p_id_op::character varying);
	
	--DIFERENCIA DE 1 GS	
	PERFORM redondear_asiento(v_id_cabecera);
	
	--------------------------------------------------------------------------------------------------------------------------------
	
	--SE OBTIENE EL IMPORTE TOTAL DE LOS LC ASOCIADOS A LA OP
	SELECT SUM(importe) 
	INTO v_total_detalle
	FROM libros_compras l
	JOIN op_detalle d ON d.id_libro_compra = l.id
	WHERE d.id_cabecera = p_id_op;     
	
	--SE ACTUALIZA EL ESTADO DE LA OP A PROCESADO
	UPDATE op_cabecera 
	SET id_estado = 53, 
		id_usuario_procesado = p_id_usuario, 
		fecha_hora_procesado = (now())::timestamp(0) without time zone,
		total_op_detalle = v_total_detalle,
		total_op = v_total_detalle + v_total_gastos,
		id_asiento = v_id_cabecera/*,
		cotizacionoperativa = (SELECT public.get_cotizacion_actual(v_id_empresa))*/
	WHERE id = p_id_op; 
	
	--SI ES FACTURA CONTADO SE LE ASIGNA EL MISMO ASIENTO QUE LA OP
	IF 	v_id_tipo_documento = 5 THEN
		UPDATE libros_compras
		SET id_asiento = v_id_cabecera
		WHERE fondo_fijo = false AND id IN (SELECT id_libro_compra FROM op_detalle WHERE id_cabecera = p_id_op);
	END IF;
	
	--UNA VEZ PROCESADA EL LC, PENDIENTE PASA A SER FALSO
	UPDATE libros_compras
	SET pendiente = false
	WHERE id IN (SELECT id_libro_compra FROM op_detalle WHERE id_cabecera = p_id_op and id_nc is null);
	
	--UNA VEZ PROCESADA EL LC, PENDIENTE PASA A SER FALSO
	UPDATE libros_compras
	SET pendiente = false
	WHERE id IN (SELECT id_libro_compra FROM op_detalle WHERE id_cabecera = p_id_op and id_nc is not null);
	
	--UNA VEZ PROCESADA EL ANTICIPO, PENDIENTE PASA A SER FALSO
	UPDATE anticipos
	SET pendiente = false
	WHERE id IN (SELECT id_anticipo FROM op_detalle WHERE id_cabecera = p_id_op);
	
	--SE ACTUALIZA EL ID_ASIENTO DE ANTICIPO
	UPDATE anticipos
	SET id_asiento = v_id_cabecera
	WHERE id_op = p_id_op;
	
END;
$BODY$;

ALTER FUNCTION public.procesar_op(integer, integer)
    OWNER TO postgres;
