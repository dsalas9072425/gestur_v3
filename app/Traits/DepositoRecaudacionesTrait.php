<?php 

namespace App\Traits;

use App\DepositoBancario;
use App\FormaCobroReciboDetalle;
use DB;
use Session;
use Log;
use App\Exceptions\ExceptionCustom;


trait DepositoRecaudacionesTrait {


    public function depositar_recaudaciones($p_id_deposito){

                          
        $deposito_banco = DepositoBancario::with('deposito_detalle.forma_cobro_recibo_detalle.forma_cobro','banco_detalle')->findOrFail($p_id_deposito);


        /**
         * - Agrupar por forma de cobro
         * - Cotizar cada forma de cobro
         * - Obtener la cuenta contable de la forma de cobro
         */

         $asientos = [];
         $valores_cta_ctb = [];
         $valores_cta_ctb_original = [];
         $valores_cta_ctb_cotizaciones = [];
         $total = 0;
         $subtotal = 0;
         $saldo = 0;
         $v_id_cabecera = 0;
         $total_original = 0;

        //Calcular el total por cuenta contable, ya que ahora es independiente de la forma de cobro
        foreach ($deposito_banco->deposito_detalle as  $detalle) {

          
            $total_original += $detalle->importe;
            // $saldo = $detalle->forma_cobro_recibo_detalle->saldo;
            $subtotal = $detalle->importe;

            //Obtener el total cotizado de esa forma de cobro solo si no es guaranies
            if($deposito_banco->id_moneda != 111){
                $subtotal = $detalle->importe * $detalle->forma_cobro_recibo_detalle->cotizacion_contable;
            } 

            //Se actualiza el saldo de la forma de cobro
            $forma_cobro = FormaCobroReciboDetalle::find($detalle->forma_cobro_recibo_detalle->id);
            // Log::info($forma_cobro->id.' - '.$detalle->importe.' - '.$forma_cobro->saldo);
            $forma_cobro->saldo = $forma_cobro->saldo - $detalle->importe;
            $forma_cobro->save();
            
           

          
            if(!$detalle->forma_cobro_recibo_detalle->id_cta_ctb){
                throw new ExceptionCustom('Error en cuenta contable de forma de cobro del recibo.');
            }

            /**
             * Vamos a agrupar las cuentas contables
             * Y segun la cuenta vamos a hacer la suma
             */
            if(isset($valores_cta_ctb[$detalle->forma_cobro_recibo_detalle->id_cta_ctb])){
                $valores_cta_ctb[$detalle->forma_cobro_recibo_detalle->id_cta_ctb] += $subtotal; //Total por cuenta contable
                $valores_cta_ctb_original[$detalle->forma_cobro_recibo_detalle->id_cta_ctb] += $detalle->importe; //Total monto sin cotizar
                $valores_cta_ctb_cotizaciones[$detalle->forma_cobro_recibo_detalle->id_cta_ctb] += $detalle->forma_cobro_recibo_detalle->cotizacion_contable; //Total cotizaciones que se cargaron en el cobro
            } else {
                $valores_cta_ctb[$detalle->forma_cobro_recibo_detalle->id_cta_ctb] = $subtotal; //Total por cuenta contable
                $valores_cta_ctb_original[$detalle->forma_cobro_recibo_detalle->id_cta_ctb] = $detalle->importe; //Total monto sin cotizar
                $valores_cta_ctb_cotizaciones[$detalle->forma_cobro_recibo_detalle->id_cta_ctb] = $detalle->forma_cobro_recibo_detalle->cotizacion_contable; //Total cotizaciones que se cargaron en el cobro
            }
           
            $total += $subtotal; //suma total
            $subtotal = 0;

            $values = [
                null, //p_id_cuenta_banco_origen
                null, //p_nro_comprobante_origen
                $deposito_banco->id_banco_detalle, //p_id_cuenta_banco_destino
                $deposito_banco->nro_operacion, //p_nro_comprobante_destino
                $deposito_banco->id_sucursal,
                $deposito_banco->id_centro_costo,
                $deposito_banco->fecha_emision, //p_fecha_transferencia
                $detalle->importe, //p_importe
                $deposito_banco->cotizacion_contable_original, //p_cotizacion
                'DEPÓSITO', //p_concepto
                $deposito_banco->id_usuario, //p_id_usuario
                2, //p_id_tipo_transferencia - Deposito de Recaudaciones
                $deposito_banco->id_empresa, //p_id_empresa
                $detalle->importe, //p_importe
                null,
                null,
                null
            ];

            //Insertar cada forma de cobro como movimiento de deposito

           $id =  DB::select("SELECT insertar_transferencias(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", $values)[0]->insertar_transferencias;
           
           //Se actualiza a aprobado
           DB::select("UPDATE transferencias_cuentas 
                      SET id_estado = 58,
                          fecha_hora_procesado = '$deposito_banco->fecha_emision 00:00:00',
                          id_usuario_procesado = $deposito_banco->id_usuario
                      WHERE id = $id");

        }

         /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												--ASIENTO CONTABLE SEGUN FORMA DE COBRO
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */
        foreach ($valores_cta_ctb as $id_cuenta_contable => $monto_gs) {

           $total_cotizado = round($monto_gs);
           $total_original_m = $valores_cta_ctb_original[$id_cuenta_contable];

           //Si es diferente a GS
            if($deposito_banco->id_moneda != 111){
                 //Es un promedio de todas las formas de cobro
                $cotizacion_referencia = round($total_cotizado /$total_original_m );
            } else {
                $cotizacion_referencia = 1;
            }

            // dd( $id_cuenta_contable , $monto_gs);

            $params_asientos = [
                $id_cuenta_contable,
                $total_original_m, //total original por cuenta
                $total_cotizado, //total de montos cotizados con sus cotizaciones 
                'H',
                $deposito_banco->id_usuario,
                6,
                $deposito_banco->id_moneda,
                $cotizacion_referencia,
                $v_id_cabecera,
                $deposito_banco->concepto,
                $deposito_banco->id_sucursal,
                $deposito_banco->id_centro_costo,
                $deposito_banco->id_empresa,
                $deposito_banco->nro_deposito,
                $deposito_banco->fecha_emision //Fecha del asiento segun fecha deposito
            ];
       
            $v_id_cabecera = DB::select('SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos_custom;
            
        }


        /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												--REGISTRO DE MOVIMIENTO DE CUENTA
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */
            DB::select("SELECT registrar_movimiento_cta_cte_bancos(
                ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            , [
                'Depósito Recaudación Crédito',
                $total_original,
                $deposito_banco->id_banco_detalle,
                $deposito_banco->nro_operacion,
                $deposito_banco->fecha_emision,
                null,
                $deposito_banco->id_sucursal,
                null,
                $deposito_banco->id_centro_costo,
                null,
                $deposito_banco->cotizacion_contable_original,
                $deposito_banco->id_usuario,
                25,
                $p_id_deposito //Numero deposito
            ]);


        /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												        ASIENTO DE CUENTA DE FONDO
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */

         $total_cotizado_custom = $total_original;
           //Total cotizado
           if($deposito_banco->id_moneda != 111){
                $total_cotizado_custom =  round($total_original * $deposito_banco->cotizacion_contable_original);
           }
            

            $params_asientos = [
                $deposito_banco->banco_detalle->id_cuenta_contable,
                $total_original, //Total no cotizado
                $total_cotizado_custom, //total de montos cotizados con sus cotizaciones 
                'D',
                $deposito_banco->id_usuario,
                3,
                $deposito_banco->id_moneda,
                $deposito_banco->cotizacion_contable_original,
                $v_id_cabecera,
                $deposito_banco->concepto,
                $deposito_banco->id_sucursal,
                $deposito_banco->id_centro_costo,
                $deposito_banco->id_empresa,
                $deposito_banco->nro_deposito,
                $deposito_banco->fecha_emision
            ];
            $v_id_cabecera = DB::select('SELECT insertar_asientos_custom(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', $params_asientos)[0]->insertar_asientos_custom;

             



        /**
         * 
         * 	----------------------------------------------------------------------------------------------------------------------------------
		 *												        DIFERENCIA DE CAMBIO
	     *  ----------------------------------------------------------------------------------------------------------------------------------
         * 
         */

        // Suma y resta de valores
        $v_diferencia_asiento = DB::table('asientos_contables_detalle')
        ->selectRaw('SUM(debe) - SUM(haber) as diferencia')
        ->where('id_asiento_contable', $v_id_cabecera)
        ->first()
        ->diferencia;
        

        // Ejecución de la función generar_diferencia_cambio
        DB::statement("SELECT generar_diferencia_cambio(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [
            $v_id_cabecera, 
            $deposito_banco->id_usuario, 
            $deposito_banco->concepto, 
            $deposito_banco->id_sucursal, 
            $deposito_banco->id_centro_costo, 
            $deposito_banco->id_empresa, 
            $v_diferencia_asiento, 
            1, 
            111, 
            true, 
            3, 
            $deposito_banco->nro_deposito
        ]);

    

   
        $deposito_banco->id_asiento = $v_id_cabecera;
        $deposito_banco->save();


    }


    private function deposito_efectivo(){

    }

    private function deposito_cheque(){
        
    }


}