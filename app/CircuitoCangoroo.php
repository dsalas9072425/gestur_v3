<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CircuitoCangoroo extends Model
{
    protected $table = 'circuitos_cangoroo';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


}