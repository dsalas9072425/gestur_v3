<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HabitacionesReserva extends Model
{
    protected $table = 'habitaciones_reserva';

     public function reserva()
    {
        return $this->belongsTo('App\Reserva','id_reserva','reserva');
    }
	
	public function ocupantesHabitacion()
    {
        return $this->hasMany('App\OcupantesHabitacion','habitacion','id');
    }

}