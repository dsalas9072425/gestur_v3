<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaPagoOpCabecera extends Model
{
    protected $table = 'forma_pago_op_cabecera';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


    public function fp_detalle()
    {
      return  $this->hasMany('App\FormaPagoOpDetalle','id_cabecera');
    }

    public function moneda()
    {
      return $this->belongsTo('App\Currency','id_moneda');
    }

    public function op()
    {
      return $this->belongsTo('App\OpCabecera','id_op');
    }



    
}
