<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProformaCliente extends Model
{
    protected $table = 'proforma_clientes';
    protected $primarykey = 'id';
    public $timestamps = false;
    public function cliente()
    {
        return $this->belongsTo('App\Persona', 'id_persona','id');
    }

}