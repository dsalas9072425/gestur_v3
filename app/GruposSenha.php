<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GruposSenha extends Model
{
    protected $table = 'grupos_senhas';
	protected $primarykey = 'id';
    public $timestamps = false;
}
