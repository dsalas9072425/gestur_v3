<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anticipo extends Model
{
    protected $table = 'anticipos';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;



    public function beneficiario_format()
    {
      return $this->hasOne('App\Persona','id','id_beneficiario')->selectRaw("CONCAT(nombre,' ',apellido) as beneficiario_n,id");
    }

    public function beneficiario()
    {
      return $this->hasOne('App\Persona','id','id_beneficiario');
    }

    public function currency()
    {
      return $this->belongsTo('App\Divisas','id_moneda');
    }

    public function tipoDocumento()
    {
        return $this->belongsTo('App\TipoDocumento', 'id_tipo_documento','id');
    }

    public function estado()
    {
        return $this->hasOne('App\EstadoFactour', 'id','id_estado');
    }

    public function recibo()
    {
        return $this->hasOne('App\Recibo', 'id','id_recibo');
    }

    public function grupo()
    {
        return $this->belongsTo('App\Grupos','id_grupo','id');
    }

    public function proforma()
    {
        return $this->belongsTo('App\Proforma','id_proforma','id');
    }

    public function op()
    {
        return $this->belongsTo('App\OpCabecera','id_op','id');
    }

    public function op_aplicacion()
    {
        return $this->hasMany('App\OpDetalle','id_anticipo','id');
    }
    public function usuarioAnulacion()
    {
        return $this->belongsTo('App\Persona','id_usuario_anulacion','id');
    }

    public function aplicacion()
    {
        return $this->hasMany('App\AplicarAnticipo','id_anticipo','id');
    }


}
