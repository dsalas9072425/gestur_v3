<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaCreditoDetalle extends Model
{
    protected $table = 'nota_credito_detalle';
    protected $primaryKey = 'id'; 
	public $timestamps = false;
	protected $dateFormat = 'Y-m-d H:i:s';


	public function producto()
    {
        return $this->belongsTo('App\Producto', 'id_producto','id');
    }







}