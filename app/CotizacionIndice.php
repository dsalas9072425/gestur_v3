<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CotizacionIndice extends Model
{
    protected $table = 'cotizaciones_indice';
    protected $primaryKey = 'id';
    //Para evitar campos de auditorias
    public $timestamps = false;

	//Relacion de Cotizacion con Divisas
	public function moneda_origen()
    {
        return $this->belongsTo('App\Divisas','id_moneda_origen','currency_id');
    }

    public function moneda_destino()
    {
        return $this->belongsTo('App\Divisas','id_moneda_destino','currency_id');
    }


}


