<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProformaReprice extends Model
{
    protected $table = 'proformas_reprice';
    protected $primarykey = 'id';
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';
 
    
    public function agencia()
    {
        return $this->belongsTo('App\Persona', 'cliente_id', 'id');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Persona', 'id_usuario', 'id');
    }
    public function asistente()
    {
        return $this->belongsTo('App\Persona', 'id_asistente', 'id');
    }

    public function vendedor()
    {
        return $this->belongsTo('App\Persona', 'vendedor_id', 'id');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoFactour', 'estado_id', 'id');
    }

    public function pasajero()
    {
        return $this->belongsTo('App\Persona', 'pasajero_id', 'id');
    }
  
    public function cliente()
    {
        return $this->belongsTo('App\Persona', 'cliente_id', 'id');
    }

    public function currency()
    {
        return  $this->belongsTo('App\Divisas', 'id_moneda_venta', 'currency_id');
    }

    public function factura()
    {
        return $this->belongsTo('App\Factura', 'factura_id', 'id');
    }

    public function operativo()
    {
        return $this->belongsTo('App\Persona', 'responsable_id', 'id');
    }

    public function tipoFacturacion()
    {
        return $this->belongsTo('App\TipoFacturacion', 'tipo_facturacion', 'id');
    }

    public function voucher()
    {
        return $this->hasMany('App\Voucher','id_proforma','id');
    }

    public function tarifas()
    {
        return $this->belongsTo('App\Tarifa','id_tarifa','id');
    }
}
