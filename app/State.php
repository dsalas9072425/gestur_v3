<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'estado_reserva';
    protected $primaryKey = 'state_id'; 

}