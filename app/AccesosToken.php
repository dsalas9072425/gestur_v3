<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccesosToken extends Model
{
    protected $table = 'accesos_dtpmundo';
    protected $primaryKey = 'accesos_dtpmundo'; 

    public function usuario()
    {
        return $this->belongsTo('App\Usuario','id_usuario','usuario');
    }


}