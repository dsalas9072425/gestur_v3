<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SucursalEmpresa extends Model
{
    protected $table = 'sucursales_empresa';
    protected $primaryKey = 'id';  

}