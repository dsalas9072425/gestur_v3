<?php

namespace App\Exceptions;

use Exception;

//Esta es una exepcion customizada para las que se generan manualmente, lo demas sera capturado y no sera mostrado al cliente

class ExceptionCustom extends Exception
{
    // Puedes personalizar tu excepción agregando métodos o propiedades adicionales si es necesario
}