<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsientoDetalle extends Model
{
    protected $table = 'asientos_contables_detalle';
    protected $primaryKey = 'id';
    public $timestamps = false;

	public function asiento()
    {
        return $this->belongsTo('App\Asiento','id_asiento_contable','id');
    }

    public function moneda()
    {
        return $this->belongsTo('App\Divisas','id_moneda','currency_id');
    }

    public function cuentaContable()
    {
        return $this->belongsTo('App\PlanCuenta','id_cuenta_contable','id');
    }


    

}


