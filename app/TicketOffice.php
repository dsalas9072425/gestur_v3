<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketOffice extends Model
{
    protected $table = 'tickets_offices';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;

}
