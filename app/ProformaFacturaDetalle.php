<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ProformaFacturaDetalle extends Model
{

    protected $table = 'proformas_detalle_factura';
    protected $primaryKey = 'id';  
    public $timestamps = false;

}
