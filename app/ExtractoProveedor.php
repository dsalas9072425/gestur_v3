<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtractoProveedor extends Model
{
    protected $table = 'extracto_proveedor';
    public $timestamps = false;
	public function usuario()
    {
        return $this->belongsTo('App\Usuario','user_id','id_usuario');
    }

	public function proveedores()
    {
        return $this->belongsTo('App\Infoprovider','proveedor','infoprovider_id');
    }

    public function extracto_detalles()
    {
        return $this->hasMany('App\ExtractoDetalles','extracto_proveedor_id','id');
    }
    
}