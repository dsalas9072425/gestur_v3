<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketFractal extends Model
{
    protected $table = 'tickets_fractal';
    public $timestamps = false;
	protected $primaryKey = 'id';
}