<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoCierreCaja extends Model
{
    
    protected $table = 'historico_cierre_caja_cabecera';
    public $timestamps = false;

    public function usuario()
    {
       return $this->belongsTo('App\Persona','id_usuario_cierre','id');
    }

	public function usuarioAnulacion()
    {
       return $this->belongsTo('App\Persona','id_usuario_anulacion','id');
    }


}
