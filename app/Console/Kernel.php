<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\HistoricoCuentasJob;
use App\Jobs\HistoricoCierreCajaJob;
use App\Jobs\GenerarPdfEjecutivo;
use App\Jobs\AlertaTempranaUtilidadesJob;
use App\Jobs\NotificarVoucherDtpPuntos;
use App\Jobs\RefreshNewDataMiniuto;
use App\Jobs\RefreshNewData;
use App\Jobs\RefreshPagoProveedor;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //'App/Console/Commands/LogTest'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('test:prueba')->everyMinute();
        $schedule->call(function(){
            (new HistoricoCuentasJob)->handle();
            (new HistoricoCierreCajaJob)->handle();
            (new GenerarPdfEjecutivo)->handle();
        })->dailyAt('23:45'); //Una vez al dia a las 23:00

        $schedule->call(function () {
            (new NotificarVoucherDtpPuntos)->handle();
        })->dailyAt('12:00');

        // ========== TAREA DE ALERTA TEMPRANA ==========
        $schedule->call(function () {
            (new AlertaTempranaUtilidadesJob)->handle();
        })->dailyAt('15:00');

        $schedule->call(function () {
            (new AlertaTempranaUtilidadesJob)->handle();
        })->dailyAt('11:27');

        $schedule->call(function () {
            (new AlertaTempranaUtilidadesJob)->handle();
        })->dailyAt('08:00');

        // $schedule->call(function () {
        //     (new RefreshNewDataMiniuto)->handle();
        // })->everyTenMinutes();
        // ==============================================

        //======TAREA PARA REFRESCAR LA VISTA MATERILAZADA========


        $schedule->call(function () {
            (new RefreshNewDataMiniuto)->handle();
        })->everyTenMinutes();

        $schedule->call(function () {
            (new RefreshNewData)->handle();
        })->hourly();

        $schedule->call(function () {
            (new RefreshPagoProveedor)->handle();
        })->everyTenMinutes();

        //========================================================
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
       // $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
