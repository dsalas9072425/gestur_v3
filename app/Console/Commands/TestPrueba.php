<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestPrueba extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:prueba';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comentario de la Prueba';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      ///////////////////////////////////////////////////////////////////////////////
  # Listo, aquí llama a Log::algunMetodo();
      Log::debug("Hola mundo, soy un mensaje de debug desde parzibyte.me");
      # También puedes pasar datos en forma de arreglo:
      $nombre = "Luis";
      Log::debug("Se está registrando un usuario", ["nombre" => $nombre]);
      # O usar var_export concatenando, aunque no lo recomiendo. Lee sobre var_export:
      # https://parzibyte.me/blog/2018/05/18/alternativa-var_dump-php-detalles-variable/
      $mascota = [
        "nombre" => "Maggie"
      ];
      Log::info("Tenemos una mascota: " . var_export($mascota, true));
///////////////////////////////////////////////////////////////////////////////
    }
}
