<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DestinoDtpmundo extends Model
{
    protected $table = 'destinos_dtpmundo';
	protected $primaryKey = 'id_destino_dtpmundo';
	//protected $fillable = ['id', 'id_usuario','hora_fecha'];
    public $timestamps = false;


	public function destinosProveedor()
    {
        return $this->belongsToMany('App\DestinoProveedor', 'destinos_proveedor_dtpmundo', 'id_destino_dtpmundo', 'id_destino');
    }

		
}
