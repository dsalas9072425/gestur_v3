<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = 'masterbm_hoteles';
    protected $primaryKey = 'id';  

}