<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class LiquidacionComisionDtpPuntoDetalle extends Model
{
    protected $table = 'liquidacion_dtp_puntos_detalle';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
    protected $fillable = [
        'liquidacion_id',
        'factura_id',
        'proforma_id',
        'promocion_id',
        'monto_comision',
        'puntos',
        'pagado',
        'comentario',
        'created_at',
        'updated_at',
    ];

    
    
}
