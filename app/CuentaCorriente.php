<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentaCorriente extends Model
{
	protected $table = 'cuenta_corriente';
	public $primarykey = 'id';
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';

	public function persona()
    {
        return $this->belongsTo('App\Persona', 'id_persona','id');
    }
    public function moneda()
    {
        return $this->belongsTo('App\Currency', 'id_moneda','currency_id');
    }
    public function factura()
    {
        return $this->belongsTo('App\Factura', 'documento','nro_factura');
    }
 }   