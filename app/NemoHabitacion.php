<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NemoHabitacion extends Model
{
    protected $table = 'reservas_nemo_habitaciones';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
}