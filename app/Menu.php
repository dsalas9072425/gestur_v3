<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
    
	public function submenus()
    {
		return $this->hasMany('App\Menu', 'id_menu_padre', 'id');
	}

}