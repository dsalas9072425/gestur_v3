<?php
namespace App\Http\Middleware;
use Redirect;
use Closure;
use Session;
use Config;

class AutorizacionVistas
{
  public function handle($request, Closure $next)
  {
    $menuEmpresas = Session::get('arrayMenu');
    $rutaActual = $request->route()->getName();
    $cantidad = 0;

    foreach ($menuEmpresas as $key => $menu) 
    {
      foreach ($menu as $key => $menuEmpresa) 
        {
            if (isset($menuEmpresa['submenu'])) 
            {
                foreach ($menuEmpresa['submenu'] as $key => $menu) 
                {
                    if ($menu['url'] === $rutaActual) //si tiene permiso le deja entrar a la página
                    {
                        $cantidad = 1;
                    }
                }
            }
    
        }
      }

    if($cantidad == 1)
    {
        return $next($request);
    }
    else // si no, le redirecciona al home
    {
        flash('No tienes los permisos necesarios para acceder a esta dirección')->error();
        return redirect()->route('home');
    }

  }




}