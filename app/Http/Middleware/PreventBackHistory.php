<?php

namespace App\Http\Middleware;

use Session;
use Config;
use Closure;

class PreventBackHistory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       // return redirect()->away(Session::get('urlFull'));  
    }

}