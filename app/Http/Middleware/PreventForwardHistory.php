<?php

namespace App\Http\Middleware;

use Closure;

class PreventForwardHistory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if(empty($request->session()->get('token'))){
            return redirect()->route('home');       
        }
        return $response;
    }
}