<?php
namespace App\Http\Middleware;
use Redirect;
use Closure;
use Session;
use Config;

class Autorization
{ 
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil)){   
            if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 10 && $request->session()->get('datos-loggeo')->datos->datosUsuarios->id_sistema_facturacion != null ){
                flash('No tienes los permisos para vvvvvvvvvvvvvvvvvvvvvv acceder a esta dirección')->error();
                return Redirect::back();
            }
            return $next($request);
        }else{
            return redirect()->guest('login');
        }    
    }
}
