<?php
namespace App\Http\Middleware;
use Redirect;
use Closure;
use Session;
use Config;

class ForgetSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Session::forget('ocupancia');
        Session::forget('hotelsSession');
        Session::forget('datosPreReservaRQ');
        Session::forget('datosPreReservaRP');
        Session::forget('totalReservaPrecioNeto');
        Session::forget('totalReservaSinImpuestos');
        Session::forget('totalReservaConComision');
        Session::forget('totalPrecioNetoConvertido');
        return $next($request);
    }
}