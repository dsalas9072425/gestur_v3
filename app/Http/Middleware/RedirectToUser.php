<?php

namespace App\Http\Middleware;
use Session;
use Closure;

class RedirectToUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
       if($request->session()->get('datos-loggeo')->datos->datosUsuarios->id_sistema_facturacion === null){
            //return redirect()->route('usuariosEdit', ['id' => $request->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario]);
       }
        return $response;

    }
}