<?php

namespace App\Http\Middleware;
use Session;
use Closure;

class SearchToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty($request->session()->get('token'))){
            return redirect()->route('home');       
        }
        return $next($request);
    }
}
