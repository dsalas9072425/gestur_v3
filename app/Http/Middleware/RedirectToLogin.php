<?php

namespace App\Http\Middleware;
use Session;
use Closure;

class RedirectToLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next 
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd('llegó');
        if(!$request->session()->has('datos-loggeo')){
                flash('<b>Error de Sessión</b></br> No posee los permisos o su session ha caducado.')->error();
    			return redirect()->route('login');
		}
        return $next($request);
    }
}
