<?php
namespace App\Http\Middleware;
use Redirect;
use Closure;
use Session;
use Config;

class AdminOperativo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil)){   
            if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminOpAdmin')|| Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 8){
                flash('No tienes los permisos para acceder a esvfasbbbbbbbbbbbbbbta dirección')->error();
                return Redirect::back();
            }
            return $next($request);
        }else{
            return redirect()->guest('login');
        }     
    }
}
