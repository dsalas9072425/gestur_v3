<?php
namespace App\Http\Middleware;
use Redirect;
use Closure;
use Session;
use Config;

class GenerateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->put('token',"");
        $request->session()->put('token', \Session::token()."_".rand());
        return $next($request);
    }
}