<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
// use Automattic\WooCommerce\Client; //PACKAGE WOOCOMMERCE
use Carbon\Carbon;
use App\PedidoWoo;
use App\FormaCobroCliente;
use App\ClientePersona;
use App\Persona;
use App\Producto;
use App\Divisas;
use App\VentasRapidasCabecera;
use App\VentasRapidasDetalle;
use App\EstadoFactour;
use App\AuditoriaPersona;
use DB;
use Response;
use Image;
use Session;


class ApiWooCommerceController extends Controller
{


       /**
       * ===============================================================================================
       *                                METODOS WEBHOOKS API WOO DE UN SOLO CANAL 
       * ===============================================================================================
       * */  

           //VALIDACION DE SECRET KEY PENDIENTE
        // $secret = 'CwFs$75w$wGg`Y: AfPn&YD.IBJP:!EV/`T)%M>S7VTq@9FF <'; //CLAVE WEBHOOK WOOCOMMERCE
        // $headerPass = $req->header('x-wc-webhook-signature'); //CLAVE GENERADA WOO
        // $yourHashSig = base64_encode(hash_hmac('sha256',$req->getContent() , $secret, true));
        // $rsp = false;

        // if($headerPass == $yourHashSig){
        //     $rsp = true;
        // }
        // return response()->json(['2'=>json_encode($req->getContent(),true),'0'=>$yourHashSig,'1'=>$headerPass]);

    public function end_point_webhooks(Request $req)
    {  
	
	
        //PRUEBA DE WEBHOOKS API
        if($req->input('webhook_id'))
        {
                /*-------------------------------------------------------------------------------------------- */
                Log::info('PRUEBA WEBHOOKS'.' '.json_encode($req->all()));
                return response()->json(['OK']);
                /*-------------------------------------------------------------------------------------------- */
        } else {
                Log::info('WEBHOOKS INFO'.' '.json_encode($req->all()));
        }

        /**
         * SE CONOCO EL  TIPO DE WEBHOOKS
         * Y LA OPERACION QUE REALIZA
         */
        $tipo_webhooks = $req->header('x-wc-webhook-resource');
        $tipo_operacion_webhooks = $req->header('x-wc-webhook-event');
        $carga_util = new \StdClass;
		
		

          //ENCONTRAR EL ID_EMPRESA RELACIONADA A LA PAGINA
          $url_empresa = $req->header('x-wc-webhook-source');
		  $url_empresa = parse_url ( $url_empresa, PHP_URL_HOST);		  
		  
          $empresas = DB::table('comercio_empresa')->where('url_web',$url_empresa)->get();
          //$empresas[0]->id_empresa = 1;
          if(isset($empresas[0]->id_empresa)){
              $id_empresa = $empresas[0]->id_empresa;
			  $id_comercio_empresa = $empresas[0]->id;
          } else {
			  $id_empresa = 6;
			  $id_comercio_empresa = 1;
              Log::info('No se encuentra la empresa, asignamos 1-1 por defecto y continuamos.');
              //return response()->json(['Error no se encuentra la empresa']);
          }


        $carga_util->tipo_operacion = $tipo_operacion_webhooks;
        $carga_util->id_empresa = $id_empresa;
		$carga_util->id_comercio_empresa = $id_comercio_empresa;
        $carga_util->request = $req;
        $carga_util->option = 0;
        $id_pedido = 0;
        $count_prod_exist = null;
        $redondeo = 0; //DEFAULT
        $id_cliente_woo = null;
        $detalle_arr = [];
        $id_producto = null;
        

          /**
           * TIPO WEBHOOKS
           */
          Log::info('INIT  WEB HOOKS '.strtoupper($tipo_webhooks).' '.strtoupper($tipo_operacion_webhooks));
          try{
            switch ($tipo_webhooks) 
            {
                case 'product':
                        $id_producto = $this->producto($carga_util);
                    break;  
                case 'order':
                       
                    if($tipo_operacion_webhooks == 'created')
                    { 
					
                                //PROCESO DE REGISTRO POR ORDER CREATED
                                DB::beginTransaction();
                                //CONTAR E INSERTAR PRODUCTOS

                                foreach($req->input('line_items') as $value)
                                {       
                                        $product = new \StdClass;
                                        if($value['variation_id']){
                                            $id_producto = $value['variation_id'];
                                        } else {
                                            $id_producto = $value['product_id'];
                                        }
                                        $product->id = $id_producto;
                                        $product->name = $value['name'];
                                        $product->price = $value['price'];
										$product->woo_sku = $value['sku'];
                                        // $product->id_variation = $value['variation_id'];
                                        $product->stock_quantity = null;

                                        $carga_util->option = 1;    
                                        $carga_util->product = $product;
                                        
                                        $prodObj = new \StdClass;
                                        $prodObj->id_producto = $this->producto($carga_util);
                                        $this->update_producto($carga_util);
                                        $prodObj->cantidad = $value['quantity'];
                                        $prodObj->precio = $value['price'];
                                        $prodObj->item = $value['name'];
                                        $prodObj->total = $value['total'];
					                    $prodObj->item = $value['name'];					
										if($value['sku']=='') $prodObj->nro_factura_proveedor = $value['name'];
										else $prodObj->nro_factura_proveedor = $value['sku'];
										
                                        $detalle_arr[] = $prodObj;
                                }
								
                                Log::info('OK END FOR PRODUCTO');
								
								//REGISTRAR SI HAY COSTO DE DELIVERY
                                foreach($req->input('shipping_lines') as $value)
                                {       
									if($value['total'] > 0){
                                        $product = new \StdClass;
                                        $id_producto = $value['instance_id'];
                                        $product->id = $id_producto;
                                        $product->name = 'DELIVERY '.$value['method_title'];
                                        $product->price = $value['total'];
                                        // $product->id_variation = $value['variation_id'];
										$product->woo_sku = $id_producto;
                                        $product->stock_quantity = null;

                                        $carga_util->option = 1;    
                                        $carga_util->product = $product;
                                        
                                        $prodObj = new \StdClass;
                                        $prodObj->id_producto = $this->producto($carga_util);
                                        $this->update_producto($carga_util);
                                        $prodObj->cantidad = 1;
                                        $prodObj->precio = $value['total'];
                                        $prodObj->total = $value['total'];
		                    $prodObj->item = 'DELIVERY '.$value['method_title'];
					$prodObj->nro_factura_proveedor = 'DELIVERY';
                                        $detalle_arr[] = $prodObj;
									}
                                }
                                Log::info('OK END FOR DELIVERY');
								
								// Obtenemos latitud y longitud para ventas_rapidas_cabecera
								foreach($req->input('meta_data') as $meta => $detalle)
								{ 	
									
									if($detalle['key'] == '_shipping_lat'){
										$carga_util->delivery_lat = $detalle['value'];
									}
									
									if($detalle['key'] == '_shipping_lng'){
										$carga_util->delivery_lng = $detalle['value'];
									}				
								}

                                 //INSERTAR PEDIDO
                                 $carga_util->id_pedido = $this->pedido($carga_util);
                                 Log::info('OK PEDIDO');

                                //INSERTAR CLIENTE
                                $carga_util->id_cliente = $this->cliente($carga_util);
                                $carga_util->detalle_arr = $detalle_arr;
								$carga_util->req_body = json_encode($req->all());
  
			
                                //INSERTAR VENTA RAPIDA  
                                $this->venta_rapida($carga_util);
                                Log::info('OK VENTA RAPIDA');


                                DB::commit();
                                // DB::rollBack();

                    } else {
                        Log::error('Error operacion order no contemplada');
                        return response('Error operacion order no contemplada',200);
                    }
                      


                break;           
                default:
                    # RESPONDE 200 (Error al procesar los datos)
                    Log::error('Error al procesar los datos - SWITCH ENDPOINT');
                    return response('Error al procesar los datos - SWITCH ENDPOINT',200);
                    break;
            }

        }  catch(\Exception $e){
            Log::error('ROLLBACK  ERROR '.$tipo_webhooks.' '.$tipo_operacion_webhooks.' PROCESS');
            Log::error($e);
            DB::rollBack();
            return response('Ocurrio un error al procesar el pedido - R', 200);
        }

    

        return response()->json(['exito']);
    }

      /*
       * ===============================================================================================
       *                                VENTA RAPIDA
       * ===============================================================================================
       */  

    /**
     * Funcion que asigna la operacion Venta Rapida
     */
        private function venta_rapida($obj)
        {
            $tipo_operacion = $obj->tipo_operacion;
            $id = null;
            switch ($tipo_operacion) 
            {
                case 'created':
                     $this->insert_venta_rapida($obj);
                    break;
                case 'updated':
                        $this->update_venta_rapida($obj);
                    break;       
                case 'deleted':
                        $this->delete_soft_venta_rapida($obj);
                    break;          
                default:
                    # RESPONDE 200 (Error al procesar los datos)
                    break;
            }

        }
    
        private function insert_venta_rapida($obj)
        {   
            $req = $obj->request;
            $id_empresa = $obj->id_empresa;
            $id_cliente_woo = $obj->id_cliente;
            $id_pedido = $obj->id_pedido;
            $detalle_arr = $obj->detalle_arr;
			$direccion_cliente = "";
			

             $monto_total_orden = $req->input('total');
             $date = Carbon::parse($req->input('date_created'));
             $moneda = Divisas::where('currency_code',$req->input('currency'))->get();

            //  if((isset($moneda[0]->currency_id))){
            //      if($moneda[0]->currency_id == 111){
            //          $redondeo = null;
            //      } 
            //  }
             $iva_total = $monto_total_orden / 11;
             
			 $facturacion_data = (Object)$req->input('billing');
			 
			 foreach($req->input('meta_data') as $meta => $detalle)
			{ 	
				if($detalle['key'] == '_billing_direccion'){
					$direccion_cliente = $detalle['value'];
				}
			}			
						
             //INSERT CABECERA
             $venta_rapida_cabecera = new VentasRapidasCabecera;
             $venta_rapida_cabecera->id_cliente =  $id_cliente_woo;
             $venta_rapida_cabecera->id_empresa = $id_empresa; 
             $venta_rapida_cabecera->id_vendedor = $this->control_vendedor_venta_rapida($obj,'vendedor_empresa', $facturacion_data->email);
             $venta_rapida_cabecera->id_vendedor_agencia = $this->control_vendedor_venta_rapida($obj,'vendedor_agencia', $facturacion_data->email);

			 if($venta_rapida_cabecera->id_vendedor != null && $venta_rapida_cabecera->id_vendedor_agencia == null){
				$venta_rapida_cabecera->id_tipo_venta_rapida = 2;
			 }
			 else if($venta_rapida_cabecera->id_vendedor == null && $venta_rapida_cabecera->id_vendedor_agencia != null){
				$venta_rapida_cabecera->id_tipo_venta_rapida = 3;
			 }
			 else {
				$venta_rapida_cabecera->id_tipo_venta_rapida = 4;
			 }

			//REGISTRAR SI HAY COSTO DE DELIVERY
			$delivery_tipo = "";
			foreach($req->input('shipping_lines') as $value)
			{       
				if($value['total'] > 0){
					$delivery_tipo = $value['method_title'];
				}
			}

          /* if(isset($obj->delivery_lat)){
                $delivery_lng = $obj->delivery_lat;
            }else{   
                $delivery_lng = "";
            }
            if(isset($obj->delivery_lng)){
                $delivery_lng = $obj->delivery_lng;
            }else{   
                $delivery_lng ="";
            }*/
	
              $venta_rapida_cabecera->id_estado = 71; //"Venta Abierta"
             $venta_rapida_cabecera->fecha_venta = $date->format('Y-m-d');
             $venta_rapida_cabecera->total_bruto = $monto_total_orden;
             $venta_rapida_cabecera->total_neto = $monto_total_orden;
             $venta_rapida_cabecera->total_exentas = 0;
             $venta_rapida_cabecera->total_gravadas =  $monto_total_orden - $iva_total;
             $venta_rapida_cabecera->total_iva = $iva_total;
             $venta_rapida_cabecera->total_venta = $monto_total_orden;
             $venta_rapida_cabecera->costo_delivery = $req->input('shipping_total');
             $venta_rapida_cabecera->activo = true;
             $venta_rapida_cabecera->id_moneda = (isset($moneda[0]->currency_id)) ? $moneda[0]->currency_id: null;
             $venta_rapida_cabecera->total_iva_5 = 0;
             $venta_rapida_cabecera->id_tipo_factura = 2; //CONTADO
             $venta_rapida_cabecera->id_tipo_facturacion = 1; //BRUTO
             $venta_rapida_cabecera->id_pedido = $id_pedido;
			 $venta_rapida_cabecera->delivery_direccion = $direccion_cliente;
			
          		/* $venta_rapida_cabecera->delivery_lat = $delivery_lat;
			 $venta_rapida_cabecera->delivery_lng = $delivery_lng;*/

			// $venta_rapida_cabecera->delivery_lat = $obj->delivery_lat;//atk
			// $venta_rapida_cabecera->delivery_lng = $obj->delivery_lng;
			 $venta_rapida_cabecera->delivery_referencia = $facturacion_data->address_2;
			 $venta_rapida_cabecera->delivery_tipo = $delivery_tipo; 
			 $venta_rapida_cabecera->id_comercio_empresa = $obj->id_comercio_empresa;
			 $venta_rapida_cabecera->req_woo = json_encode($obj->req_body);
			 
			 $venta_rapida_cabecera->delivery_cliente = strtoupper($facturacion_data->first_name.' '.$facturacion_data->last_name);
			 $venta_rapida_cabecera->delivery_telefono = $facturacion_data->phone;
			 
			 if(strtoupper($req->input('payment_method')) == 'BACS') $venta_rapida_cabecera->id_forma_pago = 4;
			 else if(strtoupper($req->input('payment_method')) == 'COD') $venta_rapida_cabecera->id_forma_pago = 1;
			 else if(strtoupper($req->input('payment_method')) == 'CHEQUE') $venta_rapida_cabecera->id_forma_pago = 7;
			 else $venta_rapida_cabecera->id_forma_pago = 1; // por defecto efectivo.
			 //dd($venta_rapida_cabecera);
             $venta_rapida_cabecera->save();
			 //dd($venta_rapida_cabecera);
			 //INSERT DETALLE
             foreach($detalle_arr as $value)
             {  
                 $venta_rapida_detalle = new VentasRapidasDetalle;
                 $venta_rapida_detalle->id_venta_cabecera = $venta_rapida_cabecera->id;
                 $venta_rapida_detalle->id_producto =  $value->id_producto;
                 $venta_rapida_detalle->precio_costo = 0;
                 $venta_rapida_detalle->precio_venta = $value->total;
                 $venta_rapida_detalle->cantidad = $value->cantidad;
		         $venta_rapida_detalle->item = $value->item;
                 $venta_rapida_detalle->activo = true;
                 $venta_rapida_detalle->origen = 'A';
                 $venta_rapida_detalle->monto_bruto = $value->total;
                 $venta_rapida_detalle->monto_neto = $value->total;
				 $venta_rapida_detalle->nro_factura_proveedor = $value->nro_factura_proveedor;
                 $venta_rapida_detalle->iva = null;
                 $venta_rapida_detalle->iva_5 = null;
                 $venta_rapida_detalle->save();
                 DB::table('productos')->where('id',$value->id_producto)
					->update([
                            'denominacion'=>$value->item,
                            'frase_predeterminada'=>$value->item
                            ]);	
             }
                        
        }

        private function update_venta_rapida($obj)
        {

        }

        private function select_venta_rapida($obj)
        {

        }

        private function delete_soft_venta_rapida($obj)
        {

        }
        /**
         * Asigna el id_persona a id_vendedor y id_vendedor_agencia de venta rapida
         */
        private function control_vendedor_venta_rapida($obj,$opt,$email)
        {
            $correo = [];
            $cliente_reg = [];
            $tipo_persona = [];
            $tipo_vend_empresa = 3;
            $tipo_vend_agencia = 10;
            $resp = null;
			
            //IDENTIFICAR CORREO
            //$correo = Persona::where('id',$obj->id_cliente)->get(['email']);
            //if(isset($correo[0]->email)){
               //$correo = trim(strtoupper($correo[0]->email));
			if(isset($email)){
               $cliente_reg =  DB::select("SELECT id_persona 
                                            FROM personas_clientes 
                                            WHERE UPPER(e_mail_cliente) = '".trim(strtoupper($email))."' 
                                            AND activo = true");
            }

            //IDENTIFICAR TIPO PERSONA
			
			Log::info("SELECT id_persona FROM personas_clientes WHERE UPPER(e_mail_cliente) = '".strtoupper($email)."' AND activo = true");
			
            if(isset($cliente_reg[0]->id_persona))
            {
				
                $tipo_persona = Persona::where('id',$cliente_reg[0]->id_persona)->get(['id_tipo_persona']);
				
				if($opt == 'vendedor_empresa' && $tipo_persona[0]->id_tipo_persona != $tipo_vend_agencia)
                {
                    $resp = $cliente_reg[0]->id_persona;
                } 
                else if($tipo_persona[0]->id_tipo_persona == $tipo_vend_agencia && $opt == 'vendedor_agencia')
                {
                    $resp = $cliente_reg[0]->id_persona;
                }
				else{
				}
            }
			else Log::info('::No encontramos este cliente como recomendado');

            return $resp;

        }



     /*
       * ===============================================================================================
       *                                PEDIDOS
       * ===============================================================================================
       */  
      
     /**
     * Funcion que asigna la operacion Pedido
     */
      private function pedido($obj)
      { 
            $tipo_operacion = $obj->tipo_operacion;
            $id = null;
            switch ($tipo_operacion) 
            {
                case 'created':
                    $id = $this->insert_pedido($obj);
                    break;
                case 'updated':
                        $this->update_pedido($obj);
                    break;       
                case 'deleted':
                        $this->delete_soft_pedido($obj);
                    break;          
                default:
                    # RESPONDE 200 (Error al procesar los datos)
                    break;
            }

            return  $id;
      }
   
      private function insert_pedido($obj)
      { 
        $id = null;
        $count_prod_exist = null;
        $req = $obj->request;
        $fc_git = null;
        $cant_product = 0;

        //VERIFICAR SI EXISTE PRODUCTO
        $count_prod_exist = PedidoWoo::where('id_woo_order',$req->input('id'))->get();
        //OBTENER LA FORMA DE COBRO DEL CLIENTE
        $forma_cobro_git =  FormaCobroCliente::where('id_fc_woo',$req->input('payment_method'))->get();
        if(isset($forma_cobro_git[0]->id)){
                $fc_git = $forma_cobro_git[0]->id;
        }

            foreach($req->input('line_items') as $value)
            {
                $cant_product += (integer)$value['quantity'];
            }

        if(!count($count_prod_exist))
        {
			

            $pedidos_woo = new PedidoWoo;
            $pedidos_woo->created_via = $req->input('created_via');
            $pedidos_woo->currency = $req->input('currency');
            $pedidos_woo->id_woo_order = $req->input('id');
            $pedidos_woo->status = $req->input('status');
            $pedidos_woo->order_key = $req->input('order_key');
            $pedidos_woo->created_at = $req->input('date_created');
            $pedidos_woo->updated_at = $req->input('date_created');
			// $pedidos_woo->url_cliente = $req->input('_links')['self'][0]['href'];
            // $pedidos_woo->url_pedido = $req->input('_links')['customer'][0]['href'];
            $pedidos_woo->customer_id = $req->input('customer_id');
			//if(strtoupper($req->input('payment_method')) == 'BACS') $pedidos_woo->payment_method = 6;
			//else if(strtoupper($req->input('payment_method')) == 'COD') $pedidos_woo->payment_method = 10;
            $pedidos_woo->payment_method = $req->input('payment_method');
            $pedidos_woo->payment_method_title = $req->input('payment_method_title');
            $pedidos_woo->total = $req->input('total');
            $pedidos_woo->id_forma_cobro_cliente = $fc_git;
            $pedidos_woo->id_empresa = $obj->id_empresa;
            $pedidos_woo->cant_item =  $cant_product;
			
			foreach($req->input('meta_data') as $meta => $detalle)
			{ 	
				
				if($detalle['key'] == '_shipping_lat'){
					$pedidos_woo->delivery_lat = $detalle['value'];
				}
				
				if($detalle['key'] == '_shipping_lng'){
					$pedidos_woo->delivery_lng = $detalle['value'];
				}				
			}
            $pedidos_woo->save();
            $id = $pedidos_woo->id;
        } else {
            $id = $count_prod_exist[0]->id;
        }

        return  $id;
      }
  
      private function update_pedido($obj)
      {
  
      }
  
      private function select_pedido($obj)
      {
  
      }
  
      private function delete_soft_pedido($obj)
      {
  
      }

       /*
       * ===============================================================================================
       *                                CLIENTES
       * ===============================================================================================
       */

    /**
     * Funcion que asigna la operacion Cliente
     */
      private function cliente($obj)
      {
            $tipo_operacion = $obj->tipo_operacion;
            $id = null;
            switch ($tipo_operacion) 
            {
                case 'created':
                    $id = $this->insert_cliente($obj);
                    break;
                case 'updated':
                        $this->update_cliente($obj);
                    break;       
                case 'deleted':
                        $this->delete_soft_cliente($obj);
                    break;          
                default:
                    # RESPONDE 200 (Error al procesar los datos)
                    break;
            }

            return  $id;
      }
   
      private function insert_cliente($obj)
      { 
          $req = $obj->request;
          $client_exist_count = [];
          $id = null;
		  $documento_cliente = "";
		  $direccion_cliente = "";

            $facturacion_data = (Object)$req->input('billing');
			$meta_data = (Object)$req->input('meta_data');
			
			foreach($req->input('meta_data') as $meta => $detalle)
			{ 	
				//if($detalle['key'] == '_billing_wooccm11'){
				if($detalle['key'] == '_billing_ruc'){	
					$documento_cliente = $detalle['value'];
				}
				if($detalle['key'] == '_billing_direccion'){
					$direccion_cliente = $detalle['value'];
				}
			}
			
			
			
			//Log::info('documento_cliente: ' . $documento_cliente);
			
            /**
             * Si es costumer_id = 0 es un cliente que realizo una compra con sus 
             * datos pero no se registro en la web.
             */
			/*
            if($req->input('customer_id') != '0'){
                //VERIFICAR SI YA EXISTE CLIENTE
                 $client_exist_count = Persona::where('customer_woo_id',$req->input('customer_id').'-'.$obj->id_empresa)
                 ->where('activo',true)->get();
            }
			else{
				$client_exist_count = Persona::where('email',$facturacion_data->email)
                 ->where('activo',true)->get();
			}
			*/
			
		
			$client_exist_count = Persona::where('documento_identidad',$documento_cliente)
                 ->where('activo',true)->where('id_empresa',$obj->id_empresa)->get();
			
            if(!count($client_exist_count)){
                //CREAR CLIENTE
                $cliente_woo = new Persona;
                $cliente_woo->customer_woo_id = $req->input('customer_id').'-'.$obj->id_empresa;
                $cliente_woo->id_tipo_persona = 11; //CLIENTE DIRECTO
                $cliente_woo->id_personeria = 1;
                $cliente_woo->id_empresa = $obj->id_empresa;
                $cliente_woo->activo = true;
                //$cliente_woo->documento_identidad = 'PENDIENTE'; //OBNTENER CEDULA
				$cliente_woo->documento_identidad = $documento_cliente; //OBNTENER CEDULA
                $cliente_woo->apellido = $facturacion_data->last_name;
                $cliente_woo->nombre = $facturacion_data->first_name;
                $cliente_woo->direccion = $direccion_cliente;
                $cliente_woo->email = $facturacion_data->email;
                $cliente_woo->celular = $facturacion_data->phone;
                $cliente_woo->created_at = $req->input('date_created'); //RECUPERAR DATO CON RESPONSE ADICIONAL
                $cliente_woo->updated_at = $req->input('date_created'); //RECUPERAR DATO CON RESPONSE ADICIONAL
                $cliente_woo->save();
                $id = $cliente_woo->id;
                Log::info('CLIENTE NUEVO');
            } else {
                $id = $client_exist_count[0]->id;
                Log::warning('CLIENTE YA EXISTIA');
            }


            return $id;
      }
  
      private function update_cliente($obj)
      {
  
      }
  
      private function select_cliente($obj)
      {
  
      }
  
      private function delete_soft_cliente($obj)
      {
  
      }


       /*
       * ===============================================================================================
       *                                PRODUCTOS
       * ===============================================================================================
       */

    /**
     * Funcion que asigna la operacion Producto
     */
      private function producto($obj)
      { 
          $tipo_operacion = $obj->tipo_operacion;
          $id = null;
          switch ($tipo_operacion) {
            case 'created':
                $id = $this->insert_producto($obj);
                break;
            case 'updated':
                   $this->update_producto($obj);
                break;       
            case 'deleted':
                    $this->delete_soft_producto($obj);
                break;  
                # RESPONDE 200 (Error al procesar los datos)
                break;
        }

            return  $id;

      }

      /**
       * @return id Producto
       */
      private function insert_producto($obj)
      { 
        Log::info('INIT PRODUCTO INSERT');
            $id = null;
            $count_prod_exist = null;
            $id_producto_woo = $obj->request->input('id');

            if($obj->option){
                $id_producto_woo = $obj->product->id;
            }

            //VERIFICAR SI EXISTE PRODUCTO Y LA VARIACION
            $count_prod_exist = new Producto;
            $count_prod_exist =  $count_prod_exist->where('id_producto_woo',$id_producto_woo);
			$count_prod_exist =  $count_prod_exist->where('id_comercio_empresa',$obj->id_empresa);
            if($obj->option){
                // $count_prod_exist =  $count_prod_exist->where('id_variation_woo',$obj->product->id_variation);
            }
            $count_prod_exist =  $count_prod_exist->get();
            if(!count($count_prod_exist)){
                $prod = new Producto;
				
				//dd($obj);

                if($obj->option){
                    //SE CREA PRODUCTO CON PEDIDO
                    $prod->denominacion = $obj->product->name;
                    $prod->id_empresa = $obj->id_empresa; 
                    $prod->updated_at = date('Y-m-d H:i:00'); //RECUPERAR DATO CON RESPONSE ADICIONAL
                    $prod->created_at = date('Y-m-d H:i:00'); //RECUPERAR DATO CON RESPONSE ADICIONAL
                    $prod->tiene_dtplus = false;
                    $prod->frase_predeterminada = $obj->product->name;
                    $prod->id_producto_woo = $obj->product->id;
                    $prod->porcentaje_gravada = 100; //POR DEFECTO AHORA 100
                    $prod->id_tipo_impuesto = 3; //IMPUESTO 10%
                    // $prod->id_variation_woo = $obj->product->id_variation;//VARIACION DEL PRODUCTO//
                    $prod->comision_base = 0; 
                    $prod->precio_venta = (float)$obj->product->price; 
                    $prod->stock = (Integer)$obj->product->stock_quantity; 
                    $prod->id_grupos_producto = 11; //Productos WOO;
					$prod->woo_sku = $obj->product->woo_sku;
					$prod->id_comercio_empresa = $obj->id_empresa;
					$prod->id_cuenta_contable_venta = 1155;
                    $prod->id_usuario_alta = 41117;
                } else {
                    //INSERT NORMAL DE PRODUCTO
					$prod->denominacion = $obj->request->input('name');
                    $prod->id_empresa = $obj->id_empresa; 
                    $prod->updated_at = $obj->request->input('date_modified'); 
                    $prod->created_at = $obj->request->input('date_created'); 
                    $prod->tiene_dtplus = false;
                    $prod->frase_predeterminada = $obj->request->input('name');
                    $prod->id_producto_woo = $obj->request->input('id');
                    $prod->porcentaje_gravada = 100; //POR DEFECTO AHORA 100
                    $prod->comision_base = 0; 
					//$prod->variation_woo_arr = $this->setConvertArray($obj->request->input('variations'));
					$prod->id_tipo_impuesto = 3; //IMPUESTO 10%
                    $prod->precio_venta = (float)$obj->request->input('price'); 
                    $prod->stock = (Integer)$obj->request->input('stock_quantity'); 
                    $prod->id_grupos_producto = 11; //Productos WOO;
					$prod->woo_sku = $obj->request->input('sku'); 
					$prod->id_comercio_empresa = $obj->id_empresa;
					$prod->id_cuenta_contable_venta = 1155;
					$prod->id_usuario_alta = 41117;
					//dd($prod);

                }
                $prod->save();
				Log::info('OK PRODUCTO INSERT!!!');
                $id = $prod->id;
            } else {
                Log::warning('OK PRODUCTO EXISTENTE');
                $id = $count_prod_exist[0]->id;
                //HACER UPDATE DE PRODUCTO DE EXISTENTE CUANDO SEA TIPO WEBHOOKS SEA DEL TIPO PRODUCTO
                if(!$obj->option){
                    Log::info('PRODUCTO SE PUEDE ACTUALIZAR');
                    $this->update_producto($obj);
                }
                
            }

            
            return  $id;
      }

 
  
      private function update_producto($obj)
      { 
        Log::info('INIT PRODUCTO UPDATE');
        $variations_prod = [];
        $array_variation = [];
        $arr_product_delete_soft = [];
        $delete_data = new \StdClass;

        //PUEDEN UPDATE SOBRE PRODUCTOS NO CREADOS. ENTONCES SE CREA EL PRODUCTO
        //VERIFICAR SI SE ELIMINA VARIACIONES;
        //$prod = Producto::where('id_producto_woo',$obj->request->input('id'))->first();
		
		$prod = Producto::where('id_producto_woo',$obj->request->input('id'))
						->where('id_comercio_empresa',$obj->id_empresa)->first();
		
		//$prod = Producto::where('id_comercio_empresa',$obj->request->id_empresa);
				
        //VERIFICAR SI EXISTE
        if($prod){
            
			//$variations_prod = $this->getConvertArray($prod->variation_woo_arr);
            //$array_variation = $obj->request->input('variations');
			
			 ############AGREGAR IF ADICIONAL PARA VERIFICAR EL ARRAY##################
            $variations_prod = [];
            //VERIFICAR QUE EXISTE VARIACION
            //if(count($prod->variation_woo_arr) > 0){
			if(isset($prod->variation_woo_arr)){
				if(is_array($prod->variation_woo_arr)){
					$variations_prod = $this->getConvertArray($prod->variation_woo_arr);
					$array_variation = $obj->request->input('variations');
				}
			}

            if(count($variations_prod) > 0){
                foreach($variations_prod as $value)
                {
                    if(!in_array($value,$array_variation)){
                        $request = new Request(['id'=>$value]); //CONVERTIR ARRAY EN REQUEST
                        $delete_data->request = $request;
                        $this->delete_soft_producto( $delete_data);
                    }
                }
            } 
              
            $prod->denominacion = $obj->request->input('name');
            $prod->frase_predeterminada = $obj->request->input('name');
            $prod->created_at = $obj->request->input('date_created');
            $prod->updated_at = $obj->request->input('date_modified');
            $prod->variation_woo_arr = $this->setConvertArray($array_variation);
            $prod->precio_venta = (float)$obj->request->input('price'); 
            $prod->stock = (integer)$obj->request->input('stock_quantity');
			$prod->woo_sku = $obj->request->input('sku');
            
			$prod->save();
            Log::info('PRODUCTO WEBHOOKS'.' '.json_encode($prod));
			
        } else {
            Log::warning('NO EXISTE PRODUCTO PARA UPDATE');
            $id_producto = $this->insert_producto($obj);
        }
     }
  
      private function delete_soft_producto($obj)
      {
        Log::info('INIT PRODUCTO DELETE SOFT');
        $prod = Producto::where('id_producto_woo',$obj->request->input('id'))->first();
        if($prod){
            $prod->updated_at = date('Y-m-d H:i:00');
            $prod->activo = false;
            Log::info('OK PRODUCTO DELETE SOFT');
            $prod->save();
        } else {
            Log::warning('NO EXISTE PRODUCTO PARA DELETE SOFT');
        }

      }

     /**
       * ===============================================================================================
       *                                METODOS USANDO API WOO DOBLE CANAL
       * ===============================================================================================
       * */  

      /**
       * PENDIENTE VER PORQUE SE GENERA ERROR AL INTENTAR CONECTAR CON LA PAGINA DE WOO
       */
      public function migrar_productos()
      {
        // $local_web = 'http://beta.atukasa.com.py';
        // // dd(env('CK_KEY_WOO_API'));
        // $woocommerce = new Client(
        //     'http://beta.atukasa.com.py', 
        //     "ck_30f43f1e453ed76e16bab5087634eb2d5312a8d", 
        //     "cs_486d79e73dc458c55cf4c5f6bc6ab88ca8d0cec8",
        //     [   
        //         // 'wp_api' => true, // Enable the WP REST API integration
        //         "version" => "wc/v3",
        //         //  "query_string_auth" => "true",
        //         //  "verify_ssl" => "false"
        //     ]
        // );
        
        // // $data = [
        // //     'status' => 'completed'
        // // ];
        
        // print_r($woocommerce->get('products'));
        
      }


    /**
       * ===============================================================================================
       *                                METODOS AUXILIARES
       * ===============================================================================================
       *  */  

       /**
        * Convertir array para almacenar en postgresql
        */
    public function setConvertArray($value) {
        return str_replace(['[', ']'], ['{', '}'], json_encode($value));
    }

    public function getConvertArray($value) {
        return json_decode(str_replace(['{', '}'], ['[', ']'], $value));
    }

      private function getIdUsuario()
      {
  
        return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
      }
    
      private function getIdEmpresa()
      {
    
       return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
      }
}
