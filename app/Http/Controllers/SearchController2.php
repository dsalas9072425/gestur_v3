<?php

namespace App\Http\Controllers;
//pasar a la sesion el searchInfo al pre
// al hacer click en el boton modificar sid
use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use \App\Dtp\Proveedor;
use App\Aeropuertos;
use Session;
use DB;
use Carbon\Carbon;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Promise;
use Illuminate\Support\Facades\URL;

class SearchController extends Controller
{
	//private $sid;

	private function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}
	
	/*
	Página principal de busqueda de hoteles
	-Se toman los datos de la ocupancia que se generaron en el home
	*/
	public function index(Request $req){

			$sid = Session::get('sid');
			Session::put('selectedHotel',null);
			$iibReq = new \StdClass;
			$datosLoggeo = Session::get('datos-loggeo');
			$iibReq->token = $datosLoggeo->token;
			$datosBusqueda = new \StdClass;
			$iibReq->codDestino = $req->input('destino');
			$fechaDesde = explode("/",$req->input('fecha_desde'));
			$iibReq->fechaDesde = $fechaDesde[1]."/".$fechaDesde[0]."/".$fechaDesde[2];
			$fechaHasta = explode("/",$req->input('fecha_hasta'));
			$iibReq->fechaHasta = $fechaHasta[1]."/".$fechaHasta[0]."/".$fechaHasta[2];
			$ocupanciasDtp = json_decode($req->input('ocupancia'));
			$req->session()->put('ocupanciasDtp', $ocupanciasDtp);
			$iibReq->cantAdultos = $req->input('tadults');
			$iibReq->cantHabitaciones = $req->input('trooms');
			$iibReq->cantNinos = $req->input('tchild');
			$datosUsuario = new \StdClass;
			$datosUsuario->idUsuario = $datosLoggeo->datos->datosUsuarios->idUsuario;
			$datosUsuario->idAgencia = $datosLoggeo->datos->datosUsuarios->idAgencia;
			$iibReq->datosUsuario = $datosUsuario;
			
			include("../proveedor.php");	//de aca sale marcadorFiltro
			$datos = file_get_contents("../destination.json");
			$destinos =  json_decode($datos, true);
			foreach ($destinos as $key=>$value){
				if(intval($value['idDestino']) == intval($req->destino)){
						$destino = $destinos[$key];
						break;
				}
			}

			$ssl   = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on';
		    $proto = strtolower($_SERVER['SERVER_PROTOCOL']);
		    $proto = substr($proto, 0, strpos($proto, '/')) . ($ssl ? 's' : '' );
			$req->session()->put('urlFull', $proto . '://' .$_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI']);

			return view('pages.hoteles.search')->with('inputs',$req->except('ocupancia'))->with('ocupancia',$req->input('ocupancia'))->with('destino',$destino['desDestino'])->with('sid', $sid);

	}

	/*
	Se genera las destinaciones para el listado de busqueda
	*/
	public function getDestinations($id){
		$datos = file_get_contents("../destination.json");
		$destinos =  json_decode($datos, true);
			foreach ($destinos as $key=>$destino){
				if(intval($destino['idDestino']) == intval($id)){
						$marcador = $destinos[$key];
				}
			}
        return response()->json($marcador);
	}
	
	public function doTotalSearch(Request $req){
		$providers = explode(',', env('PROVIDERS',"2,3,4,5,6"));
		$client = new Client([
			'defaults' => [
				'config' => [
					'curl' => [
						CURLMOPT_MAX_TOTAL_CONNECTIONS => 100,
						'body_as_string' => true
					]
				]
			]
		]);
		
		$jar = CookieJar::fromArray([
			'gestur_session' => Session::getId()
			], env("COOKIE_URL")
		);
		$sid = $req->input('sid');
		$promises = [];
		$urls = [];
		for($k=0 ; $k<sizeof($providers) ; $k++){
			$url = route('doSearch') . '?' . $req->getQueryString() . '&provId=' . $providers[$k] . '&sid=' . $sid;
			$urls[] = $url;
			$promises[] = $client->getAsync($url, ['cookies' => $jar]);
			
		}
		$results = Promise\settle($promises)->wait();
		
		$fp = fopen('searchids/' . $sid ."/acabo.txt", "w");
		fclose($fp);
		return json_encode("termino");
		foreach ($results as $domain => $result) {
			return json_encode($result);
		}

	}
	
	public function _doTotalSearch(Request $req){
		//solo ESCRIBE seguramente en la sesion, no retorna hoteles
		$providers = [2,3,4,7,8,9,10,11,12,13];
		$client = new \GuzzleHttp\Client();
		$jar = CookieJar::fromArray([
			'gestur_session' => Session::getId()
			], env("COOKIE_URL")
		);
		for($k=0 ; $k<sizeof($providers) ; $k++){
		}
		$results = Promise\settle($requestPromises)->wait();
		for($k = 0 ; $k<sizeof($providers) ; $k++){
		}
		$fp = fopen('searchids/' . Session::get('sid') ."/acabo.txt", "w");
		return json_encode("termino");
	}

	public function getCurrentHotels(Request $req){
		//esta funcion tiene que leer los hoteles solo una vez que hay nuevos resultados o doTotalSearch termino
		$searchRsp = new \StdClass;
		$searchRsp->codRetorno = 0;
		$searchRsp->desRetorno = "request de vistas e info exitosas";
		$sid = $req->input('sid');
		Session::put('sid',$req->input('sid'));
		$hashPath = 'searchids/' . $sid . '/hash';
		$searchInfo = new \StdClass;
		//identifica el directorio del hash viejo
		$gcHashPath = 'searchids/' . $sid . '/gchash';
		
		//la primera vez crea el directorio y asigna como hash viejo 'new' para que siempre sea distinto al hash nuevo
		if (!is_dir($gcHashPath))
		{
			mkdir($gcHashPath, 0755, true);
			$fpp = fopen($gcHashPath . '/new','w');
			fclose($fpp);
		}
		
		//aca siempre se tiene el hash viejo (que puede ser uno viejo o 'new')
		$gcHash = scandir($gcHashPath . '/')[2];
		while(true){
			if(file_exists($hashPath)){
				$lastHash = scandir($hashPath . '/')[2];
				if($gcHash != $lastHash || file_exists('searchids/' . $sid ."/acabo.txt")){					
					$gcHash = $lastHash;		//para sesion si se usa
					if($req->has('segurola')){
						$searchInfo = $this->getSearchInfo(null,false,true,$sid);
					}else $searchInfo = $this->waitUntilRead($sid);
					$files = glob($gcHashPath . '/*');
					foreach($files as $file){
					  if(is_file($file))
						unlink($file);
					}
					$fp = fopen($gcHashPath . '/' .  $lastHash, "w");	
					fclose($fp);
					
					$searchRsp->minimo = $searchInfo->minimo;
					$searchRsp->hashh = $lastHash;
					$searchRsp->maximo = $searchInfo->masBaratoMasCaro;
					$searchRsp->cantResults = $searchInfo->cantResults;
					
					$searchRsp->providers = $searchInfo->providers;
					$searchRsp->filtro = 0;
					$searchRsp->hotelViews = $this->getHotels(0,9, false,$req->input('sid'));
					
					$searchRsp->codConsumiciones = $searchInfo->codConsumiciones;
					
					return json_encode($searchRsp);
				}
			}
			usleep(100);
			
		}
	}
	
	
	
	public function doSearch(Request $req){
		Session::put('sid',$req->input('sid'));
		$iibReq = new \StdClass;
		$datosLoggeo = Session::get('datos-loggeo');
		$iibReq->token = $datosLoggeo->token;
		$datosBusqueda = new \StdClass;
		$iibReq->codDestino = $req->input('destino');
		
		$fechaDesde = explode("/",$req->input('fecha_desde'));
		$iibReq->fechaDesde = $fechaDesde[1]."/".$fechaDesde[0]."/".$fechaDesde[2];
		
		$fechaHasta = explode("/",$req->input('fecha_hasta'));
		$iibReq->fechaHasta = $fechaHasta[1]."/".$fechaHasta[0]."/".$fechaHasta[2];
		$iibReq->ocupanciasDtp = json_decode($req->input('ocupancia'));
		$datosUsuario = new \StdClass;
		$datosUsuario->idUsuario = $datosLoggeo->datos->datosUsuarios->idUsuario;
		$datosUsuario->idAgencia = $datosLoggeo->datos->datosUsuarios->idAgencia;
		$iibReq->idProveedor = $req->input('provId');
		$iibReq->searchId = $req->input('sid');

		$logger = new Logger('Log');
		$logger->pushHandler(new \Monolog\Handler\StreamHandler(storage_path() . '/log/'. 'hotelesSearch' . '.log'), Logger::DEBUG);
		
		$stack = HandlerStack::create();
		$stack->push(Middleware::log($logger, new MessageFormatter(
					'{method} {uri} HTTP/{version} FIRST REQUEST: {req_body}'. 'RESPONSE: {code} - {res_body}'
		)));
		$client = new Client([
			'defaults' => [
				'config' => [
					'curl' => [
						CURLMOPT_MAX_TOTAL_CONNECTIONS => 100,
						'body_as_string' => true
						//CURLOPT_TCP_NODELAY => 1
					]
				]
			],
			'handler' => $stack
		]);
		
		
		$iibRsp = $client->post(Config::get('config.iibSearch'), [
			'json' => $iibReq
		]);
		
		$iibObjRsp = json_decode($iibRsp->getBody());
		if($iibObjRsp->codRetorno != 0) return -1;
		$hotels = $iibObjRsp->hoteles;
		$this->getSearchInfo($hotels,false,false,$req->input('sid'));
		return "ok, por lo menos el prov que respondio ultimo";
	}

	//HAY QUE APLICAR UN LOCK A ESTA FUNCION
	private function getSearchInfo($hotels, $wFilter,$sort, $sid){
		if(!$wFilter){
			$previousHotels = [];
			$previousSearchInfo = $this->waitUntilRead($sid);
			try{
				$previousHotels = $previousSearchInfo->hotels;
			}catch(\Exception $e){
				
			}
			if(empty($hotels)){
				if($sort){
					if(!empty($previousSearchInfo->hotels))usort($previousSearchInfo->hotels,array( $this, 'hotelComparator' ));
					$this->waitUntilWrite($previousSearchInfo, $sid);
				}
				return $previousSearchInfo;
			}
			$hotels = array_merge((array) $hotels,(array)$previousHotels);
		}
		
		$cantResults = 0;
		$comMasBarata = 0;
		$comMasCara = 0;
		$masBaratoMasCaro = 0;	//*
		$providers = array();
		$codConsumiciones = [];
		foreach($hotels as $key=>$hotel){
			$hotel->comMasCostosaHotel = 0;
			if ($hotel === reset($hotels)){
				$comMasBarata = $hotel->comMasEconomicaHotel;
				$comMasCara = $hotel->comMasCostosaHotel;
			}
			if(!array_key_exists($hotel->codProveedor,$providers)) $providers[$hotel->codProveedor] = 1;
			else $providers[$hotel->codProveedor]++;
			if(!property_exists($hotel,'catHotel')) $hotel->catHotel = 0;
			if($this->intParsed($hotel->catHotel) != -1) $hotel->catHotel = $this->intParsed($hotel->catHotel);
			if($hotel->comMasEconomicaHotel < $comMasBarata) $comMasBarata = $hotel->comMasEconomicaHotel;
			if($hotel->comMasCostosaHotel > $comMasCara) $comMasCara = $hotel->comMasCostosaHotel;
			if($hotel->comMasEconomicaHotel > $masBaratoMasCaro) $masBaratoMasCaro = $hotel->comMasEconomicaHotel;
			foreach($hotel->codConsumiciones as $hCodCon ){
				if(!in_array($hCodCon,$codConsumiciones)) $codConsumiciones[] = $hCodCon;
			}
			$cantResults++;
		}
		$searchInfo = new \StdClass;
		$searchInfo->minimo = floor($comMasBarata);
		$searchInfo->maximo = ceil($comMasCara);
		$searchInfo->masBaratoMasCaro = ceil($masBaratoMasCaro);
		$searchInfo->cantResults = $cantResults;
		$searchInfo->providers = $providers;
		$searchInfo->codConsumiciones = $codConsumiciones;
		if($sort){
			if(!empty($hotels)) usort($hotels,array( $this, 'hotelComparator'));
		}
		$searchInfo->hotels = $hotels;
		if(!$wFilter)$this->waitUntilWrite($searchInfo, $sid);
		return $searchInfo;
	}
	
	private function waitUntilWrite($searchInfo, $sid){
		$text = json_encode($searchInfo);
		
		$filename = 'searchids/' . $sid . '/lock.txt';
		$dirname = dirname($filename);
		if (!is_dir($dirname))
		{
			mkdir($dirname, 0755, true);
		}
		
		$fp = fopen($filename,'w');
		$hashh = bin2hex(openssl_random_pseudo_bytes(5));
		
		$hashFilename = 'searchids/' . $sid . '/hash/'.$hashh;
		$hashDirname = dirname($hashFilename);
		if (!is_dir($hashDirname))
		{
			mkdir($hashDirname, 0755, true);
		}
		else{
			$files = glob('searchids/' . $sid . '/hash/*'); // get all file names
			foreach($files as $file){ // iterate files
			  if(is_file($file))
				unlink($file); // delete file
			}
		}
		
		$fpp = fopen('searchids/' . $sid ."/hash/". $hashh, "w");
		fclose($fpp);
		while(!flock($fp, LOCK_EX)) {  // acquire an exclusive lock
			
		}
		fwrite($fp, $text);
		fflush($fp);            
		flock($fp, LOCK_UN);
		fclose($fp);
	}
	private function waitUntilRead($sid){
		$fileName = 'searchids/' . $sid . '/lock.txt';
		if(!file_exists($fileName)){
			return null;
		}
		$fp = fopen($fileName, "r");
		
		while(!flock($fp, LOCK_EX)) {  
		}
		$oldSearchInfo;
		$oldSearchInfo = fgets($fp);
		if($oldSearchInfo == '') $oldSearchInfo = '{}';
		flock($fp, LOCK_UN);    // release the lock
		fclose($fp);
		return json_decode($oldSearchInfo);
	}	
	
	public function paginar(Request $req){
		$searchRsp = new \StdClass;
		$searchRsp->codRetorno = 0;
		$searchRsp->desRetorno = "PAGINACION EXITOSA";
		$searchInfo = $this->waitUntilRead($req->input('sid'));
		$searchRsp->minimo = $searchInfo->minimo;
		$searchRsp->maximo = $searchInfo->maximo;
		$searchRsp->maximo = $searchInfo->masBaratoMasCaro;
		$searchRsp->cantResults = $searchInfo->cantResults;
		$searchRsp->filtro = $req->has('filter');
		$searchRsp->hotelViews = $this->getHotels($req->input('from'), $req->input('to'),$req->input('filter'),$req->input('sid'));
		return json_encode($searchRsp);
	}
	
	public function doFiltro(Request $req){
		$req->session()->put('filterInfo', null);
		$hoteles = $this->waitUntilRead($req->input('sid'))->hotels;
		$filteredHotels = array();
		if(false){
		}
		else{
			foreach($hoteles as $hotel){
				if($req->has('proveedores')){
					if(!in_array($hotel->codProveedor,$req->input('proveedores'))) continue;
				}
				if($req->has('estrellas')){
					$cat = substr($hotel->catHotel,0,1);
					$sePidioPorOtros = in_array(-1,$req->input('estrellas'));
					if( (!in_array($cat, $req->input('estrellas')) ) &&   
							 (   (!$sePidioPorOtros )   || (  ($sePidioPorOtros) && !$this->isOtherStar($cat)  ))        ) continue;
				}
				if($req->has('consumiciones')){
					$clonedHotel = clone $hotel;
					$clonedHotel->habitaciones = [];
					foreach($hotel->habitaciones as $habitacion){
						foreach($habitacion->tarifas as $tarifa){
							if(in_array($tarifa->codConsumicion,$req->input('consumiciones'))){
								$clonedHotel->habitaciones[] = clone $habitacion;
								$clonedHotel->habitaciones[count($clonedHotel->habitaciones)-1]->tarifas = [];
								$clonedHotel->habitaciones[count($clonedHotel->habitaciones)-1]->tarifas[] = clone $tarifa;
							}
						}
					}
					if(empty($clonedHotel->habitaciones)) continue;
				}
				if($req->has('precio_minimo')){
					if($hotel->comMasEconomicaHotel <= $req->input('precio_minimo')) continue;
				}
				if($req->has('precio_maximo')){
					if(    $hotel->comMasEconomicaHotel >= $req->input('precio_maximo')) continue;
				}
				if($req->has('sugeridos')){
					if(!$hotel->sugerido) continue;
				}
				if($req->has('promociones')){
					if(!$hotel->promocion) continue;
				}
				if($req->has('hotel') && trim($req->input('hotel')) != ''){
					if(  !str_contains(  strtolower($hotel->nomHotel),strtolower($req->input('hotel')))  ) continue;
				}
				
				$filteredHotels[] = $req->has('consumiciones') ? $clonedHotel : clone $hotel;
			}
			$filterInfo = $this->getSearchInfo($filteredHotels,true,true,$req->input('sid'));
			$req->session()->put('filterInfo',$filterInfo);
			
			$searchRsp = new \StdClass;
			$searchRsp->codRetorno = 0;
			$searchRsp->desRetorno = '';
		
			$searchRsp->minimo = $filterInfo->minimo;
			//$searchRsp->maximo = $filterInfo->maximo;
			$searchRsp->maximo = $filterInfo->masBaratoMasCaro;
			$searchRsp->cantResults = $filterInfo->cantResults;
			
			$searchRsp->hotelViews = $this->getHotels(0, 9,true,$req->input('sid'));
		}
		

		return json_encode($searchRsp);
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		public function doBack(Request $req){

			$hoteles = $this->waitUntilRead($req->input('sid'))->hotels;
			$proveedor  = $this->waitUntilRead($req->input('sid'))->providers;
			$codConsumiciones = $this->waitUntilRead($req->input('sid'))->codConsumiciones;
			$filteredHotels = array();
			if(false){
			}
			else{
				foreach($hoteles as $hotel){
					if($req->has('proveedores')){
						if(!in_array($hotel->codProveedor,$req->input('proveedores'))) continue;
					}
					if($req->has('estrellas')){
						$cat = substr($hotel->catHotel,0,1);
						$sePidioPorOtros = in_array(-1,$req->input('estrellas'));
						if( (!in_array($cat, $req->input('estrellas')) ) &&   
								 (   (!$sePidioPorOtros )   || (  ($sePidioPorOtros) && !$this->isOtherStar($cat)  ))        ) continue;
					}
					if($req->has('consumiciones')){
						$clonedHotel = clone $hotel;
						$clonedHotel->habitaciones = [];
						foreach($hotel->habitaciones as $habitacion){
							foreach($habitacion->tarifas as $tarifa){
								if(in_array($tarifa->codConsumicion,$req->input('consumiciones'))){
									$clonedHotel->habitaciones[] = clone $habitacion;
									$clonedHotel->habitaciones[count($clonedHotel->habitaciones)-1]->tarifas = [];
									$clonedHotel->habitaciones[count($clonedHotel->habitaciones)-1]->tarifas[] = clone $tarifa;
								}
							}
						}
						if(empty($clonedHotel->habitaciones)) continue;
					}
					if($req->has('precio_minimo')){
						if($hotel->comMasEconomicaHotel <= $req->input('precio_minimo')) continue;
					}
					if($req->has('precio_maximo')){
						if(    $hotel->comMasEconomicaHotel >= $req->input('precio_maximo')) continue;
					}
					if($req->has('sugeridos')){
						if(!$hotel->sugerido) continue;
					}
					if($req->has('promociones')){
						if(!$hotel->promocion) continue;
					}
					if($req->has('hotel') && trim($req->input('hotel')) != ''){
						if(  !str_contains(  strtolower($hotel->nomHotel),strtolower($req->input('hotel')))  ) continue;
					}
					
					$filteredHotels[] = $req->has('consumiciones') ? $clonedHotel : clone $hotel;
				}
				$filterInfo = $this->getSearchInfo($filteredHotels,true,true,$req->input('sid'));
				$req->session()->put('filterInfo',$filterInfo);
				
				$searchRsp = new \StdClass;
				$searchRsp->codRetorno = 0;
				$searchRsp->desRetorno = '';
				$searchRsp->minimo = $filterInfo->minimo;
				$searchRsp->maximo = $filterInfo->masBaratoMasCaro;
				$searchRsp->cantResults = $filterInfo->cantResults;
				$searchRsp->providers = $proveedor;
				$searchRsp->codConsumiciones = $codConsumiciones;
				$searchRsp->hotelViews = $this->getHotels(0, 9,true,$req->input('sid'));
			}
			
			return json_encode($searchRsp);
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	private function getHotels($from, $to, $hasFiltro,$sid){
		$searchInfoForView;
		if($hasFiltro){
			$searchInfoForView = \Session::get('filterInfo');
			
		}else{
			$searchInfoForView = $this->waitUntilRead($sid);
		}
		$hotelViews = array();
		for($k = $from ; $k<=$to ; $k++){
			if(array_key_exists($k,$searchInfoForView->hotels)){
				$searchInfoForView->hotels[$k]->hotelIndex = $k;		
				$hotelViews[] = preg_replace('/\s+/', ' ',(string) View::make('partials.singleResult')->with(['hotel'=>$searchInfoForView->hotels[$k], 'sid'=>$sid]));
			}
		}
		return $hotelViews;
	}

	//maneja los requests con filtro o recien removido
	private function isOtherStar($cat){
		if( in_array($cat, [1,2,3,4,5] ) )  return false;
		return true;
	}
	public function isHotelInSession($hotel, $hotelsSession){
		$is = false;
		if($hotelsSession == null) return $is;
		foreach($hotelsSession as $hs){
			if($hs->codHotel ==  $hotel->codHotel){
				$is=true;
				break;
			}
		}
		return $is;
	}
	
	private function occsToIibFormat($occs){
		$ocupancia = array();
		foreach($occs as $occ){
			$ocupancia[] = $this->occToIibFormat($occ);
		}
		return $ocupancia;
	}
	
	private function occToIibFormat($occ){
		$oc = new \StdClass;
		if(!empty($occ->edadesNinhos)){
		}else{
		}
		$detalle = array();
		for($k=0; $k< $occ->adultos; $k++){
			$d = new \StdClass;
			$d->tipo = "AD";
			$detalle[]= $d;
		}
		if($occ->edadesNinhos != null){
			for($k=0 ; $k < count($occ->edadesNinhos) ; $k++){
				$d = new \StdClass;
				$d->tipo = "CH";
				$d->edad = $occ->edadesNinhos[$k];
				$detalle[] = $d;
			}
		}
		$oc->detalle = $detalle;
		return $detalle;
	}
	//segundo pedido

	private function intParsed($try){
		if(ctype_digit($try)) return $try;
		if(ctype_digit($try[0])){
			$hasDecimalSeparator = (($try[1] == ".") or ($try[1] == ","));
			if($hasDecimalSeparator){
				if($ctype_digit(substr($try,0,3))) return substr($try,0,3);
			}else return $try[0];
		}
		return -1;
	}
	
	private function getSelectedHotel($identifier,$sid){
		$hoteles = $this->waitUntilRead($sid)->hotels;
		foreach($hoteles as $hotel){
			if($hotel->identifier == $identifier){
				return $hotel;
			}
		}
	}
	
	private function getSelectedOccupancy($hotel, $claveTarifa){
		//itera por las rates hasta encontrar la que corresponde, ahi retorna la ocupancia de esa rate
		foreach($hotel->habitaciones as $habitacion){
			foreach($habitacion->tarifas as $tarifa){
				if($tarifa->claveTarifa == $claveTarifa){
					$oc = new \StdClass;
					$oc->adultos= (int)$tarifa->adultos;
					$oc->edadesNinhos = [];
					foreach($tarifa->edadNinos as $edad){
						$oc->edadesNinhos[] = (int) $edad;
					}
					return $oc;
				}
			}
		}
	}
	
	public function checkForPreReserva(Request $req){
		return json_encode($this->allowPreReserva($req->input('rates'),$req->input('sid')));
	}

	private function allowPreReserva($rates,$sid){
		Session::put('selectedHotel',null);
		$ocsSearched = Session::get('ocupanciasDtp');
		foreach($ocsSearched as $ocSearched){
			$ocSearched->matched=false;
		}
		
		$ocs = [];
		$hotel = $this->getSelectedHotel(explode(':',$rates[0])[0],$sid);
		
		$selectedHotel = json_decode(json_encode($hotel));
		$selectedHotel->habitaciones = [];
		foreach($rates as $rate){
			$codReserva= explode(':',$rate)[1];
			foreach($hotel->habitaciones as $habitacion){
				$newHabitacion = json_decode(json_encode($habitacion));
				$newHabitacion->tarifas = [];//+
				foreach($habitacion->tarifas as $tarifa){				
					if($tarifa->codReserva== $codReserva){
						$newTarifa = json_decode(json_encode($tarifa));
						$ocs[] = $newTarifa->ocupanciaDtp;
						$newHabitacion->tarifas[] = $newTarifa;		//su unica tarifa
						break;
					}
				}
				if(!empty($newHabitacion->tarifas)){
					$selectedHotel->habitaciones[] = $newHabitacion;
					break;
				}
				
			}
		}
		$nMatched =0;
		foreach($ocs as $oc){
			foreach($ocsSearched as $ocSearched){
				//si ya se encontro una ocupancia para la ocupancia buscada, se deja de buscar para ella.
				if(property_exists($ocSearched,'matched') && $ocSearched->matched) continue;
				else if($this->occsMatch($oc,$ocSearched)){
					$nMatched++;
					$ocSearched->matched = true;
					break;
				}
				
			}
		}
		foreach($ocsSearched as $ocSearched){
			if($ocSearched->matched == false) return false;
		}
		if($nMatched > 0 && $nMatched == count($ocs)){
			Session::put('selectedHotel',$selectedHotel);
			return true;
		}
		
		return false;
	}
	
	private function occsMatch($midOc, $trueOc){
		if($midOc->adultos == $trueOc->adultos ){
			if($midOc->edadesNinhos==$trueOc->edadesNinhos) return true;
			if(count($midOc->edadesNinhos) == count($trueOc->edadesNinhos)){
				if(count($midOc->edadesNinhos)==0) return true;
				for($k=0 ; $k< count($midOc->edadesNinhos) ; $k++){
					if( ($midOc->edadesNinhos)[$k] != -1) return false;
					if($k == count($midOc->edadesNinhos) - 1 )return true;
				}
			}
			else return false;
		}
		return false;
	}

	private function hotelComparator($o1, $o2){
		
		if($o1->sugerido xor $o2->sugerido){
			if($o1->sugerido) return -1;
			else return 1;
		}
		else if($o1->promocion xor $o2->promocion){
			if($o1->promocion) return -1;
			else return 1;
		}
		else if($o1->comMasEconomicaHotel <= $o2->comMasEconomicaHotel) return -1;
		else return 1;
	}

	/*
	Página principal de busqueda de vuelos
	*/
	public function searchFlight(Request $req){
		$req->session()->put('ultimaReserva', null);
		include("../destinosVuelos.php");
		$origenBruto = $aeropuerto[$req->input('origen')];
		$opcionOrigen =  explode(",",$origenBruto);
		$destinoBruto = $aeropuerto[$req->input('destino')];
		$opcionDestino =  explode(",",$destinoBruto);
		return view('pages.vuelos.searchFlight')->with(['inputs'=>$req->input(), 'origenF'=>ucwords(strtolower($opcionOrigen[0]).", ".strtolower($opcionOrigen[3])), 'destinoF'=>ucwords(strtolower($opcionDestino[0]).", ".strtolower($opcionDestino[3]))]);
	}

	private function getDestinationsFligth(){
        include("../destinationFligth.php");
        $listadoDestino = [];
        foreach($destination as $key=>$resultado){
        	$listadoDestino[$key]['value'] = $key;
			$listadoDestino[$key]['label'] = ucwords(strtolower($resultado));
        }
        $listadoDestino = json_encode($listadoDestino);
        return $listadoDestino;
    }    
	/*
		Función Get de Busqueda de los Vuelos
	*/
	public function doFlightSearch(Request $req){
		$req->session()->put('ref',0);
		$req->session()->put('token', $this->getId4Log());
		$paxReferences = [];
		$guardarBusqueda = new \StdClass;
		$fechaIda = explode("/",$req->input('fecha_ida_vuelo'));
		$guardarBusqueda->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$guardarBusqueda->fecha_checkin = $fechaIda[2]."-".$fechaIda[1]."-".$fechaIda[0];
		$req->session()->put('fechaIda', $req->input('fecha_ida_vuelo'));
		$dateIn = $fechaIda[0]."".$fechaIda[1]."".substr($fechaIda[2], -2);
		$req->dateIn = $fechaIda[0]."-".$fechaIda[1]."-".substr($fechaIda[2], -2);
		if(!empty($req->input('fecha_vuelta_vuelo'))){
			$fechaVuelta = explode("/",$req->input('fecha_vuelta_vuelo'));
			$guardarBusqueda->fecha_checkout =  $fechaVuelta[2]."-".$fechaVuelta[1]."-".$fechaVuelta[1];
			$dateOut = $fechaVuelta[0]."".$fechaVuelta[1]."".substr($fechaVuelta[2], -2); 
			$req->dateOut = $fechaVuelta[0]."-".$fechaVuelta[1]."-".substr($fechaVuelta[2], -2);
			$req->session()->put('fechaVuelta', $req->input('fecha_vuelta_vuelo'));
		}else{
			$req->session()->put('fechaVuelta', $req->input('fecha_ida_vuelo'));
		}	
		$iibReq = new \StdClass;
		$iibReq->token = $req->session()->get('token');
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json',
						   'Cache-Control'=>'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
						   ]
		]);
		$guardarBusqueda->id_origen = $req->input('origen');
		$guardarBusqueda->id_destino = $req->input('destino');
		$guardarBusqueda->ocupancia = "ADT =".$req->input('cantidad_adultos')."| CHD =".$req->input('cantidadN')."| INF =".$req->input('cantidad_infantes');

		$totalUnidades = $req->input('cantidad_adultos') + $req->input('cantidadN');
		$iibReq = new \StdClass; 
		$details =  new \StdClass;
		$numberOfUnit = new \StdClass;
		$unitNumberDetail= array();
		$numberOfUnit = new \StdClass;

		///Preparando para el foreach de numberOfUnit
		for($k=0; $k<2; $k++){
			if($k == 0){
				$numberOfUnits ="200";
				$typeOfUnit ="RC";
			}
			if($k == 1){
				$numberOfUnits =$totalUnidades;
				$typeOfUnit ="PX";
			}
			$d = new \StdClass;
			$d->numberOfUnits = $numberOfUnits;
			$d->typeOfUnit = $typeOfUnit;
			$unitNumberDetail[$k] = $d;
		}	
		///Fin Preparando para el foreachde numberOfUnit
		$iibReq->token = $req->session()->get('token');
		$numberOfUnit->unitNumberDetail = $unitNumberDetail;
		$iibReq->numberOfUnit = $numberOfUnit;

		$paxReference = array();
		$traveller = array();	
		$pReference = new \StdClass;
		$travel = new \StdClass;
		$resultado = [];
		if($req->input('cantidad_adultos')!= 0){
			$resultado[] = $this->getPaxReference($req->input('cantidad_adultos'), "ADT");
		}
		if($req->input('cantidadN')!= 0){
			$resultado[] = $this->getPaxReference($req->input('cantidadN'), "CHD");
		}
		if($req->input('cantidad_infantes')!= 0){
			$resultado[]= $this->getPaxReference($req->input('cantidad_infantes'),"INF");
		}
			
		$iibReq->paxReference = $resultado;

		$pricingTicketing = new \StdClass;
		$priceValue = array();
		///Preparando para el foreach de priceType
		for($x=0; $x<5; $x++){	
			switch ($x) {
				case 0:	
					$priceValue[$x] = "ET"/*Electronic Ticket Only*/;
					break;
				case 1:	
					$priceValue[$x] = "RU"/*Unifares*/;
					break;		
				case 2:	
					$priceValue[$x] = "TAC"/*Ticket ability check*/;
					break;	
				case 3:	
					$priceValue[$x] = "RP"/*Published fares*/;
					break;	
				/*case 4:	
					$priceValue[$x] = "NSD"/*Disable slice and dice process*/;
				//break;	*/
				case 4:	
					$priceValue[$x] = "BD"/*Disable slice and dice process*/;
					break;		
			}			
		}	

		$pricingTicketing->priceType =$priceValue;
		$pricingTickInfo = new \StdClass;;
		$pricingTickInfo->pricingTicketing = $pricingTicketing;
		$fareOptions = new \StdClass;
		$fareOptions->pricingTickInfo = $pricingTickInfo;
		$iibReq->fareOptions = $fareOptions;

		$itinerary = array();
		$segRef = $req->input('tipo');
		$locationOrigen = $req->input('origen');
		$locationDestiny = $req->input('destino');

		$req->session()->put('origen', $req->input('origen'));
		$req->session()->put('destino', $req->input('destino'));

		switch ($segRef) {
			case "idayvuelta":
				$limite = 2;
				break;
			case "ida":
				$limite = 1;
				break;
			case "multidestino":
				$limite = 0;
				break;
		}
		$req->session()->put('tipo', $limite);
		for($x=0; $x<$limite; $x++){
			$seg = $x + 1;
			if($seg == 1){
				$departPoint = $locationOrigen;
				$arrivalPoint= $locationDestiny;
				$dateDetail= $dateIn;
			}elseif($seg == 2){
				$departPoint = $locationDestiny;
				$arrivalPoint= $locationOrigen;
				$dateDetail= $dateOut;
			}
			$fligthItnerary[$x] = $this->getItinerary($seg, $departPoint, $arrivalPoint, $dateDetail);
		}
		$itinerary = $fligthItnerary;
		$iibReq->itinerary = $itinerary;

		if($req->input('first') != null){//if es primer pedido...
			$req->session()->put('initCantResults', 0);
			$req->session()->put('fligthSession',null);
			$req->session()->put('firstIibReq',$iibReq);
			$req->session()->put('itinerary',$itinerary);

		}else{
			if($req->input('filtro') == 0){
				$filtro = 0;
			}else{
				$filtro = 1;
			}
			$searchRsp = new \StdClass;
			$searchRsp->codRetorno = 0;
			$searchRsp->desRetorno = 'Procesado exitosamente';
			$searchRsp->fligthViews = $this->getFligthFiltro($req->input('from'),$req->input('to'),$req,$filtro);
			return json_encode($searchRsp);			
	
		}
		try{
			$iibRsp = $client->post(Config::get('config.iibFligthSearch'),
						[
							'body' => json_encode($iibReq),
							]
				);
		}	
		catch(RequestException $e){
			//return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			//return view('pages.errorConexion');	
		}	
		$iibObjRsp = json_decode($iibRsp->getBody());
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));*/
		$req->session()->put('iibObjRsp', $iibObjRsp);
		$fligthSession = array();
		$searchRsp = new \StdClass;
		$searchRsp->codRetorno = 0;
		$searchRsp->desRetorno = 'Procesado exitosamente';
		$resultsPerProvider = array();
		$initCantResults =0;
		$filtrosSession = [];
		$precio = [];
		include("../comAerea.php");
		foreach($iibObjRsp->recommendation as $index=>$fligth){
			$initCantResults++;
			$fligthSession[] = $fligth;
			$precio[] = $fligth->recPriceInfo->monetaryDetail[0]->amount;
			foreach($fligth->paxFareProduct as $baseIndex=>$paxFareProduct){
				if(isset($comAerea[$paxFareProduct->paxFareDetail->codeShareDetails[0]->company])){
					$compañiaAerea= explode("|",$comAerea[$paxFareProduct->paxFareDetail->codeShareDetails[0]->company]);
					$flightCompany= $compañiaAerea[0];
				}else{
					$flightCompany= "No disponible";
				}

				$filtrosSession[$paxFareProduct->paxFareDetail->codeShareDetails[0]->company] = $flightCompany;
			}
		}
		Session::put('precio', $precio);
		if(isset($iibObjRsp->serviceFeesGrp[0])){
			$req->session()->put('baggage', $iibObjRsp->serviceFeesGrp[0]);
		}else{
			$req->session()->put('baggage', '');
		}
		$req->session()->put('initCantResults', $initCantResults);
		$req->session()->put('fligthSession',$fligthSession);
		$req->session()->put('fligthIndex',$iibObjRsp->flightIndex);
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp->flightIndex));*/
		$searchRsp->fligthViews = $this->getFligth($req->input('from'),$req->input('to'),$req);
		if($req->input('first') != null){
			$this->getCargaFinal($req);
		}	
		if(Session::get('precio') !=null){
			$searchRsp->maxPrecio = max(Session::get('precio'));
			$searchRsp->minPrecio = min(Session::get('precio'));
		}else{
			$searchRsp->maxPrecio =	0;
			$searchRsp->minPrecio = 0;
		}
		$this->guardarBusqueda($guardarBusqueda);	
		return json_encode($searchRsp);
	}
	private function getCargaFinal($req){
		$fligthSession = $req->session()->get('fligthSession');
		$cantidad=count($fligthSession);
		$inicio = $req->input('to');
		for($k = $inicio; $k<=$cantidad ; $k++){
			if(isset($fligthSession[$k])){
				foreach($fligthSession[$k]->segmentFlightRef as $index=>$referencial){
					$segmento = 0;
					$bagage = 0;
					$refNumber = "";
					$fechaAbordajeSegmento = "";
					$horaAbordajeSegmento = "";
					foreach($referencial->referencingDetail as $key=>$valor){
						if($valor->refQualifier =="S"){
							if($segmento == 0){
								$segmento++;
								$refNumber.= "".$segmento."|".$valor->refNumber;
							}else{
								$segmento++;
								$refNumber.= "||".$segmento."|".$valor->refNumber;
							}

							$detalles[$key] = $this->fligthSegment($k, $valor->refQualifier,$valor->refNumber,$segmento,$fligthSession[$k]->paxFareProduct[0]->fareDetails[$key]->groupOfFares);

							$clase[$fligthSession[$k]->paxFareProduct[0]->fareDetails[$key]->groupOfFares[0]->productInformation->cabinProduct->rbd] = $detalles[$key]->flightDetails[0]->flightInformation->tipoAsiento;

							$detallesArray = $detalles[$key];
							$contadorSegmento= 0;
							$totalHoras = "";
							foreach($detallesArray->flightDetails as $base=>$detArray){
								if($contadorSegmento == 0){
									$fechaAbordaje = $detArray->flightInformation->productDateTime->dateOfDeparture;
									$horaAbordaje = $detArray->flightInformation->productDateTime->timeOfDeparture;
									$fechaLlegada = $detArray->flightInformation->productDateTime->dateOfArrival;	
									$horaLlegada = $detArray->flightInformation->productDateTime->timeOfArrival;
									if($base == 0){
										$fechaAbordajeSegmento = $fechaAbordaje;
										$horaAbordajeSegmento = $horaAbordaje;
									}
								}else{
									$fechaLlegada = $detArray->flightInformation->productDateTime->dateOfArrival;	
									$horaLlegada = $detArray->flightInformation->productDateTime->timeOfArrival;
								}
								$contadorSegmento++;
							}
				
							$detalles[$key]->totalHoras = $detalles[$key]->propFlightGrDetail->flightProposal[1]->ref;
							$detalles[$key]->cantidad = $contadorSegmento;
							$escalas[$detalles[$key]->cantidad] = $detalles[$key]->cantidad - 1;
							$detalles[$key]->dateDeparture = date_create_from_format('d-m-y',$fechaAbordaje);
							$detalles[$key]->timeDeparture = $horaAbordaje;
							$detalles[$key]->dateArrival = date_create_from_format('d-m-y',$fechaLlegada);
							$detalles[$key]->timeArrival = $horaLlegada;
						}else{
							$bagage = $valor->refNumber;
						}
					}
				}
				foreach($detalles as $ichi=>$detalle){
					foreach($detalle->flightDetails as $ichi1=>$flightDetails){
						$marketingCompany = explode('/',$flightDetails->flightInformation->aerolinea);
						$filtrosSession[$flightDetails->flightInformation->companyId->marketingCarrier] = $marketingCompany[0];
					}
				}
				$itinerario = Session::get('itinerary');
				$fechaInVuelo = explode('/', $req->input('fecha_ida_vuelo'));
				$fechaIns = $fechaInVuelo[0]."-".$fechaInVuelo[1]."-".date('y', $fechaInVuelo[2]);		
				$fechaOutVuelo = explode('/', $req->input('fecha_vuelta_vuelo'));
				$fechaOuts = $fechaOutVuelo[0]."-".$fechaOutVuelo[1]."-".date('y', $fechaOutVuelo[2]);

				$fechaIn = date_create_from_format('d-m-y', $fechaIns);
				$fechaOut = date_create_from_format('d-m-y', $fechaOuts);

				include("../destinationFligth.php");
				include("../destinosVuelos.php");
				if(isset($destination[$req->input('origen')])){
					$origenBase = $destination[$itinerario[0]->departureLocalization->departurePoint->locationId];
				}else{
					$origenes = $aeropuerto[$itinerario[0]->departureLocalization->departurePoint->locationId];
					$origenExplode = explode(",", $origenes);
					$destinoBase = $origenExplode[2].",".$origenExplode[3];
				}
				$origen = explode(",", $origenBase);
				if(isset($destination[$req->input('destino')])){
					$destinoBase = $destination[$itinerario[0]->arrivalLocalization->arrivalPointDetails->locationId];
				}else{
					$destinos = $aeropuerto[$itinerario[0]->arrivalLocalization->arrivalPointDetails->locationId];
					$destinoExplode = explode(",", $destinos);
					$destinoBase = $destinoExplode[2].",".$destinoExplode[3];
				}	
				$destino = explode(",", $destinoBase);

				$fligthSession[$k]->fligthIndex = $k;
				$fligthSession[$k]->bagage = $bagage;
				$fligthSession[$k]->origen = $itinerario[0]->departureLocalization->departurePoint->locationId;
				$fligthSession[$k]->origen_aeropuerto = "";
				$fligthSession[$k]->origen_localidad = ucwords(strtolower($origen[0]));
				$fligthSession[$k]->pais_origen = ucwords(strtolower($origen[1]));
				$fligthSession[$k]->destino = $itinerario[0]->arrivalLocalization->arrivalPointDetails->locationId;
				$fligthSession[$k]->destino_aeropuerto = "";
				$fligthSession[$k]->destino_localidad = ucwords(strtolower($destino[0]));
				$fligthSession[$k]->pais_destino = ucwords(strtolower($destino[1]));
				$fligthSession[$k]->dateIn = date_format($fechaIn, 'j M');
				$fligthSession[$k]->dateOut = date_format($fechaOut, 'j M');
				$fligthSession[$k]->segmentos= $refNumber;
				$fligthSession[$k]->cantAdultos= $req->input('cantidad_adultos');
				$fligthSession[$k]->cantNinhos= $req->input('cantidadN');
				$fligthSession[$k]->cantInfante= $req->input('cantidad_infantes');
				$fligthSession[$k]->detalleBtn = $detalles;
			} 
			else break;
		}	
		if(isset($escalas)){
			Session::put('escalas', $escalas);
		}
		if(isset($filtrosSession)){
			$req->session()->put('filtro', $filtrosSession);
		}
		if(isset($fligthSession)){
			$req->session()->put('restFligth', $fligthSession);
		}
	}
	private function getFligth($from, $to, $req){
		$fligthViews = array();
		$detalles = [];
		$fligthGetFilgth = [];
		$fligthSession = $req->session()->get('fligthSession');
		$memory = [];
		for($k = $from ; $k<=$to ; $k++){
			if(isset($fligthSession[$k])){
				foreach($fligthSession[$k]->segmentFlightRef as $index=>$referencial){
					$segmento = 0;
					$bagage = 0;
					$refNumber = "";
					$fechaAbordajeSegmento = "";
					$horaAbordajeSegmento = "";
					foreach($referencial->referencingDetail as $key=>$valor){
						if($valor->refQualifier =="S"){
							if($segmento == 0){
								$segmento++;
								$refNumber.= "".$segmento."|".$valor->refNumber;
							}else{
								$segmento++;
								$refNumber.= "||".$segmento."|".$valor->refNumber;
							}
							/*echo '<pre>';
							print_r(json_encode($fligthSession[$k]));*/
							$detalles[$key] = $this->fligthSegment($k, $valor->refQualifier,$valor->refNumber,$segmento,$fligthSession[$k]->paxFareProduct[0]->fareDetails[$key]->groupOfFares);
							$detallesArray = $detalles[$key];
							$contadorSegmento= 0;
							$totalHoras = "";
							//die;
							foreach($detallesArray->flightDetails as $base=>$detArray){
								if($contadorSegmento == 0){
									$fechaAbordaje = $detArray->flightInformation->productDateTime->dateOfDeparture;
									$horaAbordaje = $detArray->flightInformation->productDateTime->timeOfDeparture;
									$fechaLlegada = $detArray->flightInformation->productDateTime->dateOfArrival;	
									$horaLlegada = $detArray->flightInformation->productDateTime->timeOfArrival;
									if($base == 0){
										$fechaAbordajeSegmento = $fechaAbordaje;
										$horaAbordajeSegmento = $horaAbordaje;
									}
								}else{
									$fechaLlegada = $detArray->flightInformation->productDateTime->dateOfArrival;	
									$horaLlegada = $detArray->flightInformation->productDateTime->timeOfArrival;
								}
								$contadorSegmento++;
							}
							$detalles[$key]->totalHoras = $detalles[$key]->propFlightGrDetail->flightProposal[1]->ref;
							$detalles[$key]->cantidad = $contadorSegmento;
							$escalas[$detalles[$key]->cantidad] = $detalles[$key]->cantidad-1;
							$detalles[$key]->dateDeparture = date_create_from_format('d-m-y',$fechaAbordaje);
							$detalles[$key]->timeDeparture = $horaAbordaje;
							$detalles[$key]->dateArrival = date_create_from_format('d-m-y',$fechaLlegada);
							$detalles[$key]->timeArrival = $horaLlegada;
						}else{
							$bagage = $valor->refNumber;
						}
					}
				}
				
				$fechaIn = date_create_from_format('d-m-y', $req->dateIn);
				$fechaOut = date_create_from_format('d-m-y', $req->dateOut);
				include("../destinationFligth.php");
				include("../destinosVuelos.php");
				if(isset($destination[$req->input('origen')])){
					$origenBase = $destination[$req->input('origen')];
				}else{
					$origenes = $aeropuerto[$req->input('origen')];
					$origenExplode = explode(",", $origenes);
					$destinoBase = $origenExplode[2].",".$origenExplode[3];
				}

				$origen = explode(",", $origenBase);
				if(isset($destination[$req->input('destino')])){
					$destinoBase = $destination[$req->input('destino')];
				}else{
					$destinos = $aeropuerto[$req->input('destino')];
					$destinoExplode = explode(",", $destinos);
					$destinoBase = $destinoExplode[2].",".$destinoExplode[3];
				}	
				$destino = explode(",", $destinoBase);
				$fligthSession[$k]->fligthIndex = $k;
				$fligthSession[$k]->bagage = $bagage;
				$fligthSession[$k]->origen = $req->input('origen');
				$fligthSession[$k]->origen_aeropuerto = "";
				$fligthSession[$k]->origen_localidad = ucwords(strtolower($origen[0]));
				$fligthSession[$k]->pais_origen = ucwords(strtolower($origen[1]));
				$fligthSession[$k]->destino = $req->input('destino');
				$fligthSession[$k]->destino_aeropuerto = "";
				$fligthSession[$k]->destino_localidad = ucwords(strtolower($destino[0]));
				$fligthSession[$k]->pais_destino = ucwords(strtolower($destino[1]));
				$fligthSession[$k]->dateIn = date_format($fechaIn, 'j M');
				$fligthSession[$k]->dateOut = date_format($fechaOut, 'j M');
				$fligthSession[$k]->segmentos= $refNumber;
				$fligthSession[$k]->cantAdultos= $req->input('cantidad_adultos');
				$fligthSession[$k]->cantNinhos= $req->input('cantidadN');
				$fligthSession[$k]->cantInfante= $req->input('cantidad_infantes');
				$fligthSession[$k]->detalleBtn = $detalles;
				$fligthViews[] = preg_replace('/\s+/', ' ',(string) View::make('partials.fligth.singleResult')->with('fligth',$fligthSession[$k]));
			} 
			else break;
		}
		return $fligthViews;
	}
	private function fligthSegment($indiceBase, $refQualifier, $refNumber, $segmento, $majkabin){
		$result = [];
		$opcionSegment = [];
		$fligthIndex = Session::get('fligthIndex');
		include("../comAerea.php");
		include("../aeronaves.php");
		include("../aeropuertos.php");
		include("../tipo_asiento.php");
		foreach($fligthIndex as $key=>$indiceVuelo){
			if($indiceVuelo->requestedSegmentRef->segRef == $segmento){
				foreach($indiceVuelo->groupOfFlights as $marcador=>$opcionSegment){
					if($opcionSegment->propFlightGrDetail->flightProposal[0]->ref == $refNumber){
						$numero = 0;
						$contador = count($opcionSegment->flightDetails);
						foreach($opcionSegment->flightDetails as $index1=>$segment){
							if(isset($segment->flightInformation->attributeDetails[0]->attributeDescription)){
								$duracion = $segment->flightInformation->attributeDetails[0]->attributeDescription;
							}else{
								$duracion = 0;
							}
							/*$this->diffFechaHora($segment->flightInformation->productDateTime->dateOfArrival,$segment->flightInformation->productDateTime->timeOfArrival, $segment->flightInformation->productDateTime->dateOfDeparture,$segment->flightInformation->productDateTime->timeOfDeparture);*/
							if($index1 > 0){
								$espera = $this->diffFechaHora($dateArrival,$timeOfArrival, $segment->flightInformation->productDateTime->dateOfDeparture,$segment->flightInformation->productDateTime->timeOfDeparture);
								$opcionSegment->flightDetails[$index1]->flightInformation->horaEspera = $espera;
								$dateArrival = $segment->flightInformation->productDateTime->dateOfDeparture;
								$timeOfArrival = $segment->flightInformation->productDateTime->timeOfArrival;
							}else{
								$dateArrival = $segment->flightInformation->productDateTime->dateOfDeparture;
								$timeOfArrival = $segment->flightInformation->productDateTime->timeOfArrival;
							}

							$opcionSegment->flightDetails[$index1]->flightInformation->duracion = $duracion;
							$numero = $numero +1;

							if(isset($comAerea[$segment->flightInformation->companyId->marketingCarrier])){
								$marketing= explode("|",$comAerea[$segment->flightInformation->companyId->marketingCarrier]);
							}else{
								$marketing[0]= "Compañia Indefinida (".$segment->flightInformation->companyId->marketingCarrier.")";
							}
							
							$marketingCarrier = $marketing[0];
							if(isset($segment->flightInformation->companyId->operatingCarrier)){
								if(isset($comAerea[$segment->flightInformation->companyId->operatingCarrier])){
									$carrier = explode("|",$comAerea[$segment->flightInformation->companyId->operatingCarrier]);
									$operatingCarrier = $carrier[0];
								}else{
									$carrier[0] = $segment->flightInformation->companyId->operatingCarrier;
								}
							}else{
								$operatingCarrier = $marketingCarrier;
							}

							if(isset($aeronave[$segment->flightInformation->productDetail->equipmentType])){
								$opcionSegment->flightDetails[$index1]->flightInformation->aeronave = $aeronave[$segment->flightInformation->productDetail->equipmentType];
							}else{
									$opcionSegment->flightDetails[$index1]->flightInformation->aeronave = "Indefinido";
							}

							$opcionSegment->flightDetails[$index1]->flightInformation->aerolinea = $marketingCarrier." / ".$operatingCarrier;

							if(isset($tarifa[$majkabin[$index1]->productInformation->cabinProduct->rbd])){	
								$opcionSegment->flightDetails[$index1]->flightInformation->tipoAsiento = $tarifa[$majkabin[$index1]->productInformation->cabinProduct->rbd]."(".$majkabin[$index1]->productInformation->cabinProduct->rbd.")";
							}else{
								$opcionSegment->flightDetails[$index1]->flightInformation->tipoAsiento = "Clase: (".$majkabin[$index1]->productInformation->cabinProduct->rbd.")";							
							}

							$opcionSegment->flightDetails[$index1]->flightInformation->class = $majkabin[$index1]->productInformation->cabinProduct->rbd;

							foreach($segment->flightInformation->location as $index2=>$segment2){
								$destino = $aeropuerto[$segment2->locationId];
								$datosDestino = explode("|", $destino);
								$opcionSegment->flightDetails[$index1]->flightInformation->location[$index2]->locationId = $segment2->locationId;
								$opcionSegment->flightDetails[$index1]->flightInformation->location[$index2]->nombre = ucwords(strtolower($datosDestino[0]));
								$opcionSegment->flightDetails[$index1]->flightInformation->location[$index2]->cuidad =  ucwords(strtolower($datosDestino[2]));
								$opcionSegment->flightDetails[$index1]->flightInformation->location[$index2]->pais =  ucwords(strtolower($datosDestino[3]));
							}
						}
						$result = $opcionSegment;
					}
				}	
			}
		}
		return $result;
	}
	private function diffFechaHora($date1,$hour1, $date2,$hour2){
		$timezone = new \DateTimeZone('UTC'); 
		$dateArrival = \DateTime::createFromFormat('dmY', $date1, $timezone);
        $dateDepature = \DateTime::createFromFormat('dmY', $date2, $timezone);	
		$dateR1 = new \DateTime('20'.$dateDepature->format('y-m-d')." ".date("G:i",strtotime($hour2)));
		$dateR2 = new \DateTime('20'.$dateArrival->format('y-m-d')." ".date("G:i",strtotime($hour1)));
		$diff = $dateR1->diff($dateR2);
		$duracion= "";
		if($diff->d != 0){
			$duracion.= $diff->d." d";
		}
		if($diff->h != 0){ 
			$duracion.= $diff->h." h";
		}
		if($diff->i != 0){
			$duracion.= $diff->i." min";
		}
		return $duracion;
	}
	private function itinerary($segRef, $locationOrigen, $locationDestiny, $date ){
		$itinerario = new \StdClass;
		$requestedSegmentRef = new \StdClass;
		$departurePoint = new \StdClass;
		$departureLocalization = new \StdClass;
		$arrivalPointDetails = new \StdClass;
		$arrivalLocalization = new \StdClass;
		$firstDateTimeDetail= new \StdClass;
		$timeDetails =  new \StdClass;
		$requestedSegmentRef->segRef = $segRef;
		$itinerario->requestedSegmentRef= $requestedSegmentRef;
		$departurePoint->locationId = $locationOrigen;
		$departureLocalization->departurePoint = $departurePoint;
		$itinerario->departureLocalization = $departureLocalization;
		$arrivalPointDetails->locationId = $locationDestiny;
		$arrivalLocalization->arrivalPointDetails = $arrivalPointDetails;
		$itinerario->arrivalLocalization = $arrivalLocalization;
		$itinerario->arrivalLocalization->arrivalPointDetails = $arrivalPointDetails;
		$firstDateTimeDetail->date = $date;
		$timeDetails->firstDateTimeDetail = $firstDateTimeDetail;
		$itinerario->timeDetails = $timeDetails;
		return $itinerario;
	}	
	private function getPaxReference($cantidad, $ptc){
		$ref =\Session::get('ref');
		$result = new \StdClass;
		$traveller = [];
		if($ptc =="INF"){
			$ref = 0;
		}
		
		$pReference = new \StdClass;
		$pReference->ptc[0] = $ptc;
		$indice = 0;
		for($x=0; $x<$cantidad; $x++){
			$ref = $ref+1;
			$travel = new \StdClass;
			if($ptc =="INF"){
				$travel->infantIndicator= "1";
			}	
			$travel->ref = $ref;
			$traveller[$x] = $travel;
		}

		$pReference->traveller = $traveller;
		$result = $pReference;

		\Session::put('ref', $ref);
		return $result;
	}
	private function getPartialPaxReference($ref,$ptc){
		$travel = new \StdClass;
		$pReference = new \StdClass;
		$pReference->ptc[0] = $ptc;
		$travel->ref = $ref;
		if($ptc =="INF"){
			$travel->infantIndicator= "1";
		}	
		$traveller[$ref] = $travel;
		$pReference->traveller = $traveller;
		return $pReference;
	}
	private function getItinerary($segRef, $departPoint, $arrivalPoint, $dateDetail){
		$itinerario = [];
		$requestedSegmentRef = new \StdClass;
		$departurePoint = new \StdClass;
		$departureLocalization = new \StdClass;
		$arrivalPointDetails = new \StdClass;
		$arrivalLocalization = new \StdClass;
		$firstDateTimeDetail= new \StdClass;
		$timeDetails =  new \StdClass;
		$indice = new \StdClass;
		$segRef = $segRef;
		$requestedSegmentRef->segRef = $segRef;
		$indice->requestedSegmentRef= $requestedSegmentRef;
		$departurePoint->locationId = $departPoint;
		$departureLocalization->departurePoint = $departurePoint;
		$indice->departureLocalization = $departureLocalization;
		$arrivalPointDetails->locationId = $arrivalPoint;
		$arrivalLocalization->arrivalPointDetails = $arrivalPointDetails;
		$indice->arrivalLocalization = $arrivalLocalization;
		$indice->arrivalLocalization->arrivalPointDetails = $arrivalPointDetails;
		$firstDateTimeDetail->date = $dateDetail;
		$timeDetails->firstDateTimeDetail = $firstDateTimeDetail;
		$indice->timeDetails = $timeDetails;
		$itinerario= $indice;		
		return $itinerario;
	}	
	public function checkForMoreFligth(Request $req){
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json',
						   'Cache-Control'=>'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
						   ]
		]);

		$doCheck = true;
		$iibReqObj = $req->session()->get('firstIibReq');
		if(!is_null($iibReqObj)){
			$iibRsp = $client->post(Config::get('config.iibFligthSearch'), [
					'json' => $iibReqObj,
				]);

			$iibObjRsp = json_decode($iibRsp->getBody());
			//print_r("RESP BUSQUE<br>".json_encode($iibObjRsp));

			$fligthSession = array();
			$searchRsp = new \StdClass;
			$searchRsp->codRetorno = 0;
			$searchRsp->desRetorno = 'Procesado exitosamente';
			$resultsPerProvider = array();

			$initCantResults =0;
			foreach($iibObjRsp->recommendation as $index=>$fligth){
				$initCantResults++;
				$fligthSession[] = $fligth;
			}
		}else{
			$flightSession = 0;
		}	
		$req->session()->put('fligthSession',$fligthSession);

		return count($req->session()->get('fligthSession'));
	}	
	public function localizacion($localizacion){
		$aeropuerto = DB::select("SELECT * FROM aeropuerto where codigo_iata = '".$localizacion."'");
		if(isset($aeropuerto[0]->codigo_iata)){
			$datosAeropuerto = $aeropuerto[0]->codigo_iata."|".$aeropuerto[0]->nombre."|".$aeropuerto[0]->localidad."|".$aeropuerto[0]->pais_eng;
		}else{
			$datosAeropuerto = "Sin Registro";
		}
		return $datosAeropuerto;
	}	
	public function aerolinea($codigo){
		/*echo '<pre>';
		print_r($codigo);*/
			$aerolinea = DB::select("SELECT * FROM aerolineas where codigo_iata = '".$codigo."'");
			if(isset($aerolinea[0]->nombre)){
				$datosAerolinea = $aerolinea[0]->nombre;
			}else{
				$datosAerolinea = "Sin Registro";
			}
			return $datosAerolinea;
	}	
	public function aeronave($codigo){
			$aeronaves = DB::select("SELECT * FROM aeronaves where codigo_iata = '".$codigo."'");
			if(isset($aeronaves[0]->nombre)){
				$datosAeronaves = $aeronaves[0]->nombre;
			}else{
				$datosAeronaves = "";
			}
			return $datosAeronaves;
	}	
	public function ciudad($indice){	
        $ciudades = DB::select("SELECT * FROM ciudad_pais where codigo_iata = '".$indice."'");
		return  $ciudades[0]->codigo_iata."|".$ciudades[0]->nombreCiudad."|".$ciudades[0]->pais;          
	}	
	public function format_interval(DateInterval $interval) {
	    $result = "";
	    if ($interval->y) { $result .= $interval->format("%y years "); }
	    if ($interval->m) { $result .= $interval->format("%m months "); }
	    if ($interval->d) { $result .= $interval->format("%d days "); }
	    if ($interval->h) { $result .= $interval->format("%h hours "); }
	    if ($interval->i) { $result .= $interval->format("%i minutes "); }
	    if ($interval->s) { $result .= $interval->format("%s seconds "); }

	    return $result;
	}
	public function dataFilterFligth(Request $req){
		$fligthFilterSession['aerolinea'] = $req->session()->get('filtro');
		$fligthFilterSession['escala'] = $req->session()->get('escalas');
		/*$fligthFilterSession['clase'] = $req->session()->get('clase');*/
		return json_encode($fligthFilterSession);
	}
	public function doFlightFiltro(Request $req){
		$req->session()->put('indicador', 0);
		$req->session()->put('busquedaObj', "");
		$searchRsp = new \StdClass;
		$searchRsp->codRetorno = 0;
		$searchRsp->desRetorno = 'Procesado exitosamente';
		$marcador = 0;
		$searchRsp->fligthViews = $this->getFligthFiltro($req->input('from'),$req->input('to'),$req, $marcador);
		return json_encode($searchRsp);
	}
	public function getFligthFiltro($from, $to, $req, $marcador){
		$fligthViews = [];
		if($marcador == 0){
			/*echo '<pre>';
			print_r($marcador);*/
			$fligthSession = $req->session()->get('restFligth');
			if($req->input('precio_minimo') > 0 && $req->input('precio_maximo')){
				$fligthArray = array_filter($fligthSession, function($fligth) use ($req){
								return ($fligth->recPriceInfo->monetaryDetail[0]->amount <= $req->input('precio_maximo')&&$fligth->recPriceInfo->monetaryDetail[0]->amount >= $req->input('precio_minimo'));
							});
			}else{
				$fligthArray = $fligthSession;
			}
			$fligthArrays = [];
			if($req->input('aerolinea')!=""){
				foreach($req->input('aerolinea') as $base=>$aerolinea){
						foreach($fligthArray as $indice=>$fligth){
							foreach($fligth->detalleBtn as $key2=>$detalleBtn){
								$indiceProveedor = 0;
								foreach($detalleBtn->flightDetails as  $key3=>$flightDetails){
										if($flightDetails->flightInformation->companyId->marketingCarrier == $aerolinea){
											$indiceProveedor = $indiceProveedor+0;
										}else{
											$indiceProveedor = $indiceProveedor+1;
										}						
									}
							}
							if($indiceProveedor == 0){
								$fligthArrays[] = $fligthArray[$indice];
							}
						}
				}	
				$fligthArray = $fligthArrays;
			}else{
				$fligthArray = $fligthArray;
			}
			/*echo '<pre>';
			print_r();	*/
			$indicador = 0;
			$fligthArrays = [];
			if($req->input('escala')> 0){
				foreach($fligthArray as $ul=>$fligth){
					$indicador = 0;
					foreach($fligth->detalleBtn as $ul1=>$detalleBtn){
						if($detalleBtn->cantidad == $req->input('escala')){
							$indicador = $indicador+0;
						}else{
							$indicador = $indicador+1;
						}	
					}
					if($indicador == 0){
						$fligthArrays[] = $fligthArray[$ul];
					}
				}
				$fligthArray = $fligthArrays;
			}
			$req->session()->put('busquedaObj', $fligthArray);
		}else{
			$fligthArray = $req->session()->get('busquedaObj');
		}	

		for($k = $from ; $k<=$to ; $k++){
			if(isset($fligthArray[$k])){
				$fligthViews[] = preg_replace('/\s+/', ' ',(string) View::make('partials.fligth.singleResult')->with('fligth',$fligthArray[$k]));
				$fligthArray[$k];
			}
		}	
		return $fligthViews;	
	}
	private function guardarBusqueda($saveReserva){
 		$values = array(
 						'id_usuario' => $saveReserva->id_usuario,
 						'fecha_checkin' => $saveReserva->fecha_checkin,
 						'fecha_checkout' => $saveReserva->fecha_checkout,
 						'id_origen' => $saveReserva->id_origen,
 						'id_destino' => $saveReserva->id_destino,
 						'ocupancia' => $saveReserva->ocupancia
 						);	
 		/*echo '<pre>';
		print_r($values);*/
		DB::table('busqueda_vuelos')->insert($values);
	}	

}



