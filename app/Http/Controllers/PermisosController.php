<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Session;
use Redirect;
use App\Autorizaciones;
use App\Producto;
use App\ProformasDetalle;
use App\Divisas;
use App\Persona;
use App\AdjuntoDocumento;
use App\HistoricoComentariosProforma;
use App\Voucher;
use Response;
use Image; 
use DB;



class PermisosController extends Controller {


private function getIdUsuario(){
	
	 return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

}

private function getIdEmpresa(){

  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

}




public function  index(){

$personas = Persona::with('tipoPersona')
					->where('activo',true)
					->whereIn('id_tipo_persona',array(1,2,3,4,5,7,6))
					->where('id_empresa',$this->getIdEmpresa())
					->orderBy('nombre','ASC')->get();

$permisosPersona =  DB::table('persona_permiso_especiales as ppe');
$permisosPersona = $permisosPersona->select('p.nombre','p.apellido','pe.desc');
$permisosPersona = $permisosPersona->leftJoin('personas as p','ppe.id_persona','=','p.id');
$permisosPersona = $permisosPersona->leftJoin('permisos_especiales as pe','ppe.id_permiso','=','pe.id');
$permisosPersona = $permisosPersona->where('ppe.id_empresa','=',$this->getIdEmpresa());
$permisosPersona = $permisosPersona->get();

// dd($permisosPersona);

return view('pages.mc.permisos.index')->with(['personas'=>$personas,'permisosPersona'=>$permisosPersona]);




}//function






public function getPermisosPersona(Request $req){
	// dd($req->all());

	$permisosPersona =  DB::table('persona_permiso_especiales as ppe');
	$permisosPersona = $permisosPersona->select('p.nombre','p.apellido','pe.desc');
	$permisosPersona = $permisosPersona->leftJoin('personas as p','ppe.id_persona','=','p.id');
	$permisosPersona = $permisosPersona->leftJoin('permisos_especiales as pe','ppe.id_permiso','=','pe.id');
	$permisosPersona = $permisosPersona->where('ppe.id_empresa','=',$this->getIdEmpresa());

	if($req->input('id') != ''){
		$permisosPersona = $permisosPersona->where('ppe.id_persona',$req->input('id'));
	}

	$permisosPersona = $permisosPersona->get();

	return response()->json(['data'=>$permisosPersona]);
}

public function  edit(Request $req){

	$id = 0;
	if ($req->input('id') != '') {
		$id = $req->input('id');
	}
	$personas = Persona::with('tipoPersona')->where('activo',true)->whereIn('id_tipo_persona',array(1,2,3,4,5,7,6))->where('id_empresa',$this->getIdEmpresa())->orderBy('nombre','ASC')->get();
	$permisos = DB::table('permisos_especiales')->select('*')->orderBy('desc', 'ASC')->get();

	$idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	return view('pages.mc.permisos.edit')->with(['personas'=>$personas, 'permisos'=>$permisos,'idPersona'=>$id,'idEmpresa'=>$idEmpresa]);


}//function





public function doEdit(Request $req){


$id = $req->input('id');

$permisos =  DB::table('persona_permiso_especiales as ppe');
$permisos = $permisos->select('p.nombre','p.apellido','pe.desc','ppe.id_permiso');
$permisos = $permisos->leftJoin('personas as p','ppe.id_persona','=','p.id');
$permisos = $permisos->leftJoin('permisos_especiales as pe','ppe.id_permiso','=','pe.id');
$permisos = $permisos->where('ppe.id_persona',$id);
$permisos = $permisos->get();



return response()->json(array('permisos'=>$permisos));

}//function



public function doGuardarPermiso(Request $req) {

	// dd($req->all());

	$err = false;
	$idPersona = $req->input('id_persona');
	unset($req['id_persona']);
	$idUsuario = $this->getIdUsuario();
	$created_at = date('Y-m-d H:i:00');

	$idActivo = array();
	$totalId = array();

	// dd($req->all());
	foreach ($req->all() as $key => $value) {
		// echo " ->".$key."<- ";

		if($value != ''){
		$idActivo['id_persona'] = $idPersona;
		$idActivo['id_permiso'] = $key;
		$idActivo['id_usuario'] = $idUsuario;
		$idActivo['id_empresa'] = $this->getIdEmpresa();
		$idActivo['creacion'] = $created_at;
		array_push($totalId,$idActivo);
		}

		
	}//foreach
 
	// dd($totalId);
	
try{
	DB::beginTransaction();
	 DB::table('persona_permiso_especiales')->where('id_persona',$idPersona)->delete();
	 if(count($totalId) > 0){
	 DB::table('persona_permiso_especiales')->insert($totalId);
	 }
	$err = true;
}catch(\Exception $e){
	DB::rollBack();
	$err = false;
}
	
	if($err)
		DB::commit();


	
	return response()->json(['rsp'=>$err]); 


} //function



}//class