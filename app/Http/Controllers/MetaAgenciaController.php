<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Persona;
use App\MetaAgencia;
use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;
use DataTables;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;




class MetaAgenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     private function getIdEmpresa(){

        return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
  
      }
      private function getMes($mes)
    {
        switch($mes) {
            case "Enero": $Mes = '01';
                break;
            case "Febrero": $Mes = '02';
                break;
            case "Marzo": $Mes = '03';
                break;
            case "Abril": $Mes = '04';
                break;
            case "Mayo": $Mes = '05';
                break;
            case "Junio": $Mes = '06';
                break;
            case "Julio": $Mes = '07';
                break;
            case "Agosto": $Mes = '08';
                break;
            case "Septiembre": $Mes = '09';
                break;
            case "Octubre": $Mes = '10';
                break;
            case "Noviembre": $Mes = '11';
                break;
            case "Diciembre": $Mes = '12';
                break;
        }

        return $Mes;
    }

      /*private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = trim($date[2])."-".trim($date[1])."-".trim($date[0]);
            return $fecha;
        } else {
            return null;
        }
    }*/

    private function formatoFechaEntrada($mes, $anio) {
        $meses = array(
            'Enero' => 1, 'Febrero' => 2, 'Marzo' => 3,
            'Abril' => 4, 'Mayo' => 5, 'Junio' => 6,
            'Julio' => 7, 'Agosto' => 8, 'Septiembre' => 9,
            'Octubre' => 10, 'Noviembre' => 11, 'Diciembre' => 12
        );
    
        if ($mes != '' && $anio != '') {
            $numeroMes = $meses[$mes];
            $fecha = $anio . "-" . str_pad($numeroMes, 2, '0', STR_PAD_LEFT) . "-01";
            return $fecha;
        } else {
            return null;
        }
    }
    
    
    
    
    
    
    

    public function index(Request $request)
    {
        $id_empresa = $this->getIdEmpresa();
		$persona = strtoupper($request->input('nombre'));
		$denominacion = strtoupper($request->input('denominacion_comercial'));
		$email = $request->input('correo');
		$ruc = $request->input('ruc');
        $vendedorId = $request->input('id_vendedor_agencia');
		$buscar = $request->input('buscar');
        //para periodo
		/*$explode = explode("/", $datos->fecha_hora_facturacion);
        $mesLetra = $explode[0];
        $year = $explode[1];
        $mes = $this->getMes($mesLetra);
        $data = $data->where('fecha_hora_facturacion', $mes.''.$year);*/
        //fin periodo

		$query = DB::table('personas AS p')
    ->leftJoin('facturas as f', 'p.id', '=', 'f.cliente_id')
    ->leftjoin('metas_agencias as ma', 'p.id', '=', 'ma.id_vendedor_agencia')
    ->select([
        'ma.id_meta',
        'ma.id_vendedor_agencia',
        DB::raw('EXTRACT(MONTH FROM f.fecha_hora_facturacion) AS mes'),
       // DB::raw('COALESCE(SUM(f.renta), 0) AS renta_bruta'),
       // DB::raw('COALESCE(SUM(f.renta_comisionable), 0) AS renta_neta'),
        DB::raw('COALESCE(ma.meta_mensual, 0) AS meta_mensual'),
        //DB::raw('COALESCE(SUM(f.total_bruto_factura), 0) AS renta_bruta_factura'),
        DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre_completo"),
        DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
            p_cotizacion := f.cotizacion_factura, 
            p_monto := COALESCE(f.total_bruto_factura, 0),
            p_id_moneda_costo := f.id_moneda_venta,
            p_id_moneda_venta := 143
        )), 0) as total_factura_cotizado'),
        DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
            p_cotizacion := f.cotizacion_factura, 
            p_monto := COALESCE(f.renta, 0),
            p_id_moneda_costo := f.id_moneda_venta,
            p_id_moneda_venta := 143
        )), 0) as renta_cotizada'),
        'ma.anho as periodo_meta',
        DB::raw('(COALESCE(SUM(get_monto_cotizacion_custom(
            p_cotizacion := f.cotizacion_factura, 
            p_monto := COALESCE(f.total_bruto_factura, 0),
            p_id_moneda_costo := f.id_moneda_venta,
            p_id_moneda_venta := 143
        )), 0) - COALESCE(ma.meta_mensual, 0)) as diferencia'),
    ])
            ->where('p.id_empresa', $id_empresa)
            ->groupBy('ma.id_meta','ma.id_vendedor_agencia', 'ma.meta_mensual', 'p.nombre', 'p.apellido','ma.anho', DB::raw('EXTRACT(MONTH FROM f.fecha_hora_facturacion)'));
				
	
       /*     if ($desde && $hasta) {
                $query->whereBetween('f.fecha_hora_facturacion', [$desde, $hasta]);
            }
*/
			if ($buscar) {
				$query->where(function ($query) use ($buscar) {
					$query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'like', '%' . $buscar . '%');
				});
			}
			
			
            if ($vendedorId) {
                $query->where('ma.id_vendedor_agencia', $vendedorId); // Filtrar por el ID del vendedor
            }
/*
		if ($email) {
			$query->where('p.email', 'LIKE', '%' . $email . '%');
		}*/
	
		$metas = $query->orderBy('ma.id_meta', 'asc')->paginate(10);

        $agencias = DB::select("SELECT id, nombre from personas where id_tipo_persona = 8 and activo = true and id_empresa = ".$this->getIdEmpresa()." order by nombre asc");
        $metas = $query->orderBy('ma.id_meta', 'desc')->paginate(10);
        
        //para mostrar el periodo
       

		return view('pages.mc.metas.metrica')->with(['metas' => $metas, 'agencias' => $agencias]);
		
    }


    public function indexAjax(Request $request)
    {
        $id_empresa = $this->getIdEmpresa();
        $persona = strtoupper($request->input('nombre'));
        $denominacion = strtoupper($request->input('denominacion_comercial'));
        $ruc = $request->input('ruc');
        $vendedorId = $request->input('id_vendedor_agencia');
        $buscar = $request->input('buscar');
        
      

        $query = DB::table('personas AS p')
        ->leftJoin('facturas as f', 'p.id', '=', 'f.cliente_id')
        ->leftJoin('metas_agencias as ma', function ($join) {
            $join->on('p.id', '=', 'ma.id_vendedor_agencia')
                ->where(DB::raw('EXTRACT(MONTH FROM f.fecha_hora_facturacion)'), '=', DB::raw('ma.mes'))
                ->where(DB::raw('EXTRACT(YEAR FROM f.fecha_hora_facturacion)'), '=', DB::raw('CAST(ma.anho AS INT)'));
        })        
        ->leftJoin('estados as estado', 'f.id_estado_factura', '=', 'estado.id')
        ->select([
            DB::raw('COALESCE(ma.id_meta, 0) AS id_meta_ma'),
            DB::raw('COALESCE(ma.id_vendedor_agencia, 0) AS id_vendedor_agencia'),
            DB::raw('COALESCE(ma.mes, ma.mes) AS mes'),
            DB::raw("CASE 
                WHEN EXTRACT(MONTH FROM f.fecha_hora_facturacion) IS NULL OR EXTRACT(MONTH FROM f.fecha_hora_facturacion) = 0 THEN '' 
                ELSE CAST(EXTRACT(MONTH FROM f.fecha_hora_facturacion) AS TEXT)
            END AS mes_fecha_facturacion"),
            DB::raw('COALESCE(ma.meta_mensual, 0) AS meta_mensual'),
            DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre_completo"),
            DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
                p_cotizacion := f.cotizacion_factura,
                p_monto := COALESCE(f.total_bruto_factura, 0),
                p_id_moneda_costo := f.id_moneda_venta,
                p_id_moneda_venta := 143
            )), 0) as total_factura_cotizado'),
            DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
                p_cotizacion := f.cotizacion_factura,
                p_monto := COALESCE(f.renta, 0),
                p_id_moneda_costo := f.id_moneda_venta,
                p_id_moneda_venta := 143
            )), 0) as renta_cotizada'),
            DB::raw('MAX(COALESCE(CAST(ma.anho AS INT), EXTRACT(YEAR FROM f.fecha_hora_facturacion))) as periodo_meta'),
            DB::raw('(COALESCE(SUM(get_monto_cotizacion_custom(
                p_cotizacion := f.cotizacion_factura,
                p_monto := COALESCE(f.total_bruto_factura, 0),
                p_id_moneda_costo := f.id_moneda_venta,
                p_id_moneda_venta := 143
            )), 0) - COALESCE(ma.meta_mensual, 0)) as diferencia')
        ])
            ->where('p.id_empresa', $id_empresa)
            ->where('estado.id', '!=', 30)
            ->groupBy('ma.id_meta', 'ma.id_vendedor_agencia','ma.meta_mensual', 'p.nombre', 'p.apellido','ma.anho', DB::raw('EXTRACT(MONTH FROM f.fecha_hora_facturacion)'),'ma.mes');
            if ($request->input('anio') != "") {
                $anio = $request->input('anio');
                
                $query->whereRaw("EXTRACT(YEAR FROM f.fecha_hora_facturacion) = $anio");
                   // ->where('ma.anho', $anio);
            }
            
            

     
        
        if ($request->input('search.value')) {
            $searchValue = $request->input('search.value');
            $query->where(function ($query) use ($searchValue) {
                $query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'LIKE', '%' . $searchValue . '%');
            });
        }
    
        
        if ($vendedorId) {
			$query->where('f.cliente_id', $vendedorId); // Filtrar por el ID del vendedor
		}
    
       
    
        // if ($email) {
        //     $query->where('p.email', 'LIKE', '%' . $email . '%');
        // }
    
       // $totalRecords = $query->count();

        // Obtener los parámetros de paginación de la solicitud
        //$draw = $request->input('draw');
        $start = $request->input('start', 0);
        $length = $request->input('length', 10);
        //$recordsFiltered = $totalRecords;
    
        // Aplicar la paginación
      //  $metas = $query->paginate($start);
     // $metas = $query->offset($start)->limit($length)->get();
//dd($request->all());
/*$metas = $query->get();
return response()->json(array('data'=>$metas));*/
//$start = request()->get('page', 1); // Número de página actual obtenido de la solicitud

//$metas = $query->paginate($length, ['*'], 'page', $start);
//$length = 10; // Número de elementos por página
//$start = request()->get('start', 0); // Índice de inicio obtenido de la solicitud

$metas = $query->paginate($length, ['*'], 'page', ceil(($start + 1) / $length));

$metaData = [
    'draw' => $metas, // Número de solicitud de dibujo (draw request)
    'recordsTotal' => $metas->total(),
    'recordsFiltered' => $metas->total(),
    'data' => $metas->items(),
];

return response()->json($metaData);
//return response()->json($metas);
//return response()->json(array('response'=>$metas));
/*
return response()->json([
    'data' => $metas,
    'draw' => $draw,
    'recordsTotal' => $totalRecords,
    'recordsFiltered' => $recordsFiltered,
]);*/
      
    }




    public function metaAgencias(Request $request){
		$id_empresa = $this->getIdEmpresa();
		$persona = strtoupper($request->input('nombre'));
		$denominacion = strtoupper($request->input('denominacion_comercial'));
		$email = $request->input('correo');
		$ruc = $request->input('ruc');
		$buscar = $request->input('buscar');
		
		//dd($inputPrestador);

		$query = DB::table('metas_agencias AS metas')
		->select([
			'metas.id_meta',
            'metas.id_vendedor_agencia',
			DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre"),
			'p.denominacion_comercial',
			'p.email',
            'metas.meta_anual',
            'metas.meta_semestral',
            'metas.meta_trimestral',
            'metas.meta_mensual',
            'metas.anho as periodo',
            'metas.mes'
		])
		->leftJoin('personas AS p', 'p.id', '=', 'metas.id_vendedor_agencia')
    				->where('p.id_empresa', $id_empresa);

      
	
			if ($buscar) {
				$query->where(function ($query) use ($buscar) {
					$query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'like', '%' . $buscar . '%')
							->orWhere('p.documento_identidad', 'LIKE', '%' . $ruc . '%');;
				});
			}
			
			
		if ($persona) {
				$query->whereRaw("CONCAT(p.nombre, ' ', p.apellido) LIKE ?", ['%' . $persona . '%']);
		}

		if ($denominacion) {
			$query->where('p.denominacion_comercial', 'LIKE', '%' . $denominacion . '%');
		}

		if ($ruc) {
			$query->where('p.documento_identidad', 'LIKE', '%' . $ruc . '%');
		}
/*
		if ($email) {
			$query->where('p.email', 'LIKE', '%' . $email . '%');
		}*/
	
	
        $metas = $query->orderBy('metas.id_meta', 'desc')->paginate(10);
        $agencias = DB::select("SELECT id, nombre from personas where id_tipo_persona = 8 and activo = true and id_empresa = ".$this->getIdEmpresa()." order by nombre asc");
		return view('pages.mc.metas.index')->with(['metas' => $metas,'agencias' => $agencias]);
		
	}

    
	public function metaAgenciasAjax(Request $request)
    {
        $id_empresa = $this->getIdEmpresa();
        $persona = strtoupper($request->input('nombre'));
        $denominacion = strtoupper($request->input('denominacion_comercial'));
        $ruc = $request->input('ruc');
        $vendedorId = $request->input('vendedor_id');
        $buscar = $request->input('buscar');
        $meses = [
            1 => 'Enero',
            2 => 'Febrero',
            3 => 'Marzo',
            4 => 'Abril',
            5 => 'Mayo',
            6 => 'Junio',
            7 => 'Julio',
            8 => 'Agosto',
            9 => 'Septiembre',
            10 => 'Octubre',
            11 => 'Noviembre',
            12 => 'Diciembre',
        ];
        $query = DB::table('metas_agencias AS metas')
		->select([
			'metas.id_meta',
            'metas.id_vendedor_agencia',
			DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre"),
			'p.denominacion_comercial',
			'p.documento_identidad',
            'metas.meta_anual',
            'metas.meta_semestral',
            'metas.meta_trimestral',
            'metas.meta_mensual',
            'metas.anho AS periodo',
            'metas.mes'
		])
		->leftJoin('personas AS p', 'p.id', '=', 'metas.id_vendedor_agencia')
    				->where('p.id_empresa', $id_empresa);
    
        if ($request->input('search.value')) {
            $searchValue = $request->input('search.value');
            $query->where(function ($query) use ($searchValue) {
                $query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'LIKE', '%' . $searchValue . '%');
            });
        }
    
        if ($persona) {
            $query->whereRaw("CONCAT(p.nombre, ' ', p.apellido) LIKE ?", ['%' . $persona . '%']);
        }
        if ($vendedorId && $vendedorId != 1) {
            $query->where('id', $vendedorId); // Cambia 'id' al nombre correcto de la columna en tu tabla
        }
    
         if ($ruc) {
             $query->where('p.documento_identidad', 'LIKE', '%' . $ruc . '%');
         }
    
        // if ($email) {
        //     $query->where('p.email', 'LIKE', '%' . $email . '%');
        // }
    
        $totalRecords = $query->count();
    
        $draw = $request->input('draw');
        $start = $request->input('start', 0);
        $length = $request->input('length', 10);
        $recordsFiltered = $totalRecords;
    
        $metas = $query->orderBy('metas.id_meta', 'desc')->offset($start)->limit($length)->get();
    
        $data = [];
        foreach ($metas as $meta) {
            $botones = '<a href="' . route('metaAgencias.edit', $meta->id_meta) . '" class="btn btn-warning ml-2"><span class="fa fa-edit"></span></a>';
    
            $data[] = [
                'id_meta' => $meta->id_meta,
                'id_vendedor_agencia' => $meta->id_vendedor_agencia,
                'nombre' => $meta->nombre,
                'denominacion_comercial' => $meta->denominacion_comercial,
                'meta_anual' => $meta->meta_anual,
                'meta_semestral' => $meta->meta_semestral,
                'meta_trimestral' => $meta->meta_trimestral,
                'meta_mensual' => $meta->meta_mensual,
                'periodo' => $meta->periodo,
                'mes' => $meses[$meta->mes],
                'accion' => $botones,
            ];
        }
    
        
        return response()->json([
            'data' => $data,
            'draw' => $draw,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $recordsFiltered
        ]);
      
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agencias = DB::select("SELECT id, nombre from personas where id_tipo_persona = 8 and activo = true and id_empresa = ".$this->getIdEmpresa()." order by nombre asc");
        return view('pages.mc.metas.agregar',compact('agencias'));
        //return view('pages.mc.metas.editar', compact('meta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$meta = new MetaAgencia();
        if ($request->has('accion')) {
            $accion = $request->input('accion');
            if ($accion === 'guardar') {
       // $meta->id_vendedor_agencia = $request->get('id_vendedor_agencia');

        $meta_anual = $request->input('meta_anual');
        $meta_mensual = $meta_anual/12;
        $meta_trimestral = $meta_anual /4 ;
        $meta_semestral = $meta_anual / 2;
        $anho = $request->input('anho');

        $id_vendedor_agencia = $request->input('id_vendedor_agencia');
        //validacion
        $registroExistente = MetaAgencia::where('id_vendedor_agencia', $id_vendedor_agencia)
        ->where('anho', $anho)
        ->first();

        if ($registroExistente) {
            // Ya existe un registro con la misma combinación, maneja el error o realiza alguna acción
            return redirect()->back()->with('error', 'Ya existe un registro para este vendedor y año.');
        }

       /* $meta->meta_anual = $meta_anual;
        $meta->meta_mensual = $meta_mensual;
        $meta->meta_trimestral = $meta_trimestral;
        $meta->meta_diaria = $meta_diaria;*/
       /* $meta->meta_anual = round($meta_anual * 100);
        $meta->meta_mensual = round($meta_mensual * 100);
        $meta->meta_trimestral = round($meta_trimestral * 100);
        $meta->meta_diaria = round($meta_diaria * 100);
       */
     
/*
      $meta->id_vendedor_agencia = $id_vendedor_agencia;
      $meta->meta_anual = (float)number_format($meta_anual, 2, '.', '');
      $meta->meta_mensual = (float)number_format($meta_mensual, 2, '.', '');
      $meta->meta_trimestral = (float)number_format($meta_trimestral, 2, '.', '');
      $meta->meta_semestral = (float)number_format($meta_semestral, 2, '.', '');
      $meta->anho = $anho;
        $meta->save();*/
        for ($mes = 1; $mes <= 12; $mes++) {
            $meta = new MetaAgencia();
            $meta->id_vendedor_agencia = $id_vendedor_agencia;
            $meta->meta_anual = (float)number_format($meta_anual, 2, '.', '');
            $meta->meta_mensual = (float)number_format($meta_mensual, 2, '.', '');
            $meta->meta_trimestral = (float)number_format($meta_anual / 4, 2, '.', '');
            $meta->meta_semestral = (float)number_format($meta_anual / 2, 2, '.', '');
            $meta->anho = $anho;
            $meta->mes = $mes;
    
            $meta->save();
        }
    }
    }
        return redirect()->route('metaAgencias')->with('message', 'El registro fue exitoso');
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_meta)
    {
        $meta = MetaAgencia::findOrFail($id_meta);    
        return view('pages.mc.metas.editar', compact('meta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_meta)
    {
       // DB::statement('SET session_replication_role = replica;');
        $meta = MetaAgencia::findOrFail($id_meta);
        $meta_mensual_original = $meta->meta_mensual;
    
       
        $mes_actualizado = $request->input('mes');
    
        $meta->meta_mensual = $request->input('meta_mensual', $meta->meta_mensual);
    
        if ($meta->meta_mensual !== $meta_mensual_original) {
            $meta->meta_anual = $meta->meta_mensual * 12;
            $meta->meta_trimestral = $meta->meta_anual / 4;
            $meta->meta_semestral = $meta->meta_anual / 2;
    
            // Actualiza los valores de los meses subsiguientes al mes actualizado
            for ($mes = $mes_actualizado + 1; $mes <= 12; $mes++) {
                $meta_mes_siguiente = MetaAgencia::where('id_vendedor_agencia', $meta->id_vendedor_agencia)
                    ->where('anho', $meta->anho)
                    ->where('mes', $mes)
                    ->first();
    
                if ($meta_mes_siguiente) {
                    $meta_mes_siguiente->meta_mensual = $meta->meta_mensual;
                    $meta_mes_siguiente->meta_anual = $meta->meta_mensual * 12;
                    $meta_mes_siguiente->meta_trimestral = $meta->meta_anual / 4;
                    $meta_mes_siguiente->meta_semestral = $meta->meta_anual / 2;
                    $meta_mes_siguiente->save();
                }
            }
        }
    
        $meta->meta_anual = number_format($meta->meta_anual, 2, '.', '');
        $meta->meta_mensual = number_format($meta->meta_mensual, 2, '.', '');
        $meta->meta_trimestral = number_format($meta->meta_trimestral, 2, '.', '');
        $meta->meta_semestral = number_format($meta->meta_semestral, 2, '.', '');
    
        $meta->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
        $meta->save();
        //DB::statement('SET session_replication_role = DEFAULT;');//funciona para desactivar trigger
        return redirect()->route('metaAgencias')->with('message', 'La actualización fue exitosa');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

public function AgenciasMasivo(Request $request)
{
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 1640); // 240 segundos = 4 minutos

    $path = $request->file('file')->getRealPath();
    $agencias = Excel::load($path)->get();
    
    if ($request->input('accion') === 'procesar') {
        foreach ($agencias as $vendedor) {
            $id = (int)$vendedor['id_vendedor_agencia'];
            $meta_anual = (int)$vendedor['meta_anual'];
            $anho = (int)$vendedor['anho'];
            
            if ($meta_anual > 0) {
                // Intentar encontrar un registro existente con la misma combinación
                $meta = MetaAgencia::firstOrNew([
                    'id_vendedor_agencia' => $id,
                    'anho' => $anho,
                ]);
                
                // Si no existe, establecer los valores y guardar
                if (!$meta->exists) {
                    $meta->meta_anual = $meta_anual;
                    //$meta->save();
                }

                // Calcular metas mes a mes y agregarlas
                for ($mes = 1; $mes <= 12; $mes++) {
                    $meta_mensual = $meta_anual / 12;
                    $meta_trimestral = $meta_anual / 4;
                    $meta_semestral = $meta_anual / 2;

                    // Verificar si ya existe un registro para este mes
                    $existingMetaMes = MetaAgencia::where([
                        'id_vendedor_agencia' => $id,
                        'anho' => $anho,
                        'mes' => $mes,
                    ])->first();

                    if (!$existingMetaMes) {
                        $meta_mes = new MetaAgencia();
                        $meta_mes->id_vendedor_agencia = $id;
                        $meta_mes->anho = $anho;
                        $meta_mes->mes = $mes;
                        $meta_mes->meta_mensual = (float)number_format($meta_mensual, 2, '.', '');
                        $meta_mes->meta_trimestral = (float)number_format($meta_trimestral, 2, '.', '');
                        $meta_mes->meta_semestral = (float)number_format($meta_semestral, 2, '.', '');
                        $meta_mes->meta_anual=$meta_anual;
                        $meta_mes->save();
                    }
                }
            }
        }

        flash('Metas actualizadas');

        return back();
    }

    return redirect()->route('metaAgencias')->with('error', 'No se ha seleccionado ningún archivo válido.');
}


public function historicoAgencias(Request $request)
    {
        $id_empresa = $this->getIdEmpresa();
		$persona = strtoupper($request->input('nombre'));
		$denominacion = strtoupper($request->input('denominacion_comercial'));
		$email = $request->input('correo');
		$ruc = $request->input('ruc');
        $vendedorId = $request->input('id_vendedor_agencia');
		$buscar = $request->input('buscar');
        //para periodo
		/*$explode = explode("/", $datos->fecha_hora_facturacion);
        $mesLetra = $explode[0];
        $year = $explode[1];
        $mes = $this->getMes($mesLetra);
        $data = $data->where('fecha_hora_facturacion', $mes.''.$year);*/
        //fin periodo

		$query = DB::table('personas AS p')
    ->leftJoin('facturas as f', 'p.id', '=', 'f.cliente_id')
    ->join('meta_agencia_historico as ma', 'p.id', '=', 'ma.id_vendedor_agencia')
    ->select([
        'ma.id',
        DB::raw('COALESCE(SUM(f.renta), 0) AS renta_bruta'),
        DB::raw('COALESCE(SUM(f.renta_comisionable), 0) AS renta_neta'),
        DB::raw('COALESCE(ma.meta_mensual, 0) AS meta_mensual'),
        DB::raw('COALESCE(SUM(f.total_bruto_factura), 0) AS renta_bruta_factura'),
        DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre_completo"),
        DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
            p_cotizacion := f.cotizacion_factura, 
            p_monto := COALESCE(f.total_bruto_factura, 0),
            p_id_moneda_costo := f.id_moneda_venta,
            p_id_moneda_venta := 143
        )), 0) as total_factura_cotizado'),
        DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
            p_cotizacion := f.cotizacion_factura, 
            p_monto := COALESCE(f.renta, 0),
            p_id_moneda_costo := f.id_moneda_venta,
            p_id_moneda_venta := 143
        )), 0) as renta_cotizada'),
        'ma.anho as periodo_meta',
        DB::raw('(COALESCE(SUM(get_monto_cotizacion_custom(
            p_cotizacion := f.cotizacion_factura, 
            p_monto := COALESCE(f.total_bruto_factura, 0),
            p_id_moneda_costo := f.id_moneda_venta,
            p_id_moneda_venta := 143
        )), 0) - COALESCE(ma.meta_mensual, 0)) as diferencia'),
    ])
            ->where('p.id_empresa', $id_empresa)
            ->groupBy('ma.id', 'ma.meta_mensual', 'p.nombre', 'p.apellido','ma.anho');
				
	
       /*     if ($desde && $hasta) {
                $query->whereBetween('f.fecha_hora_facturacion', [$desde, $hasta]);
            }
*/
			if ($buscar) {
				$query->where(function ($query) use ($buscar) {
					$query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'like', '%' . $buscar . '%');
				});
			}
			
			
            if ($vendedorId) {
                $query->where('ma.id_vendedor_agencia', $vendedorId); // Filtrar por el ID del vendedor
            }
/*
		if ($email) {
			$query->where('p.email', 'LIKE', '%' . $email . '%');
		}*/
	
		$metas = $query->orderBy('ma.id', 'asc')->paginate(10);

        $agencias = DB::select("SELECT id, nombre from personas where id_tipo_persona = 8 and activo = true and id_empresa = ".$this->getIdEmpresa()." order by nombre asc");
        $metas = $query->orderBy('ma.id', 'desc')->paginate(10);
        
        //para mostrar el periodo
       

		return view('pages.mc.metas.historico')->with(['metas' => $metas, 'agencias' => $agencias]);
		
    }


    public function historicoAgenciasAjax(Request $request)
    {
        $id_empresa = $this->getIdEmpresa();
        $persona = strtoupper($request->input('nombre'));
        $denominacion = strtoupper($request->input('denominacion_comercial'));
        $ruc = $request->input('ruc');
        $vendedorId = $request->input('id_vendedor_agencia');
        $buscar = $request->input('buscar');
        
    
    
        $query = DB::table('personas AS p')
        ->leftJoin('facturas as f', 'p.id', '=', 'f.cliente_id')
        ->join('meta_agencia_historico as ma', 'p.id', '=', 'ma.id_vendedor_agencia')
        ->leftJoin('estados as estado', 'f.id_estado_factura', '=', 'estado.id')
        ->select([
        'ma.id',
        DB::raw('COALESCE(SUM(f.renta), 0) AS renta_bruta'),
        DB::raw('COALESCE(SUM(f.renta_comisionable), 0) AS renta_neta'),
        DB::raw('COALESCE(ma.meta_mensual, 0) AS meta_mensual'),
        DB::raw('COALESCE(SUM(f.total_bruto_factura), 0) AS renta_bruta_factura'),
        DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre_completo"),
        DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
            p_cotizacion := f.cotizacion_factura, 
            p_monto := COALESCE(f.total_bruto_factura, 0),
            p_id_moneda_costo := f.id_moneda_venta,
            p_id_moneda_venta := 143
        )), 0) as total_factura_cotizado'),
        DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
            p_cotizacion := f.cotizacion_factura, 
            p_monto := COALESCE(f.renta, 0),
            p_id_moneda_costo := f.id_moneda_venta,
            p_id_moneda_venta := 143
        )), 0) as renta_cotizada'),
        'ma.anho as periodo_meta',
        DB::raw("TO_CHAR(ma.fecha_modificacion, 'DD/MM/YYYY HH24:MI:SS') AS fecha_modificacion"),
        DB::raw('(COALESCE(SUM(get_monto_cotizacion_custom(
            p_cotizacion := f.cotizacion_factura, 
            p_monto := COALESCE(f.total_bruto_factura, 0),
            p_id_moneda_costo := f.id_moneda_venta,
            p_id_moneda_venta := 143
        )), 0) - COALESCE(ma.meta_mensual, 0)) as diferencia')
    ])
            ->where('p.id_empresa', $id_empresa)
            ->where('estado.id', '!=', 30)
            ->groupBy('ma.id', 'ma.meta_mensual', 'p.nombre', 'p.apellido','ma.anho', 'ma.fecha_modificacion');
    		
            if ($request->input('mes') != "") {
                $fechaPeriodo = explode(' ', $request->input('mes'));
                $mes = $fechaPeriodo[0];
                $anio = $fechaPeriodo[1];
                $numeroMes=$this->getMes($mes);
            
                // Convierte el nombre del mes a su número correspondiente
               // $numeroMes = date('m', strtotime("$mes"));
            
                // Crea objetos Carbon con el mes, el año y el primer día del mes
                $desdeFecha = Carbon::create($anio, $numeroMes, 1, 0, 0, 0);
                $hastaFecha = $desdeFecha->copy()->endOfMonth(); // Para obtener el último día del mes
            
                $query->whereBetween('f.fecha_hora_facturacion', [$desdeFecha, $hastaFecha])
                ->where('ma.anho', $anio);
            }
            
            

     
        
        if ($request->input('search.value')) {
            $searchValue = $request->input('search.value');
            $query->where(function ($query) use ($searchValue) {
                $query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'LIKE', '%' . $searchValue . '%');
            });
        }
    
        
        if ($vendedorId) {
			$query->where('ma.id_vendedor_agencia', $vendedorId); // Filtrar por el ID del vendedor
		}
    
       
    
        // if ($email) {
        //     $query->where('p.email', 'LIKE', '%' . $email . '%');
        // }
    
        $totalRecords = $query->count();

		$draw = $request->input('draw');
		$start = $request->input('start', 0);
		$length = $request->input('length', 10);
		$recordsFiltered = $totalRecords;
    
      //  $metas = $query->orderBy('ma.id_meta', 'asc')->offset($start)->limit($length)->get();
        $metas = $query->offset($start)->limit($length)->get();
       // $metas = $query->orderBy('ma.id_meta', 'desc')->paginate(10);
        //para mostrar el periodo
       
        
        return response()->json([
            'data' => $metas,
            'draw' => $draw,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $recordsFiltered,
        ]);
      
    }

    
    public function indexhistorico(Request $request){
		$id_empresa = $this->getIdEmpresa();
		$persona = strtoupper($request->input('nombre'));
		$denominacion = strtoupper($request->input('denominacion_comercial'));
		$email = $request->input('correo');
		$ruc = $request->input('ruc');
		$buscar = $request->input('buscar');
		
		//dd($inputPrestador);

		$query = DB::table('meta_agencia_historico AS metas')
		->select([
			'metas.id_meta',
			DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre"),
            'metas.meta_anual',
            'metas.meta_trimestral',
            'metas.meta_mensual',
            'metas.meta_semestral',
            'metas.anho as periodo',
            DB::raw("TO_CHAR(metas.fecha_modificacion, 'DD/MM/YYYY HH24:MI:SS') AS fecha_modificacion")
		])
		->leftJoin('personas AS p', 'p.id', '=', 'metas.id_vendedor_agencia')
    				->where('p.id_empresa', $id_empresa);

      
	
			if ($buscar) {
				$query->where(function ($query) use ($buscar) {
					$query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'like', '%' . $buscar . '%')
							->orWhere('p.documento_identidad', 'LIKE', '%' . $ruc . '%');;
				});
			}
			
			
		if ($persona) {
				$query->whereRaw("CONCAT(p.nombre, ' ', p.apellido) LIKE ?", ['%' . $persona . '%']);
		}

		if ($denominacion) {
			$query->where('p.denominacion_comercial', 'LIKE', '%' . $denominacion . '%');
		}

		if ($ruc) {
			$query->where('p.documento_identidad', 'LIKE', '%' . $ruc . '%');
		}
/*
		if ($email) {
			$query->where('p.email', 'LIKE', '%' . $email . '%');
		}*/
	
	
        $metas = $query->orderBy('metas.id_meta', 'desc')->paginate(10);
        $agencias = DB::select("SELECT id, nombre from personas where id_tipo_persona = 8 and activo = true and id_empresa = ".$this->getIdEmpresa()." order by nombre asc");
		return view('pages.mc.metas.indexhistorico')->with(['metas' => $metas,'agencias' => $agencias]);
		
	}

    
	public function indexhistoricoAjax(Request $request)
    {
        $id_empresa = $this->getIdEmpresa();
        $persona = strtoupper($request->input('nombre'));
        $denominacion = strtoupper($request->input('denominacion_comercial'));
        $ruc = $request->input('ruc');
        $vendedorId = $request->input('vendedor_id');
        $buscar = $request->input('buscar');
    
        $query = DB::table('meta_agencia_historico AS metas')
		->select([
			'metas.id_meta',
			DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre"),
            'metas.meta_anual',
            'metas.meta_trimestral',
            'metas.meta_mensual',
            'metas.meta_semestral',
            'metas.anho as periodo',
            DB::raw("TO_CHAR(metas.fecha_modificacion, 'DD/MM/YYYY HH24:MI:SS') AS fecha_modificacion")
		])
		->leftJoin('personas AS p', 'p.id', '=', 'metas.id_vendedor_agencia')
    				->where('p.id_empresa', $id_empresa);
    
        if ($request->input('search.value')) {
            $searchValue = $request->input('search.value');
            $query->where(function ($query) use ($searchValue) {
                $query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'LIKE', '%' . $searchValue . '%');
            });
        }
    
        if ($persona) {
            $query->whereRaw("CONCAT(p.nombre, ' ', p.apellido) LIKE ?", ['%' . $persona . '%']);
        }
        if ($vendedorId && $vendedorId != 1) {
            $query->where('id', $vendedorId); 
        }
    
         if ($ruc) {
             $query->where('p.documento_identidad', 'LIKE', '%' . $ruc . '%');
         }
    
        // if ($email) {
        //     $query->where('p.email', 'LIKE', '%' . $email . '%');
        // }
    
        $totalRecords = $query->count();
    
        $draw = $request->input('draw');
        $start = $request->input('start', 0);
        $length = $request->input('length', 10);
        $recordsFiltered = $totalRecords;
    
     //   $metas = $query->orderBy('metas.id_meta', 'desc')->offset($start)->limit($length)->get();
    
     
      //  $metas = $query->orderBy('ma.id_meta', 'asc')->offset($start)->limit($length)->get();
        $metas = $query->offset($start)->limit($length)->get();
    
        
        return response()->json([
            'data' => $metas,
            'draw' => $draw,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $recordsFiltered
        ]);
      
    }




}
