<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\TipoPersoneria;
use DB;


class TipoPersoneriaController extends Controller
{
	public function index(Request $request){
		$tipoPersoneria = TipoPersoneria::where('activo', true)->get();
		return view('pages.mc.tipoPersoneria.index')->with('tipoPersonerias', $tipoPersoneria);
	}	

	public function add(Request $request){
		return view('pages.mc.tipoPersoneria.add');
	}	

	public function doAddTipoPersoneria(Request $request){
		$empresa = new TipoPersoneria;
		$empresa->denominacion = $request->input('denominacion');
		$empresa->abreviatura  = $request->input('abreviatura');
		$empresa->activo = true;
		try{
			$empresa->save();
			flash('Se ha ingresado el Tipo Personeria exitosamente')->success();
			return redirect()->route('tipoPersonerias');
		} catch(\Exception $e){
			flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			return redirect()->back();
		}
	}

	public function edit(Request $request){
		$tipoPersoneria = TipoPersoneria::where('id', $request->input('id'))->get();
		return view('pages.mc.tipoPersoneria.edit')->with('tipoPersonerias', $tipoPersoneria);
	}	

	public function doEditTipoPersoneria(Request $request){
		DB::table('tipo_personeria')
					    ->where('id',$request->input('id'))
					    ->update([
								'denominacion'=>$request->input('denominacion'),
								'abreviatura'=>$request->input('abreviatura'),
					            ]);
		flash('Se ha editado el Tipo Personeria exitosamente')->success();
		return redirect()->route('tipoPersonerias');
	}	
	public function delete(Request $request){
		DB::table('tipo_personeria')
					    ->where('id',$request->input('id'))
					    ->update([
								'activo'=>false,
					            ]);
		flash('Se ha eliminado exitosamente')->error();
		return redirect()->route('tipoPersonerias');
	}	
}