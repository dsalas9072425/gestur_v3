<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\TipoTimbrado;
use App\Timbrado;
use App\SucursalEmpresa;
use App\OpTimbrado;
use App\Persona;
use Log;

class TimbradoController extends Controller
{
	private function getIdUsuario(){
        
        return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
  	}//function

	private function getIdEmpresa(){

	  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

	}//function

	public function index(Request $Request){


		return view('pages.mc.timbrados.index');
	}	

	public function add(Request $Request){
		$tipoTimbrado = TipoTimbrado::where('activo', true)->get();
     	//$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);

	$sucursalEmpresa = Persona::where('id_tipo_persona','21')->where('activo', true)->where('id_empresa',$this->getIdEmpresa())->get();
        $centroCostos = DB::table('centro_costo')->where('id_empresa',$this->getIdEmpresa())->orderBy('nombre','ASC')->get();

		return view('pages.mc.timbrados.add')->with(['tipoTimbrados'=>$tipoTimbrado, 'sucursalEmpresas'=>$sucursalEmpresa, 'centroCostos'=>$centroCostos]);
	}	

	public function doAddTimbrado(Request $request){
		$fecha_inicio = explode("/", $request->input('inicio'));
		$vencimiento = explode("/", $request->input('vencimiento'));
		$fecha_autorizacion = explode("/", $request->input('fecha_autorizacion'));

		$fecha_inicio_format = $fecha_inicio[2]."-".$fecha_inicio[1]."-".$fecha_inicio[0];
		$vencimiento_format = $vencimiento[2]."-".$vencimiento[1]."-".$vencimiento[0];
		$fecha_autorizacion_format = $fecha_autorizacion[2]."-".$fecha_autorizacion[1]."-".$fecha_autorizacion[0];

		try{
			DB::beginTransaction();

			DB::select("UPDATE timbrados SET activo = FALSE WHERE id_empresa =".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa." AND id_tipo_timbrado = ".$request->input('tipoTimbrado')." AND id_sucursal_empresa = ".$request->input('sucursal'));
			
			$timbrado = new Timbrado;
			$timbrado->id_sucursal_empresa= $request->input('sucursal');
			$timbrado->numero= $request->input('numero');
			$timbrado->fecha_inicio= $fecha_inicio_format;
			$timbrado->vencimiento=  $vencimiento_format;
			$timbrado->fecha_autorizacion=  $fecha_autorizacion_format;
			$timbrado->autorizacion= $request->input('autorizacion');
			$timbrado->establecimiento= $request->input('Establecimiento');
			$timbrado->expedicion= $request->input('expedicion');
			$timbrado->id_empresa= Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
			$timbrado->usuario_id= Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			$timbrado->id_tipo_timbrado= $request->input('tipoTimbrado');
			$timbrado->activo= true;
			$timbrado->created_at= date('Y-m-d h:m:s');
			$timbrado->numero_actual= $request->input('numeroActual');
			$timbrado->numero_inicial= $request->input('numeroInicio');
			$timbrado->numero_final= $request->input('numeroFinal');
			$timbrado->id_sucursal_contable= $request->input('centroCosto');
			$timbrado->save();



	    	flash('El timbrado se guardo exitosamente!!')->success();
	    	DB::commit();

            return redirect()->route('indexTimbrado');
		} catch(\Exception $e){
			Log::error($e);
			DB::rollBack();
	    	flash('El timbrado no se guardo. Intentelo nuevamente !!')->error();
            return redirect()->back();
		}
	}	

	public function edit(Request $Request, $id){	
		flash('No tiene los permisos para acceder a esta pagina')->error();
		return redirect()->route('indexTimbrado');
		$timbrado = Timbrado::findOrFail($id);	
		$tipoTimbrado = TipoTimbrado::where('activo', true)->get();
		$sucursalEmpresa = Persona::where('id_tipo_persona','21')->where('activo', true)->where('id_empresa',$this->getIdEmpresa())->get();

        $centroCostos = DB::table('centro_costo')->where('id_empresa',$this->getIdEmpresa())->orderBy('nombre','ASC')->get();

		return view('pages.mc.timbrados.edit')->with(['tipoTimbrados'=>$tipoTimbrado, 'sucursalEmpresas'=>$sucursalEmpresa, 'centroCostos'=>$centroCostos, 'timbrado'=>$timbrado]);
	}

	public function getTimbrados(Request $Request){	
 		$timbrados = DB::table('timbrados as t');
        $timbrados = $timbrados->select('t.*',
        								'p.nombre as usuario',
        								'tt.denominacion as tipoTimbrado',
        								'cc.nombre as sucursalContable',
        								'se.nombre as sucursal'
        								); 
        $timbrados = $timbrados->leftJoin('personas as p', 'p.id', '=','t.usuario_id');
        $timbrados = $timbrados->leftJoin('tipo_timbrado as tt', 'tt.id', '=','t.id_tipo_timbrado');
        $timbrados = $timbrados->leftJoin('centro_costo as cc', 'cc.id', '=','t.id_sucursal_contable');
        $timbrados = $timbrados->leftJoin('personas as se', 'se.id', '=','t.id_sucursal_empresa');

        $timbrados = $timbrados->where('t.id_empresa',$this->getIdEmpresa());  
        $timbrados = $timbrados->get();
        return response()->json($timbrados);
	}

	public function doEditTimbrado(Request $request){	
		
		if($request->input('activo') == 1){
			$activo = true;
		}else{
			$activo = false;
		}
		$fecha_inicio = explode("/", $request->input('inicio'));
		$vencimiento = explode("/", $request->input('vencimiento'));
		$fecha_autorizacion = explode("/", $request->input('fecha_autorizacion'));

		$fecha_inicio_format = $fecha_inicio[2]."-".$fecha_inicio[1]."-".$fecha_inicio[0];
		$vencimiento_format = $vencimiento[2]."-".$vencimiento[1]."-".$vencimiento[0];
		$fecha_autorizacion_format = $fecha_autorizacion[2]."-".$fecha_autorizacion[1]."-".$fecha_autorizacion[0];

		try{
			DB::table('timbrados')
					        ->where('id',$request->input('id'))
					        ->update([
					            	'id_sucursal_empresa'=>$request->input('sucursal'),
					            	'numero'=>$request->input('numero'),
					            	'fecha_inicio'=>$fecha_inicio_format,
					            	'vencimiento'=>$vencimiento_format,
					            	'establecimiento'=>$request->input('Establecimiento'),
					            	'expedicion'=>$request->input('expedicion'),
					            	'id_tipo_timbrado'=>$request->input('tipoTimbrado'),
					            	//'numero_actual'=>$request->input('numeroActual'),
					            	'numero_inicial'=>$request->input('numeroInicio'),
					            	'numero_final'=>$request->input('numeroFinal'),
					            	'id_sucursal_contable'=>$request->input('centroCosto'),
									'autorizacion'=>$request->input('autorizacion'),
									'fecha_autorizacion'=>$fecha_autorizacion_format,
					            	'activo'=>$activo
					            	]);
	    	flash('El timbrado se actualizo exitosamente!!')->success();
	    	DB::commit();
            return redirect()->route('indexTimbrado');
		} catch(\Exception $e){
			DB::rollBack();
	    	flash('El timbrado no se actualizo. Intentelo nuevamente !!')->error();
            return redirect()->back();
		}
			
	}	

	public function buscarTimbrado(Request $Request){	
		$proveedor = Persona::where('id_tipo_persona','14')->where('id_empresa',$this->getIdEmpresa())->get();
		$permisos_especiales = false;
	    $btn = DB::select("SELECT pe.* 
                          FROM persona_permiso_especiales p, permisos_especiales pe
                          WHERE p.id_permiso = pe.id 
                          AND p.id_persona = ".$this->getIdUsuario() ." 
                          AND pe.url = 'buscarTimbrado'");

	  	if(!empty($btn)){
	  		$permisos_especiales = true;
	  	}	

		return view('pages.mc.timbrados.buscar')->with(['getProveedor'=>$proveedor, 'permiso'=>$permisos_especiales]);
	}

	public function getTimbradosCargados(Request $request){	
		  $opTimbrado = OpTimbrado::query();
          $opTimbrado = $opTimbrado->with('proveedor');
          if($request->input('numero') != ''){
               $opTimbrado = $opTimbrado->where('nro_timbrado',$request->input('numero'));  
           }
           if($request->input('proveedor') != ''){
               $opTimbrado = $opTimbrado->where('id_persona',$request->input('proveedor'));  
           }
          
        $opTimbrado = $opTimbrado->where('id_empresa',$this->getIdEmpresa());
        $opTimbrado = $opTimbrado->get();

		return response()->json($opTimbrado);
	}	


	public function getModificarTimbrados(Request $request){	
		$timbrado = OpTimbrado::where('id',$request->input('id_timbrado'))->get(['id','nro_timbrado','fecha_inicio','fecha_vencimiento']);;
		return response()->json($timbrado);
	}	

	public function doUpdateTimbrado(Request $request){
		$mensaje = new \StdClass; 

		$desde = explode("/", $request->input('fecha_creacion'));
		$hasta = explode("/", $request->input('fecha_vencimiento'));
		$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0];
		$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0];
		try{
			DB::table('op_timbrados')
					        ->where('id',$request->input('idMovimiento'))
					        ->update([
					            	'nro_timbrado'=>$request->input('numero_timbrado'),
					            	'fecha_inicio'=>$fecha_desde,
					            	'fecha_vencimiento'=>$fecha_hasta
					            	]);
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se ha modificado el timbrado exitosamente';
		} catch(\Exception $e){
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se ha podido modificar el timbrado. Intentelo nuevamente !!';
		}
		return response()->json($mensaje);
	}	

}	  
