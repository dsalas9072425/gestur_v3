<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\TipoPersona;
use DB;


class TipoPersonaController extends Controller
{
	public function index(Request $request){
		$tipoPersona = TipoPersona::where('activo', true)->get();
		return view('pages.mc.tipoPersona.index')->with('tipoPersonas', $tipoPersona);
	}	

	public function add(Request $request){
		return view('pages.mc.tipoPersona.add');
	}	

	public function doAddTipoPersona(Request $request){
		$empresa = new TipoPersona;
		$empresa->denominacion = $request->input('denominacion');
		$empresa->activo = true;
		try{
			$empresa->save();
			flash('Se ha ingresado el Tipo Persona exitosamente')->success();
			return redirect()->route('tipoPersonas');
		} catch(\Exception $e){
			flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			return redirect()->back();
		}
	}

	public function edit(Request $request){
		$tipoPersona = TipoPersona::where('id', $request->input('id'))->get();
		return view('pages.mc.tipoPersona.edit')->with('tipoPersonas', $tipoPersona);
	}	

	public function doEditTipoPersona(Request $request){
		DB::table('tipo_persona')
					    ->where('id',$request->input('id'))
					    ->update([
								'denominacion'=>$request->input('denominacion'),
					            ]);
		flash('Se ha editado el Tipo Persona exitosamente')->success();
		return redirect()->route('tipoPersonas');
	}	
	public function delete(Request $request){
		DB::table('tipo_persona')
					    ->where('id',$request->input('id'))
					    ->update([
								'activo'=>false,
					            ]);
		flash('Se ha eliminado exitosamente')->error();
		return redirect()->route('tipoPersonas');
	}	
}