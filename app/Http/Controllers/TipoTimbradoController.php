<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\TipoTimbrado;
use DB;


class TipoTimbradoController extends Controller
{
	public function index(Request $request){
		$tipoTimbrado = TipoTimbrado::where('activo', true)->get();
		return view('pages.mc.tipoTimbrado.index')->with('tipoTimbrados', $tipoTimbrado);
	}	

	public function add(Request $request){
		return view('pages.mc.tipoTimbrado.add');
	}	

	public function doAddTipoTimbrado(Request $request){
		$empresa = new TipoTimbrado;
		$empresa->denominacion = $request->input('denominacion');
		$empresa->activo = true;
		try{
			$empresa->save();
			flash('Se ha ingresado el Tipo Timbrado exitosamente')->success();
			return redirect()->route('tipoTimbrados');
		} catch(\Exception $e){
			flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			return redirect()->back();
		}
	}

	public function edit(Request $request){
		$tipoTimbrado = TipoTimbrado::where('id', $request->input('id'))->get();
		return view('pages.mc.tipoTimbrado.edit')->with('tipoTimbrados', $tipoTimbrado);
	}	

	public function doEditTipoTimbrado(Request $request){
		DB::table('tipo_persona')
					    ->where('id',$request->input('id'))
					    ->update([
								'denominacion'=>$request->input('denominacion'),
					            ]);
		flash('Se ha editado el Tipo Timbrado exitosamente')->success();
		return redirect()->route('tipoTimbrados');
	}	
	public function delete(Request $request){
		DB::table('tipo_persona')
					    ->where('id',$request->input('id'))
					    ->update([
								'activo'=>false,
					            ]);
		flash('Se ha eliminado exitosamente')->error();
		return redirect()->route('tipoTimbrados');
	}	
}