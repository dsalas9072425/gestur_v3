<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\HistoricoSaldoBanco;
use App\HistoricoCierreCaja;
use App\HistoricoCierreCajaDetalle;
use App\BancoCabecera;



class HistoricoController extends Controller
{    

    public function historicoCuentaBanco(Request $req)
	{

		$idUsuario = $this->getIdUsuario();
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'indexBancos'");

	 	if(empty($permisos)){
	    	return response()->json(['data'=>[]]);
		} 


		$id_empresa = $this->getIdEmpresa();
 
		$bancos = new HistoricoSaldoBanco;
        $bancos = $bancos->select(
							DB::raw("to_char(created_at, 'DD/MM/YYYy') as fecha_data"),
							DB::raw('saldo as saldodetalle'),
							DB::raw('nombre as banco_n'),
						    'numero_cuenta',
							'currency_code',
							DB::raw('tipo_cuenta as tipo_cuenta_banco_n'),
							'id_banco_cabecera',
							DB::raw("to_char(created_at,'DD/MM/YYYY') as fecha_data"),
							DB::raw("'historial' as tipo_data")
						  );
	 	$bancos = $bancos->where('id_empresa',$id_empresa);


        if($req->input('id_banco')){
			$bancos = $bancos->where('id_banco_cabecera',$req->input('id_banco'));
		}

        if($req->input('tipo_cuenta')){
			$bancos = $bancos->where('cuenta_bancaria',$req->input('tipo_cuenta'));
		}

		if($req->input('tipo_cuenta_bancaria')){

			if($req->input('tipo_cuenta_bancaria') == 'operativa'){
				$bancos = $bancos->where('tipo_cuenta','Cuenta Operativa');
			} else {
				$bancos = $bancos->where('tipo_cuenta','<>','Cuenta Operativa');
			}
		}


	
		if($req->input('periodo_fecha')){
            $fecha = explode(' - ', $req->input('periodo_fecha'));
            $desde     = $this->formatoFechaEntrada($fecha[0]).' 00:00:00';
            $hasta     = $this->formatoFechaEntrada($fecha[1]).' 23:59:59';
			$bancos = $bancos->whereBetween('created_at', array($desde,$hasta));
		}

		if($req->input('fecha')){
            $fecha = explode(' - ', $req->input('fecha'));
            $desde     = $this->formatoFechaEntrada($fecha[0]).' 00:00:00';
            $hasta     = $this->formatoFechaEntrada($fecha[0]).' 23:59:59';
			$bancos = $bancos->whereBetween('created_at', array($desde,$hasta));
		}

		$bancos = $bancos->orderByRaw('CASE WHEN cuenta_bancaria THEN 1 ELSE 2 END , nombre, currency_code ASC');
		$bancos = $bancos->get();
    
		return response()->json(['data'=>$bancos]);
	}//

	
	public function cierre(Request $req){

		$query = "SELECT DISTINCT concat(personas.nombre || ' ' || personas.apellido) as nombre_apellido, personas.id, cierre_caja.id_empresa
				  FROM cierre_caja, personas 
				  WHERE cierre_caja.usuario_cierre = personas.id
				  AND cierre_caja.id_empresa =".$this->getIdEmpresa();
		$usuarios = DB::select($query);		
		/*echo '<pre>';
				print_r($usuarios);	*/							
		return view('pages.mc.reportes.historicoCierre')->with(['usuarios'=>$usuarios]);
	}

	public function getCierre(Request $request){
			// dd($request->all());
		$cierres = HistoricoCierreCaja::select(['*',DB::raw('DATE(created_at) as created_at_format')])->with('usuario','usuarioAnulacion')->where(function($query) use($request) 
		 {
		   if(!empty($request->input('periodo')))
		   {
			   $fechaTrim = trim($request->input('periodo'));
			   $periodo = explode('-', trim($fechaTrim));
			   $desde = explode("/", trim($periodo[0]));
			   $hasta = explode("/", trim($periodo[1]));
			   $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0];
			   $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0];
			   $query->whereBetween(DB::raw('DATE(created_at)'), [$fecha_desde, $fecha_hasta]);
		   }
		   if(!empty($request->input('usuario')))
		   {	
			   $query->where('id_usuario_cierre',$request->input('usuario'));          
		   }	
		   $query->where('id_empresa',$this->getIdEmpresa());
		 })->get();

		return response()->json($cierres);
   }

   public function getResumenCierre(Request $request){
		$cierreResumen = DB::select("SELECT 
			fcc.denominacion,
			cu.currency_code,
			hccd.monto,
			fcc.tipo
			FROM historico_cierre_caja_detalle hccd
			JOIN currency cu ON cu.currency_id = hccd.moneda_id
			JOIN forma_cobro_cliente fcc ON fcc.id = hccd.forma_cobro_id
			WHERE hccd.id_historico_cierre_caja_cabecera = ?
			ORDER BY fcc.tipo = 'VALORES' DESC, fcc.denominacion DESC, cu.currency_code DESC
		",[$request->input('id')]);
		
		return response()->json($cierreResumen);
   }
   
   public function reporteCierrePdf(Request $req, $id){
		$cierreCajas = DB::table('vw_historico_cierre_caja');
		$cierreCajas = $cierreCajas->where('id',$id);
		$cierreCajas = $cierreCajas->get();

		
		$cierreCajaDetalles = DB::table('vw_historico_resumen_cierre_caja');
		$cierreCajaDetalles = $cierreCajaDetalles->where('id_historico_cierre_caja_cabecera',$cierreCajas[0]->id);
		$cierreCajaDetalles = $cierreCajaDetalles->get();

		$pdf = \PDF::loadView('pages.mc.cobranzas.historicoResumenCaja',compact('cierreCajas', 'cierreCajaDetalles'));
		$pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true,'enable_css_float'=>true]);
		$pdf->setPaper('a4', 'letter')->setWarnings(false);
		return $pdf->download('resumen_historial_cierre.pdf');

	}


    private function getIdUsuario()
	{
		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	}
  
	private function getIdEmpresa()
	{
	 return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}

           /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
	private function formatoFechaEntrada($date)
	{
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
	}//function


}
