<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Traits\RecibosTrait;
use App\Persona;
use App\Anticipo;
use App\Currency;
use App\CuentaCorriente;
use App\Negocio;
use App\Grupos;
use App\Divisas;
use Session;
use Redirect;
use Response;
use Image; 
use DB;


class CuentaCorrienteController extends Controller {

	use RecibosTrait;

	private function getIdUsuario(){
  
		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	  }
	  
	  private function getIdEmpresa(){
	  
	   return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	  }

	  public function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	  }
  
  
	public function indexCttaCorriente(Request $request)
	{
        $personas = DB::select("
			SELECT p.id,CONCAT(p.nombre, p.apellido,'-'||p.documento_identidad,'-'||tp.denominacion) AS data_n
			FROM personas p
			JOIN tipo_persona tp on tp.id = p.id_tipo_persona
			WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
			AND p.activo = true AND id_empresa = ".$this->getIdEmpresa());    					

		$lineaCredito = [];		
		$currencys = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();

    
		return view('pages.mc.cuentaCorriente.index', compact('personas', 'currencys', 'agencias', 'lineaCredito'));
	}	

	public function getCuentaCorriente(Request $req)
	{


		$data = DB::table(DB::raw("(SELECT cuenta_corriente.id_persona, personas.nombre, 
			sum(cuenta_corriente.debe) as debe,sum(cuenta_corriente.haber) as haber, 
			(SELECT CASE 
			 	WHEN cuenta_corriente.tipo = 'C' THEN sum(cuenta_corriente.debe) -  sum(cuenta_corriente.haber)
			 ELSE sum(cuenta_corriente.haber) - sum(cuenta_corriente.debe) END) as saldo,
			cuenta_corriente.tipo, 
			cuenta_corriente.id_moneda, currency.currency_code
			FROM cuenta_corriente, currency, personas
			WHERE cuenta_corriente.id_moneda = currency.currency_id
			AND cuenta_corriente.id_persona = personas.id
			AND cuenta_corriente.activo = true
			GROUP BY cuenta_corriente.id_persona, cuenta_corriente.tipo, cuenta_corriente.id_moneda,
			personas.nombre,currency.currency_code) as result"));

		if($req->input('tipo')){
			$data = $data->where('tipo',$req->input('tipo'));
		}

		if($req->input('persona')){
			$data = $data->where('id_persona',$req->input('persona'));
		}

		if($req->input('idMoneda')){
			$data = $data->where('id_moneda',$req->input('idMoneda'));
		}
	
		$data = $data->get();

		return response()->json(['data'=>$data]);
	}

	public function consultarDetalleCuentas(Request $request)
	{
		$datos = $this->responseFormatDatatable($request);

        $query = "SELECT cuenta_corriente.id_persona, personas.nombre, cuenta_corriente.debe as debe,
					cuenta_corriente.haber as haber, cuenta_corriente.saldo as saldo, cuenta_corriente.tipo, 
					cuenta_corriente.id_moneda, currency.currency_code, cuenta_corriente.fecha_hora, 
					cuenta_corriente.documento, tipo_documento_ctacte.denominacion
					FROM cuenta_corriente, currency, personas, tipo_documento_ctacte
					WHERE cuenta_corriente.id_moneda = currency.currency_id
					AND cuenta_corriente.id_persona = personas.id
					AND cuenta_corriente.id_tipo_documento = tipo_documento_ctacte.id
					AND cuenta_corriente.activo = true
					AND cuenta_corriente.id_persona = '".$datos->persona_id."'
					AND id_moneda = '".$datos->moneda_id."'
					AND cuenta_corriente.tipo = '".$datos->tipo."'";

		


		$totales = DB::select("SELECT tipo, SUM(debe) as total_debe, SUM(haber) as total_haber, 
								(	SELECT CASE WHEN cuenta_corriente.tipo = 'C' THEN SUM(cuenta_corriente.debe) -  SUM(cuenta_corriente.haber)
									ELSE SUM(cuenta_corriente.haber) - SUM(cuenta_corriente.debe) END
								) AS saldo
								FROM cuenta_corriente
								WHERE id_persona = '".$datos->persona_id."' AND activo = true AND tipo = '".$datos->tipo."' 
									AND id_moneda = '".$datos->moneda_id."'
								GROUP BY tipo");

		foreach ($totales as $key => $total) 
		{
			$totales[$key]->total_debe = number_format($total->total_debe, 2, ",", ".");
			$totales[$key]->total_haber = number_format($total->total_haber, 2, ",", ".");
			$totales[$key]->saldo = number_format($total->saldo, 2, ",", ".");
		}
		
        if($datos->periodo != ''){
        	$fechaPeriodo = explode(' - ', $datos->periodo);
        	$desde     = $this->formatoFechaEntrada($fechaPeriodos[0]);
            $hasta     = $this->formatoFechaEntrada($fechaPeriodos[1]);
            $query .= " AND cuenta_corriente.fecha BETWEEN '".$desde."' AND '".$hasta."'";
		}
		
		$query .= ' ORDER BY cuenta_corriente.id ASC';
		$resultado = DB::select($query);
		// dd($resultado);

		return response()->json(['data'=>$resultado, 'totales'=>$totales[0]]);
	}	


	/**
	 * Recibe como parametro el tipo de persona del reporte de cuenta corriente
	 * Retorna las personas del tipo solicitado por ajax
	 */
	public function getTipoPersona(Request $req)
	{
		$err = true;
		$result = [];

		try{
			if($req->input('tipo') == 'C'){
				//CLIENTES
				$result = DB::select("SELECT p.id,CONCAT(p.nombre, p.apellido,'-'||p.documento_identidad,'-'||tp.denominacion) AS data_n
									  FROM personas p
									  JOIN tipo_persona tp on tp.id = p.id_tipo_persona
									  WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
									  AND p.activo = true AND id_empresa = ".$this->getIdEmpresa());

			} else if($req->input('tipo') == 'P'){
				//SOLO PROVEEDOR
				$result = DB::select("SELECT p.id,CONCAT(p.nombre, p.apellido,'-'||p.documento_identidad,'-'||tp.denominacion) AS data_n
										FROM personas p
										JOIN tipo_persona tp on tp.id = p.id_tipo_persona
										WHERE p.id_tipo_persona = 14
										AND p.activo = true AND id_empresa = ".$this->getIdEmpresa());
			}
			
		} catch(\Exception $e){
			$err = false;
		 }
		 	return response()->json(['err'=>$err,'result'=>$result]);
	}




	/**
	 * Funcion que recibe datos array del datatable 
	 * ! Esta funcion ya no es necesaria y debe ser cambiada usando el serializeJSON ****
	 */
	private function  responseFormatDatatable($req)
	  	{
		    $datos = array();
		    $data =  new \StdClass;
		    
		    foreach ($req->formSearch as $key => $value) 
		    {
		     	$n = $value['name'];
		        $datos[$value['name']] = $value['value'];
		        $data-> $n = $value['value'];
		    }

		    return $data;
 	} 





	private function formatoFechaEntrada($date)
	{
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }//function



	public function cuentaCliente(Request $req)
	{
		$cliente = DB::select("SELECT * FROM personas 
							  WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
							  							WHERE puede_facturar = true) 
							  AND id_empresa = ".$this->getIdEmpresa()." order by nombre ASC");

		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$vendedores = Persona::whereIn('id_tipo_persona',array(2, 4, 3))->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
		$negocios = Negocio::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
		$grupos = Grupos::where('estado_id', 11)
						->where('empresa_id',$this->getIdEmpresa())
						->get(['id','denominacion']);

		return view('pages.mc.cuentaCorriente.cuentaCliente',compact('currency','cliente','vendedores','negocios','grupos'));

	}


	public function getCuentaCorrienteCliente(Request $req)
	{
		$exacto =  DB::table('vw_cuenta_corriente_cliente_resumen');
		if($req->input("idCliente") != ''){
			$exacto =  $exacto->where('id_cliente',$req->input("idCliente"));
		}
		if($req->input("idMoneda") != ''){
			$exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
		}
		$exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
		$exacto =  $exacto->get();
		$resultado = [];
        $contador = 0;
		$listado = [];
        foreach($exacto as $key=>$valor){
			$extractoDetalles =  DB::table('vw_cuenta_corriente_cliente');
			if($req->input("fecha_emision") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
			}
			if($req->input("fecha_vencimiento") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
			}
			if($req->input("negocio") != ''){
				$extractoDetalles =  $extractoDetalles->where('id_unidad_negocio',$req->input("negocio"));
			}	
			if($req->input("grupo_id") != ''){
				$extractoDetalles =  $extractoDetalles->where('id_grupo',$req->input("grupo_id"));
			}	
			if($req->input("idVendedor") != ''){
				$extractoDetalles =  $extractoDetalles->where('vendedor_id',$req->input("idVendedor"));
			}	

			$extractoDetalles =  $extractoDetalles->where('id_cliente',$valor->id_cliente);
			$extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
			$extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
			$extractoDetalles =  $extractoDetalles->orderBy('id_tipo_documento', 'DESC');
			$extractoDetalles =  $extractoDetalles->get();
			/*echo '<pre>';
			print_r($extractoDetalles);*/
			if(isset($extractoDetalles[0]->nro_documento)){
				$resultado[$key] = $valor;
				$detalles = [];
				foreach($extractoDetalles as $keys=>$detalle){
					$tipo_documento = $detalle->tipo_documento;
					$detalles[$keys]['nro_documento'] = $detalle->nro_documento;
					$detalles[$keys]['tipo_documento'] = $tipo_documento;
					$detalles[$keys]['moneda'] = $detalle->moneda;
					$detalles[$keys]['fecha_emision'] = date('d/m/Y',strtotime($detalle->fecha_emision));
					$detalles[$keys]['fecha_vencimiento'] =date('d/m/Y',strtotime($detalle->fecha_vencimiento));
					$detalles[$keys]['check_in'] = date('d/m/Y',strtotime($detalle->check_in));
					$detalles[$keys]['atraso'] = $detalle->atraso;
					$detalles[$keys]['corte'] = $detalle->corte;
					$detalles[$keys]['importe'] = $detalle->importe_bruto;
					$detalles[$keys]['total_comision'] = $detalle->total_comision;
					$detalles[$keys]['importe_neto'] = $detalle->total_neto_documento;
					$detalles[$keys]['saldo_neto'] = $detalle->saldo_neto;
					$detalles[$keys]['senha'] = $detalle->senha;
					if($detalle->proforma != ""){
						$detalles[$keys]['proforma'] = $detalle->proforma;
					}else{
						$detalles[$keys]['proforma'] = $detalle->id_venta_rapida;
					}
					if($detalle->vendedor != " "){
						$detalles[$keys]['vendedor'] = $detalle->vendedor;
					}else{
						$detalles[$keys]['vendedor'] = $detalle->vendedor_rapida;
					}
					$detalles[$keys]['pasajero'] = $detalle->pasajero_online;
					$detalles[$keys]['ticketcount'] = $detalle->ticketcount;
					$detalles[$keys]['usuario_proforma'] = $detalle->vendedor_rapida;
				}
				$resultado[$key]->detalles = $detalles;
			}
		}
        foreach($resultado as $key=>$st){
            $listado['data'][] = $st;	
        }

        if(empty($listado)){
                $listado['data'][] = $listado;
        }
        return response()->json($listado);
	}

	public function getTotalesResumenCliente(Request $req)
	{
		$exacto =  DB::table('vw_cuenta_corriente_cliente');
		$exacto =  $exacto->where('id_cliente',$req->input("idCliente"));
		if($req->input("idMoneda") != ''){
			$exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
		}
		$exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
		$exacto =  $exacto->orderBy('id_moneda', 'DESC');
		$exacto =  $exacto->get();
		$importeGs = 0;
		$totalComisionGs = 0;
		$importeNetoGs = 0;
		$saldoNetoGs = 0;
		$importeUs = 0;
		$totalComisionUs = 0;
		$importeNetoUs = 0;
		$saldoNetoUs = 0;
		$arrayResultado = array();
		foreach($exacto as $cuenta){
			if($cuenta->id_moneda == 143){
				$importeUs = $importeUs + (float)$cuenta->importe;
				$totalComisionUs = $totalComisionUs + (float)$cuenta->total_comision;
				$importeNetoUs = $importeNetoUs + (float)$cuenta->importe_neto;
				$saldoNetoUs = $saldoNetoUs + (float)$cuenta->saldo_neto;
			}else{
				$importeGs = $importeGs + (float)$cuenta->importe;
				$totalComisionGs = $totalComisionGs + (float)$cuenta->total_comision;
				$importeNetoGs = $importeNetoGs + (float)$cuenta->importe_neto;
				$saldoNetoGs = $saldoNetoGs + (float)$cuenta->saldo_neto;
			}
		}
		$arrayResultado[111]['importe'] = $importeGs;
		$arrayResultado[111]['comision'] = $totalComisionGs;
		$arrayResultado[111]['importe_neto'] = $importeNetoGs;
		$arrayResultado[111]['saldo'] = $saldoNetoGs;
		$arrayResultado[143]['importe'] = $importeNetoUs;
		$arrayResultado[143]['comision'] = $totalComisionUs;
		$arrayResultado[143]['importe_neto'] = $importeNetoUs;
		$arrayResultado[143]['saldo'] = $saldoNetoUs;
		return response()->json($arrayResultado);
	}

	public function getResumenCliente(Request $req)
	{
		$exacto =  DB::table('vw_cuenta_corriente_cliente_resumen');
		$exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
		$exacto =  $exacto->get();
		return response()->json(['data'=>$exacto]);
	}

	public function guardarAnticipoFactura(Request $req)
	{
		$anticipo = Anticipo::with('recibo')->where('id',$req->input('anticipo_id'))->first();
		if($anticipo && isset($anticipo->recibo)){

			//Si no es aplicado retornamos error
			if($anticipo->recibo->id_estado != 56){
				return response()->json('Error, El recibo debe estar aplicado');
			}
		}

		$anticipo_resp = $this->aplica_anticipo_factura(
			(int)$req->input('id_facturas'),
			(float)str_replace(',','.', str_replace('.','',$req->input('saldo_credito'))),
			(int)$req->input('anticipo_id'),
			$this->getIdUsuario()
		);
		return response()->json($anticipo_resp);
	}

	public function regenerarAsientoAnticipoFactura($id_empresa)
	{
		$n = $this->regenerarAsientoAplicacion($id_empresa);
		dd('FIN');
	}

	public function cuentaProveedor(Request $req)
	{
		$cliente = DB::select("SELECT * FROM personas 
							  WHERE id_tipo_persona in (14,8,11)
							  AND id_empresa = ".$this->getIdEmpresa()." order by nombre ASC");
		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();

		return view('pages.mc.cuentaCorriente.cuentaProveedor',compact('currency','cliente'));

	}

	public function getCuentaCorrienteProveedor(Request $req)
	{

		$exacto =  DB::table('vw_cuenta_corriente_proveedor_resumen');
		if($req->input("idProveedor") != ''){
			$exacto =  $exacto->where('id_proveedor',$req->input("idProveedor"));
		}
		if($req->input("idMoneda") != ''){
			$exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
		}
		if($req->input("proveedor_existencia") != ''){
			if($req->input("proveedor_existencia") == 'SI'){
				$exacto =  $exacto->where('id_pais', '<>', 1455);
			}else{
				$exacto =  $exacto->where('id_pais', 1455);
			}
		}

		$exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
		$exacto =  $exacto->get();
		$resultado = [];
        $contador = 0;
		$listado = [];
        foreach($exacto as $key=>$valor){
			$extractoDetalles =  DB::table('vw_cuenta_corriente_proveedor');
			if($req->input("fecha_emision") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
			}
			if($req->input("fecha_vencimiento") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
			}
			$extractoDetalles =  $extractoDetalles->where('id_proveedor',$valor->id_proveedor);
			$extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
			$extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
			$extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
			$extractoDetalles =  $extractoDetalles->get();
			if(isset($extractoDetalles[0]->nro_documento)){
				$resultado[$key] = $valor;
				$detalles = [];
				foreach($extractoDetalles as $keys=>$detalle){
					$detalles[$keys]['nro_documento'] = $detalle->nro_documento;
					$detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
					$detalles[$keys]['moneda'] = $detalle->moneda;
					$detalles[$keys]['fecha_emision'] = date('d/m/Y',strtotime($detalle->fecha_emision));
					$detalles[$keys]['fecha_vencimiento'] =date('d/m/Y',strtotime($detalle->fecha_vencimiento));
					$detalles[$keys]['fecha_de_gasto'] = date('d/m/Y',strtotime($detalle->fecha_de_gasto));
					$detalles[$keys]['atraso'] = $detalle->atraso;
			    	$detalles[$keys]['corte'] = $detalle->corte;
					$detalles[$keys]['importe'] = $detalle->importe;
					$detalles[$keys]['saldo_neto'] = $detalle->saldo;
					$detalles[$keys]['proforma'] = $detalle->proforma;
				}
				$resultado[$key]->detalles = $detalles;
			}
		}
        foreach($resultado as $key=>$st){
            $listado['data'][] = $st;	
        }

        if(empty($listado)){
                $listado['data'][] = $listado;
        }
        return response()->json($listado);
	}

	public function getResumenProveedor(Request $req)
	{
		$exacto =  DB::table('vw_cuenta_corriente_proveedor_resumen');
		$exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
		$exacto =  $exacto->get();
		return response()->json(['data'=>$exacto]);
	}

	public function cuentaClientePdf(Request $req)
	{
		$cliente = "";
		$fecha_emision = "";
		$fecha_vencimiento = "";
		$exacto =  DB::table('vw_cuenta_corriente_cliente_resumen');
		if($req->input("idCliente") != ''){
			$exacto =  $exacto->where('id_cliente',$req->input("idCliente"));
			$cliente = Persona::where('id', '=',$req->input("idCliente"))->first();
			$cliente = $cliente->nombre;
		}
		if($req->input("idMoneda") != ''){
			$exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
		}
		$exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
		$exacto =  $exacto->get();
		$resultado = [];
        $contador = 0;
		$listado = [];
        foreach($exacto as $key=>$valor){
			$extractoDetalles =  DB::table('vw_cuenta_corriente_cliente');
			if($req->input("fecha_emision") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
				$fecha_emision = $req->input("fecha_emision");
			}
			if($req->input("fecha_vencimiento") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$fecha_vencimiento = $req->input("fecha_vencimiento");
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
			}
			if($req->input("negocio") != ''){
				$extractoDetalles =  $extractoDetalles->where('id_unidad_negocio',$req->input("negocio"));
			}	
			if($req->input("grupo_id") != ''){
				$extractoDetalles =  $extractoDetalles->where('id_grupo',$req->input("grupo_id"));
			}	
			if($req->input("idVendedor") != ''){
				$extractoDetalles =  $extractoDetalles->where('vendedor_id',$req->input("idVendedor"));
			}	

			$extractoDetalles =  $extractoDetalles->where('id_cliente',$valor->id_cliente);
			$extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
			$extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
			$extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
			$extractoDetalles =  $extractoDetalles->get();
			/*echo '<pre>';
			print_r($extractoDetalles);*/
			if(isset($extractoDetalles[0]->nro_documento)){
				$resultado[$key] = $valor;
				$detalles = [];
				foreach($extractoDetalles as $keys=>$detalle){
					$detalles[$keys]['nro_documento'] = $detalle->nro_documento;
					$detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
					$detalles[$keys]['moneda'] = $detalle->moneda;
					$detalles[$keys]['fecha_emision'] = date('d/m/Y',strtotime($detalle->fecha_emision));
					$detalles[$keys]['fecha_vencimiento'] =date('d/m/Y',strtotime($detalle->fecha_vencimiento));
					$detalles[$keys]['check_in'] = date('d/m/Y',strtotime($detalle->check_in));
					$detalles[$keys]['atraso'] = $detalle->atraso;
					$detalles[$keys]['corte'] = $detalle->corte;
					//$detalles[$keys]['importe'] = $detalle->importe;
					$detalles[$keys]['importe'] = $detalle->importe_bruto;
					$detalles[$keys]['total_comision'] = $detalle->total_comision;
					//$detalles[$keys]['importe_neto'] = $detalle->importe_neto;
					$detalles[$keys]['importe_neto'] = $detalle->total_neto_documento;
					$detalles[$keys]['saldo_neto'] = $detalle->saldo_neto;
					$detalles[$keys]['senha'] = $detalle->senha;
					if($detalle->proforma != ""){
						$detalles[$keys]['proforma'] = $detalle->proforma;
					}else{
						$detalles[$keys]['proforma'] = $detalle->id_venta_rapida;
					}
					if($detalle->vendedor != " "){
						$detalles[$keys]['vendedor'] = $detalle->vendedor;
					}else{
						$detalles[$keys]['vendedor'] = $detalle->vendedor_rapida;
					}
					$detalles[$keys]['pasajero'] = $detalle->pasajero_online;
				}
				$resultado[$key]->detalles = $detalles;
			}
		}
        foreach($resultado as $key=>$st){
            $listado['data'][] = $st;	
        }

        if(empty($listado)){
                $listado['data'][] = $listado;
        }

		//return view('pages.mc.cuentaCorriente.cuentaPdf',compact('listado','cliente','moneda','fecha_emision','fecha_vencimiento'));
		$pdf = \PDF::loadView('pages.mc.cuentaCorriente.cuentaPdf',compact('listado','cliente','moneda','fecha_emision','fecha_vencimiento'));

		$pdf->setPaper('a4', 'landscape')->setWarnings(false);
		return $pdf->download('Cuentas por Cobrar'.$this->getId4Log().'.pdf');
	}




	public function cuentaProveedorPdf(Request $req)
	{
		$proveedor = "";
		$fecha_emision = "";
		$fecha_vencimiento = "";
		$exacto =  DB::table('vw_cuenta_corriente_proveedor_resumen');
		if($req->input("idProveedor") != ''){
			$exacto =  $exacto->where('id_proveedor',$req->input("idProveedor"));
			$proveedor = Persona::where('id', '=',$req->input("idProveedor"))->first();
			$proveedor = $proveedor->nombre;
		}
		if($req->input("idMoneda") != ''){
			$exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
		}
		$exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
		$exacto =  $exacto->get();
		$resultado = [];
        $contador = 0;
		$listado = [];
        foreach($exacto as $key=>$valor){
			$extractoDetalles =  DB::table('vw_cuenta_corriente_proveedor');
			if($req->input("fecha_emision") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
				$fecha_emision = $req->input("fecha_emision");
			}
			if($req->input("fecha_vencimiento") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
				$fecha_vencimiento = $req->input("fecha_vencimiento");
			}
			$extractoDetalles =  $extractoDetalles->where('id_proveedor',$valor->id_proveedor);
			$extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
			$extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
			$extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
			$extractoDetalles =  $extractoDetalles->get();
			if(isset($extractoDetalles[0]->nro_documento)){
				$resultado[$key] = $valor;
				$detalles = [];
				foreach($extractoDetalles as $keys=>$detalle){
					$detalles[$keys]['nro_documento'] = $detalle->nro_documento;
					$detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
					$detalles[$keys]['moneda'] = $detalle->moneda;
					$detalles[$keys]['fecha_emision'] = date('d/m/Y',strtotime($detalle->fecha_emision));
					$detalles[$keys]['fecha_vencimiento'] =date('d/m/Y',strtotime($detalle->fecha_vencimiento));
					$detalles[$keys]['atraso'] = $detalle->atraso;
			    	$detalles[$keys]['corte'] = $detalle->corte;
					$detalles[$keys]['importe'] = $detalle->importe;
					$detalles[$keys]['saldo_neto'] = $detalle->saldo;
					$detalles[$keys]['proforma'] = $detalle->proforma;
				}
				$resultado[$key]->detalles = $detalles;
			}
		}
        foreach($resultado as $key=>$st){
            $listado['data'][] = $st;	
        }

        if(empty($listado)){
                $listado['data'][] = $listado;
        }
		//return view('pages.mc.cuentaCorriente.cuentaProveedorPdf',compact('listado','proveedor','moneda','fecha_emision','fecha_vencimiento'));
		$pdf = \PDF::loadView('pages.mc.cuentaCorriente.cuentaProveedorPdf',compact('listado','proveedor','moneda','fecha_emision','fecha_vencimiento'));

		$pdf->setPaper('a4', 'landscape')->setWarnings(false);
		return $pdf->download('Cuentas por Cobrar'.$this->getId4Log().'.pdf');	
	}



	
}
