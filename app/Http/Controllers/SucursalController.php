<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use \App\Dtp\Proveedor;
use App\Sucursal;
use App\Agencias;
use Session;
use Redirect;
use DB;

class SucursalController extends Controller
{
	public function index(){
		$arrayAgencia = [];
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			$agencias = Agencias::all();
			$sucursales = Sucursal::with('agencia')
								->get();			
		}else{
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
			$sucursales = Sucursal::with('agencia')
								->where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
								->get();	
		}
		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}		

		$agenciaId = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
		return view('pages.sucursales.sucursales')->with(['sucursales'=>$sucursales, 'listadoAgencia'=>$arrayAgencia, 'agenciaId'=>$agenciaId]);
	}	

	public function add(Request $req){
		$arrayAgencia = [];
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			$agencias = Agencias::all();	
		}else{
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
		}
		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}
		return view('pages.sucursales.sucursalesAdd')->with('listadoAgencia', $arrayAgencia);
	}	

	public function edit(Request $request, $id){
		$sucursales = Sucursal::where('id_sucursal_agencia', '=',$id)->get();
		$arrayAgencia = [];
		
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			$agencias = Agencias::all();	
		}else{
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
		}
	
		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}
		return view('pages.sucursales.sucursalesEdit')->with(['sucursales'=>$sucursales, 'listadoAgencia'=>$arrayAgencia]);
	}	


	public function doAdd(Request $request){
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json' ]
		]);

		$iibReq = new \StdClass;
		$iibReq->reqToken = "ertetrytrey"; 
 		$iibReq->ts= date('Y-m-d h:m:s');
 		$sucursal = new \StdClass;
 		if(!ctype_space($request->input('descripcionAgencia'))){
 			$sucursal->nombre = $request->input('descripcionAgencia');
 		}else{
			flash('El nombre esta formado por espacios vacios')->error();
			return Redirect::back();
 		}
 			
 		if(!ctype_space($request->input('telefono'))){
 			$sucursal->telefono = $request->input('telefono');
 		}else{
			flash('El teléfono esta formado por espacios vacios')->error();
			return Redirect::back();
 		}
 		$sucursal->email = $request->input('email');
 		$sucursal->activo = (boolean)$request->input('activo');
 		$sucursal->idAgencia = (int)$request->input('agencia_id');
 		$iibReq->sucursal = $sucursal;
 		//print_r(json_encode($iibReq));
 		try{
	 		$iibRsp = $client->post(Config::get('config.iibRegSucursal'),
				['body' => json_encode($iibReq)]
			);
		}	
		catch(RequestException $e){
			return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			return view('pages.errorConexion');	
		}	

		$iibObjRsp = json_decode($iibRsp->getBody());
		//print_r(json_encode($iibObjRsp));

		if($iibObjRsp->codRetorno !=0){
			//TODO: armar template para mensaje de error
			flash($iibObjRsp->desRetorno)->error();
			return Redirect::back();
		}

		flash('Se ha ingresado la sucursal exitosamente')->success();
		return redirect()->route('sucursales');
	}	

	public function doEdit(Request $request){
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json' ]
		]);

		$iibReq = new \StdClass;
		$iibReq->reqToken = "ertetrytrey"; 
 		$iibReq->ts= date('Y-m-d h:m:s');
 		$sucursal = new \StdClass;
 		$sucursal->id = (int)$request->input('idSucursalAgencia');
 		if(!ctype_space($request->input('descripcionAgencia'))){
 			$sucursal->nombre = $request->input('descripcionAgencia');
 		}else{
			flash('El nombre esta formado por espacios vacios')->error();
			return Redirect::back();
 		}
 		if(!ctype_space($request->input('telefono'))){
 			$sucursal->telefono = $request->input('telefono');
 		}else{
			flash('El teléfono esta formado por espacios vacios')->error();
			return Redirect::back();
 		}
 		$sucursal->email = $request->input('email');
 		$sucursal->activo = (boolean)$request->input('activo');
 		$sucursal->idAgencia = (int)$request->input('agencia_id');
 		$iibReq->sucursal = $sucursal;
 		//print_r(json_encode($iibReq));
 		try{
	 		$iibRsp = $client->post(Config::get('config.iibActSucursal'),
				['body' => json_encode($iibReq)]
			);
		}	
		catch(RequestException $e){
			return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			return view('pages.errorConexion');	
		}	

		$iibObjRsp = json_decode($iibRsp->getBody());
		//print_r(json_encode($iibObjRsp));

		if($iibObjRsp->codRetorno !=0){
			//TODO: armar template para mensaje de error
			flash($iibObjRsp->desRetorno)->error();
			return Redirect::back();
		}
		flash('Se ha editado la sucursal exitosamente')->success();
		return redirect()->route('sucursales');
	}	

	public function getSucursales(Request $req){
		$sucursales = Sucursal::where('id_agencia', '=',$req->dataSucursal)->get();
		$arraySucursal = [];
		foreach ($sucursales as $key=>$sucursal){
			$arraySucursal[$key]['value']= $sucursal->id_sucursal_agencia;
			$arraySucursal[$key]['label']= $sucursal->descripcion_agencia;
		}
		return json_encode($arraySucursal);
	}	

	public function getAgenciasSucursales(Request $request){
		$agenciaId= $request->dataAgencia;
		$sucursales = Sucursal::with('agencia')
								->where('id_agencia',$agenciaId)
								->get();	
		return response()->json($sucursales);

	}
/*************************************************************************************************************/
/*									MC
/************************************************************************************************************/
public function indexMc(){
		$arrayAgencia = [];
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==7){
			$agencias = Agencias::all();
			$sucursales = Sucursal::with('agencia')
								->get();			
		}else{
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
			$sucursales = Sucursal::with('agencia')
								->where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
								->get();	
		}
		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}		

		$agenciaId = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
		return view('pages.mc.sucursales.sucursales')->with(['sucursales'=>$sucursales, 'listadoAgencia'=>$arrayAgencia, 'agenciaId'=>$agenciaId]);
	}	

	public function addMc(Request $req){
		$arrayAgencia = [];
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==7){
			$agencias = Agencias::all();	
		}else{
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
		}
		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}
		return view('pages.mc.sucursales.sucursalesAdd')->with('listadoAgencia', $arrayAgencia);
	}	

	public function editMc(Request $request, $id){
		$sucursales = Sucursal::where('id_sucursal_agencia', '=',$id)->get();
		$arrayAgencia = [];
		
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==7){
			$agencias = Agencias::all();	
		}else{
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
		}
	
		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}
		return view('pages.mc.sucursales.sucursalesEdit')->with(['sucursales'=>$sucursales, 'listadoAgencia'=>$arrayAgencia]);
	}	

	/*
	Hacer Agregar
	Función para agregar los datos de usuario
	*/
	public function doAddMc(Request $request){
			$id = Sucursal::select('id_sucursal_agencia')->max('id_sucursal_agencia');
			$sucursal = new Sucursal;
			/* Se comprueba si los datos solo con espacios en blanco*/
			if(!ctype_space($request->input('descripcionAgencia'))){
				$descripcionAgencia = $request->input('descripcionAgencia');
			}else{
				flash('El nombre esta formado por espacios vacios')->error();
				return Redirect::back();
			}
			$sucursal->id_sucursal_agencia = $id+1;
			$sucursal->descripcion_agencia = $descripcionAgencia;
			$sucursal->id_agencia = $request->input('agencia_id');
			$sucursal->telefono = $request->input('telefono');
			if($request->input('activo') == 'true') $sucursal->activo = 'S';
			else $sucursal->activo = 'N';
			$sucursal->email = $request->input('email');
			try{
				$sucursal->save();
				flash('Se ha ingresado la sucursal exitosamente')->success();
			} catch(\Exception $e){
				flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			}

		//return "deluxe";
		return redirect()->route('mc.sucursales');
	}

	public function doEditMc(Request $request){
		
		if($request->input('activo') == 'true') $activo = 'S';
		else $activo = 'N';
		DB::table('sucursales')
						    ->where('id_sucursal_agencia',$request->input('idSucursalAgencia'))
						    ->update([
						            'descripcion_agencia'=>$request->input('descripcionAgencia'),
						            'email'=>$request->input('email'),
						            'telefono'=>$request->input('telefono'),
						            'activo' => $activo
						            ]);
		flash('Se ha editado la sucursal exitosamente')->success();
		return redirect()->route('mc.sucursales');
	}	
}