<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use Session;
use App\ManualFile;
use Illuminate\Support\Facades\Validator;


class ManualController extends Controller
{
    //
    public function verActualizacionesManual(){
        return view('pages.mc.manual.index');
    }

    public function addManual(){
        return view('pages.mc.manual.addManual');
    }
    public function subirManual(Request $request){

        if ($request->hasFile('pdf')) {
            //validamos la extension del archivo en este caso solo puede ser PDF
            $validacion = Validator::make($request->all(), [
                'pdf' => 'required|mimes:pdf',
            ]);
            
            if ($validacion->fails()) {
                return response()->json(array('estado'=>2));
            }

            //datos para identificar el archivo subido por cada usuario y fecha
            $hoy=date('Y-m-d');
            $id_usuario=Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
            //recibimos el archivo
            $archivo = $request->file('pdf');
            //manejo de nombre para subir el archivo
            $nombre_original = $archivo->getClientOriginalName();
            $nombre_modificado = pathinfo($nombre_original, PATHINFO_FILENAME) . '_' . $hoy . '_' . $id_usuario . '.'. $archivo->getClientOriginalExtension();
            //subimos el archivo al directorio local como backup
            $archivo->storeAs('pdfManual', $nombre_modificado);
            //remplazamos el archivo que se encuentra online para descargar desde la pagina
            $archivo->storeAs('manual', 'Gestur - Manual del Usuario.pdf','manual');
            //guardamos en la base de datos.
            $usuario = ManualFile::create([
                'fecha_subida' => date('Y-m-d'),
                'usuario_subida' => Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
                'archivo_nombre' => $nombre_modificado,
            ]);
            return response()->json(array('estado'=>1));
        } else {
            return response()->json(array('estado'=>0));
        }
     
    }
    public function verDatosManualesSubidos(){
        $datos=DB::table('actualizacion_manual')->join('personas', 'actualizacion_manual.usuario_subida', '=', 'personas.id')->get(); 
        $data = array();
        foreach ($datos as $dato) {
            $verPdf = '<a href="'.asset('../storage/app').'/pdfManual/'.$dato->archivo_nombre.'" target="_blank" class="btn btn-primary text-white pull-right" title="Ver PDF"><i class="fa fa-eye"></i></a>';

            $fila = array(
                $dato->fecha_subida,
                $dato->nombre,
                $verPdf
            );
    
            array_push($data, $fila);
        }
        return response()->json([
            'data' => $data
        ]);

    }
}
