<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use GuzzleHttp\Client;
use App\Currency;
use App\Persona;
use App\TipoTicket;
use App\Ticket;
use App\ReservaCangoroo;
use App\AlojamientoCangoroo;
use App\TrasladoCangoroo;
use App\CircuitoCangoroo;
use Carbon\Carbon;
use App\EstadoReserva;
use DB;
use Response;
use Image; 
use Session;
use Log;
use App\Exports\ListadoReservasExport;

class ReservaCangorooController extends Controller
{

    private $id_empresa = 0;

    private function getIdEmpresa(){
        if(!$this->id_empresa){
           $this->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
        }
       return $this->id_empresa;
    }


    public function download(Request $request)
    {
        Log::info('Inicia llamada a descarga de reservas cangoroo');

		$client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);


        $Credential = [];
        $Credential['Username']= "api.dtp";
        $Credential['Password']= "api23DTP!";
        $SearchBookingCriteria = [];
        $SearchBookingCriteria['FinalBookingDate'] = "2024-07-13"; 
        $SearchBookingCriteria['FinalServiceDate'] = "2024-07-13"; 
        $SearchBookingCriteria['FinalUpdateDate'] = "2024-07-13"; 
        $SearchBookingCriteria['InitialBookingDate'] = "2023-08-13";   
        $SearchBookingCriteria['InitialServiceDate'] = "2023-08-13"; 
        $SearchBookingCriteria['InitialUpdateDate'] = "2023-08-13"; 

        $jsonRequest = new \StdClass;
        $jsonRequest->Credential = $Credential;
        // $jsonRequest->SearchBookingCriteria = $SearchBookingCriteria;
        $estado_api = 'Procesado correctamente';

        try {

            //Recibe todas las reservas
            $iibRsp = $client->post('http://ws-dtpmundo.cangooroo.net/API/REST/CangoorooBackOffice.svc/GetBookingList', [
                'body' => json_encode($jsonRequest)
            ]);
        
            $jsonResponse = json_decode($iibRsp->getBody());

            // Log::info('================================================ SOLICITUD INICIAL =================================================================');
            // Log::info($iibRsp->getBody());
            // Log::info('================================================ SOLICITUD INICIAL =================================================================');

        
        } catch (\Exception $e) {
            Log::error($e);
            $estado_api = 'Procesado con errores';
            return response()->json(['estado' => $estado_api]);
        }

    
        Log::info('Reservas recibidas');
        $booking_registrados = [];
    
        //Recorre cada reserva y verificamos si el id ya existe para no volver a validar
        foreach($jsonResponse->Reservations as $item){                    

            Log::info('Solicitando datos de la reserva ID: ' . $item->BookingId);
            // if($item->BookingId != 694){
            //     continue;
            // }


            //Validamos si ya leimos la reserva para evitar descagar la misma reserva mas de una vez
            if(in_array($item->BookingId, $booking_registrados)){
                continue;
            }
            $booking_registrados[] = $item->BookingId;

            $Credential = [];
            $Credential['Username']= "api.dtp";
            $Credential['Password']= "api23DTP!";
            $jsonRequests = new \StdClass;
            $jsonRequests->Credential = $Credential;
            $jsonRequests->BookingId =  $item->BookingId;

            try {

                Log::info('Solicitando datos de la reserva ID: ' . $item->BookingId);

                $iibRsp = $client->post('http://ws-dtpmundo.cangooroo.net/API/REST/CangoorooBackOffice.svc/GetBookingDetail',
                    ['body' => json_encode($jsonRequests)]
                );

                $jsonResponses = json_decode($iibRsp->getBody());
               
                //Solo cuando tenemos error viene el elemento
                if(isset($jsonResponses->Error)){
                    $estado_api = 'Procesado con errores';
                    Log::error('================================================ DETALLE =================================================================');
                    Log::error($iibRsp->getBody());
                    Log::error('================================================ DETALLE =================================================================');
                    continue;
                }

                // Log::info('================================================ DETALLE =================================================================');
                // Log::info($iibRsp->getBody());
                // Log::info('================================================ DETALLE =================================================================');


            } catch (\Exception $e) {
                Log::error($e);
                $estado_api = 'Procesado con errores';
                continue; //Cortamos el proceso para solicitar datos de la siguiente reserva
            }
            

          
            //TODO: Chutar reserva sin identificador
            // if(!$item->UniqueIdentifier){
            //     continue;
            // }


            $servicio_id = 0;
            switch ($item->ServiceType) {
                case 'HOTEL':
                    $servicio_id = 1;
                    break;
                case 'TOUR':
                    $servicio_id = 2;
                    break;
                case 'TRANSFER':
                    $servicio_id = 3;
                    break;
            }
            
            //Si ya existe solo actualizamos, sino creamos
            $reservaCangoroo = ReservaCangoroo::where('bookingid', $item->BookingId)->first();

            if(!$reservaCangoroo){
                $reservaCangoroo = new ReservaCangoroo;
                $reservaCangoroo->created_at = date('Y-m-d H:m:s');
            }

            $reservaCangoroo->updated_at = date('Y-m-d H:m:s');
            $reservaCangoroo->bookingdate = $item->BookingDate;
            $reservaCangoroo->servicedate = $item->ServiceDate;
            $reservaCangoroo->sinfo = $jsonResponses->SInfo;
            $reservaCangoroo->uniqueidentifier = $item->UniqueIdentifier;

            //################ LA COMBINACION ES EL IDENTIFICADOR UNICO DE LA RESERVA ################
            $reservaCangoroo->bookingid = $item->BookingId;
            // $reservaCangoroo->serviceid = $item->ServiceId;
            //################ LA COMBINACION ES EL IDENTIFICADOR UNICO DE LA RESERVA ################

            if(isset($jsonResponses->BookingDetail->ClientID)){
                $clienteID = $jsonResponses->BookingDetail->ClientID;
                $reservaCangoroo->clientid = $jsonResponses->BookingDetail->ClientID;
            }else{
                $clienteID = 0;
                $reservaCangoroo->clientid = '';
            }

            /**
             * 10 Vendedor Agencia
             */
            if($reservaCangoroo->clientid){

                //DTP NETO
                if($reservaCangoroo->clientid == 269){
                    $reservaCangoroo->id_cliente = 1; //Persona DTP
                } else {
                    $cliente = Persona::where('cod_cangoroo',$reservaCangoroo->clientid)->whereIn('id_tipo_persona',[10])->first(['id']);
                    if(isset($cliente->id)){
                        $reservaCangoroo->id_cliente = $cliente->id;
                    }else{
                        
                        $reservaCangoroo->id_cliente =   0;
                    }
                }

               
            }


            $reservaCangoroo->clientname = $jsonResponses->BookingDetail->ClientName;
            $reservaCangoroo->operatorname =$jsonResponses->BookingDetail->OperatorName;
            $reservaCangoroo->price = str_replace(',','.',$item->Price->Value);
            $reservaCangoroo->currency = $item->Price->Currency;
            $reservaCangoroo->id_tipo_servicio = $servicio_id; 
            $reservaCangoroo->servicio_name = $item->ServiceType; 
            $reservaCangoroo->status = $item->Status;
            $reservaCangoroo->main_request = json_encode($item); //La cabecera que trae la reserva
            $reservaCangoroo->detail_request =  json_encode($jsonResponses); //Detalles de la reserva
            $reservaCangoroo->id_empresa = 1;
            $reservaCangoroo->save();
            $reserva_id = $reservaCangoroo->id; 

            if(!$reservaCangoroo->clientid){
                Log::error('id_cliente no encontrado en cangoroo reserva_id:'.$reserva_id);
            }

            /**
             * TODO: Validar que no tenga id proforma, sino tiene eliminar y volver a crear
             * Si ya tengo insertado el supplierreservationcode con un id_proforma no debo hacer nada
             */

            if($servicio_id == 1){

                //Se guardan los codigos ya leidos
                $reservas_leidas_alojamiento = [];

                foreach($jsonResponses->BookingDetail->Rooms as $key=>$alojamiento){

                    $reservas_leidas_alojamiento[] = $alojamiento->SupplierReservationCode;
                    $reserva_alojamiento = AlojamientoCangoroo::where('id_reserva_cangoroo',$reserva_id)
                                                  ->where('serviceid',$alojamiento->ServiceId)
                                                  ->first();
                    if($reserva_alojamiento){
                         //Si ya tiene proforma no tocamos la reserva
                        if($reserva_alojamiento->id_proforma){
                            continue;
                        } else {
                            $reserva_alojamiento->delete();
                        }
                    }

                    $alojamientoCangoroo = new AlojamientoCangoroo;
                    $alojamientoCangoroo->id_reserva_cangoroo = $reserva_id;
                    if(isset($alojamiento->BreakdownSales[0]->DateBreakdownStr)){
                        $alojamientoCangoroo->datebreakdownstr = $alojamiento->BreakdownSales[0]->DateBreakdownStr;
                    }else{
                        $alojamientoCangoroo->datebreakdownstr = '';
                    }
                    $alojamientoCangoroo->mainpax = '';
                    $alojamientoCangoroo->numberofbookingrooms = $jsonResponses->BookingDetail->NumberOfBookingRooms;
                    $alojamientoCangoroo->currency = $alojamiento->PaymentCurrency;
                    $alojamientoCangoroo->checkin = $alojamiento->CheckIn;
                    $alojamientoCangoroo->checkinhour = $alojamiento->CheckInHour;
                    $alojamientoCangoroo->checkout = $alojamiento->CheckOut;
                    $alojamientoCangoroo->checkouthour = $alojamiento->CheckOutHour;
                    $alojamientoCangoroo->cityid = $alojamiento->CityId;
                    $alojamientoCangoroo->cityname = $alojamiento->CityName;
                    $alojamientoCangoroo->countrycode = $alojamiento->CountryId;
                    $alojamientoCangoroo->countryname = $alojamiento->CountryName;
                    $alojamientoCangoroo->serviceid= $alojamiento->ServiceId;

                    //USUARIO CREACION DE LA RESERVA
                    $alojamientoCangoroo->creationuserdetail_id = $alojamiento->CreationUserDetail->id;
                    $alojamientoCangoroo->creationuserdetail_email = $alojamiento->CreationUserDetail->Email;
                    $alojamientoCangoroo->creationuserdetail_name = $alojamiento->CreationUserDetail->Name;
                    $alojamientoCangoroo->creationuserdetail_username = $alojamiento->CreationUserDetail->Username;

                    $alojamientoCangoroo->supplierreservationcode = $alojamiento->SupplierReservationCode;
                    $alojamientoCangoroo->destinationid = $alojamiento->DestinationId;
                    $alojamientoCangoroo->destinationname = $alojamiento->DestinationName;
                    $alojamientoCangoroo->hoteladdress = $alojamiento->HotelAddress;
                    $alojamientoCangoroo->hotelphone = $alojamiento->Phone;
                    $alojamientoCangoroo->hotelid = $alojamiento->HotelId;
                    $alojamientoCangoroo->hotelname = $alojamiento->HotelName;
                    $alojamientoCangoroo->supplierhotelid = $alojamiento->SupplierHotelId;
                    $alojamientoCangoroo->netprice_currency = $alojamiento->SellingPriceWithoutTax->Currency;
                    $alojamientoCangoroo->netprice_value = $alojamiento->SellingPriceWithoutTax->Value;
                    $alojamientoCangoroo->sellingpricewithouttax_currency = $alojamiento->SellingPriceWithoutTax->Currency;
                    $alojamientoCangoroo->sellingpricewithouttax_value = str_replace(',','.',$alojamiento->SellingPriceWithoutTax->Value);
                    $alojamientoCangoroo->sellingprice_currency =$alojamiento->SellingPrice->Currency;
                    $alojamientoCangoroo->sellingprice_value = str_replace(',','.',$alojamiento->SellingPrice->Value);
                    $alojamientoCangoroo->paymentdate =  $alojamiento->PaymentDate;
                    $alojamientoCangoroo->paymentdeadline =  $alojamiento->PaymentDeadLine;
                    $alojamientoCangoroo->paymentstatus =  $alojamiento->PaymentStatus;
                    $alojamientoCangoroo->paymenttype =  $alojamiento->PaymentType;
                    $alojamientoCangoroo->cancellationdate =  $alojamiento->CancellationDate;
                    $alojamientoCangoroo->cancellationtype =  $alojamiento->CancellationType;
                    $alojamientoCangoroo->cancellationpolicies_startdate = $alojamiento->CancellationPolicies[0]->StartDate;
                    $alojamientoCangoroo->cancellationpolicies_enddate =   $alojamiento->CancellationPolicies[0]->EndDate;
                    $alojamientoCangoroo->cancellationpolicies_currency =   $alojamiento->CancellationPolicies[0]->Value->Currency;
                    $alojamientoCangoroo->cancellationpolicies_value =   $alojamiento->CancellationPolicies[0]->Value->Value;

                    if(isset($jsonResponses->BookingDetail->ResponsibleOperator)){
                        //VENDEDOR EMPRESA
                        $alojamientoCangoroo->responsible_operator_email =   $jsonResponses->BookingDetail->ResponsibleOperator->emailField;
                        $alojamientoCangoroo->responsible_operator_id =   $jsonResponses->BookingDetail->ResponsibleOperator->idField;
                        $alojamientoCangoroo->responsible_operator_name =   $jsonResponses->BookingDetail->ResponsibleOperator->nameField;
                        $alojamientoCangoroo->responsible_operator_username =   $jsonResponses->BookingDetail->ResponsibleOperator->userNameField;


                        /**
                         * 4 SUPERVIRO EMPRESA
                         * 3 VENDEDOR EMPRESA
                         */
                        $vendedor_empresa = Persona::where('cod_cangoroo',$jsonResponses->BookingDetail->ResponsibleOperator->idField)
                                            ->whereIn('id_tipo_persona',[4,3])
                                            ->where('activo',true)
                                            ->where('id_empresa',1)
                                            ->first(['id']);
                        if(isset($vendedor_empresa->id)){
                            $alojamientoCangoroo->id_vendedor_empresa =   $vendedor_empresa->id;
                        }else{
                            Log::error('id_vendedor_empresa no encontrado => '.$jsonResponses->BookingDetail->ResponsibleOperator->idField);
                            $alojamientoCangoroo->id_vendedor_empresa =   0;
                        }
                    } else {
                        Log::error('No se encuentra ResponsibleOperator - reserva : '.$reserva_id);
                    } 


                    foreach($alojamiento->CustomFields as $keys=>$customFiels){
                        if($customFiels->Name == 'SupplierId'){
                            $alojamientoCangoroo->supplierid= $customFiels->Value;
                        }
                       if($customFiels->Name == 'SupplierName'){
                            $alojamientoCangoroo->suppliername= $customFiels->Value;
                        }
                        if($customFiels->Name == 'SupplierReservationStatus'){
                            $alojamientoCangoroo->supplierreservationstatus= str_replace(',','.',$customFiels->Value);
                        }
                        if($customFiels->Name == 'SupplierPrice.Price'){
                            $alojamientoCangoroo->supplierprice= str_replace(',','.',$customFiels->Value);
                        }
                        if($customFiels->Name == 'SupplierPrice.Currency'){
                            $alojamientoCangoroo->supplierprice_currency= str_replace(',','.',$customFiels->Value);
                        }
                        // if($customFiels->Name == 'SupplierPaymentStatus'){
                        //     $alojamientoCangoroo->supplierprice_supplierpaymentstatus= str_replace(',','.',$customFiels->Value);
                        // }
                        if($customFiels->Name == 'SupplierReservationStatus'){
                            $status= $customFiels->Value;
                        }
                    }

                    $proveedor = Persona::where('cod_cangoroo',$alojamientoCangoroo->supplierid)->whereIn('id_tipo_persona',[15,14])->first(['id']);
                    if(isset($proveedor->id)){
                        $alojamientoCangoroo->id_proveedor =   $proveedor->id;
                    }else{
                        Log::error('id_proveedor no encontrado => '.$alojamientoCangoroo->supplierid);
                        $alojamientoCangoroo->id_proveedor =   0;
                    }

                    $alojamientoCangoroo->id_prestador = $this->identificarPrestador($alojamiento->HotelId,  $alojamiento->HotelName, $reserva_id);

                    /**
                     * 4 SUPERVIRO EMPRESA
                     * 3 VENDEDOR EMPRESA
                     * 10 Vendedor Agencia
                     */
                    $usuario = Persona::where('cod_cangoroo',$alojamiento->CreationUserDetail->id)
                               ->where('id_empresa',1)
                               ->where('activo',true)
                               ->whereIn('id_tipo_persona',[3,4,10])
                               ->first(['id']);
                    if(isset($usuario->id)){
                        $alojamientoCangoroo->id_usuario = $usuario->id;
                    }else{
                        Log::error('id_usuario no encontrado => '.$alojamiento->CreationUserDetail->id);
                        $alojamientoCangoroo->id_usuario = 0;
                    }

                    $alojamientoCangoroo->id_tipo =  $servicio_id ;
                    if( $status='Rejected'||  $status='Cancelled'){
                        $estado = 2; // No tomar
                    }else{
                        $estado = 1;
                    }
                    $alojamientoCangoroo->id_estado =  $estado;
                    $alojamientoCangoroo->created_at = date('Y-m-d H:m:s');
                    $alojamientoCangoroo->updated_at = date('Y-m-d H:m:s');
                    $alojamientoCangoroo->save();
                }

                //Eliminar todas las reservas que no se hayan actualizado
                AlojamientoCangoroo::where('id_reserva_cangoroo',$reserva_id)->whereNotIn('supplierreservationcode',$reservas_leidas_alojamiento)->delete(); 

            }


            if($servicio_id == 3){

                $reservas_leidas_traslado = [];

                foreach($jsonResponses->BookingDetail->Transfers as $key=>$traslado){

                    $reservas_leidas_traslado[] = $traslado->SupplierReservationCode;
                    $reserva_traslado = TrasladoCangoroo::where('id_reserva_cangoroo',$reserva_id)
                                                  ->where('serviceid',$traslado->ServiceId)
                                                  ->first();
                    if($reserva_traslado){
                        //Si ya tiene proforma no tocamos la reserva
                        if($reserva_traslado->id_proforma){
                            continue;
                        } else {
                            $reserva_traslado->delete();
                        }
                    }
                    
                    $trasladoCangoroo = new TrasladoCangoroo;
                    $trasladoCangoroo->id_reserva_cangoroo = $reserva_id;
                    $trasladoCangoroo->supplierreservationcode= $traslado->SupplierReservationCode;
                    $trasladoCangoroo->additinformationdestinationcity= $traslado->AdditInformationDestinationCity;
                    $trasladoCangoroo->additinformationorigincity= $traslado->AdditInformationOriginCity;
                    $trasladoCangoroo->additionalairportname= $traslado->AdditionalAirportName;
                    $trasladoCangoroo->additionalreturnairportname= $traslado->AdditionalReturnAirportName;
                    $trasladoCangoroo->aditdestinationcompanynameinformation= $traslado->AditDestinationCompanyNameInformation;
                    $trasladoCangoroo->aditorigincompanynameinformation= $traslado->AditOriginCompanyNameInformation;
                    $trasladoCangoroo->aditreturncompanynameinformation= $traslado->AditReturnCompanyNameInformation;
                    $trasladoCangoroo->aditionaldestinationnumberinfo= $traslado->AditionalDestinationNumberInfo;
                    $trasladoCangoroo->aditionaloriginnumberinfo= $traslado->AditionalOriginNumberInfo;
                    $trasladoCangoroo->aditionalreturnnumberinfo= $traslado->AditionalReturnNumberInfo;
                    $trasladoCangoroo->adults= $traslado->Adults;
                    $trasladoCangoroo->childs= $traslado->Childs;
                    $trasladoCangoroo->airportname= $traslado->AirportName;
                    $trasladoCangoroo->cancellationdate= $traslado->CancellationDate;
                    $trasladoCangoroo->checkindate= $traslado->CheckInDate;
                    $trasladoCangoroo->cityname= $traslado->CityName;
                    $trasladoCangoroo->creationuserdetail_id= $traslado->CreationUserDetail->id;
                    $trasladoCangoroo->creationuserdetail_email= $traslado->CreationUserDetail->Email;
                    $trasladoCangoroo->creationuserdetail_name= $traslado->CreationUserDetail->Name;
                    $trasladoCangoroo->creationuserdetail_type= $traslado->CreationUserDetail->Type;
                    $trasladoCangoroo->creationuserdetail_username= $traslado->CreationUserDetail->Username;
                    foreach($traslado->CustomFields as $keys=>$customFiels){
                        if($customFiels->Name == 'SupplierId'){
                            $trasladoCangoroo->supplierid= $customFiels->Value;
                        }
                       if($customFiels->Name == 'SupplierName'){
                            $trasladoCangoroo->suppliername= $customFiels->Value;
                        }
                        if($customFiels->Name == 'SupplierReservationStatus'){
                            $trasladoCangoroo->supplierreservationstatus= str_replace(',','.',$customFiels->Value);
                        }
                        if($customFiels->Name == 'SupplierPrice.Price'){
                            $trasladoCangoroo->supplierprice= str_replace(',','.',$customFiels->Value);
                        }
                        if($customFiels->Name == 'SupplierPrice.Currency'){
                            $trasladoCangoroo->supplierprice_currency= str_replace(',','.',$customFiels->Value);
                        }
                        if($customFiels->Name == 'SupplierPaymentStatus'){
                            $trasladoCangoroo->supplierprice_supplierpaymentstatus= str_replace(',','.',$customFiels->Value);
                        }
                        if($customFiels->Name == 'SupplierReservationStatus'){
                            $status= $customFiels->Value;
                        }
                    }
                    $trasladoCangoroo->destinationaddress= $traslado->DestinationAddress;
                    $trasladoCangoroo->destinyaddress= $traslado->DestinyAddress;
                    $trasladoCangoroo->destinyaddresstype= $traslado->DestinyAddressType;
                    $trasladoCangoroo->destinylocation= $traslado->DestinyLocation;
                    $trasladoCangoroo->destinyname= $traslado->DestinyName;
                    $trasladoCangoroo->operatorcancellationpolicies_startdate= $traslado->OperatorCancellationPolicies[0]->StartDate;
                    $trasladoCangoroo->operatorcancellationpolicies_enddate= $traslado->OperatorCancellationPolicies[0]->EndDate;
                    $trasladoCangoroo->operatorcancellationpolicies_value= str_replace(',','.',$traslado->OperatorCancellationPolicies[0]->Value->Value);
                    $trasladoCangoroo->operatorcancellationpolicies_currency= $traslado->OperatorCancellationPolicies[0]->Value->Currency;
                    $trasladoCangoroo->originaircianame= $traslado->OriginAirCiaName;
                    $trasladoCangoroo->originairnumber= $traslado->OriginAirNumber;
                    $trasladoCangoroo->originairtime= $traslado->OriginAirTime;
                    $trasladoCangoroo->originlocation= $traslado->OriginLocation;
                    $trasladoCangoroo->originname= $traslado->OriginName;
                    $trasladoCangoroo->paymentdate= $traslado->PaymentDate;
                    $trasladoCangoroo->paymentdeadline= $traslado->PaymentDeadline;
                    $trasladoCangoroo->paymentstatus= $traslado->PaymentStatus;
                    $trasladoCangoroo->price_value= str_replace(',','.',$traslado->Price->Value);
                    $trasladoCangoroo->price_currency= $traslado->Price->Currency;
                    $trasladoCangoroo->reservationdate= $traslado->ReservationDate;
                    $trasladoCangoroo->returndestinationaddress= $traslado->ReturnDestinationAddress;
                    $trasladoCangoroo->returndestinationaircianame= $traslado->ReturnDestinationAirCiaName;
                    $trasladoCangoroo->returndestinationairnumber= $traslado->ReturnDestinationAirNumber;
                    $trasladoCangoroo->returndestinationairtime= $traslado->ReturnDestinationAirTime;
                    $trasladoCangoroo->returndestinationlocation= $traslado->ReturnDestinationLocation;
                    $trasladoCangoroo->returndestinationname= $traslado->ReturnDestinationName;
                    $trasladoCangoroo->returnservicedate= $traslado->ReturnServiceDate;
                    $trasladoCangoroo->servicedescription= json_encode($traslado->ServiceDescription);
                    $trasladoCangoroo->serviceid= $traslado->ServiceId;
                    $trasladoCangoroo->servicetype= $traslado->ServiceType;
                    $trasladoCangoroo->termsandconditions= $traslado->TermsAndConditions;
                    $trasladoCangoroo->transfername= $traslado->TransferName;
                    $trasladoCangoroo->transportname= $traslado->TransportName;
                    $trasladoCangoroo->transporttype= $traslado->TransportType;
                    $trasladoCangoroo->usedtoken= $traslado->UsedToken;
                    $trasladoCangoroo->voucherobservation= $traslado->VoucherObservation;
                    $trasladoCangoroo->principalpicture= $traslado->PrincipalPicture; 

                    if(isset($jsonResponses->BookingDetail->ResponsibleOperator)){
                        //VENDEDOR EMPRESA
                        $trasladoCangoroo->responsible_operator_email =   $jsonResponses->BookingDetail->ResponsibleOperator->emailField;
                        $trasladoCangoroo->responsible_operator_id =   $jsonResponses->BookingDetail->ResponsibleOperator->idField;
                        $trasladoCangoroo->responsible_operator_name =   $jsonResponses->BookingDetail->ResponsibleOperator->nameField;
                        $trasladoCangoroo->responsible_operator_username =   $jsonResponses->BookingDetail->ResponsibleOperator->userNameField;

                        /**
                         * 4 SUPERVIRO EMPRESA
                         * 3 VENDEDOR EMPRESA
                         */
                        $vendedor_empresa = Persona::where('cod_cangoroo',$jsonResponses->BookingDetail->ResponsibleOperator->idField)
                                            ->whereIn('id_tipo_persona',[4,3])
                                            ->where('activo',true)
                                            ->where('id_empresa',1)
                                            ->first(['id']);
                        if(isset($vendedor_empresa->id)){
                            $trasladoCangoroo->id_vendedor_empresa =   $vendedor_empresa->id;
                        }else{
                            Log::error('id_vendedor_empresa no encontrado => '.$jsonResponses->BookingDetail->ResponsibleOperator->idField);
                            $trasladoCangoroo->id_vendedor_empresa =   0;
                        }
                    } else {
                        Log::error('No se encuentra ResponsibleOperator - reserva : '.$reserva_id);
                    } 

                    $proveedor = Persona::where('cod_cangoroo',$trasladoCangoroo->supplierid)->whereIn('id_tipo_persona',[15,14])->first(['id']);
                    if(isset($proveedor->id)){
                        $trasladoCangoroo->id_proveedor =   $proveedor->id;
                    }else{
                        Log::error('id_proveedor no encontrado => '.$trasladoCangoroo->supplierid);
                        $trasladoCangoroo->id_proveedor =   0;
                    }


                    $trasladoCangoroo->id_prestador = $this->identificarPrestador($trasladoCangoroo->supplierid,  $trasladoCangoroo->suppliername, $reserva_id);

                    /**
                     * 4 SUPERVIRO EMPRESA
                     * 3 VENDEDOR EMPRESA
                     * 10 VENDEDOR AGENCIA
                     */
                    $usuario = Persona::where('cod_cangoroo',$trasladoCangoroo->creationuserdetail_id)
                    ->where('id_empresa',1)
                    ->where('activo',true)
                    ->whereIn('id_tipo_persona',[3,4,10])
                    ->first(['id']);
                    if(isset($usuario->id)){
                        $trasladoCangoroo->id_usuario = $usuario->id;
                    }else{
                        Log::error('id_usuario no encontrado => '.$trasladoCangoroo->creationuserdetail_id);
                        $trasladoCangoroo->id_usuario = 0;
                    }


                    $trasladoCangoroo->id_tipo =  $servicio_id;
                    if( $status='Rejected'||  $status='Cancelado'){
                        $estado = 2; // No tomar
                    }else{
                        $estado = 1;
                    }
                    $trasladoCangoroo->id_estado =  $estado;
                    $trasladoCangoroo->created_at = date('Y-m-d H:m:s');
                    $trasladoCangoroo->updated_at = date('Y-m-d H:m:s');
                    $trasladoCangoroo->save();
    
                } 

                //Eliminar todas las reservas que no se hayan actualizado
                TrasladoCangoroo::where('id_reserva_cangoroo',$reserva_id)->whereNotIn('supplierreservationcode',$reservas_leidas_traslado)->delete(); 

            }

           if($servicio_id == 2){

                $reservas_leidas_circuito = [];

                foreach($jsonResponses->BookingDetail->Tours as $key=>$circuito){

                    $reservas_leidas_circuito[] = $circuito->SupplierReservationCode;
                    $reserva_circuito = CircuitoCangoroo::where('id_reserva_cangoroo',$reserva_id)
                                                  ->where('serviceid',$circuito->ServiceId)
                                                  ->first();
                    if($reserva_circuito){
                        //Si ya tiene proforma no tocamos la reserva
                        if($reserva_circuito->id_proforma){
                            continue;
                        } else {
                            $reserva_circuito->delete();
                        }
                    }


                    $circuitoCangoroo = new CircuitoCangoroo;
                    $circuitoCangoroo->id_reserva_cangoroo = $reserva_id;
                    $circuitoCangoroo->agencycurrencycommission= $circuito->AgencyCurrencyCommission;
                    $circuitoCangoroo->automaticcancellationenabled= $circuito->AutomaticCancellationEnabled;
                    $circuitoCangoroo->cancellationdate= $circuito->OperatorCancellationPolicies[0]->DataFim;
                    $circuitoCangoroo->checkindate= $circuito->CheckInDate;
                    $circuitoCangoroo->checkoutdate= $circuito->CheckOutDate;
                    $circuitoCangoroo->creationuserdetail_id= $circuito->CreationUserDetail->id;
                    $circuitoCangoroo->creationuserdetail_email= $circuito->CreationUserDetail->Email;
                    $circuitoCangoroo->creationuserdetail_name= $circuito->CreationUserDetail->Name;
                    $circuitoCangoroo->creationuserdetail_type= $circuito->CreationUserDetail->Type;
                    $circuitoCangoroo->creationuserdetail_username= $circuito->CreationUserDetail->Username;
                    $circuitoCangoroo->serviceid= $circuito->ServiceId;

                    foreach($circuito->CustomFields as $keys=>$customFiels){
                        if($customFiels->Name == 'SupplierId'){
                            $circuitoCangoroo->supplierid= $customFiels->Value;
                        }
                       if($customFiels->Name == 'SupplierName'){
                            $circuitoCangoroo->suppliername= $customFiels->Value;
                        }
                        if($customFiels->Name == 'SupplierReservationStatus'){
                            $circuitoCangoroo->supplierreservationstatus= $customFiels->Value;
                        }
                        if($customFiels->Name == 'SupplierPrice.Price'){
                            $circuitoCangoroo->supplierprice= str_replace(',','.',$customFiels->Value);
                        }
                        if($customFiels->Name == 'SupplierPrice.Currency'){
                            $circuitoCangoroo->supplierprice_currency= $customFiels->Value;
                        }
                        if($customFiels->Name == 'SupplierPaymentStatus'){
                            $circuitoCangoroo->supplierprice_supplierpaymentstatus= $customFiels->Value;
                        }
                        if($customFiels->Name == 'SupplierTax.Price'){
                            $circuitoCangoroo->supplierprice_suppliertaxprice= $customFiels->Value;
                        }

                    }
 
                    $circuitoCangoroo->durationinhours= $circuito->DurationInHours;
                    $circuitoCangoroo->operatorcancellationpolicies_moeda= $circuito->OperatorCancellationPolicies[0]->Moeda;
                    $circuitoCangoroo->operatorcancellationpolicies_preco= $circuito->OperatorCancellationPolicies[0]->DataFim;
                    $circuitoCangoroo->operatorcancellationpolicies_comissaocliente= $circuito->OperatorCancellationPolicies[0]->ComissaoCliente;
                    $circuitoCangoroo->originalagencycommission= $circuito->OriginalAgencyCommission;
                    $circuitoCangoroo->paymentstatus= $circuito->PaymentStatus;
                    $circuitoCangoroo->price_value= str_replace(',','.',$circuito->Price->Value);
                    $circuitoCangoroo->price_currency= $circuito->Price->Currency;
                    $circuitoCangoroo->reservationdate= $circuito->ReservationDate;
                    $circuitoCangoroo->supplierreservationcode= $circuito->SupplierReservationCode;
                    $circuitoCangoroo->systemstatus= $circuito->SystemStatus;
                    $circuitoCangoroo->voucherobservation= $circuito->VoucherObservation;
                    $circuitoCangoroo->touractivitytipe= $circuito->tourActivityTipe;
                    $circuitoCangoroo->tourcheckindate= $circuito->tourCheckInDate;
                    $circuitoCangoroo->tourcheckoutdate= $circuito->tourCheckOutDate;
                    $circuitoCangoroo->tourcityname= $circuito->tourCityName;
                    $circuitoCangoroo->tourdescription= $circuito->tourDescription;
                    $circuitoCangoroo->tourname= $circuito->tourName;
                    $circuitoCangoroo->tournumberofadults= $circuito->tourNumberOfAdults;
                    $circuitoCangoroo->tourpaymentdate= $circuito->tourPaymentDate;
                    $circuitoCangoroo->tourservicetype= $circuito->tourServiceType;
                    $circuitoCangoroo->tourpaymentdeadline= $circuito->tourPaymentDeadline;
                    $circuitoCangoroo->tourcityid= $circuito->tourCityId;
                    $circuitoCangoroo->tourcityname= $circuito->tourCityName;
                    $circuitoCangoroo->code_circuito = $circuito->ReservationId;

                    if(isset($jsonResponses->BookingDetail->ResponsibleOperator)){
                        //VENDEDOR EMPRESA
                        $circuitoCangoroo->responsible_operator_email =   $jsonResponses->BookingDetail->ResponsibleOperator->emailField;
                        $circuitoCangoroo->responsible_operator_id =   $jsonResponses->BookingDetail->ResponsibleOperator->idField;
                        $circuitoCangoroo->responsible_operator_name =   $jsonResponses->BookingDetail->ResponsibleOperator->nameField;
                        $circuitoCangoroo->responsible_operator_username =   $jsonResponses->BookingDetail->ResponsibleOperator->userNameField;


                        /**
                         * 4 SUPERVIRO EMPRESA
                         * 3 VENDEDOR EMPRESA
                         */
                        $vendedor_empresa = Persona::where('cod_cangoroo',$jsonResponses->BookingDetail->ResponsibleOperator->idField)
                                            ->whereIn('id_tipo_persona',[4,3])
                                            ->where('activo',true)
                                            ->where('id_empresa',1)
                                            ->first(['id']);
                        if(isset($vendedor_empresa->id)){
                            $circuitoCangoroo->id_vendedor_empresa =   $vendedor_empresa->id;
                        }else{
                            Log::error('id_vendedor_empresa no encontrado => '.$jsonResponses->BookingDetail->ResponsibleOperator->idField);
                            $circuitoCangoroo->id_vendedor_empresa =   0;
                        }
                    } else {
                        Log::error('No se encuentra ResponsibleOperator - reserva : '.$reserva_id);
                    } 
                    

                    $proveedor = Persona::where('cod_cangoroo',$circuitoCangoroo->supplierid)->whereIn('id_tipo_persona',[15,14])->first(['id']);
                    if(isset($proveedor->id)){
                        $circuitoCangoroo->id_proveedor =   $proveedor->id;
                    }else{
                        Log::error('id_proveedor no encontrado => '.$circuitoCangoroo->supplierid);
                        $circuitoCangoroo->id_proveedor =   0;
                    }

                    $circuitoCangoroo->id_prestador = $this->identificarPrestador($circuitoCangoroo->supplierid,  $circuitoCangoroo->suppliername, $reserva_id);

                    
                    /**
                     * 4 SUPERVIRO EMPRESA
                     * 3 VENDEDOR EMPRESA
                     * 10 VENDEDOR AGENCIA
                     */
                    $usuario = Persona::where('cod_cangoroo',$circuitoCangoroo->creationuserdetail_id)
                    ->where('id_empresa',1)
                    ->where('activo',true)
                    ->whereIn('id_tipo_persona',[3,4,10])
                    ->first(['id']);
                    if(isset($usuario->id)){
                        $circuitoCangoroo->id_usuario = $usuario->id;
                    }else{
                        Log::error('id_usuario no encontrado => '.$circuitoCangoroo->creationuserdetail_id);
                        $circuitoCangoroo->id_usuario = 0;
                    }

                    $circuitoCangoroo->id_tipo =  $servicio_id;
                    if( $status='Rejected'||  $status='Cancelado'){
                        $estado = 2; // No tomar
                    }else{
                        $estado = 1;
                    }
                    $circuitoCangoroo->id_estado =  $estado;
                    $circuitoCangoroo->created_at = date('Y-m-d H:m:s');
                    $circuitoCangoroo->updated_at = date('Y-m-d H:m:s');

                    $circuitoCangoroo->save();


                }   
                
                //Eliminar todas las reservas que no se hayan actualizado
                CircuitoCangoroo::where('id_reserva_cangoroo',$reserva_id)->whereNotIn('supplierreservationcode',$reservas_leidas_circuito)->delete(); 

            } 


        }

        Log::info('Finaliza llamada a descarga de reservas cangoroo');
        return response()->json(['estado' => $estado_api]);

    }


    public function identificarPrestador($cod_cangoroo, $prestador_name, $grupo_reserva_id){
        /**
         * Buscar si prestador ya existe en sistema
         * Sino buscar por nombre
         * Sino crear prestador y retornar id
         */

        
        if($cod_cangoroo && $prestador_name){

            //Buscar por codigo
            $prestador = Persona::where('cod_cangoroo',$cod_cangoroo)->whereIn('id_tipo_persona',[15,14])->first(['id']);
            if(isset($prestador->id)){
                return $prestador->id;
            }

            //Buscar por nombres
            Persona::where('nombre', 'ILIKE', $prestador_name)
            ->whereIn('id_tipo_persona', [15, 14])
            ->first(['id']);
            if(isset($prestador->id)){
                return $prestador->id;
            }


            $persona = new Persona();
            $persona->cod_cangoroo = $cod_cangoroo;
            $persona->id_tipo_persona = 15;
            $persona->nombre = $prestador_name;
            $persona->denominacion_comercial = $prestador_name;
            $persona->id_empresa = 1;
            $persona->activo = true;
            $persona->fecha_alta = date('Y-m-d H:i:s');
            $persona->id_personeria = 1;
            $persona->save();
    
            return $persona->id;

        }

        Log::error('Prestador con datos incorrecto en cangoroo, grupo_reserva_id :'.$grupo_reserva_id);
        return null;
    }

    public function buscarReserva(Request $request){
		$id_tipo = "";
		if($request->input('id_tipo') !== null){
			$id_tipo = $request->input('id_tipo');
		}	
		$vendedor_empresa = Persona::whereIn('id_tipo_persona',array(2, 4, 3))->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
		$vendedor_agencia = Persona::where('id_tipo_persona','10')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
	    $cliente = DB::select("SELECT * FROM personas 
							  WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
							  							WHERE puede_facturar = true) 
							  AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");

		return view('pages.mc.proforma.buscarReservaCangoroo')->with(['vendedor_empresas'=>$vendedor_empresa,
											                  'vendedor_agencia'=>$vendedor_agencia,
											                  'clientes'=>$cliente,
															  'id_tipo'=>$id_tipo
															]);
	}//function

    public function buscarReservaAjax(Request $request){
        
    
		
			$reservas = DB::table('vw_reservas_cangoroo')
            ->select(DB::raw("to_char(booking_checkin::date::timestamp with time zone,'DD/MM/YYYY') as booking_checkin_f"),"*");
 			

			// if($request->id_tipo != ''){
			// 	if($request->id_tipo == 'disponible'){
			// 		$reservas = $reservas->where('nemo','true');
			// 		$reservas = $reservas->whereNull('id_proforma');
			// 		$reservas = $reservas->whereIn('estado_id', array(2, 3));
			// 	}else{	
			// 		$reservas = $reservas->where('nemo','true');
			// 	 	$reservas = $reservas->whereNotNull('id_proforma');
			// 		$reservas = $reservas->whereIn('estado_id', array(2, 3));
			// 	}	
		  	// }

			if($request->cliente_id != ''){
				 $reservas = $reservas->where('id_cliente',$request->cliente_id);
			}

			if($request->id_vendedor_empresa != ''){
				 $reservas = $reservas->where('id_usuario',$request->id_vendedor_empresa);
			}

			if($request->vendedor_id != ''){
				 $reservas = $reservas->where('vendedor_id',$request->vendedor_id);
			}

			if($request->proforma != ''){
				$reservas = $reservas->where('id_proforma',$request->proforma);
		    }

			if($request->proforma_estado != ''){
				$reservas = $reservas->where('estado_gestur',$request->proforma_estado);
            }
 			
			if($request->nombrePasajero != ''){
				$nombre = "%".trim(strtolower($request->nombrePasajero))."%";

				 $reservas = $reservas->where('nombres','LIKE',$nombre);
			}

			if($request->cod_confirmacion){
				$cod = "%".trim($request->cod_confirmacion)."%";
				$reservas = $reservas->where('code_supplier','ILIKE',$cod);
			}

			if($request->estado){
				$reservas = $reservas->where('status',$request->estado);
			}


			 if($request->periodo_in != ''){
			           $fecha = explode('-', $request->periodo_in);
			                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
			                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
 
			  $reservas = $reservas->whereBetween('booking_checkin', 
			  							array($desde.' 00:00:00',$hasta.' 23:59:59'));
			        }
			  if($request->periodo_out != ''){
			           $fecha = explode('-', $request->periodo_out);
			                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
			                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
 
			  $reservas = $reservas->whereBetween('booking_checkout', 
			  							array($desde.' 00:00:00',$hasta.' 23:59:59'));
			        }        
			

			
			  $reservas = $reservas->where('id_empresa',$this->getIdEmpresa());       
			  $reservas = $reservas->orderBy('bookingid','DESC');       

			 

			/* foreach($reservas as $key=>$profDetalle){
				if($profDetalle->id_empresa != Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa){
					unset($reservas[$key]);
				}
			}*/

            $reservas	= $reservas->get();

            if ($request->has('format') && $request->format == 'excel') {
                // Descargar como Excel
                return $this->exportToExcel($reservas);
            } 
        
       
        return response()->json(['data'=>$reservas]);
	}//function


    public function exportToExcel($reservas){
           
           $data = [];

           foreach ($reservas as  $reserva) {
                $data[] = [
                    $reserva->bookingid,
                    $reserva->serviceid,
                    $reserva->code_supplier,
                    $reserva->id_proforma,
                    $reserva->estado_gestur,
                    $reserva->status,
                    $reserva->tipo_reserva,
                    $reserva->proveedor ? $reserva->proveedor : 'N/A ('.$reserva->proveedor_name_cangoroo.'-'.$reserva->proveedor_id_cangoroo.')',
                    $reserva->moneda_costo,
                    $reserva->precio_costo,
                    $reserva->moneda_venta,
                    $reserva->precio_venta,
                    $reserva->client_name? $reserva->client_name : 'N/A ('.$reserva->clientname.'-'.$reserva->cliente_id_cangoroo.')',
                    $reserva->usuario ? $reserva->usuario : 'N/A ('.$reserva->creationuserdetail_name.'-'.$reserva->creationuserdetail_id.')',
                    $reserva->vendedor_empresa ? $reserva->vendedor_empresa : 'N/A ('.$reserva->responsible_operator_name.'-'.$reserva->responsible_operator_id.')',
                    $reserva->booking_checkin_f,
                    $reserva->pasajero_n,
                ];
           }

           $export = new ListadoReservasExport($data);
           return Excel::download($export, 'Listado Reservas -'.date('d-m-Y').'.xlsx');

      
    
    }


}



