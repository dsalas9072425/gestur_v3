<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\DestinoDtpmundo;
use App\Currency;
use App\Aerolinea;
use App\EstadoFactour;
use App\Grupo;
use App\Producto;
use App\Persona;
use App\LiquidacionComisionDtpPunto;
use App\LiquidacionComisionDtpPuntoDetalle;
use App\TipoTicket;
use App\Ticket;
use App\MetaVendedor;
use App\LibroCompra;
use App\Empresa;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Response;
use Image;
use Session;
use App\Exports\ListaVendedoresExport;

class DtpPuntosController extends Controller
{
    public function index()
    {
        //Controlar permisos


        $mes = $this->mes();

        return view('pages.mc.liquidaciones_dtp_puntos.liquidar_comision', compact('mes'));
    }

    /**
     * Listado de todos los vendedores de agencia que tengan puntos en los periodos que se liquidaron las comisiones
     */
    public function listado(Request $req)
    {
        $periodo = "";
        $indicador = 0;
        $id_vendedor = 0;
        $idliquidacion="";
        $vendedores = [];
        $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

        //TODO: Pendiente definir permisos
        $permisos = ['ok'];
        // $permisos = DB::select("SELECT pe.*
        // 					FROM persona_permiso_especiales p, permisos_especiales pe
        // 					WHERE p.id_permiso = pe.id
        // 					AND p.id_persona = ".$idUsuario."
        // 					AND pe.url = 'verLiq'");

        /*	if(empty($permisos)){
                flash('No tiene los permisos para ver esta vista')->error();
                return redirect()->route('home');
            }*/

        if ($req->input('datos')) {
            $datos = explode('_', $req->input('datos'));
            $id_vendedor= $datos[0];
            $periodo = $this->getFecha($datos[1], $datos[2]);
            $idliquidacion= $datos[3];
            $indicador = 1;
        }

        if ($req->input('periodo')) {
            $fecha = explode('/', $req->input('periodo'));
            $periodo = $this->getFecha($fecha[0], $fecha[1]);
        }

        if (empty($permisos)) {
            //Usuario que no tiene permisos para ver todos
            $vendedores = DB::select("SELECT 
            p.id,
            p.nombre,
            p.apellido,
            e.nombre as empresa,
            p.documento_identidad 
            from personas p
            join personas e ON e.id = p.id_persona
            where exists (
                select * from meta_vendedores where id_vendedor = p.id and activo = true
            ) 
                and p.activo = true 
                and p.id_empresa = ".$this->getIdEmpresa()." 
                and p.id_tipo_persona = 10
                and p.id = ".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario."
                order by p.nombre asc 
            ");
        } else {
            //Puede ver todos los vendedores de agencia que tengan meta
            $vendedores = DB::select("SELECT 
            p.id,
            p.nombre,
            p.apellido,
            e.nombre as empresa,
            p.documento_identidad 
            from personas p
            join personas e ON e.id = p.id_persona
            where exists (
                select * from meta_vendedores where id_vendedor = p.id and activo = true
            ) 
                and p.activo = true 
                and p.id_empresa = ".$this->getIdEmpresa()." 
                and p.id_tipo_persona = 10
                order by p.nombre asc 
            ");
        }


        $fecha_liquidacion = DB::select("SELECT * from (
													select  DISTINCT (mes_anho) as fecha, 
                                                            to_date(mes_anho,'MMYYYY') as fecha_liq
													from liquidacion_dtp_puntos 
													where mes_anho is not null
												) as result
													order by fecha_liq desc");

        $listadoFechas = array();

        $agencias = DB::select("SELECT id, nombre from personas where id_tipo_persona = 8 and activo = true and id_empresa = ".$this->getIdEmpresa()." order by nombre asc");

        foreach ($fecha_liquidacion as $key => $value) {
            $fecha = explode('-', $value->fecha_liq);
            $mes = $fecha[1];
            $anho = $fecha[0];
            $listadoFechas[] = $this->getFecha($mes, $anho);
        }


        return view('pages.mc.liquidaciones_dtp_puntos.verLiquidaciones', compact('vendedores', 'listadoFechas', 'periodo', 'id_vendedor', 'idliquidacion', 'indicador','agencias'));
    }

    public function reporteComisionVendedores(){
 

        $permisos = ['ok'];
        $vendedores = DB::select("SELECT 
        p.id,
        p.nombre,
        p.apellido,
        e.nombre as empresa,
        p.documento_identidad 
        from personas p
        join personas e ON e.id = p.id_persona
        where exists (
            select * from meta_vendedores where id_vendedor = p.id and activo = true
        ) 
            and p.activo = true 
            and p.id_empresa = ".$this->getIdEmpresa()." 
            and p.id_tipo_persona = 10
            order by p.nombre asc 
        ");

        $fecha_liquidacion = DB::select("SELECT * from (
                                                    select  DISTINCT (mes_anho) as fecha, 
                                                            to_date(mes_anho,'MMYYYY') as fecha_liq
                                                    from liquidacion_dtp_puntos 
                                                    where mes_anho is not null
                                                ) as result
                                                    order by fecha_liq desc");

        $listadoFechas = array();

        foreach ($fecha_liquidacion as $key => $value) {
            $fecha = explode('-', $value->fecha_liq);
            $mes = $fecha[1];
            $anho = $fecha[0];
            $listadoFechas[] = $this->getFecha($mes, $anho);
        }

        return view('pages.mc.liquidaciones_dtp_puntos.reporteComisionVendedores', compact('vendedores', 'listadoFechas'));
    }

    /**
     * Reporte de puntos no liquidados
     */
    public function reportePuntos(Request $req)
    {
        $periodo = "";
        $indicador = 0;
        $id_vendedor = 0;
        $idliquidacion="";
        $vendedores = [];
        $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

        //TODO: Pendiente definir permisos
        $permisos = ['ok'];
        // $permisos = DB::select("SELECT pe.*
        // 					FROM persona_permiso_especiales p, permisos_especiales pe
        // 					WHERE p.id_permiso = pe.id
        // 					AND p.id_persona = ".$idUsuario."
        // 					AND pe.url = 'verLiq'");

        /*	if(empty($permisos)){
                flash('No tiene los permisos para ver esta vista')->error();
                return redirect()->route('home');
            }*/

        if ($req->input('datos')) {
            $datos = explode('_', $req->input('datos'));
            $id_vendedor= $datos[0];
            $periodo = $this->getFecha($datos[1], $datos[2]);
            $idliquidacion= $datos[3];
            $indicador = 1;
        }

        if ($req->input('periodo')) {
            $fecha = explode('/', $req->input('periodo'));
            $periodo = $this->getFecha($fecha[0], $fecha[1]);
        }

  
            //Puede ver todos los vendedores de agencia que tengan meta
            $vendedores = DB::select("SELECT 
            p.id,
            p.nombre,
            p.apellido,
            e.nombre as empresa,
            p.documento_identidad 
            from personas p
            join personas e ON e.id = p.id_persona
            where exists (
                select * from meta_vendedores where id_vendedor = p.id and activo = true
            ) 
                and p.activo = true 
                and p.id_empresa = ".$this->getIdEmpresa()." 
                and p.id_tipo_persona = 10
                order by p.nombre asc 
            ");
        


        $fecha_liquidacion = DB::select("SELECT * from (
													select  DISTINCT (mes_anho) as fecha, 
                                                            to_date(mes_anho,'MMYYYY') as fecha_liq
													from liquidacion_dtp_puntos 
													where mes_anho is not null
												) as result
													order by fecha_liq desc");


    $mes = $this->mes();

        return view('pages.mc.liquidaciones_dtp_puntos.reporteComisionVendedores', compact('vendedores', 'periodo', 'id_vendedor', 'mes', 'indicador'));
    }


    public function reportePuntosDetalle(Request $request){
        $datos = $this->responseFormatDatatable($request);
  

       $anho = $datos->anho;
       $mes = $datos->mes;
       $id_persona = $datos->id_vendedor;


        $reporte_facturas = DB::select(
            "SELECT 
            f.id as id_factura,
            f.nro_factura,
            f.id,
            f.fecha_hora_facturacion,
            f.total_neto_factura,
            pr.descripcion as promocion,
            per.nombre as pasajero,
            f.id_estado_cobro,
            CASE
                WHEN f.id_estado_cobro = 40  THEN 'COBRADO'::character varying
                    ELSE 'PENDIENTE'::character varying
            END AS estado,
            COALESCE(ROUND((((get_monto_cotizado_factura(f.total_neto_factura,f.id)/100) * pr.porcentaje_comision) * 20)::decimal,2),0) total_puntos,
            COALESCE(ROUND(((get_monto_cotizado_factura(f.total_neto_factura,f.id)/100) * pr.porcentaje_comision)::decimal,2),0) total_usd,
			'FACTURA' AS tipo,
            f.id_moneda_venta
            from facturas f
            join proformas p on p.id = f.id_proforma
            join promociones pr ON pr.id = p.id_promocion
            left join personas per ON per.id = f.id_pasajero_principal
            where f.vendedor_id = $id_persona
            and f.id_estado_factura = 29
            and to_char(f.fecha_hora_facturacion, 'YYYY') = '$anho'
            and to_char(f.fecha_hora_facturacion, 'MM') = '$mes'
            UNION ALL
            SELECT 
            nc.id as id_factura,
            nc.nro_nota_credito,
            nc.id,
            nc.fecha_hora_nota_credito,
            nc.total_neto_nc,
            pr.descripcion as promocion,
            per.nombre as pasajero,
            0 as n,
            '' as estado,
            COALESCE(ROUND((((get_monto_cotizado_nc(nc.total_neto_nc,nc.id)/100) * pr.porcentaje_comision) * 20)::decimal,2),0) AS total_puntos,
            0 as total_usd,
			'NOTA CREDITO' AS tipo,
            nc.id_moneda_venta
            from nota_credito nc
			JOIN facturas f ON nc.id_factura = f.id
            join proformas p on p.id = nc.id_proforma
            join promociones pr ON pr.id = p.id_promocion
            left join personas per ON per.id = f.id_pasajero_principal
            where f.vendedor_id = $id_persona
            and nc.id_estado_nc = 36
            and to_char(nc.fecha_hora_nota_credito, 'YYYY') = '$anho'
            and to_char(nc.fecha_hora_nota_credito, 'MM') = '$mes' 
            ");


            return response()->json(['data'=>$reporte_facturas]);

    }

    //Obtener el total de puntos y comision de cada vendedor de un mes determinado
    /**
     * TODO: Verificar porque es un codigo vulnerable a sql injection
     */
    public function reportePuntosCabecera(Request $request){
        $datos = $this->responseFormatDatatable($request);

      
        $fecha = Carbon::createFromDate($datos->periodo_anho,$datos->periodo_mes)->endOfMonth();
        $fecha_hasta = $fecha->year.'-'.$fecha->month.'-'.$fecha->day;
        $fecha_desde = $fecha->year.'-'.$fecha->month.'-01';

        $query = "SELECT 
                p.id,
                CONCAT(p.nombre,' ',p.apellido) as vendedor,
                ag.nombre as agencia,
                m.meta,
                SUM(COALESCE(ROUND((((get_monto_cotizado_factura(f.total_neto_factura,f.id)/100) * pr.porcentaje_comision) * 20)::decimal,2),0)) total_puntos,
                SUM(COALESCE(ROUND(((get_monto_cotizado_factura(f.total_neto_factura,f.id)/100) * pr.porcentaje_comision)::decimal,2),0)) total_comision,
                SUM(COALESCE(ROUND((get_monto_cotizado_factura(f.total_neto_factura,f.id))::decimal,2),0)) total_neto_facturas
                from personas p
                left join facturas f ON f.vendedor_id = p.id
                left join proformas pro ON pro.id = f.id_proforma
                left join promociones pr ON pr.id = pro.id_promocion
                left join personas ag ON ag.id = p.id_persona
                left join meta_vendedores m ON m.id_vendedor = p.id
                where f.id_estado_factura = 29
                and to_char(f.fecha_hora_facturacion, 'YYYY') = '$datos->periodo_anho'
                and to_char(f.fecha_hora_facturacion, 'MM') = '$datos->periodo_mes'
                AND m.anho_mes_desde <= to_date('$fecha_desde', 'YYYY-MM-DD')
                AND m.anho_mes_hasta >= to_date('$fecha_hasta', 'YYYY-MM-DD') ";


            if($datos->vendedor){
                $query .= " AND p.id = ".$datos->vendedor;
            }

            $query .= " group by p.id,ag.nombre, m.meta";
            // dd( $query);
            $data = DB::select($query);
            return response()->json(['data'=>$data]);
    }


    private function getFecha($mes, $anho)
    {
        $month = $mes; //Reemplazable por número del 1 a 12
        $year = $anho; //Reemplazable por un año valido

        switch(date('n', mktime(0, 0, 0, $month, 1, $year))) {
            case 1: $Mes = "Enero";
                break;
            case 2: $Mes = "Febrero";
                break;
            case 3: $Mes = "Marzo";
                break;
            case 4: $Mes = "Abril";
                break;
            case 5: $Mes = "Mayo";
                break;
            case 6: $Mes = "Junio";
                break;
            case 7: $Mes = "Julio";
                break;
            case 8: $Mes = "Agosto";
                break;
            case 9: $Mes = "Septiembre";
                break;
            case 10: $Mes = "Octubre";
                break;
            case 11: $Mes = "Noviembre";
                break;
            case 12: $Mes = "Diciembre";
                break;
        }

        return $Mes.'/'.date('Y', mktime(0, 0, 0, $month, 1, $year));
    }

    public function consultarLiquidaciones(Request $request)
    {
        $datos = $this->responseFormatDatatable($request);


        $explode = explode("/", $datos->periodo);
        $mesLetra = $explode[0];
        $year = $explode[1];
        $mes = $this->getMes($mesLetra);
        // $liquidaciones = LiquidacionComisionDtpPunto::with('meta.vendedor', 'factura_lc');
        //Si op es 53 esta procesado
        $query = "SELECT 
        ldp.*,
        op_c.nro_op,
        op_c.id_estado as id_estado_op,
        m.meta,
        ldp.total_puntos_alcanzados,
        CONCAT(v.nombre,' ',v.apellido) as nombre_completo,
        v.id as id_vendedor,
        v.documento_identidad,
        ag.nombre as agencia
        FROM liquidacion_dtp_puntos ldp
        JOIN meta_vendedores m ON m.id = ldp.id_meta
        JOIN personas v ON v.id = m.id_vendedor
        JOIN personas ag ON ag.id = v.id_persona
        LEFT JOIN op_detalle op_d ON op_d.id_libro_compra = ldp.id_libros_compras and op_d.activo = true
        LEFT JOIN op_cabecera op_c ON op_c.id = op_d.id_cabecera
        WHERE ldp.mes_anho = '".$mes.''.$year."' ";
    
        if($datos->vendedor != '1'){
            $query .= " AND v.id = ".$datos->vendedor;
        }

        if($datos->agencia != '1'){
            $query .= " AND v.id_persona = ".$datos->agencia;
        }

        if($datos->alcanzo_meta){
            $alcanzo_meta = $datos->alcanzo_meta == 'SI' ? 'true' : 'false';
            $query .= " AND ldp.meta_cumplida = $alcanzo_meta";
        }
        


      
        $liquidaciones = DB::select($query);
      
        return response()->json(['data'=>$liquidaciones]);
    }

    public function consultarDetalleLiquidaciones(Request $request)
    {
        $datos = $this->responseFormatDatatable($request);
 
        if (isset($datos->vendedorDetalle)) {
            $idVendedor = $datos->vendedorDetalle;
        } else {
            $idVendedor = $datos->vendedorId;
        }
        $explode = explode("/", $datos->periodoDetalle);
        $mesLetra = $explode[0];
        $year = $explode[1];
        $mes = $this->getMes($mesLetra);

       

        $data = [];
        $total_facturado = 0;

        if ($datos->id_liquidacion) {
            $data = DB::table('vw_comision_dtp_puntos_detalle');
            if($idVendedor){
                $data = $data->where('id_vendedor', $idVendedor);
            }
      
            $data = $data->where('id_liquidacion_dtp_puntos', $datos->id_liquidacion);
            $data = $data->where('periodo', $mes.''.$year);
            $data = $data->orderBy('periodo', 'DESC');
            $data = $data->get();
            $total_facturado = LiquidacionComisionDtpPunto::find($datos->id_liquidacion)->total_facturado;

        }

  
    

        return response()->json(['data'=>$data, 'data2'=>$total_facturado ]);
    }

    public function generarLiquidaciones(Request $request)
    {
        $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
        // $permisos = DB::select("SELECT pe.*
        // 					FROM persona_permiso_especiales p, permisos_especiales pe
        // 					WHERE p.id_permiso = pe.id
        // 					AND p.id_persona = ".$idUsuario."
        // 					AND pe.url = 'addLiq'");

        // if(empty($permisos)){
        // 	flash('No tiene los permisos para ver esta vista')->error();
        //     return redirect()->route('home');
        // }


        $data = DB::select("SELECT procesar_comision_dtp_puntos('".$request->input("periodo_mes")."','".(int)$request->input('periodo_anho')."',".$this->getIdEmpresa().")")[0]->procesar_comision_dtp_puntos;
        if ($data == 'OK') {
            flash('Liquidación generada');
        } else {
            flash('Ocurrio un error al generar liquidación : '.$data)->error();
        }

        return back();
    }

    public function excel(Request $request)
    {
        $mes = $this->mes();
        return view('pages.mc.liquidaciones_dtp_puntos.excel',compact('mes'));
    }

    public function generar_excel_template(Request $request)
    {

        $cuerpo = array();
       
        
        
        if($request->mes_generar && $request->anho_generar){

            $fecha = Carbon::createFromDate($request->anho_generar,$request->mes_generar)->endOfMonth();
            $fecha_hasta = $fecha->year.'-'.$fecha->month.'-'.$fecha->day;
            $fecha_desde = $fecha->year.'-'.$fecha->month.'-01';

        
            $query =  "SELECT 
                       p.id,
                       ag.nombre as agencia,
                       CONCAT(p.nombre,' ',p.apellido) as vendedor,
                       p.email,
                       COALESCE(m.meta,0) AS meta
                       FROM personas p 
                       JOIN personas ag ON ag.id = p.id_persona
                       LEFT JOIN meta_vendedores m ON m.id_vendedor = p.id
                       WHERE p.id_empresa = ".$this->getIdEmpresa()
                       ." AND p.id_tipo_persona = 10
                       AND p.activo = true 
                       AND m.anho_mes_desde <= to_date('$fecha_desde', 'YYYY-MM-DD')
                       AND m.anho_mes_hasta >= to_date('$fecha_hasta', 'YYYY-MM-DD') ";
            

                      $personas = DB::select($query);

       
        } else {

            $query =  "SELECT 
                       p.id,
                       ag.nombre as agencia,
                       CONCAT(p.nombre,' ',p.apellido) as vendedor,
                       p.email,
                       '0'AS meta
                       FROM personas p 
                       JOIN personas ag ON ag.id = p.id_persona
                       WHERE p.id_empresa = ".$this->getIdEmpresa()
                       ." AND p.id_tipo_persona = 10
                       AND p.activo = true ";
        


                      $personas = DB::select($query);


        }
          

        if(empty($personas)){
            flash('Sin resultados ...');
            return back();
        }
    
        foreach ($personas as $persona) {
            array_push(
                $cuerpo,
                array($persona->id,
                      $persona->agencia,
                      $persona->vendedor,
                      $persona->email,
                      $persona->meta
                    )
            );
        }

        $nombreArchivo = 'Lista-vendedores-' . date('d-m-Y') . '.xls'; 
        return Excel::download(new ListaVendedoresExport($cuerpo), $nombreArchivo); 
    }

    public function cargar_excel_comision(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 1640); //240 segundos = 4 minutos

        $path = $request->file('file')->getRealPath();
        $vendedores = Excel::load($path)->get();

        /**
         * Primero lo que hacemos es ver si la meta coincide con exactitud a una meta ya subida, debe coincidir el mes y año en el periodo
         * Si coincide vamos a desactivar todas las metas de ese periodo y activar las nuevas metas , si no coincide entonces
         * Buscamos cual es el ultimo periodo hasta, para evitar que se pueda actualizar un periodo que no coincide con el periodo que estan subiendo, tendria
         * que ser un nuevo periodo para que no afecte los que ya se crearon.
         */

         $fecha_desde = $request->anho_desde.'-'.$request->mes_desde.'-01';
         $fecha_hasta = Carbon::createFromDate($request->anho_hasta,$request->mes_hasta)->endOfMonth();
         $fecha_hasta = $fecha_hasta->year.'-'.$fecha_hasta->month.'-'.$fecha_hasta->day;


        //Verificar que el periodo ya existe y coincide con lo que ya se subio en la BD
         $meta_existe = MetaVendedor::where('anho_mes_desde', $fecha_desde)
         ->where('anho_mes_hasta',$fecha_hasta)->count();

         if($meta_existe){
            //Inactivar las metas que coinciden con ese periodo
           MetaVendedor::where('anho_mes_desde', $fecha_desde)
            ->where('anho_mes_hasta',$fecha_hasta)
            ->update(['activo' => false]);

         } else {
            //Obtener el anho_mes_hasta mas alto de las metas.
            $fecha_mas_alta = MetaVendedor::orderBy('anho_mes_hasta','DESC')->first();

            if($fecha_mas_alta){
                // dd($fecha_mas_alta->anho_mes_hasta);
                $first = strtotime($fecha_mas_alta->anho_mes_hasta);
                $second = strtotime($fecha_desde);
                
                //Si la fecha mas alta es mayor o igual a la fecha desde entonces no se puede actualizar
                if($first >= $second){
                    flash('Ya existe una meta en el periodo seleccionado');
                    return back();
                }

            }

         }

  
      
        foreach ($vendedores as $vendedor) {
            $id = (int)$vendedor['id'];
            $meta_valor = (int)$vendedor['meta'];
           
 
          

            if ($meta_valor > 0) {
                $meta = new MetaVendedor();
                $meta->id_vendedor = $id;
                $meta->fecha = date('Y-m-d');
                $meta->activo = true;
                $meta->id_usuario = 1;
                $meta->id_moneda = 143;
                $meta->anho_mes_desde = $fecha_desde;
                $meta->anho_mes_hasta =  $fecha_hasta;
                $meta->meta = $meta_valor;
                $meta->save();
            }
        }


        flash('Metas actualizadas');

        return back();
    }

    /**
     * Asigna una factura a la cabecera de la liquidacion para el pago total de la comision
     */
    public function pagarComision(Request $request)
    {
        /**
         * Verificamos si ya se pago para no volver a cambiar
         * Faltaia verificar permisos de personas
         */

        $liquidacion = LiquidacionComisionDtpPunto::where('id', $request->id_liquidacion)->first();
        if ($liquidacion) {
            // if ($liquidacion->fecha_hora_pago) {
            //     return response()->json(['err' => true, 'msg' => 'La comisión ya fue facturada']);
            // }

            $liquidacion->fecha_hora_pago = date('Y-m-d H:i:s');
            $liquidacion->id_libros_compras = $request->id_libro_compra;
            $liquidacion->save();

            return response()->json(['err' => false, 'msg' => 'Factura asignada a la comisión']);
        }

        return response()->json(['err' => true, 'msg' => 'Error al intentar encontrar la liquidación']);
    }

    /**
     * Obtener las facturas cargadas en libro compra para aplicar
     */
    public function getFacturasAplicar(Request $request)
    {
        $datos = $this->responseFormatDatatable($request);
 
        $ventas = DB::table('vw_libro_compra');

        if (isset($datos->idProveedor) && $datos->idProveedor) {
            $ventas = $ventas->where('id_proveedor', $datos->idProveedor);
        }

        if ($datos->numero_documento != '') {
            $ventas = $ventas->where('nro_documento', $datos->numero_documento );
        }

        if ($datos->numero_lc != '') {
            $ventas = $ventas->where('id', $datos->numero_lc );
        }

        if ($datos->proveedor_desde_hasta != '') {
            $fechaTrim = trim($datos->proveedor_desde_hasta);
            $periodo = explode('-', trim($fechaTrim));
            $desde = explode("/", trim($periodo[0]));
            $hasta = explode("/", trim($periodo[1]));
            $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
            $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
            $ventas = $ventas->whereBetween('fecha_creacion', [$fecha_desde, $fecha_hasta]);
        }

        $ventas = $ventas->where('fecha_creacion', '>', '2023-01-01 00:00:00'); //Recien inicio la promocion en esta fecha por eso es a partir de aca
        $ventas = $ventas->where('id_empresa', $this->getIdEmpresa())->get();

        return response()->json(['data'=>$ventas]);
    }

    private function mes()
    {
        $meses = [];
        $meses['01'] = "Enero";
        $meses['02'] = "Febrero";
        $meses['03'] = "Marzo";
        $meses['04'] = "Abril";
        $meses['05'] = "Mayo";
        $meses['06'] = "Junio";
        $meses['07'] = "Julio";
        $meses['08'] = "Agosto";
        $meses['09'] = "Septiembre";
        $meses['10'] = "Octubre";
        $meses['11'] = "Noviembre";
        $meses['12'] = "Diciembre";
        return $meses;
    }

    private function getMes($mes)
    {
        switch($mes) {
            case "Enero": $Mes = '01';
                break;
            case "Febrero": $Mes = '02';
                break;
            case "Marzo": $Mes = '03';
                break;
            case "Abril": $Mes = '04';
                break;
            case "Mayo": $Mes = '05';
                break;
            case "Junio": $Mes = '06';
                break;
            case "Julio": $Mes = '07';
                break;
            case "Agosto": $Mes = '08';
                break;
            case "Septiembre": $Mes = '09';
                break;
            case "Octubre": $Mes = '10';
                break;
            case "Noviembre": $Mes = '11';
                break;
            case "Diciembre": $Mes = '12';
                break;
        }

        return $Mes;
    }


    private function getIdEmpresa()
    {
        return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
    }

    private function responseFormatDatatable($req)
    {
        $datos = array();
        $data =  new \StdClass();

        foreach ($req->formSearch as $key => $value) {
            $n = $value['name'];
            $datos[$value['name']] = $value['value'];
            $data-> $n = $value['value'];
        }

        return $data;
    }

    public function generarVoucherPdf(Request $request){

                $empresa = Empresa::find(1);
                $empresa_logo = 'https://gestur.git.com.py/logoEmpresa/'.$empresa->logo;	
                $liquidacion = DB::select("SELECT 
                                        ldp.id,
                                        p.id as id_vendedor,
                                        concat(p.nombre, ' ', p.apellido) as nombre,
                                        mv.meta,
                                        ldp.total_puntos_alcanzados,
                                        ldp.total_cobrado as total_cobrar,
                                        ldp.meta_cumplida,
                                        p.email,
                                        ldp.mes_anho as fecha_corte
                                        FROM liquidacion_dtp_puntos ldp
                                        JOIN meta_vendedores mv ON ldp.id_meta = mv.id
                                        JOIN personas p ON p.id = mv.id_vendedor
                                        WHERE ldp.id = ?", [$request->input('id_liquidacion')]);
            
            
                    if(isset($liquidacion[0])){
                        $liquidacion = $liquidacion[0];
                        $email = $liquidacion->email;
                        $nombre = $liquidacion->nombre;
                        $fecha_corte = $liquidacion->fecha_corte;
                      
    
                        
                        //Obtener los detalles de la liquidacion, facturas, puntos, etc
                        $detalles = DB::select("SELECT * 
                        from vw_comision_dtp_puntos_detalle
                        WHERE id_liquidacion_dtp_puntos = ?", [$liquidacion->id]);
             
                        $anho = substr($fecha_corte, 2, 4 );
                        $mes = substr($fecha_corte, 0, 2);	
                        $saldo = false; //El correo si es de saldos
    
                        //Enviar el correo 
                        $pdf = \PDF::loadView('pages.mc.pdfEmails.voucher_dtp_puntos',compact('empresa','empresa_logo','liquidacion','detalles','saldo','anho','mes'));
                        $pdf->setPaper('A4');
                        $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true,'enable_css_float'=>true]);
    
                        // //   Imprimir en la pantalla
                        $pdf->setPaper('a4', 'letter')->setWarnings(false);
                        return response($pdf->output(), 200)->header('Content-Type', 'application/pdf');
                    }
     
                    return back();
                            

            
    } 
}
