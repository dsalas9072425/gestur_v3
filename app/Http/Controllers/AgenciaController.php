<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use \App\Dtp\Proveedor;
use \App\Agencias;
use \App\Image;
use Session;
use Redirect;
use DB;

class AgenciaController extends Controller
{
	/*
	Listado de agencias
	- Muestra el listado de todas las agencias habilitadas
	*/
	public function index(Request $req){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminDtp')){
			$agencia = Agencias::all();	
			return view('pages.agencias.agencias')->with('agencias',$agencia);
		}else{
			flash('No tienes los permisos para acceder a esta dirección')->error();
            return Redirect::back();
		}	
	}	

	/*
	Página de editar las agencias
	*/
	public function edit(Request $request, $id){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminDtp')){
			$agencias = Agencias::where('id_agencia', '=',$id)->get();
			return view('pages.agencias.agenciasEdit')->with('agencias',$agencias);
		}else{
			flash('No tienes los permisos para acceder a esta dirección')->error();
            return Redirect::back();
		}		
	}	


	/*
	Editar
	- Función de editar los datos de agencias
	*/
	public function doEdit(Request $request){
 		if(!ctype_space($request->input('razonSocial'))){
 			$razonSocial = $request->input('razonSocial');
 		}else{
			flash('La razon social esta formada por espacios vacios')->error();
			return Redirect::back();
 		}
 		if($request->input('comision') > 1){
 			flash('El indice de comision de Agencia no puede se mayor a 1')->error();
			return Redirect::back();
 		}

	    DB::table('agencias')
					    ->where('id_agencia',$request->input('idAgencia'))
					    ->update([
					            'razon_social'=>$razonSocial,
					            'email' =>$request->input('email'),
					            'telefono'=>$request->input('telefono'),
					            'activo'=>$request->input('activo'),
					            'comision' => $request->input('comision'),
					            ]);

		flash('Se ha editado la agencia exitosamente')->success();
		return redirect()->route('agencias');
	}
/*************************************************************************************************************/
/*									MC
/************************************************************************************************************/

	public function indexMc(Request $req){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminDtp')||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 7){
			$agencia = Agencias::all();	
			return view('pages.mc.agencias.agencias')->with('agencias',$agencia);
		}else{
			flash('No tienes los permisos para acceder a esta dirección')->error();
            return Redirect::back();
		}	
	}	

	public function addMc(Request $request){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminDtp')||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 7){
			return view('pages.mc.agencias.agenciasAdd');
		}else{
			flash('No tienes los permisos para acceder a esta dirección')->error();
            return Redirect::back();
		}		
	}	


	/*
	Página de editar las agencias
	*/
	public function editMc(Request $request, $id){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminDtp')||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 7){
			$agencias = Agencias::where('id_agencia', '=',$id)->get();
			return view('pages.mc.agencias.agenciasEdit')->with('agencias',$agencias);
		}else{
			flash('No tienes los permisos para acceder a esta dirección')->error();
            return Redirect::back();
		}		
	}	

	public function doAddMc(Request $request){
		if(!ctype_space($request->input('razonSocial'))){
 			$razonSocial = $request->input('razonSocial');
 		}else{
			flash('La razon social esta formada por espacios vacios')->error();
			return Redirect::back();
 		}
 		if($request->input('comision') > 1){
 			flash('El indice de comision de Agencia no puede se mayor a 1')->error();
			return Redirect::back();
 		}

 		$id = Agencias::select('id_agencia')->max('id_agencia');
		$agencia = new Agencias;	
		$agencia->id_agencia = $id+1 ;
		$agencia->razon_social = $razonSocial;
		$agencia->email = $request->input('email');
		$agencia->fecha_alta = date('Y-m-d');
		$agencia->telefono = $request->input('telefono');
		$agencia->activo = $request->input('activo');
		$agencia->comision = floatval($request->input('comision'));
		$agencia->ruc = $request->input('ruc');
		$agencia->cod_pais = 2;
		try{
			$agencia->save();
			flash('Se ha ingresado la agencia exitosamente')->success();
			return redirect()->route('mc.agencias');
		} catch(\Exception $e){
			flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
		}
	}	

	public function doEditMc(Request $request){
 		if(!ctype_space($request->input('razonSocial'))){
 			$razonSocial = $request->input('razonSocial');
 		}else{
			flash('La razon social esta formada por espacios vacios')->error();
			return Redirect::back();
 		}
 		if($request->input('comision') > 1){
 			flash('El indice de comision de Agencia no puede se mayor a 1')->error();
			return Redirect::back();
 		}

	    DB::table('agencias')
					    ->where('id_agencia',$request->input('idAgencia'))
					    ->update([
					            'razon_social'=>$razonSocial,
					            'email' =>$request->input('email'),
					            'telefono'=>$request->input('telefono'),
					            'activo'=>$request->input('activo'),
					            'comision' => $request->input('comision'),
					            ]);

		flash('Se ha editado la agencia exitosamente')->success();
		return redirect()->route('mc.agencias');
	}


}
