<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack; 
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use Session;
use Redirect;
use DB;

class PreConfirmationController extends Controller
{
	public function getPreconfirmation(Request $req){
		$client = new Client();
		$req->session()->put('datosPreReservaRQ', null);
		$iibReq = new \StdClass;
		$datosLoggeo = Session::get('datos-loggeo');
		$iibReq->token = $datosLoggeo->token;
		$habitacionesArray = explode(",",$req->input('habitaciones'));
		foreach($habitacionesArray as $habitacion){
			$codReserva = explode(':',$habitacion);
			$codsReserva[] = $codReserva[1]; 
		}
		$iibReq->codsReserva = $codsReserva;
		/*echo '<pre>';
		print_r($iibReq);*/
		$iibRsp = $client->post(Config::get('config.iibPreConfirm'), [
				'json' => $iibReq
			]);
		$iibObjRsp = json_decode($iibRsp->getBody());
        return response()->json($iibObjRsp);
	}

	private function getSelectedHotel($identifier,$sid){
		//itera por los hoteles hasta encontrar el que corresponde
		//$hoteles = Session::get('searchInfo')->hotels;
		//dd($this->waitUntilRead());
		//dd($sid);
		$hoteles = $this->waitUntilRead($sid)->hotels;
		//dd($identifier);
		//dd($hoteles[0]);
		
		
		foreach($hoteles as $hotel){
			if($hotel->identifier == $identifier){
				//ver la forma de eliminar las rooms y rates no seleccionadas y eso nomas meter en sesion
				//Session::get('searchData')->selectedHotel = $hotel;
				//dd($identifier);
				return $hotel;
			}
		}
	}

	private function waitUntilRead($sid){
		 ini_set('memory_limit','256M');
		//$fileName='searchids/' . Session::get('sid') . 'lock.txt';
		$fileName = 'searchids/' . $sid . '/lock.txt';
		if(!file_exists($fileName)){
			
			return null;
			
		}
		$fp = fopen($fileName, "r");
		
		while(!flock($fp, LOCK_EX)) {  // acquire an exclusive lock
			
		}
		//return json_decode(fread($fp));
		$oldSearchInfo;
		//var_dump(filesize($fileName));
		/*
		if(filesize($fileName) <= 0) $oldSearchInfo = "{}";
		else{
			//$oldSearchInfo = fread($fp,filesize($fileName));
			//$oldSearchInfo = fgets($fp);
			//var_dump($oldSearchInfo);
			//dd($oldSearchInfo);
		}
		*/
		$oldSearchInfo = fgets($fp);
		if($oldSearchInfo == '') $oldSearchInfo = '{}';
		flock($fp, LOCK_UN);    // release the lock
		fclose($fp);
		//dd(json_decode($oldSearchInfo));
		//dd(json_decode($oldSearchInfo));
		return json_decode($oldSearchInfo);
		//return $oldSearchInfo;
	}	

		
	public function index(Request $req){
		ini_set('max_execution_time', 60); //60 seconds = 1 minuto
		$req->session()->put('datosPreReservaRQ', null);
		$iibReq = new \StdClass;
		$datosLoggeo = Session::get('datos-loggeo');
		$iibReq->token = $datosLoggeo->token;
        Session::forget('codRetorno');
        Session::forget('desRetorno');

		//$searchData = Session::get('searchData');
		$selectedHotel = Session::get('selectedHotel');
		$codsReserva = [];
		foreach($selectedHotel->habitaciones as $habitacion){
			foreach($habitacion->tarifas as $tarifa){
				$codsReserva[] = $tarifa->codReserva;
			}
		}
		$iibReq->codsReserva = $codsReserva;

		$logger = new Logger('Log');
		$logger->pushHandler(new \Monolog\Handler\StreamHandler(storage_path() . '/log/'. 'hotelesPreConfirm' . '.log'), Logger::DEBUG);
		
		$stack = HandlerStack::create();
		$stack->push(Middleware::log($logger, new MessageFormatter(
					'{method} {uri} HTTP/{version} REQUEST: {req_body}'. 'RESPONSE: {code} - {res_body}'
		)));

		$client = new Client(
			[
				'handler' => $stack
			]
		);
		
		try{
			$iibRsp = $client->post(Config::get('config.iibPreConfirm'), [
				'json' => $iibReq
			]);
			$iibObjRsp = json_decode($iibRsp->getBody());
			
			
			if($iibObjRsp->codRetorno !=0){
				flash('<b>'.$iibObjRsp->desRetorno.'</b></br>'.$iibObjRsp->desDetalle)->error();
				Session::put('marcador',$datosLoggeo->token);
				Session::put('codRetorno', $iibObjRsp->codRetorno);
				Session::put('desRetorno', $iibObjRsp->desRetorno);
				return redirect()->away(Session::get('urlTotal'));
			}
		
		}catch(\Exception $e){
			Session::put('marcador',$datosLoggeo->token);
			Session::put('codRetorno', 666);
			Session::put('desRetorno', 'Lo sentimos la ultima habitación ha sido tomada. Por favor seleccione otro hotel');
			return redirect()->away(Session::get('urlTotal'));
		}
		$selectedHotel = $iibObjRsp->hotel;		//este es el que persiste en la sesion
		Session::put('selectedHotel',$selectedHotel);
		if($datosLoggeo->datos->datosUsuarios->idAgencia == Config::get('constants.agenciaDtp')){
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_usuario =".$datosLoggeo->datos->datosUsuarios->idUsuario);
		}else{
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_perfil = 10");
		}

		//a partir de aca no modifique mas nada
		$arrayAgente = [];
		foreach($agentes as $key=>$agente){
			//$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_vendedor_dtp;
			$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_sistema_facturacion;
			$arrayAgente[$key]['label']= $agente->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agente->usuario;
		}	
		
		$client = new Client();

		$cuerpoProforma = [];
		$RestConsultaProfIn= new \StdClass;
		$RestConsultaProfIn->reqToken = $req->session()->get('token');
		$RestConsultaProfIn->ts = date('Y-m-d h:m:s');
		$Detalles= new \StdClass;
		$Detalles->FechaDesde = '2017-01-01';
		$Detalles->FechaHasta = date('Y-m-d');
		$Detalles->VendedorId = Session::get('datos-loggeo')->datos->datosUsuarios->id_sistema_facturacion;
		$RestConsultaProfIn->Detalles =  $Detalles;
		$iibReqPro = new \StdClass;
		$iibReqPro->RestConsultaProfIn = $RestConsultaProfIn;
		//die(json_encode())
		//dd($iibReq);
		try{
			$iibRsp = $client->post(Config::get('config.iibConProForma'), [
				'json' => $iibReqPro
			]);
			
			$iibObjRspPro = json_decode($iibRsp->getBody());
		
			if(isset($iibObjRspPro->RestConsultaProfOut->Proformas)){
				foreach($iibObjRspPro->RestConsultaProfOut->Proformas as $key=>$datosProforma){
					$cuerpoProforma[$key]['value'] = $datosProforma->ProformaId;
					$cuerpoProforma[$key]['label'] = $datosProforma->ProformaId." - ".$datosProforma->ProformasNombrePasajero;
				}
			}
			
		}
		catch(RequestException $e){
			//entra aca pero si pones Exception nomas no agarra
		}
		//hasta aca
		//dd($hotelInfo);
		return view('pages.hoteles.preconfirmation')->with([
			'selectedHotel'=>$selectedHotel, 
			'selectAgentes'=>$arrayAgente,
			'totalReserva'=>$iibObjRsp->totalReserva,
			'fechaCan' => date_format(date_create($iibObjRsp->politicaDtp->desde), 'd/m/Y'),
			'precioCan' => $iibObjRsp->politicaDtp->monto,
			'selectProforma'=>$cuerpoProforma,
			'fechaDesde' => (\DateTime::createFromFormat("m/d/Y" , $iibObjRsp->fechaDesde))->format('d/m/Y'),
			'fechaHasta' => (\DateTime::createFromFormat("m/d/Y" , $iibObjRsp->fechaHasta))->format('d/m/Y'),
			'noches' => (\DateTime::createFromFormat("m/d/Y",$iibObjRsp->fechaHasta))->diff(\DateTime::createFromFormat("m/d/Y",$iibObjRsp->fechaDesde))->format('%a'),
			'idBusqueda' => $req->input('sid'),
		]);	
	}	

	
	public function armarOcupancia($ocupanciaReal)
	{
		$ocupancia = array();
		$detalle = "";
		$ocupancia[] = $this->armarElementoOcupancia($ocupanciaReal);
		return $ocupancia;
	}
	
	public function armarElementoOcupancia($ocupanciaReal)
	{
		$elemOcup = new \StdClass;
		$elemOcup->cantAdultos = (string) $ocupanciaReal->adultos;
		$elemOcup->cantNinos = (string) count($ocupanciaReal->edadesNinhos);
		$detalle = array();
		//$edad = explode("_",$ed);

		for($k=0; $k< $ocupanciaReal->adultos; $k++){
			$d = new \StdClass;
			$d->tipo = "AD";
			$detalle[]= $d;
		}

		for($k=0 ; $k < $elemOcup->cantNinos ; $k++){
			$d = new \StdClass;
			$d->tipo = "CH";
			$d->edad = $ocupanciaReal->edadesNinhos[$k];
			$detalle[] = $d;
		}
		$elemOcup->detalle = $detalle;
		return $elemOcup;
	}

	public function preconfirmationFlight(Request $req){
		
		$preConfirmacion ="";
		if(session('data') !== null){
			$preConfirmacion = session('data');
		}

		if($req->session()->get('ultimaReserva') !== null){
			flash('<b>Cierre Reserva</b></br> La reservación de registro ya existe.')->error();
			return redirect('/home#flights-tab');
    	}
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json',
						   'Cache-Control'=>'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
						   ]

		]);
		$baggageSession = $req->session()->get('baggage');
		$datosBase = explode("_", $req->input('datosVuelo'));
		$adultos = explode(":",$datosBase[0]);
		$ninho = explode(":",$datosBase[1]);
		$infante = explode(":",$datosBase[2]);
		$ordenBusqueda =  explode(":",$datosBase[3]);
		/*echo '<pre>';
		print_r($req->all());*/
		$segTramo =  explode(":",$datosBase[4]);
		$segmentos = explode("||",$segTramo[1]);
		$monto =  explode(":",$datosBase[5]);
		$indiceBaggage = explode(":",$datosBase[6]);
		$sessionFligth = Session::get('fligthIndex');
		$sessionEstable = $sessionFligth;
		$sessionBusqueda = $this->opcionFareBusqueda($ordenBusqueda[1], Session::get('fligthSession'));
		$indicador = 0;
		$numeroIndicador = 0;
		$iibReq = new \StdClass;
		$iibReq->idRequest = $req->session()->get('token');
		$segmentGroup=[];
		$passengersGroup = [];
		$passengersGroup = $this->armarElementoPasajero($adultos[1],$ninho[1], $infante[1]);
		$total = $adultos[1]+$ninho[1];
		$iibReq->passengersGroup = $passengersGroup;
		$itinerarios= Session::get('itinerary');
		include("../destinationFligth.php");

		$departure = $destination[$itinerarios[0]->departureLocalization->departurePoint->locationId];
		$datosDeparture = explode(",", $departure);
		$itinerarios[0]->departureLocalization->departurePoint->datosciudad= ucwords(strtolower($datosDeparture[1]))." (".$itinerarios[0]->departureLocalization->departurePoint->locationId.")";
		$itinerarios[0]->departureLocalization->departurePoint->datosAeropuerto= "";
		$itinerarios[0]->departureLocalization->departurePoint->datosCiudadPais= ucwords(strtolower($datosDeparture[0])).", ".ucwords(strtolower($datosDeparture[1]));
		
		$arrival = $destination[$itinerarios[0]->arrivalLocalization->arrivalPointDetails->locationId];
		$datosArrival = explode(",", $arrival);
		$itinerarios[0]->arrivalLocalization->arrivalPointDetails->datosciudad = ucwords(strtolower($datosArrival[1]))." (".$itinerarios[0]->arrivalLocalization->arrivalPointDetails->locationId.")";
		$itinerarios[0]->arrivalLocalization->arrivalPointDetails->datosAeropuerto = "";
		$itinerarios[0]->arrivalLocalization->arrivalPointDetails->datosCiudadPais = ucwords(strtolower($datosArrival[0])).", ".ucwords(strtolower($datosArrival[1]));
		foreach($segmentos as $key=>$segmento){
			$cantidad = 0;
			$indices = explode("|",$segmento);
			$indicadorSegmento = $key+1;
			foreach($sessionFligth as $base=>$fligthSeg){
				if($indices[0] == $fligthSeg->requestedSegmentRef->segRef){
					foreach($fligthSeg->groupOfFlights as $indexs=>$flight_Prop){
						if($indices[1] == $flight_Prop->propFlightGrDetail->flightProposal[0]->ref){
							$flightInformation = [];
							foreach($flight_Prop->flightDetails as $indice=>$preflightDetails){
								/*echo '<pre>';
								print_r($preflightDetails->flightInformation);*/
								include("../aeropuertos.php");
								$paxFareProduct = $sessionBusqueda->paxFareProduct;
								if(isset($aeropuerto[$preflightDetails->flightInformation->location[0]->locationId])){
									$location1 = $aeropuerto[$preflightDetails->flightInformation->location[0]->locationId];
									$location1Base = explode('|', $location1);
									$preflightDetails->flightInformation->location[0]->nombre = ucwords(strtolower($location1Base[0]));
									$preflightDetails->flightInformation->location[0]->ciudad = ucwords(strtolower($location1Base[1]));
									$preflightDetails->flightInformation->location[0]->pais = ucwords(strtolower($location1Base[2]));
									$location2 = $aeropuerto[$preflightDetails->flightInformation->location[1]->locationId];
									$location2Base = explode('|', $location2);
									$preflightDetails->flightInformation->location[1]->nombre = ucwords(strtolower($location2Base[0]));
									$preflightDetails->flightInformation->location[1]->ciudad = ucwords(strtolower($location2Base[1]));
									$preflightDetails->flightInformation->location[1]->pais = ucwords(strtolower($location2Base[2]));
								}else{
									$preflightDetails->flightInformation->location[0]->nombre = "No disponible";
									$preflightDetails->flightInformation->location[0]->ciudad = "No disponible";
									$preflightDetails->flightInformation->location[0]->pais = "No disponible";
									$preflightDetails->flightInformation->location[1]->nombre = "No disponible";
									$preflightDetails->flightInformation->location[1]->ciudad = "No disponible";
									$preflightDetails->flightInformation->location[1]->pais = "No disponible";
								}
								
								$flightInformation[] = $preflightDetails->flightInformation;
								$itinerarios[$base]->flightInformation = $flightInformation;
								$itinerarios[$base]->cantidad = $cantidad;

								$numeroIndicador++;
								$segInf = new \StdClass;
								$fDate = new \StdClass;
								$boardPointDetails = new \StdClass;
								$offpointDetails = new \StdClass;
								$companyDetails = new \StdClass;
								$flightIdentification = new \StdClass;
								$segmentInformation = new \StdClass;
								$flightTypeDetails =  new \StdClass;
								$fDate->departureDate = $preflightDetails->flightInformation->productDateTime->dateOfDeparture;
								$fDate->departureTime = $preflightDetails->flightInformation->productDateTime->timeOfDeparture;
								$fDate->arrivalDate = $preflightDetails->flightInformation->productDateTime->dateOfArrival;
								$fDate->arrivalTime = $preflightDetails->flightInformation->productDateTime->timeOfArrival;
								$segInf->flightDate = $fDate;
								
								$boardPointDetails->trueLocationId = $preflightDetails->flightInformation->location[0]->locationId;
								$offpointDetails ->trueLocationId = $preflightDetails->flightInformation->location[1]->locationId;
								$segInf->boardPointDetails = $boardPointDetails;
								$segInf->offpointDetails= $offpointDetails;
								$companyDetails->marketingCompany = $preflightDetails->flightInformation->companyId->marketingCarrier;
								$req->session()->put('marketingCompany', $preflightDetails->flightInformation->companyId->marketingCarrier);
								$companyDetails->operatingCompany = $preflightDetails->flightInformation->companyId->operatingCarrier;
								$segInf->companyDetails = $companyDetails;
								foreach($sessionBusqueda->paxFareProduct[0]->fareDetails as $keys=>$fareDetails){
									if((int)$fareDetails->segmentRef->segRef == (int)$indices[0]){
										$class = $fareDetails->groupOfFares[$cantidad]->productInformation->cabinProduct->rbd;
									}
								}
								$flightIdentification->flightNumber = $preflightDetails->flightInformation->flightOrtrainNumber;
								$flightIdentification->bookingClass = $class;

								$segInf->flightIdentification = $flightIdentification;
								$flightIndicator[0] = $indicadorSegmento;
								$flightTypeDetails->flightIndicator = $flightIndicator;
								$segInf->flightTypeDetails = $flightTypeDetails;
								$segInf->itemNumber = $numeroIndicador;
								$segmentInformation->segmentInformation =$segInf;								
								$segmentGroup[]= $segmentInformation;
								$dateOfArrival = $itinerarios[$base]->flightInformation[$indice]->productDateTime->dateOfArrival;
								$timeOfArrival = $itinerarios[$base]->flightInformation[$indice]->productDateTime->timeOfArrival;
								if($indice == 0){
									$dateOfDeparture = $itinerarios[$base]->flightInformation[$indice]->productDateTime->dateOfDeparture;
									$timeOfDeparture = $itinerarios[$base]->flightInformation[$indice]->productDateTime->timeOfDeparture;
								}
								$cantidad++;
							}
							$itinerarios[$key]->departureLocalization->departurePoint->dateOfDeparture = $dateOfDeparture;
							$itinerarios[$key]->departureLocalization->departurePoint->timeOfDeparture = $timeOfDeparture;
							$itinerarios[$key]->arrivalLocalization->arrivalPointDetails->timeOfArrival =  $timeOfArrival; 
							$itinerarios[$key]->totalHora =$flight_Prop->propFlightGrDetail->flightProposal[1]->ref;
						}
					}	
				}	
			}
			$itinerarios[0]->arrivalLocalization->arrivalPointDetails->dateOfArrival =  $dateOfArrival;

			}	
		$req->session()->put('segmentGroup', $segmentGroup);
		
		$iibReq->segmentGroup = $segmentGroup;
		$pricingOptionGroup= new \StdClass;
		$pricingOptionKey= new \StdClass;
		$pricingOptionKey->pricingOptionKey = "RP";
		$pricingOptionKey->pricingOptionKey = "RLO";
		$pricingOptionGroup->pricingOptionKey = $pricingOptionKey;
		$iibReq->pricingOptionGroup = $pricingOptionGroup;
		/*echo '<pre>Ver Precio';
		print_r(json_encode($iibReq));*/

		try{
			$iibRsp = $client->post(Config::get('config.iibFligthPreConfirm'), [
												'json' => $iibReq
												]);
			}
		catch(RequestException $e){
			return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			$error = $e;
			$iibReqLog->response = $iibRsp;
			$iibReqLog->estado = "ERROR";
			$iibReqLog->stackTrace = $error; 
			$iibReqLog->fechaHoraResponse = date('Y-m-d H:i:s');
			$token_idRequest = (int)$req->session()->get('subNumero')+1;
			$req->session()->put('subNumero', $token_idRequest);
			try{
				$iibRspLog = $client->post(Config::get('config.iibLog'),
									[
										'body' => json_encode($iibReqLog)
										]
								);
				}
			catch(RequestException $e){}
			catch(ClientException $e){}				
			return view('pages.errorConexion');	
		}
	
									
		$iibObjRsp = json_decode($iibRsp->getBody());
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));
		echo '<pre>';
		echo '==========================================================';*/

		if(!empty($iibObjRsp->errorGroup->errorOrWarningCodeDetails->errorDetails->errorCode)){
			flash('<b>Ver Precio</b></br>'.$iibObjRsp->errorGroup->errorWarningDescription->freeText[0])->error();

			$iibReq = new \StdClass;
			$iibReq->idRequest = $req->session()->get('token');
			$iibReq->sessionStatus= "End";
			$iibReq->sessionId = $iibObjRsp->sessionId;
			$iibReq->seqNumber = $iibObjRsp->seqNumber;
			$iibReq->securyToken = $iibObjRsp->securyToken;
			$iibReq->Security_SignOut = "";
			try{	
				$iibRsp = $client->post(Config::get('config.iibFligthSignOffResConfirm'), [
												'json' => $iibReq
											]);
			}
			catch(RequestException $e){
				return view('pages.timeErrorConexion');
			}
			catch(ClientException $e){
				return view('pages.errorConexion');	
			}			

			return Redirect::back();
		}

		$verRegla = $iibObjRsp;

		$endSegment = $this->endkey($verRegla->mainGroup->pricingGroupLevelGroup[0]->fareInfoGroup->fareComponentDetailsGroup);
		$iibReq = new \StdClass;
		$contador = 0;
  		foreach($verRegla->mainGroup->pricingGroupLevelGroup[0]->fareInfoGroup->fareComponentDetailsGroup as $indice=>$sementoRegla){
			$numeroInidice = $sementoRegla->fareComponentID->itemNumberDetails[0]->number;
			$endSegmento= $this->endkey($sementoRegla->couponDetailsGroup);
			foreach($sementoRegla->couponDetailsGroup as $keyFligth=>$tramoRegla){
					$contador =  $contador+1;
					$iibReq->idRequest = $req->session()->get('token');
					$iibReq->sessionId = $verRegla->sessionId;
					if($endSegment == $indice&&$endSegmento == $keyFligth){
						$iibReq->seqNumber = 2;
						$iibReq->sessionStatus ="End";
					}else{
						$iibReq->seqNumber = 1;
						$iibReq->sessionStatus =$verRegla->sessionStatus;
					}
					$iibReq->securyToken = $verRegla->securyToken;
					$msgType = new \StdClass;
					$messageFunction = "712";
					$messageFunctionDetails = new \StdClass;
					$messageFunctionDetails->messageFunction = $messageFunction;
					$msgType->messageFunctionDetails =  $messageFunctionDetails;
					$iibReq->msgType = $msgType;
					$itemDetails = [];
					$itemNumberDetails = [];
					$numero = new \StdClass;
					$numero->number = 1;
					$itemNumberDetails[0] = $numero;
					$details = new \StdClass;
					$details->number = $tramoRegla->productId->referenceDetails->value;
					$details->type = "FC";
					$itemNumberDetails[1]= $details;
					$itemNumber = new \StdClass;
					$itemNumber->itemNumberDetails = $itemNumberDetails;
					$iibReq->itemNumber = $itemNumber;
					$ruleSectionId = ["RU","PE","SR","MX" ];
					$tarifFareRule = new \StdClass;
					$tarifFareRule->ruleSectionId = $ruleSectionId;
					$fareRule=new \StdClass; 
					$fareRule->tarifFareRule = $tarifFareRule;
					$iibReq->fareRule = $fareRule;
					/*echo '<pre>Ver Reglas';
					print_r(json_encode($iibReq));*/
					try{
						$iibRsp = $client->post(Config::get('config.iibFligthReglasConfirm'), [
													'json' => $iibReq
												]);
					}
					catch(RequestException $e){
						return view('pages.timeErrorConexion');
					}
					catch(ClientException $e){
						$error = $e;
						$iibReqLog->response = $iibRsp;
						$iibReqLog->estado = "ERROR";
						$iibReqLog->stackTrace = $error; 
						$iibReqLog->fechaHoraResponse = date('Y-m-d H:i:s');
						$token_idRequest = (int)$req->session()->get('subNumero')+1;
						$req->session()->put('subNumero', $token_idRequest);
						return view('pages.errorConexion');	
					}
					$iibObjRsp = json_decode($iibRsp->getBody());
					/*echo '<pre>';
					print_r($iibObjRsp);*/

			}
  			
		}

	$iibReq = new \StdClass;
	$iibReq->idRequest = $req->session()->get('token');
	$messageFunctionDetails = new \StdClass;
	$additionalMessageFunction = [];
	$messageFunctionDetails->messageFunction = 183;
	$additionalMessageFunction[0] = "M1";
	$messageFunctionDetails->additionalMessageFunction = $additionalMessageFunction;
	$messageActionDetails = new \StdClass;
	$messageActionDetails->messageFunctionDetails = $messageFunctionDetails;
	$iibReq->messageActionDetails =$messageActionDetails;
	/*echo '<pre>';
	print_r(json_encode($sessionEstable));
	echo '<pre>';
	print_r('============================================');
//die;
/*foreach($segmentos as $key=>$segmento){
			$indice = explode("|",$segmento);
			$indicadorSegmento = $key+1;
			foreach($sessionFligth as $base=>$fligthSeg){
				if($indice[0] == $fligthSeg->requestedSegmentRef->segRef){
					foreach($fligthSeg->groupOfFlights as $index=>$flight_Prop){
						if($indice[1] == $flight_Prop->propFlightGrDetail->flightProposal[0]->ref){
							$inicio = 0;
							$flightInformation = [];
							foreach($flight_Prop->flightDetails as $indice=>$preflightDetails){*/




	foreach($segmentos as $key=>$segmento){
		$indices = explode("|",$segmento);
		foreach($sessionEstable as $base=>$fligthSeg){
			/*echo '<pre>';
			print_r(json_encode($fligthSeg->requestedSegmentRef->segRef));*/
			if($indices[0] == $fligthSeg->requestedSegmentRef->segRef){
				foreach($fligthSeg->groupOfFlights as $index1=>$flight_Prop){
					$segmentInformation = [];
					if($indices[1] == $flight_Prop->propFlightGrDetail->flightProposal[0]->ref){
						$inicio = 0;
						foreach($flight_Prop->flightDetails as $index=>$preflightDetails){
							/*echo '<pre>';
							print_r(json_encode($preflightDetails->flightInformation->productDateTime));
							echo '<pre>';*/
							foreach($sessionBusqueda->paxFareProduct[0]->fareDetails as $keys=>$fareDetails){
								if((int)$fareDetails->segmentRef->segRef == (int)$indices[0]){
									$class = $fareDetails->groupOfFares[$inicio]->productInformation->cabinProduct->rbd;
								}
							}
							$itinerary = new \StdClass;
							$originDestinationDetails= $this->destination($indice[0]);
							$itinerary->originDestinationDetails = $originDestinationDetails;
							$messageFunctionDetails = new \StdClass;
							$message = new \StdClass;
							$message->messageFunctionDetails =$messageFunctionDetails;
							$messageFunctionDetails->messageFunction = 183;
							$itinerary->message = $message;
						    $segment = [];
						    $flightDate = new \StdClass;
						    $flightDate->departureDate = $preflightDetails->flightInformation->productDateTime->dateOfDeparture;
						    $flightDate->departureTime = $preflightDetails->flightInformation->productDateTime->timeOfDeparture;
						     $flightDate->arrivalDate = $preflightDetails->flightInformation->productDateTime->dateOfArrival;
						    $flightDate->arrivalTime = $preflightDetails->flightInformation->productDateTime->timeOfArrival;
						    $inicial = $preflightDetails->flightInformation->location[0]->locationId;
						    $final = $preflightDetails->flightInformation->location[1]->locationId;
						    $company = $preflightDetails->flightInformation->companyId->marketingCarrier;
						    $number = $preflightDetails->flightInformation->flightOrtrainNumber;
						    $segmentInformation[] = $this->armarSegment($index, $flightDate, $inicial, $final, $company, $number,$total, $class);
						    $itinerary->segmentInformation = $segmentInformation;
						    $inicio++;
						}
						$itineraryDetails[] = $itinerary;
					}
				}
			}
		}
	}	
	$iibReq->itineraryDetails = $itineraryDetails;
	  	/*echo '<pre>Compra Vuelo';
		print_r(json_encode($iibReq)); */
		try{
		$iibRsp = $client->post(Config::get('config.iibFligthCompraConfirm'), [
												'json' => $iibReq
											]);
		}
		catch(RequestException $e){
			$error = $e;
			$iibReqLog->response = json_encode($iibRsp);
			$iibReqLog->estado = "ERROR";
			$iibReqLog->stackTrace = $error;
			$iibReqLog->fechaHoraResponse = date('Y-m-d H:i:s');
			$token_idRequest = (int)$req->session()->get('subNumero')+1;
			$req->session()->put('subNumero', $token_idRequest);
			try{
				$iibRspLog = $client->post(Config::get('config.iibLog'),
									[
										'body' => json_encode($iibReqLog)
										]
								);
				}
			catch(RequestException $e){}
			catch(ClientException $e){}				
			return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			$error = $e;
			$iibReqLog->response = $iibRsp;
			$iibReqLog->estado = "ERROR";
			$iibReqLog->stackTrace = $error; 
			$iibReqLog->fechaHoraResponse = date('Y-m-d H:i:s');
			$token_idRequest = (int)$req->session()->get('subNumero')+1;
			$req->session()->put('subNumero', $token_idRequest);
			$iibRspLog = $client->post(Config::get('config.iibLog'),
							[
								'body' => json_encode($iibReqLog)
								]
						);
			return view('pages.errorConexion');	
		}
		$iibObjRsp = json_decode($iibRsp->getBody());
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));
		echo '========================================================';*/
		$req->session()->put('compRes', json_encode($iibObjRsp));

	if(isset($iibObjRsp->errorAtMessageLevel[0]->errorSegment)){
		flash('<b>Compra Vuelo</b></br>'.$iibObjRsp->errorAtMessageLevel[0]->errorSegment->errorDetails->errorCode.' Unable to Satisfy, Need Confirmed Flight Status')->error();
		$iibReq = new \StdClass;
		$iibReq->idRequest = $req->session()->get('token');
		$iibReq->sessionStatus= "End";
		$iibReq->sessionId = $iibObjRsp->sessionId;
		$iibReq->seqNumber = $iibObjRsp->seqNumber;
		$iibReq->securyToken = $iibObjRsp->securyToken;
		$iibReq->Security_SignOut = "";
		try{	
			$iibRsp = $client->post(Config::get('config.iibFligthSignOffResConfirm'), [
												'json' => $iibReq
											]);
		}
		catch(RequestException $e){
			return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			return view('pages.errorConexion');	
		}			
		return Redirect::back();
	}
	$security = new \StdClass;
	$security->sessionStatus = $iibObjRsp->sessionStatus;
	$security->sessionId = $iibObjRsp->sessionId;
	$security->seqNumber = $iibObjRsp->seqNumber;
	$security->securyToken = $iibObjRsp->securyToken;
	$req->session()->put('security', $security);
	//Inicio de Generación de Listado de agentes DTP
	$datosLoggeo = Session::get('datos-loggeo');
	
	if($datosLoggeo->datos->datosUsuarios->idAgencia == Config::get('constants.agenciaDtp')){
		$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_usuario =".$datosLoggeo->datos->datosUsuarios->idUsuario);
	}else{
		$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_perfil = 10");
	}

	$arrayAgente = [];
	foreach($agentes as $key=>$agente){
		$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_vendedor_dtp;
		$arrayAgente[$key]['label']= $agente->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agente->usuario;
	}	

	$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json',
						   'Cache-Control'=>'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
						   'id_proveedor'=>$req->input('codProveedor'),
						   'id_usuario'=>Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
						   'fechaRequest'=> date('Y-m-d H:i:s')
						   ]

	]);
	
	$cuerpoProforma = [];
	$RestConsultaProfIn= new \StdClass;
	$RestConsultaProfIn->reqToken = $req->session()->get('token');
	$RestConsultaProfIn->ts = date('Y-m-d h:m:s');
	$Detalles= new \StdClass;
	$Detalles->FechaDesde = '2017-01-01';
	$Detalles->FechaHasta = date('Y-m-d');
	$Detalles->VendedorId = $datosLogueo = Session::get('datos-loggeo')->datos->datosUsuarios->id_sistema_facturacion;
	$RestConsultaProfIn->Detalles =  $Detalles;
	$iibReq = new \StdClass;
	$iibReq->RestConsultaProfIn = $RestConsultaProfIn;
	$logger = new Logger('Log');
	$logger->pushHandler(new \Monolog\Handler\StreamHandler(storage_path() . '/log/preConfirmacion.log'), Logger::DEBUG);
	$stacks = HandlerStack::create();
	$stacks->push(Middleware::log(
							$logger,
							new MessageFormatter(
											'{databaseProforma} \n'
												)
									)
					);
	/*echo '<pre>';
	print_r(json_encode($iibReq));*/

	try{
		$iibRsp = $client->post(Config::get('config.iibConProForma'),
				[
				'body' => json_encode($iibReq),
				'handler' => $stacks
				]
			);
	}
	catch(RequestException $e){
			
		//return view('pages.timeErrorConexion');
	}
	catch(ClientException $e){
		//return view('pages.errorConexion');	
	}	
	$iibObjRsp = json_decode($iibRsp->getBody());
		/*echo '<pre>';
		print_r(json_encode(Session::get('iibObjRsp')));*/
	if(isset($iibObjRsp->RestConsultaProfOut->Proformas)){
		foreach($iibObjRsp->RestConsultaProfOut->Proformas as $key=>$datosProforma){
			$cuerpoProforma[$key]['value'] = $datosProforma->ProformaId;
			$cuerpoProforma[$key]['label'] = $datosProforma->ProformaId." - ".$datosProforma->ProformasNombrePasajero;
		}
	}

	include("../comida.php");

	foreach($comida as $key=>$datosMeal){
		$meal[$key]['value'] = $key;
		$meal[$key]['label'] = $datosMeal;
	}

	$equipaje ="";
	foreach($baggageSession->freeBagAllowanceGrp as $key=>$baggage){
		if(intval($baggage->itemNumberInfo->itemNumberDetails[0]->number) == intval($indiceBaggage[1])){
			$equipaje = $baggage->freeBagAllownceInfo->baggageDetails;
		}
	}

	include("../asientos.php");
	$arrayAsiento = [];
	foreach($asiento as $key=>$asientos){
		$arrayAsiento[$key]['value']= $key;
		$arrayAsiento[$key]['label']= $asientos;
	}	

	/*echo '<pre>';
	print_r($itinerarios);*/
	
	$req->session()->put('subNumero', 0);
	//Fin de Generación de Listado de agentes DTP
	return view('pages.vuelos.preconfirmationFlight')->with(['adulto'=>$adultos[1], 'ninho'=>$ninho[1], 'infante'=>$infante[1], 'itinerary'=>$itinerarios, 'selectAgentes'=>$arrayAgente, 'selectProforma'=>$cuerpoProforma, 'meals'=>$meal, 'monto'=>$monto[1],'equipaje'=>$equipaje, 'asientos'=>$arrayAsiento])->with('inputs',$preConfirmacion);
	}

	private function armarSegment($indice, $flightDate, $inicial, $final, $company, $number, $total, $class){
		$respose = [];
		$segment = new \StdClass;
		$travelProductInformation = new \StdClass;
		$travelProductInformation->flightDate = $flightDate;
		$boardPointDetails = new \StdClass;
		$boardPointDetails->trueLocationId = $inicial;
		$travelProductInformation->boardPointDetails = $boardPointDetails;
		$offpointDetails = new \StdClass;
		$offpointDetails->trueLocationId = $final;
		$travelProductInformation->offpointDetails = $offpointDetails;
		$companyDetails = new \StdClass;
		$companyDetails->marketingCompany = $company;
		$travelProductInformation->companyDetails = $companyDetails;
		$flightIdentification = new \StdClass;
		$flightIdentification->flightNumber = $number;
		$flightIdentification->bookingClass = $class;
		$travelProductInformation->flightIdentification = $flightIdentification;
		$relatedproductInformation = new \StdClass;
		$relatedproductInformation->quantity = $total;
		$statusCode = [];
		$statusCode[0] = "NN";
		$relatedproductInformation->statusCode = $statusCode;
		$segment->travelProductInformation = $travelProductInformation;
		$segment->relatedproductInformation = $relatedproductInformation;
		return $segment;
	}


	private function armarItemNumber($indicadorTramo, $itemArray){
		$itemDetails = [];
		$numero = new \StdClass;
		$numero->number = $indicadorTramo;
		$itemDetails[] = $numero;
		$itemDetails[]=$itemArray;
		return $itemDetails;
	}

	private function opcionFareBusqueda($indice, $busquedaSession){
		foreach($busquedaSession as $key=>$fareBusquedas){
			if($indice == $fareBusquedas->itemNumber->itemNumberId->number){
				$resultadoBusqueda = $fareBusquedas;
			}
		}
		return $resultadoBusqueda;
	}	

	private function destination($destination){
		$itinerario = Session::get('itinerary');
		$originDestinationDetails = new \StdClass;
		foreach($itinerario as $indice=>$datosItinerario){
			if($destination == $datosItinerario->requestedSegmentRef->segRef){
				$originDestinationDetails->origin = $datosItinerario->departureLocalization->departurePoint->locationId;
				$originDestinationDetails->destination = $datosItinerario->arrivalLocalization->arrivalPointDetails->locationId;
			}
		}
		return $originDestinationDetails;
	}

	private function armarElementoPasajero($cantAd, $cantCh, $cantInf){
		$quantity = 0; 
		$segmentControlDetails = [];
		$indicePasajero = 0;
		$passengersGroup = [];
		if($cantAd != 0){
			$quantity++;
			$travellerDetails = [];
			for($k=0; $k< $cantAd; $k++){
				$indicePasajero++;
				$travelDetails = new \StdClass;
				$travelDetails->measurementValue = $indicePasajero;
				$travellerDetails[] = $travelDetails;
			}
			$segmentRepetitionControl = new \StdClass;
			$segContDet = new \StdClass;
			$segContDet->quantity = $quantity;
			$segContDet->numberOfUnits = $cantAd;
			$passGroup = new \StdClass;
			$segmentControlDetails[0] = $segContDet;
			$segmentRepetitionControl->segmentControlDetails = $segmentControlDetails;
			$passGroup->segmentRepetitionControl = $segmentRepetitionControl;
			$travellersID = new \StdClass;
			$travellersID->travellerDetails = $travellerDetails;
			$passGroup->travellersID = $travellersID;
			$discountPtc = new \StdClass;
			$discountPtc->valueQualifier = "ADT";
			$passGroup->discountPtc = $discountPtc;
			$passengersGroup[] = $passGroup;
		}
		if($cantCh != 0){
			$quantity++;
			$travellerDetails = [];
			for($x=0; $x< $cantCh; $x++){
				$indicePasajero++;
				$travelDetails = new \StdClass;
				$travelDetails->measurementValue = $indicePasajero;
				$travellerDetails[] = $travelDetails;
			}
			$indicePasajero++;
			$segmentRepetitionControl = new \StdClass;
			$segContDet = new \StdClass;
			$segContDet->quantity = $quantity;
			$segContDet->numberOfUnits = $cantCh;
			$passGroup = new \StdClass;
			$segmentControlDetails[0] = $segContDet;
			$segmentRepetitionControl->segmentControlDetails = $segmentControlDetails;
			$passGroup->segmentRepetitionControl = $segmentRepetitionControl;
			$travellersID = new \StdClass;
			$travellersID->travellerDetails = $travellerDetails;
			$passGroup->travellersID = $travellersID;
			$discountPtc = new \StdClass;
			$discountPtc->valueQualifier = "CHD";
			$passGroup->discountPtc = $discountPtc;
			$passengersGroup[] = $passGroup;
		}
		if($cantInf != 0){	
			$quantity++;
			$indicePasajeroInfante = 0;
			$travellerDetails = [];
			for($y=0; $y< $cantInf; $y++){
				$indicePasajeroInfante++;
				$travelDetails = new \StdClass;
				$travelDetails->measurementValue = $indicePasajeroInfante;
				$travellerDetails[] = $travelDetails;			
			}
			$segmentRepetitionControl = new \StdClass;
			$segContDet = new \StdClass;
			$segContDet->quantity = $quantity;
			$segContDet->numberOfUnits = $cantInf;
			$passGroup = new \StdClass;
			$segmentControlDetails[0] = $segContDet;
			$segmentRepetitionControl->segmentControlDetails = $segmentControlDetails;
			$passGroup->segmentRepetitionControl = $segmentRepetitionControl;
			$travellersID = new \StdClass;
			$travellersID->travellerDetails = $travellerDetails;
			$passGroup->travellersID = $travellersID;
			$discountPtc = new \StdClass;
			$discountPtc->valueQualifier = "INF";
			$fareDetails = new \StdClass;
			$fareDetails->qualifier = "766";
			$discountPtc->fareDetails = $fareDetails;
			$passGroup->discountPtc = $discountPtc;
			$passengersGroup[] = $passGroup;
		}	
		return $passengersGroup;
	}

	private function endKey($array ){

	    //Aquí utilizamos end() para poner el puntero
	    //en el último elemento, no para devolver su valor
	    end( $array );

	    return key( $array );

	}

	private function diffFechaHora($date1,$hour1, $date2,$hour2){
		$timezone = new \DateTimeZone('UTC'); 
		$dateArrival = \DateTime::createFromFormat('dmY', $date1, $timezone);
        $dateDepature = \DateTime::createFromFormat('dmY', $date2, $timezone);	
		$dateR1 = new \DateTime('20'.$dateDepature->format('y-m-d')." ".date("G:i",strtotime($hour2)));
		$dateR2 = new \DateTime('20'.$dateArrival->format('y-m-d')." ".date("G:i",strtotime($hour1)));
		$diff = $dateR1->diff($dateR2);
		$duracion= "";
		if($diff->d != 0){
			$duracion.= $diff->d." d";
		}
		if($diff->h != 0){ 
			$duracion.= $diff->h." h";
		}
		if($diff->i != 0){
			$duracion.= $diff->i." min";
		}
		return $duracion;
	}

	public function localizacion($localizacion){
		$aeropuerto = DB::select("SELECT * FROM aeropuerto where codigo_iata = '".$localizacion."'");
		$datosAeropuerto = $aeropuerto[0]->codigo_iata."|".$aeropuerto[0]->nombre."|".$aeropuerto[0]->localidad."|".$aeropuerto[0]->pais_eng;
		return $datosAeropuerto;
	}	

	public function ciudad($indice){	
        $ciudades = DB::select("SELECT * FROM ciudad_pais where codigo_iata = '".$indice."'");
		return  $ciudades[0]->codigo_iata."|".$ciudades[0]->nombreCiudad."|".$ciudades[0]->pais;          
	}	


}
