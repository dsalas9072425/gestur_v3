<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use \App\Dtp\Proveedor;
use \App\Cheque;
use \App\Banco;
use \App\Currency;
use \App\TipoCheque;
use \App\Image;
use \App\DtpPlus;
use \App\LibroCompra;
use Session;
use Redirect;
use DB;

class ChequesController extends Controller
{
	/*
	Listado de cheques
	- Muestra el listado de todas las agencias habilitadas
	*/
	public function index(Request $req){
		$resultados = Cheque::with('banco', 'currency', 'tipo_cheque', 'usuario')
									->where('usuario_id', '=' ,Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)
									->get();

		return view('pages.mc.cheques.index')->with(['listadoCheques'=>$resultados]);
	}	

	public function add(Request $request){
		$bancos = Banco::all();
		$monedas = Currency::where('activo', '=' ,'S')->get();
		$tipo_cheques = TipoCheque::all();
		return view('pages.mc.cheques.add')->with(['bancos'=>$bancos, 'monedas'=>$monedas, 'tipo_cheque'=>$tipo_cheques]);	
	}	

	public function doAdd(Request $request){
		echo '<pre>';
		print_r($request->all());
		die;
	}	
	public function addDtplus(Request $request){
		$resultados = Cheque::with('banco', 'currency')->get();

		$dtpPlus = DtpPlus::with('usuario')
							->where('estado_pedido', '=', 'P')
							->get();
		return view('pages.mc.pagos.add')->with(['listadoCheques'=>$resultados, 'dtpPlus'=>$dtpPlus]);
	}	

	public function chequesImg(Request $request){
		$cheques = DtpPlus::where('id', '=' ,$request->input('id'))->get();
		$arrayResultado = new \StdClass; 
		$arrayResultado->estado = 'OK';
		$arrayResultado->imagen = $cheques[0]['id_imagen'];

		return json_encode($arrayResultado);
	}	

	public function chequesList(Request $request)
	{
		$cheques = Cheque::with('currency')
						   ->where('id', '=' ,$request->input('id'))->get();

		$arrayResultado = new \StdClass; 
		$arrayResultado->monto = $cheques[0]['importe'];
		$arrayResultado->moneda = $cheques[0]['currency']['currency_code'];

		return json_encode($arrayResultado);
	}

	public function indexChequePrueba(Request $request)
	{
		return view('pages.mc.cheques.indexChequePrueba');
	}

	public function imprimirCheque(Request $request)
	{
        // dd($request->id_op);
        $array = [];
		$cheque = DB::select(" SELECT f.importe_total, EXTRACT (DAY FROM CURRENT_DATE) AS dia,
            to_char(CURRENT_DATE, 'MM'::text) AS mes,  
            to_char(now(), 'YY')AS anho,
            to_char(CURRENT_DATE, 'DD/MM/YYYY'::text) AS fecha_emision_cheque, 
            upper(CONCAT(p.nombre, ' ', p.apellido)) AS proveedor, o.concepto 
            FROM op_cabecera o 
            JOIN forma_pago_op_cabecera f ON f.id_op = o.id
            JOIN personas p ON p.id = o.id_proveedor
			WHERE o.id = ".$request->id_op." ");
            $chequeN = DB::select("SELECT max(id),emisor_txt FROM fp_cheques WHERE  id_op = ".$request->id_op." GROUP BY emisor_txt" );
            $array['proveedor'] = $chequeN[0]->emisor_txt;
            $array['fecha'] = $cheque[0]->fecha_emision_cheque;
            $array['concepto'] = $cheque[0]->concepto;
            $array['importe'] = number_format($cheque[0]->importe_total, 2, ",", ".");
            $array['importeLetras'] = $this->convertir($cheque[0]->importe_total);   
            $array['dia'] = $cheque[0]->dia;
            $array['mes'] = $cheque[0]->mes;
            $array['anho'] = $cheque[0]->anho;
        // DD($array);

        return response()->json(['data'=>$array]);
	}

    public function imprimirChequeTransaccion(Request $request)
    {
        // dd($request->id_op);
        $array = [];
        $cheque = DB::select("SELECT EXTRACT (DAY FROM CURRENT_DATE) AS dia,to_char(CURRENT_DATE, 'MM'::text) AS mes, to_char(now(), 'YY')AS anho, to_char(CURRENT_DATE, 'DD/MM/YYYY'::text) AS fecha_emision_cheque, * FROM fp_cheques WHERE id = ".$request->id." ");
            $array['proveedor'] = $cheque[0]->emisor_txt;
            $array['fecha'] = $cheque[0]->fecha_emision_cheque;
            $array['concepto'] ='Transferencia entre cuentas';
            $array['importe'] = number_format($cheque[0]->importe, 2, ",", ".");
            $array['importeLetras'] = $this->convertir($cheque[0]->importe);   
            $array['dia'] = $cheque[0]->dia;
            $array['mes'] = $cheque[0]->mes;
            $array['anho'] = $cheque[0]->anho;
          
        return response()->json(['data'=>$array]);
    }


	    private static $UNIDADES = [
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
    ];
    private static $DECENAS = [
        'VENTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
    ];
    private static $CENTENAS = [
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
    ];
    public static function convertir($number, $moneda = '', $centimos = '', $forzarCentimos = false)
    {
        $converted = '';
        $decimales = '';
        if (($number < 0) || ($number > 999999999)) {
            return 'No es posible convertir el numero a letras';
        }
        $div_decimales = explode('.',$number);
        if(count($div_decimales) > 1){
            $number = $div_decimales[0];
            $decNumberStr = (string) $div_decimales[1];
            //SI VIENEN NUMERO SOLO LE RELLENA CON CERO
            $decNumberStr = str_pad($decNumberStr, 2, "0", STR_PAD_RIGHT);
            if(strlen($decNumberStr) == 2){
                $decNumberStrFill = str_pad($decNumberStr, 9, '0', STR_PAD_LEFT);
                $decCientos = substr($decNumberStrFill, 6);
                $decimales = self::convertGroup($decCientos);
            }
        }
        else if (count($div_decimales) == 1 && $forzarCentimos){
            $decimales = 'CERO ';
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', self::convertGroup($millones));
            }
        }
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', self::convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', self::convertGroup($cientos));
            }
        }
        if(empty($decimales)){
            $valor_convertido = $converted . strtoupper($moneda);
        } else {
            $valor_convertido = $converted . strtoupper($moneda) . ' CON ' . $decimales . ' ' . strtoupper($centimos);
        }
        return $valor_convertido;
    }
    private static function convertGroup($n)
    {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = self::$CENTENAS[$n[0] - 1];
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= self::$UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }
	
}	
