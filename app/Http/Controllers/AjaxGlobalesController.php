<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DestinoDtpmundo;
use App\Persona;
use App\Proforma;
use Session;
use DB;

class AjaxGlobalesController extends Controller
{

	private $id_empresa = 0;
	private $id_usuario = 0;


    private function getIdUsuario(){
    
		if(!$this->id_usuario){
		  $this->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		}
	  
	  return $this->id_usuario;
	}
	
	private function getIdEmpresa(){
	
	  if(!$this->id_empresa){
		 $this->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	  }
	 return $this->id_empresa;
	}



    /**
     * Listado de clientes que pueden facturar
     * Preparado para utilizar con select2 server side y paginacion
     * @return array 
     */
    public function getClientes(Request $req){
        $q = '%'.strtoupper($req->q).'%';
        $id_empresa = $this->getIdEmpresa();
        $pageSize = 10;
        $page = $req->page ?: 1;
        $offset = ($page - 1) * $pageSize;

        $countSql = "SELECT COUNT(*) AS count
            FROM personas p
            JOIN tipo_persona tp on tp.id = p.id_tipo_persona
            WHERE p.activo = true 
            AND  tp.puede_facturar = true
            AND  p.id_empresa = ? 
            AND (p.documento_identidad LIKE ?
                OR p.nombre LIKE ?
                OR p.apellido LIKE ?
                OR p.denominacion_comercial LIKE ?)";

        $querySql = "SELECT 
             CASE WHEN TRIM(p.denominacion_comercial) != '' 
                THEN CONCAT(p.nombre, ' ', p.apellido, ' - ', p.denominacion_comercial,' - ',p.documento_identidad)
                ELSE CONCAT(p.nombre, ' ', p.apellido, ' - ' ,p.documento_identidad)
            END as text,
            p.id
            FROM personas p
            JOIN tipo_persona tp on tp.id = p.id_tipo_persona
            WHERE p.activo = true 
            AND  tp.puede_facturar = true
            AND  p.id_empresa = ? 
            AND (p.documento_identidad LIKE ?
                OR p.nombre LIKE ?
                OR p.apellido LIKE ?
                OR p.denominacion_comercial LIKE ?)
            ORDER BY p.nombre ASC
            LIMIT $pageSize OFFSET $offset";

        $count = DB::select($countSql, [$id_empresa, $q, $q, $q, $q])[0]->count;
        $clientes = DB::select($querySql, [$id_empresa, $q, $q, $q, $q]);

        return response()->json([
            'results' => $clientes,
            'pagination' => [
                'more' => count($clientes) == $pageSize,
            ],
            'total_count' => $count,
        ]);
    }

    /**
     * Listado de destinos presente en las proforma de la empresa
     * Preparado para utilizar con select2 server side y paginacion
     * @return array
     */
    public function getDestinos(Request $req) {
        $q = '%'.strtoupper($req->q).'%';
        $id_empresa = $this->getIdEmpresa();
        $pageSize = 10;
        $page = $req->page ?: 1;
        $offset = ($page - 1) * $pageSize;
    
        $destinos = DestinoDtpmundo::select("destinos_dtpmundo.id_destino_dtpmundo as id",
            DB::raw("CASE 
                        WHEN destinos_dtpmundo.iata_code IS NOT NULL THEN
                        CONCAT(destinos_dtpmundo.desc_destino,' - ',destinos_dtpmundo.iata_code) 
                        ELSE destinos_dtpmundo.desc_destino 
                    END
                        AS text"))
            ->join('proformas', 'proformas.destino_id', '=', 'destinos_dtpmundo.id_destino_dtpmundo')
            ->where('proformas.id_empresa', $id_empresa)
            ->whereNotNull('destino_id')
            ->where(function($query) use ($q) {
                $query->where(DB::raw("UPPER(destinos_dtpmundo.desc_destino)"), 'LIKE', $q)
                ->orWhere(DB::raw("UPPER(iata_code)"), 'LIKE', $q);
            })
            ->groupBy('destinos_dtpmundo.id_destino_dtpmundo', 'destinos_dtpmundo.desc_destino')
            ->orderBy(DB::raw("CASE WHEN UPPER(iata_code) LIKE '$q' THEN 0 ELSE 1 END"))
            ->skip($offset)
            ->take($pageSize)
            ->get();
    
        $total = DestinoDtpmundo::selectRaw("destinos_dtpmundo.id_destino_dtpmundo as id, destinos_dtpmundo.desc_destino as text")
            ->join('proformas', 'proformas.destino_id', '=', 'destinos_dtpmundo.id_destino_dtpmundo')
            ->where('proformas.id_empresa', $id_empresa)
            ->whereNotNull('destino_id')
            ->where(function($query) use ($q) {
                $query->where(DB::raw("UPPER(destinos_dtpmundo.desc_destino)"), 'LIKE', $q)
                ->orWhere(DB::raw("UPPER(iata_code)"), 'LIKE', $q);
            })
            ->groupBy('destinos_dtpmundo.id_destino_dtpmundo', 'destinos_dtpmundo.desc_destino')
            ->count();
   
        $pagination = ['more' => ($offset + $pageSize) < $total];
    
        return response()->json(['results' => $destinos, 'pagination' => $pagination]);
    }

    /**
     * Listado de Vendedor de Agencia
     * Preparado para utilizar con select2 server side y paginacion
     * @return array
     */
    public function getVendedorAgencia(Request $req) {
        $q = '%'.strtoupper($req->q).'%';
        $id_buscar = (int) $req->q; //convertimos en entero para la busqueda de id

        $id_empresa = $this->getIdEmpresa();
        $pageSize = 10;
        $page = $req->page ?: 1;
        $offset = ($page - 1) * $pageSize;
    
        $datos = Persona::selectRaw("CONCAT(id,'-',nombre, ' ', apellido, '-',documento_identidad) AS text, id")
                    ->where('id_tipo_persona', '10')
                    ->where(function($query) use ($q , $id_buscar) {
                        $query->where(DB::raw("UPPER(nombre)"), 'LIKE', $q)
                        ->orWhere(DB::raw("UPPER(apellido)"), 'LIKE', $q)
                        ->orWhere('documento_identidad', 'LIKE', $q)
                        ->orWhere('id', $id_buscar);
                    })
                    ->where('id_empresa', $id_empresa)
                    ->where('activo', true)
                    ->orderBy('nombre', 'ASC')
                    ->skip($offset)
                    ->take($pageSize)
                    ->get();
    
        $total = Persona::where('id_tipo_persona', '10')
                ->where(function($query) use ($q, $id_buscar) {
                    $query->where(DB::raw("UPPER(nombre)"), 'LIKE', $q)
                    ->orWhere(DB::raw("UPPER(apellido)"), 'LIKE', $q)
                    ->orWhere('documento_identidad', 'LIKE', $q)
                    ->orWhere('id', $id_buscar);
                })
                ->where('id_empresa', $id_empresa)
                ->where('activo', true)
                ->count();

        // $datos = array_merge($datos, ['id' => '', 'text' => '']);   
        $pagination = ['more' => ($offset + $pageSize) < $total];
    
        return response()->json(['results' => $datos, 'pagination' => $pagination]);
    }
	
    /**
     * Listado de Personas por tipo de empresa
     * Preparado para utilizar con select2 server side y paginacion
     * @return array
     */
    public function getPersonas(Request $req) {
        $q = '%'.strtoupper($req->q).'%';
        $id_empresa = $this->getIdEmpresa();
        $pageSize = 10;
        $page = $req->page ?: 1;
        $offset = ($page - 1) * $pageSize;
    
        $datos = Persona::selectRaw("CONCAT(nombre, ' ', apellido) AS text, id")
                    ->where(function($query) use ($q) {
                        $query->where(DB::raw("UPPER(nombre)"), 'LIKE', $q)
                        ->orWhere(DB::raw("UPPER(apellido)"), 'LIKE', $q)
                        ->orWhere('documento_identidad', $q);
                    })
                    ->where('id_empresa', $id_empresa)
                    ->where('activo', true)
                    ->orderBy('nombre', 'ASC')
                    ->skip($offset)
                    ->take($pageSize)
                    ->get();
    
        $total = Persona::where('id_tipo_persona', '10')
                ->where(function($query) use ($q) {
                    $query->where(DB::raw("UPPER(nombre)"), 'LIKE', $q)
                    ->orWhere(DB::raw("UPPER(apellido)"), 'LIKE', $q)
                    ->orWhere('documento_identidad', $q);
                })
                ->where('id_empresa', $id_empresa)
                ->where('activo', true)
                ->count();

        // $datos = array_merge($datos, ['id' => '', 'text' => '']);   
        $pagination = ['more' => ($offset + $pageSize) < $total];
    
        return response()->json(['results' => $datos, 'pagination' => $pagination]);
    }


    public function getUsuariosProformas(Request $req){
        $q = '%'.strtoupper($req->q).'%';
        $id_empresa = $this->getIdEmpresa();
        $pageSize = 10;
        $page = $req->page ?: 1;
        $offset = ($page - 1) * $pageSize;

        $usuarios = DB::table('vw_listado_factura')
        ->select('vw_listado_factura.id_usuario AS id', DB::raw("CONCAT(personas.nombre, ' ', personas.apellido) AS text"))
        ->where('vw_listado_factura.id_empresa', '=', $id_empresa)
        ->leftJoin('personas', 'vw_listado_factura.id_usuario', '=', 'personas.id')
        ->where(function($query) use ($q) {
            $query->where(DB::raw("UPPER(nombre)"), 'LIKE', $q)
            ->orWhere(DB::raw("UPPER(apellido)"), 'LIKE', $q)
            ->orWhere('documento_identidad', $q);
        })
        ->distinct('vw_listado_factura.id_usuario');

        $usuariosTotales = DB::table('vw_listado_factura')
        ->select('vw_listado_factura.id_usuario, personas.nombre, personas.apellido')
        ->where('vw_listado_factura.id_empresa', '=', $id_empresa)
        ->leftJoin('personas', 'vw_listado_factura.id_usuario', '=', 'personas.id');

        

        $countSql = $usuariosTotales->count();

        $querySql = $usuarios->take($pageSize)->skip($offset)->get();



        return response()->json([
            'results' => $querySql,
            'pagination' => [
                'more' => count($querySql) == $pageSize,
            ],
            'total_count' => $countSql,
        ]);
    }

    public function getProforma(Request $request)
{
    $id_proforma = (string)$request->q;
    $id_empresa = $this->getIdEmpresa();
    $pageSize = 10;
    $page = $request->page ?: 1;
    $offset = ($page - 1) * $pageSize;

    $proformas = Proforma::select('id',DB::raw('id as text'))
        ->where('id_empresa', $id_empresa)
        ->whereRaw('CAST(id AS VARCHAR) LIKE ?', ['%' . $id_proforma . '%'])
        ->offset($offset) // Utiliza el método offset en lugar de skip
        ->limit($pageSize)
        ->get();

    $proforma_contador = Proforma::select('id',DB::raw('id as text'))
        ->where('id_empresa', $id_empresa)
        ->whereRaw('CAST(id AS VARCHAR) LIKE ?', ['%' . $id_proforma . '%'])
        ->count();

    $countSql = $proforma_contador;

    return response()->json([
        'results' => $proformas,
        'pagination' => [
            'more' => count($proformas) == $pageSize,
        ],
        'total_count' => $countSql,
    ]);
}

    
    

}