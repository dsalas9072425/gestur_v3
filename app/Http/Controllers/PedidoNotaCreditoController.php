<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Session;
use Redirect;
use App\Proforma;
use App\NotaCredito;
use App\ProformasDetalle;
use App\Divisas;
use App\Persona;
use App\AdjuntoDocumento;
use App\PedidosNc;
use App\Voucher;
use App\TipoFactura;
use App\EstadoFactour;
use App\Factura;
use App\FacturaDetalle;
use App\Empresa;
use App\Producto;
Use App\Timbrado;
use App\SolicitudFacturaParcial;
use App\LiquidacionComision;
use App\PasajeroProforma;
use Response;
use Image;
use DB;
use Log;
use Carbon\Carbon;

class PedidoNotaCreditoController extends Controller {



  private function getDatosUsuario(){
  
   $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
   return $persona = Persona::where('id',$idUsuario)->first();

  }//function

  private function getIdUsuario(){
  
  return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
}//function

  private function getIdEmpresa(){

    return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

  }//function


    /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }


       private function formatMoney($num,$f)  {
    
          if($f != 111){
          return number_format($num, 2, ",", ".");  
          } 
          return number_format($num,0,",",".");

        }//function




    public function add(Request $request, $id){
      $id_empresa = $this->getIdEmpresa();
      ini_set('memory_limit', '-1');
      set_time_limit(300);

      $fechaActual = Carbon::now(); //se trae la fecha actual para comparar con la fechad de vencimiento del timbrado en ese caso si es mayor la fecha de vencimiento arroja error
   
      $timbrado = DB::table('timbrados')
      ->where('vencimiento', '>', $fechaActual)
      ->where('id_empresa',$id_empresa)
      ->first();
      //valimamos si la fecha de vencimiento es mayor a la fecha actual nos arroja el mensaje de error y no podra aplicar la nc
      if (!$timbrado) {
          flash('No se puede generar la nota de credito debido a que no hay timbrado válido.')->error();
          return redirect()->route('home');
      }
  
      $factura = Factura::with('cliente',
                      'pasajero',
                      'vendedorAgencia',
                      'vendedorEmpresa',
                      'tipoFactura',
                      'proforma',
                      'currency',
                      'estado',
                      'empresa',
                      'facturaDetalle.libroCompra.currency',
                      'facturaDetalle.currencyVenta',
                      'usuarioFacturacion',
                      'facturaDetalle.proveedor',
                      'facturaDetalle.prestador',
                      'facturaDetalle.producto',
                      'destino'
                      )
                      ->where('id',$id)
                      ->where('id_empresa', '=', $id_empresa)->get();
  
      if(!isset($factura[0]->id)){
          flash('La nota de credito solicitada no existe. Intentelo nuevamente !!')->error();
          return redirect()->route('home');
      }
      
      $id_plan_sistema = Session::get('datos-loggeo')->datos->datosUsuarios->id_plan_sistema;
      $producto = Producto::where('id_empresa',$id_empresa)->get();
      $currency = Divisas::all();

        if(!empty($factura[0]->id_pasajero_principal)){
          if(isset($factura[0]->pasajero->nombre)){         
              $pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id 
                              FROM personas WHERE personas.id = ".$factura[0]->id_pasajero_principal);
          }else{
              $pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id FROM personas WHERE id IN (SELECT pasajero FROM pasajeros_proformas WHERE id_proforma=".$factura[0]->id_proforma." ORDER BY pasajeros_proformas.id LIMIT 1)");
          } 
          $pasjeroNombre = $pasajero[0]->pasajero_n;
        }else{
          $pasjeroNombre ="";
        } 

      $detalles = $factura[0]->facturaDetalle;

      $totalFactura = DB::select('select get_monto_factura as total from get_monto_factura('.$id.')');
      return view('pages.mc.notaCredito.add')->with([
                                                        'facturas'=>$factura,
                                                        'producto'=>$producto,
                                                        'currency'=>$currency,
                                                        'totalFactura'=>$totalFactura,
                                                        'id_plan_sistema'=>$id_plan_sistema,
                                                        'pasjeroNombre'=>$pasjeroNombre,
                                                        'facturaDetalles'=>$detalles,
                                                        'fechaActual'=>$fechaActual
                                                        ]);


    }
 
    public function addVenta(Request $request, $id){
      $factura = Factura::with('cliente',
                              'pasajero',
                              'vendedorAgencia',
                              'vendedorEmpresa',
                              'tipoFactura',
                              'proforma',
                              'currency',
                              'estado',
                              'empresa',
                              'ventaRapida.vendedor',
                              'facturaDetalle.libroCompra.currency',
                              'facturaDetalle.currencyVenta'

                      )
                      ->where('id',$id)
                      ->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
      
      if(!isset($factura[0]->id)){
          flash('La nota de credito solicitada no existe. Intentelo nuevamente !!')->error();
          return redirect()->route('home');
      }

      $id_plan_sistema = Session::get('datos-loggeo')->datos->datosUsuarios->id_plan_sistema;
      $persona = Persona::all();
      $producto = Producto::all();
      $currency = Divisas::all();
      $datos = file_get_contents("../destination_actual.json");
      $destinoJson =  json_decode($datos, true);
      $nombreDestino= '';
        if(isset($factura[0]->destino_id)){
          foreach($destinoJson as $key=>$destino){
            if($destino['idDestino'] == $factura[0]->destino_id){
              $nombreDestino= $destino['desDestino'];
            }
          }
        }  
        if(!empty($factura[0]->id_pasajero_principal)){
          if(isset($factura[0]->pasajero->nombre)){         
              $pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id 
                              FROM personas WHERE personas.id = ".$factura[0]->id_pasajero_principal);
          }else{
              $pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id FROM personas WHERE id IN (SELECT pasajero FROM pasajeros_proformas WHERE id_proforma=".$factura[0]->id_proforma." ORDER BY pasajeros_proformas.id LIMIT 1)");
          } 
          $pasjeroNombre = $pasajero[0]->pasajero_n;
        }else{
          $pasjeroNombre ="";
        } 

      $totalFactura = DB::select('select get_monto_factura as total from get_monto_factura('.$id.')');
      return view('pages.mc.notaCredito.addVenta')->with([
                                                        'facturas'=>$factura,
                                                        'personas'=>$persona,
                                                        'producto'=>$producto,
                                                        'destino'=>$nombreDestino,
                                                        'currency'=>$currency,
                                                        'totalFactura'=>$totalFactura,
                                                        'id_plan_sistema'=>$id_plan_sistema,
                                                        'pasjeroNombre'=>$pasjeroNombre
                                                        ]);


    }


    public function controlarSaldoDisponibleNc(Request $request)
    {
      $data = [];
      $montoNc = DB::select("SELECT COALESCE(SUM(monto_nc), 0) AS monto_nc
        FROM pedidos_nc
        WHERE id_factura = ".$request->input('id_factura')." AND id_factura_detalle = ".$request->input('id_factura_detalle')." AND generado=true");
      if(isset($montoNc[0]->sum)){
        $montoNCs =  $montoNc[0]->sum;
      }else{
        $montoNCs =  0;
      }

      $montoNc = $montoNc[0]->monto_nc;
      // dd($montoNc);
      $totalDetalle = DB::select("SELECT public.get_monto_factura_detalle(".$request->input('id_factura').", ".$request->input('id_factura_detalle').")");
      $totalDetalle = $totalDetalle[0]->get_monto_factura_detalle;
      $data['monto_nc'] =  $montoNCs;
      $data['total_detalle'] =  $totalDetalle;
      // dd($data);

      return response()->json($data);

      // SELECT get_monto_factura_detalle(102088, 21066);
}

  public function doAdd(Request $request){
    //  dd($request->all());
    ini_set('memory_limit', '-1');
    set_time_limit(300);
    $mensaje = new \StdClass; 
    $log_info = "(USERID:".$this->getIdUsuario()." METOD:doAdd)".json_encode($request->all());
    $log_error = "(USERID:".$this->getIdUsuario()." METOD:doAdd) ";

    try{  

      Log::info($log_info);
   //   DB::beginTransaction(); 

      foreach($request->input('data') as $key=>$nota){
        if(isset($nota['monto'])){
          $monto = (float)str_replace(',','.', str_replace('.','',$nota['monto']));
        }else{
          $monto = 0;
        }
        $credito = ( $nota['credito'] != '' ) ? (float)str_replace(',','.', str_replace('.','',$nota['credito'])): 0;
            if($monto != ""){

              $pedidoNc = new PedidosNc;
              $pedidoNc->id_factura = $request->input('factura_id');
              $pedidoNc->id_factura_detalle = $nota['id_detalle'];
              $pedidoNc->monto_nc = $monto;
              $pedidoNc->credito_proveedor =  $credito;
              $pedidoNc->fecha = date('Y-m-d');
              $pedidoNc->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
              $pedidoNc->opcion = $request->input('opcion');
              $pedidoNc->save();
              $idPedido = $pedidoNc->id;


              $saldo_anterior = DB::table('facturas')
              ->where('id', $request->input('factura_id')) 
              //->value('saldo_factura');
              ->first(['saldo_factura']);
              $saldo= $saldo_anterior->saldo_factura;


              //cuando el saldo es 0 y hace la nc no tiene que sumar como positivo en el saldo 
              //se hara tomar la factura anterior y hacer que el saldo sea 0
              /*if($saldo == 0){
                $saldo_cero = DB::table('libros_ventas')
                     ->where('id_documento', $request->input('factura_id'))
                     ->first(['saldo']);
               }  */
             /* if($saldo == 0){
               $saldo_cero = DB::table('libros_ventas')
                    ->where('id_documento', $request->input('factura_id'))
                    ->update(['saldo' == 0]);
              }  */
            }
      }  
      $nro_factura='';
      $notaCreditoRsp = DB::select("SELECT public.generar_nota_credito(".$request->input('factura_id').", ".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.", ".$request->input('opcion').")");
     //se llama a la funcion para refacturar
      if($request->input('opcion') == '3'){
        $nro_factura=$this->proformaRefacturacion($request,$saldo);
        $response = $notaCreditoRsp[0]->generar_nota_credito;
        $cero = explode('_', $response);
        $id_nota_credito = trim($cero[1]);
        if($saldo == 0){
          $saldo_cero = DB::table('libros_ventas')
               ->where('id_documento', $id_nota_credito)
               ->update(['saldo' =>0]);
        }

  
      }    
      //fin llamado



        $buscar = 'OK';

        $response = $notaCreditoRsp[0]->generar_nota_credito;
        $resultado = strpos($response, $buscar); 

        if($resultado !== FALSE){
            $desgloceResponse = explode('_', $response);
            $mensaje->status = 'OK';
            $mensaje->id = trim($desgloceResponse[1]);
            $mensaje->mensaje = 'Se ha generado la nota de crédito';
            if($request->input('opcion') == '3'){
              $mensaje->mensaje = 'Se ha generado la nota de crédito y la factura nro: '. $nro_factura;
            }
           /* if($id_empresa == 17){
              $this->generarJsonNC($desgloceResponse[1]);
            }*/
    
        }else{
            $mensaje->status = 'ERROR';
            $mensaje->id = 0;
            $mensaje->mensaje = 'Ha ocurrido un inconveniente al generar la nota de crédito';
        } 

   //    DB::commit();

      } catch(\Exception $e){
            Log::error($e);
            $mensaje->status = 'ERROR';
            $mensaje->id = 0;
            //$mensaje->mensaje = 'No se ha generado la nota de crédito. Inténtelo nuevamente';
            $mensaje->mensaje = 'Ocurrio un error al momento de realizar la NC';
            DB::rollBack();
      } 


    return response()->json($mensaje);

  }	

    public function index(Request $request){

      $personaLogin = $this->getDatosUsuario();
    
            //DATOS DE USUARIO
        $tipoPersona = $personaLogin->id_tipo_persona;
        $idUsuario = $personaLogin->id;
        $sucursalEmpresa = $personaLogin->id_sucursal_empresa;


          $nota = DB::table('nota_credito as nc');
          $nota = $nota->select(
                                'nc.id',
                                DB::raw("to_char(nc.fecha_hora_nota_credito ,'DD/MM/YYYY') as fecha_nota_credito"),
                                'nc.fecha_hora_nota_credito',
                                'nc.total_neto_nc',
                                'nc.id_moneda_venta',
                                 'nc.id_proforma',
                                 'nc.nro_nota_credito',
                                'f.nro_factura',
                                'cl.nombre as cliente_n',
                                'cl.apellido as cliente_a',
                                'vd.nombre as vendedor_n',
                                'vd.apellido as vendedor_a',
                                'cu.currency_code',
                                'e.denominacion',
                                'us.id_sucursal_empresa',
                                'lv.saldo as saldo_nota_credito'
                              );
          $nota = $nota->leftJoin('facturas as f', 'f.id', '=','nc.id_factura');
          $nota = $nota->leftJoin('personas as cl', 'cl.id', '=','nc.cliente_id');
          $nota = $nota->leftJoin('personas as vd', 'vd.id', '=','nc.vendedor_id');
          $nota = $nota->leftJoin('currency as cu', 'cu.currency_id', '=','nc.id_moneda_venta');
          $nota = $nota->leftJoin('estados as e', 'e.id', '=','nc.id_estado_nc');
          $nota = $nota->leftJoin('personas as us', 'us.id', '=','nc.id_usuario');
          $nota = $nota->leftJoin('libros_ventas as lv', 'lv.id_documento', '=','nc.id');
          $nota = $nota->distinct();

          //REGLAS ESPECIALES
           //SUPERVISOR EMPRESA
          if($tipoPersona == '4'){
               $nota = $nota->where('nc.id_sucursal_empresa',$sucursalEmpresa);
          }
          //VENDEDOR EMPRESA
          if($tipoPersona == '3'){
              $nota = $nota->where('nc.id_usuario',$idUsuario);  
          }//if

          $nota = $nota->where('nc.id_empresa',$this->getIdEmpresa());  
          
          $nota = $nota->get();

          //FORMATEAR MONEDA
          foreach ($nota as $key => $value) {    
            $value->saldo_nota_credito= $this->formatMoney($value->saldo_nota_credito,$value->id_moneda_venta); 
            $value->total_neto_nc_format = $this->formatMoney($value->total_neto_nc,$value->id_moneda_venta);
          }

      //  $getTipoEstado = EstadoFactour::where('id_tipo_estado','11')->get();

      $cliente = DB::select("
      SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
      concat(p.nombre,' ',p.apellido,' ','- '|| trim(p.denominacion_comercial)) as full_data
      FROM personas p
      JOIN tipo_persona tp on tp.id = p.id_tipo_persona
      WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
      AND p.activo = true and p.id_empresa = ".$this->getIdEmpresa()." order by p.nombre asc");

      $getFacturaVendedorEmpresa = Factura::with('vendedorEmpresa')->where('id_empresa',$this->getIdEmpresa())->distinct()->get(['vendedor_id']);
      $getDivisa = Divisas::where('activo','S')->get();
      $getTipoFactura = TipoFactura::get();

      	return view('pages.mc.notaCredito.index')->with([
                                                       'resultados'=>$nota,
                                                       'monedas'=>$getDivisa,
                                                       'tipoFactura'=>$getTipoFactura,
                                                       'clientes'=>$cliente,
                                                       'vendedorEmpresa'=>$getFacturaVendedorEmpresa,
                                                      //  'tipoEstado'=>$getTipoEstado
                                                        ]);

    }//function

    public function consultaNota(Request $request){
      
      $personaLogin = $this->getDatosUsuario();
    
            //DATOS DE USUARIO
        $tipoPersona = $personaLogin->id_tipo_persona;
        $idUsuario = $personaLogin->id;
        $sucursalEmpresa = $personaLogin->id_sucursal_empresa;
          /*
            2 "ADMIN_EMPRESA"
            3 "VENDEDOR_EMPRESA"
            4 "SUPERVISOR_EMPRESA"
            5 "ADMINISTRATIVO_EMPRESA"
            6 "OPERATIVO_EMPRESA"
           */
          //VALIDAR SI ES EL TIPO DE PERSONA QUE PUEDE VER FACTURAS
          // if($tipoPersona == 3 || $tipoPersona == 4 || $tipoPersona == 2 || $tipoPersona == 5 || $tipoPersona == 6){
          // 
          $nota = DB::table('nota_credito as nc');
          $nota = $nota->select('nc.*',
                                 DB::raw("to_char(nc.fecha_hora_nota_credito ,'DD/MM/YYYY') as fecha_nota_credito"),
                                'nc.fecha_hora_nota_credito',
                                'nc.saldo_nota_credito as nota_credito_saldo',
                                'f.nro_factura',
                                'f.id_proforma',
                                'cl.nombre as cliente_n',
                                'cl.apellido as cliente_a',
                                'vd.nombre as vendedor_n',
                                'vd.apellido as vendedor_a',
                                'cu.currency_code',
                                'e.denominacion',
                                'us.id_sucursal_empresa',
                                'lv.saldo as saldo_nota_credito',
                                'pn.opcion'
                              );
          $nota = $nota->leftJoin('facturas as f', 'f.id', '=','nc.id_factura');
          $nota = $nota->leftJoin('personas as cl', 'cl.id', '=','nc.cliente_id');
          $nota = $nota->leftJoin('personas as vd', 'vd.id', '=','nc.vendedor_id');
          $nota = $nota->leftJoin('currency as cu', 'cu.currency_id', '=','nc.id_moneda_venta');
          $nota = $nota->leftJoin('estados as e', 'e.id', '=','nc.id_estado_nc');
          $nota = $nota->leftJoin('personas as us', 'us.id', '=','nc.id_usuario');
          $nota = $nota->leftJoin('libros_ventas as lv', 'lv.id_documento', '=','nc.id');
          $nota = $nota->leftJoin('pedidos_nc as pn', 'pn.id_factura', '=','nc.id_factura');
          $nota = $nota->distinct();
          //REGLAS ESPECIALES
           //SUPERVISOR EMPRESA
          if($tipoPersona == '4'){
               $nota = $nota->where('nc.id_sucursal_empresa',$sucursalEmpresa);
          }
          //VENDEDOR EMPRESA
          if($tipoPersona == '3'){
              $nota = $nota->where('nc.id_usuario',$idUsuario);  
          }//if

          //FILTRO DE FORMULARIO
           if($request->input('cliente_id') != ''){
               $nota = $nota->where('nc.cliente_id',trim($request->input('cliente_id')));  
           }
            if($request->input('idMoneda') != ''){
               $nota = $nota->where('nc.id_moneda_venta',trim($request->input('idMoneda')));  
           }
            if($request->input('vendedor_id') != ''){
               $nota = $nota->where('nc.vendedor_id',trim($request->input('vendedor_id')));  
           }
            if($request->input('numFactura') != ''){
               $nota = $nota->where('nro_factura', $request->input('numFactura'));  
           }
            if($request->input('numNotaCredito') != ''){
               $nota = $nota->where('nc.nro_nota_credito',trim($request->input('numNotaCredito')));  
           }
            if($request->input('numProforma') != ''){
               $nota = $nota->where('nc.id_proforma',trim($request->input('numProforma')));  
           }
            if($request->input('periodo_fecha_facturacion') != ''){
                $fecha = explode('-', $request->input('periodo_fecha_facturacion'));
                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
                // dd(array($desde.' 00:00:00',$hasta.' 23:59:59'));
                $nota = $nota->whereBetween('nc.fecha_hora_nota_credito',array($desde.' 00:00:00',$hasta.' 23:59:59'));  
           }
        
           if($request->input('tipoEstado') && count($request->input('tipoEstado'))){

                if( in_array(1, $request->input('tipoEstado')) && !in_array(2, $request->input('tipoEstado'))){
                  $nota = $nota->where('nc.saldo_nota_credito','>',0);
                } else if( in_array(2, $request->input('tipoEstado')) && !in_array(1, $request->input('tipoEstado')) ){
                  $nota = $nota->where('nc.saldo_nota_credito','<=',0);
                } 
                // else Si es combinado no importa el saldo
 
                if(in_array(3, $request->input('tipoEstado')) && !in_array(2, $request->input('tipoEstado')) && !in_array(1, $request->input('tipoEstado'))){
                  $nota =  $nota->where('nc.id_estado_nc',35);
                } else if( in_array(3, $request->input('tipoEstado')) && ( in_array(2, $request->input('tipoEstado')) || in_array(1, $request->input('tipoEstado')) ) ){
                  $nota =  $nota->whereIn('nc.id_estado_nc',[36,35]);
                } else if ( in_array(2, $request->input('tipoEstado')) || in_array(1, $request->input('tipoEstado')) ){
                  $nota = $nota->where('nc.id_estado_nc',36);
                }
            
           }  

            $nota = $nota->where('nc.id_empresa',$this->getIdEmpresa());  

            $nota = $nota->orderBy('fecha_hora_nota_credito','DESC');  

            $nota = $nota->get();
    
           

             //FORMATEAR MONEDA
              $resultado = [];
              foreach ($nota as $key => $value) {  
                $contador = DB::select("SELECT count(*) as contador FROM nota_credito_aplicacion where id_nota_credito =".$value->id);
                if($contador[0]->contador == 0){
                  $contadorCount = 1;
                }else{
                  $contadorCount = $contador[0]->contador;
                }
                $value->contador= $contadorCount;
                $value->saldo_nota_credito= $this->formatMoney($value->saldo_nota_credito,$value->id_moneda_venta); 
                $value->total_neto_nc_format = $this->formatMoney($value->total_neto_nc,$value->id_moneda_venta);
                $resultado[] = $value;

              }
          return response()->json($resultado);

    } 

    public function verNota(Request $request, $id){

      ini_set('memory_limit', '-1');
      set_time_limit(300);
      
      $btn = $this->btnPermisos(array('id_nota_cred'=>$id),'verNotaCredito');
      
      $permiso = DB::select("select * from libros_ventas where id_documento =".$id);
      if(empty($permiso)){
        $permiso = 1;
      }else{
        $permiso = 0;
      }  

      $resultados = NotaCredito::with('detalleNotaCredito','factura','currency','cliente','vendedorEmpresa','estado', 'pasajero','libroVenta','libroVentaSaldo0')
                                ->where('id',$id)
                                ->where('id_empresa',$this->getIdEmpresa())
                                ->get();

      if(!isset($resultados[0]->id)){
        flash('La nota de credito solicitada no existe. Intentelo nuevamente !!')->error();
        return redirect()->route('home');
      }
    
      $datos = file_get_contents("../destination_actual.json");
      $destinoJson =  json_decode($datos, true);
      $nombreDestino= '';
      foreach($destinoJson as $key=>$destino){
        if($destino['idDestino'] == $resultados[0]->destino_id){
          $nombreDestino= $destino['desDestino'];
        }
      }
      $currency = Divisas::all();
      $persona = Persona::all();
      $producto = Producto::all();
      $perfil = Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil;
      $liquidacion =  new \StdClass;
      if($resultados[0]->id_liquidacion_comision != 0 || $resultados[0]->id_liquidacion_comision != 0 ){
        $liquidacionBase = LiquidacionComision::where('id', $resultados[0]->id_liquidacion_comision)->first();
        $liquidacion->id = $resultados[0]->id_usuario."_".substr($liquidacionBase->periodo, 0, -4)."_".substr($liquidacionBase->periodo, -4)."_".$resultados[0]->id_liquidacion_comision;
        $liquidacion->periodo = $this->getFecha(substr($liquidacionBase->periodo, 0, -4),substr($liquidacionBase->periodo, -4));
      }
      
     return view('pages.mc.notaCredito.verNota')->with([
                                                       'resultados'=>$resultados,
                                                        'destino'=>$nombreDestino,
                                                        'personas'=>$persona,
                                                        'producto'=>$producto,
                                                        'currency'=>$currency,
                                                        'btn'=>$btn,
                                                        'perfil'=>$perfil,
                                                        'permiso'=>$permiso,
                                                        'liquidacion'=>$liquidacion
                                                        ]);
    }

    public function verNotaVenta(Request $request, $id){
      
      $btn = $this->btnPermisos(array('id_nota_cred'=>$id),'verNotaCredito');
      
      $permiso = DB::select("select * from libros_ventas where id_documento =".$id);
      if(empty($permiso)){
        $permiso = 1;
      }else{
        $permiso = 0;
      }  

      $resultados = NotaCredito::with('detalleNotaCredito',
                                      'factura',
                                      'currency',
                                      'cliente',
                                      'vendedorEmpresa',
                                      'estado',
                                      'pasajero',
                                      'libroVenta',
                                      'factura.ventaRapida.vendedor'
                                      )
                                ->where('id',$id)
                                ->where('id_empresa',$this->getIdEmpresa())
                                ->get();

      if(!isset($resultados[0]->id)){
        flash('La nota de credito solicitada no existe. Intentelo nuevamente !!')->error();
        return redirect()->route('home');
      }
      $datos = file_get_contents("../destination_actual.json");
      $destinoJson =  json_decode($datos, true);
      $nombreDestino= '';
      foreach($destinoJson as $key=>$destino){
        if($destino['idDestino'] == $resultados[0]->destino_id){
          $nombreDestino= $destino['desDestino'];
        }
      }
      $currency = Divisas::all();
      $persona = Persona::all();
      $producto = Producto::all();
      $perfil = Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil;
      $liquidacion =  new \StdClass;
      if($resultados[0]->id_liquidacion_comision != 0 || $resultados[0]->id_liquidacion_comision != 0 ){
             $liquidacionBase = LiquidacionComision::where('id', $resultados[0]->id_liquidacion_comision)->first();
             $liquidacion->id = $resultados[0]->id_usuario."_".substr($liquidacionBase->periodo, 0, -4)."_".substr($liquidacionBase->periodo, -4)."_".$resultados[0]->id_liquidacion_comision;
             $liquidacion->periodo = $this->getFecha(substr($liquidacionBase->periodo, 0, -4),substr($liquidacionBase->periodo, -4));
      }

      return view('pages.mc.notaCredito.verNotaVenta')->with([
                                                       'resultados'=>$resultados,
                                                        'destino'=>$nombreDestino,
                                                        'personas'=>$persona,
                                                        'producto'=>$producto,
                                                        'currency'=>$currency,
                                                        'btn'=>$btn,
                                                        'perfil'=>$perfil,
                                                        'liquidacion'=>$liquidacion,
                                                        'permiso'=>$permiso
                                                        ]);
    }

    public function anularNota(Request $request){
      $anular = DB::select('SELECT public."anular_nota_credito"('.$request->input('idNota').','.Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.')');
      return response()->json($anular);
    }//function


    private function getFecha($mes, $anho)      
  	{
  		$month = $mes; //Reemplazable por número del 1 a 12
		$year = $anho; //Reemplazable por un año valido
		
		switch(date('n',mktime(0, 0, 0, $month, 1, $year)))
		{
			case 1: $Mes = "Enero"; break;
			case 2: $Mes = "Febrero"; break;
			case 3: $Mes = "Marzo"; break;
			case 4: $Mes = "Abril"; break;
			case 5: $Mes = "Mayo"; break;
			case 6: $Mes = "Junio"; break;
			case 7: $Mes = "Julio"; break;
			case 8: $Mes = "Agosto"; break;
			case 9: $Mes = "Septiembre"; break;
			case 10: $Mes = "Octubre"; break;
			case 11: $Mes = "Noviembre"; break;
			case 12: $Mes = "Diciembre"; break;
		}
		
		return $Mes.'/'.date('Y',mktime(0, 0, 0, $month, 1, $year));
	}



        public function btnPermisos($parametros, $vista){

  $btn = DB::select("SELECT pe.* 
                        FROM persona_permiso_especiales p, permisos_especiales pe
                        WHERE p.id_permiso = pe.id 
                        AND p.id_persona = ".$this->getIdUsuario() ." 
                        AND pe.url = '".$vista."' ");

  $htmlBtn = '';
  $boton =  array();
  $idParametro = '';

  // dd( $btn );

  if(!empty($btn)){

  foreach ($btn as $key => $value) {

    $idParametro = '';
    $ruta = 'factour.'.$value->accion;
     $htmlBtn = '';



     //LLEVA PARAMETRO
    if($value->lleva_parametro == '1'){

    foreach ($parametros as $indice=>$valor) { 

      if($indice == $value->nombre_parametro){
        $idParametro = $valor;
      }
    }

    //PARAMETRO OCULTO EN DATA
    if($value->parametro_oculto == '1'){
       $htmlBtn = "<a role='button' class='".$value->clase."' data-btn='".$idParametro."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    } else {
       $htmlBtn = "<a role='button' href='".route($ruta,['id'=>$idParametro])."' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    }


   



    } else {
      $htmlBtn = "<a role='button' href='#' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i>".$value->texto_alternativo." </a>"; 
    }

    
    $boton[] = $htmlBtn;
  }
   return $boton;
 
   } else {
    return array();
   }



}//function


      public function getGenearLibroVenta(Request $request){
        $libro_venta = DB::select("SELECT public.generar_libro_venta_nc(".$request->input('id_nc').", ".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.")");
        $respuesta = 'OK';
        return response()->json($respuesta);

      }
  
  
      public function getNotaCredito(Request $req){
        $flag = 0;
        $draw = intval($req->draw);
        $start = intval($req->start);
        $length = intval($req->length);
        $cont = 0;
        $filtrarMax = 0;

        $factura = DB::table('vw_listado_factura');
        $factura = $factura->where('id_empresa',$this->getIdEmpresa());
        $factura = $factura->where('saldo_factura','>',0);
        $factura = $factura->where('id_estado_cobro','=',31);
        $factura = $factura->where('id_estado_factura','!=',30);
        $factura = $factura->where('id_tipo_factura','=',1);//ARTURO
        $factura = $factura->where('id_moneda_venta','=',$req->input('datamoneda'));
        $factura = $factura->where('cliente_id','=',$req->input('idCliente'));
        $factura = $factura->get();
        /*echo '<pre>';
        print_r($factura);*/
        return response()->json($factura);
    }  

    public function guardarNotaCredito(Request $request){
        $aplicar_nc_factura = DB::select("SELECT public.aplicar_nc_factura(".$request->input('id_nc').", ".(float)str_replace(',','.', str_replace('.','',$request->input('saldo_credito'))).", ".$request->input('id_factura').", ".(float)str_replace(',','.', str_replace('.','',$request->input('saldo_factura'))).", ".$this->getIdUsuario().")");
        return response()->json($aplicar_nc_factura[0]->aplicar_nc_factura);
    } 

    public function getExtractoNC(Request $request){
       $factura = New   \StdClass;
      $exacto =  DB::table('vw_extracto_nc');
      $exacto =  $exacto->where('id_nc',$request->input("idFactura"));
      $exacto =  $exacto->get();
      return response()->json(['exacto'=>$exacto]);

    } 

    public function proformaRefacturacion(Request $request,$saldo){
      $id_proforma = $request->input('id_proforma');
      $id_cliente = $request->input('cliente_id');
      $id_factura_vieja = $request->input('factura_vieja');

      $proforma = DB::table('proformas')->where('id', $id_proforma)->first();
      $id_factura = $proforma->factura_id;
      $factura = DB::table('facturas')->where('id', $id_factura)->first(['id_timbrado']);
      $id_timbrado = $factura->id_timbrado;
      if($id_cliente==null){ 
        $id_cliente=$proforma->cliente_id;
       }
      $proforma_detalles = DB::table('proformas_detalle')->where('id_proforma', $id_proforma)->where('activo',true)->get();
   
      //$pasajeros= DB::table('pasajeros_proformas')->where('id_proforma',$id_proforma)->get();
      //capturar los pasajeros de la proforma anterior y pasar a la nueva proforma
      //recorrer el id proforma detalle y pasar al nuevo id_proforma
    //hacer un select y un insert de la proforma

    if (!$proforma) {
      // Si no se encuentra la proforma con el id_proforma 
      $mensaje = new \StdClass; 
      $mensaje->status = 'ERROR';
      $mensaje->mensaje = 'La proforma no existe.';
      return json_encode($mensaje);
    }

  // Crear una nueva proforma con los datos obtenidos
    // try {
    //     DB::beginTransaction(); // Iniciar transacción
        $nuevaProforma = new Proforma;
        
        // Copiar todos los campos de la proforma original a la nueva proforma
       // $nuevaProforma->fill((array)$proforma);
       $nuevaProforma = new Proforma;

       $nuevaProforma->id_usuario = $proforma->id_usuario;
       $nuevaProforma->fecha_alta = $proforma->fecha_alta;
       $nuevaProforma->tipo_factura_id = $proforma->tipo_factura_id;
      // $nuevaProforma->estado_id = $proforma->estado_id;
       $nuevaProforma->estado_id = 1;
       $nuevaProforma->check_in = $proforma->check_in;
       $nuevaProforma->check_out = $proforma->check_out;
       $nuevaProforma->id_moneda_venta = $proforma->id_moneda_venta;
       $nuevaProforma->cotizacion = $proforma->cotizacion;
       $nuevaProforma->proforma_padre = $proforma->proforma_padre;
       $nuevaProforma->total_costos = $proforma->total_costos;
       $nuevaProforma->vendedor_id = $proforma->vendedor_id;
       $nuevaProforma->vencimiento = $proforma->vencimiento;
       $nuevaProforma->fecha_gasto = $proforma->fecha_gasto;
       $nuevaProforma->destino_id = $proforma->destino_id;
       $nuevaProforma->cliente_id = $id_cliente;
       $nuevaProforma->pago_tarjeta = $proforma->pago_tarjeta;
       //$nuevaProforma->usuario_facturacion_id = $proforma->usuario_facturacion_id;
       //$nuevaProforma->factura_id = $proforma->factura_id;
       $nuevaProforma->total_bruto_facturar = $proforma->total_bruto_facturar;
       $nuevaProforma->total_neto_facturar = $proforma->total_neto_facturar;
       $nuevaProforma->pasajero_id = $proforma->pasajero_id;
       $nuevaProforma->senha = $proforma->senha;
       $nuevaProforma->tipo_facturacion = $proforma->tipo_facturacion;
       $nuevaProforma->id_empresa = $proforma->id_empresa;
       $nuevaProforma->responsable_id = $proforma->responsable_id;
       //$nuevaProforma->fecha_facturacion = $proforma->fecha_facturacion;
       $nuevaProforma->fecha_anulacion = $proforma->fecha_anulacion;
       $nuevaProforma->usuario_anulacion_id = $proforma->usuario_anulacion_id;
       $nuevaProforma->total_exentas = $proforma->total_exentas;
       $nuevaProforma->total_gravadas = $proforma->total_gravadas;
       $nuevaProforma->total_iva = $proforma->total_iva;
       $nuevaProforma->total_comision = $proforma->total_comision;
       $nuevaProforma->total_no_comisiona = $proforma->total_no_comisiona;
       $nuevaProforma->ganancia_venta = $proforma->ganancia_venta;
       $nuevaProforma->incentivo_vendedor_agencia = $proforma->incentivo_vendedor_agencia;
       $nuevaProforma->comision_vendedor_empresa = $proforma->comision_vendedor_empresa;
       $nuevaProforma->markup = $proforma->markup;
       $nuevaProforma->renta = $proforma->renta;
       $nuevaProforma->porcentaje_ganancia = $proforma->porcentaje_ganancia;
       $nuevaProforma->tiene_incentivo_vendedor_agencia = $proforma->tiene_incentivo_vendedor_agencia;
       $nuevaProforma->id_usuario_autorizacion = $proforma->id_usuario_autorizacion;
       $nuevaProforma->fecha_hora_autorizacion = $proforma->fecha_hora_autorizacion;
       $nuevaProforma->pasajero_online = $proforma->pasajero_online;
       $nuevaProforma->id_prioridad = $proforma->id_prioridad;
       $nuevaProforma->id_promocion = $proforma->id_promocion;
       $nuevaProforma->id_grupo = $proforma->id_grupo;
       $nuevaProforma->cantidad_pasajero = $proforma->cantidad_pasajero;
       $nuevaProforma->id_tarifa = $proforma->id_tarifa;
       $nuevaProforma->calificacion = $proforma->calificacion;
       $nuevaProforma->comentario_calificacion = $proforma->comentario_calificacion;
       $nuevaProforma->id_usuario_post_venta = $proforma->id_usuario_post_venta;
       $nuevaProforma->fecha_hora_post_venta = $proforma->fecha_hora_post_venta;
       $nuevaProforma->id_fp = $proforma->id_fp;
       $nuevaProforma->emergencia = $proforma->emergencia;
       $nuevaProforma->file_codigo = $proforma->file_codigo;
       $nuevaProforma->reserva_nemo_id = $proforma->reserva_nemo_id;
       $nuevaProforma->comision_operador = $proforma->comision_operador;
       $nuevaProforma->comision_iva_operador = $proforma->comision_iva_operador;
       $nuevaProforma->id_unidad_negocio = $proforma->id_unidad_negocio;
       $nuevaProforma->incentivo_venta = $proforma->incentivo_venta;
       $nuevaProforma->concepto_generico = $proforma->concepto_generico;
       $nuevaProforma->imprimir_precio_unitario_venta = $proforma->imprimir_precio_unitario_venta;
       $nuevaProforma->renta_extra = $proforma->renta_extra;
       $nuevaProforma->renta_no_comisionable = $proforma->renta_no_comisionable;
       $nuevaProforma->renta_neta = $proforma->renta_neta;
       $nuevaProforma->saldo_por_facturar = $proforma->saldo_por_facturar;
       $nuevaProforma->facturacion_parcial = $proforma->facturacion_parcial;
       $nuevaProforma->id_asistente = $proforma->id_asistente;
       $nuevaProforma->procesado = $proforma->procesado;
       $nuevaProforma->id_equipo = $proforma->id_equipo;
       $nuevaProforma->save();

    

      $pasajeros = DB::table('pasajeros_proformas')->where('id_proforma', $id_proforma)->get();
      foreach ($pasajeros as $pasajero) {
          $nuevoPasajero = new PasajeroProforma;
          $nuevoPasajero->id_proforma = $nuevaProforma->id;
          $nuevoPasajero->pasajero = $pasajero->pasajero;
          $nuevoPasajero->save();
      }
    
      DB::table('tickets')
          ->where('id_proforma', $id_proforma)
          ->update(['id_proforma' =>$nuevaProforma->id]);

      //die();
         /****PARA LOS DETALLES RECORRER EL ANTERIOR Y PASARLE AL NUEVO ID QUE GENERO******/
        foreach ($proforma_detalles as $detalle) {
          $nuevoDetalle = new ProformasDetalle;
          $nuevoDetalle->id_proforma = $nuevaProforma->id; // Asignar el nuevo id_proforma generado
         // $nuevoDetalle->fill((array)$detalle); // Copiar todos los campos del detalle original al nuevo detalle
          //dd($nuevoDetalle);
          $nuevoDetalle->id_proforma = $nuevaProforma->id;
          $nuevoDetalle->item = $detalle->item;
          $nuevoDetalle->id_producto = $detalle->id_producto;
          $nuevoDetalle->precio_costo = $detalle->precio_costo;
          $nuevoDetalle->precio_venta = $detalle->precio_venta;
          $nuevoDetalle->cantidad = $detalle->cantidad;
          $nuevoDetalle->porcion_exenta = $detalle->porcion_exenta;
          $nuevoDetalle->porcion_gravada = $detalle->porcion_gravada;
          $nuevoDetalle->descripcion = $detalle->descripcion;
          $nuevoDetalle->comision_agencia = $detalle->comision_agencia;
          $nuevoDetalle->currency_costo_id = $detalle->currency_costo_id;
          $nuevoDetalle->currency_venta_id = $detalle->currency_venta_id;
          $nuevoDetalle->porcentaje_comision_agencia = $detalle->porcentaje_comision_agencia;
          $nuevoDetalle->markup = $detalle->markup;
          $nuevoDetalle->total_pago = $detalle->total_pago;
          $nuevoDetalle->renta = $detalle->renta;
          $nuevoDetalle->dtplus = $detalle->dtplus;
          $nuevoDetalle->cod_confirmacion = $detalle->cod_confirmacion;
          $nuevoDetalle->fecha_in = $detalle->fecha_in;
          $nuevoDetalle->fecha_out = $detalle->fecha_out;
          $nuevoDetalle->id_tipo_traslado = $detalle->id_tipo_traslado;
          $nuevoDetalle->asistencia_dias = $detalle->asistencia_dias;
          $nuevoDetalle->id_proveedor = $detalle->id_proveedor;
          $nuevoDetalle->id_prestador = $detalle->id_prestador;
          $nuevoDetalle->id_tarifa_asistencia = $detalle->id_tarifa_asistencia;
          $nuevoDetalle->updated_at = $detalle->updated_at;
          $nuevoDetalle->activo = $detalle->activo;
          $nuevoDetalle->created_at = $detalle->created_at;
          $nuevoDetalle->id_usuario = $detalle->id_usuario;
          $nuevoDetalle->asistencia_cantidad_personas = $detalle->asistencia_cantidad_personas;
          $nuevoDetalle->fecha_pago_proveedor = $detalle->fecha_pago_proveedor;
          $nuevoDetalle->voucher_generado = $detalle->voucher_generado;
          $nuevoDetalle->tarjeta_id = $detalle->tarjeta_id;
          $nuevoDetalle->cod_autorizacion = $detalle->cod_autorizacion;
          $nuevoDetalle->fecha_cancelacion = $detalle->fecha_cancelacion;
          $nuevoDetalle->monto_gasto = $detalle->monto_gasto;
          $nuevoDetalle->fecha_gasto = $detalle->fecha_gasto;
          $nuevoDetalle->cantidad_pasajero = $detalle->cantidad_pasajero;
          $nuevoDetalle->id_empresa = $detalle->id_empresa;
          $nuevoDetalle->monto_no_comisionable = $detalle->monto_no_comisionable;
          $nuevoDetalle->diferencia_tributaria = $detalle->diferencia_tributaria;
          $nuevoDetalle->iva_porcion_gravada = $detalle->iva_porcion_gravada;
          $nuevoDetalle->neto_facturar = $detalle->neto_facturar;
          $nuevoDetalle->hora_in = $detalle->hora_in;
          $nuevoDetalle->hora_out = $detalle->hora_out;
          $nuevoDetalle->vuelo_in = $detalle->vuelo_in;
          $nuevoDetalle->vuelo_out = $detalle->vuelo_out;
          $nuevoDetalle->origen = $detalle->origen;
          $nuevoDetalle->fee_exento = $detalle->fee_exento;
          $nuevoDetalle->fee_gravado = $detalle->fee_gravado;
          $nuevoDetalle->costo_proveedor = $detalle->costo_proveedor;
          $nuevoDetalle->monto_no_comisionable_proveedor = $detalle->monto_no_comisionable_proveedor;
          $nuevoDetalle->verificado_operativo = $detalle->verificado_operativo;
          $nuevoDetalle->prestador_detalle = $detalle->prestador_detalle;
          $nuevoDetalle->factura_prestador = $detalle->factura_prestador;
          $nuevoDetalle->costo_gravado = $detalle->costo_gravado;
          $nuevoDetalle->fee_unico = $detalle->fee_unico;
          $nuevoDetalle->costo_total_proveedor = $detalle->costo_total_proveedor;
          $nuevoDetalle->reserva_nemo_id = $detalle->reserva_nemo_id;
          $nuevoDetalle->id_detalle_proforma_padre = $detalle->id_detalle_proforma_padre;
          $nuevoDetalle->comision_operador = $detalle->comision_operador;
          $nuevoDetalle->comision_iva_operador = $detalle->comision_iva_operador;
          $nuevoDetalle->id_libro_compra = $detalle->id_libro_compra;
          $nuevoDetalle->porcentaje_renta_item = $detalle->porcentaje_renta_item;
          $nuevoDetalle->renta_minima_producto = $detalle->renta_minima_producto;
          $nuevoDetalle->cumple_rentabilidad_minima = $detalle->cumple_rentabilidad_minima;
          $nuevoDetalle->renta_sin_comision_operador = $detalle->renta_sin_comision_operador;
          $nuevoDetalle->comision_moneda_costo = $detalle->comision_moneda_costo;
          $nuevoDetalle->cotizacion_costo = $detalle->cotizacion_costo;
          $nuevoDetalle->id_usuario_anulacion = $detalle->id_usuario_anulacion;
          $nuevoDetalle->fecha_anulacion = $detalle->fecha_anulacion;
          $nuevoDetalle->refacturacion = true;
          $nuevoDetalle->save();
      }
  //obtengo el nuevo id_proforma y pasar a la funcion facturar
  $newIdProforma = $nuevaProforma->id;


  //se preparapa para hacer la factura 
// Llamar a la función facturar en la base de datos solo con el nuevo id_proforma
//id de factura, id de usuario que esta facturando y el id de punto de venta
$cargarFactura = DB::select('SELECT * FROM facturar(?,?,?)', [$newIdProforma,$id_timbrado,Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario])[0]->facturar;
if ($cargarFactura != 'OK') {
  throw new \Exception($cargarFactura);
}
$obtener_factura = DB::table('facturas')
    ->where('id_proforma', $newIdProforma)
    ->first();

//$id_factura_vieja=$obtener_factura->id;
// Inserto un registro en la tabla facturas factura_anterior
/*DB::table('facturas')->insert([
  'id_factura_anterior' => $id_factura_vieja
]);
*/

DB::table('facturas')
->where('id', $obtener_factura->id)
->update(['id_factura_anterior' => $id_factura_vieja]);

    DB::table('libros_ventas')
    ->where('id_documento', $obtener_factura->id)
    ->update(['saldo' => $saldo]);



    
    return $obtener_factura->nro_factura;
//para los saldos 



      //DB::commit(); // Confirmar transacción
      

        $mensaje = new \StdClass; 
        $mensaje->status = 'SUCCESS';
        $mensaje->mensaje = 'Proforma refacturada correctamente.';
    //  } catch (\Illuminate\Database\QueryException $e) {
    //     DB::rollback(); // Revertir transacción en caso de error
    //      $mensaje = new \StdClass; 
    //      $mensaje->status = 'ERROR';
    //      $mensaje->mensaje = 'No se ha podido guardar. Intentelo nuevamente !!';
    //  }    

    //return json_encode($mensaje);
    }


  }
