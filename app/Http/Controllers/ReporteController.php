<?php

namespace App\Http\Controllers;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Session;
use Response;
use App\State;
use App\Perfil;
use App\Reserva;
use App\Usuario;
use App\Sucursal;
use App\Persona;
use App\Factura;
use App\ProformasDetalle;
use App\AccesosToken;
use App\DtpPlus;
use App\Busqueda;
use App\Agencias;
use Carbon\Carbon;
use App\NotaCredito;
use GuzzleHttp\Client;
use App\FacturaDetalle;
use App\EscalaComision;
use App\BusquedaVuelos;
use App\Divisas;
use App\LiquidacionComision;
use App\TipoPersona;
use App\Grupo;
use App\Recibo;
use App\BancoPlaza;
use App\Cheque;
use App\EstadoFactour;
use App\OrigenAsiento;
use App\GestionPagos;
use App\PagosOnline;
use App\Timbrado;
use App\LiquidacionComisionDetalle;
use App\BancoDetalle;
use App\Cierre;
use App\CierreResumen;
use App\CierreRecibo;
use App\Currency;
use App\CentroCosto; 
use App\PlanCuenta;
use App\FormaCobroCliente;
use App\SucursalEmpresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\CarbonInterval;
use App\Asiento;
use App\AsientoDetalle;
use App\ComercioPersona;
use App\Negocio;
use DataTables;
use App\Producto;
use Illuminate\Support\Facades\Redis;

class ReporteController extends Controller
{
	private function getIdEmpresa()
	{
		return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}

	public function dtPlus(Request $Request){
		return view('pages.reportes.dtPlus');	
	}

	public function getdtPlus(Request $req){
		include("../mes.php");
		/*
			cambiar la url, por mid.dtpmundo.com
			controlar si el estado es distinto a OK y no mostrar las pentañas como estoy y detalle
			poner el separador de miles
		*/
		$client = new Client();
		//$iibRsp = $client->get('http://mid.dtpmundo.com:9292/ControlDTP/resources/estadisticas/promo?idVendedor='.$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario.'&detalle='.$req->input('dataTipo'));
		$iibRsp = $client->get('http://181.40.66.162:9292/ControlDTP/resources/estadisticas/promo?idVendedor=603&detalle='.$req->input('dataTipo'));
		$iibObjRsp = json_decode($iibRsp->getBody());
			$solicitados = DtpPlus::where('user_id', '=',$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario)
									->get();
			$facturas  = "";
			foreach($solicitados as $key=>$dtplus){
				if($key == "0"){
					$facturas .= $dtplus['id_facturas'];
				}else{
					$facturas .= ",".$dtplus['id_facturas'];
				}
			}

			$facturasNumeros = explode(",", $facturas);
			foreach($iibObjRsp->detallePromo as $key=>$detallePromo){
				foreach($facturasNumeros as $voucher){
					if($detallePromo->idFactura == $voucher){
						unset($iibObjRsp->detallePromo[$key]);	

					}
				}
			}	

			$resultado = [];
			$arrayResultado = new \StdClass; 
			$contadorTotal = 0;
			$contadorPendiente = 0;
			$contadorCobrado  =  0;
			$resultadoCobrado = [];
			$resultadoPendiente = [];
			foreach($iibObjRsp->detallePromo as $key=>$detallePromo){
				$contadorTotal++;
				if($detallePromo->estadoFactura == 'P'){
					$resultadoPendiente[] = $detallePromo;
					$contadorPendiente++;
				}else{
					$resultadoCobrado[] = $detallePromo;
					$contadorCobrado++;
				}
			}
			$arrayResultado->estado = $iibObjRsp->estado;
			$arrayResultado->total = $contadorTotal;
			$arrayResultado->cobrado = $contadorCobrado;
			$arrayResultado->pendiente = $contadorPendiente;
			$arrayResultado->resultadoPendiente = $resultadoPendiente;
			$arrayResultado->resultadoCobrado = $resultadoCobrado;
			return json_encode($arrayResultado);
	}

	public function tipoCambioDtPlus(Request $req){
		return view('pages.reportes.cambioDtplus');	
	}	


	public function detalleCanje(Request $req){
		$client = new Client();
		//$iibRsp = $client->get('http://181.40.66.162:9292/ControlDTP/resources/estadisticas/promo?idVendedor='.$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario.'&detalle=C');
		$iibRsp = $client->get('http://181.40.66.162:9292/ControlDTP/resources/estadisticas/promo?idVendedor=603&detalle=C');

		$iibObjRsp = json_decode($iibRsp->getBody());
		$solicitados = DtpPlus::where('user_id', '=',$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario)
								->get();
		$facturas  = "";
		foreach($solicitados as $key=>$dtplus){
			if($key == "0"){
				$facturas .= $dtplus['facturas'];
			}else{
				$facturas .= ",".$dtplus['facturas'];
			}
		}

		$facturasNumeros = explode(",", $facturas);
		foreach($iibObjRsp->detallePromo as $key=>$detallePromo){
			foreach($facturasNumeros as $voucher){
				if($detallePromo->numeroFactura == $voucher){
					unset($iibObjRsp->detallePromo[$key]);	

				}
			}
		}	
		$arrayFacturas = [];
		$objResultado = new \StdClass; 
		$contadorPendiente = 0;
		$contadorMonto  =  0;
		$contador =  0;
		$facturas = [];
		$facturaString = "";
		$facturaIdString = "";
		foreach($iibObjRsp->detallePromo as $key=>$detallePromo){
			if($detallePromo->estadoFactura == 'C'){
				$contadorMonto = $contadorMonto + floatval($detallePromo->incentivo);
				$arrayFacturas[] = $detallePromo->idFactura;
				$facturas[] = $detallePromo->numeroFactura;
				$contadorPendiente++;
				if($contador == 0){
					$facturaString .=$detallePromo->numeroFactura;
					$facturaIdString .= $detallePromo->idFactura;
				}else{
					$facturaString .=",".$detallePromo->numeroFactura;
					$facturaIdString .=",".$detallePromo->idFactura;
				}
				$contador++;
			}
		}
		if($contador != 0){
			$ssl   = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on';
			$proto = strtolower($_SERVER['SERVER_PROTOCOL']);
			$proto = substr($proto, 0, strpos($proto, '/'));
			$usuario = Usuario::with('agencia')
								->where('id_usuario', '=',$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario)
								->get();
			$imagen = '../public/images/promociones/'.$req->input('id').'.png';
			$objResultado->usuario_id = $usuario[0]['id_usuario'];
			$objResultado->nombre_apellido = $usuario[0]['nombre_apellido'];
			$objResultado->agencia = $usuario[0]['agencia']['razon_social'];
			$objResultado->id_facturas = $arrayFacturas;
			$objResultado->facturas = $facturas;
			$objResultado->cantidad = $contadorPendiente;
			$objResultado->monto = floatval($contadorMonto);
			$objResultado->imagen = $imagen;
			$objResultado->imagenNombre = $req->input('id'); 
			$dtpPlus = new DtpPlus;	
			$dtpPlus->codigo = $this->getId4Log();
			$dtpPlus->user_id = $usuario[0]['id_usuario'];
			$dtpPlus->fecha_pedido = date('Y-m-d h:m:s');
			$dtpPlus->opcion_cambio = $req->input('id');
			$dtpPlus->facturas = $facturaString;
			$dtpPlus->id_facturas = $facturaIdString;
			$dtpPlus->tipo = 1;
			$dtpPlus->monto = floatval($contadorMonto);
			$dtpPlus->id_imagen = $req->input('imagen');
			$dtpPlus->save();
			$indice = $dtpPlus->id;
			$objResultado->idVoucher = $indice; 
		    Session::put('objResultado',$objResultado);
		}else{
			$objResultado = Session::get('objResultado');
			$imagen = Session::get('objResultado')->imagen;
			$indice = Session::get('objResultado')->idVoucher;
		}   
		return view('pages.detallepromocion')->with(['resultado'=>$objResultado, 'imagen'=>$imagen, 'indice'=>$indice]);
	}	

	public function getDetalleAttach(Request $req){
		$dtpPlus = DtpPlus::where('id', '=',$req->input('id'))
							->get();		

		return Response::json(['success' => true,'file'=>asset('facturasDTPlus/'.$dtpPlus[0]->id_imagen)]);
	}	

	public function buscarLocalizadores(Request $req){
		return view('pages.mc.reportes.buscarLocalizador');
	}	


	public function getBuscarLocalizadores(Request $request){
        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);
        $jsonRequest = new \StdClass;
        $localizadoresFactour  = [];
		$localizadoresArray = explode(',', $request->input('dataAgencia'));

        foreach($localizadoresArray as $key=>$extracto){
            $localizadoresFactour[]['localizadorFactour'] = $extracto;
        }     
        $jsonRequest->localizadoresFactour = $localizadoresFactour;
        try{
        $iibRsp = $client->post('http://181.40.66.162:9292/ControlDTP/resources/factour/buscarlocalizadores',
                                    ['body' => json_encode($jsonRequest)]
                                );
        }
        catch(RequestException $e){
           		return view('pages.mc.timeErrorConexion');
        }
       catch(ClientException $e){
                return view('pages.mc.errorConexion');  
        }

        $jsonResponse = json_decode($iibRsp->getBody());
		return json_encode($jsonResponse);

    }  

    public function getFecha($mes, $anho)      
  	{
  		$month = $mes; //Reemplazable por número del 1 a 12
		$year = $anho; //Reemplazable por un año valido
		
		switch(date('n',mktime(0, 0, 0, $month, 1, $year)))
		{
			case 1: $Mes = "Enero"; break;
			case 2: $Mes = "Febrero"; break;
			case 3: $Mes = "Marzo"; break;
			case 4: $Mes = "Abril"; break;
			case 5: $Mes = "Mayo"; break;
			case 6: $Mes = "Junio"; break;
			case 7: $Mes = "Julio"; break;
			case 8: $Mes = "Agosto"; break;
			case 9: $Mes = "Septiembre"; break;
			case 10: $Mes = "Octubre"; break;
			case 11: $Mes = "Noviembre"; break;
			case 12: $Mes = "Diciembre"; break;
		}
		
		return $Mes.'/'.date('Y',mktime(0, 0, 0, $month, 1, $year));
	}

	public function getMes($mes)      
  	{		
		switch($mes)
		{
			case "Enero": $Mes = '01'; break;
			case "Febrero": $Mes = '02'; break;
			case "Marzo": $Mes = '03'; break;
			case "Abril": $Mes = '04'; break;
			case "Mayo": $Mes = '05'; break;
			case "Junio": $Mes = '06'; break;
			case "Julio": $Mes = '07'; break;
			case "Agosto": $Mes = '08'; break;
			case "Septiembre": $Mes = '09'; break;
			case "Octubre": $Mes = '10'; break;
			case "Noviembre": $Mes = '11'; break;
			case "Diciembre": $Mes = '12'; break;
		}
		
		return $Mes;
	}

	public function verLiquidaciones(Request $req)
	{
		$periodo = "";
		$indicador = 0;
		$id_vendedor = 0;
		$idliquidacion="";
		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'verLiq'");

	/*	if(empty($permisos)){
	    	flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}*/

		if($req->input('datos')){
			$datos = explode('_', $req->input('datos'));
			$id_vendedor= $datos[0];
			$periodo = $this->getFecha($datos[1],$datos[2]);
			$idliquidacion= $datos[3];
			$indicador = 1;
		}

		if($req->input('periodo')){
			$fecha = explode('/',$req->input('periodo'));
			$periodo = $this->getFecha($fecha[0],$fecha[1]);
		}

		if(empty($permisos)){
			$vendedores = Persona::where('id',  Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->orderBy('nombre','asc')->get();
		}else{
			$vendedores = DB::select("select * 
										from personas where id_empresa = ".$this->getIdEmpresa()." 
										and activo = true 
										and id in (select distinct(id_vendedor) from escala_comision_cabecera)");

		}


		$fecha_liquidacion = DB::select("select * from (
													select  DISTINCT (periodo) as fecha, to_date(periodo,'MMYYYY') as fecha_liq
													from liquidacion_comisiones_vendedores 
													where periodo is not null
												) as result
													order by fecha_liq desc");
			
		$listadoFechas = array();

        foreach ($fecha_liquidacion as $key => $value) {
			$fecha = explode('-', $value->fecha_liq);
        	$mes = $fecha[1];		
        	$anho = $fecha[0];
        	$listadoFechas[] = $this->getFecha($mes,$anho);
        }
		
		return view('pages.mc.liquidaciones.verLiquidaciones', compact('vendedores', 'listadoFechas', 'periodo','id_vendedor','idliquidacion','indicador'));
	}
	
    public function indexLiquidacionComisiones(Request $request)
    {

		if (Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 3)
        {
            $vendedor = Persona::where('id',  Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
        }
        else
        {
        	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2)
        	{
            	$vendedor = Persona::where('id_tipo_persona',  3)
            						->orWhere('id_tipo_persona',  4)
            						->where('activo','=', true)
            						->get();
            	foreach($vendedor as $key=>$sales){
            		if($sales->activo != true){
            			unset($vendedor[$key]);
            		}
            	}
        	}else{
            	$vendedor = Persona::where('id',  Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
        	}

        }


		$max_fecha_liq = DB::select("select 
			max(periodo) fecha
			from liquidacion_comisiones_vendedores
			");

    	/*MOSTRAR LOS MESES NO LIQUIDADOS HASTA LA FECHA ACTUAL*/
    	$flagDate = false;
    		$fecha = $max_fecha_liq[0]->fecha;
        	$mes = substr($fecha, 0,-4);		
        	$anho = substr($fecha, 2);

        	//Obtener ultimo dia del mes de la liquidacion
        	 $fecha_ultima_liquidacion = Carbon::createFromDate($anho,$mes)->endOfMonth();
        	 $endDate = Carbon::createFromDate($anho,$mes)->endOfMonth();
        	//Sumar un mes
        	$endDate = $endDate->addMonths(1);
        	$mes_nuevo =  $endDate->month;


        	
        	$mes_actual = date('m');
        	$diferencia =  $mes_actual - $mes_nuevo ;
        	$listadoFechas = [];

        	$cont_mes = 1;
        	$mes ++;
			for($i=0 ; $i<= $diferencia; $i+=1){
			   $fecha_g = Carbon::createFromDate($anho,$mes)->addMonths($i);
			   $listadoFechas[] = $this->getFecha($fecha_g->month,$fecha_g->year);
			   $cont_mes += 1;
			} 
        	
		  
   
    	return view('pages.mc.reportes.indexLiquidacionComisiones', compact('vendedor', 'listadoFechas'));
  	}  

  	public function consultarLiquidaciones(Request $request)
	{
		$datos = $this->responseFormatDatatable($request);

		$liquidacionesVendedores = [];
		$comisionTotalVendedor = 0;
		
		// if (!empty($datos->periodo)) 
		// {
			$explode = explode("/", $datos->periodo);
		    $mesLetra = $explode[0];
		    $year = $explode[1];
		    $mes = $this->getMes($mesLetra);
		// }

		$idVendedor = $datos->vendedor;

		if ($idVendedor == 1) 
		{
			$liquidacionesVendedores = LiquidacionComision::with('vendedor','liquidacionesDetalle')->where('periodo', $mes.''.$year)->where('activo', true)->get();
			$base = [];
			foreach ($liquidacionesVendedores as $key => $liquidacionVend){
				if($liquidacionVend->vendedor->id_empresa == $this->getIdEmpresa()){
					$pyg = 0;
					$usd = 0;
					$eur = 0;
					foreach($liquidacionVend->liquidacionesDetalle as $liquidacionesDetalle){
						/*echo '<pre>';
						print_r($liquidacionesDetalle);*/
						if($liquidacionesDetalle->id_moneda == 111){
							$pyg = $pyg + (round($liquidacionesDetalle->monto_comision)+round($liquidacionesDetalle->comision_renta_extra) );
						}
						if($liquidacionesDetalle->id_moneda == 143){
							$usd = $usd + ($liquidacionesDetalle->monto_comision+$liquidacionesDetalle->comision_renta_extra);
						}
						if($liquidacionesDetalle->id_moneda == 43){
							$eur = $eur + ($liquidacionesDetalle->monto_comision+$liquidacionesDetalle->comision_renta_extra);
						}
					}
					$liquidacionVend->comision_total_gs =round($pyg, 0,PHP_ROUND_HALF_EVEN);
					$liquidacionVend->comision_total_usd = $usd;
					$liquidacionVend->comision_total_eur = $eur;
					$base[] = $liquidacionVend;
				}
			}			$liquidacionesVendedores = $base;

			foreach ($liquidacionesVendedores as $key => $liquidacionVendedor) 
			{	
				if (empty($liquidacionVendedor['liquidacionesDetalle'])) 
				{
					$comisionTotalVendedor = 0;
					$liquidacionesVendedores[$key]->comisionTotal = number_format($comisionTotalVendedor,2,",",".");
				}
				else
				{
					$comisionTotalVendedor = $liquidacionVendedor['liquidacionesDetalle']->sum('monto_comision');
					$liquidacionesVendedores[$key]->comisionTotal = number_format($comisionTotalVendedor,2,",",".");
				}
			}
			$liquidacionesVendedores = $base;
		}
		else
		{
			$liquidacionesVendedores = LiquidacionComision::with('vendedor','liquidacionesDetalle')->where('id_vendedor', $idVendedor)->where('periodo', $mes.''.$year)->where('activo', true)->get();
			
			if(!empty($liquidacionesVendedores[0]->liquidacionesDetalle))
			{	
				$comisionTotalVendedor = $liquidacionesVendedores[0]->liquidacionesDetalle
				->sum('monto_comision');

				$liquidacionesVendedores[0]->comisionTotal = number_format($comisionTotalVendedor,2,",",".");			
			}
		}
// dd($liquidacionesVendedores);
		return response()->json(['data'=>$liquidacionesVendedores]);
	}


	public function consultarDetalleLiquidaciones(Request $request)
	{
		$datos = $this->responseFormatDatatable($request);
		$liquidacionesVendedores = [];
		$detalleLiquidacionesVendedores = [];
		$comisionTotal = 0;
		$facturas = [];
		$notaCredito = [];
		$rentaTotal = 0;
		$porcentajeComision = 0;
		$comisionGs = 0;
		$comisionUsd = 0;
		$comisionEur = 0;
		/*if(!empty($datos->vendedorDetalle)&&!empty($datos->vendedorDetalle))
		{*/
			if(isset($datos->vendedorDetalle)){
				$idVendedor = $datos->vendedorDetalle;
			}else{
				$idVendedor = $datos->vendedorId;
			}
			$explode = explode("/", $datos->periodoDetalle);
	    	$mesLetra = $explode[0];
	    	$year = $explode[1];
	    	$mes = $this->getMes($mesLetra);
			
			$data = DB::table('vw_liquidacion_vendedores_detalle');
			$data = $data->where('id_vendedor',$idVendedor);
			if($datos->id_liquidacion != ""){
				$data = $data->where('id_liquidacion',$datos->id_liquidacion);
			}
			$data = $data->where('id_empresa', $this->getIdEmpresa());
			$data = $data->where('periodo', $mes.''.$year);
			$data = $data->orderBy('periodo','DESC');
			$data = $data->get();
			
			$pyg = 0;
			$usd = 0;
			$usd_nc = 0;
			$usd_fa = 0;
			$eur = 0;
			$base = '';
			foreach($data as $liquidacionesDetalle){
				/*echo '<pre>';
				print_r($liquidacionesDetalle);*/
				if($liquidacionesDetalle->id_moneda == 111){
					$pyg = $pyg + round($liquidacionesDetalle->monto_comision)+round($liquidacionesDetalle->comision_renta_extra);
				}
				if($liquidacionesDetalle->id_moneda == 143){
					if($liquidacionesDetalle->tipo_documento == 'NC'){
						$usd_nc = $usd_nc + (float)$liquidacionesDetalle->monto_comision;
					}else{
						$usd_fa = $usd_fa + (float)$liquidacionesDetalle->monto_comision + round($liquidacionesDetalle->comision_renta_extra);
						$base .= ', '.(float)$liquidacionesDetalle->monto_comision .' + '.round($liquidacionesDetalle->comision_renta_extra);
					}
				}
				if($liquidacionesDetalle->id_moneda == 43){
					$eur = $eur + $liquidacionesDetalle->monto_comision + $liquidacionesDetalle->comision_renta_extra;
				}
			}			
			$usd = $usd_fa + $usd_nc;
			$totales = DB::select("SELECT sum(f_renta_comisionable) as rentafactura
									FROM vw_liquidacion_vendedores_detalle
									WHERE id_vendedor = ".$idVendedor."
									AND periodo = '".$mes."".$year."'");
			$totalRenta = (float)$totales[0]->rentafactura;
			
			$comisiones = DB::select("SELECT porcentaje_comision, comision_total_gs,comision_total_usd,comision_total_eur,renta_total
									  FROM liquidacion_comisiones_vendedores
									  WHERE id_vendedor = ".$idVendedor."
									  AND periodo = '".$mes."".$year."'
									  ORDER BY id DESC
									  LIMIT 1 ");
			if(isset($comisiones[0])){
				$totalRenta = $comisiones[0]->renta_total;
				$porcentajeComision = $comisiones[0]->porcentaje_comision;
				$comisionGs = "".$pyg."";
				$comisionUsd = "".$usd."";
				$comisionEur = "".$eur."";
			}
		//}
		return response()->json(['data'=>$data, 'data2'=>$totalRenta, 'data3'=>$porcentajeComision, 'data4'=>$comisionGs, 'data5'=>$comisionUsd, 'data6'=>$comisionEur, 'datao'=>$base]);

	}



	public function mostrarLiquidaciones(Request $req)
    {
        $datos = $this->responseFormatDatatable($req);
        $aprobado = 0;
        $comentario = '';
        $rentaCotizado = 0;
        $porcentajeComision = 0;
    	$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
        $id_vendedor = $datos->vendedor;
		$facturasVendedor =[];
		$rentaTotal = 0;

        if(!empty($datos->periodo)) //si no está vacío quiere decir que se buscó
        {
            $explode = explode("/", $datos->periodo);
            $mesLetra = $explode[0];
            $year = $explode[1];
            $mes = $this->getMes($mesLetra);

            $facturasVendedor = DB::select('SELECT * FROM  get_liquidaciones_vendedor(?,?,?) where id_liquidacion = 0', [$mes, $year, $id_vendedor]);

            $rentaTotal =  DB::select('SELECT * FROM  get_renta_total_factura(?,?,?)', [$mes, $year, $id_vendedor]);
            $rentaCotizado = number_format($rentaTotal[0]->get_renta_total_factura,2,",",".");

            $porcentaje =  DB::select('SELECT * FROM  get_porcentaje_comision(?,?)', [$id_vendedor, $rentaTotal[0]->get_renta_total_factura]);
            $porcentajeComision = $porcentaje[0]->get_porcentaje_comision;
            // dd($porcentaje);

            if ($porcentajeComision == 0)
            {
            	$comentario = 'No llegaste a tu meta';
            	$aprobado = 0;
            }
            else
            {
            	$comentario = 'Alcanzaste tu meta: ';
            	$aprobado = 1;
            }

           
        }
  
        return response()->json(['data'=>$facturasVendedor, 'data2'=>$comentario, 'data3'=>$rentaCotizado, 'data4'=>$porcentajeComision, 'data5'=>$aprobado]);
    }


  	private function  responseFormatDatatable($req)
  	{
	    $datos = array();
	    $data =  new \StdClass;
	    
	    foreach ($req->formSearch as $key => $value) 
	    {
	     	$n = $value['name'];
	        $datos[$value['name']] = $value['value'];
	        $data-> $n = $value['value'];
	    }

	    return $data;

 } 



  /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = trim($date[2])."-".trim($date[1])."-".trim($date[0]);
            return $fecha;
        } else {
            return null;
        }
    }


public function reporteVentaSucursal(){

if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 4){
	$sucursales = Persona::where('id_tipo_persona','21')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
}else{
	$sucursales = Persona::where('id_empresa',$this->getIdEmpresa())->where('id',Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia)->where('activo','true')->orderBy('nombre','ASC')->get();
}


if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 4){
	$vendedor = Persona::whereIn('id_tipo_persona',array(3,4))->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
}else{
	$vendedor = Persona::whereIn('id_tipo_persona',array(3,4))->where('id_sucursal_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia)->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
}	

return view('pages.mc.reportes.ventaSucursal')->with(['sucursales'=>$sucursales,'vendedor'=>$vendedor]);
}


	public function ajaxReporteSucursal(Request $req)
	{

		$form = $this->responseFormatDatatable($req);
		$and  = '';

		if($form->idSucursal != ''){
			$base = ' AND f.id_sucursal_empresa ='.$form->idSucursal;
		}else{
			$base = '';
		}
	    $query = " 
	SELECT (to_char(fecha_factura,'DD/MM/YYYY')) as fecha,* FROM (

	SELECT DISTINCT(f.fecha_factura) as fecha_factura ,
	f.nombre,
	f.id_sucursal_empresa,
	f.id_empresa,
	f.cantidad_f,
	f.acumulado_neto as total_neto_factura,
	f.acumulado_venta as total_venta_factura,
	f.acumulado_renta as total_renta_factura,
	p.acumulado_venta as total_venta_proforma,
	p.acumulado_neto as total_neto_proforma,
	p.acumulado_renta as total_renta_proforma
	from vw_totales_fecha_factura_venta_sucursal f
	FULL JOIN ( 
	SELECT *
	from vw_totales_fecha_proforma
	  
	) p ON (p.id_sucursal_empresa = f.id_sucursal_empresa)
	where 
	f.dia_semana = 5 
	OR f.fecha_factura = (( SELECT date_trunc('month'::text, 'now'::date::timestamp with time zone)::date AS date_trunc))
	OR f.fecha_factura::date = CURRENT_DATE
	AND f.id_empresa =".$this->getIdEmpresa()."".$base."
	ORDER BY f.id_sucursal_empresa
		
			) AS result 
	";
		/*
		if($form->periodo != ''){
			$fecha = explode('-', $form->periodo);
			$low =  $this->formatoFechaEntrada($fecha[0]);
			$high = $this->formatoFechaEntrada($fecha[1]);
			$query .= " fecha_factura  BETWEEN '".$low."' AND '".$high."'";
		}

		$query .= $and;*/

	   $dato = DB::select($query);

	   $datos = [];

	   foreach($dato as $key=>$value){
		   	if($value->id_empresa == $this->getIdEmpresa()){
		   		if($form->idSucursal != ''){
		   			if($value->id_sucursal_empresa == $form->idSucursal){
		   				$datos[] = $value;
		   			}
		   		}else{
		   			$datos[] = $value;
		   		}
		   		
		   	}
	   }

	     return response()->json(['data'=>$datos]);
	  
	}


public function ajaxReporteVenta(Request $req){

	$form = $this->responseFormatDatatable($req);
	//$and = '';
	//$query = "SELECT * FROM vw_total_facturacion_mes ";
	$data = DB::table('vw_total_facturacion_venta_sucursal');

	/*if($form->idSucursal != '' || $form->idVendedor != ''){
		$query .= 'WHERE ';
		if($form->idSucursal != '' && $form->idVendedor != ''){
			$and = ' AND ';
		}
	}*/

	$data = $data->where('id_empresa',$this->getIdEmpresa());

	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 4){
		//$query .= 'id_sucursal_empresa = '.$form->idSucursal.' ';
		$data = $data->where('id_sucursal_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia);
	}

	//$query .= $and;

	if($form->idVendedor != ''){
		//$query .= 'id_usuario = '.$form->idVendedor;
		$data = $data->where('id_usuario',$form->idVendedor);
	}
	
    /* if($and == ""){
                $query .= 'AND id_empresa ='.$this->getIdEmpresa();
        }else{
                 $query .= 'AND id_empresa ='.$this->getIdEmpresa();
        }*/

	// dd($query);
    $data = $data->get();

   // dd($datos);

     return response()->json(['data'=>$data]);
  
	}


	public function incentivoReporte(){

		$agencias = Persona::where('id_tipo_persona','8')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
		$vendedor = Persona::where('id_tipo_persona','10')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
		return view('pages.mc.reportes.incentivo')->with(['agencias'=>$agencias,'vendedores'=>$vendedor]);
	}




public function getdatosDTPLus(Request $req){
			$form = $this->responseFormatDatatable($req);
			$detalles = array();
			$idFactour = 0; 
			// $idUsuario = $form->usuarioId;
			$email = $form->email;
			$err = 0;

			if($form->email != ''){ 	
			$correo = strtolower(trim($form->email));	
			$Usuario = DB::select("SELECT email, id FROM personas WHERE LOWER(email) = '".$correo."'");
			
			if(!empty($Usuario)){

				$email = $Usuario[0]->email;
				$idUsuario = $Usuario[0]->id; 
			} else {
				$err++;
			}

			} else {
				$err++;
			}

			if($err == 0){
									
			$usuario = Usuario::where('email', '=',$email)->get(['id_sistema_facturacion']);							
			if($usuario->count() > 0){
				$idFactour = $usuario[0]->id_sistema_facturacion;
			}

			$client = new Client();
			$iibRsp = $client->get('http://159.89.232.102:9292/ControlDTP/resources/dtplus?idVendedor='.$idFactour.'&detalle=C');
			$iibObjRsp = json_decode($iibRsp->getBody());

			$facturasGestur = Factura::with('pasajero','estado')
									 ->where('vendedor_id', '=',$idUsuario)
									// ->where('id_estado_cobro', '=',40) 
									 ->where('usuario_facturacion_id', '<>',4987)
									 ->where('id_estado_factura', '=',29)
									 ->where('id', '>',90000)
									 ->get();

			$contadores = count($iibObjRsp->detallePromo);

			$destalle = [];	
			
			foreach($facturasGestur as $key=>$factGestur){
				// echo "ingreso a foreach";
				$contadores = $contadores+1;	
				$objetoDetalle = new \StdClass;					
			

				$date = explode(' ', $factGestur->fecha_hora_facturacion);
				$date = explode('-', $date[0]);
			    $objetoDetalle->fecha = $date[2]."/".$date[1]."/".$date[0];

	            $objetoDetalle->numeroFactura = $factGestur->nro_factura;
	            $objetoDetalle->pasajero =  $factGestur->pasajero->nombre." ". $factGestur->pasajero->apellido;


	            if(intval($factGestur->id_moneda_venta) == 111){
	            	$objetoDetalle->dtplus = 	round(($factGestur->incentivo_vendedor_agencia/$factGestur->cotizacion_factura),2);
	            }else{
	            	$objetoDetalle->dtplus = $factGestur->incentivo_vendedor_agencia;	
	            }
	            $objetoDetalle->total = $factGestur->total_bruto_factura;

	            if($factGestur->id_estado_cobro == 31){
	            	$objetoDetalle->estadoFactura = 'P';

	            }else{
	            	$objetoDetalle->estadoFactura = 'C';


	            }
	            
	            $objetoDetalle->idFactura = $factGestur->id;

				$objetoDetalle->idProforma = $factGestur->id_pasajero_principal;

	            $destalle[$factGestur->id] = $objetoDetalle;
			}

			if(!empty($destalle)){
			$detalles = array_merge($iibObjRsp->detallePromo,$destalle);
			$iibObjRsp->detallePromo  = $detalles;	
			} 

			// dd($idUsuario,$idFactour);

			$solicitados = DtpPlus::whereIn('user_id',array($idUsuario,$idFactour))
								  ->where('tipo', '=',1)
										->get();
			$facturas = '';

			// echo '<pre>';
			// print_r($solicitados);

			foreach($solicitados as $key=>$dtplus){
				if($key == "0"){
					$facturas .= $dtplus['id_facturas'];
				}else{
					$facturas .= ",".$dtplus['id_facturas'];
				}
			}

			$facturasNumeros = explode(",", $facturas);
			// echo '<pre>';
			// print_r($facturasNumeros);

			foreach($iibObjRsp->detallePromo as $key=>$detallePromo){
				foreach($facturasNumeros as $voucher){
					if($detallePromo->idFactura == $voucher){
						unset($iibObjRsp->detallePromo[$key]);	

					}
				}
			}	

			$resultado = [];
			$arrayResultado = new \StdClass; 
			$contadorTotal = 0;
			$contadorPendiente = 0;
			$contadorCobrado  =  0;
			$resultadoCobrado = [];
			$resultadoPendiente = [];
			$arrayFact = new \StdClass; 

			$facturaArray = array();
			$sumPendiente  = 0;
			$sumCobrado  = 0;
			$contadorPendiente =0;
			$contadorCobrado = 0;

			foreach ($iibObjRsp->detallePromo as $key => $value) {

				if($detallePromo->idFactura != 77927 && $detallePromo->numeroFactura != '001-005-005490'){

					if($value->estadoFactura == 'P'){
						$sumPendiente += $value->dtplus;
						$contadorPendiente++;

					}
						if($value->estadoFactura == 'C'){
						$sumCobrado += $value->dtplus;
						$contadorCobrado++;
					}
					
					if(isset($value->idPasajero)){
						if($value->idPasajero == 4994){


						}
					}	

					//FILTRO DE PENDIENTE Y COBRADO
					if($form->filtroPago != ''){
						// echo "Ingreso filtro";

						if($value->estadoFactura == $form->filtroPago){
							$facturaArray[] =  $value;

						}

						
					
				} else {
						$facturaArray[] =  $value;
				}
			}
		}


			// echo '<pre>';
			// print_r($iibObjRsp);


			// $arrayResultado->estado = $iibObjRsp->estado;
			// $arrayResultado->total = $contadorTotal;
			$arrayResultado->sumCobrado = $sumCobrado;
			$arrayResultado->sumPendiente = $sumPendiente;
			// $arrayResultado->facturas = $facturaArray;


			// echo '<pre>';
			// print_r($facturaArray);





			$arrayResultado->resultadoPendiente = $contadorPendiente;
			$arrayResultado->resultadoCobrado = $contadorCobrado;

			// echo "<pre>";
			// print_r($arrayResultado);
			

			return json_encode(array('data'=>$facturaArray, 'otros'=>$arrayResultado));
			
			} else {
				return json_encode(array('data'=>array(), 'otros'=>array()));
			}
			 
			
	}

             
	public function falloCaja(){


		$vendedor = Persona::whereIn('id_tipo_persona',['3','6','5'])->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();




		return view('pages.mc.reportes.falloCaja')->with(['vendedores'=>$vendedor]);
	}

	public function ajaxFalloCaja(Request $req){
		$idPerfil = Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil;
		$form = $this->responseFormatDatatable($req);

		if($form->vendedor_id != ''){

			$data = DB::table('vw_reporte_falla_caja');

			if($form->periodo != ''){
				 $fecha = explode('-', $form->periodo);
				  $desde = $this->formatoFechaEntrada(trim($fecha[0]));
				  $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
  
				 $data = $data->whereBetween('fecha_hora_facturacion', array(
				date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
				date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
					));
			 }
  
			 if($idPerfil == 3 || $idPerfil == 10){
				$data = $data->where('id_usuario',$form->vendedor_id);
			 }

			$data = $data->where('id_empresa',$this->getIdEmpresa());
			$data = $data->get();
		} else {
			$data = DB::table('vw_reporte_falla_caja');

			if($form->periodo != ''){
				 $fecha = explode('-', $form->periodo);
				  $desde = $this->formatoFechaEntrada(trim($fecha[0]));
				  $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
  
				 $data = $data->whereBetween('fecha_hora_facturacion', array(
				date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
				date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
					));
			 }
  
			$data = $data->where('id_empresa',$this->getIdEmpresa());
			$data = $data->get();

		}

		return json_encode(array('data'=>$data));

	}


	public function extractoAgencias(){
		return view('pages.mc.reportes.envioCorreoAgencia'); 
	}

	public function doEnvioCorreo(Request $req){
		$client = new Client();
		$iibRsp = $client->request('GET', 'http://159.89.232.102:9292/ControlDTP/resources/notificarDtpMundo?que=FacturasVencidas');
		$iibObjRsp = json_decode($categoria->getBody());
		$respuestJason =[];
		$respuestJason['status'] = 'OK';
		return json_encode($respuestJason); 
	}	



public function reporteTicketsPendientes(Request $req){

       $getDivisa = Divisas::where('activo','S')->get();
       $cliente = DB::select("
							SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
							concat(p.nombre,' ',p.apellido,' ','- '|| trim(p.denominacion_comercial)) as full_data
							FROM personas p
							JOIN tipo_persona tp on tp.id = p.id_tipo_persona
							WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
							AND p.activo = true and p.id_empresa = ".$this->getIdEmpresa()." order by p.nombre asc");




 return view('pages.mc.factura.ticketsPendientes')->with(['clientes'=>$cliente,
                                                          'divisas'=>$getDivisa]);
}








	public function ajaxReporteTicketsPendientes(Request $req){

        $ticket_pendientes = DB::table("vw_tickets_pendientes as vw");
        $ticket_pendientes = $ticket_pendientes->select("vw.*", "p.nombre","p.apellido",'cu.currency_code');
        $ticket_pendientes = $ticket_pendientes->leftJoin('personas as p', 'p.id', '=','vw.cliente_id' );
        $ticket_pendientes = $ticket_pendientes->leftJoin('currency as cu', 'cu.currency_id', '=','vw.id_moneda_venta' );
        $ticket_pendientes = $ticket_pendientes->where('vw.id_empresa',$this->getIdEmpresa());

        $form = $this->responseFormatDatatable($req);


            if($form->numFactura != ''){

               $ticket_pendientes = $ticket_pendientes->where('vw.nro_factura',$form->numFactura);
           }
            if($form->numProforma != ''){
               $ticket_pendientes = $ticket_pendientes->where('vw.id_proforma',$form->numProforma);
           } 
           if($form->idCliente != ''){
               $ticket_pendientes = $ticket_pendientes->where('vw.cliente_id',$form->idCliente);
           }
            if($form->idMoneda != ''){
               $ticket_pendientes = $ticket_pendientes->where('vw.id_moneda_venta',$form->idMoneda);
           }
            if($form->facturacion_desde_hasta != ''){
              $fecha =  explode('-', $form->facturacion_desde_hasta);

                $desde     = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta     = $this->formatoFechaEntrada(trim($fecha[1])); 

                $ticket_pendientes = $ticket_pendientes->whereBetween('vw.fecha_hora_facturacion',array($desde,$hasta));

           }

        $ticket_pendientes = $ticket_pendientes->get();

       /* echo '<pre>';
        print_r($ticket_pendientes);*/
        
     	return response()->json(['data'=>$ticket_pendientes]);

}//function


	public function indexPago(Request $req){

		return view('pages.mc.reportes.listadoPago')/*->with(['clientes'=>$getFacturaClientes,
                                                          'divisas'=>$getDivisa])*/;	

	}	

	public function getDatosPagos(Request $req){
			$lista = DB::select('select grupos_anticipo.fecha, sum(grupos_anticipo.monto),grupos_anticipo.moneda_id , currency.currency_code
				from grupos_anticipo, currency
				where currency.currency_id = grupos_anticipo.moneda_id
				and grupos_anticipo.activo = true
				GROUP BY grupos_anticipo.fecha, grupos_anticipo.moneda_id, currency.currency_code
				ORDER BY grupos_anticipo.fecha');
			return response()->json(['data'=>$lista]);
    		
    }


	public function getDatosPagosDetalle(Request $req){
		$datos = $this->responseFormatDatatable($req);

		$listado = DB::select("select 
									grupos_anticipo.id,
									grupos_anticipo.fecha, 
									grupos_anticipo.id_grupo, 
									grupos_anticipo.id_proforma, 
									grupos_anticipo.monto,
									grupos_anticipo.forma_pago_id, 
									grupos_anticipo.moneda_id , 
									currency.currency_code, 
									grupos_anticipo.nro_comprobante, 
									grupos_anticipo.nro_recibo, 
									forma_pago_cliente.denominacion,
							CASE
								  WHEN (grupos_anticipo.id_grupo IS NULL) THEN  (SELECT  personas.nombre FROM proformas, personas WHERE proformas.id = grupos_anticipo.id_proforma AND personas.id = proformas.cliente_id)
								  WHEN (grupos_anticipo.id_proforma IS NULL) THEN (SELECT  personas.nombre FROM grupos, personas WHERE grupos.id = grupos_anticipo.id_grupo AND personas.id = grupos.cliente_id)
								  ELSE ' '
								 END AS cliente
			from grupos_anticipo, currency, forma_pago_cliente
			where currency.currency_id = grupos_anticipo.moneda_id
			and forma_pago_cliente.id = grupos_anticipo.forma_pago_id
			and grupos_anticipo.activo = true
			and grupos_anticipo.moneda_id = ".$datos->moneda." and grupos_anticipo.fecha = date('".$datos->fecha."')");
		foreach($listado as $key=>$lista){
			if($lista->id_grupo != ""){
				$grupo = Grupo::where('id',$lista->id_grupo)->get(['denominacion']);

				if(isset($grupo[0]['denominacion'])){
					$listado[$key]->descgrupo = $grupo[0]['denominacion'];
				}else{
					$listado[$key]->descgrupo = "";
				}
				$listado[$key]->tipo = 'Grupo';
			}else{
				$listado[$key]->descgrupo = $lista->id_proforma;
				$listado[$key]->tipo = 'Proforma';
			}
		}

		return response()->json(['data'=>$listado]);




	}


	public function reporteCobros(Request $req){
    $moneda = '';
    $cliente = DB::select("SELECT  DISTINCT c.id, 
                          c.nombre nombre_c, 
                          c.denominacion_comercial,
						  c.documento_identidad,
						  c.dv 
                          FROM facturas f, personas c 
                          
                          WHERE  c.id = f.cliente_id
                          AND f.id_estado_factura = 29  
                          AND id_tipo_factura = 1
                          AND f.id_empresa = ".$this->getIdEmpresa()."
                          AND f.id_tipo_factura = 1 
                          AND f.id_estado_cobro = 31 
                          AND f.saldo_factura > 0 
                          ORDER BY nombre_c ASC");

    $moneda = Divisas::where('activo','S')->get();

    $sucursalCartera = Persona::where('id_tipo_persona','21')
                               ->where('id_empresa',$this->getIdEmpresa())
                               ->get();

//    $tipoPersona = TipoPersona::where('puede_facturar',true)->get();
	$id_empresa = $this->getIdEmpresa();
    if($id_empresa == 1 || $id_empresa == 4){
      $tipoPersona = TipoPersona::where('puede_facturar',true)->get();
    }else{
      $tipoPersona = TipoPersona::where('puede_facturar',true)->where('ver_agencia','false')->get();
    }

    $facturas = DB::select("SELECT to_char(f.vencimiento,'DD/MM/YYYY') vencimiento, 
                  to_char(f.fecha_hora_facturacion, 'DD/MM/YYYY') fecha_hora_facturacion,
                  f.fecha_hora_facturacion fecha_hora_facturacion_s,
                   to_char(f.check_in,'DD/MM/YYYY') check_in,
                  f.nro_factura,
                  f.id,
                  f.id_estado_cobro,
                  f.saldo_factura,
                  f.pasajero_factour,
                  f.vencimiento - CURRENT_DATE  dias,
                  f.check_in - CURRENT_DATE as diferencia_check_in,
                  CASE 
                        WHEN f.vencimiento > CURRENT_DATE  THEN 'A VENCER'
                        ELSE 'VENCIDO'
                  END as venc_estado,
                   get_monto_factura(f.id) as total_factura,
                  p.nombre pasajero_n,
                  p.apellido pasajero_a,
                  c.nombre cliente_n,
                  c.apellido cliente_a,
                  c.id cliente_id,
                  car.nombre cartera_n,
                  car.denominacion_comercial cartera_dc,
                  cu.currency_code moneda,
                  u.nombre usuario_nombre,
                  u.apellido usuario_apellido,
                  vrc.id_comercio_empresa,
                  ce.descripcion as nombreComercio,
		 (SELECT COUNT(*) 
					    FROM facturas_detalle 
					    WHERE activo = true and id_producto IN(SELECT id  FROM productos WHERE id_grupos_producto = 1) and id_factura = f.id ) as ticketCount  
			                
				  FROM facturas f 
                  LEFT JOIN personas u ON u.id = f.usuario_facturacion_id
                  LEFT JOIN personas c ON c.id = f.cliente_id
                  LEFT JOIN personas p ON p.id = f.id_pasajero_principal
                  LEFT JOIN currency cu ON cu.currency_id = f.id_moneda_venta
                  LEFT JOIN personas car ON car.id = c.id_sucursal_cartera
                  LEFT JOIN ventas_rapidas_cabecera vrc ON f.id_venta_rapida = vrc.id
                  LEFT JOIN comercio_empresa ce ON vrc.id_comercio_empresa = ce.id
                  WHERE f.id_estado_factura = 29 
                  AND f.id_empresa = ".$this->getIdEmpresa()." 
                  AND f.id_estado_cobro = 31 
                  AND f.saldo_factura > 0
				  ORDER BY cliente_n ASC
                  ");

          $comercio_empresa = ComercioPersona::where('id_empresa', $this->getIdEmpresa())->get();
          $usuarios = Persona::whereIn('id_tipo_persona', ['3', '4', '10'])->get(['nombre', 'apellido', 'id']);
      
          return view('pages.mc.reportes.gestionCobros')->with([
                                                          'facturas'=>$facturas,
                                                          'clientes'=>$cliente,
                                                          'sucursalCartera'=>$sucursalCartera,
                                                          'tipoPersona'=>$tipoPersona,
                                                          'monedas'=>$moneda,
                                                          'usuarios'=>$usuarios,
                                                          'comercioEmpresas'=>$comercio_empresa
                                                          ]);
  }//function

  public function consultaFacturasPendientes(Request $req){
  		/*echo '<pre>';
                print_r($req->all());*/

        $query = "SELECT *
				  FROM vw_reporte_cobros
                  WHERE id_empresa = ".$this->getIdEmpresa(); 
                
                if($req->input("estado_cobro") == 1){
                  	$query.= ' AND id_estado_cobro = 31 
                  AND saldo_factura > 0';
                }else{
 					$query.= ' AND  id_estado_cobro = 40 
                  AND saldo_factura = 0';                
                }

                if($req->input("cliente_id") != ''){
                  $query.= ' AND cliente_id = '.$req->input("cliente_id");
                }
                if($req->input("moneda_id") != ''){
                  $query.= ' AND currency_id = '.$req->input("moneda_id");
                }
                if($req->input("estado") != ''){
                   $query.= " AND CASE 
                        WHEN vencimiento > CURRENT_DATE  THEN 'A_VENCER'
                        ELSE 'VENCIDO'
                          END  = '".$req->input("estado")."' ";
                }
                if($req->input("sucursal_id") != ''){
                  $query.= ' AND id_sucursal_cartera = '.$req->input("sucursal_id");
                }

				if($req->input("cliente_especial") != ''){
					$query.= ' AND cliente_especial = '.$req->input("cliente_especial");
				  }

				if($req->input("comercio_id") != ''){
					if($req->input("comercio_id") == 0){
						$query.= ' AND id_comercio_empresa IS NULL ';
					}else{
						$query.= ' AND id_comercio_empresa = '.$req->input("comercio_id");
					}
				  }

              	if($req->input('numFactura')){
              		$query.= " AND nro_factura  LIKE '%".$req->input("numFactura")."%'";	
              	}

                if($req->input("usuario_id") != ''){
                  $query.= ' AND id_usuario = '.$req->input("usuario_id");
                }
                if($req->input("tipo_persona_id") != ''){
                	$condiciones = '';
                	foreach($req->input("tipo_persona_id") as $key=>$tipo){
	               	  if($key == 0){
                			$condiciones .= $tipo;
                		}else{
                			$condiciones .= ','.$tipo;
                		}

                	}

                   $query.= ' AND id_tipo_persona IN ('.$condiciones.')';

                }

                $query.= ' ORDER BY cliente_n ASC';

                $facturas = DB::select($query);

            return response()->json(array('facturas'=>$facturas));      

}


public function grupoAnticipoReporte()
{
	return view('pages.mc.reportes.grupos');
}//
 
 public function reporteAsiento(Request $req)
 {
	//PASAR DATOS PARA ASIENTO CABECERA
	//PASAR DATOS PARA ASIENTO DETALLE
	$id = 0;
	$id_origen = '';
	$nro_doc = '';
	if($req->input('id_detalle_asiento')){
		$nro_doc = $req->input('nro_documento');
		$id = $req->input('id_detalle_asiento');
		$id_origen = $req->input('id_origen_asiento');
	}

	$btn = $this->btnPermisos([],'reporteAsiento');
	$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
	$usuarios = Persona::whereIn('id_tipo_persona',[1,2,3,4,5])->where('id_empresa',$this->getIdEmpresa())->get();
	$plan_cuentas = PlanCuenta::with('currency')->where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();
	
	$origenAsiento = OrigenAsiento::where('activo', true)->get();
	$sucursales = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
	$centro_costos = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();  

	return view('pages.mc.reportes.asientoContable',compact('usuarios', 'id', 'plan_cuentas', 
	'sucursales', 'centro_costos', 'btn', 'origenAsiento','currency','id_origen','nro_doc'));
}

public function getCotizacionContable(Request $req)
{
	if($req->idDetalleAsiento == ""){

		if($req->idMonedaCuenta == 111)
		{
			$cotizacion = 1;
			$etiqueta = "PYG";
		}
		else
		{
			$cotizacion = DB::select('SELECT get_cotizacion_contable_actual(?,?)', [$this->getIdEmpresa(), $req->tipoCotizacionCuenta]);
			$cotizacion = $cotizacion[0]->get_cotizacion_contable_actual;

			if($req->idMonedaCuenta == 143){
				$etiqueta = "USD";
			}else{
				$etiqueta = "EUR";
			}
		}
	}else{
		$idDetalleAsiento = str_replace("row","", $req->idDetalleAsiento);	 
		$asiento = AsientoDetalle::where('id', (int)$idDetalleAsiento)->first(['cotizacion']);
		$cotizacion = $asiento->cotizacion;
		$etiqueta = "";
	}	
	
	return response()->json(['cotizacionContable'=>$cotizacion, 'etiquetaContable'=>$etiqueta]);
	
}

public function getImporteCotizado(Request $req)
{
	$montoCotizado = DB::select('SELECT get_monto_cotizado_asiento(?, ?, ?, ?)', [$req->importeAsiento, $req->idMonedaCuenta, 111,  $req->cotizacionContable]);
	$montoCotizado = $montoCotizado[0]->get_monto_cotizado_asiento;

	return response()->json(['montoCotizado'=>$montoCotizado]);
}

public function actualizarAsiento(Request $req)
{
	$resp = new \StdClass; 
	$resp->err = true;

	//try{
		DB::beginTransaction();

	$tipo_operacion = '';
	
	$id_origen_asiento = Asiento::where('id', $req->id_asiento)->get(['id_origen_asiento']);
	$id_origen_asiento = $id_origen_asiento[0]->id_origen_asiento;
	$next_sec = DB::select("select nextval('sec_numero_grupo')");
	$next_sec = $next_sec[0]->nextval;


	// dd($next_sec[0]->nextval);

	//SE ACTUALIZA LA SECUENCIA DE GRUPO DEL ASIENTO CABECERA
	DB::table('asientos_contables')->where('id', $req->id_asiento)
	->update([
		'numero_grupo_cabecera'=> $next_sec,
		'updated_at' => date('Y-m-d H:i:s')
	]);
		
	//SE DESACTIVAN TODOS LOS DETALLES DEL ASIENTO
	DB::table('asientos_contables_detalle')
		->where('id_asiento_contable', $req->id_asiento)
		->update([
					'id_usuario_anulacion'=> Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
					'fecha_hora_anulacion'=>date('Y-m-d H:i:s'),
					'activo'=>false,
					'updated_at' => date('Y-m-d H:i:s')
		]);

	//SE INSERTAN DE NUEVO (SÓLO EN ASIENTO_DETALLE)
	foreach ($req->asiento_detalle as $key => $asientoDetalle) 
	{
		/*echo '<pre>';
		print_r($asientoDetalle);*/

		if($asientoDetalle['debe'] == 0)
		{
			$tipo_operacion = 'H';
		}
		else
		{
			$tipo_operacion = 'D';
		}

		$cuentaAsiento = $asientoDetalle['cuenta'];
		if ($cuentaAsiento === 'null')
		{
			$cuenta = 0;
		}
		else{
			$cuenta = $asientoDetalle['cuenta'];
		}
		DB::select('select * from insertar_asientos(?,?,?,?,?,?,?,?,?,?,?,?,?)', 
		[
			$cuenta,
			$asientoDetalle['importe_original'],
			$tipo_operacion,
			Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
			$id_origen_asiento,
			$asientoDetalle['moneda'],
			str_replace(',','.', str_replace('.','',$asientoDetalle['cotizacion'])),
			$req->id_asiento,
			$asientoDetalle['concepto'],
			$asientoDetalle['sucursal'],
			$asientoDetalle['centro_costo'],
			$this->getIdEmpresa(),
			$req->input('documento_asiento')
		]);
	}


	DB::commit();
  /*} catch(\Exception $e){

	$resp->err = false;         
	if(env('APP_DEBUG')){
		$resp->err = $e;
	  }*/
/*	DB::rollBack();
  }*/
	return response()->json($resp);
}


public function ajaxReporteAsiento(Request $req)
{
	// dd($req->input('asiento_anulado'));
	$asientos = DB::table('v_asientos_contables');
	$asientos = $asientos->where('id_empresa', $this->getIdEmpresa());

	if($req->input('asiento_anulado') != ''){
		$asientos = $asientos->where('activo', $req->input('asiento_anulado') == 1 ? false : true);
	}


	if($req->input('id_usuario')){
		$asientos = $asientos->where('id_usuario',$req->input('id_usuario'));
   }
   	//LAS FECHAS YA VIENEN FORMATEADAS DESDE LA VISTA
	if($req->input('periodo')){
		$fecha =  explode(' ', $req->input('periodo'));
		$desde  = $fecha[0].' 00:00:01';
		$hasta  = $fecha[1].' 23:59:59'; 

		$asientos = $asientos->whereBetween('fecha_hora', array($desde,$hasta));
   }

   if($req->input('id_asiento')){
		$asientos = $asientos->where('id_asiento', $req->input('id_asiento'));
   }

   if($req->input('tipo_operacion')){
		$asientos = $asientos->where('id_origen_asiento', $req->input('tipo_operacion'));
   }

   /*if($req->input('documento')){
	$asientos = $asientos->where('nro_documento', $req->input('documento'));
   }*/
   if ($req->input('documento')) {
	$documento = $req->input('documento');
	$asientos = $asientos->where('nro_documento', 'like', "%$documento%");
}


   if($req->input('balanceado')){
	$asientos = $asientos->where('balanceado', $req->input('balanceado'));
   }

   $asientos = $asientos->get();
   // dd($asientos);

	return response()->json(['data'=>$asientos]);
}
	public function ajaxGrupoAnticipo(Request $req) {

		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$tipo_persona = Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil;
		$data = DB::table("vw_anticipo_grupo");

		if( $tipo_persona == 3){
			$data = $data->where('id_usuario',$idUsuario);
		}
		$data = $data->get();

		return response()->json(['data'=>$data]);
	}//

public function ajaxReporteAsientoDetalle(Request $req)
{
	$asientos = DB::table("asientos_contables_detalle as acd");
	$asientos = $asientos->select("acd.*", 
								 DB::raw("CONCAT(lc.nro_documento,lv.nro_documento) as nro_factura"),
								 "ac.concepto",
								"pc.descripcion as cuenta_n",
								"pc.cod_txt as num_cuenta",
								"s.denominacion as sucursal",
								"acd.id_sucursal",
								"acd.id_centro_costo",
								 DB::raw("CONCAT(cc.nombre, ' ') AS centro_costo"));
	$asientos = $asientos->leftJoin('plan_cuentas as pc', 'pc.id', '=','acd.id_cuenta_contable' );
	$asientos = $asientos->leftJoin('asientos_contables as ac', 'ac.id', '=','acd.id_asiento_contable' );
	$asientos = $asientos->leftJoin('libros_compras as lc', 'lc.id_asiento', '=','ac.id' );
	$asientos = $asientos->leftJoin('libros_ventas as lv', 'lv.id_asiento', '=','ac.id' );
	$asientos = $asientos->leftJoin('sucursales_empresa as s', 's.id', '=','acd.id_sucursal' );
	// $asientos = $asientos->leftJoin('sucursales_empresa as se', 'se.id_empresa', '=','ac.id_empresa');
	$asientos = $asientos->leftJoin('centro_costo as cc', 'cc.id', '=','acd.id_centro_costo' );
	$asientos = $asientos->where('acd.activo', true);
	// $asientos = $asientos->where('ac.id_empresa', true);
	
	$asientos = $asientos->orderBy('acd.debe','desc');
	
	if($req->input('id_asiento')){
		$asientos = $asientos->where('acd.id_asiento_contable',$req->input('id_asiento'));
   }

	$asientos = $asientos->get();

	/*echo '<pre>';
    print_r($asientos);*/

	$asiento = DB::select("SELECT to_char(fecha_hora, 'DD/MM/YYYY') AS fecha, nro_documento, concepto, id FROM asientos_contables WHERE id =  ?", [$req->input('id_asiento')]);
	
	/*echo '<pre>';
    print_r($asiento);*/

	return response()->json(['data'=>$asientos, 'data1'=>$asiento[0]]);
}


public function reporteRetenciones(Request $req){


	return view('pages.mc.reportes.retenciones');
}


	
	public function getDetallesGrupoAnticipo(Request $req){

		$grupoDetalle = DB::table('vw_anticipo_grupo_detalle');

		if($req->input('id_proforma')){
			$grupoDetalle = $grupoDetalle->where('id_proforma',$req->input('id_proforma'));
		}
		if($req->input('id_proforma_factura')){
			$grupoDetalle = $grupoDetalle->where('id_proforma',$req->input('id_proforma_factura'));
		}
		if($req->input('id_grupo')){
			$grupoDetalle = $grupoDetalle->where('id_grupo',$req->input('id_grupo'));
		}

		$grupoDetalle = $grupoDetalle->get();



		foreach($grupoDetalle as $key=>$lista){
			if($lista->id_grupo != ""){
				$grupo = Grupo::where('id',$lista->id_grupo)->get(['denominacion']);

				if(isset($grupo[0]['denominacion'])){
					$grupoDetalle[$key]->descgrupo = $grupo[0]['denominacion'];
				}else{
					$grupoDetalle[$key]->descgrupo = "";
				}
				$grupoDetalle[$key]->tipo = 'Grupo';
			}else{
				$grupoDetalle[$key]->descgrupo = $lista->id_proforma;
				$grupoDetalle[$key]->tipo = 'Proforma';
			}
		}

		return response()->json(['data'=>$grupoDetalle]);
	}//


	public function pagosAgencia(Request $req){
    	$estadoPago = EstadoFactour::where('id_tipo_estado','10')
                               ->get();
        $agencias = Persona::where('id_tipo_persona', 8)->get(['id', 'nombre']);

        $sucursal = Persona::where('id_tipo_persona', 21)->get(['id', 'nombre']);

        $monedas = Divisas::where('activo','S')->get(['currency_id', 'currency_code']);
		return view('pages.mc.reportes.pagosAgencia')->with(['estadoPago'=>$estadoPago, 'agencias'=>$agencias, 'monedas'=>$monedas, 'sucursales'=>$sucursal]);
	}

	public function consultaPagosServerSide(Request $req){


   	 $query = "SELECT gestion_pagos.id, gestion_pagos.fecha,gestion_pagos.numero_recibo, gestion_pagos.imagen_recibo, gestion_pagos.imagenDocumento,  gestion_pagos.nro_comprobante,agencia.nombre,gestion_pagos.estado_id, gestion_pagos.comentario,gestion_pagos.importe,usuario.nombre as nombreUsuario,usuario.apellido as apellidoUsuario, currency.currency_code, currency.currency_id, gestion_pagos.mensaje  from gestion_pagos, personas as agencia , personas as usuario, currency where gestion_pagos.id_agencia = agencia.id and gestion_pagos.usuario_id = usuario.id and gestion_pagos.currency_id = currency.currency_id ";
   	 if($req->input('agencia_id') != ""){
   	 	$query .= " and gestion_pagos.id_agencia = ". $req->input('agencia_id');
   	 }

   	 if($req->input('id_moneda') != ""){
   	 	$query .= " and currency.currency_id = ". $req->input('id_moneda');
   	 }

   	 if($req->input('estado_id') != ""){
   	 	$query .= " and gestion_pagos.estado_id = ". $req->input('estado_id');
   	 }

	 $pagoOffline = DB::select($query);
 
	 $querys = "SELECT pagos_online.id, pagos_online.moneda_id, pagos_online.numero_recibo, pagos_online.fecha , pagos_online.imagen_recibo ,pagos_online.autorizacion, agencia.nombre, pagos_online.estado_id, pagos_online.comentario , usuario.nombre as usuarionombre,usuario.apellido as usuarioapellido,pagos_online.monto_gs, pagos_online.mensaje from pagos_online, personas as agencia, personas as usuario where pagos_online.persona_id = agencia.id and pagos_online.usuario_id = usuario.id ";

   	 if($req->input('agencia_id') != ""){
	     $querys .= " and pagos_online.persona_id = ".$req->input('agencia_id');
	 }    

	 if($req->input('id_moneda') != ""){
	     $querys .= " and pagos_online.moneda_id = ". $req->input('id_moneda');
	 }   

     if($req->input('estado_id') != ""){
    	$querys .= " and pagos_online.estado_id = ". $req->input('estado_id');
     }	

	 $pagoOnline = DB::select($querys);

		/*echo '<pre>';
		print_r(json_encode($pagoOffline));*/
		/*echo '<pre>';
		print_r($querys);*/

		$key = 0;
		$resultado = [];
		foreach($pagoOffline as $key=>$pagoOff){
			$resultado[$key]['id'] = $pagoOff->id;
			$resultado[$key]['fecha'] = date('d/m/Y', strtotime($pagoOff->fecha));
			$resultado[$key]['nro_comprobante'] = $pagoOff->nro_comprobante;
			$resultado[$key]['agencia'] = $pagoOff->nombre;
			if($pagoOff->estado_id == 31){
				$resultado[$key]['estado'] = 'Pendiente';
			}else{
				$resultado[$key]['estado'] = 'Cobrado';
			}
			$resultado[$key]['estado_id'] = $pagoOff->estado_id;
			$resultado[$key]['recibo'] = $pagoOff->numero_recibo;
			$resultado[$key]['comentario'] = $pagoOff->mensaje;
			$resultado[$key]['usuario'] = $pagoOff->nombreusuario. " ".$pagoOff->apellidousuario ;
			$resultado[$key]['monto'] = $pagoOff->importe;
			$resultado[$key]['moneda'] = $pagoOff->currency_code;
			$resultado[$key]['tipo'] = 'Transferencia';
			$resultado[$key]['imagen'] = $pagoOff->imagendocumento;
			$resultado[$key]['imagenRecibo'] = $pagoOff->imagen_recibo;
		}

		$indicador = $key+1;
		foreach($pagoOnline as $key=>$pagoOn){
			$resultado[$indicador]['id'] = $pagoOn->id;
			$resultado[$indicador]['fecha'] =  date('d/m/Y', strtotime($pagoOn->fecha));
			$resultado[$indicador]['nro_comprobante'] = $pagoOn->autorizacion;
			$resultado[$indicador]['agencia'] = $pagoOn->nombre;
			if($pagoOn->estado_id == 31){
				$resultado[$indicador]['estado'] = 'Pendiente';
			}else{
				$resultado[$indicador]['estado'] = 'Cobrado';
			}
			$resultado[$indicador]['estado_id'] = $pagoOn->estado_id;
			$resultado[$indicador]['recibo'] = $pagoOn->numero_recibo;
			$resultado[$indicador]['comentario'] = $pagoOn->mensaje;
			$resultado[$indicador]['usuario'] = $pagoOn->usuarionombre. " ".$pagoOn->usuarioapellido;
			$resultado[$indicador]['monto'] = $pagoOn->monto_gs;
			$resultado[$indicador]['moneda'] = 'GS';
			$resultado[$indicador]['tipo'] = 'Pago con Tarjeta';
			$resultado[$indicador]['imagen'] = '';
			$resultado[$indicador]['imagenRecibo'] = $pagoOn->imagen_recibo;
			$indicador++;
		}

		return response()->json($resultado);
	}

	public function getConfirmarPago(Request $req){
 		$timbrado = Timbrado::where('activo', true)
 							->where('id_tipo_timbrado', 1)
 							->where('id_empresa', $this->getIdEmpresa())
 							->where('id_sucursal_empresa',$req->input('sucursal_id'))
 		                    ->get(['establecimiento', 'expedicion']);		

 		$numero = $req->input('numero');  
 		$cantNumero = strlen($numero);

 		$diferencia = 7 - $cantNumero;
 		$ceros = '';
 		for($x=0; $x<$diferencia; $x++){
			$ceros .= '0'; 
 		}
		$numero_recibo = $timbrado[0]['establecimiento'].'-'.$timbrado[0]['expedicion'].'-'.$ceros.''.$req->input('numero');

		if($req->input('tipo') == 'Pago con Tarjeta'){
			$proforma = DB::table('pagos_online')
						    ->where('id',$req->input('id'))
							->update([
										'estado_id'=>'40',
										'id_usuario_dtp'=> Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
										'fecha_cobro'=>date('Y-m-d h:m:i'),
										'numero_recibo'=>$numero_recibo,
										'imagen_recibo'=> $req->input('imagen')
									]);

		}else{
			$proforma = DB::table('gestion_pagos')
						    ->where('id',$req->input('id'))
							->update([
										'estado_id'=>'40',
										'id_usuario_dtp'=> Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
										'fecha_cobro'=>date('Y-m-d h:m:i'),
										'numero_recibo'=>$numero_recibo,
										'imagen_recibo'=> $req->input('imagen')
									]);


		}
		$objResultado = new \StdClass; 
		$objResultado->codigo = 'OK'; 
		$objResultado->mensaje = 'Se ha registrado el pago';
		return response()->json($objResultado);
	}

	public function comentarioPago(Request $req){
		if($req->input('tipo') == 'Pago con Tarjeta'){	
 			$pagoComentario = PagosOnline::where('id', $req->input('idPago'))->get(['id', 'mensaje']);		
 		}else{
 			$pagoComentario = GestionPagos::where('id', $req->input('idPago'))->get(['id', 'mensaje']);		
		}	

		if(!empty($pagoComentario)){
			$mensaje = new \StdClass; 
			$mensaje->status = 'OK';
			$mensaje->id = $pagoComentario[0]->id;
			$mensaje->mensaje = $pagoComentario[0]->mensaje;
			$mensaje->tipo = $req->input('idPago');
		}else{
			$mensaje = new \StdClass; 
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No existe comentario';
		}	

		return response()->json($mensaje);
	}

	public function agregarComentarioPago(Request $req){
		if($req->input('tipo') == 'Pago con Tarjeta'){
			$proforma = DB::table('pagos_online')
						    ->where('id',$req->input('id'))
							->update([
										'comentario'=> $req->input('comentario')
									]);
		}else{
			$proforma = DB::table('gestion_pagos')
						    ->where('id',$req->input('id'))
							->update([
										'comentario'=> $req->input('comentario')
									]);


		}
		$objResultado = new \StdClass; 
		$objResultado->codigo = 'OK'; 
		$objResultado->mensaje = 'Se ha registrado el comentario';
		return response()->json($objResultado); 



	}

	public function getAdjuntosVoucher(Request $req){
        $mensaje = []; 
		if($req->input('dataTipo') == 'Pago con Tarjeta'){	
 			$pagoComentario = PagosOnline::where('id', $req->input('dataProforma'))->get(['id', 'imagen_documentos']);	
 			if(isset($pagoComentario[0]['imagen_documentos'])){	
				$mensaje = []; 	
				$mensaje[0]['imagen_documentos'] = $pagoComentario[0]['imagen_documentos']; 
			}
 		}else{
 			$pagoComentario = GestionPagos::where('id', $req->input('dataProforma'))->get(['id', 'imagenDocumento']); 
 			if(isset($pagoComentario[0]['imagenDocumento'])){	
				$mensaje = []; 	
				$mensaje[0]['imagen_documentos'] = $pagoComentario[0]['imagenDocumento'];
			}
		}	
			
		return response()->json($mensaje);
	}

	public function cierre(Request $req){

		$query = "SELECT DISTINCT concat(personas.nombre || ' ' || personas.apellido) as nombre_apellido, personas.id, cierre_caja.id_empresa
				  FROM cierre_caja, personas 
				  WHERE cierre_caja.usuario_cierre = personas.id
				  AND cierre_caja.id_empresa =".$this->getIdEmpresa();
		$usuarios = DB::select($query);		
		/*echo '<pre>';
				print_r($usuarios);	*/							
		return view('pages.mc.reportes.cierre')->with(['usuarios'=>$usuarios]);
	}

	public function getDatoscierre(Request $req){
		$cierreRecibos = new CierreRecibo;
		$cierreRecibos = $cierreRecibos->with([
												'recibo',
									       		'recibo.formaCobroCabecera.formaCobroReciboDetalles'=> function($query) use($req){
										        	if($req->input('idCobro')){
										        		$query->where('id_tipo_pago','=', $req->input('idCobro'));	
										        	}
													if($req->input('moneda')){
										        		$query->where('id_moneda','=', $req->input('moneda'));	
										        	}									       		 
										        },
										        'recibo.formaCobroCabecera.formaCobroReciboDetalles.moneda',
									       	  ]);

												 if($req->input('fecha_cierre')) {
													$fecha_cierre =$this->formatoFechaEntrada($req->input('fecha_cierre'));
													$ids = Cierre::where('fecha_cierre', $fecha_cierre)->where('id_empresa', $this->getIdEmpresa())->pluck('id');
													$cierreRecibos = $cierreRecibos->whereIn('id_cierre', $ids);
													} else {
												  $cierreRecibos = $cierreRecibos->where('id_cierre',  $req->input('id_cierre'));
												  }
												   $cierreRecibos = $cierreRecibos->get();
 	   /* echo '<pre>';
		print_r($cierreRecibos); */
		//die;
		$listado = [];
		$totalUs = 0;
		$totalGs = 0;
		$data = [];
		if(isset($cierreRecibos)){ 
			foreach($cierreRecibos as $key=>$cierreRecibo){
				if(isset($cierreRecibo->recibo->id)){
					foreach($cierreRecibo->recibo->formaCobroDetalle as $key2=>$formaCobroDetalle){
						if(isset($formaCobroDetalle->id_tipo_pago)){
							if($formaCobroDetalle->id_tipo_pago == $req->input('idCobro')){
								if($formaCobroDetalle->id_moneda == $req->input('moneda')){
										
										$data[$cierreRecibo->recibo->id]['id'] = $cierreRecibo->recibo->id;
										$data[$cierreRecibo->recibo->id]['cliente'] = '<b>'.$cierreRecibo->recibo->cliente->nombre.'</b>';
										$data[$cierreRecibo->recibo->id]['nro_recibo'] = '<a href="'.route('controlRecibo', ['id' => $cierreRecibo->recibo->id]).'"  class="bgRed"><i class="fa fa-fw fa-search"></i>'.$cierreRecibo->recibo->nro_recibo.'</a>'; 
										$data[$cierreRecibo->recibo->id]['total_pago'] = '<b>'.number_format($cierreRecibo->recibo->total_pago,2,",",".").'</b>';
										$data[$cierreRecibo->recibo->id]['moneda'] = $cierreRecibo->recibo->moneda->currency_code;
										$data[$cierreRecibo->recibo->id]['concepto'] = $cierreRecibo->recibo->concepto;
										$data[$cierreRecibo->recibo->id]['cotizacion'] = $cierreRecibo->recibo->cotizacion;

										$data[$cierreRecibo->recibo->id]['detalles'][$key2]['forma_cobro_id'] = $formaCobroDetalle->forma_cobro->id;
										$data[$cierreRecibo->recibo->id]['detalles'][$key2]['forma_cobro'] = $formaCobroDetalle->forma_cobro->denominacion;
										$data[$cierreRecibo->recibo->id]['detalles'][$key2]['forma_cobro_abreviatura'] = $formaCobroDetalle->forma_cobro->abreviatura;
										$cheque ='';
										if($formaCobroDetalle->id_cheque != ''){
											$cheques = Cheque::where('id', $formaCobroDetalle->id_cheque)->get();
											if(isset($cheques[0]->cuenta)){
												$cheque = $cheques[0]->cuenta;
											}
										}

										$data[$cierreRecibo->recibo->id]['detalles'][$key2]['cheque'] = $cheque;

										$bancoPlaza ='';
										if($formaCobroDetalle->id_banco_plaza != 0){
											$bancosPlaza = BancoPlaza::where('id', $formaCobroDetalle->id_banco_plaza)->get();
											if(isset($bancosPlaza[0]->nombre)){
												$bancoPlaza = $bancosPlaza[0]->nombre;
											}
										}
										$data[$cierreRecibo->recibo->id]['detalles'][$key2]['banco_plaza'] = $bancoPlaza;
										
										$banco ='';
										$nomBanck = '';
										if($formaCobroDetalle->id_banco_detalle != 0){
											$bancos = BancoDetalle::with('banco_cab')
																	->where('id', $formaCobroDetalle->id_banco_detalle)->get();
											if(isset($bancos[0]->numero_cuenta)){
												$banco =  $bancos[0]->numero_cuenta;
											}

											if(isset($bancos[0]->numero_cuenta)){
												$nomBanck = $bancos[0]->banco_cab->nombre;
											}	
										}
										$bancos = $nomBanck." ".$banco;
										$data[$cierreRecibo->recibo->id]['detalles'][$key2]['banco'] = $bancos;
										$data[$cierreRecibo->recibo->id]['detalles'][$key2]['nro_comprobante'] = $formaCobroDetalle->nro_comprobante;
										$data[$cierreRecibo->recibo->id]['detalles'][$key2]['importe_pago'] =$formaCobroDetalle->importe_pago;
										$data[$cierreRecibo->recibo->id]['detalles'][$key2]['id_moneda'] = $formaCobroDetalle->id_moneda;
								}		
							}
						}	
					}
				}	
			}
		}	

		foreach($data as $key=>$st){ 
			if(isset($st['detalles'])){
				$listado['data'][] = $st;	
			}	
		}
		return response()->json($listado);
	}

	public function getCierre(Request $request){

		 $cierres = Cierre::with('usuario','usuarioAnulacion')->where(function($query) use($request) 
	      {
	        if(!empty($request->input('periodo')))
	        {
		        $fechaTrim = trim($request->input('periodo'));
		        $periodo = explode('-', trim($fechaTrim));
		        $desde = explode("/", trim($periodo[0]));
		        $hasta = explode("/", trim($periodo[1]));
		        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0];
		        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0];
		        $query->whereBetween('fecha_cierre', [$fecha_desde, $fecha_hasta]);
	        }
  			if(!empty($request->input('activo')))
	        {	
	        	$query->where('activo',$request->input('activo'));          
	        }
	        if(!empty($request->input('usuario')))
	        {	
	        	$query->where('usuario_cierre',$request->input('usuario'));          
	        }	
	        $query->where('id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
	      })->get();

		 return response()->json($cierres);
	}

	public function getResumenCierre(Request $request){
		$cierreResumen = DB::select("SELECT 
		fcc.id,
		fcc.denominacion,
		cu.currency_code,
		cr.monto,
		cr.forma_cobro_id,
		cr.id_cierre,
		cr.moneda_id,
		fcc.tipo
		from cierre_resumen cr
		JOIN currency cu ON cu.currency_id = cr.moneda_id
		JOIN forma_cobro_cliente fcc ON fcc.id = cr.forma_cobro_id
		WHERE cr.id_cierre = ?
		ORDER BY fcc.tipo = 'VALORES' DESC, fcc.denominacion DESC, cu.currency_code DESC  ",[ $request->input('id')]);

		return response()->json($cierreResumen);
	}	


	public function generarCierre(Request $req){
		return view('pages.mc.reportes.generarCierre');
	}

	public function getResumenCierreGenerar(Request $request){
		// dd($request->all());
		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$data = DB::table('vw_resumen_cierre_caja');
	 	/*$data = $data->whereBetween('fecha', array(
              $request->input('fecha_creacion'),
              $request->input('fecha_creacion')
                  ));*/
        $data = $data->where('id_usuario_aplicado', $idUsuario);
        $data = $data->where('id_cierre', null);
		$data = $data->get();
		// dd($data);
		return response()->json($data);
	}	

	public function getDatosCierreGenerar(Request $request){

		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	//	$fecha = date('Y-m-d', strtotime($form[0]['value']));
		$data = DB::table('vw_cierre_recibos');
	  	/*$data = $data->whereBetween('fecha_creacion_recibo', array(
	             $form[0]['value'],
	             $form[0]['value']
                  )); */
        $data = $data->where('id_usuario_aplicado', $idUsuario);
        $data = $data->where('id_cierre', null);
        $data = $data->where('activo', true);

		$data = $data->get();
		$anterior = 0;
		$contador = 0;
		$indice = 0;
		$resultado = [];
		$listado = [];
		/*echo '<pre>';
		print_r($data);*/
		foreach($data as $key=>$valor){
			if($valor->activo == true){
				/*echo '<pre>';
				print_r($valor);*/
				$resultado[$valor->id]['fecha'] = '<b>'.$valor->fecha_creacion_recibo.'</b>';
				$resultado[$valor->id]['nro_recibo'] = '<a href="../controlRecibo/'.$valor->id.'"  class="bgRed"><i class="fa fa-fw fa-search"></i>'.$valor->nro_recibo.'</a>';
				$resultado[$valor->id]['cliente'] = $valor->cliente;
				$resultado[$valor->id]['concepto'] = '<b>'.$valor->concepto_recibo.'</b>';
				$resultado[$valor->id]['estado'] = $valor->estado_recibo;
				$resultado[$valor->id]['moneda'] = $valor->currency_code;
				$resultado[$valor->id]['totalRecibo'] = '<b>'.number_format($valor->total_recibo_detalle,2,",",".").'</b>';
				$resultado[$valor->id]['totalGs'] = 'GS';
				$resultado[$valor->id]['totalUs'] = "US";
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['forma_cobro'] = '<b>'.$valor->forma_cobro.'</b>';
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['moneda_cobro'] = '<b>'.$valor->currency_code.'</b>';
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['id_tipo_pago'] = $valor->id_tipo_pago;
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['importe_cobro'] = $valor->importe_pago; 
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['banco_plaza'] = $valor->banco_plaza; 
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['numero'] = $valor->numero; 
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['nro_comprobante'] = '<b>'.$valor->nro_comprobante.'</b>'; 
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['id_deposito_bancario'] = $valor->id_deposito_bancario; 
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['numero_cuenta'] = $valor->numero_cuenta; 
				$resultado[$valor->id]['detalles'][$valor->iddetalle]['banco'] = $valor->banco; 
			}	
		}
		
		foreach($resultado as $key=>$st){

			$listado['data'][] = $st;	
		}

		if(empty($listado)){
			$listado['data'][] = $listado;
		}
		return response()->json($listado);
	}	

	public function getCerrarCaja(Request $request)
	{
		
		$getCerrarCaja=DB::select('SELECT public."cerrar_caja"('.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.','.Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.')');

		$cerrarCaja['respuesta'] = $getCerrarCaja[0]->cerrar_caja;

		return response()->json($cerrarCaja);
	}	

	public function anularCierreCaja(Request $request){

		$getAnularCierreCaja=DB::select('SELECT public."anular_cierre_caja"('.$request->input('id').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.','.Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.')');

		$getAnularCierreCaja['respuesta'] = 'OK';

		return response()->json($getAnularCierreCaja);
	}	



	private function btnPermisos($parametros, $vista)
	{

				$btn = DB::select("SELECT pe.* 
									FROM persona_permiso_especiales p, permisos_especiales pe
									WHERE p.id_permiso = pe.id 
									AND p.id_persona = ".$this->getIdUsuario() ." 
									AND pe.url = '".$vista."' ");
		
				$htmlBtn = '';
				$boton =  array();
				$idParametro = '';
		
				// dd( $btn );
		
				if(!empty($btn)){
		
				foreach ($btn as $key => $value) {
		
				$idParametro = '';
				$ruta = 'factour.'.$value->accion;
				$htmlBtn = '';
				//LLEVA PARAMETRO
				if($value->lleva_parametro == '1'){
		
				foreach ($parametros as $indice=>$valor) { 
		
					if($indice == $value->nombre_parametro){
					$idParametro = $valor;
					}
				}
		
				//PARAMETRO OCULTO EN DATA
				if($value->parametro_oculto == '1'){
					$htmlBtn = "<a role='button' class='".$value->clase."' data-btn='".$idParametro."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
				} else {
					$htmlBtn = "<a role='button' href='".route($ruta,['id'=>$idParametro])."' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
				}
		
				} else {
					$htmlBtn = "<a role='button' href='#' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i>".$value->texto_alternativo." </a>"; 
				}
		
				
				$boton[] = $htmlBtn;
				}
				return $boton;
			
				} else {
				return array();
				}
		
	}//function

	private function getIdUsuario()
	{
		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
    }
   
	public function corteProveedor()
	{

		$proveedor = Persona::where('id_tipo_persona', '=', '14')
							->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->get();
    	return view('pages.mc.reportes.corteProveedor')->with(['proveedores'=>$proveedor]);	
    }

	public function getCorteProveedor(Request $request){
		//dd($request->all());

		$query = "select to_char(c.fecha_venta,
							 'dd/mm/yyyy') fecha_venta,
							 to_char(c.fecha_venta, 'dd') hora_corte,
							 d.id_proveedor, d.id_producto,	
							 'fecha_limite_entraga' fecha_limite_entraga,
							 w.id_woo_order numero_pedido, 
							 'numero_oc' numero_oc, 
							 p.nombre proveedor,
							 r.denominacion producto, 
							 d.nro_factura_proveedor sku, 
							 d.cantidad, d.precio_costo,
							 'dia_pedido_corte' dia_pedido_corte,
							 c.id numero_venta, 
							 c.id_empresa,
							 e.denominacion estado_venta
							 from ventas_rapidas_cabecera c, ventas_rapidas_detalle d, estados e,
									personas p, productos r, comercio_empresa m, pedidos_woo w
									where c.id = d.id_venta_cabecera
									and c.id_estado = e.id
									and d.id_proveedor = p.id
									and d.id_producto = r.id
									and c.id_comercio_empresa = m.id
									and c.id_pedido = w.id
									and c.id_empresa =".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

		if($request->input("id_vendedor_empresa") != ''){
            $query.= ' AND d.id_proveedor = '.$request->input("id_vendedor_empresa");
        }

        if($request->input('periodo') != ""){
	        $fechaPeriodo = explode(' - ', $request->input('periodo'));
 			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
            $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
        	$query .= " AND c.fecha_venta BETWEEN '".$desde."' AND '".$hasta."'";
        } 
        
        if($request->input("id_estado") != ''){
            $query.= " AND to_char(c.fecha_venta, 'dd') = '".$request->input("id_estado")."'";
        }						

        $corte = DB::select($query);

		return response()->json(['data'=>$corte]);
	}


	public function vendedorRentabilidad(Request $request)
	{
		$vendedores = Persona::where('id_tipo_persona','3')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
		$monedas = Divisas::where('activo','S')->get();
		$negocios = Negocio::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();

    	return view('pages.mc.reportes.vendedorRentabilidad')->with(['vendedores'=>$vendedores, 'currency'=>$monedas,'negocios'=>$negocios]);	

	}	

	
	public function getResumenVendedorIncentivo(Request $request)
	{
		$query = "SELECT vendedor_nombre, vendedor_apellido,id_moneda_venta, currency_code,sum(total) as total, sum(total_costos) as costo, sum(renta) as renta
				  FROM incentivo_vendedor_detalle
				  WHERE id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa." AND id_estado_factura = 29";

			if($request->input("idVendedor") != ''){
			$query.= ' AND id_usuario = '.$request->input("idVendedor");
			}

			 if($request->input("negocio") != ''){
				$query.= " AND id_unidad_negocio =".$request->input("negocio");
			}	

			if($request->input("idMoneda") != ''){
				$query.= " AND id_moneda_venta = ".$request->input("idMoneda");
			}	

			if($request->input('fecha_facturacion') != ""){
				$fechaPeriodo = explode(' - ', $request->input('fecha_facturacion'));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
				$query .= " AND fecha_hora_facturacion BETWEEN '".$desde."' AND '".$hasta."'";
			} 
						
			$query.= " GROUP BY id_usuario, id_moneda_venta, vendedor_nombre,vendedor_apellido,currency_code";

			$incentivo = DB::select($query);
		return response()->json(['data'=>$incentivo]);
	}


	public function getDetalleVendedorIncentivo(Request $request)
	{
		    $query = "SELECT id_usuario, vendedor_nombre, vendedor_apellido,id_moneda_venta, currency_code,sum(total) as total, sum(total_costos) as costo, sum(renta) as renta
				  FROM incentivo_vendedor_detalle
				  WHERE id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa." AND id_estado_factura = 29";

			if($request->input("idVendedor") != ''){
			$query.= ' AND id_usuario = '.$request->input("idVendedor");
			}

			 if($request->input("negocio") != ''){
				$query.= " AND id_unidad_negocio =".$request->input("negocio");
			}	

			if($request->input("idMoneda") != ''){
				$query.= " AND id_moneda_venta = ".$request->input("idMoneda");
			}	

			if($request->input('fecha_facturacion') != ""){
				$fechaPeriodo = explode(' - ', $request->input('fecha_facturacion'));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
				$query .= " AND fecha_hora_facturacion BETWEEN '".$desde."' AND '".$hasta."'";
			} 
						
			$query.= " GROUP BY id_usuario, id_moneda_venta, vendedor_nombre,vendedor_apellido,currency_code";

			$incentivo = DB::select($query);

		$resultado = [];
        $contador = 0;

        foreach($incentivo as $key=>$valor){
			$extractoDetalles =  DB::table('incentivo_vendedor_detalle');
			$extractoDetalles =  $extractoDetalles->where('id_usuario',$valor->id_usuario);
			$extractoDetalles =  $extractoDetalles->where('id_moneda_venta',$valor->id_moneda_venta);
			if($request->input("negocio") != ''){
				$extractoDetalles =  $extractoDetalles->where('id_unidad_negocio',$request->input("negocio"));
			}
	
			$extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
			if($request->input("fecha_facturacion") != ''){
				$fechaPeriodo = explode(' - ', $request->input("fecha_facturacion"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_hora_facturacion', array($desde.' 00:00:00',$hasta .' 23:59:59'));
			}
			$extractoDetalles =  $extractoDetalles->orderBy('id_moneda_venta', 'DESC');
			$extractoDetalles =  $extractoDetalles->get();
			 if(isset($extractoDetalles[0]->nro_factura)){
				$resultado[$key] = $valor;
				$detalles = [];
				foreach($extractoDetalles as $keys=>$detalle){
					$detalles[$keys]['cliente'] = $detalle->nombre." ".$detalle->apellido;
					$detalles[$keys]['nro_factura'] = $detalle->nro_factura;
					$detalles[$keys]['id_proforma'] = $detalle->id_proforma;
					$detalles[$keys]['total'] = $detalle->total;
					$detalles[$keys]['total_costos'] = $detalle->total_costos;
					if($detalle->id_estado_factura == 29){
						$detalles[$keys]['renta'] = $detalle->renta;
					}else{
						$detalles[$keys]['renta'] = 0;
					}
					$detalles[$keys]['moneda'] = $detalle->currency_code;
				}
				$resultado[$key]->detalles = $detalles;
			}
		}

		foreach($resultado as $key=>$st){
            $listado['data'][] = $st;	
        }

        if(empty($listado)){
                $listado['data'][] = array();
        }
        return response()->json($listado);
	}

	public function ingresos(Request $req){					
		return view('pages.mc.ingresos.index');
	}

	public function ajaxIngresos(Request $req){
	
		$fecha_cierre =$this->formatoFechaEntrada($req->input('fecha'));
		$id_empresa = $this->getIdEmpresa();

		$ingresos = DB::select('SELECT 
			SUM(monto) as total,
			cr.moneda_id,
			cr.forma_cobro_id,
			fcc.denominacion AS forma_cobro,
			cu.currency_code
		FROM cierre_resumen cr
		JOIN forma_cobro_cliente fcc ON fcc.id = cr.forma_cobro_id
		JOIN currency cu ON cu.currency_id = cr.moneda_id
		WHERE id_cierre IN (
			SELECT 
				id
			FROM cierre_caja cc 
			WHERE cc.fecha_cierre = ?
			AND cc.id_empresa = ?
			AND cc.activo = true
		)
		GROUP BY fcc.denominacion, cu.currency_code, cr.moneda_id, cr.forma_cobro_id', [$fecha_cierre, $id_empresa]);



		
		return response()->json($ingresos);
	}


	public function egresos(Request $req){		
		return view('pages.mc.egresos.index');
	}

	/**
	 * Recumen de las formas de pago y moneda
	 */
	public function ajaxEgresoCuentas(Request $req){

		$fecha_cierre =$this->formatoFechaEntrada($req->input('fecha'));
		$id_empresa = $this->getIdEmpresa();

		$egresos = DB::select('SELECT 
		bc.nombre as cuenta,
		cu.currency_code as moneda,
		sum(fpod.importe_pago) as total,
		fpod.id_banco_detalle,
		fpoc.id_moneda,
		bd.numero_cuenta
		
		from op_cabecera op_c
		join forma_pago_op_cabecera fpoc ON fpoc.id_op = op_c.id
		join forma_pago_op_detalle fpod ON fpod.id_cabecera = fpoc.id
		join forma_pago_cliente fpc ON fpc.id = fpod.id_tipo_pago
		join banco_detalle bd ON bd.id = fpod.id_banco_detalle
		join banco_cabecera bc ON bc.id = bd.id_banco
		join currency cu ON cu.currency_id = fpoc.id_moneda
		where op_c.id_estado IN (53,50)
		AND op_c.id_empresa = ?
		AND op_c.activo = true
		AND (
			date(op_c.fecha_hora_autorizado) = ? OR 
			date(op_c.fecha_hora_procesado)  = ?
		)
		group by bc.nombre,fpoc.id_moneda,cu.currency_code,fpod.id_banco_detalle, bd.numero_cuenta', [$id_empresa, $fecha_cierre, $fecha_cierre]);

		return response()->json(['data' =>$egresos]);

	}

	/**
	 * Recumen de las formas de pago y moneda
	 */
	public function ajaxEgresos(Request $req){

		$fecha_cierre =$this->formatoFechaEntrada($req->input('fecha'));
		$id_empresa = $this->getIdEmpresa();
		$id_moneda = $req->input('id_moneda');
		$id_banco_detalle = $req->input('id_banco_detalle');

		$egresos = DB::select('
		SELECT 
			SUM(fpoc.importe_total) as total_pagado,
			fpoc.id_moneda,
			fpod.id_tipo_pago,
			fpc.denominacion AS forma_pago,
			cu.currency_code
		FROM forma_pago_op_cabecera fpoc
		JOIN forma_pago_op_detalle fpod ON fpoc.id = fpod.id_cabecera
		JOIN forma_pago_cliente fpc ON fpod.id_tipo_pago = fpc.id
		JOIN currency cu ON cu.currency_id = fpoc.id_moneda
		WHERE fpoc.id_op IN (
			SELECT 
				id
			FROM op_cabecera op_c
			WHERE op_c.id_estado IN (53,50)
			AND op_c.id_empresa = ?
			AND op_c.activo = true
			AND (
			date(op_c.fecha_hora_autorizado) = ? OR 
			date(op_c.fecha_hora_procesado)  = ?
			)
		)
		AND fpoc.id_moneda = ?
		AND fpod.id_banco_detalle = ?
		GROUP BY fpoc.id_moneda, fpod.id_tipo_pago, fpc.denominacion, cu.currency_code', [$id_empresa, $fecha_cierre, $fecha_cierre, $id_moneda, $id_banco_detalle ]);

		return response()->json($egresos);

	}

	/**
	 * Obtener las facturas de las OP segun la forma de pago, fecha y moneda
	 */
	public function ajaxDetallesOp(Request $req){
		$fecha =$this->formatoFechaEntrada($req->input('fecha'));
		$id_empresa = $this->getIdEmpresa();
		$id_moneda = $req->input('id_moneda');
		$id_tipo_pago = $req->input('id_forma_pago');
		$id_banco_detalle = $req->input('id_banco_detalle');


		// Obtener todas las formas de pago de las ops que tengan el mismo id_moneda y id_tipo_pago y en la fecha
		$ops = DB::select("SELECT 
		op_c.nro_op,
		op_c.id,
		concat(proveedor.nombre, ' ',proveedor.apellido) as proveedor,
		op_c.total_neto_pago as total,
		e.denominacion as estado,
		op_c.cotizacion,
		cu.currency_code
		FROM op_cabecera op_c
		JOIN forma_pago_op_cabecera fpoc ON fpoc.id_op = op_c.id
		JOIN forma_pago_op_detalle fpod ON fpoc.id = fpod.id_cabecera
		JOIN currency cu ON cu.currency_id = op_c.id_moneda
		JOIN personas proveedor ON proveedor.id = op_c.id_proveedor
		JOIN estados e ON e.id = op_c.id_estado
		WHERE op_c.id_estado IN (53,50)
		AND op_c.id_empresa = ?
		AND op_c.activo = true
		AND (
			date(op_c.fecha_hora_autorizado) = ? OR
			date(op_c.fecha_hora_procesado)  = ?
			)
		AND fpod.id_tipo_pago = ?
		AND fpod.id_banco_detalle = ?
		AND fpoc.id_moneda = ?", [ $id_empresa, $fecha, $fecha, $id_tipo_pago, $id_banco_detalle, $id_moneda]);

		//Recorrer las ops para obtener los detalles de las formas de pago
		foreach ($ops as $op) {
			$detalles = DB::select("SELECT 
			fpc.denominacion as forma_pago,
			fpod.beneficiario_txt,
			bc.nombre as banco,
			cu.currency_code,
			fpod.nro_comprobante,
			fpod.importe_pago
			FROM forma_pago_op_cabecera fpoc
			JOIN forma_pago_op_detalle fpod ON fpoc.id = fpod.id_cabecera
			JOIN forma_pago_cliente fpc ON fpod.id_tipo_pago = fpc.id
			JOIN currency cu ON cu.currency_id = fpoc.id_moneda
			join banco_detalle bd ON bd.id = fpod.id_banco_detalle
			join banco_cabecera bc ON bc.id = bd.id_banco
			where fpoc.id_op  = ?", [$op->id]);

			$op->detalles = $detalles;
		}

		return response()->json(['data' => $ops]);
	}


//GES899
public function indexVtaPrestador(Request $request)
	{
		$id_empresa = $this->getIdEmpresa();
		$desde = $request->input('desde');
		$hasta = $request->input('hasta');
		$buscar = $request->input('buscar');
		$vendedorId = $request->input('vendedor_id');
		$nroFactura = $request->input('nro_factura'); // Nuevo: Obtener el valor del parámetro 'nro_factura'
		$idProforma = $request->input('id_proforma'); // Nuevo: Obtener el valor del parámetro 'id_proforma'

		$moneda = $request->input('id_moneda');
		$producto = $request->input('producto_0');
		$inputPrestador = $request->input('id_prestador');
		$inputvendedorEmpresa = $request->input('vendedor_empresa');
		$id_proveedor = $request->input('id_proveedor');
		//dd($inputPrestador);


		$currency = Currency::where('activo', 'S')->get();//moneda

$prestador = Persona::select('id')
    ->selectRaw('COALESCE(denominacion_comercial, nombre) AS denominacion_comercial')
    ->whereIn('id', function ($query) use ($id_empresa) {
        $query->select('id_prestador')
            ->from('facturas_detalle')
            ->leftJoin('facturas', 'facturas.id', '=', 'facturas_detalle.id_factura')
            ->where('facturas.id_empresa', $id_empresa)
            ->whereNotNull('id_prestador')
            ->groupBy('id_prestador');
    })
    ->get();


	$vendedorEmpresa = DB::table('personas AS vde')
    ->select('vde.id', DB::raw("CONCAT(vde.nombre, ' ', vde.apellido) AS vendedorEmpresa"))
    ->whereExists(function ($query) use ($id_empresa) {
        $query->select(DB::raw(1))
            ->from('facturas AS f')
            ->whereColumn('f.id_usuario', 'vde.id')
            ->where('f.id_empresa', $id_empresa);
    })
    ->get();


	$proveedor = Persona::select('id')
    ->selectRaw('nombre AS proveedor')
    ->whereIn('id', function ($query) use ($id_empresa) {
        $query->select('id_proveedor')
            ->from('facturas_detalle')
            ->leftJoin('personas', 'personas.id', '=', 'facturas_detalle.id_proveedor')
			->leftJoin('facturas', 'facturas.id', '=', 'facturas_detalle.id_factura')
            ->where('facturas.id_empresa', $id_empresa)
            ->whereNotNull('id_proveedor')
            ->groupBy('id_proveedor');
    })
    ->get();

	
	
		$productos = Producto::with('sucursales')//producto
		->where('visible', true)
		->where('activo', true)
		->where('tipo_producto', 'P')
		->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
		->get();

		$query = DB::table('facturas AS f')
		->select(
			'f.id',
			'mv.currency_code AS moneda_venta',
			'mc.currency_code AS moneda_costo',
			'provee.nombre AS proveedor',
			'ft.id_prestador',
			'f.total_bruto_factura',
			'ft.renta',
			'ft.fecha_in',
			'f.fecha_hora_facturacion',
			'f.nro_factura',
			'f.id_proforma',
			'v.nombre AS VendedorAgencia',
			'f.cliente_id',
			'pc.nombre',
			'pc.email',
			'pc.telefono',
			DB::raw("CONCAT(vde.nombre, ' ', vde.apellido) AS VendedorEmpresa"),
			'ft.costo_proveedor as precio_costo',
			'ft.precio_venta',
			'ft.cod_confirmacion',
			'ft.fecha_de_gasto',
			'd.desc_destino',
			'ft.descripcion',
			'pt.denominacion_comercial',
			DB::raw("CONCAT(pt.nombre, ' ', pt.apellido) AS prestador"),
			'producto.denominacion as producto'
		)
		->leftJoin('personas AS c', 'c.id', '=', 'f.cliente_id')
		->leftJoin('empresas AS em', 'em.id', '=', 'f.id_empresa')
		->leftJoin('personas AS v', 'v.id', '=', 'f.vendedor_id')
		->leftJoin('personas AS vde', 'f.id_usuario', '=', 'vde.id')
		//->leftJoin('personas AS vde', 'f.id_usuario', '=', 'vde.id_vendedor_empresa')
		->leftJoin('personas AS pc', 'pc.id', '=', 'f.cliente_id')
		->leftJoin('facturas_detalle AS ft', 'ft.id_factura', '=', 'f.id')
		->leftJoin('personas AS pt', 'pt.id', '=', 'ft.id_prestador')
		->leftJoin('currency AS mc', 'mc.currency_id', '=', 'ft.currency_costo_id')
		->leftJoin('currency AS mv', 'mv.currency_id', '=', 'ft.currency_venta_id')
		//->leftJoin('currency AS m', 'm.currency_id', '=', 'f.id_moneda_venta')
		->leftJoin('personas AS u', 'u.id', '=', 'f.id_usuario')
		->leftJoin('destinos_dtpmundo AS d', 'd.id_destino_dtpmundo', '=', 'f.destino_id')
		->leftJoin( 'personas AS provee', 'provee.id','=', 'ft.id_proveedor')
		->leftJoin( 'productos AS producto', 'producto.id','=', 'ft.id_producto')
		//->leftJoin('proformas AS proforma', 'proforma.id', '=', 'f.id_proforma')
		//->leftJoin('nemo_reservas AS pre', 'pre.id_proforma', '=', 'proforma.id' )
		//->distinct('')
		->where('f.id_empresa', $id_empresa)
		->where('f.id_estado_factura', '<>', 30);
	
	
			if ($desde && $hasta) {
				$query->whereBetween('f.fecha_hora_facturacion', [$desde. ' 00:00:00', $hasta.' 23:59:59']);
			}
	
	
			if ($buscar) {
				$query->where(function ($query) use ($buscar) {
					$query->where(DB::raw("CAST(provee.nombre AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.id_prestador AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.total_bruto_factura AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.renta AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.fecha_in AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.fecha_hora_facturacion AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.nro_factura AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.id_proforma AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(v.nombre AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.cliente_id AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(pc.nombre AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(pc.telefono AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(pc.email AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(CONCAT(vde.nombre, ' ', vde.apellido) AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.precio_costo AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.precio_venta AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.cod_confirmacion AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.fecha_de_gasto AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(d.desc_destino AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.descripcion AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(pt.denominacion_comercial AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(CONCAT(pt.nombre, ' ', pt.apellido) AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(producto.denominacion AS TEXT)"), 'like', '%' . $buscar . '%');
				});
			}
			
			
		if ($vendedorId) {
			$query->where('v.id', $vendedorId); // Filtrar por el ID del vendedor
		}

		if ($nroFactura) {
			$query->where('f.nro_factura', $nroFactura); // Filtrar por el número de factura
		}
	
		if ($idProforma) {
			$query->where('f.id_proforma', $idProforma); // Filtrar por el ID de proforma
		}

		if ($moneda) {
			$query->where('f.id_moneda_venta', $moneda);
		}
		if ($producto) {
			$query->where('ft.id_producto', $producto);
		}
		if ($inputPrestador) {
			$query->where('ft.id_prestador', $inputPrestador);
		}
		
		if ($inputvendedorEmpresa) {
			$query->where('vde.id', $inputvendedorEmpresa);
		}
		
		if ($id_proveedor) {
			$query->where('ft.id_proveedor', $id_proveedor);
		}
		//dd($inputPrestador);
		$facturas = $query->orderBy('f.id', 'desc')->paginate(10);
		//dd($query->toSql());
		//return view('pages.mc.reportes.ventaPrestador')->with('facturas', $facturas);
		/*return view('pages.mc.reportes.ventaPrestador')->with(['facturas' => $facturas, 
																'currency' => $currency]);*/
	return view('pages.mc.reportes.ventaPrestador')->with(['facturas' => $facturas,
																'currency' => $currency,
																'producto' => $productos,
																'prestador' => $prestador,
																'vendedorEmpresa' => $vendedorEmpresa,
																'proveedor' => $proveedor]);


									
	}

	public function ventaPrestadorAjax(Request $request){
		//dd($request->all());
		//dd($datos);*/
		$redisKey = 'ventaPrestadorAjax:' . md5(serialize($request->input()));
		
		if (Redis::exists($redisKey)) {
			// Recuperar los Connection to 172.17.0.2 6379 port [tcp/*] succeeded!resultados de la consulta de Redis
			$facturas = json_decode(Redis::get($redisKey));

		}else{

				
		$id_empresa = $this->getIdEmpresa();
		if($request->input('periodo_out') != ""){
			$fechaPeriodo = explode(' - ', $request->input('periodo_out'));
			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
			$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
		} 
		//$buscar = $request->input('buscar');
		$vendedorId = $request->input('vendedor_id');
		$nroFactura = $request->input('nro_factura');
		$idProforma = $request->input('id_proforma');
		$moneda = $request->input('id_moneda');
		$producto = $request->input('producto_0');
		$inputPrestador = $request->input('id_prestador');
		$inputvendedorEmpresa = $request->input('vendedor_empresa');
		$id_proveedor = $request->input('id_proveedor');
		/*echo '<pre>';
		print_r($inputvendedorEmpresa);	*/
		$query = DB::table('facturas AS f')
			->select(
				'f.id',
				'mv.currency_code AS moneda_venta',
				'mc.currency_code AS moneda_costo',
				'provee.nombre AS proveedor',
				'ft.id_prestador',
				'f.total_bruto_factura',
				'ft.renta',
				'ft.fecha_in',
				'f.fecha_hora_facturacion',
				'f.nro_factura',
				'f.id_proforma',
				'v.nombre AS VendedorAgencia',
				'f.cliente_id',
				'pc.nombre',
				'pc.email',
				'pc.telefono',
				DB::raw("CONCAT(vde.nombre, ' ', vde.apellido) AS VendedorEmpresa"),
				DB::raw("COALESCE(ft.costo_proveedor, 0) + COALESCE(ft.costo_gravada, 0) as precio_costo"),
				'ft.precio_venta',
				'ft.cod_confirmacion',
				'ft.fecha_de_gasto',
				'd.desc_destino',
				'ft.descripcion',
				'pt.denominacion_comercial',
				DB::raw("CONCAT(pt.nombre, ' ', pt.apellido) AS prestador"),
				'producto.denominacion as producto'
			)
			->leftJoin('personas AS c', 'c.id', '=', 'f.cliente_id')
			->leftJoin('empresas AS em', 'em.id', '=', 'f.id_empresa')
			->leftJoin('personas AS v', 'v.id', '=', 'f.vendedor_id')
			->leftJoin('personas AS vde', 'f.id_usuario', '=', 'vde.id')
			->leftJoin('personas AS pc', 'pc.id', '=', 'f.cliente_id')
			->leftJoin('facturas_detalle AS ft', 'ft.id_factura', '=', 'f.id')
			->leftJoin('personas AS pt', 'pt.id', '=', 'ft.id_prestador')
			->leftJoin('currency AS mc', 'mc.currency_id', '=', 'ft.currency_costo_id')
			->leftJoin('currency AS mv', 'mv.currency_id', '=', 'ft.currency_venta_id')
			//->leftJoin('currency AS m', 'm.currency_id', '=', 'f.id_moneda_venta')
			->leftJoin('personas AS u', 'u.id', '=', 'f.id_usuario')
			->leftJoin('destinos_dtpmundo AS d', 'd.id_destino_dtpmundo', '=', 'f.destino_id')
			->leftJoin('personas AS provee', 'provee.id', '=', 'ft.id_proveedor')
			->leftJoin('productos AS producto', 'producto.id', '=', 'ft.id_producto')
			->where('f.id_empresa', $id_empresa)
			->where('f.id_estado_factura', '<>', 30)
			->groupBy(
				'f.id',
				'mv.currency_code',
				'mc.currency_code',
				'provee.nombre',
				'ft.id_prestador',
				'f.total_bruto_factura',
				'ft.renta',
				'ft.fecha_in',
				'f.fecha_hora_facturacion',
				'f.nro_factura',
				'f.id_proforma',
				'v.nombre',
				'f.cliente_id',
				'pc.nombre',
				'pc.email',
				'pc.telefono',
				'vde.nombre',
				'vde.apellido',
				'ft.costo_proveedor',
				'ft.costo_gravada',
				'ft.precio_venta',
				'ft.cod_confirmacion',
				'ft.fecha_de_gasto',
				'd.desc_destino',
				'ft.descripcion',
				'pt.denominacion_comercial',
				'pt.nombre',
				'pt.apellido',
				'producto.denominacion'
			);

			if ($request->input('search.value')) {
				$searchValue = $request->input('search.value');
				$query->where(function ($query) use ($searchValue) {
					$query->where('f.nro_factura', 'LIKE', "%$searchValue%")
					->orWhere(DB::raw("CONCAT(pt.nombre, ' ', pt.apellido)"), 'LIKE', '%' . $searchValue . '%')
					->orWhere('f.nro_factura', 'LIKE', '%' . $searchValue . '%')
					->orWhere('pc.nombre', 'LIKE', '%' . $searchValue . '%')
					->orWhere('pc.telefono', 'LIKE', '%' . $searchValue . '%')
					->orWhere('pc.email', 'LIKE', '%' . $searchValue . '%')
					->orWhere('ft.descripcion', 'LIKE', '%' . $searchValue . '%')
					->orWhere('producto.denominacion', 'LIKE', '%' . $searchValue . '%')
					->orWhere('ft.cod_confirmacion', 'LIKE', '%' . $searchValue . '%')
					->orWhere(DB::raw("CONCAT(vde.nombre, ' ', vde.apellido)"), 'LIKE', '%' . $searchValue . '%')
					->orWhere('d.desc_destino', 'LIKE', '%' . $searchValue . '%')
					->orWhere(DB::raw("CAST(ft.precio_costo AS TEXT)"), 'LIKE', '%' . $searchValue . '%')
					->orWhere(DB::raw("CAST(ft.precio_venta AS TEXT)"), 'LIKE', '%' . $searchValue . '%')
					->orWhere(DB::raw("CAST(ft.renta AS TEXT)"), 'LIKE', '%' . $searchValue . '%')
					->orWhere(DB::raw("CAST(f.id_proforma AS TEXT)"), 'LIKE', '%' . $searchValue . '%');;
				});
			}
	
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('f.fecha_hora_facturacion', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);
		}
	
		if ($vendedorId) {
			$query->where('v.id', $vendedorId);
		}
	
		if ($nroFactura) {
			$query->where('f.nro_factura', $nroFactura);
		}
	
		if ($idProforma) {
			$query->where('f.id_proforma', $idProforma);
		}
	
		if ($moneda) {
			$query->where('f.id_moneda_venta', $moneda);
		}
	
		if ($producto) {
			$query->where('ft.id_producto', $producto);
		}
	
		if ($inputPrestador) {
			$query->where('ft.id_prestador', $inputPrestador);
		}
	
		if ($inputvendedorEmpresa) {
			$query->where('vde.id', $inputvendedorEmpresa);
		}
		if ($id_proveedor) {
			$query->where('ft.id_proveedor', $id_proveedor);
		}
		$totalRecords = $query->count();

		$draw = $request->input('draw');
		$start = $request->input('start', 0);
		$length = $request->input('length', 10);
		$recordsFiltered = $totalRecords;
	
		$facturas = $query->offset($start)->limit($length)->get();

		Redis::set($redisKey, json_encode($facturas));
	}	//fin else redis
		return response()->json([
			'data' => $facturas,
			'draw' => $draw,
			'recordsTotal' => $totalRecords,
			'recordsFiltered' => $recordsFiltered,
		]);
	
	}

	
//ATK
public function ventaRapida(Request $request)
	{
		$id_empresa = $this->getIdEmpresa();
		$desde = $request->input('desde');
		$hasta = $request->input('hasta');
		$buscar = $request->input('buscar');
		$vendedorId = $request->input('vendedor_id');
		$nroFactura = $request->input('nro_factura');

		$query = DB::table('facturas AS f')
			->select(
				'f.id',
				'm.currency_code AS moneda_venta',
				'provee.nombre AS proveedor',
				'ft.renta',
				'ft.fecha_in',
				'f.fecha_hora_facturacion',
				'f.nro_factura',
				'f.id_proforma',
				'v.nombre AS VendedorAgencia',
				'f.cliente_id',
				'pc.nombre',
				DB::raw("CONCAT(vde.nombre, ' ', vde.apellido) AS VendedorEmpresa"),
				'f.id_venta_rapida',
				'ft.descripcion',
				'ft.precio_costo',
				'ft.precio_venta',
				'ft.cantidad',
				'estado.denominacion as estado',
				'producto.denominacion as producto'
			)
			->leftJoin('personas AS c', 'c.id', '=', 'f.cliente_id')
			->leftJoin('empresas AS em', 'em.id', '=', 'f.id_empresa')
			->leftJoin('personas AS v', 'v.id', '=', 'f.vendedor_id')
			->leftJoin('personas AS vde', 'f.id_usuario', '=', 'vde.id')
			//->leftJoin('personas AS vde', 'f.id_usuario', '=', 'vde.id_vendedor_empresa')
			->leftJoin('personas AS pc', 'pc.id', '=', 'f.cliente_id')
			->leftJoin('facturas_detalle AS ft', 'ft.id_factura', '=', 'f.id')
			->leftJoin('currency AS m', 'm.currency_id', '=', 'f.id_moneda_venta')
			->leftJoin('personas AS u', 'u.id', '=', 'f.id_usuario')
			->leftJoin('destinos_dtpmundo AS d', 'd.id_destino_dtpmundo', '=', 'f.destino_id')
			->leftJoin( 'personas AS provee', 'provee.id','=', 'ft.id_proveedor')
			->leftJoin( 'estados AS estado', 'estado.id','=', 'f.id_estado_factura')
			->leftJoin( 'productos AS producto', 'producto.id','=', 'ft.id_producto')
			->whereNotNull('f.id_venta_rapida') // Agregar esta cláusula;
			->where('f.id_empresa', $id_empresa)
			->where('f.id_estado_factura', '<>', 30);
	
			if ($desde && $hasta) {
				$query->whereBetween('f.fecha_hora_facturacion', [$desde. ' 00:00:00', $hasta.' 23:59:59']);
			}
	
			if ($buscar) {
				$query->where(function ($query) use ($buscar) {
					$query->where(DB::raw("CAST(provee.nombre AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.id_prestador AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.total_bruto_factura AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.renta AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.fecha_in AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.fecha_hora_facturacion AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.nro_factura AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.id_proforma AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(v.nombre AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(f.cliente_id AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(pc.nombre AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(CONCAT(vde.nombre, ' ', vde.apellido) AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.precio_costo AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.precio_venta AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.cod_confirmacion AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.fecha_de_gasto AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(d.desc_destino AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(ft.descripcion AS TEXT)"), 'like', '%' . $buscar . '%')
						->orWhere('estado.denominacion', 'like', '%' . $buscar . '%')
						->orWhere(DB::raw("CAST(producto.denominacion AS TEXT)"), 'like', '%' . $buscar . '%');
				});	
			}
			
			if ($vendedorId) {
				$query->where('v.id', $vendedorId); // Filtrar por el ID del vendedor
			}

			if ($nroFactura) {
				$query->where('f.nro_factura', $nroFactura); // Filtrar por el número de factura
			}
			
			$facturas = $query->orderBy('f.id', 'desc')->paginate(10);
	
		return view('pages.mc.reportes.ventaRapida')->with('facturas', $facturas);
	}


	public function resumenCobros(Request $request)
	{
		$id_empresa = $this->getIdEmpresa();
		$desde = $request->input('desde');
		if($request->input('periodo_out') != ""){
			$fechaPeriodo = explode(' - ', $request->input('periodo_out'));
			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
			$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
		} 
		$unidad_negocio = $request->input('unidad_negocio');
		$unidadNegocio = DB::table('unidad_negocios')
		->select('id','descripcion')
		->where('id_empresa', $id_empresa)
		->get();	

		$query = DB::table('recibos as rcc')
		->join('recibos_detalle as rcd', 'rcd.id_cabecera', '=', 'rcc.id')
		->join('libros_ventas as lv', 'lv.id', '=', 'rcd.id_libro_venta')
		->join('proformas as pf', 'pf.id', '=', 'lv.id_proforma')
		->join('unidad_negocios as uni', 'uni.id', '=', 'pf.id_unidad_negocio')
		->join('forma_cobro_recibo_cabecera as fcc', 'fcc.id_recibo', '=', 'rcc.id')
		->join('forma_cobro_recibo_detalle as fcd', 'fcd.id_cabecera', '=', 'fcc.id')
		->join('currency as cu', 'cu.currency_id', '=', 'fcd.id_moneda')
		->join('banco_detalle as banco_d', 'banco_d.id', '=', 'fcd.id_banco_detalle')
		->join('banco_cabecera as banco_c', 'banco_c.id', '=', 'banco_d.id_banco')
		->join('tipo_cuenta_banco as tc_banco', 'tc_banco.id', '=', 'banco_d.id_tipo_cuenta')
		->groupBy('pf.id', 'uni.descripcion', 'uni.id','banco_c.nombre', 'tc_banco.denominacion', 'banco_d.numero_cuenta', 'rcc.id')
		
		->selectRaw('
        pf.id,
		uni.descripcion as unidad_negocio,
		uni.id,
        CONCAT(banco_c.nombre, \' \', tc_banco.denominacion, \' \', banco_d.numero_cuenta) AS banco_cuenta,
        CONCAT(banco_c.nombre, \' \', tc_banco.denominacion) AS banco_cuenta_n,
        SUM(CASE WHEN cu.currency_id = 143 THEN fcd.importe_pago ELSE 0 END) AS total_importe_pago_usd,
        SUM(CASE WHEN cu.currency_id = 43 THEN fcd.importe_pago ELSE 0 END) AS total_importe_pago_eur,
        SUM(CASE WHEN cu.currency_id = 111 THEN fcd.importe_pago ELSE 0 END) AS total_importe_pago_pyg'
    	)
		//->distinct('')
		->where('pf.id_empresa', $id_empresa)
		->where('rcc.id_estadi', '<>', 55)
		->where('rcc.id_estadi', '<>', 31)
		->where('fcc.actio', '<>', false);
			
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('fcc.fecha_hora', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);
		}
	
			
		if ($unidad_negocio) {
			$query->where('uni.id', $unidad_negocio); // Filtrar por Unidad de negocio
		}

		//$cobros = $query->orderBy('rcc.id', 'desc')->paginate(10);
		return view('pages.mc.reportes.resumenCobros')->with([
															'unidadNegocio'=>$unidadNegocio]);


									
	}

	public function resumenCobrosAjax(Request $request){
		$id_empresa = $this->getIdEmpresa();
		if($request->input('periodo_out') != ""){
			$fechaPeriodo = explode(' - ', $request->input('periodo_out'));
			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
			$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
		} 
		$buscar = $request->input('buscar');
		$unidad_negocio = $request->input('unidad_negocio');
		$unidadNegocio = DB::table('unidad_negocios')
		->select('id','descripcion')
		->where('id_empresa', $id_empresa)
		->get();	

		$query = DB::table('recibos as rcc')
		->join('recibos_detalle as rcd', 'rcd.id_cabecera', '=', 'rcc.id')
		->join('libros_ventas as lv', 'lv.id', '=', 'rcd.id_libro_venta')
		->join('proformas as pf', 'pf.id', '=', 'lv.id_proforma')
		->join('unidad_negocios as uni', 'uni.id', '=', 'pf.id_unidad_negocio')
		->join('forma_cobro_recibo_cabecera as fcc', 'fcc.id_recibo', '=', 'rcc.id')
		->join('forma_cobro_recibo_detalle as fcd', 'fcd.id_cabecera', '=', 'fcc.id')
		->join('currency as cu', 'cu.currency_id', '=', 'fcd.id_moneda')
		->join('banco_detalle as banco_d', 'banco_d.id', '=', 'fcd.id_banco_detalle')
		->join('banco_cabecera as banco_c', 'banco_c.id', '=', 'banco_d.id_banco')
		->join('tipo_cuenta_banco as tc_banco', 'tc_banco.id', '=', 'banco_d.id_tipo_cuenta')
		->join('forma_cobro_cliente as f1', 'f1.id', '=', 'fcd.id_tipo_pago')
		->groupBy('pf.id', 'uni.descripcion', 'uni.id','banco_c.nombre', 'tc_banco.denominacion', 'banco_d.numero_cuenta', 'rcc.id','f1.denominacion')
		
		->selectRaw('
        pf.id,
		uni.descripcion as unidad_negocio,
		uni.id,
		f1.denominacion,
        CONCAT(banco_c.nombre, \' \' , tc_banco.denominacion, \' \' , banco_d.numero_cuenta) AS banco_cuenta,
        CONCAT(banco_c.nombre, \' \', tc_banco.denominacion) AS banco_cuenta_n,
        SUM(CASE WHEN cu.currency_id = 143 THEN fcd.importe_pago ELSE 0 END) AS total_importe_pago_usd,
        SUM(CASE WHEN cu.currency_id = 43 THEN fcd.importe_pago ELSE 0 END) AS total_importe_pago_eur,
        SUM(CASE WHEN cu.currency_id = 111 THEN fcd.importe_pago ELSE 0 END) AS total_importe_pago_pyg'
    	)
		//->distinct('')
		->where('pf.id_empresa', $id_empresa)
		->where('rcc.id_estado', '<>', 55)
		->where('rcc.id_estado', '<>', 31)
		->where('fcc.activo', '<>', false);

			if (isset($desde)  && isset($hasta)) {
				$query->whereBetween('fcc.fecha_hora', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);
			}
		
	
			
		if ($unidad_negocio) {
			$query->where('uni.id', $unidad_negocio); // Filtrar por Unidad de negocio
		}
		/*$totalRecords = $query->count();

		$draw = $request->input('draw');
		$start = $request->input('start', 0);
		$length = $request->input('length', 10);
		$recordsFiltered = $totalRecords;
	
		$recibos = $query->offset($start)->limit($length)->get();
	
		return response()->json([
			'data' => $recibos,
			'draw' => $draw,
			'recordsTotal' => $totalRecords,
			'recordsFiltered' => $recordsFiltered,
		]);*/
		/*$query->get();

		$totalRecords = $query->count();
	
		$draw = $request->input('draw');
		$start = $request->input('start', 0);
		$length = $request->input('length', 10);
		$recordsFiltered = $totalRecords;
	
		$recibos = $query->orderBy('pf.id', 'asc')->offset($start)->limit($length)->get();
		return response()->json([
			'data' => $recibos,
			'draw' => $draw,
			'recordsTotal' => $totalRecords,
			'recordsFiltered' => $recordsFiltered,
		]);*/
	/*	$resultados = $query->get(); 
		return response()->json(['data'=>$resultados]);*/
		$resultados = $query->get(); 
		$totalRecords = $resultados->count();
		$draw = $request->input('draw');
		$start = $request->input('start', 0);
		$length = $request->input('length', 10);
		$recordsFiltered = $totalRecords;

		$recibos = $query->orderBy('pf.id', 'asc')->offset($start)->limit($length)->get();
		return response()->json([
			'data' => $recibos,
			'draw' => $draw,
			'recordsTotal' => $totalRecords,
			'recordsFiltered' => $recordsFiltered,
		], 200);

	}

	public function sumaSaldos(Request $request)
    {
		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'sumaSaldos'");

	 	if(empty($permisos)){
	    	flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		} 


        $id_empresa = $this->getIdEmpresa();
		if($request->input('periodo_out') != ""){
			$fechaPeriodo = explode(' - ', $request->input('periodo_out'));
			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
			$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
		} 
		$query = DB::table('asientos_contables_detalle as acd')
		->Join('asientos_contables as ac', 'acd.id_asiento_contable', '=', 'ac.id')
		->Join('personas as p', 'p.id', '=', 'ac.id_usuario')
		->Join('origen_asiento_contable as o', 'o.id', '=', 'ac.id_origen_asiento')
		->leftJoin('plan_cuentas as pc', 'pc.id', '=', 'acd.id_cuenta_contable')
		->leftJoin('tipo_cuenta_corriente as tcc', 'pc.id_tipo_cuenta_corriente', '=', 'tcc.id')
		->groupBy('acd.id_cuenta_contable', 'pc.descripcion', 'pc.cod_txt', 'tcc.denominacion')
		

		->selectRaw('
        pc.cod_txt, 
        pc.descripcion, 
        tcc.denominacion, 
        count(*) as count, 
        sum(acd.debe) as debe, 
        sum(acd.haber) as haber,
        CASE
            WHEN (sum(acd.debe) - sum(acd.haber)) > 0.0 THEN sum(acd.debe) - sum(acd.haber)
            ELSE 0.0
        END AS saldo_debe,
        CASE
            WHEN (sum(acd.debe) - sum(acd.haber)) <= 0.0 THEN (sum(acd.debe) - sum(acd.haber)) * -1.0
            ELSE 0.0
        END AS saldo_haber
    ')
		->where('ac.activo', true)
		->where('acd.activo', true)
		->where('ac.id_empresa', $id_empresa);
		//->orderBy('pc.cod_txt', 'asc');
			
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('ac.fecha_hora', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);}
		
		$saldos = $query->orderBy('pc.cod_txt', 'asc')->paginate(10);

        
        //para mostrar el periodo
       

		return view('pages.mc.reportes.sumaSaldosContabilidad')->with(['saldos' => $saldos],'permisos',$permisos);
		
    }

	public function sumaSaldosAjax(Request $request)
    {
        $id_empresa = $this->getIdEmpresa();
		if($request->input('periodo_out') != ""){
			$fechaPeriodo = explode(' - ', $request->input('periodo_out'));
			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
			$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
		} 

		$query = DB::table('asientos_contables_detalle as acd')
		->Join('asientos_contables as ac', 'acd.id_asiento_contable', '=', 'ac.id')
		->Join('personas as p', 'p.id', '=', 'ac.id_usuario')
		->Join('origen_asiento_contable as o', 'o.id', '=', 'ac.id_origen_asiento')
		->leftJoin('plan_cuentas as pc', 'pc.id', '=', 'acd.id_cuenta_contable')
		->leftJoin('tipo_cuenta_corriente as tcc', 'pc.id_tipo_cuenta_corriente', '=', 'tcc.id')
		->groupBy('acd.id_cuenta_contable', 'pc.descripcion', 'pc.cod_txt', 'tcc.denominacion')
		

		->selectRaw('
        pc.cod_txt, 
        pc.descripcion, 
        tcc.denominacion, 
        count(*) as count, 
        sum(acd.debe) as debe, 
        sum(acd.haber) as haber,
        CASE
            WHEN (sum(acd.debe) - sum(acd.haber)) > 0.0 THEN sum(acd.debe) - sum(acd.haber)
            ELSE 0.0
        END AS saldo_debe,
        CASE
            WHEN (sum(acd.debe) - sum(acd.haber)) <= 0.0 THEN (sum(acd.debe) - sum(acd.haber)) * -1.0
            ELSE 0.0
        END AS saldo_haber
    ')
		->where('ac.activo', true)
		->where('acd.activo', true)
		->where('ac.id_empresa', $id_empresa);
		//->orderBy('pc.cod_txt', 'asc');
			
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('ac.fecha_hora', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);}
    
    
			$resultados = $query->get(); 
			$totalRecords = $resultados->count();
			$draw = $request->input('draw');
			$start = $request->input('start', 0);
			$length = $request->input('length', 10);
			$recordsFiltered = $totalRecords;
	
			$saldos = $query->orderBy('pc.cod_txt', 'asc')->offset($start)->limit($length)->get();
			return response()->json([
				'data' => $saldos,
				'draw' => $draw,
				'recordsTotal' => $totalRecords,
				'recordsFiltered' => $recordsFiltered,
			], 200);
      
    }

	public function ventaNc(Request $request)
    {
		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'ventaNc'");

	 	if(empty($permisos)){
	    	flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		} 

		//$id_estado_factura = $request->input('id_estado_factura');
		//$anulado_nc = $request->input('anulado_nc');  
        $id_empresa = $this->getIdEmpresa();
		if($request->input('periodo_out') != ""){
			$fechaPeriodo = explode(' - ', $request->input('periodo_out'));
			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
			$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
		} 
		$query = DB::table('vw_ventas_nc_diferencia')
		->where('id_empresa', $id_empresa);

	/*	if ($id_estado_factura) {
			$query->where('id_estado_factura', $id_estado_factura); // Filtrar por Unidad de negocio
		}
		if ($anulado_nc) {
			$query->where('anulado_nc', $anulado_nc); // Filtrar por Unidad de negocio
		}*/
			
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('fecha_hora_facturacion', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);}
		
		$saldos = $query->orderBy('id', 'asc')->paginate(10);


		return view('pages.mc.reportes.ventaReal')->with(['saldos' => $saldos,'permisos' => $permisos]);
		
    }

	public function ventaNcAjax(Request $request)
    {
		//$id_estado_factura = $request->input('id_estado_factura');
		//$anulado = $request->input('anulado'); 
        $id_empresa = $this->getIdEmpresa();
		if($request->input('periodo_out') != ""){
			$fechaPeriodo = explode(' - ', $request->input('periodo_out'));
			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
			$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
		} 

		$query = DB::table('vw_ventas_nc_diferencia')

		->where('id_empresa', $id_empresa);
	
		/*if ($id_estado_factura) {
			$query->where('id_estado_factura', $id_estado_factura); // Filtrar por Unidad de negocio
		}
		if ($anulado) {
			$query->where('anulado', $anulado); // Filtrar por Unidad de negocio
		}*/
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('fecha_hora_facturacion', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);}
    
    
			$resultados = $query->get(); 
			$totalRecords = $resultados->count();
			$draw = $request->input('draw');
			$start = $request->input('start', 0);
			$length = $request->input('length', 10);
			$recordsFiltered = $totalRecords;
	
			$saldos = $query->orderBy('id', 'asc')->offset($start)->limit($length)->get();
			return response()->json([
				'data' => $saldos,
				'draw' => $draw,
				'recordsTotal' => $totalRecords,
				'recordsFiltered' => $recordsFiltered,
			], 200);
      
    }
	

	//REPORTE DETALLES CIERRE DE CAJA

	public function detallesCierreCaja(Request $request)
    {
		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'ventaNc'");

	 	if(empty($permisos)){
	    	flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		} 

		//$id_estado_factura = $request->input('id_estado_factura');
		//$anulado_nc = $request->input('anulado_nc');  
        $id_empresa = $this->getIdEmpresa();
		if($request->input('periodo_out') != ""){
			$fechaPeriodo = explode(' - ', $request->input('periodo_out'));
			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
			$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
		} 
		$query = DB::table('vw_reporte_cierre_caja_detalle')
		->where('id_empresa', $id_empresa);

	/*	if ($id_estado_factura) {
			$query->where('id_estado_factura', $id_estado_factura); // Filtrar por Unidad de negocio
		}
		if ($anulado_nc) {
			$query->where('anulado_nc', $anulado_nc); // Filtrar por Unidad de negocio
		}*/
			
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('fecha_cierre', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);}
		
		$saldos = $query->orderBy('id_cierre', 'asc')->paginate(10);


		return view('pages.mc.reportes.reporteCajas')->with(['saldos' => $saldos,'permisos' => $permisos]);
		
    }

	public function detallesCierreCajaAjax(Request $request)
    {
		//$id_estado_factura = $request->input('id_estado_factura');
		//$anulado = $request->input('anulado'); 
        $id_empresa = $this->getIdEmpresa();
		if($request->input('periodo_out') != ""){
			$fechaPeriodo = explode(' - ', $request->input('periodo_out'));
			$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
			$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
		} 

		$query = DB::table('vw_reporte_cierre_caja_detalle')

		->where('id_empresa', $id_empresa);
	
		/*if ($id_estado_factura) {
			$query->where('id_estado_factura', $id_estado_factura); // Filtrar por Unidad de negocio
		}
		if ($anulado) {
			$query->where('anulado', $anulado); // Filtrar por Unidad de negocio
		}*/
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('fecha_cierre', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);}
    
    
			$resultados = $query->get(); 
			$totalRecords = $resultados->count();
			$draw = $request->input('draw');
			$start = $request->input('start', 0);
			$length = $request->input('length', 10);
			$recordsFiltered = $totalRecords;
	
			$saldos = $query->orderBy('id_cierre', 'asc')->offset($start)->limit($length)->get();
			return response()->json([
				'data' => $saldos,
				'draw' => $draw,
				'recordsTotal' => $totalRecords,
				'recordsFiltered' => $recordsFiltered,
			], 200);
      
    }


}//clase
