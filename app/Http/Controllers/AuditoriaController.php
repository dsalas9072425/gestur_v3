<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Aerolinea;
use App\EstadoFactour;
use App\Producto;
use App\Persona;
use App\AuditoriaPersona;
use DB;
use Response;
use Image;
use Session;


class AuditoriaController extends Controller
{
	private function getIdEmpresa()
	{
  		return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}

  public function indexAuditoriaPersonas(Request $request)
  {
	$persona = DB::select('SELECT nombre, apellido, id FROM personas WHERE id = ?', [$request->input('id')]);
	//   $usuarios = Persona::where('activo','true')->selectRaw("*,concat(nombre||' ', apellido,'- '||denominacion_comercial ) as data_n")->orderBy('nombre', 'ASC')->get(['nombre','apellido','id','denominacion_comercial']);
	//   $usuarios = [];
	return view('pages.mc.auditoria.indexAuditoriaPersonas', compact('persona'));
  }

  public function getAuditoriaPersonas(Request $request)
  {
  	// VALIDACIONES
    // $rules = ['usuario' => 'required'];
	// $this->validate($request, $rules);

 
	$data = DB::table('vw_auditoria_persona');
	
			if(!empty($request->periodo))
			{
				$fechaTrim = trim($request->periodo);
          		$periodo = explode('-', trim($fechaTrim));
          		$desde = explode("/", trim($periodo[0]));
          		$hasta = explode("/", trim($periodo[1]));
          		$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
          		$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
          		$data = $data->whereBetween('fecha_hora_auditoria', [$fecha_desde, $fecha_hasta]);
			}

			if(!empty($request->evento))
			{
				$data = $data->where('evento','=', $request->evento);
			}

			if(!empty($request->usuario))
			{
				$data = $data->where('id','=', $request->usuario);
			}

			$data = $data->where('id_empresa','=', $this->getIdEmpresa());
			$data = $data->orderBy('fecha_hora_auditoria','DESC');
			$data = $data->get();

			// dd($data);

	return response()->json(['data'=>$data]);
  }

  public function indexAuditoriaProductos()
  {

    return view('pages.mc.auditoria.indexAuditoriaProductos');
  }

  public function getAuditoriaProductos(Request $request)
  {
  	// VALIDACIONES
    // $rules = ['id_producto' => 'required'];
	// $this->validate($request, $rules);

 
	$data = DB::table('vw_auditoria_producto');
	
			if(!empty($request->periodo))
			{
				$fechaTrim = trim($request->periodo);
          		$periodo = explode('-', trim($fechaTrim));
          		$desde = explode("/", trim($periodo[0]));
          		$hasta = explode("/", trim($periodo[1]));
          		$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
          		$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
          		$data = $data->whereBetween('fecha_hora_auditoria', [$fecha_desde, $fecha_hasta]);
			}

			if(!empty($request->evento))
			{
				$data = $data->where('evento', $request->evento);
			}

			if(!empty($request->id_producto))
			{
				$data = $data->where('id_producto', $request->id_producto);
			}

			$data = $data->where('id_empresa', $this->getIdEmpresa());
			$data = $data->orderBy('fecha_hora_auditoria','DESC');
			$data = $data->get();

			// dd($data);

	return response()->json(['data'=>$data]);
  }


  public function getProductoListado(Request $request){

	$q = strtoupper($request->q);
	$productos = Producto::where('id_empresa', $this->getIdEmpresa())
							->where(DB::raw('UPPER(denominacion)'),'like',"%$q%")
							->get(['id',DB::raw("CONCAT(tipo_producto,'-',denominacion) as text")]);
	return response()->json($productos);					
}	

 
}