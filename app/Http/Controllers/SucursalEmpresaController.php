<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\SucursalEmpresa;
use App\Empresa;
use App\Persona;
use DB;


class SucursalEmpresaController extends Controller
{
	public function index(Request $request){
		$sucursalEmpresa = SucursalEmpresa::where('activo', true)->get();
		return view('pages.mc.sucursalEmpresa.index')->with('sucursalEmpresas', $sucursalEmpresa);
	}	

	public function add(Request $request){
		$persona = Persona::where('activo', true)->get();
		$empresa = Empresa::where('activo', true)->get();
		return view('pages.mc.sucursalEmpresa.add')->with(['personas'=>$persona, 'empresas'=>$empresa]);
	}	

	/**
	 * Insertar en BD
	 */
	public function doAddSucursalEmpresa(Request $request){
		$empresa = new SucursalEmpresa;
		$empresa->denominacion = $request->input('denominacion');
		$empresa->ruc = $request->input('ruc');
		$empresa->direccion = $request->input('direccion');
		$empresa->id_encargado = $request->input('id_encargado');
		$empresa->telefono = $request->input('telefono');
		$empresa->telefono = $request->input('id_empresa');
		$empresa->activo = true;
		try{
			$empresa->save();
			flash('Se ha ingresado el Tipo Persona exitosamente')->success();
			return redirect()->route('sucursalEmpresas');
		} catch(\Exception $e){
			flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			return redirect()->back();
		}
	}

	public function edit(Request $request){
		$sucursalEmpresa = SucursalEmpresa::where('id', $request->input('id'))->get();
		return view('pages.mc.sucursalEmpresa.edit')->with('sucursalEmpresas', $sucursalEmpresa);
	}	

	public function doEditSucursalEmpresa(Request $request){
		DB::table('tipo_persona')
					    ->where('id',$request->input('id'))
					    ->update([
								'denominacion'=>$request->input('denominacion'),
					            ]);
		flash('Se ha editado el Tipo Persona exitosamente')->success();
		return redirect()->route('sucursalEmpresas');
	}	
	public function delete(Request $request){
		DB::table('tipo_persona')
					    ->where('id',$request->input('id'))
					    ->update([
								'activo'=>false,
					            ]);
		flash('Se ha eliminado exitosamente')->error();
		return redirect()->route('sucursalEmpresas');
	}	
}