<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Session;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\TipoTimbrado;
use App\Timbrado;
use App\SucursalEmpresa;
use App\FormaPagoCliente;
use App\CtaCtbFormaPago;
use App\Currency;
use App\BancoCabecera;
use App\PlanCuenta;
use App\BancoDetalle;

class FormaPagoClienteController extends Controller
{

	private function getIdEmpresa(){

	  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

	}//function

	public function index(Request $Request){

		$selectCuentaKey	= array_keys(FormaPagoCliente::SELECTCUENTA);// Muestre que cuentas se pueden parametrizar
		$formas_pagos = FormaPagoCliente::where('visible',true)->get()->map(function($fc) use($selectCuentaKey){
			$fc->select_cuenta = true;
		
			if(in_array( $fc->id, $selectCuentaKey)){
				$fc->select_cuenta = FormaPagoCliente::SELECTCUENTA[$fc->id];
			}

			return $fc;
		});

		return view('pages.mc.formaPagoCliente.index',compact('formas_pagos'));
	}	


	public function asignarCuentaContable(Request $request, $id){

		$formaPago = FormaPagoCliente::findOrFail($id);	

		$cttaCtte = CtaCtbFormaPago::with('moneda','planCuenta','banco.banco_cabecera')
									->where('id_empresa',$this->getIdEmpresa())
									->where('id_forma_pago',$id)
									->where('activo',true)
									->get();

		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$data_plans = PlanCuenta::where('activo',true)->where('asentable', true)
					  ->where('id_empresa', $this->getIdEmpresa())->get();

		$bancos = DB::table('banco_detalle as bd')
						->select('bd.*','cu.hb_desc as currency_code','tcb.denominacion','bc.nombre AS banco_n')
						->leftJoin('currency as cu','cu.currency_id','=','bd.id_moneda')
						->leftJoin('tipo_cuenta_banco as tcb','tcb.id','=','bd.id_tipo_cuenta')
						->leftJoin('banco_cabecera as bc', 'bc.id','=','bd.id_banco')
						->where('bc.id_empresa',$this->getIdEmpresa())
						->get();
						
		return view('pages.mc.formaPagoCliente.asignarCuenta')->with(['cttaCtte'=>$cttaCtte,'formaPago'=>$formaPago,'currency'=>$currency,'bancos'=>$bancos,'data_plans'=>$data_plans]);

	}	

	public function eliminarCuenta(Request $request){	
		$resp = new \StdClass; 
		try{

			$CtaCtbFormaPago = CtaCtbFormaPago::where('id_empresa',$this->getIdEmpresa())
						  ->findOrFail($request->input('idCtta'));

			$CtaCtbFormaPago->activo = false;
			$CtaCtbFormaPago->save();

			$resp->status = 'OK';
			$resp->mensaje = 'Se ha eliminado el registro';
		} catch(\Exception $e){
			Log::error($e);
			$resp->status = 'ERROR';
			$resp->mensaje = 'No se ha eliminado el registro';
		}
		return response()->json($resp);
	}

	public function getAsignarCtaCtte(Request $request){	
		$resp = new \StdClass; 

	 	 try{
			$cttaCtte = new CtaCtbFormaPago;
			$cttaCtte->id_forma_pago = $request->input('id');


			if($request->input('plan_contable')){
				$cttaCtte->id_cuenta_contable = $request->input('plan_contable');
			}else{
				throw new \Exception("Falta cuenta contable", 1);
				
			}

			if($request->input('banco')){
				$cttaCtte->id_cuenta_banco = $request->input('banco');
				$banco = BancoDetalle::findOrFail($request->input('banco'));
				$cttaCtte->id_moneda = $banco->id_moneda;

			}else{
				throw new \Exception("Falta cuenta de banco", 1);
				
			}

			
		$CtaCtbFormaPago = CtaCtbFormaPago::where('id_empresa',$this->getIdEmpresa())
		->where('id_forma_pago',$request->input('id'))
		->where('id_moneda',$banco->id_moneda)
		->where('activo', true)
		->first();

		if($CtaCtbFormaPago){
			throw new \Exception("Ya existe cuenta en esa moneda", 1);
		}

			$cttaCtte->id_empresa = $this->getIdEmpresa();
			$cttaCtte->save();


			$id = $cttaCtte->id;
			$resp->status = 'OK';
			$resp->id = $id;
			$resp->mensaje = 'Se ha agregado el registro';

	 	 } catch(\Exception $e){
			Log::error($e);
			$resp->status = 'ERROR';
			$resp->mensaje = $e->getMessage();
		} 

		return response()->json($resp);
	}	



}
