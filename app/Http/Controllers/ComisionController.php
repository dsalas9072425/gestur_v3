<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\ClientePersona;
use Session;
use Response;
use Image;
use DB;



class ComisionController extends Controller
{

	private function getIdUsuario()
	{
	 return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	}

	private function getIdEmpresa()
	{
	return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}

	

	public function altaClienteComision()
	{
		
			return view('pages.mc.comision.add');
	}


	public function ajaxAltaClienteComision(Request $req)
	{
		$save = true;
		$err = [];

		/**
		 * VERIFICAR EMAIL
		 */
			$correo = trim(strtoupper($req->correo));
			$err = DB::select("SELECT count(p.*) as c 
							  FROM personas_clientes p
							  WHERE UPPER(p.e_mail_cliente) = '".$correo."' 
							  AND p.activo = true ");
	
	
			$err = $err[0]->c;

			if(!$err){
				try{
					$data = new ClientePersona;
					$data->id_persona = $this->getIdUsuario();
					$data->e_mail_cliente = $req->correo;
					$data->documento_cliente = $req->documento;
					$data->celular = $req->telefono;
					$data->nombre_apellido = trim(strtoupper($req->nombre_apellido));
					$data->save();
	
				} catch(\Exception $e){
					$save = false;
				}//try
			}


			return response()->json(['count'=>$err, 'save'=>$save]);
	}

	public function getAjaxClienteComision(Request $req)
	{
			$data = new ClientePersona;
			$data = $data->with('vendedor');
			$data = $data->where('id_persona',$this->getIdUsuario());
			$data = $data->selectRaw("*,to_char(fecha_alta, 'DD/MM/YYYY HH:MM') AS fecha_alta_format");

			if($req->input('correo')){
				$data = $data->whereRaw("UPPER(e_mail_cliente) LIKE '%". strtoupper($req->input('correo'))."%'"); 
			}

			if($req->input('activo')){
				$data = $data->where('activo',$req->input('activo')); 
			}

			$data = $data->get();

					
			return response()->json(['data'=>$data]);
	}

	public function editCliente(Request $req)
	{
		$clientes = ClientePersona::where('id',$req->input('dataCliente'))->first();

		return response()->json($clientes);
	}	


	public function ajaxEditClienteComision(Request $req)
	{
		$save = true;
		$err = [];
		try{
				DB::table('personas_clientes')
					        ->where('id',$req->input('id'))
					        ->update([
					        		'documento_cliente'=>$req->input('documento'),
					            	'celular'=>$req->input('telefono'),
					            	'nombre_apellido'=>$req->input('nombre_apellido')
					            	]);
	 	} catch(\Exception $e){
				$save = false;
		}//try
		return response()->json(['count'=>$err, 'save'=>$save]);
	}


	public function anularCliente(Request $req)
	{
		$save = true;
		$err = [];
		try{
				DB::table('personas_clientes')
						        ->where('id',$req->input('dataCliente'))
						        ->update([
						            	'activo'=>false,
						            	]);			        
	  	} catch(\Exception $e){
				$save = false;
		}//try
		return response()->json(['save'=>$save]);


	}	


}