<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\DestinoDtpmundo;
use App\Currency;
use App\Aerolinea;
use App\EstadoFactour;
use App\Grupo;
use App\Producto;
use App\Persona;
use App\Factura;
use App\FacturaDetalle;
use DB;
use Response;
use Image; 
use Session;

class MigracionCompraController extends Controller
{

    private function getIdEmpresa()
      {
    
       return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
      }
    public function indexReporteCompra()
    {
      // 
      }

    public function createMigracionCompras()
    {
      $comprasMigradas = DB::select("
        SELECT substr(f.nro_factura,1,3)||''|| substr(f.nro_factura,5,3)||''||substr(f.nro_factura,9)||'-'|| fd.item as nro_factura,
          tf.denominacion, p.nombre
        FROM facturas f 
        JOIN tipo_factura tf ON tf.id = f.id_tipo_factura
        JOIN facturas_detalle fd ON fd.id_factura = f.id
        JOIN personas p ON p.id = f.cliente_id


        WHERE fd.migrado = false 
        AND f.id_empresa = ". Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);


      // dd($ventasMigradas);

      return view('pages.mc.reportes.migracionCompras', compact('comprasMigradas'));
     
    }

    public function getMigracionCompra(Request $request)
    {
      $librosCompras = DB::table('v_migracion_compras');
      $librosCompras = $librosCompras->where('id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

      if(!empty($request->input('periodo')))
      {
        $fechaTrim = trim($request->input('periodo'));
        $periodo = explode('-', trim($fechaTrim));
        $desde = explode("/", trim($periodo[0]));
        $hasta = explode("/", trim($periodo[1]));
        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0];
        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0];
        $librosCompras = $librosCompras->whereBetween('fecha_hora_facturacion', [$fecha_desde, $fecha_hasta]);
      // dd( $fecha_desde);
      }

      if(!empty($request->input('operacion')))
      {
        $librosCompras = $librosCompras->where('migrado_detalle','=', $request->input('operacion'));    
      }

      $librosCompras = $librosCompras->get();


      return response()->json($librosCompras);
    
    }

    public function edit($id)
    {
      // $ticket = Ticket::findOrFail($id);
      // $estados = EstadoFactour::where('id_tipo_estado', '8')->get();  
        
      // return view('pages.mc.tickets.edit', compact('ticket', 'estados'));
    }

    public function update(Request $request, $id)
    {
        // $ticket = Ticket::find($id);

        // try{
        //     $ticket->update([
        //     $ticket->pnr = $request->input('PNR'),
        //     $ticket->id_estado = $request->input('estado'),
        //     $ticket->numero_amadeus = $request->input('amadeus'),
        //   ]);

        //   flash('¡Se ha actualizado exitosamente!')->success();
        //   return redirect()->route('factour.indexTicket');

        // } catch(\Exception $e){
        //   flash('Ha ocurrido un error, inténtelo nuevamente')->error();
        //   return redirect()->back();

    }      
        
    
    public function hechaukaLc(Request $request)
    {

	  $proveedores = Persona::where('id_tipo_persona', '=', '14')
              ->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
              ->get();
  

      return view('pages.mc.contabilidad.hechaukaLc', compact('proveedores'));
    }  

       public function cabecerahechaukaLc(Request $request)
    {
   
      $cabecerahechauka = DB::table('vw_hechauka_lc_cabecera');
      $cabecerahechauka = $cabecerahechauka->where('id_empresa',$this->getIdEmpresa());
      $cabecerahechauka = $cabecerahechauka->get();
      return response()->json($cabecerahechauka);
    }  

      public function detallehechaukaLc(Request $request)
    {
     /* echo '<pre>';
      print_r($request->all()); */
      $cabecerahechauka = DB::table('vw_hechauka_lc_detalle');
      $cabecerahechauka = $cabecerahechauka->where('id_empresa',$this->getIdEmpresa());
      if($request->input('id_proveedor') != ''){
          $cabecerahechauka = $cabecerahechauka->where('id_proveedor',$request->input('id_proveedor'));
      }
      if($request->input('mes') != ''){
         if($request->input('anho')){
            $cabecerahechauka = $cabecerahechauka->where('mes', trim($request->input('mes')))->where('anho', $request->input('anho'));
         }
      }      $cabecerahechauka = $cabecerahechauka->get();

     /* echo '<pre>';
      print_r($cabecerahechauka);*/

      return response()->json(['data'=>$cabecerahechauka]);
    }  

}
