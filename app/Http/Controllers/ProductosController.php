<?php

namespace App\Http\Controllers;

use App\Producto;
use App\PlanCuenta;
use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Session;
use Redirect;
use App\Persona;

use Response;
use Image; 
use DB;


class ProductosController extends Controller
{


private function getIdUsuario(){
	
	 return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

}

private function getIdEmpresa(){

  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

}


public function index()
{
	$proveedores = Persona::where('activo',true)->where('id_tipo_persona',14)->orderBy('nombre','ASC')->get();
	$data_plan = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa', $this->getIdEmpresa())->get();
	return view('pages.mc.productos.index')->with(['data_plan'=>$data_plan,'proveedores'=>$proveedores]);
}


/**
 * VALIDA QUE UN DATO NO SEA ESPACIO O CERO
 * INSERTA NULL
 */
private function validNotEmpty($data){
	if($data){
		return $data;
	}
	return null;
}


public function guardarProducto(Request $req)
{
	// dd($req->all());
	$resp = new \StdClass;
	$resp->err = true;
    	try{
			if($req->input('id_plan_cuenta') != ""){
				$idplan = $req->input('id_plan_cuenta');
			}else{
				$idplan = null;
			}
			if($req->input('id_sucursal') != ""){
				$id_sucursal = $req->input('id_sucursal');
			}else{
				$id_sucursal = null;
			}
			if($req->input('id_plan_cuenta_venta') != ""){
				$id_plan_cuenta_venta = $req->input('id_plan_cuenta_venta');
			}else{
				$id_plan_cuenta_venta = null;
			}
			$producto = new Producto;
			$producto->id_empresa = $this->getIdEmpresa();
			$producto->created_at = date('Y-m-d H:i:00');
			$producto->denominacion = $req->input('denominacion');
			$producto->activo = $req->input('activo');
			$producto->id_grupos_producto = $req->input('grupo_producto');
			$producto->cta_contable_compra = $req->input('cuenta_contable');
			$producto->genera_voucher = $req->input('genera_voucher');
			$producto->descrip_voucher = $req->input('desc_voucher');
			$producto->tiene_dtplus = $req->input('tiene_dtplus');
			$producto->es_comisionable = $req->input('comisionable');
			$producto->comisiona_estando_solo = $req->input('comisiona_solo');
			$producto->comision_base = (float)$req->input('comision_base');
			$producto->frase_predeterminada = $req->input('frase_predet');
			$producto->origen_extranjero = $req->input('origen_extranjero');
			$producto->multiple_voucher = $req->input('multiple_voucher');
			$producto->precio_costo = (float)str_replace(',','.', str_replace('.','', $req->input('precio_costo')));
			$producto->precio_venta = (float)str_replace(',','.', str_replace('.','', $req->input('precio_venta')));
			$producto->id_proveedor = $this->validNotEmpty($req->input('id_proveedor'));
			$producto->porcentaje_gravada = (float)$req->input('porcentaje_gravada');
			$producto->imprimir_en_factura = $req->input('imprimir_factura');
			$producto->visible = $req->input('visible');
			$producto->renta_minima_para_comisionar = (integer)$req->input('minimo_comisionable');
			$producto->activo = $req->input('activo');
			$producto->tipo_producto = $req->input('tipo_producto');
			$producto->id_sucursal = $id_sucursal; 
			$producto->icono = $req->input('icono');
			//$producto->migrar = $req->input('migrar');
			$producto->id_plan_cuenta = $idplan;
			$producto->id_cuenta_contable_venta = $id_plan_cuenta_venta;
			$producto->woo_sku = $req->input('cod_producto');
			$producto->id_usuario_alta = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			$producto->id_usuario_update = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			$producto->pago_tc_detalle_proforma = $req->pago_tc_detalle_proforma;
			$producto->save();
		 } catch(\Exception $e){
			Log::error($e);
			$resp->err = false;
			if(env('APP_DEBUG')){
				$resp->e = $e;
			  }
		} 

	return  response()->json($resp);


}//function


public function actualizarProducto(Request $req){

	// dd($req->input('cod_producto'));
	$idProducto = $req->input('id');
	$resp = new \StdClass;
	$resp->err = true;

    	try{
			if($req->input('id_plan_cuenta') != ""){
				$idplan = $req->input('id_plan_cuenta');
			}else{
				$idplan = null;
			}
			if($req->input('id_sucursal') != ""){
				$id_sucursal = $req->input('id_sucursal');
			}else{
				$id_sucursal = null;
			}
			if($req->input('id_plan_cuenta_venta') != ""){
				$id_plan_cuenta_venta = $req->input('id_plan_cuenta_venta');
			}else{
				$id_plan_cuenta_venta = null;
			}

			$producto = Producto::find($idProducto);
			$producto->denominacion = $req->input('denominacion');
			$producto->activo = $req->input('activo');
			$producto->id_grupos_producto = $req->input('grupo_producto');
			$producto->cta_contable_compra = $req->input('cuenta_contable');
			$producto->genera_voucher = $req->input('genera_voucher');
			$producto->descrip_voucher = $req->input('desc_voucher');
			$producto->tiene_dtplus = $req->input('tiene_dtplus');
			$producto->es_comisionable = $req->input('comisionable');
			$producto->comisiona_estando_solo = $req->input('comisiona_solo');
			$producto->comision_base = (float)$req->input('comision_base');
			$producto->frase_predeterminada = $req->input('frase_predet');
			$producto->origen_extranjero = $req->input('origen_extranjero');
			$producto->porcentaje_gravada = (float)$req->input('porcentaje_gravada');
			$producto->imprimir_en_factura = $req->input('imprimir_factura');
			$producto->multiple_voucher = $req->input('multiple_voucher');
			$producto->id_sucursal = $id_sucursal;
			$producto->precio_costo = (float)str_replace(',','.', str_replace('.','', $req->input('precio_costo')));
			$producto->precio_venta = (float)str_replace(',','.', str_replace('.','', $req->input('precio_venta')));
			$producto->id_proveedor = $this->validNotEmpty($req->input('id_proveedor'));
			$producto->visible = $req->input('visible');
			$producto->activo = $req->input('activo');
			$producto->icono = $req->input('icono');
			$producto->tipo_producto = $req->input('tipo_producto');
			$producto->renta_minima_para_comisionar = (integer)$req->input('minimo_comisionable');
			//$producto->migrar = $req->input('migrar');
			$producto->id_plan_cuenta = $idplan;
			$producto->woo_sku = $req->input('cod_producto');
			$producto->id_cuenta_contable_venta = $id_plan_cuenta_venta;
			$producto->id_usuario_update = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			$producto->updated_at = date('Y-m-d H:i:00');
			$producto->pago_tc_detalle_proforma = $req->pago_tc_detalle_proforma;

			$producto->save();

 	   } catch(\Exception $e){
			Log::error($e);
			$resp->err = false;
			if(env('APP_DEBUG')){
				$resp->e = $e;
			  }
		} 

	return  response()->json($resp);

}

public function getDatosProductos(Request $req) {


	$datos = $this->responseFormatDatatable($req);
	// dd($datos->cod_producto);

		$productos = DB::table('vw_productos');
												
		$productos = $productos->where('id_empresa',$this->getIdEmpresa());
		if($datos->tiene_dtplus != ''){
			$productos = $productos->where('tiene_dtplus',$datos->tiene_dtplus);
		}
		if($datos->genera_voucher != ''){
			$productos = $productos->where('genera_voucher',$datos->genera_voucher);
		}

		if($datos->comisionable != ''){
			$productos = $productos->where('es_comisionable',$datos->comisionable);
		}
		if($datos->imprimir_en_factura != ''){
			$productos = $productos->where('imprimir_en_factura',$datos->imprimir_en_factura);
		}
		if($datos->activo != ''){
			$productos = $productos->where('activo',$datos->activo);
		}
		/*if($datos->num_cuenta != ''){
			$productos = $productos->where('cta_contable_compra',$datos->num_cuenta);
		}*/
		if($datos->denominacion != ''){
			/*echo '<pre>';
			print_r($datos->denominacion);*/
			$productos = $productos->where('denominacion', 'ilike','%'. $datos->denominacion.'%');
		}
		if($datos->tipo_producto != '')
		{
			$productos = $productos->where('tipo_producto',$datos->tipo_producto);
		}
		if($datos->cod_producto != '')
		{
			$productos = $productos->where('woo_sku', 'ilike','%'.$datos->cod_producto.'%');
		}

		if($datos->id_proveedor != '')
		{
			$productos = $productos->where('id_proveedor',$datos->id_proveedor);
		}

		if($datos->id_plan_cuenta_venta != '')
		{
			$productos = $productos->where('id_cuenta_contable_venta',$datos->id_plan_cuenta_venta);
		}

		if($datos->id_plan_cuenta != '')
		{
			$productos = $productos->where('id_plan_cuenta',$datos->id_plan_cuenta);
		}
		$productos = $productos->get();
		/*echo '<pre>';
		print_r($productos);*/

	foreach ($productos as $key => $value) {
		$value->btn = "<a href='".route('editProducto',['id' =>$value->id])."' class='btn btn-info' style='padding-left: 6px;padding-right: 6px;' role='button'><i class='fa fa-fw fa-edit'></i></a>";

				if($value->activo == true){
					$value->estado_val = 'SI';
				}else{
					$value->estado_val = 'NO';
				}
	}
	return response()->json(['data'=>$productos]);
	//return response()->json(['response'=>$productos]);
}

private function  responseFormatDatatable($req)
  	{
	    $datos = array();
	    $data =  new \StdClass;
	    
	    foreach ($req->formSearch as $key => $value) 
	    {
	     	$n = $value['name'];
	        $datos[$value['name']] = $value['value'];
	        $data-> $n = $value['value'];
	    }

	    return $data;
	 }

public function edit($id){

	$producto = Producto::where('id',$id)
						->where('id_empresa', $this->getIdEmpresa())
						->first();
	if(!isset($producto->id)){
		flash('El producto solicitado no existe. Intentelo nuevamente !!')->error();
		return redirect()->route('home');
	}
			
	$grupo_producto = DB::table('grupos_producto')->get();
	$data_plan = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa', $this->getIdEmpresa())->get();
	$proveedores = Persona::where('activo',true)
							->where('id_tipo_persona',14)
							->where('id_empresa', $this->getIdEmpresa())
							->orderBy('nombre','ASC')->get();
	$sucursales = Persona::where('id_tipo_persona','21')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();

	return view('pages.mc.productos.edit')->with(['sucursales'=>$sucursales,'producto'=>$producto,'grupo_producto'=>$grupo_producto,'id'=>$id, 'data_plan'=>$data_plan,'proveedores'=>$proveedores]);
	
}//function


public function add(){

	$grupo_producto = DB::table('grupos_producto')->get();
	$proveedores = Persona::where('activo',true)
							->where('id_tipo_persona',14)
							->where('id_empresa', $this->getIdEmpresa())
							->orderBy('nombre','ASC')->get();
	$data_plan = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa', $this->getIdEmpresa())->get();
	$sucursales = Persona::where('id_tipo_persona','21')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();

	return view('pages.mc.productos.add')->with(['sucursales'=>$sucursales, 'grupo_producto'=>$grupo_producto, 'data_plan'=>$data_plan,'proveedores'=>$proveedores]);
}//function








}