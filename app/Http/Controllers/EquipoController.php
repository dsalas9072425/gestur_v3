<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Session;
use Redirect;
use App\Proforma;
use App\ProformasDetalle;
use App\Divisas;
use App\OpTimbrado;
use App\CentroCosto;
use App\SucursalEmpresa;
use App\TipoDocumentoHechauka;
use App\Recibo;
use App\TipoFacturacion;
use Response;
use Image;
use DB;
use App\Equipo;
use App\EquipoDetalle;
use App\LibroVenta;
use App\Retencion;
use App\OrigenAsiento;
use App\TipoProveedor;
use Illuminate\Support\Facades\Log;

class EquipoController extends Controller
{
    private function getIdUsuario()
	{
 		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	}

	private function getIdEmpresa()
	{
 		return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}

    public function  add(Request $request){
		$vendedores = DB::select("
								SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
								concat(p.nombre||' ',p.apellido,' ','- '|| tp.denominacion) as full_data
								FROM personas p
								JOIN tipo_persona tp on tp.id = p.id_tipo_persona
								WHERE p.id_tipo_persona IN (2,4,3,5,6,7)
								AND p.activo = true
								AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

		return view('pages.mc.equipos.add', compact('vendedores'));
	 }//

	public function doAdd(Request $request){
		$equipos = new Equipo;
		$equipos->nombre_equipo = $request->input('nombre_equipo');
		$equipos->id_persona_responsable = $request->input('id_persona_responsable');
		$equipos->id_usuario_creacion = $this->getIdUsuario();
		$equipos->fecha_hora_creacion = date('Y-m-d H:m:s');
		$equipos->id_empresa = $this->getIdEmpresa();
		$equipos->activo = true;
		try{
			$equipos->save();
			$idEquipo = $equipos->id;
			foreach($request->input('detalle') as $key=>$detalle){
				$equipos = new EquipoDetalle;
				$equipos->id_equipo_cabecera = $idEquipo;
				$equipos->id_persona = $detalle['id'];
				$equipos->id_usuario_creacion = $this->getIdUsuario();
				$equipos->fecha_hora_creacion = date('Y-m-d H:m:s');
				$equipos->activo = true;
				$equipos->save();
			}
            flash('Se creo el Equipo')->success();
            return redirect()->route('reporteEquipo');
	 	} catch(\Exception $e){
			DB::rollBack();
	    	flash('El Equipo no se genero. Intentelo nuevamente !!')->error();
            return redirect()->back();
		} 
	}

    public function  edit(Request $request,$id){
		$equipo = Equipo::where('id',$id)->firstOrFail();

		$equipoDetalles = EquipoDetalle::with('vendedor')->where('id_equipo_cabecera',$id)->orderBy('id', 'ASC')->get();
	
		$vendedores =  DB::select("
									SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
									concat(p.nombre||' ',p.apellido,' ','- '|| tp.denominacion) as full_data
									FROM personas p
									JOIN tipo_persona tp on tp.id = p.id_tipo_persona
									WHERE p.id_tipo_persona IN (2,4,3,5,6,7)
									AND p.activo = true
									AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);


		return view('pages.mc.equipos.edit', compact('vendedores','equipo','equipoDetalles'));
	 }//

	 public function doEdit(Request $request)
	 {
		 try{
			 DB::table('equipo_cabecera')
				 ->where('id',$request->input('id'))
				 ->update([
						'nombre_equipo' =>$request->input('nombre_equipo'),
						'id_persona_responsable' =>$request->input('id_persona_responsable'),
						'activo' => $request->input('activo')
						 ]);

			EquipoDetalle::where('id_equipo_cabecera', $request->input('id'))->delete();			 
			foreach($request->input('detalle') as $key=>$detalle){
				$equipos = new EquipoDetalle;
				$equipos->id_equipo_cabecera = $request->input('id');
				$equipos->id_persona = $detalle['id'];
				$equipos->id_usuario_creacion = $this->getIdUsuario();
				$equipos->fecha_hora_creacion = date('Y-m-d H:m:s');
				$equipos->activo = true;
				$equipos->save();
			}
			 flash('Se edito el Equipo')->success();
			 return redirect()->route('reporteEquipo');
		 } catch(\Exception $e){
			 DB::rollBack();
			 flash('El Equipo no se ha editado. Intentelo nuevamente !!')->error();
			 return redirect()->back();
		 } 

	 }
 

	 public function  index(Request $request){
		$vendedores =  DB::select("
								SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
								concat(p.nombre||' ',p.apellido,' ','- '|| tp.denominacion) as full_data
								FROM personas p
								JOIN tipo_persona tp on tp.id = p.id_tipo_persona
								WHERE p.id_tipo_persona IN (2,4,3,5,6,7)
								AND p.activo = true
								AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

		return view('pages.mc.equipos.index', compact('vendedores'));
	 }//


	 public function  getEquipos(Request $request){
        $promociones = DB::table('vw_equipos');
        if($request->input('nombre_equipo') != ''){
            $promociones = $promociones->where('nombre_equipo', 'like', '%' .$request->input('nombre_equipo').'%');  
        }
		if($request->input('id_persona_responsable') != ''){
            $promociones = $promociones->where('id_persona_responsable', $request->input('id_persona_responsable'));  
        }
		$promociones = $promociones->where('activo',true);
		$promociones = $promociones->where('id_empresa',$this->getIdEmpresa());
		$promociones = $promociones->get();
        return response()->json($promociones);	
    }


	public function  delete(Request $request){
		$resp = new \StdClass;
		try{
			DB::table('equipo_cabecera')
				->where('id',$request->input('id_equipo'))
				->update([
					   		'activo' => false
						]);
			flash('Se Elimino el Equipo')->success();
			$resp->status = 'OK';
		} catch(\Exception $e){
			DB::rollBack();
			flash('El Equipo no se ha editado. Intentelo nuevamente !!')->error();
			$resp->status = 'ERROR';
		} 
		return response()->json($resp);
   }


}