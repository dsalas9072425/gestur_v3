<?php

namespace App\Http\Controllers;

use App\Parametros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
class ParametroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //utilizamos el index para poder listar los parametros tienen los filtros de busqued por clave
        if($request->buscar){
            $parametros = Parametros::where('clave','like', '%'.$request->buscar.'%' )
                                    ->orderBy("id", "desc")
                                    ->paginate(10);
    
            return view("pages.mc.configuraciones.listar")->with("parametros", $parametros);

      


        }else{
            
            $parametros = Parametros::orderBy("id", "desc")->paginate(10);
            
            return view("pages.mc.configuraciones.listar")->with("parametros", $parametros);

     

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view("pages.mc.configuraciones.nuevoParametro"); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parametro = new Parametros();
        $parametro->clave = $request->get('clave');
        $parametro->valor = $request->get('valor');
        $parametro->descripcion = $request->get('descripcion');
        $parametro->editable = $request->get('editable');
   
        $parametro->save();
        return redirect()->route('parametros.index')->with('mensaje', 'El registro fue exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parametro = Parametros::findOrFail($id);
        return view('pages.mc.configuraciones.editarParemtro')->with('parametro', $parametro);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $parametro = Parametros::findOrFail($id);
        $parametro->clave = $request->get('clave');
        $parametro->valor = $request->get('valor');
        $parametro->descripcion = $request->get('descripcion');
       
        return redirect()->route('parametros.index')->with('mensaje', 'La actualización fue exitosa');
        // Verificar si el campo editable es 'true' en la solicitud
        if ($request->get('editable') === 'true') {
            // Guardar los cambios
            $parametro->update();
      
        } else {
            // No realizar ninguna actualización, redirigir con mensaje de error
            return redirect()->route('parametros.index')->with('mensaje_error', 'No se realizó ninguna actualización');
        }
    }
    
    
    
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
