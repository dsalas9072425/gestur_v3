<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Currency;
use App\EstadoCuenta;
use App\Grupo;
use App\Producto;
use App\Persona;
use App\PlanCuenta;
use App\TipoTicket;
use App\CentroCosto;
use App\Ticket;
use App\LineaCredito;
use App\CuentaCorrienteBanco;
use App\BancoDetalle;
use App\BancoCabecera;
use App\TipoCuentaBanco;
use App\DepositoBancario;
use App\SucursalEmpresa;
use App\TransferenciasCuentas;
use App\AsientoDetalle;
use App\ChequeFp;
use App\EstadoFactour;
use App\TipoTransferencia;
use App\Empresa;
use Illuminate\Support\Facades\Log;
use Session;
use DB;
use Response;
use Image; 
use App\TarjetaPersona;

class BancosController extends Controller{



	public function index()
	{

		
        $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'indexBancos'");

	 	if(empty($permisos)){
	    	flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		} 



		$bancos = BancoCabecera::where('activo',true)->where('id_empresa',$this->getIdEmpresa())->where('cuenta_bancaria',true)->get();
		$cuentas = DB::table('banco_detalle as bd')
		->select('bd.*','cu.currency_code','tcb.denominacion','bc.nombre AS banco_n')
		->leftJoin('currency as cu','cu.currency_id','=','bd.id_moneda')
		->leftJoin('tipo_cuenta_banco as tcb','tcb.id','=','bd.id_tipo_cuenta')
		->leftJoin('banco_cabecera as bc', 'bc.id','=','bd.id_banco')
		->where('bc.id_empresa',$this->getIdEmpresa())
		//->where('bc.activo',true)
		->get();
	    //dd($cuentas);
		$tipo_op = DB::table('tipo_documento_ctacte')->get();
		$sucursales = DB::table('personas')->where('id_tipo_persona',21)->where('id_empresa',$this->getIdEmpresa())->get();
		$centro_costo = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get(); 
		$beneficiario = DB::table('personas')->where('activo',true)->where('id_empresa',$this->getIdEmpresa())->get();

		return view('pages.mc.banco.index', compact('bancos','cuentas','tipo_op','sucursales','centro_costo','beneficiario'));
	}//

	public function ajaxReporteCuentaBanco(Request $req)
	{

		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'indexBancos'");

	 	if(empty($permisos)){
	    	return response()->json(['data'=>[]]);
		} 


		$id_empresa = $this->getIdEmpresa();

		$cuenta_corriente_banco = DB::table('vw_reporte_cuenta');
		$cuenta_corriente_banco = $cuenta_corriente_banco->where('id_empresa',$id_empresa);
		$cuenta_corriente_banco = $cuenta_corriente_banco->select('*',
																  DB::raw("to_char(CURRENT_DATE,'DD/MM/YYYY') as fecha_data"),
															      DB::raw("'actual' as tipo_data"));
		if($req->input('id_cuenta_detalle')){
			$cuenta_corriente_banco = $cuenta_corriente_banco->where('id_banco_detalle',$req->input('id_cuenta_detalle'));
		}
		if($req->input('id_banco')){
			$cuenta_corriente_banco = $cuenta_corriente_banco->where('id_banco_cabecera',$req->input('id_banco'));
		}
		if($req->input('tipo_cuenta')){
			$cuenta_corriente_banco = $cuenta_corriente_banco->where('cuenta_bancaria',$req->input('tipo_cuenta'));
		}
		if($req->input('activo')){

			$cuenta_corriente_banco = $cuenta_corriente_banco->where('activo',$req->input('activo'));
		}

		$cuenta_corriente_banco = $cuenta_corriente_banco->get();
		// dd($cuenta_corriente_banco);
		
		return response()->json(['data'=>$cuenta_corriente_banco]);

	}


	public function anularTransferencia(Request $req)
	{
		// dd($req->input('id_transferencia'));

		$respuesta = '';
		$obj = new \StdClass; 
		$resp = new \StdClass; 
		$idUsuario = $this->getIdUsuario();

		$resp->err = true;
		$rsp = [];
		$query = '';
		$log_info = "(USERID:".$this->getIdUsuario()." METOD:anularTransferencia)".json_encode($req->all());
		$log_error = "(USERID:".$this->getIdUsuario()." METOD:anularTransferencia) ";

	try
	{		
		$query = DB::select('SELECT public."anular_transferencia"(?,?)', [$req->input('id_transferencia'), $idUsuario]);

		$respuesta = $query[0]->anular_transferencia;

		if ($respuesta != 'OK') 
		{
			$resp->err = false;
		}
				
		$resp->msj = $respuesta;
	}
	catch(\Exception $e)
	{
		Log::error($log_error);
		Log::error($e);

		$resp->err = false;
		if(env('APP_DEBUG')){
			$resp->e = $e;
		}
		DB::rollBack();
	}
		// dd($resp);
			
	return response()->json($resp);


	}

	public function reporteBanco(Request $req)
	{
		$id_empresa = $this->getIdEmpresa();
 
		$bancos = DB::table('vw_cuenta_corriente_bancos');
	
	 	$bancos = $bancos->where('id_empresa',$id_empresa);

		if($req->input('id_cuenta_detalle')){
			$bancos = $bancos->where('id_banco_detalle',$req->input('id_cuenta_detalle'));
		}

		// if ($req->input('anulado') == 0) 
		// {
		// 	$bancos = $bancos->where('activo', true);
		// }
	
		if($req->input('periodo_fecha_emision')){
			$fecha = explode(' ',$req->input('periodo_fecha_emision'));
			$desde = $fecha[0].' 00:00:00';
			$hasta = $fecha[1].' 23:59:59';
			$bancos = $bancos->whereBetween('fecha_emision', array($fecha[0].' 00:00:00',$fecha[1].' 23:59:59'));
		}
		if($req->input('fecha_hora_creacion')){
			$fecha = explode(' ',$req->input('fecha_hora_creacion'));
			$desde = $fecha[0].' 00:00:00';
			$hasta = $fecha[1].' 23:59:59';
			$bancos = $bancos->whereBetween('fecha_hora_creacion', array($fecha[0].' 00:00:00',$fecha[1].' 23:59:59'));
		}
		// if($req->input('periodo_fecha_anulacion')){
		// 	$fecha = explode(' ',$req->input('periodo_fecha_anulacion'));
		// 	$desde = $fecha[0].' 00:00:00';
		// 	$hasta = $fecha[1].' 23:59:59';
		// 	$bancos = $bancos->whereBetween('fecha_hora_anulacion', array($fecha[0].' 00:00:00',$fecha[1].' 23:59:59'));
		// }

		$bancos = $bancos->orderByRaw('fecha_emision ASC, fecha_hora_creacion ASC');

		$bancos = $bancos->get();

		// dd($bancos);
		return response()->json(['data'=>$bancos]);
	}//


	
	private function cotizarMonto($obj)
	{
		$rsp = new \StdClass; 
		$rsp->err = true;

		try{
			$rsp->result = DB::select('SELECT 
			get_monto_cotizacion_custom('.$obj->cotizacion.',
										'.$obj->monto.',
							   			'.$obj->moneda_costo.',
							   			'.$obj->moneda_venta.') AS total');
			} catch(\Exception $e){
				$rsp->err = false;
			}
		return $rsp;
	}

/** 
 * ==============================================================================================
 * 									ABM CUENTAS
 * ==============================================================================================
*/

	public function listadoCuenta()
	{
		$bancos = BancoCabecera::where('id_empresa',$this->getIdEmpresa())->get();
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa', $this->getIdEmpresa())->get();
		$monedas = Currency::where('activo', 'S')->orderBy('currency_code', 'DESC')->get();
		$tipo_cuenta = TipoCuentaBanco::where('activo','true')->get();

		return view('pages.mc.banco.listadoCuenta',compact('tipo_cuenta','monedas','cuentas_contables','bancos'));
	}

	public function ajaxListadoCuenta(Request $req)
	{
		$bancos = new BancoCabecera;
		$bancos = $bancos->with('usuario');
		$bancos = $bancos->where('id_empresa',$this->getIdEmpresa());
		$bancos = $bancos->selectRaw("*,to_char(fecha_creacion, 'DD/MM/YYYY') AS fecha_creacion_format");


		if($req->input('activo')){
			$bancos = $bancos->where('activo',$req->input('activo'));
		}
		$bancos = $bancos->get();

		return response()->json(['data'=>$bancos]);
	}

	public function getDataCuenta(Request $req)
	{	
		$bancos = BancoCabecera::with('usuario')
				  ->where('id',$req->input('id_cuenta'))
				  ->where('id_empresa',$this->getIdEmpresa())
				  ->get();

		return response()->json(['data'=>$bancos]);
	}

	public function setDataCuenta(Request $req)
	{	
		$resp = new \StdClass;
		$resp->err = true;

		// dd($req->all());

	  try{
		$banco = new BancoCabecera;

		if($req->input('id_cuenta')){

			$update = [
				"direccion" 		=> $req->input('direccion'),
				"telefonos" 		=> $req->input('telefono'),
				"activo" 			=> $req->input('activo'),
				"cuenta_bancaria" 	=> $req->input('cuenta_bancaria'),
				"nombre"	    	=> $req->input('nombre')
			];

			$banco = $banco->where('id',$req->input('id_cuenta'));
			$banco = $banco->update($update);

		} else {
			
			$banco->id_empresa = $this->getIdEmpresa();
			$banco->id_usuario = $this->getIdUsuario();
			$banco->fecha_creacion = date('Y-m-d H:i:00');
			$banco->direccion = $req->input('direccion');
			$banco->nombre = $req->input('nombre');
			$banco->telefonos = $req->input('telefono');
			$banco->activo = true;
			$banco->cuenta_bancaria = $req->input('cuenta_bancaria');
			$banco->save();	
		}



	   } catch(\Exception $e){
		$resp->err = false;
		if(env('APP_DEBUG')){
			$resp->e = $e;
		  }
	   }

		return response()->json($resp);
	}

	public function ajaxListadoCuentaDetalle(Request $req)
	{
		$bancos = new BancoDetalle; 
		/*$bancos = $bancos->whereHas('banco_cabecera', function($query){
			$query->where('id_empresa', $this->getIdEmpresa());
		});*/
		$bancos = $bancos->with('banco_cab','cuenta_contable','currency','tipo_cuenta');
		$bancos = $bancos->selectRaw("*,to_char(fecha_creacion, 'DD/MM/YYYY') AS fecha_creacion_format");

		if($req->input('id_banco')){
			$bancos = $bancos->where('id_banco',$req->input('id_banco'));
		}
		if($req->input('activo')){
			$bancos = $bancos->where('activo',$req->input('activo'));
		}
		$bancos = $bancos->get();

		$arrayBase = [];
		foreach($bancos as $key=>$banco)
		{
			if(isset($banco->banco_cab['id_empresa']))
			{
				if($banco->banco_cab['id_empresa'] == $this->getIdEmpresa()){
					$arrayBase[] = $banco;
				}
			}	
		}
		
		$bancos = $arrayBase;
		/*	echo '<pre>';
			print_r($bancos);*/

		return response()->json(['data'=>$bancos]);
	}

	public function getDataCuentaDetalle(Request $req)
	{	
		$bancos = new \StdClass;
		$bancoDetalle = BancoDetalle::with('banco_cabecera','cuenta_contable','currency','tipo_cuenta')
				  ->where('id',$req->input('id_cuenta_detalle'))
				  ->first();
			
		return response()->json(['data'=>$bancoDetalle]);
	}

	public function setDataCuentaDetalle(Request $req)
	{	
		$resp = new \StdClass;
		$resp->err = true;

		//dd($req->all());

	  try{
		$banco = new BancoDetalle;

		if($req->input('id_cuenta_detalle')){

			$update = [
				"numero_cuenta" 	=> $req->input('nro_cuenta'),
				"id_moneda" 		=> (integer)$req->input('id_moneda'),
				"id_tipo_cuenta" 	=> (integer)$req->input('id_tipo_cuenta'),
				"tarjeta_debito" 	=> $req->input('tarjeta_debido'),
				"monto_sobregiro"	=> (float)$req->input('sobregiro'),
				"chequera"			=> $req->input('chequera'),
				"id_cuenta_contable"=> (integer)$req->input('id_cuenta_contable'),
				"activo"	    	=> $req->input('activo')
			];

			$banco = $banco->where('id',$req->input('id_cuenta_detalle'));
			$banco = $banco->update($update);
			$cuenta_detalle = $req->input('id_cuenta_detalle');

		} else {
			
			$banco->id_usuario 			= $this->getIdUsuario();
			$banco->fecha_creacion 		= date('Y-m-d H:i:00');
			$banco->numero_cuenta 		= $req->input('nro_cuenta');
			$banco->id_moneda 			= (integer)$req->input('id_moneda');
			$banco->id_banco			= (integer)$req->input('id_banco');
			$banco->tarjeta_debido 		= $req->input('tarjeta_debido');
			$banco->monto_sobregiro 	= (float)$req->input('sobregiro');
			$banco->chequera 			= $req->input('chequera');
			$banco->id_tipo_cuenta 		= (integer)$req->input('id_tipo_cuenta');
			$banco->id_cuenta_contable 	= (integer)$req->input('id_cuenta_contable');
			$banco->saldo_inicial		= (float)$req->input('saldoinicial');
			$banco->saldo		= (float)$req->input('saldoinicial');
			$banco->activo 				= true;
			$banco->save();	
			$cuenta_detalle = $banco->id;

			DB::select('SELECT public."registrar_movimiento_cta_cte_bancos"(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
			          [ 'Saldo Inicial de Cuenta',                                                
			            (float)$req->input('saldoinicial'), 
			            $cuenta_detalle, 
			            null,//id_grupo
			            null,
			            null,
			            Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia,
			            (integer)$req->input('id_cuenta_contable'), 
			            Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia, 
			            null,
			            0,
			            Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario, 
			            24,
			            null
			            ]);
		}



	  } catch(\Exception $e){
		$resp->err = false;
		if(env('APP_DEBUG')){
			$resp->e = $e;
		  }
	   }

		return response()->json($resp);
	}


		/** 
 * ==============================================================================================
 * 									ABM TIPO CUENTAS
 * ==============================================================================================
 */

	public function listadoTipoCuenta()
	{
		return view('pages.mc.banco.listadoTipoCuenta');
	}

	public function ajaxListadoTipoCuenta(Request $req)
	{
		$tipoCuentaBanco = new TipoCuentaBanco;
		$tipoCuentaBanco = $tipoCuentaBanco->with('usuario');
		$tipoCuentaBanco = $tipoCuentaBanco->selectRaw("*,to_char(fecha_creacion, 'DD/MM/YYYY') AS fecha_creacion_format");

		if($req->input('activo')){
			$tipoCuentaBanco = $tipoCuentaBanco->where('activo',$req->input('activo'));
		}
		$tipoCuentaBanco = $tipoCuentaBanco->get();

		return response()->json(['data'=>$tipoCuentaBanco]);
	}

	public function getDataTipoCuenta(Request $req)
	{	
		$tipoCuentaBanco = TipoCuentaBanco::with('usuario')
				->where('id',$req->input('id_tipo_cuenta'))
				->get();

		return response()->json(['data'=>$tipoCuentaBanco]);
	}

	public function setDataTipoCuenta(Request $req)
	{	
		$resp = new \StdClass;
		$resp->err = true;

		// dd($req->all());

			try{
				$banco = new TipoCuentaBanco;

				if($req->input('id_cuenta')){

					$update = [
						"activo" 			=> $req->input('activo'),
						"denominacion"	    => $req->input('nombre')
					];

					$banco = $banco->where('id',$req->input('id_cuenta'));
					$banco = $banco->update($update);

				} else {
					$banco->id_usuario = $this->getIdUsuario();
					$banco->fecha_creacion = date('Y-m-d H:i:00');
					$banco->denominacion = $req->input('nombre');
					$banco->activo = true;
					$banco->save();	
				}

			} catch(\Exception $e){
				$resp->err = false;
				if(env('APP_DEBUG')){
					$resp->e = $e;
				}
			}

				return response()->json($resp);
	}



/** 
 * ==============================================================================================
 * 									TRANSFERENCIAS
 * ==============================================================================================
 */

	public function getAsientoDetalles(Request $req)
	{
		$idDetalle = str_replace("row","",$req->input('idDetalle'));

		$asientoDetalle = AsientoDetalle::where('id',$idDetalle)->firstOrFail();

		$cotizacion = $asientoDetalle->cotizacion;

		return response()->json(['cotizacion'=>$cotizacion]);

	}

	public function generarMovimientoBancario(Request $req)
	{

		$result = [];
		$idEmpresa = $this->getIdEmpresa();
		$idUsuario = $this->getIdUsuario();
		$resp_transferencia = '';

		$cotizacion = $req->input('cotizacion');
		$obj = new \StdClass; 
		$resp = new \StdClass; 
		$resp->err = true;
		$rsp = [];
		$query = '';
		$log_info = "(USERID:".$this->getIdUsuario()." METOD:generarMovimientoBancario)".json_encode($req->all());
		$log_error = "(USERID:".$this->getIdUsuario()." METOD:generarMovimientoBancario) ";

		$chequeId = 0;

		if ($req->input('id_tipo_movimiento') == 1) 
		{
			$es_deposito = true;
		}
		else
		{
			$es_deposito = false;
		}

		/*echo '<pre>';
		print_r($req->input('imagen'));*/

			//try{

				if($req->input('tipo_operacion') == '4'){
					$idUsuario = (Integer)Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	                $idEmpresa = $this->getIdEmpresa();
	                $cheque = new ChequeFp;
	                $cheque->id_beneficiario = ($req->input('al_portador')) ?  null  : 0;
	                $cheque->id_cuenta = (Integer)$req->input('alBancoCuenta');
	                $cheque->nro_cheque = $req->input('num_cheque');
	                $cheque->id_moneda = (Integer)$req->input('id_moneda');
	                $cheque->importe = (float)$req->input('importe');
	                $cheque->cotizacion = (float)$cotizacion;
	                $cheque->fecha_emision = $req->input('fecha_emision');
	                $cheque->fecha_vencimiento =$req->input('fecha_emision_ch');
	                $cheque->id_usuario = $idUsuario;
	                $cheque->id_empresa = $idEmpresa;
	                $cheque->id_tipo_cheque = 3;
	                $cheque->id_op = (Integer)0;
	                $cheque->emisor_txt = $req->input('beneficiario');
	                $cheque->al_portador = $req->input('al_portador');
	                $cheque->save();
	                $chequeId =  $cheque->id;
	                $resp->chequeId =  $chequeId;
	            }   

	     		Log::info($log_info);
				$alBanco = BancoDetalle::where('id',$req->input('alBancoCuenta'))->firstOrFail();

			$base_nro = DB::select("SELECT public.get_documento(".$idEmpresa.",'TRANSFERENCIA')");
			$nro_transferencia = $base_nro[0]->get_documento;

 			$respuesta = DB::select('SELECT public."generar_movimiento_bancario"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
				[ (integer)$req->input('alBancoCuenta'),
				(integer)$req->input('id_cuenta_contable'),
				(string)$req->input('nro_comprobante'),
				(integer)$req->input('id_sucursal'),
					(integer)$req->input('id_centro_costo'),
					$req->input('fecha_emision'),
				(float)$req->input('importe'),
				(integer)$cotizacion,
				(string)$req->input('concepto'),
				$idUsuario,
				$idEmpresa,
				$chequeId,
				$es_deposito,
				$req->input('imagen'),
				(integer)$nro_transferencia
				]);

				$resp_transferencia = $respuesta[0]->generar_movimiento_bancario;

				if ($resp_transferencia != 'OK') 
				{
					$resp->err = false;
				}
				
				$resp->msj = $resp_transferencia;
					
			/*} catch(\Exception $e){
					Log::error($log_error.$e);

					$resp->err = false;
					if(env('APP_DEBUG')){
						$resp->e = $e;
					}
					DB::rollBack();
			}*/
			
			return response()->json($resp);


	}

	public function getCotizacion($moneda){

		$getCotizacion=DB::select('SELECT public."get_cotizacion"('. $moneda.', '.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.','.Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.')');
		
		return $getCotizacion[0]->get_cotizacion;
	}

	public function setTransferencia(Request $req)
	{

		$result = [];
		$idEmpresa = $this->getIdEmpresa();
		// dd($idEmpresa);
		$idUsuario = $this->getIdUsuario();
		$resp_transferencia = '';
		$obj = new \StdClass; 
		$resp = new \StdClass; 
		$resp->err = true;
		$rsp = [];
		$query = '';
		$log_info = "(USERID:".$this->getIdUsuario()." METOD:setTransferencia)".json_encode($req->all());
		$log_error = "(USERID:".$this->getIdUsuario()." METOD:setTransferencia) ";
		$alBanco = BancoDetalle::where('id',$req->input('alBancoCuenta'))->firstOrFail();
		$delBanco = BancoDetalle::where('id',$req->input('delBancoCuenta'))->firstOrFail();

		$chequeId = 0;

		$cotizacion = ($req->input('cotizacion')) ? (float)$req->input('cotizacion') : $this->getCotizacion((Integer)$req->input('id_moneda'));

		if($alBanco->id_moneda != $delBanco->id_moneda && !$cotizacion){
			$resp->err = false;
			$resp->msj = 'Se debe cargar la cotización en el sistema para continuar.';
			return response()->json($resp);
		}

		 try{
				if($req->input('tipo_operacion') == '4'){
					$idUsuario = (Integer)Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	                $idEmpresa = $this->getIdEmpresa();
	                $cheque = new ChequeFp;
	                $cheque->id_beneficiario = ($req->input('al_portador')) ?  null  : 0;
	                $cheque->id_cuenta = (Integer)$req->input('alBancoCuenta');
	                $cheque->nro_cheque = $req->input('num_cheque');
	                $cheque->id_moneda = (Integer)$req->input('id_moneda');
	                $cheque->importe = (float)$req->input('importe');
	                $cheque->cotizacion = (float)$cotizacion;
	                $cheque->fecha_emision = $req->input('fecha_emision');
	                $cheque->fecha_vencimiento =$req->input('fecha_emision_ch');
	                $cheque->id_usuario = $idUsuario;
	                $cheque->id_empresa = $idEmpresa;
	                $cheque->id_tipo_cheque = 3;
	                $cheque->id_op = (Integer)0;
	                $cheque->emisor_txt = $req->input('beneficiario');
	                $cheque->al_portador = $req->input('al_portador');
	                $cheque->save();
	                $chequeId =  $cheque->id;
	                $resp->chequeId =  $chequeId;
	            }   

	     		Log::info($log_info);
				
				$base_nro = DB::select("SELECT public.get_documento(".$idEmpresa.",'TRANSFERENCIA')");
				$nro_transferencia = $base_nro[0]->get_documento;
				$concepto = 'OM '.$nro_transferencia." ".$req->input('concepto');

				// TRANSFERENCIA DEL BANCO delBancoNumOperacion
				$query = "SELECT * FROM transferir_entre_cuentas(";
				$query .=(integer)$req->input('delBancoCuenta').",";
				$query .="'".$req->input('delBancoNumOperacion')."',";
				$query .=(integer)$req->input('alBancoCuenta').",";
				$query .="'',";
				$query .=(integer)$req->input('id_sucursal').",";
				$query .=(integer)$req->input('id_centro_costo').",";
				$query .="'".$req->input('fecha_emision')."',";
				$query .=(float)$req->input('importe').",";
				$query .=(float)$cotizacion.",";
				$query .="'".$concepto."',";
				$query .=$idUsuario.",";
				$query .=$idEmpresa.",";
				$query .=$chequeId.",";
				$query .="'".$req->input('imagen')."',";
				$query .=$nro_transferencia;
				$query .=") AS t";

					DB::beginTransaction();

					$resp_transferencia = DB::select($query)[0]->t;
					if(!preg_match('/OK/i', $resp_transferencia))
					{
						$resp->err = false;
					}else{
						$resp_transferencia = 'OK '.$nro_transferencia;
					}	
					$resp->msj = $resp_transferencia;
					DB::commit();
					
				 } catch(\Exception $e){
					Log::error($log_error.$e);

					$resp->err = false;
					if(env('APP_DEBUG')){
						$resp->e = $e;
					}
					DB::rollBack();
				}
				return response()->json($resp);
	}


	public function reporteTransferencia()
	{
		$cuentas = DB::table('banco_detalle as bd')
		->select('bd.*','cu.currency_code','tcb.denominacion','bc.nombre AS banco_n','bd.numero_cuenta')
		->leftJoin('currency as cu','cu.currency_id','=','bd.id_moneda')
		->leftJoin('tipo_cuenta_banco as tcb','tcb.id','=','bd.id_tipo_cuenta')
		->leftJoin('banco_cabecera as bc', 'bc.id','=','bd.id_banco')
		->where('bc.id_empresa',$this->getIdEmpresa())
		->where('bc.activo',true)
		->get();

		$estados = EstadoFactour::where('id_tipo_estado','14')->where('visible',true)->orderBy('denominacion','ASC')->get();

		$monedas = Currency::where('activo', 'S')->orderBy('currency_code', 'DESC')->get();	

		$tipoTransferencias = TipoTransferencia::all();

		$permiso_modificar_transferencia = $this->datosPermiso('reporteTransferencia',64);

		return view('pages.mc.banco.reporteTransferencia',compact('cuentas','monedas','estados','tipoTransferencias','permiso_modificar_transferencia'));
	}

	public function ajaxReporteTransferenciaCuenta(Request $req)
	{

		$data = DB::table('vw_reporte_transferencia');
		$data = $data->where('id_empresa',$this->getIdEmpresa());
		

		if($req->input('id_cuenta_origen')){
			$data = $data->where('id_cuenta_banco_origen',$req->input('id_cuenta_origen'));
		}

		if($req->input('id_cuenta_destino')){ 
			$data = $data->where('id_cuenta_banco_destino',$req->input('id_cuenta_destino'));
		}
		if($req->input('cuenta_bancaria')){ 
			$data = $data->where('cuenta_bancaria',$req->input('cuenta_bancaria'));
		}

		if($req->input('fecha_emision')){
			//$data = $data->where('fecha_transferencia',$req->input('fecha_emision')); 
			$fecha = explode('-', $req->input('fecha_emision'));
			$desde = $this->formatoFechaEntrada(trim($fecha[0]));
			$hasta = $this->formatoFechaEntrada(trim($fecha[1]));
			$data = $data->whereBetween('fecha_transferencia', array(
														date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
														date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
													));
		}
		if($req->input('fecha_proceso')){
			$fecha = explode('-', $req->input('fecha_proceso'));
			$desde = $this->formatoFechaEntrada(trim($fecha[0]));
			$hasta = $this->formatoFechaEntrada(trim($fecha[1]));
			$data = $data->whereBetween('fecha_hora_procesado', array(
														date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
														date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
													));
		}

		if($req->input('fecha_proceso')){
			$fecha = explode('-', $req->input('fecha_proceso'));
			$desde = $this->formatoFechaEntrada(trim($fecha[0]));
			$hasta = $this->formatoFechaEntrada(trim($fecha[1]));
			$data = $data->whereBetween('fecha_hora_procesado', array(
														date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
														date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
													));
		}

		if($req->input('id_estado_proforma')!== null){
			$data = $data->whereIn('id_estado',$req->input('id_estado_proforma'));
		}
		
		if($req->input('delBancoNumOperacion')){
			$data = $data->where('nro_transferencia',$req->input('delBancoNumOperacion'));
		}
		
		if($req->input('tipo_transferencia')){
			$data = $data->where('tipo_transferencia_id',$req->input('tipo_transferencia'));
		}

		$data = $data->orderBy('id','DESC');
		$data = $data->get();

		$btn = DB::select("SELECT pe.* 
                        FROM persona_permiso_especiales p, permisos_especiales pe
                        WHERE p.id_permiso = pe.id 
                        AND p.id_persona = ".$this->getIdUsuario() ." 
                        AND pe.url = 'reporteTransferencia' ");
	
		foreach ($data as $key => $value) {
		    if(!empty($btn)){
				$data[$key]->permiso = 1;
			}else{
				$data[$key]->permiso = 0;	
			}
		}
		return response()->json(['data'=>$data]);
	}

	public function pdfTransferencia(Request $req, $id)
	{

		$data = DB::table('vw_reporte_transferencia');
		$data = $data->where('id_empresa',$this->getIdEmpresa());
		$data = $data->where('id',$id);
		$data = $data->orderBy('id','DESC');
		$data = $data->get();
		$empresa = Empresa::where('id',$this->getIdEmpresa())->first();
		
		$asiento_detalles = DB::table("asientos_contables_detalle as acd");
        $asiento_detalles = $asiento_detalles->select("acd.*", 
                       DB::raw("CONCAT(lc.nro_documento,lv.nro_documento) as nro_factura"),
                       "ac.concepto",
                      "pc.descripcion as cuenta_n",
                      "pc.cod_txt as num_cuenta",
                      "s.denominacion as sucursal",
                      "acd.id_sucursal",
                      "acd.id_centro_costo",
					  "ac.id as id_asiento",
                       DB::raw("CONCAT(cc.nombre, ' ') AS centro_costo"));
        $asiento_detalles = $asiento_detalles->leftJoin('plan_cuentas as pc', 'pc.id', '=','acd.id_cuenta_contable' );
        $asiento_detalles = $asiento_detalles->leftJoin('asientos_contables as ac', 'ac.id', '=','acd.id_asiento_contable' );
        $asiento_detalles = $asiento_detalles->leftJoin('libros_compras as lc', 'lc.id_asiento', '=','ac.id' );
        $asiento_detalles = $asiento_detalles->leftJoin('libros_ventas as lv', 'lv.id_asiento', '=','ac.id' );
        $asiento_detalles = $asiento_detalles->leftJoin('sucursales_empresa as s', 's.id', '=','acd.id_sucursal' );
        $asiento_detalles = $asiento_detalles->leftJoin('centro_costo as cc', 'cc.id', '=','acd.id_centro_costo' );
        $asiento_detalles = $asiento_detalles->where('acd.activo', true);
        $asiento_detalles = $asiento_detalles->orderBy('acd.debe','desc');
        $asiento_detalles = $asiento_detalles->where('acd.id_asiento_contable',$data[0]->id_asiento);
        $asiento_detalles = $asiento_detalles->get(); 

		$logoSucursal = DB::select('SELECT p.logo FROM sucursales_empresa s, personas p WHERE s.id_persona = p.id AND s.id ='.$data[0]->id_sucursal);
		
		if(isset($logoSucursal[0]) && $logoSucursal[0]->logo != "" && $logoSucursal[0]->logo != 'factour.png'){
			$baseLogo = $logoSucursal[0]->logo;
			$logo = asset("personasLogo/$baseLogo");
		  }else{
			$logo = asset("logoEmpresa/$empresa->logo");
		  }


		DB::table('transferencias_cuentas')
							->where('id',$id)
							->update([
								"fecha_descarga"=> date('Y-m-d H:m:s'),
								"id_usuario_descarga"=> $this->getIdUsuario()
							]);
		$pdf = \PDF::loadView('pages.mc.banco.pdfTransferencia',compact('data','empresa','asiento_detalles','logo'));

		$pdf->setPaper('a4', 'letter')->setWarnings(false);
		return $pdf->download('Detalles de OP'.date('d/m/Y').'.pdf');
		//return view('pages.mc.banco.pdfTransferencia',compact('data','empresa','asiento_detalles'));
	}

	public function cotizarMontoTransferencia(Request $req)
	{
		$obj = new \StdClass; 
			$obj->cotizacion = (float)$req->input('cotizacion');
			$obj->monto = (float)$req->input('monto');
			$obj->moneda_costo = (integer)$req->input('moneda_costo');
			$obj->moneda_venta = (integer)$req->input('moneda_venta');
			$result = $this->cotizarMonto($obj);
		// dd($result);
		return response()->json($result);
	}//

	public function registrarMovimiento(Request $req)
	{

		// dd($req->all());
		$err = true;
		$idUsuario = (Integer)Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;


		try {
		$registrar = DB::SELECT("SELECT * FROM registrar_movimiento_cta_cte_bancos(
		'".$req->input('concepto')."',
		".(float)$req->input('importe').",
		".(Integer)$req->input('id_banco_detalle').",
		null,
		null,
		'".$req->input('fecha_emision')."',
		'".$req->input('fecha_vencimiento')."',
		".(Integer)$req->input('id_sucursal').",
		".(Integer)$req->input('id_cuenta_contable').",
		".(Integer)$req->input('id_centro_costo').",
		".(Integer)$req->input('id_beneficiario').",
		".(float)$req->input('cotizacion').",
		".(Integer)$idUsuario.",
		".(Integer)$req->input('id_tipo_documento').",
		null)");

		} catch(\Exception $e){
			$err = false;
		}
			return response()->json(['err'=>$err]);

	}//


	public function transferenciaCuenta()
	{

		$centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->orderBy('nombre','ASC')->get(); 
		$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())
		->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
		$bancoDetalle = BancoDetalle::whereHas('banco_cab',function($query){
			$query->where('id_empresa',$this->getIdEmpresa());
			//$query->where('cuenta_bancaria',true);
			$query->where('activo',true);
		})->with('banco_cab','currency','tipo_cuenta')->get();

		/*echo '<pre>';
		print_r($bancoDetalle);*/

		$currency = Currency::where('activo', 'S')->orderBy('currency_code', 'DESC')->get();	


		return view('pages.mc.banco.transferenciaCuentas',compact('centro','sucursalEmpresa','bancoDetalle','currency'));
	}


	public function indexMovimientoBancario()
	{

		$centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->orderBy('nombre','ASC')->get(); 
		$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())
		->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
		$bancoDetalle = BancoDetalle::whereHas('banco_cab',function($query){
			$query->where('id_empresa',$this->getIdEmpresa());
			//$query->where('cuenta_bancaria',true);
			$query->where('activo',true);
		})->with('banco_cab','currency','tipo_cuenta')->get();

		$cuentasContables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();

		/*echo '<pre>';
		print_r($bancoDetalle);*/

		$currency = Currency::where('activo', 'S')->orderBy('currency_code', 'DESC')->get();	


		return view('pages.mc.banco.movimientoBancario',compact('centro','sucursalEmpresa','bancoDetalle','currency', 'cuentasContables'));
	}
/** 
 * ==============================================================================================
 * 									METODOS AUXILIARES
 * ==============================================================================================
 */

	private function getIdUsuario()
	{
		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	}
  
	  private function getIdEmpresa()
	  {
	   return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	  }


 public function btnPermisos($parametros, $vista){

  	$btn = DB::select("SELECT pe.* 
                        FROM persona_permiso_especiales p, permisos_especiales pe
                        WHERE p.id_permiso = pe.id 
                        AND p.id_persona = ".$this->getIdUsuario() ." 
                        AND pe.url = '".$vista."' ");

  $htmlBtn = '';
  $boton =  array();
  $idParametro = '';

 // dd( $btn );

  if(!empty($btn)){

  foreach ($btn as $key => $value) {

    $idParametro = '';
    $ruta = $value->accion;
     $htmlBtn = '';



     //LLEVA PARAMETRO
    if($value->lleva_parametro == '1'){

    foreach ($parametros as $indice=>$valor) { 

      if($indice == $value->nombre_parametro){
        $idParametro = $valor;
      }
    }

    //PARAMETRO OCULTO EN DATA
    if($value->parametro_oculto == '1'){
       $htmlBtn = "<a  class='".$value->clase."' data-btn='".$idParametro."'   title='".$value->titulo."'>
      	".$value->titulo."
       <i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    } else {
       $htmlBtn = "<a class='".$value->clase."' href='".route($ruta,['id'=>$idParametro])."'    title='".$value->titulo."'><i class='".$value->icono."'>
      
       </i> ".$value->titulo." </a>";  
    }

    } else {
      $htmlBtn = "<a  href='#' class='".$value->clase."'   title='".$value->titulo."'>
      
      <i class='".$value->icono."'></i>".$value->texto_alternativo." </a>"; 
    }

    
    $boton[] = $htmlBtn;
  }
   return $boton;
 
   } else {
    return array();
   }



}//function


	public function getModificacionesTransferencias(Request $request)
	{
		$resp = new \StdClass;
		$resp->err = false;
		if($request->input('dataTipo') == 1){
			$esActualizar = 'true';
		} else {
			$esActualizar = 'false';
		}
		try{
				$transferenciasCuentas = TransferenciasCuentas::where('id',$request->input('dataId'))->first(); 
				// TRANSFERENCIA DEL BANCO
				
				$query = "SELECT * FROM procesar_transferencia_cuentas(";
				$query .=(integer)$transferenciasCuentas->id_cuenta_banco_origen.",";
				$query .="'".$transferenciasCuentas->nro_comprobante_origen."',";
				$query .=(integer)$transferenciasCuentas->id_cuenta_banco_destino.",";
				$query .="'".$transferenciasCuentas->nro_comprobante_destino."',";
				$query .=(integer)$transferenciasCuentas->id_sucursal.",";
				$query .=(integer)$transferenciasCuentas->id_centro_costo.",";
				$query .="'".$transferenciasCuentas->fecha_transferencia."',";
				$query .=(float)$transferenciasCuentas->importe.",";
				$query .=(float)$transferenciasCuentas->cotizacion.",";
				$query .="'".$transferenciasCuentas->concepto."',";
				$query .=$transferenciasCuentas->id_usuario.",";
				$query .=$transferenciasCuentas->id_empresa.",";
				$query .=$request->input('dataId').",";
				$query .=$esActualizar;
				$query .=") AS t";
	
				$resp_transferencia = DB::select($query)[0]->t;	

				if($resp_transferencia == 'OK'){
					if($request->input('dataTipo') == 1){
						$resp->mensaje = 'Se autorizó la transferencia';
					} else {
						$resp->mensaje = 'Se rechazó la transferencia';
					}
				}else{
					$resp->err = true;	
					$resp->mensaje =$resp_transferencia;
				}
			} catch(\Exception $e){
				Log::error($e);
				$resp->err = true;
				if(env('APP_DEBUG')){
					$resp->e = $e;
				}
			}
			return response()->json($resp);
	}	

	public function getConceptos(Request $request){

		$data = DB::table('vw_reporte_transferencia');
		$data = $data->where('id',$request->input('id_movimiento'));
		$data = $data->get();
		return response()->json($data);
	}

	public function editarMovimiento(Request $request){

		
			
		try{

			$resp = new \StdClass;
			$fecha = explode('/',$request->input('fecha_creacion'));
			$fechaCreacion = $fecha[2].'-'.$fecha[1].'-'.$fecha[0].' '.date('H:m:s');

			$transferencia = TransferenciasCuentas::find($request->input('idMovimiento'));
			$transferencia->fecha_hora = $fechaCreacion;
			$transferencia->fecha_transferencia = $fechaCreacion;
			$transferencia->concepto = $request->input('concepto');

			if($request->hasFile('image')){
				ini_set("gd.jpeg_ignore_warning", 1);
				$file = Input::file('image');
				$input = array('image' => $file);
				$rules = array(
					'image' => 'image',
					'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png,jpg|max:20000'
				);

				$validator = Validator::make($input, $rules);
				if ( $validator->fails() ) {
					$resp->err = false;
					$resp->mensaje = 'Ocurrio un error al momento de validar la imagen';

				} else { 

					$filename = $file->getClientOriginalName();

					$base =  explode('.', $file->getClientOriginalName());

					$files = $this->getId4Log().'_'.$filename;

					$indice = 0;
					if($base[1]== 'pdf'||$base[1]== 'xls'||$base[1]== 'doc'||$base[1]== 'docx'||$base[1]== 'pptx'||$base[1]== 'pps'||$base[1]== 'xlsx'){
						$indice = 1;
					}

					Storage::disk('uploadDocumento')->put($files, \File::get($file));
					$transferencia->adjunto = $files;
				}

				$resp->err = false;
				$resp->mensaje = 'Se modificaron los datos';

			} else {

				$resp->err = false;
				$resp->mensaje = 'Se modificaron los datos';
			}

			$transferencia->save();
		
		} catch(\Exception $e){
				Log::error($e);
				$resp->err = true;
				$resp->mensaje = 'No se modificaron los datos, ocurrio un error al procesar la información';
		}

		return response()->json($resp);	
	}
	
	public function movimientoBancarios(Request $request){	
		$transferencia = TransferenciasCuentas::where('id',$request->input('id_movimiento'))
												->get(['adjunto']);
		return response()->json($transferencia);	

	}

	public function getDataListado(Request $req){	
		$bancos = new BancoCabecera;
		$bancos = $bancos->where('id_empresa',$this->getIdEmpresa());
		$bancos = $bancos->selectRaw("*,to_char(fecha_creacion, 'DD/MM/YYYY') AS fecha_creacion_format");


		if($req->input('activo')){
			$bancos = $bancos->where('activo',$req->input('activo'));
		}
		$bancos = $bancos->get(['id','nombre','activo']);

		return response()->json($bancos); 
	}
    /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }


	public function saldoFechaCuenta(){

		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			$permisos = DB::select("SELECT pe.* 
			FROM persona_permiso_especiales p, permisos_especiales pe
			WHERE p.id_permiso = pe.id 
			AND p.id_persona = ".$idUsuario." 
			AND pe.url = 'saldo-fecha-cuenta'");

			if(empty($permisos)){
			flash('No tiene los permisos para ver esta vista')->error();
			return redirect()->route('home');
			} 

		$cuentas = DB::table('banco_detalle as bd')
		->select('bd.*','cu.currency_code','tcb.denominacion','bc.nombre AS banco_n')
		->leftJoin('currency as cu','cu.currency_id','=','bd.id_moneda')
		->leftJoin('tipo_cuenta_banco as tcb','tcb.id','=','bd.id_tipo_cuenta')
		->leftJoin('banco_cabecera as bc', 'bc.id','=','bd.id_banco')
		->where('bc.id_empresa',$this->getIdEmpresa())
		//->where('bc.activo',true)
		->get();


		return view('pages.mc.banco.saldo_fecha_cuenta',compact('cuentas'));
	}

	public function setSaldoFechaCuenta( Request $request){
		
		
		try {
			DB::beginTransaction();
			$id_cuenta = $request->input('id_cuenta_detalle');
			$saldo = $request->input('saldo');
			$fecha_reset = $request->input('fecha');
			$fecha  = $this->formatoFechaEntrada(trim($fecha_reset)).' 00:00:00';
			$eliminar_mov_manual = $request->input('movimiento_manuales') == 'SI' ? 1 : 0;

			$recalcular = DB::select("SELECT calculo_saldo_custom(?,?,?,?)", [$id_cuenta, $saldo, $fecha, $eliminar_mov_manual]);

			DB::commit();
			flash('La operación fue realizada con exito')->success();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);
			flash('Ocurrio un error al intentar definir el saldo => '.$e->getMessage())->error();
		}

		return back();
	}

	public function permisoTc() {
		$personas = Persona::with('tarjetas.banco_detalle.currency','tarjetas.banco_detalle.banco_cabecera','tipo_persona')
		->whereIn('id_tipo_persona',[2,1,3,5,6,4])
		->where('id_empresa',$this->getIdEmpresa())
		->where('activo',true)->get();
		

		$tarjetas = DB::select("SELECT 
							bd.id id_tarjeta, 
							bd.numero_cuenta||' - '||m.currency_code  as nro_tarjeta, 
							bc.nombre as banco
                            FROM banco_detalle bd,banco_cabecera bc, currency m
                            WHERE bd.id_tipo_cuenta = 4 
                            and bd.id_moneda = m.currency_id
                            and bc.activo = true
                            and bd.activo = true
                            and bd.id_banco  = bc.id
                            and bc.id_empresa =".$this->getIdEmpresa());


		return view('pages.mc.banco.index_asignar_tc', compact('personas','tarjetas'));
	}

	public function setTcPersona(Request $req) {
		

		
			TarjetaPersona::where('id_persona',$req->id_persona)->delete();

			if($req->input('id_tarjetas') && count($req->id_tarjetas)){
				foreach ($req->id_tarjetas as $key => $value) {
					TarjetaPersona::create([
						'id_banco_detalle' => $value,
						'id_persona' => $req->id_persona,
						'created_at' => date('Y-m-d H:m:s'),
						'updated_at' => date('Y-m-d H:m:s'),
					]);
				}
			}
		
	


		flash('Datos guardados correctamente')->success();
		return back();
	}

	private function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}

	public function datosPermiso($vista, $permiso){

			$btn = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$this->getIdUsuario() ." 
							AND pe.url = '".$vista."' 
							AND pe.id = ".$permiso);
			//print_r($parametros['id_factura']);
			if(!empty($btn)){
			$respuesta = 1;  		
				return $respuesta;
		} else {
			$respuesta = 0;
			return $respuesta;
		}
	}//function

	public function modificarTransferencia(Request $request){

		$resp = new \StdClass;
			
		try{

			$cuenta = TransferenciasCuentas::find($request->id_movimiento);

			if(!$cuenta){
				$resp->err = true;
				$resp->mensaje = 'No se encuentra la transferencia';
			}

			$cuenta->importe = $request->importe;
			$cuenta->cotizacion = $request->cotizacion;
			$cuenta->importe_cotizado = $request->importe_cotizado;
			$cuenta->save();

			$resp->err = true;
			$resp->mensaje = 'Los datos fueron actualizados';
		
		} catch(\Exception $e){
				Log::error($e);
				$resp->err = true;
				$resp->mensaje = 'No se modificaron los datos, ocurrio un error al procesar la información';
		}

		return response()->json($resp);	
	}

}//CLASS

