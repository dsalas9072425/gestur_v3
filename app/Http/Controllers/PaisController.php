<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Pais;
use DB;


class PaisController extends Controller
{
	public function index(Request $request){
		$paises = Pais::where('activo', true)->get();
		return view('pages.mc.paises.index')->with('paises', $paises);
	}	

	public function add(Request $request){
		return view('pages.mc.paises.add');
	}	

	public function doAddPais(Request $request){
		$empresa = new Pais;
		$empresa->denominacion = $request->input('denominacion');
		$empresa->activo = true;
		try{
			$empresa->save();
			flash('Se ha ingresado la Pais exitosamente')->success();
			return redirect()->route('paises');
		} catch(\Exception $e){
			flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			return redirect()->back();
		}
	}

	public function edit(Request $request){
		$paises = Pais::where('id', $request->input('id'))->get();
		return view('pages.mc.paises.edit')->with('paises', $paises);
	}	

	public function doEditPais(Request $request){
		DB::table('paises')
					    ->where('id',$request->input('id'))
					    ->update([
								'denominacion'=>$request->input('denominacion'),
					            ]);
		flash('Se ha editado la Pais exitosamente')->success();
		return redirect()->route('paises');
	}	
	public function delete(Request $request){
		DB::table('paises')
					    ->where('id',$request->input('id'))
					    ->update([
								'activo'=>false,
					            ]);
		flash('Se ha eliminado exitosamente')->error();
		return redirect()->route('paises');
	}	
}