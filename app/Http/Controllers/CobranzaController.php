<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Recibo;
use App\ReciboDetalle;
use App\Anticipo;
use App\BancoCabecera;
use App\BancoDetalle;
use App\FormaCobroCliente;
use App\CtaCtb;
use App\Persona;
use App\Proforma;
use App\Currency;
use App\Grupo;
use App\CentroCosto;
use App\LibroVenta;
use App\LibroCompra;
use App\EstadoFactour;
use App\OpDetalle;
use App\ChequeFp;
use App\FormaPagoCliente;
use App\FormaCobroDetalle;
use App\FormaCobroRecibo;//MIGRAR A FormaCobroReciboCabecera PENDIENTE
use App\FormaCobroReciboCabecera;
use App\FormaCobroReciboDetalle;
use App\DepositoBancario;
use App\DepositoBancarioDetalle;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReciboCobranza;
use App\Procesadora;
use App\PlanCuenta;
use App\TipoOperacionRecibo;
use App\SucursalEmpresa;
use App\Retencion;
use App\ComercioPersona;
use App\Empresa;
use App\Cierre;
use App\ReciboAdjunto;
use App\VentasRapidasCabecera;
use App\ProformaCliente;
use App\CtaCtteFormaCobro;
use App\CuentaCorrienteEmpresa;
use App\CotizacionIndice;
use App\Traits\RecibosTrait;
use App\Traits\DepositoRecaudacionesTrait;
use DB;
use stdClass;
use Illuminate\Support\Facades\Log;
use App\Exceptions\ExceptionCustom;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


class CobranzaController extends Controller
{

	use RecibosTrait,DepositoRecaudacionesTrait ;

	//VALORES DE GENERAR RECIBO EN NUMEROS
	private static $UNIDADES = [
		'',
		'UN ',
		'DOS ',
		'TRES ',
		'CUATRO ',
		'CINCO ',
		'SEIS ',
		'SIETE ',
		'OCHO ',
		'NUEVE ',
		'DIEZ ',
		'ONCE ',
		'DOCE ',
		'TRECE ',
		'CATORCE ',
		'QUINCE ',
		'DIECISEIS ',
		'DIECISIETE ',
		'DIECIOCHO ',
		'DIECINUEVE ',
		'VEINTE '
	];
	private static $DECENAS = [
		'VEINTI',
		'TREINTA ',
		'CUARENTA ',
		'CINCUENTA ',
		'SESENTA ',
		'SETENTA ',
		'OCHENTA ',
		'NOVENTA ',
		'CIEN '
	];
	private static $CENTENAS = [
		'CIENTO ',
		'DOSCIENTOS ',
		'TRESCIENTOS ',
		'CUATROCIENTOS ',
		'QUINIENTOS ',
		'SEISCIENTOS ',
		'SETECIENTOS ',
		'OCHOCIENTOS ',
		'NOVECIENTOS '
	];


	/**
	 * =================================================================================================
	 *       							LISTADO	RECIBO
	 * =================================================================================================
	 */


	public function index(Request $request)
	{

		$clientes = DB::select("SELECT id,nombre,apellido,documento_identidad,dv  FROM personas 
							  WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
							  							WHERE puede_facturar = true) 
							  AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");
		$estadoCobro = EstadoFactour::where('id_tipo_estado',10)->where('activo',true)->where('visible',true)->get();		
		// dd($estadoCobro);
		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();	

		$cierres = Cierre::with('usuario')
							->where('id_empresa',$this->getIdEmpresa())
							->where('activo', true)
							->orderBy('fecha_cierre', 'DESC')
							->get();	
		return view('pages.mc.cobranzas.index',compact('clientes','estadoCobro','currency','cierres'));
	}	


	
	public function ajaxTableRecibo(Request $req)
	{

		$recibos = DB::table('vw_recibo');
		
		$recibos = $recibos->where('id_empresa',$this->getIdEmpresa());

		if($req->input('id_cliente')){
			$recibos = $recibos->where('id_cliente',$req->input('id_cliente'));
		}

		if($req->input('id_estado') != ""){
				$recibos = $recibos->whereIn('id_estado',$req->input('id_estado'));
		}
		if($req->input('id_moneda') != ""){ 
			$recibos = $recibos->where('id_moneda',$req->input('id_moneda'));
		}
		if($req->input('num_recibo') != ""){
			$recibos = $recibos->where('nro_recibo',$req->input('num_recibo'));
		}

		if(!$req->input('id_cierre')){
			if($req->input('periodo')){
				$fechaPeriodo = explode(' - ', $req->input('periodo'));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
				$recibos = $recibos->whereBetween('fecha_hora_creacion',[$desde,$hasta]);
			}
		}else{
			$recibos = $recibos->where('id_cierre',$req->input('id_cierre'));
		}	
		$recibos = $recibos->orderBy('nro_recibo','DESC');
        $recibos = $recibos->get();


		if($req->input('factura')){
			$resultado= [];
			foreach($recibos as $recibo){
				$recibo_detalles = DB::select("SELECT lc.nro_documento as lcdocumento, lv.nro_documento as lvdocumento, a.id as adocumento
												FROM recibos_detalle rd
												LEFT JOIN libros_compras lc ON lc.id = rd.id_libro_compra
												LEFT JOIN libros_ventas lv ON lv.id = rd.id_libro_venta
												LEFT JOIN anticipos a ON a.id = rd.id_anticipo
												WHERE id_cabecera =".$recibo->id);
				foreach($recibo_detalles as $recibo_detalle){
					if($recibo_detalle->lvdocumento == $req->input('factura') || $recibo_detalle->adocumento == $req->input('factura') || $recibo_detalle->lcdocumento == $req->input('factura')){
						$resultado[] = $recibo;
					}
				}
			}
			$recibos = $resultado;
		}
		return response()->json(['data'=>$recibos]);

	}
	
	public function ajaxTableReciboFormaPago(Request $req)
	{
			$recibos = new Recibo; 
			//$sucursales= $sucursales->with(['SucursalEmpresa']);
			$recibos = $recibos->with(['SucursalEmpresa']);
			$recibos = $recibos->with(['usuario_generado' => function($query){
				$query->selectRaw("substring(concat(nombre,apellido) for 10)||'...' as full_name,*");
			},'cliente' => function($query){
				$query->selectRaw("substring(concat(nombre,apellido,denominacion_comercial) for 10)||'...' as full_name,*");
			},'detalle.libroVenta'=> function($query) use($req){
				if($req->input('factura')){
					$query->where('nro_documento','like', '%'.trim($req->input('factura')).'%');	
				}
			},'formaCobro.formaCobroReciboDetalles.forma_cobro','formaCobro.formaCobroReciboDetalles.banco','formaCobro.formaCobroReciboDetalles.banco.banco_cabecera','formaCobro.formaCobroReciboDetalles.moneda','moneda', 'sucursal', 'estado','tipo_recibo','sucursalEmpresa'
			]);

			$recibos = $recibos->where('id_empresa',$this->getIdEmpresa());

			//CONTADOR DE RETENCIONES
			$recibos = $recibos->withCount(['detalle'=> function($query){
				$query->whereNotNull("importe_retencion");
			}]);

			$recibos = $recibos->selectRaw("TO_CHAR(fecha_hora_creacion,'DD/MM/YYYY') as fecha_creacion_format,
															substring(concepto for 10)||'...' as concepto_cortado,
															TO_CHAR(fecha_hora_cobro,'DD/MM/YYYY') as fecha_hora_cobro_format,
															CASE
																WHEN id_estado = 40 THEN 2
																WHEN id_estado = 56 THEN 3
																WHEN id_estado = 55 THEN 4
																WHEN id_estado = 31 THEN 5
																ELSE 0
															END AS orden,* ");
			if($req->input('id_cliente')){
				$recibos = $recibos->where('id_cliente',$req->input('id_cliente'));
			}
			if(null !== $req->input('id_estado')){
				if(count($req->input('id_estado')) > 0){
					$recibos = $recibos->whereIn('id_estado',$req->input('id_estado'));
				}
			}	
			if($req->input('id_moneda')){
				$recibos = $recibos->where('id_moneda',$req->input('id_moneda'));
			}
	 	    if(!$req->input('id_cierre')){
				if($req->input('periodo')){
					$fechaPeriodo = explode(' - ', $req->input('periodo'));
					$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
					$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
					$recibos = $recibos->whereBetween('fecha_hora_creacion',array($desde,$hasta));
				}
		 	}else{
				$recibos = $recibos->where('id_cierre',$req->input('id_cierre'));
			}	 

			if($req->input('num_recibo')){
				$recibos = $recibos->where('nro_recibo','like', '%'.trim($req->input('num_recibo')).'%');
			}

			$recibos = $recibos->orderBy('nro_recibo','DESC');
			$recibos = $recibos->get();
			if($req->input('factura')){
				$resultado= [];
				foreach($recibos as $recibo){
					if(!empty($recibo->detalle[0]->libroVenta)){
						$resultado[] = $recibo;
					}
				}
				$recibos = $resultado;
			}
		$resultado = [];
		$contador = 0;
		foreach($recibos as $key=>$recibo){
			$fp_recibo = DB::table('vw_forma_cobro_listado');
			$fp_recibo = $fp_recibo->where('id_recibo',$recibo->id);
			$fp_recibo = $fp_recibo->where('activo',true);
			$denominacionSucursal = $recibo->sucursalEmpresa->denominacion;
			//$resultado[$contador]['denominacion'] = $recibo->sucursalEmpresa->denominacion;
			//$resultado[$contador]['sucursal'] = $denominacionSucursal;
			$fp_recibo = $fp_recibo->get();
			if(empty($fp_recibo)){
				$tipo_id ="";
				$tipo_n ="";
				$fecha = '';
				$nombre = '';
				$resultado[$contador]['id'] = $recibo->id;
				$resultado[$contador]['cliente'] = $recibo->cliente->full_name;
				$resultado[$contador]['nro_recibo'] = $recibo->nro_recibo;
				if($recibo->tipo_recibo !== null){
					$tipo_id = $recibo->tipo_recibo->id ;
					$tipo_n =$recibo->tipo_recibo->denominacion;
				}	
				$resultado[$contador]['tipo_recibo_id'] = $tipo_id;
				$resultado[$contador]['tipo_recibo_denominacion'] = $tipo_n;
				$resultado[$contador]['detalle_count'] = $recibo->detalle_count;
				$resultado[$contador]['orden'] = $recibo->orden;
				$resultado[$contador]['estado'] = $recibo->estado->denominacion;
				$resultado[$contador]['moneda'] = $recibo->moneda->currency_code;
				$resultado[$contador]['fecha_creacion_format'] = $recibo->fecha_creacion_format;
				$resultado[$contador]['concepto_cortado'] = $recibo->concepto_cortado;
				if($recibo->fecha_hora_cobro_format !== null){
					$fecha= $recibo->fecha_hora_cobro_format;
				}
				$resultado[$contador]['fecha_hora_cobro_format'] = $fecha;
				$resultado[$contador]['forma_cobro'] = 'NO POSEE';
				if($recibo->usuario_generado !== null){
					$nombre = $recibo->usuario_generado->full_name;
				}
				$resultado[$contador]['usuario_generado'] = $nombre;
				$resultado[$contador]['importe'] = $recibo->importe;
				$resultado[$contador]['banco'] = "";
				$resultado[$contador]['nro_comprobante'] = "";
				$resultado[$contador]['sucursal'] = $denominacionSucursal;
				$contador++;
			}else{
				foreach($fp_recibo as $keys=>$formaCobro){
					$resultado[$contador]['id'] = $recibo->id;
					$resultado[$contador]['cliente'] = $recibo->cliente->full_name;
					$resultado[$contador]['nro_recibo'] = $recibo->nro_recibo;
					if($recibo->tipo_recibo !== null){
						$tipo_id = $recibo->tipo_recibo->id ;
						$tipo_n =$recibo->tipo_recibo->denominacion;
					}	
					$resultado[$contador]['tipo_recibo_id'] = $tipo_id;
					$resultado[$contador]['tipo_recibo_denominacion'] = $tipo_n;
					$resultado[$contador]['detalle_count'] = $recibo->detalle_count;
					$resultado[$contador]['orden'] = $recibo->orden;
					$resultado[$contador]['estado'] = $recibo->estado->denominacion;
					$resultado[$contador]['moneda'] = $formaCobro->currency_code;
					$resultado[$contador]['fecha_creacion_format'] = $recibo->fecha_creacion_format;
					$resultado[$contador]['concepto_cortado'] = $recibo->concepto_cortado;
					$fecha = null;
					if($recibo->fecha_hora_cobro_format !== null){
						$fecha= $recibo->fecha_hora_cobro_format;
					}
					$resultado[$contador]['fecha_hora_cobro_format'] = $fecha;
					$resultado[$contador]['forma_cobro'] = $formaCobro->forma_cobro_cliente_n;
					if($recibo->usuario_generado !== null){
						$nombre = $recibo->usuario_generado->full_name;
					}
					$resultado[$contador]['usuario_generado'] = $nombre;
					$resultado[$contador]['importe'] = $formaCobro->importe_pago;
					$banco_cabecera = $formaCobro->banco_cuenta;
					$numero_cuenta = ''; 
					$resultado[$contador]['banco'] = $banco_cabecera." ".$numero_cuenta;
					$resultado[$contador]['nro_comprobante'] = $formaCobro->nro_comprobante;
					$resultado[$contador]['sucursal'] = $denominacionSucursal;
					$contador++;
				}
			}	
		}	
		return response()->json(['data'=>$resultado]);

	}


	/**
	 * =================================================================================================
	 *       							CONTROL RECIBO
	 * =================================================================================================
	 */


	public function controlRecibo(Request $request, $id)
	{
		$cotizacion = 0;
		$datos = Recibo::with(
 								'usuario', 
 								'cliente', 
 								'moneda', 
 								'sucursal', 
 								'estado', 
 								'gestor', 
								'usuario_generado',
								'usuario_cobrado',
								'usuario_aplicado',
 								'usuario_anulacion'
 							)
							->where('id', $id)
							->where('id_empresa',$this->getIdEmpresa())
							->get();
	
		if(!isset($datos[0]->id)){
	    	flash('El recibo solicitado no existe. Intentelo nuevamente !!')->error();
            return redirect()->route('home');
	    }

		$asiento = [];
		$id_origen_asiento = '';
		$btn = $this->btnPermisos([],'verificarRecibo');
		$pms = $this->btnPermisos([],'controlRecibo');
		if(empty($pms)){
			$permiso = 0;
		}else{
			$permiso = 1;
		}

		$cotizaciones = DB::select("select y.currency_id, y.currency_code, c.venta from cotizaciones c, currency y where c.id_currency = y.currency_id and fecha >= current_date and c.id = (select max(id) from cotizaciones x where y.currency_id = x.id_currency and id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.") order by 2 asc;"); 
		foreach($cotizaciones as $cotiza){
			if($cotiza->currency_id == $datos[0]->id_moneda){
				$cotizacion = $cotiza->venta;
			}
		}

		$origen = BancoCabecera::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->get();

		$correos_administrativos = $datos[0]->cliente->correo_administrativo;
		  
		$reciboDetalle = DB::select("SELECT
									concat(lv.nro_documento,lc.nro_documento, ant.id, r.id) AS nro_documento,
									concat(lv.id_documento,lc.id_documento) AS id_factura,
									f.id_venta_rapida AS id_venta_rapida,
									f.id_proforma AS id_proforma,
									ant.id AS id_anticipo,
									concat(tdv.denominacion, tdc.denominacion, tda.denominacion, tdr.denominacion) as documento_n,
									concat(tdv.id, tdc.id, tda.id, tdr.id) as documento_tipo,
									concat(to_char(lv.fecha_hora,'DD/MM/YYYY'), to_char(lc.fecha,'DD/MM/YYYY'), to_char(ant.fecha,'DD/MM/YYYY'), to_char(r.fecha_retencion,'DD/MM/YYYY')) as fecha_hora, 
									rd.importe 
									FROM recibos_detalle rd 
									LEFT JOIN libros_compras lc ON lc.id =  rd.id_libro_compra
									LEFT JOIN libros_ventas lv ON lv.id =  rd.id_libro_venta
									LEFT JOIN retencion r ON r.id = rd.id_retencion
									LEFT JOIN anticipos ant ON ant.id =  rd.id_anticipo
									LEFT JOIN tipo_documento_ctacte tdv ON tdv.id = lv.id_tipo_documento
									LEFT JOIN tipo_documento_ctacte tdc ON tdc.id = lc.id_tipo_documento
									LEFT JOIN tipo_documento_ctacte tda ON tda.id = ant.id_tipo_documento
									LEFT JOIN tipo_documento_ctacte tdr ON tdr.id = r.id_tipo_documento
									LEFT JOIN facturas f ON f.id = lv.id_documento
									WHERE rd.id_cabecera =".(integer)$id);

		if($datos[0]->id_asiento){
			$asiento = DB::select("SELECT * FROM asientos_contables where id = ".$datos[0]->id_asiento);
			$id_origen_asiento = $asiento[0]->id_origen_asiento;
		}


		$fp_recibo = DB::table('vw_forma_cobro_listado');
		$fp_recibo = $fp_recibo->where('id_recibo',(integer)$id);
		$fp_recibo = $fp_recibo->where('activo',true);
		$fp_recibo = $fp_recibo->get();
		$tiene_efectivo = 0;
		foreach($fp_recibo as $kwy=>$detalles){
			if($detalles->abreviatura == 'EFECT'){
				$tiene_efectivo = 1;
			}
		}
		$adjuntos = ReciboAdjunto::where('id_recibo',$id)->get();
		return view('pages.mc.cobranzas.controlRecibo',compact('tiene_efectivo','datos','id_origen_asiento','origen','btn','reciboDetalle','id', 'correos_administrativos','permiso','cotizacion','adjuntos'));		
	}

	public function getCtaCtb(Request $request)
    {
        $err = true;
		$msj = '<b>Falta parametrizar forma de cobro/moneda seleccionada.</b>';
		$ctaCtb = 0;

		//Verificar si es una cuenta que debe tener configuracion y parametro
		$forma_cobro_requerido = FormaCobroCliente::SELECTCUENTA;
		$forma_cobro_data = FormaCobroCliente::findOrFail($request->idFormaCobro);

	

			/**
			 * Las formas de cobro cheque, efectivo y cheque diferido usan parametro global los demas usan los parametrizados en forma de cobro
			 */

			switch ($forma_cobro_data->abreviatura) {
				case 'CHEQ':

					$ctaCtb = CuentaCorrienteEmpresa::where('id_empresa',  $this->getIdEmpresa())
					->where('parametro', 'CHEQUE_A_DEPOSITAR')
					->where('id_moneda', $request->idMoneda)
					->count();
				
					break;
				case 'CHEQ_DIF':

					$ctaCtb = CuentaCorrienteEmpresa::where('id_empresa', $this->getIdEmpresa())
					->where('parametro', 'CHEQUE_DIFERIDO_A_DEPOSITAR')
					->where('id_moneda', $request->idMoneda)
					->count();
						
					break;		
					
				case 'EFECT':

					$ctaCtb = CuentaCorrienteEmpresa::where('id_empresa', $this->getIdEmpresa())
					->where('parametro', 'EFECTIVO_A_DEPOSITAR')
					->where('id_moneda', $request->idMoneda)
					->count();
						
					break;	

				default:

					$ctaCtb = 1;
						
				break;

		
		}

		if ($ctaCtb ){
			return response()->json(['err'=>$err]);
		}

		$err = false;
			return response()->json(['err'=>$err, 'msj'=>$msj]); 
    }


	public function aplicarRecibo($id)
	{
		$cantidad_facturas = 0;

		$datos = Recibo::with(
								'usuario', 
								'cliente', 
								'moneda', 
								'sucursal', 
								'estado', 
								'gestor'
							)
							->where('id_empresa', $this->getIdEmpresa())
							->where('id', $id)->get();

		if(!isset($datos[0]->id)){
			flash('El recibo solicitado no existe. Intentelo nuevamente !!')->error();
			return redirect()->route('home');
		}

	    $facturas_retencion = DB::select("SELECT  nro_documento, monto_retencion, id
										  FROM libros_ventas 
										  WHERE id IN(
											  	SELECT id_libro_venta 
												FROM recibos_detalle 
												WHERE id_cabecera = ".$id." 
												AND importe_retencion IS NOT NULL)");
		if(!empty($facturas_retencion)){
			$cantidad_facturas = count($facturas_retencion);
		}										

		$tipo_operacion_pago =  FormaCobroCliente::where('activo',true)
								->where('visible',true)->where('id_empresa', $this->getIdEmpresa())
								->get();
		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$bancos = DB::table('bancos_plaza')->where('activo','S')->get();
		$btn = $this->btnPermisos([],'aplicarRecibo');
		$origen = BancoCabecera::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->where('cuenta_bancaria',true)->get();
		$pocesadora = Procesadora::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->get();
		$adjuntos = ReciboAdjunto::where('id_recibo',$id)->get();

		return view('pages.mc.cobranzas.aplicar',compact('tipo_operacion_pago','currency','origen','btn','id','datos','bancos','pocesadora','facturas_retencion','cantidad_facturas','adjuntos'));	
	}


	public function  getCanjeRetencion(Request $req)
	{

		$recibo = Recibo::where('id',$req->input('id_recibo'))->first();

		$libro_compra_canje = DB::select(" SELECT * 
			FROM libros_compras where id IN (
				SELECT od.id_libro_compra
				FROM op_cabecera oc
				JOIN op_detalle od ON od.id_cabecera = oc.id
				where oc.id_estado = 53 --OP PROCESADA
				AND od.id_libro_compra IS NOT NULL
				AND id_proveedor = ".$recibo->id_cliente.")
				AND id NOT IN(
					SELECT fd.id_libro_compra_canje 
					FROM forma_cobro_recibo_cabecera fc
					JOIN forma_cobro_recibo_detalle fd ON fd.id_cabecera = fc.id
					WHERE fc.id_recibo = ".$req->input('id_recibo')."
					AND fd.id_libro_compra_canje IS NOT NULL
					AND fc.activo = true
				) ");

		// dd($libro_compra_canje);

		return response()->json(['data'=> $libro_compra_canje]);

	}

	//aqui
	public function  getDataCanjeRetencion(Request $req)
	{

		// dd($req->input('id_libro_compra'));
		$data = new stdClass;
		if ($req->input('id_libro_compra') != "") 
		{
			$data = LibroCompra::where('id',$req->input('id_libro_compra'))->first();
		}

		return response()->json(['data'=> $data]);

	}



	/**
	 * =================================================================================================
	 *       					RETENCIONES
	 * =================================================================================================
	 */

	/**
	 * Devuelve las facturas del recibo habilitadas para retencion
	 * Estan aun no se encuentran dentro de forma de cobro recibo
	 */
	public function getFactRetencionRecibo(Request $req)
	{
		$id_recibo = (Integer)$req->input('id_recibo');
		$option = (integer)$req->input('option');
		$facturas_retencion = [];
		$cantidad_facturas = 0;

		if($id_recibo){

			if($option == 1){

				//OBTENER LAS FACTURAS QUE ESTAN EN EL RECIBO Y CON RETENCION
				$facturas_retencion = DB::select(
					"SELECT  nro_documento, monto_retencion, id
					 FROM libros_ventas 
					 WHERE id IN(
								 SELECT id_libro_venta 
								 FROM recibos_detalle 
								 WHERE id_cabecera = ".$id_recibo
							   ."AND importe_retencion IS NOT NULL )");

				if(!empty($facturas_retencion)){
					$cantidad_facturas = count($facturas_retencion);
				}
			 
			//OBTENER LAS LAS FACTURAS CON RETENCIONES PENDIENTES 
			} else if($option == 2){

				$recibo = Recibo::where('id',$id_recibo)->first();

				$facturas_retencion = DB::select(
					"SELECT nro_documento, monto_retencion, id FROM libros_ventas WHERE id IN(
					select id_libro_venta from retencion WHERE id_estado = 61) 
					AND cliente_id = ".$recibo->id_cliente
					);

			}

					
		}
										
		return response()->json(['cant_fact'=>$cantidad_facturas, 'facturas'=>$facturas_retencion]);
	}

	public function verificarRetenciones(Request $req)
	{

		$idUsuario = $this->getIdUsuario();
		$idEmpresa = $this->getIdEmpresa();
		$cant_retencion_recibo = 0;
		$cant_retencion_forma_cobro = 0;

		$resp = new \StdClass;
		$resp->estado_err = true;
		$resp->msj = '';
		$resp->continuar = true;
		$id_recibo = (integer)$req->input('id_recibo');


		try{	
			DB::beginTransaction();

			//OBTENER ESTADO ACTUAL DEL RECIBO
			$recibo_estado = Recibo::where('id',$id_recibo)->where('id_empresa',$idEmpresa)->first(['id_estado']);

			//SI ESTA GENERADO VERIFICAR
			if($recibo_estado->id_estado === 31){
										
					$cant_retencion_recibo = DB::select("SELECT COUNT(rd.id) as n
											FROM recibos r 
											JOIN recibos_detalle rd ON rd.id_cabecera = r.id
											WHERE importe_retencion IS NOT NULL
											AND r.id = ".$id_recibo)[0]->n;
	
					$cant_retencion_forma_cobro = DB::select("SELECT COUNT(fd.id) as n
									FROM forma_cobro_recibo_cabecera f
									JOIN forma_cobro_recibo_detalle fd ON fd.id_cabecera = f.id
									WHERE fd.id_lv_retencion IS NOT NULL 
									AND fd.activo = true
									AND f.id_recibo = ".$id_recibo."
									AND fd.id_tipo_pago = 5;")[0]->n;

				if($cant_retencion_recibo != $cant_retencion_forma_cobro){
					$resp->msj = 'Existen retenciones sin aplicar. ¿Desea continuar ?';
					$resp->continuar = false;
				}	
				
			}
				DB::commit();

			} catch(\Exception $e){
				Log::error($e);
				$resp->estado_err = false;
				DB::rollBack();
			}

		return response()->json($resp);

	}

	//TRASLADAR A LIBRO COMPRA AL GENERAR CANJE
	public function calcRetencionCanje($obj)
	{	
		// dd($obj);
		$idEmpresa = $this->getIdEmpresa();
		$total_gravadas = $obj->gravada10;
		$iva10 = $obj->gravada10 / 11; 
		$total_exenta = $obj->importe_pago - $total_gravadas;

		$respuesta = new \StdClass;
		$respuesta->retencion = 0;
		$respuesta->total_gravadas = $total_gravadas;
		$respuesta->iva10 = $iva10;
		$respuesta->exenta = $total_exenta;
	
		//DEVOLVER VALOR REDONDEADO Y COTIZADO EN GURANIES
		$retencion = DB::select('SELECT get_retencion('.$idEmpresa.','.$total_gravadas.','.(integer)$obj->id_cliente.',true,'.$obj->id_moneda.',true) as total');
		$respuesta->retencion = $retencion[0]->total;

		return $respuesta;

	}

	/**
	 * Vista de listado retencion
	 */
	public function listadoRetencion(Request $req)
	{
		$clientes = DB::select("SELECT * FROM personas WHERE id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true) AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");

		$estados = EstadoFactour::where('id_tipo_estado',15)->where('activo',true)->where('visible',true)->get();	

		return view('pages.mc.cobranzas.listadoRetencion',compact('clientes', 'estados' ));
	}

	/**
	 * AJAX listado retencion
	 */
	public function ajaxListadoRetencion(Request $req)
	{
		$data = new \StdClass;

		$data= DB::table('vw_retencion');
		if($req->input('cliente_id') != ""){
			$data = $data->where('id_persona',$req->input('cliente_id'));
		}

		if($req->input('factura') != ""){
			$data = $data->where(function($q)use ($req) {
                            $q->where('documento_venta', 'like', '%' .trim($req->input('factura')).'%')->orWhere('documento_compra', 'like', '%' .trim($req->input('factura')).'%');
                     });
		}

		if($req->input('num_retencion') != ""){
			$data = $data->where('numero',$req->input('num_retencion'));
		}

		if($req->input('id_tipo_retencion') != ""){
			$data = $data->where('tipo',$req->input('id_tipo_retencion'));
		}

		if($req->input('estado') != ""){
			$data = $data->where('id_estado',$req->input('estado'));
		}
		$data = $data->get();

		return response()->json(['data'=>$data]);
	}



	/**
	 * =================================================================================================
	 *       					EMITIR RECIBOS
	 * =================================================================================================
	 */

	/**
	 * Retorna los datos para la tabla de Emitir Recibo
	 */ 	
	public function listadoCobroAjax(Request $req)
	{
		// dd($req);
		$data = DB::table('vw_libros_ventas_compras');
		$data = $data->where('id_tipo_factura',1);
		$data = $data->where('tipo','!=','NC');
		/*echo '<pre>';
		print_r($data);*/
		$anticipo = [];


		//-----------LOGICA TIPO RECIBO-----------

		 if($req->input('tipo_operacion') == 2)
		 {

			//CONSULTAR DATOS DEL ANTICIPO
			$dato_anticipo = Anticipo::where('id',$req->input('anticipos'))
									  ->where('id_empresa',$this->getIdEmpresa())
									  ->first();
			// dd($dato_anticipo);
			if(!is_null($dato_anticipo)){
				//SOLO DEVULEVE ANTICIPOS COBRADOS Y DE TIPO CLIENTE
				$anticipo = DB::table('vw_generar_recibo_anticipo');

				if($req->input('anticipos')){
					$anticipo = $anticipo->where('id',$req->input('anticipos'));
				}

				//BUSCAR EN LIBRO COMPRA
				if($dato_anticipo->id_proforma){
					$data = $data->where('id_proforma',$dato_anticipo->id_proforma);
				}
				if($dato_anticipo->id_grupo){
					$data = $data->where('id',$dato_anticipo->id_grupo);
				}

				//DOCUMENTOS CON MONEDA DEL ANTICIPO
				$data = $data->where('id_moneda_venta',$dato_anticipo->id_moneda);
			

				$anticipo = $anticipo->get()->toArray();
				}

				// IN FACT CONT Y FACT CRED
				$data = $data->whereIn('id_tipo_documento',[1,5]); 

		} else {
			//ANULA LA BUSQUEDA POR DATOS DE FACTURA Y LIMITA A LOS DEL ANTICIPO
			if($req->input('idMoneda')){
				$data = $data->where('id_moneda_venta',$req->input('idMoneda'));
			}
			if($req->input('idProforma')){
				$data = $data->where('id_proforma',$req->input('idProforma'));
			}
		} 

		//TIPO RETENCION
		if($req->input('tipo_operacion') == 3){
			$data = $data->where('id_tipo_documento',27);
		} else {
			$data = $data->where('id_tipo_documento','<>',27);
		}
		


		$data = $data->where('id_empresa',$this->getIdEmpresa());

		if($req->input('nroFactura')){
			$data = $data->where('nro_documento','like','%'.$req->input('nroFactura').'%');
		}
		if($req->input('checkin_desde_hasta')){
			$fecha = explode(' ',$req->input('checkin_desde_hasta'));
			$data = $data->whereBetween('check_in_f', array($fecha[0].' 00:00:00',$fecha[1].' 00:59:59'));
		}
		if($req->input('facturacion_desde_hasta')){
			$fecha = explode(' ',$req->input('facturacion_desde_hasta'));
			$data = $data->whereBetween('fecha_hora_documento', array($fecha[0].' 00:00:00',$fecha[1].' 00:59:59'));
		}
		if($req->input('estadoCobro')){
			$data = $data->where('id_estado_cobro',$req->input('estadoCobro'));
		}
		if($req->input('pendiente')){
			$data = $data->where('pendiente',$req->input('pendiente'));
		}
		if($req->input('cobrado')){
			$data = $data->where('pagado_txt',$req->input('cobrado'));
		}
		if($req->input('idCliente')){
			$data = $data->where('cliente_id',$req->input('idCliente'));
		}

		$data = $data->where('activo',true);

		$data = $data->get()->toArray();
		// dd($data);
		$result = array_merge($data, $anticipo);
		// dd($result);
		
		return response()->json(['data'=>$result]);
	}

	/**
	 * Carga la vista de emitir recibo
	 */
	public function listadoCobro()
	{

		$estadoCobro = EstadoFactour::where('id_tipo_estado',10)->get();
		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$cliente = DB::select("SELECT * FROM personas 
							  WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
							  							WHERE puede_facturar = true) 
							  AND id_empresa = ".$this->getIdEmpresa()." order by nombre ASC");

		$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
		$centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();    
		//PENDIENTE CREAR TIPO PERSONA COBRADOR
		$gestor = Persona::where('id_empresa',$this->getIdEmpresa())->where('id_tipo_persona',24)->where('activo',true)->get(['id', 'nombre','apellido']);
		$tipo_operaciones = TipoOperacionRecibo::get();


		return view('pages.mc.cobranzas.listadoCobro',compact('currency',
															'estadoCobro',
															'cliente',
															'centro',
															'sucursalEmpresa',
															'tipo_operaciones',
															'gestor'));
	}


	public function clientesCobro(Request $req){
		$destinos = DB::select("
							SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
							concat(p.nombre||' ',p.apellido,' ','- '|| tp.denominacion) as pasajero_data
							FROM personas p
							JOIN tipo_persona tp on tp.id = p.id_tipo_persona
							WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
							AND p.activo = true
							AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

		$dest =  json_encode($destinos);
		$destinos =  json_decode($dest);

		if($req->has('q')){
			$search = $req->q;
				foreach($destinos as $key=>$destino){
					$datos = $destino->pasajero_data." ".$destino->dv." ".$destino->documento_identidad." ".$destino->id;
					$resultado = strpos(strtolower($datos), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		return response()->json($destinos);
	}
	
	/**
	 * Recibe los datos del recibo para crear el recibo
	 */
	public function setCrearRecibo(Request $req)
	{	
		$datos = new \StdClass; 
		$datos->data = $req->input('data');
		$datos->nro_recibo = $req->input('nro_recibo');
		$datos->sucursal = (integer)$req->input('sucursal');
		$datos->centro_costo = (integer)$req->input('centro_costo');
		$datos->fecha = $req->input('fecha');
		$datos->id_gestor = (integer)$req->input('id_gestor');
		$datos->cotizacion = $req->input('cotizacion');
		$datos->concepto = $req->input('concepto');
		$datos->id_tipo_recibo = (integer)$req->input('id_tipo_recibo');
		$datos->recibo = $req->input('nro_recibo'); 

		$respuesta = $this->crearRecibo($datos);

		return response()->json($respuesta);

	}

	/**
	 * Recibe datos para realizar un precalculo para vista previa y mostrar los datos 
	 */
	public function setCalcRecibo(Request $req)
	{

		$datos = new \StdClass; 
		$datos->data = $req->input('data');
		$datos->id_tipo_recibo = $req->input('id_tipo_recibo');
		$datos->sucursal = '';
		$timbrado = 0;
		
		//Obtener sucursal cuando hay libro venta
		foreach ($datos->data as $value) {
			$value = (Object)$value;
			if($timbrado  == 0){

					if($value->id_venta){
						$libro_venta = LibroVenta::where('id',$value->id_venta)->first();

						if($libro_venta->origen == 'A'){
								
							$timbrado  = $libro_venta->id_timbrado;
							$sucursal_empresa = DB::select('SELECT sucursales_empresa.id_persona
															FROM timbrados, sucursales_empresa
															WHERE sucursales_empresa.id_persona = timbrados.id_sucursal_empresa
															AND timbrados.id =?',[$timbrado])[0]->id_persona;
							$datos->sucursal = $sucursal_empresa;
							break;												
						} else {

							$sucursal_lv = SucursalEmpresa::find($libro_venta->id_sucursal);
							$datos->sucursal   = $sucursal_lv->id_persona; // id de la persona sucursal
							break;
						}
					}

			} //$timbrado  == 0
		}//foreach


		
		$result = $this->calcRecibo($datos);

		return response()->json($result);
		

	}


	/**
	 * Procesa y prueba los datos para generar el recibo
	 */
	public function crearRecibo($obj)
	{

		$idUsuario = $this->getIdUsuario();
		$idEmpresa = $this->getIdEmpresa();
		$respuesta_proceso = new \StdClass; 
		$respuesta_proceso->err = true;
		$respuesta_proceso->msj = '';
		$anticipo_total = 0;
		$total_pago = 0;
		$anular_anticipo = false;
		$id_anticipo = 0;
		$monto_anticipo = 0;
		$concepto = '';
		$timbrado = 0;
		$sucursal_recibo = null;
		$centro_costo = null;

		//ITEMS SELECCIONADOS
		$datos = new \StdClass; 
		$datos->data = $obj->data;
		$datos->id_tipo_recibo = $obj->id_tipo_recibo;
		$datos->recibo = $obj->recibo;


		//Obtener sucursal cuando hay libro venta
		foreach ($datos->data as $value) {
			$value = (Object)$value;
			if($timbrado  == 0){
				if(isset($value->id_venta)){
					$libro_venta = LibroVenta::where('id',$value->id_venta)->first();

					//Cuando es de origen automatico estiramos los timbrados de la tabla de la empresa porque son facturas de gestur
					if($libro_venta->origen == 'A'){
						$timbrado  = $libro_venta->id_timbrado;
						$sucursal_empresa = DB::select('SELECT sucursales_empresa.id_persona, 
															   sucursales_empresa.id,
															   sucursales_empresa.id_sucursal_contable
														FROM timbrados, sucursales_empresa
														WHERE sucursales_empresa.id_persona = timbrados.id_sucursal_empresa
														AND timbrados.id =?',[$timbrado])[0];
						$obj->sucursal = $sucursal_empresa->id_persona;
						$sucursal_recibo = $sucursal_empresa->id;
						$centro_costo = $sucursal_empresa->id_sucursal_contable;
						break;

					} else {

						$sucursal_lv = SucursalEmpresa::find($libro_venta->id_sucursal);

						if(!$sucursal_lv){
							$respuesta_proceso->err = false;
							$respuesta_proceso->msj = 'Error, no se pudo identificar la sucursal de la factura LV manual seleccionada';
							break;
						} 

						$obj->sucursal   = $sucursal_lv->id_persona; // id de la persona sucursal
						$centro_costo    = $libro_venta->id_centro_costo; //Centro costo
						$sucursal_recibo = $libro_venta->id_sucursal; //id de la tabla sucursales empresa
						break;
					}
																
				}
			}
		}
		
		$datos->sucursal = $obj->sucursal;

		//CALCULOS PARA GENERAR EL RECIBO
		$result_calc_recibo = $this->calcRecibo($datos);
			//----VALIDACION DE CALCULOS DE RECIBO
			if(!$result_calc_recibo->err){
				$respuesta_proceso->err = false;
				$respuesta_proceso->msj = 'Error al obtener los datos del recibo';
			} else {
				//SE OBTIENE EL NUMERO DE RECIBO
				$respuesta_proceso->num_recibo = $result_calc_recibo->nro_recibo;

				//MODIFICAR EL CONCEPTO CON EL NUMERO DE RECIBO
				$concepto = 'REC '.$result_calc_recibo->nro_recibo.' - '.$obj->concepto;
				
			}

	
		//VALIDAR DATOS ANTES DE GUARDAR
		if($respuesta_proceso->err){

			//EN CASO DE SER RECIBO ANTICIPO DE VALOR 0 INSERTAR FORMA COBRO ANTICIPO
			//TAMBIEN ANULAR EL INSERT DE ANTICIPO EN EL DETALLE DEL RECIBO
			// dd($result_calc_recibo->total_pago_neto == 0 && $obj->id_tipo_recibo == 2);
			if($result_calc_recibo->total_pago_neto == 0 && $obj->id_tipo_recibo == 2){
				$anticipo_total = 0;
				$total_pago = $result_calc_recibo->total_pago;
				$anular_anticipo = true;
			} else {
				$anticipo_total = $result_calc_recibo->total_anticipo;
				$total_pago = $result_calc_recibo->total_pago_neto;
				$anular_anticipo = false;
			}

			
				try{
					DB::beginTransaction();

				$recibo = new Recibo;
				$recibo->fecha_hora_creacion = date('Y-m-d H:i:00');
				$recibo->id_usuario_creacion = $idUsuario;
				$recibo->id_empresa = $idEmpresa;
				$recibo->id_sucursal = $sucursal_recibo;
				$recibo->id_estado = 31;//ESTADO GENERADO
				$recibo->id_cliente = $result_calc_recibo->id_cliente;
				$recibo->importe = $result_calc_recibo->total_pago;
				$recibo->total_nc = $result_calc_recibo->total_nc;
				$recibo->total_anticipo = $anticipo_total;
				$recibo->total_pago = $total_pago;
				$recibo->id_centro_costo = $centro_costo;
				$recibo->fecha_emision  = $obj->fecha;
				// $recibo->fecha_vencimiento = 0;
				$recibo->id_moneda 		= $result_calc_recibo->id_moneda;
				$recibo->total_canje 	= $result_calc_recibo->total_canje;
				$recibo->id_gestor 		= $obj->id_gestor;
				$recibo->nro_recibo 	= $respuesta_proceso->num_recibo;
				$recibo->cotizacion 	= $obj->cotizacion;
				$recibo->concepto 		= $concepto;
				$recibo->id_tipo_operacion_recibo = $obj->id_tipo_recibo;
				/*echo '</pre>';
				print_r($recibo);

					die;*/
				$recibo->save();
				$reciboDetalle = [];

				
				foreach($datos->data as $value){
					$value = (Object)$value;

			
					
					//ANULAR INSERT DE ANTICIPO
					//DISEÑO PARA UN ANTICIPO
					if($value->id_anticipo && $obj->id_tipo_recibo == 2){

						if(!$anular_anticipo){
							$reciboDetalle[] = [
								'id_cabecera'=>	$recibo->id,
								'id_libro_venta' => ($value->id_venta) ? $value->id_venta : null,
								'id_libro_compra' => ($value->id_compra) ? $value->id_compra : null,
								'id_anticipo' => ($value->id_anticipo) ? $value->id_anticipo : null, 
								'id_retencion' => ($value->id_retencion) ? $value->id_retencion : null, 
								'importe' => (float)$value->monto_pago
							];
						}
						$id_anticipo = $value->id_anticipo;
						$monto_anticipo = (float)$value->monto_pago;

				  } else {
						$reciboDetalle[] = [
							'id_cabecera'=>	$recibo->id,
							'id_libro_venta' => ($value->id_venta) ? $value->id_venta : null,
							'id_libro_compra' => ($value->id_compra) ? $value->id_compra : null,
							'id_anticipo' => ($value->id_anticipo) ? $value->id_anticipo : null, 
							'id_retencion' => ($value->id_retencion) ? $value->id_retencion : null, 
							'importe' => (float)$value->monto_pago
						];
				  }

				}

				//insert detalle y generar recibo
				DB::table('recibos_detalle')->insert($reciboDetalle);  
				DB::select('SELECT generar_recibo('.$recibo->id.','.$idUsuario.')');

				//TIPO COBRANZA
				if($obj->id_tipo_recibo == 1){

					$rt = DB::select('SELECT generar_retencion_cobro('.$recibo->id.')');
				//TIPO ANTICIPO	
				} else if($obj->id_tipo_recibo == 2){
					if($anular_anticipo){
						//GENERAR RECIBO Y APLICAR
						$txt_err = $this->aplicar_recibo_anticipo($recibo->id,$id_anticipo,$monto_anticipo);
						
						if($txt_err!= 'OK'){
							$respuesta_proceso->err = false;
							$respuesta_proceso->msj = 'Ocurrió un error al intentar aplicar el anticipo.';
						    DB::rollBack();
						}
					}

				}

				
				
				DB::commit();
				// DB::rollBack();
				// dd();

			    } catch(\Exception $e){

					Log::error($e);
					 $respuesta_proceso->err = false;
					 $respuesta_proceso->msj = 'Ocurrió un error al intentar generar el recibo.';
					DB::rollBack();
				}

		} 
	
		return $respuesta_proceso;

		

	}

	public function cotizarMontosRecibo(Request $req) 
	{

				// dd($req->all());
			$moneda_costo = $req->cabecera['id_moneda_costo'];   
			$result = DB::select('SELECT 
			get_monto_cotizado('.$req->total_factura.','.$moneda_costo.',143) AS total_factura, 
			get_monto_cotizado('.$req->total_nc.','.$moneda_costo.',143) AS total_nc,
			get_monto_cotizado('.$req->total_anticipo.','.$moneda_costo.',143) AS total_anticipo,
			get_monto_cotizado('.$req->total_pago_neto.','.$moneda_costo.',143) AS total_pago_neto,
			get_monto_cotizado('.$req->total_pago_neto.','.$moneda_costo.',111) AS total_guaranies,
			get_monto_cotizado('.$req->total_canje.','.$moneda_costo.',143) AS total_canje');
			
			
			return response()->json(['info'=>$result]);
	
	
	}//

	/**
	* Realiza los calculos para la generacion del recibo
	*/
	private function calcRecibo($obj)
	{
		  
		  $data = $obj->data;
		  $libroVenta = [];
		  $anticipo = [];
		  $ok = 0;
		  $datos_documento = new \StdClass; 
		  $e = new \StdClass; 
		  $resultado = new \StdClass; 
		  $documentos = [];
		  $log_info = "(USERID:".$this->getIdUsuario()." METOD:calcRecibo)".json_encode($obj);
		  $log_error = "(USERID:".$this->getIdUsuario()." METOD:calcRecibo) ";
		  Log::info($log_info);
  
		  //----------- DATOS CALC ------------
		  $id_tipo_documento = 0;
		  $id_moneda = 0;
		  $id_cliente = 0;
		  $saldo = 0;
		  $pago_saldo = 0;
		  $iva10 = 0;
		  $gravada10 = 0;
		  $old_cliente = 0;
		  $old_moneda = 0;
		  
		  $total_pago= 0;
		  $id_libro_venta = [];
		  $total_nc = 0;
		  $total_anticipo = 0; 
		  $total_canje = 0;
		  $total_restar = 0;
		  $total_saldos = 0;
  
		  //---------FLAG ERROR ---------
		  $error = 0;
		  $flag_1 = 0; //QUE SEA MISMO PROVEEDOR Y MISMA MONEDA
		  $flag_2 = 0; //QUE SALDO NO SEA NEGATIVO Y PAGO NO SEA CERO 
		  $flag_3 = 0; //QUE SEA UN TIPO DE DOCUMENTO NO VALIDO
		  $flag_4 = 0; //VALIDAR QUE LA MONEDA NO SEA EURO 
		  $flag_5 = 0;
		  $flag_6 = 0;//VALIDAR QUE LO PAGADO NO SEA MAYOR AL TOTAL
		  $flag_7 = 0;//VALIDAR QUE EL PAGO NETO NO SEA 0 CUANDO NO ES ANTICIPO
		  $flag_8 = 0;//VALIDAR QUE NO SEA PAGO DE VALOR NEGATIVO 
		  $flag_9 = 0;//VALIDAR QUE SI ES OPERACION 2 TENGA UN ANTICIPO
		  $msj = [];
  
		  //------CONCEPTO S/ FACTURA-----
		  $concepto = 'Recibo s/ Factura/s ';


		  //FOR PARA CALCULAR LOS VALORES DE LIBRO VENTA , COMPRA Y ANTICIPO
		  foreach ($data as $key => $value)
		  {
			  	//   dd( $value);
				  $datos_documento = new \StdClass; 
				  $detalle = (Object)$value;


			//RETENCIONES
			if($detalle->id_retencion){
				// dd($detalle->id_compra);
				try {
					$data_retencion = Retencion::where('pendiente',false)
					->where('id_empresa',$this->getIdEmpresa())
					->findOrFail($detalle->id_retencion);

				} catch(\Exception $e){ 
				  Log::error( $log_error.$e);
					$ok++; array_push($msj,'<b>Error en ID de Libro Compra o item utilizado en Recibo.</b>'); 
				  break; 
				  }

				$datos_documento->pago = (float)$detalle->monto_pago;
				$datos_documento->id_cliente = (integer)$data_retencion->id_persona;
				$datos_documento->id_tipo_documento = 27; //RETENCIONES A COBRAR
				$datos_documento->saldo = (float)$data_retencion->saldo;
				$datos_documento->iva10 = 0;
				$datos_documento->gravada10 = 0;
				$datos_documento->id_moneda_venta = (integer)$data_retencion->id_moneda;

				array_push($documentos, (Array)$datos_documento);
				unset($datos_documento);
				
			}



			  if($detalle->id_venta){
				  // dd($detalle->id_venta);
				  try {
					  $data_libro_venta = LibroVenta::where('pendiente',false)
					  ->where('id_empresa',$this->getIdEmpresa())
					  ->findOrFail($detalle->id_venta);
					  // dd($data_libro_venta);
				  } catch(\Exception $e){ 
					Log::error( $log_error.$e);
					  $ok++; 
					  array_push($msj,'<b>Error en ID de Libro Venta o item utilizado en Recibo.</b>'); 
					break; }

					$numeros_fact = explode('-',$data_libro_venta->nro_documento);
					// var_dump($numeros_fact);
				   if(isset($numeros_fact[2])){
							$concepto .= $numeros_fact[2].'// ';  
					  } else { 
						$concepto .= $data_libro_venta->nro_documento; 
				   }

				  $datos_documento->pago = (float)$detalle->monto_pago;
				  $datos_documento->id_cliente = (integer)$data_libro_venta->cliente_id;
				  $datos_documento->id_tipo_documento = (integer)$data_libro_venta->id_tipo_documento;
				  $datos_documento->saldo = (float)$data_libro_venta->saldo;
				  $datos_documento->iva10 = (float)$data_libro_venta->total_iva;
				  $datos_documento->gravada10 = (float)$data_libro_venta->total_gravadas;
				  $datos_documento->id_moneda_venta = (integer)$data_libro_venta->id_moneda_venta;

				  array_push($documentos, (Array)$datos_documento);
				  unset($datos_documento);
				  
			  }
  
			  if($detalle->id_compra){
				  // dd($detalle->id_compra);
				  try {
					  $data_libro_compra = LibroCompra::where('pendiente',false)
					  ->where('id_empresa',$this->getIdEmpresa())
					  ->findOrFail($detalle->id_compra);


					  //VERIFICAR QUE EL ITEM DE LIBRO COMPRA NO SE ENCUENTRE DENTRO DE UNA OP Y QUE LA OP NO SEA ANULADA
					  $controlOp = OpDetalle::with(['opCabecera' => function($query){ 
									  $query->where('id_estado','<>',54); }
								  ])->where('id_libro_compra',$detalle->id_compra)->count();

						// dd($data_libro_compra,$detalle->id_compra, $controlOp);

					  if($controlOp > 0){ $ok++; array_push($msj,'<b>El item de libro compra se encuentra dentro de una OP.</b>'); break; }
				  } catch(\Exception $e){ 
					Log::error( $log_error.$e);
					  $ok++; array_push($msj,'<b>Error en ID de Libro Compra o item utilizado en Recibo.</b>'); 
					break; 
					}
  
		  
				  $numeros_fact = explode('-',$data_libro_compra->nro_documento);
				  // var_dump($numeros_fact);
				  if(count($numeros_fact) > 1){$concepto .= $numeros_fact[2].'// ';  } else { $concepto .= $data_libro_compra->nro_documento; }
				  
  
				  $datos_documento->pago = (float)$detalle->monto_pago;
				  $datos_documento->id_cliente = (integer)$data_libro_compra->id_persona;
				  $datos_documento->id_tipo_documento = (integer)$data_libro_compra->id_tipo_documento;
				  $datos_documento->saldo = (float)$data_libro_compra->saldo;
				  $datos_documento->iva10 = (float)$data_libro_compra->iva10;
				  $datos_documento->gravada10 = (float)$data_libro_compra->gravadas10;
				  $datos_documento->id_moneda_venta = (integer)$data_libro_compra->id_moneda;

				  array_push($documentos, (Array)$datos_documento);
				  unset($datos_documento);
				  
			  }

			  //QUE EXISTA ANTICIPO Y QUE EL RECIBO SEA TIPO ANTICIPO
			  if($detalle->id_anticipo && $obj->id_tipo_recibo == 2){

				  try {
				  $anticipo = DB::table('anticipos')
				  ->where('pendiente',false) //QUE NO SE ENCUENTRE UTILIZDO EN OTRO DOCUMENTO
				  ->where('tipo','C')//QUE SEA ANTICIPO DE TIPO CLIENTE
				  ->where('id',$detalle->id_anticipo)
				  ->where('saldo','>',0)//SALDO MAYOR A CERO
				  ->where('id_empresa',$this->getIdEmpresa())
				  ->first();

				  } catch(\Exception $e){ 
					Log::error( $log_error.$e);
					  $ok++; 
					  array_push($msj,'<b>Error en ID de Anticipo o item utilizado en Recibo.</b>'); 
					break; 
					}
  
				  $datos_documento->pago = (float)$detalle->monto_pago;
				  $datos_documento->id_cliente = (integer)$anticipo->id_beneficiario;
				  $datos_documento->id_tipo_documento = (integer)$anticipo->id_tipo_documento;
				  $datos_documento->saldo = (float)$anticipo->saldo;
				  $datos_documento->iva10 = 0;
				  $datos_documento->gravada10 = 0;
				  $datos_documento->id_moneda_venta = $anticipo->id_moneda;

				  array_push($documentos, (Array)$datos_documento);
				  unset($datos_documento);
			  }
  
  
		  }
		  
		//   dd($ok);
			  if($ok > 0){
				  $resultado->err = false;
				  $resultado->msj = $msj;
				  return $resultado;
			  }
  
				  if(count($documentos) > 0){
					// dd($documentos);
			  foreach($documentos as $key => $value){	
				  $value = (Object)$value;
				 /* echo '<pre>';
				  print_r($value);*/
				  $id_cliente = $value->id_cliente;
				  $id_moneda = $value->id_moneda_venta;
				  $id_tipo_documento = $value->id_tipo_documento;
				  $saldo = $value->saldo;
				  $pago = $value->pago;
				  $iva10 += $value->iva10;
				  $gravada10 += $value->gravada10;
				  
				  
				  // echo $saldo, ' - ',$pago, '<br>';
				  if($key === 0){
					  $old_cliente = $id_cliente;
					  $old_moneda = $id_moneda;
				  }
		
				  // FACTURA CONTADO Y CREDITO / LV / RETENCIONES A COBRAR RC
				  if($id_tipo_documento === 1 || $id_tipo_documento === 5 || $id_tipo_documento == 27){
					  $total_pago += $pago;
				  // NOTA DE CREDITO / LV
				  } else if($id_tipo_documento === 2){
					  $total_nc += $pago;
				  //ANTICIPO - ES EQUIVALENTE A COBRAR
				  } else if($id_tipo_documento === 20){
					  $total_anticipo += $pago;
				  //FACTURA CREDITO PROVEEDOR - CANJE / LC
				  } else if($id_tipo_documento === 22){
					  $total_canje += $pago;
				  }else {
					  $flag_3 ++;
				  }
				  
				  if($id_moneda == 111){
					$saldo = round($saldo);
				  }else {
					$saldo = round($saldo,2);
				  } 
				  /* echo '<pre>';	
				  print_r($pago.'-'.$saldo); */
				  //------VALIDACIONES
				  if($pago > $saldo){ $flag_6++; }
				  if($id_moneda != 143 && $id_moneda != 111 && $id_moneda != 43){ $flag_4++; }
				  if($id_cliente !== $old_cliente || $old_moneda !== $id_moneda){ $flag_1++; }
				  if(($saldo < 0 || $pago <= 0 || $pago == null)){ $flag_2++;}
				  // echo $total_restar,'-',$pago,'<br>';
  
				  //---FORMAT VARIABLE
				  $id_tipo_documento = 0;
				  $id_moneda = 0;
				  $id_cliente = 0;
				  $saldo = 0;
				  $pago = 0;
  
			  }//FOR
			  $total_restar = $total_nc + $total_anticipo + $total_canje;
			  $neto_pago = $total_pago - $total_restar;
			//   DD($neto_pago);
			//   dd( $neto_pago, $total_pago, $total_restar,$obj->id_tipo_recibo == 2);
  

			  //CALCULAR SI EL TOTAL DE LOS CREDITOS ES MAYOR AL PAGO
			  if($total_restar > $total_pago){ $flag_5++;}
			  //VALIDAR QUE EL VALOR 0 DEL NETO SOLO SEA EN CASO DE ANTICIPO
			//  dd($obj->id_tipo_recibo);
			  if( $obj->id_tipo_recibo == 2){
				  if($neto_pago == 0){
					$flag_7++;
				  }
				}
			  if($neto_pago < 0 ){ $flag_8++;}

			  //VALIDAR QUE TENGA ANTICIPO SI ES OPERACION ANTICIPO
			  if($total_anticipo <=0 && $obj->id_tipo_recibo == 2){ $flag_9++;}

			  //SUMAR SI EXISTE UN ERROR	
			  $error = $flag_1 + $flag_2 + $flag_3 + $flag_4 + $flag_5+ $flag_6 + $flag_7 + $flag_8 + $flag_9;
		 // dd($flag_1.''.$flag_2 .' '.$flag_3 .' '.$flag_4.' '.$flag_5.' '.$flag_6.' '.$flag_7.' '.$flag_8.' '.$flag_9);
		  if ($error == 0) {

 			//NRO DE RECIBO
			// TODO: Ya se realizo un desarrollo para obtener numero de recibo por sucursal
			// pero esta comentado en un trait para implementar mas adelante

				if(!isset($obj->recibo)){
					$file_seq = DB::select('SELECT public."get_documento_recibo"(?,?)', 
							[ 
								$this->getIdEmpresa(),
								$obj->sucursal,
								]);

					$nro_recibo = $file_seq[0]->get_documento_recibo;
				}else{
					if($obj->recibo == ''){
						$file_seq = DB::select('SELECT public."get_documento_recibo"(?,?)', 
							[ 
								$this->getIdEmpresa(),
								$obj->sucursal,
								]);
						$nro_recibo = $file_seq[0]->get_documento_recibo;
					}else{	
						$nro_recibo = $obj->recibo;
					}
				}  
			  $neto_pago = $total_pago - $total_restar;
			  $resultado->total_pago = $total_pago;
			  $resultado->total_nc = $total_nc;
			  $resultado->total_canje = $total_canje;
			  $resultado->total_anticipo = $total_anticipo;
			  $resultado->total_pago_neto = $neto_pago;
			  $resultado->iva10 = $iva10;
			  $resultado->gravada10 = $gravada10;
			  $resultado->err = true;
			  $resultado->msj = $msj;
			  $resultado->id_cliente = $old_cliente;
			  $resultado->id_moneda = $old_moneda;
			  $resultado->concepto = $concepto;
			  $resultado->nro_recibo = $nro_recibo;
  
		  } else {

			  if($flag_2){ array_push($msj,'<b>El pago del saldo no puede ser menor o igual a cero.</b>'); }
			  if($flag_1){ array_push($msj,'<b>Seleccione el mismo cliente y la misma moneda.</b>'); }
			  if($flag_3){ array_push($msj,'<b>El tipo de documento seleccionado no es valido.</b>'); }
			  if($flag_4){ array_push($msj,'<b>No se puede procesar en otras monedas que no sean USD, EUR y PYG.</b>'); }
			  if($flag_5){ array_push($msj,'<b>El total de las notas de creditos, anticipos, factura credito proveedor y canje supera el total de la factura credito.</b>'); }
			  if($flag_6){ array_push($msj,'<b>El pago supera al saldo del item</b>'); }
			  //if($flag_7){ array_push($msj,'<b>El pago neto del recibo no puede ser 0</b>'); }
			 // if($flag_8){ array_push($msj,'<b>El pago neto del recibo no puede ser menor a 0</b>'); }
			  if($flag_9){ array_push($msj,'<b>Si selecciona tipo operación anticipo debe tener al menos un anticipo seleccionado.</b>');}
  
			  $resultado->err = false;
			  $resultado->msj = $msj;
		
		  }
  
		  } else { 
			  array_push($msj,'Error desconocido comuniquese con el area técnica de la empresa.');
			  $resultado->err = false;
			  $resultado->msj = $msj;
			  $ok++;
		  }

		return $resultado;
		 //  
	}



		/**
	 * =================================================================================================
	 *       				ABM	FORMA DE COBRO 
	 * =================================================================================================
	 */


	/**
	 * Funcion para insertar las formas de cobros del recibo
	 */
	public function insertFpRecibo(Request $req)
	{	
		/* echo '<pre>';
		print_r($req->all());*/
		//die;
		$err = true;
		$idUsuario = $this->getIdUsuario();
		$idEmpresa = $this->getIdEmpresa();
		$tipo_operacion = $req->input('id_tipo_operacion');
		$msj = [];
		$diferencia = 0;
		$cabecera = true;
		$id_fp_cabecera = 0;
		$obj = new \StdClass; 
		$obj->id_recibo = 	$req->input('id_recibo');
		$obj->cotizacion = 	$req->input('cotizacion');
		$obj->moneda = 	$req->input('id_moneda');
		$obj->total_real = 	$req->input('total_real');
		$obj->indice_cotizacion = 	$req->input('indice_cotizacion') ? $req->input('indice_cotizacion') : 0;
		$tipo_select = $req->input('tipo');
		$cotizacion_operativa = 0;
		$cotizacion_contable = 0;
		$total_pago_recibo  = 0;
		$idCtaCtb = $req->input('id_cuenta_contable');
		$log_info = "(USERID:".$idUsuario." METOD:insertFpRecibo) ".json_encode($req->all());
		$log_error = "(USERID:".$idUsuario." METOD:insertFpRecibo) ";
		$cotizacion_sistema = 1;


		if($this->getIdEmpresa() ==1){
			if($req->input('total_real')){
				$totalReal = $req->input('total_real'); 
			}else{
				$totalReal = $req->input('importe_pago'); 
			}	
		}else{
			$totalReal = $req->input('importe_pago'); 
		}

		Log::info($log_info);


		try{
			$recibo = Recibo::where('id',$req->input('id_recibo'))->firstOrFail();
			$total_pago_recibo = (float)$recibo->total_pago;
			$moneda_recibo = (int)$recibo->id_moneda;
			$moneda_fp = (Integer)$req->input('id_moneda');
			$cotizacion_contable = (float)$req->input('cotizacion');

			

			$cotizacion_indice = $recibo->indice_cotizacion;
			if(!$recibo->indice_cotizacion){
				$cotizacion_indice = CotizacionIndice::where('id_empresa',$this->getIdEmpresa())
				  ->where('id_moneda_origen',$moneda_recibo)
				  ->where('id_moneda_destino',$moneda_fp)
				  ->orderBy('id','DESC')
				  ->first(['indice']);
	
				  if($cotizacion_indice){
					$cotizacion_indice = $cotizacion_indice->indice;
				  }
				}

			if($moneda_recibo != $moneda_fp && $moneda_fp != 111 && $moneda_recibo != 111){
				$importe_pago =  round($totalReal / $cotizacion_indice,2);

				//Toamos la cotizacion tomamos de la vista porque se puede editar, ahora mismo es sin permiso debido al alto flujo de cobros
				if(!$cotizacion_contable){
					$cotizacion_contable = DB::select('SELECT get_cotizacion_moneda('.$moneda_fp.','.$this->getIdEmpresa().') as cotizacion')[0]->cotizacion;
				}
				$cotizacion_sistema = DB::select('SELECT get_cotizacion_moneda('.$moneda_fp.','.$this->getIdEmpresa().') as cotizacion')[0]->cotizacion;

			} else {

				$moneda_cotizar = $moneda_recibo;
				if($moneda_recibo == 111){
					$moneda_cotizar = $moneda_fp;
				}

				$cotizacion_sistema = DB::select('SELECT get_cotizacion_moneda('.$moneda_cotizar.','.$this->getIdEmpresa().') as cotizacion')[0]->cotizacion;

				//Toamos la cotizacion tomamos de la vista porque se puede editar, ahora mismo es sin permiso debido al alto flujo de cobros
				if(!$cotizacion_contable){
					$cotizacion_contable = DB::select('SELECT get_cotizacion_moneda('.$moneda_cotizar.','.$this->getIdEmpresa().') as cotizacion')[0]->cotizacion;
				}

				//Si es PYG a PYG ponemos 1
				if($moneda_recibo == 111 && $moneda_fp == 111){
					$cotizacion_sistema = 1;
				}
		


				$query = "";
				$query .= "SELECT get_monto_cotizado_asiento(";
				$query .= $totalReal.",";
				$query .= $moneda_fp.",";
				$query .= $moneda_recibo.",";
				$query .= $cotizacion_contable.",";
				$query .= $this->getIdEmpresa().") as total";
				$result = DB::select($query);
				$importe_pago =  round((float)$result[0]->total,2);
			}
				

				$diferencia = (float)$recibo->total_pago;

		   } catch(\Exception $e){
				Log::error($log_error.$e);
				$err = false;
				return response()->json(['err'=>$err, 'msj'=>$e->getMessage()]);
		  }


		
		 

		  try{
					$fp_recibo = FormaCobroRecibo::with('fp_detalle')
								->where('id_recibo',$req->input('id_recibo'))
								->where('activo',true)
								->first();
					if(!is_null($fp_recibo)) {
						$id_fp_cabecera = $fp_recibo->id;
			}

			} catch(\Exception $e){
					Log::error($log_error.$e);
					$err = false;
					return response()->json(['err'=>$err, 'msj'=>'<b>Error al obtener la forma de pago.</b>']);
			}
 
			//VALIDAR SI ES LA PRIMERA FP DE RECIBO
			if(!is_null($fp_recibo)){
			
				$cabecera = false;
				//OBTENER DIFERENCIA
				$rsp = $this->calcularDiferencias($obj);
				$diferencia = $rsp->diferencia;
			}
			// dd($diferencia,$importe_pago);

			if( !(is_numeric($importe_pago) && $importe_pago > 0)){ array_push($msj, '<b>El monto cargado no es válido.</b>'); } 
			if( !(is_numeric($cotizacion_contable) && $cotizacion_contable > 0)){ array_push($msj, '<b>La cotización no es válida.</b>'); }
			if(count($msj) == 0)
			{ 
				if( round($importe_pago,2,PHP_ROUND_HALF_UP) > $total_pago_recibo){ 
					$ajuste = round($importe_pago,2,PHP_ROUND_HALF_UP) - $total_pago_recibo;
					//Tolera diferencias de 0.50 para abajo
						if($ajuste > 0.50){
							array_push($msj, '<b>El monto ingresado es mayor al monto a pagar.</b>'/*.$importe_pago.' - '.$total_pago_recibo*/);
						}
				
			
			}

				if($diferencia != 0){ 	
					$ajuste = $importe_pago - $diferencia;
					
					//Tolera diferencias de 0.50 para abajo
					if( round($importe_pago,2,PHP_ROUND_HALF_UP) > ((float)$diferencia)+1 && $ajuste > 0.50){ 

						array_push($msj, '<b>El monto ingresado es mayor al saldo.</b>'.$importe_pago.' - '.$diferencia); 
					}
				}	
			}
			
		  try{

			 DB::beginTransaction();

			if(empty($msj)){ 



				if($cabecera){
					$fp_cabecera = new FormaCobroRecibo;
					$fp_cabecera->id_usuario = $idUsuario;
					$fp_cabecera->importe_total = $totalReal; 
					$fp_cabecera->id_recibo =  (integer)$req->input('id_recibo');
					$fp_cabecera->id_empresa =  $idEmpresa;
					$fp_cabecera->activo = true;
					$fp_cabecera->save();
					$id_fp_cabecera = $fp_cabecera->id;

					DB::table('recibos')
					->where('id',$req->input('id_recibo'))
					->update(['id_forma_cobro' =>  $id_fp_cabecera]);

				}

					$fp_detalle = new FormaCobroReciboDetalle;
					$fp_detalle->id_cabecera = (Integer)$id_fp_cabecera;
					$fp_detalle->pendiente_retencion = false;
					$fp_detalle->id_tipo_pago = $req->input('id_tipo_operacion');
					
					//Cotizcion del sistema
					$fp_detalle->cotizacion_operativa = $cotizacion_sistema;
					$fp_detalle->id_moneda = (Integer)$req->input('id_moneda');
					$fp_detalle->fecha_documento = ($req->input('fecha_documento')) ? $req->input('fecha_documento') : date('Y-m-d H:i:00'); //FECHA EN LA QUE SE REALIZA EL COBRO 
					//CONTROL DE DETALLE
					$fp_detalle->id_usuario_insert = $idUsuario;
					$fp_detalle->fecha_hora_insert = date('Y-m-d H:i:00');
					$fp_detalle->activo = 'true';
					//CANJE
					$fp_detalle->cotizacion_contable = $cotizacion_contable; //Cotizacion del lado del cliente

					$fp_detalle->importe_pago = $totalReal;
					$fp_detalle->saldo = $totalReal; 
					$fp_detalle->documento = '0';
					$fp_detalle->indice_cotizacion = $req->input('indice_cotizacion') ? $req->input('indice_cotizacion') : 0;


					//SE CARGA SEGUN ABREVIACION DE LA FORMA DE COBRO
					$forma_cobro = FormaCobroCliente::findOrFail($req->input('id_tipo_operacion'));

					switch ($tipo_select) {
						case 'EFECT':

							$forma_cobro_data = CuentaCorrienteEmpresa::where('id_empresa', $idEmpresa)
							->where('parametro', 'EFECTIVO_A_DEPOSITAR')
							->where('id_moneda', $req->input('id_moneda'))
							->first();


							if(!$forma_cobro_data){
								throw new \Exception('Falta configurar parametros de cobro.');
							}

							$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 
							$fp_detalle->documento = (String)$req->input('documento');


							break;
							
						case 'FACT':
							$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$forma_cobro->id)
							->where('activo',true)
							->where('id_moneda',$req->input('id_moneda'))
							->first();

							if(!$forma_cobro_data){
								throw new \Exception('Falta configurar parametros de cobro.');
							}

							$banco_detalle = BancoDetalle::find($forma_cobro_data->id_cuenta_banco);

							if(!$banco_detalle){
								throw new \Exception('Cuenta de banco no encontrada en sistema.');
							}

							$fp_detalle->id_banco_detalle = $banco_detalle->id; 
							$fp_detalle->id_banco_plaza = $banco_detalle->id_banco; 
							$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable;
							$fp_detalle->id_libro_compra_canje = $req->input('id_libro_compra_canje') ;

							break;

						case 'CHEQ':

								$nombreEmpresa = DB::select('SELECT denominacion FROM empresas WHERE id = '.$this->getIdEmpresa());
								$nombreEmpresa = $nombreEmpresa[0]->denominacion;

								if(!$req->input('banco')){
									throw new \Exception('Falta seleccionar la cuenta de banco.');
								}

								//En esta forma de pago nosotros no necesitamos una cuenta banco detalle seleccionado en el front end
								$cheque = new ChequeFp;
								$cheque->id_beneficiario = $req->input('id_beneficiario');
								$cheque->emisor_txt = $nombreEmpresa;
								$cheque->id_cuenta = (Integer)$req->input('banco');
								$cheque->nro_cheque = $req->input('nro_comprobante');
								$cheque->id_moneda = (Integer)$req->input('id_moneda');
								$cheque->importe = (float)$req->input('importe_pago');
								$cheque->cotizacion = $cotizacion_contable; //Cotizacion del lado del cliente
								$cheque->fecha_emision = $req->input('fecha_documento');
								$cheque->id_usuario = $idUsuario;
								$cheque->id_empresa = $idEmpresa;
								$cheque->id_tipo_cheque = 3;
								$cheque->al_portador = 'f';
								$cheque->id_recibo = (Integer)$req->input('id_recibo');
								$cheque->save();

								
								$forma_cobro_data = CuentaCorrienteEmpresa::where('id_empresa', $idEmpresa)
								->where('parametro', 'CHEQUE_A_DEPOSITAR')
								->where('id_moneda', $req->input('id_moneda'))
								->first();

								if(!$forma_cobro_data){
									throw new \Exception('Falta configurar parametros de cobro.');
								}

								$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable;



								$this->validarNumeroComprobante($req);
								$fp_detalle->nro_comprobante = $req->input('nro_comprobante');
								$fp_detalle->id_cheque = $cheque->id;


						
							break;						
							
							
						case 'CHEQ_DIF':

								$nombreEmpresa = DB::select('SELECT denominacion FROM empresas WHERE id = '.$this->getIdEmpresa());
								$nombreEmpresa = $nombreEmpresa[0]->denominacion;

								if(!$req->input('banco')){
									throw new \Exception('Falta seleccionar la cuenta de banco.');
								}

								//En esta forma de pago nosotros no necesitamos una cuenta banco detalle seleccionado en el front end
								$cheque = new ChequeFp;
								$cheque->id_beneficiario = $req->input('id_beneficiario');
								$cheque->emisor_txt = $nombreEmpresa;
								$cheque->id_cuenta = (Integer)$req->input('banco');
								$cheque->nro_cheque = $req->input('nro_comprobante');
								$cheque->id_moneda = (Integer)$req->input('id_moneda');
								$cheque->importe = (float)$req->input('importe_pago');
								$cheque->cotizacion = $cotizacion_contable; //Cotizacion del lado del cliente
								$cheque->fecha_emision = $req->input('fecha_documento');
								$cheque->id_usuario = $idUsuario;
								$cheque->id_empresa = $idEmpresa;
								$cheque->id_tipo_cheque = 2;
								$cheque->al_portador = 'f';
								$cheque->id_recibo = (Integer)$req->input('id_recibo');
								$cheque->save();

								$forma_cobro_data = CuentaCorrienteEmpresa::where('id_empresa', $idEmpresa)
								->where('parametro', 'CHEQUE_DIFERIDO_A_DEPOSITAR')
								->where('id_moneda', $req->input('id_moneda'))
								->first();

								if(!$forma_cobro_data){
									throw new \Exception('Falta configurar parametros de cobro.');
								}

								$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable;



								$this->validarNumeroComprobante($req);
								$fp_detalle->nro_comprobante = $req->input('nro_comprobante');
								$fp_detalle->id_cheque = $cheque->id;


						
							break;

						case 'DEP':
							$banco_detalle = BancoDetalle::find($req->input('id_cuenta_banco'));

							if(!$banco_detalle){
								throw new \Exception('Cuenta de banco no encontrada en sistema.');
							}


							$fp_detalle->id_banco_plaza = $banco_detalle->id_banco; 
							$fp_detalle->id_banco_detalle = $banco_detalle->id; 
							$fp_detalle->id_cta_ctb     = $banco_detalle->id_cuenta_contable; 

							$this->validarNumeroComprobante($req);
							$fp_detalle->nro_comprobante = $req->input('nro_comprobante');
						
							break;

						case 'TRANSF':
							$banco_detalle = BancoDetalle::find($req->input('id_cuenta_banco'));

							if(!$banco_detalle){
								throw new \Exception('Cuenta de banco no encontrada en sistema.');
							}

							$fp_detalle->id_banco_plaza = $banco_detalle->id_banco; 
							$fp_detalle->id_banco_detalle = $banco_detalle->id; 
							$fp_detalle->id_cta_ctb     = $banco_detalle->id_cuenta_contable; 

							$this->validarNumeroComprobante($req);
							$fp_detalle->nro_comprobante = $req->input('nro_comprobante');
						
							break;	

						case 'TC':
							$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$forma_cobro->id)
							->where('activo',true)
							->where('id_moneda',$req->input('id_moneda'))
							->first();

							if(!$forma_cobro_data){
								throw new \Exception('Falta configurar parametros de cobro.');
							}

							$banco_detalle = BancoDetalle::find($forma_cobro_data->id_cuenta_banco);

							if(!$banco_detalle){
								throw new \Exception('Cuenta de banco no encontrada en sistema.');
							}

							$fp_detalle->id_banco_detalle = $banco_detalle->id; 
							$fp_detalle->id_banco_plaza = $banco_detalle->id_banco; 
							$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 

							$this->validarNumeroOperacion($req);
							$fp_detalle->tarjeta_num_operacion = $req->input('num_operacion');
							$fp_detalle->id_procesadora = $req->input('id_procesadora');

						
							break;

						case 'TD':
							$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$forma_cobro->id)
							->where('id_moneda',$req->input('id_moneda'))
							->where('activo',true)
							->first();
							if(!$forma_cobro_data){
								throw new \Exception('Falta configurar parametros de cobro.');
							}
							
							
							$banco_detalle = BancoDetalle::find($forma_cobro_data->id_cuenta_banco);

							if(!$banco_detalle){
								throw new \Exception('Cuenta de banco no encontrada en sistema.');
							}

							$fp_detalle->id_banco_detalle = $banco_detalle->id; 
							$fp_detalle->id_banco_plaza = $banco_detalle->id_banco; 
							$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 
							
							$this->validarNumeroOperacion($req);
							$fp_detalle->tarjeta_num_operacion = $req->input('num_operacion');
							$fp_detalle->id_procesadora = $req->input('id_procesadora');

						
							break;
							
						case 'VO':
							$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$forma_cobro->id)
							->where('activo',true)
							->where('id_moneda',$req->input('id_moneda'))
							->first();

							if(!$forma_cobro_data){
								throw new \Exception('Falta configurar parametros de cobro.');
							}

							$banco_detalle = BancoDetalle::find($forma_cobro_data->id_cuenta_banco);

							if(!$banco_detalle){
								throw new \Exception('Cuenta de banco no encontrada en sistema.');
							}

							$fp_detalle->id_banco_detalle = $banco_detalle->id; 
							$fp_detalle->id_banco_plaza = $banco_detalle->id_banco; 
							$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 

							$this->validarNumeroComprobante($req);
							$fp_detalle->nro_comprobante = $req->input('nro_comprobante');
							$fp_detalle->vencimiento = $req->input('vencimiento');
							$fp_detalle->tarjeta_num_operacion = $req->input('num_operacion');
						
							break;

						case 'RETEN':
							$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$forma_cobro->id)
							->where('activo',true)
							->where('id_moneda',$req->input('id_moneda'))
							->first();

							if(!$forma_cobro_data){
								throw new \Exception('Falta configurar parametros de cobro.');
							}

							$banco_detalle = BancoDetalle::find($forma_cobro_data->id_cuenta_banco);

							if(!$banco_detalle){
								throw new \Exception('Cuenta de banco no encontrada en sistema.');
							}

							$fp_detalle->id_banco_detalle = $banco_detalle->id; 
							$fp_detalle->id_banco_plaza = $banco_detalle->id_banco; 
							$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 

							//RETENCION
							if($req->input('num_retencion') !=""){
								$fp_detalle->num_retencion = $req->input('num_retencion');
							}

							if($req->input('id_factura_retencion') !=""){
								$fp_detalle->id_lv_retencion = $req->input('id_factura_retencion');
							}	

							if($req->input('id_tipo_retencion') !=""){
								$fp_detalle->id_tipo_retencion = $req->input('id_tipo_retencion');
							}	

							//OPERACION TIPO RETENCION
							$fp_detalle->retencion = (float)$req->input('importe_pago');
						
							break;

						// case 'ANT':
							

						
						// 	break;

						default:
							$banco_detalle = BancoDetalle::find($req->input('id_cuenta_banco'));

							if(!$banco_detalle){
								throw new \Exception('Cuenta de banco no encontrada en sistema.');
							}

							$fp_detalle->id_banco_plaza = $banco_detalle->id_banco; 
							$fp_detalle->id_banco_detalle = $banco_detalle->id; 
							$fp_detalle->id_cta_ctb     = $banco_detalle->id_cuenta_contable; 


							break;
					}



					$fp_detalle->save();
					$id_fp_detalle = $fp_detalle->id;

					///--CALCULAR DIFERENCIA
					$diferencia = 0;
					$rsp = $this->calcularDiferencias($obj);
					$diferencia = $rsp->diferencia;


					//TOTAL COBRADO UPDATE
					$total_cobrado = $rsp->total_cobrado;
					DB::table('forma_cobro_recibo_cabecera')
										->where('id_recibo',$req->input('id_recibo'))
										->update(['importe_total' =>  $total_cobrado]);

					DB::commit();
					// DB::rollBack();
		

			} else {
				$err = false;
				return response()->json(['err'=>$err,'msj'=>$msj]);
			}

		 } catch(\Exception $e){
					Log::error($log_error);
					Log::error($e);
					DB::rollBack();
					$err = false;
					$err_json = [];
					if(env('APP_DEBUG')){
						$err_json = $e;
					 }

					return response()->json(['err'=>$err, 'e'=>json_encode($err_json), 'msj'=>$e->getMessage()]);
			} 
			return response()->json(['err'=>$err,'msj'=>$msj,'diferencia'=>$diferencia,'id_fp_detalle'=>$id_fp_detalle]);
	}

	private function validarNumeroComprobante(Request $req){

		$forma_cobro_detalle = FormaCobroReciboDetalle::with('fp_cabecera')
		->where('id_banco_detalle', (Integer)$req->input('id_cuenta_banco'))
		->where('nro_comprobante', $req->input('nro_comprobante'))
		->where('activo', true)
		->first();

		if (isset($forma_cobro_detalle->id)) {
			//Si encuentra un recibo que no esta anulado entonces ya existe el numero de comprobante
			$reciboComprobar = Recibo::where('id',$forma_cobro_detalle->fp_cabecera->id_recibo)
			->where('id_estado','<>',55)
			->first();

			if($reciboComprobar){
				throw new \Exception('Número de comprobante ya existe.');
			}

		}
	}


	private function validarNumeroOperacion(Request $req){

		$idEmpresa = $this->getIdEmpresa();

		$forma_cobro_detalle = FormaCobroReciboDetalle::with(['fp_cabecera' => function($fp) use($idEmpresa) {
			$fp->where('id_empresa',$idEmpresa);
		}])
		->where('id_procesadora', (Integer)$req->input('id_procesadora'))
		->where('tarjeta_num_operacion', $req->input('num_operacion'))
		->where('activo',true)
		->first();

		if (isset($forma_cobro_detalle->id)) 
		{
			$reciboComprobar = Recibo::where('id',$forma_cobro_detalle->fp_cabecera->id_recibo)
			->where('id_estado','<>',55)
			->first();

			if($reciboComprobar){
				throw new \Exception('Número de comprobante ya existe.');
			}	
		}
		
	}


	/*
	 * HALLAR LA DIFERENCIA EN LA FORMA DE PAGO CON RENDONDEO DE HASTA 2 DIGITOS
	 */
	private function calcularDiferenciaRecibo($obj)
	{
		$resp = new \StdClass;
		$resp->diferencia = 0;
		$resp->total = 0;
		$total = 0;
		$diferencia = 0;
		$idEmpresa = $this->getIdEmpresa();

		//VERIFICAR LOS ACTIVOS
		$fp_recibo = DB::table('vw_forma_cobro_listado');
		$fp_recibo = $fp_recibo->where('id_recibo',$obj->id_recibo);
		$fp_recibo = $fp_recibo->where('activo',true);
		$fp_recibo = $fp_recibo->get();
		$recibo = Recibo::where('id',$obj->id_recibo)->first(['id_moneda','total_pago','cotizacion_especial']);
		$result = 0;
		if(!is_null($fp_recibo)){
			foreach($fp_recibo as $value){
					if($recibo->id_moneda == $value->id_moneda){
						$result = (float)$value->importe_pago;	
					}else{
						if($value->id_moneda == 111 && $recibo->id_moneda !=111){

							if($recibo->cotizacion_especial == ""){	
								$result = round((float)$value->importe_pago / $value->cotizacion,3);	
							}else{
								$result = round((float)$value->importe_pago * $recibo->cotizacion_especial,2);	
							}

						}elseif($value->id_moneda !=111 && $recibo->id_moneda==111){
							$result = round((float)$value->importe_pago * $value->cotizacion,0);	
						}elseif($value->id_moneda !=111 && $recibo->id_moneda !=111){

							//Si tiene indice usa cotizacion fijada en recibo para conversion
								if($value->indice_cotizacion){	
									$result = round($value->importe_pago/$value->indice_cotizacion,2);		
								}else{
									$result = round((float)$value->importe_pago/$recibo->cotizacion_especial,2);	
								}

						}
					}
				$total  +=  $result;
			}

			$resp->total_cobrado = $total;

			/*echo '<pre>';
			print_r($recibo->total_pago ." - ".$total);*/

			if($total != (float)$recibo->total_pago){
				$diferencia = (float)$recibo->total_pago - $total;
				// dd($diferencia);
				$diferencia = round($diferencia,2);
			}
			if($diferencia < 1 || ($diferencia < 0 && $diferencia >= ·1)){
				$diferencia = 0;
			}
			// dd($diferencia);
			$resp->diferencia =  $diferencia;
			
		}
		return $resp;
	}

	/*
	 * HALLAR LA DIFERENCIA EN LA FORMA DE PAGO CON RENDONDEO DE HASTA 2 DIGITOS
	 */
	private function calcularDiferencia($obj)
	{
		$resp = new \StdClass;
		$resp->diferencia = 0;
		$resp->total = 0;
		$total = 0;
		$diferencia = 0;
		$idEmpresa = $this->getIdEmpresa();

		//VERIFICAR LOS ACTIVOS
		$fp_recibo = DB::table('vw_forma_cobro_listado');
		$fp_recibo = $fp_recibo->where('id_recibo',$obj->id_recibo);
		$fp_recibo = $fp_recibo->where('activo',true);
		$fp_recibo = $fp_recibo->get();
		$recibo = Recibo::where('id',$obj->id_recibo)->first(['id_moneda','total_pago']);
		if(!is_null($fp_recibo)){
			$result = 0;
			foreach($fp_recibo as $value){
					/*$result = DB::select("SELECT get_monto_cotizado(".$value->importe_pago.",".$value->id_moneda.",".$recibo->id_moneda.",".$idEmpresa.",0) as total");
					$total  +=  (float)$result[0]->total;*/

					if($recibo->id_moneda == $value->id_moneda){
						$result = (float)$value->importe_pago;	
					}else{
						if($value->id_moneda == 111 && $recibo->id_moneda != 111){
							$result = (float)$value->importe_pago / $value->cotizacion;	
						}elseif($value->id_moneda != 111 && $recibo->id_moneda==111){
							$result = (float)$value->importe_pago * $value->cotizacion;	
						}
						elseif($value->id_moneda != 111 && $recibo->id_moneda!=111){
							$resultado = DB::select("SELECT get_monto_cotizado(".$value->importe_pago.",".$value->id_moneda.",".$recibo->id_moneda.",".$idEmpresa.",0) as total");
							$result =  (float)$resultado[0]->total;						
						}
					}
					$total  +=  round((float)$result,2);
			}
			$resp->total_cobrado = $total;

			/*echo '<pre>';
			print_r($recibo->total_pago ." - ".$total);*/

			if($total != (float)$recibo->total_pago){
				$diferencia = (float)$recibo->total_pago - $total;
				// dd($diferencia);
				$diferencia = round($diferencia,2);
			}
			if($diferencia < 1){
				$diferencia = 0;
			}
			// dd($diferencia);
			$resp->diferencia =  $diferencia;
			
		}
		return $resp;
	}

	private function calcularDiferencias($obj)
	{
			$resp = new \StdClass;
			$resp->diferencia = 0;
			$resp->total = 0;
			$total = 0;
			$diferencia = 0;
			$idEmpresa = $this->getIdEmpresa();
			// dd($obj);
			//VERIFICAR LOS ACTIVOS
			$fp_recibo = DB::table('vw_forma_cobro_listado');
			$fp_recibo = $fp_recibo->where('id_recibo',$obj->id_recibo);
			$fp_recibo = $fp_recibo->where('activo',true);
			$fp_recibo = $fp_recibo->get();

			$recibo = Recibo::where('id',$obj->id_recibo)->first(['id_moneda','total_pago','cotizacion_especial']);
			$result = 0;

			if(!is_null($fp_recibo)){
				foreach($fp_recibo as $value){
						if($recibo->id_moneda == $value->id_moneda){
							$result = (float)$value->importe_pago;	
						}else{
							if($value->id_moneda == 111 && $recibo->id_moneda !=111){
								$result = round((float)$value->importe_pago / $value->cotizacion,2);	
							}elseif($value->id_moneda !=111 && $recibo->id_moneda==111){
								$result = round((float)$value->importe_pago * $value->cotizacion,0);	
							}elseif($value->id_moneda !=111 && $recibo->id_moneda !=111){
								//Si tiene indice usa cotizacion fijada en recibo para conversion
									if($value->indice_cotizacion){	
										$result = round($value->importe_pago/$value->indice_cotizacion,2);	
									}else{
										$result = round($value->importe_pago/$value->cotizacion,2);	
									}
							}
						}
					$total  +=  $result;
				}
			
				
				$resp->total_cobrado = $total;
				if($total != (float)$recibo->total_pago){
					$diferencia = (float)$recibo->total_pago - $total;
					// dd($diferencia);
					$diferencia = round($diferencia,2);
				}

				//Casos de PYG
				if($recibo->id_moneda == 111){
					//Tolerancia de 100 guaranies
					if($diferencia < 100){
						$diferencia = 0;
					}
				} else {
					// Tolerancia de decimales menor a 1
					if($diferencia < 1){
						$diferencia = 0;
					}
				}

				
			
				$resp->diferencia =  $diferencia;
				
			}
			//die;
			return $resp;
	}

	/**
	 * ACTUALIZAR LOS DATOS DE FORMA DE COBRO
	 */
	public function saveFpRecibo(Request $req)
	{	
		// dd($req->all());
		$completo = false;
		$err = true;

		if($req->input('id_tipo_retencion') == 1 || $req->input('id_tipo_retencion') == 3){
		//Retenciones generadas y Retenciones pendientes
			if($req->input('num_retencion')
			&& $req->input('fecha_documento') 
			&& $req->input('id_tipo_retencion')
			&& $req->input('id_factura_retencion')){
				$completo = true;
			}

		} else if($req->input('id_tipo_retencion') == 2){
		//Retenciones a generar
		 if($req->input('id_factura_retencion')){
			$completo = true;
		 }
		}
			
	


		try{

			$detalle = [
				'num_retencion'=> $req->input('num_retencion'),
				'fecha_documento' => $req->input('fecha_documento'),
				'id_tipo_retencion' => $req->input('id_tipo_retencion'),
				'id_lv_retencion' => $req->input('id_factura_retencion'),
				'datos_completos' => $completo,
				'indice_cotizacion' => $req->input('indice_cotizacion') ? $req->input('indice_cotizacion') : 0
			];


			FormaCobroReciboDetalle::where('id',$req->input('id_detalle'))
			->update($detalle);

		}catch(\Exception $e){
			Log::error($e);
			$err = false;
		}

		return response()->json(['err'>=$err]);
	}

	/**
	 * Recuperar los datos del detalle a editar
	 */
	public function getEditFpRecibo(Request $req)
	{
	
		//RECUPERAR LOS DATOS CON LAS OPCIONES DE LA RETENCION ALMACENADA

		$id_recibo = (Integer)$req->input('id_recibo');
		$option = (integer)$req->input('option');
		$facturas_retencion = [];
		$err = true;
		$msj = '';
		$fp_recibo = [];

		try{

			if($id_recibo){


				//OBTENER LOS DATOS DEL DETALLE DE RECIBO
				$fp_recibo = DB::table('vw_forma_cobro_listado');
				$fp_recibo = $fp_recibo->where('id_recibo',$id_recibo);
				$fp_recibo = $fp_recibo->where('activo',true);
				
	
				if($req->input('id_detalle')){
					$fp_recibo = $fp_recibo->where('id',$req->input('id_detalle'));
				}
				$fp_recibo = $fp_recibo->get();


				$option = $fp_recibo[0]->id_tipo_retencion;

				if($option == 1){

					//OBTENER LAS FACTURAS QUE ESTAN EN EL RECIBO Y CON RETENCION
					$facturas_retencion = DB::select(
						"SELECT  nro_documento, monto_retencion, id
						FROM libros_ventas 
						WHERE id IN(
									SELECT id_libro_venta 
									FROM recibos_detalle 
									WHERE id_cabecera = ".$id_recibo
								."AND importe_retencion IS NOT NULL )");

				
				//OBTENER LAS LAS FACTURAS CON RETENCIONES PENDIENTES 
				} else if($option == 2){

					$recibo = Recibo::where('id',$id_recibo)->first();

					$facturas_retencion = DB::select(
						"SELECT nro_documento, monto_retencion, id FROM libros_ventas WHERE id IN(
						select id_libro_venta from retencion WHERE id_estado = 61) 
						AND cliente_id = ".$recibo->id_cliente
						);

				}


		


			} else {

				$err = false;
				$msj = 'Error al obtener la forma de pago.';
			}	

		} catch(\Exception $e){
				$err = false;
				$msj = 'Error al obtener la forma de pago.';
		}


										
		return response()->json(['facturas'=>$facturas_retencion,'err'=>$err,'msj'=>$msj,'data'=>$fp_recibo ]);
	}

	/**
	 * REFACTORIZAR CODIGO !!!!
	 */
	public function getFpReciboControl(Request $req)
	{
		$err = true;
		$msj = '';
		$total = 0;
		$diferencia = 0;
		$monto_cotizado = 0;
		$obj = new \StdClass; 
		$fp_recibo = [];
		
		// try{

			$fp_recibo = DB::table('vw_forma_cobro_listado');
			$fp_recibo = $fp_recibo->where('id_recibo',$req->input('id_recibo'));
			$fp_recibo = $fp_recibo->where('activo',true);
			$fp_recibo = $fp_recibo->get();

			// $fp_recibo = FormaCobroReciboCabecera::with('formaCobroReciboDetalles.forma_cobro',
			// 											'formaCobroReciboDetalles.moneda',
			// 											'formaCobroReciboDetalles.banco_detalle'
			// 											)
			// ->selectRaw("TO_CHAR(forma_cobro_recibo_detalle.fecha_emision,'DD/MM/YYYY') as fecha_emision_format,* ")														
			// ->where('id_recibo',$req->input('id_recibo'))->first();

			if(!is_null($fp_recibo)){
				$obj->id_recibo = 	$req->input('id_recibo');
				$obj->cotizacion = 	str_replace(',','.', str_replace('.','',$req->input('cotizacion')));
				$rsp = $this->calcularDiferencias($obj);
				//$rsp = $this->calcularDiferencias($obj);
				$diferencia = $rsp->diferencia;
			}

		// } catch(\Exception $e){
		// 		$err = false;
		// 		$msj = 'Error al obtener la forma de pago.';
		// }

		// dd($fp_recibo);
		return response()->json(['err'=>$err,'msj'=>$msj,'data'=>$fp_recibo,'diferencia'=>$diferencia]);
		
	}
	public function getFpRecibo(Request $req)
	{
		$err = true;
		$msj = '';
		$total = 0;
		$diferencia = 0;
		$monto_cotizado = 0;
		$obj = new \StdClass; 
		$fp_recibo = [];
		
		// try{

			$fp_recibo = DB::table('vw_forma_cobro_listado');
			$fp_recibo = $fp_recibo->where('id_recibo',$req->input('id_recibo'));
			$fp_recibo = $fp_recibo->where('activo',true);
			

			if($req->input('id_detalle')){
				$fp_recibo = $fp_recibo->where('id',$req->input('id_detalle'));
			}
			$fp_recibo = $fp_recibo->get();

			if(!is_null($fp_recibo)){
				$obj->id_recibo = 	$req->input('id_recibo');
				$rsp = $this->calcularDiferenciaRecibo($obj);
				$diferencia = $rsp->diferencia;
		// 	}

		// } catch(\Exception $e){
		// 		$err = false;
		// 		$msj = 'Error al obtener la forma de pago.';
		}

		// dd($fp_recibo);
		return response()->json(['err'=>$err,'msj'=>$msj,'data'=>$fp_recibo,'diferencia'=>$diferencia]);
		
	}

	private function validarAplicarFormaCobro($datos)
	{
		// dd($datos);
		$err = true;
		$option = 0;
		$diferencia = 0;
		$obj = new \StdClass; 
		$resp = new \StdClass; 
		$errTxt = '';
		
		try{
			
			$fp_recibo = FormaCobroRecibo::with('fp_detalle.forma_cobro','fp_detalle.moneda')
						->where('id_recibo',$datos->id_recibo)
						->where('activo',true)->first();
		 	
		 	//dd($fp_recibo->fp_detalle);

			if(!is_null($fp_recibo)){

				$obj->id_recibo = 	$datos->id_recibo;
				$rsp = $this->calcularDiferencias($obj);
				$diferencia = (float)$rsp->diferencia;
			
			if($diferencia > 10){	
				//DIFERENCIA MAYOR A UN DOLAR
				$option = 1;
				$errTxt = 'Existe una diferencia mayor a uno y no se puede cobrar!';
			} else if($diferencia < 0) {
				//DIFERENCIA MENOR A 0
				 $option = 1;
				 $errTxt = 'Existe una diferencia negativa, ponerse en contacto con soporte técnico.';
			} else if($diferencia > 0.1 && $diferencia < 0.99) {
				//DIFERENCIA MENOR A UN DOLAR
				$option = 2;
			} else if($diferencia == 0 || $diferencia < 10 ){
				//SIN DIFERENCIA
				$option = 3;
			}
			//dd($option);
		} else {
			$option = 4;
			$errTxt = 'Ocurrió un error al intentar obtener la forma de cobro';
		}

		} catch(\Exception $e){
				Log::error($e);
				$err = false;
				if(env('APP_DEBUG')){
					$resp->e = $e;
				 }
				 $errTxt = 'Error en la consulta de BD';
		}

		// dd($fp_recibo);
		 $resp->err = $err;
		 $resp->option= $option;
		 $resp->txt= $errTxt;
		

		 return $resp;
	}

	private function validarAplicarCobro($datos)
	{
		// dd($datos);
		$err = true;
		$option = 0;
		$diferencia = 0;
		$obj = new \StdClass; 
		$resp = new \StdClass; 
		$errTxt = '';
		
		// try{
			
			$fp_recibo = FormaCobroRecibo::with('fp_detalle.forma_cobro','fp_detalle.moneda')
						->where('id_recibo',$datos->id_recibo)
						->where('activo',true)->first();

			if(!is_null($fp_recibo)){
				$obj->id_recibo = 	$datos->id_recibo;
				$rsp = $this->calcularDiferencia($obj);
				$diferencia = $rsp->diferencia;
				// dd($diferencia);

			
		// 	if($diferencia >= 1){
		// 		//DIFERENCIA MAYOR A UN DOLAR
		// 		$option = 1;
		// 		$errTxt = 'Existe una diferencia mayor a un dolar y no se puede cobrar!';
		// 	} else if($diferencia < 0) {
		// 		//DIFERENCIA MENOR A 0
		// 		 $option = 1;
		// 		 $errTxt = 'Existe una diferencia negativa, ponerse en contacto con soporte técnico.';
		// 	} else if($diferencia > 0.1 && $diferencia < 0.99) {
		// 		//DIFERENCIA MENOR A UN DOLAR
		// 		$option = 2;
		// 	} else if($diferencia == 0 || $diferencia < 0.1 ){
		// 		//SIN DIFERENCIA
		// 		$option = 3;
		// 	}



		}
		 // else {
		// 	$option = 4;
		// 	$errTxt = 'Ocurrió un error al intentar obtener la forma de cobro';
		// }

		// } catch(\Exception $e){
		// 		Log::error($e);
		// 		$err = false;
		// 		if(env('APP_DEBUG')){
		// 			$resp->e = $e;
		// 		 }
		// 		 $errTxt = 'Error en la consulta de BD';
		// }

		// dd($fp_recibo);
		 $resp->err = $err;
		 $resp->option= $option;
		 $resp->txt= $errTxt;
		

		 return $resp;
	}

	public function validarFormaPago(Request $req)
	{
		// dd($req->all());
		$obj = new \StdClass; 
		$obj->id_recibo = $req->input('id_recibo');
		$resp = $this->validarAplicarFormaCobro($obj);
		
		return response()->json($resp);

	}
	public function deletePago(Request $req)
	{
		 //dd($req->all());
		try{ 
		 $this->validate($req, [
		   'id_fp_detalle' => 'required|numeric',
		   'id_recibo'=> 'required|numeric'
	   ]);

	   $e = '';

	   } catch(\Exception $e){
		Log::error($e);
		 return response()->json(['err'=>false]);
	   }
	   
	   $obj = new \StdClass; 
	   $err = true; 
	   $diferencia = 0;
	   $idEmpresa = $this->getIdEmpresa();
	   $idUsuario = $this->getIdUsuario();
	   $idCabeceraCobro = 0;

		try{
			DB::beginTransaction();


			/**
			 * 	=============>VERIFICAR SI EXISTE FC O RETORNAR ERROR<===================
			 * */
		$data = FormaCobroReciboDetalle::where('id',$req->input('id_fp_detalle'))
					->where('activo',true)->first();

			if(!is_null($data)){
				$idCabeceraCobro = $data->id_cabecera;
			} else {
				return response()->json(['err'=>false,'diferencia'=>$diferencia, 'e'=>'Datos Corruptos']);
			}
					
			//ANULAR CHEQUE DE LA FORMA DE COBRO
			if(!is_null($data)){
			ChequeFp::where('id',$data->id_cheque)->update(['activo'=>false]);
			}

			//ELIMINAR - SOFTDELETE
			FormaCobroReciboDetalle::where('id',$req->input('id_fp_detalle'))
											->update([
											'id_usuario_delete'=>$idUsuario,
											'fecha_hora_delete'=>date('Y-m-d H:i:00'),
											'activo'=>false
										]);

			//VERIFICAR SI EXISTEN DETALLES ACTIVOS
			$forma_cobro = DB::select("SELECT COUNT(fd.*) FROM forma_cobro_recibo_cabecera fc, forma_cobro_recibo_detalle fd 
									WHERE fc.id = ".$idCabeceraCobro." 
									AND fc.activo = true 
									AND fd.activo = true 
									AND fd.id_cabecera = fc.id")[0]->count;			   

			//ELIMINAR CABECERA SI ES EL ULTIMO DETALLE 
		if($forma_cobro === 0){
			FormaCobroRecibo::where('id_recibo',$req->input('id_recibo'))
								->where('activo',true)
								->update([
									'activo'=> false
								]);
				//EL RECIBO PASA A GENERADO , SE QUITA FORMA DE COBRO ID Y CONCEPTO
			DB::table('recibos')
					->where('id',$req->input('id_recibo'))
					->update(['id_forma_cobro' => null,
								'id_estado'=>31,
								'concepto_forma_cobro'=>null]);
				
				$obj->id_recibo = 	$req->input('id_recibo');
				$rsp = $this->calcularDiferencia($obj);
				$diferencia = $rsp->diferencia;
			}else{
			///--CALCULAR DIFERENCIA
				$obj->id_recibo = 	$req->input('id_recibo');
				$rsp = $this->calcularDiferencia($obj);
				$diferencia = $rsp->diferencia;
			}
			DB::commit();
		} catch(\Exception $e){
			Log::error($e);
			$err = false;
			if(env('APP_DEBUG')){ $e = $e; } 
			DB::rollBack();
		}
	   
		 return response()->json(['err'=>$err,'diferencia'=>$diferencia, 'e'=>$e]);

   }
   
   ///CORREGIR FUNCION SET MONTO COTIZADO PORQUE TIENE EN DURO 30
   public function setMontoCotizadoRetencion(Request $req)
   {
		// dd($req->all());
		$err = true;
		$total = 0;
		$importe_pago = (float)$req->input('importe_pago');
		$id_moneda_venta = (integer)$req->input('id_moneda');
		$opt = $req->input('opt');
		$query = '';

		$cotizacion_retencion = (float)$req->input('cotizacion_contable');
		$cotizacion_operativa = (integer)$req->input('cotizacion');
		$cotizacion = 0;


		if($opt == 1){
			$cotizacion = $cotizacion_operativa;
		} else {
			$cotizacion = $cotizacion_retencion;
		}
		
		try{
			$recibo = Recibo::where('id',(integer)$req->input('id_recibo'))->first(['id_moneda']);
			$id_moneda_costo = $recibo->id_moneda;

			// $query = "SELECT round((get_monto_cotizado_asiento(round(total_iva::numeric,2),".$id_moneda_costo.",111,".$cotizacion.")/100 * 30)::numeric)  as result
			// from libros_ventas
			// where id =";

		
			$query = "";
			$query .= "SELECT get_monto_cotizado_asiento(";
			$query .= $importe_pago.",";
			$query .= $id_moneda_costo.",";
			$query .= $id_moneda_venta.",";
			$query .= $cotizacion.") as total";
			$result = DB::select($query);
			$total +=  (float)$result[0]->total;


		} catch(\Exception $e){
			Log::error($e);
			$err = false;
		}


		return response()->json(['total'=>$total, 'err'=>$err]);
   }


   public function setMontoCotizado(Request $req)
   {
		$err = true;
		$total = 0;
		$importe_pago = (float)$req->input('importe_pago');
		$id_moneda_venta = (integer)$req->input('id_moneda');
		$opt = $req->input('opt');
		$cotizacion = (integer)$req->input('cotizacion');



		try{

			$recibo = Recibo::where('id',(integer)$req->input('id_recibo'))->first(['id_moneda','indice_cotizacion']);
			$id_moneda_costo = $recibo->id_moneda;
			$cotizacion_indice = $recibo->indice_cotizacion;

			if(!$recibo->indice_cotizacion){
				$cotizacion_indice = CotizacionIndice::where('id_empresa',$this->getIdEmpresa())
				  ->where('id_moneda_origen',$id_moneda_costo)
				  ->where('id_moneda_destino',$id_moneda_venta)
				  ->orderBy('id','DESC')
				  ->first(['indice']);
	
				  if($cotizacion_indice){
					$cotizacion_indice = $cotizacion_indice->indice;
				  }
				}

			if($id_moneda_costo != $id_moneda_venta && $id_moneda_venta != 111 && $id_moneda_costo != 111){
				$total =  round($importe_pago * $cotizacion_indice,2);
			} else {
				$moneda_cotizar = $id_moneda_venta;
				if($id_moneda_venta == 111){
					$moneda_cotizar = $id_moneda_costo;
				}

				//Toamos la cotizacion tomamos de la vista porque se puede editar, ahora mismo es sin permiso debido al alto flujo de cobros
				if(!$cotizacion){
					$cotizacion = (integer) DB::select('SELECT get_cotizacion_moneda('.$moneda_cotizar.','.$this->getIdEmpresa().') as cotizacion')[0]->cotizacion;
				}
				

				$query = "";
				$query .= "SELECT get_monto_cotizado_asiento(";
				$query .= $importe_pago.",";
				$query .= $id_moneda_costo.",";
				$query .= $id_moneda_venta.",";
				$query .= $cotizacion.",";
				$query .= $this->getIdEmpresa().") as total";
				$result = DB::select($query);
				$total =  $result[0]->total;

				if($id_moneda_venta == 111){
					$total =  round($total,0);
				} else {
					$total =  round($total,2);
				}
			}

			


		} catch(\Exception $e){
			Log::error($e);
			$err = false;
		} 


	return response()->json(['total'=>$total, 'err'=>$err]);
   }

  /* public function generarConceptoCobro($obj)
   {	
		$id_recibo = $obj->id_recibo;

		$concepto = DB::select("SELECT string_agg(CONCAT(result.abreviatura,' ',result.num_doc)::text,' ') AS r FROM (
			select fc_cliente.abreviatura, 
				string_agg(
					substring(
						CONCAT(fcd.nro_comprobante||'//'),
						length(CONCAT(fcd.nro_comprobante||'//'))- 5,
						length(CONCAT(fcd.nro_comprobante||'//'))),' ') AS num_doc
			FROM forma_cobro_recibo_detalle fcd
			JOIN forma_cobro_recibo_cabecera frc ON frc.id = fcd.id_cabecera
			JOIN forma_cobro_cliente fc_cliente ON fc_cliente.id = fcd.id_tipo_pago
			WHERE frc.id_recibo = ".$id_recibo."
			AND fcd.activo = true
			GROUP BY fcd.id_tipo_pago,fc_cliente.abreviatura
				) as result")[0]->r;


				// dd($concepto);

		//SE ACTUALIZA EL CONCEPTO DE FORMA DE COBRO
		$update = ['concepto_forma_cobro'=> (String)$concepto];
		DB::table('recibos')->where('id',$id_recibo)->update($update);

   }*/

   public function estadoRecibo(Request $req)
   {
			/*echo '<pre>';
			print_r($req->all());
			die;*/
		    $obj = new \StdClass; 
		    $resp = new \StdClass; 
			$idUsuario = $this->getIdUsuario();
			$idEmpresa = $this->getIdEmpresa();
			$update = [];
			$resp->err = true;
			$resp->continuar = true;
			$resp->e = '';
			$txtErr = '';
			$forma_cobro_txt = '';
			$cant_retencion_recibo = 0;
			$cant_retencion_forma_cobro = 0;
			$option = (Integer)$req->input('option');
			$id_recibo = (integer)$req->input('id_recibo');
			$obj->id_recibo = $id_recibo;
			$concepto = (String)$req->input('concepto');
		

	 	try {	

		DB::beginTransaction();

	
		//OPCION PARA COBRAR
		 if($req->option === '1'){

				//VALIDAR DIFERENCIA
				$dif = $this->validarAplicarCobro($obj);


				//RESPUESTA DE VALIDAR LAS FORMAS DE COBRO RECIBO
				if($dif->option != 1 &&  $dif->option != 4){
					$txtErr = DB::select("SELECT cobrar_recibo(".$id_recibo.",".$idUsuario.")");	
					$txtErr = 'OK';	
					
					
					//GENERAR CONCEPTO FP
					/*$this->generarConceptoCobro($obj);*/

					$update = ['concepto_adicional'=> (String)$concepto];
					DB::table('recibos')->where('id',$id_recibo)->update($update);

					if($req->input('tipo') == 1){ 
						
						$resultado = Recibo::where('id', $id_recibo)
									->first();
		
						$cant_retencion_forma_cobro = DB::select("SELECT COUNT(fd.id) as n
														FROM forma_cobro_recibo_cabecera f
														JOIN forma_cobro_recibo_detalle fd ON fd.id_cabecera = f.id
														WHERE fd.id_lv_retencion IS NOT NULL 
														AND fd.activo = true
														AND f.id_recibo = ".$id_recibo."
														AND fd.id_tipo_pago = 5;")[0]->n;
						if($resultado->id_tipo_operacion_recibo == 2){ 
							DB::table('anticipos')->where('id_recibo',$id_recibo)->update(['id_estado'=>77]);
						}
							//GENERAR CONCEPTO FP
						//$this->generarConceptoCobro($obj);


						$txtErr = $this->procesar_recibo($req->input('id_recibo'), $idUsuario); 

							//INSERTA LA RETENCION TOMANDO LOS DATOS DE FORMA DE COBRO
							//LAS RETENCIONES QUE INSERTA SON DE TIPO CLIENTE
						DB::select("SELECT generar_retencion_documento(".(integer)$req->input('id_recibo').",'C',".$idEmpresa .",".$idUsuario.")");
					}	
				} else {
					$txtErr = 'Existe una diferencia no permitida';	
				}
			}
			//APLICAR Y PROCESAR
			else if($req->option === '2'){
				$resultado = Recibo::where('id', $id_recibo)
							->first();

				$cant_retencion_forma_cobro = DB::select("SELECT COUNT(fd.id) as n
												FROM forma_cobro_recibo_cabecera f
												JOIN forma_cobro_recibo_detalle fd ON fd.id_cabecera = f.id
												WHERE fd.id_lv_retencion IS NOT NULL 
												AND fd.activo = true
												AND f.id_recibo = ".$id_recibo."
												AND fd.id_tipo_pago = 5;")[0]->n;

				if($resultado->id_tipo_operacion_recibo == 2){ 
					DB::table('anticipos')->where('id_recibo',$id_recibo)->update(['id_estado'=>77]);
				}

				if($cant_retencion_forma_cobro > 0){ 
					//LLAMAR FUNCION RETENCION CUANDO SE COBRE CON RETENCION
					//PENDIENTE LOGICA PARA VALIDAR RETENCIONES
					// $txtRetencion =  DB::select("SELECT aplicar_retencion_cobro(".(integer)$req->input('id_recibo').",".$idUsuario.") AS retencion");
				} 
					//GENERAR CONCEPTO FP
					//$this->generarConceptoCobro($obj);
				

					$txtErr = $this->procesar_recibo($req->input('id_recibo'), $idUsuario);

					//INSERTA LA RETENCION TOMANDO LOS DATOS DE FORMA DE COBRO
					//LAS RETENCIONES QUE INSERTA SON DE TIPO CLIENTE
					DB::select("SELECT generar_retencion_documento(".(integer)$req->input('id_recibo').",'C',".$idEmpresa .",".$idUsuario.")");
					/*$update = ['concepto_forma_cobro'=> (String)$concepto];
					DB::table('recibos')->where('id',$id_recibo)->update($update);*/

										
			}
			//ANULAR RECIBO GENERADO
			else if($req->option === '3'){
				$txtErr = DB::select("SELECT anular_recibo(".(integer)$req->input('id_recibo').",".$idUsuario.")");	
				$txtErr = $txtErr[0]->anular_recibo;

			}
			//ANULAR RECIBO APLICADO
			else if($req->option === '4'){

				$txtErr = DB::select("SELECT anular_cobro_aplicado(".(integer)$req->input('id_recibo').",".$idUsuario.")");	
				$txtErr = $txtErr[0]->anular_cobro_aplicado;
				
				//ANULAR EL RECIBO COMPLETO
				$rec = Recibo::where('id',$id_recibo)->first();
				 if($rec->id_tipo_operacion_recibo == 2){
					//ANTICIPO
					/*DB::table('recibos')->update(['id_usuario_anulacion'=>$idUsuario, 
												  'fecha_hora_anulacion'=>date('Y-m-d H:i:00')]);*/
					$update = [ 'id_usuario_anulacion'=>$idUsuario, 
								'fecha_hora_anulacion'=>date('Y-m-d H:i:00')];
				    DB::table('recibos')->where('id',$id_recibo)->update($update);
				}
			}
			else if($req->option === '5'){
				$txtErr = DB::select("SELECT anular_cobro_recibo(".(integer)$req->input('id_recibo').",".$idUsuario.")");	
				$txtErr = $txtErr[0]->anular_cobro_recibo;
			}


			if ($txtErr !== 'OK') 
			{
				$resp->err =  false;
				DB::rollBack();

			} else {
				DB::commit();
			}
		} catch(\Exception $e){
			Log::error($e);
			$resp->err = false;
			if(env('APP_DEBUG')){
				$resp->e = $e;
			 }
			$txtErr = 'Ocurrió un error en el proceso interno.';
			DB::rollBack();
		} 

		$resp->txtErr = $txtErr;

		return response()->json($resp);
   }
   /**
	 * =================================================================================================
	 *       					RECAUDACIONES - DEPOSITO BANCARIO
	 * =================================================================================================
	 */

   /**
	* Carga la vista de Deposito de Recaudaciones
    */
	public function depositoBancario()
	{		
		$idEmpresa = $this->getIdEmpresa();
		$bancos = BancoCabecera::with('banco_detalle', 'banco_detalle.currency', 'banco_detalle.tipo_cuenta')->where('id_empresa',$this->getIdEmpresa())->where('cuenta_bancaria',true)->where('activo',true)->get();
		$cotizacion_operativa = DB::select('SELECT get_cotizacion_actual('.$idEmpresa.') AS cotizacion')[0]->cotizacion;
		$centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get(); 
		$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$idEmpresa)->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);

		return view('pages.mc.cobranzas.depositoBancario')->with(['bancos' => $bancos, 'centro' => $centro, 'sucursalEmpresa' => $sucursalEmpresa, 'cotizacion_operativa' => $cotizacion_operativa]);	
	}	

	
		   public function getListFormaCobro(Request $req)
		   {
				$id_empresa = $this->getIdEmpresa();
				$err = true;
				$data = [];
				if($req->input('id_banco_detalle')){
						//OBTENER MONEDA DE CUENTA
						$data = DB::table('vw_deposito_recaudaciones');
						$data = $data->where('id_empresa',$id_empresa);
						$data = $data->where('id_moneda',$req->input('monedaCuenta_id'));
						if($req->input('num_recibo')){
							$data = $data->where('nro_recibo', 'like', '%'.trim($req->input('num_recibo')).'%');
						}
						if($req->input('emision_desde_hasta')){
							$fechaPeriodo = explode(' - ', $req->input('emision_desde_hasta'));
							$desde     = $this->formatoFechaEntrada(trim($fechaPeriodo[0])).' 00:00:00';
							$hasta     = $this->formatoFechaEntrada(trim($fechaPeriodo[1])).' 23:59:59';
							$data = $data->whereBetween('fecha_emision',array($desde,$hasta));
						}
						$data = $data->get();
				}
			
			return response()->json(['data'=>$data]);
		   }


		   public function getResumenFormaCobro(Request $req)
		   {
					$data = [];
					$id_empresa = $this->getIdEmpresa();
					
					if($req->input('listIdFormaCobro')){
							$data = DB::table('vw_forma_cobro_listado');
							$data = $data->where('id_empresa',$id_empresa);
							$data = $data->whereIn('id',$req->input('listIdFormaCobro'));
							$data = $data->where('activo',true);
							$data = $data->whereIn('abreviatura',$req->input('abreviatura'));
							$data = $data->get();
							if($req->input('saldos')!== null){
								foreach($req->input('saldos') as $key =>$resultado){
									$indice = explode('_', $resultado);
									foreach($data as $key1=>$valor){
										if((int)$indice[0] == (int)$valor->id){
											if((int)$valor->id == 111){
												$data[$key1]->importe = number_format($indice[1],0,",","");
											}else{
												$data[$key1]->importe = number_format($indice[1],2,",","");
											}
												
										}
									}
								}
							}
					}
					// dd('fin');
					return response()->json(['data'=>$data]);
		   }


		   public function saveAplicarDeposito(Request $req)
		   {
			$id_empresa = $this->getIdEmpresa();
			$total_deposito = 0;
			$txt_deposito = 'OK';
			$err = true;
			$nro_deposito = 0;

			$moneda = Currency::where('currency_id',$req->input('id_moneda'))->first()->currency_code;
			$bancos =BancoDetalle::with('banco_cabecera')->where('id', $req->input('id_banco_detalle'))->first();

			if($req->input('concepto') != ""){
				$conceptoPantalla = " - ".$req->input('concepto');
			}else{
				$conceptoPantalla = $req->input('concepto');
			}
			$boleta = 'BOL NRO '.$req->input('num_operacion')." - ";
			$concepto = 'DEP '. $boleta."".$bancos->banco_cabecera->nombre."  ".$moneda."".$req->input('total_deposito')." - ".$req->input('recibo_concepto')."".$conceptoPantalla;
			/**
			 * =================================================
			 * 		REGLAS DE VALIDACION
			 * =================================================
			 */
			//SI EL LISTADO ESTA VACIO

			 $list_forma_cobro = json_decode($req->input('list_forma_cobro'));
				if(count($list_forma_cobro) == 0){
					return response()->json(['err'=>false,'txt'=>'Error al momento de obtener las formas de cobro.']);
				}

		

				$f_cobro = DB::table('vw_forma_cobro_listado');
				$f_cobro = $f_cobro->where('id_empresa',$id_empresa);
				$f_cobro = $f_cobro->whereIn('id',$list_forma_cobro );
				$f_cobro = $f_cobro->where('id_moneda',$req->input('id_moneda'));
				$f_cobro = $f_cobro->where('depositable',true);
				$f_cobro = $f_cobro->where('activo',true);
				$f_cobro = $f_cobro->where('saldo','>', 0);
				$f_cobro = $f_cobro->get();

				try{
					DB::beginTransaction();
	
					if(count($f_cobro) == 0){
						return response()->json(['err'=>false,'txt'=>'La moneda no coincide con los elementos selecciondos.']);
					}
					
					//TODO: Revisarlo porque aqui se esta confiando en el calculo realizado del lado del cliente, debemos calcular de este lado.
					$total_deposito = str_replace(',','.', str_replace('.','',$req->input('total_deposito')));   

					if($total_deposito <= 0){
						return response()->json(['err'=>false,'txt'=>'El monto a depositar es de 0.']);
					}

			
					if(!$req->input('adjunto')){
						throw new ExceptionCustom("Falta adjunto de deposito");
					} 
				

					$data = [];
					$forma_cobro_list = [];
					$err = true;
					$idEmpresa = $this->getIdEmpresa();
					$idUsuario = $this->getIdUsuario();
					$base_nro = DB::select("SELECT public.get_documento(".$idEmpresa.",'DEPOSITO')");
					$nro_deposito = $base_nro[0]->get_documento;

					$depositoBancario = new DepositoBancario;
					$depositoBancario->nro_deposito = $nro_deposito;
					$depositoBancario->id_usuario = $idUsuario;
					$depositoBancario->id_moneda = $req->input('id_moneda');
					$depositoBancario->id_empresa = $idEmpresa;
					$depositoBancario->id_sucursal = $req->input('id_sucursal');
					$depositoBancario->id_centro_costo = $req->input('id_centro_costo');
					$depositoBancario->id_banco_detalle = $req->input('id_banco_detalle');
					$depositoBancario->monto_total = $total_deposito;
					$depositoBancario->nro_operacion = $req->input('num_operacion');
					$depositoBancario->cotizacion_contable_venta = str_replace('.','',$req->input('cambio'));
					$depositoBancario->cotizacion_contable_original = str_replace('.','',$req->input('cambio'));
					$depositoBancario->fecha_emision =  $this->formatoFechaEntrada(trim($req->input('fecha_emision')));
					$depositoBancario->concepto = $concepto;
					$depositoBancario->adjunto = $req->input('adjunto');
			   
		
					$depositoBancario->save();
					$saldo = json_decode($req->input('list_saldo'));
					$importe = json_decode($req->input('list_importe'));

					foreach($list_forma_cobro as $key => $value){
						$forma_cobro_list[] = [
							'id_forma_cobro'=> $value,
							'id_cabecera'=> $depositoBancario->id,
							'importe'=>$saldo[$key],
							'saldo'=>$importe[$key]
						];

					}

					DB::table('deposito_bancario_detalle')->insert($forma_cobro_list);
					$this->depositar_recaudaciones($depositoBancario->id);

				
					DB::commit();

				} catch(ExceptionCustom $e){
					//Aqui cae la exepcion creada manualmente
					Log::error($e);
					$txt_deposito = $e->getMessage();
					$err = false;
				} catch(\Exception $e){
					//Lo que no sabemos que es cae aqui, no le mostramos al cliente pero se registra en el log
					Log::error($e);
					$txt_deposito = 'Ocurrio un error al momento de intentar hacer el deposito';
					$err = false;
					DB::rollBack();
				}
		
				
				return response()->json(['err'=>$err,'txt'=>$txt_deposito,'nro_deposito'=>$nro_deposito]);
		   }


	 public function reporteDepositoRecaudaciones()
	 {

		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$bancoDetalle = BancoDetalle::whereHas('banco_cab',function($query){
			$query->where('id_empresa',$this->getIdEmpresa());
			$query->where('cuenta_bancaria',true);
			$query->where('activo',true);
		})->with('banco_cab','currency','tipo_cuenta')->get();

		return view('pages.mc.cobranzas.reporteDepositoRecaudaciones',compact('currency','bancoDetalle'));
	 }	   

	 public function ajaxreporteDepositoCabecera(Request $req)
	 {
		$data = DB::table('vw_deposito');
		$data = $data->where('id_empresa',$this->getIdEmpresa());
		if($req->input('id_moneda')){
			$data = $data->where('id_moneda',$req->input('id_moneda'));
		}

		if($req->input('periodo_emision')){
			$fecha = explode(' ',$req->input('periodo_emision'));
			$desde     = $fecha[0].' 00:00:00';
			$hasta     = $fecha[1].' 23:59:59';

			$data = $data->whereBetween('fecha_emision',array($desde,$hasta));
		}
		$data = $data->get();
		return response()->json(['data'=>$data]);

	 }


	 public function ajaxreporteDepositoDetalle(Request $req)
	 {

		$data = DB::table('vw_reporte_detalle_deposito_bancario');
		$data = $data->where('id_empresa',$this->getIdEmpresa());
		$data = $data->where('id_cabecera',$req->input('id_cabecera'));
		$data = $data->get();
		
		// dd($data);

		return response()->json(['data'=>$data]);
	 }
		   
		   

	/**
	 * =================================================================================================
	 *       									CANJE 
	 * =================================================================================================
	 */

	 public function indexCanje()
	 {

		$clientes = DB::select("SELECT id,documento_identidad,dv, CONCAT(nombre||' ',apellido,' '||denominacion_comercial) AS cliente_n  FROM personas 
							  WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
							  							WHERE puede_facturar = true) 
							  AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");

		return view('pages.mc.cobranzas.listadoCanje', compact('clientes'));

	 }

	 public function  canjeTable(Request $req)
	 {

		// $idEmpresa = $this->getIdEmpresa();
		$data = DB::table('vw_forma_cobro_listado');
		$data = $data->where('id_empresa',$this->getIdEmpresa());
		$data = $data->where('id_tipo_pago',6);
		$data = $data->where('activo',true);
		$data = $data->whereNull('id_libro_compra');

		if($req->input('id_cliente')){
			$data = $data->where('id_cliente',$req->input('id_cliente'));
		}

		if($req->input('id_moneda')){
			$data = $data->where('id_moneda',$req->input('id_moneda'));
		}

		$data = $data->get();

		// dd($data);

		return response()->json(['data'=>$data]);
	 }


	/**
	 * =================================================================================================
	 *       						ANTICIPO - CLIENTE --- EN PROCESO 03/02/2020
	 * =================================================================================================
	 */


	 	//ANTICIPO - ANTERIOR 
		public function add(Request $request)
		{

			$currency = Currency::where('activo', 'S')->orderBy('currency_code', 'DESC')->get();	
			$formaPagos = FormaPagoCliente::all();		
			return view('pages.mc.cobranzas.add', compact('currency','formaPagos'));	
		}


		//ANTICIPO - ANTERIOR
		public function getListadoPago(Request $request){

			if($request->input('tipo') == 2){
				//$lista = Proforma::where('estado_id','!=',5)->get(['id']);	
				$lista = DB::select('SELECT proformas.id, personas.nombre as descripcion from proformas, personas where proformas.estado_id != 5 and proformas.pasajero_id = personas.id');
			}else{
				$lista = DB::select('SELECT grupos.id, grupos.denominacion as descripcion from grupos where grupos.estado_id != 12');
			}
			
			return response()->json($lista);
		}

	 ////////////////////////////ANTICIPO NUEVO/////////////////////////////////////////

	/**
	 * Index de Anticipos de tipo cliente, carga los datos en la vista
	 *  */	
	 public function indexAnticipoCliente()
	 {
		  $currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		  $beneficiario = DB::select("SELECT id,
											CONCAT(nombre||' ',apellido,' '||denominacion_comercial) AS cliente_n,
										CASE 
											WHEN dv is not null THEN
												CONCAT(documento_identidad||'-',dv) 
											ELSE 	
												documento_identidad
										END AS documento_n FROM personas 
													  WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
							  							WHERE puede_facturar = true) 
							  AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");	
		 $grupos = Grupo::whereNotIn('estado_id',[12])->get();
		 $estados = EstadoFactour::where('id_tipo_estado',16)->get(); 
        return view('pages.mc.cobranzas.anticipoIndex',compact('currency','beneficiario','grupos','estados'));
	 }


	 /**
	 * Funcion ajax que retorna el listado de anticipos al datatable de la vista index de anticipo
	 *  */	
	public function getListAnticipoCliente(Request $req) 
	{ 
	   // dd($req->all());
	   $data = new Anticipo;
	   $data = $data->with('beneficiario_format','estado','currency','recibo','proforma')
	   ->selectRaw("to_char(fecha_pago,'DD/MM/YYYY') as fecha_format_pago,
				   to_char(fecha_vencimiento,'DD/MM/YYYY') as fecha_format_vencimiento,
				   to_char(fecha,'DD/MM/YYYY') as fecha_format_anticipo,
				   *");
			 
	   $data = $data->where('id_empresa',$this->getIdEmpresa());       
	   $data = $data->where('tipo','C');//OBTIENE LOS ANTICIPOS DE TIPO CLIENTE 
	   if($req->input('fecha_anticipo')){
		   //$data = $data->where('fecha_transferencia',$req->input('fecha_emision')); 
		   $fecha = explode('-', $req->input('fecha_anticipo'));
		   $desde = $this->formatoFechaEntrada(trim($fecha[0]));
		   $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
		   $data = $data->whereBetween('fecha', array(
								 date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
								 date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
							   ));
		 }
 
	   if($req->input('nro_anticipo')){
		 $data = $data->where('id',$req->input('nro_anticipo'));
	   }
	   if($req->input('id_beneficiario')){
		   $data = $data->where('id_beneficiario',$req->input('id_beneficiario'));
	   }
	   if($req->input('id_moneda')){
		 $data = $data->where('id_moneda',$req->input('id_moneda'));
	   }
	  /* if($req->input('estado')){
		 if($req->input('estado') == 3){ 
			 $data = $data->where('activo', false);  
		 }else{
			 if($req->input('estado') == 1){ 
				 $data = $data->where('saldo','>',0);
				 $data = $data->where('activo',true);  
			 }else if($req->input('estado') == 2){ 
			   $data = $data->where('saldo','<=',0);
			   $data = $data->where('activo',true);  

			 }
		 }
		 //data = $data->where('pendiente',$req->input('pendiente'));
	   }*/
	   if($req->input('numProforma')){
		   $data = $data->where('id_proforma',$req->input('numProforma'));  
	   }
	   $data = $data->get();
	   $arrayBase = [];
	   foreach($data as $key=>$base){
			$arrayBase[$key] = $base;
			if($base->id_proforma != ""){
				$usuario = DB::select("SELECT CONCAT(personas.nombre,' ',personas.apellido) as usuario_proforma FROM personas, proformas WHERE personas.id = proformas.id_usuario AND proformas.id =".$base->id_proforma);
				$usuarioProforma = $usuario[0]->usuario_proforma;;
			}else{
				$usuarioProforma = "";
			}
			$arrayBase[$key]['usuario_proforma'] = $usuarioProforma;
	   }
	   $resultadoBase = $data;

	   if($req->input('nro_recibo')){
		   $resultados = [];
		   foreach($resultadoBase as $key=>$resultado){
			 if($resultado->recibo->nro_recibo == $req->input('nro_recibo')){
			   $resultados[] = $resultado;
			 }
		   }
		   $resultadoBase = [];
		   $resultadoBase = $resultados;
		 }


		 if($req->input('estado') && count($req->input('estado'))){
			$resultados = [];
			$estados_seleccionados = $req->input('estado');
			foreach($resultadoBase as $key=>$resultado){
				$coincidencia = strpos($resultado->saldo, 'e-');
				if ($coincidencia === false) {
				}else{
					$resultado->saldo = 0;
				}
				if($resultado->activo == false){
					$resultado->estado = 'ANULADO';
				}else{
					if($resultado->saldo > 0){
						$resultado->estado_detalle = 'PENDIENTE';
					}else{
						$resultado->estado_detalle = 'APLICADO';
					}
				}
		
				if(in_array(3, $estados_seleccionados)){ 
					if($resultado->activo == false){
						$resultados[] = $resultado;
					}
				}
				if(in_array(1, $estados_seleccionados)){ 
					if($resultado->activo == true && $resultado->saldo > 0){
						$resultados[] = $resultado;
					}
				}
				if(in_array(2, $estados_seleccionados)){ 
					if($resultado->activo == true && $resultado->saldo <= 0){
						$resultados[] = $resultado;
					}
				}
				
			}

			$resultadoBase = [];
			$resultadoBase = $resultados;
		}

	   return response()->json(['data'=>$resultadoBase]);
   }//
   
	   /**
	 	* Carga la vista de Agregar Anticipo de tipo cliente 
	 	* */
	   public function anticipoAddCobranza()
	   {

			//Limitado a USD y PYG porque no tenemos desarrollado la aplicacion de anticipo en otras monedas
			$currency = Currency::where('activo', 'S')->whereIn('currency_id',[111,143])->orderBy('currency_id', 'DESC')->get();

			$banco = BancoDetalle::whereHas('banco_cabecera', function ($query) {
			$query->where('id_empresa', $this->getIdEmpresa());
				})->with('currency','tipo_cuenta')
			->get();
			$beneficiario = DB::select("SELECT id,
									CONCAT(nombre||' ',apellido,' '||denominacion_comercial ,'-'||id ) AS cliente_n,
								CASE 
									WHEN dv is not null THEN
										CONCAT(documento_identidad||'-',dv) 
									ELSE 	
										documento_identidad
								END AS documento_n FROM personas 
								WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
															WHERE puede_facturar = true) 
								AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");	  
			$centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get(); 
			$cuentas_contables = PlanCuenta::where('activo',true)->where('id_empresa',$this->getIdEmpresa())->where('asentable', true)->get();
			$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
			$gestor = Persona::where('id_empresa',$this->getIdEmpresa())->where('id_tipo_persona',24)->where('activo',true)->get(['id', 'nombre','apellido']);	
			//$proforma_contado= Proforma::where('tipo_factura_id', '=', '1');

			return view('pages.mc.cobranzas.anticipoAdd',compact('sucursalEmpresa','centro','currency','beneficiario','banco','cuentas_contables','gestor'));


	   }//

	 /**
	  * Obtener listado de grupos y proformas asignado al al cliente enviado 
	  * desde el listado de agregar anticipo
	  */  
	  public function getLisAddAnticipo(Request $req)
	  {
			 $proformas = [];
			 $grupos = [];
			 $ventas = [];
			 $resultas = [];
			 $resultado = [];
		 if($req->input('cliente_id')){
				 //PROFORMAS NO FACTURADAS Y ANULADAS
				 $proformas = Proforma::where('id_empresa',$this->getIdEmpresa())
										 ->where('cliente_id',$req->input('cliente_id'))
										 ->whereNotIn('estado_id',[4,5])//QUE NO SEAN FACTURADOS Y ANULADOS 
										 ->where('tipo_factura_id', '=', '1')
										 ->get(['id']);
				 $resultado = [];

				 $proformaClientes = ProformaCliente::where('id_persona',$req->input('cliente_id'))->get(['id_proforma']);
				 foreach($proformas as $key1=>$proforma){
					 if($proforma->id != ""){
						 $resultado[]['id'] = $proforma->id;
					 }
				 }
 
				 foreach($proformaClientes as $key1=>$proformaCliente){
						 $resultado[]['id'] = $proformaCliente->id_proforma;
				 }
				 foreach($resultado as $key1=>$result){
					 $resultas[]['id'] = $result['id'] ;
				 }
				 
				 $grupos = Grupo::whereNotIn('estado_id',[12])// QUE NO SEA CANCELADO
				 ->where('cliente_id',$req->input('cliente_id'))
				 ->where('empresa_id',$this->getIdEmpresa())->get(['id','denominacion']);
			 
				 $ventas = VentasRapidasCabecera::whereNotIn('id_estado',[73,74])//QUE NO SEAN FACTURADOS Y ANULADOS
				 ->where('id_cliente',$req->input('cliente_id'))
				 ->where('id_empresa',$this->getIdEmpresa())->get(['id','nro_venta']);
		 }
 
		 return response()->json(['proformas'=>$resultado,'grupos'=>$grupos,'ventas'=>$ventas]);
	 }

	public function getLisAddAnticipoProveedor(Request $req)
	 {
			$proformas = [];
			$grupos = [];
			$ventas = [];

		if($req->input('cliente_id')){
				//PROFORMAS NO FACTURADAS Y ANULADAS
				$proformas = Proforma::where('id_empresa',$this->getIdEmpresa())
				->whereNotIn('estado_id',[5])->get(['id']);//QUE NO SEAN FACTURADOS Y ANULADOS
			
				$grupos = Grupo::whereNotIn('estado_id',[12])// QUE NO SEA CANCELADO
				->where('empresa_id',$this->getIdEmpresa())->get(['id','denominacion']);

				$ventas = VentasRapidasCabecera::whereNotIn('id_estado',[74,75])
				->where('id_empresa',$this->getIdEmpresa())->get(['id']);
		}
		return response()->json(['proformas'=>$proformas,'grupos'=>$grupos,'ventas'=>$ventas]);
	}
	/**
	 * Carga la vista de detalles del anticipo
	 */
	public function anticipoDetalle($id)
	{
			$anticipo = Anticipo::with('recibo')
								->where('id',$id)
								->where('id_empresa',$this->getIdEmpresa())
								->first();
			
			if(!isset($anticipo->id)){
				flash('El anticipo solicitado no existe. Intentelo nuevamente !!')->error();
				return redirect()->route('home');
			}
					
					
			$btn =  $this->btnPermisos([],'detalleAnticipo');
			$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
			// $banco = BancoDetalle::whereHas('banco_cabecera', function ($query) {
			// $query->where('id_empresa', $this->getIdEmpresa());
			// 	})->with('currency','tipo_cuenta')
			// ->get();
			$beneficiario = DB::select("SELECT id,
									CONCAT(nombre||' ',apellido,' '||denominacion_comercial ,'-'||id ) AS cliente_n,
								CASE 
									WHEN dv is not null THEN
										CONCAT(documento_identidad||'-',dv) 
									ELSE 	
										documento_identidad
								END AS documento_n FROM personas 
								WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
															WHERE puede_facturar = true) 
								AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");	  
			$centro = $centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();  
			$cuentas_contables = PlanCuenta::where('id_empresa',$this->getIdEmpresa())->where('asentable', true)->get();
			$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
			// dd($anticipo);
			return view('pages.mc.cobranzas.anticipoDetalle',compact('sucursalEmpresa','centro','currency','beneficiario','cuentas_contables','anticipo','btn'));

	}//

	   /**
		* Almacena los datos del Anticipo del cliente
		*/
	   public function saveAnticipoCliente(Request $req)
	   {
				    
					$this->validate($req, [
					'concepto' => 'required|string',
					'importe'=>'required|numeric',
					'cotizacion' => 'required|numeric',
					'id_moneda'=>'required|numeric',
					'id_cuenta_contable'=>'required|numeric',
					// 'fecha_pago'=>'required',
					// 'fecha_vencimiento'=>'required',
					//   'id_centro_costo'=>'required|numeric',
					'id_cliente'=>'required|numeric',
					'id_sucursal'=>'required|numeric'
				]);
					$idUsuario =  $this->getIdUsuario();
					$idEmpresa = $this->getIdEmpresa();
					$respuesta_crear_anticipo = new \StdClass;
					$respuesta_crear_anticipo->err = true;
					$respuesta_crear_recibo = [];
					$rec_ok = 0;
					$ant_ok = 0;
					$nro_recibo = 0;
					$concepto_anticipo = $req->input('concepto'); 
					$concepto_forma_pago = "";

					$bancoConcepto = BancoDetalle::with('banco_cabecera')->where('id',$req->input('id_banco_detalle'))->first();
					$banco_concepto ="";
					if(isset($bancoConcepto->banco_cabecera->nombre)){
						$banco_concepto = $bancoConcepto->banco_cabecera->nombre;
					}

					$cuenta_concepto ="";
					if(isset($bancoConcepto->numero_cuenta)){
						$cuenta_concepto = $bancoConcepto->numero_cuenta;
					}

					try{  

						DB::beginTransaction();
						

						$anticipo = new Anticipo;
						$anticipo->concepto = $req->input('concepto');
						$anticipo->importe = $req->input('importe');
						$anticipo->saldo = $req->input('importe');
						$anticipo->cotizacion = $req->input('cotizacion');
						$anticipo->id_moneda = $req->input('id_moneda');
						$anticipo->id_venta = (int)$req->input('id_venta');
						$anticipo->documento = $req->input('nro_cheque');
						$anticipo->id_banco_detalle = $req->input('id_banco_detalle');
						$anticipo->id_op = $req->input('id_op');
						$anticipo->id_cuenta_contable = $req->input('id_cuenta_contable');
						$anticipo->id_usuario = $idUsuario;
						$anticipo->fecha_pago = $req->input('fecha_pago');
						$anticipo->fecha_vencimiento = $req->input('fecha_vencimiento');
						$anticipo->id_centro_costo = $req->input('id_centro_costo');
						$anticipo->id_beneficiario = $req->input('id_cliente');
						$anticipo->id_grupo = $req->input('id_grupo');
						$anticipo->id_proforma = $req->input('id_proforma');
						$anticipo->id_sucursal = $req->input('id_sucursal');
						$anticipo->saldo = $req->input('importe');
						$anticipo->pendiente = false;
						$anticipo->id_tipo_documento = 20;//TIPO CTA CTE ANTICIPO
						$anticipo->id_empresa = $idEmpresa;
						$anticipo->id_estado = 63; //ESTADO PENDIENTE
						$anticipo->tipo = "C"; //ANTICIPO TIPO CLIENTE
						$anticipo->save();
						DB::commit();
						$idAnticipo = $anticipo->id;
//////////////////////////////////////////////////////////////////////////////////////////////////////////
						if($req->input('id_cliente') !=""){
							$persona = Persona::where('id',$req->input('id_cliente'))->first();
							$beneficiario_concepto =  $persona->nombre."". $persona->apellido;
						}
						$importe_total = (float)$req->input('importe');

						$concepto_final = "ANTICIPO NRO ".$idAnticipo."-FECHA ".date('d/m/Y', strtotime($req->input('fecha_pago')))."-IMPORTE ".$importe_total;
						
						if($concepto_forma_pago != ""){
							$concepto_final .= "-MODO DE PAGO ".$concepto_forma_pago;
						}
						if($banco_concepto != ""){
							$concepto_final .= "-BANCO ".$banco_concepto;
						}
						if($cuenta_concepto != ""){
							$concepto_final .= "-CTA ".$cuenta_concepto;
						}
						if($beneficiario_concepto != ""){
							$concepto_final .= "-BENEFICIARIO ".$beneficiario_concepto;
						}
						if($req->input('id_grupo') != ""){
							$concepto_final .= "-GRUPO ".$req->input('id_grupo');
						}
						if($req->input('id_proforma') != ""){
							$concepto_final .= "-PP ".$req->input('id_proforma');
						}

						if((int)$req->input('id_venta') != ""){
							$concepto_final .= "-VTA ".(int)$req->input('id_venta');
						}

						if($concepto_anticipo != ""){
							$concepto_final .= "- ".$concepto_anticipo;
						}

						Anticipo::where('id', $idAnticipo)
								->update(['concepto' => $concepto_final]); 


						$id_sucursal_documento = SucursalEmpresa::find($req->input('id_sucursal'))->id_persona;
						$file_seq = DB::select('SELECT public."get_documento_recibo"(?,?)', 
								[ 
									$this->getIdEmpresa(),
									$id_sucursal_documento,// id_persona
									]);
						    //($file_seq);
						$nro_recibo = $file_seq[0]->get_documento_recibo;
			
						//DATOS PARA CREAR RECIBO
						$recibo = new \StdClass;
						$recibo->nro_recibo = $nro_recibo;
						$recibo->sucursal = $req->input('id_sucursal');
						$recibo->centro_costo = $req->input('id_centro_costo');
						$recibo->fecha = $req->input('fecha_pago');
						$recibo->id_gestor = $req->input('id_gestor');
						$recibo->cotizacion = $req->input('cotizacion');
						$recibo->concepto = $req->input('concepto');
						$recibo->total_pago = $req->input('importe');
						$recibo->id_anticipo = $anticipo->id;
						$recibo->id_moneda = $req->input('id_moneda');
						$recibo->id_cliente = $req->input('id_cliente');
						$recibo->id_tipo_operacion_recibo = 2;//TIPO RECIBO COBRANZA
		
						$respuesta_crear_recibo = $this->crearReciboAnticipo($recibo);
						// dd($respuesta_crear_recibo);

						if(!$respuesta_crear_recibo->estado_err){
							$respuesta_crear_anticipo->err = false;
						} else {
							//DEVOLVER EL NUMERO DE RECIBO
							$respuesta_crear_anticipo->nro_recibo = $nro_recibo;

							//ACTUALIZAR ANTICIPO CON EL RECIBO CREADO
							Anticipo::where('id',$anticipo->id)
							->update(['id_recibo'=> $respuesta_crear_recibo->id_recibo]);
						}

			 	 } catch(\Exception $e){
					Log::error($e);
					Anticipo::where('id',$anticipo->id)->delete();
					$respuesta_crear_anticipo->err = false;
					if(env('APP_DEBUG')){
						$respuesta_crear_anticipo->e = $e;
					}
					$ant_ok++;
					DB::rollBack();
				} 

				
				return response()->json($respuesta_crear_anticipo);


	   }//
	   
	/**
	* INSERTAR RECIBO PARA EL ANTICIPO CREADO
	*/
	private function crearReciboAnticipo($obj)
	{		
		// dd($obj);
		$resp = new \StdClass;
		$resp->estado_err = true;
		$idEmpresa = $this->getIdEmpresa();
		$idUsuario = $this->getIdUsuario();
		$nombreCliente = '';

		$cliente = Persona::where('id',$obj->id_cliente)->first();
		if(!is_null($cliente->getFullNameAttribute())){
			$nombreCliente = $cliente->getFullNameAttribute();
		} else {
			$nombreCliente = $cliente->denominacion_comercial;
		}
			// dd($nombreCliente);
		$concepto = 'REC '.$obj->nro_recibo.' - ANTICIPO '.$obj->id_anticipo.' '.$nombreCliente;

		try{
			
			DB::beginTransaction();

			$recibo = new Recibo;
			$recibo->fecha_hora_creacion = date('Y-m-d H:i:00');
			$recibo->id_usuario_creacion = $idUsuario;
			$recibo->id_empresa 		 = $idEmpresa;
			$recibo->id_sucursal 		 = $obj->sucursal;
			$recibo->id_estado 			 = 31;//ESTADO GENERADO
			$recibo->id_cliente 		 = $obj->id_cliente;
			$recibo->importe 			 = $obj->total_pago;
			$recibo->total_anticipo 	 = $obj->total_pago;
			$recibo->total_pago 		 = $obj->total_pago;
			$recibo->id_centro_costo 	 = $obj->centro_costo;
			$recibo->fecha_emision  	 = date('Y-m-d');
			$recibo->id_moneda 			 = $obj->id_moneda;
			$recibo->id_gestor 			 = $obj->id_gestor;
			$recibo->nro_recibo 		 = $obj->nro_recibo;
			$recibo->cotizacion 		 = $obj->cotizacion;
			$recibo->concepto 			 = $concepto;
			$recibo->id_tipo_operacion_recibo = 2;//TIPO RECIBO COBRANZA
	
			$recibo->save();
			$reciboDetalle = [];
			

		
				$reciboDetalle[] = [
					'id_cabecera'=>	$recibo->id,
					'id_libro_venta' =>  null,
					'id_libro_compra' =>  null,
					'id_anticipo' =>  $obj->id_anticipo,
					'importe' => (float)$obj->total_pago
				];
				DB::table('recibos_detalle')
					->insert($reciboDetalle); 
			//DEVOLVER ID DE RECIBO	
			$resp->id_recibo = $recibo->id;

			//FUNCION PARA GENERAR RECIBO	
			DB::select('SELECT generar_recibo('.$recibo->id.','.$idUsuario.')');
			
			// DB::rollBack();
			DB::commit();
		} catch(\Exception $e){
			Log::error($e);
			$resp->estado_err = false;
			if(env('APP_DEBUG')){
				$resp->e = $e;
			}
			DB::rollBack();
		}

		return $resp;

		
	}

		/**
	 * @param id_cliente recibe como parametro para obtener anticipos
	 * @return array Anticipos para el combo de la vista de emitir recibo
	 */
	public function getAnticipoList(Request $req)
	{
		// dd($req->all());
		$anticipos = Anticipo::with('currency')
		->where('tipo','C') //TIPO CLIENTE
		->where('id_empresa',$this->getIdEmpresa())
		->where('id_beneficiario',$req->input('id_cliente'))
		->where('id_estado',77)//ANTICPOS COBRADOS
		->where('saldo','>',0)->get(); 

		return response()->json(['data'=>$anticipos]);

	}


	/**
	 * =================================================================================================
	 *       					CUENTA CORRIENTE CLIENTE
	 * =================================================================================================
	 */

	public function indexCttaCorrienteCliente(Request $request)
	{
        $personas = DB::select("SELECT p.id,CONCAT(p.nombre, p.apellido,'-'||p.documento_identidad,'-'||tp.denominacion) AS data_n
								FROM personas p
								JOIN tipo_persona tp on tp.id = p.id_tipo_persona
								WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
								AND p.activo = true AND id_empresa = ".$this->getIdEmpresa());  					

		$lineaCredito = [];		
		$currencys = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();

    
		return view('pages.mc.cobranzas.cuentaCorrienteCliente', compact('personas', 'currencys', 'lineaCredito'));
	}	

	public function getCuentaCorrienteCliente(Request $req)
	{
		$data = DB::table('vw_forma_cobro_listado');
		$data = $data->where('id_empresa',$this->getIdEmpresa());
		$data = $data->where('tipo','C');
		

		if($req->input('persona')){
			$data = $data->where('id_persona',$req->input('persona'));
		}

		if($req->input('idMoneda')){
			$data = $data->where('id_moneda',$req->input('idMoneda'));
		}
	
		$data = $data->get();

		return response()->json(['data'=>$data]);
	}

	public function consultarDetalleCuentasCliente(Request $request)
	{
		// dd($request->all());
		// $datos = $this->responseFormatDatatable($request);

        $query = "SELECT cuenta_corriente.id_persona, 
						 personas.nombre, 
						 cuenta_corriente.debe as debe,
						cuenta_corriente.haber as haber, 
						cuenta_corriente.saldo as saldo, 
						cuenta_corriente.tipo, 
						cuenta_corriente.id_moneda, 
						currency.currency_code,
						cuenta_corriente.fecha_hora, 
						cuenta_corriente.documento, 
						tipo_documento_ctacte.denominacion
					FROM cuenta_corriente, 
						 currency, 
						 personas, 
						 tipo_documento_ctacte
					WHERE cuenta_corriente.id_moneda = currency.currency_id
					AND cuenta_corriente.id_persona = personas.id
					AND cuenta_corriente.id_tipo_documento = tipo_documento_ctacte.id
					AND personas.id_empresa = ".$this->getIdEmpresa()." 
					AND cuenta_corriente.activo = true 
					AND cuenta_corriente.tipo = 'C' ";

		$totales_query = "SELECT 
					SUM(cc.debe) as total_debe, 
					SUM(cc.haber) as total_haber, 
					SUM(cc.debe) -  SUM(cc.haber)  AS saldo
				FROM cuenta_corriente cc, personas p
				WHERE  cc.activo = true 
					AND cc.tipo = 'C' 
					AND p.id = cc.id_persona
					AND p.id_empresa = ".$this->getIdEmpresa();
					

					if($request->input('persona_id')){
						$query .= " AND cuenta_corriente.id_persona = ".$request->input('persona_id')." ";
						$totales_query .= " AND cc.id_persona = ".$request->input('persona_id')." ";
					}

					if($request->input('moneda_id')){
						$query .=" AND cuenta_corriente.id_moneda = ".$request->input('moneda_id')." ";
						$totales_query .= " AND cc.id_moneda = ".$request->input('moneda_id')." ";
					}
					
					
	


		

		if($request->input('periodo')){
			// dd($request->input('periodo'));
			$fechaPeriodos = explode(' ', $request->input('periodo'));
			$desde     = $fechaPeriodos[0];
			$hasta     = $fechaPeriodos[1];
			$query 		   .= " AND cuenta_corriente.fecha_hora BETWEEN '".$desde." 00:00:00' AND '".$hasta." 23:59:59' ";
			$totales_query .= " AND cc.fecha_hora BETWEEN '".$desde." 00:00:00' AND '".$hasta." 23:59:59' ";
		}			

		$totales_query .= " GROUP BY tipo";
		$totales = DB::select($totales_query);

		foreach ($totales as $key => $total) 
		{
			$totales[$key]->total_debe = number_format($total->total_debe, 2, ",", ".");
			$totales[$key]->total_haber = number_format($total->total_haber, 2, ",", ".");
			$totales[$key]->saldo = number_format($total->saldo, 2, ",", ".");
		}
		
		if(empty($totales)){
			$totales = [];
		} else {
			$totales = $totales[0];
		}
		
		$query .= ' ORDER BY cuenta_corriente.id ASC';

		// dd($query);
		$resultado = DB::select($query);
		// dd($resultado);

		return response()->json(['data'=>$resultado, 'totales'=>$totales]);
	}	






   /**
	 * =================================================================================================
	 *       					IMPRIMIR RECIBO
	 * =================================================================================================
	 */



	public function correoRecibo()
	{

		Mail::to('ti@dtp.com.py')->send(new ReciboCobranza());

		return response()->json(['err'=>true]);
	}  
	
   public function imprimirRecibo(Request $req,$id)
   {

			$id_recibo = $id;
			$id_usaurio_anticipo = 0;
			$obj = new \StdClass; 
			$fp_recibo = [];
			$recibos = new Recibo; 
			$recibos = $recibos->with(['detalle.libroVenta','detalle.libroVenta.pasajero','detalle.libroCompra','detalle.anticipo.aplicacion.factura','cliente','estado','moneda','formaCobroDetalle.forma_cobro','usuario']);
			$recibos = $recibos->selectRaw("TO_CHAR(fecha_hora_creacion,'DD/MM/YYYY') as fecha_creacion_format,
															substring(concepto for 10)||'...' as concepto_cortado,
															TO_CHAR(fecha_hora_cobro,'DD/MM/YYYY') as fecha_hora_cobro_format,* ");
			$recibos = $recibos->where('id',$id_recibo);

			$recibos = $recibos->get();

    		$timbrado  = 0;
			foreach( $recibos as $kay=>$value){
				$value->totalLetras = $this->convertir($value->total_pago);
				foreach($value->detalle as $key=>$detalle){
					if($timbrado  == 0){
						if(isset($detalle->libroVenta->id_timbrado)){
							$timbrado  = $detalle->libroVenta->id_timbrado;
							// $id_sucursal = 0;
						}else{
							if(isset($detalle->anticipo->id_sucursal)){
								// $id_sucursal = $detalle->anticipo->id_sucursal;
								$timbrado  = 0;
							}else{
								// $id_sucursal = 0;
								$timbrado  = 0;
							}
						}
					}
				}
			}
			
			$empresa = Empresa::where('id',$this->getIdEmpresa())->first();

			$sucursalEmpresa =  Persona::where('id_empresa',$this->getIdEmpresa())
                                     ->where('id_tipo_persona', 21)
                                     ->where('activo', true)
									 ->get();
									 
			$fp_recibo = DB::table('vw_forma_cobro_listado');
			$fp_recibo = $fp_recibo->where('id_recibo',$id_recibo);
			$fp_recibo = $fp_recibo->where('activo',true);
			$fp_recibo = $fp_recibo->get();
			$nro_recibo = '0';
			$abreviatura = '';
			if(!is_null($fp_recibo)){ 
				$obj->id_recibo = 	$id_recibo;
				$rsp = $this->calcularDiferencia($obj);
				$diferencia = $rsp->diferencia;
				$nro_recibo = $recibos[0]->nro_recibo;
				if(isset($fp_recibo[0]->abreviatura)){
					$abreviatura = $fp_recibo[0]->abreviatura;
				}	
			}	
			if(isset($recibos[0]->detalle[0]->anticipo->id_proforma)){
				$proforma = Proforma::where('id',  $recibos[0]->detalle[0]->anticipo->id_proforma)
									->first();
				if(isset($proforma->id_usuario)){
					$id_usaurio_anticipo =	$proforma->id_usuario;
				}else{
					$id_usaurio_anticipo = 0;
				}
			}else{
				$id_usaurio_anticipo = 0;
			}

			if($id_usaurio_anticipo == 0 || $id_usaurio_anticipo == ""){ 
				if(isset($recibos[0]->detalle[0]->libroVenta->id_usuario)){
					$id_usuario = $recibos[0]->detalle[0]->libroVenta->id_usuario;
				}else{
					$id_usuario = $id_usaurio_anticipo;
				}
			}else{
				$id_usuario = $id_usaurio_anticipo;
			}


			if($id_usuario != ""){
				if($timbrado != 0){
					$logoSucursal = DB::select('select personas.logo
												from personas, timbrados
												where personas.id = timbrados.id_sucursal_empresa
												and timbrados.id ='.$timbrado);
				}else{
					$logoSucursal = DB::select('select personas.logo
													from personas, sucursales_empresa
													where personas.id = sucursales_empresa.id_persona
													and sucursales_empresa.id ='.$recibos[0]->id_sucursal);
				}
		
				if(isset($logoSucursal[0]->logo)){
					if($logoSucursal[0]->logo != "" && $logoSucursal[0]->logo != 'factour.png'){
						$baseLogo = $logoSucursal[0]->logo;
						$logo = asset("personasLogo/$baseLogo");
					}else{
						if(isset($comercioPersona[0]->logo)){
							$logo = $comercioPersona[0]->logo;
						}else{
							$logo = asset("logoEmpresa/$empresa->logo");
						}
					}
				}else{
					$logo = asset("logoEmpresa/$empresa->logo");
				}	
			}else{
				$logo = asset("logoEmpresa/$empresa->logo");
			}	


		    /*echo '<pre>';
			print_r($logo);
			die; */

			//IMPRIMIR ORIGINAL SIEMPRE
			$option = $req->input('option');

			$diferencia = $rsp->diferencia;
			$pdf = \PDF::loadView('pages.mc.cobranzas.reciboImpreso',compact('recibos','option','sucursalEmpresa','fp_recibo','empresa','logo'));
			// dd($pdf);
			$pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);

			$pdf->setPaper('a4', 'letter')->setWarnings(false);
				return $pdf->download('Recibo Dinero-'.$nro_recibo.'.pdf');

			// return view('pages.mc.cobranzas.reciboImpreso',compact('recibos','option','sucursalEmpresa','fp_recibo','empresa','abreviatura'));	

   }


   public function imprimirReciboDuplicadoLista(Request $req)
   {
   		ini_set('max_execution_time', 0);
		$recibos = new Recibo; 
		$recibos = $recibos->with(['usuario_generado' => function($query){
			$query->selectRaw("substring(concat(nombre,apellido) for 10)||'...' as full_name,*");
		},'cliente' => function($query){
			$query->selectRaw("substring(concat(nombre,apellido,denominacion_comercial) for 10)||'...' as full_name,*");
		}, 'moneda', 'sucursal', 'estado','tipo_recibo','sucursalEmpresa']);

		$recibos = $recibos->where('id_empresa',$this->getIdEmpresa());

		//CONTADOR DE RETENCIONES
		$recibos = $recibos->withCount(['detalle'=> function($query){
			$query->whereNotNull("importe_retencion");
		}]);

		$recibos = $recibos->selectRaw("TO_CHAR(fecha_hora_creacion,'DD/MM/YYYY') as fecha_creacion_format,
														substring(concepto for 10)||'...' as concepto_cortado,
														TO_CHAR(fecha_hora_cobro,'DD/MM/YYYY') as fecha_hora_cobro_format,
														CASE
															WHEN id_estado = 40 THEN 2
															WHEN id_estado = 56 THEN 3
															WHEN id_estado = 55 THEN 4
															WHEN id_estado = 31 THEN 5
															ELSE 0
														END AS orden,* ");
		if($req->input('id_cliente')){
			$recibos = $recibos->where('id_cliente',$req->input('id_cliente'));
		}
		if(!empty($req->input('id_estado'))){
			$recibos = $recibos->whereIn('id_estado',$req->input('id_estado'));
		}
		if($req->input('id_moneda')){
			$recibos = $recibos->where('id_moneda',$req->input('id_moneda'));
		}
		if($req->input('fecha_creacion')){
			$fecha = $req->input('fecha_creacion');
			$desde     = $fecha.' 00:00:00';
		    $hasta     = $fecha.' 23:59:59';
			// dd(array($desde,$hasta));
			$recibos = $recibos->whereBetween('fecha_hora_creacion',array($desde,$hasta));
		}

		$recibos = $recibos->orderBy('nro_recibo','DESC');
		$recibos = $recibos->get();
		///////////////////////////////////////////////////////////	
			/*echo '<pre>';
			print_r($recibos);*/

			//AGREGAR MONTO EN LETRAS
			foreach( $recibos as $value){
				$value->totalLetras = $this->convertir($value->total_pago);
			}
			//SI ES IMPRESION ORIGINAL, DUPLICADO O AMBOS
			$option = $req->input('option');

  			/*$comercioPersona= ComercioPersona::where('id', $idComercio)->get(['logo']);

            if(isset($comercioPersona[0]->logo)){
              $logo = $comercioPersona[0]->logo;
            }else{
              $logo = asset("logoEmpresa/$empresa->logo");
            }*/
			
			// return view('pages.mc.cobranzas.reciboImpreso',compact('recibos','option'));

			$pdf = \PDF::loadView('pages.mc.cobranzas.reciboImpreso',compact('recibos','option'));
			$pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);

			$pdf->setPaper('a4', 'letter')->setWarnings(false);
				return $pdf->download('Recibo Dinero-'.$this->getId4Log().'.pdf');

   }


	/**
	 * =================================================================================================
	 *       					FUNCIONES Y VARIABLES AUXILIARES
	 * =================================================================================================
	 */
   

/**
 * RECIBE NUMEROS Y CONVIERTE A LETRAS
 */
public static function convertir($number, $moneda = '', $centimos = '', $forzarCentimos = false)
{
	$converted = '';
	$decimales = '';
	if (($number < 0) || ($number > 999999999)) {
		return 'No es posible convertir el numero a letras';
	}
	$div_decimales = explode('.',$number);
	if(count($div_decimales) > 1){
		$number = $div_decimales[0];
		$decNumberStr = (string) $div_decimales[1];
		//SI VIENEN NUMERO SOLO LE RELLENA CON CERO
		$decNumberStr = str_pad($decNumberStr, 2, "0", STR_PAD_RIGHT);
		if(strlen($decNumberStr) == 2){
			$decNumberStrFill = str_pad($decNumberStr, 9, '0', STR_PAD_LEFT);
			$decCientos = substr($decNumberStrFill, 6);
			$decimales = self::convertGroup($decCientos);
		}
	}
	else if (count($div_decimales) == 1 && $forzarCentimos){
		$decimales = 'CERO ';
	}
	$numberStr = (string) $number;
	$numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
	$millones = substr($numberStrFill, 0, 3);
	$miles = substr($numberStrFill, 3, 3);
	$cientos = substr($numberStrFill, 6);
	if (intval($millones) > 0) {
		if ($millones == '001') {
			$converted .= 'UN MILLON ';
		} else if (intval($millones) > 0) {
			$converted .= sprintf('%sMILLONES ', self::convertGroup($millones));
		}
	}
	if (intval($miles) > 0) {
		if ($miles == '001') {
			$converted .= 'MIL ';
		} else if (intval($miles) > 0) {
			$converted .= sprintf('%sMIL ', self::convertGroup($miles));
		}
	}
	if (intval($cientos) > 0) {
		if ($cientos == '001') {
			$converted .= 'UN ';
		} else if (intval($cientos) > 0) {
			$converted .= sprintf('%s ', self::convertGroup($cientos));
		}
	}
	if(empty($decimales)){
		$valor_convertido = $converted . strtoupper($moneda);
	} else {
		$valor_convertido = $converted . strtoupper($moneda) . ' CON ' . $decimales . ' ' . strtoupper($centimos);
	}
	return $valor_convertido;
}
private static function convertGroup($n)
{
	$output = '';
	if ($n == '100') {
		$output = "CIEN ";
	} else if ($n[0] !== '0') {
		$output = self::$CENTENAS[$n[0] - 1];
	}
	$k = intval(substr($n,1));
	if ($k <= 20) {
		$output .= self::$UNIDADES[$k];
	} else {
		if(($k > 30) && ($n[2] !== '0')) {
			$output .= sprintf('%sY %s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
		} else {
			$output .= sprintf('%s%s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
		}
	}
	return $output;
}


	private function getIdUsuario()
	{
  
		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
  	}

  private function getIdEmpresa()
  {
   return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
  }


       /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
	private function formatoFechaEntrada($date)
	{
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
	}//function
	



	private function getId4Log()
	{
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}


	private function btnPermisos($parametros, $vista)
	{

				$btn = DB::select("SELECT pe.* 
									FROM persona_permiso_especiales p, permisos_especiales pe
									WHERE p.id_permiso = pe.id 
									AND p.id_persona = ".$this->getIdUsuario() ." 
									AND pe.url = '".$vista."' ");
		
				$htmlBtn = '';
				$boton =  array();
				$idParametro = '';
		
				// dd( $btn );
		
				if(!empty($btn)){
		
				foreach ($btn as $key => $value) {
		
				$idParametro = '';
				$ruta = 'factour.'.$value->accion;
				$htmlBtn = '';
				//LLEVA PARAMETRO
				if($value->lleva_parametro == '1'){
		
				foreach ($parametros as $indice=>$valor) { 
		
					if($indice == $value->nombre_parametro){
					$idParametro = $valor;
					}
				}
		
				//PARAMETRO OCULTO EN DATA
				if($value->parametro_oculto == '1'){
					$htmlBtn = "<a role='button' class='".$value->clase."' data-btn='".$idParametro."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
				} else {
					$htmlBtn = "<a role='button' href='".route($ruta,['id'=>$idParametro])."' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
				}
		
				} else {
					$htmlBtn = "<a role='button' href='#' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i>".$value->texto_alternativo." </a>"; 
				}
		
				
				$boton[] = $htmlBtn;
				}
				return $boton;
			
				} else {
				return array();
				}
		
	}//function

		/**
	 * Funcion que recibe datos array del datatable 
	 * ! Esta funcion ya no es necesaria y debe ser cambiada usando el serializeJSON ****
	 */
	private function  responseFormatDatatable($req)
	{
		    $datos = array();
		    $data =  new \StdClass;
		    
		    foreach ($req->formSearch as $key => $value) 
		    {
		     	$n = $value['name'];
		        $datos[$value['name']] = $value['value'];
		        $data-> $n = $value['value'];
		    }

		    return $data;
 	} 

    
	public function modificarRetenciones($id)
   {
   		$resultado = Retencion::with('usuario', 'persona','currency','empresa', 'libroVenta')
   							   ->where('id', $id)
   		                       ->get();
   		return view('pages.mc.cobranzas.editarRetencion', compact('resultado'));
   }

	public function doEditar(Request $request){
		$mensaje =  new \StdClass;
	   try{
			DB::table('retencion')
					        ->where('id',$request->input('retencion_id'))
					        ->update([
					        		'tipo_retencion'=>$request->input('id_tipo_retencion'),
					            	'fecha_retencion'=>$this->formatoFechaEntrada($request->input('fecha_documento')),
					            	'numero'=>$request->input('num_retencion')
					            	]);
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se han editados los datos correctamente';	        
		} catch(\Exception $e){
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se han editado los datos';		        

		}
		return response()->json($mensaje);


	}

	public function getContContable(Request $request){
		/*$cotizacion = DB::select('SELECT get_cotizacion_contable_actual(?,?)', [$this->getIdEmpresa(), $request->input("id_moneda")]);
		echo '<pre>';
		print_r('SELECT get_cotizacion_contable_actual('.$this->getIdEmpresa().','.$request->input("id_moneda").')');

		echo '<pre>';
		print_r($cotizacion);
		$cotizacion = $cotizacion[0]->get_cotizacion_contable_actual;*/
		$recibo = Recibo::where('id',$request->input('id_recibo'))->first(['cotizacion_especial','id_moneda']);

		
			if($request->input('id_moneda') == 111){
				$moneda = 143;
			}else{
				$moneda =$request->input('id_moneda');
			}
			$result = DB::select('SELECT get_cotizacion_moneda('.(integer) $moneda.','.$this->getIdEmpresa().') as cotizacion');
			$cotizacion = floatval($result[0]->cotizacion);
		

			if($recibo->id_moneda == 111 && $request->input('id_moneda') == 111){
				$cotizacion = 1;
			}


        return response()->json($cotizacion);
	}	

	public function getExtractoAnticipo(Request $request){
		$exacto =  DB::table('vw_extracto_anticipo');
		$exacto =  $exacto->where('nro_anticipo',$request->input("idFactura"));
		$exacto =  $exacto->get();
		return response()->json(['exacto'=>$exacto]);
  	}	

	public function getDesposito(Request $request){

		$resultado = DB::select("select asientos_contables.* 
									from deposito_bancario, asientos_contables 
									where deposito_bancario.id_asiento = asientos_contables.id
									and deposito_bancario.id =". $request->input("deposito_id"));
		return response()->json(['resultado'=>$resultado]);
  	}	
	
	public function guardarCotizacionEspecial(Request $request){
		$mensaje =  new \StdClass;
	    try{
			DB::table('recibos')
					        ->where('id',$request->input('id_recibo'))
					        ->update([
					        		'indice_cotizacion'=> str_replace(',','.', str_replace('.','',$request->input('cotizacion'))),
					            	'id_usuario_cotizacion_especial'=>$this->getIdUsuario(),
									'fecha_cotizacion_especial'=>date('Y-m-d h:m:s')
					            	]);
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se han editados los datos correctamente';	        
		} catch(\Exception $e){
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se han editado los datos';		        
		}

		return response()->json($mensaje);
	}



		public function getCotizacionFPR(Request $req)
		{	
			$recibo = Recibo::where('id',$req->input('id_recibo'))->first(['indice_cotizacion','id_moneda']);
			$cotizacion_indice = CotizacionIndice::where('id_empresa',$this->getIdEmpresa())
                        ->where('id_moneda_origen',$recibo->id_moneda)
                        ->where('id_moneda_destino',$req->input('id_moneda_costo'))
                        ->orderBy('id','DESC')
                        ->first();

			$base =  new \StdClass;
			$base->indice_cotizacion = $recibo->indice_cotizacion;
			if($cotizacion_indice){
				$base->indice_cotizacion = $cotizacion_indice->indice;
			}

			if($req->input('id_moneda_costo') == 111){
				$moneda = 143;
			}else{
				$moneda =$req->input('id_moneda_costo');
			}

			//Si es PYG a PYG
			if($recibo->id_moneda == 111 && $req->input('id_moneda_costo') == 111 ){
				$base->cotizacion = 1;
			} else {
				$base->cotizacion = DB::select('SELECT get_cotizacion_moneda('.(integer) $moneda.','.$this->getIdEmpresa().') as cotizacion')[0]->cotizacion;
			}

			

			return response()->json($base);
		}

		public function aplicacionAnticipos(Request $req)
		{
			$beneficiario = DB::select("SELECT id,
									CONCAT(nombre||' ',apellido,' '||denominacion_comercial) AS cliente_n,
								CASE 
									WHEN dv is not null THEN
										CONCAT(documento_identidad||'-',dv) 
									ELSE 	
										documento_identidad
								END AS documento_n FROM personas 
								WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
															WHERE puede_facturar = true) 
								AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");	  

			return view('pages.mc.cobranzas.aplicacionAnticipos',compact('beneficiario'));
		}

		public function getAplicacionAnticipos(Request $req){
			$resultado =[];
		
			$query = "SELECT id,id_beneficiario,fecha, beneficiario_nombre, beneficiario_apellido, nro_recibo,importe,saldo, moneda";
				
			$query .=" FROM vw_anticipo_clientes ";

					$query .="WHERE id_empresa = ".$this->getIdEmpresa();

			if($req->input('id_cliente') != 0){
			$query .="AND id_beneficiario =".$req->input('id_cliente');
			}

			if($req->input('fecha_emision') != ""){
				$fechaPeriodo= explode('-', $req->input('fecha_emision'));
				$check_in = explode('/', $fechaPeriodo[0]);
				$check_out = explode('/', $fechaPeriodo[1]);
				$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]).' 00:00:00';
				$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]).' 23:59:59';
				$query .="AND fecha BETWEEN '".$checkIn."' AND '".$checkOut."'";
			}

			$query .=" GROUP BY id, beneficiario_nombre, beneficiario_apellido, nro_recibo, importe,saldo, moneda,id_beneficiario,fecha";

			$anticipos = DB::select($query);
		
			if(!empty($anticipos)){
				$contador = 0;
				foreach($anticipos as $key=>$resumen){
					$detalleResumenes = DB::select("SELECT *
													FROM vw_anticipo_clientes
													WHERE monto is not null
													AND id = ".$resumen->id );	
					if(!empty($detalleResumenes)){
						$resultado[$contador] = $anticipos[$key];
						$resultado[$contador]->detalles = $detalleResumenes;
						$contador = $contador + 1;
					}
				}
			}	
			foreach($resultado as $key=>$st){
				$listado['data'][] = $st;	
			}
			if(empty($listado)){
					$listado['data'][] = $resultado;
			}
			return response()->json($listado);

		}


		public function aplicacionAnticiposPdf(Request $req)
		{
			ini_set('memory_limit', '-1');
			set_time_limit(300);
	
			$resultado =[];
		
			$query = "SELECT id,id_beneficiario,fecha, beneficiario_nombre, beneficiario_apellido, nro_recibo,importe,saldo, moneda";
				
			$query .=" FROM vw_anticipo_clientes ";

					$query .="WHERE id_empresa = ".$this->getIdEmpresa();

			if($req->input('id_cliente') != 0){
			$query .="AND id_beneficiario =".$req->input('id_cliente');
			}

			if($req->input('fecha_emision') != ""){
				$fechaPeriodo= explode('-', $req->input('fecha_emision'));
				$check_in = explode('/', $fechaPeriodo[0]);
				$check_out = explode('/', $fechaPeriodo[1]);
				$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]).' 00:00:00';
				$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]).' 23:59:59';
				$query .="AND fecha BETWEEN '".$checkIn."' AND '".$checkOut."'";
			}

			$query .=" GROUP BY id, beneficiario_nombre, beneficiario_apellido, nro_recibo, importe,saldo, moneda,id_beneficiario,fecha";

			$anticipos = DB::select($query);
		
			if(!empty($anticipos)){
				$contador = 0;
				foreach($anticipos as $key=>$resumen){
					$detalleResumenes = DB::select("SELECT *
													FROM vw_anticipo_clientes
													WHERE monto is not null
													AND id = ".$resumen->id );	
					if(!empty($detalleResumenes)){
						$resultado[$contador] = $anticipos[$key];
						$resultado[$contador]->detalles = $detalleResumenes;
						$contador = $contador + 1;
					}
				}
			}	
			foreach($resultado as $key=>$st){
				$listado['data'][] = $st;	
			}
			if(empty($listado)){
					$listado['data'][] = $resultado;
			}

			//return view('pages.mc.cuentaCorriente.cuentaProveedorPdf',compact('listado','proveedor','moneda','fecha_emision','fecha_vencimiento'));
			$pdf = \PDF::loadView('pages.mc.cobranzas.aplicacionAnticipoPdf',compact('listado'));
	
			$pdf->setPaper('a4', 'landscape')->setWarnings(false);
			return $pdf->download('Cuentas por Cobrar'.$this->getId4Log().'.pdf');	
		}

		public function getFacturasAplicar(Request $req)
		{
			$flag = 0;
			$draw = intval($req->draw);
			$start = intval($req->start);
			$length = intval($req->length);
			$cont = 0;
			$filtrarMax = 0;
	
			$factura = DB::table('vw_listado_factura');
			$factura = $factura->where('id_empresa',$this->getIdEmpresa());
			$factura = $factura->where('saldo_factura','>',0);
			$factura = $factura->where('id_estado_cobro','=',31);
			$factura = $factura->where('id_estado_factura','!=',30);
			$factura = $factura->where('id_tipo_factura','=',1);//arturo GES-945
			$factura = $factura->where('cliente_id','=',$req->input('idCliente'));
			$factura = $factura->get();
			$resultado = [];
			$cotizacion = $req->input('baseC');
			foreach($factura as $key=>$value){
				$cotizado = DB::select('SELECT public.get_monto_cotizacion_custom('.$cotizacion.','.$value->saldo_factura.','.$value->id_moneda_venta.','.$req->input('datamoneda').')AS resultado');
				$value->saldo_cotizado = $cotizado[0]->resultado;
				$resultado[] = $value;
			}
			return response()->json($resultado);
		}


		public function limitar_cadena($cadena, $limite, $sufijo){
			// Si la longitud es mayor que el límite...
			if(strlen($cadena) > $limite){
				// Entonces corta la cadena y ponle el sufijo
				return substr($cadena, 0, $limite) . $sufijo;
			}
			return $cadena;
		}


		public function regenerarAsientoRecibo($id_empresa){


			// $asientos_no_balanceados = DB::table('v_asientos_contables')
			// 						   ->where('id_empresa', $id_empresa)
			// 						   ->where('activo',true)
			// 						   ->where('id_origen_asiento',4) //Recibo
			// 						   ->where('balanceado','NO')
			// 						   ->limit(50)
			// 						   ->get();

			$recibos = DB::select("SELECT * from recibos where id_asiento in (
				select 
				ac.id
				from  asientos_contables ac 
				join (
				select * from asientos_contables_detalle
				WHERE id_cuenta_contable is null 
					  and fecha_hora_anulacion is null 
				)  acd ON ac.id = acd.id_asiento_contable
				left join origen_asiento_contable oac ON oac.id = ac.id_origen_asiento
				join empresas e ON e.id = ac.id_empresa
				where acd.id_cuenta_contable is null 
				and DATE(ac.fecha_hora) > '2023-01-01'::date 
				and ac.fecha_hora_anulacion is null
				and ac.id_empresa in (1)
				and oac.descripcion = 'RECIBOS'
				and ac.activo = true
				order by ac.id_empresa, ac.fecha_hora  ASC
			)");

			// $recibos = DB::select("SELECT * from recibos where nro_recibo in (
			// 	select nro_documento from v_asientos_contables
			// 	where id_empresa = 1
			// 	and activo = true
			// 	and id_origen_asiento = 4
			// 	and balanceado = 'NO'
			// 	and fecha_hora > '2023-01-01 00:00:00'
			// 	and nro_documento <> 'CO'
			// ) and id_empresa = 1 
			//   and fecha_hora_creacion > '2023-01-01 00:00:00'
			//   and fecha_hora_anulacion is null");


				//$recibos = Recibo::where('id',39424)->get();
	
			
			foreach ($recibos as $key => $recibo) {
				
				// $recibo = Recibo::where('id_asiento', $asiento->id_asiento)->first();
				// if(!$recibo){
				// 	Log::error('Sin numero de recibo, ID_ASIENTO_E: '.$asiento->id_asiento);
				// 	continue;
				// }

				Log::debug('RECIBO_ID: '.$recibo->id.' ID_ASIENTO: '.$recibo->id_asiento);
				$resultado = $this->regenerar_procesar_recibo($recibo->id);

				if($resultado != 'OK'){
					Log::error($resultado);
				}
				
			}

			dd('FIN');
		

		}



}
