<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Empresa;
use App\Persona;
use DB;
use Illuminate\Support\Facades\Config;


class EmpresaController extends Controller
{

	private function controlAutorizacion(){
		$id_usuario = $this->getIdUsuario();
		$usuario = Persona::where('id_empresa',1)->find($id_usuario);
		$arr = [28566,89299,469,52163];
		$permiso = false;

		if(isset($usuario->id) && in_array($usuario->id, $arr)){
			$permiso = true;
		}

		if(isset($usuario->id) && $usuario->id_tipo_persona == 1){
			$permiso = true;
		}


		return $permiso;
	}


	public function index(Request $request){

		if(!$this->controlAutorizacion()){
			flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}
	
		    //utilizamos el index para poder listar los parametros tienen los filtros de busqued por clave
			if($request->buscar){
				$empresas = Empresa::where('denominacion','like', '%'.$request->buscar.'%' )
									->orderBy("id", "desc")
									->paginate(10);
				return view('pages.mc.empresas.index')->with('empresas', $empresas);
			}else{
				$empresas = Empresa::orderBy("id", "desc")->paginate(10);
				return view('pages.mc.empresas.index')->with('empresas', $empresas);
			}

		
	}	

	/*public function add(Request $request){
		return view('pages.mc.empresas.add');
	}	*/
	public function create()
    {
		if(!$this->controlAutorizacion()){
			flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}

		$proveedor = DB::table('personas')->whereIn('id_tipo_persona', ['14', '15'])->get();
        return view("pages.mc.empresas.nuevaEmpresa", ['proveedor' => $proveedor]);
    }

	public function store(Request $request)
	{
		if(!$this->controlAutorizacion()){
			flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}

		$empresa = new Empresa();
		$empresa->denominacion = strtoupper($request->get('denominacion'));
		$empresa->fill($request->except('denominacion'));

		if ($request->hasFile('logo')) {
			$logo = $request->file('logo');
			$logoPath = 'personasLogo/' . time() . '.' . $logo->getClientOriginalExtension();
			$logo->move(public_path('personasLogo'), $logoPath);
			$empresa->logo = $logoPath;
		}
	//dd($request->all());
		$empresa->save();
		return redirect()->route('empresa.index')->with('mensaje', 'El registro fue exitoso');
	}

	public function edit($id)
	{
		if(!$this->controlAutorizacion()){
			flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}

		$empresa = Empresa::find($id);
		$proveedor = DB::table('personas')->whereIn('id_tipo_persona', ['14', '15'])->get();
		return view('pages.mc.empresas.edit', compact('empresa', 'proveedor'));
	}


	public function update(Request $request, $id)
    {
		if(!$this->controlAutorizacion()){
			flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}
       
		$empresa = Empresa::findOrFail($id);
		$empresa->denominacion = strtoupper($request->get('denominacion'));
		$empresa->fill($request->except('denominacion', 'logo'));
	
		if ($request->hasFile('logo')) {
			$logo = $request->file('logo');
			$logoPath = 'personasLogo/' . time() . '.' . $logo->getClientOriginalExtension();
			$logo->move(public_path('personasLogo'), $logoPath);
			$empresa->logo = $logoPath;
		}
	
		$empresa->save();
		return redirect()->route('empresa.index')->with('mensaje', 'La actualización fue exitosa');
    }



	public function doAddEmpresa(Request $request){



		$empresa = new Empresa;
		$empresa->denominacion = $request->input('denominacion');
		$empresa->activo = true;
		try{
			$empresa->save();
			flash('Se ha ingresado la Empresa exitosamente')->success();
			return redirect()->route('empresas');
		} catch(\Exception $e){
			flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			return redirect()->back();
		}
	}

/*	public function edit(Request $request){
		$empresas = Empresa::where('id', $request->input('id'))->get();
		return view('pages.mc.empresas.edit')->with('empresas', $empresas);
	}	*/

	public function doEditEmpresa(Request $request){

	

		DB::table('empresas')
					    ->where('id',$request->input('id'))
					    ->update([
								'denominacion'=>$request->input('denominacion'),
					            ]);
		flash('Se ha editado la Empresa exitosamente')->success();
		return redirect()->route('empresas');
	}	
	public function delete(Request $request){
		DB::table('empresas')
					    ->where('id',$request->input('id'))
					    ->update([
								'activo'=>false,
					            ]);
		flash('Se ha eliminado exitosamente')->error();
		return redirect()->route('empresas');
	}	


	private function getIdUsuario()
	{
 		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	}

}