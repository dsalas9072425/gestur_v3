<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use \App\DestinoDtpmundo;
use App\Currency;
use App\Aerolinea;
use App\EstadoFactour;
use App\Grupo;
use App\Producto;
use App\Persona;
use App\TipoTicket;
use App\Ticket;
use App\Factura;
use Carbon\Carbon;
use DB;
use Response;
use Image; 
use Session;

class LiquidacionesController extends Controller
{
    public function addLiquidaciones(Request $request)
    {
		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'addLiq'");

		if(empty($permisos)){
	    	flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}

    	$fecha_liquidacion = DB::select("select 
										max(periodo) fecha 
										from liquidacion_comisiones_vendedores
										");

		$listadoFechas = array();
        foreach ($fecha_liquidacion as $key => $value) {
        	$mes = substr($value->fecha, 0,-4);		
			$anho = substr($value->fecha, 2);
        	//Obtener ultimo dia del mes
        	$starDate = Carbon::createFromDate($anho,$mes,01);
			$endDate = Carbon::createFromDate($anho,$mes,01)->endOfMonth();
        	//Sumar un mes
        	$endDate = $starDate->addMonths(1);
        	// print_r($endDate);
        	$year = $endDate->year;
        	$month = $endDate->month;
        	// dd($month);
        	$listadoFechas[] = $this->getFecha($month,$year);
        }
		$mes = $this->mes();
  

    	return view('pages.mc.liquidaciones.addLiquidaciones', compact('mes'));
    }

	public function generarLiquidaciones(Request $request)
    {	
		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'addLiq'");

		if(empty($permisos)){
	    	flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1){
			DB::select("SELECT insertar_liquidacion_comision_dtp('".$request->input("periodo_mes")."',".(int)$request->input('periodo_anho').",".$idUsuario.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
		}else{
			DB::select("SELECT insertar_liquidacion_comision_agencia('".$request->input("periodo_mes")."',".(int)$request->input('periodo_anho').",".$idUsuario.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
		}

	    return $this->verLiquidaciones($request->input('periodo_mes').'/'.$request->input('periodo_anho'));

	}

    private function getFecha($mes, $anho)      
  	{
  		$month = $mes; //Reemplazable por número del 1 a 12
		$year = $anho; //Reemplazable por un año valido
		
		switch(date('n',mktime(0, 0, 0, $month, 1, $year)))
		{
			case 1: $Mes = "Enero"; break;
			case 2: $Mes = "Febrero"; break;
			case 3: $Mes = "Marzo"; break;
			case 4: $Mes = "Abril"; break;
			case 5: $Mes = "Mayo"; break;
			case 6: $Mes = "Junio"; break;
			case 7: $Mes = "Julio"; break;
			case 8: $Mes = "Agosto"; break;
			case 9: $Mes = "Septiembre"; break;
			case 10: $Mes = "Octubre"; break;
			case 11: $Mes = "Noviembre"; break;
			case 12: $Mes = "Diciembre"; break;
		}
		
		return $Mes.'/'.date('Y',mktime(0, 0, 0, $month, 1, $year));
	}
	private function mes()      
  	{
		$meses = [];
		$meses['01'] = "Enero";
		$meses['02'] = "Febrero";
		$meses['03'] = "Marzo";
		$meses['04'] = "Abril";
		$meses['05'] = "Mayo";
		$meses['06'] = "Junio";
		$meses['07'] = "Julio";
		$meses['08'] = "Agosto";
		$meses['09'] = "Septiembre";
		$meses['10'] = "Octubre";
		$meses['11'] = "Noviembre";
		$meses['12'] = "Diciembre";
		return $meses;
	}

	private function getMes($mes)      
  	{		
		switch($mes)
		{
			case "Enero": $Mes = '01'; break;
			case "Febrero": $Mes = '02'; break;
			case "Marzo": $Mes = '03'; break;
			case "Abril": $Mes = '04'; break;
			case "Mayo": $Mes = '05'; break;
			case "Junio": $Mes = '06'; break;
			case "Julio": $Mes = '07'; break;
			case "Agosto": $Mes = '08'; break;
			case "Septiembre": $Mes = '09'; break;
			case "Octubre": $Mes = '10'; break;
			case "Noviembre": $Mes = '11'; break;
			case "Diciembre": $Mes = '12'; break;
		}
		
		return $Mes;
	}

	private function verLiquidaciones($periodo)
	{

		if (Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 3)
        {
            $vendedores = Persona::where('id',  Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
        }
        else
        {
        	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2)
        	{
            	$vendedores = Persona::where('id_tipo_persona',3)->orWhere('id_tipo_persona',4)->where('activo',  true)->orderBy('nombre','asc')->get();
        	}
        	else
        	{
            	$vendedores = Persona::where('id',  Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->orderBy('nombre','asc')->get();
        	}

        }

		// $vendedores = Persona::where('id_tipo_persona', 3)->orderBy('nombre', 'asc')->get()->toArray();
		$listadoFechas[0] = $this->getFecha(date('m'), date('Y'));
		$listadoFechas[1] = $this->getFecha(date('m')-1, date('Y'));
		$listadoFechas[2] = $this->getFecha(date('m')-2, date('Y')); //mes anterior

		// return view('pages.mc.liquidaciones.verLiquidaciones', compact('vendedores', 'listadoFechas', 'periodo'));
		 return redirect()->route('verLiquidaciones', ['periodo'=>$periodo]);
	}

	public function anularLiquidacion(Request $request)
    {	
		$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$mensaje = new \StdClass; 
 	     try{
			DB::select("SELECT anular_liquidacion(".$request->input('id_liquidacion').",".$idUsuario.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");			$mensaje->status = 'OK';
				$mensaje->status = 'OK';
				$mensaje->mensaje = 'Se ha anulado la Liquidación de Vendedor';
	  	}catch(\Exception $e){
				$mensaje->status = 'ERROR';
				$mensaje->mensaje = 'No se ha anulado la Liquidación de Vendedor, Intentelo nuevamente';
		} 

		return json_encode($mensaje);
	}


	public function addLiqManual(Request $request)
    {	
		ini_set('memory_limit', '-1');
		set_time_limit(300);

		$fecha_liquidacion = DB::select("select * from (
											select  DISTINCT (periodo) as fecha, to_date(periodo,'MMYYYY') as fecha_liq
											from liquidacion_comisiones_vendedores 
											where periodo is not null
										) as result
											order by fecha_liq desc");

		$listadoFechas = array();

		foreach ($fecha_liquidacion as $key => $value) {
			$fecha = explode('-', $value->fecha_liq);
			$mes = $fecha[1];		
			$anho = $fecha[0];
			$listadoFechas[] = $this->getFecha($mes,$anho);
		}

		$facturas = DB::table('vw_facturas_liquidacion');
		$facturas = $facturas->where('id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
		$facturas = $facturas->where('factura_saldo',0);
		$facturas = $facturas->get();
    	return view('pages.mc.liquidaciones.addLiqManual', compact('listadoFechas','facturas'));
	}	


	public function doAddLiquidacion(Request $request)
    {	
		$mensaje = new \StdClass; 
		try{
			foreach($request->datos as $datos){
				$periodo = explode('/',$datos['periodo']);
				$mes =  $this->getMes($periodo[0]);
				$anho = $periodo[1];
				$factura = $datos['id_factura'];
				$liquidacionManual = DB::select("SELECT insert_liquidacion_manual('".$mes."',".$anho.",".$factura.")");
				$liquidacion_manual = $liquidacionManual[0]->insert_liquidacion_manual;
				if($liquidacion_manual == 'OK'){
					$mensaje->status = 'OK';
				}else{
					$mensaje->status = $liquidacion_manual;
				}
			}
		}catch(\Exception $e){
			Log::error($e);
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se ha insertado la Liquidación de Vendedor, Intentelo nuevamente';
		} 

		return json_encode($mensaje);
	}

}
