<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Session;
use Redirect;
use App\Cotizacion;
//Cambiar Divisa por Currency
use App\Divisas;
use App\CotizacionIndice;
use DB;

class CotizacionController extends Controller
{
	private function getIdEmpresa()
	{
	return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}

	public function index()
	{	
		//Estirar todas las divisas
		$id_empresa = $this->getIdEmpresa();
		$divisa = Divisas::whereNotIn('currency_id', [111])->where('activo','S')->get();
		$cotizacion = Cotizacion::where('id_empresa',$id_empresa)->orderBy('fecha','DESC')->get();	

		foreach ($cotizacion as $cot) {
			
			$cotizacion_contable_compra = $cot->cotizacion_contable_compra;
			$cotizacion_contable_venta = $cot->cotizacion_contable_venta;
			$venta = $cot->cotizacion_operativa;

			// $cot->cotizacion_operativa = number_format($venta, 2,',','.');
			$cot->cotizacion_contable_compra = number_format($cotizacion_contable_compra, 2,',','.');
			$cot->cotizacion_contable_venta = number_format($cotizacion_contable_venta, 2,',','.');
	}
		return view('pages.mc.cotizacion.index')->with(['cotiSelect'=>$cotizacion, 'divisa'=>$divisa]);
	}

	public function cotiSelect()
	{
		$id_empresa = $this->getIdEmpresa();
		$divisa = Divisas::where('activo','S')->get();
		$cotizacion = Cotizacion::with('moneda')
								->where('id_empresa',$id_empresa)
								->orderBy('fecha','DESC')->get();
		 // echo '<pre>';
		 // print_r($cotizacion);


		return view('pages.mc.cotizacion.index')->with(['cotiSelect'=>$cotizacion, 'divisa'=>$divisa]);
	}



	//Consulta cotizacion segun periodo y divisa
	public function consultaCotizacion(Request $req){

		$id_empresa = $this->getIdEmpresa();
		$hora = date('h:i:s');
		$fecha = trim($req->input('periodo'));
		$divisa = trim($req->input('divisa_id'));
		$err = array();
		$temp = array();
		//Validar
		

		$cotizacion = Cotizacion::query();
		$cotizacion = $cotizacion->with('moneda')->where('id_empresa',$id_empresa);

		if($fecha != ''){
			$date = explode('-', $fecha);
		 	$fechaP = explode('/',  trim($date[0]));
		 	$fechaP = $fechaP[2]."-".$fechaP[1]."-".$fechaP[0];
		 	$fechaU =  explode('/', trim($date[1]));
		 	$fechaU = $fechaU[2]."-".$fechaU[1]."-".$fechaU[0];

			$cotizacion = $cotizacion->whereBetween('fecha', array($fechaP.' 00:00:00',$fechaU.' 23:59:59'));
		}
		if($divisa != ''){
			$cotizacion = $cotizacion->where('id_currency',$divisa);
		}

		$cotizacion = $cotizacion->get();

		foreach ($cotizacion as $cot) {
			
			$cotizacion_contable_venta = $cot->cotizacion_contable_venta;
			$cotizacion_contable_compra = $cot->cotizacion_contable_compra;
			$cotizacion_operativa = $cot->venta;
			$cotizacion_jurcaip = $cot->cotizacion_jurcaip;

			$cot->venta = number_format($cotizacion_operativa, 2,',','.');
			$cot->cotizacion_contable_venta = number_format($cotizacion_contable_venta, 2,',','.');
			$cot->cotizacion_contable_compra = number_format($cotizacion_contable_compra, 2,',','.');
			$cot->cotizacion_jurcaip = number_format($cotizacion_jurcaip, 2,',','.');

		}		
		return response()->json(array('divisa'=>$cotizacion));

	}

	public function consultaIndiceCot(Request $req){

		$id_empresa = $this->getIdEmpresa();
		$hora = date('h:i:s');
		$fecha = trim($req->input('periodo_indice'));
		$divisa_origen = trim($req->input('divisa_id_origen'));
		$divisa_destino = trim($req->input('divisa_id_destino'));
		$err = array();
		$temp = array();
		//Validar
		

		$cotizacion = new CotizacionIndice;
		$cotizacion = $cotizacion->with('moneda_origen','moneda_destino')->where('id_empresa',$id_empresa);

		if($fecha != ''){
			$date = explode('-', $fecha);
		 	$fechaP = explode('/',  trim($date[0]));
		 	$fechaP = $fechaP[2]."-".$fechaP[1]."-".$fechaP[0];
		 	$fechaU =  explode('/', trim($date[1]));
		 	$fechaU = $fechaU[2]."-".$fechaU[1]."-".$fechaU[0];

			$cotizacion = $cotizacion->whereBetween('created_at', array($fechaP.' 00:00:00',$fechaU.' 23:59:59'));
		}

		if($divisa_origen != ''){
			$cotizacion = $cotizacion->where('id_moneda_origen',$divisa_origen);
		}

		if($divisa_destino != ''){
			$cotizacion = $cotizacion->where('id_moneda_destino',$divisa_destino);
		}

		$cotizacion = $cotizacion->orderBy('created_at','DESC');
		$cotizacion = $cotizacion->get();

		
			return response()->json(array('divisa'=>$cotizacion));
	}

	
	public function addIndice()
	{
		//Estirar todas las divisas
		$divisa = Divisas::whereNotIn('currency_id', [111])->where('activo','S')->get();

		return view('pages.mc.cotizacion.add_indice')->with(['divisa'=>$divisa]);
	}

	public function add()
	{
		//Estirar todas las divisas
		$divisa = Divisas::whereNotIn('currency_id', [111])->where('activo','S')->get();

		return view('pages.mc.cotizacion.add')->with(['divisa'=>$divisa]);
	}



	/**
	 * Guardar Cotizacion con la fecha actual
	 * @param  Request $request Formulario Cotizacion
	 * @return string           1-0
	 */
	public function doAddCotizacion(Request $request)
	{
		// dd($request->all());

		$id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		$id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$results = 'true';

		$cotizacion = new Cotizacion;
		$cotizacion->fecha = date('Y-m-d h:i:s');
		$cotizacion->venta = str_replace(',','.', str_replace('.','',$request->input('venta')));
		$cotizacion->cotizacion_contable_venta = str_replace(',','.', str_replace('.','',$request->input('venta')));
		$cotizacion->cotizacion_contable_compra = str_replace(',','.', str_replace('.','',$request->input('venta')));
		$cotizacion->cotizacion_jurcaip = str_replace(',','.', str_replace('.','',$request->input('cotizacion_jurcaip')));
		$cotizacion->id_currency = $request->input('divisa_id');
		$cotizacion->id_empresa = $id_empresa;
		$cotizacion->id_usuario = $id_usuario;
		
		// try
		// {
			$cotizacion->save();
		// } 
		// catch(\Exception $e)
		// {
		// 	$results = 'false';
		// }

		return response()->json(array('err'=>$results));


	}

	public function saveIndice(Request $request)
	{

		$id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		$id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

		$cotizacion = new CotizacionIndice;
		$cotizacion->created_at = date('Y-m-d h:i:s');
		$cotizacion->updated_at = date('Y-m-d h:i:s');
		$cotizacion->id_moneda_origen = $request->input('divisa_id_origen');
		$cotizacion->id_moneda_destino = $request->input('divisa_id_destino');
		$cotizacion->indice =  $request->input('indice_cotizacion');
		$cotizacion->id_empresa = $id_empresa;
		$cotizacion->id_usuario = $id_usuario;
		
		$cotizacion->save();


		return response()->json('ok');
	}

	/**
	 * Estirar datos de la base de datos
	 * @param  Request $req id de la Divisa
	 * @return [type]       campos con la divisa en fecha actual
	 */
	public function doInfoCotizacion(Request $req){

		$divisa = $req->input('divisa_id');
		$fecha = date('Y-m-d');


			$r = Cotizacion::with('moneda')->whereBetween('fecha',
								  array($fecha.' 10:00:00',
										$fecha.' 21:00:00'
									))->where('id_currency',$divisa)
									->where('id_empresa',$this->getIdEmpresa())
									->orderBy('fecha','DESC')->get();
		 
			 echo json_encode(array('divisa'=>$r));
		}






}