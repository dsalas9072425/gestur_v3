<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use Redirect;
use App\Proforma;
use App\Autorizaciones;
use App\Producto;
use App\ProformasDetalle;
use App\Divisas;
use App\Persona;
use App\AdjuntoDocumento;
use App\HistoricoComentariosProforma;
use App\SolicitudFacturaParcial;
use App\Voucher;
use App\Timbrado;
use Response;
use Image; 
use DB;
use Log;


class ControlOperacionesController extends Controller {



private function getIdUsuario(){
	
	 return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

}//function

private function getDatosUsuario(){
	
	 $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	 return $persona = Persona::where('id',$idUsuario)->first();

	}//function

private function getIdEmpresa(){

  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

}//function



  private function btnPermisos($parametros, $vista){

  $btn = DB::select("SELECT pe.* 
                        FROM persona_permiso_especiales p, permisos_especiales pe
                        WHERE p.id_permiso = pe.id 
                        AND p.id_persona = ".$this->getIdUsuario() ." 
                        AND pe.url = '".$vista."' ");

  $htmlBtn = '';
  $boton =  array();
  $idParametro = '';

  // dd( $btn );

  if(!empty($btn)){

  foreach ($btn as $key => $value) {

    $idParametro = '';
    $ruta = 'factour.'.$value->accion;
     $htmlBtn = '';



     //LLEVA PARAMETRO
    if($value->lleva_parametro == '1'){

    foreach ($parametros as $indice=>$valor) { 

      if($indice == $value->nombre_parametro){
        $idParametro = $valor;
      }
    }

    //PARAMETRO OCULTO EN DATA
    if($value->parametro_oculto == '1'){
       $htmlBtn = "<a role='button' class='".$value->clase."' data-btn='".$idParametro."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    } else {
       $htmlBtn = "<a role='button' href='".route($ruta,['id'=>$idParametro])."' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    }

    } else {
      $htmlBtn = "<a role='button' href='#' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i>".$value->texto_alternativo." </a>"; 
    }

    
    $boton[] = $htmlBtn;
  }
   return $boton;
 
   } else {
    return array();
   }

}//function

	/**
	 * Cargar datos en la pagina principal de control de proforma, recibe como parametro el id de proforma
	 * @param  Request $request [description]
	 * @param  [type]  $id      id proforma
	 * @return [type]           datos de la proforma y sus detalles
	 */
	public function index($id){ 

		$idEmpresa = $this->getIdEmpresa();
		$proformas = Proforma::with('usuario')->where('id',$id)->where('id_empresa' , $idEmpresa)->get();

	    if(!isset($proformas[0]->id)){
	    	flash('La proforma solicitada no existe. Intentelo nuevamente !!')->error();
            return redirect()->route('home');
	    }
		$btnAutorizar = 'false';
		$btn = $this->btnPermisos('','operaciones');

		$cont_permiso = DB::select("SELECT COUNT(*) cant FROM  persona_permiso_especiales WHERE id_permiso IN(7,9) 
                        AND id_persona = ".$this->getIdUsuario());
		$cont_permiso = (int)$cont_permiso[0]->cant;

		if($cont_permiso > 1){
			$btnAutorizar = 'true';
		}

		$personaLogin = $this->getDatosUsuario();
	
		$idProforma = $id;
		

		//UPDATE ESTADO DE LA PROFORMA SELECCIONADA
		//prueba
		$profoEstado = DB::select('select * from proformas where id = '.$idProforma);

		//ASIGNAR RESPONSABLE
		//prueba
		$ar = array('responsable_id'=>$this->getIdUsuario());

		//SOLO SI ESTA EN ESTADO A VERIFICAR CAMBIAR ESTADO A EN VERIFICACION
		//prueba
		if($profoEstado[0]->estado_id == 3){
			$ar['estado_id'] = 41;
		}
			DB::table('proformas')
            ->where('id', $idProforma)
            ->update($ar);
		
		$solicitud = SolicitudFacturaParcial::with('pasajero','currency', 'cliente', 'usuario')->where('id_estado', 80)->where('id_proforma', $idProforma)->get();
		$mensajeSaldoProforma = DB::select("SELECT * FROM control_saldo(".$idProforma.")");
		$comentario = HistoricoComentariosProforma::with('persona')
												   ->where('id_proforma',$idProforma)
												   ->orderBy('fecha_hora','DESC')
												   ->get();
													 //dd($comentario);

		$proformas = Proforma::with('vendedor','agencia','pasajero','currency','usuario')->where('id',$idProforma)->get();
		$idEmpresa = $proformas[0]->usuario->id_empresa;
		$idSucursalEmpresa = $proformas[0]->usuario->id_sucursal_empresa;

		$proformasDetalle = ProformasDetalle::with('prestador',
												   'proveedor',
												   'producto',
												   'proforma',
												   'currencyVenta')
												->where('id_proforma',$idProforma)
												->where('activo',true)->get();
	
		$saldoDisponible = null;
		if($proformas != null){

		$saldoDisponible = DB::select("SELECT * FROM get_saldo_real(".$idProforma.")");

		}

		$currency = Divisas::get();	
		$prestador = Persona::orWhere('id_tipo_persona',"=", 14)
							  ->orWhere('id_tipo_persona',"=", 15)
							  ->where('id_empresa', $idEmpresa)
							  ->get();

		$imagenes = AdjuntoDocumento::where('proforma_id',$idProforma)
									 ->get();
		$idUsuario = Persona::where('id',$this->getIdUsuario())->first();
		$totalProforma = DB::select('SELECT get_monto_proforma as total FROM get_monto_proforma('.$proformas[0]->id.') ');

		if(!empty($personaLogin)){
			$tipoPersona = $personaLogin->id_tipo_persona;

			// //SUPERVISOR EMPRESA
			// if($tipoPersona == '4'){
			// $btnAutorizar = 'true';

			// }//if 	
			
			
		}//if

		$productosVoucher = Producto::where('genera_voucher',true)
									->where('id_empresa', $idEmpresa)
									->get();
		$pasajeroProforma = $proformas[0]->pasajero_id;
		$nombrePasajeroProforma = Persona::where('id', $pasajeroProforma)->get();
		if(isset($nombrePasajeroProforma[0]->nombre)){					
			$pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id 
							    FROM personas WHERE personas.id = ".$proformas[0]->pasajero_id);
		}else{
			$pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id FROM personas WHERE id IN (SELECT pasajero FROM pasajeros_proformas WHERE id_proforma=".$id.")");
		}	


		$datos = file_get_contents("../destination_actual.json");
		$destinoJson =  json_decode($datos, true);
		// dd($destinoJson);
		$datos ="";
		$id_categoria = "";
		$id_destinos = "";
		$arrayDestino = [];

		if(session('data') !== null){
			$datos = session('data');
			$id_categoria = $datos['id_categoria'];
			$id_destinos = $datos['id_destino'];
			$paquetesDestinos = explode(",", $datos['id_destino']);
			foreach($paquetesDestinos as $key=>$destino){
				foreach($destinoJson as $key1=>$dest){
					if($destino == $dest['idDestino']){
						$arrayDestino[$key1]['id'] = $dest['idDestino'];
						$arrayDestino[$key1]['value'] = $dest['desDestino'];
					}
				}
			}
		}

		$tipo_timbrado = [1,6];
		$p_venta_default = 0;

		$puntos_ventas = Timbrado::with('sucursal','tipo_timbrado')
									->whereIn('id_tipo_timbrado',$tipo_timbrado)
									->where('id_empresa',$idEmpresa)
									->where('activo',true)
									->orderBy('expedicion','ASC')
									->get();


		
		if($idEmpresa == 21 && count($puntos_ventas)){
			$p_venta_default = $puntos_ventas->where('id_tipo_timbrado',6)->first();
			if($p_venta_default){
				$p_venta_default = $p_venta_default->id;
			}
		} else if(count($puntos_ventas)){
			$p_venta_default = $puntos_ventas[0]->id;
			$buscar_sucursal_usuario = $puntos_ventas->where('id_sucursal_empresa',$proformas[0]->usuario->id_sucursal_empresa)->first();
			if($buscar_sucursal_usuario){
				$p_venta_default = $buscar_sucursal_usuario->id;
			}
			
		}										
	
		
		if(isset($puntos_ventas_default->id)){
			$p_venta_default = $puntos_ventas_default->id;
		}				
		

		if (empty($idEmpresa)) {
			// Si id_empresa está vacío, traer todos los registros
			$query = DB::table('motivos_autorizacion_proforma');
		} else {
			// Si id_empresa tiene un valor, traer registros con id_empresa nulo y aquellos con el valor de id_empresa
			$query = DB::table('motivos_autorizacion_proforma')
				->where('id_empresa', $idEmpresa)
				->orWhereNull('id_empresa');
		}
		
		$motivo_anulacion = $query->orderBy('descripcion', 'asc')->get(['id', 'descripcion']);
		
		return view('pages.mc.operaciones.index')->with(['proformasDetalle'=>$proformasDetalle,
														 'prestadores'=>$prestador,
														 'mensajeSaldoProforma'=>$mensajeSaldoProforma,
														 'imagenes'=>$imagenes,
														 'proformas'=>$proformas,
														 'comentario'=>$comentario,
														 'currency'=>$currency,
														 'btnAutorizar'=>$btnAutorizar,
														 'saldoDisponible'=>$saldoDisponible,
														 'idUsuario'=>$idUsuario,
														 'productosVoucher'=>$productosVoucher,
														 'pasajeros'=>$pasajero, 
														 'totalProforma'=>$totalProforma,
														 'valorDestinos'=>$arrayDestino,
														 'btn'=>$btn,
														 'solicitudes'=>$solicitud,
														 'puntos_ventas'=>$puntos_ventas,
														 'p_v_d'=>$p_venta_default,
														 'motivo_anulacion'=>$motivo_anulacion
														 ]);
	
	}

	public function obtenerDestino(Request $req){
		$id_destino = $req->id_destino;
		$destino_name = '';

		$datos = file_get_contents("../destination_actual.json");
		$destinoJson =  json_decode($datos, true);

		foreach ($destinoJson as $key => $value) {
			if($value['idDestino'] == $id_destino){
				$destino_name = $value['desDestino'];
				break;
			}
		}

		return response()->json(['data'=>$destino_name]);
	}

	/**
	 * Recibe el id de del detalle de la proforma por Ajax seleccionada en 
	 * el Control de Proforma, y retorna los detalles para 
	 * Cargar los campos de edicion
	 * @param  Request $req  id detalle proforma
	 * @return [type]       detalles de proforma
	 */
	public function getDetallesAjax(Request $req){

		$id_empresa=$this->getIdEmpresa();
		$idDetalleProforma = $req->id;
		$proformasDetalle = false;
		$prestador = array();
		$prest = '';
		$proveedor = '';
		$asistencia = '';
		// dd($idDetalleProforma);

	
		$proformasDetalle = ProformasDetalle::with(	'producto',
													'proforma',
													'proforma.currency',
												   'currencyCosto',
												   'currencyVenta')
		->where('id_empresa',$id_empresa)
		->where('id',$idDetalleProforma)->get();
		// dd($proformasDetalle);
	
		if(!is_null($proformasDetalle)){	

			if($proformasDetalle[0]->producto->id_grupos_producto == 3){
			$asistencia = DB::select('SELECT * from tarifas_assiscard where id = '.$proformasDetalle[0]->id_tarifa_asistencia);
			if(!empty($asistencia)){
				$asistencia = $asistencia[0]->tarifa_asistencia_nombre;
			} else {
				$asistencia = '';
			}

			}
			$producto = $proformasDetalle[0]->id_producto;
			if($producto == 7){
				$producto = 1;
			}

			$grupoProducto = Producto::where('id',  '=', $proformasDetalle[0]->id_producto)
							->first();
			$id_grupo_producto = $grupoProducto->id_grupos_producto;
			if($id_grupo_producto == 1){
				$producto = $proformasDetalle[0]->id_producto;
				$aerolineas1 = DB::select("SELECT id as out_id_persona,
										nombre as out_nombre,
									CASE 
										WHEN denominacion_comercial IS NULL THEN
											nombre
										ELSE
										nombre || ' - ' || denominacion_comercial
									END as out_nombre,	
									id_tipo_facturacion as out_tipo 
									from personas where id_empresa = ? and id in (select distinct(id_proveedor)
									from tickets where id_empresa = ?) ",[$this->getIdEmpresa(),$this->getIdEmpresa()]); 
				$aerolineas2 = DB::select('select * from get_producto_persona(?,?)', [$producto,$this->getIdEmpresa()]);
				$aerolineas = array_merge($aerolineas1, $aerolineas2);
				$resultado = [];
				foreach($aerolineas as $key=>$aerolinea){
					$resultado[$key]['id'] = $aerolinea->out_id_persona;
					$resultado[$key]['nombre'] = $aerolinea->out_nombre;
					$resultado[$key]['tipo'] =  $aerolinea->out_tipo;
				}
				usort($resultado, function($a, $b){
					return strcmp($a["nombre"], $b["nombre"]);
				});
			}else{
				$producto = $proformasDetalle[0]->id_producto;
				$getProveedors =DB::select('SELECT public."get_producto_persona"('.$proformasDetalle[0]->id_producto.','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');

				$resultado = [];
				foreach($getProveedors as $key=>$getProveedor){
					$valor = str_replace(')', '',str_replace('"', '',str_replace('(', '', $getProveedor->get_producto_persona)));
					$proveedor = explode(',',$valor);
					$resultado[$key]['id'] = $proveedor[0];
					$resultado[$key]['nombre'] = $proveedor[1];
					$resultado[$key]['tipo'] = $proveedor[2];
				}
				usort($resultado, function($a, $b){
					return strcmp($a["nombre"], $b["nombre"]);
				});
			}
			$getProveedors = $resultado;
			$prestador = Persona::whereIn('id_tipo_persona', [15, 14, 3, 2])
								->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
								->get(['id', 'nombre']);
		} else {
			$resultado = null;
			$prestador = null;
		} 

		 $respuesta = [];
		 $respuesta['datos'] = $proformasDetalle;
		 $respuesta['proveedor'] = $getProveedors;
		 $respuesta['prestador'] = $prestador;
		 $respuesta['tipo_asistencia'] = $asistencia;
	

		return json_encode($respuesta);
	}


	/**
	 * Recibe una fecha 02/05/2017 y pasa a 2017-05-02
	 * @param  string $date fecha
	 * @return string       fecha
	 */
	private function formatoFecha($date){
		if( $date != ''){

		$date = explode('/', $date);
		 	$fecha = $date[2]."-".$date[1]."-".$date[0];
		 	return $fecha;
		} else {
			return null;
		}
	}

	/**
	 * Recibe los datos del formulario de proforma detalle editados en 
	 * Control Operativo
	 * @param Request $req datos proformadetalle
	 */
	public function add(Request $req){
		
		$idEmpresa = $this->getIdEmpresa();
		$ok = 0;
		$fechaIn = ($req->input('fechaIn') != '') 	? $this->formatoFecha($req->input('fechaIn')) : $ok++;
		$fechaOut = ($req->input('fechaOut') != '') ? $this->formatoFecha($req->input('fechaOut')) : $ok++;
		$idProforma = $req->input('idProforma');
		$descripcion = $req->input('descripcion');
		$idDetalleProforma = ($req->input('idDetalleProforma') != '') 	? $req->input('idDetalleProforma') : $ok++;

		$proveedor = ($req->input('proveedor') != '' &&  $req->input('proveedor') != 0) 	? $req->input('proveedor') : $ok++;
		$prestador = ($req->input('prestador')  != '' && $req->input('prestador') != 0) 	? $req->input('prestador') : $ok++;
		$costoGravado = ($req->input('costoGravado') != '' &&  $req->input('costoGravado') != 0) 	? $req->input('costoGravado') :0;
		$codigoConfirmacion = ($req->input('codigoConfirmacion')  != '') 		? $req->input('codigoConfirmacion')  : $ok++;
		$comision_agencia = ($req->input('comisionAgencia') != '' && $req->input('comisionAgencia') != '') 	? $req->input('comisionAgencia') : $ok++;
		$currencyCosto = ($req->input('currencyCosto') != '' && $req->input('currencyCosto') != 0) 			? $req->input('currencyCosto') : $ok++;
		$precioCosto = ($req->input('precioCosto') != '' && $req->input('precioCosto') != 0) 				? $req->input('precioCosto') : $ok++;

		$fechaPagoProveedor = $this->formatoFecha($req->input('pagoProveedorFecha'));
		if($req->input('gastoFecha') != ''){
			$gastoFecha = $this->formatoFecha($req->input('gastoFecha'));
		}else {
			$gastoFecha = null;
		}	
		if($ok === 0){

		 	$response = true;

		 	try{

			DB::table('proformas_detalle')
			->where('id',$idDetalleProforma)
			->update([
					  'id_proveedor'=>$proveedor,
					  'id_prestador'=>$prestador,
					  'descripcion'=>$descripcion,
					  'verificado_operativo'=>true,
					  'cod_confirmacion'=>$codigoConfirmacion,
					  'fecha_in'=>$fechaIn,
					  'fecha_out'=>$fechaOut,
					  'porcentaje_comision_agencia'=>$comision_agencia,
					  'currency_costo_id'=>$currencyCosto,
					  'costo_proveedor'=>$precioCosto,
					  'fecha_pago_proveedor'=>$fechaPagoProveedor,
					  'fecha_gasto'=>$gastoFecha,
					  'costo_gravado'=>$costoGravado
					]);

		 	} catch(\Exception $e){

			$response = false;
		} 
	

	//	DB::select('SELECT public.actualizar_vencimiento('.$req->input('idProforma').','.$req->input('idDetalleProforma').','.$idEmpresa.')');



	 	}else {
			$response = false;
		} 


		$proforma = Proforma::where('id',$idProforma)->get();


		return response()->json(array('rsp'=>$response,'proforma'=>$proforma));
	}




	/**
	 * Guarda los comentarios que son enviados a traves de la caja de comentarios en 
	 * Control Operativo de Proforma, retorna true si se guardo 
	 * con exito y false si no ademas consulta el ultimo comentario
	 * almacenado en base de datos  y lo envia
	 * @param  Request $req comentario, idproforma y id usuario
	 * @return [type]       [description]
	 */
	public function guardarComentario(Request $req){

		$comentario = $req->comentario;
		$fechaActual = date('Y-m-d H:i:s');
		$response = 'true';
		$idUsuario = $this->getIdUsuario();
		$idEmpresa = $this->getIdEmpresa();
		$idProforma = $req->idProforma;

		$historicoComentario = new HistoricoComentariosProforma;
		$historicoComentario->comentario = $comentario;
		$historicoComentario->id_proforma = $idProforma;
		$historicoComentario->fecha_hora = $fechaActual;
		$historicoComentario->id_usuario = $idUsuario;
		$historicoComentario->id_empresa = $idEmpresa;
		
		

		
		try{
			$historicoComentario->save();
		} catch(\Exception $e){
			Log::error($e);
			$response = 'false';
		}

		$comentario = HistoricoComentariosProforma::where('id_proforma',$idProforma)
													->orderBy('fecha_hora','DESC')
													->first();


      return response()->json(['rsp'=>$response,'comentario'=>$comentario]);
	}






	/**
	 * Cambia los estados de la proforma segun sea facturado, aprobado o 
	 * rechazado en el formulario de Control Operativo, retorna true o false si tuvo exito y datos de 
	 * de la proforma
	 * @param  Request $req estado, id proforma
	 * @return [type]       
	 */
	public function guardarEstado(Request $req){

		$now = new \DateTime();	
		$idUsuario = $this->getIdUsuario();
    	$fecha_hora = date('Y-m-d H:i:s');
		$idProforma = $req->idProforma;
		$estado = $req->estado;	
	


		$fechaActual = $now->format('Y-m-d H:i:s');
		$response = true;
		$proforma = Proforma::where('id',$idProforma)->first();

		//SI CLIENTE REQUIERE AUTORIZACION PARA FACTURAR INSERTAR 
		$tipo_persona_autorizar = DB::select('
			SELECT COUNT(*) AS cant 
			FROM PERSONAS 
			WHERE id = '.$proforma->cliente_id.' 
			AND id_tipo_persona IN (
				SELECT id_tipo_persona 
				FROM tipo_persona_autorizacion
				WHERE requiere_autorizacion = true 
				AND id_empresa = '.$this->getIdEmpresa().')');
	   try{	
 	
			//Guardar rechazo solo cuando se trate de solicitar autorizacion y que el estado a cambiar sea abierto
			if($proforma->estado_id == 24 && $estado == 1){
				
				if($tipo_persona_autorizar[0]->cant > 0){

					$autorizacion_1 = new Autorizaciones;
					$autorizacion_1->id_usuario_pedido = $proforma->id_usuario;
					$autorizacion_1->id_usuario_autorizacion = $idUsuario;
					$autorizacion_1->fecha_hora_autorizacion = $fecha_hora;
					$autorizacion_1->id_estado = 39;
					$autorizacion_1->id_tipo_autorizacion = 2; //TIPO_AUTORIZACION MARKUP
					$autorizacion_1->valor = $proforma->markup;
					$autorizacion_1->comentario = null;
					$autorizacion_1->id_proforma = $idProforma;
					$autorizacion_1->cliente_id = $proforma->cliente_id;

				}
				$autorizacion = new Autorizaciones;
				$autorizacion->id_usuario_pedido = $proforma->id_usuario;
				$autorizacion->id_usuario_autorizacion = $idUsuario;
				$autorizacion->fecha_hora_autorizacion = $fecha_hora;
				$autorizacion->id_estado = 39;
				$autorizacion->id_tipo_autorizacion = 1; //TIPO_AUTORIZACION MARKUP
				$autorizacion->valor = $proforma->markup;
				$autorizacion->comentario = null;
				$autorizacion->id_proforma = $idProforma;
				$autorizacion->cliente_id = $proforma->cliente_id;
			}
	
    
         

			if($proforma->estado_id == 24 && $estado == 1)
			{
				if($tipo_persona_autorizar[0]->cant > 0){
					$autorizacion_1->save();
				}
				
			$autorizacion->save();
			DB::table('proformas')
			->where('id',$idProforma)
			->update(['responsable_id'=>null]);	
			}	
	
			DB::table('proformas')
			->where('id',$idProforma)
			->update(['estado_id'=>$estado]);

			if($estado == 1){
				$getNotificacion= DB::select("SELECT public.insert_notificacion(".$proforma->id_usuario.",null, null, null,16,".$idProforma.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
				$getNotificacion= DB::select("SELECT public.insert_notificacion(".$proforma->id_asistente.",null, null, null,16,".$idProforma.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");

			}	

			if($estado == 2 ||$estado == 3){
				$getNotificacion= DB::select("SELECT public.insert_notificacion(".$proforma->id_usuario.",null, null, null,15,".$idProforma.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
				$getNotificacion= DB::select("SELECT public.insert_notificacion(".$proforma->id_asistente.",null, null, null,15,".$idProforma.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
			}	

			  
	
		
		} catch(\Exception $e){
			Log::error($e);
			$response = false;
		}



		$proforma = Proforma::with('estado')->where('id',$idProforma)->first();

		
		return response()->json(['rsp'=>$response,'proforma'=>$proforma]);						
			
	}//funcion




	/**
	 * Guarda el voucher generado en la vista de control operativo , la informacion se envia
	 * por ajax
	 * @param  Request $request datos voucher
	 * @return [type]           [description]
	 */
	public function guardarVoucher(Request $request){

	//	dd($request->all());
		$mensaje = new \StdClass;
        $indice = 0;
        $indiceError = 0;
        $count = count($request->base);
        foreach($request->base as $key1=>$base){
            $voucher = new Voucher;
            $voucher->id_proforma = $request->input('proforma');
            $voucher->id_proforma_detalle = $request->input('detalle_proforma');
            $voucher->descripcion_servicio = $base['descripcion'];
            $voucher->comentario = html_entity_decode($base['comentario']);
            $voucher->cant_adultos = $base['adultos'];
            $voucher->cant_child = $base['ninhos'];
            $voucher->otros_pasajeros = $base['otros_pasajeros'];
            $voucher->pasajero_principal = $base['pasajero_principal'];
            $voucher->id_estados = 1;
            $voucher->id_usuario= $this->getIdUsuario();
            $voucher->fecha = date('Y-m-d H:i:00');

            try{
                $voucher->save();
                $indice = $indice+1;
            } catch(\Exception $e){
                $indiceError = $indiceError+1;
            }        
        }

        if($indice == $count){
            $mensaje->status = 'OK';
            $mensaje->mensaje = 'Se han guardado los Vouchers exitosamente';
        }else{
            $mensaje->status = 'ERROR';
            $mensaje->mensaje = 'No se han guardado la Vouchers exitosamente, Intentelo Nuevamente';
        }    
        return json_encode($mensaje);

	}

	public function getVoucherUpdate(Request $request){

        $voucher = Voucher::whith('prestador')->where('id_proforma_detalle', $request->input('dataVoucher'))
        					->where('activo', true)
                            ->get();

        return json_encode($voucher);                    
    }


    /**
     * Asigna a un responsable de la autorizacion para facturar cuando la proforma
     * esta por debajo del markup establecido
     */
    public function autorizarProforma(Request $req){
    	$idUsuario = $this->getIdUsuario();
    	$fecha_hora = date('Y-m-d H:i:00');
    	$idProforma = $req->idProforma;
    	$err = 'true';
		$motivo_autorizacion = $req->input('motivo_autorizacion');
		$comentario_autorizacion = $req->input('comentario_autorizacion');
		$proforma = Proforma::where('id',$idProforma)->first();
		
		//SI CLIENTE REQUIERE AUTORIZACION PARA FACTURAR INSERTAR 
		$tipo_persona_autorizar = DB::select('
			SELECT COUNT(*) AS cant 
			FROM PERSONAS 
			WHERE id = '.$proforma->cliente_id.' 
			AND id_tipo_persona IN (
				SELECT id_tipo_persona 
				FROM tipo_persona_autorizacion
				WHERE requiere_autorizacion = true 
				AND id_empresa = '.$this->getIdEmpresa().')');
				
				try {
					DB::table('autorizaciones')
						 ->where('id_proforma',$idProforma)
						 ->update(['activo'=>false]);
	   
				 
				 
		if($tipo_persona_autorizar[0]->cant > 0){

			$autorizacion_1 = new Autorizaciones;
			$autorizacion_1->id_usuario_pedido = $proforma->id_usuario;
			$autorizacion_1->id_usuario_autorizacion = $idUsuario;
			$autorizacion_1->fecha_hora_autorizacion = $fecha_hora;
			$autorizacion_1->id_estado = 38; //AUTORIZADO
			$autorizacion_1->id_tipo_autorizacion = 2; //AUTORIZACION TIPO PERSONA
			$autorizacion_1->valor = $proforma->markup;
			$autorizacion_1->comentario = $req->input('comentario_autorizacion');
			$autorizacion_1->id_proforma = $proforma->id;
			$autorizacion_1->cliente_id = $proforma->cliente_id;
			$autorizacion_1->motivo_id = $req->input('motivo_autorizacion');
			$autorizacion_1->activo = true; 
			$autorizacion_1->save();

		} else {		

	
    	//En caso de autorizacion guardar estado autorizado
    	$autorizacion = new Autorizaciones;
    	$autorizacion->id_usuario_pedido = $proforma->id_usuario;
    	$autorizacion->id_usuario_autorizacion = $idUsuario;
    	$autorizacion->fecha_hora_autorizacion = $fecha_hora;
    	$autorizacion->id_estado = 38; //AUTORIZADO
    	$autorizacion->id_tipo_autorizacion = 1; //MARKUP
    	$autorizacion->valor = $proforma->markup;
    	$autorizacion->comentario = $req->input('comentario_autorizacion');
		$autorizacion->id_proforma = $proforma->id;
		$autorizacion->cliente_id = $proforma->cliente_id;
		$autorizacion->motivo_id = $req->input('motivo_autorizacion');
		$autorizacion->activo = true;
		$autorizacion->save();
	}

	DB::table('proformas')
						 ->where('id',$idProforma)
						 ->update(['id_usuario_autorizacion'=>$idUsuario,
									 'fecha_hora_autorizacion'=>$fecha_hora,
									 'responsable_id'=>null]);
		}catch(\Exception $e){
				Log::error($e);
                $err = 'false';
            }
            
           return response()->json(array('err'=>$err));

    }


    public function infoProforma(Request $req){
    	// dd($req->all());
    	
    	$err = false;
    	$resp = array();
    	$proforma = '';


    	// $resp = DB::table('proformas')->select('proformas.*,estados.denominacion')
    	// ->leftJoin('estados','proformas.estado_id','=','estados.id')
    	// ->where('id',$req->id)->get();
    	$resp = Proforma::with('estado')->where('id',$req->id)->get();
    	$monto = DB::select('SELECT get_monto_proforma as total FROM get_monto_proforma('.$req->id.')');
    	
    	 // dd($resp);

    	if(!$resp->isEmpty()){
    	$proforma  = array(
    			'monto_proforma'=>$monto[0]->total,
    			'total_bruto'=> $this->formatMoney($resp[0]->id_moneda_venta,$resp[0]->total_bruto_facturar),
    			'total_neto'=> $this->formatMoney($resp[0]->id_moneda_venta,$resp[0]->total_neto_facturar),
    			'total_exentas'=> $this->formatMoney($resp[0]->id_moneda_venta,$resp[0]->total_exentas),
    			'total_gravadas'=> $this->formatMoney($resp[0]->id_moneda_venta,$resp[0]->total_gravadas),
    			'total_iva'=> $this->formatMoney($resp[0]->id_moneda_venta,$resp[0]->total_iva),
    			'total_comision' => $this->formatMoney($resp[0]->id_moneda_venta,$resp[0]->total_comision),
    			'markup' => $resp[0]->markup,
    			'fecha_in'=>$this->formatoFechaSalida($resp[0]->check_in),
    			'fecha_out'=>$this->formatoFechaSalida($resp[0]->check_out), 
    			'fecha_venc'=>$this->formatoFechaSalida($resp[0]->vencimiento),
    			'estado_id'=>$resp[0]->estado_id,
    			'estado_denominacion'=>$resp[0]->estado['denominacion']
    	);
    	$err = true;

    	}


    	return response()->json(array('resp'=>$proforma,'err'=>$err));


    }//function


    /**
     * SI ES DIFERENTE AL GUARANI REALIZA 
     * EL FORMATEO CON COMAS SINO SERA SIN COMAS
     */
    private function formatMoney($id_moneda_venta, $valor){

    	if($id_moneda_venta != 111){
    		return number_format($valor,2,",",".");
    	}
    	return number_format($valor,0,",",".");

    }//function


    /**
     * Recibe una fecha 2017-05-02 y pasa a 02/05/2017 
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaSalida($date){
        if( $date != ''){

        $date = explode('-', $date);
            $fecha = $date[2]."/".$date[1]."/".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }


    public function verificarDetalle(Request $req){

    	$option = ($req->option == 1) ? true : false ;
    	$err = 'false';



    	try{
				DB::table('proformas_detalle')
	            ->where('id', $req->id_proforma_detalle)
	            ->update(['verificado_operativo'=> $option]);

	            if($req->id_adjunto != '')
	            DB::table('adjuntos_documentos')
	            ->where('id', $req->id_adjunto)
	            ->update(['verificado_operativo'=> $option,
	        			 'id_proforma_detalle'=>$req->id_proforma_detalle
	        			]);
	        	


	            $err = 'true';

    	  } catch(\Exception $e){
                $err = 'false';
            }

            $adjuntos = DB::select("SELECT * FROM adjuntos_documentos WHERE id_proforma_detalle = ".$req->id_proforma_detalle);
            
     return response()->json(['resp'=>$err,'data_adjunto'=>$adjuntos]);       

    }//fucntion


    public function verificarAdjunto(Request $req){
    	// dd($req->all());
    	$option = ($req->option == 1) ? true : false ;
    	$err = 'false';



    	// try{
				DB::table('adjuntos_documentos')
	            ->where('id', $req->id_adjunto)
	            ->update(['verificado_operativo'=> $option,
	        			 'id_proforma_detalle'=>$req->id_proforma_detalle
	        			]);
	            $err = 'true';

    	  // } catch(\Exception $e){
       //          $err = 'false';
       //      }
            
     return response()->json(['resp'=>$err]);       

    }//fucntion

    public function datosSolicitud(Request $req){
		$solicitud = SolicitudFacturaParcial::with('cliente')->where('id', $req->input('idSolicitud'))->first();
		$respuesta = new \StdClass;
		if(isset($solicitud->cliente->email)){
			$respuesta->correo = $solicitud->cliente->email;
		}else{
			$respuesta->correo = '';
		}
		if(isset($solicitud->cliente->documento_identidad)){
			$respuesta->ruc = $solicitud->cliente->documento_identidad."-".$solicitud->cliente->dv;
		}else{
			$respuesta->ruc = '';
		}

		return response()->json(['respuesta'=>$respuesta]);      
	}


}
