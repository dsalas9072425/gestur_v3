<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\PagoOpformValidate; //CLASE DE VALIDACION

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;
use App\IntermediaAdjuntoOp;
use App\Proforma;
use App\ProformasDetalle;
use App\Divisas;
use App\Persona;
use App\PlazoPago;
use App\HistoricoComentariosProforma;
use App\Voucher;
use App\TipoFactura;
use App\EstadoFactour;
use App\TipoPersona;
use App\Factura;
use App\FacturaDetalle;
use App\AdjuntoDocumento;
use App\Empresa;
use App\Producto;
use App\FormaPagoCliente;
use App\Currency;
use App\OpCabecera;
use App\OpDetalle;
use App\GastoOp;
use App\FormaPagoOpCabecera;
use App\FormaPagoOpDetalle;
use App\BancoCabecera;
use App\BancoDetalle;
use App\Anticipo;
use App\PlanCuenta;
use App\ChequeFp;
use App\CentroCosto;
use App\LibroCompra;
use App\SucursalEmpresa;
use App\Retencion;
use App\NemoReserva;
use App\CtaCtbFormaPago;
use App\CotizacionIndice;
use App\Cotizacion;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use App\Exceptions\ExceptionCustom;

use Session;
use Redirect;
use App\Traits\OpTrait;
// use DB;

class PagoProveedorController extends Controller
{

  use OpTrait;


    public function getId4Log(){
      //Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
        list($MiliSegundos, $Segundos) = explode(" ", microtime());
        $id=substr(strval($MiliSegundos),2,4);
        return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
    }

/**
 *==========================================================================================================  
 *                                  PAGO PROVEEDOR INDEX
 *======================================================================================================== 
 */
      /**
       * Listado de OP
       */
      public function indexOp()
      {

        $forma_pago = FormaPagoCliente::all();
        $currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
        $origen = DB::table('banco_cabecera')->get();     
        $getDivisa = Divisas::where('activo','S')->get();
        $getProveedor = Persona::whereIn('id_tipo_persona',array(14,8))->where('id_empresa',$this->getIdEmpresa())->get();  

        /*DATOS CONFIRMAR OP*/
        $sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
        $centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();     
        $estado = EstadoFactour::where('id_tipo_estado',13)->get();


        return view('pages.mc.pagoProveedor.indexOp')->with(['origen'=>$origen,
                                                              'currency'=>$currency,
                                                              'getDivisa'=>$getDivisa,
                                                              'getProveedor'=>$getProveedor,
                                                              'centro'=>$centro,
                                                              'bancos'=>$origen,
                                                              'sucursalEmpresa'=>$sucursalEmpresa,
                                                              'estados'=>$estado]);;
      }//  

      /**
       * ajax que carga el listado de OP
       */
      public function reporteOp(Request $req)
      {
        if($req->input('cod_confirmacion') != "" || $req->input('proforma') !=""){
            $detalles = DB::table('vw_detalle_op_anticipo');
            if($req->input('cod_confirmacion')){
              $detalles = $detalles->where('cod_confirmacion',$req->input('cod_confirmacion'));
            }
            if($req->input('proforma')){
              $detalles = $detalles->where('id_proforma',$req->input('proforma'));
            } 
            $detalles = $detalles->get(['id_op']);
            $ids = [];
            foreach($detalles as $detalle){
              $ids[] =$detalle->id_op;
            }
        }

        $data = DB::table('vw_listado_op');
        $data = $data->where('id_empresa',$this->getIdEmpresa());

        if($req->input('id_moneda')){
          $data = $data->where('id_moneda',$req->input('id_moneda'));
        }
        if($req->input('id_proveedor')){
          $data = $data->where('id_proveedor',$req->input('id_proveedor'));
        } 
         if($req->input('id_estado')){
          $data = $data->whereIn('id_estado',$req->input('id_estado'));
        } 
         if($req->input('nro_op')){
          $data = $data->where('nro_op',$req->input('nro_op'));
        }
        if($req->input('periodo_creacion')){
          $fechaPeriodo = explode(' ', $req->input('periodo_creacion'));
          $desdeC = $fechaPeriodo[0].' 00:00:00';
          $hastaC = $fechaPeriodo[1].' 23:59:59';
          $data = $data->whereBetween('fecha_hora_creacion',array($desdeC,$hastaC));         
        }
        if($req->input('periodo_proceso')){
          $fechaPeriodo = explode(' ', $req->input('periodo_proceso'));
          $desdeC = $fechaPeriodo[0].' 00:00:00';
          $hastaC = $fechaPeriodo[1].' 23:59:59';
          $data = $data->whereBetween('fecha_hora_procesado',array($desdeC,$hastaC));         
        }
        if($req->input('periodo_verificacion')){
          $fechaPeriodo = explode(' ', $req->input('periodo_verificacion'));
          $desdeC = $fechaPeriodo[0].' 00:00:00';
          $hastaC = $fechaPeriodo[1].' 23:59:59';
          $data = $data->whereBetween('fecha_hora_verificado',array($desdeC,$hastaC));         
        }
        if($req->input('periodo_autorizacion')){
          $fechaPeriodo = explode(' ', $req->input('periodo_autorizacion'));
          $desdeC = $fechaPeriodo[0].' 00:00:00';
          $hastaC = $fechaPeriodo[1].' 23:59:59';
          $data = $data->whereBetween('fecha_hora_autorizado',array($desdeC,$hastaC));         
        }
        if($req->input('periodo_anulacion')){
          $fechaPeriodo = explode(' ', $req->input('periodo_anulacion'));
          $desdeC = $fechaPeriodo[0].' 00:00:00';
          $hastaC = $fechaPeriodo[1].' 23:59:59';
          $data = $data->whereBetween('fecha_hora_anulacion',array($desdeC,$hastaC));         
        }
        if($req->input('tipo_op') != "")
        {
          if($req->input('tipo_op') == 1){
              $data = $data->where('fondo_fijo', true);   
          }else{
              $data = $data->where('fondo_fijo', false);   
          }
        } 
        if($req->input('cod_confirmacion') != "" || $req->input('proforma') !=""){
          $data = $data->whereIn('id',$ids);
        } 

        $data = $data->get();

        return response()->json(['data'=>$data]);
      }

/**
 *==========================================================================================================  
 *                                  CREACION Y CONTROL DE OP
 *======================================================================================================== 
 */
      /**
       * Carga la vista de seleccion y creacion de OP
       */
      public function indexPagoProveedor()
      {
        $currency = Currency::where('activo', 'S')->orderBy('currency_id', 'ASC')->get();

        /*FILTROS*/      
        $getDivisa = Divisas::where('activo','S')->get();
        $getProveedor = DB::select("SELECT * FROM personas 
                            WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
                                          WHERE puede_facturar = true) 
                            AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");
        $calendariobsp = DB::select("SELECT * FROM vw_calendario_bcp");
        /*DATOS CONFIRMAR OP*/
        $sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
        $centro  = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();   
        return view('pages.mc.pagoProveedor.index')->with(['currency'=>$currency,
                                                          'getDivisa'=>$getDivisa,
                                                          'getProveedor'=>$getProveedor,
                                                          'centro'=>$centro,
                                                          'sucursalEmpresa'=>$sucursalEmpresa,
                                                          'calendariobsp'=>$calendariobsp
                                                        ]);
      }


      public function estadoReserva()
      {
        $currency = Currency::where('activo', 'S')->orderBy('currency_id', 'ASC')->get();

        /*FILTROS*/      
        $getDivisa = Divisas::where('activo','S')->get();
        $getProveedor = Persona::whereIn('id_tipo_persona',array(14,8))->where('id_empresa',$this->getIdEmpresa())->get();  

        /*DATOS CONFIRMAR OP*/
        $sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
        $centro  = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();   
        return view('pages.mc.pagoProveedor.estadoReserva')->with(['currency'=>$currency,
                                                          'getDivisa'=>$getDivisa,
                                                          'getProveedor'=>$getProveedor,
                                                          'centro'=>$centro,
                                                          'sucursalEmpresa'=>$sucursalEmpresa]);
      }

      /**
       * Envia los datos para la vist de seleccion de documentos para generar una op
       */
      public function getListProveedor(Request $req)
      { 
       /* $id_pais = DB::table('vw_libro_compra_anticipos');
        if ($req->input('extranjero') == 1) {
          $id_pais = $id_pais->where('id_pais', '<>', 1455);// Filtro para Extranjeros (SI)
        } else if ($req->input('extranjero') == 1455) {
            $id_pais = $id_pais->where('id_pais', 1455); // Filtro para No Extranjeros (NO)
        }*/
    //se agrego cuidado
            $contador =0;
            ///////////////////////////////////////ANTICIPO//////////////////////////////////////////////
            $libroCompra = DB::table('mv_pago_proveedores');
            $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
            $libroCompra = $libroCompra->where('fondo_fijo',false);
            $libroCompra = $libroCompra->where('id_tipo_documento',20);
            if ($req->input('extranjero') == 1) {
              $libroCompra = $libroCompra->where('id_pais', '<>', 1455);// Filtro para Extranjeros (SI)
            } else if ($req->input('extranjero') == 1455) {
                $libroCompra = $libroCompra->where('id_pais', 1455); // Filtro para No Extranjeros (NO)
            }
            if($req->input('idProveedor'))
                  $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
              if($req->input('idProforma'))
                  $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
              if($req->input('codigoConfirmacion'))
                  $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
              if($req->input('nroFactura'))
                  $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
              if($req->input('idMoneda'))
                  $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
             
              if($req->input('tieneFechaProveedor'))
                  $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
                  if($req->input('reprice'))
                  $libroCompra = $libroCompra->where('reprice_pago_pro',$req->input('reprice')); 
        
              if($req->input('proveedor_desde_hasta')){
                  $fecha = explode(' ', $req->input('proveedor_desde_hasta'));
                  $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha[0],$fecha[1]));   
                }
              if($req->input('gasto_desde_hasta')){
                  $fecha = explode(' ', $req->input('gasto_desde_hasta'));
                  $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($fecha[0]." 00:00:00",$fecha[1]." 23:59:59"));   
                }
              /*if($req->input('pendiente')){
                $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
              }   */
              if($req->input('fecha_pago')){
                $desde     = $req->input('fecha_pago').' 00:00:00';
                $hasta     = $req->input('fecha_pago').' 23:59:59';

                $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($desde,$hasta));
              } 

              if($req->input('vencimiento')){
                $periodo =  explode(' - ', $req->input('vencimiento'));
                $desde = explode("/", trim($periodo[0]));
                $hasta = explode("/", trim($periodo[1]));
                $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
                $libroCompra = $libroCompra->whereBetween('fecha_prim_ven',array($fecha_desde,$fecha_hasta));   
              } 
  
              if($req->input('periodo_desde_hasta')){
                $periodo =  explode(' - ', $req->input('periodo_desde_hasta'));
                $desde = explode("/", trim($periodo[0]));
                $hasta = explode("/", trim($periodo[1]));
                $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
                $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
              } 
              $libroCompra = $libroCompra->where('saldo','>',0);
              $libroCompra = $libroCompra->get();
              $resultado = $libroCompra;
              $contador = count($resultado);
              ///////////////////////////////////////NOTA DE CREDITO//////////////////////////////////////////////
              $libroCompra = DB::table('mv_pago_proveedores');
              $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
              $libroCompra = $libroCompra->where('fondo_fijo',false);
              $libroCompra = $libroCompra->whereIn('id_tipo_documento',array(2,32));
              if ($req->input('extranjero') == 1) {
                $libroCompra = $libroCompra->where('id_pais', '<>', 1455);// Filtro para Extranjeros (SI)
              } else if ($req->input('extranjero') == 1455) {
                  $libroCompra = $libroCompra->where('id_pais', 1455); // Filtro para No Extranjeros (NO)
              }
                if($req->input('idProveedor'))
                    $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
                if($req->input('idProforma'))
                    $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
                if($req->input('codigoConfirmacion'))
                    $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
                if($req->input('nroFactura'))
                    $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
                if($req->input('idMoneda'))
                    $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
            
                if($req->input('tieneFechaProveedor'))
                    $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));

                    if($req->input('reprice'))
                    $libroCompra = $libroCompra->where('reprice_pago_pro',$req->input('reprice')); 
              /*  if($req->input('pendiente')){
                  $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
                } */  
                if($req->input('proveedor_desde_hasta')){
                  $fecha = explode(' ', $req->input('proveedor_desde_hasta'));
                  $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha[0],$fecha[1]));   
                }
                if($req->input('gasto_desde_hasta')){
                  $fecha = explode(' ', $req->input('gasto_desde_hasta'));
                  $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($fecha[0]." 00:00:00",$fecha[1]." 23:59:59"));   
                }
                if($req->input('vencimiento')){
                  $periodo =  explode(' - ', $req->input('vencimiento'));
                  $desde = explode("/", trim($periodo[0]));
                  $hasta = explode("/", trim($periodo[1]));
                  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
                  $libroCompra = $libroCompra->whereBetween('fecha_prim_ven',array($fecha_desde,$fecha_hasta));   
                } 
  
                if($req->input('pendiente')){
                  $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
                }      
                if($req->input('fecha_pago')){
                  $libroCompra = $libroCompra->where('fecha_de_gasto',$req->input('fecha_pago')); 
                }    
                  
                if($req->input('periodo_desde_hasta')){
                  $periodo =  explode(' - ', $req->input('periodo_desde_hasta'));
                  $desde = explode("/", trim($periodo[0]));
                  $hasta = explode("/", trim($periodo[1]));
                  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
                  $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
                }   
                $libroCompra = $libroCompra->where('saldo','>',0); 
                $libroCompra = $libroCompra->get();
                foreach($libroCompra as $key=>$lc){
                  $resultado[$contador] = $lc;
                  $contador++;
                }
              ///////////////////////////////////////FACTURA CREDITO//////////////////////////////////////////////
              $libroCompra = DB::table('mv_pago_proveedores');
              $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
              $libroCompra = $libroCompra->where('fondo_fijo',false);
              $libroCompra = $libroCompra->whereIn('id_tipo_documento',array(1,5,33));
              if ($req->input('extranjero') == 1) {
                $libroCompra = $libroCompra->where('id_pais', '<>', 1455);// Filtro para Extranjeros (SI)
              } else if ($req->input('extranjero') == 1455) {
                  $libroCompra = $libroCompra->where('id_pais', 1455); // Filtro para No Extranjeros (NO)
              }
                if($req->input('idProveedor'))
                    $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
                if($req->input('idProforma'))
                    $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
                if($req->input('codigoConfirmacion'))
                    $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
                if($req->input('nroFactura'))
                    $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
                if($req->input('idMoneda'))
                    $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
            
                if($req->input('tieneFechaProveedor'))
                    $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
                  if($req->input('reprice'))
                    $libroCompra = $libroCompra->where('reprice_pago_pro',$req->input('reprice')); 
               /* if($req->input('pendiente')){
                  $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
                } */     
                if($req->input('checkin_desde_hasta')){
                  $fecha = explode(' ', $req->input('checkin_desde_hasta'));
                  $libroCompra = $libroCompra->whereBetween('check_in',array($fecha[0],$fecha[1]));   
                }
                if($req->input('proveedor_desde_hasta')){
                  $fecha = explode(' ', $req->input('proveedor_desde_hasta'));
                  $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha[0],$fecha[1]));   
                }
                if($req->input('gasto_desde_hasta')){
                  $fecha = explode(' ', $req->input('gasto_desde_hasta'));
                  $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($fecha[0]." 00:00:00",$fecha[1]." 23:59:59"));   
                }
                if($req->input('fecha_pago')){
                  $libroCompra = $libroCompra->where('fecha_de_gasto',$req->input('fecha_pago')); 
                }   

                if($req->input('vencimiento')){
                  $periodo =  explode(' - ', $req->input('vencimiento'));
                  $desde = explode("/", trim($periodo[0]));
                  $hasta = explode("/", trim($periodo[1]));
                  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
                  $libroCompra = $libroCompra->whereBetween('fecha_prim_ven',array($fecha_desde,$fecha_hasta));   
                } 
   
                if($req->input('periodo_desde_hasta')){
                  $periodo =  explode(' - ', $req->input('periodo_desde_hasta'));
                  $desde = explode("/", trim($periodo[0]));
                  $hasta = explode("/", trim($periodo[1]));
                  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
                  $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
                }   
                $libroCompra = $libroCompra->where('saldo','>',0);
                $libroCompra = $libroCompra->get();
                foreach($libroCompra as $key=>$lc){
                  $resultado[$contador] = $lc;
                  $contador++;
                }
              //////////////////////////////////FACTURA//////////////////////////////////////
              $libroCompra = DB::table('mv_pago_proveedores');
              $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
              $libroCompra = $libroCompra->where('fondo_fijo',false);
              $libroCompra = $libroCompra->where('id_tipo_documento',22);
              if ($req->input('extranjero') == 1) {
                $libroCompra = $libroCompra->where('id_pais', '<>', 1455);// Filtro para Extranjeros (SI)
              } else if ($req->input('extranjero') == 1455) {
                  $libroCompra = $libroCompra->where('id_pais', 1455); // Filtro para No Extranjeros (NO)
              }

                if($req->input('idProveedor'))
                    $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
                if($req->input('idProforma'))
                    $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
                if($req->input('codigoConfirmacion'))
                    $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
                if($req->input('nroFactura'))
                    $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
                if($req->input('idMoneda'))
                    $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));

                if($req->input('tieneFechaProveedor'))
                    $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
                    if($req->input('reprice'))
                    $libroCompra = $libroCompra->where('reprice_pago_pro',$req->input('reprice')); 
               /* if($req->input('pendiente')){
                  $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
                }*/    
                if($req->input('checkin_desde_hasta')){
                  $fecha = explode(' ', $req->input('checkin_desde_hasta'));
                  $libroCompra = $libroCompra->whereBetween('check_in',array($fecha[0],$fecha[1]));   
                }
                if($req->input('proveedor_desde_hasta')){
                    $fecha = explode(' ', $req->input('proveedor_desde_hasta'));
                    $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha[0],$fecha[1]));   
                  }
                if($req->input('gasto_desde_hasta')){
                    $fecha = explode(' ', $req->input('gasto_desde_hasta'));
                    $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($fecha[0]." 00:00:00",$fecha[1]." 23:59:59"));   
                  }
                  if($req->input('fecha_pago')){
                    $libroCompra = $libroCompra->where('fecha_de_gasto',$req->input('fecha_pago')); 
                  }    
                if($req->input('vencimiento')){
                  $periodo =  explode(' - ', $req->input('vencimiento'));
                  $desde = explode("/", trim($periodo[0]));
                  $hasta = explode("/", trim($periodo[1]));
                  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
                  $libroCompra = $libroCompra->whereBetween('fecha_prim_ven',array($fecha_desde,$fecha_hasta));   
                } 
  
                if($req->input('periodo_desde_hasta')){
                  $periodo =  explode(' - ', $req->input('periodo_desde_hasta'));
                  $desde = explode("/", trim($periodo[0]));
                  $hasta = explode("/", trim($periodo[1]));
                  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
                  $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
                }   
                $libroCompra = $libroCompra->where('saldo','>',0);
                $libroCompra = $libroCompra->get(); 
               // dd($libroCompra);
                foreach($libroCompra as $key=>$lc){
                  $resultado[$contador] = $lc;
                  $contador++;
                }
                //dd(substr($resultado, 0, 5000));
              return response()->json(['data'=>$resultado]); 

      }//FUNCTION

      //aqui

      /**
       * Envia los datos para la vist de seleccion de documentos para generar una op
       */
      public function getListFondoFijo(Request $req)
      { 
            
                 ///////////////////////////////////////ANTICIPO//////////////////////////////////////////////
                 $libroCompra = DB::table('vw_libro_compra_anticipos');
                 $libroCompra = $libroCompra->where('fondo_fijo',true);
                 $libroCompra = $libroCompra->where('id_tipo_documento',20);
                 $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
                 
                   if($req->input('idProveedor'))
                       $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
                   if($req->input('idProforma'))
                       $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
                   if($req->input('codigoConfirmacion'))
                       $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
                   if($req->input('nroFactura'))
                       $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
                   if($req->input('idMoneda'))
                       $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
                   if($req->input('tieneFechaProveedor'))
                       $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
                       
                   if($req->input('checkin_desde_hasta')){
                       $fecha = explode(' ', $req->input('checkin_desde_hasta'));
                       $libroCompra = $libroCompra->whereBetween('check_in',array($fecha[0],$fecha[1]));   
                     }
                   if($req->input('proveedor_desde_hasta')){
                       $fecha = explode(' ', $req->input('proveedor_desde_hasta'));
                       $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha[0],$fecha[1]));   
                     }
                   if($req->input('pendiente')){
                     $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
                   }      
     
                   $libroCompra = $libroCompra->where('saldo','>',0);
                   $libroCompra = $libroCompra->get();
                   $resultado = $libroCompra;
                   $contador = count($resultado);
                   ///////////////////////////////////////NOTA DE CREDITO//////////////////////////////////////////////
                   $libroCompra = DB::table('vw_libro_compra_anticipos');
                   $libroCompra = $libroCompra->where('fondo_fijo',true);
                   $libroCompra = $libroCompra->whereIn('id_tipo_documento',array(2,32));
                   $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
                   
                     if($req->input('idProveedor'))
                         $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
                     if($req->input('idProforma'))
                         $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
                     if($req->input('codigoConfirmacion'))
                         $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
                     if($req->input('nroFactura'))
                         $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
                     if($req->input('idMoneda'))
                         $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
                     if($req->input('tieneFechaProveedor'))
                         $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
                         
                     if($req->input('checkin_desde_hasta')){
                         $fecha = explode(' ', $req->input('checkin_desde_hasta'));
                         $libroCompra = $libroCompra->whereBetween('check_in',array($fecha[0],$fecha[1]));   
                       }
                     if($req->input('proveedor_desde_hasta')){
                         $fecha = explode(' ', $req->input('proveedor_desde_hasta'));
                         $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha[0],$fecha[1]));   
                       }
                     if($req->input('pendiente')){
                       $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
                     }      
       
                     $libroCompra = $libroCompra->where('saldo','>',0);
                     $libroCompra = $libroCompra->get();                    
                      foreach($libroCompra as $key=>$lc){
                       $resultado[$contador] = $lc;
                       $contador++;
                     }
                   ///////////////////////////////////////FACTURA CREDITO//////////////////////////////////////////////
                   $libroCompra = DB::table('vw_libro_compra_anticipos');
                   $libroCompra = $libroCompra->where('fondo_fijo',true);
                   $libroCompra = $libroCompra->whereIn('id_tipo_documento',array(1,5,33));

                   $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
                   
                     if($req->input('idProveedor'))
                         $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
                     if($req->input('idProforma'))
                         $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
                     if($req->input('codigoConfirmacion'))
                         $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
                     if($req->input('nroFactura'))
                         $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
                     if($req->input('idMoneda'))
                         $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
                     if($req->input('tieneFechaProveedor'))
                         $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
                         
                     if($req->input('checkin_desde_hasta')){
                         $fecha = explode(' ', $req->input('checkin_desde_hasta'));
                         $libroCompra = $libroCompra->whereBetween('check_in',array($fecha[0],$fecha[1]));   
                       }
                     if($req->input('proveedor_desde_hasta')){
                         $fecha = explode(' ', $req->input('proveedor_desde_hasta'));
                         $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha[0],$fecha[1]));   
                       }
                     if($req->input('pendiente')){
                       $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
                     }      
       
                     $libroCompra = $libroCompra->where('saldo','>',0);
                     $libroCompra = $libroCompra->get();
                     foreach($libroCompra as $key=>$lc){
                       $resultado[$contador] = $lc;
                       $contador++;
                     }
                   //////////////////////////////////FACTURA//////////////////////////////////////
                   $libroCompra = DB::table('vw_libro_compra_anticipos');
                   $libroCompra = $libroCompra->where('fondo_fijo',true);
                   $libroCompra = $libroCompra->where('id_tipo_documento',22);

                   $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
                   
                     if($req->input('idProveedor'))
                         $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
                     if($req->input('idProforma'))
                         $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
                     if($req->input('codigoConfirmacion'))
                         $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
                     if($req->input('nroFactura'))
                         $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
                     if($req->input('idMoneda'))
                         $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
                     if($req->input('tieneFechaProveedor'))
                         $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
                         
                     if($req->input('checkin_desde_hasta')){
                         $fecha = explode(' ', $req->input('checkin_desde_hasta'));
                         $libroCompra = $libroCompra->whereBetween('check_in',array($fecha[0],$fecha[1]));   
                       }
                     if($req->input('proveedor_desde_hasta')){
                         $fecha = explode(' ', $req->input('proveedor_desde_hasta'));
                         $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha[0],$fecha[1]));   
                       }
                     if($req->input('pendiente')){
                       $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
                     }      
       
                     $libroCompra = $libroCompra->where('saldo','>',0);
                     $libroCompra = $libroCompra->get();
                     foreach($libroCompra as $key=>$lc){
                       $resultado[$contador] = $lc;
                       $contador++;
                     }
                   return response()->json(['data'=>$resultado]); 

      }//FUNCTION

      //aqui

      public function reposiocionFondo()
      {
        $currency = Currency::where('activo', 'S')->orderBy('currency_id', 'ASC')->get();

        /*FILTROS*/      
        $getDivisa = Divisas::where('activo','S')->get();
        $getProveedor = Persona::whereIn('id_tipo_persona',array(14,8))->where('id_empresa',$this->getIdEmpresa())->get();  

        /*DATOS CONFIRMAR OP*/
        $sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
        $centro  = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();   
        return view('pages.mc.pagoProveedor.reposicion')->with(['currency'=>$currency,
                                                          'getDivisa'=>$getDivisa,
                                                          'getProveedor'=>$getProveedor,
                                                          'centro'=>$centro,
                                                          'sucursalEmpresa'=>$sucursalEmpresa]);
      }
      //aqui

      /**
       * Cambia el estado de la OP
       */
      public function estadoOp(Request $req)
      {

        //1 - VERIFICAR
        //2 - AUTORIZAR
        //3 - ANULAR
       //dd($req->all());
        $idUsuario = (Integer)Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
        $err= true;
        $opt = $req->input('opt');
        $id_op = (Integer)$req->input('id_op');
        $msj = 'La opción seleccionada no es conocida';

       try {

            DB::beginTransaction();

            //VERIFICAR
            if($opt === "1"){
              
              DB::SELECT("SELECT verificar_op( ".$id_op.",".$idUsuario.",49)");
              if($req->input('beneficiario')!= "" && $req->input('beneficiario') != $req->input('cf_entrega')){
                //echo 'ugfjdhgfd';
                  DB::table('op_cabecera')
                              ->where('id',$id_op)
                              ->update(['entregado' => $req->input('cf_entrega')." - ".$req->input('beneficiario')]);          
              }else{
                  DB::table('op_cabecera')
                              ->where('id',$id_op)
                              ->update(['entregado' => $req->input('cf_entrega')]);           
            }

              


              //AUTORIZAR
            } else if($opt === "2"){

              DB::SELECT("SELECT autorizar_op( ".$id_op.",".$idUsuario.")");
            
            } else if($opt === "3"){
              //TODO: Probar con NC, LC, anticipo, verificar el saldo principalmente
              $this->anular_op($id_op,$idUsuario,null );

            } else {
              $err = false;
            }

            DB::commit();

       } catch(ExceptionCustom $e){

          DB::rollback();
          //Aqui cae la exepcion creada manualmente
          Log::error($e);
          $err = false;
          $msj = $e->getMessage();

       } catch(\Exception $e){

          DB::rollback();
           Log::error($e);
           $err = false;
           $msj = 'Ocurrió un error en la operación';
           
       }



        return response()->json(['err'=>$err,'msj'=>$msj]);
      }


      public function getCotizacionFP(Request $req)
      {
         if($req->input('id_moneda_costo') == 111){
            $moneda = 143;
          }else{
            $moneda =$req->input('id_moneda_costo');
          }

          $result = DB::select('SELECT get_cotizacion_moneda('.(integer) $moneda.','.$this->getIdEmpresa().') as cotizacion');

          $r = floatval($result[0]->cotizacion);

          if($r > 0){
            return response()->json(['info'=>$result]);
          } else {
            return response()->json(['info'=>'']);
          }

        
      }

      public function getCotizacionFPO(Request $req)
      {
          $indice_cotizacion = 0;
          $base =  new \StdClass;
          $base->indice_cotizacion = 0;

          $op = OpCabecera::where('id',$req->input('id_op'))->first(['indice_cotizacion','id_moneda','cotizacion_especial']);

          if(!$op->indice_cotizacion){
            $cotizacion_indice = CotizacionIndice::where('id_empresa',$this->getIdEmpresa())
              ->where('id_moneda_origen',$op->id_moneda)
              ->where('id_moneda_destino',$req->input('id_moneda_costo'))
              ->orderBy('id','DESC')
              ->first(['indice']);

              if($cotizacion_indice){
                $base->indice_cotizacion = $cotizacion_indice->indice;
              }

          } else {
            $base->indice_cotizacion = $op->indice_cotizacion;
          }
     
            if($req->input('id_moneda_costo') == 111){
              $moneda = 143;
            }else{
              $moneda =$req->input('id_moneda_costo');
            }

            if(!$op->cotizacion_especial){
              $base->cotizacion = DB::select('SELECT get_cotizacion_moneda('.(integer) $moneda.','.$this->getIdEmpresa().') as cotizacion')[0]->cotizacion;
            } else {
              $base->cotizacion = $op->cotizacion_especial;
            }

           

        

          return response()->json($base);


      }

      public function cotizarMontosProveedor(Request $req) 
      {
     
           // dd($req->all());
           $id_empresa = $this->getIdEmpresa();
           $moneda_costo = $req->cabecera['id_moneda_costo'];   
           if($moneda_costo == 111){
               $result = DB::select('SELECT 
               get_monto_cotizado('.str_replace(',','.', str_replace('.','',$req->total_factura)).','.$moneda_costo.',143,'.$id_empresa.') AS total_factura, 
               get_monto_cotizado('.$req->total_nc.','.$moneda_costo.',143,'.$id_empresa.') AS total_nc,
               get_monto_cotizado('.$req->total_nc.','.$moneda_costo.',111,'.$id_empresa.') AS total_nc_gs,
               get_monto_cotizado('.$req->total_anticipo.','.$moneda_costo.',143,'.$id_empresa.') AS total_anticipo,
               get_monto_cotizado('.str_replace(',','.', str_replace('.','',$req->total_factura)).','.$moneda_costo.',143,'.$id_empresa.') AS total_pago_neto,
               get_monto_cotizado('.str_replace(',','.', str_replace('.','',$req->total_factura)).','.$moneda_costo.',111,'.$id_empresa.') AS total_pago_neto_gs,
               get_monto_cotizado('.str_replace(',','.', str_replace('.','',$req->total_factura)).','.$moneda_costo.',111,'.$id_empresa.') AS total_guaranies,
               get_monto_cotizado('.$req->retencion.','.$moneda_costo.',143,'.$id_empresa.') AS total_retencion,
               get_monto_cotizado('.$req->retencion.','.$moneda_costo.',111,'.$id_empresa.') AS total_retencion_gs,
               get_monto_cotizado('.$req->total_anticipo.','.$moneda_costo.',111,'.$id_empresa.') AS total_anticipo_gs'
               );
            }else{
               $result = DB::select('SELECT 
               get_monto_cotizado('.$req->total_factura.','.$moneda_costo.',111,'.$id_empresa.') AS total_factura,
               get_monto_cotizado('.$req->total_nc.','.$moneda_costo.',143,'.$id_empresa.') AS total_nc,
               get_monto_cotizado('.$req->total_nc.','.$moneda_costo.',111,'.$id_empresa.') AS total_nc_gs,
               get_monto_cotizado('.$req->total_anticipo.','.$moneda_costo.',143,'.$id_empresa.') AS total_anticipo,
               get_monto_cotizado('.str_replace(',','.', str_replace('.','',$req->total_factura)).','.$moneda_costo.',143,'.$id_empresa.') AS total_pago_neto,
               get_monto_cotizado('.str_replace(',','.', str_replace('.','',$req->total_factura)).','.$moneda_costo.',111,'.$id_empresa.') AS total_pago_neto_gs,
               get_monto_cotizado('.str_replace(',','.', str_replace('.','',$req->total_factura)).','.$moneda_costo.',111,'.$id_empresa.') AS total_guaranies,
               get_monto_cotizado('.$req->retencion.','.$moneda_costo.',143,'.$id_empresa.') AS total_retencion,
               get_monto_cotizado('.$req->retencion.','.$moneda_costo.',111,'.$id_empresa.') AS total_retencion_gs,
               get_monto_cotizado('.$req->total_anticipo.','.$moneda_costo.',111,'.$id_empresa.') AS total_anticipo_gs'
               );

            }
     
         return response()->json(['info'=>$result, 'moneda'=>$moneda_costo]);
     
     
      }//




  /**
   *  POSEE UNA CLASE DE VALIDACION DE FORMULARIO PagoOpformValidate
   *  Alamacena los datos de pago a proveedores
   *  CREA LA OP
   */
  public function setPagoProveedorFondoFijo(PagoOpformValidate $req)
  {

          //DETALLES CREACION
          $id_usuario = $this->getIdUsuario();
          $id_empresa = $this->getIdEmpresa();
          $id_estado_op =  52;//PENDIENTE  
          $err = false;
          $result_retencion = new \StdClass; 
          $result_retencion->retencion = false;
          $id_cabecera_op = null;
          $total_gravadas = null;
          $porcentaje_retencion = null;
          $base_imponible = null;
          $total_retencion = null;


          //DETALLE DE PROVEEDOR SELECCIONADOS
          
          $pagoDetalle = $req->input('pagosDetalle');
          $pagoCabecera = (Object)$req->input('pagosCabecera');

          //DETALLES DE PAGO PROVEEDOR
          $id_sucursal = $req->input('sucursal');
          $cotizacion = $req->input('cotizacion');
          $relacion_cotizacion = $req->input('relacion_cotizacion');
          $id_moneda = $pagoCabecera->id_moneda;

          $empresa = Empresa::where('id',$req->input('idProveedor'))->first(['id_persona']);
          $pagoCabecera->id_proveedor = $empresa->id_persona;
   
      try {
                    Log::info("(USERID:".$id_usuario." METOD:setPagoProveedorFondoFijo)".json_encode($req->all()));

                    DB::beginTransaction();

              
                
                //GENERAR INSERT PARA OP
                $detalles_op = [];
                $cabecera_op = [];
                $total_op = str_replace(',','.', str_replace('.','', $req->input('total_pago')));
                $info = '';
                $id_cuenta_contable = 0;
                
                //VERIFICAR SI ES PROVEEDOR LOCAL O EXTERIOR
                $proveedor_py = Persona::where('id',$pagoCabecera->id_proveedor)->where('id_pais',1455)->count();
              

                // --------------------- SI EL PROVEEDOR ES LOCAL ---------------------
                if($proveedor_py > 0){       
                  //TODO: Aqui no se realiza las retenciones porque los montos son pequeños         
                  $id_cuenta_contable = DB::select("SELECT get_id_cuenta_contable('PROV_LOCALES',".$id_empresa.",".$id_moneda.") AS n")[0]->n;
                } else {
                  $id_cuenta_contable = DB::select("SELECT get_id_cuenta_contable('PROVEEDORES_EXT',".$id_empresa.",null)  AS n")[0]->n;

                }

                $nro_op_funct = DB::select("SELECT public.get_documento(".$id_empresa.",'OP')");
                $nro_op = $nro_op_funct[0]->get_documento;

                  //GENERAR CABECERA 
                $cabecera_op = [
                                  'nro_op'=> $nro_op,
                                  'id_usuario_creacion'=> $id_usuario,
                                  'id_estado'=>$id_estado_op,
                                  'total_pago'=> $req->input('total_pago'), 
                                  'total_neto_pago'=>$req->input('total_pago_neto'),
                                  'total_anticipo'=>$req->input('total_anticipo'),
                                  'total_nota_credito'=>$req->input('total_nc'),
                                  'total_retencion'=>$req->input('retencion'),
                                  'total_gravadas'=>$total_gravadas,
                                  'id_moneda'=> $id_moneda,
                                  'id_sucursal'=>$id_sucursal,
                                  'id_centro_costo'=>$req->input('centro_costo'),
                                  'cotizacion'=>$cotizacion,
                                  'id_proveedor'=>$pagoCabecera->id_proveedor,
                                  'id_empresa'=>$id_empresa,
                                  'id_cuenta_contable'=>  $id_cuenta_contable,
                                  'base_imponible'=>$base_imponible,
                                  'porcentaje_retencion'=>$porcentaje_retencion,
                                  'origen'=>'PP',
                                  'fondo_fijo'=>true];

                  // dd($cabecera_op);
                                    
                $id_cabecera_op =  DB::table('op_cabecera')->insertGetId ($cabecera_op);                   
                
                //GENERAR DETALLE OP
                foreach ($pagoDetalle as $key => $value) {
                  $detalles_op[] = [
                                    'id_libro_compra'=>($value['operacion']['id_compra']) ? $value['operacion']['id_compra'] : null,
                                    'id_anticipo'=>($value['operacion']['id_anticipo']) ? $value['operacion']['id_anticipo']: null,
                                    'monto'=>$value['operacion']['monto_pago'],
                                    'id_cabecera' => $id_cabecera_op
                                  ];
                          
                }//FOREACH

                DB::table('op_detalle')->insert($detalles_op);  

                //FUNCION PARA GENERAR ASIENTOS
                DB::SELECT(' SELECT * FROM generar_op('.$id_cabecera_op.','. $id_usuario.')');

                $err = true;
                DB::commit();
     

            } catch(\Exception $e){
                Log::error($e);
                $err = false;
                $info = 'Ocurrio un error al intentar crear la OP';
                DB::rollBack();
                
            }
           
            return response()->json(['err'=> $err,'infoError'=>$info,'id_op'=>$nro_op]);
          
  }//function



  public function setPagoProveedor(PagoOpformValidate $req){  
        //DETALLES CREACION
          $id_usuario = $this->getIdUsuario();
          $id_empresa = $this->getIdEmpresa();
          $id_estado_op =  52;//PENDIENTE  
          $err = false;
          $result_retencion = new \StdClass; 
          $result_retencion->retencion = false;
          $id_cabecera_op = null;
          $total_gravadas = null;
          $porcentaje_retencion = null;
          $base_imponible = null;
          $total_retencion = 0;
          $id_nc = 0;
          $id_anticipo = 0;
          $monto_anticipo = 0;
          $monto_nc = 0;
          $nro_op = 0;

          //DETALLE DE PROVEEDOR SELECCIONADOS
          
          $pagoDetalle = $req->input('pagosDetalle');
          $pagoCabecera = (Object)$req->input('pagosCabecera');

          //DETALLES DE PAGO PROVEEDOR
          $id_sucursal = $req->input('sucursal');
          $cotizacion = $req->input('cotizacion');
          $relacion_cotizacion = $req->input('relacion_cotizacion');
          $id_moneda = $pagoCabecera->id_moneda;
        try {
              Log::info("(USERID:".$id_usuario." METOD:setPagoProveedor)".json_encode($req->all()));
             DB::beginTransaction();
          
          //GENERAR INSERT PARA OP
          $detalles_op = [];
          $cabecera_op = [];
          $total_op = str_replace(',','.', str_replace('.','', $req->input('total_pago')));
          $info = '';
          $id_cuenta_contable = 0;
          
          //VERIFICAR SI ES PROVEEDOR LOCAL O EXTERIOR
          $proveedor_py = Persona::where('id',$pagoCabecera->id_proveedor)->where('id_pais',1455)->count();
        

          // --------------------- SI EL PROVEEDOR ES LOCAL ---------------------
          if($proveedor_py > 0 && $req->input('total_pago') > 0){


              $obj = new \StdClass; 
              $obj->id_proveedor = $pagoCabecera->id_proveedor;
              $obj->moneda = $id_moneda;
              $obj->data = $pagoDetalle;
              $result_retencion = $this->retencion($obj);
            //SI EXISTE RETENCION
              if($result_retencion->retencion){
                  $consulta = DB::select("SELECT get_parametro('monto_imponible') as base_imponible, 
                                                get_parametro('porcentaje_retencion') as porcentaje_retencion");                               
                  $base_imponible =  $consulta[0]->base_imponible;
                  $porcentaje_retencion = $consulta[0]->porcentaje_retencion;  
                  $total_retencion =  $result_retencion->monto;
        
              }
            
          
            $id_cuenta_contable = DB::select("SELECT get_id_cuenta_contable('PROV_LOCALES',".$id_empresa.",".$id_moneda.") AS n")[0]->n;

            if(!$id_cuenta_contable){
              throw new \Exception('No se encuentra configurado cuenta contable de parametro PROV_LOCALES en la moneda seleccionada');
            }

          } else {
            $id_cuenta_contable = DB::select("SELECT get_id_cuenta_contable('PROVEEDORES_EXT',".$id_empresa.",null)  AS n")[0]->n;

              if(!$id_cuenta_contable){
                throw new \Exception('No se encuentra configurado cuenta contable de parametro PROVEEDORES_EXT');
              }
          }


          // dd( $result_retencion,$pagoDetalle );


          if((float)$req->input('total_anticipo') != 0){
              $anular_anticipo = true;
          }else{
              $anular_anticipo = false;
          }
          if((float)$req->input('total_nc') != 0){
              $anular_nc = true;
          }else{
              $anular_nc = false;
          }

          $nro_op_funct = DB::select("SELECT public.get_documento(".$id_empresa.",'OP')");
          $nro_op = $nro_op_funct[0]->get_documento;

            //GENERAR CABECERA 
            $cabecera_op = [
                            'nro_op'=> $nro_op,
                            'id_usuario_creacion'=> $id_usuario,
                            'id_estado'=>$id_estado_op,
                            'total_pago'=> $req->input('total_pago'), 
                            'total_neto_pago'=>$req->input('total_pago_neto'),
                            'total_anticipo'=>$req->input('total_anticipo'),
                            'total_nota_credito'=>$req->input('total_nc'),
                            'total_retencion'=>$total_retencion,
                            'total_gravadas'=>$total_gravadas,
                            'id_moneda'=> $id_moneda,
                            'id_sucursal'=>$id_sucursal,
                            'id_centro_costo'=>$req->input('centro_costo'),
                            'cotizacion'=>$cotizacion,
                            'id_proveedor'=>$pagoCabecera->id_proveedor,
                            'id_empresa'=>$id_empresa,
                            'id_cuenta_contable'=>  $id_cuenta_contable,
                            'base_imponible'=>$base_imponible,
                            'porcentaje_retencion'=>$porcentaje_retencion,
                            'origen'=>'PP',
                            'fondo_fijo'=>false];

          $id_cabecera_op =  DB::table('op_cabecera')->insertGetId($cabecera_op); 

          DB::table('op_cabecera')
              ->where('id',$id_cabecera_op)
              ->update(['total_retencion' => $total_retencion]);           

          //GENERAR  DETALLE OP  
          foreach ($pagoDetalle as $key => $value) {
                if($value['operacion']['id_anticipo'] != ""){
                    $detalles_op[] = [
                                    'id_libro_compra'=>($value['operacion']['id_compra']) ? $value['operacion']['id_compra'] : null,
                                    'id_anticipo'=>($value['operacion']['id_anticipo']) ? $value['operacion']['id_anticipo']: null,
                                    'id_nc'=>($value['operacion']['id_nc']) ? $value['operacion']['id_nc']: null,
                                    //'monto'=>((float)$value['operacion']['monto_pago'] * -1),
                                    'monto'=>(float)$value['operacion']['monto_pago'],
                                    'id_cabecera' => $id_cabecera_op
                                    ];
                    
                    $id_anticipo = $value['operacion']['id_anticipo'];
                   // $monto_anticipo =  $monto_anticipo +((float)$value['operacion']['monto_pago'] * -1);
                   $monto_anticipo =  $monto_anticipo +(float)$value['operacion']['monto_pago'];
                }      

                if($value['operacion']['id_nc'] != ""){
                    $detalles_op[] = [
                                      'id_libro_compra'=>($value['operacion']['id_compra']) ? $value['operacion']['id_compra'] : null,
                                      'id_anticipo'=>($value['operacion']['id_anticipo']) ? $value['operacion']['id_anticipo']: null,
                                      'id_nc'=>($value['operacion']['id_nc']) ? $value['operacion']['id_nc']: null,
                                      //'monto'=>((float)$value['operacion']['monto_pago'] * -1),
                                      'monto'=>(float)$value['operacion']['monto_pago'],
                                      'id_cabecera' => $id_cabecera_op
                                      ];
                    $id_nc = $value['operacion']['id_nc'];
                   // $monto_nc =  $monto_nc + ((float)$value['operacion']['monto_pago'] * -1);
                   $monto_nc =  $monto_nc + (float)$value['operacion']['monto_pago'];
                }

                if($value['operacion']['id_compra'] != ""){
                    $detalles_op[] = [
                                      'id_libro_compra'=>($value['operacion']['id_compra']) ? $value['operacion']['id_compra'] : null,
                                      'id_anticipo'=>($value['operacion']['id_anticipo']) ? $value['operacion']['id_anticipo']: null,
                                      'id_nc'=>($value['operacion']['id_nc']) ? $value['operacion']['id_nc']: null,
                                      'monto'=>$value['operacion']['monto_pago'],
                                      'id_cabecera' => $id_cabecera_op
                                    ];
                }
          }//FOREACH

          DB::table('op_detalle')->insert($detalles_op);  



          if($anular_nc == true || $anular_anticipo == true){  
              if($req->input('total_pago_neto') == 0){

                DB::table('op_cabecera')
                ->where('id',$id_cabecera_op)
                ->update([
                          'fecha_hora_autorizado' =>  date('Y-m-d H:i:s'),
                          'id_usuario_autorizado' => $id_usuario,
                          'fecha_hora_verificado' => date('Y-m-d H:i:s'),
                          'id_usuario_verificado' => $id_usuario,
                          'id_estado' => 50 //Autorizado
                          ]);   


                  $this->procesar_op($id_cabecera_op, $id_usuario);
                
          
                }else{
                    //FUNCION PARA GENERAR ASIENTOS
                    DB::SELECT(' SELECT * FROM generar_op('.$id_cabecera_op.','. $id_usuario.')');
                }
          }else{
        
            //FUNCION PARA GENERAR ASIENTOS
            /**
             * La funcion generar_op hace que el LC que quede en estado pendiente de pago y marca
             * que tambien tiene un pago parcial
             */
            DB::SELECT(' SELECT * FROM generar_op('.$id_cabecera_op.','. $id_usuario.')');
          }

          $err = true;
          DB::commit();
        }  catch(ExceptionCustom $e){

          DB::rollback();
          //Aqui cae la exepcion creada manualmente
          Log::error($e);
          $err = false;
          $info = $e->getMessage();

       } catch(\Exception $e){
           Log::error($e);
           $err = false;
           $info = 'Ocurrio un error al procesar la OP';
           DB::rollBack();
       }
      


       return response()->json(['err'=> $err,'infoError'=>$info,'id_op'=>$nro_op, 'retencion'=>$result_retencion->retencion,'montoRetencion'=>$total_retencion, 'base_imponible'=>$base_imponible ]);
          
  }//function


/**
 *==========================================================================================================  
 *                                  VALIDACION OP
 *======================================================================================================== 
 */

public function cotrolSeleccionOp(Request $req)
{
        //  dd($req->all());
         $libroCompra =  DB::table('vw_item_detalle_op');
         $libroCompra = $libroCompra->where('id',$req->input('id_op'));
         $libroCompra = $libroCompra->where('activo',true);
         $libroCompra = $libroCompra->get();
        return response()->json(['data'=>$libroCompra]);
}//

public function cotrolSeleccionOps($id_op)
{
        //  dd($req->all());
         $libroCompra =  DB::table('vw_item_detalle_op');
         $libroCompra = $libroCompra->where('id',$id_op);
         $libroCompra = $libroCompra->get();
    /*     echo '</pre>';
         print_r(json_encode($libroCompra));
  die;*/
        return $libroCompra;
}//


public function pagosReservaNemo(Request $req){ 
      $libroCompra =  DB::table('vw_item_detalle_op');
      $libroCompra = $libroCompra->where('id',$req->input('id_op'));
      $libroCompra = $libroCompra->where('notificado_nemo',true);
      $libroCompra = $libroCompra->get();
      return response()->json(['data'=>$libroCompra]);
}


public function listPagoOp(Request $req)
{

      $forma_pago =  DB::table('op_cabecera as op');
      $forma_pago =  $forma_pago->select("fp_op_d.*",
                                         "pc.descripcion as cuenta_contable_n",
                                         "bc.nombre as origen_n"
                                        );
      $forma_pago = $forma_pago->Join('forma_pago_op_cabecera as fp_op', 'fp_op.id', '=','op.id_forma_pago');
      $forma_pago = $forma_pago->Join('forma_pago_op_detalle as fp_op_d', 'fp_op_d.id_cabecera', '=','fp_op.id');
      $forma_pago = $forma_pago->Join('banco_cabecera as bc', 'bc.id', '=','fp_op_d.id_forma_pago');
      $forma_pago = $forma_pago->Join('banco_detalle as bcd', 'bcd.id', '=','fp_op_d.id_banco_detalle');
      $forma_pago = $forma_pago->Join('plan_cuentas as pc', 'pc.id', '=','bcd.id_cuenta_contable');
      $forma_pago = $forma_pago->where('op.id',$req->input('id_op'));
      $forma_pago = $forma_pago->get();

      return response()->json(['data'=>$forma_pago]);


}//


//=====================================================================================================

  /**
   * Genera un pdf de los datos de una OP
   * @param  idOp
   * @return pdf de op
   */
public function pdfOp($idOp){

      $id_origen_asiento = '';
      $btn = $this->btnPermisos([],'verificarOp');
      $local = false;   
      $respuestaFactura = '';
      $facturasConceptos = [];
      $facturasRecortadas = '';

      $data = DB::table('op_cabecera as opc');
      $data = $data->select("opc.*", 
                            "e.denominacion as estado_n",
                            "prov.nombre as proveedor_n",
                            "prov.id_pais",
                            "cu.currency_code",
                            "cu.currency_id",
                            "prov.agente_retentor",
                            "prov.id_banco_cabecera",
                            "prov.nro_cuenta",
                            "prov.cheque_emisor_txt",
                            "bp.nombre as banco",
                            DB::raw("(SELECT sum (monto) as total_anticipo
                                    FROM op_detalle opd
                                    WHERE opd.id_cabecera =".$idOp."
                                    AND opd.id_anticipo is not null)"),
                          DB::raw("(SELECT sum (monto) as total_nc
                                    FROM op_detalle opd
                                    LEFT JOIN libros_compras lc ON lc.id = opd.id_libro_compra
                                    WHERE lc.id_tipo_documento in (2,32)
                                    AND opd.id_cabecera = ".$idOp.")"),

                            DB::raw("concat(us_c.nombre,' ',us_c.apellido) as persona_creacion_n"),
                            DB::raw("to_char(opc.fecha_hora_creacion, 'DD/MM/YYYY HH:MM') as fecha_format_creacion"),

                            DB::raw("concat(us_v.nombre,' ',us_v.apellido) as persona_verificado_n"),
                            DB::raw("to_char(opc.fecha_hora_verificado, 'DD/MM/YYYY HH:MM') as fecha_format_verificado"),

                            DB::raw("concat(us_a.nombre,' ',us_a.apellido) as persona_autorizado_n"),
                            DB::raw("to_char(opc.fecha_hora_autorizado, 'DD/MM/YYYY HH:MM') as fecha_format_autorizado"),

                            DB::raw("concat(us_anu.nombre,' ',us_anu.apellido) as persona_anulacion_n"),
                            DB::raw("to_char(opc.fecha_hora_anulacion, 'DD/MM/YYYY HH:MM') as fecha_format_anulacion"),

                            DB::raw("concat(us_proc.nombre,' ',us_proc.apellido) as persona_procesado_n"),
                            DB::raw("to_char(opc.fecha_hora_procesado, 'DD/MM/YYYY HH:MM') as fecha_format_procesado")
                          );
      $data = $data->leftJoin('estados as e', 'e.id', '=','opc.id_estado');
      $data = $data->leftJoin('personas as prov', 'prov.id', '=','opc.id_proveedor');
      $data = $data->leftJoin('bancos_plaza as bp', 'bp.id', '=','prov.id_banco_cabecera');
      $data = $data->leftJoin('currency as cu', 'cu.currency_id', '=','opc.id_moneda');
      $data = $data->leftJoin('personas as us_c', 'us_c.id', '=','opc.id_usuario_creacion');
      $data = $data->leftJoin('personas as us_v', 'us_v.id', '=','opc.id_usuario_verificado');
      $data = $data->leftJoin('personas as us_a', 'us_a.id', '=','opc.id_usuario_autorizado');
      $data = $data->leftJoin('personas as us_anu', 'us_anu.id', '=','opc.id_usuario_anulacion');
      $data = $data->leftJoin('personas as us_proc', 'us_proc.id', '=','opc.id_usuario_procesado');
      $data = $data->where('opc.id',$idOp);
      $data = $data->get();
      
      $formaPago = FormaPagoOpCabecera::with('fp_detalle')->where('id_op', $idOp)->get();

      if ($formaPago->isEmpty()) 
      {
        $formaPago = [];
      }   

      $asiento= [];
      $asiento_detalles= [];
      if($data[0]->id_asiento){
        $asiento = DB::select("SELECT * FROM asientos_contables where id = ".$data[0]->id_asiento);
        $id_origen_asiento = $asiento[0]->id_origen_asiento;

        $asiento_detalles = DB::table("asientos_contables_detalle as acd");
        $asiento_detalles = $asiento_detalles->select("acd.*", 
                       DB::raw("CONCAT(lc.nro_documento,lv.nro_documento) as nro_factura"),
                       "ac.concepto",
                      "pc.descripcion as cuenta_n",
                      "pc.cod_txt as num_cuenta",
                      "s.denominacion as sucursal",
                      "acd.id_sucursal",
                      "acd.id_centro_costo",
                       DB::raw("CONCAT(cc.nombre, ' ') AS centro_costo"));
        $asiento_detalles = $asiento_detalles->leftJoin('plan_cuentas as pc', 'pc.id', '=','acd.id_cuenta_contable' );
        $asiento_detalles = $asiento_detalles->leftJoin('asientos_contables as ac', 'ac.id', '=','acd.id_asiento_contable' );
        $asiento_detalles = $asiento_detalles->leftJoin('libros_compras as lc', 'lc.id_asiento', '=','ac.id' );
        $asiento_detalles = $asiento_detalles->leftJoin('libros_ventas as lv', 'lv.id_asiento', '=','ac.id' );
        $asiento_detalles = $asiento_detalles->leftJoin('sucursales_empresa as s', 's.id', '=','acd.id_sucursal' );
        $asiento_detalles = $asiento_detalles->leftJoin('centro_costo as cc', 'cc.id', '=','acd.id_centro_costo' );
        $asiento_detalles = $asiento_detalles->where('acd.activo', true);
        $asiento_detalles = $asiento_detalles->orderBy('acd.debe','desc');
        $asiento_detalles = $asiento_detalles->where('acd.id_asiento_contable',$data[0]->id_asiento);
        $asiento_detalles = $asiento_detalles->get();
      }

      $facturas = $this->cotrolSeleccionOps($idOp);

      $gastos = $this->getGasto($idOp);

      $fps = $this->getFps($idOp);
         
      foreach($facturas as $key=>$factura){
          if($key == 0){
            $respuestaFactura .= "$factura->nro_documento";
          }else{
            $respuestaFactura .= ",$factura->nro_documento";
          }
      }
      $resultado ='{'.$respuestaFactura.'}';
      // $facturasConceptos =  DB::select("SELECT public.extraer_parte_numerica_documento('".$resultado."')");
      foreach($facturasConceptos as $key=>$facturasConcepto){
          if($key == 0){
            $facturasRecortadas .= "$facturasConcepto->extraer_parte_numerica_documento";
          }else{
            $facturasRecortadas.= ",$facturasConcepto->extraer_parte_numerica_documento";
          }
      }
      $empresa = Empresa::where('id',$this->getIdEmpresa())->first();

			$logoSucursal = DB::select('SELECT p.logo FROM sucursales_empresa s, personas p WHERE s.id_persona = p.id AND s.id ='.$data[0]->id_sucursal);
			if(isset($logoSucursal[0]->logo)){
        if($logoSucursal[0]->logo != "" && $logoSucursal[0]->logo != 'factour.png'){
          $baseLogo = $logoSucursal[0]->logo;
          $logo = asset("personasLogo/$baseLogo");
          }else{
          $logo = asset("logoEmpresa/$empresa->logo");
          }
      }else{
        $logo = asset("logoEmpresa/$empresa->logo");
      }
   
    $pdf = \PDF::loadView('pages.mc.pagoProveedor.detalleOp',compact('asiento', 'asiento_detalles', 'data','idOp','btn', 'facturasRecortadas','formaPago','id_origen_asiento','gastos','fps','facturas','empresa','logo'));

    $pdf->setPaper('a4', 'letter')->setWarnings(false);
    return $pdf->download('Detalles de OP'.$this->getId4Log().'.pdf');


}//FUNCTION


/*
 * =========================================================================================================================
 *                                         VISTA DE CONTROL OP
 * =========================================================================================================================
 */
public function controlOp($id)
{

    /**
     * HOLA, ESTA FUNCION SIRVE PARA REGENERAR CUANDO TIENE EL TERCER PARAMETRO 
     * USAR SOLO DESDE UN ENTORNO LOCAL, UTILIZAR LA FUNCION CON PRECAUCION
     */
      //===================******=============================
      // DB::beginTransaction();
      //   $this->procesar_op($id,1,false);
      // DB::rollback();
      // DB::commit();
      //===================******=============================


      $cotizacion = 0;
      $id_origen_asiento = '';
      $btn = $this->btnPermisos([],'verificarOp');
      $local = false;   
      $respuestaFactura = '';
      $facturasConceptos = [];
      $facturasRecortadas = '';
  
      $data = DB::table('op_cabecera as opc');
      $data = $data->select("opc.*", 
                            DB::raw("COALESCE(opc.indice_cotizacion,0) as indice_cotizacion"),
                            "e.id as estado_id",
                            "e.denominacion as estado_n",
                            "prov.nombre as proveedor_n",
                            "prov.id_pais",
                            "cu.currency_code",
                            "cu.currency_id",
                            "prov.agente_retentor",
                            "prov.id_banco_cabecera",
                            "prov.nro_cuenta",
                            "prov.cheque_emisor_txt",
                            "bp.nombre as banco",
                            DB::raw("(SELECT sum (monto) as total_anticipos
                                      FROM op_detalle opd
                                      WHERE opd.id_cabecera =".$id."
                                      AND opd.id_anticipo is not null)"),
                            DB::raw("(SELECT sum (monto) as total_nc
                                      FROM op_detalle opd
                                      LEFT JOIN libros_compras lc ON lc.id = opd.id_libro_compra
                                      WHERE lc.id_tipo_documento in (2,32)
                                      AND opd.id_cabecera = ".$id.")"),
                            DB::raw("concat(us_c.nombre,' ',us_c.apellido) as persona_creacion_n"),
                            DB::raw("to_char(opc.fecha_hora_creacion, 'DD/MM/YYYY HH:MM') as fecha_format_creacion"),

                            DB::raw("concat(us_v.nombre,' ',us_v.apellido) as persona_verificado_n"),
                            DB::raw("to_char(opc.fecha_hora_verificado, 'DD/MM/YYYY HH:MM') as fecha_format_verificado"),

                            DB::raw("concat(us_a.nombre,' ',us_a.apellido) as persona_autorizado_n"),
                            DB::raw("to_char(opc.fecha_hora_autorizado, 'DD/MM/YYYY HH:MM') as fecha_format_autorizado"),

                            DB::raw("concat(us_anu.nombre,' ',us_anu.apellido) as persona_anulacion_n"),
                            DB::raw("to_char(opc.fecha_hora_anulacion, 'DD/MM/YYYY HH:MM') as fecha_format_anulacion"),

                            DB::raw("concat(us_proc.nombre,' ',us_proc.apellido) as persona_procesado_n"),
                            DB::raw("to_char(opc.fecha_hora_procesado, 'DD/MM/YYYY HH:MM') as fecha_format_procesado")
                          );
      $data = $data->leftJoin('estados as e', 'e.id', '=','opc.id_estado');
      $data = $data->leftJoin('personas as prov', 'prov.id', '=','opc.id_proveedor');
      $data = $data->leftJoin('bancos_plaza as bp', 'bp.id', '=','prov.id_banco_cabecera');
      $data = $data->leftJoin('currency as cu', 'cu.currency_id', '=','opc.id_moneda');
      $data = $data->leftJoin('personas as us_c', 'us_c.id', '=','opc.id_usuario_creacion');
      $data = $data->leftJoin('personas as us_v', 'us_v.id', '=','opc.id_usuario_verificado');
      $data = $data->leftJoin('personas as us_a', 'us_a.id', '=','opc.id_usuario_autorizado');
      $data = $data->leftJoin('personas as us_anu', 'us_anu.id', '=','opc.id_usuario_anulacion');
      $data = $data->leftJoin('personas as us_proc', 'us_proc.id', '=','opc.id_usuario_procesado');
      $data = $data->where('opc.id',$id);
      $data = $data->where('opc.id_empresa',$this->getIdEmpresa());
      $data = $data->get();
      if ($data[0]->adjunto){
      $adjuntos[] = ['archivo'=>$data[0]->adjunto]; 
      }else{
        $adjuntos = IntermediaAdjuntoOp::where('op_cabecera_id', $id)->get(['archivo']);
      }
      

      if(!isset($data[0]->id)){
				flash('La orden de pago solicitada no existe. Intentelo nuevamente !!')->error();
				return redirect()->route('home');
			}

      $cotizaciones = DB::select("select y.currency_id, y.currency_code, c.venta from cotizaciones c, currency y where c.id_currency = y.currency_id and fecha >= current_date and c.id = (select max(id) from cotizaciones x where y.currency_id = x.id_currency and id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.") order by 2 asc;"); 
      foreach($cotizaciones as $cotiza){
        if($cotiza->currency_id == $data[0]->id_moneda){
          $cotizacion = $cotiza->venta;
        }
      }

      $pms = $this->btnPermisos([],'controlRecibo');
      if(empty($pms)){
        $permiso = 0;
      }else{
        $permiso = 1;
      }
  
      $formaPago = FormaPagoOpCabecera::with('fp_detalle')->where('id_op', $id)->get();

      if ($formaPago->isEmpty()) 
      {
        $formaPago = [];
      }                     


      if($data[0]->id_asiento){
        $asiento = DB::select("SELECT * FROM asientos_contables where id = ".$data[0]->id_asiento);
        $id_origen_asiento = $asiento[0]->id_origen_asiento;
      }

 
      $facturas = $this->cotrolSeleccionOps($id);
      foreach($facturas as $key=>$factura){
          if($key == 0){
            $respuestaFactura .= "$factura->nro_documento";
          }else{
            $respuestaFactura .= ",$factura->nro_documento";
          }
      }
      $resultado ='{'.$respuestaFactura.'}';
      // $facturasConceptos =  DB::select("SELECT public.extraer_parte_numerica_documento('".$resultado."')");

      foreach($facturasConceptos as $key=>$facturasConcepto){
          if($key == 0){
            $facturasRecortadas .= "$facturasConcepto->extraer_parte_numerica_documento";
          }else{
            $facturasRecortadas.= ",$facturasConcepto->extraer_parte_numerica_documento";
          }
      }

      return view('pages.mc.pagoProveedor.controlOp',compact('permiso','cotizacion', 'data','id','btn', 'facturasRecortadas','formaPago','id_origen_asiento','adjuntos'));
}//

//aqui
public function aplicarOp($id)
{

  $data = DB::table('op_cabecera as opc');
  $data = $data->select(
                        "opc.id",
                        "opc.nro_op",
                        "opc.total_neto_pago",
                        DB::raw("COALESCE(opc.total_retencion,0) AS total_retencion"),
                        DB::raw("COALESCE(opc.total_gastos,0) AS total_gastos"),
                        DB::raw("COALESCE(opc.total_descuento,0) AS total_descuento"),
                        "opc.id_moneda",
                        "opc.id_estado",
                        "opc.id_proveedor",
                        "opc.entregado",
                        "e.denominacion as estado_n",
                        "prov.nombre as proveedor_n",
                        "prov.id_pais",
                        "cu.currency_code",
                        "cu.currency_id",
                        "opc.numeros_comprobantes"
                      );
  $data = $data->leftJoin('estados as e', 'e.id', '=','opc.id_estado');
  $data = $data->leftJoin('personas as prov', 'prov.id', '=','opc.id_proveedor');
  $data = $data->leftJoin('currency as cu', 'cu.currency_id', '=','opc.id_moneda');
  $data = $data->leftJoin('personas as us_c', 'us_c.id', '=','opc.id_usuario_creacion');
  $data = $data->leftJoin('personas as us_v', 'us_v.id', '=','opc.id_usuario_verificado');
  $data = $data->leftJoin('personas as us_a', 'us_a.id', '=','opc.id_usuario_autorizado');
  $data = $data->leftJoin('personas as us_anu', 'us_anu.id', '=','opc.id_usuario_anulacion');
  $data = $data->leftJoin('personas as us_proc', 'us_proc.id', '=','opc.id_usuario_procesado');
  $data = $data->where('opc.id',$id);
  $data = $data->first();
           
/*echo '<pre>';
print_r($data);*/

  $data_plan = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa', $this->getIdEmpresa())->get();
  // dd($data_plan);

  $btn = $this->btnPermisos([],'verificarOp');
  $local = false;                    
  
  /*FORMA PAGO*/
  $currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
  $origen = BancoCabecera::where('id_empresa',$this->getIdEmpresa())->where('activo',true)/*->where('cuenta_bancaria',true)*/->get();
  $tipo_operacion_pago = FormaPagoCliente::where('activo',true)->where('visible',true)->get();
  $beneficiario = Persona::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->get(['id','nombre','apellido','documento_identidad']); 
  // $idTipoPago = $formaPago[0]->fp_detalle[0];

  $formaPago = FormaPagoOpCabecera::with('fp_detalle')->where('id_op', $id)->get();

  if ($formaPago->isEmpty()) 
  {
    $formaPago = [];
  }
  // dd($formaPago);

  if($data->id_pais == '1455'){
    $local = true;
  }

  $respuestaFactura = '';
  $facturas = $this->cotrolSeleccionOps($id);
  foreach($facturas as $key=>$factura){
      if($key == 0){
        $respuestaFactura .= "$factura->nro_documento";
      }else{
        $respuestaFactura .= ",$factura->nro_documento";
      }
  }
  $resultado ='{'.$respuestaFactura.'}';
  // $facturasConceptos =  DB::select("SELECT public.extraer_parte_numerica_documento('".$resultado."')");
  $facturasConceptos = [];
  $facturasRecortadas = '';
  foreach($facturasConceptos as $key=>$facturasConcepto){
      if($key == 0){
        $facturasRecortadas .= "$facturasConcepto->extraer_parte_numerica_documento";
      }else{
        $facturasRecortadas.= ",$facturasConcepto->extraer_parte_numerica_documento";
      }
  }

  return view('pages.mc.pagoProveedor.aplicar',compact('data','data_plan','id','currency','origen','btn','tipo_operacion_pago','beneficiario','local', 'formaPago', 'facturasRecortadas'));
}//





/**
 * ========================================================================================================
 *                                GASTOS DE OP
 * ========================================================================================================
 */


    public function insertGasto(Request $req) {
        //VALIDAR SI LOS DATOS ESTAN COMPLETOS Y SON CORRECTOS.
      try{ 
          $this->validate($req, [
            'id_op' => 'required|numeric',
            'id_cuenta_contable'=>'required:numeric',
            'importe' => 'required|numeric',
            'id_tipo_gasto_descuento' => 'required|numeric'
        ]);

      } catch(\Exception $e){
                Log::error($e);
        return response()->json(['err'=>false]);
      }

      /**
       * El gasto se rige por la moneda de la OP
       * Entonces siempre sera cotizado a la  moneda de la op y luego se cotiza si fuera necesario a la moneda de la forma de pago
       * Se tiene que validar que exista cotizacion si es:
       * - Una moneda extranjera a PYG
       * - PYG a una moneda extranjera
       * Si la moneda es diferente a PYG y diferente a la moneda de la OP, entonces buscamos la relacion
       * Luego seria sumar al total de la OP y enviar al cotizador para actualizar la forma de pago
       */
      
        $id_empresa = $this->getIdEmpresa();
        $err = true;
        $id_gasto = 0;
        $id_op = (Integer)$req->id_op;
        $idUsuario = (Integer)Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
        $id_cuenta_contable = (Integer)$req->id_cuenta_contable;
        $importe = (float)$req->importe;
        $conceptoIng = $req->concepto;
        $id_moneda_gasto = (integer)$req->id_moneda_gasto;
        $total_op = 0;
        $editable = null;
        $total_gastos = 0;
        $sum  =true; 
        $info_err = '';

        //DATOS DE OP
        $op = OpCabecera::where('id',$id_op)->first();
        
        try{

        if($id_moneda_gasto == $op->id_moneda){
          $cant_decimal = $id_moneda_gasto != 111 ? 2 : 0;

          //Solo validamos tener la cotizacion , aqui no se cotizar porque el gasto es igual a la OP
          if($id_moneda_gasto != 111){
            $cotizacion = Cotizacion::where('id_currency',$op->id_moneda)
            ->where('id_empresa',$id_empresa)
            ->whereDate('fecha', '>=', date('Y-m-d'))
            ->orderBy('fecha', 'desc')
            ->first();
  
            if(!$cotizacion){
              throw new ExceptionCustom('Falta cargar cotización.');
            }

          } 

            $importeCotizado = $importe;
          
        } else if($id_moneda_gasto != 111 && $op->id_moneda != 111){
           //Si son dos monedas extranjeras entonces buscar su indice de cambio

            $cant_decimal = 2;
            $indice_data = CotizacionIndice::where('id_moneda_origen',$id_moneda_gasto)
            ->where('id_moneda_destino', $op->id_moneda)
            ->whereDate('created_at', '>=', date('Y-m-d'))
            ->where('id_empresa',$id_empresa)
            ->orderBy('created_at', 'desc')
            ->first();

            if(!$indice_data){
              throw new ExceptionCustom('Falta cargar indice de cotización.');
            }

            $cotizacion_indice = !$op->indice_cotizacion ? $indice_data->indice : $op->indice_cotizacion;
            $importeCotizado =  round($importe * $cotizacion_indice,2);
          
        } else if ($id_moneda_gasto == 111 && $op->id_moneda != 111){
          //Buscar la cotizacion de la OP

            $cant_decimal = 2;
            $cotizacion = Cotizacion::where('id_currency',$op->id_moneda)
            ->where('id_empresa',$id_empresa)
            ->whereDate('fecha', '>=', date('Y-m-d'))
            ->orderBy('fecha', 'desc')
            ->first();

            if(!$cotizacion){
              throw new ExceptionCustom('Falta cargar cotización.');
            }

            $importeCotizadoBase = DB::select('SELECT public.get_monto_cotizado('.$importe.','.$id_moneda_gasto.','.$op->id_moneda.','.$id_empresa.','.$cant_decimal.')');
            $importeCotizado = $importeCotizadoBase[0]->get_monto_cotizado;

        } else if($id_moneda_gasto != 111 && $op->id_moneda == 111){
          //Buscar la cotizacion de la moneda de gasto

            $cant_decimal = 0;
            $cotizacion = Cotizacion::where('id_currency',$id_moneda_gasto)
            ->where('id_empresa',$id_empresa)
            ->whereDate('fecha', '>=', date('Y-m-d'))
            ->orderBy('fecha', 'desc')
            ->first();

            if(!$cotizacion){
              throw new ExceptionCustom('Falta cargar cotización.');
            }

            $importeCotizadoBase = DB::select('SELECT public.get_monto_cotizado('.$importe.','.$id_moneda_gasto.','.$op->id_moneda.','.$id_empresa.','.$cant_decimal.')');
            $importeCotizado = $importeCotizadoBase[0]->get_monto_cotizado;

        }



        

      
              DB::beginTransaction();
            
              $tipo = $req->input('id_tipo_gasto_descuento') == '1' ? '+' /*TIPO GASTO:*/ : '-' /*TIPO DESCUENTO*/ ;
              $editable = $op->id_estado == 52 ? false : true; //ESTADO PENDIENTE /ES PARA EVITAR QUE SE PUEDA EDITAR EL GASTO CARGADO ANTERIORMENTE
              $cotizacion = DB::select('SELECT get_cotizacion_contable_actual(?,?,?)',[$id_empresa, $id_moneda_gasto, 'C']);//Cotizacion
              $cotizacion_gasto = $cotizacion[0]->get_cotizacion_contable_actual;

              //INSERT DE GASTO OP
              $gasto_table = array('id_op'                  => $id_op, 
                                    'id_cuenta_contable'    => $id_cuenta_contable,
                                    'monto'                 => $importeCotizado,
                                    'monto_moneda_original' => $importe,
                                    'id_usuario'            =>$idUsuario,
                                    'concepto'              =>$conceptoIng,
                                    'editable'              =>$editable,
                                    'tipo'                  => $tipo,
                                    'cotizacion_gasto'      => $cotizacion_gasto,
                                    'id_moneda_gasto'       =>$id_moneda_gasto
                                  );                      
              //INSERT                 
              $id_gasto = DB::table('gastos_op')->insertGetId($gasto_table);     
              
              //TOTAL GASTO Y TOTAL DESCUENTO
              $total_gastos = GastoOp::where('id_op', $id_op)->where('tipo','+')->sum('monto'); //SUMA GASTOS
              $total_descuento = GastoOp::where('id_op', $id_op)->where('tipo','-')->sum('monto'); //RESTA GASTOS  


                                 
              //ACTUALIZAR CABECERA DE OP
              if($this->getIdEmpresa()  == 1 || $this->getIdEmpresa()  == 5 || $this->getIdEmpresa()  == 14 || $this->getIdEmpresa() == 17){
                OpCabecera::where('id',$id_op)->update(['total_gastos' => $total_gastos,
                                                        'total_descuento' => $total_descuento]); 

                  $total_op = $op->total_neto_pago + $total_gastos -  $total_descuento;
                    //SUMAR O DESCONTAR EL PAGO DE OP  / FORMA DE PAGO
              }else{
                  $total_op = $op->total_neto_pago ;  
              } 
    
            
              if($total_op < 0.00){
                throw new ExceptionCustom('Valor negativo en insertar Gasto');
              }


              $cabecera_pago = FormaPagoOpCabecera::where('id_op',$id_op)->first();
              if($cabecera_pago){

                $total_fp = $this->cotizarFP($id_op, $cabecera_pago->id_moneda, $cabecera_pago->cotizacion);

                //ACTUALIZA LA FORMA DE PAGO CON EL VALOR A SER PAGADO
                FormaPagoOpDetalle::where('id_cabecera',$cabecera_pago->id)->update(['importe_pago'=>$total_fp]);
                FormaPagoOpCabecera::where('id',$cabecera_pago->id)->update(['importe_total'=>$total_fp]);
              }



              DB::commit();

        } catch(ExceptionCustom $e){

            DB::rollback();
            //Aqui cae la exepcion creada manualmente
            Log::error($e);
            $err = false;
            $info_err = $e->getMessage();

        } catch(\Exception $e){
            Log::error($e);
            $err = false;
            $info_err = 'Ocurrio un error al intentar insertar el gasto';
            DB::rollBack();
        }

        return response()->json(['err'=>$err,'id_gasto'=>$id_gasto,'total'=> $total_op, 'concepto'=>$conceptoIng, 'msj' => $info_err]);
    }

    public function deleteGasto(Request $req)
    {

        // try{ 
          $this->validate($req, [
            'id_gasto' => 'required|numeric',
            'id_op'=> 'required|numeric'
        ]);

        
          $err = true;
          $info_err = '';
          $total_op = 0;
          $total_gastos = 0;
          $importe = 0;
          $id_op = $req->input('id_op');
          $op = OpCabecera::where('id',$id_op)->first();

         try{
            DB::beginTransaction();
              //ELIMINAR GASTOS
              GastoOp::where('id', $req->id_gasto)->delete();

              //OBTENER TOTAL DE GASTOS Y DSCUENTOS
              $total_gastos = GastoOp::where('id_op', $id_op)->where('tipo','+')->sum('monto'); //SUMA GASTOS
              $total_descuento = GastoOp::where('id_op', $id_op)->where('tipo','-')->sum('monto'); //RESTA GASTOS 

                                                                        
              //ACTUALIZAR CABECERA DE OP
              if($this->getIdEmpresa()  == 1 || $this->getIdEmpresa()  == 5 || $this->getIdEmpresa()  == 14 || $this->getIdEmpresa() == 17){
                OpCabecera::where('id',$id_op)->update(['total_gastos' => $total_gastos,
                                                        'total_descuento' => $total_descuento]); 

                  $total_op = $op->total_neto_pago + $total_gastos -  $total_descuento;
                    //SUMAR O DESCONTAR EL PAGO DE OP  / FORMA DE PAGO
              }else{
                  $total_op = $op->total_neto_pago ;  
              } 
    
            
              if($total_op < 0.00){
                throw new ExceptionCustom('Valor negativo en insertar Gasto');
              }


              $cabecera_pago = FormaPagoOpCabecera::where('id_op',$id_op)->first();
              if($cabecera_pago){

                $total_fp = $this->cotizarFP($id_op, $cabecera_pago->id_moneda, $cabecera_pago->cotizacion);

                //ACTUALIZA LA FORMA DE PAGO CON EL VALOR A SER PAGADO
                FormaPagoOpDetalle::where('id_cabecera',$cabecera_pago->id)->update(['importe_pago'=>$total_fp]);
                FormaPagoOpCabecera::where('id',$cabecera_pago->id)->update(['importe_total'=>$total_fp]);
              }
                                      

 
            DB::commit();
         

          } catch(ExceptionCustom $e){

              DB::rollback();
              //Aqui cae la exepcion creada manualmente
              Log::error($e);
              $err = false;
              $info_err = $e->getMessage();

          } catch(\Exception $e){
              Log::error($e);
              $err = false;
              $info_err = 'Ocurrio un error al intentar insertar el gasto';
              DB::rollBack();
          }
         
         return response()->json(['err'=>$err,'e'=>$info_err,'total_op'=>$total_op,'total_gastos'=>$total_gastos]);
    }//


    public function getGastos(Request $req)
    {
      try{ 
        $this->validate($req, [
          'id_op' => 'required|numeric'
      ]);

      } catch(\Exception $e){
          Log::error($e);
        return response()->json(['err'=>false]);
      }
        $err = true;
        $data;
        try{
          $data =  GastoOp::with('cuenta_contable','moneda')->where('id_op', $req->id_op)->get();
        } catch(\Exception $e){
            Log::error($e);
          $err = false;
        }

        return response()->json(['err'=>$err,'data'=>$data]);

    }//


    public function getGasto($id_op)
    {
        // dd($req->all());
        $err = true;
        $data;
        try{
          $data =  GastoOp::with('cuenta_contable')->where('id_op', $id_op)->get();
        } catch(\Exception $e){
            Log::error($e);
          $err = false;
        }

        return $data;

    }//

/**
 * ========================================================================================================
 *                                FORMA DE PAGO DE OP  - ABM
 * ========================================================================================================
 */

public function insertFp(Request $req)
{

      $resp = new \StdClass;
      $resp->fp_op = true;
      $resp->err = true;
      $resp->msj = '';
      $resp->id_fp_detalle = 0;
      $resp->id_cabecera = 0;
      $idUsuario = (Integer)Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
      $idEmpresa = $this->getIdEmpresa();
      $tipo_operacion = $req->input('id_tipo_operacion');
      $id_banco_detalle = (Integer)$req->input('id_banco_detalle');
      $cotizacion = 0;
      $log_info = "(USERID:".$this->getIdUsuario()." METOD:insertFp)".json_encode($req->all());
      $log_error = "(USERID:".$this->getIdUsuario()." METOD:insertFp) ";

      try{
      $forma_pago_check = FormaPagoOpCabecera::where('id_op',$req->input('id_op'))->where('id_empresa',$idEmpresa)->firstOrFail();
    } catch(\Exception $e){
      $resp->fp_op = false;
    }

    try{
          Log::info($log_info);

          DB::beginTransaction();

          $fp_detalle = new FormaPagoOpDetalle;
      

          switch ($tipo_operacion) {
            case 4: //Cheque

                $cheque = new ChequeFp;
                $cheque->id_beneficiario = ($req->input('al_portador')) ?  null  : $req->input('id_beneficiario');
                $cheque->id_cuenta = $id_banco_detalle;
                $cheque->nro_cheque = $req->input('nro_comprobante');
                $cheque->id_moneda = (Integer)$req->input('id_moneda');
                $cheque->importe = (float)$req->input('importe_pago');
                $cheque->cotizacion = (float)$req->input('cotizacion');
                $cheque->fecha_emision = $this->formatoFechaEntrada($req->input('fecha_emision'));
                $cheque->fecha_vencimiento = $this->formatoFechaEntrada($req->input('fecha_vencimiento'));
                $cheque->id_usuario = $idUsuario;
                $cheque->id_empresa = $idEmpresa;
                $cheque->id_tipo_cheque = 3;
                $cheque->id_op = (Integer)$req->input('id_op');
                $cheque->emisor_txt = $req->input('beneficiario');
                $cheque->al_portador = $req->input('al_portador');
                $cheque->save();
              
                
                $fp_detalle->id_cheque =  $cheque->id;
                $fp_detalle->id_banco_detalle = $id_banco_detalle;

                $banco_detalle = BancoDetalle::findOrFail($id_banco_detalle);
                if(!$banco_detalle->id_cuenta_contable){
                  throw new \Exception('Falta configurar cuenta contable de cuenta de fondo.');
                }

                $fp_detalle->id_cuenta_contable = $banco_detalle->id_cuenta_contable;

              break;

            case 1: //EFECTIVO
              $cuenta_contable_pago = CtaCtbFormaPago::where('id_forma_pago',1)
                                      ->where('id_moneda',$req->input('id_moneda'))
                                      ->where('id_empresa',$idEmpresa)
                                      ->where('activo',true)
                                      ->first();
                if(!$cuenta_contable_pago){
                  throw new \Exception('Falta configurar parametros de forma pago.');
                }

                $fp_detalle->id_banco_detalle = $cuenta_contable_pago->id_cuenta_banco;
                $fp_detalle->id_cuenta_contable = $cuenta_contable_pago->id_cuenta_contable;

              
              break;

            case 10: //CANJE
              $cuenta_contable_pago = CtaCtbFormaPago::where('id_forma_pago',10)
              ->where('id_moneda',$req->input('id_moneda'))
              ->where('id_empresa',$idEmpresa)
              ->where('activo',true)
              ->first();

              if(!$cuenta_contable_pago){
                throw new \Exception('Falta configurar parametros de forma pago.');
              }

              $fp_detalle->id_banco_detalle = $cuenta_contable_pago->id_cuenta_banco;
              $fp_detalle->id_cuenta_contable = $cuenta_contable_pago->id_cuenta_contable;
            
              break;

            case 12: //CANJE 2
                $cuenta_contable_pago = CtaCtbFormaPago::where('id_forma_pago',12)
                ->where('id_moneda',$req->input('id_moneda'))
                ->where('id_empresa',$idEmpresa)
                ->where('activo',true)
                ->first();
  
                if(!$cuenta_contable_pago){
                  throw new \Exception('Falta configurar parametros de forma pago.');
                }
  
                $fp_detalle->id_banco_detalle = $cuenta_contable_pago->id_cuenta_banco;
                $fp_detalle->id_cuenta_contable = $cuenta_contable_pago->id_cuenta_contable;
              
              break;

            
            default:

              $banco_detalle = BancoDetalle::findOrFail($id_banco_detalle);
              $fp_detalle->id_banco_detalle = $id_banco_detalle;
              $fp_detalle->id_cuenta_contable = $banco_detalle->id_cuenta_contable;

            break;
            
          }


      //VERIFICAR SI EXISTE FORMA PAGO DE LA OP
        if($resp->fp_op){
          $resp->id_cabecera = $forma_pago_check->id;

        } else {

          $cotizacion_contable_venta = (float)DB::select("SELECT get_cotizacion_contable_actual(".$this->getIdEmpresa().",'V') AS n")[0]->n;

          if((float)$req->input('cotizacion') == $cotizacion_contable_venta){
            $cotizacion = (float)$req->input('cotizacion');
          } else {
            $cotizacion =  (float)$req->input('cotizacion');
          }

          $op = OpCabecera::find((Integer)$req->input('id_op'));
          $resp->id_cabecera = DB::table('forma_pago_op_cabecera')->insertGetId(
                ['importe_total' => str_replace(',','.', str_replace('.','', $req->input('total'))), 
                'id_op' => (Integer)$req->input('id_op'),
                'id_usuario'=>$idUsuario ,
                'id_moneda'=>(Integer)$req->input('id_moneda'),
                'cotizacion'=>  $cotizacion,
                'id_empresa'=> $idEmpresa,
                'indice_cotizacion' => $op->indice_cotizacion
                ]
            );


        }

 
          $fp_detalle->id_cabecera = $resp->id_cabecera;
          $fp_detalle->documento = (String)$req->input('documento');
          $fp_detalle->importe_pago = (float)str_replace(',','.', str_replace('.','', $req->input('importe_pago')));

          $fp_detalle->nro_comprobante = $req->input('nro_comprobante');
          $fp_detalle->fecha_emision = $this->formatoFechaEntrada($req->input('fecha_emision'));
          $fp_detalle->fecha_vencimiento = $this->formatoFechaEntrada($req->input('fecha_vencimiento'));
          $fp_detalle->id_tipo_pago = $req->input('id_tipo_operacion');
          $fp_detalle->id_beneficiario = ($req->input('al_portador')) ?  null  : $req->input('id_beneficiario');

          $fp_detalle->beneficiario_txt = $req->input('beneficiario');
          $fp_detalle->al_portador = $req->input('al_portador');
 
          $fp_detalle->save();
          $resp->id_fp_detalle = $fp_detalle->id;
          DB::commit();
        

      } catch(\Exception $e){
            Log::error($log_error);
            Log::error($e);
          
          if(env('APP_DEBUG')){
            $resp->e = $e;
          }
          $resp->err = false;
          $resp->msj = $e->getMessage();
         DB::rollBack();
       }



    return response()->json($resp);

}//

        public function getFp(Request $req) 
        {

          try{ 
            $this->validate($req, [
              'id_op' => 'required|numeric'
          ]);
          
          } catch(\Exception $e){
                Log::error($e);
            return response()->json(['err'=>false]);
          }

          $err = true;
          try{
              $forma_pago_cab = FormaPagoOpCabecera::with('op',
                                                          'fp_detalle',
                                                          'fp_detalle.banco_detalle.banco_cabecera',
                                                          'fp_detalle.banco_detalle.cuenta_contable',
                                                          'fp_detalle.forma_pago',
                                                          'moneda'
                                                          )->where('id_op',$req->input('id_op'))->first();    
                                                    
            } catch(\Exception $e){
                Log::error($e);
              $err = false;
              $forma_pago_cab = [];
            }
             
            return response()->json(['err'=>$err,'data'=>$forma_pago_cab]);

        }//
        
  
        public function getFps($id_op) 
        {

          $err = true;
          try{
              $forma_pago_cab = FormaPagoOpCabecera::with('fp_detalle',
                                                          'fp_detalle.banco_detalle.banco_cabecera',
                                                          'fp_detalle.banco_detalle.cuenta_contable',
                                                          'fp_detalle.forma_pago',
                                                          'moneda'
                                                          )->where('id_op',$id_op)->first();    
                                                    
            } catch(\Exception $e){
                Log::error($e);
              $err = false;
              $forma_pago_cab = [];
            }
             
            return $forma_pago_cab;

        }//

        public function deleteFp(Request $req)
        {

                  // dd($req->all());
                  //VALIDACION
                try{ 
                  $this->validate($req, [
                    'id_fp_detalle' => 'required|numeric',
                    'id_op' =>'required|numeric'
                ]);
                
                } catch(\Exception $e){
                    Log::error($e);
                  return response()->json(['err'=>false]);
                }

                $fp_op = true;
                $err = true;
                try{
                    DB::beginTransaction();

                    $data = FormaPagoOpDetalle::where('id',$req->input('id_fp_detalle'))->first();

                  //ELIMINAR CHEQUE CARGADO
                  if($data->id_cheque){
                    ChequeFp::where('id',$data->id_cheque)->update(['activo'=>false]);
                  }

                  //ELIMINAR ADJUNTO
                  //PASAR ESTO A ANULAR OP
                  // if($data->adjunto){
                  //   $directorioImagen = 'adjuntoDetalle/documentos_op/';    
                  //   unlink($directorioImagen.$data->adjunto);
                    
                  // }

                    FormaPagoOpDetalle::where('id',$req->input('id_fp_detalle'))->delete();
                    $forma_pago = FormaPagoOpCabecera::has('fp_detalle','>','0')->where('id_op',$req->input('id_op'))->get();
                    // dd($forma_pago);
                    //ELIMINAR CABECERA SI ES EL ULTIMO DETALLE
                    if($forma_pago->isEmpty()){
                      FormaPagoOpCabecera::where('id_op',$req->input('id_op'))->delete();
                      DB::table('op_cabecera')
                          ->where('id',$req->input('id_op'))
                          ->update(['id_forma_pago' => null]);
                    }

                    DB::commit();
                    // dd($forma_pago);
                } catch(\Exception $e){
                      Log::error($e);
                    $err = false;
                    DB::rollBack();
                }
                
                  return response()->json(['err'=>$err]);
        }



        public function  procesarOp(Request $req) 
        {

                  //FALTA VALIDAR CONCEPTO Y ENTREGADO
                  try{ 
                    $this->validate($req, [
                      'id_op' =>'required|numeric'
                  ]);
                  
                  } catch(\Exception $e){
                        Log::error($e);
                    return response()->json(['err'=>false, 'e' => $e->getMessage()]);
                  }


                  $idUsuario = (Integer)Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
                  $id_op = (Integer)$req->input('id_op');
                  $resp = new \StdClass;
                  $resp->err = true;
                  $resp->e = '';

                 try{ 
                    DB::beginTransaction();
                    $indicador = DB::select("SELECT count(DISTINCT fondo_fijo) > 1 AS control FROM vw_op_detalle WHERE id_op = ".$id_op);
                    if($indicador[0]->control == false){
                          OpCabecera::where('id',$id_op)->update(['concepto' =>  $req->input('concepto'),
                                                                  'entregado'=> $req->input('entregado')]); 
                          $this->procesar_op($id_op, $idUsuario);

                    }else{
                          $resp->err = false;
                          $resp->e = 'Exiten Libros de Compras de diferentes tipos en la OP';
                    }
             
                    DB::commit();


                  } catch(ExceptionCustom $e){
                    DB::rollBack();
                    $resp->err = false;
                    $resp->e = $e->getMessage();
          
                 } catch(\Exception $e){
                     Log::error($e);
                     $resp->err = false;
                     $resp->e = 'Ocurrio un error al procesar la OP';
                     DB::rollBack();
                 } 


                   return response()->json($resp);

          
        }


        public function cotizarFP($id_op, $id_moneda, $cotizacion){
               //ENCONTRAR O FALLAR 
               $op = OpCabecera::where('id',$id_op)->firstOrFail();
               $moneda_costo = $op->id_moneda;
               $moneda_venta = $id_moneda;
               $cotizacion_indice = null;

              // if( $this->getIdEmpresa() == 1){
                     $total_neto_pago = ($op->total_neto_pago +  $op->total_gastos) - ($op->total_descuento/*+$op->total_retencion*/);
              //  }else{   
              //        $total_neto_pago = ($op->total_neto_pago +  $op->total_gastos) - ($op->total_descuento+$op->total_retencion);
              //  }
               

               $cotizacion_indice = $op->indice_cotizacion;
               if(!$op->indice_cotizacion){
                 $cotizacion_indice = CotizacionIndice::where('id_empresa',$this->getIdEmpresa())
                   ->where('id_moneda_origen',$moneda_costo)
                   ->where('id_moneda_destino',$moneda_venta)
                   ->orderBy('id','DESC')
                   ->first(['indice']);
     
                   if($cotizacion_indice){
                     $cotizacion_indice = $cotizacion_indice->indice;
                   }
                 }
     


               if( $moneda_costo != $moneda_venta && $moneda_venta != 111 && $moneda_costo != 111){
                 $total =  round($total_neto_pago * $cotizacion_indice,2);
               } else {
                 $cotizacion = str_replace(',','.', str_replace('.','',$cotizacion));
                 $total = DB::select('SELECT get_monto_cotizacion_custom(?,?,?,?) AS total', [$cotizacion, $total_neto_pago, $moneda_costo, $moneda_venta])[0]->total;
               }

               return $total;
        }



        public function cotizarMontoFP(Request $req)
        {
            try{

                $total = $this->cotizarFP($req->id_op, $req->id_moneda, $req->cotizacion);
            
                return response()->json(['total'=> $total ]);

            } catch(\Exception $e){
                  Log::error($e);
              return response('La operación no puede continuar ocurrio un error en la conversión de valores',406);
            }


        }//  

        /**
         * Devuelve la cuenta contable de los tipos de operaciones habilitadas
         */
        public function getListCuentas(Request $req)
        {
          // dd($req->all());
          $cuentas = DB::table('banco_detalle as bd');
          $cuentas = $cuentas->select('bd.*','tc.denominacion','cu.currency_code');
          $cuentas = $cuentas->leftJoin('banco_cabecera as bc', 'bc.id', '=','bd.id_banco');
          $cuentas = $cuentas->leftJoin('tipo_cuenta_banco as tc', 'tc.id', '=','bd.id_tipo_cuenta');
          $cuentas = $cuentas->leftJoin('currency as cu', 'cu.currency_id', '=','bd.id_moneda');
          $cuentas = $cuentas->where('bc.id_empresa',$this->getIdEmpresa());
          $cuentas = $cuentas->where('bd.id_banco',$req->input('id_banco'));
          if($req->input('id_tipo_operacion')){
              if($req->input('id_tipo_operacion') == 11){
               
                $cuentas = $cuentas->where('bd.id_tipo_cuenta',4);
              }
              /*if($req->input('id_tipo_operacion') == 4|| $req->input('id_tipo_operacion') == 6){
                 $tipocuenta = array(1,2);
                 $cuentas = $cuentas->whereIn('id_tipo_cuenta', $tipocuenta );
              }  */
          }
          $cuentas = $cuentas->get();
          return response()->json(['cuentas'=>$cuentas]);

        }//



      /**
       * ===============================================================================================
       *                                ANTICIPO
       * ===============================================================================================
       *  */  


       public function indexAnticipo()
       {
          $currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
          $banco = BancoDetalle::whereHas('banco_cabecera', function ($query) {
            $query->where('id_empresa', $this->getIdEmpresa());
                  })->with('currency','tipo_cuenta')->get();
          $beneficiario = Persona::whereIn('id_tipo_persona',array(14,8))->where('id_empresa',$this->getIdEmpresa())->where('activo',true)->get(['id','nombre','apellido', 'documento_identidad', 'dv']);  
          $centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();  
          $sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
          return view('pages.mc.pagoProveedor.anticipo',compact('sucursalEmpresa','centro','currency','beneficiario','banco'));
       }//


       public function anticipoAdd()
       {
          $idEmpresa = $this->getIdEmpresa();

          $origen = BancoCabecera::where('id_empresa',$idEmpresa)->where('activo',true)->where('cuenta_bancaria',true)->get();
          $tipo_operacion_pago = FormaPagoCliente::where('activo',true)->where('visible',true)->get();
          $currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
          $banco = BancoDetalle::whereHas('banco_cabecera', function ($query) {
            $query->where('id_empresa', $this->getIdEmpresa());
                })->with('currency','tipo_cuenta')
          ->get();
        //  $beneficiario = Persona::whereIn('id_tipo_persona',array(14,8))->where('id_empresa',$idEmpresa)->where('activo',true)->get(['id','nombre','apellido','tipo_proveedor']);  
          $beneficiario = DB::select("SELECT * FROM personas 
                                        WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
                                                      WHERE puede_facturar = true) 
                                        AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");

          $centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();  
          $cuentas_contables = PlanCuenta::where('activo',true)->where('id_empresa', $idEmpresa)->where('asentable', true)->get();
          $sucursalEmpresa = SucursalEmpresa::where('id_empresa',$idEmpresa)->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
          $cotizacion_contable_venta = (float)DB::select("SELECT get_cotizacion_contable_actual(".$idEmpresa.",'V') AS n")[0]->n;     


        return view('pages.mc.pagoProveedor.anticipoAdd',compact('sucursalEmpresa',
                                                                 'centro','currency','beneficiario',
                                                                 'banco','cuentas_contables','origen','tipo_operacion_pago',
                                                                 'cotizacion_contable_venta'));


       }//

       public function anticipoDetalle($id)
       {
            $idEmpresa = $this->getIdEmpresa();
            $btn =  $this->btnPermisos([],'detalleAnticipo');
            $currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
            $beneficiario = Persona::whereIn('id_tipo_persona',array(14,8))->where('id_empresa',$this->getIdEmpresa())->get(['id','nombre','apellido']);  
            $centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();    
            $cuentas_contables = PlanCuenta::where('id_empresa', $idEmpresa)->where('asentable', true)->get();
            $sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
            $anticipo = Anticipo::with('op','usuarioAnulacion')->where('id',$id)->where('id_empresa',$idEmpresa)->first();
            $tipo_operacion_pago = FormaPagoCliente::get();
            $forma_pago = FormaPagoOpCabecera::with(['fp_detalle.forma_pago', 'fp_detalle' => function($query){
                $query->selectRaw("*, to_char(fecha_emision, 'DD/MM/YYYY') AS fecha_emision_format, to_char(fecha_vencimiento, 'DD/MM/YYYY') AS fecha_vencimiento_format");
            },'moneda','fp_detalle.banco_detalle.banco_cabecera'])->where('id_op',$anticipo->id_op)
            ->get();
            return view('pages.mc.pagoProveedor.anticipoDetalle',compact('sucursalEmpresa',
                                                                        'centro','currency',
                                                                        'beneficiario','cuentas_contables',
                                                                        'anticipo','btn','tipo_operacion_pago','forma_pago'));

        }//

        public function anularAnticipo(Request $req) 
        {
            $idUsuario = $this->getIdUsuario();
            $idAnticipo = $req->input('id_anticipo');
            $err= true;
            $msj = '';

            //Verificar permiso
          try{ 
            $anular_anticipo = DB::select('SELECT * FROM anular_anticipo('.$idAnticipo.','.$idUsuario.')');

            if($anular_anticipo['0']->anular_anticipo !== 'OK'){
              $err = false;
              $msj = $anular_anticipo['0']->anular_anticipo;
            }

          } catch(\Exception $e){
                Log::error($e);
            $err = false;
            $msj = 'Ocurrió un error al intentar anular.';
          }

          return response()->json(['err'=>$err,'msj'=>$msj]);

        }


    		public function getFacturasAplicar(Request $req)
        {
           $flag = 0;
            $draw = intval($req->draw);
            $start = intval($req->start);
            $length = intval($req->length);
            $cont = 0;
            $filtrarMax = 0;
        
            $factura = DB::table('vw_anticipo_lc');
            $factura = $factura->where('id_empresa',$this->getIdEmpresa());
            $factura = $factura->where('saldo','>',0);
            $factura = $factura->where('id_proveedor','=',$req->input('idProveedor'));
            $factura = $factura->get();

            $resultado = [];
            $cotizacion = $req->input('baseC');
            foreach($factura as $key=>$value){
                $coincidencia = strpos($value->saldo, 'e-');
			          if ($coincidencia === false) {
                    $cotizado = DB::select('SELECT public.get_monto_cotizacion_custom('.$cotizacion.','.$value->saldo.','.$value->id_moneda.','.$req->input('datamoneda').')AS resultado');
                    $value->saldo_cotizado = $cotizado[0]->resultado;
                    $resultado[] = $value;
                }
            }
            return response()->json($resultado); 
        }



   public function getListAnticipo(Request $req) 
       {

        $data = new Anticipo;
        $data = $data->with('beneficiario_format','currency','op','op_aplicacion.opCabecera')
        ->selectRaw("to_char(fecha_pago,'DD/MM/YYYY') as fecha_format_pago,
                    to_char(fecha_vencimiento,'DD/MM/YYYY') as fecha_format_vencimiento,
                    *");
              
        $data = $data->where('id_empresa',$this->getIdEmpresa());       
        $data = $data->where('tipo','P');//OBTIENE LOS ANTICIPOS DE TIPO PROVEEDOR 

        if($req->input('fecha_anticipo')){
          //$data = $data->where('fecha_transferencia',$req->input('fecha_emision')); 
          $fecha = explode('-', $req->input('fecha_anticipo'));
          $desde = $this->formatoFechaEntrada(trim($fecha[0]));
          $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
          $data = $data->whereBetween('fecha_pago', array(
                                date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
                                date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                              ));
        }
    
        if($req->input('id_beneficiario')){
          $data = $data->where('id_beneficiario',$req->input('id_beneficiario'));
        }
        if($req->input('estado')){
          if($req->input('estado') == 3){ 
              $data = $data->where('activo', false);  
          }else{
              if($req->input('estado') == 1){ 
                  $data = $data->where('saldo','>',0);
                  $data = $data->where('activo',true);  
              }else if($req->input('estado') == 2){ 
                $data = $data->where('saldo','<=',0);
                $data = $data->where('activo',true);  

              }
          }
          //data = $data->where('pendiente',$req->input('pendiente'));
        }
        if($req->input('nro_anticipo')){
          $data = $data->where('id',$req->input('nro_anticipo'));
        }

        if($req->input('id_moneda')){
          $data = $data->where('id_moneda',$req->input('id_moneda'));
        }
        if($req->input('activo')){
        }
        $data = $data->get();


        $resultadoBase = [];
        foreach($data as $key=>$resultado){
            $ops = '';
            foreach($resultado->op_aplicacion as $keys=>$opAplicacion){
                if($keys == 0){
                  $ops .= $opAplicacion->opCabecera->nro_op;
                }else{
                  $ops .= ','.$opAplicacion->opCabecera->nro_op;
                }
            }
            if($resultado->activo == false){
              $resultado->estado = 'ANULADO';
            }else{
                if($resultado->saldo > 0){
                    $resultado->estado = 'PENDIENTE';
                }else{
                    $resultado->estado = 'APLICADO';
                }
            }


           
            $resultado->op_aplicado = $ops;
            $resultadoBase[] = $resultado;
        }

        if($req->input('nro_op')){
          $resultados = [];
          foreach($resultadoBase as $key=>$resultado){
            if($resultado->op->nro_op == $req->input('nro_op')){
              $resultados[] = $resultado;
            }
          }
          $resultadoBase = [];
          $resultadoBase = $resultados;
        }


        return response()->json(['data'=>$resultadoBase]);
       }//


       public function saveAnticipo(Request $req)
       {
                $idUsuario =  $this->getIdUsuario();
                $idEmpresa = $this->getIdEmpresa();
                $resp = new \StdClass;
                $resp->err = true;
                $resp->id_op = 0;
                $resp->msj = 'Ocurrio un error al intentar almacenar los datos. ';
                $tipo_operacion = $req->input('id_tipo_operacion');
                $concepto_op = '';
                $tipo_operacion_pago_base = FormaPagoCliente::where('id',$tipo_operacion)->first();
                $concepto_forma_pago = $tipo_operacion_pago_base->denominacion;

                $concepto_anticipo = $req->input('concepto'); 

                $importe_total = (float)$req->input('importe');
                $id_banco_detalle = $req->input('id_banco_detalle');
                $id_cheque = null;
                $nombreCliente = '';


 


              try{  
                DB::beginTransaction();
     


            
              //OBTENER COTIZACION CONTABLE VENTA
              /*$cotizacion_contable_venta = (float)DB::select("SELECT get_cotizacion_contable_actual(".$idEmpresa.",'V') AS n")[0]->n;*/
              
              //SE INSERTA POR DEFECTO EL DE VENTA PORQUE EN LA FUNCION DE PROCESAR_OP ES LO QUE SE UTILIZA
              //CERO SIGNIFICA USAR LA COTIZACION DE VENTA
              $cotizacion = str_replace(',','.', str_replace('.','',$req->input('cotizacion')));



            
            $bancoConcepto = BancoDetalle::with('banco_cabecera')->where('id',$id_banco_detalle)->first();
            $banco_concepto = $bancoConcepto->banco_cabecera->nombre;
            $cuenta_concepto = $bancoConcepto->numero_cuenta;
            
              //-------------INSERT DE ANTICIPO------------------------
              $anticipo = new Anticipo;
              $anticipo->concepto = $concepto_anticipo;
              $anticipo->importe = $importe_total;
              $anticipo->cotizacion = $cotizacion;
              $anticipo->id_grupo = $req->input('id_grupo');
              $anticipo->id_proforma = $req->input('id_proforma');
              $anticipo->id_venta = $req->input('id_venta');
              $anticipo->id_moneda = $req->input('id_moneda');
              $anticipo->documento = $req->input('nro_cheque');
              $anticipo->id_banco_detalle =  $id_banco_detalle;
              $anticipo->id_op = $req->input('id_op');
              $anticipo->id_cuenta_contable = $req->input('id_cuenta_contable');
              $anticipo->id_usuario = $idUsuario;
              $anticipo->fecha_pago = ($req->input('fecha_pago')) ? $req->input('fecha_pago') : null;//CORRGEGIR 
              $anticipo->fecha_vencimiento = ($req->input('fecha_vencimiento')) ? $req->input('fecha_vencimiento') : null;
              $anticipo->id_centro_costo = $req->input('id_centro_costo');
              $anticipo->id_beneficiario = $req->input('id_beneficiario');
              $anticipo->id_sucursal = $req->input('id_sucursal');
              $anticipo->saldo = $importe_total;
              $anticipo->pendiente = true;
              $anticipo->id_tipo_documento = 20;//TIPO CTA CTE ANTICIPO
              $anticipo->id_empresa = $idEmpresa;
              $anticipo->tipo = "P";
              $anticipo->save();
              $idAnticipo = $anticipo->id;

              if($req->input('id_beneficiario') !=""){

                $persona = Persona::where('id',$req->input('id_beneficiario'))->first();
                $beneficiario_concepto =  $persona->nombre."". $persona->apellido;

              }else{  

                if($req->input('id_beneficiario_cheque') !=""){
                  $persona = Persona::where('id',$req->input('id_beneficiario_cheque'))->first();
                  $beneficiario_concepto =  $persona->nombre."". $persona->apellido;
                }else{
                  $beneficiario_concepto =  $req->input('beneficiario_txt');
                }  

              } 

              $concepto_final = "ANTICIPO NRO ".$idAnticipo."-FECHA ".date('d/m/Y', strtotime($req->input('fecha_pago')))."-IMPORTE ".$importe_total;
             
              if($concepto_forma_pago != ""){
                $concepto_final .= "-MODO DE PAGO ".$concepto_forma_pago;
              }
              if($banco_concepto != ""){
                $concepto_final .= "-BANCO ".$banco_concepto;
              }
              if($cuenta_concepto != ""){
                $concepto_final .= "-CTA ".$cuenta_concepto;
              }
              if($beneficiario_concepto != ""){
                $concepto_final .= "-BENEFICIARIO ".$beneficiario_concepto;
              }
              if($req->input('id_grupo') != ""){
                $concepto_final .= "-GRUPO ".$req->input('id_grupo');
              }
              if($req->input('id_proforma') != ""){
                $concepto_final .= "-PP ".$req->input('id_proforma');
              }

              if($req->input('id_venta') != ""){
                $concepto_final .= "-VTA ".$req->input('id_proforma');
              }

              if($concepto_anticipo != ""){
                $concepto_final .= "- ".$concepto_anticipo;
              }

              Anticipo::where('id', $idAnticipo)
                      ->update(['concepto' => $concepto_final]); 

              $nro_op_funct = DB::select("SELECT public.get_documento(".$idEmpresa.",'OP')");
              $nro_op = $nro_op_funct[0]->get_documento;

              //--------------------INSERT DE OP------------------------
              $opcabecera = new OpCabecera;
              $opcabecera->id_usuario_creacion  = $idUsuario;
              $opcabecera->id_estado  = 50;//AUTORIZADO
              $opcabecera->id_proveedor = $req->input('id_beneficiario');
              $opcabecera->total_pago = $importe_total;
              $opcabecera->total_neto_pago  = $importe_total;
              $opcabecera->id_moneda  = $req->input('id_moneda');
              $opcabecera->id_sucursal  = $req->input('id_sucursal');
              $opcabecera->cotizacion  = $cotizacion;  
              $opcabecera->id_centro_costo  = $req->input('id_centro_costo');    
              $opcabecera->concepto  =  $concepto_op;  
              $opcabecera->total_op_detalle  = $importe_total;
              $opcabecera->total_op = $importe_total;
              $opcabecera->id_cuenta_contable = $req->input('id_cuenta_contable');
              $opcabecera->id_empresa = $idEmpresa;
              $opcabecera->id_usuario_verificado = $idUsuario;
              $opcabecera->fecha_hora_verificado = date('Y-m-d H:i:00');
              $opcabecera->id_usuario_autorizado = $idUsuario;
              $opcabecera->fecha_hora_autorizado = date('Y-m-d H:i:00');
              $opcabecera->origen = 'AN';
              $opcabecera->adjunto = $req->input('nombreImagen');
              $opcabecera->fondo_fijo = false;
							$opcabecera->nro_op = $nro_op;
              $opcabecera->save();

              $resp->id_op = $opcabecera->id;
              $detalles_op = new OpDetalle;
              $detalles_op->id_anticipo =  $anticipo->id;
              $detalles_op->monto = $importe_total; 
              $detalles_op->id_cabecera = $opcabecera->id;
              $detalles_op->save();



              //----------------FORMA DE PAGO------------------------------
              $formaPagoCabecera = new FormaPagoOpCabecera;
              $formaPagoCabecera->id_usuario = $idUsuario;
              $formaPagoCabecera->importe_total = $importe_total; 
              $formaPagoCabecera->id_moneda = $req->input('id_moneda');
              $formaPagoCabecera->cotizacion = $cotizacion;
              $formaPagoCabecera->id_op = $opcabecera->id;
              $formaPagoCabecera->id_empresa = $idEmpresa;
              $formaPagoCabecera->save();

              
              $formaPagoDetalle = new FormaPagoOpDetalle;

              switch ($tipo_operacion) {
                
                case 1: //EFECTIVO

                  $cuenta_contable_pago = CtaCtbFormaPago::where('id_forma_pago',1)
                  ->where('id_moneda',$req->input('id_moneda'))
                  ->where('id_empresa',$idEmpresa)
                  ->where('activo',true)
                  ->first();

                  if(!$cuenta_contable_pago){
                    throw new \Exception('Falta configurar parametros de forma pago.');
                  }

                  $formaPagoDetalle->id_banco_detalle = $cuenta_contable_pago->id_cuenta_banco;
                  $formaPagoDetalle->id_cuenta_contable = $cuenta_contable_pago->id_cuenta_contable;
                 
                  break;

                case 4: //CHEQUE

                      $cheque = new ChequeFp;
                      $cheque->id_beneficiario = $req->input('id_beneficiario_cheque') && $req->input('id_beneficiario_cheque') != 'null' ? $req->input('id_beneficiario_cheque') : null;
                      $cheque->id_cuenta =  $id_banco_detalle;
                      $cheque->nro_cheque = $req->input('nro_comprobante');
                      $cheque->id_moneda = (Integer)$req->input('id_moneda');
                      $cheque->importe = $importe_total;
                      $cheque->cotizacion = $cotizacion;
                      $cheque->fecha_emision = ($req->input('fecha_emision')) ? $req->input('fecha_emision') : null;
                      $cheque->fecha_vencimiento = ($req->input('fecha_vencimiento')) ? $req->input('fecha_vencimiento') : null;
                      $cheque->id_usuario = $idUsuario;
                      $cheque->id_empresa = $idEmpresa;
                      $cheque->id_tipo_cheque = 3;//
                      $cheque->emisor_txt = $req->input('beneficiario_txt');
                      $cheque->al_portador = $req->input('al_portador');
                      $cheque->id_op = $opcabecera->id;
                      $cheque->save();
                      $id_cheque =  $cheque->id;

                      $formaPagoDetalle->id_cheque =  $cheque->id;
                      $formaPagoDetalle->id_banco_detalle = $id_banco_detalle;

                      $banco_detalle = BancoDetalle::findOrFail($id_banco_detalle);
                      $formaPagoDetalle->id_cuenta_contable = $banco_detalle->id_cuenta_contable;

                      //CHEQUE
                      $formaPagoDetalle->al_portador = $req->input('al_portador');
                      $formaPagoDetalle->beneficiario_txt = $req->input('beneficiario_txt');
                      $formaPagoDetalle->fecha_emision =  ($req->input('fecha_emision')) ? $req->input('fecha_emision') : null;
                      $formaPagoDetalle->fecha_vencimiento =  ($req->input('fecha_vencimiento')) ? $req->input('fecha_vencimiento') : null;
                      
                 
                  break;

                case 10: //CANJE

                  $cuenta_contable_pago = CtaCtbFormaPago::where('id_forma_pago',10)
                  ->where('id_moneda',$req->input('id_moneda'))
                  ->where('id_empresa',$idEmpresa)
                  ->where('activo',true)
                  ->first();
    
                  if(!$cuenta_contable_pago){
                    throw new \Exception('Falta configurar parametros de forma pago.');
                  }
    
                  $formaPagoDetalle->id_banco_detalle = $cuenta_contable_pago->id_cuenta_banco;
                  $formaPagoDetalle->id_cuenta_contable = $cuenta_contable_pago->id_cuenta_contable;
                 
                  break;

                case 12: //CANJE 2
                    $cuenta_contable_pago = CtaCtbFormaPago::where('id_forma_pago',12)
                    ->where('id_moneda',$req->input('id_moneda'))
                    ->where('id_empresa',$idEmpresa)
                    ->where('activo',true)
                    ->first();
      
                    if(!$cuenta_contable_pago){
                      throw new \Exception('Falta configurar parametros de forma pago.');
                    }
      
                    $fp_detalle->id_banco_detalle = $cuenta_contable_pago->id_cuenta_banco;
                    $fp_detalle->id_cuenta_contable = $cuenta_contable_pago->id_cuenta_contable;
                  
                  break;

                default:

                  $banco_detalle = BancoDetalle::findOrFail($id_banco_detalle);
                  $formaPagoDetalle->id_banco_detalle = $id_banco_detalle;
                  $formaPagoDetalle->id_cuenta_contable = $banco_detalle->id_cuenta_contable;
                    
                break;
              }



              $formaPagoDetalle->id_cabecera =  $formaPagoCabecera->id;
              $formaPagoDetalle->importe_pago =  $importe_total;
              $formaPagoDetalle->documento =  ($req->input('documento')) ? $req->input('documento') : null;
              $formaPagoDetalle->nro_comprobante =  ($req->input('nro_comprobante')) ? $req->input('nro_comprobante') : null;
              $formaPagoDetalle->id_tipo_pago =  $tipo_operacion; 


              $formaPagoDetalle->save();

              //CONCEPTO OP
              $forma_pago = FormaPagoCliente::where('id', $tipo_operacion)->first(['denominacion']);
              $proveedor_name = Persona::where('id',$req->input('id_beneficiario'))->first();

              if(!is_null($proveedor_name->getFullNameAttribute())){
                $nombreCliente = $proveedor_name->getFullNameAttribute();
              } else {
                $nombreCliente = $proveedor_name->denominacion_comercial;
              }
              $concepto_op = 'OP '.$nro_op.' - '.$forma_pago->denominacion.' '.$nombreCliente.' - ANTICIPO '. $anticipo->id;

              //FORMA PAGO QUE PERTENECE AL OP
              OpCabecera::where('id', $opcabecera->id)
                ->update(['id_forma_pago' =>  $formaPagoCabecera->id,
                         'concepto'=> $concepto_op]);

              //OP QUE PERTENECE AL ANTICIPO  
              Anticipo::where('id', $anticipo->id)
              ->update(['id_op' =>  $opcabecera->id]); 


                $this->procesar_op($opcabecera->id, $idUsuario);
                $resp->msj = 'Se generó anticipo Nº: '. $anticipo->id.' y OP Nº: '.$nro_op;
                DB::commit();

              
            } catch(ExceptionCustom $e){
               //Aqui cae las exepcione manuales
                Log::error($e);
                $resp->msj .= $e->getMessage();
                $resp->err = false;
                DB::rollBack();

            } catch(\Exception $e){
                Log::error($e);
                $resp->msj .= 'Ocurrio un error al intentar procesar el anticipo';
                $resp->err = false;
                DB::rollBack();
            }
          
            return response()->json($resp);


       }//


       /**
       * ===============================================================================================
       *                                RETENCIONES
       * ===============================================================================================
       *  */  


       public function getRetencion(Request $req)
       {
          $obj = new \StdClass; 
          $obj->id_proveedor = $req->input('id_proveedor');
          $obj->moneda = $req->input('id_moneda');
          $obj->data = $req->input('data');
          return response()->json($this->retencion($obj));
       }//



       /**
       * ===============================================================================================
       *                             CUENTA CORRIENTE PROVEEDOR
       * ===============================================================================================
       *  */  

        public function indexCttaCorrienteProveedor(Request $request)
        {

              $personas = DB::select("SELECT id, CONCAT(nombre||' ',apellido,' '||denominacion_comercial) AS data_n, CONCAT(documento_identidad||'-',dv) AS documento_n  FROM personas 
              WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
                            WHERE puede_facturar = true) 
              AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");


          $lineaCredito = [];		
          $currencys = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();

          
          return view('pages.mc.pagoProveedor.cuentaCorrienteProveedor', compact('personas', 'currencys', 'lineaCredito'));
        }	

        public function getCuentaCorrienteProveedor(Request $req)
        {

          $data = DB::table(DB::raw("(SELECT cuenta_corriente.id_persona, personas.nombre, 
            sum(cuenta_corriente.debe) as debe,sum(cuenta_corriente.haber) as haber, 
            (SELECT CASE 
                WHEN cuenta_corriente.tipo = 'C' THEN sum(cuenta_corriente.debe) -  sum(cuenta_corriente.haber)
                ELSE sum(cuenta_corriente.haber) - sum(cuenta_corriente.debe) 
              END) as saldo,
            cuenta_corriente.tipo, 
            cuenta_corriente.id_moneda, currency.currency_code
            FROM cuenta_corriente, currency, personas
            WHERE cuenta_corriente.id_moneda = currency.currency_id
            AND cuenta_corriente.id_persona = personas.id
            AND cuenta_corriente.activo = true
            AND personas.id_empresa = ".$this->getIdEmpresa()." 
            GROUP BY cuenta_corriente.id_persona, cuenta_corriente.tipo, cuenta_corriente.id_moneda,
            personas.nombre,currency.currency_code) as result"));


          $data = $data->where('tipo','P');
          

          if($req->input('persona')){
            $data = $data->where('id_persona',$req->input('persona'));
          }

          if($req->input('idMoneda')){
            $data = $data->where('id_moneda',$req->input('idMoneda'));
          }
        
          $data = $data->get();

          return response()->json(['data'=>$data]);
        }

        public function consultarDetalleCuentasProveedor(Request $request)
        {
          
          $query = "SELECT cuenta_corriente.id_persona, 
                            personas.nombre, 
                            cuenta_corriente.debe as debe,
                          cuenta_corriente.haber as haber, 
                          cuenta_corriente.saldo as saldo, 
                          cuenta_corriente.tipo, 
                          cuenta_corriente.id_moneda, 
                          currency.currency_code,
                          cuenta_corriente.fecha_hora, 
                          cuenta_corriente.documento, 
                          tipo_documento_ctacte.denominacion
                        FROM cuenta_corriente, 
                            currency, 
                            personas, 
                            tipo_documento_ctacte
                        WHERE cuenta_corriente.id_moneda = currency.currency_id
                        AND cuenta_corriente.id_persona = personas.id
                        AND cuenta_corriente.id_tipo_documento = tipo_documento_ctacte.id
                        AND personas.id_empresa = ".$this->getIdEmpresa()." 
                        AND cuenta_corriente.activo = true 
                        AND cuenta_corriente.tipo = 'P' ";

          $totales_query = "SELECT 
                      SUM(cc.debe) as total_debe, 
                      SUM(cc.haber) as total_haber, 
                      SUM(cc.debe) -  SUM(cc.haber)  AS saldo
                    FROM cuenta_corriente cc, personas p
                    WHERE  cc.activo = true 
                      AND cc.tipo = 'P' 
                      AND p.id = cc.id_persona
                      AND p.id_empresa = ".$this->getIdEmpresa();
                      

                      if($request->input('persona_id')){
                        $query .= " AND cuenta_corriente.id_persona = ".$request->input('persona_id')." ";
                        $totales_query .= " AND cc.id_persona = ".$request->input('persona_id')." ";
                      }

                      if($request->input('moneda_id')){
                        $query .=" AND cuenta_corriente.id_moneda = ".$request->input('moneda_id')." ";
                        $totales_query .= " AND cc.id_moneda = ".$request->input('moneda_id')." ";
                      }
     
                if($request->input('periodo')){
                // dd($request->input('periodo'));
                $fechaPeriodos = explode(' ', $request->input('periodo'));
                $desde     = $fechaPeriodos[0];
                              $hasta     = $fechaPeriodos[1];
                              $query 		   .= " AND cuenta_corriente.fecha_hora BETWEEN '".$desde." 00:00:00' AND '".$hasta." 23:59:59' ";
                              $totales_query .= " AND cc.fecha_hora BETWEEN '".$desde." 00:00:00' AND '".$hasta." 23:59:59' ";
                              }			

                              $totales_query .= " GROUP BY tipo";
                              $totales = DB::select($totales_query);

                              foreach ($totales as $key => $total) 
                              {
                              $totales[$key]->total_debe = number_format($total->total_debe, 2, ",", ".");
                              $totales[$key]->total_haber = number_format($total->total_haber, 2, ",", ".");
                              $totales[$key]->saldo = number_format($total->saldo, 2, ",", ".");
                              }

                              if(empty($totales)){
                              $totales = [];
                              } else {
                              $totales = $totales[0];
                              }


              $query .= ' ORDER BY cuenta_corriente.id ASC';
              // dd($query);
              $resultado = DB::select($query);
              // dd($resultado);

              return response()->json(['data'=>$resultado, 'totales'=>$totales]);

        }	




      /**
       * ===============================================================================================
       *                                METODOS AUXILIARES
       * ===============================================================================================
       *  */  


      private function getIdUsuario()
      {
  
        return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
      }
    
      private function getIdEmpresa()
      {
    
       return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
      }

    /**
     * Funcion que retorna un array de botones habilitados para el usuario y la vista
     */
    private function btnPermisos($parametros, $vista)
    {
      // dd($vista);
      $btn = DB::select("SELECT pe.* 
                            FROM persona_permiso_especiales p, permisos_especiales pe
                            WHERE p.id_permiso = pe.id 
                            AND p.id_persona = ".$this->getIdUsuario() ." 
                            AND pe.url = '".$vista."' ");

      $htmlBtn = '';
      $boton =  array();
      $idParametro = '';

      // dd( $btn );

      if(!empty($btn)){

      foreach ($btn as $key => $value) {

        $idParametro = '';
        $ruta = 'factour.'.$value->accion;
        $htmlBtn = '';



        //LLEVA PARAMETRO
        if($value->lleva_parametro == '1'){

        foreach ($parametros as $indice=>$valor) { 

          if($indice == $value->nombre_parametro){
            $idParametro = $valor;
          }
        }

        //PARAMETRO OCULTO EN DATA
        if($value->parametro_oculto == '1'){
          $htmlBtn = "<a role='button' class='".$value->clase."' data-btn='".$idParametro."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
        } else {
          $htmlBtn = "<a role='button' href='".route($ruta,['id'=>$idParametro])."' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
        }

        } else {
          $htmlBtn = "<a role='button' href='#' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i>".$value->texto_alternativo." </a>"; 
        }

        
        $boton[] = $htmlBtn;
      }
      return $boton;
    
      } else {
        return array();
      }



    }//function


    /**
     * Obtiene el mayor numero de cheque hasta el momento
     */
    public function numMaxCheque()
    {

      $num_cheque = DB::select('SELECT MAX (nro_cheque) AS nro_cheque FROM fp_cheques');
      $num_cheque = (integer)$num_cheque[0]->nro_cheque +1 ;

      return response()->json(['num'=>$num_cheque]);
    }

      /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date)
    {
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }//function


    	/**
	 * Funcion que recibe datos array del datatable 
	 * ! Esta funcion ya no es necesaria y debe ser cambiada usando el serializeJSON ****
	 */
	private function  responseFormatDatatable($req)
  {
    $datos = array();
    $data =  new \StdClass;
    
    foreach ($req->formSearch as $key => $value) 
    {
       $n = $value['name'];
        $datos[$value['name']] = $value['value'];
        $data-> $n = $value['value'];
    }

    return $data;
} 


  public function beneficiarioCheque(Request $request){
    $beneficiario = Persona::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->get(['id','nombre','apellido','documento_identidad']); 

    $beneficiarios =  json_decode($beneficiario, true);

    if(isset($_GET['q'])){
        $search = $_GET['q'];
        foreach($beneficiarios as $key=>$destino){
          $resultado = strpos(strtolower($destino['nombre'].''.$destino['apellido'].''.$destino['documento_identidad']), strtolower($search));
          if($resultado === FALSE){
            unset($beneficiarios[$key]);
          }
        } 
    }

    $json = [];
    foreach($beneficiarios as $key=>$row){
        $json[$row['id']]['text'] = $destino['documento_identidad']." - ".$row['nombre']." ".$row['apellido'];
    }
    echo json_encode($json);
  }


  public function anularOpDetalle(Request $request){
        try{
              $OpDetalle = OpDetalle::where('id', $request->input('id_op'))->first();   
           /*   echo '<pre>';
          print_r($OpDetalle);*/
              $idLibroCompra= $OpDetalle->id_libro_compra;
              $Op = OpCabecera::where('id', $OpDetalle->id_cabecera)->first();   
              $totalPagar = $Op->total_pago ;
              $totalAnticipo = $Op->total_anticipo;
              $totalNotaCredito = $Op->total_nota_credito;
              DB::table('libros_compras')->where('id', $idLibroCompra)
                                     ->update([
                                                'pendiente'=> false,
                                                'pagado_proveedor'=> false,
                                              ]);

              DB::table('op_detalle')->where('id', $request->input('id_op'))
                                     ->update([
                                                'activo'=> false,
                                                'id_libro_compra'=> null
                                              ]);

              $totalDetalle = DB::table('op_detalle')->where('id_cabecera' , $OpDetalle->id_cabecera)->where('activo' , true )->sum('monto');                 

              $nuevoNeto =  floatval($totalDetalle) - floatval($totalAnticipo) - floatval($totalNotaCredito);
              DB::table('op_cabecera')->where('id', $OpDetalle->id_cabecera)
                                     ->update([
                                                'total_neto_pago'=> $nuevoNeto,
                                                'total_pago'=> $totalDetalle
                                              ]);
              $objResultado = new \StdClass; 
              $objResultado->codigo = 'OK'; 
              $objResultado->mensaje = 'Se ha eliminado el detalle de OP'; 

             return response()->json($objResultado);
        } catch(\Exception $e){
              Log::error($e);
              $response->err = false;
              $objResultado = new \StdClass; 
              $objResultado->codigo = 'ERROR'; 
              $objResultado->mensaje = 'No se a eliminado el detalle OP';  
              return response()->json($objResultado);
        }                           
  }
  
  public function reservas_pagar(Request $request, $id){
    $getNotificacion= DB::select("SELECT public.insert_notificacion(null,null, null, null,14,null,".$id.")");
    return redirect()->route('notificar');
  }


  public function procesoPagoNemo(Request $request){
        ini_set('memory_limit', '-1');
        set_time_limit(300);
        $login = 'dtpmundo.admin';
        $password = 'martes5';       
        $client = new Client(['auth' => [$login, $password]]);
        $data = DB::table('op_cabecera as opc');
        $data = $data->where('opc.activo',true);
        $data = $data->where('opc.id_empresa',1);
        $data = $data->where('opc.id_estado',53);
        $data = $data->get(['id']);

        foreach($data as $key=>$op){
              $libroCompra =  DB::table('vw_item_detalle_op');
              $libroCompra = $libroCompra->where('id',$op->id);
              $libroCompra = $libroCompra->where('saldo',0);
              $libroCompra = $libroCompra->where('activo',true);
              $libroCompra = $libroCompra->get(); 
              foreach($libroCompra as $keys=>$lc){
                    if($lc->id_reserva_nemo != null){
                      if($lc->notificado_nemo != true){
                       /* echo '<pre>';
                        print_r($lc);*/
                    // if($lc->id_reserva_nemo == 6114){
 
                        $reservaNemo = DB::select("SELECT get_datos_pago_nemo(".$lc->id_reserva_nemo.")");
                        $datosPago = explode(' ', $reservaNemo[0]->get_datos_pago_nemo);
                       /* echo '<pre>';
                        print_r($datosPago);*/
                        $xml_body = '<BookingPaymentStatusRQ>
                                      <Details>
                                          <Bookings ItemsCount="1">
                                              <Booking Sequence="1">
                                                  <BookingReference ReferenceType="NMO.GBL.BRT.NAV">'.$datosPago[0].'</BookingReference>
                                                  <BookingItems>                    
                                                      <Hotels ItemsCount="1">
                                                          <Hotel Sequence="1">
                                                              <BookingReference ReferenceType="NMO.FLT.BRT.NAV">'.$datosPago[1].'</BookingReference>                            
                                                          </Hotel>
                                                      </Hotels>                                
                                                  </BookingItems>
                                              </Booking>
                                          </Bookings>
                                      </Details>
                                  </BookingPaymentStatusRQ>';

                        $request_uri = 'http://backend.psurfer.net/export.php/api/BookingPaymentStatusRQ';
                        
                        $response = $client->request('POST', $request_uri, [
                                                      'headers' => [
                                                        'Accept' => 'application/xml'
                                                      ],
                                                      'body'   => $xml_body
                                                      ])->getBody()->getContents();
                        $xmlobj = @new \SimpleXMLElement($response);  
                        $datos = json_decode(json_encode($xmlobj));
                        
                        if(trim($datos->Notifications->Notification->NotificationType) == 'Success'){
                          DB::table('libros_compras')  
                                            ->where('id',$lc->id_libro)
                                            ->update([
                                                'notificado_nemo'=>true,
                                                'fecha_notificado_nemo'=>date('Y-m-d H:s:m'),
                                                'mensaje_nemo'=>$datos->Notifications->Notification->NotificationMessage,
                                                'estado_respuesta_nemo'=>$datos->Notifications->Notification->NotificationType,
                                                'request_nemo'=>$xml_body,
                                                'response_nemo'=>$response
                                                ]);     
                        }else{ 
                          DB::table('libros_compras')  
                                            ->where('id',$lc->id_libro)
                                            ->update([
                                                'notificado_nemo'=>false,
                                                'fecha_notificado_nemo'=>date('Y-m-d H:s:m'),
                                                'mensaje_nemo'=>$datos->Notifications->Notification->NotificationMessage,
                                                'estado_respuesta_nemo'=>$datos->Notifications->Notification->NotificationType,
                                                'request_nemo'=>$xml_body,
                                                'response_nemo'=>$response
                                                ]);     

                          
                        }              
                        DB::table('reservas_nemo')  
                                            ->where('id',$lc->id_reserva_nemo)
                                            ->update([
                                                  'pagado'=>true,
                                                  'notificado_nemo'=>true,
                                                  'fecha_notificado_nemo'=>date('Y-m-d H:s:m'),
                                                  'mensaje_nemo'=>$datos->Notifications->Notification->NotificationMessage,
                                                  'estado_respuesta_nemo'=>$datos->Notifications->Notification->NotificationType,
                                                  'request_nemo'=>$xml_body,
                                                  'response_nemo'=>$response
                                                  ]);        
    
                     //////////////////////////////////////////////////////////////////////////////////////////////////////
                    //  }
                   }
                } 
            }  
       }
   }

   public function aplicacionAnticipoProveedor(Request $req)
   {
    $proveedores= Persona::whereIn('id_tipo_persona',array(14,8))->where('id_empresa',$this->getIdEmpresa())->get();  
     return view('pages.mc.pagoProveedor.aplicacionAnticipos',compact('proveedores'));
   }

   public function getAplicacionAnticiposProveedor(Request $req){
        $resultado =[];
				$query = "SELECT id,id_beneficiario,fecha, beneficiario_nombre, beneficiario_apellido, op_anticipo,importe,saldo, moneda";
				
        $query .=" FROM vw_anticipo_proveedores ";

				$query .="WHERE id_empresa = ".$this->getIdEmpresa();

        if($req->input('id_proveedor') != 0){
          $query .="AND id_beneficiario =".$req->input('id_proveedor');
        }

        if($req->input('fecha_emision') != ""){
          $fechaPeriodo= explode('-', $req->input('fecha_emision'));
          $check_in = explode('/', $fechaPeriodo[0]);
          $check_out = explode('/', $fechaPeriodo[1]);
          $checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]).' 00:00:00';
          $checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]).' 23:59:59';
          $query .="AND fecha BETWEEN '".$checkIn."' AND '".$checkOut."'";
        }
				$query .=" GROUP BY id, beneficiario_nombre, beneficiario_apellido, op_anticipo, importe,saldo, moneda,id_beneficiario,fecha";

				$anticipos = DB::select($query);

        if(!empty($anticipos)){
          $contador = 0;
          foreach($anticipos as $key=>$resumen){
            $detalleResumenes = DB::select("SELECT *
                            FROM vw_anticipo_proveedores
                            WHERE monto is not null
                            AND id = ".$resumen->id );	
            if(!empty($detalleResumenes)){
              $resultado[$contador] = $anticipos[$key];
              $resultado[$contador]->detalles = $detalleResumenes;
              $contador = $contador + 1;
            }
          }
        }	
        foreach($resultado as $key=>$st){
          $listado['data'][] = $st;	
        }
        if(empty($listado)){
            $listado['data'][] = [];
        }
        return response()->json($listado);
  }


  
  public function aplicacionAnticiposProveedorPdf(Request $req)
  {
    $resultado =[];
    $query = "SELECT id,id_beneficiario,fecha, beneficiario_nombre, beneficiario_apellido, op_anticipo,importe,saldo, moneda";
    
    $query .=" FROM vw_anticipo_proveedores ";

    $query .="WHERE id_empresa = ".$this->getIdEmpresa();

    if($req->input('id_proveedor') != 0){
      $query .="AND id_beneficiario =".$req->input('id_proveedor');
    }

    if($req->input('fecha_emision') != ""){
      $fechaPeriodo= explode('-', $req->input('fecha_emision'));
      $check_in = explode('/', $fechaPeriodo[0]);
      $check_out = explode('/', $fechaPeriodo[1]);
      $checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]).' 00:00:00';
      $checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]).' 23:59:59';
      $query .="AND fecha BETWEEN '".$checkIn."' AND '".$checkOut."'";
    }
    $query .=" GROUP BY id, beneficiario_nombre, beneficiario_apellido, op_anticipo, importe,saldo, moneda,id_beneficiario,fecha";

    $anticipos = DB::select($query);

    if(!empty($anticipos)){
      $contador = 0;
      foreach($anticipos as $key=>$resumen){
        $detalleResumenes = DB::select("SELECT *
                        FROM vw_anticipo_proveedores
                        WHERE monto is not null
                        AND id = ".$resumen->id );	
        if(!empty($detalleResumenes)){
          $resultado[$contador] = $anticipos[$key];
          $resultado[$contador]->detalles = $detalleResumenes;
          $contador = $contador + 1;
        }
      }
    }	
    foreach($resultado as $key=>$st){
      $listado['data'][] = $st;	
    }
    if(empty($listado)){
        $listado['data'][] = $listado;
    }
    //return view('pages.mc.cuentaCorriente.cuentaProveedorPdf',compact('listado','proveedor','moneda','fecha_emision','fecha_vencimiento'));
    $pdf = \PDF::loadView('pages.mc.pagoProveedor.aplicacionAnticiposProveedorPdf',compact('listado'));

    $pdf->setPaper('a4', 'landscape')->setWarnings(false);
    return $pdf->download('Cuentas por Cobrar'.$this->getId4Log().'.pdf');	
  }

	public function guardarCotizacionEspecial(Request $request){
		$mensaje =  new \StdClass;
	   // try{
			DB::table('op_cabecera')
					        ->where('id',$request->input('id_op'))
					        ->update([
					        		  'indice_cotizacion'=> $request->input('indice_cotizacion'),
					        		  'cotizacion_especial'=> $request->input('cotizacion'),
					            	'id_usuario_cotizacion_especial'=>$this->getIdUsuario(),
									      'fecha_cotizacion_especial'=>date('Y-m-d h:m:s')
					            	]);
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se han editados los datos correctamente';	        
	/*	} catch(\Exception $e){
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se han editado los datos';		        
		}*/

		return response()->json($mensaje);
	}


  public function guardarNumeroComprobantes(Request $request){
      //Guardar el campo numero de comprobante en la tabla de op cabecera

      $mensaje =  new \StdClass;
      try{
        DB::table('op_cabecera')
                  ->where('id',$request->input('id_op'))
                  ->update([
                            'numeros_comprobantes'=> $request->input('numeros_comprobantes')
                          ]);
        $mensaje->status = 'OK';
        $mensaje->mensaje = 'Se ha guardado el numero de comprobante correctamente';	        
      } catch(\Exception $e){
        $mensaje->status = 'ERROR';
        $mensaje->mensaje = 'Se ha producido un error al guardar el numero de comprobante';		        
      }

      return response()->json($mensaje);
  }


}//class