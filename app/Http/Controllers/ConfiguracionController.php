<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\PagoOpformValidate; //CLASE DE VALIDACION

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;

use App\Proforma;
use App\ProformasDetalle;
use App\Divisas;
use App\Persona;
use App\PlazoPago;
use App\HistoricoComentariosProforma;
use App\Voucher;
use App\TipoFactura;
use App\EstadoFactour;
use App\TipoPersona;
use App\Factura;
use App\FacturaDetalle;
use App\AdjuntoDocumento;
use App\Empresa;
use App\Producto;
use App\FormaPagoCliente;
use App\Currency;
use App\OpCabecera;
use App\OpDetalle;
use App\GastoOp;
use App\FormaPagoOpCabecera;
use App\FormaPagoOpDetalle;
use App\BancoCabecera;
use App\BancoDetalle;
use App\Anticipo;
use App\PlanCuenta;
use App\ChequeFp;
use App\CentroCosto;
use App\LibroCompra;
use App\SucursalEmpresa;
use App\Parametros;
use App\Retencion;
use App\PlanSistema;
use App\CuentaCorrienteEmpresa;
use App\Negocio;


use Session;
use Redirect;
// use DB;


class ConfiguracionController extends Controller
{

    public function abmParametros()
    {
        $param = Parametros::where('editable','true')->get();
        return view('pages.mc.configuraciones.abmParametros', compact('param'));
    }

    public function addParametros()
    {
        return view('pages.mc.configuraciones.addParametros');
    }

    public function consultaParametros(){
        $data = Parametros::where('editable','true')->get();
        return response()->json(array($data));
    }

    public function agregarNuevoParametro(Request $req)
    {
        $clave=$req->clave;
        $valor=$req->valor;
        $descripcion=$req->descripcion;
        $editable=$req->editable;
        //Cargar en la base de datos.
        $nuevoParametro = new Parametros;
		$nuevoParametro->clave = $clave;
		$nuevoParametro->valor = $valor;
        $nuevoParametro->descripcion = $descripcion;
        $nuevoParametro->editable = $editable;
		$nuevoParametro->save();

        return response()->json(array('estado'=>1));

    }

    public function setDataParametro(Request $req)
    {	
        $resp = new \StdClass;
        $resp->err = true;
        $param = $req->all();
        foreach ($param as $key => $value) {
            DB::table('parametros')
            ->where('id', $key)
            ->update(['valor' => $value]);
        }
        return response()->json(array('estado'=>1));
    }

    public function configuracionEmpresa(Request $req)
    {	
        $empresas = Empresa::where('activo', true)->get();
        $planes = PlanSistema::all();
        $aerolineas = Persona::where('id_tipo_persona', '=', '14')
                            ->get();
        return view('pages.mc.configuraciones.configuracionEmpresa',compact('empresas','planes','aerolineas'));
    }



    public function getConfiguracionEmpresa(Request $req)
    {	
        $empresas = Empresa::where('id', $req->input('idEmpresa'))->where('activo', true)->first();
        return response()->json($empresas);
    }

    public function guardarConfiguracion(Request $req)
    {	
        $resp = new \StdClass;
        $tipo_impresion = $req->input('tipo_impresion');
        if($req->input('tipo_impresion') == ""){
            $tipo_impresion = 0;
        }
        $es_exportador = $req->input('es_exportador');
        if($req->input('es_exportador') == ""){
            $es_exportador = 'false';
        }else{
            if($req->input('es_exportador') == 1){
                $es_exportador = 'true';
            }else{
                $es_exportador = 'false';
            }
        }

        $imprimir_detalle_factura = $req->input('imprimir_detalle_factura');
        if($req->input('imprimir_detalle_factura') == ""){
            $imprimir_detalle_factura = 'false';
        }else{
            if($req->input('imprimir_detalle_factura') == 1){
                $imprimir_detalle_factura = 'true';
            }else{
                $imprimir_detalle_factura = 'false';
            }
        }
        $agente_retentor = $req->input('agente_retentor');
        if($req->input('agente_retentor') == ""){
            $agente_retentor = 'false';
        }else{
            if($req->input('agente_retentor') == 1){
                $agente_retentor = 'true';
            }else{
                $agente_retentor = 'false';
            }
        }
        $proveedor_ticket = $req->input('proveedor_ticket');
        if($req->input('proveedor_ticket') == ""){
            $proveedor_ticket = 0;
        }

        $datosEmpresa = [
                        "tipo_impresion"=>  $tipo_impresion,
                        "tipo_calculo_vencimiento"=> $req->input('tipo_calculo_vencimiento'),
                        "tipo_empresa"=> $req->input('tipo_empresa'),
                        "es_exportador"=> $es_exportador,
                        "id_plan_sistema"=> $req->input('id_plan_sistema'),
                        "agente_retentor"=> $agente_retentor,
                        "control_de_rentabilidad"=> $req->input('control_de_rentabilidad'),
                        "imprimir_detalle_factura"=> $imprimir_detalle_factura,
                        "representante"=> $req->input('representante'),
                        "ruc_representante"=> $req->input('ruc_representante'),
                        "dias_vencimiento"=> $req->input('dias_vencimiento'),
                        "proveedor_ticket"=>$proveedor_ticket,
                        "markup_minimo_venta"=> $req->input('markup_minimo_venta'),
                        "venta_minima_incentivo"=> $req->input('venta_minima_incentivo'),
                        "pie_factura_txt"=> $req->input('pie_factura_txt'),
                        "head_factura_txt" => $req->input('cabecera_factura_txt'), 
                        ];

				//ACTUALIZAR CONFIGURACION
         try{
		    $update = DB::table('empresas')
                            ->where('id',$req->input('id_empresa'))
                            ->update($datosEmpresa);
            $resp->err = 'OK';
        } catch(\Exception $e){
            $resp->err = 'ERROR';
        } 
        return response()->json($resp);

    }


    public function permisoEmpresa(Request $req)
    {	
        $empresas = Empresa::where('activo', true)->get();



        return view('pages.mc.configuraciones.permisoEmpresa',compact('empresas'));
    }


    public function cuentaContableEmpresa(Request $req)
    {	
        $empresas = Empresa::where('activo', true)->get();
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->get();
		$divisa = Currency::where('activo', 'S')->orderBy('currency_id', 'ASC')->get();
        $parametros = [
                        "ANTICIPOS"=>"ANTICIPOS",
                        "DIF_COB_NEG"=>"DIF_COB_NEG",
                        "DIF_CAMBIO_POS"=>"DIF_CAMBIO_POS",
                        "PROV_LOCALES"=>"PROV_LOCALES",
                        "IVA_DEBITO"=>"IVA_DEBITO",
                        "VENTA_EXENTA"=>"VENTA_EXENTA",
                        "ANTICIPOS_PROV_LOCALES"=>"ANTICIPOS_PROV_LOCALES",
                        "RETENCIONES_A_COBRAR"=>"RETENCIONES_A_COBRAR",
                        "DEUDORES_VENTAS"=>"DEUDORES_VENTAS",
                        "IVA_CREDITO_5"=>"IVA_CREDITO_5",
                        "CANJE"=>"CANJE",
                        "CANJE"=>"CANJE",
                        "CLIENTES"=>"CLIENTES",
                        "DIF_CAMBIO_NEG"=>"DIF_CAMBIO_NEG",
                        "DIF_COB_POS"=>"DIF_COB_POS",
                        "VENTA_NC"=>"VENTA_NC",
                        "ANTICIPOS_PROV_EXT_USD"=>"ANTICIPOS_PROV_EXT_USD",
                        "IVA_CREDITO"=>"IVA_CREDITO",
                        "CUENTA_NC_DEVOLUCION"=>"CUENTA_NC_DEVOLUCION",
                        "RETENCION"=>"RETENCION",
                        "VENTA_GRAVADA"=>"VENTA_GRAVADA",
                        "ANTICIPOS_CLIENTES"=>"ANTICIPOS_CLIENTES",
                        "PROVEEDORES_EXT"=>"PROVEEDORES_EXT",
                        "DEV_Y_DES"=>"DEV_Y_DES",
                        "CRE_A_FAVOR_CLI"=>"CRE_A_FAVOR_CLI",
                        "CRE_A_FAVOR_AGE"=>"CRE_A_FAVOR_AGE",
                        "COSTO_TICKET_BSP"=>"COSTO_TICKET_BSP",
                        "ANTICIPO_PROVEEDOR"=>"ANTICIPO_PROVEEDOR",
                        "NOTA_CREDITO"=>"NOTA_CREDITO",
                        "ANTICIPO_COBRANZA"=>"ANTICIPO_COBRANZA",
                        "ANTICIPO_PROVEEDOR_LOCAL"=>"ANTICIPO_PROVEEDOR_LOCAL",
                        "EXENTA_LC"=>"EXENTA_LC",
                        "IVA_10_LC"=>"IVA_10_LC",
                        "IVA_CREDITO_10"=>"IVA_CREDITO_10",
                        "IVA_DEBITO_10"=>"IVA_DEBITO_10",
                        "VENTA"=>"VENTA",
                        "BANCO"=>"BANCO",
                        "PROVEEDOR"=>"PROVEEDOR",
                        "FACT_TRANS_LOCAL" => "FACT_TRANS_LOCAL",
                        "FACT_TRANS_EXT" => "FACT_TRANS_EXT",
                        "EFECTIVO_A_DEPOSITAR" => "EFECTIVO_A_DEPOSITAR",
                        "CHEQUE_A_DEPOSITAR" => "CHEQUE_A_DEPOSITAR",
                        "CHEQUE_DIFERIDO_A_DEPOSITAR" => "CHEQUE_DIFERIDO_A_DEPOSITAR",
                    ];
        return view('pages.mc.configuraciones.cuentaContableEmpresa',compact('empresas','cuentas_contables','divisa','parametros'));
    }

    public function gesturCuentaContable(Request $req)
    {	
        $id_empresa = $req->formSearch[0]['value'];
        $parametro = $req->formSearch[1]['value']; 
        $cuenta_contable = $req->formSearch[2]['value']; 
        $valor = $req->formSearch[3]['value']; 
        $divisa_id = $req->formSearch[4]['value'];  

		$cuentaContable = DB::table('vw_cuenta_contable_empresa');
		if($id_empresa != ""){
			$cuentaContable = $cuentaContable->where('id_empresa',$id_empresa);
		}
	/*	if($parametro != ""){
			$cuentaContable = $cuentaContable->where('parametro',$parametro);
		}
		if($cuenta_contable != ""){
			$cuentaContable = $cuentaContable->where('id_cuenta_contable',$cuenta_contable);
		}
		if($valor != ""){
			$cuentaContable = $cuentaContable->where('valor',$valor);
		}
		if($divisa_id != ""){
			$cuentaContable = $cuentaContable->where('id_moneda',$divisa_id);
		}*/
        $cuentaContable = $cuentaContable->where('activo',true);
		$cuentaContable = $cuentaContable->get();
        foreach($cuentaContable as $key=>$cuenta){
            echo '<pre>';
            print_r($cuenta);



        }
        die;

        return response()->json(['data'=>$cuentaContable]);
    }

    public function guardarCuentaEmpresa(Request $req)
    {	
 
        $resp = new \StdClass;
       try{
            $moneda = null;
	    if($req->input('divisa_id')){
                $moneda = $req->input('divisa_id');
            }

            //Si ya existe actualizamos sino creamos
            $cuenta = CuentaCorrienteEmpresa::where('id_empresa', $req->input('idEmpresa'))
                      ->where('parametro', $req->input('parametro'))
                      ->where('id_cuenta_contable',$req->input('cuenta_contable'))
                      ->where('id_moneda', $moneda)
                      ->first();

            if(!$cuenta){
                $cuenta = new CuentaCorrienteEmpresa;	
            }

            
            $cuenta->id_empresa = $req->input('idEmpresa');
            $cuenta->parametro = $req->input('parametro');
            $cuenta->valor = $req->input('valor');
            $cuenta->activo = true;
            $cuenta->id_cuenta_contable = $req->input('cuenta_contable');
            $cuenta->id_moneda = $moneda;
            $cuenta->save();

            $resp->err = 'OK';
        } catch(\Exception $e){
            $resp->err = 'ERROR';
        } 
        return response()->json($resp);

    } 

    public function obtenerPlanCuentas(Request $req)
    {
		$cuentas_contables = PlanCuenta::where('id_empresa',$req->input('id_empresa'))->where('activo',true)->where('asentable', true)->get();
        return response()->json($cuentas_contables);

    }


    public function anularCuentaEmpresa(Request $req)
    {
        $resp = new \StdClass;
        $datosEmpresa = [
            "activo"=>  false
            ];
        try{
		    $update = DB::table('cuentas_contables_empresa')
                            ->where('id',$req->input('cuenta_contable'))
                            ->update($datosEmpresa);
            $resp->err = 'OK';
        } catch(\Exception $e){
            $resp->err = 'ERROR';
        } 
        return response()->json($resp);
    }

    public function cuentaEmpresa(Request $req)
    {
        if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 1){
            $empresas = Empresa::where('activo', true)->get();
            $cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->get();
        }else{
            $empresas = Empresa::where('activo', true)
                                ->where('id',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
                                ->get();
           $cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();

        }
        $divisa = Currency::where('activo', 'S')->orderBy('currency_id', 'ASC')->get();
        $parametros = [
            "ANTICIPOS"=>"ANTICIPOS",
            "DIF_COB_NEG"=>"DIF_COB_NEG",
            "DIF_CAMBIO_POS"=>"DIF_CAMBIO_POS",
            "PROV_LOCALES"=>"PROV_LOCALES",
            "IVA_DEBITO"=>"IVA_DEBITO",
            "VENTA_EXENTA"=>"VENTA_EXENTA",
            "ANTICIPOS_PROV_LOCALES"=>"ANTICIPOS_PROV_LOCALES",
            "RETENCIONES_A_COBRAR"=>"RETENCIONES_A_COBRAR",
            "DEUDORES_VENTAS"=>"DEUDORES_VENTAS",
            "IVA_CREDITO_5"=>"IVA_CREDITO_5",
            "CANJE"=>"CANJE",
            "CLIENTES"=>"CLIENTES",
            "DIF_CAMBIO_NEG"=>"DIF_CAMBIO_NEG",
            "DIF_COB_POS"=>"DIF_COB_POS",
            "VENTA_NC"=>"VENTA_NC",
            "ANTICIPOS_PROV_EXT_USD"=>"ANTICIPOS_PROV_EXT_USD",
            "IVA_CREDITO"=>"IVA_CREDITO",
            "CUENTA_NC_DEVOLUCION"=>"CUENTA_NC_DEVOLUCION",
            "RETENCION"=>"RETENCION",
            "VENTA_GRAVADA"=>"VENTA_GRAVADA",
            "ANTICIPOS_CLIENTES"=>"ANTICIPOS_CLIENTES",
            "PROVEEDORES_EXT"=>"PROVEEDORES_EXT",
            "DEV_Y_DES"=>"DEV_Y_DES",
            "CRE_A_FAVOR_CLI"=>"CRE_A_FAVOR_CLI",
            "CRE_A_FAVOR_AGE"=>"CRE_A_FAVOR_AGE",
            "COSTO_TICKET_BSP"=>"COSTO_TICKET_BSP",
            "ANTICIPO_PROVEEDOR"=>"ANTICIPO_PROVEEDOR",
            "NOTA_CREDITO"=>"NOTA_CREDITO",
            "ANTICIPO_COBRANZA"=>"ANTICIPO_COBRANZA",
            "ANTICIPO_PROVEEDOR_LOCAL"=>"ANTICIPO_PROVEEDOR_LOCAL",
            "EXENTA_LC"=>"EXENTA_LC",
            "IVA_10_LC"=>"IVA_10_LC",
            "IVA_CREDITO_10"=>"IVA_CREDITO_10",
            "IVA_DEBITO_10"=>"IVA_DEBITO_10",
            "VENTA"=>"VENTA",
            "BANCO"=>"BANCO",
            "PROVEEDOR"=>"PROVEEDOR",
            "FACT_TRANS_LOCAL" => "FACT_TRANS_LOCAL",
            "FACT_TRANS_EXT" => "FACT_TRANS_EXT",
            "EFECTIVO_A_DEPOSITAR" => "EFECTIVO_A_DEPOSITAR",
            "CHEQUE_A_DEPOSITAR" => "CHEQUE_A_DEPOSITAR",
            "CHEQUE_DIFERIDO_A_DEPOSITAR" => "CHEQUE_DIFERIDO_A_DEPOSITAR",
        ];

        return view('pages.mc.configuraciones.cuentaEmpresa',compact('empresas', 'cuentas_contables','parametros','divisa'));

    }

    public function obtenerCuentas(Request $req)
    {
        $id_empresa = $req->formSearch[0]['value'];
		$cuentaContable = DB::table('vw_cuenta_contable_empresa');
		$cuentaContable = $cuentaContable->where('id_empresa',$id_empresa);
        $cuentaContable = $cuentaContable->where('activo',true);
        $cuentaContable = $cuentaContable->orderBy('parametro','ASC');
		$cuentaContable = $cuentaContable->get();
        $empresa = Empresa::where('activo', true)->where('id', $id_empresa)->first(['denominacion']);
        $parametros = [
                        "ANTICIPOS|143",
                        "ANTICIPOS|111",
                        "ANTICIPOS|21",
                        "DIF_COB_NEG"=>"DIF_COB_NEG",
                        "DIF_CAMBIO_POS"=>"DIF_CAMBIO_POS",
                        "PROV_LOCALES|111",
                        "PROV_LOCALES|43",
                        "PROV_LOCALES|143",
                        "PROV_LOCALES|21",
                        "IVA_DEBITO",
                        "VENTA_EXENTA"=>"VENTA_EXENTA",
                        "ANTICIPOS_PROV_LOCALES|111",
                        "ANTICIPOS_PROV_LOCALES|143",
                        "ANTICIPOS_PROV_LOCALES|21",
                        "RETENCIONES_A_COBRAR|111",
                        "RETENCIONES_A_COBRAR|143",
                        "RETENCIONES_A_COBRAR|21",
                        "DEUDORES_VENTAS|143",
                        "DEUDORES_VENTAS|111",
                        "DEUDORES_VENTAS|21",
                        "DEUDORES_VENTAS|43",
                        "IVA_CREDITO_5"=>"IVA_CREDITO_5",
                        "CANJE|143",
                        "CANJE|111",
                        "CANJE|21",
                        "CLIENTES"=>"CLIENTES",
                        "DIF_CAMBIO_NEG"=>"DIF_CAMBIO_NEG",
                        "DIF_COB_POS"=>"DIF_COB_POS",
                        "VENTA_NC",
                        "ANTICIPOS_PROV_EXT_USD|111",
                        "ANTICIPOS_PROV_EXT_USD|143",
                        "ANTICIPOS_PROV_EXT_USD|21",
                        "IVA_CREDITO"=>"IVA_CREDITO",
                        "CUENTA_NC_DEVOLUCION"=>"CUENTA_NC_DEVOLUCION",
                        "RETENCION"=>"RETENCION",
                        "VENTA_GRAVADA"=>"VENTA_GRAVADA",
                        "ANTICIPOS_CLIENTES|143",
                        "ANTICIPOS_CLIENTES|111",
                        "ANTICIPOS_CLIENTES|21",
                        "PROVEEDORES_EXT"=>"PROVEEDORES_EXT",
                        "DEV_Y_DES"=>"DEV_Y_DES",
                        "CRE_A_FAVOR_CLI"=>"CRE_A_FAVOR_CLI",
                        "CRE_A_FAVOR_AGE"=>"CRE_A_FAVOR_AGE",
                        "COSTO_TICKET_BSP",
                        "ANTICIPO_PROVEEDOR",
                        "NOTA_CREDITO",
                        "ANTICIPO_COBRANZA",
                        "ANTICIPO_PROVEEDOR_LOCAL"=>"ANTICIPO_PROVEEDOR_LOCAL",
                        "EXENTA_LC",
                        "IVA_10_LC",
                        "IVA_CREDITO_10",
                        "IVA_DEBITO_10",
                        "VENTA",
                        "BANCO|143",
                        "BANCO|111",
                        "PROVEEDOR|143",
                        "PROVEEDOR|111",
                        "FACT_TRANS_LOCAL|143",
                        "FACT_TRANS_LOCAL|111",
                        "FACT_TRANS_LOCAL|43",
                        "FACT_TRANS_EXT|143",
                        "FACT_TRANS_EXT|111",
                        "FACT_TRANS_EXT|43",
                        "EFECTIVO_A_DEPOSITAR|143",
                        "EFECTIVO_A_DEPOSITAR|111",
                        "EFECTIVO_A_DEPOSITAR|43",
                        "CHEQUE_A_DEPOSITAR|143",
                        "CHEQUE_A_DEPOSITAR|111",
                        "CHEQUE_A_DEPOSITAR|43",
                        "CHEQUE_DIFERIDO_A_DEPOSITAR|143",
                        "CHEQUE_DIFERIDO_A_DEPOSITAR|111",
                        "CHEQUE_DIFERIDO_A_DEPOSITAR|43",
                    ];
            
        foreach($cuentaContable as $keys=>$cuenta){
            foreach($parametros as $key=>$parametro){
                $separador = explode('|',$parametro);
                if($cuenta->parametro == $separador[0]){
                    if(isset($separador[1])){
                        if($separador[1] == $cuenta->id_moneda){
                            unset($parametros[$key]);
                        }
                    }else{
                        unset($parametros[$key]);
                    }
                    
                }
            }
        }

        $indicador = count($cuentaContable);

        foreach($parametros as $key=>$parametro){
            $separador = explode('|',$parametro);
            $moneda = "";
            $detalleMoneda  ="";
            if(isset($separador[1])){
                if($separador[1] == 111){
                    $moneda = 111;
                    $detalleMoneda  ="PYG";
                }
                if($separador[1] == 43){
                    $moneda = 43;
                    $detalleMoneda  ="EUR";
                }
                if($separador[1] == 143){
                    $moneda = 143;
                    $detalleMoneda  ="USD";
                }
                if($separador[1] == 21){
                    $moneda = 21;
                    $detalleMoneda  ="REAL";
                }

            }    
            $arrayBase  = [];
            $arrayBase['id'] = $indicador.'0';
            $arrayBase['id_empresa'] = 1;
            $arrayBase['parametro'] = $separador[0];
            $arrayBase['valor'] = '';
            $arrayBase['activo'] = true;
            $arrayBase['id_cuenta_contable'] = 0;
            $arrayBase['id_moneda'] = $moneda;
            $arrayBase['empresa'] = $empresa->denominacion;
            $arrayBase['cuenta_contable'] = '';
            $arrayBase['moneda'] = $detalleMoneda;
            $cuentaContable[$indicador] = $arrayBase ;
            $indicador = $indicador + 1;
        }

        return response()->json(['data'=>$cuentaContable]);

    }

    public function negocioAdd(Request $req)
    {

        return view('pages.mc.negocios.add');
    }

    public function negocioIndex(Request $req)
    {
        $negocios = Negocio::where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();

        return view('pages.mc.negocios.index', compact('negocios'));
    }


    public function doAddNegocio(Request $req)
    {
       $resp = new \StdClass;
      try{
            $negocio = new Negocio;	
            $negocio->descripcion = $req->input('denominacion');
            $negocio->rentabilidad_minima = $req->input('rentabilidad_minima');
            $negocio->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
            $negocio->save();
            $resp->err = 'OK';
       } catch(\Exception $e){
            $resp->err = 'ERROR';
        } 
        return response()->json($resp);


    }

    public function negocioEdit(Request $req, $id)
    {
        $negocios = Negocio::where('id',$id)
                            ->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first();
       
        return view('pages.mc.negocios.edit', compact('negocios'));
    } 

    public function doEditNegocio(Request $req)
    {
        $resp = new \StdClass;
        $datosEmpresa = [
                'descripcion' => $req->input('denominacion'),
                'rentabilidad_minima' => $req->input('rentabilidad_minima')
            ];
        try{
		    $update = DB::table('unidad_negocios')
                            ->where('id',$req->input('id'))
                            ->update($datosEmpresa);
            $resp->err = 'OK';
        } catch(\Exception $e){
            $resp->err = 'ERROR';
        } 
        return response()->json($resp);

    }
    public function doDeleteNegocio(Request $req, $id)
    {
        try{
            Negocio::where('id', $id)->delete();
            flash('Se ha eliminado el negocio')->success();
            return redirect()->route('negocioIndex');
        } catch(\Exception $e){
            flash('No se ha eliminado el negocio. Intentelo nuevamente!')->error();
			return Redirect::back();

        } 
    }       


}
