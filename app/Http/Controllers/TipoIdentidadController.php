<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\TipoIdentidad;
use DB;


class TipoIdentidadController extends Controller
{
	public function index(Request $request){
		$tipoIdentidad = TipoIdentidad::where('activo', true)->get();
		return view('pages.mc.tipoIdentidad.index')->with('tipoIdentidads', $tipoIdentidad);
	}	

	public function add(Request $request){
		return view('pages.mc.tipoIdentidad.add');
	}	

	public function doAddTipoIdentidad(Request $request){
		$empresa = new TipoIdentidad;
		$empresa->denominacion = $request->input('denominacion');
		$empresa->activo = true;
		try{
			$empresa->save();
			flash('Se ha ingresado el Tipo Identidad exitosamente')->success();
			return redirect()->route('tipoIdentidads');
		} catch(\Exception $e){
			flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			return redirect()->back();
		}
	}

	public function edit(Request $request){
		$tipoIdentidad = TipoIdentidad::where('id', $request->input('id'))->get();
		return view('pages.mc.tipoIdentidad.edit')->with('tipoIdentidads', $tipoIdentidad);
	}	

	public function doEditTipoIdentidad(Request $request){
		DB::table('tipo_persona')
					    ->where('id',$request->input('id'))
					    ->update([
								'denominacion'=>$request->input('denominacion'),
					            ]);
		flash('Se ha editado el Tipo Identidad exitosamente')->success();
		return redirect()->route('tipoIdentidads');
	}	
	public function delete(Request $request){
		DB::table('tipo_persona')
					    ->where('id',$request->input('id'))
					    ->update([
								'activo'=>false,
					            ]);
		flash('Se ha eliminado exitosamente')->error();
		return redirect()->route('tipoIdentidads');
	}	
}