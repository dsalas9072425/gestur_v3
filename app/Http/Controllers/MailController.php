<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use App\Notifications\SuspiciousLogin;
use App\Notifications\CopiaRecibo;
use App\Mail\NotificacionesMail;
use App\Mail\NotificacionesSistema;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Storage;
use App\Persona;
use App\Notificacion;
use App\Recibo;
use App\EmpresaMail;
use App\Proforma;
use App\Empresa;
use App\ReservaNemo;
use App\Factura;
use App\FacturaDetalle;
use App\ComercioPersona;
use App\SolicitudFacturaParcial;
use App\HistoricoSaldoBanco;
use App\DocumentosEcos;
use Illuminate\Support\Facades\Log;
use Session;
use Config;
use DB;
use Redirect;
use Maatwebsite\Excel\Facades\Excel;
use App\TipoTimbrado;
use App\Exports\MailPagoProveedorExport;


class MailController extends Controller
{
	//====================PARAMETROS DE ESTADO FACTURA======================
    private $facturaFacturada = 29;
    private $facturaAnulada =  30;
    private $facturaVerificado = 2;
//===============================================================

 	private function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}

	public function enviarCorreo(Request $request){

		$id_recibo = $request->input('id_recibo');
		$email = explode(',', $request->input('correos_administrativos'));
		$con_copia = $request->input('concopia');

		$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first();
		if(empty($mailEmpresa)){
			$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', 0)->first();
		}
		
		$empresa = Empresa::where('id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first();
		$nombre_empresa = $empresa->denominacion;

        Config::set('mail.driver', $mailEmpresa->driver);
		Config::set('mail.host', $mailEmpresa->host);
		Config::set('mail.port', $mailEmpresa->port);
		Config::set('mail.encryption', $mailEmpresa->encryption);
		Config::set('mail.username', $mailEmpresa->username);
		Config::set('mail.password', $mailEmpresa->password); 

		//CORRECCION PARA FILTRAR LOS COBROS NO ACTIVOS
			$recibos = new Recibo; 
			/*$recibos = $recibos->whereHas('formaCobroDetalle', function ($query) {
				$query->where('activo', true);
			});*/
			$recibos = $recibos->with(['detalle.libroVenta','detalle.libroCompra','detalle.anticipo','cliente','moneda','formaCobroDetalle.forma_cobro','sucursalEmpresa']);
			$recibos = $recibos->selectRaw("TO_CHAR(fecha_hora_creacion,'DD/MM/YYYY') as fecha_creacion_format,
															substring(concepto for 10)||'...' as concepto_cortado,
															TO_CHAR(fecha_hora_cobro,'DD/MM/YYYY') as fecha_hora_cobro_format,* ");
			$recibos = $recibos->where('id',$id_recibo);

			$recibos = $recibos->get();
			
		foreach( $recibos as $value){
			$value->totalLetras = $this->convertir($value->total_pago);
		}

		$option = 1;

		$pdf = \PDF::loadView('pages.mc.cobranzas.reciboImpreso',compact('recibos','option'));
		$pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);

		$pdf->setPaper('a4', 'letter')->setWarnings(false);
		//return $pdf->download('Recibo Dinero-'.$this->getId4Log().'.pdf');

		$adjunto = $pdf->output();

		foreach ($recibos as $recibo ){
			$ruc =$recibo->cliente->documento_identidad;
			if($recibo->cliente->dv){
				$ruc .= $ruc."-".$recibo->cliente->dv;
			}
			$idCliente = $recibo->cliente->id;
			$nombreCliente = $recibo->cliente->nombre;
			$emailCliente = $recibo->cliente->correo_administrativo;
			$reciboDetalle = $recibo->detalle;
	        $denominacionCliente = $recibo->cliente->nombre.' '.$recibo->cliente->apellido;
	        $rucCliente = $ruc;
	        $monedaLetras = $recibo->moneda->hb_desc;
	        $monedaCodigo = $recibo->moneda->currency_code;
	        $montoLetras = $recibo->totalLetras;
	        $concepto = $recibo->concepto;
	        $observaciones = '';
	        $total = $recibo->total_pago;
	        $numRecibo = $recibo->nro_recibo;
	        $fechaEmision = $recibo->fecha_creacion_format;   
	        $detalleValores = ''; 
	        $forma_pago = '';
	        //SI EL RECIBO ESTA APLICADO O COBRADO MOSTRAR LAS FORMAS DE PAGO
	        if(!is_null($recibo->formaCobroDetalle)){
	            foreach($recibo->formaCobroDetalle as $value){
		            if(isset($value->forma_cobro->denominacion)){
		                $forma_pago = $value->forma_cobro->abreviatura;
		            }else{
		                $forma_pago = '';
		            }
		            $detalleValores .= $forma_pago.' '.$value->importe_pago.' '.$value->moneda->currency_code.'  /  ';
	            }   
	        }
	        
	        $imprimir  = [];    
	        $opcionImpresion = 1;
	        $contadorFila = 0;		
    	}

		$reciboHtml = '<table style="width:850px; color:#111;" class="" cellpadding="0">';
		$reciboHtml .= '<tr>';
		$reciboHtml .= '<td style="width:70%">';
		$reciboHtml .= '<table cellpadding="0" style="">';
		$reciboHtml .= '<colgroup>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '<col style="width:10%"/>';
		$reciboHtml .= '</colgroup>';
		$reciboHtml .= '<tr>';
		$reciboHtml .= '<td colspan="2">';
		$reciboHtml .= '<img src="https://gestur.git.com.py/logoEmpresa/logoDtp.png" width="80px">';
		$reciboHtml .= '</td>'; 
        $reciboHtml .= '<td class="c-text" colspan="10" style="font-size: 7px !important; font-weight:800;">';
        $reciboHtml .= '<span class=""></span><br>';
        $reciboHtml .= '<span class="">OPERADORA MAYORISTA DE TURISMO</span><br>';
        $reciboHtml .= 'Actividades de Agencia de Viaje<br>';   
        $reciboHtml .= 'CAPITAL Gs. 94.779.358.-<br>';
        $reciboHtml .= '<span class="">REGISTRO SENATUR Nro. 287-B</span>';
        $reciboHtml .= '</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '</table>';
        $reciboHtml .= '</td>';
        $reciboHtml .= '<td style="width:30%">';
        $reciboHtml .= '<table style:"border: 1px solid; color:#111;">';
        $reciboHtml .= '<colgroup>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '</colgroup>';
        $reciboHtml .= '<tr style="font-size: 12px !important; font-weight:800; color:#111;">';
        $reciboHtml .= '<td colspan="5" style="margin-top:10px;" class="mt-10">RUC : 80001813-3</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '<tr style="font-size: 12px !important; color:#111; font-weight:800; margin-top:10px;" class="mt-10">';
        $reciboHtml .= '<td colspan="12">RECIBO N°:'.$recibo->sucursalEmpresa->denominacion.'-'.$numRecibo.'</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '<tr style="font-size: 10px !important; color:#111; font-weight:800; margin-top:10px;" class="mt-10">';
        $reciboHtml .= '<td colspan="12">FECHA : '.$fechaEmision.'</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '<tr style="font-size: 10px !important; color:#111; font-weight:800; margin-top:10px;" class="mt-10">';
        $reciboHtml .= '<td colspan="12">MONTO '.$monedaCodigo.' '.$total.'</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '<tr style="font-size: 10px !important; color:#111; font-weight:800; margin-top:10px;" class="mt-10">';
        $reciboHtml .= '<td colspan="12"></td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '</table>';
        $reciboHtml .= '</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '</table>';
   	    $reciboHtml .= '<table>';
        $reciboHtml .= '<tr class="" style="font-size:8px; color:#111;">';
        $reciboHtml .= '<td>';
		$reciboHtml .= '<b>Casa Matriz:</b> Edificio SkyPark Aviadores de Chaco 2581 Torre 2 - Piso 17. Asunción - Paraguay Telefonos: +59521 729 7070<br>';
        $reciboHtml .= `<b>Sucursal :</b> Avda Pioneros del Este entre Adrian Jara y Pa'i Perez, Galeria JyD. Ciudad del Este - Paraguay. Telefono: 061-511-779 al 80<br>`;
        $reciboHtml .= '<b>Sucursal :</b> Mcal Estigarriba 1997. Galeria San Jorge, Local 13 y 16. Encarnación - Paraguay. Telefono: 071-200-104<br>';
        $reciboHtml .= '<b>Sucursal :</b> Receptivo:  Yvypyta c/ Cerro Leon. Asunción - Paraguay Telefonos: 0985-811-5221<br>';
        $reciboHtml .= '</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '</table>';

        $reciboHtml .= '<table style="width:850px;" style="border-top:1px solid; color:#111;" class="b-buttom b-top">';
        $reciboHtml .= '<colgroup>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '<col style="width:10%"/>';
        $reciboHtml .= '</colgroup>';
        $reciboHtml .= '<tr style="font-size: 10px; color:#111;">';
        $reciboHtml .= '<td class="" colspan="6">';
        $reciboHtml .= '<b>Recibí de</b> '.$denominacionCliente.'';
        $reciboHtml .= '</td>';
        $reciboHtml .= '<td class="f-10"  colspan="4">';
        $reciboHtml .= '<b> RUC :</b>'.$rucCliente.'';
        $reciboHtml .= '</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '<tr style="font-size: 10px;color:#111;">';
        $reciboHtml .= '<td class="" colspan="10">'; 
        $reciboHtml .= '<b>La Cantidad de '.$monedaLetras.' </b><span style="border-radius:10px; padding:1px;background-color:#B6AAAA; width: 100%;">'.$montoLetras.'</span>'; 
        $reciboHtml .= '</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '<tr style="font-size: 9px;color:#111;">';
        $reciboHtml .= '<td colspan="10" style=" word-wrap: break-word;">';
        $reciboHtml .= '<b>Por concepto de :</b>'.$concepto.'';
        $reciboHtml .= '</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '</table>';
		$reciboHtml .= '<table style="width:850px; text-align: center;border-collapse: collapse; color:#111;" class="c-text b-col" border="1">';
		$reciboHtml .= '<colgroup>';
		$reciboHtml .= '<col/>';
		$reciboHtml .= '<col/>';
		$reciboHtml .= '<col/>';
		$reciboHtml .= '<col/>';
		$reciboHtml .= '<col/>';
		$reciboHtml .= '<col/>';
		$reciboHtml .= '</colgroup>';
		$reciboHtml .= '<tr style="font-size: 10px !important;">';
		$reciboHtml .= '<td width="15%">';
		$reciboHtml .= 'Factura N°';
		$reciboHtml .= '</td>';
		$reciboHtml .= '<td width="15%">';
		$reciboHtml .= 'Fecha';
		$reciboHtml .= '</td>';
		$reciboHtml .= '<td width="15%">';
		$reciboHtml .= 'Importe Total';
		$reciboHtml .= '</td>';
		$reciboHtml .= '<td width="15%">';
		$reciboHtml .= 'Factura N°';
		$reciboHtml .= '</td>';
		$reciboHtml .= '<td width="15%">';
		$reciboHtml .= 'Fecha';
		$reciboHtml .= '</td>';
		$reciboHtml .= '<td width="15%">';
		$reciboHtml .= 'Importe Total';
		$reciboHtml .= '</td>';
        $contadorFila = 0;
        $cantDetalle = count($reciboDetalle) -1 ;
        $cantInsert = 40 - count($reciboDetalle); 
        //SABER SI ES PAR  O IMPAR  
        if ($cantInsert%2==0){
            $cantInsert = 40 - count($reciboDetalle); 
        }else{
            $cantInsert = 40 - count($reciboDetalle) - 1 ; 
        }
     	foreach($reciboDetalle as $key => $value){
	        if($contadorFila == 0){
	        	$reciboHtml .= '<tr style="font-size: 9px !important;">';
	        }    
	       
	    	if(!empty($value->libroVenta)){
	            $imprimir = " 	<td>".$value->libroVenta->nro_documento."</td>
	            				<td>".$value->libroVenta->fecha_hora_documento."</td>
	            				<td>".$value->importe."</td>";
	        }
	        if(!empty($value->libroCompra)){
	            $imprimir =  "  <td>".$value->libroCompra->nro_documento."</td>
	            				<td>".$value->libroCompra->fecha."</td>
	            				<td>".$value->importe."</td>";
	        }
	        if(!empty($value->anticipo)){
	            $imprimir =  "  <td>".$value->anticipo->documento."</td>
	             				<td>".$value->anticipo->fecha_hora_creacion."</td>
	             				<td>".$value->importe."</td>";
	        }
	        $reciboHtml .= $imprimir;
            if($contadorFila == 1 || $key == $cantDetalle){
	                if($key == $cantDetalle && $contadorFila == 0){
	                    $reciboHtml .= '<td>--- ---</td>';
	                    $reciboHtml .= '<td>--- ---</td>';
	                    $reciboHtml .= '<td>--- ---</td>';
	        		}
	                $reciboHtml .= '</tr>';
	                $contadorFila = 0;
	        }else{
	            $contadorFila++;    
	        } 
        }     
    
        $reciboHtml .= '<tr style="font-size: 10px !important;">';
        $reciboHtml .= '<td colspan="6">';
        $reciboHtml .= '<b>TOTAL:</b> '.$total.'';
        $reciboHtml .= '</td>';
        $reciboHtml .= '</tr>';
        $reciboHtml .= '</table>';
		$reciboHtml .= '<table style="width:850px;">';
		$reciboHtml .= '<tr>';
		$reciboHtml .= '<td style="font-size:9px; border: 1px solid; color:#111;">';
		$reciboHtml .= 'Detalle de Valores:  '.$detalleValores.'';
		$reciboHtml .= '</td>';
		$reciboHtml .= '</tr>';
		$reciboHtml .= '<tr>';
		$reciboHtml .= '<td style="font-size:9px;">';
		$reciboHtml .= '</td>';
		$reciboHtml .= '</tr>';
		$reciboHtml .= '</table>';
		$reciboHtml .= '</td>';
		$reciboHtml .= '</tr>';
		$reciboHtml .= '</table>';

		$direccionRecibo = "https://gestur.git.com.py/imprimirRecibo/".$id_recibo."?option=1";
 		
 		$mensaje = new \StdClass; 
		$user = new Persona();
	    $user->name = $nombreCliente;
	    $user->email = $email;
	    $user->cuerpoHtml = $reciboHtml;
	    if($con_copia != ""){
	    	$user->cc = $con_copia;
	    }
	    $user->subject = 'Recibo de Pago';
	    $user->greeting = 'Hola ' . $user->name;
	    $user->linea1 = 'Este es el detalle del recibo abonado';
	    $user->accionRecibo = $nombre_empresa;
		$user->direccionRecibo = $direccionRecibo;
		$user->linea2 ='';
		$user->adjunto = $adjunto;
		$user->numRecibo = $numRecibo;
		$user->from = $mailEmpresa->username;
		$bodyMail = $user->greeting.'<br>'.$user->linea1.'<br>'.$user->cuerpoHtml.'<br><a href="'.$direccionRecibo.'" class="button" target="_blank">'.$user->accionRecibo.'<br></a>'.$user->linea2.';';
		//try{
	    	$user->notify(new SuspiciousLogin());
	    	$user->email = $mailEmpresa->cco;
			$user->notify(new CopiaRecibo());
	    	$notificacion = new Notificacion; 
			$notificacion->id_usuario = $idCliente;
			$notificacion->id_agencia = $idCliente;
			$notificacion->fecha_hora_creacion = date('Y-m-d h:m:s');
			$notificacion->de = $mailEmpresa->username;
			$notificacion->para = $user->email; 
			$notificacion->cc = $mailEmpresa->cco;
			$notificacion->contenido = $bodyMail;
			$notificacion->fecha_hora_envio = date('Y-m-d h:m:s');
			$notificacion->estado = 'N';
			$notificacion->asunto = $user->subject;
			$notificacion->id_log = $this->getId4Log();
			$notificacion->tipo = 13;
			$notificacion->leido = 'N';
			$notificacion->save();
			$mensaje->status = 'OK';
            $mensaje->mensaje = 'Se ha enviado correctamente el correo';
			return response()->json($mensaje);
		/*}	
	    catch(\Exception $e){ // Using a generic exception
		   	$mensaje->status = 'ERROR';
            $mensaje->mensaje = 'No se envio el correo';
		    return response()->json($mensaje);
		}	*/

	}    


public static function convertir($number, $moneda = '', $centimos = '', $forzarCentimos = false)
{
	$converted = '';
	$decimales = '';
	if (($number < 0) || ($number > 999999999)) {
		return 'No es posible convertir el numero a letras';
	}
	$div_decimales = explode('.',$number);
	if(count($div_decimales) > 1){
		$number = $div_decimales[0];
		$decNumberStr = (string) $div_decimales[1];
		//SI VIENEN NUMERO SOLO LE RELLENA CON CERO
		$decNumberStr = str_pad($decNumberStr, 2, "0", STR_PAD_RIGHT);
		if(strlen($decNumberStr) == 2){
			$decNumberStrFill = str_pad($decNumberStr, 9, '0', STR_PAD_LEFT);
			$decCientos = substr($decNumberStrFill, 6);
			$decimales = self::convertGroup($decCientos);
		}
	}
	else if (count($div_decimales) == 1 && $forzarCentimos){
		$decimales = 'CERO ';
	}
	$numberStr = (string) $number;
	$numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
	$millones = substr($numberStrFill, 0, 3);
	$miles = substr($numberStrFill, 3, 3);
	$cientos = substr($numberStrFill, 6);
	if (intval($millones) > 0) {
		if ($millones == '001') {
			$converted .= 'UN MILLON ';
		} else if (intval($millones) > 0) {
			$converted .= sprintf('%sMILLONES ', self::convertGroup($millones));
		}
	}
	if (intval($miles) > 0) {
		if ($miles == '001') {
			$converted .= 'MIL ';
		} else if (intval($miles) > 0) {
			$converted .= sprintf('%sMIL ', self::convertGroup($miles));
		}
	}
	if (intval($cientos) > 0) {
		if ($cientos == '001') {
			$converted .= 'UN ';
		} else if (intval($cientos) > 0) {
			$converted .= sprintf('%s ', self::convertGroup($cientos));
		}
	}
	if(empty($decimales)){
		$valor_convertido = $converted . strtoupper($moneda);
	} else {
		$valor_convertido = $converted . strtoupper($moneda) . ' CON ' . $decimales . ' ' . strtoupper($centimos);
	}
	return $valor_convertido;
}


private static function convertGroup($n)
{
	$output = '';
	if ($n == '100') {
		$output = "CIEN ";
	} else if ($n[0] !== '0') {
		$output = self::$CENTENAS[$n[0] - 1];
	}
	$k = intval(substr($n,1));
	if ($k <= 20) {
		$output .= self::$UNIDADES[$k];
	} else {
		if(($k > 30) && ($n[2] !== '0')) {
			$output .= sprintf('%sY %s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
		} else {
			$output .= sprintf('%s%s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
		}
	}
	return $output;
}

 private static $UNIDADES = [
	'',
	'UN ',
	'DOS ',
	'TRES ',
	'CUATRO ',
	'CINCO ',
	'SEIS ',
	'SIETE ',
	'OCHO ',
	'NUEVE ',
	'DIEZ ',
	'ONCE ',
	'DOCE ',
	'TRECE ',
	'CATORCE ',
	'QUINCE ',
	'DIECISEIS ',
	'DIECISIETE ',
	'DIECIOCHO ',
	'DIECINUEVE ',
	'VEINTE '
];
private static $DECENAS = [
	'VENTI',
	'TREINTA ',
	'CUARENTA ',
	'CINCUENTA ',
	'SESENTA ',
	'SETENTA ',
	'OCHENTA ',
	'NOVENTA ',
	'CIEN '
];
private static $CENTENAS = [
	'CIENTO ',
	'DOSCIENTOS ',
	'TRESCIENTOS ',
	'CUATROCIENTOS ',
	'QUINIENTOS ',
	'SEISCIENTOS ',
	'SETECIENTOS ',
	'OCHOCIENTOS ',
	'NOVECIENTOS '
];

	public function notificar(Request $request){
		$data = DB::table('vw_notificacion_pendiente');
		$data = $data->where('estado', 'P');
		if($request->input('id') !== null){
			$data = $data->where('id', $request->input('id'));
		}

	//	$data = $data->where('tipo','<>' ,27);
		$data = $data->orderBy('id','DESC');
		$data = $data->get();

		/*echo '<pre>';
		print_r($data);
		die;*/

		$pdf_resumen = '';
		foreach($data as $key=>$correo){
			$empresa = $correo->empresa;
			$de = $correo->de;
			$asunto = $correo->asunto;

			if($correo->tipo == 14){
				$excelFile = $this->excelData($correo->empresa_id);
				$adjunto = public_path('excelPagoProveedor/' . $excelFile.'.xls');
			}else{	
				$adjunto = "";
			}
			$email = $correo->para;
			//$id_asistente = $correo->id_asistente; //se agrega el id_asistente
			$persona = Persona::where('id', $correo->id_usuario)->first(['nombre', 'apellido']);
			//$asistente = Proforma::where('id_asistente', $correo->id_asistente)->first(['nombre', 'apellido']);
			/*if(isset($asistente->nombre)){
				$name = $asistente->nombre." ".$asistente->apellido;
			}else{	
				$name =	$id_asistente;
			}*/
			if(isset($persona->nombre)){
				$name = $persona->nombre." ".$persona->apellido;
			}else{	
				$name =	$email;
			}

			if($correo->tipo == 15 ||$correo->tipo == 16 ||$correo->tipo == 17 ){
				$datos= $this->proformaData($correo->adjunto_documento);
			    $asunto = $correo->asunto."-".$correo->adjunto_documento;

				$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', $datos->id_empresa)->first();
				if(empty($mailEmpresa)){
					$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', 0)->first();
					$empresaArray = Empresa::where('id', $datos->id_empresa)->first();
					$de = $mailEmpresa->username;
					$empresa = $empresaArray->denominacion;
					$logoEmpresa = $empresaArray->logo;
	
				}else{
					$de = $mailEmpresa->username;
					$empresa = $mailEmpresa->empresa->denominacion;
					$logoEmpresa = $mailEmpresa->empresa->logo;
				}
				Config::set('mail.driver', $mailEmpresa->driver);
				Config::set('mail.host', $mailEmpresa->host);
				Config::set('mail.port', $mailEmpresa->port);
				Config::set('mail.encryption', $mailEmpresa->encryption);
				Config::set('mail.username', $mailEmpresa->username);
				Config::set('mail.password', $mailEmpresa->password); 
				if($correo->tipo == 15 ){
					$estado= 'a Facturar';
				}	
				if($correo->tipo == 16 ){
					$estado= 'Rechazada';
				}	
				if($correo->tipo == 17 ){
					$estado= 'Facturada';
				}	
				if(isset($datos->vendedor->nombre)){
					$vendedor = $datos->vendedor->nombre." ".$datos->vendedor->apellido;	
				}else{
					$vendedor = "";
				}
				if(isset($datos->asistente->nombre)){
					$asistente = $datos->asistente->nombre." ".$datos->asistente->apellido;	
				}else{	
					$asistente = "";
				}	
				$empresaLogo = "https://gestur.git.com.py/logoEmpresa/".$logoEmpresa;

				$data = [
					'nombre' => $name,
					'proforma' => $datos->id,
					'usuario' => $datos->usuario->nombre." ".$datos->usuario->apellido,
					'vendedor' => $vendedor,
					'asistente' => $asistente,
					'cliente' => $datos->cliente->nombre." ".$datos->cliente->apellido,
					'estado'=> $estado,
					'logo_empresa'=>$empresaLogo,
					'empresa'=>$empresa,
					
				];
			}else{	
				if($correo->tipo == 1 ||$correo->tipo == 2 ||$correo->tipo == 18||$correo->tipo == 19 ||$correo->tipo == 20){
					$persona = Persona::where('id', $correo->id_usuario)->first(['nombre', 'apellido']);
					if(isset($persona->nombre)){
						$name = $persona->nombre." ".$persona->apellido;
					}else{	
						$name =	$email;
					}
					$reservas = ReservaNemo::where('id', $correo->token)->first();
					if(isset($reservas->booking_reference)){
						$booking_reference = $reservas->booking_reference;
					}else{
						$booking_reference = "";
					}	
					if(isset($reservas->booking_creation_date)){
						$booking_creation_date = $reservas->booking_creation_date;
					}else{
						$booking_creation_date = "";
					}	
					if(isset($reservas->supplier_name)){
						$supplier_name = $reservas->supplier_name;
					}else{
						$supplier_name = "";
					}	
					if(isset($reservas->client_name)){
						$client_name = $reservas->client_name;
					}else{
						$client_name = "";
					}	
					if(isset($reservas->booking_state_descripcion)){
						$booking_state_descripcion = $reservas->booking_state_descripcion;
					}else{
						$booking_state_descripcion = "";
					}
					if(isset($reservas->destino_ciudad)){
						$destino = $reservas->destino_ciudad." - ".$reservas->destino_pais;
					}else{
						$destino = "";
					}
					if(isset($reservas->booking_checkin)){
						$booking_checkin = $reservas->booking_checkin;
					}else{
						$booking_checkin = "";
					}
					if(isset($reservas->booking_checkout)){
						$booking_checkout = $reservas->booking_checkout;
					}else{
						$booking_checkout = "";
					}

					$data = [
						'titulo' => $correo->titulo,
						'nombre' => $name,
						'reserva_reference' => $booking_reference,
						'fecha_creacion' =>$booking_creation_date,
						'proveedor' => $supplier_name,
						'email' => $email,
						'cliente' => $client_name,
						'estado'=> $booking_state_descripcion,
						'destino'=> $destino,
						'checkin'=> $booking_checkin,
						'checkout'=> $booking_checkout
					];
				}else{
					if($correo->tipo == 3){
						$data = [
							'nombre' => $name,
							'token' => $correo->token
						];
						$empresa = 'GIT';
					}else{
						$data = [
							'nombre' => $name,
							'token' => $correo->token
						];
					}
				}	
			}
			if($correo->tipo == 21){
				$idFactura = $correo->adjunto_documento;
				$factura = Factura::with('cliente',
						   'pasajero',
						   'vendedorAgencia',
						   'vendedorEmpresa',
						   'tipoFactura',
						   'timbrado',
						   'proforma',
						   'currency')
						   ->where('id',$idFactura)->first();

				$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', $factura->id_empresa)->first();
				if(empty($mailEmpresa)){
					$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', 0)->first();
					$empresaArray = Empresa::where('id', $factura->id_empresa)->first();
					$de = $mailEmpresa->username;
					$empresa = $empresaArray->denominacion;
					$logoEmpresa = $empresaArray->logo;
				}else{
					$de = $mailEmpresa->username;
					$empresa = $mailEmpresa->empresa->denominacion;
					$logoEmpresa = $mailEmpresa->empresa->logo;
				}
				$totalFactura = DB::select('SELECT * FROM get_monto_factura('.$idFactura.')');
				$totalFactura = $totalFactura[0]->get_monto_factura;
				if($factura->id_moneda_venta == 143){
					$monedaFactura = 'USD';
				}else{
					$monedaFactura = 'PYG';
				}
				$empresaLogo = "https://gestur.git.com.py/logoEmpresa/".$logoEmpresa;

				$fechaBase = explode(' ',$factura->fecha_hora_facturacion);
				$fechaEmisionFactura = $this->formatoFechaSalida($fechaBase[0]);
				$data = [
					'factura' => $factura->nro_factura,
					'nombre' =>$factura->cliente->nombre." ". $factura->cliente->apellido,
					'fecha_emision' => $fechaEmisionFactura,
					'total_factura' =>$totalFactura,
					'moneda' => $monedaFactura,
					'logo_empresa'=>$empresaLogo,
					'empresa'=>$empresa,
				];
				$pdfFile = $this->facturaAdjunto($correo->adjunto_documento);
				$pdf = $pdfFile;
				$nro_factura = $factura->nro_factura;
			}else{
				$pdf = "";
				$nro_factura ="";
			}	
			if($correo->tipo == 23){
				$solicitud = SolicitudFacturaParcial::with('currency', 'cliente', 'usuario')->where('id', $correo->adjunto_documento)->first();
			
				$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', $solicitud->id_empresa)->first();
				if(empty($mailEmpresa)){
					$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', 0)->first();
					$empresaArray = Empresa::where('id', $solicitud->id_empresa)->first();
					$de = $mailEmpresa->username;
					$empresa = $empresaArray->denominacion;
					$logoEmpresa = $empresaArray->logo;
				}else{
					$de = $mailEmpresa->username;
					$empresa = $mailEmpresa->empresa->denominacion;
					$logoEmpresa = $mailEmpresa->empresa->logo;
				}
				Config::set('mail.driver', $mailEmpresa->driver);
				Config::set('mail.host', $mailEmpresa->host);
				Config::set('mail.port', $mailEmpresa->port);
				Config::set('mail.encryption', $mailEmpresa->encryption);
				Config::set('mail.username', $mailEmpresa->username);
				Config::set('mail.password', $mailEmpresa->password); 

				$empresaLogo = "https://gestur.git.com.py/logoEmpresa/".$logoEmpresa;

				if($solicitud->id_moneda == 111){
					$monto_facturar =  number_format($solicitud->monto, 0,',','.');
				}else{ 
					$monto_facturar = number_format( $solicitud->monto,2,",","."); 
				}

				$ruc =$solicitud->cliente->documento_identidad;
				if($solicitud->cliente->dv){
					$ruc .= $ruc."-".$solicitud->cliente->dv;
				}

				$data = [
					'nombre'=>$correo->para,
					'proforma' => $solicitud->id_proforma,
					'cliente_nombre' =>$solicitud->cliente->nombre." ". $solicitud->cliente->apellido,
					'cliente_ruc' =>$ruc,
					'fecha_pedido' => date('d/m/Y H:m:s', strtotime($solicitud->fecha_hora_pedido)),
					'usuario_nombre' =>$solicitud->usuario->nombre." ". $solicitud->usuario->apellido,
					'total_factura_parcial' =>$monto_facturar,
					'moneda' => $solicitud->currency->currency_code,
					'logo_empresa'=>$empresaLogo,
					'empresa'=>$empresa,
				];
			}

			if($correo->tipo == 24){
				$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', $correo->empresa_id)->first();
				if(empty($mailEmpresa)){
					$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', 0)->first();
					$empresaArray = Empresa::where('id', $correo->empresa_id)->first();
					$de = $mailEmpresa->username;
					$empresa = $empresaArray->denominacion;
					$logoEmpresa = $empresaArray->logo;
				}else{
					$de = $mailEmpresa->username;
					$empresa = $mailEmpresa->empresa->denominacion;
					$logoEmpresa = $mailEmpresa->empresa->logo;
				}
				Config::set('mail.driver', $mailEmpresa->driver);
				Config::set('mail.host', $mailEmpresa->host);
				Config::set('mail.port', $mailEmpresa->port);
				Config::set('mail.encryption', $mailEmpresa->encryption);
				Config::set('mail.username', $mailEmpresa->username);
				Config::set('mail.password', $mailEmpresa->password); 

				$empresaLogo = "https://gestur.git.com.py/logoEmpresa/".$logoEmpresa;

				$data = [
					'nombre'=>$correo->para,
					'logo_empresa'=>$empresaLogo,
					'empresa'=>$empresa,
					'id_asiento'=>$correo->adjunto_documento
				];
			}
			
			if($correo->tipo == 22){
				$datos  = explode('_', $correo->id_log);
				$reservas = ReservaNemo::where('id', $datos[0])->first();
				if(isset($reservas->booking_reference)){
					$booking_reference = $reservas->code_supplier;
				}else{
					$booking_reference = "";
				}	
				if(isset($reservas->booking_creation_date)){
					$booking_creation_date = $reservas->booking_creation_date;
				}else{
					$booking_creation_date = "";
				}	
				if(isset($reservas->supplier_name)){
					$supplier_name = $reservas->supplier_name;
				}else{
					$supplier_name = "";
				}	
				if(isset($reservas->booking_state_descripcion)){
					$booking_state_descripcion = $reservas->booking_state_descripcion;
				}else{
					$booking_state_descripcion = "";
				}
				if(isset($reservas->destino_ciudad)){
					$destino = $reservas->destino_ciudad." - ".$reservas->destino_pais;
				}else{
					$destino = "";
				}
				if(isset($reservas->booking_checkin)){
					$booking_checkin = $reservas->booking_checkin;
				}else{
					$booking_checkin = "";
				}
				if(isset($reservas->booking_checkout)){
					$booking_checkout = $reservas->booking_checkout;
				}else{
					$booking_checkout = "";
				}

				$factura = Factura::with('cliente',
										'pasajero',
										'vendedorAgencia',
										'vendedorEmpresa',
										'tipoFactura',
										'timbrado',
										'proforma',
										'currency')
										->where('id',$datos[1])->first();

				$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', $factura->id_empresa)->first();
				if(empty($mailEmpresa)){
					$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', 0)->first();
					$empresaArray = Empresa::where('id', $factura->id_empresa)->first();
					$de = $mailEmpresa->username;
					$empresa = $empresaArray->denominacion;
					$logoEmpresa = $empresaArray->logo;
				}else{
					$de = $mailEmpresa->username;
					$empresa = $mailEmpresa->empresa->denominacion;
					$logoEmpresa = $mailEmpresa->empresa->logo;
				}
				Config::set('mail.driver', $mailEmpresa->driver);
				Config::set('mail.host', $mailEmpresa->host);
				Config::set('mail.port', $mailEmpresa->port);
				Config::set('mail.encryption', $mailEmpresa->encryption);
				Config::set('mail.username', $mailEmpresa->username);
				Config::set('mail.password', $mailEmpresa->password); 

				$nombre = $name;

				$persona = Persona::where('email', $correo->para)->first(['nombre', 'apellido']);
				if(isset($persona->nombre)){
					$name = $persona->nombre." ".$persona->apellido;
				}else{	
					$name =	$email;
				}
				$totalFactura = DB::select('SELECT * FROM get_monto_factura('.$datos[1].')');
				$totalFactura = $totalFactura[0]->get_monto_factura;
				if($factura->id_moneda_venta == 143){
					$monedaFactura = 'USD';
				}else{
					$monedaFactura = 'PYG';
				}
				$empresaLogo = "https://gestur.git.com.py/logoEmpresa/".$logoEmpresa;

				$data = [
						'nombre' => $name." __ ".$email,
						'reserva_reference' => $reservas->code_supplier,
						'fecha_creacion' =>$booking_creation_date,
						'proveedor' => $supplier_name,
						'email' => $email,
						'ejecutivo'=>$nombre,
						'estado'=> $booking_state_descripcion,
						'destino'=> $destino,
						'checkin'=> $booking_checkin,
						'checkout'=> $booking_checkout,
						'factura' => $factura->nro_factura,
						'cliente' =>$factura->cliente->nombre." ". $factura->cliente->apellido,
						'fecha_emision' => $fechaEmisionFactura = date('d/m/Y', strtotime($factura->fecha_hora_facturacion)),
						'total_factura' =>$totalFactura,
						'moneda' => $monedaFactura,
						'logo_empresa'=>$empresaLogo,
						'empresa'=>$empresa,
					];
			}

			if($correo->tipo == 25 || $correo->tipo == 26){

				//Si la empresa tiene su propia configuracion para envio de correo
				$this->setearNewConfig($correo);

				//Generar el resumen ejecutivo para enviar
				$empresa = Empresa::find($correo->empresa_id);
				$empresa_logo = 'https://gestur.git.com.py/logoEmpresa/'.$empresa->logo;
				$pdf_resumen = $this->resumenEjecutivo($empresa);

					$data = [
						'denominacion_empresa' => $correo->empresa,
						'empresa_logo'=>$empresa_logo,
					];

				$empresa = $correo->empresa;
			}

			// if($correo->tipo == 27){
			// 		$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', $correo->empresa_id)->first();
			// 		if(empty($mailEmpresa)){
			// 			$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', 0)->first();
			// 			$empresaArray = Empresa::where('id', $correo->empresa_id)->first();
			// 			$de = $mailEmpresa->username;
			// 			$empresa = $empresaArray->denominacion;
			// 			$logoEmpresa = $empresaArray->logo;
			// 		}else{
			// 			$de = $mailEmpresa->username;
			// 			$empresa = $mailEmpresa->empresa->denominacion;
			// 			$logoEmpresa = $mailEmpresa->empresa->logo;
			// 		}
			// 		Config::set('mail.driver', $mailEmpresa->driver);
			// 		Config::set('mail.host', $mailEmpresa->host);
			// 		Config::set('mail.port', $mailEmpresa->port);
			// 		Config::set('mail.encryption', $mailEmpresa->encryption);
			// 		Config::set('mail.username', $mailEmpresa->username);
			// 		Config::set('mail.password', $mailEmpresa->password); 
			// 		$empresaLogo = "https://gestur.git.com.py/logoEmpresa/".$logoEmpresa;

			// 		$documentos_ecos = DocumentosEcos::with('factura')->where('id',$correo->adjunto_documento)->first();

			// 		$persona = Persona::where('email', $correo->para)->first(['nombre', 'apellido']);
			// 		if(isset($persona->nombre)){
			// 			$name = $persona->nombre." ".$persona->apellido;
			// 		}else{	
			// 			$name =	$email;
			// 		}
						
			// 		if ($correo->empresa_id == 1) {
			// 			$username = 'admin@dtparaguayo.com.py';
			// 			$password = 'ILZXg16OE';
			// 		} else {
			// 			$username = 'admin@boardingpass.com.py';
			// 			$password = 'tiRRmrHQce';
			// 		}

			// 		$client = new Client();
			// 		//$response = $client->post('http://3.140.122.68:8080/Token', [
			// 		$response = $client->post('http://3.140.122.68/Token', [
			// 			'headers' => [
			// 				'Content-Type' => 'application/x-www-form-urlencoded',
			// 			],
			// 			'form_params' => [
			// 				'username' => $username,
			// 				'password' => $password,
			// 				'grant_type' => 'password',
			// 			],
			// 		]);

			// 		$body = $response->getBody();
			// 		$data = json_decode($body, true);

			// 		// Accede a los datos de la respuesta
			// 		$accessToken = $data['access_token'];
			// 		//$response = $client->get('http://3.140.122.68:8080/api/electronicdocument/getkude/'.$documentos_ecos->cdc, [
			// 		$response = $client->get('http://3.140.122.68/api/electronicdocument/getkude/'.$documentos_ecos->cdc, [
			// 			'headers' => [
			// 				'Authorization' => 'Bearer ' . $accessToken,
			// 			],
			// 		]);

			// 		if ($response->getStatusCode() === 200){
			// 			$contentDisposition = $response->getHeaderLine('Content-Disposition');
			// 			$filename = '';
			// 			if (preg_match('/filename="(.+)"/', $contentDisposition, $matches)) {
			// 				$filename = $matches[1];
			// 			}

			// 			$fileContents = $response->getBody()->getContents();
			// 			// Guardar el archivo en el disco "facturaElectronica" de Laravel
			// 			Storage::disk('facturaElectronica')->put($filename, $fileContents);

			// 			// Generar la URL de descarga del archivo almacenado en Laravel
			// 			$downloadUrl = asset('factura_eca/' . $filename);
			// 			$adjunto = $downloadUrl;
			// 		}	
			// 		$data = [
			// 			'nombre'=>$name,
			// 			'logo_empresa'=>$empresaLogo,
			// 			'empresa'=> $correo->empresa,
			// 			'cdc'=> $documentos_ecos->cdc,
			// 			'factura'=> $documentos_ecos->factura->nro_factura,
			// 			'fecha'=> date('d/m/Y H:m:s', strtotime($documentos_ecos->factura->fecha_hora_facturacion))
			// 		];
			// }

			if($correo->plantilla != ""){
				$plantilla = $correo->plantilla;
			}else{
				$plantilla = "email";
			}
			
	 		try {
				if($email != ""){
					Mail::send('mail.'.$plantilla, $data, function($message) use ($nro_factura, $pdf, $email, $name,$asunto,$de,$empresa,$adjunto, $pdf_resumen) {
						$message->to($email, $name);
						//$message->to('spereira@git.com.py', $name);
						$message->from($de, $empresa); 

						if($adjunto !=""){
							$message->attach($adjunto);
						}	

						if($pdf_resumen !=""){
							$message->attachData($pdf_resumen->output(), "resumen_ejecutivo.pdf");
						}

						if($pdf !=""){
							$message->attachData($pdf->output(), "Factura N°".$nro_factura .".pdf");
						}
						$message->subject($asunto);
					}); 
					$estado= 'OK';
					$envio = 'N';
				}else{ 
					$estado= 'No existe remitente';
					$envio = 'P';
				}
			}catch (Exception $e) {
								$envio = 'P';
								$estado= $e->getMessage();
			}

		  	DB::table('notificaciones')->where('id', $correo->id)
									   ->update([
												'estado'=>$envio,
												'respuesta_envio'=>$estado,
												'fecha_hora_envio'=>date('Y-m-d H:m:s'),
												]);  
		}
		if($request->input('id') !== null){
			flash('Le enviamos un correo para el cambio de su contraseña')->success();
			return redirect()->route('recuperarContrasenha');
		}
		

	}

	public function excelData($idEmpresa)
	{

		$libroCompra1 = DB::select("SELECT * 
							FROM vw_libro_compra_anticipo 
							WHERE id_empresa = ".$idEmpresa."
							AND pagado_proveedor_txt = 'NO'
							AND saldo > 0
							AND pendiente = false
							AND fecha_pago_proveedor >= current_date-10
							AND fecha_pago_proveedor <= current_date+10
							order by fecha_pago_proveedor");

		$libroCompra2 = DB::select("SELECT *
							FROM vw_libro_compra_anticipo 
							WHERE id_empresa =".$idEmpresa."
							AND pagado_proveedor_txt = 'NO'
							AND saldo > 0
							AND pendiente = false
							AND check_in >= current_date-10
							AND check_in <= current_date+10
							order by check_in;");



		$resultados = array();
		foreach($libroCompra1 as $key=>$lista)
		{
			$resultados[$lista->id] = $lista;
		}
		foreach($libroCompra2 as $keys=>$listados)
		{
			$resultados[$listados->id] = $listados;
		}
		$cuerpo = array();
		$req = array();	
 		foreach($resultados as $key=>$lista)
		{
			if(isset($lista->fecha_de_gasto)){
				$date = explode('-', $lista->fecha_de_gasto);
				$fecha = $date[2]."/".$date[1]."/".$date[0];
			}else{  
				$fecha = '';
			}	
		 	array_push($cuerpo, 
								array($lista->proveedor_n,
									  $lista->fecha_proveedor_format,
									  $fecha,
									  $lista->nro_factura,
									  $lista->fecha_in_format,
									  $lista->tipo_doc_n,
									  $lista->pagado_proveedor_txt,
									  $lista->pasajero_n,
									  $lista->currency_code,
									  $lista->saldo,
									  $lista->cod_confirmacion,
									  $lista->denominacion_producto,
									  $lista->importe,
									  $lista->id_proforma,
									  $lista->fecha_facturacion_format,
									  $lista->vendedor_n,
									  $lista->origen
									)
					  );
		}  
		
		$excelNombre = 'pago_proveedor-'.date('d-m-Y');
		$export = new MailPagoProveedorExport($cuerpo);
		$excelFile = Excel::store($export, 'xls', public_path('excelPagoProveedor'));
		return $excelNombre;

	}

	public function proformaData($idProforma)
	{
		
		$proformas = Proforma::with('usuario', 'vendedor','cliente','asistente')
							->where('id',$idProforma)
							->first();
		return $proformas;	
	}

	public function getPasajero($id_factura = null, $id_proforma = null, $facturacion_parcial = false){

		$pasajero_nombre = '';
		$factura = Factura::find($id_factura);
		$empresa = Empresa::find($factura->id_empresa);
  
		//Si es preview proforma generamos un modelo para no cambiar nada abajo
		if($id_proforma){
		  $proforma = Proforma::select('pasajero_id')->find($id_proforma);
		  $factura = new Factura;
		  $factura->id_proforma = $id_proforma;
		  $factura->id_pasajero_principal = $proforma->pasajero_id;
		  $factura->pasajero_online = '';
		}
  
		if($facturacion_parcial){
		  if($factura->id_pasajero_principal){
			$pasajero_nombres= DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n
						FROM personas WHERE personas.id = ".$factura->id_pasajero_principal);
  
			if(isset($pasajero_nombres[0]->pasajero_n)){
				$pasajero_nombre = $pasajero_nombres[0]->pasajero_n;
			}
			
		  }
		}else if($empresa->pasajero_factura == 'TODOS'){
  
		  $pasajeros = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n
								  FROM personas 
								  WHERE id IN (
												SELECT pasajero FROM pasajeros_proformas WHERE id_proforma=".$factura->id_proforma."
												) LIMIT 3");
		  foreach ($pasajeros as $pasajero) {
			$pasajero_nombre.= $pasajero->pasajero_n.' ,';
		  }
  
		} else if($empresa->pasajero_factura == 'PRINCIPAL') {
		  if($factura->id_pasajero_principal){
			$pasajero_nombres= DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n
						FROM personas WHERE personas.id = ".$factura->id_pasajero_principal);
  
			if(isset($pasajero_nombres[0]->pasajero_n)){
				$pasajero_nombre = $pasajero_nombres[0]->pasajero_n;
			}
			
		  }
  
		}
		
		$pasajero_nombre = rtrim($pasajero_nombre, ',');
		return $pasajero_nombre;
  
	  }
  

	public function facturaAdjunto($idFactura){
			   $mensajeErr = "Error Desconocido";  
			   $imprimir = '4'; //Por defecto imprimir copias
			   $empresa = '0';
			   $totalFactura = '0';
			   $NumeroALetras = 'Cero';
			   $factura = array('');
			   $err = 0;
			   $idVendedorHash = '';
	 
				 $factura = Factura::with('cliente',
						   'pasajero',
						   'vendedorAgencia',
						   'vendedorEmpresa',
						   'tipoFactura',
						   'timbrado',
						   'proforma',
						   'currency')
						   ->where('id',$idFactura)->get();

				 $id_empresa = $factura[0]->id_empresa;

				 $empresa = Empresa::where('id',$id_empresa)->first();

				 $sucursalEmpresa =  Persona::where('id_empresa',$id_empresa)
										  ->where('id_tipo_persona', 21)
										  ->where('activo', true)
										  ->get();
	 
			   if(isset($factura[0]->pasajero['nombre'])){         
				 $pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id 
							 FROM personas WHERE personas.id = ".$factura[0]->id_pasajero_principal);
	 
			   }else{
				 $pasajero = "";
			   } 
			   if($pasajero == ""){
				 $pasjeroNombre = "";
			   }else{
				 $pasjeroNombre = $pasajero[0]->pasajero_n;
			   } 
	 
				//Los datos estan completos y no hay error entonces seguir          
			 if(!empty($factura) && $err == 0){
	 
			   $facturaDetalle = FacturaDetalle::with('producto')
						   ->where('id_factura',$idFactura)
						   ->get();  
			   $contadorDetalle= count($facturaDetalle); 
			   $vendedorEmpresa = Persona::where('id',$factura[0]->id_usuario)->get();    
	 
	 
			   $empresa = Empresa::where('id',$factura[0]->id_empresa)->first();
	 
	 
				 //saber si es bruto o neto
				 $totalFactura = DB::select('SELECT * FROM get_monto_factura('.$factura[0]->id.')');
				 $totalFactura = $totalFactura[0]->get_monto_factura;
					  
	 
				 $NumeroALetras = $this->convertir($totalFactura);
				 $estadoFactura = $factura[0]->id_estado_factura;
	 
	 
				 //Imprimir factura anulada
				  if($estadoFactura == $this->facturaAnulada) {
				   $imprimir = '3';
			   
				 } 
	 
				 } else {
	 
					// DEVOLVER ERROR O IMPRIMIR UNA PAGINA DE ERROR*/
				   abort(404);
				 }
	 
				 if(isset($factura[0]->ventaRapida->id_comercio_empresa)){
				   $idComercio = $factura[0]->ventaRapida->id_comercio_empresa; 
				 }else{
				   $idComercio = 0;
				 }
	 
				 $comercioPersona= ComercioPersona::where('id', $idComercio)->get(['logo']);
	 
				 if(isset($comercioPersona[0]->logo)){
				   $logo = $comercioPersona[0]->logo;
				 }else{
				   $logo = asset("logoEmpresa/$empresa->logo");
				 }

				 $tipoFacturas = TipoTimbrado::where('id',$factura[0]->timbrado->id_tipo_timbrado)->first();  
				 if(isset($tipoFacturas->denominacion)){
					 $invoceFactura = strtoupper($tipoFacturas->denominacion);
				 }else{
					 $invoceFactura = "";
				 }
				 $pasajeroNombre = $this->getPasajero($factura[0]->id, null, $factura[0]->parcial);
				 $pdf = \PDF::loadView('pages.mc.factura.facturaEsqueleto',
				   compact('factura','imprimir','facturaDetalle','empresa','totalFactura','NumeroALetras','vendedorEmpresa','sucursalEmpresa','pasjeroNombre', 'logo','contadorDetalle','pasajeroNombre','invoceFactura'));
	 
				return $pdf;   
	    }

    private function formatoFechaSalida($date){
        if( $date != ''){

        $date = explode('-', $date);
            $fecha = $date[2]."/".$date[1]."/".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }

	public function resumenEjecutivo($empresa, $fecha_test = null){

		/**
		 * Vamos a dividir los ingresos en documento y valores
		 * Vamos a poner las transferencia en valores
		 */

		 $fecha_actual = date('Y-m-d');

        if($fecha_test != null){
			$fecha_actual = $fecha_test;
		}
       
		$pdf = '';
       
       try {
           
                    //    $fecha_actual = '2023-03-20';
                      //Debe recorrer cada empresa para poder generar el cierre de caja
                      //Generar pdf resumen de cierre de caja y enviar por correo
                      $cierreCajas = DB::table('vw_historico_cierre_caja');
                      $cierreCajas = $cierreCajas->where(DB::raw('DATE(created_at)'),$fecha_actual);
                      $cierreCajas = $cierreCajas->where('id_empresa',$empresa->id);
                      $cierreCajas = $cierreCajas->get();

                      

                      if(count($cierreCajas)){
                          foreach ($cierreCajas as $cierre) {
                              $cierreCajaDetalles = DB::table('vw_historico_resumen_cierre_caja');
                              $cierreCajaDetalles = $cierreCajaDetalles->where('id_historico_cierre_caja_cabecera',$cierre->id);
                              $cierreCajaDetalles = $cierreCajaDetalles->get();
                              $cierre->detalles = $cierreCajaDetalles;
                          }
                      }




                    //Cuentas no bancarias activas
                      $bancos_1 = new HistoricoSaldoBanco;
                      $bancos_1 = $bancos_1->select(
                                          DB::raw("to_char(created_at, 'DD/MM/YYYy') as fecha_data"),
                                          DB::raw('saldo as saldo_detalle'),
                                          DB::raw('nombre as banco_n'),
                                          'numero_cuenta',
                                          'currency_code',
                                          DB::raw('tipo_cuenta as tipo_cuenta_banco_n'),
                                          'id_banco_cabecera',
                                          DB::raw("to_char(created_at,'DD/MM/YYYY') as fecha_data"),
                                          DB::raw("'historial' as tipo_data")
                                        );
                      $bancos_1 = $bancos_1->where(DB::raw('DATE(created_at)'),$fecha_actual);
                      $bancos_1 = $bancos_1->where('id_empresa',$empresa->id);
                      $bancos_1 = $bancos_1->where('cuenta_bancaria',false);
					  $bancos_1 = $bancos_1->where('activo',true);
					  $bancos_1 = $bancos_1->where('saldo','<>',0);
					  $bancos_1 = $bancos_1->orderByRaw('nombre, currency_code ASC')->get();
						

					//Cuenta Corriente
					  $bancos_2 = new HistoricoSaldoBanco;
                      $bancos_2 = $bancos_2->select(
                                          DB::raw("to_char(created_at, 'DD/MM/YYYy') as fecha_data"),
                                          DB::raw('saldo as saldo_detalle'),
                                          DB::raw('nombre as banco_n'),
                                          'numero_cuenta',
                                          'currency_code',
                                          DB::raw('tipo_cuenta as tipo_cuenta_banco_n'),
                                          'id_banco_cabecera',
                                          DB::raw("to_char(created_at,'DD/MM/YYYY') as fecha_data"),
                                          DB::raw("'historial' as tipo_data")
                                        );
                      $bancos_2 = $bancos_2->where(DB::raw('DATE(created_at)'),$fecha_actual);
                      $bancos_2 = $bancos_2->where('id_empresa',$empresa->id);
					  $bancos_2 = $bancos_2->where('cuenta_bancaria',true);
					  $bancos_2 = $bancos_2->where('activo',true);
					  $bancos_2 = $bancos_2->where('saldo','<>',0);
					  $bancos_2 = $bancos_2->where('tipo_cuenta','Cuenta Corriente');
					  $bancos_2 = $bancos_2->orderByRaw('nombre, currency_code ASC')->get();

					  //Cuenta No corriente y no operativas
					  $bancos_2_1 = new HistoricoSaldoBanco;
					  $bancos_2_1 = $bancos_2_1->select(
					  					DB::raw("to_char(created_at, 'DD/MM/YYYy') as fecha_data"),
					  					DB::raw('saldo as saldo_detalle'),
					  					DB::raw('nombre as banco_n'),
					  					'numero_cuenta',
					  					'currency_code',
					  					DB::raw('tipo_cuenta as tipo_cuenta_banco_n'),
					  					'id_banco_cabecera',
					  					DB::raw("to_char(created_at,'DD/MM/YYYY') as fecha_data"),
					  					DB::raw("'historial' as tipo_data")
					  				  );
					  $bancos_2_1 = $bancos_2_1->where(DB::raw('DATE(created_at)'),$fecha_actual);
					  $bancos_2_1 = $bancos_2_1->where('id_empresa',$empresa->id);
					  $bancos_2_1 = $bancos_2_1->where('cuenta_bancaria',false);
					  $bancos_2_1 = $bancos_2_1->where('activo',true);
					  $bancos_2_1 = $bancos_2_1->where('saldo','<>',0);
					  $bancos_2_1 = $bancos_2_1->where('tipo_cuenta','<>','Cuenta Corriente');
					  $bancos_2_1 = $bancos_2_1->where('tipo_cuenta','<>','Cuenta Operativa');
					  $bancos_2_1 = $bancos_2_1->orderByRaw('nombre, currency_code ASC')->get();
										
					  $egresos = DB::select("SELECT * FROM (SELECT 
					  bc.nombre as cuenta,
					  cu.currency_code as moneda,
					  sum(fpod.importe_pago) as total,
					  fpod.id_banco_detalle,
					  fpoc.id_moneda,
					  bd.numero_cuenta,
					  'E' as tipo_data
					  from op_cabecera op_c
					  join forma_pago_op_cabecera fpoc ON fpoc.id_op = op_c.id
					  join forma_pago_op_detalle fpod ON fpod.id_cabecera = fpoc.id
					  join forma_pago_cliente fpc ON fpc.id = fpod.id_tipo_pago
					  join banco_detalle bd ON bd.id = fpod.id_banco_detalle
					  join banco_cabecera bc ON bc.id = bd.id_banco
					  join currency cu ON cu.currency_id = fpoc.id_moneda
					  where op_c.id_estado IN (53,50)
					  AND op_c.id_empresa = ?
					  AND op_c.activo = true
					  AND (
						  date(op_c.fecha_hora_autorizado) = ? OR 
						  date(op_c.fecha_hora_procesado)  = ?
					  )
					  group by bc.nombre,fpoc.id_moneda,cu.currency_code,fpod.id_banco_detalle, bd.numero_cuenta
					  UNION ALL
					  	SELECT 
						CONCAT('Extracción','-',b_destino_cab.nombre) AS cuenta,
						b_destino_currency.currency_code as moneda,
						tc.importe as total,
						b_destino.id,
						b_destino.id_moneda,
						b_destino.numero_cuenta,
						'T' as tipo_data
						FROM transferencias_cuentas tc
						LEFT JOIN banco_detalle b_destino ON b_destino.id = tc.id_cuenta_banco_destino
						LEFT JOIN banco_cabecera b_destino_cab ON b_destino_cab.id = b_destino.id_banco
						LEFT JOIN currency b_destino_currency ON b_destino_currency.currency_id = b_destino.id_moneda
						WHERE date(tc.fecha_hora_procesado) = ?
						AND tc.id_empresa = ? 
						AND tc.id_tipo_transferencia = 22 -- Extraccion 
						UNION ALL
						SELECT 
						CONCAT('Transferencia','-',b_origen_cab.nombre) AS cuenta,
						b_origen_currency.currency_code as moneda,
						tc.importe as total,
						b_origen.id,
						b_origen.id_moneda,
						b_origen.numero_cuenta,
						'T' as tipo_data
						FROM transferencias_cuentas tc
						LEFT JOIN banco_detalle b_origen ON b_origen.id = tc.id_cuenta_banco_origen
						LEFT JOIN banco_cabecera b_origen_cab ON b_origen_cab.id = b_origen.id_banco
						LEFT JOIN currency b_origen_currency ON b_origen_currency.currency_id = b_origen.id_moneda
						WHERE date(tc.fecha_hora_procesado) = ?
						AND tc.id_empresa = ?
						AND tc.id_tipo_transferencia = 1 -- Transferencias 
					  ) AS data 
					  ORDER BY tipo_data = 'E' DESC, cuenta DESC,  moneda DESC", [
						$empresa->id, 
						$fecha_actual, 
						$fecha_actual,
						$fecha_actual,
						$empresa->id,
						$fecha_actual,
						$empresa->id]);

					  
					//Obtener el total por cada moneda y guardarlo en un array 
					$egresos_totales_moneda = [];
					foreach ($egresos as $key => $value) {
						if(in_array($value->moneda, array_keys($egresos_totales_moneda))) {
							$egresos_totales_moneda[$value->moneda] = $egresos_totales_moneda[$value->moneda] + $value->total;
						} else {
							$egresos_totales_moneda[$value->moneda] = $value->total;
						}
					}

			

					$ingresos_valores = DB::select("SELECT 
					SUM(monto) as total,
					cr.moneda_id,
					cr.forma_cobro_id,
					fcc.denominacion AS forma_cobro,
					cu.currency_code,
					fcc.tipo
					FROM cierre_resumen cr
					JOIN forma_cobro_cliente fcc ON fcc.id = cr.forma_cobro_id
					JOIN currency cu ON cu.currency_id = cr.moneda_id
					WHERE id_cierre IN (
						SELECT 
							id
						FROM cierre_caja cc 
						WHERE cc.fecha_cierre = ?
						AND cc.id_empresa = ?
						AND cc.activo = true
					)
					AND fcc.tipo = 'VALORES'
					GROUP BY fcc.tipo, fcc.denominacion, cu.currency_code, cr.moneda_id, cr.forma_cobro_id
					ORDER BY fcc.denominacion, cu.currency_code DESC
					", [$fecha_actual, $empresa->id]);

					$total_ingreso_valores = DB::select("SELECT 
										SUM(monto) as total,
										cu.currency_code
										FROM cierre_resumen cr
										JOIN forma_cobro_cliente fcc ON fcc.id = cr.forma_cobro_id
										JOIN currency cu ON cu.currency_id = cr.moneda_id
										WHERE id_cierre IN (
											SELECT 
												id
											FROM cierre_caja cc 
											WHERE cc.fecha_cierre = ?
											AND cc.id_empresa = ?
											AND cc.activo = true
										)
										AND fcc.tipo = 'VALORES'
										GROUP BY cu.currency_code
										", [$fecha_actual, $empresa->id]);




					$ingresos_documento = DB::select("SELECT * FROM (SELECT 
								SUM(monto) as total,
								cr.moneda_id,
								cr.forma_cobro_id,
								fcc.denominacion AS forma_cobro,
								cu.currency_code,
								fcc.tipo
								FROM cierre_resumen cr
								JOIN forma_cobro_cliente fcc ON fcc.id = cr.forma_cobro_id
								JOIN currency cu ON cu.currency_id = cr.moneda_id
								WHERE id_cierre IN (
									SELECT 
										id
									FROM cierre_caja cc 
									WHERE cc.fecha_cierre = ?
									AND cc.id_empresa = ?
									AND cc.activo = true
								)
								AND fcc.tipo = 'DOCUMENTO'
								GROUP BY fcc.tipo, fcc.denominacion, cu.currency_code, cr.moneda_id, cr.forma_cobro_id
								UNION ALL
								SELECT 
								tc.importe_cotizado as total,
								b_destino.id_moneda as moneda_id,
								0 as forma_cobro_id,
								CONCAT('Transferencia','-',b_destino_cab.nombre,'-',b_destino.numero_cuenta) AS forma_cobro,
								b_destino_currency.currency_code,
								'DOCUMENTO' as tipo
								FROM transferencias_cuentas tc
								JOIN banco_detalle b_destino ON b_destino.id = tc.id_cuenta_banco_destino
								JOIN banco_cabecera b_destino_cab ON b_destino_cab.id = b_destino.id_banco
								JOIN currency b_destino_currency ON b_destino_currency.currency_id = b_destino.id_moneda
								WHERE date(tc.fecha_hora_procesado) = ?
								AND tc.id_empresa = ? 
								AND tc.id_tipo_transferencia IN (1,2,23) -- Transferencias, Deposito Recaudaciones y Deposito
									) as data 
								ORDER BY  forma_cobro, currency_code DESC", [$fecha_actual, $empresa->id,$fecha_actual, $empresa->id]);
								

					  $empresa_logo = 'https://gestur.git.com.py/logoEmpresa/'.$empresa->logo;					
                      $pdf = \PDF::loadView('pages.mc.pdfEmails.resumen_ejecutivo', 
					  compact('cierreCajas','egresos','ingresos','empresa','bancos_1','bancos_2','empresa_logo','bancos_2_1','ingresos_valores','ingresos_documento',
					  'total_ingreso_valores','egresos_totales_moneda'));
                      $pdf->setPaper('A4');
                      $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true,'enable_css_float'=>true]);

					if($fecha_test){
						//   Imprimir en la pantalla
						$pdf->setPaper('a4', 'letter')->setWarnings(false);
						return response($pdf->output(), 200)->header('Content-Type', 'application/pdf');
					}
                 

     
            		
           
                                 
      } catch (\Exception $e) {
          Log::error('GenerarPdfEjecutivo: Error en proceso: '.$e->getMessage());
          //Enviar correo de alerta
          $proceso = 'GenerarPdfEjecutivo';
          $mensaje = 'Error en proceso: '.$e->getMessage();
          Mail::to('ti@git.com.py')->send(new NotificacionesSistema($proceso, $mensaje));
      }

	  return $pdf;

    }

	private function setearNewConfig($correo){
		$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', $correo->empresa_id)->first();
		if(empty($mailEmpresa)){
			$mailEmpresa = EmpresaMail::with('empresa')->where('id_empresa', '=', 0)->first();
			$empresaArray = Empresa::where('id', $correo->empresa_id)->first();
			$de = $mailEmpresa->username;
			$empresa = $empresaArray->denominacion;
			$logoEmpresa = $empresaArray->logo;
		}else{
			$de = $mailEmpresa->username;
			$empresa = $mailEmpresa->empresa->denominacion;
			$logoEmpresa = $mailEmpresa->empresa->logo;
		}
		Config::set('mail.driver', $mailEmpresa->driver);
		Config::set('mail.host', $mailEmpresa->host);
		Config::set('mail.port', $mailEmpresa->port);
		Config::set('mail.encryption', $mailEmpresa->encryption);
		Config::set('mail.username', $mailEmpresa->username);
		Config::set('mail.password', $mailEmpresa->password); 
	}


}
