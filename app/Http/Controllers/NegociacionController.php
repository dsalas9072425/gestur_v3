<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use Redirect;
use App\ProductoCliente;
use App\Producto;
use App\Persona;
use App\PlazoPago;
use Response;
use Image; 
use DB;


class NegociacionController extends Controller
{

	private function getIdUsuario(){
	
	 return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

}//function

private function getIdEmpresa(){

  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

}//function

	
	public function index(){

		$producto = ProductoCliente::where('id_empresa',$this->getIdEmpresa())->get();
		$personasAgencia = Persona::where('id_tipo_persona','8')
						   ->where('id_empresa',$this->getIdEmpresa())
						   ->where('activo',true)
						   ->get();


		return view('pages.mc.negociacion.index')->with(['negociacion'=>$producto, 'personas'=>$personasAgencia]);
	}//function


	public function consultaNegociacion(Request $req){

		//$productoCliente = ProductoCliente::where('id_cliente',$req->idAgencia)->get();
		$producto = array();


		if($req->input('idAgencia') != ''){
			$producto = DB::select('SELECT p.frase_predeterminada, c.comision_pactada, per.nombre, per.denominacion_comercial 
				FROM productos as p 
				INNER JOIN productos_clientes as c ON p.id = c.id_producto INNER JOIN personas as per ON 
				per.id = c.id_cliente  
				where c.id_cliente ='.$req->input('idAgencia').' AND p.id_empresa ='.$this->getIdEmpresa());


	} else {
		$producto = DB::select('SELECT p.frase_predeterminada, c.comision_pactada, per.nombre, per.denominacion_comercial 
			FROM productos as p 
			INNER JOIN productos_clientes as c ON p.id = c.id_producto INNER JOIN personas as per ON 
			per.id = c.id_cliente
			WHERE p.id_empresa ='.$this->getIdEmpresa());
	}

	

		return json_encode(array('productos'=>$producto));

	}//function






	public function edit(Request $req){


		$idPersona = $req->input('id');
		// $productoCliente = ProductoCliente::where('id_cliente',$idPersona)->get();
		$persona = Persona::where('id',$idPersona)->first();
		$productos = Producto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->where('tipo_producto','P')->get();
		// $plazosPago = PlazoPago::where('id_empresa',$this->getIdEmpresa())
		// 			  ->get();

		

		 return view('pages.mc.negociacion.edit')->with(['productos'=>$productos,'persona'=>$persona]);

	}//function





	public function actualizarNegociacion(Request $req){
		// dd($req->all());
		$err = false;	
		$idPersona = $req->input('id');
		$idEmpresa = $this->getIdEmpresa();
		$insert = array();
		$cargarDatos = array();
		unset($req['id']);

		

		 foreach ($req->all() as $key => $value) {
		 	
		 //	if($value != 0){
		 	 $cargarDatos['id_producto'] = $key;
		     $cargarDatos['id_cliente'] = $idPersona;
		     $cargarDatos['comision_pactada'] = $value;
		     array_push($insert, $cargarDatos);
		    //	}*/
			 	
			}//foreach

		try{
			 DB::beginTransaction();
			 DB::table('productos_clientes')->where('id_cliente',$idPersona)->delete();
			 DB::table('productos_clientes')->insert($insert);

			$err = true;
		}catch(\Exception $e){
			DB::rollBack();
			$err = false;
		}

		if($err)
		DB::commit();


		return response()->json(array('resp'=>$err));
	}//function







	public function getDatosNegociacion(Request $req){

		$idPersona = $req->input('id');
		// $productoCliente = ProductoCliente::where('id_cliente',$idPersona)->get();

		$productos = DB::select("SELECT p.id,
										p.denominacion,
										p.frase_predeterminada, 
									   c.comision_pactada, 
									   per.nombre, 
									   per.denominacion_comercial 
			FROM productos as p 
			INNER JOIN productos_clientes as c ON p.id = c.id_producto 
			INNER JOIN personas as per ON per.id = c.id_cliente  
			where c.id_cliente =".$idPersona." AND p.activo = true AND p.tipo_producto='P' AND p.id_empresa =".$this->getIdEmpresa());


		return response()->json(array('datos'=>$productos));

	}








}
