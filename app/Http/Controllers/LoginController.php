<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use App\Mail\NotificacionesMail;
use Redirect;
use App\Usuario;
use App\Empresa;
use App\TipoPersona;
use App\MenuTipoPersona;
use App\MenuEmpresa;
use App\EmpresaPersona;
use App\Persona;
use App\ReservaNemo;
use App\ReservaNemoHabitacion;
use App\ReservaNemoPasajero;
use App\Traslado;
use App\DetalleTraslado;
use App\ReservaNemoActividad;
use App\ProformasDetalle;
use App\Factura;
use App\Notificacion;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class LoginController extends Controller
{
	private function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}

	/*
	Pagina de Inicio del Login
	*/
    public function index(Request $req){
		if($req->session()->has('datos-loggeo')){
			return redirect()->route('home');
		}

		$ssl   = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on';
		$proto = strtolower($_SERVER['SERVER_PROTOCOL']);
		$proto = substr($proto, 0, strpos($proto, '/')) . ($ssl ? 's' : '' );
		if($_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI'] == 'localhost/factour-v2/public/login'){
			$indicador = 0;
		}else{
			$indicador = 1;
		}

		return view('pages.login')->with('indicador',$indicador);
	}
	/*
	Función de Redirección de Login
	*/
	public function doLogin(Request $req){
///////////////////////////////////////////////////////////////////////////////////////////
		$idOrigen = 4;
		$ip = \Request::getClientIp(true);
		/*echo '<pre>';
		print_r('select login_('.$req->input('usuario').','.$req->input('password').','.$idOrigen.','.$this->getId4Log().','.$ip.')');
		/*die;-*/
		$respuesta_login = DB::select('select login_(?,?,?,?,?)', [$req->input('usuario'),$req->input('password'),$idOrigen,$this->getId4Log(),$ip]);
		if(isset($respuesta_login[0]->login_)){ 
			$datosBase = explode(',', $respuesta_login[0]->login_);
			if($datosBase[1] == 0){ 
				$persona = Persona::where('id', '=',$datosBase[12])->first(['facturacion_automatica_en_gastos','puede_reservar','puede_reservar_en_gastos']);
				$datosLoggeo = new \StdClass;
				$datosUsuarios = new \StdClass;
				$datos = new \StdClass;
				$permisos = new \StdClass;
				$datosUsuarios->idUsuario =  $datosBase[12];
				$datosUsuarios->nombreApellido = preg_replace('([^A-Za-z0-9 ])', '', $datosBase[13]);
				$datosUsuarios->usuario = $datosBase[14];
				$datosUsuarios->idPerfil =  $datosBase[4];
				$datosUsuarios->idAgencia = $datosBase[16];
				$datosUsuarios->logoAgencia =  $datosBase[10];
				$datosUsuarios->idSucursalAgencia =  $datosBase[6];
				$datosUsuarios->desSucursal =  "";
				$datosUsuarios->email =  $datosBase[8];
				$datosUsuarios->codAgente =  $datosBase[20];
				$datosUsuarios->nomAgente =  str_replace(')','',$datosBase[21]);
				$datosUsuarios->lineaCredito =  $datosBase[17];
				$datosUsuarios->comisionAgencia =  $datosBase[18];
				$datosUsuarios->idEmpresa = $datosBase[19];
				$datosLoggeo->token =  str_replace('(','',$datosBase[0]);
				$datosLoggeo->codRetorno =  $datosBase[1];
				$datosLoggeo->desRetorno = $datosBase[2];
				$datos->datosUsuarios = $datosUsuarios;
				if(isset($persona->facturacion_automatica_en_gastos)){ 
					$facturacion_automatica_en_gastos = $persona->facturacion_automatica_en_gastos;
				}else{
					$facturacion_automatica_en_gastos = false;
				}
				if(isset($persona->puede_reservar)){ 
					$puede_reservar = $persona->puede_reservar;
				}else{
					$puede_reservar = false;
				}
				if(isset($persona->puede_reservar_en_gastos)){ 
					$puede_reservar_en_gastos = $persona->puede_reservar_en_gastos;
				}else{
					$puede_reservar_en_gastos = false;
				}
				$permisos->facturacion_automatica_en_gastos = $facturacion_automatica_en_gastos;
				$permisos->puede_reservar = $puede_reservar;
				$permisos->puede_reservar_en_gastos =  $puede_reservar_en_gastos;
				$datos->permisos = $permisos;
				$empresa = Empresa::where('id', '=', $datosBase[19])->first(['logo','id','denominacion','id_plan_sistema','tipo_impresion','tipo_empresa']);
				$idUsuario = $datosBase[12];

				$listado = DB::select('select * from empresa_persona where id_persona = '.$idUsuario
								);
				$listadoEmpresa = EmpresaPersona::with('empresas')->where('id_persona', $idUsuario)->get();
				$empresas = array();
				if(isset($empresa->id)){
					$empresas[$empresa->id]['id'] = $empresa->id;
					$empresas[$empresa->id]['denominacion'] = $empresa->denominacion;
					foreach($listadoEmpresa as $key=>$listado){
						$empresas[$listado->empresas->id]['id'] = $listado->empresas->id;
						$empresas[$listado->empresas->id]['denominacion'] = $listado->empresas->denominacion;		
					}
				}
				$datos->datosUsuarios->tipo_impresion = $empresa->tipo_impresion;
				$datos->datosUsuarios->logoEmpresa = $empresa->logo;
				$datos->empresas = $empresas;
				$datos->datosUsuarios->id_plan_sistema = $empresa->id_plan_sistema;
				$datos->datosUsuarios->tipo_empresa = $empresa->tipo_empresa;
				$datosLoggeo->datos = $datos;
				Session::put('datos-loggeo',$datosLoggeo);
				$this->getMenuJson($req);
				$mensaje =  new \stdClass();
				$mensaje->codRetorno = $datosBase[1];
				$mensaje->desRetorno = $datosBase[2];
				$mensaje->idPerfil = $datosBase[4]; 
			}else{
				$mensaje =  new \stdClass();
				$mensaje->codRetorno = 'ERROR';
				$mensaje->desRetorno = $datosBase[2];
			}	
		}else{
			$mensaje =  new \stdClass();
			$mensaje->codRetorno = 'ERROR';
			$mensaje->desRetorno = 'Error de Conexión';

		}	
		return response()->json($mensaje);
	}
	
	/*
	Pagina de Inicio del Login
	*/
    public function indexMc(Request $req){
		if($req->session()->has('datos-loggeo')){
			return redirect()->route('home');
		}
		return view('pages.mc.login');
	}
	/*
	Función de Redirección de Login
	*/
	public function doLoginMc(Request $req){
		$iibReq = new \StdClass;
		$client = new Client();
		$iibReq->usuario = $req->input('usuario');
		$iibReq->password = $req->input('password');
		try{
			$iibRsp = $client->post(config('config.iibLogin'), [
				'json' => $iibReq
			]);
		}
		catch(RequestException $e){
		}
		catch(ClientException $e){
		}	

		
		$iibRsp = $iibRsp->getBody();
		$iibObjRsp =  json_decode($iibRsp);
		$promo = Usuario::with('agencia','sucursal','perfil')
						->where('usuario', '=', $req->input('usuario'))->get();
		Session::put('tiene_promo',$promo[0]->tiene_promo);
		$iibObjRsp->datos->datosUsuarios->idPais = $promo[0]->agencia->cod_pais;
		$iibObjRsp->datos->datosUsuarios->tokenBk = md5($req->input('usuario'));

		if($iibObjRsp->codRetorno ==0){ 
			Session::put('datos-loggeo',$iibObjRsp);
		}
		return $iibRsp;
	}
	/*
	Función de Salida de la Página
	*/
	public function logout(Request $req){
		$req->session()->flush();
		if($req->has('servicios') && $req->input('servicios') == 'true'){
			try{
				
				//$client = new Client();
				$client = new \GuzzleHttp\Client(['verify' => false]);
			
				//$midRsp = $client->get(Config::get('config.trasladosLogout'));
				$midRsp = $client->get('https://servicios.dtpmundo.com/es/login/logout');
				//dd($midRsp);
				//$midObjRsp = json_decode($midRsp->getBody());
				
				//return json_encode($midObjRsp);
			}catch(Exception $e){
				
			}
		}
		return redirect('login');
	}
	public function logoutMc(Request $req){
		$req->session()->flush();
		if($req->has('servicios') && $req->input('servicios') == 'true'){
			try{
				//$client = new Client();
				$client = new \GuzzleHttp\Client(['verify' => false]);
			
				//$midRsp = $client->get(Config::get('config.trasladosLogout'));
				$midRsp = $client->get('https://servicios.dtpmundo.com/es/login/logout');
				//dd($midRsp);
				//$midObjRsp = json_decode($midRsp->getBody());
				
				//return json_encode($midObjRsp);
			}catch(Exception $e){
				
			}
		}
		return redirect('mc/login');
	}


	private function getMenuJson(Request $request)
	{
		$arrayMenu = [];
		$urlRestringidas = ['liquidar_dtp_puntos.index','liquidar_dtp_puntos.reporte_puntos','liquidar_dtp_puntos.excel','liquidar_dtp_puntos.ver_liquidacion','buscarReservaCangoroo','dtplus.index'];
		$idTipoPersona = Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil;
		$idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		$menuEmpresas = Empresa::with('planSistema', 'planSistema.menus', 'planSistema.menus.submenus')->where('id', $idEmpresa)->get();
		
		if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'V'){
			$menusExcluidos = array(1,5);
		}else{
			$menusExcluidos = array(155);
		}

		foreach ($menuEmpresas[0]->planSistema->menus as $key => $menu) 
		{
			if ($menu->pivot->id_tipo_persona == $idTipoPersona) 
				{
					$arrayMenu['menus'][$menu->orden][$menu->id]['descripcion'] = $menu->descripcion;
					$arrayMenu['menus'][$menu->orden][$menu->id]['url'] = $menu->url;
					$arrayMenu['menus'][$menu->orden][$menu->id]['icono'] = $menu->icono;
					$arrayMenu['menus'][$menu->orden][$menu->id]['visible'] = $menu->visible;
					$arrayMenu['menus'][$menu->orden][$menu->id]['activo'] = $menu->activo;
					
				}
				$arraySubmenu = [];

			foreach ($menu->submenus as $key1 => $submenu) 
				{

					//Validar que existe la url para no llamar
					if (!Route::has($submenu->url)) {
						continue;
					}

					//Si es una url restringida y no es empresa 1 entonces no mostrar
					if(in_array($submenu->url, $urlRestringidas)){
						if($idEmpresa == 1){
							$arraySubmenu[$submenu->orden]['descripcion'] = $submenu->descripcion;
							$arraySubmenu[$submenu->orden]['url'] = $submenu->url;
							$arraySubmenu[$submenu->orden]['icono'] = $submenu->icono;
							$arraySubmenu[$submenu->orden]['visible'] = $submenu->visible;
							$arraySubmenu[$submenu->orden]['activo'] = $submenu->activo;
							ksort($arraySubmenu);
						}
					
					} else {
						$arraySubmenu[$submenu->orden]['descripcion'] = $submenu->descripcion;
						$arraySubmenu[$submenu->orden]['url'] = $submenu->url;
						$arraySubmenu[$submenu->orden]['icono'] = $submenu->icono;
						$arraySubmenu[$submenu->orden]['visible'] = $submenu->visible;
						$arraySubmenu[$submenu->orden]['activo'] = $submenu->activo;
						ksort($arraySubmenu);
					}
				
				}
			$arrayMenu['menus'][$menu->orden][$menu->id]['submenu'] = $arraySubmenu;
		}

		foreach($arrayMenu['menus'] as $key=>$valores){
			$indicador = 0;
			foreach($valores as $key2=>$menus ){
				foreach($menusExcluidos as $valor){
					if((int)$valor == (int)$key2){
						unset($arrayMenu['menus'][$key][$key2]);
					}
				} 
			}
		}
		ksort($arrayMenu['menus']);

		//Session::put('arrayMenu',$arrayMenu['menus']);
	 	$request->session()->put('arrayMenu', $arrayMenu['menus']);
	}

	public function modificarDatos(Request $request)
	{
		$mensajes = new \StdClass;
		$empresa = Empresa::where('id', '=', $request->input('id'))->first(['id']);
		$baseSession = Session::get('datos-loggeo');
		$baseSession->datos->datosUsuarios->idEmpresa = $empresa->id;
		Session::put('datos-loggeo', $baseSession);
		$this->getMenuJson($request);
		$mensajes->status = 'OK';
		$mensajes->mensaje = 'Se ha seleccionado la empresa para el usuario';
		return response()->json($mensajes);
	}
	public function contrasenha(Request $request)
	{
		return view('pages.mc.recuperar');
	}

	public function doRecuperar(Request $request)
	{
		$persona = DB::select("SELECT 							
								tp.puede_loguearse,
								p.denominacion des_perfil, u.nombre || ' ' || u.apellido nombre_apellido, u.usuario, u.password, 
								p.activo,
								u.fecha_validez, u.id id_usuario, u.id_persona id_agencia, u.id_sucursal_empresa id_sucursal_agencia, 
								u.id_tipo_persona id_perfil, a.nombre razon_social, a.telefono, u.email, u.logo, 
								REPLACE( a.denominacion_comercial, ',', '') descripcion_agencia, 1 as agencia_dtp, 0 linea_credito,
								0 as comision_pactada, u.id_empresa p_out_id_empresa, g.id id_agente, g.nombre || ' ' || u.apellido agente
								FROM  personas u, personas a, personas s, tipo_persona p, personas g, empresas e,tipo_persona tp
								where u.id_persona = a.id
								and u.id_sucursal_empresa = s.id
								and u.id_tipo_persona = p.id
								and u.id_vendedor_empresa = g.id
								and u.usuario = '".$request->input('email')."'
								and u.activo = true
								and u.activo_dtpmundo = true
								and a.activo_dtpmundo = true
								and u.id_empresa = e.id
								and tp.id = u.id_tipo_persona
								and u.id_tipo_persona IN (5,4,2,6,3,7,5)
								and e.activo = true
								and tp.puede_loguearse = true");
		if(!empty($persona)){
			$fecha = date_create(date('Y-m-d H:m:s'));
			date_add($fecha, date_interval_create_from_date_string("1 day"));
			$fecha_validaz = date_format($fecha,"Y-m-d H:m:s");
			$token = md5($request->input('email')." ".date('Y-m-d H:m:s'));
			DB::table('personas')->where('id', $persona[0]->id_usuario)
			->update([
					   'token_mail'=> $token,
					   'token_validez'=> $fecha_validaz,
					 ]); 

		    $getNotificacion= DB::select("SELECT public.insert_notificacion(".$persona[0]->id_usuario.",'".$request->input('email')."', 'recuperar', '".$token."',3,null)");
			return redirect()->route('notificar', ['id' => $getNotificacion[0]->insert_notificacion]);
		}else{
			flash('No existe un usuario con ese correo. Intentelo nuevamente !!')->error();
			return Redirect::back();
		}
		
	}

	public function actualizarPass(Request $request)
	{
		return view('pages.mc.actualizarPass')->with('tokenEmail',$request->input('tokenEmail'));
	}

    public function activarPassword(Request $request){
		$mensajes = new \StdClass;
		$personas = Persona::where('token_mail',$request->input('tokenEmail'))
					->where('activo','true')
					->orderBy('nombre', 'ASC')->first();
		
		/*echo '<pre>';
		print_r($personas);
		
		die;*/
        if(!empty($personas->id)){
        	if(!ctype_space($request->input('pass'))){
	       		//$password = md5(Config::get('constants.semilla_left') ."".$request->input('password')."".Config::get('constants.semilla_right'));
	         	DB::table('personas')
					            ->where('id_usuario',$personas->id)
					            ->update([
					            			'token_mail'=>null,
											]);
				DB::select('select get_password_hash(?,?)', [$request->input('password'),$personas->id]);
	    		Session::flush();
				$mensajes->status = 'OK';
				$mensajes->mensaje = 'Su contraseña se ha actualizado, ya puede ingresar al sistema.';
				return response()->json($mensajes);	    	
			}else{
				$mensajes->status = 'ERROR';
				$mensajes->mensaje = 'La contraseña no puede estar compuesta por espacios en blanco';
				return response()->json($mensajes);	    	
	    	}
		}else{
			$mensajes->status = 'ERROR';
			$mensajes->mensaje = 'El token no exite.! Intentelo Nuevamente';
			return response()->json($mensajes);	    	
		}	
	}

	public function loginNemo(Request $request){
		$configuracion_nemo = DB::select("SELECT * FROM nemo_credenciales WHERE activo =  true");
		foreach($configuracion_nemo as $key=>$valores){
			echo '<pre>';
			print_r($valores->usuario);
			$login = $valores->usuario;
			$password = $valores->contrasenha;       
			$request_uri = $valores->url_busqueda;  
			$xml_body = '<BookingsQueryRQ>
							<GeneralParameters>
								<PreferedLanguage LanguageCode="es-AR"/>
							</GeneralParameters>
							<Details>
								<Criterion>
									<CreationDate> 
										<DateFrom>2023-11-11</DateFrom>
										<DateTo>2024-12-31</DateTo>
									</CreationDate>
									<DetailLevel>full</DetailLevel>
									<LastChangeDate>
										<DateFrom>2023-11-02T00:00:00</DateFrom>
										<DateTo>2024-12-30T23:59:59</DateTo>
									</LastChangeDate>
								</Criterion>
							</Details>
						</BookingsQueryRQ>';
			$client = new Client(['auth' => [$login, $password]]);
			$response = $client->request('POST', $request_uri, [
							'headers' => [
								'Accept' => 'application/xml'
							],
							'body'   => $xml_body
							])->getBody()->getContents();
			$xml = simplexml_load_string($response);
			$json_response = json_encode($xml);
			$json = json_decode($json_response); 
			$xmlobj = @new \SimpleXMLElement($response);
			foreach($json->Details->Bookings->Booking as $key=>$bookings){
				foreach($xmlobj->Details->Bookings->Booking[$key]->Price->children() as $precio){
					$moneda = json_encode($precio->attributes()->{'CurrencyCode'}); 
					$monedaBase = explode(':',$moneda);
					$moneda = substr($monedaBase[1], 1, 3);
					if($moneda == 'USD'){
						$moneda_id = 143;
					}
					if($moneda == 'EUR'){
						$moneda_id = 43;
					}
					if($moneda == 'PYG'){
						$moneda_id = 111;
					}
				}

				if(!is_array($bookings->BookingReferences->BookingReference)){
					$booking_id =$bookings->BookingReferences->BookingReference;
				}else{ 
					$booking_id =$bookings->BookingReferences->BookingReference[0];
				}
				$reserva_detalle = json_decode($this->detallesReserva($booking_id,$bookings, $valores->tid));
				
				echo '<pre>';
				print_r($booking_id);
			//	try {
					$code_supplier = "";
					if(isset($reserva_detalle->booking_supplier_code->{0})){
						$code_supplier = $reserva_detalle->booking_supplier_code->{0};
					}

					$deadline = null;
					if(isset($reserva_detalle->booking_deadline->{0})){
						/*if($booking_id == 'PH_1MSJV'){
							echo '<pre>';
							print_r('DEADLINeZX '. $reserva_detalle->booking_deadline->{0}); 
						}*/
						$posicion_coincidencia = strpos($reserva_detalle->booking_deadline->{0}, '/');
						if ($posicion_coincidencia === false) {
							$deadline = date("Y-m-d",strtotime($reserva_detalle->booking_deadline->{0}));
						}else{
							$deadline = $this->formatoFechaEntrada(trim($reserva_detalle->booking_deadline->{0}));
						}	
					}
					/*echo '<pre>';
					print_r('DEADLINe '. $deadline);*/
					$booking_checkin = null;
					if(isset($reserva_detalle->booking_checkin->{0})){
						$booking_checkin = date('Y-m-d', strtotime($reserva_detalle->booking_checkin->{0}));
					}else if(isset($reserva_detalle->booking_checkin)){
						$booking_checkin = date('Y-m-d', strtotime($reserva_detalle->booking_checkin));
					}

					$booking_checkout = null;
					if(isset($reserva_detalle->booking_checkout->{0})){
						$booking_checkout = date('Y-m-d', strtotime($reserva_detalle->booking_checkout->{0}));
					}else if(isset($reserva_detalle->booking_checkout)){
						$booking_checkout = date('Y-m-d', strtotime($reserva_detalle->booking_checkout));
					}

					$tipo_notificacion = 0;
					$estado = 0;
					if($reserva_detalle->booking_state_code == 'NMO.GBL.BST.CAN' || $reserva_detalle->booking_state_code == 'NMO.HTL.BST.CAN'  || $reserva_detalle->booking_state_code == 'NMO.GBL.BST.RQF'){
						$tipo_notificacion = 2;
						$estado = 1; //cancelado
					}	
					if($reserva_detalle->booking_state_code == 'NMO.GBL.BST.CNF'|| $reserva_detalle->booking_state_code == 'NMO.HTL.BST.CNF'){
						$tipo_notificacion = 1;
						$estado = 2;//confirmado
					}	
					if($reserva_detalle->booking_state_code == 'NMO.GBL.BST.PEN' || $reserva_detalle->booking_state_code == 'NMO.GBL.BST.PBC'){
						$tipo_notificacion = 18;
						$estado = 3; //pendiente
					}	
					if($reserva_detalle->booking_state_code == 'NMO.GBL.BST.CNP'){
						$tipo_notificacion = 19;
						$estado = 4;//Pendiente de cancelación
					}	
					if($reserva_detalle->booking_state_code == 'NMO.GBL.BST.COD'){
						$tipo_notificacion = 20;
						$estado = 5;//Denegada
					}	

					$fecha_gasto_inicio = '';
					$fecha_gasto_final = '';

						if(!empty($reserva_detalle->chargue_conditions)){
							foreach($reserva_detalle->chargue_conditions as $key=>$conditions){
								$fecha = '';
								$fecha2 = '';
								$dias = 0;
								$dias2 = 0;
								$monto =0;
								$monto_gasto = 0;
								$diaTo = 0;
								$BeginD = "";
								$EndD = "";
								if(is_array($conditions->ChargeCondition)){
									foreach($conditions->ChargeCondition as $key=>$condicion){	
											//if(){
												if($condicion->{"@attributes"}->BeginD == ""){
													$inicio = date("Y-m-d",strtotime($booking_checkin."- 7 days"));
													$expire_dt = new \DateTime($inicio);
													$today_dt = new \DateTime(date('Y-m-d'));
													if($today_dt > $expire_dt){
														$BeginD = date('Y-m-d');		
													}else{ 
														$BeginD = $inicio;
													}
												}else{
													$BeginD = $condicion->{"@attributes"}->BeginD;
												}

												if($condicion->{"@attributes"}->EndD == ""){
													$inicio = date("Y-m-d",strtotime($booking_checkin."- 7 days"));
													$expire_dt = new \DateTime($inicio);
													$today_dt = new \DateTime(date('Y-m-d'));
													if($today_dt > $expire_dt){
														$EndD = date('Y-m-d');		
													}else{ 
														if($condicion->{"@attributes"}->NettForMarketer > 0){
															$EndD = $inicio;
														}
													}
												}else{
													if($condicion->{"@attributes"}->NettForMarketer > 0){
														$EndD = $condicion->{"@attributes"}->EndD;
													}
												}

												$date1 = new \DateTime(date('Y-m-d'));
												$date2 = new \DateTime($BeginD);
												$diff = $date1->diff($date2);

												$date3 = new \DateTime($EndD);
												$diff2 = $date1->diff($date3);

												$montoGross = $condicion->{"@attributes"}->Gross;
												$diasTo = $condicion->{"@attributes"}->DaysTo;

												if($key == 0){
													if($condicion->{"@attributes"}->NettForMarketer > 0){
														$dias = $diff->days;
														$fecha = $BeginD;
													}	
												}else{ 
													if($dias > $diff->days){
														$dias = $dias;
														$fecha = $fecha;
													}else{	
														if($condicion->{"@attributes"}->NettForMarketer > 0){
															$dias = $diff->days; 
															$fecha = $BeginD;
														}
													}	
												}	
											/*	if($booking_id == 'PH_1Q7R0'){
													echo '<pre>';
													print_r($condicion);
													echo '<pre>';
													print_r('fecha  '.$fecha);

												}  */

												if($key == 0){
													$dias2 = $diff2->days;
													$fecha2 = $EndD;
												}else{ 
													if($dias2 < $diff2->days){
														$dias2 = $diff2->days; 
														$fecha2 = $EndD;
													}else{	
														$dias2 = $dias2;
														$fecha2 = $fecha2;
													}	
												}	
												if($montoGross > $monto_gasto){
													$monto_gasto = $montoGross;
												}	

												if($diasTo > $diaTo){
													$diaTo = $diasTo;
												}	
											//}
									}

									if($fecha != "" && $diaTo != 0 ){
										
										$inicio = date("Y-m-d",strtotime($booking_checkin."- ".$diaTo." days"));
										$fecha = $inicio;
									}	

									$fecha_gasto_inicio = $fecha;
									$fecha_gasto_final = $fecha2;
									$monto_gasto =  $monto_gasto;
								}else{
									if($conditions->ChargeCondition->{"@attributes"}->BeginD == ""){
											$fecha_actual = $deadline;
											$fecha_gasto_inicio = date("Y-m-d",strtotime($fecha_actual."- 7 days"));
										}else{ 
											$fecha_gasto_inicio = $conditions->ChargeCondition->{"@attributes"}->BeginD;
										}
									if($conditions->ChargeCondition->{"@attributes"}->EndD == ""){
										$fecha_actual = $deadline;
										$fecha_gasto_inicio = date("Y-m-d",strtotime($fecha_actual."- 7 days"));
									}else{ 
										$fecha_gasto_final = $conditions->ChargeCondition->{"@attributes"}->EndD;
									}
									$monto_gasto =  $conditions->ChargeCondition->{"@attributes"}->Gross;
									if($conditions->ChargeCondition->{"@attributes"}->BeginD == ""){
										$diaTo = $conditions->ChargeCondition->{"@attributes"}->DaysTo;
										$inicio = date("Y-m-d",strtotime($booking_checkin."- ".$diaTo." days"));
										$fecha_gasto_inicio = $inicio;
									}
								}	
							}
				
						}else{
							$fecha_actual = $deadline;
							//resto 1 día
							$fecha_gasto_inicio = date("Y-m-d",strtotime($fecha_actual."+ 7 days")); 
							$fecha_gasto_final = null;
							if(isset($reserva_detalle->booking_price_neto->{0})){
								$monto_gasto = $reserva_detalle->booking_price_neto->{0};
							}else{ 
								$monto_gasto = $reserva_detalle->price_amount;
							}

						}



					$tipo_reserva_nemo_id = 0;
					if(isset($reserva_detalle->tipo_reserva_nemo_id)){
						$tipo_reserva_nemo_id = $reserva_detalle->tipo_reserva_nemo_id;
					}
					$booking_supplier_id = 0;
					$booking_supplier_code = "";
					if(isset($reserva_detalle->booking_supplier_id->{0})){
						$booking_supplier_id =$reserva_detalle->booking_supplier_id->{0};
						$booking_supplier_code =$reserva_detalle->booking_supplier_id->{0};
						$proveedor_id = Persona::where('cod_nemo', $booking_supplier_id)->where('id_empresa', $valores->id_empresa)->first(['id']);
						if(isset($proveedor_id->id)){
							$booking_supplier_id = $proveedor_id->id;
						}else{
							$booking_supplier_id = 0;
						}
					}
					$supplier_name = null;
					if(isset($reserva_detalle->supplier_name->{0})){
						$supplier_name = $reserva_detalle->supplier_name->{0};
					}

					$destino_ciudad = "";
					if(isset($reserva_detalle->destino_ciudad->{0})){
						$destino_ciudad = $reserva_detalle->destino_ciudad->{0};
					}
					$destino_pais = "";
					if(isset($reserva_detalle->destino_pais->{0})){
						$destino_pais = $reserva_detalle->destino_pais->{0};
					}

					$booking_agency_invoice = null;
					if(isset($reserva_detalle->booking_agency_invoice->{0})){
						$booking_agency_invoice = $reserva_detalle->booking_agency_invoice->{0};
					}

					$booking_user_name = $reserva_detalle->booking_user_name;

					$user_email = null;
					$login_email = null;
					$vendedor_id = 0;
					$agencia_id = 0;
					if(isset($reserva_detalle->vendedor_mail->{0})){
						$base_mail= explode('.', $reserva_detalle->vendedor_mail->{0});
						if($base_mail[0] == 'dtp'){
							if(isset($base_mail[2])){
								$base1 = $base_mail[2];
							}else{
								$base1 = "";
							}	
							if(isset($base_mail[3])){
								$base2 =".".$base_mail[3];
							}else{
								$base2 = "";
							}	
							if(isset($base_mail[4])){
								$base3 =".".$base_mail[4];
							}else{
								$base3= "";
							}	

							$mail = $base_mail[1].".".$base1."".$base2."".$base3;
						}else{
							$mail =$reserva_detalle->vendedor_mail->{0};
						}
						$login_email = $mail;
						$agencia = Persona::where('email',trim($login_email))
											->where('id_empresa', $valores->id_empresa)
											->first(['id', 'id_persona']);
						if(isset($agencia->id)){
							$vendedor_id = $agencia->id;
							$agencia_id = $agencia->id_persona;
						}else{
							$vendedor_id = null;
							$agencia_id = null;
						}
					}

					if(isset($reserva_detalle->client_email->{0})){
						$user_email = $reserva_detalle->client_email->{0};
						$personas = Persona::where('email',$user_email)
											->where('id_empresa', $valores->id_empresa)
											->whereIn('id_tipo_persona',[2,3,10,5])
											->first(['id']);
						if(isset($personas->id)){
							$usuario_id = $personas->id;
						}else{
							$usuario_id = null;
						}
					}
					$lender_name = null;
					$lender_id = 0;
					if(isset($reserva_detalle->lender_name->{0})){
						$lender_name = $reserva_detalle->lender_name->{0};
						$lender_namex = str_replace("'", '',$lender_name);
						$lender_base= DB::select("SELECT get_prestador('".$lender_namex."',".$valores->id_empresa.")");
						$lender_id = $lender_base[0]->get_prestador;
					}else{
						$lender_id = $booking_supplier_id;	
					}
					$reservaGuardada = DB::select("SELECT * FROM public.vw_reservas_nemo where booking_reference = '".$reserva_detalle->booking_reference."'");
					$persona = DB::select("SELECT 
										p.denominacion des_perfil, u.nombre || ' ' || u.apellido nombre_apellido, u.usuario, u.password, 
										p.activo,
										u.fecha_validez, u.id id_usuario, u.id_persona id_agencia, u.id_sucursal_empresa id_sucursal_agencia, 
										u.id_tipo_persona id_perfil, a.nombre razon_social, a.telefono, u.email, u.logo, 
										REPLACE( a.denominacion_comercial, ',', '') descripcion_agencia, 1 as agencia_dtp, 0 linea_credito,
										0 as comision_pactada, u.id_empresa p_out_id_empresa, g.id id_agente, g.nombre || ' ' || u.apellido agente
										FROM  personas u, personas a, personas s, tipo_persona p, personas g, empresas e
										where u.id_persona = a.id
										and u.id_sucursal_empresa = s.id
										and u.id_tipo_persona = p.id
										and u.id_vendedor_empresa = g.id
										and u.usuario = '".$user_email."'
										and u.activo_dtpmundo = true
										and a.activo_dtpmundo = true
										and u.id_empresa = e.id
										and u.id_tipo_persona IN (5,4,2,6,3,7)
										and u.id_empresa=".$valores->id_empresa);
							if(!empty($persona)){
								$persona = $persona[0]->id_usuario;
							}else{
								$persona = 0;
							}

					if(!isset($reservaGuardada[0]->booking_reference)){	
			////////////////////////////RESERVA NEMO ///////////////////////////////////////////////////		
						$datos_creacion = DB::select("SELECT fee_venta, fee_venta, descontar_fee_costo FROM nemo_configuraciones where producto_nemo = ".$tipo_reserva_nemo_id." and id_empresa = ".$valores->id_empresa); 	
						if($datos_creacion[0]->descontar_fee_costo == true){
							$descuento = (float)$datos_creacion[0]->fee_venta;
						}else{
							$descuento = 0;
						}		
						$nemo_reserva = new ReservaNemo;
						$nemo_reserva->code_supplier = $code_supplier;
						$nemo_reserva->booking_reference = $reserva_detalle->booking_reference;
						$nemo_reserva->booking_state_code = $reserva_detalle->booking_state_code;			
						$nemo_reserva->booking_state_descripcion = $reserva_detalle->booking_state_descripcion;
						$nemo_reserva->booking_creation_date = $reserva_detalle->booking_creation_date;
						$nemo_reserva->client_name = $reserva_detalle->client_name;		
						$nemo_reserva->price_type = $reserva_detalle->price_type;
						$nemo_reserva->price_amount = $reserva_detalle->price_amount;
						//$nemo_reserva->respuesta_gds = json_encode($reserva_detalle->respuesta_gds);
						$nemo_reserva->moneda = $moneda;
						$nemo_reserva->moneda_id = $moneda_id;
						$nemo_reserva->precio_total = (float)$bookings->Price->Amount - $descuento;
						$nemo_reserva->booking_contrac_id = $reserva_detalle->booking_contrac_id->{0};
						$nemo_reserva->booking_service_id = $reserva_detalle->booking_service_id->{0};
						$nemo_reserva->client_email = $user_email;
						$nemo_reserva->login_email = $login_email;
						$nemo_reserva->user_id = $usuario_id;
						$nemo_reserva->agencia_id = $agencia_id;
						$nemo_reserva->booking_agency_id = $reserva_detalle->booking_agency_id->{0};
						$nemo_reserva->booking_agency_name = $reserva_detalle->booking_agency_name->{0};
						$nemo_reserva->booking_agency_invoice = $booking_agency_invoice;
						$nemo_reserva->booking_agency_creation_user = $reserva_detalle->booking_agency_creation_user->{0};
						$nemo_reserva->booking_user_id = $reserva_detalle->booking_user_id->{0};
						$nemo_reserva->booking_user_name = $booking_user_name;
						$nemo_reserva->booking_payment_type = $reserva_detalle->booking_payment_type->{0};
						$nemo_reserva->tipo_reserva_nemo_id = $tipo_reserva_nemo_id;
						$nemo_reserva->supplier_id = $booking_supplier_id;
						$nemo_reserva->supplier_name = $supplier_name;
						$nemo_reserva->destino_pais = $destino_pais;
						$nemo_reserva->destino_ciudad = $destino_ciudad;
						$nemo_reserva->booking_checkin = $booking_checkin;
						$nemo_reserva->booking_checkout = $booking_checkout;
						$nemo_reserva->booking_deadline = $deadline;
						$nemo_reserva->vendedor_id = $vendedor_id;
						//if(isset($reserva_detalle->booking_hotel_cancelacion_gross->{0})){
							$nemo_reserva->monto_gasto = $monto_gasto - $descuento;
						/*}else{
							$nemo_reserva->monto_gasto = 0;
						}*/

						if($fecha_gasto_inicio != ''){
							$nemo_reserva->cancelacion_begin_d = $fecha_gasto_inicio;
						}else{
							if($reserva_detalle->booking_hotel_cancelacion_begin_d->{0}){
								$nemo_reserva->cancelacion_begin_d = $reserva_detalle->booking_hotel_cancelacion_begin_d->{0};
							}	
						}
						if($fecha_gasto_final != ''){
							$nemo_reserva->cancelacion_end_d = $fecha_gasto_final;
						}
						$nemo_reserva->estado_id = $estado;
						$nemo_reserva->empresa_id = $valores->id_empresa;
						$nemo_reserva->lender_name = $lender_name;
						$nemo_reserva->lender_id = $lender_id;
						$nemo_reserva->supplier_code = $booking_supplier_code;

						if(isset($reserva_detalle->booking_price_bruto->{0})){
							$nemo_reserva->price_bruto = (float)$reserva_detalle->booking_price_bruto->{0} - $descuento;
						}
						if(isset($reserva_detalle->booking_price_neto_conversion->{0})){
							$nemo_reserva->price_neto_conversion = $reserva_detalle->booking_price_neto_conversion->{0};
						}
						if(isset($reserva_detalle->booking_price_neto->{0})){
							$nemo_reserva->price_neto = (float)$reserva_detalle->booking_price_neto->{0} - $descuento;
						}
						if(isset($reserva_detalle->booking_price_neto_marketer->{0})){
							$nemo_reserva->price_neto_marketer = $reserva_detalle->booking_price_neto_marketer->{0};
						}
						if(isset($reserva_detalle->booking_price_neto_marketer_cancelacion->{0})){
							$nemo_reserva->price_neto_marketer_cancelacion = $reserva_detalle->booking_price_neto_marketer_cancelacion->{0};
						}
						if(isset($reserva_detalle->booking_price_bruto_cancelacion->{0})){
							$nemo_reserva->price_bruto_cancelacion = $reserva_detalle->booking_price_bruto_cancelacion->{0};
						}
						if(isset($reserva_detalle->booking_price_neto_cancelacion->{0})){
							$nemo_reserva->price_neto_cancelacion = $reserva_detalle->booking_price_neto_cancelacion->{0};
						}
						if(isset($reserva_detalle->duration_days->{0})){
							$nemo_reserva->duration_days = $reserva_detalle->duration_days->{0};
						} 
						if(isset($reserva_detalle->booking_costo->{0})){
							$nemo_reserva->price_costo = (float)$reserva_detalle->booking_costo->{0}- $descuento;
						} 
						
						$nemo_reserva->save();
						$reservaId = $nemo_reserva->id;
			/////////////////////////////////////RESERVA HOTEL//////////////////////////////////////////////
						if(isset($reserva_detalle->booking_hotel_room->{"@attributes"}->RoomType)){
							foreach($reserva_detalle->rooms as $key=>$room){
								$nemo_habitacion = new ReservaNemoHabitacion;	
								$nemo_habitacion->tipo = $room->booking_service_item_id->{0};
								$nemo_habitacion->descripcion = $reserva_detalle->booking_hotel_room_description->{0};
								$nemo_habitacion->booking_service_item_id = $reserva_detalle->booking_hotel_room->{"@attributes"}->RoomType;
								$nemo_habitacion->referencia_global_nemo = $reserva_detalle->booking_hotel_reference;
								$nemo_habitacion->nemo_habitaciones = $room->room_count->{0};
								$nemo_habitacion->cod_consumicion = $room->room_type->{0};
								$nemo_habitacion->desc_consumicion = $room->desc_consumicion->{0};
								$nemo_habitacion->sequencia = $room->sequence->{0};
								$nemo_habitacion->ref_sequencia_hab = $room->booking_service_item_id->{0};
								$nemo_habitacion->id_reserva = $reservaId;
								$nemo_habitacion->save();
							}
						}
			/////////////////////////////////RESERVA TRASLADO///////////////////////////////////////////
						if(isset($reserva_detalle->transfer_origin_date)){
							if(isset($reserva_detalle->go_segment)){
								$nemo_traslado = new Traslado;
								$nemo_traslado->origin_date = $reserva_detalle->transfer_origin_date;
								$nemo_traslado->destination_date = $reserva_detalle->transfer_destination_date;
								$nemo_traslado->origin_time = $reserva_detalle->transfer_origin_time;
								$nemo_traslado->destination_time = $reserva_detalle->transfer_destination_time;
								$nemo_traslado->tipo_segmento = 1;
								$nemo_traslado->description = $reserva_detalle->go_segment->description->{0};
								if(isset($reserva_detalle->go_segment->pickup_information->{0})){
									$nemo_traslado->pickup_information = $reserva_detalle->go_segment->pickup_information->{0};
								}else{
									$nemo_traslado->pickup_information = 'N/A';
								}
								if(isset( $reserva_detalle->go_segment->maximum_client_waiting_time->{0})){
									$nemo_traslado->maximum_client_waiting_time = $reserva_detalle->go_segment->maximum_client_waiting_time->{0};
								}else{
									$nemo_traslado->maximum_client_waiting_time = $reserva_detalle->go_segment->maximum_client_waiting_time;
								}

								if(isset($reserva_detalle->go_segment->supplier_comment->{0})){
									$nemo_traslado->supplier_comment = $reserva_detalle->go_segment->supplier_comment->{0};
								}else{
									$nemo_traslado->supplier_comment = 'N/A';
								}

								$nemo_traslado->reserva_nemo_id = $reservaId;
								$nemo_traslado->save();	
								$datos_traslado_id = $nemo_traslado->id;
								if(isset($reserva_detalle->go_segment->pick_up_info)){
									$nemo_detalle_traslado = new DetalleTraslado;
									$nemo_detalle_traslado->reserva_nemo_id = $reservaId;
									$nemo_detalle_traslado->datos_traslado = $datos_traslado_id;
									$nemo_detalle_traslado->tipo_detalle = 'pick_up_info';
									if(isset($reserva_detalle->go_segment->pick_up_info->travel_company_name->{0})){
										$nemo_detalle_traslado->travel_company_name = $reserva_detalle->go_segment->pick_up_info->travel_company_name->{0};
									}else{
										$nemo_detalle_traslado->travel_company_name = "";
									}
									if(isset($reserva_detalle->go_segment->pick_up_info->arrival_time->{0})){
										$nemo_detalle_traslado->arrival_time = date('Y-m-d H:m', strtotime($reserva_detalle->go_segment->pick_up_info->arrival_time->{0}));							
									}else{
										$nemo_detalle_traslado->arrival_time  = null;
									}
									if(isset($reserva_detalle->go_segment->pick_up_info->flight_number->{0})){
										$nemo_detalle_traslado->flight_number = $reserva_detalle->go_segment->pick_up_info->flight_number->{0};							
									}else{
										$nemo_detalle_traslado->flight_number  = "";
									}

									$nemo_detalle_traslado->location = $reserva_detalle->go_segment->pick_up_info->location->{0};
									$nemo_detalle_traslado->city = $reserva_detalle->go_segment->pick_up_info->city->{0};
									$nemo_detalle_traslado->country = $reserva_detalle->go_segment->pick_up_info->country->{0};
									$nemo_detalle_traslado->save();
								}
								if(isset($reserva_detalle->go_segment->drop_off_info)){
									$nemo_detalle_traslado = new DetalleTraslado;
									$nemo_detalle_traslado->reserva_nemo_id = $reservaId;
									$nemo_detalle_traslado->datos_traslado = $datos_traslado_id;
									$nemo_detalle_traslado->tipo_detalle = 'drop_off_info';
									$nemo_detalle_traslado->location = $reserva_detalle->go_segment->pick_up_info->location->{0};
									$nemo_detalle_traslado->city = $reserva_detalle->go_segment->pick_up_info->city->{0};
									$nemo_detalle_traslado->country = $reserva_detalle->go_segment->pick_up_info->country->{0};
									$nemo_detalle_traslado->save();

								}

							}	
							if(isset($reserva_detalle->return_segment)){
								$nemo_traslado = new Traslado;
								$nemo_traslado->origin_date = $reserva_detalle->transfer_origin_date;
								$nemo_traslado->destination_date = $reserva_detalle->transfer_destination_date;
								$nemo_traslado->origin_time = $reserva_detalle->transfer_origin_time;
								$nemo_traslado->destination_time = $reserva_detalle->transfer_destination_time;
								$nemo_traslado->tipo_segmento = 2;
								if(isset($reserva_detalle->return_segment->description->{0})){
									$nemo_traslado->description = $reserva_detalle->return_segment->description->{0};							
								}else{
									$nemo_traslado->description = null;
								}

								
								if(isset($reserva_detalle->return_segment->pickup_information->{0})){
									$nemo_traslado->pickup_information = $reserva_detalle->return_segment->pickup_information->{0};
								}else{
									$nemo_traslado->pickup_information = 'N/A';
								}
								if(isset( $reserva_detalle->return_segment->maximum_client_waiting_time->{0})){
									$nemo_traslado->maximum_client_waiting_time = $reserva_detalle->return_segment->maximum_client_waiting_time->{0};
								}else{
									$nemo_traslado->maximum_client_waiting_time = 'N/A';
								}
								if(isset($reserva_detalle->return_segment->supplier_comment->{0})){
									$nemo_traslado->supplier_comment =$reserva_detalle->return_segment->supplier_comment->{0};
								}else{
									$nemo_traslado->supplier_comment = 'N/A';
								}

								$nemo_traslado->reserva_nemo_id = $reservaId;
								$nemo_traslado->save();	

								$datos_traslado_id = $nemo_traslado->id;
								if(isset($reserva_detalle->return_segment->pick_up_info)){
									$nemo_detalle_traslado = new DetalleTraslado;
									$nemo_detalle_traslado->reserva_nemo_id = $reservaId;
									$nemo_detalle_traslado->datos_traslado = $datos_traslado_id;
									$nemo_detalle_traslado->tipo_detalle = 'pick_up_info';
									if(isset($reserva_detalle->return_segment->pick_up_info->location->{0})){
										$nemo_detalle_traslado->location = $reserva_detalle->return_segment->pick_up_info->location->{0};
									}else{
										$nemo_detalle_traslado->location= 'N/A';
									}

									if(isset( $reserva_detalle->return_segment->pick_up_info->city->{0})){
										$nemo_detalle_traslado->city = $reserva_detalle->return_segment->pick_up_info->city->{0};
									}else{
										$nemo_detalle_traslado->city ='';
									}
									if(isset($reserva_detalle->return_segment->pick_up_info->country->{0})){
										$nemo_detalle_traslado->country = $reserva_detalle->return_segment->pick_up_info->country->{0};
									}else{
										$nemo_detalle_traslado->country ='';
									}

									$nemo_detalle_traslado->save();
								}
								if(isset($reserva_detalle->return_segment->drop_off_info)){
									$nemo_detalle_traslado = new DetalleTraslado;
									$nemo_detalle_traslado->reserva_nemo_id = $reservaId;
									$nemo_detalle_traslado->datos_traslado = $datos_traslado_id;
									$nemo_detalle_traslado->tipo_detalle = 'drop_off_info';
									if(isset($reserva_detalle->return_segment->drop_off_info->travel_company_name->{0})){
										$nemo_detalle_traslado->travel_company_name = $reserva_detalle->return_segment->drop_off_info->travel_company_name->{0};
									}else{
										$nemo_detalle_traslado->travel_company_name = "";
									}
									if(isset($reserva_detalle->return_segment->drop_off_info->arrival_time->{0})){
										$nemo_detalle_traslado->arrival_time = date('Y-m-d H:m', strtotime($reserva_detalle->return_segment->drop_off_info->arrival_time->{0}));
									}else{
										$nemo_detalle_traslado->arrival_time = null;
									}
									if(isset($reserva_detalle->return_segment->drop_off_info->flight_number->{0})){
										$nemo_detalle_traslado->flight_number = $reserva_detalle->return_segment->drop_off_info->flight_number->{0};
									}else{
										$nemo_detalle_traslado->flight_number = null;
									}
									if(isset($reserva_detalle->return_segment->drop_off_info->location->{0})){
										$nemo_detalle_traslado->location = $reserva_detalle->return_segment->drop_off_info->location->{0};
									}else{
										$nemo_detalle_traslado->location = "";
									}

					
									if(isset($reserva_detalle->return_segment->drop_off_info->city->{0})){
										$nemo_detalle_traslado->city = $reserva_detalle->return_segment->drop_off_info->city->{0};
									}else{
										$nemo_detalle_traslado->city = "";
									}
									
									if(isset($reserva_detalle->return_segment->drop_off_info->country->{0})){
										$nemo_detalle_traslado->country = $reserva_detalle->return_segment->drop_off_info->country->{0};
									}else{
										$nemo_detalle_traslado->country = "";
									}

									$nemo_detalle_traslado->save();
								}
							}	
						}
			/////////////////////////////////RESERVA ACTIVIDAD//////////////////////////////////////
						if(isset($reserva_detalle->activit_name->{0})){

							$nemo_actividad = new ReservaNemoActividad;
							$nemo_actividad->reserva_nemo_id = $reservaId;
							$nemo_actividad->name = $reserva_detalle->activit_name->{0};
							$nemo_actividad->checkin_date =  date('Y-m-d', strtotime($reserva_detalle->checkin_date));
							$tmplocation = (json_encode($reserva_detalle->location));
							if($tmplocation == '{}'){
								$nemo_actividad->location =  'N/A';
							}else{
								$nemo_actividad->location =  $reserva_detalle->location->{0};
							}
							if(isset($reserva_detalle->address->{0})){
								$nemo_actividad->address = $reserva_detalle->address->{0};
							}else{
								$nemo_actividad->address ="";
							}

							$nemo_actividad->booking_service_item_id =  $reserva_detalle->booking_service_item_id->{0};
							//$tmp = (json_encode($reserva_detalle->actividad_description->{0}));
							if(isset($reserva_detalle->actividad_description->{0})){
								$nemo_actividad->description =  json_encode($reserva_detalle->actividad_description->{0});
							}else{
								$nemo_actividad->description  = "";
							}
							$nemo_actividad->actividad_reference = $reserva_detalle->actividad_reference;
							$nemo_actividad->save();	

						}
						$adulto = 0;
						$ninho = 0; 
						$infante = 0; 
						if(isset($reserva_detalle->booking_passenger)){
							foreach($reserva_detalle->booking_passenger as $key=>$passenger){
								if($tipo_reserva_nemo_id == 1){
									$nemo_pasajero = new ReservaNemoPasajero;
									$nemo_pasajero->nombres = $passenger->{"@attributes"}->FirstName;
									$nemo_pasajero->apellidos = $passenger->{"@attributes"}->LastName;
									$nemo_pasajero->tipo = $passenger->{"@attributes"}->AgeType;
									$nemo_pasajero->id_reserva_nemo = $reservaId;
									$nemo_pasajero->texto = null;
									$nemo_pasajero->codigo_pasajero = $passenger->{"@attributes"}->PassengerCode;
									$nemo_pasajero->ref_sequencia_hab = $passenger->{"@attributes"}->Sequence;
									$nemo_pasajero->id_habitacion = null;
									$nemo_pasajero->documento = null;
									if($passenger->{"@attributes"}->AgeType == 'NMO.GBL.AGT.ADT'){
										$adulto = $adulto + 1;
									}
									if($passenger->{"@attributes"}->AgeType == 'NMO.GBL.AGT.CHD'){
										$ninho = $ninho + 1;
									}
									if($passenger->{"@attributes"}->AgeType == 'NMO.GBL.AGT.INF'){
										$infante = $infante + 1;
									}
									$nemo_pasajero->save();
								}
								if($tipo_reserva_nemo_id == 3){
									$birthDate = null;
									if(isset($passenger->{"@attributes"}->BirthDate)){
										$birthDate = $passenger->{"@attributes"}->BirthDate;
									}
									$nemo_pasajero = new ReservaNemoPasajero;
									$nemo_pasajero->nombres = $passenger->{"@attributes"}->FirstName;
									$nemo_pasajero->apellidos = $passenger->{"@attributes"}->LastName;
									$nemo_pasajero->tipo = $birthDate;
									$nemo_pasajero->id_reserva_nemo = $reservaId;
									$nemo_pasajero->texto = null;
									$nemo_pasajero->codigo_pasajero = $passenger->{"@attributes"}->PassengerCode;
									$nemo_pasajero->ref_sequencia_hab = $passenger->{"@attributes"}->Sequence;
									$nemo_pasajero->id_habitacion = null;
									$nemo_pasajero->documento = null;
									$nemo_pasajero->save();
								}
								if($tipo_reserva_nemo_id == 4){
									$nemo_pasajero = new ReservaNemoPasajero;
									$nemo_pasajero->nombres = $passenger->Name;
									$nemo_pasajero->apellidos = null;
									$nemo_pasajero->tipo = null;
									$nemo_pasajero->id_reserva_nemo = $reservaId;
									$nemo_pasajero->texto = null;
									$nemo_pasajero->codigo_pasajero = $passenger->{"@attributes"}->PassengerCode;
									$nemo_pasajero->ref_sequencia_hab = $passenger->{"@attributes"}->Sequence;
									$nemo_pasajero->id_habitacion = null;
									$nemo_pasajero->documento = null;
									$nemo_pasajero->save();
								}
									
							}
						}
						/*echo '<pre>';
						print_r("SELECT public.insert_notificacion(".$persona.",'".$user_email."', null, ".$reservaId.",".$tipo_notificacion.",null,1)");
						$getNotificacion= DB::select("SELECT public.insert_notificacion(".$persona.",'".$user_email."', null, '".$reservaId."',".$tipo_notificacion.",null,1)");*/
					}else{ 
						$reservaId = $reservaGuardada[0]->id;
						if($reservaGuardada[0]->estado_id != (int)$estado){ 
							DB::select("SELECT modificar_codigo(".$reservaId.", '".$code_supplier."')");
							if((int)$estado == 1){ 
								if($reservaGuardada[0]->factura_id == ""){
									if($reservaGuardada[0]->id_detalle_proforma !=""){
										DB::table('proformas_detalle')
											->where('id',$reservaGuardada[0]->id_detalle_proforma)
											->update([
													'activo'=>false,
													]);
										if(isset($reserva_detalle->booking_hotel_cancelacion_begin_d)){ 
											$today_dt = new \DateTime($reserva_detalle->booking_state_date);
											$expire_dt = new \DateTime($reserva_detalle->booking_hotel_cancelacion_begin_d->{0});
											if($expire_dt <= $today_dt){ 
												$detallesProforma = ProformasDetalle::where('id',$reservaGuardada[0]->id_detalle_proforma)->first();					
												$detallesMax = ProformasDetalle::where('id_proforma', $detallesProforma->id_proforma)->orderBy('id', 'DESC')->first();

												$proformasDetalle = new ProformasDetalle;
												$proformasDetalle->item = (int)$detallesMax->item +1 ;
												$proformasDetalle->id_producto = 7;
												$proformasDetalle->costo_proveedor = $reserva_detalle->booking_hotel_cancelacion_gross->{0};
												$proformasDetalle->precio_costo = $reserva_detalle->booking_hotel_cancelacion_gross->{0};
												$proformasDetalle->precio_venta = $reserva_detalle->booking_hotel_cancelacion_gross->{0};
												$proformasDetalle->porcion_exenta = $reserva_detalle->booking_hotel_cancelacion_gross->{0};
												$proformasDetalle->id_empresa = $detallesProforma->id_empresa;
												$proformasDetalle->cod_confirmacion = $detallesProforma->cod_confirmacion;
												$proformasDetalle->fecha_in = $detallesProforma->fecha_in;
												$proformasDetalle->fecha_out = $detallesProforma->fecha_out;
												$proformasDetalle->markup = 1;
												$proformasDetalle->id_proveedor = 30702;
												$proformasDetalle->id_prestador = 30702;
												$proformasDetalle->descripcion = 'Gastos de Cancelacion Nemo';
												$proformasDetalle->origen = 'A';
												$proformasDetalle->currency_venta_id = $detallesProforma->currency_venta_id;
												$proformasDetalle->currency_costo_id = $detallesProforma->currency_costo_id ;
												$proformasDetalle->porcentaje_comision_agencia = 0;
												$proformasDetalle->id_usuario = $detallesProforma->id_usuario;
												$proformasDetalle->activo = true;
												$proformasDetalle->id_proforma = $detallesProforma->id_proforma;
												$proformasDetalle->fecha_pago_proveedor = $detallesProforma->fecha_pago_proveedor;
												$proformasDetalle->fecha_gasto = $detallesProforma->fecha_gasto;
												$proformasDetalle->comision_agencia = 0;
												$proformasDetalle->save();
												DB::table('reservas_nemo')  
																->where('booking_reference',$reserva_detalle->booking_reference)
																->update([
																		'estado_id'=>$estado
																		]);
												//echo '<pre>';
												//print_r("SELECT public.insert_notificacion(".$persona.",'".$user_email."', null, ".$reservaId.",".$tipo_notificacion.",null)");
												//$getNotificacion= DB::select("SELECT public.insert_notificacion(".$persona.",'".$user_email."', null, '".$reservaId."',".$tipo_notificacion.",null,1)");
											}
										}								
									}else{ 
										DB::table('reservas_nemo')  
												->where('booking_reference',$reserva_detalle->booking_reference)
												->update([
														'estado_id'=>$estado
														]);
									}	
								}else{
									//echo '<pre>';
									//print_r("SELECT public.insert_notificacion(".$persona.",'".$user_email."', null, ".$reservaId.",".$tipo_notificacion.",null)");
										$notificacion = Notificacion::where('id_log',$reservaId."_".$reservaGuardada[0]->factura_id)->first();
										if(!isset($notificacion->id_log)){
											//$getNotificacion= DB::select("SELECT public.insert_notificacion(".$persona.",'".$user_email."', null, '".$reservaId."_".$reservaGuardada[0]->factura_id."',22,null,1)");
											$factura = Factura::with('cliente',
																	'vendedorAgencia',
																	'vendedorEmpresa')
																	->where('id',$reservaGuardada[0]->factura_id)->first();
											if(isset($factura->vendedorEmpresa->email)){
												$correo_vendedor = 'operaciones@dtp.com.py';

												//$getNotificacion= DB::select("SELECT public.insert_notificacion(".$persona.",'".$correo_vendedor."', null, '".$reservaId."_".$reservaGuardada[0]->factura_id."',22,null,1)");

											}	
										}
								}

							}else{
								DB::table('reservas_nemo')  
								->where('booking_reference',$reserva_detalle->booking_reference)
								->update([
										'estado_id'=>$estado
										]);
							}
						}else{
							DB::select("SELECT modificar_codigo(".$reservaId.", '".$code_supplier."')");
						}
					} 
			/*	} catch(\Exception $e){
					echo '<pre>';
					print_r('ERROR :   '.$e->getMessage());
				}	 */

			}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
		die;

 	//	return redirect()->route('notificar');
	}


	private function detallesReserva($booking_id,$bookings,$id_nemo){
		$configuracion_nemo = DB::select("SELECT * FROM nemo_credenciales WHERE tid = ".$id_nemo." AND activo =  true");
		$login = $configuracion_nemo[0]->usuario;
		$password = $configuracion_nemo[0]->contrasenha;       
		$request_uri = $configuracion_nemo[0]->url_detalle;  

		$client = new Client(['auth' => [$login, $password]]);
		$xml_body = '<BookingsDetailsRQ>
						<GeneralParameters>
							<PreferedLanguage LanguageCode="es-AR"/>
						</GeneralParameters>
						<Details>
							<Bookings ItemsCount="1">
								<Booking Sequence="1">
									<BookingReference ReferenceType="NMO.GLB.BRT.NAV">'.$booking_id.'</BookingReference>
								</Booking>
							</Bookings>
							<ProviderRQ>false</ProviderRQ>
						</Details>
					</BookingsDetailsRQ>';
			
		$responses = $client->request('POST', $request_uri, [
						'headers' => [
							'Accept' => 'application/xml'
						],
						'body'   => $xml_body
						])->getBody()->getContents();
		/*echo '<pre>';
		print_r($booking_id);*/
		echo '<pre>';
		print_r($responses);
		$jsonDetalle = simplexml_load_string($responses); 
		$xmlobj = @new \SimpleXMLElement($responses);

		$reserva_nemo =  new \StdClass;
		$reserva_nemo->booking_reference = $booking_id;
		$reserva_nemo->respuesta_gds = $responses;
		$reserva_nemo->booking_state_code = $bookings->BookingState->BookingState->Code;
		$reserva_nemo->booking_state_date = $bookings->BookingState->BookingState->StateDateTime;			
		$reserva_nemo->booking_state_descripcion = $bookings->BookingState->BookingState->Description;
		$reserva_nemo->booking_creation_date = $bookings->BookingCreationDate;
		$reserva_nemo->client_name = $bookings->ClientName;			
		$reserva_nemo->price_type = $bookings->Price->{"@attributes"}->PriceType;
		$reserva_nemo->price_amount = $bookings->Price->Amount;
		$reserva_nemo->booking_comments = $bookings->Comments;
		$reserva_nemo->booking_contrac_id = $jsonDetalle->Details->Bookings->Booking['ContractId'];
		$reserva_nemo->booking_service_id = $jsonDetalle->Details->Bookings->Booking->BookingServiceId;
		$reserva_nemo->client_email = $jsonDetalle->Details->Bookings->Booking->Email;
		$reserva_nemo->user_email = $jsonDetalle->Details->Bookings->Booking->Email;
		$reserva_nemo->vendedor_mail = $jsonDetalle->Details->Bookings->Booking->User->Login;
		$reserva_nemo->booking_agency_id = $jsonDetalle->Details->Bookings->Booking->Agency['AgencyId'];
		$reserva_nemo->booking_agency_name = $jsonDetalle->Details->Bookings->Booking->Agency->Name;
		$reserva_nemo->booking_agency_invoice = $jsonDetalle->Details->Bookings->Booking->Agency->InvoiceCompanyName;
		$reserva_nemo->booking_agency_creation_user = $jsonDetalle->Details->Bookings->Booking->Agency->CreationUser;
		$reserva_nemo->booking_user_id = $jsonDetalle->Details->Bookings->Booking->User['UserId'];
		$booking_user_name = $jsonDetalle->Details->Bookings->Booking->User['Login'];
		$reserva_nemo->booking_user_name = $booking_user_name;
		$reserva_nemo->booking_payment_type = $jsonDetalle->Details->Bookings->Booking->Payments->Payment['Type'];
		if(isset($jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels)){
			$pasajero = new \StdClass;
			$pasajeros = [];
			foreach($xmlobj->Details->Bookings->Booking->BookingItems->Hotels->Hotel->Passengers->children() as $key=>$item) {
				$attr=$item;
				$pasajero->codigo = $attr->attributes()->{'PassengerCode'};
				$pasajero->nombre = $attr->attributes()->{'FirstName'};
				$pasajero->apellido = $attr->attributes()->{'LastName'};
				$pasajero->edad = $attr->attributes()->{'AgeType'};
				$pasajero->documento = $attr->attributes()->{'Identifier'};
				$pasajeros[] = $item;
			}

			$room = new \StdClass;
			$rooms = [];
			foreach($xmlobj->Details->Bookings->Booking->BookingItems->Hotels->Hotel->Rooms->children() as $keys=>$base) {
				$atributos=$base;
				$room->sequence = $atributos->attributes()->{'Sequence'};
				$room->booking_service_item_id = $atributos->attributes()->{'BookingServiceItemId'};
				$room->room_type = $atributos->attributes()->{'RoomType'};
				$room->room_count = $atributos->attributes()->{'RoomCount'};
				$room->desc_consumicion = $base[0];
				$rooms[] = $room;
			}
			$reference = "";
			$s = 0;
			$supplier_code = '';
			foreach($xmlobj->Details->Bookings->Booking->BookingItems->Hotels->Hotel->BookingReferences->children() as $sx =>$references) {
				if($references->attributes()->{'ReferenceType'} == 'NMO.HTL.RPT.PRS.PS1'){
					$supplier_code = $references[0];
				}

				if($s == 0){
					$reference .= $references[0];	
				}else{
					$reference .= ",".$references[0];	
				}
				$s = $s + 1;
			}
			if($supplier_code == ""){
				foreach($xmlobj->Details->Bookings->Booking->BookingItems->Hotels->Hotel->BookingReferences->children() as $sx =>$references) {
					if($references->attributes()->{'ReferenceType'} == 'NMO.HTL.RPT.PRS'){
						$supplier_code = $references[0];
					}
	
					if($s == 0){
						$reference .= $references[0];	
					}else{
						$reference .= ",".$references[0];	
					}
					$s = $s + 1;
				}
			}



			$supplier_id = "";
			$lender = "";
			$supplier_name = "";
			$lender_name = "";
			foreach($xmlobj->Details->Bookings->Booking->BookingItems->Hotels->children() as $sx =>$hotels) {
				$supplier_id = $hotels->attributes()->{'SupplierID'};
				$supplier_name = $hotels->attributes()->{'Supplier'};
				$lender_name = $hotels->attributes()->{'Name'};
			}

			$reserva_nemo->tipo_reserva_nemo_id = 1;  //Alojamiento
			$reserva_nemo->booking_hotel_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->{"@attributes"}->HotelId;
			$reserva_nemo->rooms = $rooms;
			$reserva_nemo->booking_hotel_supplier_id = $supplier_id;
			$reserva_nemo->booking_supplier_code = $supplier_code;
			$reserva_nemo->supplier_name = $supplier_name;
			$reserva_nemo->booking_hotel_name = $supplier_name;
			$reserva_nemo->booking_supplier_id = $supplier_id;
			$reserva_nemo->lender_name = $lender_name;
			$reserva_nemo->booking_service_item_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel['BookingServiceItemId'];
			$reserva_nemo->booking_hotel_reference = $reference; 
			$reserva_nemo->destino_pais = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->DestinationDetails->Destination[0]; 
			$reserva_nemo->destino_ciudad = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->DestinationDetails->Destination[1];
			$reserva_nemo->booking_checkin = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->CheckIn;
			$reserva_nemo->booking_checkout = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->CheckOut;
			$reserva_nemo->booking_deadline = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->Deadline;
			$reserva_nemo->booking_hotel_board = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->Boards->Board;
			$reserva_nemo->booking_passenger = $pasajeros;
			$reserva_nemo->booking_hotel_room = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->Rooms->Room;
			$reserva_nemo->booking_hotel_room_description = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->Description;
			$reserva_nemo->booking_hotel_state_code = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->BookingStates->BookingState->Code;
			$reserva_nemo->booking_hotel_state_description = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->BookingStates->BookingState->Description;
			$reserva_nemo->booking_hotel_state_date = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->BookingStates->BookingState->StateDateTime;
			foreach($jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->Prices->Price as $key=>$prices){
				if($prices['PriceType'] == 'NMO.GBL.RPT.GRS'){
					$reserva_nemo->booking_price_bruto = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.MYB'){
					$reserva_nemo->booking_costo = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NET'){
					$reserva_nemo->booking_price_neto = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NFM'){
					$reserva_nemo->booking_price_neto_marketer = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNF'){
					$reserva_nemo->booking_price_neto_marketer_cancelacion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNG'){
					$reserva_nemo->booking_price_bruto_cancelacion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNN'){
					$reserva_nemo->booking_price_neto_cancelacion = $prices->Amount;
				}
			}

			if(isset($jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->Remarks)){
				$reserva_nemo->booking_hotel_remarks= $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->Remarks;
			}
			
			if(isset($jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition)){
				$condiciones = array();
				foreach($jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions as $key=>$condition){
					$condiciones[$key] = $condition;
				}
				$reserva_nemo->chargue_conditions = $condiciones;
				$reserva_nemo->booking_hotel_cancelacion_condition_type = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['Type'];
				$reserva_nemo->booking_hotel_cancelacion_condition_name = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['Name'];
				$reserva_nemo->booking_hotel_cancelacion_begin_d = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['BeginD'];
				$reserva_nemo->booking_hotel_cancelacion_end_d = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['EndD'];
				$reserva_nemo->booking_hotel_cancelacion_net_before_currency_conversion = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['NetBeforeCurrencyConversion'];
				$reserva_nemo->booking_hotel_cancelacion_net = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['Net'];
				$reserva_nemo->booking_hotel_cancelacion_nett_marketer = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['NettForMarketer'];
				$reserva_nemo->booking_hotel_cancelacion_gross = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['Gross'];
				$reserva_nemo->booking_hotel_cancelacion_currency_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['CurrencyId'];
				$reserva_nemo->booking_hotel_cancelacion_remarks = $jsonDetalle->Details->Bookings->Booking->BookingItems->Hotels->Hotel->ChargeConditions->ChargeCondition['Remarks'];
			}
		}	
		if(isset($jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits)){
			$reserva_nemo->tipo_reserva_nemo_id = 2;//Circuitos
			$reserva_nemo->booking_supplier_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit['SupplierID'];
			$reserva_nemo->booking_circuit_name = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit['Name'];
			$reserva_nemo->supplier_name = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit['Supplier'];
			$reserva_nemo->booking_circuit_reference = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->BookingReferences->BookingReference; 
			$reserva_nemo->destino_pais = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Origin['Country']; 
			$reserva_nemo->destino_ciudad = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Origin['City'];
			$reserva_nemo->booking_circuit_description = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Description;
			$reserva_nemo->booking_circuit_original_departure_date = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->OriginalDepartureDate;
			$reserva_nemo->booking_checkin= $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->RealDepartureDate;
			$reserva_nemo->booking_checkout = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->ArrivalDate;
			$reserva_nemo->booking_circuit_passenger = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Passengers->Passenger->FirstName." ".$jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Passengers->Passenger->LastName;
			$reserva_nemo->booking_circuit_birthdate = date('Y-m-d', strtotime($jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Passengers->Passenger->Birthdate));
			$reserva_nemo->booking_circuit_age = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Passengers->Passenger->Age;
			$reserva_nemo->booking_circuit_dni = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Passengers->Passenger->Dni;
			$reserva_nemo->booking_circuit_passport = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Passengers->Passenger->Passport;
			$reserva_nemo->booking_circuit_nationality = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Passengers->Passenger->Nationality;
			$reserva_nemo->booking_circuit_is_club = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Passengers->Passenger->isClub;
			$reserva_nemo->booking_deadline = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Deadline;
			$reserva_nemo->booking_circuit_state_code = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->BookingStates->BookingState->Code;
			$reserva_nemo->booking_circuit_state_description = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->BookingStates->BookingState->Description;
			$reserva_nemo->booking_circuit_state_date = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->BookingStates->BookingState->StateDateTime;
		 	$reserva_nemo->duration_days = $jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit['DurationDays'];
			 $s= 0;
			 $supplier_code ="";
			 $reference = "";
			 foreach($xmlobj->Details->Bookings->Booking->BookingItems->Circuits->Circuit->BookingReferences->children() as $sx =>$references) {
				if($references->attributes()->{'ReferenceType'} == 'NMO.CIR.BRT.APS'){
					$supplier_code = $references[0];
				}
				if($s == 0){
					$reference .= $references[0];	
				}else{
					$reference .= ",".$references[0];	
				}
				$s = $s + 1;
			}
			$reserva_nemo->booking_supplier_code = $supplier_code;
			 foreach($jsonDetalle->Details->Bookings->Booking->BookingItems->Circuits->Circuit->Prices->Price as $key=>$prices){
				if($prices['PriceType'] == 'NMO.GBL.RPT.GRS'){
					$reserva_nemo->booking_price_bruto = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.MYB'){
					$reserva_nemo->booking_costo = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NBC'){
					$reserva_nemo->booking_price_neto_conversion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NET'){
					$reserva_nemo->booking_price_neto = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NFM'){
					$reserva_nemo->booking_price_neto_marketer = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNF'){
					$reserva_nemo->booking_price_neto_marketer_cancelacion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNG'){
					$reserva_nemo->booking_price_bruto_cancelacion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNN'){
					$reserva_nemo->booking_price_neto_cancelacion = $prices->Amount;
				}
			}
		}

		if(isset($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers)){
			if($booking_id == "PH_1NBMI"){	

				$book_ref = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->BookingReferences->children();
				foreach($book_ref->{'BookingReference'} as $x=>$valor){
				}
			}	
			$s = 0;
			$reference="";
			$supplier_code = "";

			foreach($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->BookingReferences->BookingReference as $sx =>$references){
			
				if($references->attributes()->{'ReferenceType'} == 'NMO.ACT.BRT.APS'){
					$supplier_code = $references[0];
				}
				if($s == 0){
					$reference .= $references[0];	
				}else{
					$reference .= ",".$references[0];	
				}
				$s = $s + 1;
			}
			$reserva_nemo->booking_supplier_code = $supplier_code;

			$xmlobj = @new \SimpleXMLElement($responses);
			$pasajero = new \StdClass;
			$pasajeros = [];
			foreach($xmlobj->Details->Bookings->Booking->BookingItems->Transfers->Transfer->Passengers->children() as $key=>$item) {
				$attr=$item;
				$pasajero->codigo = $attr->attributes()->{'PassengerCode'};
				$pasajero->nombre = $attr->attributes()->{'FirstName'};
				$pasajero->apellido = $attr->attributes()->{'LastName'};
				$pasajero->sequence = $attr->attributes()->{'Sequence'};
				$pasajeros[] = $item;
			}

			$reserva_nemo->transfer_reference = $reference;
			$reserva_nemo->tipo_reserva_nemo_id = 3; //Traslado
			$reserva_nemo->booking_passenger = $pasajeros;
			$reserva_nemo->transfer_sequence = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer['Sequence'];
			$reserva_nemo->transfer_origin = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer['Origin'];
			$reserva_nemo->transfer_destination = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->{"@attributes"}->Destination;
			$reserva_nemo->booking_supplier_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer['SupplierID'];
			$reserva_nemo->supplier_name = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer['Supplier'];			
			$reserva_nemo->booking_service_item_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer['BookingServiceItemId'];		
			$reserva_nemo->booking_checkin = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->OriginDateTimeRequest;
			if(isset($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->DestinationDateTimeRequest)){
				$reserva_nemo->booking_checkout =$jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->DestinationDateTimeRequest;
			}else{
				$reserva_nemo->booking_checkout =$jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->OriginDateTimeRequest;
			}
			$reserva_nemo->booking_deadline = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->Deadline;
			$reserva_nemo->transfer_origin_date = date('Y-m-d', strtotime($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->OriginDateTimeRequest));
			$reserva_nemo->transfer_destination_date = date('Y-m-d', strtotime($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->DestinationDateTimeRequest));
			$reserva_nemo->transfer_origin_time = date('H:m:s', strtotime($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->OriginDateTimeRequest));
			$reserva_nemo->transfer_destination_time = date('H:m:s', strtotime($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->DestinationDateTimeRequest));
			$go_segment = new \StdClass;
			$go_segment->segment_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->{"@attributes"}->BookingServiceItemId;
			$go_segment->description = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->Description[0];
			$pick_up_info = new \StdClass;
			$drop_off_info = new \StdClass;
			$pick_up_info->travel_company_name = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->PickUpInfo->TravelCompanyName;
			$pick_up_info->arrival_time = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->PickUpInfo->ArrivalTime;
			$pick_up_info->flight_number = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->PickUpInfo->FlightNumber;
			$pick_up_info->location = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->PickUpInfo->Location;
			$pick_up_info->city = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->PickUpInfo->City;
			$pick_up_info->country = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->PickUpInfo->Country;
			$drop_off_info->location = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->DropOffInfo->Location;
			$drop_off_info->city = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->DropOffInfo->City;
			$drop_off_info->country = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->DropOffInfo->Country;
			$go_segment->pickup_information = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->PickupInformation;
			if(isset($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->MaximumClientWaitingTime)){
				$go_segment->maximum_client_waiting_time = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->MaximumClientWaitingTime;
			}else{
				$go_segment->maximum_client_waiting_time = 'N/A';
			}
			$go_segment->supplier_comment = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->GoSegment->SupplierComment;
			$go_segment->pick_up_info = $pick_up_info;
			$go_segment->drop_off_info = $drop_off_info;
			$reserva_nemo->go_segment = $go_segment;
			if(isset($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment)){
				$return_segment = new \StdClass;
				$pick_up_info = new \StdClass;
				$drop_off_info = new \StdClass;
				$return_segment->segment_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment['BookingServiceItemId'];
				$return_segment->description = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->Description;
				$drop_off_info->travel_company_name = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->DropOffInfo->TravelCompanyName;
				$drop_off_info->arrival_time = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->DropOffInfo->DepartureTime;
				$drop_off_info->flight_number = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->DropOffInfo->FlightNumber;
				$drop_off_info->location = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->DropOffInfo->Location;
				$drop_off_info->city = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->DropOffInfo->City;
				$drop_off_info->country = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->DropOffInfo->Country;
				$pick_up_info->location = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->PickUpInfo->Location;
				$pick_up_info->city = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->PickUpInfo->City;
				$pick_up_info->country = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->PickUpInfo->Country;
				$return_segment->pickup_information = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->PickupInformation;
				$return_segment->maximum_client_waiting_time = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->MaximumClientWaitingTime;
				$return_segment->supplier_comment = $jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->ReturnSegment->SupplierComment;
				$return_segment->drop_off_info = $drop_off_info;
				$return_segment->pick_up_info = $pick_up_info;
				$reserva_nemo->return_segment = $return_segment;
			}
			foreach($jsonDetalle->Details->Bookings->Booking->BookingItems->Transfers->Transfer->Prices->Price as $key=>$prices){
				if($prices['PriceType'] == 'NMO.GBL.RPT.GRS'){
					$reserva_nemo->booking_price_bruto = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NBC'){
					$reserva_nemo->booking_price_neto_conversion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.MYB'){
					$reserva_nemo->booking_costo = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NET'){
					$reserva_nemo->booking_price_neto = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NFM'){
					$reserva_nemo->booking_price_neto_marketer = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNF'){
					$reserva_nemo->booking_price_neto_marketer_cancelacion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNG'){
					$reserva_nemo->booking_price_bruto_cancelacion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNN'){
					$reserva_nemo->booking_price_neto_cancelacion = $prices->Amount;
				}
			}

		}
		if(isset($jsonDetalle->Details->Bookings->Booking->BookingItems->Activities)){
			$reference = "";
			$s = 0;
			foreach($jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->BookingReferences->BookingReference as $sx =>$references){
				if($s == 0){
					$reference .= $references[0];	
				}else{
					$reference .= ",".$references[0];	
				}
				$s = $s + 1;
			}
			$pasajero = new \StdClass;
			$pasajeros = [];
			foreach($jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->Passengers->Passenger as $key=>$item) {
				$attr=$item;
				$pasajero->codigo = $attr['PassengerCode'];
				$pasajero->nombre = $attr->Name;
				$pasajero->apellido = "";
				$pasajero->sequence = $attr['Sequence'];
				$pasajeros[] = $item;
			}
			$s = 0;
			$reference="";
			$supplier_code ="";
			foreach($xmlobj->Details->Bookings->Booking->BookingItems->Activities->Activity->BookingReferences->children() as $sx =>$references) {
				if($references->attributes()->{'ReferenceType'} == 'NMO.ACT.BRT.APS'){
					$supplier_code = $references[0];
				}
				if($s == 0){
					$reference .= $references[0];	
				}else{
					$reference .= ",".$references[0];	
				}
				$s = $s + 1;
			}
			$reserva_nemo->booking_supplier_code = $supplier_code;
			$reserva_nemo->actividad_reference = $reference;
			$reserva_nemo->tipo_reserva_nemo_id = 4; //Actividades
			$reserva_nemo->booking_passenger = $pasajeros;
			$reserva_nemo->activit_sequence = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity['Sequence'];
			$reserva_nemo->booking_supplier_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity['SupplierID'];
			$reserva_nemo->activit_name = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity['Name'];
			$reserva_nemo->checkin_date = trim($jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->CheckinDate);
			$reserva_nemo->booking_checkin = trim($jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->CheckinDate);
			$reserva_nemo->booking_checkout = trim($jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->CheckinDate);
	        $reserva_nemo->booking_deadline = null;
			$reserva_nemo->destino_pais = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->Destinations->Destination[0]; 
			$reserva_nemo->destino_ciudad = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->Destinations->Destination[1];
			$reserva_nemo->location = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->Location;
			$reserva_nemo->address = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->Address;
			$reserva_nemo->booking_service_item_id = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity['BookingServiceItemId'];
			$description = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->Description;
			if(empty($description)){
				$reserva_nemo->actividad_description = 'N/A';
			}else{
				$reserva_nemo->actividad_description = $jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->Description;
			}
			foreach($jsonDetalle->Details->Bookings->Booking->BookingItems->Activities->Activity->Prices->Price as $key=>$prices){
				if($prices['PriceType'] == 'NMO.GBL.RPT.GRS'){
					$reserva_nemo->booking_price_bruto = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NBC'){
					$reserva_nemo->booking_price_neto_conversion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.MYB'){
					$reserva_nemo->booking_costo = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NET'){
					$reserva_nemo->booking_price_neto = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.NFM'){
					$reserva_nemo->booking_price_neto_marketer = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNF'){
					$reserva_nemo->booking_price_neto_marketer_cancelacion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNG'){
					$reserva_nemo->booking_price_bruto_cancelacion = $prices->Amount;
				}
				if($prices['PriceType'] == 'NMO.HTL.RPT.CNN'){
					$reserva_nemo->booking_price_neto_cancelacion = $prices->Amount;
				}
			}
			
		}
		return json_encode($reserva_nemo);
	}



    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }//function


}
