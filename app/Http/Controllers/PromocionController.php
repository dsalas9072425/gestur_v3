<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use App\Proforma;
use App\Producto;
use App\Persona;
use App\TipoFactura;
use App\Autorizaciones;
use App\TipoFacturacion;
use App\Currency;
use App\FormaPagoCliente;
use App\TipoIdentidad;
use App\EstadoFactour;
use App\Ticket;
use App\Asistencia; 
use App\ProformasDetalle;
use App\ProformasDetallePasajeros;
use App\HistoricoComentariosProforma;
use App\TarjetaCredito;
use App\Voucher;
use App\TiposTicket;
use App\Grupos;
use App\Empresa;
use App\VentasRapidasCabecera;
use App\VentasRapidasDetalle;
use Session;
use Redirect;
use App\AdjuntoDocumento;
use App\LineaCredito;
use App\Hotel;
use App\Promocion;
use App\Destination;
use App\Tarifa;
use App\ReservasServicio;
use App\ReservasNemo;
use App\TipoImpuesto;
use App\EstadoPedidoWoo;
use App\Zona;
use App\FormaCobroCliente;
use App\ComercioPersona;
use App\Factura;
use App\TipoVenta;
use App\FileEmpresas;
use App\PasajeroProforma;
use App\NemoReserva;
use App\Negocio;
use App\EstadoReserva;
use App\Anticipo;
use App\SolicitudFacturaParcial;
use App\ProformaFacturaDetalle;
use App\ProformaCliente;
use DB;


class PromocionController extends Controller
{

    private function getIdEmpresa(){
        return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
    }

    public function index()
    {
        return view('pages.mc.promociones.index');
    }



    public function add(Request $request)
    {

        return view('pages.mc.promociones.add');	
    }


    public function doIndex(Request $request)
    {
        $promociones = DB::table('promociones');
        if($request->input('descripcion') != ''){
            $promociones = $promociones->where('descripcion', 'like', '%' .$request->input('descripcion').'%');  
        }
		$promociones = $promociones->where('id_empresa',$this->getIdEmpresa());
		$promociones = $promociones->get();
        return response()->json($promociones);
    }

    public function doAdd(Request $request)
    {

		$id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		$id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

        $check_in = explode('/', $request->input('fecha_inicio'));
        $check_out = explode('/', $request->input('fecha_vencimiento'));
        $inicio = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]);
        $vencimiento = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]);
        $id_promociones = Promocion::orderby('id', 'desc')->first();
        $id = $id_promociones->id + 1;
		$promocion = new Promocion;
        $promocion->id = $id;
		$promocion->fecha = date('Y-m-d h:i:s');
		$promocion->descripcion = $request->input('descripcion');
		$promocion->fecha_alta = $inicio;
        $promocion->fecha_baja = $vencimiento;
        $promocion->porcentaje_comision = str_replace(',','.', str_replace('.','',$request->input('porcentaje_comision')));
		$promocion->activo = true;
		$promocion->id_empresa = $id_empresa;
		$promocion->id_usuario_alta = $id_usuario;
       try{
            $promocion->save();
            flash('Se creo la Promocion')->success();
            return redirect()->route('reportePromocion');
	 	} catch(\Exception $e){
			DB::rollBack();
	    	flash('La promocion no se genero. Intentelo nuevamente !!')->error();
            return redirect()->back();
		} 
    }


    public function edit(Request $request,$id)
    {
        $promociones = Promocion::where('id',$id)->firstOrFail();
        return view('pages.mc.promociones.edit')->with(['promociones'=>$promociones]);	
    }


    public function doEdit(Request $request)
    {
        $check_in = explode('/', $request->input('fecha_inicio'));
        $check_out = explode('/', $request->input('fecha_vencimiento'));
        $inicio = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]);
        $vencimiento = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]);
        try{
            DB::table('promociones')
                ->where('id',$request->input('id'))
                ->update([
                        'descripcion' => $request->input('descripcion'),
                        'fecha_alta' => $inicio,
                        'fecha_baja' => $vencimiento,
                        'porcentaje_comision' => str_replace(',','.', str_replace('.','',$request->input('porcentaje_comision'))),
                        'activo' => $request->input('activo')
                        ]);
            flash('Se edito la Promoción')->success();
            return redirect()->route('reportePromocion');
        } catch(\Exception $e){
            DB::rollBack();
            flash('La Promoción no se ha editado. Intentelo nuevamente !!')->error();
            return redirect()->back();
        } 
            



    }

}
