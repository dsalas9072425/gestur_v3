<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use App\ReservasVuelos;
use App\Usuario;
use App\Agencias;
use App\Logrqrs;
use Redirect;
use Session;
use DB;


class ConfirmationController extends Controller
{
	public function index(Request $req){
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes
		$selectedHotel = Session::get('selectedHotel');
			$client = new Client();
		$agentesDTP = explode('_', $req->input('agente'));
		$datosHabitacion = $req->input('habitacion');
		$iibReq = new \StdClass;
		$iibReq->token = $req->session()->get('datos-loggeo')->token;
		$datosLoggeo = Session::get('datos-loggeo');
		if($datosLoggeo->datos->datosUsuarios->idAgencia == config('constants.adminDtp')) $iibReq->totalCobrado = $req->input('monto');
		$iibReq->clienteReferencia = $req->input('clienteReferencia');
		$iibReq->facturar = $req->has('facturar') && $req->input('facturar') == 'true' ? true : false;
		$iibReq->nroProforma = $req->input('expediente');
		//Indica los datos del Titular
		$titular = new \StdClass;
		$titular->nombre = $datosHabitacion[1]['ad'][1]['nombre'];
		$titular->apellido = $datosHabitacion[1]['ad'][1]['apellido'];
		$titular->tipoDocumento = $req->input('tipoDocumento_1');
		$titular->numeroDocumento = $req->input('numeroDocumento_1');
		$iibReq->titular = $titular;
		$habsConf = [];
		foreach($selectedHotel->habitaciones as $k=>$habitacion){
			$k++;		
			$habConf = new \StdClass;
			foreach($habitacion->tarifas as $key=>$tarifa){	
				$key++;

				$indice = $key + 1;
				$habConf->codReserva = $tarifa->codReserva;
				$habConf->descsPasajeros = [];
				$habConf->comentarioPasajero = $req->input('comentario_'.$k);
				for($j =0 ; $j < $tarifa->ocupanciaDtp->adultos ; $j++){
					$indice  = $j;
					$indice++;
					$descPasajero = new \StdClass;
					$descPasajero->nombre = $datosHabitacion[$k]['ad'][$indice]['nombre'];
					$descPasajero->apellido = $datosHabitacion[$k]['ad'][$indice]['apellido'];
					$descPasajero->tipo = 'AD';
					$descPasajero->edad = $datosHabitacion[$k]['ad'][$indice]['edad'];
					$habConf->descsPasajeros[] = $descPasajero;
				}

				foreach($tarifa->ocupanciaDtp->edadesNinhos as $llave=>$edadNinho){
					$pay =  $llave++;
					$descPasajero = new \StdClass;
					$descPasajero->nombre = $datosHabitacion[$k]['ch'][$llave]['nombre'];
					$descPasajero->apellido = $datosHabitacion[$k]['ch'][$llave]['apellido'];
					$descPasajero->tipo = 'CH';
					$descPasajero->edad = $datosHabitacion[$k]['ch'][$llave]['edad'];
					$habConf->descsPasajeros[] = $descPasajero;
					
				}
			}
			$iibReq->habsConf[] = $habConf;
		}
		$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();

		$datosLogueo = Session::get('datos-loggeo');
		$iibReq->idAgenteDtp = $agentesDTP[1];
		$logger = new Logger('Log');
		$logger->pushHandler(new \Monolog\Handler\StreamHandler(storage_path() . '/log/'. 'hotelesConfirm' . '.log'), Logger::DEBUG);
		
		$stack = HandlerStack::create();
		$stack->push(Middleware::log($logger, new MessageFormatter(
					'{method} {uri} HTTP/{version} REQUEST: {req_body}'. 'RESPONSE: {code} - {res_body}'
		)));

		$client = new Client(
			[
				'handler' => $stack
			]
		);		
		try{
			Session::put('selectedHotel',null);
			//por mas que no se use aca anulamos por si acaso
			Session::put('searchData',null);
			$iibRsp = $client->post(Config::get('config.iibConfirmation'), [
				'json' => $iibReq
			]);

			$iibObjRsp = json_decode($iibRsp->getBody());

			if($iibObjRsp->codRetorno !=0){
				$respuesta = new \StdClass;
				$respuesta->codigo = $iibObjRsp->codRetorno;
				$respuesta->mensaje = 'Póngase en contacto con su vendedor DTP para verificar el estado de esta reserva.';
				return view('pages.hoteles.bookings')->with(['respuesta'=>$respuesta]);
			}
		}catch(\Exception $e){
				$respuesta = new \StdClass;
				$respuesta->codigo = $iibObjRsp->codRetorno;
				$respuesta->mensaje = 'Póngase en contacto con su vendedor DTP para verificar el estado de esta reserva.';
				return view('pages.hoteles.bookings')->with(['respuesta'=>$respuesta]);
		}

		$selectedRates = array();
		foreach($iibObjRsp->habitaciones as $habitacion){
			foreach($habitacion->tarifas as $tarifa){
				$tarifa->roomCode = $habitacion->codHabitacion;
				$tarifa->roomName = $habitacion->desHabitacion;
				$tarifa->codConsumicion = $habitacion->tarifas[0]->codConsumicion;
				$tarifa->desConsumicion = $habitacion->tarifas[0]->desConsumicion;
				$selectedRates[] = $tarifa;
			}
		}
		
		$iibObjRsp->selectedRates = $selectedRates;
		$iibObjRsp->fechaReserva = (new \DateTime())->format('d/m/Y');
		Session::forget('marcador');
		Session::forget('urlFull');
		Session::forget('urlTotal');
		Session::forget('urlPreconf');
		return view('pages.hoteles.booking')->with(['confRsp'=> $iibObjRsp,
		'fechaDesde' => $iibObjRsp->fechaDesde,
		'fechaHasta' => $iibObjRsp->fechaHasta]);
	}
    
    public function bookingFlight(Request $req){
    	
    	/*if($req->session()->get('ultimaReserva') !== null){
			flash('<b>Cierre Reserva</b></br> La reservación de registro ya existe.')->error();
			return redirect('/home#flights-tab');
    	}*/
		include("../comAerea.php");
		include("../aeronaves.php");
		include("../aeropuertos.php");
		include("../tipo_asiento.php");
    	
    	$saveResult = new \StdClass;
    	$saveRqRs = new \StdClass;
    	$saveRqRs->compraRq = $req->session()->get('compReq');
    	$saveRqRs->compraRs = $req->session()->get('compRes');
    	$result =  new \StdClass;
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json',
						   ]
		]);
		$canNinhoBase = 0;
		$canAdultosBase = 0;
   	   	$ocupancias =  $req->session()->get('firstIibReq')->paxReference;

   	   	if(isset($ocupancias[0])){
	   	   	$canAdultosBase = count($ocupancias[0]->traveller);
	   	}
	  	if(isset($ocupancias[1])){
	   	   	$canNinhoBase = count($ocupancias[1]->traveller);
	   	}
   	
    	$iibReq = new \StdClass;
    	$pasajers= new \StdClass;
    	$iibReq->idRequest = $req->session()->get('token');
		$iibReq->sessionStatus= $req->session()->get('security')->sessionStatus;
		$iibReq->sessionId = $req->session()->get('security')->sessionId;
		$iibReq->seqNumber = $req->session()->get('security')->seqNumber;
		$iibReq->securyToken = $req->session()->get('security')->securyToken;
		$iibReq->pnrActions->optionCode[0] = 0;
		$originDestDetails = new \StdClass;
		$originDestDetails->originDestination = "";
		$originDestinationDetails = [];
		$originDestinationDetails[] = $originDestDetails;
		$indiceAdultos = 0;
   	   	$indiceNinho = 0;
   	   	$indiceInfante = 0;
   	   	$indicePasajero = 0;
   	   	$refNum = $canAdultosBase + $canNinhoBase+1;
   	   	$pasajObjSave = [];
   	   	$cantidadAdultos= 0;
   	   	$cantidadNinho= 0;
   	   	$cantidadInfante= 0;
   	   	foreach($ocupancias as $key=>$ocupancia){
   	   		$tipo = $ocupancia->ptc[0];
	   	   	switch ($ocupancia->ptc[0]){
				case "ADT":
					$indice = 0;
					$cantidadAdultos= count($ocupancia->traveller);
					for($i=0; $i<$cantidadAdultos; $i++){
						$pasajerosSave = new \StdClass;
						$indice = $indice + 1;
						$indicePasajero++;
						$base = 'adt'; 
						$travellerInfo[]= $this->armarInformacionTraveler($tipo, $req, $indiceAdultos, $indicePasajero, $base);
						if($req->input('adt_telefono_'.$i) != null){
							$dataElementsIndiv[]= $this->armarTelefono($req->input('adt_telefono_'.$i), $i, $refNum);
							$refNum = $refNum +1;
						}
						if($req->input('adt_mail_'.$i) != null){
							$dataElementsIndiv[]= $this->armarMail($req->input('adt_mail_'.$i), $i, $refNum);
							$refNum = $refNum +1;
						}
						if($req->input('adt_nrodoc_'.$i) != null){	
							$dataElementsIndiv[]= $this->armarDoc($req, $req->input('adt_nrodoc_'.$i), $i, $refNum, $indicePasajero, $base);
							$refNum = $refNum +1;
						}
						if($req->input('adt_comidaIda_'.$i) != null){	
							$ind = 'adt_comidaIda_';
							$dataElementsIndiv[]= $this->armarSr($req, $i, $indicePasajero, $refNum, $indicePasajero,$base, $ind);
							$refNum = $refNum +1;
						}	
						if($req->input('adt_asientoIda_'.$i) != null){	
							$ind = 'adt_asientoIda_';
							$dataElementsIndiv[]= $this->armarSr($req, $i, $indicePasajero, $refNum, $indicePasajero,$base, $ind);
							$refNum = $refNum +1;
						}	
						$datosSsr= $this->armarSsr($req, $i, $indicePasajero, $refNum, $indicePasajero,$base);
						$dataElementsIndiv[]= $this->armarSsr($req, $i, $indicePasajero, $refNum, $indicePasajero,$base);
						$refNum = $refNum +1;
						$result->pasajeros[] = $indicePasajero.". ".$req->input('adt_nombre_'.$i).", ".$req->input('adt_apellidos_'.$i);
						$pasajerosSave->nombre = $req->input('adt_nombre_'.$i);
						$pasajerosSave->apellido = $req->input('adt_apellidos_'.$i);
						$pasajerosSave->tipo_documento_id = $req->input('adt_tipoDocuento0_'.$i);
						$pasajerosSave->n_documento = $req->input('adt_nrodoc_'.$i);
						$pasajerosSave->fecha_vencimiento = $req->input('adt_fechVenc_'.$i); 
						$pasajerosSave->fecha_nacimiento =$req->input('adt_fechNac_'.$i); 
						$pasajerosSave->email = $req->input('adt_nrodoc_'.$i); 
						$pasajerosSave->telefono = $req->input('adt_telefono_'.$i); 
						$pasajerosSave->tipo_pasajero = $req->input('adt_nrodoc_'.$i); 
						$pasajerosSave->reserva_id = '';
						$pasajerosSave->sexo = $req->input('adt_sexo_'.$i); 
						$pasajerosSave->n_pasajero =  $indicePasajero;
						$pasajerosSave->tipo =  "ADT";
						$pasajerosSave->freeText =  $datosSsr->serviceRequest->ssr->freetext[0];
						$pasajObjSave[] = $pasajerosSave;
						$indiceAdultos++;
					}	
					/*echo '<pre>';
					print_r($dataElementsIndiv);*/
					break;
				case "CHD":
					$cantidadNinho= count($ocupancia->traveller);
					for($i=0; $i<$cantidadNinho; $i++){
						$indice = $indice + 1;
						$base = 'chd';
						$indicePasajero++;
						$pasajerosSave = new \StdClass;
						$travellerInfo[]= $this->armarInformacionTraveler($tipo, $req, $indiceNinho, $indice, $base);
						if($req->input('chd_comidaIda_'.$i) != null){	
							$ind = 'chd_comidaIda_';
							$dataElementsIndiv[]= $this->armarSr($req, $i, $indicePasajero, $refNum, $indicePasajero,$base, $ind);
							$refNum = $refNum +1;
						}	
						if($req->input('chd_asientoIda_'.$i) != null){	
							$ind = 'chd_asientoIda_';
							$dataElementsIndiv[]= $this->armarSr($req, $i, $indicePasajero, $refNum, $indicePasajero,$base, $ind);
							$refNum = $refNum +1;
						}	
						$datosSsr= $this->armarSsr($req, $i, $indicePasajero, $refNum, $indiceInfante,$base);
						$dataElementsIndiv[]= $this->armarSsr($req, $i, $indicePasajero, $refNum, $indicePasajero,$base);
						$refNum = $refNum +1;
						$result->pasajeros[] = $indicePasajero.". ".$req->input('chd_nombre_'.$i).", ".$req->input('chd_apellidos_'.$i);
						$pasajerosSave->nombre = $req->input('chd_nombre_'.$i);
						$pasajerosSave->apellido = $req->input('chd_apellidos_'.$i);
						$pasajerosSave->tipo_documento_id = $req->input('chd_tipoDocuento0_'.$i);
						$pasajerosSave->n_documento = $req->input('chd_nrodoc_'.$i);
						$fecha_vencimiento = explode("/",$req->input('chd_fechVenc_'.$i));
						$pasajerosSave->fecha_vencimiento = $req->input('chd_fechVenc_'.$i); 
						$fecha_nacimiento = explode("/",$req->input('chd_fechNac_'.$i));
						$pasajerosSave->fecha_nacimiento = $req->input('chd_fechNac_'.$i); 
						$pasajerosSave->email = $req->input('chd_mail_'.$i); 
						$pasajerosSave->telefono = $req->input('chd_telefono_'.$i); 
						$pasajerosSave->tipo_pasajero = $req->input('chd_nrodoc_'.$i); 
						$pasajerosSave->reserva_id = '';
						$pasajerosSave->sexo = $req->input('chd_sexo_'.$i); 
						$pasajerosSave->n_pasajero =  $indicePasajero;
						$pasajerosSave->tipo =  "CHD";
						$pasajerosSave->freeText =  $datosSsr->serviceRequest->ssr->freetext[0];
						$pasajObjSave[] = $pasajerosSave;
						$indiceNinho++;
					}	
					break;
				case "INF":
					$cantidadInfante= count($ocupancia->traveller);
					$passengs = [];
					for($i=0; $i<$cantidadInfante; $i++){
						$pasajerosSave = new \StdClass;
						$indicePasajero++;
						$travellerInfo[$i]->passengerData[0]->travellerInformation->passenger[0]->infantIndicator = '3';
						$travellerInfo[$i]->passengerData[0]->travellerInformation->traveller->quantity = 2;
						$base = 'inf';
						$indiceInfante = $i + 1;
						$passenger = new \StdClass;
						$travelInfo = new \StdClass;
						$traveller = new \StdClass;
						$traveller->surname =  strtoupper($req->input('inf_apellidos_'.$i));
						$passeng = new \StdClass;
						$passeng = new \StdClass;
						$passeng->firstName = strtoupper($req->input('inf_nombre_'.$i));
						$passeng->type = "INF";
						$dateOfBirth = new \StdClass;
						$dateAndTimeDetails = new \StdClass;
						$dateAndTimeDetails->date = $this->fechaFormato($req->input('inf_fechNac_'.$i));
						$dateOfBirth->dateAndTimeDetails = $dateAndTimeDetails;
						$travellerInformation = new \StdClass;
						$travellerInformation->traveller = $traveller;
						$passengs[0] = $passeng;
						$travellerInformation->passenger = $passengs;
						$passenger->travellerInformation = $travellerInformation;
						$passenger->dateOfBirth = $dateOfBirth;

						$travellerInfo[$i]->passengerData[1] = $passenger;	
						$datosSsr= $this->armarSsr($req, $i, $indicePasajero, $refNum, $indiceInfante,$base);
						$dataElementsIndiv[]= $this->armarSsr($req, $i, $indicePasajero, $refNum, $indiceInfante,$base);
						$result->pasajeros[] = $indicePasajero.". ".$req->input('inf_nombre_'.$i).", ".$req->input('inf_apellidos_'.$i);

						$pasajerosSave->nombre = $req->input('inf_nombre_'.$i);
						$pasajerosSave->apellido = $req->input('inf_apellidos_'.$i);
						$pasajerosSave->tipo_documento_id = $req->input('inf_tipoDocuento0_'.$i);
						$pasajerosSave->n_documento = $req->input('inf_nrodoc_'.$i);
						$fecha_vencimiento = explode("/",$req->input('inf_fechVenc_'.$i));
						$pasajerosSave->fecha_vencimiento = $req->input('inf_fechVenc_'.$i); 
						$fecha_nacimiento = explode("/",$req->input('inf_fechNac_'.$i));
						$pasajerosSave->fecha_nacimiento =$req->input('inf_fechNac_'.$i); 
						$pasajerosSave->email = $req->input('inf_mail_'.$i); 
						$pasajerosSave->telefono = $req->input('inf_telefono_'.$i); 
						$pasajerosSave->tipo_pasajero = $req->input('inf_nrodoc_'.$i); 
						$pasajerosSave->reserva_id = '';
						$pasajerosSave->sexo = $req->input('inf_sexo_'.$i); 
						$pasajerosSave->n_pasajero =  $indicePasajero;
						$pasajerosSave->tipo =  "INF";
						$pasajerosSave->freeText =  $datosSsr->serviceRequest->ssr->freetext[0];
						$pasajObjSave[] = $pasajerosSave;
					}
					break;
			}
   	   	}
   	   	$result->segmentos =  $req->session()->get('segmentGroup');
   	   	$itinerarioSave = [];
   	   	foreach($result->segmentos as $key=>$localizacion){
   	   		$localicacionBoard = $this->localizacion($localizacion->segmentInformation->boardPointDetails->trueLocationId);
   	   		$localicacionBoard = explode("|", $localicacionBoard);
   	   		$localicacionOff = $this->localizacion($localizacion->segmentInformation->offpointDetails->trueLocationId);
   	   		$localicacionOff = explode("|", $localicacionOff);
			$boardPointDetails =  new \StdClass;
			$offpointDetails =  new \StdClass;
   	   		$boardPointDetails->ciudad = ucwords(strtolower($localicacionBoard[2]));
   	   		$boardPointDetails->pais = ucwords(strtolower($localicacionBoard[3]));
   	   		$offpointDetails->ciudad = ucwords(strtolower($localicacionOff[2]));
   	   		$offpointDetails->pais = ucwords(strtolower($localicacionOff[3]));

   	   		$result->segmentos[$key]->boardPointDetails = $boardPointDetails;
   	   		$result->segmentos[$key]->offpointDetails = $offpointDetails;
			
			$itinerariosSave = new \StdClass;
			$itinerariosSave->tramo =  $localizacion->segmentInformation->itemNumber;
			$itinerariosSave->origen = $localizacion->segmentInformation->boardPointDetails->trueLocationId;
			$itinerariosSave->fecha_origen = $localizacion->segmentInformation->flightDate->departureDate;
			$itinerariosSave->hora_origen = $localizacion->segmentInformation->flightDate->departureTime;
			$itinerariosSave->destino =  $localizacion->segmentInformation->offpointDetails->trueLocationId;
			$itinerariosSave->fecha_destino =  $localizacion->segmentInformation->flightDate->arrivalDate;
			$itinerariosSave->hora_destino =  $localizacion->segmentInformation->flightDate->arrivalTime;
			$itinerariosSave->carrier_company =  $localizacion->segmentInformation->companyDetails->marketingCompany;
			$itinerariosSave->marketing_company =  $localizacion->segmentInformation->companyDetails->operatingCompany;
			$itinerariosSave->clase = $localizacion->segmentInformation->flightIdentification->bookingClass;
			$itinerariosSave->tipo_aeronave =  $localizacion->segmentInformation->flightIdentification->flightNumber;
			$itinerarioSave[] = $itinerariosSave;
   	   	}
 	   
		$iibReq->travellerInfo = $travellerInfo;
		$iibReq->originDestinationDetails = $originDestinationDetails;
		$dataElementsMaster = new \StdClass;
		$dataElementsMaster->marker1 = "";
		$dataElementsMaster->dataElementsIndiv = $dataElementsIndiv;
		$iibReq->dataElementsMaster = $dataElementsMaster;

		/*echo '<pre>Add Elements';
		echo '<pre>';
		print_r(json_encode($iibReq));
		echo '<pre>';*/
		$saveRqRs->addElementsRq = json_encode($iibReq);
		try{
			$iibRsp = $client->post(Config::get('config.iibFligthAddElementConfirm'), [
												'json' => $iibReq
											]);
			}
		catch(RequestException $e){
			//return view('pages.timeErrorConexion');	
		}
		catch(ClientException $e){
			//return view('pages.errorConexion');;		
		}	
		$iibObjRsp = json_decode($iibRsp->getBody());	
		$saveRqRs->addElementsRs = json_encode($iibObjRsp);
		$seqNumber = $iibObjRsp->seqNumber;	
			
		$respuestaAddElement = $iibObjRsp;
		$result->detalleSegmentos = $respuestaAddElement->originDestinationDetails;
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));//originDestinationDetails
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';*/
 		if(isset($iibObjRsp->errorOrWarningCodeDetails[0]->errorDetails->errorCode)){
 			flash('<b>Ver Precio</b></br>'.$iibObjRsp->errorOrWarningCodeDetails[0]->errorWarningDescription->freeText[0])->error();
			return redirect()->back()->with('data', $req->all());
 		}

		/*Son códigos fijos para Generar Reserva*/
		$iibReq = new \StdClass;
		$iibReq->idRequest = $req->session()->get('token');
		$iibReq->sessionStatus= $req->session()->get('security')->sessionStatus;
		$iibReq->sessionId = $req->session()->get('security')->sessionId;
		$iibReq->seqNumber = $seqNumber;
		$iibReq->securyToken = $req->session()->get('security')->securyToken;
		$pricingKey = new \StdClass;
		$pricingKey->pricingOptionKey = "RP";
		$pricingOption = new \StdClass;
		$pricingOption->pricingOptionKey = $pricingKey; 
		$iibReq->pricingOptionGroup[0] = $pricingOption;

		$pricingKey = new \StdClass;
		$pricingKey->pricingOptionKey = "RLO";
		$pricingOption = new \StdClass;
		$pricingOption->pricingOptionKey = $pricingKey; 
		$iibReq->pricingOptionGroup[1] = $pricingOption;

		$pricingKey = new \StdClass;
		$pricingKey->pricingOptionKey = "FCO";
		$pricingOption = new \StdClass;
		$pricingOption->pricingOptionKey = $pricingKey; 

		$firstCurrencyDetails = new \StdClass;
		$firstCurrencyDetails->currencyQualifier = "FCO";
		$firstCurrencyDetails->currencyIsoCode = "USD";
		$currency = new \StdClass;
		$currency->firstCurrencyDetails = $firstCurrencyDetails;
		$pricingOption->currency = $currency;
		$iibReq->pricingOptionGroup[2] = $pricingOption;

		/*echo '<pre>Reservar';
		echo '<pre>';
		print_r(json_encode($iibReq));*/
		/*die;*/
		$saveRqRs->reservarRs = json_encode($iibReq);
		try{
			$iibRsp = $client->post(Config::get('config.iibFligthRervarConfirm'), [ 
											'json' => $iibReq
										]);
		}
		catch(RequestException $e){
			//return view('pages.timeErrorConexion');	
		}
		catch(ClientException $e){
			//return view('pages.errorConexion');;		
		}									

		$iibObjRsp = json_decode($iibRsp->getBody());
		$saveRqRs->reservarRq = json_encode($iibObjRsp);
		$seqNumber = $iibObjRsp->seqNumber;	

		/*if(isset($iibObjRsp->applicationError->errorOrWarningCodeDetails->errorDetails->errorCode)){
			flash('<b>Reservar</b></br>'.$iibObjRsp->applicationError->errorWarningDescription->freeText[0])->error();
			return Redirect::back();
		}*/

		$tstTiket = $iibObjRsp->fareList;
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));
 		echo '<pre>';
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';//print_r(json_encode($tstTiket));	
		/*Fin Son códigos fijos para Generar Reserva*/
		if(isset($iibObjRsp->fareList[0]->lastTktDate)){
 			$reservar = $iibObjRsp->fareList[0]->lastTktDate;
		}else{
			flash('<b>Reservar</b></br>'.$iibObjRsp->applicationError->errorWarningDescription->freeText[0])->error();
			return redirect()->back()->with('data', $req->all());
		}

		$iibReq = new \StdClass;
		$iibReq->idRequest = $req->session()->get('token');
		$iibReq->sessionStatus= $req->session()->get('security')->sessionStatus;
		$iibReq->sessionId = $req->session()->get('security')->sessionId;
		$iibReq->seqNumber = $seqNumber;
		$iibReq->securyToken = $req->session()->get('security')->securyToken;
		foreach($tstTiket  as $key=>$tst){
			$psaL = new \StdClass;
			$referenceType = $tst->fareReference->referenceType;
			$uniqueReference = $tst->fareReference->uniqueReference;
			$itemReference =  new \StdClass;
			$itemReference->referenceType = $referenceType;
			$itemReference->uniqueReference = $uniqueReference;
			$psaL->itemReference= $itemReference;
			$psaList[] = $psaL;
		}
		$iibReq->psaList = $psaList;
		/*echo '<pre>Generate Ticket';
		echo '<pre>';
		print_r(json_encode($iibReq));*/
		$saveRqRs->generaTKRq = json_encode($iibReq);
		try{
			$iibRsp = $client->post(Config::get('config.iibFligthTicketConfirm'), [
											'json' => $iibReq
										]);
		}
		catch(RequestException $e){
			//return view('pages.timeErrorConexion');	
		}
		catch(ClientException $e){
			//return view('pages.errorConexion');;		
		}					
		$iibObjRsp = json_decode($iibRsp->getBody());
		$saveRqRs->generaTKRs = json_encode($iibObjRsp);
		$seqNumber = $iibObjRsp->seqNumber;	
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));
		echo '<pre>';
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';*/
		$iibReq = new \StdClass;
		$iibReq->idRequest = $req->session()->get('token');
		//$iibReq->sessionStatus= $req->session()->get('security')->sessionStatus;
		$iibReq->sessionStatus= $req->session()->get('security')->sessionStatus;
		$iibReq->sessionId = $req->session()->get('security')->sessionId;
		$iibReq->seqNumber = $seqNumber;
		$iibReq->securyToken = $req->session()->get('security')->securyToken;
		$optionCod = [];
		$optionCod[0] = 0;
		$dataElementsIndiv = [];

		$pnrActions = new \StdClass;
		$pnrActions->optionCode = $optionCod;
		$iibReq->pnrActions = $pnrActions;

		$originDestination = ""; 
		$originDestDet = new \StdClass;
		$originDestDet->originDestination = $originDestination;
		$originDestinationDetails[0] = $originDestDet;
		$iibReq->originDestinationDetails = $originDestinationDetails;
		$dataElementsMaster = new \StdClass;
		$dataElementsMaster->marker1 = "";
		$elementManagementData = new \StdClass;
		$elementManagementData->segmentName = "FP";
		$dataElementsInd = new \StdClass;
		$dataElementsInd->elementManagementData = $elementManagementData;
		$identification = "CA";
		$fop = [];
		$fp = new \StdClass;
		$fp->identification = $identification;
		$fop[0] = $fp;
		$formOfPayment = new \StdClass;
		$formOfPayment->fop =  $fop;
		$dataElementsInd->formOfPayment = $formOfPayment;
		$dataElementsIndiv[0] = $dataElementsInd;
		$reference = new \StdClass;
		$qualifier = "OT";
		$number = 20;
		$reference = new \StdClass;
		$reference->qualifier = $qualifier;
		$reference->number = $number;
		$elementManagementData = new \StdClass;
		$elementManagementData->reference = $reference;
		$elementManagementData->segmentName = "FM";
		$percentage = 6;
		$commissionInfo = new \StdClass;
		$commissionInfo->percentage = $percentage;
		$commission = new \StdClass;
		$commission->indicator = "M";
		$commission->commissionInfo = $commissionInfo;
		$dataElementsInd = new \StdClass;
		$dataElementsInd->elementManagementData = $elementManagementData;
		$dataElementsInd->commission = $commission;
		$dataElementsIndiv[1] = $dataElementsInd;
		$dataElementsMaster->dataElementsIndiv = $dataElementsIndiv;
		$iibReq->dataElementsMaster = $dataElementsMaster;
		/*echo '<pre>EnviaFormaPagoyComision';
		echo '<pre>';
		print_r(json_encode($iibReq));*/
		$saveRqRs->EnPagoRq = json_encode($iibReq);
		try{
			$iibRsp = $client->post(Config::get('config.iibFligthAddElementResConfirm'), [
											'json' => $iibReq
										]);
		}
		catch(RequestException $e){
			//return view('pages.timeErrorConexion');	
		}
		catch(ClientException $e){
			//return view('pages.errorConexion');;		
		}					
		$iibObjRsp = json_decode($iibRsp->getBody());
		$saveRqRs->EnPagoRs = json_encode($iibObjRsp);
		$seqNumber = $iibObjRsp->seqNumber;
///////////////////////////////////////
		$itinerario = new \StdClass;
 		$originDestinationDetail = $iibObjRsp->originDestinationDetails[0]->itineraryInfo;
 		foreach($originDestinationDetail as $llave=>$itineraryInfo){
 			$itinerario = new \StdClass;
 			$itinerario->vuelo = $itineraryInfo->travelProduct->companyDetail->identification." ".$itineraryInfo->travelProduct->productDetails->identification;
 			$itinerario->localizador_aerolínea = $seqNumber;
 			$itinerario->classOfService = $itineraryInfo->travelProduct->productDetails->classOfService;
 			$origen =  $this->localizacion($itineraryInfo->travelProduct->boardpointDetail->cityCode);
 			$destino =  $this->localizacion($itineraryInfo->travelProduct->offpointDetail->cityCode);
 			
 			$origenFormateado =  explode("|",$origen);
 			$destinoFormateado =  explode("|",$destino);

 			$itinerario->origen = ucwords(strtolower($origenFormateado[2])).", ".ucwords(strtolower($origenFormateado[3]))." - ".ucwords(strtolower($origenFormateado[1]));
 			$itinerario->destino = ucwords(strtolower($destinoFormateado[2])).", ".ucwords(strtolower($destinoFormateado[3]))." - ".ucwords(strtolower($destinoFormateado[1]));
			$itinerario->fechaIn =  $itineraryInfo->travelProduct->product->depDate;
			$itinerario->fechaOut = $itineraryInfo->travelProduct->product->arrDate;
			$itinerario->fechaInicio = $itineraryInfo->travelProduct->product->depDate;
			$itinerario->horaIn = date("G:i",strtotime($itineraryInfo->travelProduct->product->depTime));
			$itinerario->horaOut = date("G:i",strtotime($itineraryInfo->travelProduct->product->arrTime));
			$itinerario->diff  = $this->diffFechaHora($itineraryInfo->travelProduct->product->depDate,$itineraryInfo->travelProduct->product->depTime, $itineraryInfo->travelProduct->product->arrDate,$itineraryInfo->travelProduct->product->arrTime);
			if(isset($tarifa[$itineraryInfo->travelProduct->productDetails->classOfService])){
				$itinerario->tipo_asiento = $tarifa[$itineraryInfo->travelProduct->productDetails->classOfService];
			}else{
				$itinerario->tipo_asiento = "No disponible";
			}

			if(isset($comAerea[$itineraryInfo->travelProduct->companyDetail->identification])){
 				$itinerario->aerolinea = $comAerea[$itineraryInfo->travelProduct->companyDetail->identification];
			}else{
 				$itinerario->aerolinea = "No disponible";
			}
 			$result->itinerario[] = $itinerario;	
 		}
////////////////////////////////////////////

		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));
 		echo '<pre>';
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';*/
		$iibReq = new \StdClass;
		$iibReq->idRequest = $req->session()->get('token');
		$iibReq->sessionStatus= $req->session()->get('security')->sessionStatus;
		$iibReq->sessionId = $req->session()->get('security')->sessionId;
		$iibReq->seqNumber = $seqNumber;
		$iibReq->securyToken = $req->session()->get('security')->securyToken;
		$optionCode = [];
		$optionCode[0] = "11";
		$pnrActions = new \StdClass;
		$pnrActions->optionCode= $optionCode;
		$iibReq->pnrActions = $pnrActions;
		$dataElementsMaster  = new \StdClass;
		$dataElementsMaster->marker1 = "";
		$dataElements = new \StdClass; 

		$dataElementsIndiv = [];
		$reference = new \StdClass;
		$reference->qualifier = "OT";
		$reference->number = "1";
		$elementManagementData = new \StdClass;
		$elementManagementData->reference = $reference;
		$elementManagementData->segmentName = "TK";
 		$dataElements->elementManagementData = $elementManagementData;
 		$ticketElement = new \StdClass;
 		$ticket = new \StdClass;
 		$ticket->indicator = "XL";
 		$fechaHora = $this->lastTk($reservar);
 		$dateTime = explode("|",$fechaHora);

 		$ticket->date = $dateTime[0];
 		$ticket->time = $dateTime[1];
 		$ticketElement->ticket = $ticket;
 		$dataElements->ticketElement = $ticketElement;
 		$dataElementsIndiv[0] = $dataElements; 

 		$elementManagementData = new \StdClass;
 		$dataElements = new \StdClass; 

 		$elementManagementData->segmentName = "RF";
		$dataElements->elementManagementData = $elementManagementData;
		$freetextDetail = new \StdClass;
 		$freetextDetail->subjectQualifier = 3;
 		$freetextDetail->type = "P22";
		$longFreetext = new \StdClass;
		$longFreetext = "WebServices";
		$freetextData = new \StdClass;
		$freetextData->freetextDetail = $freetextDetail;
		$freetextData->longFreetext =$longFreetext;
		$dataElements->freetextData = $freetextData;
 		$dataElementsIndiv[1] = $dataElements; 
 		$dataElementsMaster->dataElementsIndiv = $dataElementsIndiv;
 		$iibReq->dataElementsMaster = $dataElementsMaster;
		/*echo '<pre>Cierre Reserva';
		echo '<pre>';
		print_r(json_encode($iibReq));*/
		$saveRqRs->CierreRq = json_encode($iibObjRsp);
		$saveRqRs->CierreRS ="";
		$datosLogueo = Session::get('datos-loggeo');
		$resevaSave = new \StdClass;;
		$resevaSave->cod_origen =  $req->session()->get('origen');
		$resevaSave->cod_destino = $req->session()->get('destino');
		$resevaSave->fecha_origen = $req->session()->get('fechaIda');
		$resevaSave->fecha_destino = $req->session()->get('fechaVuelta');
		$resevaSave->cant_adultos = $cantidadAdultos;
		$resevaSave->cant_ninhos = $cantidadNinho;
		$resevaSave->cant_infante = $cantidadInfante;
		$resevaSave->total_monto_vuelo = $req->input('totalMonto');
		$resevaSave->proforma_id = $req->input('expediente');
		$agentesDTP = explode('_', $req->input('agente'));
		$resevaSave->agente_dtp_id = $agentesDTP[0];
		$resevaSave->usuario_id = $datosLogueo->datos->datosUsuarios->idUsuario;
		$resevaSave->agencia_id = $datosLogueo->datos->datosUsuarios->idAgencia;
		$resevaSave->idRequest = $req->session()->get('token');
		$resevaSave->sessionId = $req->session()->get('security')->sessionId;
		$resevaSave->securyToken = $req->session()->get('security')->securyToken;
		$resevaSave->tipo_vuelo_id = $req->session()->get('tipo');
		$resevaSave->pocentaje_comision = $percentage;
		$resevaSave->controlNumber = "null";
		$resevaSave->hora_reserva = "null";
		$resevaSave->reservacion_company = $req->session()->get('marketingCompany');
		$resevaSave->marketingcompany =  $req->session()->get('marketingCompany');
		$resevaSave->typeOfPnrElement ="null";
		$resevaSave->agente_id ="null";
		$resevaSave->id_oficina =  "null";
		$resevaSave->codigo_iata = "null";
		$resevaSave->estado_id = 1;
		$saveResult->logRqRs = $saveRqRs;
		$saveResult->resevaSave = $resevaSave;
		$saveResult->itinerarioSave = $itinerarioSave;
		$saveResult->pasajerosSave = $pasajObjSave;
		$guardarId = $this->guardarReserva($saveResult);
		/*echo '<pre>';
		print_r($guardarId);
		die;*/
		/*echo '<pre>';
 		echo '-------------------------------------JSON PARA GUARDAR-------------------------------------------';
 		echo '<pre>';
 		print_r(json_encode($saveResult));
 		echo '<pre>';
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';*/
		try{
			$iibRsp = $client->post(Config::get('config.iibFligthAddElementConfirm'), [
											'json' => $iibReq
										]);
		}
		catch(RequestException $e){
			DB::table('reservas_vuelos')
					            ->where('id',$guardarId)
					            ->update([
					            			'estado_id'=>3
					            			]);
		}
		catch(ClientException $e){
			DB::table('reservas_vuelos')
					            ->where('id',$guardarId)
					            ->update([
					            			'estado_id'=>3
					            			]);
			return view('pages.errorConexion');		
		}			
		$iibObjRsp = json_decode($iibRsp->getBody());
		$seqNumber = $iibObjRsp->seqNumber;
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));
 		echo '<pre>';
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';*/

////////////////////////////////////////////////////////////////////////////////////////////////
				//Aqui es donde se arma el objeto
////////////////////////////////////////////////////////////////////////////////////////////////
		$iibObjRsp = json_decode($iibRsp->getBody());
		$seqNumber = $iibObjRsp->seqNumber;
		$reservaHeader = $iibObjRsp->pnrHeader[0];
		$saveResult->resevaSave->id = $guardarId;
		$saveResult->resevaSave->marketingcompany = $req->session()->get('marketingCompany');
 		$saveResult->resevaSave->controlNumber = $reservaHeader->reservationInfo->reservation[0]->controlNumber;
 		$saveResult->resevaSave->fecha_reserva = $reservaHeader->reservationInfo->reservation[0]->date;
		$saveResult->resevaSave->hora_reserva = $reservaHeader->reservationInfo->reservation[0]->time;
		$saveResult->resevaSave->reservacion_company = $reservaHeader->reservationInfo->reservation[0]->companyId;
		$saveResult->resevaSave->typeOfPnrElement =  $iibObjRsp->securityInformation->responsibilityInformation->typeOfPnrElement;
		$saveResult->resevaSave->agente_id =  $iibObjRsp->securityInformation->responsibilityInformation->agentId;
		$saveResult->resevaSave->id_oficina =  $iibObjRsp->securityInformation->responsibilityInformation->officeId;
		$saveResult->resevaSave->codigo_iata =  $iibObjRsp->securityInformation->responsibilityInformation->iataCode;
		$saveResult->logRqRs->CierreRS =json_encode($iibObjRsp);
		$result->controlNumber = $reservaHeader->reservationInfo->reservation[0]->controlNumber;
		$req->session()->put('ultimaReserva', $result->controlNumber);
		
		/*echo '<pre>';
 		echo '-------------------------------------JSON PARA GUARDAR-------------------------------------------';
 		echo '<pre>';
 		print_r(json_encode($saveResult));
 		echo '<pre>';
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';*/
		$this->guardarReserva($saveResult);
		/*if($req->session()->get('ultimaReserva') == $reservaHeader->reservationInfo->reservation[0]->controlNumber){
			flash('<b>Cierre Reserva</b></br> La reservación de registro '.$reservaHeader->reservationInfo->reservation[0]->controlNumber.' ya existe.')->error();
			return redirect('/home#flights-tab');
		}
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));
 		echo '<pre>';
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';
 		$itinerario = new \StdClass;
 		$originDestinationDetail = $iibObjRsp->originDestinationDetails[0]->itineraryInfo;
 		foreach($originDestinationDetail as $llave=>$itineraryInfo){
 			$itinerario = new \StdClass;
 			$itinerario->vuelo = $itineraryInfo->travelProduct->companyDetail->identification." ".$itineraryInfo->travelProduct->productDetails->identification;
 			$itinerario->localizador_aerolínea = $itineraryInfo->travelProduct->companyDetail->identification." ".$reservaHeader->reservationInfo->reservation[0]->controlNumber;
 			$itinerario->classOfService = $itineraryInfo->travelProduct->productDetails->classOfService;
 			$origen =  $this->localizacion($itineraryInfo->travelProduct->boardpointDetail->cityCode);
 			$destino =  $this->localizacion($itineraryInfo->travelProduct->offpointDetail->cityCode);
 			
 			$origenFormateado =  explode("|",$origen);
 			$destinoFormateado =  explode("|",$destino);

 			$itinerario->origen = ucwords(strtolower($origenFormateado[2])).", ".ucwords(strtolower($origenFormateado[3]))." - ".ucwords(strtolower($origenFormateado[1]));
 			$itinerario->destino = ucwords(strtolower($destinoFormateado[2])).", ".ucwords(strtolower($destinoFormateado[3]))." - ".ucwords(strtolower($destinoFormateado[1]));
			$itinerario->fechaIn =  $itineraryInfo->travelProduct->product->depDate;
			$itinerario->fechaOut = $itineraryInfo->travelProduct->product->arrDate;
			$itinerario->fechaInicio = $itineraryInfo->travelProduct->product->depDate;
			$itinerario->horaIn = date("G:i",strtotime($itineraryInfo->travelProduct->product->depTime));
			$itinerario->horaOut = date("G:i",strtotime($itineraryInfo->travelProduct->product->arrTime));
			$itinerario->diff  = $this->diffFechaHora($itineraryInfo->travelProduct->product->depDate,$itineraryInfo->travelProduct->product->depTime, $itineraryInfo->travelProduct->product->arrDate,$itineraryInfo->travelProduct->product->arrTime);
			if(isset($tarifa[$itineraryInfo->travelProduct->productDetails->classOfService])){
				$itinerario->tipo_asiento = $tarifa[$itineraryInfo->travelProduct->productDetails->classOfService];
			}else{
				$itinerario->tipo_asiento = "No disponible";
			}

			if(isset($comAerea[$itineraryInfo->travelProduct->companyDetail->identification])){
 				$itinerario->aerolinea = $comAerea[$itineraryInfo->travelProduct->companyDetail->identification];
			}else{
 				$itinerario->aerolinea = "No disponible";
			}
 			$result->itinerario[] = $itinerario;	
 		}

 		$iibReq = new \StdClass;
 		$iibReq->idRequest = $req->session()->get('token');
		$iibReq->sessionStatus= $req->session()->get('security')->sessionStatus;
		$iibReq->sessionId = $req->session()->get('security')->sessionId;
		$iibReq->seqNumber = $seqNumber;
		$iibReq->securyToken = $req->session()->get('security')->securyToken;
		$statusDetails = new \StdClass;
		$statusDetails->indicator = "ET";
		$switches = new \StdClass;
		$switches->statusDetails = $statusDetails;
		$optionG = new \StdClass;
		$optionG->switches = $switches;
		$optionGroup = [];
		$optionGroup[0] = $optionG;
		$iibReq->optionGroup = $optionGroup;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//					Extraer estos datos 
////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/*echo '<pre>Emitir Tiket';
		echo '<pre>';
		print_r(json_encode($iibReq));
		try{		
			$iibRsp = $client->post(Config::get('config.iibFligthpasEmisionConfirm'), [
											'json' => $iibReq
										]);
		}
		catch(RequestException $e){
			//return view('pages.timeErrorConexion');	
		}
		catch(ClientException $e){
			$error = $e;
			$iibReqLog->response = $iibRsp;
			$iibReqLog->estado = "ERROR";
			$iibReqLog->stackTrace = $error; 
			$iibReqLog->fechaHoraResponse = date('Y-m-d H:i:s');
			$token_idRequest = (int)$req->session()->get('subNumero')+1;
			$req->session()->put('subNumero', $token_idRequest);
			//return view('pages.errorConexion');;		
		}			
		$iibObjRsp = json_decode($iibRsp->getBody());
		$seqNumber = $iibObjRsp->seqNumber;
		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));
 		echo '<pre>';
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';
 		$iibReq = new \StdClass;
 		$iibReq->idRequest = $req->session()->get('token');
		//$iibReq->sessionStatus= $req->session()->get('security')->sessionStatus;
		$iibReq->sessionStatus= "End";
		$iibReq->sessionId = $req->session()->get('security')->sessionId;
		$iibReq->seqNumber = $seqNumber;
		$iibReq->securyToken = $req->session()->get('security')->securyToken;
		$retrieve = new \StdClass;
		$retrieve->type = "2";
		$retrievalFacts= new \StdClass;
		$retrievalFacts->retrieve = $retrieve;
		$controlNumber= $reservaHeader->reservationInfo->reservation[0]->controlNumber;
		$reserv = new \StdClass;
		$reserv->controlNumber= $controlNumber;
		$result->controlNumber = $controlNumber;
		$req->session()->put('ultimaReserva', $result->controlNumber);

		$reservation= [];
		$reservation[0] = $reserv;
		$reservationOrProfileIdentifier = new \StdClass;
		$reservationOrProfileIdentifier->reservation = $reservation;
		$retrievalFacts->reservationOrProfileIdentifier =  $reservationOrProfileIdentifier;
		$iibReq->retrievalFacts = $retrievalFacts;
		/*echo '<pre>Recuperar Reserva';
		echo '<pre>';
		print_r(json_encode($iibReq));
		try{		
			$iibRsp = $client->post(Config::get('config.iibFligthverLocalizaConfirm'), [
											'json' => $iibReq
										]);
		}
		catch(RequestException $e){
			
			//return view('pages.timeErrorConexion');	
		}
		catch(ClientException $e){
			
			//return view('pages.errorConexion');;		
		}			
		$iibObjRsp = json_decode($iibRsp->getBody());
		$seqNumber = $iibObjRsp->seqNumber;
	/*	echo '<pre>';
		print_r(json_encode($iibObjRsp));
 		echo '<pre>';
 		echo '--------------------------------------------------------------------------------------------';
 		echo '<pre>';
		$iibReq = new \StdClass;
		$iibReq->idRequest = $req->session()->get('token');
		$iibReq->sessionStatus= "End";
		$iibReq->sessionId = $req->session()->get('security')->sessionId;
		$iibReq->seqNumber = $seqNumber;
		$iibReq->securyToken = $req->session()->get('security')->securyToken;
		$iibReq->Security_SignOut = "";
		echo '<pre>Security SignOut';
		echo '<pre>';
		print_r(json_encode($iibReq));
		try{	
			$iibRsp = $client->post(Config::get('config.iibFligthSignOffResConfirm'), [
											'json' => $iibReq
										]);
		}
		catch(RequestException $e){
			//return view('pages.timeErrorConexion');	
		}
		catch(ClientException $e){
			//return view('pages.errorConexion');;		
		}			
		$iibObjRsp = json_decode($iibRsp->getBody());
		echo '<pre>';
		print_r(json_encode($iibObjRsp));
		echo '<pre>';
		/*print_r(json_encode($result));*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//					Fin Extraer estos datos 
////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//$req->session()->put('subNumero', 0);
    	return view('pages.vuelos.bookingFlight')->with('resultado', $result);
    }	

    	private function armarInformacionTraveler($tipo, $req, $base, $number, $prefix){
			$travellerInfo = [];
	      	$reference = new \StdClass;
			$reference->qualifier= "PR";
			$reference->number= $number;
			$elementManagementPassenger = new \StdClass;
			$elementManagementPassenger->reference = $reference;
			$elementManagementPassenger->segmentName = "NM";
			$travelInfo = new \StdClass;
			$travelInfo->elementManagementPassenger = $elementManagementPassenger;
			$traveller = new \StdClass;
			$traveller->surname =  strtoupper($req->input($prefix.'_apellidos_'.$base));
			$traveller->quantity = 1;
			$travellerInformation = new \StdClass;
			$travellerInformation->traveller = $traveller;
			$passenger = new \StdClass;
			$passenger->travellerInformation = $travellerInformation;
			$passeng = new \StdClass;
			$passeng->firstName = strtoupper($req->input($prefix.'_nombre_'.$base));
			$passeng->type = $tipo;
			$passengerData[0] = $passenger;
			$travelInfo->passengerData = $passengerData;
			if($tipo == "CHD"){
				$dateOfBirth = new \StdClass;
				$dateAndTimeDetails = new \StdClass;
				$dateAndTimeDetails->date = $this->fechaFormato($req->input($prefix.'_fechNac_'.$base));
				$dateOfBirth->dateAndTimeDetails = $dateAndTimeDetails;
				$passenger->dateOfBirth = $dateOfBirth;
			}
			$pass= [];
			$pass[] = $passeng;
			$travellerInformation->passenger = $pass;
			return $travelInfo; 
	}	

     private function armarSr($req, $indice,$indicePasajero, $refNum, $number, $base, $tipo){

		$reference = new \StdClass;
		$reference->qualifier = "OT";
		$reference->number = $refNum;
		$elementManagementData = new \StdClass;
		$elementManagementData->reference = $reference;
		$elementManagementData->segmentName = "SR";
		$ssr = new \StdClass;
		$ssr->status= "HK";
		$ssr->type = $req->input($base.''.$tipo.''.$indice);
		$ssr->quantity = 1;
		$ssr->companyId = $req->session()->get('marketingCompany');
		$serviceRequest = new \StdClass;
		$serviceRequest->ssr = $ssr;
		$dataElementsI =  new \StdClass;
		$dataElementsI->elementManagementData = $elementManagementData;
		$dataElementsI->serviceRequest = $serviceRequest;
		return $dataElementsI;
     }	


     private function armarSsr($req, $indice,$indicePasajero, $refNum, $number, $base){
		$reference = new \StdClass;
		$reference->qualifier = "OT";
		$reference->number = $refNum;
		$elementManagementData = new \StdClass;
		$elementManagementData->reference = $reference;
		$elementManagementData->segmentName = "SSR";
		$ssr = new \StdClass;
		$ssr->status= "HK";
		$ssr->type = "DOCS";
		$ssr->quantity = 1;
		$ssr->companyId = $req->session()->get('marketingCompany');
		$freetext = [];
		$sexo =  $req->input($base.'_sexo_'.$indice);
		if( $req->input($base.'_fechVenc_'.$indice) != null){
			$vencimiento = $this->fechaFormato($req->input($base.'_fechVenc_'.$indice));	
		}else{
			$vencimiento = "27JUL21";
		}

		$vencimiento = 
		$freetext[0] = "P-PRY-".$req->input($base.'_nrodoc_'.$indice)."-PRY-".$this->fechaFormato($req->input($base.'_fechNac_'.$indice))."-".$sexo."-".$vencimiento."-".str_replace(" ", "", strtoupper($req->input($base.'_apellidos_'.$indice)))."-".str_replace(" ", "", strtoupper($req->input($base.'_nombre_'.$indice)))."/P".$indicePasajero;

		$ssr->freetext = $freetext;
		$serviceRequest = new \StdClass;
		$serviceRequest->ssr = $ssr;

		$dataElementsI =  new \StdClass;
		$dataElementsI->elementManagementData = $elementManagementData;
		$dataElementsI->serviceRequest = $serviceRequest;

		$ref = new \StdClass;
		$ref->qualifier = "PR";
		$ref->number = $number;
		$reference = [];
		$reference[] = $ref;
		$referenceForDataElement = new \StdClass;
		$referenceForDataElement->reference = $reference;
		$dataElementsI->referenceForDataElement = $referenceForDataElement;
		return $dataElementsI;
     }	

     private function armarTelefono($telefono, $indice, $number){
		$reference = new \StdClass;
		$reference->qualifier = "OT";
		$reference->number = $number;
		$elementManagementData = new \StdClass;
		$elementManagementData->reference = $reference;
		$elementManagementData->segmentName = "AP";

		$freetextDetail = new \StdClass;
		$freetextDetail->subjectQualifier= 3;
		$freetextDetail->type =  6;
		$freetextData = new \StdClass;
		$freetextData->freetextDetail = $freetextDetail;
		$freetextData->longFreetext = $telefono;

		$dataElementsI =  new \StdClass;
		$dataElementsI->elementManagementData = $elementManagementData;
		$dataElementsI->freetextData = $freetextData;
		return $dataElementsI;
	 }	

     private function armarMail($mail, $indice, $number){
		$reference = new \StdClass;
		$reference->qualifier = "OT";
		$reference->number = $number;
		$elementManagementData = new \StdClass;
		$elementManagementData->reference = $reference;
		$elementManagementData->segmentName = "AP";

		$freetextDetail = new \StdClass;
		$freetextDetail->subjectQualifier= 3;
		$freetextDetail->type = "P02";
		$freetextData = new \StdClass;
		$freetextData->freetextDetail = $freetextDetail;
		$freetextData->longFreetext = $mail;

		$dataElementsI =  new \StdClass;
		$dataElementsI->elementManagementData = $elementManagementData;
		$dataElementsI->freetextData = $freetextData;
 		return $dataElementsI;	
     }

     private function armarDoc($req, $doc, $indice, $refNum, $number, $base){
     	/*echo '<pre>';
     	print_r($req->all());
     	echo '<pre>';
     	print_r($indice);
     	echo '<pre>';
     	print_r($base); */    	
     	$reference = new \StdClass;
		$reference->qualifier = "OT";
		$reference->number = $refNum;
		$elementManagementData = new \StdClass;
		$elementManagementData->reference = $reference;
		$elementManagementData->segmentName = "SSR";

		$ssr = new \StdClass;
		$ssr->type = "FOID";
		$ssr->status= "HK";
		$ssr->quantity = 1;
		$ssr->companyId = Session::get('marketingCompany');
		$freetext = [];
		//$req->input($base.'_nrodoc_'.$indice)
		$freetext[0] = $req->input($base.'_tipoDocuento0_'.$indice)."".$doc;		
		$ssr->freetext = $freetext;
		$serviceRequest = new \StdClass;
		$serviceRequest->ssr = $ssr;

		$dataElementsI =  new \StdClass;
		$dataElementsI->elementManagementData = $elementManagementData;
		$dataElementsI->serviceRequest = $serviceRequest;

		$ref = new \StdClass;
		$ref->qualifier = "PR";
		$ref->number = $number;
		$reference = [];
		$reference[] = $ref;
		$referenceForDataElement = new \StdClass;
		$referenceForDataElement->reference = $reference;
		$dataElementsI->referenceForDataElement = $referenceForDataElement;
 		return $dataElementsI;	
	}	

     private function armarInfante($number, $indice, $req, $base){
      	if($indice != 0){
      		$travellerInfo = [];
	      	$reference = new \StdClass;
			$reference->qualifier= "PR";
			$reference->number= $number;
			$elementManagementPassenger = new \StdClass;
			$elementManagementPassenger->reference = $reference;
			$elementManagementPassenger->segmentName = "NM";
			$travelInfo = new \StdClass;
			$travelInfo->elementManagementPassenger = $elementManagementPassenger;

			$traveller = new \StdClass;
			$traveller->surname =  strtoupper($req->input('adt_apellido_'.$base));
			$traveller->quantity = 1;
			$travellerInformation = new \StdClass;
			$travellerInformation->traveller = $traveller;
			$passenger = new \StdClass;
			$passenger->travellerInformation = $travellerInformation;
			$passeng = new \StdClass;
			$passeng->firstName = strtoupper($req->input('adt_nombre_'.$base));
			$passeng->type = "ADT";
			$passeng->infantIndicator = 1;
			$passengerData[0] = $passenger;
			$travelInfo->passengerData = $passengerData;

			$pass= [];
			$pass[] = $passeng;
			$travellerInformation->passenger = $pass;
			$travellerInfo[] = $travelInfo; 

			$traveller = new \StdClass;
			$traveller->surname =  strtoupper($req->input('inf_apellido_'.$base));
			$traveller->quantity = 1;
			$travellerInformation = new \StdClass;
			$travellerInformation->traveller = $traveller;
			$passenger = new \StdClass;
			$passenger->travellerInformation = $travellerInformation;
			$passeng = new \StdClass;
			$passeng->firstName = strtoupper($req->input('inf_nombre_'.$base));
			$passeng->type = "INF";
			$passeng->infantIndicator = 0;
			$dateAndTimeDetails = new \StdClass;
			$fechaNacimiento =$this->fechaFormato($req->input('inf_fechNac_'.$base));
			$dateAndTimeDetails->date = $fechaNacimiento; 
			$dateOfBirth = new \StdClass;
			$dateOfBirth->dateAndTimeDetails = $dateAndTimeDetails;
			$passenger->dateOfBirth = $dateOfBirth;;
			$passengerData[1] = $passenger;
			$travelInfo->passengerData = $passengerData;
			$pass= [];
			$pass[] = $passeng;
			$travellerInformation->passenger = $pass;
		}else{
			$travellerInfo = [];
	      	$reference = new \StdClass;
			$reference->qualifier= "PR";
			$reference->number= $number;
			$elementManagementPassenger = new \StdClass;
			$elementManagementPassenger->reference = $reference;
			$elementManagementPassenger->segmentName = "NM";
			$travelInfo = new \StdClass;
			$travelInfo->elementManagementPassenger = $elementManagementPassenger;

			$traveller = new \StdClass;
			$traveller->surname =  strtoupper($req->input('adt_apellido_'.$base));
			$traveller->quantity = 1;
			$travellerInformation = new \StdClass;
			$travellerInformation->traveller = $traveller;
			$passenger = new \StdClass;
			$passenger->travellerInformation = $travellerInformation;
			$passeng = new \StdClass;
			$passeng->firstName = strtoupper($req->input('adt_nombre_'.$base));
			$passeng->type = "ADT";
			$passengerData[0] = $passenger;
			$travelInfo->passengerData = $passengerData;

			$pass= [];
			$pass[] = $passeng;
			$travellerInformation->passenger = $pass;
		}	
		return $travelInfo; 			
	}	

	private function fechaFormato($fecha){
		$fecNac = explode("/", $fecha);
		include("../mes.php");
		$mes = $mes[$fecNac[1]];
		return $fecNac[0]."".$mes."".substr($fecNac[2], -2);
	}	
	private function lastTk($fechaHora){

		if(intval($fechaHora->dateTime->day) <= 9){
      		$diaFormato = "0".$fechaHora->dateTime->day;
      	}else{
      		$diaFormato = $fechaHora->dateTime->day;
      	}
      	if(intval($fechaHora->dateTime->month) <= 9){
      		$mesFormato = "0".$fechaHora->dateTime->month;
      	}else{
      		$mesFormato = $fechaHora->dateTime->month;
      	}
      	$anhoFormato = date("y", strtotime($fechaHora->dateTime->year));	

      	$fechaFormato =  $diaFormato."".$mesFormato."".$anhoFormato;

      	if(intval($fechaHora->dateTime->hour) <= 9){
      		$horaFormato = "0".$fechaHora->dateTime->hour;
      	}else{
      		$horaFormato = $fechaHora->dateTime->hour;
      	}
      	if(intval($fechaHora->dateTime->minutes) <= 9){
      		$minutosFormato = "0".$fechaHora->dateTime->minutes;
      	}else{
      		$minutosFormato = $fechaHora->dateTime->minutes;
      	}
      	$horarioFormato = $horaFormato."".$minutosFormato;
      	return $fechaFormato."|".$horarioFormato;
	}
	public function localizacion($localizacion){
		$aeropuerto = DB::select("SELECT * FROM aeropuerto where codigo_iata = '".$localizacion."'");
		$datosAeropuerto = $aeropuerto[0]->codigo_iata."|".$aeropuerto[0]->nombre."|".$aeropuerto[0]->localidad."|".$aeropuerto[0]->pais_eng;
		return $datosAeropuerto;
	}	
	public function ciudad($indice){	
        $ciudades = DB::select("SELECT * FROM ciudad_pais where codigo_iata = '".$indice."'");
        
		return  $ciudades[0]->codigo_iata."|".$ciudades[0]->nombreCiudad."|".$ciudades[0]->pais;          
	}
	public function asiento($indice){	
        $asientos = DB::select("SELECT * FROM tipos_lugares where codigo = '".$indice."'");
        
		return  $asientos[0]->nombre." (".$asientos[0]->codigo.")";          
	}	
	public function aerolinea($codigo){
			$aerolinea = DB::select("SELECT * FROM aerolineas where codigo_iata = '".$codigo."'");
			if(isset($aerolinea[0]->nombre)){
				$datosAerolinea = $aerolinea[0]->nombre;
			}else{
				$datosAerolinea = "Sin Registro";
			}
			return $datosAerolinea;
	}	
	public function restarHoras($horaini,$horafin){
		$horai=substr($horaini,0,2);
		$mini=substr($horaini,3,2);
		$segi=substr($horaini,6,2);
	 
		$horaf=substr($horafin,0,2);
		$minf=substr($horafin,3,2);
		$segf=substr($horafin,6,2);
	 
		$ini=((($horai*60)*60)+($mini*60)+$segi);
		$fin=((($horaf*60)*60)+($minf*60)+$segf);
	 
		$dif=$fin-$ini;
	 
		$difh=floor($dif/3600);
		$difm=floor(($dif-($difh*3600))/60);
		$difs=$dif-($difm*60)-($difh*3600);
		return date("H-i-s",mktime($difh,$difm,$difs));
	}
	private function diffFechaHora($date1,$hour1, $date2,$hour2){
		$timezone = new \DateTimeZone('UTC'); 
		$dateArrival = \DateTime::createFromFormat('dmY', $date1, $timezone);
        $dateDepature = \DateTime::createFromFormat('dmY', $date2, $timezone);	
		$dateR1 = new \DateTime('20'.$dateDepature->format('y-m-d')." ".date("G:i",strtotime($hour2)));
		$dateR2 = new \DateTime('20'.$dateArrival->format('y-m-d')." ".date("G:i",strtotime($hour1)));
		$diff = $dateR1->diff($dateR2);
		$duracion= "";
		if($diff->d != 0){
			$duracion.= $diff->d." d";
		}
		if($diff->h != 0){ 
			$duracion.= $diff->h." h";
		}
		if($diff->i != 0){
			$duracion.= $diff->i." min";
		}
		return $duracion;
	}
	private function guardarReserva($saveReserva){
		if(isset($saveReserva)){
			if(isset($saveReserva->resevaSave->id)){
				DB::table('reservas_vuelos')
					            ->where('id',$saveReserva->resevaSave->id)
					            ->update([
					            			'marketingcompany'=>$saveReserva->resevaSave->marketingcompany,
					            			'controlnumber' => $saveReserva->resevaSave->controlNumber,
					            			'fecha_reserva'=> date('d/m/Y'),
					            			'hora_reserva' =>$saveReserva->resevaSave->hora_reserva,
					            			'reservacion_company'=>$saveReserva->resevaSave->reservacion_company,
					            			'typeofpnrelement'=>$saveReserva->resevaSave->typeOfPnrElement,
					            			'agente_id' => $saveReserva->resevaSave->agente_id,
					            			'id_oficina'=>$saveReserva->resevaSave->id_oficina,
					            			'codigo_iata'=>$saveReserva->resevaSave->codigo_iata,
					            			'estado_id' => 2
					            			]);
				DB::table('logrqrs')
					            ->where('id',Session::get('idLog'))
					            ->update([
					            			'cierrers' => $saveReserva->logRqRs->CierreRS,
					            			]);
			}else{
			 		$values = array(
			 						'cod_origen' => $saveReserva->resevaSave->cod_origen,
			 						'cod_destino' => $saveReserva->resevaSave->cod_destino,
			 						'fecha_origen' => $saveReserva->resevaSave->fecha_origen,
			 						'fecha_destino' => $saveReserva->resevaSave->fecha_destino,
			 						'cant_ninhos' => $saveReserva->resevaSave->cant_ninhos,
			 						'cant_infante' => $saveReserva->resevaSave->cant_infante,
			 						'cant_adultos' => $saveReserva->resevaSave->cant_adultos,
			 						'total_monto_vuelo' => $saveReserva->resevaSave->total_monto_vuelo,
			 						'proforma_id' => $saveReserva->resevaSave->proforma_id,
			 						'agente_dtp_id' => $saveReserva->resevaSave->agente_dtp_id,
			 						'usuario_id' => $saveReserva->resevaSave->usuario_id,
			 						'idrequest' => $saveReserva->resevaSave->idRequest,
			 						'sessionid' => $saveReserva->resevaSave->sessionId,
			 						'securytoken' => $saveReserva->resevaSave->securyToken,
			 						'tipo_vuelo_id' => $saveReserva->resevaSave->tipo_vuelo_id,
			 						'pocentaje_comision' => $saveReserva->resevaSave->pocentaje_comision,
			 						'controlnumber' => $saveReserva->resevaSave->controlNumber,
			 						'fecha_reserva' => date('d/m/Y'),
			 						'hora_reserva' => $saveReserva->resevaSave->hora_reserva,
			 						'reservacion_company' => $saveReserva->resevaSave->reservacion_company,
			 						'typeofpnrelement' => $saveReserva->resevaSave->typeOfPnrElement,
			 						'agente_id' => $saveReserva->resevaSave->agente_id,
			 						'estado_id' => 3,
			 						'codigo_iata' => $saveReserva->resevaSave->codigo_iata,
			 						'id_oficina' => $saveReserva->resevaSave->id_oficina,
			 						'marketingcompany' => $saveReserva->resevaSave->marketingcompany,
			 						'agencia_id' => $saveReserva->resevaSave->agencia_id
			 						);	
		 		DB::table('reservas_vuelos')->insert($values);
				$reservaFligth= ReservasVuelos::all();
				if(isset($saveReserva->itinerarioSave)){
					foreach($saveReserva->itinerarioSave as $key=> $itinerarios){
						$value = array(
										'tramo' => $itinerarios->tramo,
										'origen' => $itinerarios->origen,
										'fecha_origen' => $itinerarios->fecha_origen,
										'hora_origen' => $itinerarios->hora_origen,
										'destino' => $itinerarios->destino,
										'fecha_destino' => $itinerarios->fecha_destino,
										'hora_destino' => $itinerarios->hora_destino,
										'carrier_company' => $itinerarios->carrier_company,
										'marketing_company' => $itinerarios->marketing_company,
										'reservas_vuelo_id' => $reservaFligth->last()->id,
										'clase' => $itinerarios->clase,
										'tipo_aeronave' => $itinerarios->tipo_aeronave
									);	
						DB::table('itinerarios')->insert($value);
					}
				}	
				if(isset($saveReserva->pasajerosSave)){
					foreach($saveReserva->pasajerosSave as $index=> $pasajeros){
						$val = array(
										'nombre' => $pasajeros->nombre,
										'apellido' => $pasajeros->apellido,
										'tipo_documento_id' => $pasajeros->tipo_documento_id,
										'n_documento' => $pasajeros->n_documento,
										'fecha_vencimiento' => $pasajeros->fecha_vencimiento,
										'fecha_nacimiento' => $pasajeros->fecha_nacimiento,
										'email' => $pasajeros->email,
										'telefono' => $pasajeros->telefono,
										'tipo_pasajero' => $pasajeros->tipo_pasajero,
										'reserva_id' => $reservaFligth->last()->id,
										'sexo' => $pasajeros->sexo,
										'n_pasajero' => $pasajeros->n_pasajero,
										'tipo' => $pasajeros->tipo,
										'freetext' => $pasajeros->freeText
									);	
						DB::table('pasajeros')->insert($val);
					}
				}
				if(isset($saveReserva->logRqRs)){
						$val = array(
										  'reserva_id' => $reservaFligth->last()->id,
										  'idrequest' => $saveReserva->resevaSave->idRequest,
										  'securytoken' => $saveReserva->resevaSave->securyToken,
										  'sessionid' => $saveReserva->resevaSave->sessionId,
										  'comprarq' => $saveReserva->logRqRs->compraRq,
										  'comprars' => $saveReserva->logRqRs->compraRs,
										  'addelementsrq' => $saveReserva->logRqRs->addElementsRq,
										  'addelementsrs' => $saveReserva->logRqRs->addElementsRs,
										  'reservarrq' => $saveReserva->logRqRs->reservarRq,
										  'reservarrs' => $saveReserva->logRqRs->reservarRs,
										  'generatkrq' => $saveReserva->logRqRs->generaTKRq,
										  'generatkrs' => $saveReserva->logRqRs->generaTKRs,
										  'enpagorq' => $saveReserva->logRqRs->EnPagoRq,
										  'enpagors' => $saveReserva->logRqRs->EnPagoRs,
										  'cierrerq' => $saveReserva->logRqRs->CierreRq,
										  'cierrers' => $saveReserva->logRqRs->CierreRS,
									);	
						DB::table('logrqrs')->insert($val);
				}			
			}
		}
		if(isset($reservaFligth)){
			$log= Logrqrs::all();
			Session::put('idLog', $log->last()->id);
			return 	$reservaFligth->last()->id;
		}
	}

}
