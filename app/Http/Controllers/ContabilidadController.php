<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Session;
use Redirect;
use App\Proforma;
use App\ProformasDetalle;
use App\Divisas;
use App\Persona;
use App\PlazoPago;
use App\HistoricoComentariosProforma;
use App\FormaCobroReciboCabecera;
use App\FormaCobroReciboDetalle;
use App\Voucher;
use App\TipoFactura;
use App\EstadoFactour;
use App\TipoPersona;
use App\Factura;
use App\FacturaDetalle;
use App\AdjuntoDocumento;
use App\OpDetalle;
use App\Empresa;
use App\Producto;
use App\OpCabecera;
use App\AsientoDetalle;
use App\Asiento;
use App\Currency;
use App\BancoDetalle;
use App\PlanCuenta;
use App\OpTimbrado;
use App\CentroCosto;
use App\SucursalEmpresa;
use App\TipoDocumentoHechauka;
use App\TipoFacturacion;
use App\Recibo;
use App\CuentaGasto;
use Response;
use Image;
use DB;
use App\Grupos;
use App\LibroCompra;
use App\LibroVenta;
use App\Retencion;
use App\OrigenAsiento;
use App\TipoProveedor;
use App\Traits\LibrosCompraTrait;
use App\Traits\LibrosVentasTrait;
use Illuminate\Support\Facades\Log;
use App\Exceptions\ExceptionCustom;
use Carbon\Carbon;
use App\Traits\OpTrait;
use App\FacturaDetalleHistorico;
use App\FacturaCabeceraHistorico;
use App\LibroCompraReprice;
use App\ProformaReprice;
use App\ProformaDetalleReprice;
use App\TimbradoLvManual;
use App\Negocio;

class ContabilidadController extends Controller
{

	use OpTrait;

	use LibrosCompraTrait,LibrosVentasTrait;
	
	private function getIdUsuario()
	{
 		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	}

	private function getIdEmpresa()
	{
 		return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}

	public function indexLibrosVentas(Request $request)
	{
		$getDivisa = Divisas::where('activo','S')->get();
		// $getProveedor = Persona::where('id_tipo_persona','11')->where('id_empresa',$this->getIdEmpresa())->get();
		$clientes = DB::select("SELECT * FROM personas 
		WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
									WHERE puede_facturar = true) 
		AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");
	   // $clientes = Factura::with('cliente')->where('id_empresa',$this->getIdEmpresa())->distinct()->get(['cliente_id']);
		$plazoPago = DB::select("SELECT distinct tipo_plazo as n FROM plazos_pago ");

		return view('pages.mc.contabilidad.indexLibrosVentas')->with(['monedas'=>$getDivisa, 'clientes'=>$clientes, 'plazoPago'=>$plazoPago]);
	}

	public function mostrarLibroVenta(Request $request)
	{
		$datos = $this->responseFormatDatatable($request);

		// dd($datos);
		$librosVentas = DB::table('v_libros_ventas');
		$librosVentas = $librosVentas->where('id_empresa',$this->getIdEmpresa());

		if($datos->idProveedor) $librosVentas = $librosVentas->where('cliente_id',$datos->idProveedor);
		if($datos->nroFactura) $librosVentas = $librosVentas->where('nro_documento',$datos->nroFactura);
		if($datos->desde_hasta != '')
			{
				$fechaTrim = trim($datos->desde_hasta);
				$periodo = explode('-', trim($fechaTrim));
				$desde = explode("/", trim($periodo[0]));
				$hasta = explode("/", trim($periodo[1]));
				$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
				$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
				$librosVentas = $librosVentas->whereBetween('fecha_hora', [$fecha_desde, $fecha_hasta]);
			}

        $librosVentas = $librosVentas->get();
        /*echo '<pre>';
      	print_r($librosVentas);*/
        $cuerpo = array();

		return response()->json(['data'=>$librosVentas]);
	}

	public function indexLibrosCompras(Request $request)
	{
		$getDivisa = Divisas::where('activo','S')->get();
		//$getProveedor = Persona::where('id_tipo_persona','14')->where('id_empresa',$this->getIdEmpresa())->get();
		$getProveedor = DB::select("SELECT * FROM personas 
						WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
									WHERE puede_facturar = true) 
						AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");

		$plazoPago = DB::select("SELECT distinct tipo_plazo as n FROM plazos_pago ");
		$productos = DB::select("select id, denominacion from productos where id_empresa = ".$this->getIdEmpresa()." and id in (select id_producto from libros_compras where id_producto is not null)");
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();
		// dd($cuentas_contables);
		return view('pages.mc.contabilidad.indexLibrosCompras')->with(['productos'=>$productos,'monedas'=>$getDivisa, 'getProveedor'=>$getProveedor, 'plazoPago'=>$plazoPago, 'cuentas_contables'=>$cuentas_contables]);
	}


	public function indexLibrosFondoFijo(Request $request)
	{
		$getDivisa = Divisas::where('activo','S')->get();
		$getProveedor = Persona::where('id_tipo_persona','14')->where('id_empresa',$this->getIdEmpresa())->get();
		$plazoPago = DB::select("SELECT distinct tipo_plazo as n FROM plazos_pago ");
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();
		return view('pages.mc.contabilidad.indexLibrosFondoFijo')->with(['monedas'=>$getDivisa, 'getProveedor'=>$getProveedor, 'plazoPago'=>$plazoPago, 'cuentas_contables'=>$cuentas_contables]);
	}

	public function indexMigracionAsientos(Request $request)
	{
		$origenAsientos = OrigenAsiento::orderBy('descripcion','ASC')->get();

		return view('pages.mc.contabilidad.migracionAsientos')->with(['origenAsientos'=>$origenAsientos]);
	}

	public function indexMayorCuentas(Request $request)
	{
		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'ASC')->get();
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();

		return view('pages.mc.contabilidad.mayorCuentas')->with(['cuentas_contables'=>$cuentas_contables, 'monedas'=>$currency]);
	}

	public function indexTesaka(Request $request)
	{
		$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);

		return view('pages.mc.contabilidad.tesaka')->with(['sucursalEmpresa'=>$sucursalEmpresa]);
	}

	public function consultarTesaka(Request $req)
	{
		$datos = $this->responseFormatDatatable($req);

		// dd($datos);

		if($datos->desde_hasta == '' && $datos->sucursal == '')
		{
			$tesaka = '';
		}
		else
		{
      		$tesaka = DB::table('vw_tesaka');

      		if($datos->desde_hasta != '')
			{
				$fechaTrim = trim($datos->desde_hasta);
				$periodo = explode('-', trim($fechaTrim));
				$desde = explode("/", trim($periodo[0]));
				$hasta = explode("/", trim($periodo[1]));
				$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
				$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
				$tesaka = $tesaka->whereBetween('fecha_retencion', [$fecha_desde, $fecha_hasta]);
			}

			if($datos->sucursal != '')
			{
	            $tesaka = $tesaka->where('id_sucursal',$datos->sucursal);
			}

	        $tesaka = $tesaka->get();
		}

	
		// dd($tesaka);
		return response()->json(['data'=>$tesaka]);
        

	}

	public function getCotizacionLc(Request $req)
	{
		// dd($req->input('id_moneda'));

		$cotizacion = DB::select("SELECT * FROM get_cotizacion_moneda(".$req->input('id_moneda').", ".$this->getIdEmpresa().")");

		// dd($cotizacion[0]->get_cotizacion_moneda);

		return response()->json(['data'=>$cotizacion[0]->get_cotizacion_moneda]);

	}


	public function getConsultaMigracionAsiento(Request $req)
	{
		$datos = $this->responseFormatDatatable($req);
	//	dd($datos->tipo_doc);

		if ($datos->tipo_doc == 1) //resumen
		{
			$migracionAsiento = DB::table('vw_migracion_asiento_resumen');
			$migracionAsiento = $migracionAsiento->where('id_empresa',$this->getIdEmpresa());

			if($datos->desde_hasta != '')
			{
				$fechaTrim = trim($datos->desde_hasta);
				$periodo = explode('-', trim($fechaTrim));
				$desde = explode("/", trim($periodo[0]));
				$hasta = explode("/", trim($periodo[1]));
				$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
				$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
            	$migracionAsiento = $migracionAsiento->whereBetween('fecha_asiento', [$fecha_desde, $fecha_hasta]);
			}

			if ($datos->id_origen_asiento != '') 
			{
				$migracionAsiento = $migracionAsiento->where('id_origen_asiento', $datos->id_origen_asiento);
			}

			if ($datos->asiento_anulado == '0') 
			{
				$migracionAsiento = $migracionAsiento->where('activo', true);
			}
			else
			{
				$migracionAsiento = $migracionAsiento->where('activo', false);
			}

        	$migracionAsiento = $migracionAsiento->get();

		}
		else
		{
			$migracionAsiento = DB::table('vw_migracion_asiento_detalle');
			$migracionAsiento = $migracionAsiento->where('id_empresa',$this->getIdEmpresa());

			 if ($datos->id_origen_asiento != '') 
			{
				$migracionAsiento = $migracionAsiento->where('id_origen_asiento', $datos->id_origen_asiento);
			} 

			if ($datos->asiento_anulado == '0') 
			{
				$migracionAsiento = $migracionAsiento->where('activo', true);
			}
			else
			{
				$migracionAsiento = $migracionAsiento->where('activo', false);
			}
			if($datos->desde_hasta != '')
			{
				$fechaTrim = trim($datos->desde_hasta );
				$periodo = explode('-', trim($fechaTrim));
				$desde = explode("/", trim($periodo[0]));
				$hasta = explode("/", trim($periodo[1]));
				$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
				$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
				$migracionAsiento = $migracionAsiento->whereBetween('fecha_asiento', [$fecha_desde, $fecha_hasta]);

			}

			$migracionAsiento = $migracionAsiento->get();
		}
		// dd($migracionAsiento);
		return response()->json(['data'=>$migracionAsiento]);

	}

	public function getSaldoCuenta(Request $req)
	{
		$datos = $this->responseFormatDatatable($req);
		$saldoCuenta = 0;

		if ($datos->id_cuenta_exenta != '') 
		{
			$data2 = DB::table('asientos_contables as a');
			$data2 = $data2->select(DB::raw("CASE WHEN SUM(d.debe-d.haber) < 0 THEN SUM(d.debe-d.haber) *-1ELSE SUM(d.debe-d.haber)END"));
	        $data2 = $data2->leftJoin('asientos_contables_detalle as d', 'd.id_asiento_contable', '=','a.id');
	        $data2 = $data2->where('a.id_empresa',$this->getIdEmpresa());
	        $data2 = $data2->where('a.activo', true);
			$data2 = $data2->where('d.id_cuenta_contable', $datos->id_cuenta_exenta);
       		$data2 = $data2->get();
		//	dd($data2); 

       		$saldoCuenta = $data2[0]->saldo;

			// dd($saldoCuenta);
		}

		return response()->json(['data'=>$saldoCuenta]);

		// dd($saldoCuenta);
	}



	public function getConsultaMayorCuenta(Request $req)
	{
		$datos = $this->responseFormatDatatable($req);

		$saldoCuenta = 0;
		$data = DB::table('vw_mayor_cuenta');
		$data = $data->where('id_empresa',$this->getIdEmpresa());
        $data = $data->where('asiento_activo', true);
        $data = $data->where('detalle_activo', true); 

		if ($datos->id_cuenta_exenta != '') 
		{
			$data = $data->where('id_cuenta_contable', $datos->id_cuenta_exenta);
		}
		if ($datos->idMoneda != '') 
		{
			$data = $data->where('id_moneda', $datos->idMoneda);
		}

		if ($datos->desde_hasta != '') 
		{
			$fechaTrim = trim($datos->desde_hasta);
			$periodo = explode('-', trim($fechaTrim));
			$desde = explode("/", trim($periodo[0]));
			$hasta = explode("/", trim($periodo[1]));
			$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
			$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';

			$data = $data->whereBetween('fecha_documento', [$fecha_desde, $fecha_hasta]);

		}

		$asientos = $data->get();

		return response()->json(['data'=>$asientos]);

	}
	

	public function mostrarLibroCompra(Request $req)
	{

		$datos = $this->responseFormatDatatable($req);
		//$libroCompra = DB::table('v_libros_compras');
		$libroCompra = DB::table('vw_libro_compra');
		$libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());

        if($datos->idProveedor!= '')
            $libroCompra = $libroCompra->where('id_proveedor',$datos->idProveedor);
       

	            if($datos->nroFactura != '')
	            {
					$libroCompra = $libroCompra->where('nro_documento','like', '%' .trim($datos->nroFactura).'%');
	            	
	            }
	            if($datos->idProducto != '')
	            {
            		$libroCompra = $libroCompra->where('id_producto',$datos->idProducto);
	            }

	            if($datos->codigoConfirmacion != '')
	            {
            		$libroCompra = $libroCompra->where('cod_confirmacion',$datos->codigoConfirmacion);
	            }

	            if($datos->idProducto != '')
	            {
            		$libroCompra = $libroCompra->where('id_producto',$datos->idProducto);
	            }


	            if($datos->idMoneda != '')
	            {
            		$libroCompra = $libroCompra->where('id_moneda',$datos->idMoneda);
				}

				if($datos->idLcv != '')
	            {
            		$libroCompra = $libroCompra->where('id',$datos->idLcv);
				}

				
				if($datos->estado_lc)
	            {
	            	if ($datos->estado_lc == 1) 
	            	{
	                	$libroCompra = $libroCompra->where('activo', true);
	            	}
	            	else
	            	{
	            		$libroCompra = $libroCompra->where('activo', false);
	            	}
	            }

	            if($datos->proveedor_desde_hasta != '')
		        {
					$fechaTrim = trim($datos->proveedor_desde_hasta);
					$periodo = explode('-', trim($fechaTrim));
					$desde = explode("/", trim($periodo[0]));
					$hasta = explode("/", trim($periodo[1]));
					$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
					$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
            		$libroCompra = $libroCompra->whereBetween('fecha_creacion', [$fecha_desde, $fecha_hasta]);
		        }
				if(isset($datos->pago_desde_hasta)){
					if($datos->pago_desde_hasta != '')
					{
						$fechaTrim = trim($datos->pago_desde_hasta);
						$periodo = explode('-', trim($fechaTrim));
						$desde = explode("/", trim($periodo[0]));
						$hasta = explode("/", trim($periodo[1]));
						$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
						$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
						$libroCompra = $libroCompra->whereBetween('fecha_notificado_nemo', [$fecha_desde, $fecha_hasta]);
					}
				}

	            if($datos->creacion_desde_hasta != '')
		        {
					$fechaTrim = trim($datos->creacion_desde_hasta);
					$periodo = explode('-', trim($fechaTrim));
					$desde = explode("/", trim($periodo[0]));
					$hasta = explode("/", trim($periodo[1]));
					$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
					$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
            		$libroCompra = $libroCompra->whereBetween('fecha', [$fecha_desde, $fecha_hasta]);
		        }

				if($datos->tipo_proveedor != '')
	            {
					$libroCompra = $libroCompra->where('tipo_proveedor', $datos->tipo_proveedor);
				}
				if($datos->origen != '')
	            {
					$libroCompra = $libroCompra->where('origen', $datos->origen);
				}

		        if($datos->mes != '')
		        {
			        if($datos->anho)
			        {
			            $libroCompra = $libroCompra->where('mes', trim($datos->mes))->where('anho', $datos->anho);
			         }
			      }   
           	    if($datos->idProforma)
	            {
	                $libroCompra = $libroCompra->where('id_proforma', $datos->idProforma);
	            } 

				if($datos->tiene_diferencia != '')
		        {
					if($datos->tiene_diferencia == 'S'){
						$libroCompra = $libroCompra->where('diferencia', '<>' ,0);
					}else{
						$libroCompra = $libroCompra->where('diferencia', 0);
					}
				} 

	            $libroCompra = $libroCompra->get();

 				if($datos->id_cuenta_exenta)
	            {
	            	$arrayLC = array();
	            	foreach($libroCompra as $lc){
				      	if($lc->id_cuenta_exenta == $datos->id_cuenta_exenta || $lc->id_cuenta_gravada_10 == $datos->id_cuenta_exenta || $lc->id_cuenta_gravada_5 == $datos->id_cuenta_exenta){
				      			$arrayLC[] = $lc;
				      	}
	            	}
	            	$libroCompra = $arrayLC;
       			}

		return response()->json(['data'=>$libroCompra]);
	}

	public function mostrarLibroCompraFijo(Request $req)
	{

		$datos = $this->responseFormatDatatable($req);
	
		$libroCompra = DB::table('v_libros_compras');
		$libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
		$libroCompra = $libroCompra->where('fondo_fijo',true);

        if($datos->idProveedor!= '')
            $libroCompra = $libroCompra->where('id_proveedor',$datos->idProveedor);
       

	            if($datos->nroFactura != '')
	            {
					$libroCompra = $libroCompra->where('nro_documento','like', '%' .trim($datos->nroFactura).'%');
	            	
	            }

	            if($datos->codigoConfirmacion != '')
	            {
            		$libroCompra = $libroCompra->where('cod_confirmacion',$datos->codigoConfirmacion);
	            }

	            if($datos->idMoneda != '')
	            {
            		$libroCompra = $libroCompra->where('id_moneda',$datos->idMoneda);
				}
				
				if($datos->estado_lc)
	            {
	            	if ($datos->estado_lc == 1) 
	            	{
	                	$libroCompra = $libroCompra->where('activo', true);
	            	}
	            	else
	            	{
	            		$libroCompra = $libroCompra->where('activo', false);
	            	}
	            }

	            if($datos->id_cuenta_exenta)
	            {

	                $libroCompra = $libroCompra->where('id_cuenta_exenta', $datos->id_cuenta_exenta);
	            	$libroCompra = $libroCompra->orWhere('id_cuenta_gravada_10', $datos->id_cuenta_exenta);
	            	$libroCompra = $libroCompra->orWhere('id_cuenta_gravada_5', $datos->id_cuenta_exenta);
	            }
	          
	            if($datos->proveedor_desde_hasta != '')
		        {
					$fechaTrim = trim($datos->proveedor_desde_hasta);
					$periodo = explode('-', trim($fechaTrim));
					$desde = explode("/", trim($periodo[0]));
					$hasta = explode("/", trim($periodo[1]));
					$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0];
					$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0];
            		$libroCompra = $libroCompra->whereBetween('fecha', [$fecha_desde, $fecha_hasta]);
		        }
	            
            		// $libroCompra = $libroCompra->where('id','!=', null);

	            
        $libroCompra = $libroCompra->get();
// dd($libroCompra);

		return response()->json(['data'=>$libroCompra]);
	}


	public function verDetalle(Request $request)
	{
		$resultado = [];
		$op = OpDetalle::with('opCabecera.proveedor', 'libroCompra', 'opCabecera.currency')
						->where('id_libro_compra', $request->input('idLibroCompra'))
						->get()->toArray();
		if(empty($op)){
			$resultado = $op;
		}else{
			foreach($op as $key=>$valores){
				if($valores['op_cabecera']['id_estado'] != 54){
					$resultado[] = $valores;
				}

			}
		}				
		return response()->json(['data'=>$resultado]);
	}

	public function verDetalleLc(Request $request){
		
		$libroCompra = DB::table('v_libros_compras');
		$libroCompra = $libroCompra->where('id',$request->input('idLibroCompra'));
		$libroCompra = $libroCompra->get();
		return response()->json(['data'=>$libroCompra]);
	}

	public function verAsiento(Request $request)
	{
		$asientosContables = LibroVenta::with('asiento')->with('asiento.usuario', 'asiento.asientoDetalle.moneda', 'asiento.asientoDetalle.asiento', 'asiento.asientoDetalle.asiento.usuario', 'asiento.asientoDetalle.cuentaContable')->
		where('id', $request->input('idLibroVenta'))->get()->toArray();
		// dd($asientosContables);
		return response()->json(['data'=>$asientosContables[0]['asiento']['asiento_detalle']]);
	}

	public function verAsientoCompra(Request $request)
	{
		// dd($request->all());
		$asientosContables = LibroCompra::with('asiento')->with('asiento.usuario', 'asiento.asientoDetalle.moneda', 'asiento.asientoDetalle.asiento', 'asiento.asientoDetalle.asiento.usuario', 'asiento.asientoDetalle.cuentaContable')->
		where('id', $request->input('idLibroCompra'))->get()->toArray();

		return response()->json(['data'=>$asientosContables[0]['asiento']['asiento_detalle']]);
	}

	private function  responseFormatDatatable($req)
  	{
	    $datos = array();
	    $data =  new \StdClass;
	    
	    foreach ($req->formSearch as $key => $value) 
	    {
	     	$n = $value['name'];
	        $datos[$value['name']] = $value['value'];
	        $data-> $n = $value['value'];
	    }

	    return $data;
	 } 
	 
	 /**
	  * Cargar Facturas de Compra en el Libro compra
	  */
	 public function  addLibroCompra(Request $req){

		$id_canje = (integer)$req->input('canje');
		// 
		$forma_cobro_data = [];
		if($id_canje){
			$forma_cobro_data = DB::table('vw_forma_cobro_listado')->where('id',$id_canje)->first();
		}
			// dd($forma_cobro_data);

		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'ASC')->get();
		$tipoDocumentos = TipoDocumentoHechauka::where('activo',true)->orderBy('orden', 'ASC')->get();
		$empresa = Empresa::where('id', $this->getIdEmpresa())->first(['fecha_limite_lc']);
		$fecha_libre = $empresa->fecha_limite_lc;

		$banco = BancoDetalle::/*whereHas('banco_cabecera', function ($query) {
		  $query->where('id_empresa', $this->getIdEmpresa());
	  })->*/with('currency','tipo_cuenta')
		->get();
		$cliente = DB::select("SELECT p.id,p.nombre, p.apellido, p.documento_identidad,p.dv,tp.denominacion, p.activo,p.id_pais,
								concat(p.nombre,' ',p.apellido,' ','- '|| trim(p.denominacion_comercial)) as full_data
								FROM personas p JOIN tipo_persona tp on tp.id = p.id_tipo_persona
								WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
								AND p.activo = true and p.id_empresa = ".$this->getIdEmpresa()." order by p.nombre asc");

		$tipo_factura = TipoFactura::orderBy('id','DESC')->get();
		$centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();  
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();
		$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
		$permiso_modificar_fecha = $this->datosPermiso('verLibroCompra',60);

		return view('pages.mc.contabilidad.addLibroCompra',compact('fecha_libre','sucursalEmpresa','centro','banco','currency','cliente','cuentas_contables','tipo_factura','forma_cobro_data','tipoDocumentos','permiso_modificar_fecha'));
	 }//
	 
	 /*
	  * Cargar Facturas de Compra en el Libro compra
	  */
	 public function  addLibroCompraFijo(Request $req){

		$id_canje = (integer)$req->input('canje');
		// 
		$forma_cobro_data = [];
		if($id_canje){
			$forma_cobro_data = DB::table('vw_forma_cobro_listado')->where('id',$id_canje)->first();
		}
			// dd($forma_cobro_data);

		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$banco = BancoDetalle::/*whereHas('banco_cabecera', function ($query) {
		  $query->where('id_empresa', $this->getIdEmpresa());
	  })->*/with('currency','tipo_cuenta')
		->get();
		$cliente = DB::select("
		SELECT p.id,p.nombre, p.apellido, p.documento_identidad,p.dv,tp.denominacion, p.activo,
		concat(p.nombre,' ',p.apellido,' ','- '|| trim(p.denominacion_comercial)) as full_data
		FROM personas p JOIN tipo_persona tp on tp.id = p.id_tipo_persona
		WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
		AND p.activo = true and p.id_empresa = ".$this->getIdEmpresa()." order by p.nombre asc");

		$tipoDocumentos = TipoDocumentoHechauka::where('activo',true)->orderBy('orden', 'ASC')->get();

		$tipo_factura = TipoFactura::orderBy('id','DESC')->get();
		$centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();  
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();
		$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
		

		return view('pages.mc.contabilidad.addLibroCompraFijo',compact('sucursalEmpresa','centro','banco','currency','cliente','cuentas_contables','tipo_factura','forma_cobro_data','tipoDocumentos'));
	 }//


	 public function obtenerDataClienteLc(Request $req){
		$timbrado = [];
		$ruc = 0;
		$existe = false;
		$costo_venta = 'false';
		

				//DEVOLVER UN LISTADO DE TIMBRADO
				$timbrados = TimbradoLvManual::where('id_empresa',$this->getIdEmpresa())->get(['id','numero','denominacion']);

				if($req->input('id_proveedor') != '' && $req->input('id_proveedor') != 0){

					$personas = DB::table('personas')->where('id_empresa',$this->getIdEmpresa())
								->where('id',$req->input('id_proveedor'))->get(['documento_identidad','tipo_proveedor','dv']);
					
					$ruc = '';
					if(count($personas) > 0){
						$documento = $personas[0]->documento_identidad;
						$dv = '';
						if($personas[0]->dv != ''){
							$dv = "-".$personas[0]->dv;
						}
						$ruc = $documento."".$dv;
					}

					if(count($personas) > 0 && $personas[0]->tipo_proveedor != ""){
						$tipoProveedores = TipoProveedor::where('id',$personas[0]->tipo_proveedor)->first(['costo_venta']);
						if($tipoProveedores->costo_venta == true || $tipoProveedores->costo_venta == 1){
							$costo_venta = 'true';
						}else{
							$costo_venta = 'false';
						}
					}else{
						$costo_venta = 'false';
	
					}
				}
				

				
		
		return response()->json(['data'=>$timbrados,'ruc'=>$ruc,'costo_venta'=>$costo_venta]);
	 }


	 public function obtenerDataCliente(Request $req){
		$timbrado = [];
		$ruc = 0;
		$existe = false;
		$costo_venta = 'false';
		

				//DEVOLVER UN LISTADO DE TIMBRADO
				$timbrados = TimbradoLvManual::where('id_empresa',$this->getIdEmpresa())->get(['id','numero','denominacion']);

				if($req->input('id_proveedor') != '' && $req->input('id_proveedor') != 0){

					$personas = DB::table('personas')->where('id_empresa',$this->getIdEmpresa())
								->where('id',$req->input('id_proveedor'))->get(['documento_identidad','tipo_proveedor','dv']);
					
					$ruc = '';
					if(count($personas) > 0){
						$documento = $personas[0]->documento_identidad;
						$dv = '';
						if($personas[0]->dv != ''){
							$dv = "-".$personas[0]->dv;
						}
						$ruc = $documento."".$dv;
					}

					if(count($personas) > 0 && $personas[0]->tipo_proveedor != ""){
						$tipoProveedores = TipoProveedor::where('id',$personas[0]->tipo_proveedor)->first(['costo_venta']);
						if($tipoProveedores->costo_venta == true || $tipoProveedores->costo_venta == 1){
							$costo_venta = 'true';
						}else{
							$costo_venta = 'false';
						}
					}else{
						$costo_venta = 'false';
	
					}
				}
				

				
		
		return response()->json(['data'=>$timbrados,'ruc'=>$ruc,'costo_venta'=>$costo_venta]);
	 }


	 private function validar($var){
		$var = trim($var);

		if($var !== NULL && $var != ''){
			return $var;
		} else {
			return NULL;
		}

	}


	//AQUI
	 public function saveLibroCompra(Request $req)
	 {
				$id_op = 0;
				$libro = 0;
				$err = true;
				$idUsuario = $this->getIdUsuario();
				$idEmpresa = $this->getIdEmpresa();
				$msj = [];
				$retencionConsulta = new \StdClass; 
				$get_tipo_documento = DB::select("SELECT get_documento_ctte(".$req->input('id_tipo_comprobante').",".$req->input('id_tipo_documento').", ".$idEmpresa.")");
				$tipo_documento = $get_tipo_documento[0]->get_documento_ctte;
				$porcentaje_retencion = null;
				$base_imponible = null;
				$retencion = null;
				$total_neto_pago = 0;
				$es_canje = ($req->input('id_canje')) ? true: false;
				$pais = Persona::where('id_empresa',$this->getIdEmpresa())->find($req->input('id_cliente'));				
				$cuenta_contable_proveedor = 0;
				$log_info = "(USERID:".$this->getIdUsuario()." METOD:saveLibroCompra)".json_encode($req->all());
				$log_error = "(USERID:".$this->getIdUsuario()." METOD:saveLibroCompra) ";

				//try{
					Log::info($log_info);
					//OBTIENE CUENTA CONTABLE SEGUN CASO
					if($es_canje){
						$cuenta_contable_proveedor = DB::select("SELECT get_id_cuenta_contable('CANJE', ".$idEmpresa.", ".$req->input('id_moneda').") AS r")[0]->r;
					} else {
						if($pais->id_pais == 1455){
							$cuenta_contable_proveedor = DB::select("SELECT get_id_cuenta_contable('PROV_LOCALES', ".$idEmpresa.", ".$req->input('id_moneda').") AS r")[0]->r;
						} else {
							$cuenta_contable_proveedor = DB::select("SELECT get_id_cuenta_contable('PROVEEDORES_EXT', ".$idEmpresa.", null) AS r")[0]->r;
						}
						
					}

					if(is_null($cuenta_contable_proveedor)){
						$err = false;
						array_push($msj,'No existe cuenta para el tipo de moneda y cuenta.');
						Log::info($log_info.' No existe cuenta para el tipo de moneda y cuenta');
					}

					if(!$tipo_documento){
						$err = false;
						array_push($msj,'Error interno de sistema, no se pudo asignar el tipo de documento.');
						Log::info($log_info.' Error interno de sistema, no se pudo asignar el tipo de documento');
					}

			/*	} catch(\Exception $e){
					Log::error($log_error.$e);
					array_push($msj,'Ocurrió un error al intentar guardar la factura.');
					$err = false;
				}*/

				//----VALIDAR MONTOS CARGADOS
				$suma_valores = (float)$req->input('exenta') + (float)$req->input('gravada_10') + (float)$req->input('gravada_5')+(float)$req->input('ret_iva') + (float)$req->input('ret_renta');
				$suma_gravadas = (float)$req->input('gravada_10') + (float)$req->input('gravada_5');

				//CALCULAR OP
				$calc_iva10 = (float)$req->input('gravada_10') / 11;
				$calc_iva5 = (float)$req->input('gravada_5') / 21;

				//REDONDAR SOLO CUANDO ES GURANIES
				if($req->input('id_moneda') === '111'){
					$calc_iva10 = round($calc_iva10,0);
				} 

				//Validar que numero documento no sea repetido
				$listados = LibroCompra::where('nro_documento',$req->input('nro_comprobante'))
				->where('id_persona',$req->input('id_cliente'))
				->where('id_timbrado',$req->input('timbrado'))
				->where('id_empresa',$this->getIdEmpresa())
				->where('activo',true)
				->count();

				if($listados){
					$err = false;
					array_push($msj,'Ya existe una factura con este numero para este proveedor.');
					Log::info($log_info.' Ya existe una factura con este numero para este proveedor.');
				}


		if($err) {

		 	try{
			 		if($req->input('lc') !== null && $req->input('id_tipo_documento') != 3){
			 		   $id_documento = $req->input('lc');
			 		}else{
			 			 $id_documento = 0;
			 		}

			    DB::beginTransaction();
					
					/**
					 * INSERT LIBRO COMPRA
					 */
					$contador = 0;
					$venta = "";
					if($req->input('id_tipo_documento') != 3){
						if(!empty($req->input('valor'))){
							foreach($req->input('valor') as $key=>$datos){
								$indice = explode('_', $datos);
									if($contador == 0){
										$venta .= $indice[0];
									}else{
										$venta .=" - ".$indice[0];
									}
								$contador = $contador +1;				
							}
						}
					}
					$venta = $venta." ".$req->input('nro_venta_pedido');
					$concepto = $req->input('proveedor_nombre')." - ".$req->input('nro_comprobante')." - ".date("d/m/Y",strtotime($req->input('fecha_factura')))." - ".$req->input('concepto');

					$libroCompra = new LibroCompra;
					$libroCompra->id_sucursal = $req->input('id_sucursal');
					$libroCompra->id_persona = $req->input('id_cliente');
					$libroCompra->fecha_hora_facturacion = $req->input('fecha_factura').' '.date('H:i:00');
					$libroCompra->fecha_prim_ven = $this->validar($req->input('vencimiento'));
					$libroCompra->ruc = $req->input('cliente_ruc');
					$libroCompra->id_timbrado = $req->input('timbrado'); //HAY ID_TIMBRADO EN LA TABLA
					$libroCompra->fecha = date('Y-m-d H:m:s');//fecha_creacion
					$libroCompra->id_tipo_factura = $req->input('id_tipo_comprobante');
					$libroCompra->nro_documento = $req->input('nro_comprobante');
					$libroCompra->id_moneda = $req->input('id_moneda');
					$libroCompra->importe = $req->input('total_factura');
					$libroCompra->saldo = $req->input('total_factura');
					$libroCompra->costo_venta = $req->input('costo_venta');
					$libroCompra->id_tipo_documento_hechauka= $req->input('id_tipo_documento');
					$libroCompra->origen = 'M'; //MANUAL
					$libroCompra->cambio = ($req->input('cotizacion')) ? $req->input('cotizacion'): 1;
					$libroCompra->cotizacion_contable_venta = ($req->input('cotizacion')) ? $req->input('cotizacion'): 1;
					$libroCompra->costo_exenta = $req->input('exenta');
					$libroCompra->costo_gravada = $suma_gravadas;
					$libroCompra->iva10 = $calc_iva10; //IVA CALCULADO
					$libroCompra->iva5 = $calc_iva5;
					$libroCompra->gravadas10 = (float)$req->input('gravada_10'); //IVA CALCULADO
					$libroCompra->gravadas5 = (float)$req->input('gravada_5');
					$libroCompra->id_cuenta_exenta = $req->input('id_cuenta_exenta');
					$libroCompra->id_cuenta_gravada_10 = $req->input('id_cuenta_gravada10');
					$libroCompra->id_cuenta_gravada_5 = $req->input('id_cuenta_gravada5');
					$libroCompra->id_centro_costo = $req->input('id_centro_costo');
					$libroCompra->concepto = $concepto;
					$libroCompra->id_usuario = $idUsuario;
					$libroCompra->id_empresa = $idEmpresa;
					$libroCompra->id_tipo_documento = $tipo_documento;
					$libroCompra->id_documento = $id_documento; 
					$libroCompra->diferencia = $req->input('diferencia');
					$libroCompra->nro_venta_pedido = $venta;
					$libroCompra->adjunto = $req->input('imagen');
					$libroCompra->diferencia = $req->input('diferencia');
					$libroCompra->retencion_iva = $req->input('ret_iva');
					$libroCompra->pasajero = $req->input('pasajero');
					$libroCompra->cod_confirmacion = $req->input('codigo_referencia');
					$libroCompra->retencion_renta = $req->input('ret_renta');
					$libroCompra->id_cuenta_contable_ret_iva = (int)$req->input('id_retencion_iva');
					$libroCompra->id_cuenta_contable_ret_renta = (int)$req->input('id_retencion_renta');
					$libroCompra->canje = ($req->input('id_canje')) ? true: false;//SI TIENE ID CANJE 
					$libroCompra->base_retencion = $req->input('base_retencion');

					//INSERTA VALOR TRUE
					$libroCompra->save();
					$libro =  $libroCompra->id;
			
					if($tipo_documento === 1){
						DB::select("SELECT generar_libro_compra_manual(".$libroCompra->id.",".$idUsuario.",'FACTURA_CREDITO')");
					}	
					//GENERA SOLO ES NOTA DE CREDITO
					if($req->input('id_tipo_documento') == 3){
						$nor = DB::select("SELECT generar_nc_compra(".$libro.",".$id_documento.",".$idUsuario .")");
					}

					//UPDATE A FORMA DE COBRO SI ES LIBRO COMPRA PRECARGADO**********
					if($req->input('id_canje')){
					FormaCobroReciboDetalle::where('id',$req->input('id_canje'))
											->update(['id_libro_compra'=> $libroCompra->id]);
					}
					//die;
					$montoTotal = (float)$req->input('total_factura');
					if($req->input('id_tipo_documento') != 3){
						if($req->input('valor')!== null){
							$contador=	count($req->input('valor'));
							$contadorInicio = 1;
							foreach($req->input('valor') as $key=>$datos){
								$indice = explode('_', $datos);
								if($indice[1] == 'F'){
									DB::table('facturas_detalle')
										->where('id',$indice[3])
										->update([
												'id_libro_compra'=>$libro
												]);
									if($montoTotal > (float)$indice[2]){	
										if($contadorInicio == $contador){
											$montoEnvio = $montoTotal;
										}else{		
											$montoTotal = $montoTotal - (float)$indice[2];
											$montoEnvio = (float)$indice[2];
										}	
									}else{	
										$montoEnvio = $montoTotal;
									}
									$contadorInicio ++;
									if($key == 0){
										DB::table('libros_compras')
												->where('id',$libro)
												->update([
														'id_proforma'=>$indice[0]
												]);
									}
									
									/*$query ="SELECT recalcular_factura_detalle(".$indice[3].",".$montoEnvio.",".$idEmpresa.")";
									echo '<pre>';
									print_r($query);*/
									//$recalcular = DB::select($query);	
								}elseif($indice[1] == 'P'){
									DB::table('proformas_detalle')
										->where('id',$indice[3])
										->update([
												'id_libro_compra'=>$libro
												]);
									if($key == 0){
										DB::table('libros_compras')
												->where('id',$libro)
												->update([
														'id_proforma'=>$indice[0]
												]);
									}			
								}elseif($indice[1] == 'V'){
									DB::table('ventas_rapidas_detalle')
										->where('id',$indice[3])
										->update([
												'id_libro_compra'=>$libro
												]);
								}				
							}
						}
					}

					//die;
					//GENERAR OP  SI ES FACTURA CONTADO Y SI ES CANJE
					if($tipo_documento === 5 | $es_canje){

						if($tipo_documento === 5 && !$es_canje){
								
							if($pais->id_pais == 1455){
								//Cuenta cliente local
								$cuenta_contable_proveedor =  DB::select("SELECT get_id_cuenta_contable('FACT_TRANS_LOCAL', ".$idEmpresa.", ".$req->input('id_moneda').") AS r")[0]->r;
							} else {
								//Cuenta cliente exterior
								$cuenta_contable_proveedor =  DB::select("SELECT get_id_cuenta_contable('FACT_TRANS_EXT', ".$idEmpresa.", ".$req->input('id_moneda').") AS r")[0]->r;
							}

							if(is_null($cuenta_contable_proveedor)){
								array_push($msj,'No existe cuenta para el tipo de moneda y cuenta. REF: FACT_TRANS_LOCAL o FACT_TRANS_EXT');
								throw new \Exception("No existe cuenta para el tipo de moneda y cuenta. REF: FACT_TRANS_LOCAL o FACT_TRANS_EXT");
							}
	
							
						}
						//GENERAR LC PARA FACTURA CONTADO CON CUENTA TRANSITORIA
						DB::select("SELECT generar_libro_compra_manual(".$libroCompra->id.",".$idUsuario.",'FACTURA_CONTADO')");

							//VERIFICAR SI GENERA RETENCION
							$retencionConsulta->id_proveedor = $req->input('id_cliente');
							$retencionConsulta->moneda = (integer)$req->input('id_moneda');
							$retencionConsulta->data[] = ['operacion' => [
								'monto_pago' => $req->input('total_factura'),
								'id_compra' => $libroCompra->id,
								'id_nc' => null
							]];

							$resultR = $this->retencion($retencionConsulta);
							$total_neto_pago = $req->input('total_factura');
						
						//CARGAR VALORES SI ES QUE GENERA RETENCION
						if($resultR->retencion){
								$retencion = (float)$resultR->monto;
								$consulta = DB::select("SELECT get_parametro('base_imponible') as base_imponible, 
															get_parametro('porcentaje_retencion') as porcentaje_retencion");
								$base_imponible =  $consulta[0]->base_imponible;
								$porcentaje_retencion = $consulta[0]->porcentaje_retencion;   
								$total_neto_pago = $req->input('total_factura');
								$total_neto_pago = $total_neto_pago - $retencion;
						} 
					/**
					 * INSERT OP
					 */
					if($req->input('id_tipo_documento') == 3){
					}else{	
						$nro_op_funct = DB::select("SELECT public.get_documento(".$idEmpresa.",'OP')");
						$nro_op = $nro_op_funct[0]->get_documento;
			  
						$opcabecera = new OpCabecera;
						$opcabecera->id_usuario_creacion  = $idUsuario;
						$opcabecera->id_estado  = 52;//PENDIENTE
						$opcabecera->id_proveedor = $req->input('id_cliente');
						$opcabecera->total_pago =  $req->input('total_factura');
						$opcabecera->total_neto_pago  =  $total_neto_pago;
						$opcabecera->id_moneda  = $req->input('id_moneda');
						$opcabecera->id_sucursal  = $req->input('id_sucursal');
						$opcabecera->cotizacion  = ($req->input('cotizacion')) ? $req->input('cotizacion'): 1; 
						$opcabecera->id_centro_costo  = $req->input('id_centro_costo');    
						$opcabecera->concepto  = $concepto;  
						$opcabecera->total_op_detalle  = $req->input('total_factura');
						$opcabecera->total_op = $req->input('total_factura');
						$opcabecera->id_cuenta_contable = $cuenta_contable_proveedor;
						$opcabecera->id_empresa = $idEmpresa;
						$opcabecera->origen = 'LC';
						$opcabecera->total_retencion = $retencion;
						$opcabecera->base_imponible = $base_imponible;
						$opcabecera->porcentaje_retencion = $porcentaje_retencion;
						$opcabecera->fondo_fijo = false;
						$opcabecera->nro_op = $nro_op;
						$opcabecera->save();

						$idOpCabecera = $opcabecera->id;
						$detalles_op = new OpDetalle;
						$detalles_op->id_libro_compra =  $libroCompra->id;
						$detalles_op->monto = $req->input('total_factura');
						$detalles_op->id_cabecera = $opcabecera->id;
						$detalles_op->save();
						$id_op = $nro_op;
						$persona = Persona::where('id', '=',$req->input('id_cliente'))->first(['retener_iva']);
						if($persona->retener_iva == true){
							// DB::select("SELECT generar_retencion_documento(".$idOpCabecera.",'E',".$idEmpresa.",".$idUsuario.")");	
						}
						//PONE EL ITEM EN ESTADO PENDIENTE;
						// for ($x = 0; $x <= 100; $x+=10) {}
						  
						DB::SELECT('SELECT  generar_op('.$idOpCabecera.','. $idUsuario.')');	
						//REALIZA UN INSERT EN RETENCIONES DE TIPO EMPRESA
					}
			    }	
				DB::commit();
			   } catch(\Exception $e){
					Log::error($log_error);
					Log::error($e);

					array_push($msj,'Ocurrió un error al intentar guardar la factura.');

					$err = false;
					DB::rollBack();
				} 

			}//if
			return response()->json(['err'=>$err,'op'=>$id_op,'msj'=>$msj, 'libro'=>$libro]);
	 }//


	 public function saveLibroCompraFijo(Request $req)
	 {
				$id_op = 0;
				$err = true;
				$idUsuario = $this->getIdUsuario();
				$idEmpresa = $this->getIdEmpresa();
				$msj = [];
				// $retencionConsulta = new \StdClass; 
				//TIPO FACTURA: 1 CREDITO / 2 CONTADO
				//TIPO DOCUMENTO 5 FACTURA CONTADO / 22 FACTURA CREDITO
				$tipo_documento = ($req->input('id_tipo_comprobante') === '1') ? 22 : 5;
				// $porcentaje_retencion = null;
				// $base_imponible = null;
				// $retencion = null;
				$total_neto_pago = 0;
				$es_canje = ($req->input('id_canje')) ? true: false;
				$cuenta_contable_proveedor = 0;
				$log_info = "(USERID:".$this->getIdUsuario()." METOD:saveLibroCompra)".json_encode($req->all());
				$log_error = "(USERID:".$this->getIdUsuario()." METOD:saveLibroCompra) ";

			 	try{
					Log::info($log_info);
					//OBTIENE CUENTA CONTABLE SEGUN CASO
					if($es_canje){
						$cuenta_contable_proveedor = DB::select("SELECT get_id_cuenta_contable('CANJE', ".$idEmpresa.", ".$req->input('id_moneda').") AS r")[0]->r;
					} else {
						$cuenta_contable_proveedor = DB::select("SELECT get_id_cuenta_contable('PROV_LOCALES', ".$idEmpresa.", ".$req->input('id_moneda').") AS r")[0]->r;
					}
					// dd($cuenta_contable_proveedor);
					if(is_null($cuenta_contable_proveedor)){
						$err = false;
						array_push($msj,'No existe cuenta para el tipo de moneda y cuenta.');
						Log::info($log_info.' No existe cuenta para el tipo de moneda y cuenta');
					}

			 	} catch(\Exception $e){
					Log::error($log_error.$e);
					array_push($msj,'Ocurrió un error al intentar guardar la factura.');
					$err = false;
				} 

				


				//----VALIDAR MONTOS CARGADOS
				$suma_valores = (float)$req->input('exenta') +  (float)$req->input('gravada_10') + (float)$req->input('gravada_5')+(float)$req->input('ret_iva') + (float)$req->input('ret_renta');

				$suma_gravadas = (float)$req->input('gravada_10') + (float)$req->input('gravada_5');


				if($suma_valores != (float)$req->input('total_factura')){
					$err = false;
					array_push($msj,'El total factura no coincide con la suma de exenta, gravada 10 y gravada 5.');
				}
				//CALCULAR OP
				$calc_iva10 = (float)$req->input('gravada_10') / 11;
				$calc_iva5 = (float)$req->input('gravada_5') / 21;

				//REDONDAR SOLO CUANDO ES GURANIES
				if($req->input('id_moneda') === '111'){
					$calc_iva10 = round($calc_iva10,0);
				} 


		
				//SELECCIONAR TODAS LAS FACTURAS CON EL MISMO NUMERO
				$verFacturasProveedor= LibroCompra::where('nro_documento', $req->input('nro_comprobante'))
												   ->where('activo', true)
												   ->get();
				if ($verFacturasProveedor->count() > 0) {
					// La consulta no devuelve ningún resultado
					//COMPROBAR SI LA FACTURA QUE COINCIDE ES DE LA MISMA EMPRESA
					if($verFacturasProveedor[0]->id_persona == $req->input('id_cliente') ){
						$err = false;
						array_push($msj,'El numero de factura ya existe para el mismo proveedor.');
					}
				}
				

				
		if($err)
		{
			    try{
					if($req->input('lc') !== null){
			 		   $id_documento = $req->input('lc');
			 		}else{
			 			 $id_documento = 0;
			 		}


					DB::beginTransaction();
					
					/**
					 * INSERT LIBRO COMPRA
					 */
					$libroCompra = new LibroCompra;
					$libroCompra->id_sucursal = $req->input('id_sucursal');
					$libroCompra->id_persona = $req->input('id_cliente');
					$libroCompra->fecha_hora_facturacion = $req->input('fecha_factura').' '.date('H:i:00');
					$libroCompra->fecha_prim_ven = $this->validar($req->input('vencimiento'));
					$libroCompra->ruc = $req->input('cliente_ruc');
					$libroCompra->id_timbrado = $req->input('timbrado'); //HAY ID_TIMBRADO EN LA TABLA
					$libroCompra->fecha = date('Y-m-d H:i:00');//fecha_creacion
					$libroCompra->id_tipo_factura = $req->input('id_tipo_comprobante');
					$libroCompra->nro_documento = $req->input('nro_comprobante');
					$libroCompra->id_moneda = $req->input('id_moneda');
					$libroCompra->importe = $req->input('total_factura');
					$libroCompra->saldo = $req->input('total_factura');
					$libroCompra->origen = 'M'; //MANUAL
					$libroCompra->cambio = ($req->input('cotizacion')) ? $req->input('cotizacion'): 1;
					$libroCompra->cotizacion_contable_venta = ($req->input('cotizacion')) ? $req->input('cotizacion'): 1;
					$libroCompra->costo_exenta = $req->input('exenta');
					$libroCompra->costo_gravada = $suma_gravadas;
					$libroCompra->iva10 = $calc_iva10; //IVA CALCULADO
					$libroCompra->iva5 = $calc_iva5;
					$libroCompra->gravadas10 = (float)$req->input('gravada_10'); //IVA CALCULADO
					$libroCompra->gravadas5 = (float)$req->input('gravada_5');
					$libroCompra->id_cuenta_exenta = $req->input('id_cuenta_exenta');
					$libroCompra->id_cuenta_gravada_10 = $req->input('id_cuenta_gravada10');
					$libroCompra->id_cuenta_gravada_5 = $req->input('id_cuenta_gravada5');
					$libroCompra->id_centro_costo = $req->input('id_centro_costo');
					$libroCompra->concepto = $req->input('concepto');
					$libroCompra->adjunto = $req->input('imagen');
					$libroCompra->id_usuario = $idUsuario;
					$libroCompra->id_empresa = $idEmpresa;
					$libroCompra->id_tipo_documento = $tipo_documento;
					$libroCompra->id_tipo_documento_hechauka= $req->input('id_tipo_documento');
					$libroCompra->fondo_fijo = true;
					$libroCompra->retencion_iva = $req->input('ret_iva');
					$libroCompra->retencion_renta = $req->input('ret_renta');
					$libroCompra->id_cuenta_contable_ret_iva = (int)$req->input('id_retencion_iva');
					$libroCompra->id_cuenta_contable_ret_renta = (int)$req->input('id_retencion_renta');

					$libroCompra->canje = ($req->input('id_canje')) ? true: false;//SI TIENE ID CANJE INSERTA VALOR TRUE
					$libroCompra->save();
					$libro =  $libroCompra->id;
					//GENERA SOLO ES NOTA DE CREDITO
					if($req->input('id_tipo_documento') === 3){
						DB::select("SELECT generar_libro_compra_manual(".$libro.",".$id_documento.",".$idUsuario .")");
					}


					//GENERA SOLO ES FACTUA CREDITO
					if($tipo_documento === 22){
						DB::select("SELECT generar_libro_compra_manual(".$libroCompra->id.",".$idUsuario.",'FACTURA_CREDITO')");
					}

					$montoTotal = (float)$req->input('total_factura');
					if($req->input('valor')!== null){
						$contador=	count($req->input('valor'));
						$contadorInicio = 1;
						foreach($req->input('valor') as $key=>$datos){
							$indice = explode('_', $datos);
							if($indice[1] == 'F'){
								DB::table('facturas_detalle')
									->where('id',$indice[3])
									->update([
											'id_libro_compra'=>$libro
											]);
								if($montoTotal > (float)$indice[2]){	
									if($contadorInicio == $contador){
										$montoEnvio = $montoTotal;
									}else{		
										$montoTotal = $montoTotal - (float)$indice[2];
										$montoEnvio = (float)$indice[2];
									}	
								}else{	
									$montoEnvio = $montoTotal;
								}
								$contadorInicio ++;
								/*$query ="SELECT recalcular_factura_detalle(".$indice[3].",".$montoEnvio.",".$idEmpresa.")";
								$recalcular = DB::select($query);	*/
							}elseif($indice[1] == 'P'){
								DB::table('proformas_detalle')
									->where('id',$indice[3])
									->update([
											'id_libro_compra'=>$libro
											]);
							}elseif($indice[1] == 'V'){
								DB::table('ventas_rapidas_detalle')
									->where('id',$indice[3])
									->update([
											'id_libro_compra'=>$libro
											]);
							}				
						}					
					}

					//UPDATE A FORMA DE COBRO SI ES LIBRO COMPRA PRECARGADO**********
					// if($req->input('id_canje')){
					// FormaCobroReciboDetalle::where('id',$req->input('id_canje'))
					// 						->update(['id_libro_compra'=> $libroCompra->id]);
					// }
					

						DB::commit();
				
			 } catch(\Exception $e){
					Log::error($log_error.$e);
					array_push($msj,'Ocurrió un error al intentar guardar la factura.');
					$err = false;
					DB::rollBack();
				} 

			}//if
			
				return response()->json(['err'=>$err,'op'=>$id_op,'msj'=>$msj]);

	 }//


		/**
		 * Almacena los timbrados que se cargan en el ibro compra
		 */
		public function saveTimbrado(Request $req){
			// dd($req->all());
			$obj = new \StdClass;

			$obj->nro_timbrado = $req->input('nro_timbrado');
			$obj->fecha_inicio = $req->input('inicio');
			$obj->fecha_vencimiento = $req->input('vencimiento');
			$obj->id_persona = $req->input('id_persona');
			$obj->denominacion = $req->input('denominacion');
			$resp = $this->timbradoLibroCompra($obj);


		  return response()->json($resp);

		}//


	

		/**
		 * Almacena los timbrados y verificva si existe
		 */
		private function timbradoLibroCompra($obj){

		 //dd($obj);
			$idUsuario = $this->getIdUsuario();
			$idEmpresa = $this->getIdEmpresa();
			$rsp = new \StdClass;
			$rsp->existe = false;
			$rsp->err = true;
			$rsp->id_timbrado = null;

			try{    

				$verificar = OpTimbrado::where('nro_timbrado', (Integer)$obj->nro_timbrado)
							->where('id_empresa',$idEmpresa)
							->get();

				if(!$verificar->isEmpty()){
					// dd($verificar);
					$rsp->existe = true;
					$rsp->id_timbrado = $verificar[0]->id;
					
				} else {
					$timbrado = new OpTimbrado;
					$timbrado->nro_timbrado = (Integer)$obj->nro_timbrado;
					$timbrado->fecha_inicio =  $obj->fecha_inicio;
					$timbrado->fecha_vencimiento = $obj->fecha_vencimiento;
					$timbrado->id_usuario = $idUsuario;
					$timbrado->id_empresa = $idEmpresa;
					$timbrado->id_persona = $obj->id_persona;
					$timbrado->denominacion = $obj->denominacion;

					$timbrado->save();	
					$rsp->id_timbrado = $timbrado->id;
					
				}


		} catch(\Exception $e){
			$rsp->err = false;
		}


			return $rsp;
		}

		public function obtenerFechaVencimiento(Request $req){

			// dd($req->all());
			$plazo_rsp = ['dias'=>0];
			$plazo = Persona::where('id',$req->input('id_proveedor'))->with('plazoPago')->first();
			// dd();

			if(!is_null($plazo->plazoPago)){
				$plazo_rsp = ['dias'=>$plazo->plazoPago['cantidad_dias']];
			}

			return response()->json($plazo_rsp);
		}//


		private function formatoFechaSalida($date){
			if( $date != ''){
	
			$date = explode('-', $date);
				$fecha = $date[2]."/".$date[1]."/".$date[0];
				return $fecha;
			} else {
				return null;
			}
		}//


	public function verLibroCompra($id){
		$id_empresa = $this->getIdEmpresa(); 
 		$libroCompra = LibroCompra::where('id',$id)->with('currency','usuario','tipoFactura','cliente','tipoHechauka','sucursal_empresa',
														 'tipoDocumento','OpTimbrado','centroCosto','cuentaContableIva10',
														 'cuentaContableIva5','cuentaContableExenta','cuentaContableRetencionIva','cuentaContableRetencionRenta','usuarioAnulacion')
									->where('id_empresa',$this->getIdEmpresa())						
									->first();

		if(!isset($libroCompra->id)){
			flash('El registro solicitado no existe. Intentelo nuevamente !!')->error();
			return redirect()->route('home');
		}
				
		$fecha = explode(' ',$libroCompra->fecha_hora_facturacion);
		$libroCompra->fecha_hora_facturacion = $this->formatoFechaSalida($fecha[0]);

		if($libroCompra->fecha_prim_ven){
			$libroCompra->fecha_prim_ven = $this->formatoFechaSalida($libroCompra->fecha_prim_ven);
		}
		//$idPersona = $libroCompra->cliente->id_persona;
		$permiso = $this->datosPermiso('verLibroCompra',57);
		$permiso_anular = $this->datosPermiso('verLibroCompra',58);
		$permiso_modificar_fecha = $this->datosPermiso('verLibroCompra',60);
		$reprice = $this->datosPermiso('verLibroCompra',69);
		/*$getProveedor = DB::select("SELECT * FROM personas 
						WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
									WHERE puede_facturar = true)
									AND id in
										(
										SELECT libros_compras.id_persona FROM libros_compras 
										JOIN personas on  libros_compras.id_persona=personas.id
										where libros_compras.id= " . $id . "
									)
						AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");*/
		$getProveedor = DB::select("SELECT * FROM personas 
        							WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
                                    WHERE puede_facturar = true)
                        			AND id_empresa = " . $this->getIdEmpresa() . " AND activo = true order by nombre ASC");
									
								
		return view('pages.mc.contabilidad.verLibroCompra',compact('libroCompra','permiso','permiso_anular','permiso_modificar_fecha','reprice','getProveedor'));
	}//

	public function verLibroVenta($id){
		$libroVenta = LibroVenta::where('id',$id)->with('currency','tipoFactura','timbrado','cliente','estado','tipoDocumento','comercionEmpresa','tipoFacturacion','grupo','unidadNegocio')
					->where('id_empresa',$this->getIdEmpresa())						
					->first();

		if(!isset($libroVenta->id)){
			flash('El registro solicitado no existe. Intentelo nuevamente !!')->error();
			return redirect()->route('home');
		}
		$productos = Producto::where('tipo_producto','P')
							->where('id_empresa', $this->getIdEmpresa())
							->orderBy('denominacion', 'ASC')
							->get();


		$perfil = Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil;
		$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);
		$centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();  
		$permiso_anular = $this->datosPermiso('verLibroVenta',59);

		return view('pages.mc.contabilidad.verLibroVenta',compact('libroVenta','perfil','productos','permiso_anular','sucursalEmpresa','centro'));
	}//


	public function anularLibroCompra(Request $req){

		$idUsuario = $this->getIdUsuario();
		$idEmpresa = $this->getIdEmpresa();
		$err = true;
		$msj = '';

		/**
		 * El primer catch toma las excepciones creadas de forma manual
		 * El segundo catch toma las exepciones del sistema que no son manuales y no muestra al cliente
		 */
		try{
			$this->anular_libro_compra_manual($req->input('id_libro'), $idUsuario, $this->getIdEmpresa());

		} catch(ExceptionCustom $e){
			//Aqui cae la exepcion creada manualmente
			Log::error($e);
			$msj = $e->getMessage();
			$err = false;
		} catch(\Exception $e){
			Log::error($e);
			$msj = 'Ocurrio un error al momento de intentar anular el libro compra';
			$err = false;
		}

		return response()->json(['err'=>$err,'msj'=>$msj]);

	}


	public function anularLibroVenta(Request $req){

		$idUsuario = $this->getIdUsuario();
		$idEmpresa = $this->getIdEmpresa();
		$err = true;
		$msj = '';

		/**
		 * El primer catch toma las excepciones creadas de forma manual
		 * El segundo catch toma las exepciones del sistema que no son manuales y no muestra al cliente
		 */
		try{
			$this->anular_libro_venta_manual($req->input('id_libro'), $idUsuario,$idEmpresa);

		} catch(ExceptionCustom $e){
			//Aqui cae la exepcion creada manualmente
			Log::error($e);
			$msj = $e->getMessage();
			$err = false;
		} catch(\Exception $e){
			Log::error($e);
			$msj = 'Ocurrio un error al momento de intentar anular el libro venta';
			$err = false;
		}

		return response()->json(['err'=>$err,'msj'=>$msj]);

	}



	public function getCuentaPredeterminada(Request $req)
	{
		if($req->input('id_cliente')){
			$local = Persona::where('id',$req->input('id_cliente'))->where('id_pais',1455)->count();
			foreach($req->cuentas as $key => $value)
			{	
				if($local == 1){
					$req->cuentas = ['ANTICIPOS_CLIENTES'];
				} else {
					$req->cuentas = ['ANTICIPOS_CLIENTES'];
				}
			}
		}
		$cuentas = DB::table('cuentas_contables_empresa')
							->where('id_empresa',$this->getIdEmpresa())
							->where('id_moneda',$req->id_moneda)
							->where('activo',true)
							->whereIn('parametro',$req->cuentas)
							->get();
		return response()->json(['data'=>$cuentas]);
	}

	public function mostrarListadoFacturas(Request $req)
	{
  		$datos = $this->responseFormatDatatable($req);	
		if($datos->id_tipo_documento == 3){
			$listados = LibroCompra::with('notaCredito','facturaDetalle','factura','currency')
								  ->where('id_persona',(Integer)$datos->id_cliente)
	  							  ->where('id_moneda',(Integer)$datos->id_moneda)
	  							  ->whereIn('id_tipo_documento_hechauka',[1,15,5,14])	
								  ->where('id_usuario_anulacion', null)	
								  ->where('id_empresa',$this->getIdEmpresa())
								  ->get();
		    $listado = [];
			/*echo '<pre>';
      			print_r($listados);*/

     		foreach($listados as $key=>$listadoBase){
     			/*echo '<pre>';
      				print_r($listadoBase->id);*/
      			$total_nc =0;	
				if(!isset($listadoBase->notaCredito[0]->importe)){
					$listado[] = $listadoBase;
				}else{
					foreach($listadoBase->notaCredito as $nota_credito){
						$total_nc = $total_nc + $nota_credito->importe;
					}
					if($listadoBase->importe > $total_nc){
						$listado[] = $listadoBase;
					}
				}	
			}
		}else{
  			/*$listado = FacturaDetalle::with('factura', 'producto', 'currencyVenta')
	  								 ->where('id_proveedor', (Integer)$datos->id_cliente)
	  								 ->where('currency_costo_id',(Integer)$datos->id_moneda)	
	  								 ->where('id_libro_compra', null)
									 ->where('id_empresa',$this->getIdEmpresa())
									 ->get();*/
			$listado = DB::table('vw_detalles_lc');
			$listado = $listado->where('id_proveedor',(Integer)$datos->id_cliente);
			$listado = $listado->where('id_moneda',(Integer)$datos->id_moneda); 
			$listado = $listado->where('id_libro_compra',null);
			$listado = $listado->where('id_empresa',$this->getIdEmpresa());
			$listado = $listado->get();
		}
		return response()->json(['data'=>$listado]);
	}
	
	public function mostrarListadoFacturasFF(Request $req)
	{

  		$datos = $this->responseFormatDatatable($req);	
		if($datos->id_tipo_documento == 3){
			$listados = LibroCompra::with('notaCredito','facturaDetalle','factura','currency')
								  ->where('id_persona',(Integer)$datos->id_cliente)
	  							  ->where('id_moneda',(Integer)$datos->id_moneda)
	  							  ->where('id_tipo_documento_hechauka',1)	
								  ->where('id_empresa',$this->getIdEmpresa())
								  ->get();
		    $listado = [];
			/*echo '<pre>';
      			print_r($listados);*/

     		foreach($listados as $key=>$listadoBase){
     			/*echo '<pre>';
      				print_r($listadoBase->id);*/
      			$total_nc =0;	
				if(!isset($listadoBase->notaCredito[0]->importe)){
					$listado[] = $listadoBase;
				}else{
					foreach($listadoBase->notaCredito as $nota_credito){
						$total_nc = $total_nc + $nota_credito->importe;
					}
					if($listadoBase->importe > $total_nc){
						$listado[] = $listadoBase;
					}
				}	
			}
		}else{
			$listado = DB::table('vw_detalles_lc');
			$listado = $listado->where('id_proveedor',(Integer)$datos->id_cliente);
			$listado = $listado->where('id_moneda',(Integer)$datos->id_moneda); 
			$listado = $listado->where('id_libro_compra',null);
			$listado = $listado->where('id_empresa',$this->getIdEmpresa());
			$listado = $listado->where('tipo','F');
			$listado = $listado->get();
		}
		return response()->json(['data'=>$listado]);
	}



	public function getActualizarSaldo(Request $req)
	{
		$idUsuario = $this->getIdUsuario();
		$data = DB::select("SELECT actualizar_saldo_lv(".(Integer)$req->input('id_lv').",".str_replace(',','.', str_replace('.','', $req->input('saldo'))).",".$idUsuario.")");
		$resultado = $data[0]->actualizar_saldo_lv;
		return response()->json($resultado);
	}

	public function facturaDetalles(Request $req)
	{
		
		if($req->input('indicador') == 1){
			$listados = LibroCompra::where('id', $req->input('id'))
								->where('id_empresa',$this->getIdEmpresa())
								->first(['saldo']);
			$monto = (float)$listados->saldo;
		}else{ 
			$idSearch = explode('_', $req->input('id'));
			if($req->input('tipo') == 'P'){
				$detalles = ProformasDetalle::where('id', $idSearch[0])
						->where('id_empresa',$this->getIdEmpresa())
						->first(['costo_proveedor', 'porcion_exenta', 'porcion_gravada','precio_costo']);
						if($this->getIdEmpresa() == 1){
							$monto = (float)$detalles->costo_proveedor;
						}else{ 
							if($this->getIdEmpresa() == 6&&$this->getIdEmpresa() == 3){
								$monto = (float)$detalles->precio_costo;
							}else{
								$monto = (float)$detalles->porcion_exenta + (float)$detalles->porcion_gravada;
							}	
						}
			}elseif($req->input('tipo') == 'F'){
				$detalles = FacturaDetalle::where('id', $idSearch[0])
						->where('id_empresa',$this->getIdEmpresa())
						->first(['costo_proveedor', 'exento', 'gravadas_10','precio_costo']);
						if($this->getIdEmpresa() == 1){
							$monto = (float)$detalles->costo_proveedor;
						}else{ 
							if($this->getIdEmpresa() == 6&&$this->getIdEmpresa() == 3){
								echo '<pre>';
								print_r('2');
				
								$monto = (float)$detalles->precio_costo;
							}else{
								$monto = (float)$detalles->exento + (float)$detalles->gravadas_10;
							}	
						}
			}elseif($req->input('tipo') == 'V'){
				$detalles = VentasRapidasDetalle::where('id', $idSearch[0])
						->where('id_empresa',$this->getIdEmpresa())
						->first(['precio_costo']);
						$monto = (float)$detalles->precio_costo;
			}
		}	
		return response()->json($monto);
	}

	public function comprobarFactura(Request $req)
	{
		$msj = '';
		$timbrado = $req->input('timbrado');
		if($timbrado === 'null'|| $timbrado === '') {
			$timbrado = 0;
		}

		$proveedor = $req->input('proveedor');
		if($proveedor === 'null' || $proveedor === '') {
			$proveedor = 0;
		}
	 	$listados = LibroCompra::where('nro_documento',$req->input('num_documento'))
								->where('id_persona',$proveedor)
								->where('id_timbrado',$timbrado)
								->where('id_empresa',$this->getIdEmpresa())
								->where('activo',true)
								->first();
		if(isset($listados->id)){
			$msj = 'ERROR';
		}else{	
			$msj = 'OK';
		}
		return response()->json($msj); 
	}


	public function comprobarFacturaVenta(Request $req)
	{
		$msj = '';
		$timbrado = $req->input('timbrado');
		if($timbrado === 'null'|| $timbrado === '') {
			$timbrado = 0;
		}

		$proveedor = $req->input('proveedor');
		if($proveedor === 'null' || $proveedor === '') {
			$proveedor = 0;
		}
	 	$listados = LibroVenta::where('nro_documento',$req->input('num_documento'))
								->where('cliente_id',$proveedor)
								->where('id_timbrado',$timbrado)
								->where('id_empresa',$this->getIdEmpresa())
								->where('activo',true)
								->first();
		if(isset($listados->id)){
			$msj = 'ERROR';
		}else{	
			$msj = 'OK';
		}
		return response()->json($msj); 
	}

	public function indexVentaPendiente(Request $request)
	{
		$monedas = Divisas::where('activo','S')->get();
		$proveedores = Persona::where('id_tipo_persona','14')->whereIn('tipo_proveedor',[1])->where('id_empresa',$this->getIdEmpresa())->get();
		$plazoPagos = DB::select("SELECT distinct tipo_plazo as n FROM plazos_pago ");
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();
		return view('pages.mc.contabilidad.ventaPendiente',compact('monedas','proveedores','plazoPagos','cuentas_contables'));
	}

	public function mostrarVentasPendientes(Request $request)
	{
		$datos = $this->responseFormatDatatable($request);
		/*echo '<pre>';
		print_r($datos);*/
		$ventas = DB::table('vw_ventas_pendientes');
		$ventas = $ventas->where('id_empresa',$this->getIdEmpresa());
		if($datos->idProveedor != '')
		{
			$ventas = $ventas->where('id_proveedor',$datos->idProveedor);
			
		}
		if($datos->idProforma != '')
		{
			$ventas = $ventas->where('id_proforma',$datos->idProforma);
			
		}
		if($datos->nroFactura != '')
		{
			$ventas = $ventas->where('nro_factura','like', '%' .trim($datos->nroFactura).'%');
			
		}
		if($datos->codigoConfirmacion != '')
		{
			$ventas = $ventas->where('cod_confirmacion',$datos->codigoConfirmacion);
		}
		if($datos->asociado != '')
		{
			$ventas = $ventas->where('asociado',$datos->asociado);
		}
		if($datos->idMoneda != '')
		{
			$ventas = $ventas->where('id_moneda',$datos->idMoneda);
		}
		if($datos->mes != '')
		{
			if($datos->anho)
			{
				$ventas = $ventas->where('mes', trim($datos->mes))->where('anho', $datos->anho);
			 }
		  }   
		  if($datos->proveedor_desde_hasta != '')
		  {
			  $fechaTrim = trim($datos->proveedor_desde_hasta);
			  $periodo = explode('-', trim($fechaTrim));
			  $desde = explode("/", trim($periodo[0]));
			  $hasta = explode("/", trim($periodo[1]));
			  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
			  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
			  $ventas = $ventas->whereBetween('created_at', [$fecha_desde, $fecha_hasta]);
		  }

		  if($datos->creacion_desde_hasta != '')
		  {
			  $fechaTrim = trim($datos->creacion_desde_hasta);
			  $periodo = explode('-', trim($fechaTrim));
			  $desde = explode("/", trim($periodo[0]));
			  $hasta = explode("/", trim($periodo[1]));
			  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
			  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
			  $ventas = $ventas->whereBetween('fecha_hora_facturacion', [$fecha_desde, $fecha_hasta]);
		  }

		$ventas = $ventas->get();
		/* echo '<pre>';
		print_r($ventas);*/

		return response()->json(['data'=>$ventas]);
		
	}

	public function adjuntoLibroCompras(Request $request)
	{
	 	$adjuntosLc = LibroCompra::where('id',$request->input('idLibroCompra'))
									->first(['adjunto']);

		return response()->json($adjuntosLc->adjunto); 
	}

	public function getPersonaNegociacion(Request $request){
		$dias = 0;
		$proveedor = Persona::with('plazoPago')->where('id',$request->input('dataId'))->where('id_empresa',$this->getIdEmpresa())->first();
		if(isset($proveedor->plazoPago->denominacion)){
			$dias = $proveedor->plazoPago->cantidad_dias;
		}
		return response()->json($dias);
	}


	public function comprobarAsiento(Request $request){
		$id_asiento = 0;
		$rsp = new \StdClass;
		if($request->input('option') == 'REC'){
			$recibo = Recibo::where('id',$request->input('id_data'))
									  ->first();
			$id_asiento = $recibo->id_asiento;
		}

		if($request->input('option') == 'OP'){
			$op_cabecera = OpCabecera::where('id',$request->input('id_data'))
									    ->first();
			$id_asiento = $op_cabecera->id_asiento;
		}

		if($request->input('option') == 'LC'){
			$lc = LibroCompra::where('id',$request->input('id_data'))
									    ->first();
			$id_asiento = $lc->id_asiento;
		}

		if($request->input('option') == 'LV'){
			$lv = LibroVenta::where('id',$request->input('id_data'))
									    ->first();
			$id_asiento = $lv->id_asiento;
		}
		$comprobarAsiento = DB::select("select SUM(asientos_contables_detalle.debe)- SUM(asientos_contables_detalle.haber) as diferencia
								from asientos_contables_detalle, asientos_contables
								where asientos_contables_detalle.id_asiento_contable = asientos_contables.id
								and asientos_contables_detalle.id_asiento_contable = ".$id_asiento."
								and asientos_contables_detalle.activo = true
								and asientos_contables.activo = true");

		
		if(isset($comprobarAsiento[0]->diferencia)){
			if($comprobarAsiento[0]->diferencia == 0){
				$rsp->status = 'OK';
				$rsp->id = '';
			}else{
				$rsp->status = 'ERROR';
				$rsp->id = $id_asiento;
			} 
		}else{
			$rsp->status = 'OK';
			$rsp->id = '';
		} 
		return response()->json($rsp);
	}

	public function addLibroVenta(Request $req){
		$proveedores = DB::select("SELECT p.id,p.nombre, p.apellido, p.documento_identidad,p.dv,tp.denominacion, p.activo,p.id_pais,
									concat(p.nombre,' ',p.apellido,' ','- '|| trim(p.denominacion_comercial)) as full_data
									FROM personas p JOIN tipo_persona tp on tp.id = p.id_tipo_persona
									WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
									AND p.activo = true and p.id_empresa = ".$this->getIdEmpresa()." order by p.nombre asc");
		$monedas = Divisas::where('activo','S')->orderBy('currency_code','DESC')->get();
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();
		$tipoDocumentos = TipoDocumentoHechauka::where('activo',true)->orderBy('orden', 'ASC')->get();
		$tipo_factura = TipoFactura::orderBy('id','DESC')->get();
		$tipo_facturacions = TipoFacturacion::orderBy('id','DESC')->get();
		$centro = CentroCosto::where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();  
		$cuentas_contables = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa',$this->getIdEmpresa())->get();
		$sucursalEmpresa = SucursalEmpresa::where('id_empresa',$this->getIdEmpresa())->where('activo',true)->orderBy('denominacion','ASC')->get(['id', 'denominacion']);

		$grupos = Grupos::where('estado_id', 11)->where('empresa_id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get(['id','denominacion']);
		$negocios = Negocio::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
		$permiso = $this->datosPermiso('verNotaCredito',43);
		$permiso_anular_lv = $this->datosPermiso('agregarLibroVenta',59);

		return view('pages.mc.contabilidad.addLibroVenta',compact('permiso','monedas','proveedores','cuentas_contables','tipoDocumentos','tipo_factura','sucursalEmpresa','centro','cuentas_contables','tipo_facturacions','permiso_anular_lv','grupos','negocios'));
	}

	public function doGenerarNC(Request $request){
		$rsp = new \StdClass;
		$idUsuario = $this->getIdUsuario();

		try {
			$data = DB::select("SELECT generar_nota_creditos_lv(".(Integer)$request->input('id').",".(Integer)$request->input('id_producto').",".str_replace(',','.', str_replace('.','', $request->input('monto_nota'))).",".$idUsuario.")");
			if(isset($data[0]->generar_nota_creditos_lv)){
				$resultado = $data[0]->generar_nota_creditos_lv;
				$base = explode('_',$resultado);
				$rsp->status = $base[0];
				$rsp->id = $base[1];
			}else{
				$rsp->status = 'ERROR';
				$rsp->id = '';
			}
		}catch(\Exception $e){
			Log::error($e);
			$rsp->status = 'ERROR';
			$rsp->id = '';
		}
		
		return response()->json($rsp);
	}

	public function saveLibroVenta(Request $req){
		$idUsuario = $this->getIdUsuario();
		$idEmpresa = $this->getIdEmpresa();
		$rsp = new \StdClass;    //CASE WHEN id_tipo_factura = 1 THEN 1 ELSE 5 END
		if($req->input('id_tipo_comprobante') == (int)1){
			$tipo_documento = 1;
		}else{
			$tipo_documento = 5;
		}
		$suma_valores = (float)$req->input('exenta') +  (float)$req->input('gravada_10') + (float)$req->input('gravada_5')+(float)$req->input('ret_iva') + (float)$req->input('ret_renta');
		$suma_gravadas = (float)$req->input('gravada_10') + (float)$req->input('gravada_5');

		if($suma_valores != (float)$req->input('total_factura')){
			$err = false;
			array_push($msj,'El total factura no coincide con la suma de exenta, gravada 10 y gravada 5.');
		}
		//CALCULAR OP
		$calc_iva10 = (float)$req->input('gravada_10') / 11;
		$calc_iva5 = (float)$req->input('gravada_5') / 21;

		//REDONDAR SOLO CUANDO ES GURANIES
		if($req->input('id_moneda') === '111'){
			$calc_iva10 = round($calc_iva10,0);
		} 

		if($req->input('id_tipo_documento') === 3){
		    $tipo_timbrado =  2;
			$tipo  ='NC';
		} else if($req->input('id_tipo_documento') === 2){
			$tipo_timbrado =  4;
			$tipo  ='FA';
		}else{
			$tipo_timbrado =  1;
			$tipo  ='FA';
		}

		if($req->input('total_factura') !== null){
			if((float)$req->input('total_factura') == ""){	
				$total_factura = 0;
			}else{
				$total_factura = (float)$req->input('total_factura');
			}	
		}else{	
			$total_factura = 0;
		}

		/**
		 * INSERT LIBRO VENTA
		 */
	 	try{
			
			$libroVenta = new LibroVenta;
			$libroVenta->id_sucursal = $req->input('id_sucursal');
			$libroVenta->id_centro_costo = $req->input('id_centro_costo');
			$libroVenta->id_unidad_negocio = $req->input('id_unidad_negocio') ? $req->input('id_unidad_negocio') : null;
			$libroVenta->id_grupo = $req->input('id_grupo') ? $req->input('id_grupo') : null;
			$libroVenta->cliente_id = $req->input('id_proveedor');
			$libroVenta->fecha_hora_documento = $req->input('fecha_factura').' '.date('H:i:00');
			$libroVenta->vencimiento = $this->validar($req->input('vencimiento'));
			//$libroVenta->ruc = $req->input('proveedor_ruc');
			$libroVenta->id_timbrado = $req->input('timbrado'); //HAY ID_TIMBRADO EN LA TABLA
			$libroVenta->fecha_hora = date('Y-m-d H:i:00');//fecha_creacion
			$libroVenta->id_tipo_factura = $req->input('id_tipo_comprobante');
			$libroVenta->nro_documento = $req->input('nro_comprobante');
			$libroVenta->id_moneda_venta = $req->input('id_moneda');
			$libroVenta->importe = $total_factura;
			$libroVenta->saldo = $req->input('saldo') ? $req->input('saldo') : $total_factura; //Si no pone el saldo se usa el total
			$libroVenta->origen = 'M'; //MANUAL
			$libroVenta->cotizacion_contable_compra = ($req->input('cotizacion')) ? $req->input('cotizacion'): 1;
			$libroVenta->cotizacion_documento = ($req->input('cotizacion')) ? $req->input('cotizacion'): 1;
			$libroVenta->total_exentas = $req->input('exenta');
			$libroVenta->total_gravadas = $suma_gravadas;
			$libroVenta->total_iva = $calc_iva10 + $calc_iva5; //IVA CALCULADO 
			$libroVenta->usuario_documento_id = $idUsuario;
			$libroVenta->total_bruto_documento = $total_factura;
			$libroVenta->total_neto_documento = $total_factura;
			$libroVenta->id_tipo_facturacion = (float)$req->input('id_tipo_facturacion'); //IVA CALCULADO
			$libroVenta->procesado_analisis_comision = true;
			$libroVenta->id_cuenta_exenta = $req->input('id_cuenta_exenta');
			$libroVenta->id_cuenta_gravada_10 = $req->input('id_cuenta_gravada10');
			$libroVenta->id_cuenta_gravada_5 = $req->input('id_cuenta_gravada5');
			$libroVenta->id_estado_cobro = 31;
			$libroVenta->id_estado_documento = 29;
			$libroVenta->id_tipo_timbrado = $tipo_timbrado;
			$libroVenta->id_usuario = $idUsuario;
			$libroVenta->id_empresa = $idEmpresa;
			$libroVenta->tipo = $tipo;
			$libroVenta->id_tipo_documento = $tipo_documento;
			$libroVenta->id_tipo_documento_hechauka= $req->input('id_tipo_documento');
			$libroVenta->retencion_iva = $req->input('ret_iva');
			$libroVenta->retencion_renta = $req->input('ret_renta');
			$libroVenta->id_cuenta_contable_ret_iva = (int)$req->input('id_retencion_iva');
			$libroVenta->id_cuenta_contable_ret_renta = (int)$req->input('id_retencion_renta');
			$libroVenta->gravada_5 = (float)$req->input('gravada_5');
			$libroVenta->gravada_10 = (float)$req->input('gravada_10');
			$libroVenta->save();
			$libro =  $libroVenta->id;
			$data = DB::select("SELECT generar_libro_venta_manual(".$libro.",".$idUsuario.")");
			$data = $data[0]->generar_libro_venta_manual;

			if($data == 'OK'){
				$rsp->status = 'OK';
				$rsp->libro = $libro;
			}else{
				$rsp->status = $data ;
			}

		
	
		}catch(\Exception $e){
			Log::error($e);
			$rsp->status = 'Ocurrio un error al intentar generar el libro de ventas.';
		}
		
		return response()->json($rsp);
	}
		/**
		 * Almacena los timbrados que se cargan en el ibro compra
		 */
		public function saveTimbradoVenta(Request $req){
			$obj = new \StdClass;
			$obj->nro_timbrado = $req->input('nro_timbrado');
			$obj->fecha_inicio = $req->input('inicio');
			$obj->fecha_vencimiento = $req->input('vencimiento');
			$obj->id_persona = $req->input('id_persona');
			$obj->denominacion = $req->input('denominacion');
			$resp = $this->timbradoLibroVenta($obj);
	    	return response()->json($resp);
		}//

		private function timbradoLibroVenta($obj){

			//dd($obj);
			   $idUsuario = $this->getIdUsuario();
			   $idEmpresa = $this->getIdEmpresa();
			   $rsp = new \StdClass;
			   $rsp->existe = false;
			   $rsp->err = true;
			   $rsp->id_timbrado = null;
   
			   //try{    
				$fecha_actual = date("d-m-Y");
				$fecha_vencimiento =  date("Y-m-d",strtotime($fecha_actual."- 10 days"));

				//DEVOLVER UN LISTADO DE TIMBRADO
					$verificar = TimbradoLvManual::where('id_empresa',$idEmpresa)
										->where('numero', (Integer)$obj->nro_timbrado)
										->where('fecha_vencimiento','>=',$fecha_vencimiento)
										->get(['id','numero','denominacion']);

				   if(!$verificar->isEmpty()){
					   // dd($verificar);
					   $rsp->existe = true;
					   $rsp->id_timbrado = $verificar[0]->id;
					   
				   } else {
					   $timbrado = new TimbradoLvManual;
					   $timbrado->numero = (Integer)$obj->nro_timbrado;
					   $timbrado->fecha_inicio =  $obj->fecha_inicio;
					   $timbrado->fecha_vencimiento = $obj->fecha_vencimiento;
					   $timbrado->usuario_id = $idUsuario;
					   $timbrado->id_empresa = $idEmpresa;
					   $timbrado->denominacion = $obj->denominacion;
					   $timbrado->save();	
					   $rsp->id_timbrado = $timbrado->id;
					   
				   }
   
   
		   /*} catch(\Exception $e){
			   $rsp->err = false;
		   }*/
   
   
			   return $rsp;
		   }

		   public function datosPermiso($vista, $permiso){

			$btn = DB::select("SELECT pe.* 
							  FROM persona_permiso_especiales p, permisos_especiales pe
							  WHERE p.id_permiso = pe.id 
							  AND p.id_persona = ".$this->getIdUsuario() ." 
							  AND pe.url = '".$vista."' 
							  AND pe.id = ".$permiso);
			//print_r($parametros['id_factura']);
			if(!empty($btn)){
			  $respuesta = 1;  		
				 return $respuesta;
		  } else {
			  $respuesta = 0;
			  return $respuesta;
		  }
	  }//function


	  public function indexCuentaGasto(Request $request){
		$cuentasCostos = CuentaGasto::with('cuentasPadre')->where('id_empresa',$this->getIdEmpresa())->get();
		return view('pages.mc.contabilidad.indexCuenta',compact('cuentasCostos'));
    }//function

	  
	public function cuentaGasto(Request $request){
		$cuentasCostos = CuentaGasto::with('cuentasPadre')->where('id_empresa',$this->getIdEmpresa())->get();
		return view('pages.mc.contabilidad.cuentaGasto',compact('cuentasCostos'));
    }//function

	public function saveCuentaGastos(Request $request)
	{
		$response = new \StdClass; 
		if($request->input('id_cuenta_padre') != ""){
			$id_cuenta_padre = $request->input('id_cuenta_padre');
		}else{
			$id_cuenta_padre = null;
		}

	 	try{
			$cuenta_gasto = new CuentaGasto;
			$cuenta_gasto->denominacion = $request->input('denominacion');
			$cuenta_gasto->id_cuenta_padre = $id_cuenta_padre;
			$cuenta_gasto->id_empresa =  $this->getIdEmpresa();
			$cuenta_gasto->activo = true;
			$cuenta_gasto->fecha_alta = date('Y-m-d H:m:i');
			$cuenta_gasto->id_usuario = $this->getIdUsuario();
			$cuenta_gasto->save();	
			$response->status = 'OK';
		} catch(\Exception $e){
			$response->status = 'ERROR';

		}
		return response()->json($response);

	}


	public function consultaCuentaGasto(Request $request){

		$cuentasCostos = CuentaGasto::with('usuario','cuentasPadre')->where('id_empresa',$this->getIdEmpresa())->get();

		return response()->json($cuentasCostos);

	}

	public function guardarRepriceLibro(Request $request){
		echo '<pre>';
		print_r($request->all());



		

	}




	public function modificarFechaLibro(Request $request, $id)
	{

		$permiso_modificar_fecha = $this->datosPermiso('verLibroCompra',60);
		$libroCompra = LibroCompra::find($id);

	
		// Obtén la fecha del formulario
		$fechaFactura = $request->input('fecha_factura');
	
		// Convierte la fecha al formato 'Y-m-d' (año-mes-día)
		$fechaFormateada = Carbon::createFromFormat('d/m/Y', $fechaFactura)->format('Y-m-d H:i:s');
		$libroCompra->fecha_hora_facturacion = $fechaFormateada;
		$libroCompra->save();
	

		$asiento = Asiento::find($libroCompra->id_asiento);
		$asiento->fecha_hora = $fechaFormateada;
		$asiento->save();

		return response()->json(['data'=>$libroCompra]);
	}
	
//reprice arturo 
public function modificarReprice(Request $request, $id)
{

	DB::beginTransaction();

	
		$reprice = $this->datosPermiso('verLibroCompra',69);
		$id_documento = $request->input('id_documento'); 
		$id_proforma_detalle = $request->input('id_proforma_detalle');
		$id_proforma = $request->input('id_proforma'); 
		$total_factura = $request->input('total_factura');
		//PARA GENERAR UNO NUEVO 
		/*$nuevoLibroCompra = new LibroCompra;

		$total_factura = $request->input('total_factura');
		$exenta = $request->input('exenta');
		$gravadas10 = $request->input('gravada_10');
		$gravadas5 = $request->input('gravada_5');
		$cod_confirmacion = $request->input('cod_confirmacion');

		$nuevoLibroCompra = new LibroCompra;
		$nuevoLibroCompra->importe = $total_factura;
		$nuevoLibroCompra->costo_exenta = $exenta;
		$nuevoLibroCompra->gravadas10 = $gravadas10;
		$nuevoLibroCompra->gravadas5 = $gravadas5;
		$nuevoLibroCompra->cod_confirmacion = $cod_confirmacion;*/

		//$nuevoLibroCompra->save();
		//FIN GENERAR NUEVO 
//COPIAR A HISTORICO FACTURAS CABECERA
$resultados_fact_cab = DB::table('facturas')
->where('id', $id_documento)
->get();
// Asegúrate de tener un solo resultado (si hay resultados)
// Asegúrate de tener un solo resultado (si hay resultados)
//try {
    $resultados_fact_cab = DB::table('facturas')->where('id', $id_documento)->get();
	
	if ($resultados_fact_cab) {
		$resultadosArrayFacCabecera = json_decode(json_encode($resultados_fact_cab[0]), true);
		
	
				$resultadosArrayFacCabecera['id_factura'] = $resultadosArrayFacCabecera['id'];
				unset($resultadosArrayFacCabecera['id']);

				/*echo "<pre>";
				print_r($resultadosArrayFacCabecera);
				echo "</pre>";
				die();*/	
		$ultimoIdInsertado = FacturaCabeceraHistorico::insertGetId($resultadosArrayFacCabecera);
	}
	

//////////////////////////////////////////


//COPIAR A HISTORICO FACTURAS DETALLE

		$resultados_fact = DB::table('facturas_detalle')
        ->where('id_factura', $id_documento)
		->where ('id_proforma_detalle',$id_proforma_detalle)
        ->get();

		if ($resultados_fact) {
			// Convierte los resultados a un array
			$resultadosArrayFac = json_decode(json_encode($resultados_fact), true);
			foreach ($resultadosArrayFac as &$fila) {
				$fila['id_factura_reprice'] = $ultimoIdInsertado;
				$fila['id_factura_detalle'] = $fila['id'];
				
			}
			unset($resultadosArrayFac[0]['id']);

			/*echo "<pre>";
				print_r($resultadosArrayFac);
				echo "</pre>";
				die();*/
			// Inserta los resultados en la tabla historico_libros_compras

			DB::table('facturas_detalle_reprice')->insert($resultadosArrayFac);
			
		}


		//SE HACE UN ESPEJO PARA PROFORMAS Y PODER UTILIZAR REPRICE EN FACTURA PARCIAL
$resultados_proforma_cab = DB::table('proformas')
->where('id', $id_proforma)
->get();

$resultados_proforma_cab = DB::table('proformas')->where('id', $id_proforma)->get();
	
if ($resultados_proforma_cab) {
	$resultadosArrayProformaCabecera = json_decode(json_encode($resultados_proforma_cab[0]), true);
	

			$resultadosArrayProformaCabecera['id_proforma_cabecera'] = $resultadosArrayProformaCabecera['id'];
			unset($resultadosArrayProformaCabecera['id']);

	$ultimoIdInsertadoPro = ProformaReprice::insertGetId($resultadosArrayProformaCabecera);
}
//DETALLE
$resultados_pro = DB::table('proformas_detalle')
->where('id_proforma', $id_proforma)
->where ('id',$id_proforma_detalle)
->get();

if ($resultados_pro) {
	// Convierte los resultados a un array
	$resultadosArrayPro = json_decode(json_encode($resultados_pro), true);
	foreach ($resultadosArrayPro as &$fila) {
		$fila['id_proforma_cabecera'] = $ultimoIdInsertadoPro;
		$fila['id_proforma_detalle'] = $fila['id'];
		
	}
	unset($resultadosArrayPro[0]['id']);
	//unset($resultadosArrayPro[0]['id_proforma']);
	/*echo "<pre>";
		print_r($resultadosArrayFac);
		echo "</pre>";
		die();*/
	// Inserta los resultados en la tabla historico_libros_compras

	DB::table('proformas_detalle_reprice')->insert($resultadosArrayPro);
	
}


/////////////
		$resultados = DB::table('libros_compras')
        ->where('id_documento', $id_documento)
		->orderBy('id', 'desc') 
		->limit(1)
        ->get();
		if ($resultados) {
			// Convierte los resultados a un array
			$resultadosArray = json_decode(json_encode($resultados), true);
			// Inserta los resultados en la tabla historico_libros_compras
			DB::table('historico_libros_compras')->insert($resultadosArray);
		}



//$libroCompra = LibroCompra::find($id);
	$libroCompra = LibroCompraReprice::latest('id_historico')->first();
	$total_factura = $request->input('total_factura');
	$exenta = $request->input('exenta');
	$cod_confirmacion = $request->input('cod_confirmacion');
	$libroCompra->importe = $total_factura;
	$libroCompra->costo_exenta = $total_factura;
	$libroCompra->exenta = $total_factura;
	$libroCompra->cod_confirmacion = $cod_confirmacion;
	$libroCompra->save();

	/*$id_asiento = $request->input('id_asiento');
	$asiento = Asiento::find($id_asiento);
	//$asiento->activo = false; 
	$asiento->save();*/

//PARA MODIFICAR EL COSTO DE LA FACTURA
	$id_proforma_detalle = $request->input('id_proforma_detalle');
	//no se actulaizara la factura solo proforma
	$factura = FacturaDetalleHistorico::where('id_proforma_detalle', $id_proforma_detalle)
									->orderBy('id', 'desc') 
									->limit(1)
									->first();

	$factura->costo_proveedor = $request->input('total_factura'); 
	$factura->precio_costo = $request->input('total_factura'); 
	$factura->cod_confirmacion = $request->input('cod_confirmacion'); 
	$factura->save();
//MODIFICAR COSTO EN PROFORMA
	$proforma = ProformasDetalle::where('id', $id_proforma_detalle)->first();
	//$proforma->costo_proveedor = $request->input('total_factura');
	//$proforma->precio_costo = $request->input('total_factura');
	$proforma->reprice = true;
	$proforma->save();
//PARA REPRICE PARCIAL
$id_proforma_detalle = $request->input('id_proforma_detalle');
/*$parcial = ProformaDetalleReprice::where('id_proforma_detalle', $id_proforma_detalle)
								->orderBy('id', 'desc')
								->limit(1)
								->first();
	$parcial->costo_proveedor = $request->input('total_factura'); 
	$parcial->precio_costo = $request->input('total_factura'); 
	$parcial->cod_confirmacion = $request->input('cod_confirmacion'); 
	$parcial->save();*/
	$parcial = ProformaDetalleReprice::where('id_proforma_detalle', $id_proforma_detalle)
    ->orderBy('id', 'desc')
    ->first();

if ($parcial) {
    $parcial->update([
        'costo_proveedor' => $request->input('total_factura'),
        'precio_costo' => $request->input('total_factura'),
        'cod_confirmacion' => $request->input('cod_confirmacion'),
    ]);
}


	/////////////////
	$recalcular_renta= DB::select("SELECT * FROM recalcular_renta_factura_reprice($ultimoIdInsertado)");
//FIN MODIFICACON FACTURA

//AJUSTAMOS LA RENTA PARA LA FACTURA REPRICE
$id_proforma = $request->input('id_proforma'); 
$resultaldo_renta = DB::table('proformas_reprice')
					->join('proformas_detalle_reprice', 'proformas_detalle_reprice.id_proforma', '=', 'proformas_reprice.id_proforma_cabecera')
					->select('proformas_detalle_reprice.renta')
					->where('proformas_reprice.id_proforma_cabecera',$id_proforma)
					->orderBy('proformas_detalle_reprice.id', 'desc')
					->limit(1)
					->first();		
					$id_proforma_detalle = $request->input('id_proforma_detalle');
					$factura = FacturaDetalleHistorico::where('id_proforma_detalle', $id_proforma_detalle)
					->orderBy('id', 'desc') 
					->limit(1)
					->first();
					$factura->renta = $resultaldo_renta->renta;
					$factura->save();
//FIN RENTA FACTURA
//hago otro select y llamar a la funcion para poder recalcular
$resultados2 = DB::table('libros_compras')
    ->where('id_documento', $id_documento)
    ->select('id_documento')
    ->get();
	$id_libro_compra = $request->input('id');
	$id_persona = $request->input('cliente_id');
	if ($resultados2->isNotEmpty()) {
		$id_documento = $resultados2[0]->id_documento;
		try {
			//$recalcular = DB::select("SELECT * from public.generar_libro_compra_reprice($id_documento, " . Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario . ")")[0]->generar_libro_compra_reprice;
			$recalcular = DB::select("SELECT * FROM public.generar_libro_compra_reprice($id_documento, " . Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario . ", $id_libro_compra,$id_proforma_detalle,$id_persona)")[0]->generar_libro_compra_reprice;

		
			if($recalcular !='OK'){
			DB::rollback();
			return response()->json(['error' => true,'message' => $recalcular]);
		}
		} catch (\Exception $e) {
			$mensaje = $e->getMessage();
		DB::rollback();
			 return response()->json(['error' => $mensaje], 500);
		}
	} else {
		DB::rollback();
		return response()->json(['error' => true,'message' => "No se encontraron resultados."]);
		//throw new \Exception("No se encontraron resultados.");
	}
	DB::commit();
//para actualizar el proveedor en el item de la proforma
/*$proforma_proved = DB::table('proformas_detalle_reprice')
->where('id_proforma_detalle', $id_proforma_detalle)
->orderBy('id', 'desc')
->limit(1)
->update([
	'id_proveedor' => $request->input('cliente_id'),
	'cod_confirmacion' => $request->input('cod_confirmacion'),
]);*/
$ultimoId = DB::table('proformas_detalle_reprice')
    ->where('id_proforma_detalle', $id_proforma_detalle)
    ->orderBy('id', 'desc')
    ->value('id');

if ($ultimoId) {
    DB::table('proformas_detalle_reprice')
        ->where('id', $ultimoId)
        ->update([
            'id_proveedor' => $request->input('cliente_id'),
            'cod_confirmacion' => $request->input('cod_confirmacion'),
        ]);
}


//PARA LA RENTA EN FACTURAS
$id_proforma = $request->input('id_proforma'); 
$resultaldo_renta_fac = DB::table('proformas_reprice')
					->join('proformas_detalle_reprice', 'proformas_detalle_reprice.id_proforma', '=', 'proformas_reprice.id_proforma_cabecera')
					->join('facturas', 'facturas.id_proforma', '=', 'proformas_reprice.id_proforma_cabecera')
					->select('proformas_reprice.renta_neta')
					->where('proformas_detalle_reprice.id_proforma',$id_proforma)
					->orderBy('proformas_reprice.id', 'desc')
					->limit(1)
					->first();	
					$factura_renta = Factura::where('id_proforma', $id_proforma)
					->orderBy('id', 'desc') 
					->first();
					$factura_renta->renta_reprice = $resultaldo_renta_fac->renta_neta;
					$factura_renta->save();
//////////////////////
	$libroCompra2 = LibroCompra::find($id);
	$libroCompra2->reprice_pago_pro = true;
	$libroCompra2->save();
	$libroCompraAnterior = LibroCompra::latest('id')->first();
	$libroCompraAnterior->id_libro_compra_anterior = $libroCompra2->id;
	$libroCompraAnterior->save();


	$reprice_fact = FacturaDetalle::where('id_proforma_detalle', $id_proforma_detalle)
	->orderBy('id', 'desc') 
	->first();
	$reprice_fact->reprice = true;
	$reprice_fact->save();

	return response()->json(['error' => false,'message' => "Datos actualizados y copiados correctamente"]);
////


	//return redirect()->route('verLibroCompra', ['id' => $libroCompra->id])->with('success', 'Fecha actualizada correctamente.');
	//return response()->json(['data'=>$libroCompra]);
	//return response()->json(['data' => $nuevoLibroCompra]);
	return response()->json(['message' => 'Datos actualizados y copiados correctamente.']);
}



}
