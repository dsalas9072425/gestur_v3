<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use App\Currency;
use App\EstadoCuenta;
use App\Grupo;
use App\Producto;
use App\Persona;
use App\TipoTicket;
use App\Ticket;
use App\LineaCredito;
use DB;
use Response;
use Image; 

class EstadoCuentasController extends Controller{

	public function index(Request $request){

		$estado_cuenta = [];
		$cuentas = DB::select("SELECT p.id, 
									  p.nombre, 
									  c.persona_id, 
									  c.saldo, 
									  l.monto, 
									  c.fecha, 
									  l.monto - c.saldo as saldo_real

							FROM estado_cuenta c, linea_de_credito l, personas p 
							WHERE c.persona_id = l.id_persona AND l.activo = true AND p.id = c.persona_id 
							AND p.id_tipo_persona = 8 AND c.id_empresa = ".$this->getIdEmpresa()." 
							GROUP BY p.id, p.nombre, c.persona_id, c.saldo, l.monto, c.fecha, l.monto - c.saldo
							ORDER BY c.persona_id, c.fecha DESC");

		foreach ($cuentas as $key => $cuenta) {
			$cuenta->icoSaldo = "fa fa-exclamation-triangle icoRojo";
         	$cuenta->pesoIcoSaldo = '0';
         	$saldo = $cuenta->saldo;
         	if ($cuenta->monto != 0) 
         	{
	         	$total = round(($saldo * 100) / $cuenta->monto);
         	}else{
         		$total = 0;
         	}

         	if ($total <= 30) {
         		$cuenta->pesoIcoSaldo = '1';
                $cuenta->icoSaldo = "fa fa-thumbs-o-up icoVerde";
         	}
         	elseif ($total > 30 && $total <= 70) {
         		$cuenta->pesoIcoSaldo = '2';
                $cuenta->icoSaldo = "fa fa-exclamation-triangle icoAmarillo";
         	}
         	elseif ($total > 70) {
         		$cuenta->pesoIcoSaldo = '3';
                $cuenta->icoSaldo = "fa fa-exclamation-triangle icoRojo";
         	}

			if ($key == 0) {
				$estado_cuenta[$cuenta->persona_id]['id'] = $cuenta->id;
				$estado_cuenta[$cuenta->persona_id]['nombre'] = $cuenta->nombre;
				$estado_cuenta[$cuenta->persona_id]['saldo'] = number_format($cuenta->saldo,2,",",".");
				$estado_cuenta[$cuenta->persona_id]['monto'] = number_format($cuenta->monto,2,",",".");
				$estado_cuenta[$cuenta->persona_id]['saldo_real'] = number_format($cuenta->saldo_real,2,",",".");
				$persona_anterior = $cuenta->persona_id;
				$estado_cuenta[$cuenta->persona_id]['pesoIcoSaldo'] = $cuenta->pesoIcoSaldo;
				$estado_cuenta[$cuenta->persona_id]['icoSaldo'] = $cuenta->icoSaldo;
				
			}
			elseif ($persona_anterior != $cuenta->persona_id) {
				$estado_cuenta[$cuenta->persona_id]['id'] = $cuenta->id;
				$estado_cuenta[$cuenta->persona_id]['nombre'] = $cuenta->nombre;
				$estado_cuenta[$cuenta->persona_id]['saldo'] = number_format($cuenta->saldo,2,",",".");
				$estado_cuenta[$cuenta->persona_id]['monto'] = number_format($cuenta->monto,2,",",".");
				$estado_cuenta[$cuenta->persona_id]['saldo_real'] = number_format($cuenta->saldo_real,2,",",".");
				$persona_anterior = $cuenta->persona_id;
				$estado_cuenta[$cuenta->persona_id]['pesoIcoSaldo'] = $cuenta->pesoIcoSaldo;
				$estado_cuenta[$cuenta->persona_id]['icoSaldo'] = $cuenta->icoSaldo;
			}
		}

		$currencys = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$personas = Persona::where('id_tipo_persona', '8')
							->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->get(); 

		return view('pages.mc.estadoCuentas.index', compact('estado_cuenta', 'currencys', 'personas'));
	}


	public function indexDetallado($id){
		$cuentas = DB::select("SELECT 
							p.id,p.nombre, 
							c.persona_id, 
							c.saldo, 
							c.debe, 
							c.haber,
							l.monto, 
							c.fecha, 
							l.monto - c.saldo as deuda, 
							tdoc.denominacion, 
							c.documento,
							c.importe_gs
				FROM estado_cuenta c, linea_de_credito l, personas p, tipo_documento_ctacte tdoc
				WHERE c.persona_id = l.id_persona AND l.activo = true AND p.id = c.persona_id AND tdoc.id = c.tipo_documento_id
				AND p.id_tipo_persona = 8 AND p.id = $id AND c.visible = true AND c.id_empresa = ".$this->getIdEmpresa()." 
				GROUP BY p.id,p.nombre, c.persona_id, c.saldo, c.debe, c.haber, l.monto, c.fecha, l.monto - c.saldo, tdoc.denominacion, c.documento, c.importe_gs
				ORDER BY c.persona_id, c.fecha DESC");
		// dd($cuentas);

		foreach ($cuentas as $key => $cuenta) {
			if ($key == 0) {
				$estado_cuenta[$key]['persona_id'] = $cuenta->persona_id;
				$estado_cuenta[$key]['nombre'] = $cuenta->nombre;
				$estado_cuenta[$key]['denominacion'] = $cuenta->denominacion;
				$estado_cuenta[$key]['documento'] = $cuenta->documento;
				$estado_cuenta[$key]['fecha'] = $cuenta->fecha;
				$estado_cuenta[$key]['saldo'] = number_format($cuenta->saldo,2,",",".");
				$estado_cuenta[$key]['monto'] = number_format($cuenta->monto,2,",",".");
				$estado_cuenta[$key]['debe'] = number_format($cuenta->debe,2,",",".");
				$estado_cuenta[$key]['haber'] = number_format($cuenta->haber,2,",",".");
				$estado_cuenta[$key]['deuda'] = number_format($cuenta->deuda,2,",",".");
				$estado_cuenta[$key]['importe_gs'] = number_format($cuenta->importe_gs,2,",",".");
				$persona_anterior = $cuenta->persona_id;

			}
			elseif ($persona_anterior != $cuenta->persona_id) {
				$estado_cuenta[$key]['persona_id'] = $cuenta->persona_id;
				$estado_cuenta[$key]['nombre'] = $cuenta->nombre;
				$estado_cuenta[$key]['denominacion'] = $cuenta->denominacion;
				$estado_cuenta[$key]['documento'] = $cuenta->documento;
				$estado_cuenta[$key]['fecha'] = $cuenta->fecha;
				$estado_cuenta[$key]['saldo'] = number_format($cuenta->saldo,2,",",".");
				$estado_cuenta[$key]['monto'] = number_format($cuenta->monto,2,",",".");
				$estado_cuenta[$key]['debe'] = number_format($cuenta->debe,2,",",".");
				$estado_cuenta[$key]['haber'] = number_format($cuenta->haber,2,",",".");
				$estado_cuenta[$key]['deuda'] = number_format($cuenta->deuda,2,",",".");
				$estado_cuenta[$key]['importe_gs'] = number_format($cuenta->importe_gs,2,",",".");
				$persona_anterior = $cuenta->persona_id;

			}
		}
		// 
		$lineaCredito = DB::select("SELECT l.monto 
				FROM linea_de_credito l, personas p
				WHERE p.id = l.id_persona AND l.activo = true AND  p.id =".$id." AND l.id_empresa = ".$this->getIdEmpresa());
				
		$currencys = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$agencias = Persona::where('id_tipo_persona', '8')->where('id_empresa',$this->getIdEmpresa())->where('activo',true)->get();
		// $estado_cuenta = json_encode($estado_cuenta);
// dd($estado_cuenta);
		return view('pages.mc.estadoCuentas.indexDetallado', compact('estado_cuenta', 'currencys', 'agencias', 'lineaCredito', 'id'));
	}

	private function getIdEmpresa()
	{
	  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}

	



	public function getCuenta(Request $request){

		$cuentas = DB::select("SELECT p.nombre, c.persona_id, c.saldo, 
							l.monto, c.fecha, l.monto - c.saldo as saldo_real
							FROM estado_cuenta c, linea_de_credito l, personas p 
							WHERE c.id_empresa = ".$this->getIdEmpresa()." AND c.persona_id = l.id_persona AND l.activo = true AND p.id = c.persona_id 
							AND p.id_tipo_persona = 8 AND c.id_empresa = l.id_empresa
							GROUP BY  p.nombre, c.persona_id, c.saldo, l.monto, c.fecha, l.monto - c.saldo
							ORDER BY c.persona_id, c.fecha  DESC ");
		/*echo '<pre>';
		print_r($cuentas);*/

		$estado_cuenta = [];
		
		foreach ($cuentas as $key => $cuenta) {
			$cuenta->icoSaldo = "fa fa-exclamation-triangle icoRojo";
         	$cuenta->pesoIcoSaldo = '0';
         	$saldo = $cuenta->saldo;

         	if($cuenta->monto != 0 && $saldo != 0){
         		$total = round(($saldo * 100) / $cuenta->monto);
         	}else{
         		$total = 0;
         	}

         	if ($total <= 30) {
         		$cuenta->pesoIcoSaldo = '1';
                $cuenta->icoSaldo = "fa fa-thumbs-o-up icoVerde";
         	}
         	elseif ($total > 30 && $total <= 70) {
         		$cuenta->pesoIcoSaldo = '2';
                $cuenta->icoSaldo = "fa fa-exclamation-triangle icoAmarillo";
         	}
         	elseif ($total > 70) {
         		$cuenta->pesoIcoSaldo = '3';
                $cuenta->icoSaldo = "fa fa-exclamation-triangle icoRojo";
         	}

			if ($key == 0) {
				$estado_cuenta[$cuenta->persona_id]['persona_id'] = $cuenta->persona_id;
				$estado_cuenta[$cuenta->persona_id]['nombre'] = $cuenta->nombre;
				$estado_cuenta[$cuenta->persona_id]['saldo'] = number_format($cuenta->saldo,2,",",".");
				$estado_cuenta[$cuenta->persona_id]['monto'] = number_format($cuenta->monto,2,",",".");
				$estado_cuenta[$cuenta->persona_id]['saldo_real'] = number_format($cuenta->saldo_real,2,",",".");
				$persona_anterior = $cuenta->persona_id;
				$estado_cuenta[$cuenta->persona_id]['pesoIcoSaldo'] = $cuenta->pesoIcoSaldo;
				$estado_cuenta[$cuenta->persona_id]['icoSaldo'] = $cuenta->icoSaldo;

			}
			elseif ($persona_anterior != $cuenta->persona_id) {
				$estado_cuenta[$cuenta->persona_id]['persona_id'] = $cuenta->persona_id;
				$estado_cuenta[$cuenta->persona_id]['nombre'] = $cuenta->nombre;
				$estado_cuenta[$cuenta->persona_id]['saldo'] = number_format($cuenta->saldo,2,",",".");
				$estado_cuenta[$cuenta->persona_id]['monto'] = number_format($cuenta->monto,2,",",".");
				$estado_cuenta[$cuenta->persona_id]['saldo_real'] = number_format($cuenta->saldo_real,2,",",".");
				$persona_anterior = $cuenta->persona_id;
				$estado_cuenta[$cuenta->persona_id]['pesoIcoSaldo'] = $cuenta->pesoIcoSaldo;
				$estado_cuenta[$cuenta->persona_id]['icoSaldo'] = $cuenta->icoSaldo;

			}
			}

			foreach ($estado_cuenta as $key => $cuenta) {
				if (!empty($request->input('persona'))) {
					if ((int)$cuenta['persona_id'] != (int)$request->input('persona')) {
					unset($estado_cuenta[$key]);
					}
				}	
			}

		$currencys = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$personas = Persona::where('id_tipo_persona', '8')->where('id_empresa',$this->getIdEmpresa())->get();

		return response()->json($estado_cuenta);

	}

	public function getDetallesCuenta(Request $request){
		//dd($request->all());
		$fechaTrim = trim($request->input('periodo'));
		$periodo = explode('-', trim($fechaTrim));
		$desde = explode("/", trim($periodo[0]));
		$hasta = explode("/", trim($periodo[1]));
		$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0];
		$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0];
		$id = $request->input('id');
		$visible = $request->input('visible');

		$detalles = DB::select("SELECT c.fecha, t.denominacion, c.documento, c.debe, c.haber, c.importe_gs, c.saldo
			FROM estado_cuenta c
			JOIN personas p on p.id = c.persona_id
			JOIN tipo_documento_ctacte t on t.id = c.tipo_documento_id
			WHERE c.fecha BETWEEN '".$fecha_desde." 00:00:00' AND '".$fecha_hasta." 23:59:59'
			AND c.visible = '".$visible."' AND c.persona_id = ".$id." AND c.id_empresa = ".$this->getIdEmpresa());

		$estado_cuenta = [];
		foreach ($detalles as $key => $detalle) {
				$estado_cuenta[$key]['denominacion'] = $detalle->denominacion;
				$estado_cuenta[$key]['documento'] = $detalle->documento;
				$estado_cuenta[$key]['fecha'] = date('d/m/Y H:m:s', strtotime($detalle->fecha));
				$estado_cuenta[$key]['saldo'] = number_format($detalle->saldo,2,",",".");
				$estado_cuenta[$key]['debe'] = number_format($detalle->debe,2,",",".");
				$estado_cuenta[$key]['haber'] = number_format($detalle->haber,2,",",".");
				$estado_cuenta[$key]['importe_gs'] = number_format($detalle->importe_gs,2,",",".");

		}

		return response()->json($estado_cuenta);
		// dd($detalles);
	}



}

