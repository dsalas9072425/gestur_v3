<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use DB;

class MenuController extends Controller
{

    public function addMenu(Request $req){

        $results = 'true';
        ini_set('memory_limit', '-1');
		set_time_limit(300);
        $id = $req->id;
        $datos = Menu::select('id', 'descripcion')
                           ->where('id_menu_padre', '=', 0)
                           ->where('activo', '=', true)
                           ->orderBy('descripcion', 'asc')
                           ->get();
        return view('pages.mc.menu.addMenu')->with(['menuPadre'=>$datos]);
    }


    public function agregarNuevoMenu(Request $req){

        $results = 'true';
        ini_set('memory_limit', '-1');
		set_time_limit(300);
        $nombre=$req -> menu;
        $url=$req->url;
        $icono=$req->icono;
        $padre=$req->submenu;
        $estado=$req->estado;
        $visibilidad=$req->visibilidad;
        if($padre == ""){
            $padre=0;
            $datos = Menu::select('orden')
            ->where('id_menu_padre', '=', $padre)
            ->where('activo', '=', true)
            ->get();
            $order= (count($datos))+1;
        }else{
            $datos = Menu::select('orden')
            ->where('id_menu_padre', '=', $padre)
            ->where('activo', '=', true)
            ->get();
            $order= (count($datos))+1;
        }

	    $nuevoMenu = new Menu;
		$nuevoMenu->descripcion = $nombre;
		$nuevoMenu->url = $url;
        $nuevoMenu->icono = $icono;
        $nuevoMenu->id_menu_padre = $padre;
        $nuevoMenu->orden = $order;
        $nuevoMenu->activo = $estado;
        $nuevoMenu->visible = $visibilidad;
		$nuevoMenu->save();

        return response()->json(array('estado'=>1));
    }

    public function indexMenu(){
        return view('pages.mc.menu.index');

    }
    public function verDatosMenu(){
        $datos=Menu::all();

        foreach($datos as $i=>$dato) { 
            $nombraPadre="";
            if($dato['id_menu_padre'] !== 0){
                    $auxiliarNombre = Menu::select('descripcion')
                    ->where('id', '=', $dato['id_menu_padre'])
                    ->get();
                    foreach ($auxiliarNombre as $key) {		
                        $nombraPadre=$key;
                    }
                   
                    }
                $data  ['data'][] =  [
                    'descripcion'=>$dato['descripcion'],
                    'url'=>$dato['url'],
                    'icono'=>$dato['icono'],
                    'orden'=>$dato['orden'],
                    'activo'=>$dato['activo'],
                    'id_menu_padre'=>$dato['id_menu_padre'],
                    'nombrePadre'=>$nombraPadre,


                    'id'=>$dato['id'],
                    ];
        }
        return response()->json($data);
    }

    public function editarMenu($id){
        //Capturamos los datos para la mascara de editar datos.
        $menu = Menu::find($id);
        $datos = Menu::select('id', 'descripcion')->where('id_menu_padre', '=', 0)->get();
        return view('pages.mc.menu.editMenu')->with(['menu'=>$menu,'datos'=>$datos]);
    }

    public function guardarEditar(Request $req){
        //recibimos todos las datos del fomulario de edicio para procesar y guardar cambios en la BD
        $idMenuSeleccionado= $req -> id;
        $descipcion= $req->descripcion;
        $url= $req -> url;
        $icono= $req -> icono;
        $estado = $req -> estado;
        if($estado == 1){
            $estado = true;
        }else{
            $estado = false;
        }
        $visibilidad= $req -> visibilidad;
        if($visibilidad == 1){
            $estado = true;
        }else{
            $estado = false;
        }
        $menuPadre = $req -> submenu;
        DB::table('menu')
            ->where('id', $idMenuSeleccionado)
            ->update(['descripcion' => $descipcion, 'url' => $url, 'icono' => $icono, 'activo' => $estado, 'id_menu_padre' => $menuPadre, 'visible' => $visibilidad ]);

        return response()->json(array('estado'=>1));
    }
}
