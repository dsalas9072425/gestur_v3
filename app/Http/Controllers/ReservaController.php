<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use App\Notificacion;
use App\ReservasServicio;
use App\Sucursal;
use App\Log;
use App\Agencias;
use App\State;
use App\Reserva;
use App\Usuario;
use App\ReservasVuelos;
use App\GestionPagos;
use App\Banco;
use App\Paymentmethod;
use App\Currency;
use Session;
use Redirect;
use DB;

class ReservaController extends Controller
{
	//Funcion para reservas por usuario
	public function misReservas(){

		//Inicia Reservas de Hoteles
		$listado = array();
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		
		$fecha = date('Y-m-d');
		$nuevafecha = date("Y-m-d",strtotime($fecha."- 30 day"));
		$fechaHoy = date("Y-m-d",strtotime($fecha."+ 1 day"));
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:i', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:i', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					 ->get();
			    break;
			case 2:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->orderBy('fecha_reserva', 'desc')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();

			    break;
			case 3:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}else{
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}else{
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}	
			break;
		}

		foreach($results as $reserva){
			$lista = new \StdClass;
			$lista->id_reserva = $reserva->id;
			$lista->expediente = $reserva->id_file_reserva;
			$lista->proforma = $reserva->numero_proforma;
			$lista->localizador = $reserva->codigo;
			$lista->destino = $reserva->destino->desc_destino;
			$lista->fecha =  $reserva->fecha_reserva;
			$lista->pasajero = $reserva->pasajero_principal_apellido.", ".$reserva->pasajero_principal_nombre;
			$lista->agencia = $reserva->agencia->razon_social;
			$lista->agencia_id = $reserva->agencia->id_agencia;
			$lista->vendedor_agencia = $reserva->usuario->usuario;
			$lista->cancelacion_monto = $reserva->cancelacion_monto;
			$lista->cancelacion_desde = $reserva->cancelacion_desde;
			$lista->id_proveedor = $reserva->id_proveedor;
			if($reserva->id_agencia == 1){
				$lista->vendedor_dtp = $reserva->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			if($reserva->enviado_proforma =="S"&& $reserva->codret_proforma == "0"){
				$lista->iconoproforma = 0;
			}else{
				//if($reserva->id_estado_reserva != config('constants.resConfirmada')){
					$lista->iconoproforma = 1;
				/*}else{
					$lista->iconoproforma = 1;	
				}*/
			}

			if($reserva->id_estado_reserva == config('constants.resEliminada')){
				if($reserva->cancelado_proforma == "S" && $reserva->codret_proforma_cancelacion == "0"){
					$lista->iconocancelar = 2;
				}else{
					$lista->iconocancelar = 1;
				}
			}else{
				$lista->iconocancelar = 0;
			}

			$lista->expediente_vs = "";
			$lista->factura = "";
			//$lista->monto_neto = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			$lista->monto_sin_comision = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			//$lista->monto = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->monto_cobrado = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->estado = $reserva->id_estado_reserva;
			$lista->iconoproforma=0;
			//$lista->estado = config('constants.resConfirmada');

			//dd($lista);
			$listado[] =  $lista;
		}
		$arrayState = [];
		$states = State::where('activo', '=', true)->get();	
		foreach ($states as $key=>$state){
			$arrayState[$key]['value']= $state->id_estado_reserva;
			$arrayState[$key]['label']= $state->nombre_estado;
		}
		//Fin busqueda de Hotel
		
		//Inicio de Reservas Vuelos		
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')->get();
			    break;
			case 2:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('agencia_id',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				}else{
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				}else{
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				}	
			break;

		}

		//Fin de Reservas Vuelas

		include("../comAerea.php");
		$arrayAerea = [];
		foreach ($comAerea as $key=>$area){
			$nombre = explode("|", $area);
			$arrayAerea[$key]['label']= $nombre[0];
		}

		//Inicio de Servicios Circuitos
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    										->where('tipo_servicio','=',config('constants.circuito'))
			    										->get();
			    break;
			case 2:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.circuito'))
								    					->where('id_agencia',$datosUsuarios->idAgencia)
								    					->get();
			    break;
			case 3:
				$resultadosCircuitos = [];
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
				break;
			case 4:
				$resultadosCircuitos = [];
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
				break;	
		}
		$resultsCircuitos  = array();
		foreach($resultadosCircuitos as $key=>$reservaCircuitos){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaCircuitos->id;
			$lista->localizador = $reservaCircuitos->localizador;
			$lista->proforma = $reservaCircuitos->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaCircuitos->fecha_reserva));
			$lista->pasajero = $reservaCircuitos->pasajero_principal;
			$lista->estado = $reservaCircuitos->id_estado_reserva;
			$lista->id_proveedor = $reservaCircuitos->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaCircuitos->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaCircuitos->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaCircuitos->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaCircuitos->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaCircuitos->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkout));
			$lista->monto = $reservaCircuitos->precio_venta;
			if($reservaCircuitos->id_agencia == 1){
				if(isset($reservaCircuitos->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaCircuitos->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}

			}else{
				$user = Usuario::where('id_usuario', '=', $reservaCircuitos->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsCircuitos[$key] = $lista;
		}	
		//Fin Servicios Circuitos


		//Inicio de Servicios Traslado
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.traslado'))
			    										->get();
			    break;
			case 2:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;
				
			}
		$resultsTraslado  = array();
		foreach($resultadosTraslado as $key=>$reservaTraslado){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaTraslado->id;
			$lista->localizador = $reservaTraslado->localizador;
			$lista->proforma = $reservaTraslado->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaTraslado->fecha_reserva));
			$lista->pasajero = $reservaTraslado->pasajero_principal;
			$lista->estado = $reservaTraslado->id_estado_reserva;
			$lista->id_proveedor = $reservaTraslado->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaTraslado->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaTraslado->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaTraslado->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaTraslado->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaTraslado->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaTraslado->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaTraslado->fecha_checkout));
			$lista->monto = $reservaTraslado->precio_venta;
			if($reservaTraslado->id_agencia == 1){
				if(isset($reservaTraslado->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaTraslado->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsTraslado[$key] = $lista;
		}	
		//Fin Servicios Traslado

		//Inicio de Servicios Actividad
		$resultadosActividades=[];
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio',config('constants.actividad'))
			    										->get();
			    break;
			case 2:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;	
			}

		$resultadosActividad  = array();
		foreach($resultadosActividades as $key=>$reservaActividad){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaActividad->id;
			$lista->localizador = $reservaActividad->localizador;
			$lista->proforma = $reservaActividad->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaActividad->fecha_reserva));
			$lista->pasajero = $reservaActividad->pasajero_principal;
			$lista->id_proveedor = $reservaActividad->id_proveedor;
			if(isset($reservaActividad->agencias->razon_social)){
				$lista->agencia = $reservaActividad->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaActividad->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaActividad->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaActividad->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaActividad->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaActividad->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaActividad->fecha_checkout));
			$lista->monto = $reservaActividad->precio_venta;
			$lista->estado = $reservaActividad->id_estado_reserva;
			if($reservaActividad->id_agencia == 1){
				if(isset($reservaActividad->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaActividad->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}else{
				$user = Usuario::where('id_usuario', '=', $reservaActividad->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultadosActividad[$key] = $lista;
		}	
		//Fin Servicios Traslado
		$client = new Client();
		$cuerpoProforma = [];
		$RestConsultaProfIn= new \StdClass;
		$RestConsultaProfIn->reqToken = $datosUsuarios->tokenBk;
		$RestConsultaProfIn->ts = date('Y-m-d h:m:s');
		$Detalles= new \StdClass;
		$Detalles->FechaDesde = '2017-01-01';
		$Detalles->FechaHasta = date('Y-m-d');
		$Detalles->VendedorId = $datosUsuarios->id_sistema_facturacion;
		$RestConsultaProfIn->Detalles =  $Detalles;
		$iibReq = new \StdClass;
		$iibReq->RestConsultaProfIn = $RestConsultaProfIn;
		try{
			$iibRsp = $client->post(Config::get('config.iibConProForma'), [
				'json' => $iibReq
			]);
			$iibObjRsp = json_decode($iibRsp->getBody());
			if(isset($iibObjRsp->RestConsultaProfOut->Proformas)){
				foreach($iibObjRsp->RestConsultaProfOut->Proformas as $key=>$datosProforma){
					$cuerpoProforma[$key]['value'] = $datosProforma->ProformaId;
					$cuerpoProforma[$key]['label'] = $datosProforma->ProformaId." - ".$datosProforma->ProformasNombrePasajero;
				}
			}
		}
		catch(RequestException $e){
			//entra aca pero si pones Exception nomas no agarra
		}

		if($datosUsuarios->idAgencia == Config::get('constants.agenciaDtp')){
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_usuario =".$datosUsuarios->idUsuario);
		}else{
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_perfil = 10");
		}

		$arrayAgente = [];
		foreach($agentes as $key=>$agente){
			//$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_vendedor_dtp;
			$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_sistema_facturacion;
			$arrayAgente[$key]['label']= $agente->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agente->usuario;
		}	

        if(empty($listado)&&empty($resultsCircuitos)&&empty($resultsTraslado)&&empty($resultadosActividad)&&empty($resultsFligth)){
        	flash('<b>NO POSEE RESERVAS REGISTRADAS</b>')->success();
			return Redirect::back();
        }else{
        	return view('pages.misReservas')->with(['reservas'=>$listado,'reservasCiruito'=>$resultsCircuitos,'reservasTraslado'=>$resultsTraslado,'reservasActividad'=>$resultadosActividad,'resultsFligth'=>$resultsFligth, 'listadoComp'=>$arrayAerea, 'selectProforma'=>$cuerpoProforma, 'selectAgentes'=>$arrayAgente, 'listadoState'=>$arrayState, 'isDtp' => $datosUsuarios->idAgencia == 1 ? true : false]);
        }
	}
	public function reservas(){

		//Inicia Reservas de Hoteles
		$listado = array();
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		
		$fecha = date('Y-m-d');
		$nuevafecha = date("Y-m-d",strtotime($fecha."- 30 day"));
		$fechaHoy = date("Y-m-d",strtotime($fecha."+ 1 day"));
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:i', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:i', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					 ->get();
			    break;
			case 2:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->orderBy('fecha_reserva', 'desc')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();

			    break;
			case 3:
					$results =  [];
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}else{
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}	
			break;
			case 10:
					$results =  [];
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				break;
		}

		foreach($results as $reserva){
			$lista = new \StdClass;
			$lista->id_reserva = $reserva->id;
			$lista->expediente = $reserva->id_file_reserva;
			$lista->proforma = $reserva->numero_proforma;
			$lista->localizador = $reserva->codigo;
			$lista->destino = $reserva->destino->desc_destino;
			$lista->fecha =  $reserva->fecha_reserva;
			$lista->pasajero = $reserva->pasajero_principal_apellido.", ".$reserva->pasajero_principal_nombre;
			$lista->agencia = $reserva->agencia->razon_social;
			$lista->agencia_id = $reserva->agencia->id_agencia;
			$lista->vendedor_agencia = $reserva->usuario->usuario;
			$lista->cancelacion_monto = $reserva->cancelacion_monto;
			$lista->cancelacion_desde = $reserva->cancelacion_desde;
			$lista->id_proveedor = $reserva->id_proveedor;
			if($reserva->id_agencia == 1){
				$lista->vendedor_dtp = $reserva->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			if($reserva->enviado_proforma =="S"&& $reserva->codret_proforma == "0"){
				$lista->iconoproforma = 0;
			}else{
				//if($reserva->id_estado_reserva != config('constants.resConfirmada')){
					$lista->iconoproforma = 1;
				/*}else{
					$lista->iconoproforma = 1;	
				}*/
			}

			if($reserva->id_estado_reserva == config('constants.resEliminada')){
				if($reserva->cancelado_proforma == "S" && $reserva->codret_proforma_cancelacion == "0"){
					$lista->iconocancelar = 2;
				}else{
					$lista->iconocancelar = 1;
				}
			}else{
				$lista->iconocancelar = 0;
			}

			$lista->expediente_vs = "";
			$lista->factura = "";
			//$lista->monto_neto = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			$lista->monto_sin_comision = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			//$lista->monto = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->monto_cobrado = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->estado = $reserva->id_estado_reserva;
			$lista->iconoproforma=0;
			//$lista->estado = config('constants.resConfirmada');

			//dd($lista);
			$listado[] =  $lista;
		}
		$arrayState = [];
		$states = State::where('activo', '=', true)->get();	
		foreach ($states as $key=>$state){
			$arrayState[$key]['value']= $state->id_estado_reserva;
			$arrayState[$key]['label']= $state->nombre_estado;
		}
		//Fin busqueda de Hotel
		
		//Inicio de Reservas Vuelos		
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')->get();
			    break;
			case 2:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('agencia_id',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
					$resultsFligth = [];
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				}else{
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				}	
			break;
			case 10:
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				break;

		}

		//Fin de Reservas Vuelas

		include("../comAerea.php");
		$arrayAerea = [];
		foreach ($comAerea as $key=>$area){
			$nombre = explode("|", $area);
			$arrayAerea[$key]['label']= $nombre[0];
		}

		//Inicio de Servicios Circuitos
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    										->where('tipo_servicio','=',config('constants.circuito'))
			    										->get();
			    break;
			case 2:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.circuito'))
								    					->where('id_agencia',$datosUsuarios->idAgencia)
								    					->get();
			    break;
			case 3:
				$resultadosCircuitos = [];
					$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				break;
			case 4:
				$resultadosCircuitos = [];
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosCircuitos = [];
					$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
				break;
			case 10:
				$resultadosCircuitos = [];
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				break;
		}
		$resultsCircuitos  = array();
		foreach($resultadosCircuitos as $key=>$reservaCircuitos){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaCircuitos->id;
			$lista->localizador = $reservaCircuitos->localizador;
			$lista->proforma = $reservaCircuitos->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaCircuitos->fecha_reserva));
			$lista->pasajero = $reservaCircuitos->pasajero_principal;
			$lista->estado = $reservaCircuitos->id_estado_reserva;
			$lista->id_proveedor = $reservaCircuitos->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaCircuitos->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaCircuitos->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaCircuitos->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaCircuitos->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaCircuitos->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkout));
			$lista->monto = $reservaCircuitos->precio_venta;
			if($reservaCircuitos->id_agencia == 1){
				if(isset($reservaCircuitos->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaCircuitos->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}

			}else{
				$user = Usuario::where('id_usuario', '=', $reservaCircuitos->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsCircuitos[$key] = $lista;
		}	
		//Fin Servicios Circuitos


		//Inicio de Servicios Traslado
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.traslado'))
			    										->get();
			    break;
			case 2:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
					$resultadosTraslado =[];
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;
			case 10:
					$resultadosTraslado =[];
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
			}
		$resultsTraslado  = array();
		foreach($resultadosTraslado as $key=>$reservaTraslado){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaTraslado->id;
			$lista->localizador = $reservaTraslado->localizador;
			$lista->proforma = $reservaTraslado->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaTraslado->fecha_reserva));
			$lista->pasajero = $reservaTraslado->pasajero_principal;
			$lista->estado = $reservaTraslado->id_estado_reserva;
			$lista->id_proveedor = $reservaTraslado->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaTraslado->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaTraslado->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaTraslado->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaTraslado->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaTraslado->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaTraslado->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaTraslado->fecha_checkout));
			$lista->monto = $reservaTraslado->precio_venta;
			if($reservaTraslado->id_agencia == 1){
				if(isset($reservaTraslado->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaTraslado->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsTraslado[$key] = $lista;
		}	
		//Fin Servicios Traslado

		//Inicio de Servicios Actividad
		$resultadosActividades=[];
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio',config('constants.actividad'))
			    										->get();
			    break;
			case 2:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
					$resultadosActividades =[];
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;	
			case 10:
					$resultadosActividades =[];
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
			}

		$resultadosActividad  = array();
		foreach($resultadosActividades as $key=>$reservaActividad){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaActividad->id;
			$lista->localizador = $reservaActividad->localizador;
			$lista->proforma = $reservaActividad->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaActividad->fecha_reserva));
			$lista->pasajero = $reservaActividad->pasajero_principal;
			$lista->id_proveedor = $reservaActividad->id_proveedor;
			if(isset($reservaActividad->agencias->razon_social)){
				$lista->agencia = $reservaActividad->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaActividad->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaActividad->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaActividad->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaActividad->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaActividad->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaActividad->fecha_checkout));
			$lista->monto = $reservaActividad->precio_venta;
			$lista->estado = $reservaActividad->id_estado_reserva;
			if($reservaActividad->id_agencia == 1){
				if(isset($reservaActividad->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaActividad->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}else{
				$user = Usuario::where('id_usuario', '=', $reservaActividad->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultadosActividad[$key] = $lista;
		}	
		//Fin Servicios Traslado
		$client = new Client();
		$cuerpoProforma = [];
		$RestConsultaProfIn= new \StdClass;
		$RestConsultaProfIn->reqToken = $datosUsuarios->tokenBk;
		$RestConsultaProfIn->ts = date('Y-m-d h:m:s');
		$Detalles= new \StdClass;
		$Detalles->FechaDesde = '2017-01-01';
		$Detalles->FechaHasta = date('Y-m-d');
		$Detalles->VendedorId = $datosUsuarios->id_sistema_facturacion;
		$RestConsultaProfIn->Detalles =  $Detalles;
		$iibReq = new \StdClass;
		$iibReq->RestConsultaProfIn = $RestConsultaProfIn;
		try{
			$iibRsp = $client->post(Config::get('config.iibConProForma'), [
				'json' => $iibReq
			]);
			$iibObjRsp = json_decode($iibRsp->getBody());
			if(isset($iibObjRsp->RestConsultaProfOut->Proformas)){
				foreach($iibObjRsp->RestConsultaProfOut->Proformas as $key=>$datosProforma){
					$cuerpoProforma[$key]['value'] = $datosProforma->ProformaId;
					$cuerpoProforma[$key]['label'] = $datosProforma->ProformaId." - ".$datosProforma->ProformasNombrePasajero;
				}
			}
		}
		catch(RequestException $e){
			//entra aca pero si pones Exception nomas no agarra
		}

		if($datosUsuarios->idAgencia == Config::get('constants.agenciaDtp')){
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_usuario =".$datosUsuarios->idUsuario);
		}else{
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_perfil = 10");
		}

		$arrayAgente = [];
		foreach($agentes as $key=>$agente){
			//$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_vendedor_dtp;
			$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_sistema_facturacion;
			$arrayAgente[$key]['label']= $agente->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agente->usuario;
		}	

        if(empty($listado)&&empty($resultsCircuitos)&&empty($resultsTraslado)&&empty($resultadosActividad)&&empty($resultsFligth)){
        	flash('<b>NO POSEE RESERVAS REGISTRADAS</b>')->success();
			return Redirect::back();
        }else{
        	return view('pages.reservas')->with(['reservas'=>$listado,'reservasCiruito'=>$resultsCircuitos,'reservasTraslado'=>$resultsTraslado,'reservasActividad'=>$resultadosActividad,'resultsFligth'=>$resultsFligth, 'listadoComp'=>$arrayAerea, 'selectProforma'=>$cuerpoProforma, 'selectAgentes'=>$arrayAgente, 'listadoState'=>$arrayState, 'isDtp' => $datosUsuarios->idAgencia == 1 ? true : false]);
        }
	}


	public function listadoReservas(){

		//Inicia Reservas de Hoteles
		$listado = array();
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		
		$fecha = date('Y-m-d');
		$nuevafecha = date("Y-m-d",strtotime($fecha."- 30 day"));
		$fechaHoy = date("Y-m-d",strtotime($fecha."+ 1 day"));
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:i', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:i', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					 ->get();
			    break;
			case 2:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->orderBy('fecha_reserva', 'desc')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();

			    break;
			case 3:
					$results =[];
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}else{
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}	
			break;
				case 5:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->orderBy('fecha_reserva', 'desc')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();

			    break;
			case 10:
					$results =[];
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
					break;
		}

		foreach($results as $reserva){
			$lista = new \StdClass;
			$lista->id_reserva = $reserva->id;
			$lista->expediente = $reserva->id_file_reserva;
			$lista->proforma = $reserva->numero_proforma;
			$lista->localizador = $reserva->codigo;
			$lista->destino = $reserva->destino->desc_destino;
			$lista->fecha =  $reserva->fecha_reserva;
			$lista->pasajero = $reserva->pasajero_principal_apellido.", ".$reserva->pasajero_principal_nombre;
			$lista->agencia = $reserva->agencia->razon_social;
			$lista->agencia_id = $reserva->agencia->id_agencia;
			$lista->vendedor_agencia = $reserva->usuario->usuario;
			$lista->cancelacion_monto = $reserva->cancelacion_monto;
			$lista->cancelacion_desde = $reserva->cancelacion_desde;
			$lista->id_proveedor = $reserva->id_proveedor;
			if($reserva->id_agencia == 1){
				$lista->vendedor_dtp = $reserva->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			if($reserva->enviado_proforma =="S"&& $reserva->codret_proforma == "0"){
				$lista->iconoproforma = 0;
			}else{
				//if($reserva->id_estado_reserva != config('constants.resConfirmada')){
					$lista->iconoproforma = 1;
				/*}else{
					$lista->iconoproforma = 1;	
				}*/
			}

			if($reserva->id_estado_reserva == config('constants.resEliminada')){
				if($reserva->cancelado_proforma == "S" && $reserva->codret_proforma_cancelacion == "0"){
					$lista->iconocancelar = 2;
				}else{
					$lista->iconocancelar = 1;
				}
			}else{
				$lista->iconocancelar = 0;
			}

			$lista->expediente_vs = "";
			$lista->factura = "";
			//$lista->monto_neto = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			$lista->monto_sin_comision = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			//$lista->monto = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->monto_cobrado = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->estado = $reserva->id_estado_reserva;
			$lista->iconoproforma=0;
			//$lista->estado = config('constants.resConfirmada');

			//dd($lista);
			$listado[] =  $lista;
		}
		$arrayState = [];
		$states = State::where('activo', '=', true)->get();	
		foreach ($states as $key=>$state){
			$arrayState[$key]['value']= $state->id_estado_reserva;
			$arrayState[$key]['label']= $state->nombre_estado;
		}
		//Fin busqueda de Hotel
		
		//Inicio de Reservas Vuelos		
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		$resultsFligth =[];
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')->get();
			    break;
			case 2:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('agencia_id',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				}else{
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				}	
			break;
			case 5:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('agencia_id',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 10:
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				break;

		}

		//Fin de Reservas Vuelas

		include("../comAerea.php");
		$arrayAerea = [];
		foreach ($comAerea as $key=>$area){
			$nombre = explode("|", $area);
			$arrayAerea[$key]['label']= $nombre[0];
		}
		$resultadosCircuitos = [];
		//Inicio de Servicios Circuitos
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    										->where('tipo_servicio','=',config('constants.circuito'))
			    										->get();
			    break;
			case 2:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.circuito'))
								    					->where('id_agencia',$datosUsuarios->idAgencia)
								    					->get();
			    break;
			case 3:
					$resultadosCircuitos = [];
					$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				break;
			case 4:
				$resultadosCircuitos = [];
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
				break;	
			case 5:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.circuito'))
								    					->where('id_agencia',$datosUsuarios->idAgencia)
								    					->get();
			    break;
			case 10:
				$resultadosCircuitos = [];
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				break;

		}
		$resultsCircuitos  = array();
		foreach($resultadosCircuitos as $key=>$reservaCircuitos){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaCircuitos->id;
			$lista->localizador = $reservaCircuitos->localizador;
			$lista->proforma = $reservaCircuitos->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaCircuitos->fecha_reserva));
			$lista->pasajero = $reservaCircuitos->pasajero_principal;
			$lista->estado = $reservaCircuitos->id_estado_reserva;
			$lista->id_proveedor = $reservaCircuitos->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaCircuitos->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaCircuitos->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaCircuitos->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaCircuitos->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaCircuitos->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkout));
			$lista->monto = $reservaCircuitos->precio_venta;
			if($reservaCircuitos->id_agencia == 1){
				if(isset($reservaCircuitos->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaCircuitos->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}

			}else{
				$user = Usuario::where('id_usuario', '=', $reservaCircuitos->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsCircuitos[$key] = $lista;
		}	
		//Fin Servicios Circuitos

		 $resultadosTraslado = [];
		//Inicio de Servicios Traslado
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.traslado'))
			    										->get();
			    break;
			case 2:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
			   break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;
			case 5:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 10:
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				break;									    
			}
		$resultsTraslado  = array();
		foreach($resultadosTraslado as $key=>$reservaTraslado){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaTraslado->id;
			$lista->localizador = $reservaTraslado->localizador;
			$lista->proforma = $reservaTraslado->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaTraslado->fecha_reserva));
			$lista->pasajero = $reservaTraslado->pasajero_principal;
			$lista->estado = $reservaTraslado->id_estado_reserva;
			$lista->id_proveedor = $reservaTraslado->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaTraslado->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaTraslado->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaTraslado->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaTraslado->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaTraslado->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaTraslado->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaTraslado->fecha_checkout));
			$lista->monto = $reservaTraslado->precio_venta;
			if($reservaTraslado->id_agencia == 1){
				if(isset($reservaTraslado->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaTraslado->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsTraslado[$key] = $lista;
		}	
		//Fin Servicios Traslado

		//Inicio de Servicios Actividad
		$resultadosActividades=[];
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio',config('constants.actividad'))
			    										->get();
			    break;
			case 2:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
			   break; 					
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;	
			case 5:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 10:
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				break;
			}

		$resultadosActividad  = array();
		foreach($resultadosActividades as $key=>$reservaActividad){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaActividad->id;
			$lista->localizador = $reservaActividad->localizador;
			$lista->proforma = $reservaActividad->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaActividad->fecha_reserva));
			$lista->pasajero = $reservaActividad->pasajero_principal;
			$lista->id_proveedor = $reservaActividad->id_proveedor;
			if(isset($reservaActividad->agencias->razon_social)){
				$lista->agencia = $reservaActividad->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaActividad->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaActividad->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaActividad->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaActividad->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaActividad->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaActividad->fecha_checkout));
			$lista->monto = $reservaActividad->precio_venta;
			$lista->estado = $reservaActividad->id_estado_reserva;
			if($reservaActividad->id_agencia == 1){
				if(isset($reservaActividad->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaActividad->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}else{
				$user = Usuario::where('id_usuario', '=', $reservaActividad->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultadosActividad[$key] = $lista;
		}	
		//Fin Servicios Traslado
		$client = new Client();
		$cuerpoProforma = [];
		$RestConsultaProfIn= new \StdClass;
		$RestConsultaProfIn->reqToken = $datosUsuarios->tokenBk;
		$RestConsultaProfIn->ts = date('Y-m-d h:m:s');
		$Detalles= new \StdClass;
		$Detalles->FechaDesde = '2017-01-01';
		$Detalles->FechaHasta = date('Y-m-d');
		$Detalles->VendedorId = $datosUsuarios->id_sistema_facturacion;
		$RestConsultaProfIn->Detalles =  $Detalles;
		$iibReq = new \StdClass;
		$iibReq->RestConsultaProfIn = $RestConsultaProfIn;
		try{
			$iibRsp = $client->post(Config::get('config.iibConProForma'), [
				'json' => $iibReq
			]);
			$iibObjRsp = json_decode($iibRsp->getBody());
			if(isset($iibObjRsp->RestConsultaProfOut->Proformas)){
				foreach($iibObjRsp->RestConsultaProfOut->Proformas as $key=>$datosProforma){
					$cuerpoProforma[$key]['value'] = $datosProforma->ProformaId;
					$cuerpoProforma[$key]['label'] = $datosProforma->ProformaId." - ".$datosProforma->ProformasNombrePasajero;
				}
			}
		}
		catch(RequestException $e){
			//entra aca pero si pones Exception nomas no agarra
		}

		if($datosUsuarios->idAgencia == Config::get('constants.agenciaDtp')){
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_usuario =".$datosUsuarios->idUsuario);
		}else{
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_perfil = 10");
		}

		$arrayAgente = [];
		foreach($agentes as $key=>$agente){
			//$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_vendedor_dtp;
			$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_sistema_facturacion;
			$arrayAgente[$key]['label']= $agente->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agente->usuario;
		}	

        if(empty($listado)&&empty($resultsCircuitos)&&empty($resultsTraslado)&&empty($resultadosActividad)&&empty($resultsFligth)){
        	flash('<b>NO POSEE RESERVAS REGISTRADAS</b>')->success();
			return Redirect::back();
        }else{
        	return view('pages.reservas')->with(['reservas'=>$listado,'reservasCiruito'=>$resultsCircuitos,'reservasTraslado'=>$resultsTraslado,'reservasActividad'=>$resultadosActividad,'resultsFligth'=>$resultsFligth, 'listadoComp'=>$arrayAerea, 'selectProforma'=>$cuerpoProforma, 'selectAgentes'=>$arrayAgente, 'listadoState'=>$arrayState, 'isDtp' => $datosUsuarios->idAgencia == 1 ? true : false]);
        }
	}

   	public function getReservasFlight(Request $req){
   		$results= [];
		//$results = Reserva::with('destino','agencia','usuario')->get();
		$results=ReservasVuelos::with('usuario', 'agencia')->where(function($query) use($req){
			if(!empty($req->input('companhia'))){
				$query->where('marketingcompany','=',$req->input('idFileReserva'));
			}

			if(!empty($req->input('idFileReserva'))){
				$query->where('controlnumber', 'ilike', "%".$req->input('idFileReserva')."%");
			}
			
			if(!empty($req->input('origen'))){
				$query->where('cod_origen','=',$req->input('origen'));
			}

			if(!empty($req->input('destino'))){
				$query->where('cod_destino','=',$req->input('destino'));
			}
			
			$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;

			if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
				$query->where('usuario_id',$datosUsuarios->idUsuario);
				//$query->orWhere('id_agente_dtp', $datosUsuarios->idUsuario);
			}else{
				$query->where('usuario_id',$datosUsuarios->idUsuario);
			}
		})->get();

		return response()->json($results);
   	}	
   	public function getReservasServicios(Request $req){
   				$result=ReservasServicio::with('usuarios', 'agencias')
   												->where('tipo_servicio','=',$req->input('type'))
   												->get();

   				if(!empty($req->input('localizadorBusqueda'))){
					foreach($result as $key=>$resultado){
		   				if($resultado['localizador'] != $req->input('localizadorBusqueda')){
		   					unset($result[$key]);
		   				}
		   			}
		   		}	

				if(!empty($req->input('desdeBusqueda'))||!empty($req->input('hastaBusqueda'))){
					$arrayResultado = [];
					$desdeBusqueda = explode("/",$req->input('desdeBusqueda'));
					$hastaBusqueda = explode("/",$req->input('hastaBusqueda'));
					$fechaDesde = $desdeBusqueda[2]."-".$desdeBusqueda[1]."-".$desdeBusqueda[0].' 00:00:00';
					$fechaHasta = $hastaBusqueda[2]."-".$hastaBusqueda[1]."-".($hastaBusqueda[0]). ' 23:59:59';
					foreach($result as $key=>$resultado){
						if ($resultado['fecha_reserva'] >= $fechaDesde && $resultado['fecha_reserva'] <=$fechaHasta){
							$arrayResultado[] = $result[$key];
						}	
					}
					$result = $arrayResultado;
				}else{
					$result = $result;
				}	

		   		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
				if($datosUsuarios->idPerfil != 1){
					switch ($datosUsuarios->idPerfil) {
						case 2:
							foreach($result as $key=>$resultado){
								if($resultado['id_agencia'] != $datosUsuarios->idAgencia){
									unset($result[$key]);
								}
							}	
							break;
						case 3:
								foreach($result as $key=>$resultado){
									if($resultado['id_usuario'] != $datosUsuarios->idUsuario){
										unset($result[$key]);
									}
								}	
							break;
						case 10:
								foreach($result as $key=>$resultado){
									if($resultado['id_usuario'] != $datosUsuarios->idUsuario && $resultado['id_agente_dtp'] != $datosUsuarios->idUsuario ){
										unset($result[$key]);
									}
								}
							break;
					}		
				}

		   		if(!empty($req->input('pasajeroBusqueda'))){
		   			foreach($result as $key=>$resultado){
		   				$resultados  =strpos(strtolower($resultado->pasajero_principal_nombre."".$resultado->pasajero_principal_apellido), strtolower($req->input('pasajeroBusqueda')));
		   				if($resultados === FALSE){
		   					unset($result[$key]);
		   				}
		   			}
		   		}	

			$results  = array();
			foreach($result as $key=>$reserva){
				$lista = new \StdClass;
				$lista->id_reserva = $reserva->id;
				$lista->localizador = $reserva->localizador;
				$lista->proforma = $reserva->numero_proforma;
				$lista->fecha =  date('d/m/Y', strtotime($reserva->fecha_reserva));
				$lista->pasajero = $reserva->pasajero_principal;
				$lista->agencia = $reserva->agencias->razon_social;
				$lista->vendedor_agencia = $reserva->usuarios->usuario;
				$lista->cancelacion_monto = $reserva->monto_cancelacion;
				$lista->cancelacion_desde = date('d/m/Y', strtotime($reserva->fecha_cancelacion));
				$lista->fecha_checkin = date('d/m/Y', strtotime($reserva->fecha_checkin));
				$lista->fecha_checkout = date('d/m/Y', strtotime($reserva->fecha_checkout));
				$lista->monto = $reserva->precio_venta;
				$lista->estado = $reserva->id_estado_reserva;
				$lista->id_proveedor = $reserva->id_proveedor;

				if($reserva->id_agencia == 1){
					$lista->vendedor_dtp = $reserva->usuarios->nombre_apellido;
				}else{
					$user = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->get();
					if(isset($user)){
						$lista->vendedor_dtp = $user[0]->nombre_apellido;
					}else{
						$lista->vendedor_dtp = "";
					}
				}
				$results[$key] = $lista;
			}
			return $results;
   	}	
	/*Función de busqueda de reservas según parametros*/
   	public function getReservas(Request $req){

   		$results= [];
   		$results=Reserva::with('destino', 'agencia', 'usuario')->orderBy('fecha_reserva', 'DESC')->get();

   		if(!empty($req->input('nrodeexpediente'))){
   			foreach($results as $key=>$resultado){
   				if(intval($resultado['numero_proforma']) != intval ($req->input('nrodeexpediente'))){
   					unset($results[$key]);
   				}
   			}
   		}

   		if(!empty($req->input('localizador'))){
			foreach($results as $key=>$resultado){
   				if($resultado['codigo'] != $req->input('localizador')){
   					unset($results[$key]);
   				}
   			}
   		}	

   		if(!empty($req->input('pasajeroPrincipal'))){
   			foreach($results as $key=>$resultado){
   				$resultados  =strpos(strtolower($resultado->pasajero_principal_nombre."".$resultado->pasajero_principal_apellido), strtolower($req->input('pasajeroPrincipal')));
   				if($resultados === FALSE){
   					unset($results[$key]);
   				}
   			}
   		}	
   		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		if($datosUsuarios->idPerfil != 1){
			switch ($datosUsuarios->idPerfil) {
				case 2:
					foreach($results as $key=>$resultado){
						if($resultado['id_agencia'] != $datosUsuarios->idAgencia){
							unset($results[$key]);
						}
					}	
					break;
				case 3:
						foreach($results as $key=>$resultado){
							if($resultado['id_usuario'] != $datosUsuarios->idUsuario){
								unset($results[$key]);
							}
						}

					break;
					case 10:
						foreach($results as $key=>$resultado){
							if($resultado['id_usuario'] != $datosUsuarios->idUsuario && $resultado['id_agente_dtp'] != $datosUsuarios->idUsuario ){
								unset($results[$key]);
							}
						}
						break;
				} 
		}
		if(!empty($req->input('desde'))||!empty($req->input('hasta'))){
			$arrayResultado = [];
			$desdeBusqueda = explode("/",$req->input('desde'));
			$hastaBusqueda = explode("/",$req->input('hasta'));
			$fechaDesde = $desdeBusqueda[2]."-".$desdeBusqueda[1]."-".$desdeBusqueda[0].' 00:00:00';
			$fechaHasta = $hastaBusqueda[2]."-".$hastaBusqueda[1]."-".($hastaBusqueda[0]). ' 23:59:59';
			foreach($results as $key=>$resultado){
				if ($resultado['fecha_reserva'] >= $fechaDesde && $resultado['fecha_reserva'] <=$fechaHasta){
					$arrayResultado[] = $results[$key];
				}	
			}
			$results = $arrayResultado;
		}else{
			$results = $results;
		}	

		if(!empty($req->input('estadoreserva'))){
			foreach($results as $key=>$resultado){
				if($resultado['id_estado_reserva'] != $req->input('estadoreserva')){
					unset($results[$key]);				
				}
			}
		}
		$listado = array();
		foreach($results as $key=>$reservas){
			$lista = new \StdClass;
			$lista->id_reserva = $reservas->id;
			$lista->expediente = "<b>".$reservas->id_file_reserva."</b>";
			$lista->localizador = "<b>".$reservas->codigo."</b>";
			$lista->proforma = "<b>".$reservas->numero_proforma."</b>";
			$lista->destino = $reservas->destino->desc_destino;
			$lista->fecha =  date('d-m-Y H:i:s', strtotime($reservas->fecha_reserva));
			$lista->pasajero = "<b>".$reservas->pasajero_principal_apellido.", ".$reservas->pasajero_principal_nombre."</b>";
			$lista->agencia = $reservas->agencia->razon_social;
			$lista->vendedor_agencia = "<b>".$reservas->usuario->usuario."</b>";
			$lista->cancelacion_monto = $reservas->cancelacion_monto;
			$lista->cancelacion_desde = $reservas->cancelacion_desde;
			$lista->id_proveedor = $reservas->id_proveedor;
		
			if($reservas->id_agencia == 1){
				$lista->vendedor_dtp = $reservas->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reservas->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}

			if($reservas->id_agencia == 1){
				$lista->vendedor_dtp = $reservas->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reservas->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			/*if($reservas->enviado_proforma =="S"&& $reservas->codret_proforma == "0"){
				$lista->iconoproforma = 0;
			}else{
				$lista->iconoproforma = 1;
			}*/

			/*if($reservas->id_estado_reserva == config('constants.resEliminada')){
				if($reservas->cancelado_proforma == "S" && $reservas->codret_proforma_cancelacion == "0"){
					$lista->iconocancelar = 2;
				}else{
					$lista->iconocancelar = 1;
				}
			}else{
				$lista->iconocancelar = 0;
			}*/

			$lista->expediente_vs = $reservas->expediente_vstour;
			$lista->factura = $reservas->nro_factura;
			//$lista->monto_neto = number_format($reservas->monto_sin_comision, 2, '.', ' ');
			$lista->monto_sin_comision = number_format($reservas->monto_sin_comision, 2, '.', ' '); 
			//$lista->monto = number_format($reservas->monto_cobrado, 2, '.', ' ');
			$lista->monto_cobrado = number_format($reservas->monto_cobrado, 2, '.', ' ');
			$lista->estado = '<img alt="" style="width: 20px; margin-left: 25%;" src="images/icon/'.$reservas->id_estado_reserva.'.png">';
			$lista->estados = $reservas->id_estado_reserva;
			//solamente si no esta cancelada mostrar.
			
			//$lista->iconoproforma=0;
			//$lista->estado = config('constants.resConfirmada');
			$listado['data'][] =  $lista;
		}	

		return response()->json($listado);
	}


	/*Función de busqueda de reservas según parametros*/
   	public function getReservasNuevo(Request $req){
   		$results= [];
   		$results=Reserva::with('destino', 'agencia', 'usuario')
   								->orderBy('fecha_reserva', 'DESC')
   								->get();
   		if(!empty($req->input('idFileReserva'))){
   			foreach($results as $key=>$resultado){
   				if(intval($resultado['numero_proforma']) != intval ($req->input('idFileReserva'))){
   					unset($results[$key]);
   				}
   			}
   		}

   		if(!empty($req->input('localizador'))){
			foreach($results as $key=>$resultado){
   				if($resultado['codigo'] != $req->input('localizador')){
   					unset($results[$key]);
   				}
   			}
   		}	

   		if(!empty($req->input('pasajeroPrincipal'))){
   			foreach($results as $key=>$resultado){
   				$resultados  =strpos(strtolower($resultado->pasajero_principal_nombre."".$resultado->pasajero_principal_apellido), strtolower($req->input('pasajeroPrincipal')));
   				if($resultados === FALSE){
   					unset($results[$key]);
   				}
   			}
   		}	

   		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		if($datosUsuarios->idPerfil != 1){
			switch ($datosUsuarios->idPerfil) {
				case 2:
					foreach($results as $key=>$resultado){
						if($resultado['id_agencia'] != $datosUsuarios->idAgencia){
							unset($results[$key]);
						}
					}	
					break;
				case 3:
						foreach($results as $key=>$resultado){
							if($resultado['id_usuario'] != $datosUsuarios->idUsuario){
								unset($results[$key]);
							}
						}	
					break;
				case 5:
					foreach($results as $key=>$resultado){
						if($resultado['id_agencia'] != $datosUsuarios->idAgencia){
							unset($results[$key]);
						}
					}	
					break;
				case 10:
						foreach($results as $key=>$resultado){
							if($resultado['id_usuario'] != $datosUsuarios->idUsuario && $resultado['id_agente_dtp'] != $datosUsuarios->idUsuario ){
								unset($results[$key]);
							}
						}
					break;
				} 
		}

		if(!empty($req->input('desde'))||!empty($req->input('hasta'))){
			$arrayResultado = [];
			$desdeBusqueda = explode("/",$req->input('desde'));
			$hastaBusqueda = explode("/",$req->input('hasta'));
			$fechaDesde = $desdeBusqueda[2]."-".$desdeBusqueda[1]."-".$desdeBusqueda[0].' 00:00:00';
			$fechaHasta = $hastaBusqueda[2]."-".$hastaBusqueda[1]."-".($hastaBusqueda[0]). ' 23:59:59';
			foreach($results as $key=>$resultado){
				if ($resultado['fecha_reserva'] >= $fechaDesde && $resultado['fecha_reserva'] <=$fechaHasta){
					$arrayResultado[] = $results[$key];
				}	
			}
			$results = $arrayResultado;
		}else{
			$results = $results;
		}	

		if(!empty($req->input('estadoreserva'))){
			foreach($results as $key=>$resultado){
				if($resultado['id_estado_reserva'] != $req->input('estadoreserva')){
					unset($results[$key]);				
				}
			}
		}

		$listado = array();
		foreach($results as $key=>$reservas){
			$lista = new \StdClass;
			$lista->id_reserva = $reservas->id;
			$lista->expediente = "<b>".$reservas->id_file_reserva."</b>";
			$lista->localizador = "<b>".$reservas->codigo."</b>";
			$lista->proforma = "<b>".$reservas->numero_proforma."</b>";
			$lista->destino = $reservas->destino->desc_destino;
			$lista->fecha =  date('d-m-Y H:i:s', strtotime($reservas->fecha_reserva));
			$lista->pasajero = "<b>".$reservas->pasajero_principal_apellido.", ".$reservas->pasajero_principal_nombre."</b>";
			$lista->agencia = $reservas->agencia->razon_social;
			$lista->vendedor_agencia = "<b>".$reservas->usuario->usuario."</b>";
			$lista->cancelacion_monto = $reservas->cancelacion_monto;
			$lista->cancelacion_desde = $reservas->cancelacion_desde;
			$lista->id_proveedor = $reservas->id_proveedor;
		
			if($reservas->id_agencia == 1){
				$lista->vendedor_dtp = $reservas->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reservas->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}

			if($reservas->id_agencia == 1){
				$lista->vendedor_dtp = $reservas->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reservas->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			if($reservas->enviado_proforma =="S"&& $reservas->codret_proforma == "0"){
				$lista->iconoproforma = 0;
			}else{
				$lista->iconoproforma = 1;
			}

			if($reservas->id_estado_reserva == config('constants.resEliminada')){
				if($reservas->cancelado_proforma == "S" && $reservas->codret_proforma_cancelacion == "0"){
					$lista->iconocancelar = 2;
				}else{
					$lista->iconocancelar = 1;
				}
			}else{
				$lista->iconocancelar = 0;
			}

			$lista->expediente_vs = $reservas->expediente_vstour;
			$lista->factura = $reservas->nro_factura;
			//$lista->monto_neto = number_format($reservas->monto_sin_comision, 2, '.', ' ');
			$lista->monto_sin_comision = number_format($reservas->monto_sin_comision, 2, '.', ' '); 
			//$lista->monto = number_format($reservas->monto_cobrado, 2, '.', ' ');
			$lista->monto_cobrado = number_format($reservas->monto_cobrado, 2, '.', ' ');
			$lista->estado = '<img alt="" style="width: 20px; margin-left: 25%;" src="images/icon/'.$reservas->id_estado_reserva.'.png">';
			$lista->estados = $reservas->id_estado_reserva;
			//solamente si no esta cancelada mostrar.
			
			//$lista->iconoproforma=0;
			//$lista->estado = config('constants.resConfirmada');
			$listado['data'][$key] =  $lista;
		}	
		return response()->json($listado);
   	}




  	/*Metodo de cancelación de reservas*/
   	public function getCancelarReservas(Request $request){
		$client = new Client([
			'headers' => [ 
						   'Content-Type' => 'application/json',
						   'Cache-Control'=>'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
						   'id_proveedor'=>0,
						   'id_usuario'=>Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
						   'fechaRequest'=> date('Y-m-d H:i:s')
						   ]
		]);
   		/*$iibReq = new \StdClass;
		$iibReq->idReserva = $request->input('idReserva');
		$iibReq->token = Session::get('datos-loggeo')->token;*/
		$iibReq = new \StdClass;
		$iibReq->idioma = "CAS";
   		//$reservaId= $request->dataReserva;
		$reservaId=$request->input('idReserva');
		$results = Reserva::with('usuario')
                    ->where('id', '=', $reservaId)
                    ->get(); 
        $datosCancelacionReserva = new \StdClass;
        $datos = new \StdClass;
        foreach($results as $key=>$reservas){   
        	//$iibReq->token = $reservas->id_request;  
			$iibReq->token = Session::get('datos-loggeo')->token;
			$iibReq->codReserva = $reservaId;
        	$datosCancelacionReserva->codReserva = $reservaId;
	        $datosCancelacionReserva->codProveedor = $reservas->id_proveedor;
	        $datosCancelacionReserva->codReserva = $reservas->codigo_cancelacion;
	        $datosCancelacionReserva->codHotel = $reservas->codigo_hotel;
	        $datosCancelacionReserva->proformaId = $reservas->numero_proforma;
	        $datosCancelacionReserva->nroItem = $reservas->numero_de_item;
	        if($reservas->usuario->codigo_agente!="" || $reservas->usuario->codigo_agente!=null){
		        if($reservas->id_agencia == 1){
						$datosCancelacionReserva->email = $reservas->usuario->email;
					}else{
						$user = Usuario::where('id_usuario', '=', $reservas->usuario->codigo_agente)->get();
						$datosCancelacionReserva->email  = $user[0]->email;
					}
			}else{
				$datosCancelacionReserva->email  = "";
			}		
	        $datosCancelacionReserva->fechaDesde = date('m/d/Y', strtotime($reservas->fecha_checkin));
	        $datosCancelacionReserva->fechaHasta = date('m/d/Y', strtotime($reservas->fecha_checkout));
	    }    

		$datos->datosCancelacionReserva = $datosCancelacionReserva;
		$iibReq->datos = $datos;
		$datosLogueo = Session::get('datos-loggeo');
		$datosUsuarios = new \StdClass;
		$datosUsuarios->idUsuario = $datosLogueo->datos->datosUsuarios->idUsuario;
		$iibReq->datosUsuario = $datosUsuarios;
		// Envio de datos para la cancelación
		/*echo '<pre>';
		print_r(json_encode($iibReq));*/
		try{
			$iibRsp = $client->post(Config::get('config.iibCancelations'),
				[
					'body' => json_encode($iibReq),
					]
			);
		}
		catch(RequestException $e){
			return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			return view('pages.errorConexion');	
		}	
		$iibObjRsp = json_decode($iibRsp->getBody());
		/*echo "<br/><br/>RESPONSE RESERVA+++++<br>";print_r(json_encode($iibObjRsp));
		die;*/
 		return response()->json($iibObjRsp);
   	}
   	public function detallesReservaFligth(Request $req, $id){
   		$results = ReservasVuelos::with('usuario', 'agencia', 'itinerarios','pasajeros') 
                    ->where('id', '=', $id)
                    ->get(); 

        return view('pages.vuelos.detallesReservaFlight')->with(['resultado'=>$results]);
   	}	
   	public function detallesReserva(Request $req, $id){
  		$reserva = Reserva::with('destino', 'agencia', 'usuario', 'monedaorigen', 'usuariocancelacion', 'habitacionesReserva') 
                    ->where('id', '=', $id)
                    ->first(); 
        if($reserva->id_agencia == 1){
			$nombre_agente_dtp = $reserva->usuario->nombre_apellido;
		}else{
			//dd($reserva->id_agente_dtp);
			$agente_dtp = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->first();
			if(isset($agente_dtp)){
				$nombre_agente_dtp = $agente_dtp->nombre_apellido;
			}else{
				$nombre_agente_dtp = "";
			}
		}
		$habitaciones = array();
		foreach($reserva->habitacionesReserva as $habitacion){
			$newHab = new \StdClass;
			$newHab->codHabitacion = $habitacion->codigo_habitacion;
			$newHab->desHabitacion = $habitacion->descripcion_habitacion;
			
			$tarifas = array();
			$newTarifa = new \StdClass;
			$newTarifa->codConsumicion = $habitacion->codigo_regimen;
			$newTarifa->desConsumicion = $habitacion->descripcion_regimen;
			$newTarifa->comentarioPasajero = $habitacion->comentario;
			$tarifas[] = $newTarifa;
			
			
			$ocupanciaDtp = new \StdClass;
			$na = 0;
			$edadesNinhos = array();
			$descPasajeros = array();
			foreach($habitacion->ocupantesHabitacion as $ocupante){
				$descPasajero = new \StdClass;
				$descPasajero->tipo = $ocupante->tipo;
				$descPasajero->nombre = $ocupante->nombre;
				$descPasajero->apellido = $ocupante->nombre;
				$descPasajero->edad = $ocupante->edad;
				if($ocupante->tipo == "AD"){
					$na++;
				}else{
					$edadesNinhos[] = $ocupante->edad;
				}
				$descPasajeros[] = $descPasajero;
			}
			$ocupanciaDtp->adultos = $na;
			$ocupanciaDtp->edadesNinhos = $edadesNinhos;
			$ocupanciaDtp->descPasajeros = $descPasajeros;
			
			$newTarifa->ocupanciaDtp = $ocupanciaDtp;
			
			$newHab->tarifas = $tarifas;
			
			$habitaciones[]= $newHab;
			
		}
		//dd($habitaciones);
		$confRsp = new \StdClass;
		$confRsp->codReserva = $reserva->codigo;
		$confRsp->fechaReserva = $reserva->fecha_reserva;
		
		$titular = new \StdClass;
		$titular->nombre = $reserva->pasajero_principal_nombre;
		$titular->apellido = $reserva->pasajero_principal_apellido;
		$confRsp->titular = $titular;
		
		$confRsp->nomHotel = $reserva->nombre_hotel;
		$confRsp->dirHotel = $reserva->direccion_hotel;
		
		$confRsp->habitaciones = $habitaciones;
		$confRsp->fechaDesde = $reserva->fecha_checkin;
		$confRsp->fechaHasta = $reserva->fecha_checkout;
		
		$confRsp->infoAdicional = "";
		
		$politicaDtp = new \StdClass;
		$politicaDtp->desde = $reserva->cancelacion_desde;
		$politicaDtp->monto = $reserva->cancelacion_monto;
		$confRsp->politicaDtp = $politicaDtp;
		//$confRsp->rspFactour = ($reserva->responseProforma != null && $reserva->responseProforma != "") ? true:false;
		$confRsp->rspFactour = true;
		$confRsp->factourComAg = $reserva->factour_com_ag;
		$confRsp->factourNeto = $reserva->factour_neto;
		$confRsp->factourBruto = $reserva->factour_bruto;
		$confRsp->nroFactura = $reserva->nro_factura;
		$confRsp->estado = $reserva->id_estado_reserva;
		
		return view('pages.hoteles.booking')->with(['confRsp'=> $confRsp,
			'fechaDesde' => $confRsp->fechaDesde,
			'fechaHasta' => $confRsp->fechaHasta]);
   	}
   	public function logActivity(){
   		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		$notificacion =[];
		switch ($datosUsuarios->idPerfil) {
				case 1:
				    $notificacion = Notificacion::with('agencia', 'usuario')->get();
				    break;
				case 2:
				    $notificacion = Notificacion::with('agencia', 'usuario')
				    					->where('id_agencia',$datosUsuarios->idAgencia)
				    					->get();
				    break;
				case 3:
					$notificacion = Notificacion::with('agencia', 'usuario')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->get();
				    break;					
				case 4:
					$notificacion = Notificacion::with('agencia', 'usuario')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->get();
				case 5:
				    $notificacion = Notificacion::with('agencia', 'usuario')
				    					->where('id_agencia',$datosUsuarios->idAgencia)
				    					->get();
				    break;
			   case 10:
				    $notificacion = Notificacion::with('agencia', 'usuario')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->get();
				    break;
			}
   		return view('pages.hoteles.logActivity')->with(['notificaciones'=>$notificacion]);
   	}	
   	public function getLog(Request $req){
		$logs = DB::connection('pgsqlLog')
					->table("logs")
					->where(function($query) use($req){
						if(!empty($req->input('usuario_id'))){
							$query->where('user_id','=',$req->input('usuario_id'));
						}
						if(!empty($req->input('agencia_id')) && !empty($req->input('sucursal_id'))){
							$users = Usuario::where('id_agencia', '=', $req->input('agencia_id'))
											->orWhere('id_sucursal_agencia', '=',$req->input('sucursal_id'))
											->get(['id_usuario']);
							foreach($users as $key=>$user){
								$query->where('user_id',$user->id_usuario);
							}
						} 
						if(!empty($req->input('desde'))||!empty($req->input('desde'))){
							$fecha = date('Y-m-d', strtotime($req->input('desde')));
							$query->whereBetween('date_time_request', array(
																			$fecha.' 00:00:00', $fecha.'23:59:59'
																		)
												);
										}         
					})	
					->get();
		DB::disconnect('pgsqlLog');
		$listado = array();
		foreach($logs as $index=>$log){
			$listado[$index]['id'] = $log->id;
			$listado[$index]['url'] = $log->url;
			$listado[$index]['metodo'] = $log->metodo;
			$listado[$index]['id_request'] = $log->id_request;
			$listado[$index]['date_time_request'] = $log->date_time_request;
			$listado[$index]['date_time_response'] = $log->date_time_response;
			$user = Usuario::where('id_usuario', '=', $log->id)->get(['id_usuario', 'nombre_apellido']);
			if(isset($user[0]->id_usuario)){
				$listado[$index]['usuario'] = $user[0]->id_usuario." - ".$user[0]->nombre_apellido;
			}else{
				$listado[$index]['usuario'] = "";
			}
		}
		return response()->json($listado);
   	}	
   	public function getDetalleNotificacion(Request $req){
   		$notificacion = Notificacion::with('agencia', 'usuario')
   									->where("id", $req->id)
   									->get();
   		return json_encode($notificacion[0]);	
   	}	

   	public function getDetalleLog(Request $req){
   		if(isset($req->dataLog)){
   			$dataLog = $req->dataLog;
	   		$logs = DB::connection('pgsqlLog')
	   								->table("logs")
	   								->where("id", $dataLog)
	   								->get();
	   		DB::disconnect('pgsqlLog');
	   		$response = (string)$logs[0]->url;

   		}

   		if(isset($req->dataRequest)){
   			$dataRequest = $req->dataRequest;
	   		$logs = DB::connection('pgsqlLog')
	   								->table("logs")
	   								->where("id", $dataRequest)
	   								->get();
	   		DB::disconnect('pgsqlLog');
	   		$response = $logs[0]->json_request;
   		}

   		if(isset($req->dataResponse)){
   			$dataResponse = $req->dataResponse;
	   		$logs = DB::connection('pgsqlLog')
	   								->table("logs")
	   								->where("id", $dataResponse)
	   								->get();
	   		DB::disconnect('pgsqlLog');
	   		$response = $logs[0]->json_response;
   		}
	   	return $response;						
   	}

	public function reporteCancelacion(){
		$listado = array();
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		$fecha = date('Y-m-d');
		$nuevafecha = strtotime ('+7 day' , strtotime($fecha));
		$nuevafecha = date ('Y-m-d' , $nuevafecha);
		$results =  Reserva::with('destino', 'agencia', 'usuario')
		 							->where('id_estado_reserva', '=' ,'2')
		 							->whereBetween('cancelacion_desde', array(
													date('Y-m-d h:m:s', strtotime(date('Y-m-d'). '00:00:00')),
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 23:59:59'))
													))
		 							->where('id_usuario',$datosUsuarios->idUsuario)
									->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('id_agente_dtp',$datosUsuarios->idUsuario);
									})											                  
			                        ->get();

		foreach($results as $reserva){
			if($reserva->id_estado_reserva == config('constants.resConfirmada')){
				$lista = new \StdClass;
				$lista->id_reserva = $reserva->id;
				$lista->expediente = $reserva->id_file_reserva;
				$lista->proforma = $reserva->numero_proforma;
				$lista->localizador = $reserva->codigo;
				$lista->destino = $reserva->destino->desc_destino;
				$lista->fecha =  date('d/m/Y', strtotime($reserva->fecha_reserva));
				$lista->pasajero = $reserva->pasajero_principal_apellido.", ".$reserva->pasajero_principal_nombre;
				$lista->agencia = $reserva->agencia->razon_social;
				$lista->agencia_id = $reserva->agencia->id_agencia;
				$lista->vendedor_agencia = $reserva->usuario->usuario;
				$lista->cancelacion_monto = $reserva->cancelacion_monto;
				$lista->cancelacion_desde = $reserva->cancelacion_desde;
				$lista->id_proveedor = $reserva->id_proveedor;
				if($reserva->id_agencia == 1){
					$lista->vendedor_dtp = $reserva->usuario->nombre_apellido;
				}else{
					$user = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->get();
					if(isset($user)){
						$lista->vendedor_dtp = $user[0]->nombre_apellido;
					}else{
						$lista->vendedor_dtp = "";
					}
				}
				if($reserva->enviado_proforma =="S"&& $reserva->codret_proforma == "0"){
					$lista->iconoproforma = 0;
				}else{
					$lista->iconoproforma = 1;
				}

				if($reserva->id_estado_reserva == config('constants.resEliminada')){
					if($reserva->cancelado_proforma == "S" && $reserva->codret_proforma_cancelacion == "0"){
						$lista->iconocancelar = 2;
					}else{
						$lista->iconocancelar = 1;
					}
				}else{
					$lista->iconocancelar = 0;
				}

				$lista->expediente_vs = "";
				$lista->factura = "";
				//$lista->monto_neto = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
				$lista->monto_sin_comision = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
				//$lista->monto = number_format($reserva->monto_cobrado, 2, '.', ' ');
				$lista->monto_cobrado = number_format($reserva->monto_cobrado, 2, '.', ' ');
				//$lista->estado = $reserva->id_estado_reserva;
				$lista->iconoproforma=0;
				$lista->estado = $reserva->id_estado_reserva;
				//dd($lista);
				$listado[] =  $lista;
			}
		}
		
		$arrayState = [];
		
		$states = State::where('activo', '=', true)->get();	
		foreach ($states as $key=>$state){

			$arrayState[$key]['value']= $state->id_estado_reserva;
			$arrayState[$key]['label']= $state->nombre_estado;
		}

		$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')->get();
		include("../comAerea.php");

		$arrayAerea = [];
		foreach ($comAerea as $key=>$area){
			$nombre = explode("|", $area);
			$arrayAerea[$key]['label']= $nombre[0];
		}

        if(!empty($listado)){
        	return view('pages.reporteCancelacion')->with(['reservas'=>$listado,'resultsFligth'=>$resultsFligth, 'listadoComp'=>$arrayAerea, 'isDtp' => $datosUsuarios->idAgencia == 1 ? true : false]);
        }else{
        	flash('<b>NO POSEE PRÓXIMAS A GASTOS</b>')->success();
			return Redirect::back();
        }
	}

	/*Función de busqueda de reservas según parametros*/
   	public function getReservasCancelacion(Request $req){
   		$results= [];
		$results=Reserva::with('destino', 'agencia', 'usuario')->where(function($query) use($req){
			$fecha = date('Y-m-d');
			$nuevafecha = strtotime ( '+7 day' , strtotime ( $fecha ) ) ;
			$nuevafecha = date ('Y-m-d' , $nuevafecha);
			$query->where('id_estado_reserva', '=' ,2);	
			$query->whereBetween('cancelacion_desde', array(
													date('Y-m-d h:m:s', strtotime(date('Y-m-d'). '00:00:00')),
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 23:59:59'))
													));
			if(!empty($req->input('idFileReserva'))){
				$query->where('numero_proforma','=',$req->input('idFileReserva'));
			}

			if(!empty($req->input('nroreserva'))){
				$query->where('id','=',$req->input('nroreserva'));
			}

			if(!empty($req->input('localizador'))){
				$query->where('codigo', 'ilike', "%".$req->input('localizador')."%");
			}
			
			$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;

			switch ($datosUsuarios->idPerfil) {
				case 2:
					$query->where('id_agencia','=',$datosUsuarios->idAgencia);
					break;
				case 3:
					$query->where('id_usuario',$datosUsuarios->idUsuario);
					break;
				case 10:
						$query->where('id_usuario',$datosUsuarios->idUsuario);
						$query->orWhere('id_agente_dtp', $datosUsuarios->idUsuario);

					break;
			}
			
			if(!empty($req->input('pasajeroPrincipal'))){
				$query->where(DB::raw('CONCAT(pasajero_principal_nombre, pasajero_principal_apellido)'), 'ilike', "%".$req->input('pasajeroPrincipal')."%");
			}

			if(!empty($req->input('desde'))||!empty($req->input('desde'))){
				$query->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($req->input('desde'). ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($req->input('hasta'). ' 23:59:59'))
													));
			}
		})->get();						

		$listado = array();
		foreach($results as $key=>$reservas){
			$lista = new \StdClass;
			$lista->id_reserva = $reservas->id;
			$lista->expediente = "<b>".$reservas->id_file_reserva."</b>";
			$lista->localizador = $reservas->codigo;
			$lista->proforma = $reservas->numero_proforma;
			$lista->destino = $reservas->destino->desc_destino;
			//$lista->destino = 667;
			$lista->fecha =  date('d/m/Y', strtotime($reservas->fecha_reserva));
			$lista->pasajero = "<b>".$reservas->pasajero_principal_apellido.", ".$reservas->pasajero_principal_nombre."</b>";
			$lista->agencia = $reservas->agencia->razon_social;
			$lista->vendedor_agencia = $reservas->usuario->usuario;
			$lista->cancelacion_monto = $reservas->cancelacion_monto;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservas->cancelacion_desde));
			$lista->id_proveedor = $reservas->id_proveedor;
		
			if($reservas->id_agencia == 1){
				$lista->vendedor_dtp = $reservas->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reservas->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}

			if($reservas->id_agencia == 1){
				$lista->vendedor_dtp = $reservas->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reservas->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			if($reservas->enviado_proforma =="S"&& $reservas->codret_proforma == "0"){
				$lista->iconoproforma = 0;
			}else{
				$lista->iconoproforma = 1;
			}

			if($reservas->id_estado_reserva == config('constants.resEliminada')){
				if($reservas->cancelado_proforma == "S" && $reservas->codret_proforma_cancelacion == "0"){
					$lista->iconocancelar = 2;
				}else{
					$lista->iconocancelar = 1;
				}
			}else{
				$lista->iconocancelar = 0;
			}

			$lista->expediente_vs = $reservas->expediente_vstour;
			$lista->factura = $reservas->nro_factura;
			//$lista->monto_neto = number_format($reservas->monto_sin_comision, 2, '.', ' ');
			$lista->monto_sin_comision = number_format($reservas->monto_sin_comision, 2, '.', ' '); 
			//$lista->monto = number_format($reservas->monto_cobrado, 2, '.', ' ');
			$lista->monto_cobrado = number_format($reservas->monto_cobrado, 2, '.', ' ');
			$lista->estado = $reservas->id_estado_reserva;
			//solamente si no esta cancelada mostrar.
			$listado[$key] =  $lista;
		}	
		return response()->json($listado);
   	}

   		/*Función de busqueda de reservas según parametros*/
   	public function guardarReservaRegistro(Request $req){
   		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json',
						   ]
		]);
   		$agentesDTP = explode('_', $req->input('agente'));
   		$resultadoServicios =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('id',$req->input('id_reserva'))
			    					->get();
		$iibReq =  new \StdClass; 
		$RestVentasIn =  new \StdClass;   					
   		$ventasBase = new \StdClass; 
   		$ventasBase->VendedoresAgId = $agentesDTP[1];
   		$ventasBase->VendedoresId = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
   		$ventasBase->ProformasNombrePasajero = $resultadoServicios[0]->pasajero_principal;
   		$ventasBase->ProformasCantidadPasajeros = $resultadoServicios[0]->total_pasajeros;
   		$ventasBase->ProformasCheckIn = $resultadoServicios[0]->fecha_checkin;
   		$ventasBase->MonedasId = 1;
   		$ventasBase->ProformasCheckOut = $resultadoServicios[0]->fecha_checkout;
   		$ventasBase->ProductosId = config('constants.cod'.$resultadoServicios[0]->tipo_servicio);
   		$ventasBase->ProductosItem = 1;
   		$ventasBase->ProductosCantidad = 1;
   		$ventasBase->preciocosto = $resultadoServicios[0]->precio_proveedor;
   		$ventasBase->ProveedoresId = $resultadoServicios[0]->id_proveedor;
   		$ventasBase->precioventa = $resultadoServicios[0]->precio_venta;
   		$ventasBase->CodConfirmacion = $resultadoServicios[0]->localizador;
   		$ventasBase->Cotizacion = 0;
   		$ventasBase->DestinationDtp_Id = config('constants.codBookingMotor');
   		$ventasBase->HotelNombre = $resultadoServicios[0]->nombre_proveedor;
   		$ventasBase->HotelTelefono = "";
   		$ventasBase->HotelDireccion = "";
   		$ventasBase->HotelCodigo = 0;
   		$ventasBase->IdAgencia = $resultadoServicios[0]->id_agencia;
   		$ventasBase->NroProforma = $req->input('expediente');
   		$habitaciones = [];
   		$ventasBase->Habitaciones = $habitaciones;
   		$RestVentasIn->ts = date('Y-m-d h:m:s');
   		$RestVentasIn->Ventas = $ventasBase;
   		$iibReq->RestVentasIn = $RestVentasIn;
   		$iibRsp = $client->post(Config::get('config.iibAddProForma'), [
				'json' => $iibReq
			]);
			
		$iibObjRsp = json_decode($iibRsp->getBody());
		DB::table('reservas_servicios')
					        ->where('id',$req->input('id_reserva'))
					        ->update([
					            	'numero_proforma'=>$iibObjRsp->RestVentasOut->ProformaId,
					            	'enviado_proforma' => "S",
					            	'codret_proforma'=>$iibObjRsp->RestVentasOut->codRetorno,
					            	'request_proforma'=>json_encode($iibReq),
					            	'response_proforma' => json_encode($iibObjRsp),
					            	'fechahora_proforma'=>date('Y-m-d h:m:s'),
					            	'numero_de_item'=> $iibObjRsp->RestVentasOut->item,
					            	'id_agente_dtp' => $agentesDTP[1]
					            	]);
		return response()->json($iibObjRsp);   		
   	}
/********************************************************************************************
				MC
********************************************************************************************/
	/*public function reservasMc(){

		//Inicia Reservas de Hoteles
		$listado = array();
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		
		$fecha = date('Y-m-d');
		$nuevafecha = date("Y-m-d",strtotime($fecha."- 30 day"));
		$fechaHoy = date("Y-m-d",strtotime($fecha."+ 1 day"));
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:i', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:i', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					 ->get();
			    break;
			case 2:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->orderBy('fecha_reserva', 'desc')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();

			    break;
			case 3:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}else{
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}else{
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}	
			break;
			case 5:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->orderBy('fecha_reserva', 'desc')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();

			    break;
		}

		foreach($results as $reserva){
			$lista = new \StdClass;
			$lista->id_reserva = $reserva->id;
			$lista->expediente = $reserva->id_file_reserva;
			$lista->proforma = $reserva->numero_proforma;
			$lista->localizador = $reserva->codigo;
			$lista->destino = $reserva->destino->desc_destino;
			$lista->fecha =  $reserva->fecha_reserva;
			$lista->pasajero = $reserva->pasajero_principal_apellido.", ".$reserva->pasajero_principal_nombre;
			$lista->agencia = $reserva->agencia->razon_social;
			$lista->agencia_id = $reserva->agencia->id_agencia;
			$lista->vendedor_agencia = $reserva->usuario->usuario;
			$lista->cancelacion_monto = $reserva->cancelacion_monto;
			$lista->cancelacion_desde = $reserva->cancelacion_desde;
			$lista->id_proveedor = $reserva->id_proveedor;
			if($reserva->id_agencia == 1){
				$lista->vendedor_dtp = $reserva->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			if($reserva->enviado_proforma =="S"&& $reserva->codret_proforma == "0"){
				$lista->iconoproforma = 0;
			}else{
				//if($reserva->id_estado_reserva != config('constants.resConfirmada')){
					$lista->iconoproforma = 1;
				/*}else{
					$lista->iconoproforma = 1;	
				}
			}

			if($reserva->id_estado_reserva == config('constants.resEliminada')){
				if($reserva->cancelado_proforma == "S" && $reserva->codret_proforma_cancelacion == "0"){
					$lista->iconocancelar = 2;
				}else{
					$lista->iconocancelar = 1;
				}
			}else{
				$lista->iconocancelar = 0;
			}

			$lista->expediente_vs = "";
			$lista->factura = "";
			//$lista->monto_neto = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			$lista->monto_sin_comision = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			//$lista->monto = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->monto_cobrado = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->estado = $reserva->id_estado_reserva;
			$lista->iconoproforma=0;
			//$lista->estado = config('constants.resConfirmada');

			//dd($lista);
			$listado[] =  $lista;
		}
		$arrayState = [];
		$states = State::where('activo', '=', true)->get();	
		foreach ($states as $key=>$state){
			$arrayState[$key]['value']= $state->id_estado_reserva;
			$arrayState[$key]['label']= $state->nombre_estado;
		}
		//Fin busqueda de Hotel
		
		//Inicio de Reservas Vuelos		
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')->get();
			    break;
			case 2:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('agencia_id',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				}else{
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				}else{
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				}	
			break;
			case 5:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('agencia_id',$datosUsuarios->idAgencia)
			    					->get();
			    break;

		}

		//Fin de Reservas Vuelas

		include("../comAerea.php");
		$arrayAerea = [];
		foreach ($comAerea as $key=>$area){
			$nombre = explode("|", $area);
			$arrayAerea[$key]['label']= $nombre[0];
		}

		//Inicio de Servicios Circuitos
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    										->where('tipo_servicio','=',config('constants.circuito'))
			    										->get();
			    break;
			case 2:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.circuito'))
								    					->where('id_agencia',$datosUsuarios->idAgencia)
								    					->get();
			    break;
			case 3:
				$resultadosCircuitos = [];
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
				break;
			case 4:
				$resultadosCircuitos = [];
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
				break;	
			case 5:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.circuito'))
								    					->where('id_agencia',$datosUsuarios->idAgencia)
								    					->get();
			    break;
		}
		$resultsCircuitos  = array();
		foreach($resultadosCircuitos as $key=>$reservaCircuitos){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaCircuitos->id;
			$lista->localizador = $reservaCircuitos->localizador;
			$lista->proforma = $reservaCircuitos->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaCircuitos->fecha_reserva));
			$lista->pasajero = $reservaCircuitos->pasajero_principal;
			$lista->estado = $reservaCircuitos->id_estado_reserva;
			$lista->id_proveedor = $reservaCircuitos->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaCircuitos->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaCircuitos->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaCircuitos->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaCircuitos->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaCircuitos->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkout));
			$lista->monto = $reservaCircuitos->precio_venta;
			if($reservaCircuitos->id_agencia == 1){
				if(isset($reservaCircuitos->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaCircuitos->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}

			}else{
				$user = Usuario::where('id_usuario', '=', $reservaCircuitos->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsCircuitos[$key] = $lista;
		}	
		//Fin Servicios Circuitos


		//Inicio de Servicios Traslado
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.traslado'))
			    										->get();
			    break;
			case 2:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;
			case 5:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			}
		$resultsTraslado  = array();
		foreach($resultadosTraslado as $key=>$reservaTraslado){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaTraslado->id;
			$lista->localizador = $reservaTraslado->localizador;
			$lista->proforma = $reservaTraslado->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaTraslado->fecha_reserva));
			$lista->pasajero = $reservaTraslado->pasajero_principal;
			$lista->estado = $reservaTraslado->id_estado_reserva;
			$lista->id_proveedor = $reservaTraslado->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaTraslado->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaTraslado->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaTraslado->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaTraslado->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaTraslado->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaTraslado->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaTraslado->fecha_checkout));
			$lista->monto = $reservaTraslado->precio_venta;
			if($reservaTraslado->id_agencia == 1){
				if(isset($reservaTraslado->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaTraslado->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsTraslado[$key] = $lista;
		}	
		//Fin Servicios Traslado

		//Inicio de Servicios Actividad
		$resultadosActividades=[];
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio',config('constants.actividad'))
			    										->get();
			    break;
			case 2:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;	
			}

		$resultadosActividad  = array();
		foreach($resultadosActividades as $key=>$reservaActividad){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaActividad->id;
			$lista->localizador = $reservaActividad->localizador;
			$lista->proforma = $reservaActividad->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaActividad->fecha_reserva));
			$lista->pasajero = $reservaActividad->pasajero_principal;
			$lista->id_proveedor = $reservaActividad->id_proveedor;
			if(isset($reservaActividad->agencias->razon_social)){
				$lista->agencia = $reservaActividad->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaActividad->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaActividad->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaActividad->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaActividad->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaActividad->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaActividad->fecha_checkout));
			$lista->monto = $reservaActividad->precio_venta;
			$lista->estado = $reservaActividad->id_estado_reserva;
			if($reservaActividad->id_agencia == 1){
				if(isset($reservaActividad->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaActividad->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}else{
				$user = Usuario::where('id_usuario', '=', $reservaActividad->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultadosActividad[$key] = $lista;
		}	
		//Fin Servicios Traslado
		$client = new Client();
		$cuerpoProforma = [];
		$RestConsultaProfIn= new \StdClass;
		$RestConsultaProfIn->ts = date('Y-m-d h:m:s');
		$Detalles= new \StdClass;
		$Detalles->FechaDesde = '2017-01-01';
		$Detalles->FechaHasta = date('Y-m-d');
		$Detalles->VendedorId = $datosUsuarios->id_sistema_facturacion;
		$RestConsultaProfIn->Detalles =  $Detalles;
		$iibReq = new \StdClass;
		$iibReq->RestConsultaProfIn = $RestConsultaProfIn;
		try{
			$iibRsp = $client->post(Config::get('config.iibConProForma'), [
				'json' => $iibReq
			]);
			$iibObjRsp = json_decode($iibRsp->getBody());
			if(isset($iibObjRsp->RestConsultaProfOut->Proformas)){
				foreach($iibObjRsp->RestConsultaProfOut->Proformas as $key=>$datosProforma){
					$cuerpoProforma[$key]['value'] = $datosProforma->ProformaId;
					$cuerpoProforma[$key]['label'] = $datosProforma->ProformaId." - ".$datosProforma->ProformasNombrePasajero;
				}
			}
		}
		catch(RequestException $e){
			//entra aca pero si pones Exception nomas no agarra
		}

		if($datosUsuarios->idAgencia == Config::get('constants.agenciaDtp')){
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_usuario =".$datosUsuarios->idUsuario);
		}else{
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_perfil = 10");
		}

		$arrayAgente = [];
		foreach($agentes as $key=>$agente){
			//$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_vendedor_dtp;
			$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_sistema_facturacion;
			$arrayAgente[$key]['label']= $agente->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agente->usuario;
		}	

        if(empty($listado)&&empty($resultsCircuitos)&&empty($resultsTraslado)&&empty($resultadosActividad)&&empty($resultsFligth)){
        	flash('<b>NO POSEE RESERVAS REGISTRADAS</b>')->success();
			return Redirect::back();
        }else{
        	return view('pages.mc.misReservas')->with(['reservas'=>$listado,'reservasCiruito'=>$resultsCircuitos,'reservasTraslado'=>$resultsTraslado,'reservasActividad'=>$resultadosActividad,'resultsFligth'=>$resultsFligth, 'listadoComp'=>$arrayAerea, 'selectProforma'=>$cuerpoProforma, 'selectAgentes'=>$arrayAgente, 'listadoState'=>$arrayState, 'isDtp' => $datosUsuarios->idAgencia == 1 ? true : false]);
        }
	}*/
   	public function getReservasFlightMc(Request $req){
   		$results= [];
		//$results = Reserva::with('destino','agencia','usuario')->get();
		$results=ReservasVuelos::with('usuario', 'agencia')->where(function($query) use($req){
			if(!empty($req->input('companhia'))){
				$query->where('marketingcompany','=',$req->input('idFileReserva'));
			}

			if(!empty($req->input('idFileReserva'))){
				$query->where('controlnumber', 'ilike', "%".$req->input('idFileReserva')."%");
			}
			
			if(!empty($req->input('origen'))){
				$query->where('cod_origen','=',$req->input('origen'));
			}

			if(!empty($req->input('destino'))){
				$query->where('cod_destino','=',$req->input('destino'));
			}
			
			$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;

			if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
				$query->where('usuario_id',$datosUsuarios->idUsuario);
				//$query->orWhere('id_agente_dtp', $datosUsuarios->idUsuario);
			}else{
				$query->where('usuario_id',$datosUsuarios->idUsuario);
			}
		})->get();

		return response()->json($results);
   	}	
   	public function getReservasServiciosMc(Request $req){

				$result=ReservasServicio::with('usuarios', 'agencias')
   												->where('tipo_servicio','=',$req->input('type'))
   												->orderBy('fecha_reserva', 'DESC')
   												->get();

   				if(!empty($req->input('localizadorBusqueda'))){
					foreach($result as $key=>$resultado){
		   				if($resultado['localizador'] != $req->input('localizadorBusqueda')){
		   					unset($result[$key]);
		   				}
		   			}
		   		}	

				if(!empty($req->input('desdeBusqueda'))||!empty($req->input('hastaBusqueda'))){
					$arrayResultado = [];
					$desdeBusqueda = explode("/",$req->input('desdeBusqueda'));
					$hastaBusqueda = explode("/",$req->input('hastaBusqueda'));
					$fechaDesde = $desdeBusqueda[2]."-".$desdeBusqueda[1]."-".$desdeBusqueda[0].' 00:00:00';
					$fechaHasta = $hastaBusqueda[2]."-".$hastaBusqueda[1]."-".($hastaBusqueda[0]). ' 23:59:59';
					foreach($result as $key=>$resultado){
						if ($resultado['fecha_reserva'] >= $fechaDesde && $resultado['fecha_reserva'] <=$fechaHasta){
							$arrayResultado[] = $result[$key];
						}	
					}
					$result = $arrayResultado;
				}else{
					$result = $result;
				}	

		   		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
				if($datosUsuarios->idPerfil != 1){
					switch ($datosUsuarios->idPerfil) {
						case 2:
							foreach($result as $key=>$resultado){
								if($resultado['id_agencia'] != $datosUsuarios->idAgencia){
									unset($result[$key]);
								}
							}	
							break;
						case 3:
								foreach($result as $key=>$resultado){
									if($resultado['id_usuario'] != $datosUsuarios->idUsuario){
										unset($result[$key]);
									}
								}	
							break;
						case 5:
							foreach($result as $key=>$resultado){
								if($resultado['id_agencia'] != $datosUsuarios->idAgencia){
									unset($result[$key]);
								}
							}	
							break;
						case 10:
								foreach($result as $key=>$resultado){
									if($resultado['id_usuario'] != $datosUsuarios->idUsuario && $resultado['id_agente_dtp'] != $datosUsuarios->idUsuario ){
										unset($result[$key]);
									}
								}
							break;
					}		
				}

		   		if(!empty($req->input('pasajeroBusqueda'))){
		   			foreach($result as $key=>$resultado){
		   				$resultados  =strpos(strtolower($resultado->pasajero_principal_nombre."".$resultado->pasajero_principal_apellido), strtolower($req->input('pasajeroBusqueda')));
		   				if($resultados === FALSE){
		   					unset($result[$key]);
		   				}
		   			}
		   		}	
			$results  = array();
			foreach($result as $key=>$reserva){
				$lista = new \StdClass;
				$lista->id_reserva = $reserva->id;
				$lista->localizador = $reserva->localizador;
				$lista->proforma = $reserva->numero_proforma;
				$lista->fecha =  date('d/m/Y', strtotime($reserva->fecha_reserva));
				$lista->pasajero = $reserva->pasajero_principal;
				$lista->agencia = $reserva->agencias->razon_social;
				$lista->vendedor_agencia = $reserva->usuarios->usuario;
				$lista->cancelacion_monto = $reserva->monto_cancelacion;
				$lista->cancelacion_desde = date('d/m/Y', strtotime($reserva->fecha_cancelacion));
				$lista->fecha_checkin = date('d/m/Y', strtotime($reserva->fecha_checkin));
				$lista->fecha_checkout = date('d/m/Y', strtotime($reserva->fecha_checkout));
				$lista->monto = $reserva->precio_venta;
				$lista->estado = $reserva->id_estado_reserva;
				$lista->id_proveedor = $reserva->id_proveedor;

				if($reserva->id_agencia == 1){
					$lista->vendedor_dtp = $reserva->usuarios->nombre_apellido;
				}else{
					$user = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->get();
					if(isset($user)){
						$lista->vendedor_dtp = $user[0]->nombre_apellido;
					}else{
						$lista->vendedor_dtp = "";
					}
				}
				$results[] = $lista;
			}
			return $results;
   	}	
    	public function getReservasMc(Request $req){

   		$results= [];
   		$results=Reserva::with('destino', 'agencia', 'usuario')->orderBy('fecha_reserva', 'DESC')->get();
   		if(!empty($req->input('nrodeexpediente'))){
   			foreach($results as $key=>$resultado){
   				if(intval($resultado['numero_proforma']) != intval ($req->input('nrodeexpediente'))){
   					unset($results[$key]);
   				}
   			}
   		}

   		if(!empty($req->input('localizador'))){
			foreach($results as $key=>$resultado){
   				if($resultado['codigo'] != $req->input('localizador')){
   					unset($results[$key]);
   				}
   			}
   		}	

   		if(!empty($req->input('pasajeroPrincipal'))){
   			foreach($results as $key=>$resultado){
   				$resultados  =strpos(strtolower($resultado->pasajero_principal_nombre."".$resultado->pasajero_principal_apellido), strtolower($req->input('pasajeroPrincipal')));
   				if($resultados === FALSE){
   					unset($results[$key]);
   				}
   			}
   		}	

   		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		if($datosUsuarios->idPerfil != 1){
			switch ($datosUsuarios->idPerfil) {
				case 2:
					foreach($results as $key=>$resultado){
						if($resultado['id_agencia'] != $datosUsuarios->idAgencia){
							unset($results[$key]);
						}
					}	
					break;
				case 3:
						foreach($results as $key=>$resultado){
							if($resultado['id_usuario'] != $datosUsuarios->idUsuario){
								unset($results[$key]);
							}
						}	
					break;
				case 5:
					foreach($results as $key=>$resultado){
						if($resultado['id_agencia'] != $datosUsuarios->idAgencia){
							unset($results[$key]);
						}
					}	
					break;
				case 10:
					foreach($results as $key=>$resultado){
							if($resultado['id_usuario'] != $datosUsuarios->idUsuario && $resultado['id_agente_dtp'] != $datosUsuarios->idUsuario ){
								unset($results[$key]);
							}
						}
					break;
			}
		}	

		if(!empty($req->input('desde'))||!empty($req->input('hasta'))){
			$arrayResultado = [];
			$desdeBusqueda = explode("/",$req->input('desde'));
			$hastaBusqueda = explode("/",$req->input('hasta'));
			$fechaDesde = $desdeBusqueda[2]."-".$desdeBusqueda[1]."-".$desdeBusqueda[0].' 00:00:00';
			$fechaHasta = $hastaBusqueda[2]."-".$hastaBusqueda[1]."-".($hastaBusqueda[0]). ' 23:59:59';
			foreach($results as $key=>$resultado){
				if ($resultado['fecha_reserva'] >= $fechaDesde && $resultado['fecha_reserva'] <=$fechaHasta){
					$arrayResultado[] = $results[$key];
				}	
			}
			$results = $arrayResultado;
		}else{
			$results = $results;
		}	

		if(!empty($req->input('estadoreserva'))){
			foreach($results as $key=>$resultado){
				if($resultado['id_estado_reserva'] != $req->input('estadoreserva')){
					unset($results[$key]);				
				}
			}
		}
		$listado = array();
		foreach($results as $key=>$reservas){
			$lista = new \StdClass;
			$lista->id_reserva = $reservas->id;
			$lista->expediente = "<b>".$reservas->id_file_reserva."</b>";
			$lista->localizador = "<b>".$reservas->codigo."</b>";
			$lista->proforma = "<b>".$reservas->numero_proforma."</b>";
			$lista->destino = $reservas->destino->desc_destino;
			$lista->fecha =  date('d-m-Y H:i:s', strtotime($reservas->fecha_reserva));
			$lista->pasajero = "<b>".$reservas->pasajero_principal_apellido.", ".$reservas->pasajero_principal_nombre."</b>";
			$lista->agencia = $reservas->agencia->razon_social;
			$lista->vendedor_agencia = "<b>".$reservas->usuario->usuario."</b>";
			$lista->cancelacion_monto = $reservas->cancelacion_monto;
			$lista->cancelacion_desde = $reservas->cancelacion_desde;
			$lista->id_proveedor = $reservas->id_proveedor;
		
			if($reservas->id_agencia == 1){
				$lista->vendedor_dtp = $reservas->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reservas->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}

			if($reservas->id_agencia == 1){
				$lista->vendedor_dtp = $reservas->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reservas->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			/*if($reservas->enviado_proforma =="S"&& $reservas->codret_proforma == "0"){
				$lista->iconoproforma = 0;
			}else{
				$lista->iconoproforma = 1;
			}*/

			/*if($reservas->id_estado_reserva == config('constants.resEliminada')){
				if($reservas->cancelado_proforma == "S" && $reservas->codret_proforma_cancelacion == "0"){
					$lista->iconocancelar = 2;
				}else{
					$lista->iconocancelar = 1;
				}
			}else{
				$lista->iconocancelar = 0;
			}*/

			$lista->expediente_vs = $reservas->expediente_vstour;
			$lista->factura = $reservas->nro_factura;
			//$lista->monto_neto = number_format($reservas->monto_sin_comision, 2, '.', ' ');
			$lista->monto_sin_comision = number_format($reservas->monto_sin_comision, 2, '.', ' '); 
			//$lista->monto = number_format($reservas->monto_cobrado, 2, '.', ' ');
			$lista->monto_cobrado = number_format($reservas->monto_cobrado, 2, '.', ' ');
			$lista->estado = '<img alt="" style="width: 20px; margin-left: 25%;" src="images/icon/'.$reservas->id_estado_reserva.'.png">';
			$lista->estados = $reservas->id_estado_reserva;
			//solamente si no esta cancelada mostrar.
			
			//$lista->iconoproforma=0;
			//$lista->estado = config('constants.resConfirmada');
			$listado['data'][] =  $lista;
		}	

		return response()->json($listado);
	}

   	public function detallesReservaFligthMc(Request $req, $id){
   		$results = ReservasVuelos::with('usuario', 'agencia', 'itinerarios','pasajeros') 
                    ->where('id', '=', $id)
                    ->get(); 

        return view('pages.mc.detallesReservaFlight')->with(['resultado'=>$results]);
   	}	
   	public function detallesReservaMc(Request $req, $id){
  		$reserva = Reserva::with('destino', 'agencia', 'usuario', 'monedaorigen', 'usuariocancelacion') 
                    ->where('id', '=', $id)
                    ->first(); 
        if($reserva->id_agencia == 1){
			$nombre_agente_dtp = $reserva->usuario->nombre_apellido;
		}else{
			//dd($reserva->id_agente_dtp);
			$agente_dtp = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->first();
			if(isset($agente_dtp)){
				$nombre_agente_dtp = $agente_dtp->nombre_apellido;
			}else{
				$nombre_agente_dtp = "";
			}
		}
        $ocupantesHabitacion = [];            
        $ocupantesHabitacion = DB::select("select *
											from habitaciones_reserva, ocupantes_habitacion
											where habitaciones_reserva.reserva = ".$id."
											and habitaciones_reserva.titular = ocupantes_habitacion.id");
		//dd($nombre_agente_dtp);
		return view('pages.mc.detalleReserva')->with(['reserva'=>$reserva, 'nombre_agente_dtp'=>$nombre_agente_dtp, 'ocupantesHabitacion'=>$ocupantesHabitacion]);
   	}
	public function consultaFacturasMc(Request $req){
		$arrayAgencia = [];
    	$arrayUsuario = [];
    	//Filtro para Administrador DTP
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
	    	$agencias = Agencias::where('activo', '=', 'S')->get();
			foreach ($agencias as $key=>$agencia){
				$arrayAgencia[$key]['value']= $agencia->id_sistema_facturacion;
				$arrayAgencia[$key]['label']= $agencia->razon_social;
			}
			$usuarios = Usuario::where('activo', '=', 'S')->get();
    	}
    	//Filtro para Administrador Agencia
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminAgencia')){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
				->where('activo', '=', 'S')
				->get();
			$usuarios = Usuario::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
			->where('activo', '=', 'S')
			->get();

    	}
    	//Filtro para Vendedor
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.vendedor')){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
			$usuarios = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
    	}

    	//Filtro para Operador Administrador Agencia
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminOpAdmin')){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
				->where('activo', '=', 'S')
				->get();
			$usuarios = Usuario::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
			->where('activo', '=', 'S')
			->get();

    	}
    	//Filtro para Vendedor
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==10){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
			$usuarios = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
    	}

    	//Se arman las listas para la página
		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_sistema_facturacion;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}

		foreach ($usuarios as $key=>$usuario){
			$arrayUsuario[$key]['value']= $usuario->id_sistema_facturacion;
			$arrayUsuario[$key]['labelUsuario']= $usuario->usuario;
			$arrayUsuario[$key]['labelApellidoNombre']= $usuario->nombre_apellido;
		}
		$usuario = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
		$agency = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();

        return view('pages.mc.consultarFactura')->with(['listadoAgencia'=>$arrayAgencia,'listadoUsuarios'=>$arrayUsuario, 'usuario'=>$usuario, 'agency'=>$agency]);
   	}
   	public function getCancelarReservasMc(Request $request){
		$client = new Client([
			'headers' => [ 
						   'Content-Type' => 'application/json',
						   'Cache-Control'=>'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
						   'id_proveedor'=>0,
						   'id_usuario'=>Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
						   'fechaRequest'=> date('Y-m-d H:i:s')
						   ]
		]);
   		/*$iibReq = new \StdClass;
		$iibReq->idReserva = $request->input('idReserva');
		$iibReq->token = Session::get('datos-loggeo')->token;*/
		$iibReq = new \StdClass;
		$iibReq->idioma = "CAS";
   		//$reservaId= $request->dataReserva;
		$reservaId=$request->input('idReserva');
		$results = Reserva::with('usuario')
                    ->where('id', '=', $reservaId)
                    ->get(); 
        $datosCancelacionReserva = new \StdClass;
        $datos = new \StdClass;
        foreach($results as $key=>$reservas){   
        	//$iibReq->token = $reservas->id_request;  
			$iibReq->token = Session::get('datos-loggeo')->token;
			$iibReq->codReserva = $reservaId;
        	$datosCancelacionReserva->idReserva = $reservaId;
	        $datosCancelacionReserva->codProveedor = $reservas->id_proveedor;
	        $datosCancelacionReserva->codReserva = $reservas->codigo_cancelacion;
	        $datosCancelacionReserva->codHotel = $reservas->codigo_hotel;
	        $datosCancelacionReserva->proformaId = $reservas->numero_proforma;
	        $datosCancelacionReserva->nroItem = $reservas->numero_de_item;
	        if($reservas->usuario->codigo_agente!="" || $reservas->usuario->codigo_agente!=null){
		        if($reservas->id_agencia == 1){
						$datosCancelacionReserva->email = $reservas->usuario->email;
					}else{
						$user = Usuario::where('id_usuario', '=', $reservas->usuario->codigo_agente)->get();
						$datosCancelacionReserva->email  = $user[0]->email;
					}
			}else{
				$datosCancelacionReserva->email  = "";
			}		
	        $datosCancelacionReserva->fechaDesde = date('m/d/Y', strtotime($reservas->fecha_checkin));
	        $datosCancelacionReserva->fechaHasta = date('m/d/Y', strtotime($reservas->fecha_checkout));
	    }    
		//dd($datosCancelacionReserva);

		$datos->datosCancelacionReserva = $datosCancelacionReserva;
		$iibReq->datos = $datos;
		$datosLogueo = Session::get('datos-loggeo');
		$datosUsuarios = new \StdClass;
		$datosUsuarios->idUsuario = $datosLogueo->datos->datosUsuarios->idUsuario;
		$iibReq->datosUsuario = $datosUsuarios;
		// Envio de datos para la cancelación
		try{
			$iibRsp = $client->post(Config::get('config.iibCancelations'),
				[
					'body' => json_encode($iibReq),
					]
			);
		}
		catch(RequestException $e){
			return view('pages.mc.timeErrorConexion');
		}
		catch(ClientException $e){
			return view('pages.mc.errorConexion');	
		}	
		$iibObjRsp = json_decode($iibRsp->getBody());
		//echo "<br/><br/>RESPONSE RESERVA+++++<br>";print_r(json_encode($iibObjRsp));
 		return response()->json($iibObjRsp);
   	}
    public function generarPagoMc(Request $request){
		$paymentmethod = Paymentmethod::where('paymentmethod_state', 'ACTIVO')->get();
		$banco = Banco::where('activo', 'S')->get();
		$currency = Currency::where('activo', 'S')->get();
		return view('pages.mc.generarPago')->with(['paymentmethod'=>$paymentmethod, 'banco'=>$banco, 'moneda'=>$currency]);
    }
    public function doGuardarPagoMc(Request $request){
   		$usuario = new GestionPagos;
    	$usuario->id_agencia = Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia;
    	$usuario->usuario_id = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
    	$usuario->fecha = date('Y-m-d H:i:s');
    	$usuario->updated_at= date('Y-m-d H:i:s');
    	$usuario->nro_comprobante = $request->input('n_comprobante');
    	$usuario->banco_id = $request->input('banco_id');
    	$usuario->importe = str_replace(".","", $request->input('importe'));
    	$usuario->currency_id = $request->input('currency_id');
    	$usuario->paymentmethod_id = $request->input('paymentmethod_id');
		try{
			$usuario->save();
			$results= 0;
			return $results;			
		} catch(\Exception $e){
			$results= 1;
			return $results;
		}
    }

    public function indexPagoMc(Request $request){

		return view('pages.mc.indexPago');
    }

    public function pagosMc(Request $request){

        	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 1) {
			$gestionPagos = GestionPagos::with('agencia', 'usuario','banco', 'paymentmethod', 'currency')
										  ->get();
		}else{
 			$gestionPagos =  GestionPagos::with('agencia', 'usuario','banco', 'paymentmethod', 'currency')
 									->where('id_agencia',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
			    					->get();
		}
		if(!empty($request->input('desde'))||!empty($request->input('hasta'))){
			$arrayResultado = [];
			$desdeBusqueda = explode("/",$request->input('desde'));
			$hastaBusqueda = explode("/",$request->input('hasta'));
			$fechaDesde = $desdeBusqueda[2]."-".$desdeBusqueda[1]."-".$desdeBusqueda[0].' 00:00:00';
			$fechaHasta = $hastaBusqueda[2]."-".$hastaBusqueda[1]."-".($hastaBusqueda[0]). ' 23:59:59';
			foreach($gestionPagos as $key=>$resultado){
				if ($resultado['fecha'] >= $fechaDesde && $resultado['fecha'] <=$fechaHasta){
					$arrayResultado[] = $gestionPagos[$key];
				}	
			}
			$gestionPagos = $arrayResultado;
		}else{
			$gestionPagos = $gestionPagos;
		}

		return response()->json($gestionPagos);
    }  

   	public function logActivityMc(){
   		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		switch ($datosUsuarios->idPerfil) {
				case 1:
				    $notificacion = Notificacion::with('tipos')
						    					->where('leido', '=', 'N')
						    					->orderBy('tipo', 'desc')
				    							->get(['tipo', ]);
				    break;
				case 2:
				    $notificacion = Notificacion::with('tipos')
				    					->where('id_agencia',$datosUsuarios->idAgencia)
				    					->where('leido', '=', 'N')
				    					->orderBy('tipo', 'desc')
				    					->get(['tipo']);
				    break;
				case 3:
					$notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->where('leido', '=', 'N')
				    					->orderBy('tipo', 'desc')
				    					->get(['tipo']);
				    break;					
				case 4:
					$notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->where('leido', '=', 'N')
				    					->orderBy('tipo', 'desc')
				    					->get(['tipo']);
				case 5:
				    $notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->where('leido', '=', 'N')
				    					->orderBy('tipo', 'desc')
				    					->get(['tipo']);
				    break;
				case 10:
					$notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->where('leido', '=', 'N')
				    					->orderBy('tipo', 'desc')
				    					->get(['tipo']);
				    break;					

			}

			$notifica = [];
			$tipoNotificacion = 0;
			$cantidad = 0;
			if(isset($notificacion)){
				foreach($notificacion as $key=>$notify){
					if($notify['tipo'] != $tipoNotificacion){
						$cantidad =  0;
					}
					$cantidad++;
					$notifica['cantTotal'] = count($notificacion);
		   			$notifica['tipos'][$notify['tipo']] = $notify['tipos']['titulo']."_".$notify['tipos']['clase']."_".$cantidad;
		   			$tipoNotificacion = $notify['tipo'];
				}
			}else{
				$cantidad =  0;
				$notifica['cantTotal'] = 0;
				$notifica['tipos'][0]= "";
			}	
	   		return response()->json($notifica);
   	}	

   	public function notificacionesIndexMc(Request $req){
   		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		switch ($datosUsuarios->idPerfil) {
				case 1:
				    $notificacion = Notificacion::with('tipos')
						    					->orderBy('tipo', 'desc')
				    							->get();
				    break;
				case 2:
				    $notificacion = Notificacion::with('tipos')
				    					->where('id_agencia',$datosUsuarios->idAgencia)
				    					->orderBy('tipo', 'desc')
				    					->get();
				    break;
				case 3:
					$notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->orderBy('tipo', 'desc')
				    					->get();
				    break;					
				case 4:
					$notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->orderBy('tipo', 'desc')
				    					->get();
				case 5:
				    $notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->orderBy('tipo', 'desc')
				    					->get();
				    break;
				case 10:
					$notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->orderBy('tipo', 'desc')
				    					->get();
				    break;					

			}
			$resultado = [];
			$resultadoCabecera =  [];
			$tipoNotificacion = 0;
			$cantidad = 0;
			$cantidadNoLeidos = 0;
			$totalNotificaciones  = count($notificacion);
			foreach($notificacion as $key=>$notify){
				if($notify['tipo'] != $tipoNotificacion){
					$cantidad =  0;
					$cantidadNoLeidos = 0;
				}
				$cantidad++;
				if($notify['leido'] == 'N'){
					$cantidadNoLeidos++;
				}
				$resultado[$notify['tipo']]['nombre'] = $notify['tipos']['titulo'];
				$resultado[$notify['tipo']]['detalles'][] = $notify;
				$resultadoCabecera[$notify['tipo']]['nombre'] = $notify['tipos']['titulo'];
				$resultadoCabecera[$notify['tipo']]['cantidadNoLeidos'] = $cantidadNoLeidos;
				$resultadoCabecera[$notify['tipo']]['clase'] = $notify['tipos']['clase'];
				$tipoNotificacion = $notify['tipo'];
			}

			return view('pages.mc.notificaciones')->with(['notificacion'=>$resultado, 'cabeceraNoficacion'=>$resultadoCabecera, 'total' =>$totalNotificaciones]);
   	}

   	public function getLogMc(Request $req){
		$logs = DB::connection('pgsqlLog')
					->table("logs")
					->where(function($query) use($req){
						if(!empty($req->input('usuario_id'))){
							$query->where('user_id','=',$req->input('usuario_id'));
						}
						if(!empty($req->input('agencia_id')) && !empty($req->input('sucursal_id'))){
							$users = Usuario::where('id_agencia', '=', $req->input('agencia_id'))
											->orWhere('id_sucursal_agencia', '=',$req->input('sucursal_id'))
											->get(['id_usuario']);
							foreach($users as $key=>$user){
								$query->where('user_id',$user->id_usuario);
							}
						} 
						if(!empty($req->input('desde'))||!empty($req->input('desde'))){
							$fecha = date('Y-m-d', strtotime($req->input('desde')));
							$query->whereBetween('date_time_request', array(
																			$fecha.' 00:00:00', $fecha.'23:59:59'
																		)
												);
										}         
					})	
					->get();
		DB::disconnect('pgsqlLog');
		$listado = array();
		foreach($logs as $index=>$log){
			$listado[$index]['id'] = $log->id;
			$listado[$index]['url'] = $log->url;
			$listado[$index]['metodo'] = $log->metodo;
			$listado[$index]['id_request'] = $log->id_request;
			$listado[$index]['date_time_request'] = $log->date_time_request;
			$listado[$index]['date_time_response'] = $log->date_time_response;
			$user = Usuario::where('id_usuario', '=', $log->id)->get(['id_usuario', 'nombre_apellido']);
			if(isset($user[0]->id_usuario)){
				$listado[$index]['usuario'] = $user[0]->id_usuario." - ".$user[0]->nombre_apellido;
			}else{
				$listado[$index]['usuario'] = "";
			}
		}
		return response()->json($listado);
   	}	

   	public function detalleNotificacionMc(Request $req){
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		switch ($datosUsuarios->idPerfil) {
				case 1:
				    $notificacion = Notificacion::with('tipos')
						    					->orderBy('tipo', 'desc')
				    							->get();
				    break;
				case 2:
				    $notificacion = Notificacion::with('tipos')
				    					->where('id_agencia',$datosUsuarios->idAgencia)
				    					->orderBy('tipo', 'desc')
				    					->get();
				    break;
				case 3:
					$notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->orderBy('tipo', 'desc')
				    					->get();
				    break;					
				case 4:
					$notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->orderBy('tipo', 'desc')
				    					->get();
				case 5:
				    $notificacion = Notificacion::with('tipos')
				    					->where('id_agencia',$datosUsuarios->idAgencia)
				    					->orderBy('tipo', 'desc')
				    					->get();
				    break;
				case 10:
					$notificacion = Notificacion::with('tipos')
				    					->where('id_usuario',$datosUsuarios->idUsuario)
				    					->orderBy('tipo', 'desc')
				    					->get();
				    break;					

			}
			$resultado = [];
			$resultadoCabecera =  [];
			$tipoNotificacion = 0;
			$cantidad = 0;
			$cantidadNoLeidos = 0;
			$totalNotificaciones  = count($notificacion);
			foreach($notificacion as $key=>$notify){
				if($notify['tipo'] != $tipoNotificacion){
					$cantidad =  0;
					$cantidadNoLeidos = 0;
				}
				$cantidad++;
				if($notify['leido'] == 'N'){
					$cantidadNoLeidos++;
				}
				$resultadoCabecera[$notify['tipo']]['nombre'] = $notify['tipos']['titulo'];
				$resultadoCabecera[$notify['tipo']]['cantidadNoLeidos'] = $cantidadNoLeidos;
				$resultadoCabecera[$notify['tipo']]['clase'] = $notify['tipos']['clase'];
				$tipoNotificacion = $notify['tipo'];
			}   		$notificacion = Notificacion::with('agencia', 'usuario', 'tipos')
   									->where("id", $req->id)
   									->get();
   		DB::table('notificaciones')
					->where('id',$notificacion[0]['id'])
					->update([
					        'leido' => 'S',
					        ]);
   		return view('pages.mc.detalleNotificacion')->with(['detalleNotificacion'=>$notificacion[0], 'cabeceraNoficacion'=>$resultadoCabecera]);	
   	}	

	public function reservasMc(){
		//Inicia Reservas de Hoteles
		$listado = array();
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		
		$fecha = date('Y-m-d');
		$nuevafecha = date("Y-m-d",strtotime($fecha."- 30 day"));
		$fechaHoy = date("Y-m-d",strtotime($fecha."+ 1 day"));
		$results =  [];
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:i', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:i', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					 ->get();
			    break;
			case 2:
			    $results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->orderBy('fecha_reserva', 'desc')
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();

			    break;
			case 3:
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}else{
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				}	
			break;
			case 10:
					$results =  Reserva::with('destino', 'agencia', 'usuario')
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->orWhere('id_agente_dtp', $datosUsuarios->idUsuario)
			    					->whereBetween('fecha_reserva', array(
													date('Y-m-d h:m:s', strtotime($nuevafecha. ' 00:00:00')),
													date('Y-m-d h:m:s', strtotime($fechaHoy. ' 23:59:59'))
													))
			    					->get();
				break;
		}

		foreach($results as $reserva){
			$lista = new \StdClass;
			$lista->id_reserva = $reserva->id;
			$lista->expediente = $reserva->id_file_reserva;
			$lista->proforma = $reserva->numero_proforma;
			$lista->localizador = $reserva->codigo;
			$lista->destino = $reserva->destino->desc_destino;
			$lista->fecha =  $reserva->fecha_reserva;
			$lista->pasajero = $reserva->pasajero_principal_apellido.", ".$reserva->pasajero_principal_nombre;
			$lista->agencia = $reserva->agencia->razon_social;
			$lista->agencia_id = $reserva->agencia->id_agencia;
			$lista->vendedor_agencia = $reserva->usuario->usuario;
			$lista->cancelacion_monto = $reserva->cancelacion_monto;
			$lista->cancelacion_desde = $reserva->cancelacion_desde;
			$lista->id_proveedor = $reserva->id_proveedor;
			if($reserva->id_agencia == 1){
				$lista->vendedor_dtp = $reserva->usuario->nombre_apellido;
			}else{
				$user = Usuario::where('id_usuario', '=', $reserva->id_agente_dtp)->get();
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			if($reserva->enviado_proforma =="S"&& $reserva->codret_proforma == "0"){
				$lista->iconoproforma = 0;
			}else{
				//if($reserva->id_estado_reserva != config('constants.resConfirmada')){
					$lista->iconoproforma = 1;
				/*}else{
					$lista->iconoproforma = 1;	
				}*/
			}

			if($reserva->id_estado_reserva == config('constants.resEliminada')){
				if($reserva->cancelado_proforma == "S" && $reserva->codret_proforma_cancelacion == "0"){
					$lista->iconocancelar = 2;
				}else{
					$lista->iconocancelar = 1;
				}
			}else{
				$lista->iconocancelar = 0;
			}

			$lista->expediente_vs = "";
			$lista->factura = "";
			//$lista->monto_neto = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			$lista->monto_sin_comision = number_format($reserva->monto_sin_comision, 2, '.', ' '); 
			//$lista->monto = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->monto_cobrado = number_format($reserva->monto_cobrado, 2, '.', ' ');
			$lista->estado = $reserva->id_estado_reserva;
			$lista->iconoproforma=0;
			//$lista->estado = config('constants.resConfirmada');

			//dd($lista);
			$listado[] =  $lista;
		}
		$arrayState = [];
		$states = State::where('activo', '=', true)->get();	
		foreach ($states as $key=>$state){
			$arrayState[$key]['value']= $state->id_estado_reserva;
			$arrayState[$key]['label']= $state->nombre_estado;
		}
		//Fin busqueda de Hotel
		
		//Inicio de Reservas Vuelos		
		$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios;
		
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')->get();
			    break;
			case 2:
			    $resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('agencia_id',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				}else{
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
			    					->get();
				}	
			break;
			case 10:
					$resultsFligth =  ReservasVuelos::with('usuario', 'agencia')
			    					->where('usuario_id',$datosUsuarios->idUsuario)
									->orWhere('agente_dtp_id', $datosUsuarios->idUsuario)
									->get();
				break;

		}

		//Fin de Reservas Vuelas

		include("../comAerea.php");
		$arrayAerea = [];
		foreach ($comAerea as $key=>$area){
			$nombre = explode("|", $area);
			$arrayAerea[$key]['label']= $nombre[0];
		}

		//Inicio de Servicios Circuitos
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    										->where('tipo_servicio','=',config('constants.circuito'))
			    										->get();
			    break;
			case 2:
			    $resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.circuito'))
								    					->where('id_agencia',$datosUsuarios->idAgencia)
								    					->get();
			    break;
			case 3:
				$resultadosCircuitos = [];
				$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				break;
			case 4:
				$resultadosCircuitos = [];
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosCircuitos =  ReservasServicio::with('usuarios', 'agencias')
			    			    	->where('tipo_servicio','=',config('constants.circuito'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}
				break;
			case 10:
				$resultadosCircuitos = [];
					$resultadosCircuitos = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.circuito'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.circuito'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				break;

		}
		$resultsCircuitos  = array();
		foreach($resultadosCircuitos as $key=>$reservaCircuitos){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaCircuitos->id;
			$lista->localizador = $reservaCircuitos->localizador;
			$lista->proforma = $reservaCircuitos->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaCircuitos->fecha_reserva));
			$lista->pasajero = $reservaCircuitos->pasajero_principal;
			$lista->estado = $reservaCircuitos->id_estado_reserva;
			$lista->id_proveedor = $reservaCircuitos->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaCircuitos->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaCircuitos->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaCircuitos->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaCircuitos->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaCircuitos->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaCircuitos->fecha_checkout));
			$lista->monto = $reservaCircuitos->precio_venta;
			if($reservaCircuitos->id_agencia == 1){
				if(isset($reservaCircuitos->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaCircuitos->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}

			}else{
				$user = Usuario::where('id_usuario', '=', $reservaCircuitos->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsCircuitos[$key] = $lista;
		}	
		//Fin Servicios Circuitos


		//Inicio de Servicios Traslado
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio','=',config('constants.traslado'))
			    										->get();
			    break;
			case 2:
			    $resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
			     break;					
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosTraslado =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.traslado'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;
			case 10:
					$resultadosTraslado = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.traslado'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.traslado'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				 break;
			}
		$resultsTraslado  = array();
		foreach($resultadosTraslado as $key=>$reservaTraslado){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaTraslado->id;
			$lista->localizador = $reservaTraslado->localizador;
			$lista->proforma = $reservaTraslado->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaTraslado->fecha_reserva));
			$lista->pasajero = $reservaTraslado->pasajero_principal;
			$lista->estado = $reservaTraslado->id_estado_reserva;
			$lista->id_proveedor = $reservaTraslado->id_proveedor;
			if(isset($reservaTraslado->agencias->razon_social)){
				$lista->agencia = $reservaTraslado->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaTraslado->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaTraslado->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaTraslado->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaTraslado->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaTraslado->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaTraslado->fecha_checkout));
			$lista->monto = $reservaTraslado->precio_venta;
			if($reservaTraslado->id_agencia == 1){
				if(isset($reservaTraslado->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaTraslado->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
				if(isset($user)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultsTraslado[$key] = $lista;
		}	
		//Fin Servicios Traslado

		//Inicio de Servicios Actividad
		$resultadosActividades=[];
		switch ($datosUsuarios->idPerfil) {
			case 1:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    			    						->where('tipo_servicio',config('constants.actividad'))
			    										->get();
			    break;
			case 2:
			    $resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_agencia',$datosUsuarios->idAgencia)
			    					->get();
			    break;
			case 3:
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				break;
			case 4:
				if(Config::get('constants.adminDtp') == $datosUsuarios->idAgencia){
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				}else{
					$resultadosActividades =  ReservasServicio::with('usuarios', 'agencias')
			    					->where('tipo_servicio','=',config('constants.actividad'))
			    					->where('id_usuario',$datosUsuarios->idUsuario)
			    					->get();
				}	
				break;	
			case 10:
					$resultadosActividades = ReservasServicio::with('usuarios', 'agencias')
													    ->where('tipo_servicio','=',config('constants.actividad'))
													    ->where('id_usuario',$datosUsuarios->idUsuario)
													    ->orWhere(function ($query) use ($datosUsuarios) {
													            $query->where('tipo_servicio','=',config('constants.actividad'))
													                  ->where('id_agente_dtp',$datosUsuarios->idUsuario);
													    })
													    ->get();
				break;										    
			}

		$resultadosActividad  = array();
		foreach($resultadosActividades as $key=>$reservaActividad){
			$lista = new \StdClass;
			$lista->id_reserva = $reservaActividad->id;
			$lista->localizador = $reservaActividad->localizador;
			$lista->proforma = $reservaActividad->numero_proforma;
			$lista->fecha =  date('d/m/Y', strtotime($reservaActividad->fecha_reserva));
			$lista->pasajero = $reservaActividad->pasajero_principal;
			$lista->id_proveedor = $reservaActividad->id_proveedor;
			if(isset($reservaActividad->agencias->razon_social)){
				$lista->agencia = $reservaActividad->agencias->razon_social;
			}else{
				$lista->agencia = "";
			}
			if(isset($reservaActividad->usuarios->usuario)){
				$lista->vendedor_agencia = $reservaActividad->usuarios->usuario;
			}else{
				$lista->vendedor_agencia = "";
			}	
			$lista->cancelacion_monto = $reservaActividad->monto_cancelacion;
			$lista->cancelacion_desde = date('d/m/Y', strtotime($reservaActividad->fecha_cancelacion));
			$lista->fecha_checkin = date('d/m/Y', strtotime($reservaActividad->fecha_checkin));
			$lista->fecha_checkout = date('d/m/Y', strtotime($reservaActividad->fecha_checkout));
			$lista->monto = $reservaActividad->precio_venta;
			$lista->estado = $reservaActividad->id_estado_reserva;
			if($reservaActividad->id_agencia == 1){
				if(isset($reservaActividad->usuarios->nombre_apellido)){
					$lista->vendedor_dtp = $reservaActividad->usuarios->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}else{
				$user = Usuario::where('id_usuario', '=', $reservaActividad->id_agente_dtp)->get();
				if(isset($user[0]->nombre_apellido)){
					$lista->vendedor_dtp = $user[0]->nombre_apellido;
				}else{
					$lista->vendedor_dtp = "";
				}
			}
			$resultadosActividad[$key] = $lista;
		}	
		//Fin Servicios Traslado
		$client = new Client();
		$cuerpoProforma = [];
		$RestConsultaProfIn= new \StdClass;
		$RestConsultaProfIn->reqToken = $datosUsuarios->tokenBk;
		$RestConsultaProfIn->ts = date('Y-m-d h:m:s');
		$Detalles= new \StdClass;
		$Detalles->FechaDesde = '2017-01-01';
		$Detalles->FechaHasta = date('Y-m-d');
		$Detalles->VendedorId = $datosUsuarios->id_sistema_facturacion;
		$RestConsultaProfIn->Detalles =  $Detalles;
		$iibReq = new \StdClass;
		$iibReq->RestConsultaProfIn = $RestConsultaProfIn;
		try{
			$iibRsp = $client->post(Config::get('config.iibConProForma'), [
				'json' => $iibReq
			]);
			$iibObjRsp = json_decode($iibRsp->getBody());
			if(isset($iibObjRsp->RestConsultaProfOut->Proformas)){
				foreach($iibObjRsp->RestConsultaProfOut->Proformas as $key=>$datosProforma){
					$cuerpoProforma[$key]['value'] = $datosProforma->ProformaId;
					$cuerpoProforma[$key]['label'] = $datosProforma->ProformaId." - ".$datosProforma->ProformasNombrePasajero;
				}
			}
		}
		catch(RequestException $e){
			//entra aca pero si pones Exception nomas no agarra
		}

		if($datosUsuarios->idAgencia == Config::get('constants.agenciaDtp')){
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_usuario =".$datosUsuarios->idUsuario);
		}else{
			$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_perfil = 10");
		}

		$arrayAgente = [];
		foreach($agentes as $key=>$agente){
			//$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_vendedor_dtp;
			$arrayAgente[$key]['value']=$agente->id_usuario."_".$agente->id_sistema_facturacion;
			$arrayAgente[$key]['label']= $agente->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agente->usuario;
		}	

        if(empty($listado)&&empty($resultsCircuitos)&&empty($resultsTraslado)&&empty($resultadosActividad)&&empty($resultsFligth)){
        	flash('<b>NO POSEE RESERVAS REGISTRADAS</b>')->success();
			return Redirect::back();
        }else{
        	return view('pages.reportes.reservas')->with(['reservas'=>$listado,'reservasCiruito'=>$resultsCircuitos,'reservasTraslado'=>$resultsTraslado,'reservasActividad'=>$resultadosActividad,'resultsFligth'=>$resultsFligth, 'listadoComp'=>$arrayAerea, 'selectProforma'=>$cuerpoProforma, 'selectAgentes'=>$arrayAgente, 'listadoState'=>$arrayState, 'isDtp' => $datosUsuarios->idAgencia == 1 ? true : false]);
        }
	}

   	
	public function historicoFacturasMc(Request $req){
		$arrayAgencia = [];
    	$arrayUsuario = [];
    	//Filtro para Administrador DTP
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
	    	$agencias = Agencias::where('activo', '=', 'S')->get();
			foreach ($agencias as $key=>$agencia){
				$arrayAgencia[$key]['value']= $agencia->id_sistema_facturacion;
				$arrayAgencia[$key]['label']= $agencia->razon_social;
			}
			$usuarios = Usuario::where('activo', '=', 'S')->get();
    	}
    	//Filtro para Administrador Agencia
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminAgencia')){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
				->where('activo', '=', 'S')
				->get();
			$usuarios = Usuario::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
			->where('activo', '=', 'S')
			->get();

    	}
    	//Filtro para Vendedor
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.vendedor')){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
			$usuarios = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
    	}
    	//Filtro para Vendedor
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==10){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
			$usuarios = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
    	}

    	//Filtro para Operador Administrador Agencia
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminOpAdmin')){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
				->where('activo', '=', 'S')
				->get();
			$usuarios = Usuario::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
			->where('activo', '=', 'S')
			->get();

    	}
    	//Se arman las listas para la página
		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_sistema_facturacion;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}

		foreach ($usuarios as $key=>$usuario){
			$arrayUsuario[$key]['value']= $usuario->id_sistema_facturacion;
			$arrayUsuario[$key]['labelUsuario']= $usuario->usuario;
			$arrayUsuario[$key]['labelApellidoNombre']= $usuario->nombre_apellido;
		}
		$usuario = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
		$agency = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();

        return view('pages.mc.historicoFactura')->with(['listadoAgencia'=>$arrayAgencia,'listadoUsuarios'=>$arrayUsuario, 'usuario'=>$usuario, 'agency'=>$agency]);
   	}
}
