<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use phpseclib\Crypt\RSA;
use phpseclib\File\X509;
use SoapClient;

class CertificateController extends Controller
{
    public function extractElements(Request $request)
{
    $certificateFile = '/opt/lampp/htdocs/gestur/public/SYLVIA MARIA  FERNANDEZ DE PRONO.pfx';
    $password = 'Dtp#oct2023';

    // Verificar si se ha enviado un archivo
    if (!$certificateFile) {
        return response()->json(['error' => 'No se ha enviado ningún archivo.'], 400);
    }

    // Guardar el archivo temporalmente
    $path = $certificateFile->store('temp');

    // Obtener la ruta completa del archivo guardado
    $fullPath = storage_path('app/'.$path);

    // Obtener el contenido del archivo .pfx
    $certificateData = file_get_contents($fullPath);

    // Cargar el certificado PKCS#12
    $x509 = new X509();
    $x509->loadP12($certificateData, $password);

    // Extraer los elementos del certificado
    $uri = $x509->getExtension('id-pe-authorityInfoAccess', true);
    $digestValue = $x509->getSignatureSubject();
    $signatureValue = base64_encode($x509->currentCert['tbsCertificate']['signature']['bitString']);
    $x509Certificate = base64_encode($x509->currentCert['cert']);
    $x509IssuerName = $x509->currentCert['tbsCertificate']['issuer']['rdnSequence'][0][0]['value']['utf8String'];
    $x509SerialNumber = $x509->currentCert['tbsCertificate']['serialNumber']->toString();

    // Eliminar el archivo temporal
    Storage::delete($path);

    // Retornar los elementos del certificado
    return response()->json([   
        'uri' => $uri,
        'digestValue' => $digestValue,
        'signatureValue' => $signatureValue,
        'x509Certificate' => $x509Certificate,
        'x509IssuerName' => $x509IssuerName,
        'x509SerialNumber' => $x509SerialNumber
    ]);
}

    public function AenvioFeca(Request $request)
    {

 // Aquí debes reemplazar con tu XML

      /*  $options = array(
            'location' => 'https://sifen-test.set.gov.py/de/ws/sync/recibe.wsd',
            'uri' => 'http://ekuatia.set.gov.py/sifen/xsd',
            'soap_version' => SOAP_1_2,
            'trace' => 1,
        );
        
        // Crear una instancia de SoapClient
        $client = new SoapClient(null, $options);
        
        // Enviar la solicitud SOAP
        $response = $client->__doRequest($xml, $options['location'], '', SOAP_1_2);
        
        // Procesar la respuesta del servicio web SOAP
        echo '<pre>';
        print_r($response);*/

        $wsdl = 'https://sifen-test.set.gov.py/de/ws/sync/recibe.wsd?wsdl'; // URL del servicio SOAP
        $options = [
            'trace' => 1, // Habilita el seguimiento de la solicitud y respuesta SOAP
            'exceptions' => true, // Habilita las excepciones en caso de errores SOAP
        ];
    
        try {
            $client = new SoapClient($wsdl, $options);

            echo '<pre>';
            print_r($client);
    
            // Aquí puedes construir el XML SOAP según la estructura requerida por el servicio

            $xml = '<?xml version="1.0" encoding="UTF-8"?>
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header/>
              <soap:Body>
              <rDE xmlns:xsd="http://ekuatia.set.gov.py/sifen/xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://ekuatia.set.gov.py/sifen/xsd siRecepDE_v150.xsd">
                <dVerFor>150</dVerFor>
                <DE Id="018000181303001001000000122023103011010101013">
                    <dDVId>1</dDVId>
                    <dFecFirma>2020-05-01T14:01:50</dFecFirma>
                    <dSisFact>1</dSisFact>
                    <gOpeDE>
                        <iTipEmi>1</iTipEmi>
                        <dDesTipEmi>Normal</dDesTipEmi>
                        <dCodSeg>000000023</dCodSeg>
                        <dInfoEmi>1</dInfoEmi>
                        <dInfoFisc>Información de interés del Fisco respecto al DE</dInfoFisc>
                    </gOpeDE>
                    <gTimb>
                        <iTiDE>1</iTiDE>
                        <dDesTiDE>Factura electrónica</dDesTiDE>
                        <dNumTim>12345678</dNumTim>
                        <dEst>001</dEst>
                        <dPunExp>001</dPunExp>
                        <dNumDoc>1000050</dNumDoc>
                        <dSerieNum>AB</dSerieNum>
                        <dFeIniT>2023-10-30</dFeIniT>
                    </gTimb>
                    <gDatGralOpe>
                        <dFeEmiDE>2023-10-30T15:03:57</dFeEmiDE>
                        <gOpeCom>
                            <iTipTra>2</iTipTra>
                            <dDesTipTra>Prestacion de servicios</dDesTipTra>
                            <iTImp>1</iTImp>
                            <dDesTImp>IVA</dDesTImp>
                            <cMoneOpe>PYG</cMoneOpe>
                            <dDesMoneOpe>Guarani</dDesMoneOpe>
                        </gOpeCom>
                        <gEmis>
                            <dRucEm>80001813</dRucEm>
                            <dDVEmi>3</dDVEmi>
                            <iTipCont>2</iTipCont>
                            <cTipReg>3</cTipReg>
                            <dNomEmi>DESARROLLO TURISTICO PARAGUAYO</dNomEmi>
                            <dDirEmi>CALLE 1 CASI CALLE 2</dDirEmi>
                            <dNumCas>0</dNumCas>
                            <cDepEmi>1</cDepEmi>
                            <dDesDepEmi>CAPITAL</dDesDepEmi>
                            <cCiuEmi>1</cCiuEmi>
                            <dDesCiuEmi>ASUNCION (DISTRITO)</dDesCiuEmi>
                            <dTelEmi>0217297070</dTelEmi>
                            <dEmailE>sylvia@dtp.com.py</dEmailE>
                            <gActEco>
                                <cActEco>96099</cActEco>
                                <dDesActEco>OTRAS ACTIVIDADES DE SERVICIOS PERSONALES N.C.P.</dDesActEco>
                            </gActEco>
                        </gEmis>
                        <gDatRec>
                            <iNatRec>1</iNatRec>
                            <iTiOpe>1</iTiOpe>
                            <cPaisRec>PRY</cPaisRec>
                            <dDesPaisRe>Paraguay</dDesPaisRe>
                            <iTiContRec>2</iTiContRec>
                            <dRucRec>0475871</dRucRec>
                            <dDVRec>4</dDVRec>
                            <dNomRec> GUILLERMO ERNESTO KRAUCH RIERA</dNomRec>
                            <dDirRec>CALLE 1 ENTRE CALLE 2 Y CALLE 3</dDirRec>
                            <dNumCasRec>123</dNumCasRec>
                            <cDepRec>1</cDepRec>
                            <dDesDepRec>CAPITAL</dDesDepRec>
                            <cDisRec>1</cDisRec>
                            <dDesDisRec>ASUNCION (DISTRITO)</dDesDisRec>
                            <cCiuRec>1</cCiuRec>
                            <dDesCiuRec>ASUNCION (DISTRITO)</dDesCiuRec>
                            <dTelRec>012123456</dTelRec>
                            <dCodCliente>57015</dCodCliente>
                        </gDatRec>
                    </gDatGralOpe>
                    <gDtipDE>
                        <gCamFE>
                            <iIndPres>1</iIndPres>
                            <dDesIndPres>Operación presencial</dDesIndPres>
                        </gCamFE>
                        <gCamCond>
                            <iCondOpe>2</iCondOpe>
                            <dDCondOpe>Crédito</dDCondOpe>
                            <gPagCred>
                                <iCondCred>1</iCondCred>
                                <dDCondCred>Plazo</dDCondCred>
                                <dPlazoCre>28</dPlazoCre>
                            </gPagCred>
                        </gCamCond>
                        <gCamItem>
                            <dCodInt>CAC/CTAC</dCodInt>
                            <dDesProSer>CUENTAS ACTIVAS</dDesProSer>
                            <cUniMed>77</cUniMed>
                            <dDesUniMed>UNI</dDesUniMed>
                            <dCantProSer>1</dCantProSer>
                            <dInfItem>21</dInfItem>
                            <gValorItem>
                                <dPUniProSer>1100000</dPUniProSer>
                                <dTotBruOpeItem>1100000</dTotBruOpeItem>
                                <gValorRestaItem>
                                    <dDescItem>0</dDescItem>
                                    <dPorcDesIt>0</dPorcDesIt>
                                    <dDescGloItem>0</dDescGloItem>
                                    <dTotOpeItem>1100000</dTotOpeItem>
                                </gValorRestaItem>
                            </gValorItem>
                            <gCamIVA>
                                <iAfecIVA>1</iAfecIVA>
                                <dDesAfecIVA>Gravado IVA</dDesAfecIVA>
                                <dPropIVA>100</dPropIVA>
                                <dTasaIVA>10</dTasaIVA>
                                <dBasGravIVA>1000000</dBasGravIVA>
                                <dLiqIVAItem>100000</dLiqIVAItem>
                            </gCamIVA>
                        </gCamItem>
                        <gCamItem>
                            <dCodInt>TAU/TAUR</dCodInt>
                            <dDesProSer>TARJETA URGENTE</dDesProSer>
                            <cUniMed>77</cUniMed>
                            <dDesUniMed>UNI</dDesUniMed>
                            <dCantProSer>1</dCantProSer>
                            <gValorItem>
                                <dPUniProSer>1100000</dPUniProSer>
                                <dTotBruOpeItem>1100000</dTotBruOpeItem>
                                <gValorRestaItem>
                                    <dDescItem>0</dDescItem>
                                    <dPorcDesIt>0</dPorcDesIt>
                                    <dDescGloItem>0</dDescGloItem>
                                    <dTotOpeItem>1100000</dTotOpeItem>
                                </gValorRestaItem>
                            </gValorItem>
                            <gCamIVA>
                                <iAfecIVA>1</iAfecIVA>
                                <dDesAfecIVA>Gravado IVA</dDesAfecIVA>
                                <dPropIVA>100</dPropIVA>
                                <dTasaIVA>10</dTasaIVA>
                                <dBasGravIVA>1000000</dBasGravIVA>
                                <dLiqIVAItem>100000</dLiqIVAItem>
                            </gCamIVA>
                        </gCamItem>
                        <gCamEsp>
                            <gGrupAdi>
                                <dCiclo>DICIEMBRE</dCiclo>
                                <dFecIniC>2018-11-23</dFecIniC>
                                <dFecFinC>2018-12-17</dFecFinC>
                            </gGrupAdi>
                        </gCamEsp>
                        <gTransp>
                            <iModTrans>1</iModTrans>
                            <dDesModTrans>Terrestre</dDesModTrans>
                            <iRespFlete>2</iRespFlete>
                            <dNuDespImp>1234567891asfdcs</dNuDespImp>
                        </gTransp>
                    </gDtipDE>
                    <gTotSub>
                        <dSubExe>0</dSubExe>
                        <dSubExo>0</dSubExo>
                        <dSub5>0</dSub5>
                        <dSub10>2200000</dSub10>
                        <dTotOpe>2200000</dTotOpe>
                        <dTotDesc>0</dTotDesc>
                        <dTotDescGlotem>0</dTotDescGlotem>
                        <dTotAntItem>0</dTotAntItem>
                        <dTotAnt>0</dTotAnt>
                        <dPorcDescTotal>0</dPorcDescTotal>
                        <dDescTotal>0.0</dDescTotal>
                        <dAnticipo>0</dAnticipo>
                        <dRedon>0.0</dRedon>
                        <dTotGralOpe>2200000</dTotGralOpe>
                        <dIVA5>0</dIVA5>
                        <dIVA10>200000</dIVA10>
                        <dTotIVA>200000</dTotIVA>
                        <dBaseGrav5>0</dBaseGrav5>
                        <dBaseGrav10>2000000</dBaseGrav10>
                        <dTBasGraIVA>2000000</dTBasGraIVA>
                    </gTotSub>
                </DE>
                 <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
                                    <SignedInfo>
                                        <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>
                                        <SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
                                        <Reference URI="#018000181303001001000000122023103011010101013">
                                            <Transforms>
                                                <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                                                <Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                                            </Transforms>
                                            <DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
                                            <DigestValue>b9e59aa7a26a771d2158c2fdec026739eb6eca17</DigestValue>
                                        </Reference>
                                    </SignedInfo>
                                    <SignatureValue>
                                            HoKJGuULmssmT1Lfft538hpuq7yHH+m+g7btVxsaFs5IDBNYPHQUNP1hOjo8+N8uVRAG42DLGpPiFPdP3sZIVqVOm/OGwqB0VBxjg6rkWE6s9MvtG/iCsASo3MZgGdDnUCawmOOpeCIMEOFD0cSodUX9mrFeRJq8BZMp6aqKNnRYb/zOC2jX8v7KRwV9QHl+1qEOH/VN4JH2cVHEoekLSmU4KaZkURaAAJdXGy4G4Z9WJ7+d+FudcAv621TXXJXUey71Vqn6HO17evhzAtK/xdOybqBVw/9gLf+Rxe1skm/RI5AYTEwNIPWCWmbsTVIyCWNXFWJYTOdk3WE9ZTZ93g==
                                    </SignatureValue>
                                    <KeyInfo>
                                        <X509Data>
                                            <X509Certificate>
                                                MIIIFDCCBfygAwIBAgIQbVrh2RkwO4pEQ3K6GZ1KATANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMCUFkxDTALBgNVBAoTBElDUFAxODA2BgNVBAsTL1ByZXN0YWRvciBDdWFsaWZpY2FkbyBkZSBTZXJ2aWNpb3MgZGUgQ29uZmlhbnphMRUwEwYDVQQDEwxDT0RFMTAwIFMuQS4xFjAUBgNVBAUTDVJVQzgwMDgwNjEwLTcwHhcNMjMxMDIwMTIyNTM2WhcNMjQxMDIwMTIyNTM2WjCBuTELMAkGA1UEBhMCUFkxKzApBgNVBAoTIkNFUlRJRklDQURPIENVQUxJRklDQURPIFRSSUJVVEFSSU8xCzAJBgNVBAsTAkYxMRswGQYDVQQEExJGRVJOQU5ERVogREUgUFJPTk8xFTATBgNVBCoTDFNZTFZJQSBNQVJJQTEpMCcGA1UEAxMgU1lMVklBIE1BUklBICBGRVJOQU5ERVogREUgUFJPTk8xETAPBgNVBAUTCENJNjkwNjgzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxpqKtNBE2RjY/G70++qeRK2J0jUyaoloAk4K8b2r8O7xODIXsjA35wVeI9OAkbQd1FMAylO3R/b8Y8xxIo74mh7sueOPEj1f0wS3A18OvqTjkmVMGiMDj/ZY9B/MNJk9aFuam1apdpxQJN8F0J7PEgckJ3OTIsRtNbWyPgioAZB7j7hXVIsh/CWrZWM3Wv0lx1Ccd40o+6CR82ZKqPD4rlxfnldbvXZpfL0A+pqPUYpxG4NQywngmiwncn0Ku1F1vUbOIX/Q1x7tJiSzxm2fGNfobu6Zgfe/z5Fs9aLugcNliExqbW4b2kjJQSLj8L1093915fRofKuP9obl9ptOgQIDAQABo4IDSDCCA0QwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUehL5J8PdsburoOhRNy39wE1EAXQwHwYDVR0jBBgwFoAUvjVUYmhg5ybTMcFfl7Hi9mTOB/UwDgYDVR0PAQH/BAQDAgXgMIG+BgNVHREEgbYwgbOBEVNZTFZJQUBEVFAuQ09NLlBZpIGdMIGaMSowKAYDVQQKEyFERVNBUk9MTE8gVFVSSVNUSUNPIFBBUkFHVUFZTyBTLkExEzARBgNVBAsTCkRJUkVDVE9SSU8xEzARBgNVBAwTClBSRVNJREVOVEUxKjAoBgNVBA0MIUZJUk1BIEVMRUNUUsOTTklDQSBkZSBuaXZlbCBtZWRpbzEWMBQGA1UEBRMNUlVDODAwMDE4MTMtMzCB9wYDVR0gBIHvMIHsMIHpBgsrBgEEAYOucAEBBDCB2TBGBggrBgEFBQcCARY6aHR0cHM6Ly9jb2RlMTAwLmNvbS5weS9yZXBvc2l0b3Jpby1kZS1kb2N1bWVudG9zLXB1YmxpY29zLzCBjgYIKwYBBQUHAgIwgYEMf2NlcnRpZmljYWRvIGN1YWxpZmljYWRvIGRlIGZpcm1hIGVsZWN0csOzbmljYSB0aXBvIEYxIHN1amV0YSBhIGxhcyBjb25kaWNpb25lcyBkZSB1c28gZXhwdWVzdGFzIGVuIGxhIERQQyBkZWwgUENTQyBDT0RFMTAwIFMuQS4wewYDVR0fBHQwcjA3oDWgM4YxaHR0cDovL3BjYTEuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDA3oDWgM4YxaHR0cDovL3BjYTIuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDAgBgNVHSUBAf8EFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwgYkGCCsGAQUFBwEBBH0wezA5BggrBgEFBQcwAYYtaHR0cDovL29jc3AuY29kZTEwMC5jb20ucHkvb2NzcC9jYS1jb2RlMTAwLXNhMD4GCCsGAQUFBzAChjJodHRwOi8vcGNhMS5jb2RlMTAwLmNvbS5weS9jZXJ0cy9jYS1jb2RlMTAwLXNhLmNlcjANBgkqhkiG9w0BAQsFAAOCAgEA07RWl+uU/gabJ9Sez7t7QI/CNKfUAZRA449ydVLK7iD6CqlWmQiswT/jbP3/XPgkJmvWYAHAwb0MyhRECWfk1A6BJs/ZAv6YIN4N7OlHM9lxsJIvbymumyUS6jrWgS1lLudCduwq+rp55KRPi7pohIowErIV1Rr0RqvPMCibili+ljXRGVbla2ZOLTykVcs03K2valyZFtIyzqfH/MbDOlDplvikUzcB2Ht4u2wh4Ip59e8IVdB8bcOgfgUPnf3Xh0Msi9oyI5AJI+8HaKsQhCqbJWdBqiwLeutzhOcuqwQt4ojeLKHIcddV3HKVNYi0okNs0zy/rRH+CHNEUk2z5Ovq0IimSxOx/Y7F0XyjSd/2rbucn4RZFqPBTpgLX7OawgPW5YjjpayIxiAriEZBN1o8s1e9R59KneYXGPW9ufeIfC8YCGk99TutEfLq/K+Suv3/fdKxuXyxnF3g4mP9INapJl2AmmQ9jVYFSk+3hLPOgspZs0J5/TzvEiZ4+aoD9lpY7CNH2vAGXgw+R4FEWPCGF/1aklL4oJvkwjkx1Kq1IWBfZteHFQQDiTzVGruEQG8ngw304LkJQCHVuyEEpBL5ga7zOft3tLZ163fPqU7Bk5IK0kU8Gy9Uyp/Rwotk/uvVLxunyHzWqEU6TwWOS4FQnRGkG/842DowdnSmu3E=/G70++qeRK2J0jUyaoloAk4K8b2r8O7xODIXsjA35wVeI9OAkbQd1FMAylO3R/b8Y8xxIo74mh7sueOPEj1f0wS3A18OvqTjkmVMGiMDj/ZY9B/MNJk9aFuam1apdpxQJN8F0J7PEgckJ3OTIsRtNbWyPgioAZB7j7hXVIsh/CWrZWM3Wv0lx1Ccd40o+6CR82ZKqPD4rlxfnldbvXZpfL0A+pqPUYpxG4NQywngmiwncn0Ku1F1vUbOIX/Q1x7tJiSzxm2fGNfobu6Zgfe/z5Fs9aLugcNliExqbW4b2kjJQSLj8L1093915fRofKuP9obl9ptOgQIDAQABo4IDSDCCA0QwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUehL5J8PdsburoOhRNy39wE1EAXQwHwYDVR0jBBgwFoAUvjVUYmhg5ybTMcFfl7Hi9mTOB/UwDgYDVR0PAQH/BAQDAgXgMIG+BgNVHREEgbYwgbOBEVNZTFZJQUBEVFAuQ09NLlBZpIGdMIGaMSowKAYDVQQKEyFERVNBUk9MTE8gVFVSSVNUSUNPIFBBUkFHVUFZTyBTLkExEzARBgNVBAsTCkRJUkVDVE9SSU8xEzARBgNVBAwTClBSRVNJREVOVEUxKjAoBgNVBA0MIUZJUk1BIEVMRUNUUsOTTklDQSBkZSBuaXZlbCBtZWRpbzEWMBQGA1UEBRMNUlVDODAwMDE4MTMtMzCB9wYDVR0gBIHvMIHsMIHpBgsrBgEEAYOucAEBBDCB2TBGBggrBgEFBQcCARY6aHR0cHM6Ly9jb2RlMTAwLmNvbS5weS9yZXBvc2l0b3Jpby1kZS1kb2N1bWVudG9zLXB1YmxpY29zLzCBjgYIKwYBBQUHAgIwgYEMf2NlcnRpZmljYWRvIGN1YWxpZmljYWRvIGRlIGZpcm1hIGVsZWN0csOzbmljYSB0aXBvIEYxIHN1amV0YSBhIGxhcyBjb25kaWNpb25lcyBkZSB1c28gZXhwdWVzdGFzIGVuIGxhIERQQyBkZWwgUENTQyBDT0RFMTAwIFMuQS4wewYDVR0fBHQwcjA3oDWgM4YxaHR0cDovL3BjYTEuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDA3oDWgM4YxaHR0cDovL3BjYTIuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDAgBgNVHSUBAf8EFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwgYkGCCsGAQUFBwEBBH0wezA5BggrBgEFBQcwAYYtaHR0cDovL29jc3AuY29kZTEwMC5jb20ucHkvb2NzcC9jYS1jb2RlMTAwLXNhMD4GCCsGAQUFBzAChjJodHRwOi8vcGNhMS5jb2RlMTAwLmNvbS5weS9jZXJ0cy9jYS1jb2RlMTAwLXNhLmNlcjANBgkqhkiG9w0BAQsFAAOCAgEA07RWl+uU/gabJ9Sez7t7QI/CNKfUAZRA449ydVLK7iD6CqlWmQiswT/jbP3/XPgkJmvWYAHAwb0MyhRECWfk1A6BJs/ZAv6YIN4N7OlHM9lxsJIvbymumyUS6jrWgS1lLudCduwq+rp55KRPi7pohIowErIV1Rr0RqvPMCibili+ljXRGVbla2ZOLTykVcs03K2valyZFtIyzqfH/MbDOlDplvikUzcB2Ht4u2wh4Ip59e8IVdB8bcOgfgUPnf3Xh0Msi9oyI5AJI+8HaKsQhCqbJWdBqiwLeutzhOcuqwQt4ojeLKHIcddV3HKVNYi0okNs0zy/rRH+CHNEUk2z5Ovq0IimSxOx/Y7F0XyjSd/2rbucn4RZFqPBTpgLX7OawgPW5YjjpayIxiAriEZBN1o8s1e9R59KneYXGPW9ufeIfC8YCGk99TutEfLq/K+Suv3/fdKxuXyxnF3g4mP9INapJl2AmmQ9jVYFSk+3hLPOgspZs0J5/TzvEiZ4+aoD9lpY7CNH2vAGXgw+R4FEWPCGF/1aklL4oJvkwjkx1Kq1IWBfZteHFQQDiTzVGruEQG8ngw304LkJQCHVuyEEpBL5ga7zOft3tLZ163fPqU7Bk5IK0kU8Gy9Uyp/Rwotk/uvVLxunyHzWqEU6TwWOS4FQnRGkG/842DowdnSmu3E=MIIIFDCCBfygAwIBAgIQbVrh2RkwO4pEQ3K6GZ1KATANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMCUFkxDTALBgNVBAoTBElDUFAxODA2BgNVBAsTL1ByZXN0YWRvciBDdWFsaWZpY2FkbyBkZSBTZXJ2aWNpb3MgZGUgQ29uZmlhbnphMRUwEwYDVQQDEwxDT0RFMTAwIFMuQS4xFjAUBgNVBAUTDVJVQzgwMDgwNjEwLTcwHhcNMjMxMDIwMTIyNTM2WhcNMjQxMDIwMTIyNTM2WjCBuTELMAkGA1UEBhMCUFkxKzApBgNVBAoTIkNFUlRJRklDQURPIENVQUxJRklDQURPIFRSSUJVVEFSSU8xCzAJBgNVBAsTAkYxMRswGQYDVQQEExJGRVJOQU5ERVogREUgUFJPTk8xFTATBgNVBCoTDFNZTFZJQSBNQVJJQTEpMCcGA1UEAxMgU1lMVklBIE1BUklBICBGRVJOQU5ERVogREUgUFJPTk8xETAPBgNVBAUTCENJNjkwNjgzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxpqKtNBE2RjY/G70++qeRK2J0jUyaoloAk4K8b2r8O7xODIXsjA35wVeI9OAkbQd1FMAylO3R/b8Y8xxIo74mh7sueOPEj1f0wS3A18OvqTjkmVMGiMDj/ZY9B/MNJk9aFuam1apdpxQJN8F0J7PEgckJ3OTIsRtNbWyPgioAZB7j7hXVIsh/CWrZWM3Wv0lx1Ccd40o+6CR82ZKqPD4rlxfnldbvXZpfL0A+pqPUYpxG4NQywngmiwncn0Ku1F1vUbOIX/Q1x7tJiSzxm2fGNfobu6Zgfe/z5Fs9aLugcNliExqbW4b2kjJQSLj8L1093915fRofKuP9obl9ptOgQIDAQABo4IDSDCCA0QwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUehL5J8PdsburoOhRNy39wE1EAXQwHwYDVR0jBBgwFoAUvjVUYmhg5ybTMcFfl7Hi9mTOB/UwDgYDVR0PAQH/BAQDAgXgMIG+BgNVHREEgbYwgbOBEVNZTFZJQUBEVFAuQ09NLlBZpIGdMIGaMSowKAYDVQQKEyFERVNBUk9MTE8gVFVSSVNUSUNPIFBBUkFHVUFZTyBTLkExEzARBgNVBAsTCkRJUkVDVE9SSU8xEzARBgNVBAwTClBSRVNJREVOVEUxKjAoBgNVBA0MIUZJUk1BIEVMRUNUUsOTTklDQSBkZSBuaXZlbCBtZWRpbzEWMBQGA1UEBRMNUlVDODAwMDE4MTMtMzCB9wYDVR0gBIHvMIHsMIHpBgsrBgEEAYOucAEBBDCB2TBGBggrBgEFBQcCARY6aHR0cHM6Ly9jb2RlMTAwLmNvbS5weS9yZXBvc2l0b3Jpby1kZS1kb2N1bWVudG9zLXB1YmxpY29zLzCBjgYIKwYBBQUHAgIwgYEMf2NlcnRpZmljYWRvIGN1YWxpZmljYWRvIGRlIGZpcm1hIGVsZWN0csOzbmljYSB0aXBvIEYxIHN1amV0YSBhIGxhcyBjb25kaWNpb25lcyBkZSB1c28gZXhwdWVzdGFzIGVuIGxhIERQQyBkZWwgUENTQyBDT0RFMTAwIFMuQS4wewYDVR0fBHQwcjA3oDWgM4YxaHR0cDovL3BjYTEuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDA3oDWgM4YxaHR0cDovL3BjYTIuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDAgBgNVHSUBAf8EFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwgYkGCCsGAQUFBwEBBH0wezA5BggrBgEFBQcwAYYtaHR0cDovL29jc3AuY29kZTEwMC5jb20ucHkvb2NzcC9jYS1jb2RlMTAwLXNhMD4GCCsGAQUFBzAChjJodHRwOi8vcGNhMS5jb2RlMTAwLmNvbS5weS9jZXJ0cy9jYS1jb2RlMTAwLXNhLmNlcjANBgkqhkiG9w0BAQsFAAOCAgEA07RWl+uU/gabJ9Sez7t7QI/CNKfUAZRA449ydVLK7iD6CqlWmQiswT/jbP3/XPgkJmvWYAHAwb0MyhRECWfk1A6BJs/ZAv6YIN4N7OlHM9lxsJIvbymumyUS6jrWgS1lLudCduwq+rp55KRPi7pohIowErIV1Rr0RqvPMCibili+ljXRGVbla2ZOLTykVcs03K2valyZFtIyzqfH/MbDOlDplvikUzcB2Ht4u2wh4Ip59e8IVdB8bcOgfgUPnf3Xh0Msi9oyI5AJI+8HaKsQhCqbJWdBqiwLeutzhOcuqwQt4ojeLKHIcddV3HKVNYi0okNs0zy/rRH+CHNEUk2z5Ovq0IimSxOx/Y7F0XyjSd/2rbucn4RZFqPBTpgLX7OawgPW5YjjpayIxiAriEZBN1o8s1e9R59KneYXGPW9ufeIfC8YCGk99TutEfLq/K+Suv3/fdKxuXyxnF3g4mP9INapJl2AmmQ9jVYFSk+3hLPOgspZs0J5/TzvEiZ4+aoD9lpY7CNH2vAGXgw+R4FEWPCGF/1aklL4oJvkwjkx1Kq1IWBfZteHFQQDiTzVGruEQG8ngw304LkJQCHVuyEEpBL5ga7zOft3tLZ163fPqU7Bk5IK0kU8Gy9Uyp/Rwotk/uvVLxunyHzWqEU6TwWOS4FQnRGkG/842DowdnSmu3E=
                                            </X509Certificate>
                                            <X509IssuerSerial>
                                                <X509IssuerName>C=PY,O=ICPP,CN=CODE100 S.A.,2.5.4.5=#145357739000291486709356561931893033473
                                                </X509IssuerName>
                                                <X509SerialNumber>145357739000291486709356561931893033473</X509SerialNumber>
                                            </X509IssuerSerial>
                                        </X509Data>
                                    </KeyInfo>
                                </Signature>
                                <gCamFuFD>
                                    <dCarQR>https://ekuatia.set.gov.py/consultas-test/qr?nVersion=150&Id=018000181303001001000000122023103011010101013&dFeEmiDE=323031392d30342d30395431313a30303a3032&dRucRec=80002201&dTotGralOpe=0&dTotIVA=0&cItems=1&DigestValue=754c7a535a2b426930312f4431514d7a7a7130354d49466a43305450474f704a72626d4d4439616e5948773d&IdCSC=0001&cHashQR=bfeb83a236fda8bd654f547a1e28bc218912d3f47c62962d1f6b6d5e49053590</dCarQR>                                  
                                </gCamFuFD>
            </rDE>
            </soap:Body>
            </soap:Envelope>
            ';
            // Realiza la llamada al método del servicio SOAP
            $response = $client->__doRequest($xml, $wsdl, '', SOAP_1_1);
    
            // Aquí puedes procesar la respuesta SOAP
            // Por ejemplo, puedes convertir la respuesta XML en un objeto SimpleXMLElement
            $responseXml = simplexml_load_string($response);
            
            // Retorna la respuesta o realiza otras acciones según sea necesario
            return $responseXml;
        } catch (SoapFault $e) {
            // Maneja las excepciones SOAP
            return $e->getMessage();
        }
        
    }

    public function envioFecssa(Request $request)
    {
    $url = 'https://sifen-test.set.gov.py/de/ws/sync/recibe.wsd?wsdl';

    // Construir el cuerpo del mensaje SOAP
         $soap_request = '
         <?xml version="1.0" encoding="UTF-8"?>
         <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
           <soap:Header/>
           <soap:Body>
           <rDE xmlns:xsd="http://ekuatia.set.gov.py/sifen/xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://ekuatia.set.gov.py/sifen/xsd/siRecepDE_v150.xsd">
             <dVerFor>150</dVerFor>
             <DE Id="018000181303001001000000122023103011010101013">
                 <dDVId>1</dDVId>
                 <dFecFirma>2023-01-01T16:43:50</dFecFirma>
                 <dSisFact>1</dSisFact>
                 <gOpeDE>
                     <iTipEmi>1</iTipEmi>
                     <dDesTipEmi>Normal</dDesTipEmi>
                     <dCodSeg>000000023</dCodSeg>
                     <dInfoEmi>1</dInfoEmi>
                     <dInfoFisc>Información de interés del Fisco respecto al DE</dInfoFisc>
                 </gOpeDE>
                 <gTimb>
                     <iTiDE>1</iTiDE>
                     <dDesTiDE>Factura electrónica</dDesTiDE>
                     <dNumTim>12345678</dNumTim>
                     <dEst>001</dEst>
                     <dPunExp>001</dPunExp>
                     <dNumDoc>1000050</dNumDoc>
                     <dSerieNum>AB</dSerieNum>
                     <dFeIniT>2023-10-30</dFeIniT>
                 </gTimb>
                 <gDatGralOpe>
                     <dFeEmiDE>2023-01-01T16:43:50</dFeEmiDE>
                     <gOpeCom>
                         <iTipTra>2</iTipTra>
                         <dDesTipTra>Prestacion de servicios</dDesTipTra>
                         <iTImp>1</iTImp>
                         <dDesTImp>IVA</dDesTImp>
                         <cMoneOpe>PYG</cMoneOpe>
                         <dDesMoneOpe>Guarani</dDesMoneOpe>
                     </gOpeCom>
                     <gEmis>
                         <dRucEm>80001813</dRucEm>
                         <dDVEmi>3</dDVEmi>
                         <iTipCont>2</iTipCont>
                         <cTipReg>3</cTipReg>
                         <dNomEmi>DESARROLLO TURISTICO PARAGUAYO</dNomEmi>
                         <dDirEmi>CALLE 1 CASI CALLE 2</dDirEmi>
                         <dNumCas>0</dNumCas>
                         <cDepEmi>1</cDepEmi>
                         <dDesDepEmi>CAPITAL</dDesDepEmi>
                         <cCiuEmi>1</cCiuEmi>
                         <dDesCiuEmi>ASUNCION (DISTRITO)</dDesCiuEmi>
                         <dTelEmi>0217297070</dTelEmi>
                         <dEmailE>sylvia@dtp.com.py</dEmailE>
                         <gActEco>
                             <cActEco>96099</cActEco>
                             <dDesActEco>OTRAS ACTIVIDADES DE SERVICIOS PERSONALES N.C.P.</dDesActEco>
                         </gActEco>
                     </gEmis>
                     <gDatRec>
                         <iNatRec>1</iNatRec>
                         <iTiOpe>1</iTiOpe>
                         <cPaisRec>PRY</cPaisRec>
                         <dDesPaisRe>Paraguay</dDesPaisRe>
                         <iTiContRec>2</iTiContRec>
                         <dRucRec>0475871</dRucRec>
                         <dDVRec>4</dDVRec>
                         <dNomRec> GUILLERMO ERNESTO KRAUCH RIERA</dNomRec>
                         <dDirRec>CALLE 1 ENTRE CALLE 2 Y CALLE 3</dDirRec>
                         <dNumCasRec>123</dNumCasRec>
                         <cDepRec>1</cDepRec>
                         <dDesDepRec>CAPITAL</dDesDepRec>
                         <cDisRec>1</cDisRec>
                         <dDesDisRec>ASUNCION (DISTRITO)</dDesDisRec>
                         <cCiuRec>1</cCiuRec>
                         <dDesCiuRec>ASUNCION (DISTRITO)</dDesCiuRec>
                         <dTelRec>012123456</dTelRec>
                         <dCodCliente>57015</dCodCliente>
                     </gDatRec>
                 </gDatGralOpe>
                 <gDtipDE>
                     <gCamFE>
                         <iIndPres>1</iIndPres>
                         <dDesIndPres>Operación presencial</dDesIndPres>
                     </gCamFE>
                     <gCamCond>
                         <iCondOpe>2</iCondOpe>
                         <dDCondOpe>Crédito</dDCondOpe>
                         <gPagCred>
                             <iCondCred>1</iCondCred>
                             <dDCondCred>Plazo</dDCondCred>
                             <dPlazoCre>28</dPlazoCre>
                         </gPagCred>
                     </gCamCond>
                     <gCamItem>
                         <dCodInt>CAC/CTAC</dCodInt>
                         <dDesProSer>CUENTAS ACTIVAS</dDesProSer>
                         <cUniMed>77</cUniMed>
                         <dDesUniMed>UNI</dDesUniMed>
                         <dCantProSer>1</dCantProSer>
                         <dInfItem>21</dInfItem>
                         <gValorItem>
                             <dPUniProSer>1100000</dPUniProSer>
                             <dTotBruOpeItem>1100000</dTotBruOpeItem>
                             <gValorRestaItem>
                                 <dDescItem>0</dDescItem>
                                 <dPorcDesIt>0</dPorcDesIt>
                                 <dDescGloItem>0</dDescGloItem>
                                 <dTotOpeItem>1100000</dTotOpeItem>
                             </gValorRestaItem>
                         </gValorItem>
                         <gCamIVA>
                             <iAfecIVA>1</iAfecIVA>
                             <dDesAfecIVA>Gravado IVA</dDesAfecIVA>
                             <dPropIVA>100</dPropIVA>
                             <dTasaIVA>10</dTasaIVA>
                             <dBasGravIVA>1000000</dBasGravIVA>
                             <dLiqIVAItem>100000</dLiqIVAItem>
                         </gCamIVA>
                     </gCamItem>
                     <gCamItem>
                         <dCodInt>TAU/TAUR</dCodInt>
                         <dDesProSer>TARJETA URGENTE</dDesProSer>
                         <cUniMed>77</cUniMed>
                         <dDesUniMed>UNI</dDesUniMed>
                         <dCantProSer>1</dCantProSer>
                         <gValorItem>
                             <dPUniProSer>1100000</dPUniProSer>
                             <dTotBruOpeItem>1100000</dTotBruOpeItem>
                             <gValorRestaItem>
                                 <dDescItem>0</dDescItem>
                                 <dPorcDesIt>0</dPorcDesIt>
                                 <dDescGloItem>0</dDescGloItem>
                                 <dTotOpeItem>1100000</dTotOpeItem>
                             </gValorRestaItem>
                         </gValorItem>
                         <gCamIVA>
                             <iAfecIVA>1</iAfecIVA>
                             <dDesAfecIVA>Gravado IVA</dDesAfecIVA>
                             <dPropIVA>100</dPropIVA>
                             <dTasaIVA>10</dTasaIVA>
                             <dBasGravIVA>1000000</dBasGravIVA>
                             <dLiqIVAItem>100000</dLiqIVAItem>
                         </gCamIVA>
                     </gCamItem>
                     <gCamEsp>
                         <gGrupAdi>
                             <dCiclo>DICIEMBRE</dCiclo>
                             <dFecIniC>2022-11-23</dFecIniC>
                             <dFecFinC>2023-12-17</dFecFinC>
                         </gGrupAdi>
                     </gCamEsp>
                     <gTransp>
                         <iModTrans>1</iModTrans>
                         <dDesModTrans>Terrestre</dDesModTrans>
                         <iRespFlete>2</iRespFlete>
                         <dNuDespImp>1234567891asfdcs</dNuDespImp>
                     </gTransp>
                 </gDtipDE>
                 <gTotSub>
                     <dSubExe>0</dSubExe>
                     <dSubExo>0</dSubExo>
                     <dSub5>0</dSub5>
                     <dSub10>2200000</dSub10>
                     <dTotOpe>2200000</dTotOpe>
                     <dTotDesc>0</dTotDesc>
                     <dTotDescGlotem>0</dTotDescGlotem>
                     <dTotAntItem>0</dTotAntItem>
                     <dTotAnt>0</dTotAnt>
                     <dPorcDescTotal>0</dPorcDescTotal>
                     <dDescTotal>0.0</dDescTotal>
                     <dAnticipo>0</dAnticipo>
                     <dRedon>0.0</dRedon>
                     <dTotGralOpe>2200000</dTotGralOpe>
                     <dIVA5>0</dIVA5>
                     <dIVA10>200000</dIVA10>
                     <dTotIVA>200000</dTotIVA>
                     <dBaseGrav5>0</dBaseGrav5>
                     <dBaseGrav10>2000000</dBaseGrav10>
                     <dTBasGraIVA>2000000</dTBasGraIVA>
                 </gTotSub>
             </DE>
             <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
                 <SignedInfo>
                     <CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                     <SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
                         <Reference URI="#018000181303001001000000122023103011010101013">
                             <Transforms>
                                 <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                                 <Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                             </Transforms>
                            <DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
                             <DigestValue>b9e59aa7a26a771d2158c2fdec026739eb6eca17</DigestValue>
                         </Reference>
                 </SignedInfo>
                 <SignatureValue>
                     LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUV2Z0lCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktnd2dnU2tBZ0VBQW9JQkFRREdtb3EwMEVUWkdOajgKYnZUNzZwNUVyWW5TTlRKcWlXZ0NUZ3J4dmF2dzd2RTRNaGV5TURmbkJWNGowNENSdEIzVVV3REtVN2RIOXZ4agp6SEVpanZpYUh1eTU0NDhTUFYvVEJMY0RYdzYrcE9PU1pVd2FJd09QOWxqMEg4dzBtVDFvVzVxYlZxbDJuRkFrCjN3WFFuczhTQnlRbmM1TWl4RzAxdGJJK0NLZ0JrSHVQdUZkVWl5SDhKYXRsWXpkYS9TWEhVSngzalNqN29KSHoKWmtxbzhQaXVYRitlVjF1OWRtbDh2UUQ2bW85UmluRWJnMURMQ2VDYUxDZHlmUXE3VVhXOVJzNGhmOURYSHUwbQpKTFBHYlo4WTEraHU3cG1COTcvUGtXejFvdTZCdzJXSVRHcHRiaHZhU01sQkl1UHd2WFQzZjNYbDlHaDhxNC8yCmh1WDJtMDZCQWdNQkFBRUNnZ0VCQUo3OWZ3Y3JKOEtvYXFVRGFxbjFkQWhFNk8xWVd1a0E0VHZmSUZIUVpzSjEKZHJSbkhhTXZpTUV3QWVJcVltWENUSkw2YzhxRlpGZC9PU1BudWJaUndHRVpXcWVoclpxTGVaZmowV01vaVJSRAp5azhiT3VZOW00OTNSbFVBMDZoVCtOMy9YM0QxMnFveWp0SkZuRkVib1dlc1FkdzNZTlhYYkJvd1ArWDdaZ3dCCkpiZHRzNEdBODg1UkZwYmJOcG1HekdER1Y2WkpSb0FhN1pPbEpRMmNhVUF4VEkvVjRPWjNwTzdEbGdaN2w4LzUKdytwZ1BCQzd4UDVIdWJTYkN4dHN4MXVYNGZiTjNQNTNzelcwUDAwNlpWcmxQakJqcWlaTGo4OW5McFNmd2NacAo3Z2o5L0NDRXh6YmhQTWlmNTRkMUJnSjl6bEdzS3hsYjhiS1Z6TDBKTWNVQ2dZRUEwNGJvVkp2Z3BXYkxFdHZjClJsNjFrRGRYNVY4QnZjcUVLRDIrVXJ2NFVCaVFmSk1XNjJtK3BQckZTcWtSRldYeFNBMkNrcjhyZkNHY1ZsWFIKTUJvVm8xaUlRSENMc0xnOHQzalBKQjF4ZjA4cVBvbUdndmxUUzNQNStpY1Vlc2g3SVlVdkRPb2tCN2R3Zmo3Mgp4cENESGpkcitLT2ROUEVWa1ZadmhLMkkrTThDZ1lFQThGd1BCaXBORS9VQzBldDBJUDJlbVkwTTFNS0QrQVo5CjZSK3JGQURRVWlOU2J4R1ZST20xa3lVdXR0d2E5ZFg4QWpmdzVFdmx5QUduNzUzZGxQTDNCSk9vdGRyKzZQZDQKRHhmbUoxaFVRanI1NDlFZzJ2TjVicFNvSHlGVzVjdG43bHFhWUVSc1R5dFdlaUFFT3J2V05KMjNTd3JlWmE4UQphMm96U0xMZmQ2OENnWUVBbGhSN0ZVVC96L3N1REhSb1JwbCtFeFBoVGtIcGQyS1FBQWxHMTJhODJQK0V6STZFClRZVmFtYTIvZTZpWEVXcWlGZkJYKzR5ZDZ0WGlHYk9MMTZnNlFBcFpxZmJ3ZGZOU1NRN3pVWGwraE1pRGZabVEKRWErYTlNY1Z6dklaYXNDQjlKZU5lWWxMQlJxWE8wdXJSZGxVM29TT0cvemlWcVRURzZxTE4wVDFFU3NDZ1lBRApXeFd1blZxVUk4Q1ZVN2M4V3E0ak1LLzBMSDhSTUc0RG1qaVdQK2lvck91U3Qya2hIQzQzenJZWFFYMXkzemowCmd2bHZCb3cvdlo0dG4wT2Z0OEN4SktxZlYxNWx2RWJGMXl5VWNneERISFd1czVYRTJNOWlOdHNlRENZZGNUaVQKUUZxVmEzOGpuOVJHUWpvczQ4QjYzWlRZakY5TWhpUXhpN3NKa2MzQzZ3S0JnRDhJdnE0NVg4M1JkSGI2WnpQUApuTFBFL0dQZjVMdkdHOEJHMjc4bTEzNlhsOHhiWkhmMnJpTFRzTFFUaXhxdXBUWEJYY0tYTmFiQnFtRkdEczlyCk4vM3VxdS9CK2R1R0JFTDRaQmJJU0pKS3NXNTJ0ak1FR2g0Y3ZIQnRaNFNNSUdMaHIxMDFIT2ppRmdKVXJLYUIKVlJ2NVk5bDVJNkZzSTJIVjNvSjdKbEV4Ci0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0K                                    </SignatureValue>
                     <KeyInfo>
                         <X509Data>
                             <X509Certificate>
                                 MIIIFDCCBfygAwIBAgIQbVrh2RkwO4pEQ3K6GZ1KATANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMCUFkxDTALBgNVBAoTBElDUFAxODA2BgNVBAsTL1ByZXN0YWRvciBDdWFsaWZpY2FkbyBkZSBTZXJ2aWNpb3MgZGUgQ29uZmlhbnphMRUwEwYDVQQDEwxDT0RFMTAwIFMuQS4xFjAUBgNVBAUTDVJVQzgwMDgwNjEwLTcwHhcNMjMxMDIwMTIyNTM2WhcNMjQxMDIwMTIyNTM2WjCBuTELMAkGA1UEBhMCUFkxKzApBgNVBAoTIkNFUlRJRklDQURPIENVQUxJRklDQURPIFRSSUJVVEFSSU8xCzAJBgNVBAsTAkYxMRswGQYDVQQEExJGRVJOQU5ERVogREUgUFJPTk8xFTATBgNVBCoTDFNZTFZJQSBNQVJJQTEpMCcGA1UEAxMgU1lMVklBIE1BUklBICBGRVJOQU5ERVogREUgUFJPTk8xETAPBgNVBAUTCENJNjkwNjgzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxpqKtNBE2RjY/G70++qeRK2J0jUyaoloAk4K8b2r8O7xODIXsjA35wVeI9OAkbQd1FMAylO3R/b8Y8xxIo74mh7sueOPEj1f0wS3A18OvqTjkmVMGiMDj/ZY9B/MNJk9aFuam1apdpxQJN8F0J7PEgckJ3OTIsRtNbWyPgioAZB7j7hXVIsh/CWrZWM3Wv0lx1Ccd40o+6CR82ZKqPD4rlxfnldbvXZpfL0A+pqPUYpxG4NQywngmiwncn0Ku1F1vUbOIX/Q1x7tJiSzxm2fGNfobu6Zgfe/z5Fs9aLugcNliExqbW4b2kjJQSLj8L1093915fRofKuP9obl9ptOgQIDAQABo4IDSDCCA0QwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUehL5J8PdsburoOhRNy39wE1EAXQwHwYDVR0jBBgwFoAUvjVUYmhg5ybTMcFfl7Hi9mTOB/UwDgYDVR0PAQH/BAQDAgXgMIG+BgNVHREEgbYwgbOBEVNZTFZJQUBEVFAuQ09NLlBZpIGdMIGaMSowKAYDVQQKEyFERVNBUk9MTE8gVFVSSVNUSUNPIFBBUkFHVUFZTyBTLkExEzARBgNVBAsTCkRJUkVDVE9SSU8xEzARBgNVBAwTClBSRVNJREVOVEUxKjAoBgNVBA0MIUZJUk1BIEVMRUNUUsOTTklDQSBkZSBuaXZlbCBtZWRpbzEWMBQGA1UEBRMNUlVDODAwMDE4MTMtMzCB9wYDVR0gBIHvMIHsMIHpBgsrBgEEAYOucAEBBDCB2TBGBggrBgEFBQcCARY6aHR0cHM6Ly9jb2RlMTAwLmNvbS5weS9yZXBvc2l0b3Jpby1kZS1kb2N1bWVudG9zLXB1YmxpY29zLzCBjgYIKwYBBQUHAgIwgYEMf2NlcnRpZmljYWRvIGN1YWxpZmljYWRvIGRlIGZpcm1hIGVsZWN0csOzbmljYSB0aXBvIEYxIHN1amV0YSBhIGxhcyBjb25kaWNpb25lcyBkZSB1c28gZXhwdWVzdGFzIGVuIGxhIERQQyBkZWwgUENTQyBDT0RFMTAwIFMuQS4wewYDVR0fBHQwcjA3oDWgM4YxaHR0cDovL3BjYTEuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDA3oDWgM4YxaHR0cDovL3BjYTIuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDAgBgNVHSUBAf8EFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwgYkGCCsGAQUFBwEBBH0wezA5BggrBgEFBQcwAYYtaHR0cDovL29jc3AuY29kZTEwMC5jb20ucHkvb2NzcC9jYS1jb2RlMTAwLXNhMD4GCCsGAQUFBzAChjJodHRwOi8vcGNhMS5jb2RlMTAwLmNvbS5weS9jZXJ0cy9jYS1jb2RlMTAwLXNhLmNlcjANBgkqhkiG9w0BAQsFAAOCAgEA07RWl+uU/gabJ9Sez7t7QI/CNKfUAZRA449ydVLK7iD6CqlWmQiswT/jbP3/XPgkJmvWYAHAwb0MyhRECWfk1A6BJs/ZAv6YIN4N7OlHM9lxsJIvbymumyUS6jrWgS1lLudCduwq+rp55KRPi7pohIowErIV1Rr0RqvPMCibili+ljXRGVbla2ZOLTykVcs03K2valyZFtIyzqfH/MbDOlDplvikUzcB2Ht4u2wh4Ip59e8IVdB8bcOgfgUPnf3Xh0Msi9oyI5AJI+8HaKsQhCqbJWdBqiwLeutzhOcuqwQt4ojeLKHIcddV3HKVNYi0okNs0zy/rRH+CHNEUk2z5Ovq0IimSxOx/Y7F0XyjSd/2rbucn4RZFqPBTpgLX7OawgPW5YjjpayIxiAriEZBN1o8s1e9R59KneYXGPW9ufeIfC8YCGk99TutEfLq/K+Suv3/fdKxuXyxnF3g4mP9INapJl2AmmQ9jVYFSk+3hLPOgspZs0J5/TzvEiZ4+aoD9lpY7CNH2vAGXgw+R4FEWPCGF/1aklL4oJvkwjkx1Kq1IWBfZteHFQQDiTzVGruEQG8ngw304LkJQCHVuyEEpBL5ga7zOft3tLZ163fPqU7Bk5IK0kU8Gy9Uyp/Rwotk/uvVLxunyHzWqEU6TwWOS4FQnRGkG/842DowdnSmu3E=
                             </X509Certificate>
                             <X509IssuerSerial>
                                 <X509IssuerName>C=PY,O=ICPP,CN=CODE100 S.A.,2.5.4.5=#RUC80080610-7</X509IssuerName>
                                     <X509SerialNumber>145357739000291486709356561931893033473</X509SerialNumber>
                             </X509IssuerSerial>
                         </X509Data>
                     </KeyInfo>
             </Signature>
             <gCamFuFD>
                 <dCarQR>https://ekuatia.set.gov.py/consultas-test/qr?nVersion=150&Id=018000181303001001000000122023103011010101013&dFeEmiDE=323032332d30312d30315431363a34333a3530&dRucRec=0475871&dTotGralOpe=2200000&dTotIVA=2200000&cItems=1&DigestValue=62396535396161376132366137373164323135386332666465633032363733396562366563613137&IdCSC=0001&cHashQR=c65899106a62122cc92531a0ccb6b7c2048444979423025e7cae6244a26b9d72</dCarQR>                                  
             </gCamFuFD>
         </rDE>
         </soap:Body>
         </soap:Envelope>';
        $client = new Client();
        $response = $client->request('POST', $url, [
            'headers' => [
                "Content-type" => "text/xml;charset=\"utf-8\"",
                "Accept" => "text/xml",
                "Cache-Control" => "no-cache",
                "Pragma" => "no-cache",
                "SOAPAction" => "de/ws/sync", 
                "Content-length" => strlen($soap_request),
            ],
            'body' => $soap_request,
        ]);
        
        $body = $response->getBody()->getContents();
    }

    public function envioFeca(Request $request)
    {

            $client = new Client();

//ESTE ES EL CODIGOOOOOOOOOO

            // Contenido XML SOAP que deseas enviar
            $xml = '<?xml version="1.0" encoding="UTF-8"?>
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Header/>
              <soap:Body>
              <rDE xmlns:xsd="http://ekuatia.set.gov.py/sifen/xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://ekuatia.set.gov.py/sifen/xsd siRecepDE_v150.xsd">
                <dVerFor>150</dVerFor>
                <DE Id="018000181303001001000000122023103011010101013">
                    <dDVId>1</dDVId>
                    <dFecFirma>2023-01-01T16:43:50</dFecFirma>
                    <dSisFact>1</dSisFact>
                    <gOpeDE>
                        <iTipEmi>1</iTipEmi>
                        <dDesTipEmi>Normal</dDesTipEmi>
                        <dCodSeg>000000023</dCodSeg>
                        <dInfoEmi>1</dInfoEmi>
                        <dInfoFisc>Información de interés del Fisco respecto al DE</dInfoFisc>
                    </gOpeDE>
                    <gTimb>
                        <iTiDE>1</iTiDE>
                        <dDesTiDE>Factura electrónica</dDesTiDE>
                        <dNumTim>12345678</dNumTim>
                        <dEst>001</dEst>
                        <dPunExp>001</dPunExp>
                        <dNumDoc>1000050</dNumDoc>
                        <dSerieNum>AB</dSerieNum>
                        <dFeIniT>2023-10-30</dFeIniT>
                    </gTimb>
                    <gDatGralOpe>
                        <dFeEmiDE>2023-01-01T16:43:50</dFeEmiDE>
                        <gOpeCom>
                            <iTipTra>2</iTipTra>
                            <dDesTipTra>Prestacion de servicios</dDesTipTra>
                            <iTImp>1</iTImp>
                            <dDesTImp>IVA</dDesTImp>
                            <cMoneOpe>PYG</cMoneOpe>
                            <dDesMoneOpe>Guarani</dDesMoneOpe>
                        </gOpeCom>
                        <gEmis>
                            <dRucEm>80001813</dRucEm>
                            <dDVEmi>3</dDVEmi>
                            <iTipCont>2</iTipCont>
                            <cTipReg>3</cTipReg>
                            <dNomEmi>DESARROLLO TURISTICO PARAGUAYO</dNomEmi>
                            <dDirEmi>CALLE 1 CASI CALLE 2</dDirEmi>
                            <dNumCas>0</dNumCas>
                            <cDepEmi>1</cDepEmi>
                            <dDesDepEmi>CAPITAL</dDesDepEmi>
                            <cCiuEmi>1</cCiuEmi>
                            <dDesCiuEmi>ASUNCION (DISTRITO)</dDesCiuEmi>
                            <dTelEmi>0217297070</dTelEmi>
                            <dEmailE>sylvia@dtp.com.py</dEmailE>
                            <gActEco>
                                <cActEco>96099</cActEco>
                                <dDesActEco>OTRAS ACTIVIDADES DE SERVICIOS PERSONALES N.C.P.</dDesActEco>
                            </gActEco>
                        </gEmis>
                        <gDatRec>
                            <iNatRec>1</iNatRec>
                            <iTiOpe>1</iTiOpe>
                            <cPaisRec>PRY</cPaisRec>
                            <dDesPaisRe>Paraguay</dDesPaisRe>
                            <iTiContRec>2</iTiContRec>
                            <dRucRec>0475871</dRucRec>
                            <dDVRec>4</dDVRec>
                            <dNomRec> GUILLERMO ERNESTO KRAUCH RIERA</dNomRec>
                            <dDirRec>CALLE 1 ENTRE CALLE 2 Y CALLE 3</dDirRec>
                            <dNumCasRec>123</dNumCasRec>
                            <cDepRec>1</cDepRec>
                            <dDesDepRec>CAPITAL</dDesDepRec>
                            <cDisRec>1</cDisRec>
                            <dDesDisRec>ASUNCION (DISTRITO)</dDesDisRec>
                            <cCiuRec>1</cCiuRec>
                            <dDesCiuRec>ASUNCION (DISTRITO)</dDesCiuRec>
                            <dTelRec>012123456</dTelRec>
                            <dCodCliente>57015</dCodCliente>
                        </gDatRec>
                    </gDatGralOpe>
                    <gDtipDE>
                        <gCamFE>
                            <iIndPres>1</iIndPres>
                            <dDesIndPres>Operación presencial</dDesIndPres>
                        </gCamFE>
                        <gCamCond>
                            <iCondOpe>2</iCondOpe>
                            <dDCondOpe>Crédito</dDCondOpe>
                            <gPagCred>
                                <iCondCred>1</iCondCred>
                                <dDCondCred>Plazo</dDCondCred>
                                <dPlazoCre>28</dPlazoCre>
                            </gPagCred>
                        </gCamCond>
                        <gCamItem>
                            <dCodInt>CAC/CTAC</dCodInt>
                            <dDesProSer>CUENTAS ACTIVAS</dDesProSer>
                            <cUniMed>77</cUniMed>
                            <dDesUniMed>UNI</dDesUniMed>
                            <dCantProSer>1</dCantProSer>
                            <dInfItem>21</dInfItem>
                            <gValorItem>
                                <dPUniProSer>1100000</dPUniProSer>
                                <dTotBruOpeItem>1100000</dTotBruOpeItem>
                                <gValorRestaItem>
                                    <dDescItem>0</dDescItem>
                                    <dPorcDesIt>0</dPorcDesIt>
                                    <dDescGloItem>0</dDescGloItem>
                                    <dTotOpeItem>1100000</dTotOpeItem>
                                </gValorRestaItem>
                            </gValorItem>
                            <gCamIVA>
                                <iAfecIVA>1</iAfecIVA>
                                <dDesAfecIVA>Gravado IVA</dDesAfecIVA>
                                <dPropIVA>100</dPropIVA>
                                <dTasaIVA>10</dTasaIVA>
                                <dBasGravIVA>1000000</dBasGravIVA>
                                <dLiqIVAItem>100000</dLiqIVAItem>
                            </gCamIVA>
                        </gCamItem>
                        <gCamItem>
                            <dCodInt>TAU/TAUR</dCodInt>
                            <dDesProSer>TARJETA URGENTE</dDesProSer>
                            <cUniMed>77</cUniMed>
                            <dDesUniMed>UNI</dDesUniMed>
                            <dCantProSer>1</dCantProSer>
                            <gValorItem>
                                <dPUniProSer>1100000</dPUniProSer>
                                <dTotBruOpeItem>1100000</dTotBruOpeItem>
                                <gValorRestaItem>
                                    <dDescItem>0</dDescItem>
                                    <dPorcDesIt>0</dPorcDesIt>
                                    <dDescGloItem>0</dDescGloItem>
                                    <dTotOpeItem>1100000</dTotOpeItem>
                                </gValorRestaItem>
                            </gValorItem>
                            <gCamIVA>
                                <iAfecIVA>1</iAfecIVA>
                                <dDesAfecIVA>Gravado IVA</dDesAfecIVA>
                                <dPropIVA>100</dPropIVA>
                                <dTasaIVA>10</dTasaIVA>
                                <dBasGravIVA>1000000</dBasGravIVA>
                                <dLiqIVAItem>100000</dLiqIVAItem>
                            </gCamIVA>
                        </gCamItem>
                        <gCamEsp>
                            <gGrupAdi>
                                <dCiclo>DICIEMBRE</dCiclo>
                                <dFecIniC>2022-11-23</dFecIniC>
                                <dFecFinC>2023-12-17</dFecFinC>
                            </gGrupAdi>
                        </gCamEsp>
                        <gTransp>
                            <iModTrans>1</iModTrans>
                            <dDesModTrans>Terrestre</dDesModTrans>
                            <iRespFlete>2</iRespFlete>
                            <dNuDespImp>1234567891asfdcs</dNuDespImp>
                        </gTransp>
                    </gDtipDE>
                    <gTotSub>
                        <dSubExe>0</dSubExe>
                        <dSubExo>0</dSubExo>
                        <dSub5>0</dSub5>
                        <dSub10>2200000</dSub10>
                        <dTotOpe>2200000</dTotOpe>
                        <dTotDesc>0</dTotDesc>
                        <dTotDescGlotem>0</dTotDescGlotem>
                        <dTotAntItem>0</dTotAntItem>
                        <dTotAnt>0</dTotAnt>
                        <dPorcDescTotal>0</dPorcDescTotal>
                        <dDescTotal>0.0</dDescTotal>
                        <dAnticipo>0</dAnticipo>
                        <dRedon>0.0</dRedon>
                        <dTotGralOpe>2200000</dTotGralOpe>
                        <dIVA5>0</dIVA5>
                        <dIVA10>200000</dIVA10>
                        <dTotIVA>200000</dTotIVA>
                        <dBaseGrav5>0</dBaseGrav5>
                        <dBaseGrav10>2000000</dBaseGrav10>
                        <dTBasGraIVA>2000000</dTBasGraIVA>
                    </gTotSub>
                </DE>
                <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
                    <SignedInfo>
                        <CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                        <SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
                            <Reference URI="#018000181303001001000000122023103011010101013">
                                <Transforms>
                                    <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                                    <Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                                </Transforms>
                               <DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
                                <DigestValue>laYXgoM0S0LY1aMJoYUUUsB2SUjDzxMZAZzvf9reIU8=</DigestValue>
                            </Reference>
                    </SignedInfo>
                    <SignatureValue>
                            LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUV2Z0lCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktnd2dnU2tBZ0VBQW9JQkFRREdtb3EwMEVUWkdOajgKYnZUNzZwNUVyWW5TTlRKcWlXZ0NUZ3J4dmF2dzd2RTRNaGV5TURmbkJWNGowNENSdEIzVVV3REtVN2RIOXZ4agp6SEVpanZpYUh1eTU0NDhTUFYvVEJMY0RYdzYrcE9PU1pVd2FJd09QOWxqMEg4dzBtVDFvVzVxYlZxbDJuRkFrCjN3WFFuczhTQnlRbmM1TWl4RzAxdGJJK0NLZ0JrSHVQdUZkVWl5SDhKYXRsWXpkYS9TWEhVSngzalNqN29KSHoKWmtxbzhQaXVYRitlVjF1OWRtbDh2UUQ2bW85UmluRWJnMURMQ2VDYUxDZHlmUXE3VVhXOVJzNGhmOURYSHUwbQpKTFBHYlo4WTEraHU3cG1COTcvUGtXejFvdTZCdzJXSVRHcHRiaHZhU01sQkl1UHd2WFQzZjNYbDlHaDhxNC8yCmh1WDJtMDZCQWdNQkFBRUNnZ0VCQUo3OWZ3Y3JKOEtvYXFVRGFxbjFkQWhFNk8xWVd1a0E0VHZmSUZIUVpzSjEKZHJSbkhhTXZpTUV3QWVJcVltWENUSkw2YzhxRlpGZC9PU1BudWJaUndHRVpXcWVoclpxTGVaZmowV01vaVJSRAp5azhiT3VZOW00OTNSbFVBMDZoVCtOMy9YM0QxMnFveWp0SkZuRkVib1dlc1FkdzNZTlhYYkJvd1ArWDdaZ3dCCkpiZHRzNEdBODg1UkZwYmJOcG1HekdER1Y2WkpSb0FhN1pPbEpRMmNhVUF4VEkvVjRPWjNwTzdEbGdaN2w4LzUKdytwZ1BCQzd4UDVIdWJTYkN4dHN4MXVYNGZiTjNQNTNzelcwUDAwNlpWcmxQakJqcWlaTGo4OW5McFNmd2NacAo3Z2o5L0NDRXh6YmhQTWlmNTRkMUJnSjl6bEdzS3hsYjhiS1Z6TDBKTWNVQ2dZRUEwNGJvVkp2Z3BXYkxFdHZjClJsNjFrRGRYNVY4QnZjcUVLRDIrVXJ2NFVCaVFmSk1XNjJtK3BQckZTcWtSRldYeFNBMkNrcjhyZkNHY1ZsWFIKTUJvVm8xaUlRSENMc0xnOHQzalBKQjF4ZjA4cVBvbUdndmxUUzNQNStpY1Vlc2g3SVlVdkRPb2tCN2R3Zmo3Mgp4cENESGpkcitLT2ROUEVWa1ZadmhLMkkrTThDZ1lFQThGd1BCaXBORS9VQzBldDBJUDJlbVkwTTFNS0QrQVo5CjZSK3JGQURRVWlOU2J4R1ZST20xa3lVdXR0d2E5ZFg4QWpmdzVFdmx5QUduNzUzZGxQTDNCSk9vdGRyKzZQZDQKRHhmbUoxaFVRanI1NDlFZzJ2TjVicFNvSHlGVzVjdG43bHFhWUVSc1R5dFdlaUFFT3J2V05KMjNTd3JlWmE4UQphMm96U0xMZmQ2OENnWUVBbGhSN0ZVVC96L3N1REhSb1JwbCtFeFBoVGtIcGQyS1FBQWxHMTJhODJQK0V6STZFClRZVmFtYTIvZTZpWEVXcWlGZkJYKzR5ZDZ0WGlHYk9MMTZnNlFBcFpxZmJ3ZGZOU1NRN3pVWGwraE1pRGZabVEKRWErYTlNY1Z6dklaYXNDQjlKZU5lWWxMQlJxWE8wdXJSZGxVM29TT0cvemlWcVRURzZxTE4wVDFFU3NDZ1lBRApXeFd1blZxVUk4Q1ZVN2M4V3E0ak1LLzBMSDhSTUc0RG1qaVdQK2lvck91U3Qya2hIQzQzenJZWFFYMXkzemowCmd2bHZCb3cvdlo0dG4wT2Z0OEN4SktxZlYxNWx2RWJGMXl5VWNneERISFd1czVYRTJNOWlOdHNlRENZZGNUaVQKUUZxVmEzOGpuOVJHUWpvczQ4QjYzWlRZakY5TWhpUXhpN3NKa2MzQzZ3S0JnRDhJdnE0NVg4M1JkSGI2WnpQUApuTFBFL0dQZjVMdkdHOEJHMjc4bTEzNlhsOHhiWkhmMnJpTFRzTFFUaXhxdXBUWEJYY0tYTmFiQnFtRkdEczlyCk4vM3VxdS9CK2R1R0JFTDRaQmJJU0pKS3NXNTJ0ak1FR2g0Y3ZIQnRaNFNNSUdMaHIxMDFIT2ppRmdKVXJLYUIKVlJ2NVk5bDVJNkZzSTJIVjNvSjdKbEV4Ci0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0K                    
                    </SignatureValue>
                        <KeyInfo>
                            <X509Data>
                                <X509Certificate>
                                    MIIIFDCCBfygAwIBAgIQbVrh2RkwO4pEQ3K6GZ1KATANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMCUFkxDTALBgNVBAoTBElDUFAxODA2BgNVBAsTL1ByZXN0YWRvciBDdWFsaWZpY2FkbyBkZSBTZXJ2aWNpb3MgZGUgQ29uZmlhbnphMRUwEwYDVQQDEwxDT0RFMTAwIFMuQS4xFjAUBgNVBAUTDVJVQzgwMDgwNjEwLTcwHhcNMjMxMDIwMTIyNTM2WhcNMjQxMDIwMTIyNTM2WjCBuTELMAkGA1UEBhMCUFkxKzApBgNVBAoTIkNFUlRJRklDQURPIENVQUxJRklDQURPIFRSSUJVVEFSSU8xCzAJBgNVBAsTAkYxMRswGQYDVQQEExJGRVJOQU5ERVogREUgUFJPTk8xFTATBgNVBCoTDFNZTFZJQSBNQVJJQTEpMCcGA1UEAxMgU1lMVklBIE1BUklBICBGRVJOQU5ERVogREUgUFJPTk8xETAPBgNVBAUTCENJNjkwNjgzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxpqKtNBE2RjY/G70++qeRK2J0jUyaoloAk4K8b2r8O7xODIXsjA35wVeI9OAkbQd1FMAylO3R/b8Y8xxIo74mh7sueOPEj1f0wS3A18OvqTjkmVMGiMDj/ZY9B/MNJk9aFuam1apdpxQJN8F0J7PEgckJ3OTIsRtNbWyPgioAZB7j7hXVIsh/CWrZWM3Wv0lx1Ccd40o+6CR82ZKqPD4rlxfnldbvXZpfL0A+pqPUYpxG4NQywngmiwncn0Ku1F1vUbOIX/Q1x7tJiSzxm2fGNfobu6Zgfe/z5Fs9aLugcNliExqbW4b2kjJQSLj8L1093915fRofKuP9obl9ptOgQIDAQABo4IDSDCCA0QwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUehL5J8PdsburoOhRNy39wE1EAXQwHwYDVR0jBBgwFoAUvjVUYmhg5ybTMcFfl7Hi9mTOB/UwDgYDVR0PAQH/BAQDAgXgMIG+BgNVHREEgbYwgbOBEVNZTFZJQUBEVFAuQ09NLlBZpIGdMIGaMSowKAYDVQQKEyFERVNBUk9MTE8gVFVSSVNUSUNPIFBBUkFHVUFZTyBTLkExEzARBgNVBAsTCkRJUkVDVE9SSU8xEzARBgNVBAwTClBSRVNJREVOVEUxKjAoBgNVBA0MIUZJUk1BIEVMRUNUUsOTTklDQSBkZSBuaXZlbCBtZWRpbzEWMBQGA1UEBRMNUlVDODAwMDE4MTMtMzCB9wYDVR0gBIHvMIHsMIHpBgsrBgEEAYOucAEBBDCB2TBGBggrBgEFBQcCARY6aHR0cHM6Ly9jb2RlMTAwLmNvbS5weS9yZXBvc2l0b3Jpby1kZS1kb2N1bWVudG9zLXB1YmxpY29zLzCBjgYIKwYBBQUHAgIwgYEMf2NlcnRpZmljYWRvIGN1YWxpZmljYWRvIGRlIGZpcm1hIGVsZWN0csOzbmljYSB0aXBvIEYxIHN1amV0YSBhIGxhcyBjb25kaWNpb25lcyBkZSB1c28gZXhwdWVzdGFzIGVuIGxhIERQQyBkZWwgUENTQyBDT0RFMTAwIFMuQS4wewYDVR0fBHQwcjA3oDWgM4YxaHR0cDovL3BjYTEuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDA3oDWgM4YxaHR0cDovL3BjYTIuY29kZTEwMC5jb20ucHkvY3Jscy9jYS1jb2RlMTAwLXNhLmNybDAgBgNVHSUBAf8EFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwgYkGCCsGAQUFBwEBBH0wezA5BggrBgEFBQcwAYYtaHR0cDovL29jc3AuY29kZTEwMC5jb20ucHkvb2NzcC9jYS1jb2RlMTAwLXNhMD4GCCsGAQUFBzAChjJodHRwOi8vcGNhMS5jb2RlMTAwLmNvbS5weS9jZXJ0cy9jYS1jb2RlMTAwLXNhLmNlcjANBgkqhkiG9w0BAQsFAAOCAgEA07RWl+uU/gabJ9Sez7t7QI/CNKfUAZRA449ydVLK7iD6CqlWmQiswT/jbP3/XPgkJmvWYAHAwb0MyhRECWfk1A6BJs/ZAv6YIN4N7OlHM9lxsJIvbymumyUS6jrWgS1lLudCduwq+rp55KRPi7pohIowErIV1Rr0RqvPMCibili+ljXRGVbla2ZOLTykVcs03K2valyZFtIyzqfH/MbDOlDplvikUzcB2Ht4u2wh4Ip59e8IVdB8bcOgfgUPnf3Xh0Msi9oyI5AJI+8HaKsQhCqbJWdBqiwLeutzhOcuqwQt4ojeLKHIcddV3HKVNYi0okNs0zy/rRH+CHNEUk2z5Ovq0IimSxOx/Y7F0XyjSd/2rbucn4RZFqPBTpgLX7OawgPW5YjjpayIxiAriEZBN1o8s1e9R59KneYXGPW9ufeIfC8YCGk99TutEfLq/K+Suv3/fdKxuXyxnF3g4mP9INapJl2AmmQ9jVYFSk+3hLPOgspZs0J5/TzvEiZ4+aoD9lpY7CNH2vAGXgw+R4FEWPCGF/1aklL4oJvkwjkx1Kq1IWBfZteHFQQDiTzVGruEQG8ngw304LkJQCHVuyEEpBL5ga7zOft3tLZ163fPqU7Bk5IK0kU8Gy9Uyp/Rwotk/uvVLxunyHzWqEU6TwWOS4FQnRGkG/842DowdnSmu3E=
                                </X509Certificate>
                                <X509IssuerSerial>
                                    <X509IssuerName>C=PY,O=ICPP,CN=CODE100 S.A.,2.5.4.5=#RUC80080610-7</X509IssuerName>
                                        <X509SerialNumber>145357739000291486709356561931893033473</X509SerialNumber>
                                </X509IssuerSerial>
                            </X509Data>
                        </KeyInfo>
                </Signature>
                <gCamFuFD>
                    <dCarQR>https://ekuatia.set.gov.py/consultas-test/qr?nVersion=150&Id=018000181303001001000000122023103011010101013&dFeEmiDE=323032332d30312d30315431363a34333a3530&dRucRec=0475871&dTotGralOpe=2200000&dTotIVA=2200000&cItems=1&DigestValue=62396535396161376132366137373164323135386332666465633032363733396562366563613137&IdCSC=0001&cHashQR=c65899106a62122cc92531a0ccb6b7c2048444979423025e7cae6244a26b9d72</dCarQR>                                  
                </gCamFuFD>
            </rDE>
            </soap:Body>
            </soap:Envelope>
            ';
            
            try {
                // Realizar la solicitud POST al servicio web
                $response = $client->post('https://sifen-test.set.gov.py/de/ws/sync/recibe.wsdl', [
                    'headers' => [
                        'Content-Type' => 'text/xml; charset=UTF-8',
                    ],
                    'body' => $xml,
                ]);
            
                // Obtener la respuesta del servicio web
                $responseData = $response->getBody()->getContents();

                echo '<pre>';
                print_r($responseData);
    
                // Procesar la respuesta según sea necesario
                // ...
            } catch (\Exception $e) {
                // Manejar cualquier error que ocurra durante la solicitud
                // ...
            }

    }


        public function generateSignature()
        {
                    // parse xml
        $xmlFilePath = storage_path('/opt/lampp/htdocs/gestur/public/SYLVIA MARIA  FERNANDEZ DE PRONO.pfx');
        $doc = new \DOMDocument();
        $doc->load($xmlFilePath);

        $parent = $doc->getElementsByTagName("rDE")->item(0);
        $signedElement = $parent->getElementsByTagName("DE")->item(0);
        $nextSibling = $parent->getElementsByTagName("gCamFuFD")->item(0);

        // loading keys
        $certificateFilePath = storage_path('app/tmp/certificate.pfx');
        $certificatePassphrase = 'your_passphrase'; // Replace with the actual passphrase

        $pkcs12 = file_get_contents($certificateFilePath);
        openssl_pkcs12_read($pkcs12, $certs, $certificatePassphrase);

        $privateKey = $certs['pkey'];
        $certificate = $certs['cert'];

        $xmlSignatureFactory = new \RobRichards\XMLSecLibs\XMLSecurityDSig();
        $xmlSignatureFactory->setPrivateKey($privateKey);
        $xmlSignatureFactory->add509Cert($certificate);
        $xmlSignatureFactory->addReference(
            $signedElement,
            \RobRichards\XMLSecLibs\XMLSecurityDSig::SHA256,
            ["http://www.w3.org/2000/09/xmldsig#enveloped-signature", "http://www.w3.org/2001/10/xml-exc-c14n#"],
            ["force_uri" => true]
        );
        $xmlSignatureFactory->sign($xmlSignatureFactory->getPrivateKey());

        // output the resulting document
        $resultXmlFilePath = storage_path('app/tmp/ex00-signed.xml');
        $xmlSignatureFactory->saveDocument($resultXmlFilePath);

        return response()->file($resultXmlFilePath);
    }
}
