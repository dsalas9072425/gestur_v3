<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use App\Proforma;
use App\Producto;
use App\Persona;
use App\TipoFactura;
use App\Autorizaciones;
use App\TipoFacturacion;
use App\Currency;
use App\FormaPagoCliente;
use App\TipoIdentidad;
use App\EstadoFactour;
use App\Ticket;
use App\Asistencia; 
use App\ProformasDetalle;
use App\ProformasDetallePasajeros;
use App\HistoricoComentariosProforma;
use App\TarjetaCredito;
use App\Voucher;
use App\TiposTicket;
use App\Grupos;
use App\Empresa;
use App\VentasRapidasCabecera;
use App\VentasRapidasDetalle;
use Session;
use Redirect;
use App\AdjuntoDocumento;
use App\LineaCredito;
use App\Hotel;
use App\Promocion;
use App\Destination;
use App\Tarifa;
use App\ReservasServicio;
use App\ReservasNemo;
use App\TipoImpuesto;
use App\EstadoPedidoWoo;
use App\Zona;
use App\FormaCobroCliente;
use App\ComercioPersona;
use App\Factura;
use App\TipoVenta;
use App\FileEmpresas;
use App\PasajeroProforma;
use App\NemoReserva;
use App\Negocio;
use App\EstadoReserva;
use App\Anticipo;
use App\SolicitudFacturaParcial;
use App\ProformaFacturaDetalle;
use App\ProformaCliente;
use App\TipoPersona;
use App\Equipo;
use App\EquipoDetalle;
use App\ConfigNemo;
use App\TarjetaPersona;
use App\Pais;
use DB;
use App\DestinoDtpmundo;
use App\ProformaDetalleReprice;
use App\ProformaReprice;
use Illuminate\Support\Facades\Cache;
use App\Traits\ProformaTrait;
use Log;
use App\Traits\OpTrait;
use App\Exceptions\ExceptionCustom;


class ProformaController extends Controller
{
	use ProformaTrait, OpTrait;

	private $tiempo_cache = 0;
	private $id_empresa = 0;
	private $id_usuario = 0;

	function __construct(){
		/**
		 * Vamos a fijar un cache de 1 hora para los datos de la empresa
		 */
		$this->tiempo_cache = 60;
	}

	/**
	 * Cuando se recupere el dato se va recuperar segun la empresa y el usuario si asi lo requiere, su uso es mejor cuando es para cargar las opciones
	 * de las listas desplegables de los formularios que no sean muy grandes.
	 * 
	 * La key es el nombre que le damos a esa funcionalidad, la idea es poder reutilizar la key en consultas similares en el controaldor
	 * El $variante es por ejemplo cuando la consulta implican datos de un usuario, entonces el $variante seria el id del usuario
	 * 
	 */
	private function idCacheUnique($key, $variante = ''){
    	return $key . $variante .'_FacturarController_'. $this->getIdEmpresa();
   	}

	private function getDatosUsuario(){
	
	 $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	 return $persona = Persona::where('id',$idUsuario)->first();

	}//function

	private function getIdUsuario(){
    
		if(!$this->id_usuario){
		  $this->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		}
	  
	  return $this->id_usuario;
	}
	
	private function getIdEmpresa(){
	
	  if(!$this->id_empresa){
		 $this->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	  }
	 return $this->id_empresa;
	}
	

	 public function getIconos($proformaId){
		$detalles = ProformasDetalle::where('id_proforma', $proformaId)->where('activo',true)->get(['id_producto', 'fecha_pago_proveedor']);
		$fecha_pago_proveedor = 0;
		$ticket = 0;
		foreach($detalles as $key=>$details){
			if($details->fecha_pago_proveedor != ""){
				$fecha_pago_proveedor = 1;
			}

			if($details->id_producto == 1){
				$ticket = 1;
			}

			if($details->id_producto == 9){
				$ticket = 1;
			}
		}
		return $fecha_pago_proveedor."_".$ticket;
	}
		
	/**
	 * Carga los datos en la lista de proforma
	 * 
	 */
	public function index(Request $request){
		
		$personaLogin = $this->getDatosUsuario();
		$idEmpresa = $this->getIdEmpresa();
		$tipoPersona = $personaLogin->id_tipo_persona;
		$tipoPersona = '0';
		$estado = '1';

            //DATOS DE USUARIO
        $tipoPersona = $personaLogin->id_tipo_persona;
		$currency = Currency::where('activo', 'S')->get();
		$vendedor_empresa = Cache::remember($this->idCacheUnique('vendedor_empresa'), $this->tiempo_cache, function () use ($idEmpresa) { 
				return Persona::selectRaw("CONCAT(nombre, ' ', apellido) AS text, id")
				       ->whereIn('id_tipo_persona',array(2, 4, 3,6))
					   ->where('id_empresa',$idEmpresa)
					   ->where('activo','true')
					   ->orderBy('nombre','ASC')
					   ->get();;
		});


		$getOperativo = Persona::where('id_tipo_persona','6')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();

    	$sucursalEmpresa = Persona::where('id_tipo_persona','21')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();

		$estados_factour = EstadoFactour::where('id_tipo_estado','1')->where('visible',true)->orderBy('denominacion','ASC')->get();

		$negocios = Negocio::where('id_empresa', '=', $idEmpresa)->get();
		
		$grupos = Grupos::where('estado_id', 11)
								->where('empresa_id',$this->getIdEmpresa())
								->get(['id','denominacion']);

		$asistentes = Persona::where('id_empresa', '=', $idEmpresa)
								->whereIn('id_tipo_persona',array(2, 4, 3, 5,6,7))
								->get(['id','nombre','apellido', 'email']);		
			
		$equipos = Equipo::where('activo', true)->where('id_empresa', '=', $idEmpresa)->get();

		$prioridades = DB::select("SELECT * FROM prioridades");
	
		return view('pages.mc.proforma.index')->with([
													  'vendedor_empresas'=>$vendedor_empresa, 
													  'currencys'=>$currency,
													  'tipoPersona'=>$tipoPersona,
													  'estados'=>$estados_factour,
													  'getOperativo'=>$getOperativo, 
													  'sucursals'=>$sucursalEmpresa,
													  'negocios'=>$negocios,
													  'grupos'=>$grupos,
													  'asistentes'=>$asistentes,
													  'prioridades'=>$prioridades,
													  'equipos'=>$equipos
													  ]);
	}//function	

	/**
     * Recibe una fecha 2017-05-02 y pasa a 02/05/2017 
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaSalida($date){
        if( $date != ''){

        $date = explode('-', $date);
            $fecha = $date[2]."/".$date[1]."/".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }//function


    /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }//function


    	private function  responseFormatDatatable($req)
	  	{
		    $datos = array();
		    $data =  new \StdClass;
		    
		    foreach ($req->formSearch as $key => $value) 
		    {
		     	$n = $value['name'];
		        $datos[$value['name']] = $value['value'];
		        $data-> $n = $value['value'];
		    }

		    return $data;
        } 


	public function getDatosProforma(Request $req){
		// dd($reqt->all());
		// $req = $this->responseFormatDatatable($reqt);
    	
    	// die;

		//se reciben los datos del datatables para server side
		$draw = intval($req->draw);
		$start = intval($req->start);
		$length = intval($req->length);
		
		$personaLogin = $this->getDatosUsuario();
		// dd($personaLogin);
		$tipoPersona = $personaLogin->id_tipo_persona;
		$idUsuario = $personaLogin->id;
		$sucursalEmpresa = $personaLogin->id_sucursal_empresa; 
		$flag = 0;

		$proformas = DB::table( 
					DB::raw("( 
						SELECT 
						p.id,
						p.reconfirmacion,
						p.estado_id, 
						p.pasajero_online,  
						p.id_prioridad, 
						p.id_moneda_venta,
						pri.des_prioridad,
						estados.denominacion,
						f.nro_factura, 
						p.calificacion,
						p.comentario_calificacion,
						p.factura_id,
						p.destino_id,
						p.id_equipo,
						p.id_asistente,
						p.id_grupo,
						destino.desc_destino,
						p.fecha_alta, 
						(SELECT id FROM proformas where proforma_padre = p.id LIMIT 1) as expediente,
						(SELECT currency_code FROM currency where currency_id = p.id_moneda_venta LIMIT 1) as moneda_venta,
						p.proforma_padre,
						p.emergencia,
						p.check_out,
						to_char(p.fecha_alta , 'DD/MM/YYYY') as fecha_alta_format,
						p.check_in,
						to_char(p.check_in , 'DD/MM/YYYY') as check_in_format,
						p.cliente_id,
						concat(cliente.nombre,' ',cliente.apellido) cliente_n,
						p.vendedor_id,
					    concat(vendedor.nombre,' ',vendedor.apellido) vendedor_n, 
						p.pasajero_id,	
					    concat(pasajero.nombre,' ',pasajero.apellido) pasajero_n, 
						p.responsable_id,
						concat(operativo.nombre,' ',operativo.apellido) operativo_n, 
						p.id_usuario, 
						concat(usuario.nombre,' ',usuario.apellido) usuario_n,
					    usuario.id_sucursal_empresa id_sucursal_empresa ,
					    p.file_codigo,
					    p.id_empresa,
					    p.markup,
						p.porcentaje_ganancia,
					    (SELECT get_monto_proforma as m FROM get_monto_proforma(p.id)) as total_proforma,
					    (SELECT COUNT(*) 
					    FROM proformas_detalle 
					    WHERE activo = true and id_producto IN( SELECT id  FROM productos WHERE id_grupos_producto = 1 ) and id_proforma = p.id ) as ticketCount,
					    ".$tipoPersona." as tipo_persona_usuario,
						unidad_negocios.descripcion as negocio,
						unidad_negocios.id as negocio_id
		FROM proformas p 
		LEFT JOIN personas as cliente
		ON cliente.id = p.cliente_id 
		LEFT JOIN personas as usuario 
		ON usuario.id = p.id_usuario
		LEFT JOIN personas as vendedor
		ON vendedor.id = p.vendedor_id
		LEFT JOIN personas as pasajero
		ON pasajero.id = p.pasajero_id
		LEFT JOIN personas as operativo 
		ON operativo.id = p.responsable_id
		LEFT JOIN proformas as expediente 
		ON expediente.id = p.proforma_padre
		LEFT JOIN estados
		ON estados.id = p.estado_id
		LEFT JOIN unidad_negocios
		ON unidad_negocios.id = p.id_unidad_negocio
		LEFT JOIN destinos_dtpmundo as destino
		ON destino.id_destino_dtpmundo = p.destino_id
		LEFT JOIN prioridades as pri ON pri.id_prioridad=p.id_prioridad
		LEFT JOIN facturas as f
		ON f.id = p.factura_id ) as result")		
		);
		$proformas = $proformas->where('id_empresa', $this->getIdEmpresa());
		// $query = '
		// // WHERE proformas.id_empresa = '.$this->getIdEmpresa();
			/*
            2 "ADMIN_EMPRESA"
            3 "VENDEDOR_EMPRESA"
            4 "SUPERVISOR_EMPRESA"
            5 "ADMINISTRATIVO_EMPRESA"
            6 "OPERATIVO_EMPRESA"
           */
       
        	//======ESTADO PROFORMA=========
        	//OPERATIVO EMPRESA OR ADMINISTRATIVO_EMPRESA OR ADMIN_EMPRESA
			if($tipoPersona == '6' || $tipoPersona == '5' || $tipoPersona == '2'){
				if($req->id_estado_proforma == ''){ 
					//Traer Verificado y  aVerificar
					// $proformas = $proformas->whereIn('estado_id', [3,2]);

				}
					//SUPERVISOR EMPRESA
			} else if($tipoPersona == '4'){
					$proformas = $proformas->where('id_sucursal_empresa', $sucursalEmpresa);
			} else {
					//LOS DEMAS TIPO SOLO PUEDE VER SU PROFORMA
					$flag = 1;
				    //$proformas = $proformas->where('id_usuario', $idUsuario);
					//$proformas = $proformas->orWhere('id_asistente', $idUsuario);
					$proformas = $proformas->where(function($proformas) use ($idUsuario) {
						$proformas->where('id_usuario',$idUsuario)
							  ->orWhere('id_asistente',$idUsuario);
					});

			}

			try {
			 if (count($req->input('id_estado_proforma')) > 0) { 
			 	$arrayEstado = $req->input('id_estado_proforma');
			 	foreach ($arrayEstado as $key => $value) {
			 		if(trim($value) === ''){
			 			unset($arrayEstado[$key]);
			 		}
			 	}
			 		if(count($arrayEstado) > 0)
        			$proformas = $proformas->whereIn('estado_id', $arrayEstado);
			}

			 } catch(\Exception $e){}
			
		 	 if($req->id_vendedor_empresa != '' && $flag == 0){			
        		$proformas = $proformas->where('id_usuario', $req->id_vendedor_empresa);
        	}	

			if($req->star_option != ''){
				// dd($req->star_option);
				$option = explode(',',$req->star_option);
				$proformas = $proformas->whereIn('calificacion', $option);

			}
			if($req->post_venta != ''){ 

				if($req->input('post_venta') === 'true')
				$proformas = $proformas->where('calificacion','<>', '0');

				if($req->input('post_venta') === 'false')
				$proformas = $proformas->where('calificacion', '0');
			}

			 if($req->input('nro_factura') != ''){
	        	$proformas = $proformas->where('nro_factura', trim($req->input('nro_factura')));
	        }

      	    if($req->input('responsable_id') != ''){
      	    	$proformas = $proformas->where('responsable_id', trim($req->input('responsable_id')));
      	    }

			if($req->input('id_asistente') != ''){
				$proformas = $proformas->where('id_asistente', trim($req->input('id_asistente')));
			}

			if($req->input('negocio') != ''){
				$proformas = $proformas->where('negocio_id', trim($req->input('negocio')));
			}

			if($req->input('grupo_id') != ''){
				$proformas = $proformas->where('id_grupo', $req->input('grupo_id'));
			}

      	    if($req->input('vendedor_id') != ''){
      	    	$proformas = $proformas->where('vendedor_id', trim($req->input('vendedor_id')));	
      	    }	
         	
         	if($req->input('file_codigo') != ''){
      	    	//$proformas = $proformas->where('file_codigo', trim($req->input('file_codigo')));
      	    	$nombre = "%".trim($req->input('file_codigo'))."%";
				// dd($nombre);
				$proformas = $proformas->where('file_codigo','LIKE',$nombre);	
      	    }	

			 if($req->input('id_proforma') != ''){
	        	$proformas = $proformas->where('id', trim($req->input('id_proforma')));	
	        }
	        if($req->input('cliente_id') != ''){ 
	        	$proformas = $proformas->where('cliente_id', trim($req->input('cliente_id')));	
	        }
	         if($req->input('id_proforma_padre') != ''){
	        	$proformas = $proformas->where('proforma_padre', trim($req->input('id_proforma_padre')));	
			}
			
			if($req->input('destino') != ''){
	        	$proformas = $proformas->where('destino_id',$req->input('destino'));	
	        }

			if($req->input('id_equipo') != ''){
				$proformas = $proformas->where('id_equipo', $req->input('id_equipo'));
			}

	        if($req->input('periodo') != ""){
	            $fechaPeriodo = explode(' - ', $req->input('periodo'));
 				$desdeC     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
                $hastaC     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';

                $proformas = $proformas->whereBetween('check_in',array($desdeC,$hastaC));          
            }

	        if($req->input('periodos') != ""){ 
	            $fechaPeriodos  = explode(' - ', $req->input('periodos'));
                $desde     = $this->formatoFechaEntrada($fechaPeriodos[0]).' 00:00:00';
                $hasta     = $this->formatoFechaEntrada($fechaPeriodos[1]).' 23:59:59';
                // $query .= " AND proformas.fecha_alta BETWEEN '".$desde."' AND '".$hasta."'";
                $proformas = $proformas->whereBetween('fecha_alta',array($desde,$hasta));

	        } 

	        if($req->input('periodo_out') != ""){
	            $fechaPeriodo_out = explode(' - ', $req->input('periodo_out'));
	           	/*echo "</pre>";
    			print_r($fechaPeriodo_out);*/
    			$desdeC_out     = $this->formatoFechaEntrada($fechaPeriodo_out[0]).' 00:00:00';
                $hastaC_out     = $this->formatoFechaEntrada($fechaPeriodo_out[1]).' 23:59:59';

                $proformas = $proformas->whereBetween('check_in',array($desdeC_out,$hastaC_out));          
            }

	        if($req->input('id_prioridad') != ''){
				$proformas = $proformas->where('id_prioridad', $req->input('id_prioridad'));	 
			  }
    
	        if($req->input('id_moneda') != ''){
	          $proformas = $proformas->where('id_moneda_venta', trim($req->input('id_moneda')));	 
	        }

			if ($req->input('reconfirmacion')) {
				if ($req->input('reconfirmacion') == 'true') {
					$proformas = $proformas->where('reconfirmacion', true);
				} elseif ($req->input('reconfirmacion') == 'false') {
					$proformas = $proformas->where('reconfirmacion', false);
				}
			}
			

	        $proformas = $proformas->orderBy('id', 'ASC');

			$totalProformas= $proformas->get();
			$cantidadProformas = count($totalProformas);
			$lengtArray =$cantidadProformas;

			$proformas = $proformas->offset($start)->limit($length);
	        $proformas = $proformas->get();

			return response()->json(['draw'=>$draw,
			'recordsTotal'=>$lengtArray,
			'recordsFiltered'=>$lengtArray,
			'data'=>$proformas, 'd'=>'']);	


	}//function

	public function add(Request $request){

		$getCotizacion=DB::select('SELECT public."get_cotizacion"(143, '.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.','.Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.')');
		$cotizacion = $getCotizacion[0]->get_cotizacion;

		$pasajero = DB::select("SELECT concat(nombre,' ',apellido,' - '||documento_identidad) as pasajero_data, id 
							    FROM personas WHERE id_tipo_persona 
							    IN (SELECT id FROM tipo_persona WHERE puede_ser_pasajero = true) AND activo = true   
							    AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa
							);

		$vendedor_empresa = Persona::where('id_tipo_persona', '=' ,'3')->get();
		$vendedor_agencia = Persona::where('id_tipo_persona', '=' ,'10')->orWhere('id_tipo_persona', '=' ,'3')->get();
		$proforma = [];
		$forma_pago = FormaPagoCliente::all();
		$tipoIdentidad = TipoIdentidad::all();
		$currency = Currency::where('activo', 'S')
							->where('tipo', 'V')
							->orderBy('currency_id', 'DESC')
							->get();
		$tipoFactura= TipoFactura::all();

		$promociones = Promocion::whereDate('fecha_baja', '>', date('Y-m-d'))->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
		
		$negocios = Negocio::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();

		$grupos = Grupos::where('estado_id', 11)->where('empresa_id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get(['id','denominacion']);

		$tarifas = Tarifa::all();

		$paises = Pais::where('habilitado',true)->orderBy('name_es','ASC')->get();

		$usuario = Persona::where('id',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->first(['id','nombre','apellido','usuario','id_empresa']);

		return view('pages.mc.proforma.add')
									->with([
											'vendedor_empresas'=>$vendedor_empresa, 
											'vendedor_agencias'=>$vendedor_agencia, 
											'tipoFacturas'=>$tipoFactura, 
											'currencys'=>$currency, 
											'forma_pagos'=>$forma_pago, 
											'proformas'=>$proforma,
											'pasajeros'=>$pasajero, 
											'tipoIdentidads'=>$tipoIdentidad, 
											'cotizacion'=>$cotizacion, 
											'promociones'=>$promociones, 
											'grupos'=>$grupos,
											'negocios'=>$negocios,
											'tarifas'=>$tarifas,
											'usuario'=>$usuario,
											'paises'=>$paises
										]); 
	}

	public function datosProforma(Request $request){
		$proformas = Proforma::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
		if(isset($_GET['q'])){
			$json = [];
            $search = $_GET['q'];
            $contador = strlen($_GET['q']);
            	foreach($proformas as $key=>$proforma){
            		$resultado = substr((string)$proforma['id'],0, $contador);
					if($resultado == $search){ 
						$json[$proforma['id']]['text'] = $proforma['id'];
					}

				}	
		}
		echo json_encode($json);
	}

		


	public function destinos(Request $req){
		$datos = file_get_contents("../destination_actual.json");
		$destinos =  json_decode($datos, true);
		if(isset($_GET['q'])){
            $search = $_GET['q'];
				foreach($destinos as $key=>$destino){
					$resultado = strpos(strtolower($destino['desDestino']), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		$json = [];
		foreach($destinos as $key=>$row){
		    $json[$row['idDestino']]['text'] = $row['desDestino'];
		}
		echo json_encode($json);
	}	


	public function getDatosCliente(Request $req){
		$persona = Persona::with('tipo_facturacion', 'tipo_persona')
							->where('activo', true)
							->where('id', $req->input('dataCliente'))
							->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();

		$lineaCredito =  LineaCredito::where('id_persona', $req->input('dataCliente'))
							->where('activo', '=' ,true)
							->get();
		$saldoCliente = 0;					
		if(!$lineaCredito->isEmpty()){
			$montoLineaCredito = $lineaCredito[0]->monto;
			$saldoCliente = $lineaCredito[0]->saldo;
		}else{
			$montoLineaCredito = 0;
		}					

		if(isset($persona[0]->id_tipo_persona)){
			$idTipoPersona = $persona[0]->id_tipo_persona;
		}else{
			$idTipoPersona = 0;
		}


		$saldoDisponible = DB::select("SELECT * FROM get_saldo_real(".$req->input('dataCliente').")");	

		// $montoSaldoDisponible = $saldoDisponible[0]->get_saldo_real;
			$montoSaldoDisponible = $saldoCliente;
		$datosJson = new \StdClass;
		if(isset($persona[0]->tipo_persona->denominacion)){
			$datosJson->tipo_persona = $persona[0]->tipo_persona->denominacion;
		}else{
			$datosJson->tipo_persona = '';
		}
		if(isset($persona[0]->tipo_facturacion->denominacion)){
			$datosJson->tipo_facturacion = $persona[0]->tipo_facturacion->denominacion;
		}

		$datosJson->id_tipo_persona = $idTipoPersona;
		if(isset($persona[0]->comision_pactada)){
			$datosJson->incentivo = $persona[0]->comision_pactada;
		}else{
			$datosJson->incentivo = 0;
		}
		$datosJson->limite_credito = number_format($montoLineaCredito,2,",",".");
		$datosJson->saldo_disponible = number_format($montoSaldoDisponible,2,",",".");

		$vendedor = Persona::with('tipo_facturacion', 'tipo_persona')
							->where('id_persona', $req->input('dataCliente'))
							->whereIn('id_tipo_persona', [10, 3, 22])
							->where('activo', true)
							->get(); 

		$arrayVentas = [];
		foreach($vendedor as $key=>$row){
			$arrayVentas[$key]['id'] = $row['id'];
			if($row['apellido'] != ""){
				$nombre = $row['nombre']." ".$row['apellido']; 
			}else{
				$nombre = $row['nombre'];	
			}
			$arrayVentas[$key]['denominacion'] = $nombre;
		}
		$datosJson->vendedores = $arrayVentas;

		return json_encode($datosJson);					
	}
	
	public function getCotizacion(Request $req){

		$getCotizacion=DB::select('SELECT public."get_cotizacion"('. $req->input('dataCurrency').', '.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.','.Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.')');
		
		return $getCotizacion[0]->get_cotizacion;
	}	

	public function getCargaCliente(Request $req){
		$cliente = DB::select("
								SELECT p.id,p.nombre, p.apellido,p.dv, p.documento_identidad, tp.denominacion, p.activo,
								concat(p.nombre,' ',p.apellido,' ','- '|| trim(p.denominacion_comercial)) as full_data
								FROM personas p
								JOIN tipo_persona tp on tp.id = p.id_tipo_persona
								WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
								AND p.activo = true and p.id_empresa = ".$this->getIdEmpresa()." order by p.nombre asc");
		return json_encode($cliente);
	}	

	public function getCargaPasajero(Request $req){
		$pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id 
							    FROM personas WHERE id_tipo_persona 
							    IN (SELECT id FROM tipo_persona WHERE puede_ser_pasajero = true) AND activo = true AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
		return json_encode($pasajero);
	}

	public function getCargarGrupos(Request $req){
		$grupos = Grupos::where('empresa_id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get(['id','denominacion']);
		return json_encode($grupos);
	}

	public function getCargarExpedientes(Request $req){
		$expedientes =  Proforma::where('estado_id', '=' ,1)
							->get(['id']);
		return json_encode($expedientes);
	}

	public function getSaldoProforma(Request $request){
		$proforma =  Proforma::where('id', $request->input('id_proforma'))
								->first(['saldo_por_facturar']);

		$solicitudes = SolicitudFacturaParcial::where('id_proforma', $request->input('id_proforma'))->whereIn('id_estado',[80,81])->get();	
		
		return json_encode($proforma->saldo_por_facturar);
	}
	
	public function doPrestadorProforma(Request $request){



		try{

			$nombre_apellido = $request->input('nombre_prestador').' '.$request->input('apellido_prestador');
			$persona = new Persona;
			$persona->id_tipo_identidad = $request->input('tipo_identidad_prestador');
			$persona->nombre = $nombre_apellido ;
			$persona->email = $request->input('email_prestador');
			$persona->dv = $request->input('dv_prestador');
			if($request->input('documento_prestador') != ""){
				$persona->documento_identidad = $request->input('documento_prestador');
			}
			$persona->direccion = $request->input('direccion_prestador');
			$persona->id_pais = $request->input('pais_prestador');
			$persona->id_tipo_persona = 15;
			$persona->id_personeria = 1;
			$persona->id_empresa = $this->getIdEmpresa();
			$persona->id_usuario = $this->getIdUsuario();
			$persona->created_at = date('Y-m-d H:i:00');
			$persona->updated_at = date('Y-m-d H:i:00');
			$persona->save();


			$id = $persona->id;
			$mensaje = new \StdClass; 
			$mensaje->id = $id;
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se ha guardado la Persona exitosamente';
			$mensaje->nombre = $nombre_apellido;

		} catch(\Exception $e){

			Log::info($e);
			$mensaje = new \StdClass; 
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se ha podido guardar. Intentelo nuevamente !!';
			$mensaje->nombre = $nombre_apellido;
		}


		return response()->json($mensaje);
	}

	public function doPasajeroProforma(Request $request){
	
		try{

			$nombre_apellido = $request->input('nombre').' '.$request->input('apellido');
			$persona = new Persona;
			$persona->id_tipo_identidad = $request->input('tipo_identidad');
			$persona->nombre = $nombre_apellido ;
			$persona->email = $request->input('email');
			$persona->dv = $request->input('dv');
			$persona->id_empresa = $this->getIdEmpresa();
			$persona->id_usuario = $this->getIdUsuario();
			if($request->input('documento') != ""){
				$persona->documento_identidad = $request->input('documento');
			}
			$persona->id_tipo_persona = 13;
			$persona->id_personeria = 1;
			$persona->created_at = date('Y-m-d H:i:00');
			$persona->updated_at = date('Y-m-d H:i:00');
			$persona->save();


			$id = $persona->id;
			$mensaje = new \StdClass; 
			$mensaje->id = $id;
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se ha guardado la Persona exitosamente';
			$mensaje->nombre = $nombre_apellido;
		} catch(\Exception $e){

			Log::info($e);
			$mensaje = new \StdClass; 
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se ha podido guardar. Intentelo nuevamente !!';
			$mensaje->nombre = $nombre_apellido;
		}


		return response()->json($mensaje);
	}	

	/**
	 * Crea el nuevo cliente en caso de ser necesario, se utiliza cuando se crea desde la proforma
	 */
	public function doClienteProforma(Request $request){

		$nombre_apellido = $request->input('nombre').' '.$request->input('apellido');
		$id_empresa = $this->getIdEmpresa();
		$id_usuario = $this->getIdUsuario();
		$documentoEnviado=$request->input('documento_cliente');
		try{

			//Validar que tiene documento
			if(!$request->input('documento_cliente')){
				$mensaje = new \StdClass; 
				$mensaje->status = 'ERROR';
				$mensaje->mensaje = 'Falta el documento de identidad';
				return json_encode($mensaje);
			}
			if($id_empresa != 21 && $documentoEnviado !=99999901){
			//Validar documento de identidad, que sea unico
			$unique_document = new Persona;
			$unique_document  = $unique_document->where('documento_identidad',$request->input('documento_cliente'));
			$unique_document  = $unique_document->where('id_empresa',$id_empresa);
			if($request->input('documento_dv')){
				$unique_document  = $unique_document->where('dv',$request->input('documento_dv'));
			}
			$unique_document = $unique_document->count();

			if($unique_document){
				$mensaje = new \StdClass; 
				$mensaje->status = 'ERROR';
				$mensaje->mensaje = 'El documento de identidad ya existe en la base de datos';
				return json_encode($mensaje);
			}
		}
		$persona = new Persona;
		$persona->id_tipo_identidad = $request->input('tipo_identidad');
		$persona->nombre = $nombre_apellido ;
		$persona->email = $request->input('email');
		$persona->direccion = $request->input('direccion_cliente');
		$persona->dv = $request->input('documento_dv');
			$persona->id_empresa = $id_empresa;
			$persona->created_at = date('Y-m-d H:i:00');
			$persona->updated_at = date('Y-m-d H:i:00');
			$persona->id_usuario = $id_usuario;
			$persona->email = $request->input('email');
			$persona->id_pais = $request->input('pais_cliente');
			$persona->id_tipo_facturacion = 2; //Factura NETA
			$persona->id_tipo_persona = 11; //CLIENTE DIRECTO
			$persona->id_personeria = 1; //Persona Física
			$persona->documento_identidad = $request->input('documento_cliente');

			if($request->input('telefono') != ""){
				$persona->telefono = $request->input('telefono');
			}

			$persona->save();
			$id_cliente =  $persona->id;

			$mensaje = new \StdClass; 
			$mensaje->id = $id_cliente;
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se ha guardado la Persona exitosamente';
			$mensaje->nombre = $nombre_apellido;
			$mensaje->documento = $request->input('documento_cliente');
			$mensaje->dv = $request->input('documento_dv');
			$mensaje->tipo_persona = 'CLIENTE DIRECTO';
			
		} catch(\Exception $e){
			$mensaje = new \StdClass; 
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se ha podido guardar. Intentelo nuevamente !!';
			$mensaje->nombre = $nombre_apellido;
		}
		return json_encode($mensaje);
	}	

	/**
	 * Se crea una persona de tipo cliente directo, este cliente es un cliente adicional para la proforma, ya que una 
	 * proforma puede tener multiples clientes
	 */
	public function doClienteProformas(Request $request){ 

		$nombre_apellido = $request->input('nombre').' '.$request->input('apellido');
		$id_empresa = $this->getIdEmpresa();
		$id_usuario = $this->getIdUsuario();

		try{

			//Validar que tiene documento
			if(!$request->input('documento_cliente')){
				$mensaje = new \StdClass; 
				$mensaje->status = 'ERROR';
				$mensaje->mensaje = 'Falta el documento de identidad';
				return json_encode($mensaje);
			}

			//Validar documento de identidad, que sea unico
			$unique_document = new Persona;
			$unique_document  = $unique_document->where('documento_identidad',$request->input('documento_cliente'));
			$unique_document  = $unique_document->where('id_empresa',$id_empresa);
			if($request->input('documento_dv')){
				$unique_document  = $unique_document->where('dv',$request->input('documento_dv'));
			}
			$unique_document = $unique_document->count();

			if($unique_document){
				$mensaje = new \StdClass; 
				$mensaje->status = 'ERROR';
				$mensaje->mensaje = 'El documento de identidad ya existe en la base de datos';
				return json_encode($mensaje);
			}

			//Crear Persona
	 	$persona = new Persona;
		$persona->id_tipo_identidad = $request->input('tipo_identidad');
		$persona->nombre = $nombre_apellido ;
		$persona->email = $request->input('email_cliente');
		$persona->direccion = $request->input('direccion_cliente');
		$persona->id_empresa = $id_empresa;
		$persona->documento_identidad = $request->input('documento_cliente');
		$persona->dv = $request->input('documento_dv');
		$persona->id_pais = $request->input('pais_cliente');
		$persona->created_at = date('Y-m-d H:i:00');
		$persona->updated_at = date('Y-m-d H:i:00');
		$persona->id_usuario = $id_usuario;


		if($request->input('telefono') != ""){
			$persona->telefono = $request->input('telefono');
		}

			$persona->id_tipo_facturacion = 2; //Factura Neta
			$persona->id_tipo_persona = 11; //CLIENTE DIRECTO
			$persona->id_personeria = 1; //Persona Física
			$persona->save();
			$id_cliente =  $persona->id;

			//Se crea cliente adicional para la proforma
			$proformaCliente = new ProformaCliente;
			$proformaCliente->id_proforma = $request->input('proforma_id');
			$proformaCliente->id_persona = $id_cliente; 
			$proformaCliente->fecha_hora_alta = date('Y-m-d h:m:s');
			$proformaCliente->id_persona_alta = $id_usuario;
			$proformaCliente->save();


			//Se prepara response
			$mensaje = new \StdClass; 
			$mensaje->id = $id_cliente;
			$mensaje->status = 'OK';
			$mensaje->id = $proformaCliente->id;	
			$mensaje->nombre = $nombre_apellido;	
			$mensaje->documento = $request->input('documento_cliente');
			$mensaje->tipo_persona = 'CLIENTE DIRECTO';
			$mensaje->mensaje = 'Se ha guardado la Persona exitosamente';


		} catch(\Exception $e){
			$mensaje = new \StdClass; 
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se ha podido guardar. Intentelo nuevamente !!';
		}

		return json_encode($mensaje);
	}	
	
	public function getEliminrClienteAux(Request $request)
	{
		$mensaje = new \StdClass;
		try{
			$proformaCliente=ProformaCliente::find($request->input('idCliente'));
			$proformaCliente->delete(); //returns true/false
			$mensaje->status = "OK";
			$mensaje->mensaje = 'Ha sido eliminado el cliente de la proforma.';
	    }catch(\Exception $e){
			$mensaje->status = "ERROR";
			$mensaje->mensaje = 'No ha sido eliminado el cliente de la proforma. Intentelo Nuevamente';
		}	 
		return response()->json($mensaje);
	}


	public function doGuardarProforma(Request $request){

		$fechaPeriodo= explode('-', $request->input('periodo'));
		$idComentario = null;

		$check_in = explode('/', $fechaPeriodo[0]);
		$check_out = explode('/', $fechaPeriodo[1]);

		$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]);
		$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]);

		if(!empty($request->input('senha'))){
			$senha =  $request->input('senha');
		}else{
			$senha = 0;
		}

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa > 1){

				$file_seq = DB::select('SELECT public."get_documento"(?,?)', 
					[ 
						Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa,
						'FILE'
						]);


				$valFile = $file_seq[0]->get_documento;
				$file = new FileEmpresas;
				if($request->input('file_codigo')==""){
					$file_codigo = $valFile;
				}else{
					$nombre = "%".trim(strtolower($request->input('file_codigo')))."%";
					$fileEmpresas = FileEmpresas::where('empresa_id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
										->where('file','LIKE',$nombre)
										->get(['id']);
					if(!isset($fileEmpresas[0]->id)){					
						$file_codigo = $request->input('file_codigo');
						$file->file = $file_codigo;
						$file->empresa_id = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;	
						$file->save();
					}else{
						$file_codigo = $request->input('file_codigo');
					}		
				}	
		}else{
			$file_codigo = $request->input('file_codigo');
		}	
		$tipo_facturacion = TipoFacturacion::where('denominacion', $request->input('tipo_factura'))->get();
	 	try{
		 	$proforma = new Proforma;
			$proforma->cliente_id = $request->input('cliente_id');
			$proforma->vendedor_id = $request->input('id_categoria');
			$proforma->proforma_padre = $request->input('expediente');
			$proforma->id_prioridad = $request->input('idPrioridad');
			$proforma->destino_id = $request->input('destino');
			$proforma->check_in = $checkIn;
			$proforma->check_out = $checkOut;
			$proforma->imprimir_precio_unitario_venta = $request->input('imprimir_precio_unitario');
			$proforma->id_promocion = $request->input('id_promocion');
			$proforma->senha = str_replace(',','.', str_replace('.','', $senha));
			if(!empty($request->input('vencimiento'))){
				$vencimiento = explode('/', $request->input('vencimiento'));
				$dateVencimiento = $vencimiento[2]."-".$vencimiento[1]."-".$vencimiento[0];
				$proforma->vencimiento = $dateVencimiento;
			}	
			$proforma->tipo_factura_id = $request->input('tipo_venta');
			if((int)$request->input('id_usuario') == (int)Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario){
				$proforma->id_usuario =  (int)$request->input('id_usuario') ;
				$proforma->id_asistente = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			}else{
				$proforma->id_usuario = (int)$request->input('id_usuario');
				$proforma->id_asistente = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			}
			$equipoDetalles = EquipoDetalle::where('id_persona',(int)$request->input('id_usuario'))->orderBy('id', 'ASC')->first();
			if(isset($equipoDetalles->id_equipo_cabecera)){
				$proforma->id_equipo = $equipoDetalles->id_equipo_cabecera;
			}

			$proforma->id_unidad_negocio = $request->input('negocio');
			$proforma->tiene_incentivo_vendedor_agencia = $request->input('incentivo_agencia');
			$proforma->fecha_alta = date('Y-m-d');
			$proforma->id_moneda_venta = $request->input('moneda');
			$proforma->estado_id = 1;
			$proforma->id_grupo = $request->input('grupo_id');
			$proforma->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
			$proforma->id_tarifa = $request->input('id_tarifa');
			$proforma->file_codigo = $file_codigo;

			if(!empty($request->input('cotizacion'))){
				$proforma->cotizacion =  str_replace(',','.', str_replace('.','', $request->input('cotizacion')));
			}

			if(isset($tipo_facturacion[0]->id)){
				$proforma->tipo_facturacion = $tipo_facturacion[0]->id;
			}
			
			$proforma->save();
			
			$id = $proforma->id;
			if($request->input('comentario') != ""){
				$comentario = new HistoricoComentariosProforma;
				$comentario->fecha_hora = date('Y-m-d H:m:i');
				$comentario->id_proforma = $id;
				$comentario->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
				$comentario->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
				$comentario->comentario = $request->input('comentario');
				$comentario->save();
				$idComentario =$comentario->id;
			}

			if($request->input('pasajero_principal')!== null){
				foreach($request->input('pasajero_principal') as $pasajeros){
			    	$pasajero = new PasajeroProforma;
					$pasajero->id_proforma = $id;
					$pasajero->pasajero = intval($pasajeros);
					$pasajero->save();
		    	}
			}else{
				$pasajero = new PasajeroProforma;
				$pasajero->id_proforma = $id;
				$pasajero->pasajero = intval($request->input('cliente_id'));
				$pasajero->save();
			}

			//INSERTAR EL PRIMER ESTADO DE CONTROL DE PROFORMA
			DB::table('estados_proforma_control')->insert([
			    ['fecha' => date('Y-m-d H:i:00'), 
			    'id_proforma' => $id,
			    'id_usuario' => Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario, 
			    'id_comentario' => $idComentario,
			    'estado_id' => '1']
			]);

			$proformas = Proforma::where('id', $id)->get();
			$mensaje = new \StdClass; 
			$mensaje->id = $id;
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se ha guardado la Persona exitosamente';
			$mensaje->proformas = $proformas;
  		} catch (\Illuminate\Database\QueryException $e){
			$mensaje = new \StdClass; 
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se ha podido guardar. Intentelo nuevamente !!';
		}    
		return json_encode($mensaje);

	}



	/**
	 * Carga detalles en la la vista DETALLES PROFORMA
	 */
	public function detalles(Request $request, $id)
	{
		ini_set('memory_limit', '-1');
		set_time_limit(300);

		$proformas = DB::table('vw_proformas');
		$proformas = $proformas->where('id', $id); 
		$proformas = $proformas->where('id_empresa',$this->getIdEmpresa());
		$proformas = $proformas->get();

		/*echo '<pre>';
		print_r($proformas);*/

		$detalles = DB::table('vw_detalles_proforma');
		$detalles = $detalles->where('id_proforma', $id); 
		$detalles = $detalles->where('activo',true);
		$detalles = $detalles->where('id_empresa',$this->getIdEmpresa());
		$detalles = $detalles->orderBy('item', 'ASC');
		$detalles = $detalles->get();
	    if(!isset($proformas[0]->id)){
	    	flash('La proforma solicitada no existe. Intentelo nuevamente !!')->error();
            return redirect()->route('home');
	    }
		
		$id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario; 
		$btn = $this->btnPermisos(array('id_proforma'=>$id),'detallesProforma');
		//MODAL DE AVISO ACUERDO
		$acuerdo = $this->eventAviso($id,$id_usuario);
		// dd($acuerdo);

		//$empresaMarkup = DB::select("SELECT public.get_markup_minimo_venta(".$this->getIdEmpresa().")");

		$productos = Producto::with('sucursales')
							  ->where('visible', true)
							  ->where('activo', true)
							  ->where('tipo_producto', 'P')
							  ->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							  ->get();
							  
		$productoArray = [];	
		foreach($productos as $key=>$producto){
			if(isset($producto->id_sucursal)){
				$sucursales_id = $producto->id_sucursal;
			}else{
				$sucursales_id ='';
			}
			if($producto->id_sucursal == ''|| $sucursales_id == Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia){
				$productoArray[$key]['id'] = $producto->id;
				$productoArray[$key]['denominacion'] = $producto->denominacion;
				$productoArray[$key]['imprimir_en_factura'] = $producto->imprimir_en_factura;
			}	
		}

		$productosAereos = Producto::where('id_grupos_producto','1')
								   ->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
								   ->whereIn('id', [1, 9, 23595, 23593])
							       ->get();
		$asistencia = Asistencia:: with('moneda')->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
								->where('activo', true)
								->get();	
		$ticket = Ticket::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
		// $tiposTicket = TiposTicket::all();
		 $tiposTicket = DB::select("select id, descripcion, id_empresa
										from tipos_ticket
										where id_empresa is null
										union all
										select id, descripcion, id_empresa
										from tipos_ticket
										where id_empresa = ". Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
 
		$tarjetaCredito = TarjetaCredito::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
		$tipoIdentidad = TipoIdentidad::all();

		$datosAnticipos = Anticipo::where('id_proforma',$id)
									->where('activo',true)
									->get();
		
		$anticipos = false;	
		$cobrado = false;	

		if(isset($datosAnticipos[0]->id)){
			$anticipos = true;
			foreach($datosAnticipos as $key=>$valor){
				if($valor->id_estado == 77){
					$cobrado = true;
				}
			}
		}
		$idTarifa = $proformas[0]->id_tarifa;

		$currency = Currency::where('activo', 'S')
							->where('tipo', 'V')
							->orderBy('currency_id', 'DESC')
							->get();
		$currencysVenta = Currency::where('activo', 'S')
							->where('tipo', 'V')
							->orderBy('currency_id', 'DESC')
							->get();

		$vendedor_empresa = Persona::where('id_tipo_persona', '=' ,'3')
									->where('id_tipo_persona', '=' ,'10')
									->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
									->get();
									
		$estados_factour = EstadoFactour::all();
	
		$clientesArray = DB::select("
									SELECT p.id,p.nombre, p.apellido, p.documento_identidad, tp.denominacion, p.activo
									FROM personas p
									JOIN tipo_persona tp on tp.id = p.id_tipo_persona
									WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
									AND p.activo = true
									AND p.id = ".$proformas[0]->cliente_id."
									AND p.id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
			
		$estado = '';

		if($proformas[0]->file_codigo !=""){
			$nombre = "%".trim(strtolower($proformas[0]->file_codigo))."%";
			$fileEmpresas = FileEmpresas::where('empresa_id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
										->where('file','LIKE',$nombre)
										->get(['id']);
			if(!isset($fileEmpresas[0]->id)){					
				$marcadorfile= 0;	
			}else{
				$marcadorfile= 1;
			}	

		}else{
			$marcadorfile= 0;
		}

    	$permiso_solicitud = DB::select("SELECT COUNT(*) cant 
								               FROM persona_permiso_especiales 
								               WHERE id_permiso = 19 AND id_persona = ".$id_usuario);
	
		$activar_detalle = false;
		$permisoEditar = $this->datosPermiso('detallesProforma',41);
		$permisoEditarUsuario = $this->datosPermiso('detallesProforma',42);
		$permisoEditarIncentivo = $this->datosPermiso('detallesProforma',50);
		$permisoAnularFee = $this->datosPermiso('detallesProforma',62);

		if(isset($proformas[0]->id_usuario)){
			if($proformas[0]->id_asistente == Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario || $proformas[0]->id_usuario == Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario || $permisoEditar == 1){
				$activador = 1;
			}else{
				$activador = 0; 

			}

		$usuarios = Persona::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->whereIn('id_tipo_persona',array(2, 4, 3, 5,6,7))
							->get(['id','nombre','apellido', 'email']);		


			/*SI ES SOLICITUD DE EMISION Y TIENE PERMISOS DE EDITAR SOLICITUD*/
			if($proformas[0]->estado_id == 47 && $permiso_solicitud > 0) {
				$activar_detalle = true;
			}

		}

		$cliente_id = $proformas[0]->cliente_id;
		$vendedor_id = $proformas[0]->vendedor_id;
		$destinoId = $proformas[0]->destino_id;
		$nombreDestino= "";
		$arrayDestino = [];
		$moneda = Currency::where('currency_id', $proformas[0]->id_moneda_venta)->get();
		if(isset($moneda[0]->currency_code)){
			$moneda = $moneda[0]->currency_code;
		}else{
			$moneda ="";
		}

		if($proformas[0]->vendedor_id != null){
			$nombreVendedor = Persona::where('id', $proformas[0]->vendedor_id)->get();
			if(isset($nombreVendedor[0]->nombre_apellido)){
				$nombreVendedors =  $nombreVendedor[0]->nombre_apellido;

			}else{
				$nombreVendedors =  "";	
			}
			
		}else{
			$nombreVendedors =  "";
		}

		$pasajeroProforma = $proformas[0]->pasajero_id;

		$clienteProforma = Persona::where('id', $proformas[0]->cliente_id)->get();

		$tipo_empresa = Empresa::where('id', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first(['tipo_empresa','saldo_pm','pagar_tc_detalle_proforma']); 

		if(isset($clienteProforma[0]->nombre)){
			$cliente = $clienteProforma[0]->nombre;
			$clienteId = $clienteProforma[0]->id;
		}else{
			$cliente = "";
			$clienteId = 0;		
		}		

		if(isset($nombrePasajeroProforma[0]->nombre)){
			$nombrePasajero =  $nombrePasajeroProforma[0]->nombre." ".$nombrePasajeroProforma[0]->apellido;
		}else{
			$nombrePasajero =  "";
		}

		$comentario = HistoricoComentariosProforma::with('persona')
												   ->where('id_proforma',$id)
												   ->orderBy('fecha_hora','DESC')
												   ->get();

		$currency = Currency::where('activo', 'S')
							->orderBy('currency_id', 'DESC')
							->get();
		$pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id FROM personas WHERE id IN (SELECT pasajero FROM pasajeros_proformas WHERE id_proforma=".$id.")");

		if(isset($proformas[0]->pasajero_id)){
			$pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id 
							    FROM personas WHERE personas.id = ".$proformas[0]->pasajero_id);
		}
		$aerolineas = DB::select("select p.id, p.nombre,p.apellido, p.comision_pactada from personas p, producto_persona a where p.id = a.id_persona and a.id_producto = 1");

		$grupos = Grupos::where('id', $proformas[0]->id_grupo)
						->where('empresa_id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();

		$destinos_grupos = [];

		$adjunto = AdjuntoDocumento::where('proforma_id', $id)->get();

		$grupos = Grupos::where('estado_id', 11)
						->where('id', '=', $proformas[0]->id_grupo)
						->where('empresa_id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
						->get(['id','denominacion']);


		$detallesMax = ProformasDetalle::where('id_proforma', $id)->max('item');

		$promociones = Promocion::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
								->whereDate('fecha_baja', '>', date('Y-m-d'))->get();
		$tipoFactura= TipoFactura::all();

		$prioridad = 0;
		if(!empty($proformas[0]->id_prioridad)){
			$prioridad = $proformas[0]->id_prioridad;
		}

		$proformaId = Proforma::where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get(['id']);

		if(!empty($proformas[0]->id_grupo)){
			$idGrupo = $proformas[0]->id_grupo;
		}else{
			$idGrupo = 0;
		}

		if($idGrupo != 0){
			$total = DB::select("select sum(monto) from grupos_anticipo where activo= true and id_grupo = ".$idGrupo);
			$saldo = floatval($proformas[0]->total_proforma) - floatval($total[0]->sum);
		}else{
			$saldo = 0;
		}	
		$negocios = Negocio::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
		$timeLineProforma = DB::select("SELECT * FROM public.vw_time_line WHERE id_proforma = ".$id." order by id desc");
		$tarifas = Tarifa::all();
		$forma_pago = FormaPagoCliente::all();
		$solicitudes = SolicitudFacturaParcial::with('estado','currency', 'cliente', 'usuario')->where('id_proforma', $id)->whereIn('id_estado',[80,81])->get();
		$solicitudesFacturadas = SolicitudFacturaParcial::where('id_proforma', $id)->whereIn('id_estado',[81])->get();
		$usuario_autorizacion = DB::select("
											SELECT CONCAT(personas.nombre, ' ', personas.apellido) as nombre
											FROM personas
											LEFT JOIN autorizaciones ON personas.id = autorizaciones.id_usuario_autorizacion
											WHERE autorizaciones.id_proforma = $id
											ORDER BY autorizaciones.id DESC
												LIMIT 1
										");
		$usuario_post_venta = DB::select("
		SELECT CONCAT(personas.nombre, ' ', personas.apellido) as nombre
		FROM personas
		LEFT JOIN proformas ON proformas.id_usuario_post_venta = personas.id
		WHERE proformas.id = $id
	");
	$reprice = DB::select("
	select lb.reprice from proformas_detalle pf
   				  join libros_compras lb on  pf.id=lb.id_proforma_detalle and lb.reprice=true
				  where pf.id_proforma=$id
	");
	$reprice_item = $this->datosPermiso('detallesProforma',70);
		//dd($usuario_autorizacion);
		$countSolicitudes = count($solicitudes);
		$permisoEmpresaSolicitud = true;
		
		if($countSolicitudes > 0){
			$permisoEmpresaSolicitud = false;
		}

		$tarjetas = TarjetaPersona::with('banco_detalle.currency','banco_detalle.banco_cabecera')
		->where('id_persona',$this->getIdUsuario())
		->get();	

		$paises = Pais::where('habilitado',true)->orderBy('name_es','ASC')->get();
		$clientes_auxiliar = ProformaCliente::with('cliente')->where('id_proforma',$id)->get();

		return view('pages.mc.proforma.detalle')->with(['id'=>$id, 
														'negocios'=>$negocios,
														'prioridad'=>$prioridad,
														'productos'=>$productoArray, 
														'asistencias'=>$asistencia, 
														//'tickets'=>$ticket, 
														'currencys'=>$currency, 
														'pasajeros'=>$pasajero, 
														'tipoIdentidads'=>$tipoIdentidad, 
														'pasajeroProforma'=>intval($pasajeroProforma), 
														'tarjetaCredito'=>$tarjetaCredito, 
														'nombrePasajero'=>$nombrePasajero, 
														//'moneda_venta'=>$moneda_venta,
														'vendedor'=>$nombreVendedors, 
														'destino'=>$destinoId, 
														'moneda'=>$moneda, 
														//'totalFactura'=>$totalFactura,
														'cliente'=>$cliente, 
														//'check'=>$check, 
														'tiposTickets'=>$tiposTicket, 
														'aerolineas'=>$aerolineas, 
														"grupos"=>$grupos, 
														//'proformaPadre'=>$proformaPadre,
														'clienteId'=>$clienteId, 
														'destinosGrupos'=>$destinos_grupos, 
														'detalles'=>$detalles, 
														'estado'=>$estado, 
														'comentario'=>$comentario,
														'adjunto'=>$adjunto, 
														'proformas'=>$proformas, 
														'clientes'=>$clientesArray, 
														'vendedor_id'=>$vendedor_id,
														'cliente_id'=>$cliente_id, 
														//'currencyProforma'=>$currencyProforma,
														'arrayDestino'=>$arrayDestino,
														'activador'=>$activador,
														//'markupEmpresa'=>$empresaMarkup,
													//	'productosVoucher'=>$productosVoucher,
														'tipoFacturas'=>$tipoFactura,
														//'productosAutomantico'=>$productosAutomantico,
														'proformaIds'=>$proformaId,
														//'pasasjeroVoucher'=>$pasasjeroVoucher,
														'item'=>$detallesMax,
														'btn'=> $btn,
														'promociones'=>$promociones,
														'timeLineProforma'=>$timeLineProforma,
														'grupos'=>$grupos,
														'tarifas'=>$tarifas,
														'id_tarifa'=>$idTarifa,
														'activar_detalle'=>$activar_detalle,
														'productosAereos'=>$productosAereos,
														'forma_pago'=>$forma_pago,
														'saldo'=>$saldo,
														'acuerdo'=>$acuerdo,
														'marcadorfile'=>$marcadorfile,
														'permisoEditarUsuario'=>$permisoEditarUsuario,
														'usuarios'=>$usuarios,
														'permisoEditarIncentivo'=>$permisoEditarIncentivo,
														'tipo_empresa'=>$tipo_empresa,
														'anticipos'=>$anticipos,
														'cobrado'=>$cobrado,
														'solicitudes'=>$solicitudes,
														'permisoEmpresaSolicitud'=>$permisoEmpresaSolicitud,
														'datosAnticipos' => $datosAnticipos,
														'usuario_autorizacion' =>$usuario_autorizacion,
														'usuario_post_venta' =>$usuario_post_venta,
														'permisoAnularFee' => $permisoAnularFee,
														'tarjetas' => $tarjetas,
														'paises'=>$paises,
														'reprice'=>$reprice,
														'reprice_item'=>$reprice_item,
														'clientes_auxiliar' => $clientes_auxiliar
														
													]);
	}
	
	public function nombreDestino(Request $request){
		$datos = file_get_contents("../destination_actual.json");
		$destinoJson =  json_decode($datos, true);
		$arrayDestino = [];

		foreach($destinoJson as $key=>$destino){
			/*echo '<pre>';
			print_r($destino);*/
			if($destino['idDestino'] == $request->input('dataProducto')){
				$arrayDestino[0]['id']= $destino['idDestino'];
				$arrayDestino[0]['nombre']= $destino['desDestino'];
			}
		}
		return json_encode($arrayDestino);		
	}

	public function destinoProformaGrupo(Request $request){
		$datos = file_get_contents("../destination_actual.json");
		$destinos =  json_decode($datos, true);
		if(isset($_GET['q'])){
            $search = $_GET['q'];
				foreach($destinos as $key=>$destino){
					$resultado = strpos(strtolower($destino['desDestino']), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		$json = [];
		foreach($destinos as $key=>$row){
		    $json[$row['idDestino']]['text'] = $row['desDestino'];
		}
		echo json_encode($json);
	}


	public function filtrarDatos(Request $request){
		$tickets = Ticket::with('grupo', 'currency', 'persona')
						    ->where('id_tipo_ticket', '=', '1')
						    ->where('id_proforma', null)
						    ->where('id_estado','=','25')
						    //->where('id_currency_costo','=',$request->input('idMonedaProforma'))
						    ->where('id_empresa','=',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->get();


		$ticketsSeleccionados = Ticket::with('grupo', 'currency', 'persona')
						    ->where('id_proforma', '=', $request->input('idProforma'))		
						    ->where('id_detalle_proforma',  '=', $request->input('idDetalle'))
						    ->get();

        $arrayTickets = [];
        $contador = 0;
        $total = 0;
		$key = 0;
		foreach($ticketsSeleccionados as $key=>$ticketS){

				$precio_venta = DB::select('SELECT public.get_venta_ticket('.$ticketS->id.')');
				$ticketS->precio_venta = floatval($precio_venta[0]->get_venta_ticket);
				$ticketS->ckeck = true;
				$total =  $total + $ticketS->precio_venta;
				$arrayTickets['tickets'][$key] = $ticketS;
		}	

		$contador = $key;

		foreach($tickets as $key=>$ticket){
			if(!$tickets->isEmpty()){
					$contador++ ;
					$precio_venta = DB::select('SELECT public.get_venta_ticket('.$ticket->id.')');
					$ticket->precio_venta = floatval($precio_venta[0]->get_venta_ticket);
					$ticket->ckeck = false;
					$arrayTickets['tickets'][$contador] = $ticket;
			}
		}
		
		$arrayTickets['total'] = $total;


		return json_encode($arrayTickets);

	}	
		
	public function filtrarDatosLista(Request $request){
		$tickets = Ticket::with('grupo', 'currency', 'persona')
						    ->where('id_tipo_ticket', '=', '1')
						    ->where('id_proforma', null)
						    ->where('id_estado','=','25')
						    ->where('id_empresa','=',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->get();
		foreach($tickets as $key=>$ticket){
			if(!$tickets->isEmpty()){
				$getVenta = DB::select('SELECT public.get_venta_ticket('.$ticket->id.')');
				$precio_venta = $getVenta[0]->get_venta_ticket;
				$tickets[$key]->precio_venta = $precio_venta;
			}
		}	

		return json_encode($tickets);
	}	

	public function getCosto(Request $request){
		if($request->input('dataTicket')!== null){
			$ticket = $request->input('dataTicket');
		}else{
			$ticket = 0;
		}

		if($request->input('dataAsistencia')!== null){
			$asistencia = $request->input('dataAsistencia');
		}else{
			$asistencia = 0;
		}

		if($request->input('dataDias')!== null){
			$dias = $request->input('dataDias');
		}else{
			$dias = 0;
		}

		$getCosto=DB::select('SELECT public."get_precio_costo"('. $request->input('dataProducto').',0,'. $ticket.','.$asistencia.','.$dias.','.$request->input('dataCantidad').')');

		return json_encode($getCosto[0]->get_precio_costo);
	}	

	public function guardarFila(Request $request){

		$proformasDetalle = new ProformasDetalle;
		$item = DB::select('select COALESCE(item, 0)+1 as item_nuevo from proformas_detalle where id_proforma = '.$request->input('proforma_id').' order by item desc limit 1');
		if(empty($item)){
			$numero_indice = 1;
		}else{
			$numero_indice = $item[0]->item_nuevo;
		}

		$proformasDetalle->item = $numero_indice;
		$proformasDetalle->id_producto = $request->input('productoId_0');
		$proformasDetalle->costo_proveedor = (float)str_replace(',','.', str_replace('.','',$request->input('costo_0')));
		$proformasDetalle->precio_venta = (float)str_replace(',','.', str_replace('.','',$request->input('venta_0')));
		//Precio venta para manipular total venta por reservas de cangoroo
		$proformasDetalle->precio_venta_anterior = (float)str_replace(',','.', str_replace('.','',$request->input('venta_0')));

		if(!empty($request->input('reserva_nemo_id'))){
			$reserva_nemo_id = $request->input('reserva_nemo_id');
			//$proformasDetalle->origen = 'A';name="comision_operador_0"
		}else{
			$reserva_nemo_id = null;
		}

		$proformasDetalle->reserva_nemo_id =  $reserva_nemo_id;

		if($request->input('cantidad_0') == 0 ||$request->input('cantidad_0') == null  ){
			$proformasDetalle->cantidad = 1;
		}else{
			$proformasDetalle->cantidad = $request->input('cantidad_0');
		}

		if(!empty($request->input('comision_operador_0'))){
			$proformasDetalle->comision_moneda_costo = (float)str_replace(',','.', str_replace('.','',$request->input('comision_operador_0')));
			$comision = $request->input('comision_operador_0');
		}

		if(!empty($request->input('exentos_0'))){
			$proformasDetalle->porcion_exenta = (float)str_replace(',','.', str_replace('.','',$request->input('exentos_0')));
		}	 

		if(!empty($request->input('gravada_0'))){
			$proformasDetalle->porcion_gravada = (float)str_replace(',','.', str_replace('.','',$request->input('gravada_0')));
		}	

		if(!empty($request->input('fee_0'))){
			$proformasDetalle->fee_unico = (float)str_replace(',','.', str_replace('.','',$request->input('fee_0')));
		}	

		if(!empty($request->input('confirmacion_0'))){
			$proformasDetalle->cod_confirmacion = $request->input('confirmacion_0');
		}	
		if(!empty($request->input('periodo_0'))){

			$fechas = explode(' - ', $request->input('periodo_0'));

			$fecha_in = explode('/',$fechas[0]);
			$fecha_out = explode('/',$fechas[1]);

			$proformasDetalle->fecha_in = $fecha_in[2].'-'.$fecha_in[1].'-'.$fecha_in[0];
			$proformasDetalle->fecha_out = $fecha_out[2].'-'.$fecha_out[1].'-'.$fecha_out[0];
		}
		if(!empty($request->input('proveedor_id_0'))){
			$proformasDetalle->id_proveedor = $request->input('proveedor_id_0');
		}else{
			$proformasDetalle->id_proveedor	= 0;
		}	
		if(!empty($request->input('tasas_0'))){
			if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1){
				$proformasDetalle->monto_no_comisionable_proveedor = (float)str_replace(',','.', str_replace('.','',$request->input('tasas_0')));	
			}else{
				$proformasDetalle->monto_no_comisionable_proveedor = (float)$request->input('tasas_0');	
			}
		} else {
			$proformasDetalle->monto_no_comisionable_proveedor = 0;
		}	

		if(!empty($request->input('prestador_id_0'))){
			$proformasDetalle->id_prestador = $request->input('prestador_id_0');
		}else{
			$proformasDetalle->id_prestador = 0;
		}	
		/*	if(!empty($request->input('base_comisionable_0'))){
			$proformasDetalle->base_comisionable = $request->input('base_comisionable_0');
		}else{
			$proformasDetalle->base_comisionable = 0;
		}*/

		$proformasDetalle->descripcion = $request->input('descrip_factura_0');

		$proformasDetalle->currency_venta_id = $request->input('currency_venta');

		$proformasDetalle->currency_costo_id = $request->input('moneda_compra_0');

		//$proformasDetalle->venta_neta =  (float)str_replace(',','.', str_replace('.','',$request->input('venta_neta_0')));

		//$proformasDetalle->monto_comision = $request->input('monto_comision_0');

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1){
			$proformasDetalle->markup = (float)str_replace(',','.', str_replace('.','',$request->input('markup_0')));	
		}else{
			$proformasDetalle->markup = (float)$request->input('markup_0');
		}

		$proformasDetalle->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		
		if(!empty($request->input('comision_agencia_0'))){
			$proformasDetalle->porcentaje_comision_agencia = $request->input('comision_agencia_0');
			$comision = $request->input('comision_agencia_0');
		}else{
			$proformasDetalle->porcentaje_comision_agencia = 0;
		}	

		if(!empty($request->input('tarifa_asistencia_0'))){
			$proformasDetalle->id_tarifa_asistencia = $request->input('tarifa_asistencia_0');
			$proformasDetalle->asistencia_cantidad_personas = $request->input('cantidad_personas_0');
		}else{
			$proformasDetalle->id_tarifa_asistencia = 0;
			$proformasDetalle->asistencia_cantidad_personas = 0;
		}	

		if(!empty($request->input('cantidad_dias_0'))){
			$proformasDetalle->asistencia_dias = $request->input('cantidad_dias_0');
		}else{
			$proformasDetalle->asistencia_dias = 0;
		}
		if($request->input('cantidad_personas_0') != ""){
			$proformasDetalle->cantidad_pasajero = $request->input('cantidad_personas_0');
		}else{
			$proformasDetalle->cantidad_pasajero = 0;
		}
		if($request->input('tarjeta_id_0') != ""){
			$proformasDetalle->tarjeta_id = $request->input('tarjeta_id_0');
		}
		if($request->input('cod_autorizacion_0') != ""){
			$proformasDetalle->cod_autorizacion = $request->input('cod_autorizacion_0');
		}
		
		$proformasDetalle->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

		$proformasDetalle->activo = true;

		if($request->input('id_tipo_traslado_0') != ""){
			$proformasDetalle->id_tipo_traslado = $request->input('id_tipo_traslado_0');
		}

		if($request->input('horaIn_0') != ""){
			$proformasDetalle->hora_in = date("H:i:s",strtotime($request->input('horaIn_0')));
		}

		if($request->input('horaOut_0') != ""){
			$proformasDetalle->hora_out = date("H:i:s",strtotime($request->input('horaOut_0')));
		}

		if($request->input('vueloIn_0') != ""){
			$proformasDetalle->vuelo_in = $request->input('vueloIn_0');
		}

		if($request->input('vueloOut_0') != ""){
			$proformasDetalle->vuelo_out = $request->input('vueloOut_0');
		}

		if($request->input('fecha_gasto_0') != ""){
			$fecha_gasto = explode('/',$request->input('fecha_gasto_0'));
			$proformasDetalle->fecha_gasto = $fecha_gasto[2].'-'.$fecha_gasto[1].'-'.$fecha_gasto[0];
		}

		if($request->input('gravado_costo_0') != ""){
			$proformasDetalle->costo_gravado = (float)str_replace(',','.', str_replace('.','',$request->input('gravado_costo_0')));
		}	

		if($request->input('monto_gasto_0') != ""){
			$proformasDetalle->monto_gasto = (float)str_replace(',','.', str_replace('.','',$request->input('monto_gasto_0')));
		}	

		if($request->input('vencimientos') != ""){
			$fechasInicio = $request->input('vencimientos');
			$fechaGasto = explode('/', $fechasInicio);
			$proformasDetalle->fecha_pago_proveedor =  $fechaGasto[2].'-'.$fechaGasto[1].'-'.$fechaGasto[0]; 
		}

		$cotizacionCosto=DB::select('SELECT public."get_cotizacion"('.$request->input('moneda_compra_0').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.','.Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.')');

		$proformasDetalle->cotizacion_costo = $cotizacionCosto[0]->get_cotizacion;
		
		$proformasDetalle->id_proforma = $request->input('proforma_id');

	    //try{

			$proformasDetalle->save();
			$id = $proformasDetalle->id;

			DB::table('proformas_detalle')
			->where('id',$id)
			->update([
					'costo_proveedor'=>(float)str_replace(',','.', str_replace('.','',$request->input('costo_0')))
					]);

			if(!empty($request->input('reserva_nemo_id'))){
				$reservaProforma = DB::select("SELECT * FROM generar_fee_nemo(".$id.",".$this->getIdEmpresa().")");
			} 
			
			if($request->input('tickets') != ""){
				$tickets = explode(',', $request->input('tickets'));
				foreach($tickets as $key=>$ticket){
					DB::table('tickets')
					        ->where('id',$ticket)
					        ->update([
					        		'id_detalle_proforma'=>$id,
					            	'id_proforma'=>$request->input('proforma_id'),
					            	'id_estado'=>26
					            	]);
				}
				$actualizar_precio_costo_ticket = DB::select('SELECT public.get_costo_cotizado_ticket('.$id.', '.$request->input('moneda_compra_0').', '.$request->input('currency_venta').', 0)');
			}
			if($request->input('reserva_nemo_id') != ""){
				DB::table('reservas_nemo')
					->where('id',$request->input('reserva_nemo_id'))
					->update([
							'id_detalle_proforma'=>$id,
							'id_proforma'=>$request->input('proforma_id'),
							]);
			}	

			//Marcar las reservas asociadas a cangoroo
			if($request->input('grupo_reserva_cangoroo_id') && $request->input('reserva_cangoroo_id') && $request->input('tipo_reserva_cangoroo')){

					if($request->input('tipo_reserva_cangoroo') == 'HOTEL'){
						DB::table('alojamientos_cangoroo')
						 ->where('id_reserva_cangoroo',$request->input('grupo_reserva_cangoroo_id'))
						 ->where('id',$request->input('reserva_cangoroo_id'))
						 ->update([
									'id_proforma' => $request->input('proforma_id'),
								    'id_proforma_detalle' => $id
								  ]);
					} else if ($request->input('tipo_reserva_cangoroo') == 'TOUR') {
						DB::table('circuitos_cangoroo')
						->where('id_reserva_cangoroo',$request->input('grupo_reserva_cangoroo_id'))
						->where('id',$request->input('reserva_cangoroo_id'))
						->update([
								   'id_proforma' => $request->input('proforma_id'),
								   'id_proforma_detalle' => $id
								 ]);
					} else {
						DB::table('traslados_cangoroo')
						->where('id_reserva_cangoroo',$request->input('grupo_reserva_cangoroo_id'))
						->where('id',$request->input('reserva_cangoroo_id'))
						->update([
								   'id_proforma' => $request->input('proforma_id'),
								   'id_proforma_detalle' => $id
								 ]);
					}

			}

			
			

			$actualizarSenha = DB::select('SELECT public.actualizar_senha('.$request->input('proforma_id').', '.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');
			
			$actualizar_saldo = DB::select("SELECT actualizar_saldo_proforma(".$request->input('proforma_id').",".$id.",1)");

			$actualizar_proforma_cabecera = DB::select('SELECT public.actualizar_proforma_cabecera('.$request->input('proforma_id').')');

			$this->calcularFeeCangoroo($request->input('proforma_id'));
			ProformasDetalle::where('id_proforma', $request->input('proforma_id'))
							->where('activo', true)
							->update(['precio_costo' => DB::raw('precio_costo')]);
			
			$this->calcularFeeAdministrativo($request->input('proforma_id'));

			
			$proforma = Proforma::where('id',  $request->input('proforma_id'))
								->get();
			$detalle_proforma = ProformasDetalle::with('producto','proveedor','prestador', 'currencyVenta')->where('id', $id)->get();						
			$precio_costo_proveedor = $detalle_proforma[0]->costo_proveedor;
			$porcion_exenta = $detalle_proforma[0]->porcion_exenta;
			$porcion_gravada = $detalle_proforma[0]->porcion_gravada;
			$vencimiento = $proforma[0]->vencimiento;
			$senha = $proforma[0]->senha;
			$total_bruto_facturar = $proforma[0]->total_bruto_facturar; 
			$total_neto_facturar = $proforma[0]->total_neto_facturar;
			$total_iva = $proforma[0]->total_iva;
			$total_comision = $proforma[0]->total_comision;
			$incentivo_vendedor_agencia = $proforma[0]->incentivo_vendedor_agencia;
			$total_costos = $proforma[0]->total_costos;
			$markup = $proforma[0]->markup;
			$total_exentas = $proforma[0]->total_exentas;
			$total_gravadas = $proforma[0]->total_gravadas;
			$total_no_comisiona = $proforma[0]->total_no_comisiona;
			$renta = $proforma[0]->renta;
			$porcentaje_ganancia = $proforma[0]->porcentaje_ganancia;
			$ganancia_venta = $proforma[0]->ganancia_venta;
			$grupo_id = $proforma[0]->id_grupo;
			$porcentaje_comision = $proforma[0]->porcentaje_comision_agencia;
		    if($detalle_proforma[0]->comision_operador == 0){
				$porcentaje_comision = (int)$detalle_proforma[0]->porcentaje_comision_agencia;
			 }else{
				$porcentaje_comision = number_format($detalle_proforma[0]->comision_operador,2,",",".");
			} 
			$tasas = $proforma[0]->monto_no_comisionable_proveedor;
			$origen = $detalle_proforma[0]->origen;
			$costo_total_proveedor = (float)$detalle_proforma[0]->costo_proveedor+(float)$detalle_proforma[0]->costo_gravado;
			$proveedor = $detalle_proforma[0]->proveedor->nombre;
			$prestador = $detalle_proforma[0]->prestador->nombre;
			
			$mensaje = new \StdClass; 
			$mensaje->id = $id;
			$mensaje->item = $request->input('item');
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se ha guardado el Detalle';
			$mensaje->vencimiento = $vencimiento;
			$mensaje->senha = $senha;
			$mensaje->total_bruto_facturar= number_format($total_bruto_facturar,2,",",".") ; 
			$mensaje->total_neto_facturar= number_format($total_neto_facturar,2,",","."); 
			$mensaje->total_iva= number_format($total_iva,2,",","."); 
			$mensaje->total_comision= number_format($total_comision,2,",","."); 
			$mensaje->incentivo_vendedor_agencia= number_format($incentivo_vendedor_agencia,2,",",".") ; 
			$mensaje->total_costos= number_format($total_costos,2,",","."); 
			$mensaje->markup= number_format($detalle_proforma[0]->porcentaje_renta_item,2,",",".")." / ".number_format($detalle_proforma[0]->renta_minima_producto,2,",","."); 
			$mensaje->total_exentas= number_format($total_exentas,2,",","."); 
			$mensaje->total_gravadas = number_format($total_gravadas,2,",",".");
			$mensaje->total_no_comisiona= number_format($total_no_comisiona,2,",","."); 
			$mensaje->renta= number_format($renta,2,",","."); 
			$mensaje->porcentaje_ganancia= number_format($porcentaje_ganancia,2,",","."); 
			$mensaje->ganancia_venta= number_format($ganancia_venta,2,",","."); 
			$mensaje->precio_costo_proveedor = $precio_costo_proveedor;
			$mensaje->tasas= number_format($tasas,2,",","."); 
			$mensaje->grupo_id = $grupo_id;
			$mensaje->porcentaje_comision = $porcentaje_comision;
			$mensaje->porcion_exenta = number_format($porcion_exenta,2,",","."); 
			$mensaje->porcion_gravada = number_format($porcion_gravada,2,",","."); 
			$mensaje->proveedor = $proveedor;
			$mensaje->prestador = $prestador;
			$mensaje->origen =$origen;
			$mensaje->reserva_nemo_id =$request->input('reserva_nemo_id');
			$mensaje->costo_total_proveedor =$costo_total_proveedor;
			$mensaje->saldo_factura =$actualizar_saldo[0]->actualizar_saldo_proforma;
			$mensaje->id_producto =$request->input('dataProducto');
			$mensaje->pagado_con_tc = $detalle_proforma[0]->pagado_con_tc;
			$mensaje->producto_migrar =  $detalle_proforma[0]->producto->pago_tc_detalle_proforma;
			$mensaje->prestador_migrar = $detalle_proforma[0]->proveedor->migrar;
			$mensaje->pagar_tc_detalle_proforma = Empresa::find($this->getIdEmpresa())->pagar_tc_detalle_proforma; //Habilita el modulo de pago TC en detalle proforma
			
			if($detalle_proforma[0]->cumple_rentabilidad_minima == true){
				$mensaje->color = 'green';
			}else{
				$mensaje->color = 'red';
			}
		/*	  }catch(\Exception $e){
		 	$mensaje = new \StdClass; 
		 	$mensaje->status = 'ERROR';
		 	$mensaje->mensaje = 'No se ha guardado el Detalle, Intentelo nuevamente';
		 } */
		return json_encode($mensaje);
	}	

	public function getPasajero(Request $request){
		$mensaje = new \StdClass; 
	 	$persona = new Persona;
		$persona->nombre_apellido = $request->input('dataNombre')." ".$request->input('dataApellido');
		$persona->id_tipo_pasajero = $request->input('dataTipo');
		$persona->pasajero_edad = $request->input('dataEdad');
		$persona->activo = true;
		$persona->fecha_alta = date('Y-m-d');
		$persona->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		$persona->id_pais = 2;
		$persona->id_tipo_persona = 13;
		$persona->id_personeria = 1;
		try{
			$persona->save();
			$id = Persona::select('id')->max('id');
			$mensaje->id = $id;
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se ha guardado la Persona exitosamente';
		} catch(\Exception $e){
			$id = 0;
			$mensaje->id = $id;
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se a guardado la Persona exitosamente';
		}
		return json_encode($mensaje);	
	}

	public function getDetallesPasajero(Request $request){
		$detalle = explode(',', $request->input('dataPasajero'));
		$resultadosArray = [];
		foreach($detalle as $key=>$idDetalle){
			$detallesArray = Persona::where('id', $idDetalle)->get();
			$resultadosArray[$key]['id'] = $detallesArray[0]['id'];
			$resultadosArray[$key]['nombre_apellido'] = $detallesArray[0]['nombre_apellido'];
			$resultadosArray[$key]['pasajero_edad'] = $detallesArray[0]['pasajero_edad'];
			$resultadosArray[$key]['id_tipo_pasajero'] = $detallesArray[0]['id_tipo_pasajero'];
		}
		return json_encode($resultadosArray);	
	}	

	public function getProveedor(Request $request){
		$grupoProducto = Producto::where('id',  '=', $request->input('dataProducto'))
									->first();
		$id_grupo_producto = $grupoProducto->id_grupos_producto;
		if($id_grupo_producto == 1){
			$producto = $request->input('dataProducto');
			$aerolineas1 = DB::select('select id as out_id_persona, nombre as out_nombre, id_tipo_facturacion as out_tipo from personas where id_empresa = '.$this->getIdEmpresa().' and id in (select distinct(id_proveedor) from tickets where id_empresa = '.$this->getIdEmpresa().')');  
			$aerolineas2 = DB::select('select * from get_producto_persona(?,?)', [$producto,$this->getIdEmpresa()]);
			$aerolineas = array_merge($aerolineas1, $aerolineas2);
			$resultado = [];
			foreach($aerolineas as $key=>$aerolinea){
				$resultado[$key]['id'] = $aerolinea->out_id_persona;
				$resultado[$key]['nombre'] = $aerolinea->out_nombre;
				$resultado[$key]['tipo'] =  $aerolinea->out_tipo;
			}
			usort($resultado, function($a, $b){
				return strcmp($a["nombre"], $b["nombre"]);
			});
		}else{
			$producto = $request->input('dataProducto');
			$getProveedors =DB::select('SELECT public."get_producto_persona"('.$producto.','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');

			$resultado = [];
			foreach($getProveedors as $key=>$getProveedor){
				$valor = str_replace(')', '',str_replace('"', '',str_replace('(', '', $getProveedor->get_producto_persona)));
				$proveedor = explode(',',$valor);
				$resultado[$key]['id'] = $proveedor[0];
				$resultado[$key]['nombre'] = $proveedor[1];
				$resultado[$key]['tipo'] = $proveedor[2];
			}
			usort($resultado, function($a, $b){
			return strcmp($a["nombre"], $b["nombre"]);
			});
		}
		return json_encode($resultado);	
	}

		public function getPrestador(Request $request){
		/*$grupoProducto = Producto::where('id',  '=', $request->input('dataProducto'))
									->first();
		$id_grupo_producto = $grupoProducto->id_grupos_producto;*/

		$prestador = DB::select("select p.id, p.nombre, p.apellido 
								from personas p 
								where (p.id_tipo_persona = 15 or p.id_tipo_persona = 14 or p.id_tipo_persona = 3 or p.id_tipo_persona = 2 or p.id_tipo_persona= 8 or p.id_tipo_persona= 10) 
								and p.activo= true 
								and p.id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
		return json_encode($prestador);	
	}

	public function getVoucherUpdate(Request $request){

		$voucher = Voucher::with('prestador')
				   ->where('id_proforma_detalle', $request->input('dataVoucher'))
  				   ->where('activo', true)
				   ->get();

		$producto_id_proforma_detalle = '';

		$proforma_detalle = ProformasDetalle::select('id_producto','id')->where('id', $request->input('dataVoucher'))->first();
		if($proforma_detalle){
			$producto_id_proforma_detalle = $proforma_detalle->id_producto;
		}


		return json_encode([ 'vouchers' => $voucher , 'producto_id_proforma_detalle' => $producto_id_proforma_detalle]);					
	}

	public function getTarjetaPost(Request $request){
			$detalle = ProformasDetalle::where('id', $request->input('dataVoucherId'))->get();
			$mensaje = new \StdClass;
			$tarjetaId = (int)$detalle[0]->tarjeta_id;
			if($detalle[0]->tarjeta_id != 0){
				$tarjetaCredito = TarjetaCredito::where('id', $detalle[0]->tarjeta_id)
												 ->get();
				$mensaje->tarjeta_id = $detalle[0]->tarjeta_id;
				$mensaje->cod_autorizacion = $detalle[0]->cod_autorizacion;
				$mensaje->nro_tarjeta = $tarjetaCredito[0]->nro_tarjeta;
			}else{
				$mensaje->tarjeta_id = 0;
				$mensaje->cod_autorizacion = "";
				$mensaje->nro_tarjeta = "";
			}	
			return json_encode($mensaje);
	}

	public function updateDetalle(Request $request){

		$detalle = ProformasDetalle::findOrFail($request->input('dataDetalleProforma'));

		if($request->input('dataTarjetaId') != ""){
			$tarjeta_id = $request->input('dataTarjetaId');
		}else{
			$tarjeta_id = 0;
		}

		if($request->input('dataCodAutorizacion') != ""){
			$cod_autorizacion = $request->input('dataCodAutorizacion');
		}else{
			$cod_autorizacion = 0;
		}

		if($request->input('dataFechaGasto') != ""){
			$fechasInicio = $request->input('dataFechaGasto');
			$fechaGastoBase = explode('/', $fechasInicio);
			$fechaGasto = $fechaGastoBase[2]."-".$fechaGastoBase[1]."-".$fechaGastoBase[0];
		}else{
			$fechaGasto = null;
		}

		if($request->input('dataCantidad') == 0 ||$request->input('dataCantidad') == null  ){
			$cantidad = 1;
		}else{
			$cantidad = $request->input('dataCantidad');
		}

		if($request->input('dataVencimiento') != ""){
			$fechasInicio = $request->input('dataVencimiento');
			$fechaGast = explode('/', $fechasInicio);
			$fechaVencimiento = $fechaGast[2]."-".$fechaGast[1]."-".$fechaGast[0];
		}else{
			if($detalle->fecha_pago_proveedor != null){
				$fechaVencimiento = $detalle->fecha_pago_proveedor;
			}else{
				$fechaVencimiento = null;
			}
		}

		if($request->input('dataPeriodo') != ""){

			$fechaPeriodo= explode('-', $request->input('dataPeriodo'));

			$check_in = explode('/', $fechaPeriodo[0]);
			$check_out = explode('/', $fechaPeriodo[1]);

			$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]);
			$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]);
		}

		if($request->input('dataHoraIn') != ""){
			$dataHoraIn = $request->input('dataHoraIn');
		}else{
			$dataHoraIn = null;
		}	

		if($request->input('dataHoraOut') != ""){
			$dataHoraOut = $request->input('dataHoraOut');
		}else{
			$dataHoraOut = null;
		}	

		if($request->input('dataVueloIn') != ""){
			$dataVueloIn = $request->input('dataVueloIn');
		}else{
			$dataVueloIn = null;
		}	

		if($request->input('dataVueloOut') != ""){
			$dataVueloOut = $request->input('dataVueloOut');
		}else{
			$dataVueloOut = null;
		}	

		if($request->input('dataTipoTraslado') != ""){
			$dataTipoTraslado = $request->input('dataTipoTraslado');
		}else{
			$dataTipoTraslado = null;
		}	

		if($request->input('dataCantidadDias') != ""){
			$cantidadDias = $request->input('dataCantidadDias');
		}else{
			$cantidadDias = 0;
		}	

		if($request->input('dataTarifaAsistencia') != ""){
			$tarifaAsistencia = $request->input('dataTarifaAsistencia');
		}else{
			$tarifaAsistencia = 0;
		}	

		if($request->input('dataCantidadPersonas') != ""){
			$personaAsistencia = $request->input('dataCantidadPersonas');
		}else{
			$personaAsistencia = 0;
		}	

		if($request->input('dataProveedor') != ""){
			$dataProveedor = $request->input('dataProveedor');
		}else{
			$dataProveedor = 0;
		}	

		if($request->input('dataBaseComisionable') != ""){
			$dataBaseComisionable = $request->input('dataBaseComisionable');
		}else{
			$dataBaseComisionable = 0;
		}	

		if($request->input('dataPrestador') != ""){
			$dataPrestador = $request->input('dataPrestador');
		}else{
			$dataPrestador = 0;
		}	

		if(!empty($request->input('dataExento'))){
			$porcionExenta = (float)str_replace(',','.', str_replace('.','',$request->input('dataExento')));
		}else{
			 $porcionExenta = 0;
		}	

		if(!empty($request->input('dataGravada'))){
			$porcionGravada = (float)str_replace(',','.', str_replace('.','',$request->input('dataGravada')));
		}else{
			$porcionGravada =0;
		}

		if(!empty($request->input('dataGasto'))){
			$dataGasto = (float)str_replace(',','.', str_replace('.','',$request->input('dataGasto')));
		}else{
			 $dataGasto = 0;
		}	
		 if(!empty($request->input('dataComison'))){	
			$comision_operador = (float)str_replace(',','.', str_replace('.','',$request->input('dataComison')));
		}else{
			$comision_operador = 0;
		} 
		if(!empty($request->input('dataCostoGravada'))){	
			$costoGravada = (float)str_replace(',','.', str_replace('.','',$request->input('dataCostoGravada')));
		}else{
			$costoGravada = 0;
		} 

		//Si el precio de venta es diferente al precio de venta de la BD, se actualiza el precio de venta
		$precio_venta = str_replace(',','.', str_replace('.','', $request->input('dataVenta')));
		$precio_venta_anterior = $detalle->precio_venta_anterior;
		if($detalle->precio_venta != $precio_venta){
			$precio_venta_anterior = $precio_venta;
		} 

		$arr_detalle = [
			'item'=>$request->input('dataItem'),
			'id_producto'=>$request->input('dataProducto'),
			'costo_proveedor'=>str_replace(',','.', str_replace('.','', $request->input('dataCosto'))),
			'precio_venta'=>$precio_venta,
			'precio_venta_anterior'=>$precio_venta_anterior, //Campo para manipular venta por reservas de cangoroo
			'cantidad'=>$cantidad,
			'cod_confirmacion'=>$request->input('dataConfirmacion'),
			'fecha_in'=>$checkIn,
			'fecha_out'=>$checkOut,
			'id_proveedor'=>$dataProveedor,
			'id_prestador'=>$dataPrestador,
			'descripcion'=>$request->input('dataDescripcion'),
			'currency_venta_id'=>$request->input('dataCurrency_venta'),
			'currency_costo_id'=>$request->input('dataMonedaCompra'),
			'markup'=>$request->input('dataMarkup'),
			'porcentaje_comision_agencia'=>$request->input('dataComisionAgencia'),
			'tarjeta_id'=>$tarjeta_id,
			'cod_autorizacion'=>$cod_autorizacion,
			'id_usuario'=>Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
			'fecha_pago_proveedor'=>$fechaVencimiento,
			'id_proforma'=>$request->input('dataProforma_id_'),
			'hora_in'=>$dataHoraIn,
			'hora_out'=>$dataHoraOut,
			'vuelo_in'=>$dataVueloIn,
			'vuelo_out'=>$dataVueloOut,
			'id_tipo_traslado'=>$dataTipoTraslado,
			'monto_no_comisionable_proveedor'=>str_replace(',','.', str_replace('.','', $request->input('dataTasas'))),
			'id_tarifa_asistencia'=>$tarifaAsistencia,
			'asistencia_dias'=>$cantidadDias,
			'asistencia_cantidad_personas'=>$personaAsistencia,
			'porcion_exenta'=>$porcionExenta,
			'comision_moneda_costo'=>$comision_operador,
			'porcion_gravada'=>$porcionGravada,
			'costo_gravado'=>$costoGravada,
			'fecha_gasto'=>$fechaGasto,
			'monto_gasto'=>$dataGasto,
			//'base_comisionable'=>$dataBaseComisionable
			];

		//Validar que la moneda del costo siga siendo igual, sino modificar la cotizacion
		if($detalle->cotizacion_costo != $request->input('dataMonedaCompra')){
			$cotizacionCosto=DB::select('SELECT public."get_cotizacion"(?,?,?)',[$request->input('dataMonedaCompra'),$this->getIdEmpresa(),$this->getIdUsuario()]);
			$arr_detalle['cotizacion_costo'] = $cotizacionCosto[0]->get_cotizacion;
		}
		

		DB::table('proformas_detalle')
					        ->where('id',$request->input('dataDetalleProforma'))
					        ->update($arr_detalle);
			
			$this->calcularFeeAdministrativo($request->input('dataProforma_id_'));
			$this->calcularFeeCangoroo($request->input('dataProforma_id_'), false);
			
			$grupoProducto = Producto::where('id',  '=', $request->input('dataProducto'))
						    ->first();
			$id_grupo_producto = $grupoProducto->id_grupos_producto;

			//Grupo aereo
			if($id_grupo_producto == 1){

				$ticketBase = Ticket::where('id_proforma', $request->input('dataProforma_id_'))
								->where('id_detalle_proforma', $request->input('dataDetalleProforma'))
								->get(['id']);

				foreach($ticketBase as $ticketBas){
					$estado = $ticketBas->id_estado ;
					if($estado == 33){
						$id_estado = 33;
					}else{
						$id_estado = 25;
					}

					DB::table('tickets')
							->where('id',$ticketBas->id)
							->update([
									'id_detalle_proforma'=>null,
									'id_proforma'=>null,
									'id_estado'=>$id_estado
									]);
				}									

				if($request->input('dataConfirmacion') != ""){
					$tickets = explode(',', $request->input('dataConfirmacion'));
					foreach($tickets as $key=>$ticket){
						DB::table('tickets')
						        ->where('numero_amadeus',$ticket)
						        ->update([
						        		'id_detalle_proforma'=>$request->input('dataDetalleProforma'),
						            	'id_proforma'=>$request->input('dataProforma_id_'),
						            	'id_estado'=>26
						            	]);
					}
				}
			}	

		$detalleProforma = ProformasDetalle::with('producto','proveedor','prestador', 'currencyVenta')->where('id', $request->input('dataDetalleProforma'))->get();					
    	$actualizar_saldo = DB::select("SELECT actualizar_saldo_proforma_update(".$request->input('dataProforma_id_').",".$request->input('dataDetalleProforma').",1)");									
		

		$mensaje = new \StdClass; 
		$mensaje->id = $request->input('dataDetalleProforma');
		$mensaje->item = $request->input('dataItem');
		$mensaje->status = 'OK';
		$mensaje->mensaje = 'Se ha guardado el Detalle';
		$mensaje->detalle = $detalleProforma;
		$mensaje->pagar_tc_detalle_proforma = Empresa::find($this->getIdEmpresa())->pagar_tc_detalle_proforma; 
		return json_encode($mensaje);	

	}


	public function doGuardarVoucher(Request $request){


		//Genera un numero unico para identificar que voucher vino primero
		$idInsercion = DB::select("select nextval('public.insercion_voucher')");

		$valInsercion = $idInsercion[0]->nextval;

		$detalleProforma = ProformasDetalle::where('id', $request->input('detalle_proforma'))->get(['item']);
		$mensaje = new \StdClass;

		try{

			DB::beginTransaction();

			//Recibe el formulario y recorre cada linea para generar un voucher
			foreach($request->base as $key1=>$base){
				$voucher = new Voucher;
				$voucher->created_at =  date('Y-m-d H:i:00');
				$voucher->id_insercion = $valInsercion;
				$voucher->id_proforma = $request->input('proforma');
				$voucher->id_proforma_detalle = $request->input('detalle_proforma');
				$voucher->descripcion_servicio = $base['descripcion'];
				$voucher->comentario = html_entity_decode($base['comentario']);
				$voucher->cant_adultos = $base['adultos'];
				$voucher->cant_child = $base['ninhos'];
				if(isset($base['otros_pasajeros'])){
					$voucher->otros_pasajeros = $base['otros_pasajeros'];
				}else{
					$voucher->otros_pasajeros ="";
				}

				if(isset($base['id_prestador'])){
					$voucher->id_prestador = $base['id_prestador'];
				}else{
					$voucher->id_prestador =0;
				}

				$voucher->pasajero_principal = $base['pasajero_principal'];
				$fechaPeriodo= explode(' - ', $base['fecha_in_out']);
				$check_in = explode('/', $fechaPeriodo[0]);
				$check_out = explode('/', $fechaPeriodo[1]);
				$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]);
				$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]);
				//if($base['producto'] == '2' || $base['producto'] == '5'||$base['producto'] == '16' ){
					$date1 = new \DateTime($checkIn);
					$date2 = new \DateTime($checkOut);
					$diff = $date1->diff($date2);
					$voucher->noches = $diff->days;
				/*}else{
					$voucher->noches = 0;
				}*/
				// will output 2 days
				$voucher->fecha_in = $checkIn;
				$voucher->fecha_out = $checkOut;
				$voucher->producto_id = $base['producto'];
				$voucher->id_estados = 8;
				$voucher->activo = true;
				$voucher->id_usuario= Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
				$voucher->fecha= date('Y-m-d H:m:i');
				$voucher->save();
					
			}

			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se han guardado los Vouchers exitosamente';
			if(isset($detalleProforma[0]->item)){
				$mensaje->item = $detalleProforma[0]->item;
			}

			DB::commit();

		} catch(\Exception $e){

			Log::error($e);
			DB::rollback();

			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se han guardado los vouchers, intentelo Nuevamente';
			$mensaje->item = 0;
		}

	
		return json_encode($mensaje);	
	}

	public function getComisionAgencia(Request $request){
		$cliente_id = Proforma::where('id', $request->input('dataProforma'))->get(['cliente_id']);

		$getPorComAgencia =DB::select('SELECT public."get_porcentaje_com_agencia"('.  $request->input('dataProducto') .','.$cliente_id[0]->cliente_id.','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');

		$porcentaje = $getPorComAgencia[0]->get_porcentaje_com_agencia;

		if($porcentaje != null){
			$porcentaje = $porcentaje;
		}else{
			$porcentaje = 0;
		}

		return json_encode($porcentaje);
	}				

	public function deleteLinea(Request $request){

		$mensaje = new \StdClass;	
		$nemo = 0;			        
		$detalleProforma = ProformasDetalle::where('id', $request->input('dataLinea'))->get(['id_proforma','item', 'reserva_nemo_id','id_libro_compra']);
		if($detalleProforma[0]->id_libro_compra == ""){
			DB::table('proformas_detalle')
								->where('id',$request->input('dataLinea'))
								->update([
										'activo'=>false,
										'id_usuario_anulacion'=>Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
										'fecha_anulacion'=>date('Y-m-d H:m:i')
										]);

			//Anular vouchers del detalle
			DB::table('vouchers')
			->where('id_proforma_detalle', $detalleProforma[0]->id)
			->update(['activo'=>false]);

			$actualizar_saldo = DB::select("SELECT actualizar_saldo_proforma(".$detalleProforma[0]->id_proforma .",".$request->input('dataLinea').",2)");							
										
			if($detalleProforma[0]->reserva_nemo_id != ""){
				DB::table('reservas_nemo')
										->where('id',$detalleProforma[0]->reserva_nemo_id)
										->update([
													'id_detalle_proforma'=>null,
													'id_proforma'=>null,
												]);
				$nemo = 1;	
			}

			//Desmarcar las reservas asociadas a la proforma
			DB::table('alojamientos_cangoroo')
			->where('id_proforma_detalle',$request->input('dataLinea'))
			->update([
					   'id_proforma' => null,
					   'id_proforma_detalle' => null
					 ]);

			DB::table('circuitos_cangoroo')
			->where('id_proforma_detalle',$request->input('dataLinea'))
			->update([
					   'id_proforma' => null,
					   'id_proforma_detalle' => null
					 ]);

			DB::table('traslados_cangoroo')
			->where('id_proforma_detalle',$request->input('dataLinea'))
			->update([
						'id_proforma' => null,
						'id_proforma_detalle' => null
					]);

			$this->calcularFeeCangoroo($detalleProforma[0]->id_proforma);
			$actualizar_proforma_cabecera = DB::select('SELECT public.actualizar_proforma_cabecera('.$request->input('dataProforma').')');
			$actualizarSenha = DB::select('SELECT public.actualizar_senha('.$request->input('dataProforma').', '.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');
			$this->calcularFeeAdministrativo($detalleProforma[0]->id_proforma);


			$mensaje->status = 'OK';
			$mensaje->linea = $request->input('dataLinea');
			$mensaje->item = $detalleProforma[0]->item;
			$mensaje->nemo = $nemo;
			$mensaje->mensaje = 'El detalle de proforma fue eliminado';

		}else{
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se puede eliminar, este ítem ya tiene libro de compra.';
		}        
		return json_encode($mensaje);	
	}
	
	public function getProducto(Request $request){
		$producto = Producto::where('id', $request->input('dataProducto'))->get(['frase_predeterminada']);
		return json_encode($producto[0]->frase_predeterminada);
	}	

	/**
	 * TOMA LOS DATOS DEL TICKET Y LOS DEVUELVE A LA VISTA  DE PROFORMA_DETALLE
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function procesarTicket(Request $request){
	/*	echo '<pre>'; 
		print_r($request->all()); */ 
		$contador = 0;
		$tickets = ""; 
		$nTicket = "";
		$cantidad = count($request->input('tickets'));
		$monto = 0;
		$costo = 0;
		$tasa = 0;
		$fechaIda = [];
		$fechaOut = [];
		$producto = Producto::where('id', $request->input('productoTicket'))->get(['frase_predeterminada']);
		$getPorComAgencia = DB::select('SELECT public."get_porcentaje_com_agencia"('.$request->input('productoTicket') .','.$request->input('cliente').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');
		$comision_agencia = $getPorComAgencia[0]->get_porcentaje_com_agencia;

		foreach($request->input('tickets') as $key => $destino){

			$ticket = Ticket::with('persona')->where('id', $destino)->get();
			$proveedor = $ticket[0]->id_proveedor;
			$proveedor_nombre = $ticket[0]->persona->nombre;
			$prestador = $ticket[0]->id_proveedor;
			$prestador_nombre = $ticket[0]->persona->nombre;
			$moneda = $ticket[0]->id_currency_costo;

			$fechaIda[] = $ticket[0]->fecha_ida;
			$fechaOut[] = $ticket[0]->fecha_vuelta;

			$cotizacionJurcaip = $ticket[0]->cotizacion_jurcaip;

			$getCosto = DB::select('SELECT public.get_precio_costo('.$request->input('productoTicket') .',0,'.$destino.',0,0,0,'.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');

			$getVenta = DB::select('SELECT public.get_venta_ticket('.$destino.')');

			$getTasas = DB::select('SELECT public.get_tasas_ticket('.$destino.')');
		
			if($cotizacionJurcaip == ''){
				$ventaCotizado = DB::select('SELECT public.get_monto_cotizado('.floatval($getVenta[0]->get_venta_ticket).','.$moneda.','.$request->input('currency').')');
				$ventaMontoCotizado = floatval($ventaCotizado[0]->get_monto_cotizado);
				$tasaCotizado = DB::select('SELECT public.get_monto_cotizado('.floatval($getTasas[0]->get_tasas_ticket).','.$moneda.','.$request->input('currency').')');
				$tasaMontoCotizado = floatval($tasaCotizado[0]->get_monto_cotizado);
			}else{
				$ventaCotizado = DB::select('SELECT public.get_monto_cotizacion_custom('.$cotizacionJurcaip.','.floatval($getVenta[0]->get_venta_ticket).','.$moneda.','.$request->input('currency').')');
				$ventaMontoCotizado = floatval($ventaCotizado[0]->get_monto_cotizacion_custom);
				$tasaCotizado = DB::select('SELECT public.get_monto_cotizacion_custom('.$cotizacionJurcaip.','.floatval($getTasas[0]->get_tasas_ticket).','.$moneda.','.$request->input('currency').')');
				$tasaMontoCotizado = floatval($tasaCotizado[0]->get_monto_cotizacion_custom);
			}

			$monto = $monto + floatval($ventaMontoCotizado);
			$costo = $costo + floatval($getCosto[0]->get_precio_costo);
			$tasa = $tasa + floatval($tasaMontoCotizado);
			if($contador == 0){
					$tickets .= $destino;	
					$nTicket .= $ticket[0]->numero_amadeus;
				}else{
					$tickets .= ",".$destino;
					$nTicket .= ",".$ticket[0]->numero_amadeus;	
				}
				$contador++;
			}

		sort($fechaIda, SORT_STRING);
		rsort($fechaOut, SORT_STRING);

		$primeroBase = $fechaIda[0];
		$ultimoBase = $fechaOut[0];

		$markup = DB::select('select calcular_markup(?,?,?,?,?,?)', 
			[$tasa, 
			$request->input('currency'), 
			$costo,
			$request->input('currency'), 
			$monto,
			Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa
			]); 

		$markups = $markup[0]->calcular_markup;

		$mensaje = new \StdClass;
		$mensaje->codigo = $nTicket;
		$mensaje->status = 'OK';
		$mensaje->tickets = $tickets;
		$mensaje->cantidad = $cantidad;
		$mensaje->markup = round($markups, 2);
		$mensaje->monto = round($monto, 2);
		$mensaje->costo =  round($costo, 2);
		$mensaje->determinada = $producto[0]->frase_predeterminada; 
		$mensaje->comision_agencia = $comision_agencia;
		$mensaje->proveedor = $proveedor;
		$mensaje->proveedor_nombre = $proveedor_nombre;
		$mensaje->prestador = $prestador;
		$mensaje->prestador_nombre = $prestador_nombre;
		$mensaje->moneda = $moneda;
		$mensaje->tasa = round($tasa, 2);
		$checkIn = '';
		$checkOut = '';

		if(!empty($primeroBase) && !empty($ultimoBase)){
			$check_in = explode('-', $primeroBase);
			$checkIn = trim($check_in[2]).'/'.trim($check_in[1]).'/'.trim($check_in[0]);
			$check_out = explode('-',  $ultimoBase);
			$checkOut = trim($check_out[2]).'/'.trim($check_out[1]).'/'.trim($check_out[0]);
		}
		$mensaje->checkIn = $checkIn;
		$mensaje->checkOut = $checkOut;

		return json_encode($mensaje);	
	}//FUNCTION

	

	public function getTicketEditar(Request $request){
		/*echo '<pre>';
		print_r($request->all());*/
		$tickets = Ticket::with('grupo', 'currency', 'persona')
						 ->where('id_proforma', $request->input('dataProforma'))
						 ->where('id_detalle_proforma', $request->input('dataData'))
						 ->get();
		$detalleProforma = ProformasDetalle::where('id', $request->input('dataData'))->get(['precio_venta']);
		foreach($tickets as $key=>$ticket){
			$precio_venta = DB::select('SELECT public.get_venta_ticket('.$ticket->id.')');
			$tickets[$key]->precio_venta = floatval($precio_venta[0]->get_venta_ticket);
		}
		$tickets[0]->totalDetalle= $detalleProforma[0]->precio_venta;

		return json_encode($tickets); 
	}	

	public function getProveedorPrestador(Request $request){

		$proveedorArray = Session::get('proveedorArray');
		$prestadorArray = Session::get('prestadorArray');
		$opcionProveedor = $proveedorArray[$request->input('dataDetalle')];
		$opcionPrestador = $prestadorArray[$request->input('dataDetalle')];
		return json_encode($opcionProveedor."_".$opcionPrestador);
	}	

	public function getRestaurarTicket(Request $request){
		DB::table('tickets')
			->where('id',$request->input('dataProducto'))
			->update([
					'id_proforma'=>null,
					'id_detalle_proforma'=>null
					]);
	}

	public function actualizarProforma(Request $request)
	{

		$fechaPeriodo= explode('-', $request->input('periodo'));

		$check_in = explode('/', $fechaPeriodo[0]);
		$check_out = explode('/', $fechaPeriodo[1]);

		$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]);
		$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]);

		if($request->input('vencimiento')!== null){
			$vencimiento = explode('/', $request->input('vencimiento'));
			$dataVencimiento = $vencimiento[2].'-'.$vencimiento[1].'-'.$vencimiento[0];
		}else{
			$dataVencimiento = null;
		}

		if($request->input('expediente') == ""){
			$proforma_padre = 0;
		}else{
			$proforma_padre = $request->input('expediente');
		}	

		if($request->input('imprimir_precio_unitario') == 1){
			$imprimir_precio_unitario = true;
		}else{
			$imprimir_precio_unitario = false;
		}	

		if($request->input('id_usuario') == ""){
			$proforma = Proforma::where('id', $request->input('proforma_id'))
								->first(['id_usuario']);
			$usuarioId = $proforma->id_usuario;
		}else{
			$usuarioId = $request->input('id_usuario');
		}	

		if($request->input('id_asistente') == ""){
			$proforma = Proforma::where('id', $request->input('proforma_id'))
								->first(['id_asistente']);
			$id_asistente = $proforma->id_asistente;
		}else{
			$id_asistente = $request->input('id_asistente');
		}	


		if($request->input('senha') == ""){
			$senha = 0;
		}else{
			$senha = str_replace(',','.', str_replace('.','', $request->input('senha')));
		}	

		if($request->input('id_promocion') == ""){
			$id_promocion = 0;
		}else{
			$id_promocion = $request->input('id_promocion');
		}	


 		$validarSenha = DB::select('SELECT public.validar_senha('.$request->input('proforma_id').', '.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.','.$senha.' )');	

 		$validarVencimiento = DB::select("SELECT public.validar_vencimiento_cabecera(".$request->input('proforma_id').", to_date('".$dataVencimiento."', 'yyyy-mm-dd') )");
		
		$detalle_mensaje = "";	

		if($validarSenha[0]->validar_senha == "OK"){
 			if($validarVencimiento[0]->validar_vencimiento_cabecera == "OK"){
 				//try{
					DB::table('proformas')
								        ->where('id',$request->input('proforma_id'))
								        ->update([
								            	'cliente_id'=>$request->input('cliente_id'),
												'imprimir_precio_unitario_venta'=>$imprimir_precio_unitario,
								            	'vendedor_id'=>$request->input('id_categoria'),
								            	'destino_id'=>$request->input('destino_id'),
								            	'check_in'=>$checkIn,
								            	'check_out'=>$checkOut,
								            	'id_prioridad'=>$request->input('idPrioridad'),
								            	'id_moneda_venta'=>$request->input('moneda'),
								            	'vencimiento'=>$dataVencimiento,
								            	'id_usuario'=>$usuarioId,
												'id_asistente'=>$id_asistente,
								            	'senha'=>$senha,
												'incentivo_venta'=>$request->input('incentivo'),
								            	'proforma_padre'=>$proforma_padre,
												'tiene_incentivo_vendedor_agencia' => $request->input('incentivo_agencia'),
												'id_unidad_negocio' => $request->input('negocio'),
												'tipo_factura_id' => $request->input('tipo_venta'),
												'cantidad_pasajero'=>($request->input('cant_pasajero') != '') ? $request->input('cant_pasajero') : null,
												'id_tarifa' => $request->input('id_tarifa'),
												'id_grupo' => $request->input('grupo_id'),
												'concepto_generico'=> $request->input('concepto'),
												'pago_tarjeta' => $request->input('tipo_tarjeta'),
												'file_codigo'=>($request->input('file_codigo') != '') ? $request->input('file_codigo') : null,
												'id_promocion'=> $id_promocion
								            	]);
					DB::table('pasajeros_proformas')->where('id_proforma', $request->input('proforma_id'))->delete();
					foreach($request->input('pasajero_principal') as $pasajeros){
				    	$pasajero = new PasajeroProforma;
						$pasajero->id_proforma = $request->input('proforma_id');
						$pasajero->pasajero = intval($pasajeros);
						$pasajero->save();
			    	}
				/* } catch(\Exception $e){
					$detalle_mensaje .= 'No se ha actualizado la proforma<br>';
				} */
				$proforma = Proforma::with('tipoFacturacion')
											->where('id', $request->input('proforma_id'))
											->get();
				if(isset($proforma[0]->tipoFacturacion->denominacion)){							
					$tipoFacturacion = $proforma[0]->tipoFacturacion->denominacion;
				}else{
					$tipoFacturacion = '';
				}	
				$mensaje = new \StdClass; 
				$mensaje->id = $request->input('cliente_id');
				$mensaje->status = 'OK';
				$mensaje->mensaje = 'Se ha actualizado la Proforma'; 
				$mensaje->tipo_facturacion = $tipoFacturacion;
			} else {
				$detalle_mensaje .= $validarVencimiento[0]->validar_vencimiento_cabecera.'<br>'; 

			}
		} else {
			$detalle_mensaje .= $validarSenha[0]->validar_senha.'<br>'; 
		}

		if($detalle_mensaje != ""){
			$mensaje = new \StdClass; 
			$mensaje->id = 0;
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = $detalle_mensaje; 
			$mensaje->tipo_facturacion = 0;
		}	


		return response()->json($mensaje);	

	}	

	public function getDatosFecha(Request $request){
		$detalleFecha = ProformasDetalle:: where('id_proforma', $request->input('dataProforma'))
										->where('item', $request->input('dataItem'))
										->get(['fecha_pago_proveedor']);

		return json_encode($detalleFecha); 								
	}	

	
	public function mostrarDatosDetalle(Request $request){
		$detalleProforma = ProformasDetalle::where('id', $request->input('dataId'))->get();
		//$renta_reprice= ProformaDetalleReprice::where('id_proforma_detalle', $request->input('dataId'))->get();
		$detalleArray = [];
		$cantidad = count($detalleProforma);
			foreach($detalleProforma as $key=>$detalle){
				if($detalle->comision_operador == 0){
					$comision = $detalle->comision_agencia;
				}else{
					$comision = $detalle->comision_operador;
				}
				$comision ?? '0'.PHP_EOL; 

				$detalleArray['exenta_item'] = $this->formatMoney($detalle->porcion_exenta,$request->input('dataMoneda'));
				$detalleArray['gravada_item'] = $this->formatMoney($detalle->porcion_gravada,$request->input('dataMoneda'));
				$detalleArray['costo_exenta'] = $this->formatMoney($detalle->costo_proveedor,$request->input('dataMoneda'));
				$detalleArray['costo_gravada'] = $this->formatMoney($detalle->costo_gravado,$request->input('dataMoneda'));

				$detalleArray['comision_agencia'] = $this->formatMoney($comision,$request->input('dataMoneda'));
				$detalleArray['porcentaje_comision_agencia'] = $this->formatMoney($detalle->porcentaje_comision_agencia,$request->input('dataMoneda'));
				$detalleArray['dtplus'] = $this->formatMoney($detalle->dtplus,$request->input('dataMoneda'));
				$detalleArray['renta'] = $this->formatMoney($detalle->renta,$request->input('dataMoneda'));
				$detalleArray['monto_no_comisionable'] = $this->formatMoney($detalle->monto_no_comisionable,$request->input('dataMoneda'));
				$detalleArray['diferencia_tributaria'] = $this->formatMoney($detalle->diferencia_tributaria,$request->input('dataMoneda'));
				$detalleArray['iva_porcion_gravada'] = $this->formatMoney($detalle->iva_porcion_gravada,$request->input('dataMoneda'));
				$detalleArray['neto_facturar'] = $this->formatMoney($detalle->neto_facturar,$request->input('dataMoneda'));
				$detalleArray['reprice'] = $detalle->reprice;
				//$detalleArray['renta_reprice'] = ProformaDetalleReprice::where('id_proforma_detalle', $request->input('dataId'))->get();
				$detalleArray['renta_reprice'] = ProformaDetalleReprice::leftJoin('personas', 'proformas_detalle_reprice.id_proveedor', '=', 'personas.id')
				->where('proformas_detalle_reprice.id_proforma_detalle', $request->input('dataId'))
				->get(['proformas_detalle_reprice.*', 'personas.*']);
			
				
				
		}	
		return json_encode($detalleArray); 	
	}	
	
	public function getConfirmacion(Request $request){
	}

	public function actualizarCabecera(Request $request){

			$proforma = Proforma::where('id',  $request->input('dataId'))
								->get();
			$vencimiento = $proforma[0]->vencimiento;
			$senha = $proforma[0]->senha;
			$total_bruto_facturar = $proforma[0]->total_bruto_facturar; 
			$total_neto_facturar = $proforma[0]->total_neto_facturar;
			$total_iva = $proforma[0]->total_iva;
			if($proforma[0]->comision_operador != 0){
				$total_comision = $proforma[0]->comision_operador;
			}else{
				$total_comision = $proforma[0]->total_comision;
			}
			$incentivo_vendedor_agencia = (float)$proforma[0]->incentivo_venta;
			$total_costos = $proforma[0]->total_costos;
			$markup = $proforma[0]->markup;
			$total_exentas = $proforma[0]->total_exentas;
			$total_gravadas = $proforma[0]->total_gravadas;
			$total_no_comisiona = $proforma[0]->total_no_comisiona;
			$renta = $proforma[0]->renta;
			$porcentaje_ganancia = $proforma[0]->porcentaje_ganancia;
			$ganancia_venta = $proforma[0]->ganancia_venta;
			$grupo_id = $proforma[0]->id_grupo;
			$incentivo_proforma = $proforma[0]->incentivo_vendedor_agencia;
			$mensaje = new \StdClass; 
			$mensaje->vencimiento = $vencimiento;
			$mensaje->senha = $senha;

			$totalFactura = DB::select("SELECT get_monto_proforma as m FROM get_monto_proforma(".$request->input('dataId').")");
			$totalFactura = $totalFactura[0]->m;

			if(!empty($proforma[0]->id_grupo)){
				$idGrupo = $proforma[0]->id_grupo;
			}else{
				$idGrupo = 0;
			}

			if($idGrupo != 0){
				$total = DB::select("select sum(monto) from grupos_anticipo where id_grupo = ".$idGrupo);
				$saldoGrupo = floatval($totalFactura) - floatval($total[0]->sum);
			}else{
				$saldoGrupo = 0;
			}	

			$mensaje->total_factura =number_format($totalFactura,2,",",".");
			$mensaje->total_bruto_facturar= number_format($total_bruto_facturar,2,",",".") ; 
			$mensaje->total_neto_facturar= number_format($total_neto_facturar,2,",","."); 
			$mensaje->total_iva= number_format($total_iva,2,",","."); 
			$mensaje->total_comision= number_format($total_comision,2,",","."); 
			$mensaje->incentivo_vendedor_agencia= $incentivo_vendedor_agencia; 
			$mensaje->total_costos= number_format($total_costos,2,",","."); 
			$mensaje->markup= number_format($markup,2,",","."); 
			$mensaje->total_exentas= number_format($total_exentas,2,",","."); 
			$mensaje->total_gravadas = number_format($total_gravadas,2,",",".");
			$mensaje->total_no_comisiona= number_format($total_no_comisiona,2,",","."); 
			$mensaje->renta= number_format($renta,2,",","."); 
			$mensaje->porcentaje_ganancia= number_format($porcentaje_ganancia,2,",","."); 
			$mensaje->ganancia_venta= number_format($ganancia_venta,2,",",".");
			$mensaje->periodo= date('d/m/Y', strtotime($proforma[0]->check_in))." - ".date('d/m/Y', strtotime($proforma[0]->check_out));
			$mensaje->id_grupo= $grupo_id;
			$mensaje->saldoGrupo= number_format($saldoGrupo,2,",","."); 
			$mensaje->incentivo_proforma = number_format($incentivo_proforma,2,",","."); 
			$mensaje->pasajero_id= $proforma[0]->pasajero_id;
			$mensaje->id_tarifa= $proforma[0]->id_tarifa;
			$mensaje->concepto= $proforma[0]->concepto_generico;
			$mensaje->rextra = number_format($proforma[0]->renta_extra,2,",","."); 
			$mensaje->renta_neta = number_format($proforma[0]->renta,2,",",".");
			$mensaje->saldo_factura = number_format($proforma[0]->saldo_por_facturar,2,",",".");

			$pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id FROM personas WHERE id IN (SELECT pasajero FROM pasajeros_proformas WHERE id_proforma=".$request->input('dataId').")");

			if(empty($pasajero)){					
			$pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id 
				FROM personas WHERE personas.id = ".$proforma[0]->pasajero_id);
			}

			$mensaje->pasajeros = $pasajero;
			return json_encode($mensaje);	
	}

	public function getTrasladorEditar(Request $request){
		$detalleTraslado = ProformasDetalle:: where('id_proforma', $request->input('dataProforma'))
											->where('item', $request->input('dataData'))
											->get();	

		$detTraslado = [];									
		foreach($detalleTraslado as $key=>$traslado){
			$detTraslado['id_tipo_traslado'] = $traslado->id_tipo_traslado;
			$detTraslado['fecha_in'] = $traslado->fecha_in;
			$detTraslado['fecha_out'] = $traslado->fecha_out;
			$detTraslado['hora_in'] = $traslado->hora_in;
			$detTraslado['hora_out'] = $traslado->hora_out;
			$detTraslado['vuelo_in'] = $traslado->vuelo_in;
			$detTraslado['vuelo_out'] = $traslado->vuelo_out;
		}
		return json_encode($detTraslado);								
	}

	public function anularProforma(Request $request){

		$getAnular = DB::select('SELECT public.anular_proforma('.$request->input('dataProforma').','.Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.')');
		return json_encode($getAnular);								
	}

	public function guardarAsistencia(Request $request){

		/*$fechaPeriodo= explode('-', $request->input('in_out'));
		$check_in = explode('/', $fechaPeriodo[0]);
		$check_out = explode('/', $fechaPeriodo[1]);
		$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]);
		$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]);

		$diff = abs(strtotime($checkIn) - strtotime($checkOut));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$getCosto = DB::select('SELECT public.get_precio_costo('.$request->input('producto') .',0,0,'.$request->input('tipo_asistencia') .','.$days .','.$request->input('personas').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');

		$costo = $getCosto[0]->get_precio_costo;*/
		$costo = 0;
		$asistencia = [];
		$asistencia['costo'] = $costo;
		$asistencia['cant_personas'] = $request->input('personas');
		$asistencia['dias'] = 0;
		$asistencia['tipo_asistencia'] = $request->input('tipo_asistencia');

		return json_encode($asistencia);

	}	

	public function guardarAsistenciaAU(Request $request){
		(float)$request->input('exentaAsistencia');

		$asistencia = [];
		/*if((float)$request->input('exentaAsistencia') != 0 || $request->input('gravadaAsistencia') != 0 ){
			$asistencia['costo'] = (float)str_replace(',','.', str_replace('.','', $request->input('exentaAsistencia')));
			$asistencia['venta'] = (float)str_replace(',','.', str_replace('.','', $request->input('exentaAsistencia')))+(float)str_replace(',','.', str_replace('.','', $request->input('gravadaAsistencia')));
		}else{
			$getCosto = DB::select('SELECT public.get_precio_costo('.$request->input('producto') .',0,0,'.$request->input('tipo_asistencia') .','.$request->input('dias') .','.$request->input('personas').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');

			$asistencia['costo'] = $getCosto[0]->get_precio_costo;
			$asistencia['venta'] = 0;
		}*/
		$asistencia['cant_personas'] = $request->input('personasAU');
		$asistencia['dias'] = 0;
		$asistencia['tipo_asistencia'] = $request->input('tipo_asistenciaAU');
		$asistencia['exenta'] = 0;
		$asistencia['gravada'] = 0;
		return json_encode($asistencia);
	}	

	public function getAsistencia(Request $request){
		$detalleAsistencia = ProformasDetalle:: where('id_proforma', $request->input('dataProforma'))
										->where('item', $request->input('dataDetalleProforma'))
										->get();

		$asistencia = [];
		foreach($detalleAsistencia as $key=>$detalle){
			if($detalle->id_tarifa_asistencia != ""){
				$asistencia['cant_personas'] = $detalle->asistencia_cantidad_personas;
				$asistencia['dias'] = $detalle->asistencia_dias;
				$asistencia['tipo_asistencia'] = $detalle->id_tarifa_asistencia;
			}else{
				$asistencia['cant_personas'] = 0;
				$asistencia['dias'] = 0;
				$asistencia['tipo_asistencia'] = 0;
			}
		}	
		return json_encode($asistencia);
	}

	public function getAsistenciaUA(Request $request){
		$detalleAsistencia = ProformasDetalle:: where('id_proforma', $request->input('dataProforma'))
										->where('item', $request->input('dataDetalleProforma'))
										->get();
		$asistencia = [];
		foreach($detalleAsistencia as $key=>$detalle){
			if($detalle->id_tarifa_asistencia != ""){
				$asistencia['cant_personas'] = $detalle->asistencia_cantidad_personas;
				$asistencia['dias'] = $detalle->asistencia_dias;
				$asistencia['tipo_asistencia'] = $detalle->id_tarifa_asistencia;
				$asistencia['exenta'] = $detalle->porcion_exenta;
				$asistencia['grabado'] = $detalle->porcion_gravada;
			}else{
				$asistencia['cant_personas'] = 0;
				$asistencia['dias'] = 0;
				$asistencia['tipo_asistencia'] = 0;
				$asistencia['exenta'] = 0;
				$asistencia['grabado'] = 0;
			}
		}	
		return json_encode($asistencia);
	}

	public function getDetalleProducto(Request $request){

		$detalleProducto = ProformasDetalle::with('producto')->where('id', $request->input('dataDetalleProforma'))->first();
		$resultado = [];

		if($detalleProducto){
			$prestador = $detalleProducto->id_prestador;
			$resultado['status'] = 'ok';
			$resultado['producto'] = $detalleProducto->id_producto;
			$resultado['id_grupo_producto'] = isset($detalleProducto->producto) ? $detalleProducto->producto->id_grupos_producto : null;
			$resultado['tipo_traslado'] = $detalleProducto->id_tipo_traslado;
			$resultado['cantidad'] = 1;
			$resultado['periodo'] = date('d/m/Y', strtotime($detalleProducto->fecha_in))." - ".date('d/m/Y', strtotime($detalleProducto->fecha_out));
			$resultado['id_prestador'] = $prestador;
			$resultado['prestador'] = Persona::where('id',$prestador)->first(['id', 'nombre']);
		}else{
			$resultado['status'] = 'vacio';
		}

		return json_encode($resultado);
	}	


	public function editarVoucher(Request $request){
		DB::table('vouchers')
					        ->where('id_proforma',$request->input('proforma'))
					        ->where('id_proforma_detalle',$request->input('detalle_proforma'))
					        ->update([
					            	'activo'=>false,
					            	]);

		$idInsercion = DB::select("select nextval('public.insercion_voucher')");
		$valInsercion = $idInsercion[0]->nextval;
		$detalleProforma = ProformasDetalle::where('id', $request->input('detalle_proforma'))->get(['item']);
		$mensaje = new \StdClass;

		try{							

			if(isset($request->base)){

				DB::beginTransaction();

				foreach($request->base as $key1=>$base){
					$voucher = new Voucher;
					$voucher->id_insercion = $valInsercion;
					$voucher->id_proforma = $request->input('proforma');
					$voucher->id_proforma_detalle = $request->input('detalle_proforma');
					$voucher->descripcion_servicio = $base['descripcion'];
					$voucher->comentario = html_entity_decode($base['comentario']);
					$voucher->cant_adultos = $base['adultos'];
					if(isset($base['noches'])){
						$voucher->noches = $base['noches'];
					}
					$voucher->cant_child = $base['ninhos'];
					if(isset($base['otros_pasajeros'])){
						$voucher->otros_pasajeros = $base['otros_pasajeros'];
					}else{
						$voucher->otros_pasajeros ="";
					}
		
					if(isset($base['id_prestador'])){
						$voucher->id_prestador = $base['id_prestador'];
					}else{
						$voucher->id_prestador =0;
					}
		
					$voucher->pasajero_principal = $base['pasajero_principal'];
					$fechaPeriodo= explode(' - ', $base['fecha_in_out']);
					$check_in = explode('/', $fechaPeriodo[0]);
					$check_out = explode('/', $fechaPeriodo[1]);
					$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]);
					$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]);
					
					//if($base['producto'] == 2 || $base['producto'] == 5|| $base['producto'] == 16){
						$date1 = new \DateTime($checkIn);
						$date2 = new \DateTime($checkOut);
						$diff = $date1->diff($date2);
						$voucher->noches = $diff->days;
					/*}else{
						$voucher->noches = 0;	
					}*/
		
					// will output 2 days
					$voucher->fecha_in = $checkIn;
					$voucher->fecha_out = $checkOut;
					$voucher->producto_id = $base['producto'];
					$voucher->id_estados = 1;
					$voucher->id_usuario= Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
					$voucher->fecha= date('Y-m-d H:m:i');
					$voucher->save();
					
				}

		

				DB::commit();

			} 

			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se han guardado los Vouchers exitosamente';
			$mensaje->item = $detalleProforma[0]->item;

		} catch(\Exception $e){

			Log::error($e);
			DB::rollback();

			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'No se han guardado la vouchers , intentelo Nuevamente';
			$mensaje->item = 0;
		}

	
		return json_encode($mensaje);	
	}

	public function actualizarPrecioTicket(Request $request){
		$precio_venta = DB::select('SELECT public.get_venta_ticket('.$request->input('dataIdTicket').')');
		$precioVentaTicket = floatval($precio_venta[0]->get_venta_ticket);
		return json_encode($precio_venta);	
	}

	public function getMontoDetalle(Request $request){

		$cliente_id = Proforma::where('id', $request->input('dataProforma'))->get(['cliente_id']);

		$detalleProforma = ProformasDetalle:: where('id_proforma', $request->input('dataProforma'))
												->get();
		$resultadosArray = [];
		foreach($detalleProforma as $key=>$detalles){
			$resultadosArray[$detalles->item]['markup'] = $detalles->markup;
			$resultadosArray[$detalles->item]['precio_compra'] = number_format($detalles->costo_proveedor,2,",",".");
			$resultadosArray[$detalles->item]['precio_venta'] = number_format($detalles->precio_venta,2,",",".");
			$resultadosArray[$detalles->item]['porcentaje'] = $detalles->porcentaje_comision_agencia;
		}
		return json_encode($resultadosArray);	
	}	

	public function calcularVenta(Request $request){

		$costo = (float)str_replace(',','.', str_replace('.','',$request->input('dataCosto')));
		//$costo = (float)str_replace(',','.', $request->input('dataCosto'));
		$markup = (float) $request->input('dataMarkup');
		$tasa = (float)str_replace(',','.', str_replace('.','',$request->input('dataTasas')));
		//$tasa = (float)str_replace(',','.', str_replace('.','',$request->input('dataTasas')));

		$venta = DB::select('select calcular_venta(?,?,?,?,?,?)', 
			[$tasa, 
			$request->input('dataMonedaCompra'), 
			$costo,
			$request->input('dataCurrency_venta'), 
			$markup,
			Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa
			]);
		/*echo '<pre>';
		print_r($venta);*/

		$resultado = 0;
		if($request->input('dataCurrency_venta') == 111){
			$resultado = round($venta[0]->calcular_venta,0,PHP_ROUND_HALF_UP);
		} else {
			$resultado = round($venta[0]->calcular_venta,2);
		}

		return $resultado;
	}

	public function calcularMarkup(Request $request){
		$venta = (float)str_replace(',','.', str_replace('.','',$request->input('dataVenta')));
		$costo = (float)str_replace(',','.', str_replace('.','',$request->input('dataCosto')));
		$tasa = (float)str_replace(',','.', str_replace('.','',$request->input('dataTasas')));
		$moneda_venta = $request->input('dataCurrency_venta') ? $request->input('dataCurrency_venta') : 0;
		$markup_resp = 0;

		try {
			$ventaCotizado = DB::select('SELECT public.get_monto_cotizado('.floatval($venta).','.$request->input('dataMonedaCompra').','.$request->input('dataCurrency_venta').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');

			$markup = DB::select('select calcular_markup(?,?,?,?,?,?)', 
				[$tasa, 
				$request->input('dataMonedaCompra'), 
				$costo,
				$moneda_venta, 
				$venta,
				Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa
				]); 

				$markup_resp = $markup[0]->calcular_markup;

		} catch ( \Exception $e){
			Log::error($e->getMessage());
		}
			

		return $markup_resp;

	}


	public function ventaCotizado(Request $request){
		/*echo '<pre>';
		print_r($request->all());*/
		$venta = (float)str_replace(',','.', str_replace('.','',$request->input('dataVenta')));
		$costo = (float)str_replace(',','.', str_replace('.','',$request->input('dataCosto')));
		$tasa = (float)str_replace(',','.', str_replace('.','',$request->input('dataTasas')));
	 	/*echo '<pre>';
		print_r('SELECT public.get_monto_cotizado('.floatval($venta).','.$request->input('dataMonedaCompra').','.$request->input('dataCurrency_venta').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')'); 
*/
		$ventaCotizado = DB::select('SELECT public.get_monto_cotizado('.floatval($venta).','.$request->input('dataMonedaCompra').','.$request->input('dataCurrency_venta').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');
		if($request->input('dataCurrency_venta') == 111){
			$resultado = round($ventaCotizado[0]->get_monto_cotizado,0,PHP_ROUND_HALF_UP);
		}else{
			$resultado = round($ventaCotizado[0]->get_monto_cotizado,2);
		}
		return $resultado;
	}

	public function getDescripcion(Request $request){
		$detalleProforma = ProformasDetalle::where('item', $request->input('dataData')) 	
											->where('id_proforma', $request->input('dataProforma'))
											->get(['descripcion']);
		$mensaje = new \StdClass;
		$mensaje->mensaje = $detalleProforma[0]->descripcion;								
		return json_encode($mensaje); 
	}

	public function buscarPrestador(Request $request){
		ini_set('memory_limit', '-1');
		$query = "SELECT masterbm_hoteles.id, masterbm_hoteles.name, masterbm_hoteles.address, masterbm_hoteles.phone, masterbm_hoteles_destino.id_destino 
		from masterbm_hoteles, masterbm_hoteles_destino, destinos_proveedor_dtpmundo_v2";
		if($request->input('destino') != ""){
			$query .= " where destinos_proveedor_dtpmundo_v2.id_destino_dtpmundo = ".$request->input('destino')."AND masterbm_hoteles_destino.id_hotel = masterbm_hoteles.id	AND destinos_proveedor_dtpmundo_v2.id_destino_proveedor = masterbm_hoteles_destino.id_destino AND LOWER(masterbm_hoteles.name) LIKE '%".strtolower($request->input('nombre'))."%'";
		}else{
			$query .= " where masterbm_hoteles_destino.id_hotel = masterbm_hoteles.id AND destinos_proveedor_dtpmundo_v2.id_destino_proveedor = masterbm_hoteles_destino.id_destino AND LOWER(masterbm_hoteles.name) LIKE '%".strtolower($request->input('nombre'))."%'";
		}
		$query .= "and masterbm_hoteles.id not in (select coalesce(codigo_hotel, null, 0) from personas where id_tipo_persona = 15 and id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa."and activo=true order by 1)";
		$prestador = DB::select($query);
		return json_encode($prestador); 
	}

	public function guardarPrestador(Request $request){
		$resultadoIncersion = DB::select('SELECT public.get_retorna_prestador_hotelcod('.$request->input('idPrestador').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');

		$idPresona = $resultadoIncersion[0]->get_retorna_prestador_hotelcod;

		$personas = Persona:: where('id', $idPresona)
							->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->get(['id', 'nombre']);	
		$mensaje = new \StdClass; 
		$mensaje->id = $personas[0]->id;
		$mensaje->nombre = $personas[0]->nombre;				
		return json_encode($mensaje); 
	}//function

    public function btnPermisos($parametros, $vista){

  	$btn = DB::select("SELECT pe.* 
                        FROM persona_permiso_especiales p, permisos_especiales pe
                        WHERE p.id_permiso = pe.id 
                        AND p.id_persona = ".$this->getIdUsuario() ." 
                        AND pe.url = '".$vista."' ");

  $htmlBtn = '';
  $boton =  array();
  $idParametro = '';

 // dd( $btn );

  if(!empty($btn)){

  foreach ($btn as $key => $value) {

    $idParametro = '';
    $ruta = $value->accion;
     $htmlBtn = '';



     //LLEVA PARAMETRO
    if($value->lleva_parametro == '1'){

    foreach ($parametros as $indice=>$valor) { 

      if($indice == $value->nombre_parametro){
        $idParametro = $valor;
      }
    }

    //PARAMETRO OCULTO EN DATA
    if($value->parametro_oculto == '1'){
       $htmlBtn = "<a  class='".$value->clase."' data-btn='".$idParametro."'   title='".$value->titulo."'>
      	".$value->titulo."
       <i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    } else {
       $htmlBtn = "<a class='".$value->clase."' href='".route($ruta,['id'=>$idParametro])."'    title='".$value->titulo."'><i class='".$value->icono."'>
      
       </i> ".$value->titulo." </a>";  
    }

    } else {
      $htmlBtn = "<a  href='#' class='".$value->clase."'   title='".$value->titulo."'>
      
      <i class='".$value->icono."'></i>".$value->texto_alternativo." </a>"; 
    }

    
    $boton[] = $htmlBtn;
  }
   return $boton;
 
   } else {
    return array();
   }



}//function

	  public function listadoGrupo(Request $request){
	  		$gruposSalidas = Grupos:: where('tipo_ticket_id', $request->input('dataIdGrupo'))
	  							    ->where('estado_id', 11)
	  							    ->where('empresa_id', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
									->get(['id', 'denominacion']);		
			return json_encode($gruposSalidas); 	
	  }	

	  public function listadoDestinos(Request $request){
		  	$gruposSalidas = Grupos:: where('tipo_ticket_id', $request->input('dataIdGrupo'))
		  							->where('estado_id', 11)
		  							->where('empresa_id', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
									->get(['destino_id']);
			$resultado = [];
			$datos = file_get_contents("../destination_actual.json");
			$destinoJson =  json_decode($datos, true);							
			foreach($gruposSalidas as $key=>$destino){
				foreach($destinoJson as $key1=>$dest){
					if($destino['destino_id'] == $dest['idDestino']){
						$resultado[$dest['idDestino']]['id'] = $dest['idDestino'];
						$resultado[$dest['idDestino']]['descripcion'] = $dest['desDestino'];
					}
				}
			}
		return json_encode($resultado); 	
	  }
	  
	public function buscarReserva(Request $request){
		$id_tipo = "";
		if($request->input('id_tipo') !== null){
			$id_tipo = $request->input('id_tipo');
		}	
		$estados = EstadoReserva::where('activo',true)->get();
		$vendedor_empresa = Persona::whereIn('id_tipo_persona',array(2, 4, 3))->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
		$vendedor_agencia = Persona::where('id_tipo_persona','10')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
	    $cliente = DB::select("SELECT * FROM personas 
							  WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
							  							WHERE puede_facturar = true) 
							  AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");

		return view('pages.mc.proforma.buscarReserva')->with(['vendedor_empresas'=>$vendedor_empresa,
											                  'vendedor_agencia'=>$vendedor_agencia,
											                  'clientes'=>$cliente,
															  'id_tipo'=>$id_tipo,
															  'estados'=>$estados
															]);
	}//function


	public function buscarReservaReprice(Request $request){
		$id_tipo = "";
		if($request->input('id_tipo') !== null){
			$id_tipo = $request->input('id_tipo');
		}	
		$estados = EstadoReserva::where('activo',true)->get();
		$vendedor_empresa = Persona::whereIn('id_tipo_persona',array(2, 4, 3))->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
		$vendedor_agencia = Persona::where('id_tipo_persona','10')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
	    $cliente = DB::select("SELECT * FROM personas 
							  WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
							  							WHERE puede_facturar = true) 
							  AND id_empresa = ".$this->getIdEmpresa()." AND activo = true order by nombre ASC");

		return view('pages.mc.proforma.buscarReservaReprice')->with(['vendedor_empresas'=>$vendedor_empresa,
											                  'vendedor_agencia'=>$vendedor_agencia,
											                  'clientes'=>$cliente,
															  'id_tipo'=>$id_tipo,
															  'estados'=>$estados
															]);
	}//function
	
	public function ajaxBuscarReserva(Request $req){
		ini_set('memory_limit', '-1');
		set_time_limit(300);
		$form = $this->responseFormatDatatable($req);
		//SUB SELECT PARA USAR FUNCIONES CON ALIAS EN LOS WHERE
			$proformaDetalle = DB::table('vw_busqueda_reserva');
 			// $proformaDetalle = $proformaDetalle->where('activo',true);

			if($form->id_tipo != ''){
				if($form->id_tipo == 'disponible'){
					$proformaDetalle = $proformaDetalle->where('nemo','true');
					$proformaDetalle = $proformaDetalle->whereNull('id_proforma');
					$proformaDetalle = $proformaDetalle->whereIn('estado_id', array(2, 3));
				}else{	
					$proformaDetalle = $proformaDetalle->where('nemo','true');
				 	$proformaDetalle = $proformaDetalle->whereNotNull('id_proforma');
					$proformaDetalle = $proformaDetalle->whereIn('estado_id', array(2, 3));
				}	
		  	}

			if($form->cliente_id != ''){
				 $proformaDetalle = $proformaDetalle->where('cliente_id',$form->cliente_id);
			}

			if($form->id_vendedor_empresa != ''){
				 $proformaDetalle = $proformaDetalle->where('id_usuario',$form->id_vendedor_empresa);
			}

			if($form->vendedor_id != ''){
				 $proformaDetalle = $proformaDetalle->where('vendedor_id',$form->vendedor_id);
			}

			if($form->proforma != ''){
				$proformaDetalle = $proformaDetalle->where('id_proforma',$form->proforma);
		    }

			if($form->proforma_estado != ''){
				if($form->proforma_estado == 'no'){
					$proformaDetalle = $proformaDetalle->whereNull('id_proforma');
				}else{ 
					$proformaDetalle = $proformaDetalle->whereNotNull('id_proforma');
				}
 			}
			if($form->nombrePasajero != ''){
				$nombre = "%".trim(strtolower($form->nombrePasajero))."%";
				// dd($nombre);
				 $proformaDetalle = $proformaDetalle->where('nombres','LIKE',$nombre);
			}

			if($form->cod_confirmacion){
				$cod = "%".trim(strtolower($form->cod_confirmacion))."%";
				$proformaDetalle = $proformaDetalle->where('cod_confirmacion','LIKE',$cod);
			}

			if($form->estado){
				$proformaDetalle = $proformaDetalle->where('estado',$form->estado);
			}

			if($form->cod_nemo){
				$cod = "%".trim(strtolower($form->cod_nemo))."%";
				$proformaDetalle = $proformaDetalle->where('cod_nemo','LIKE',$cod);
			}

			 if($form->periodo_in != ''){
			           $fecha = explode('-', $form->periodo_in);
			                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
			                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
 
			  $proformaDetalle = $proformaDetalle->whereBetween('fecha_in', 
			  							array($desde.' 00:00:00',$hasta.' 23:59:59'));
			        }
			  if($form->periodo_out != ''){
			           $fecha = explode('-', $form->periodo_out);
			                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
			                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
 
			  $proformaDetalle = $proformaDetalle->whereBetween('fecha_out', 
			  							array($desde.' 00:00:00',$hasta.' 23:59:59'));
			        }        
			
				if(isset($form->pago_desde_hasta)){
					if($form->pago_desde_hasta != ''){
							$fecha = explode('-', $form->pago_desde_hasta);
									$desde = $this->formatoFechaEntrada(trim($fecha[0]));
									$hasta = $this->formatoFechaEntrada(trim($fecha[1]));
		
							$proformaDetalle = $proformaDetalle->whereBetween('fecha_notificado_nemo', 
												array($desde.' 00:00:00',$hasta.' 23:59:59'));
					}        
				} 
			
			  $proformaDetalle = $proformaDetalle->where('id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);       

			  $proformaDetalle	= $proformaDetalle->get();

			/* foreach($proformaDetalle as $key=>$profDetalle){
				if($profDetalle->id_empresa != Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa){
					unset($proformaDetalle[$key]);
				}
			}*/
        

        return response()->json(['data'=>$proformaDetalle]);
	}//function

	public function ajaxBuscarReservaReprice(Request $req){
		ini_set('memory_limit', '-1');
		set_time_limit(300);
		$form = $this->responseFormatDatatable($req);
		//SUB SELECT PARA USAR FUNCIONES CON ALIAS EN LOS WHERE
			$proformaDetalle = DB::table('vw_busqueda_reserva_reprice');
 			// $proformaDetalle = $proformaDetalle->where('activo',true);

			if($form->id_tipo != ''){
				if($form->id_tipo == 'disponible'){
					$proformaDetalle = $proformaDetalle->where('nemo','true');
					$proformaDetalle = $proformaDetalle->whereNull('id_proforma');
					$proformaDetalle = $proformaDetalle->whereIn('estado_id', array(2, 3));
				}else{	
					$proformaDetalle = $proformaDetalle->where('nemo','true');
				 	$proformaDetalle = $proformaDetalle->whereNotNull('id_proforma');
					$proformaDetalle = $proformaDetalle->whereIn('estado_id', array(2, 3));
				}	
		  	}

			if($form->cliente_id != ''){
				 $proformaDetalle = $proformaDetalle->where('cliente_id',$form->cliente_id);
			}

			if($form->id_vendedor_empresa != ''){
				 $proformaDetalle = $proformaDetalle->where('id_usuario',$form->id_vendedor_empresa);
			}

			if($form->vendedor_id != ''){
				 $proformaDetalle = $proformaDetalle->where('vendedor_id',$form->vendedor_id);
			}

			if($form->proforma != ''){
				$proformaDetalle = $proformaDetalle->where('id_proforma',$form->proforma);
		    }

			if($form->proforma_estado != ''){
				if($form->proforma_estado == 'no'){
					$proformaDetalle = $proformaDetalle->whereNull('id_proforma');
				}else{ 
					$proformaDetalle = $proformaDetalle->whereNotNull('id_proforma');
				}
 			}
			if($form->nombrePasajero != ''){
				$nombre = "%".trim(strtolower($form->nombrePasajero))."%";
				// dd($nombre);
				 $proformaDetalle = $proformaDetalle->where('nombres','LIKE',$nombre);
			}

			if($form->cod_confirmacion){
				$cod = "%".trim(strtolower($form->cod_confirmacion))."%";
				$proformaDetalle = $proformaDetalle->where('cod_confirmacion','LIKE',$cod);
			}

			if($form->estado){
				$proformaDetalle = $proformaDetalle->where('estado',$form->estado);
			}

			if($form->cod_nemo){
				$cod = "%".trim(strtolower($form->cod_nemo))."%";
				$proformaDetalle = $proformaDetalle->where('cod_nemo','LIKE',$cod);
			}

			 if($form->periodo_in != ''){
			           $fecha = explode('-', $form->periodo_in);
			                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
			                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
 
			  $proformaDetalle = $proformaDetalle->whereBetween('fecha_in', 
			  							array($desde.' 00:00:00',$hasta.' 23:59:59'));
			        }
			  if($form->periodo_out != ''){
			           $fecha = explode('-', $form->periodo_out);
			                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
			                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
 
			  $proformaDetalle = $proformaDetalle->whereBetween('fecha_out', 
			  							array($desde.' 00:00:00',$hasta.' 23:59:59'));
			        }        
			
				if(isset($form->pago_desde_hasta)){
					if($form->pago_desde_hasta != ''){
							$fecha = explode('-', $form->pago_desde_hasta);
									$desde = $this->formatoFechaEntrada(trim($fecha[0]));
									$hasta = $this->formatoFechaEntrada(trim($fecha[1]));
		
							$proformaDetalle = $proformaDetalle->whereBetween('fecha_notificado_nemo', 
												array($desde.' 00:00:00',$hasta.' 23:59:59'));
					}        
				} 
			
			  $proformaDetalle = $proformaDetalle->where('id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);       

			  $proformaDetalle	= $proformaDetalle->get();

			/* foreach($proformaDetalle as $key=>$profDetalle){
				if($profDetalle->id_empresa != Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa){
					unset($proformaDetalle[$key]);
				}
			}*/
        

        return response()->json(['data'=>$proformaDetalle]);
	}//function

	public function getVencimiento(Request $req)
	{

		$fechaTrim = trim($req->input('fechaVencimiento'));
		$periodo = explode('-', trim($fechaTrim));
		$checkIn = explode("/", trim($periodo[0]));
		$checkInFormateado = $checkIn[2]."-".$checkIn[1]."-".$checkIn[0];
	//	$vto = DB::select('SELECT get_fecha_vencimiento(?,?,?)',[$checkInFormateado,$req->input('cliente_id'),$this->getIdEmpresa()]);
	    $vto = DB::select('SELECT get_fecha_vencimiento(?,?)',[$checkInFormateado,$this->getIdEmpresa()]);
		$fechaVencimiento = $vto[0]->get_fecha_vencimiento;

		$date = explode('-', $fechaVencimiento);
           $fecha = $date[2]."/".$date[1]."/".$date[0];
           // return $fecha;
		 
		return response()->json(['data'=>$fecha]);
	}

	public function almacenarCalificacion(Request $req){

		// dd($req->input('comentario'));
//dd($req->all());
		$err = "true";

		$proforma = DB::table('proformas')->where('id',$req->input('id_proforma'))->first();
		// dd($proforma->calificacion );
		if($proforma->comentario_calificacion == ''){ 
		
		try{				
							//NO GUARDAR DATOS EN CASO DE ERROR
							DB::beginTransaction();
							//INSERTAR EN PROFORMA
							 DB::table('proformas')
							->where('id',$req->input('id_proforma'))
							->update(['comentario_calificacion'=>$req->input('comentario_calificacion'),
									  'fecha_hora_post_venta'=>date('Y-m-d H:i:00'),
									  'reconfirmacion' => true,
									  'id_usuario_post_venta'=>$this->getIdUsuario()]);

							/*//INSERTAR COMENTARIO EN LOS COMENTARIOS
							$comentario = new HistoricoComentariosProforma;
							$comentario->fecha_hora = date('Y-m-d H:i:00');
							$comentario->id_proforma = $req->input('id_proforma');
							$comentario->id_usuario = $this->getIdUsuario();
							$comentario->id_empresa = $this->getIdEmpresa();
							$comentario->comentario = $req->input('comentario');
							$comentario->save();
							$idComentario =$comentario->id;*/

						//INSERTAR EN EL TIME LINE	
						/*DB::table('estados_proforma_control')->insert([
						    'fecha' => date('Y-m-d H:i:00'), 
						    'id_proforma' => $req->input('id_proforma'),
						    'id_usuario' => $this->getIdUsuario(), 
						    'id_comentario' =>  $idComentario,
						    'estado_id' => '46']);
							*/

				
			} 
			catch(\Exception $e)
			{
				$err = "false";
				DB::rollBack();
			}	

			DB::commit();

		} else {
			$err = "false";
		} 
		
			return response()->json(['resp'=>$err]);
	}//function


	public function getpostventa(Request $req){
		$proforma = Proforma::where('id',$req->id_proforma)->first(['calificacion','comentario_calificacion']);
		return response()->json(['resp'=>$proforma]);
	}//function


	public function solicitarEmisionTicket(Request $req){
		$err = true;
		$porcentaje = str_replace(',','.', str_replace('.','', $req->input('porcentaje')));
		$precio_venta = str_replace(',','.', str_replace('.','', $req->input('precio')));

		$proforma = DB::table('proformas')->where('id',$req->input('id_proforma'))->first();

		$historicoComentario = new HistoricoComentariosProforma;
		$historicoComentario->comentario = '===========Solicitud Ticket===========';
		$historicoComentario->id_proforma =$req->input('id_proforma');
		$historicoComentario->fecha_hora = date('Y-m-d H:i:00');
		$historicoComentario->id_usuario = $this->getIdUsuario();
		$historicoComentario->id_empresa = $this->getIdEmpresa();

		try{	
			//NO GUARDAR DATOS EN CASO DE ERROR
			DB::beginTransaction();

			$historicoComentario->save();
			DB::table('solicitud_emision_ticket')
					->where('id_proforma',$req->input('id_proforma'))
					->where('estado','true')
					->update(['estado'=>'false']);



			DB::table('solicitud_emision_ticket')->insert([
				    ['fecha_hora_solicitud' => date('Y-m-d H:i:00'), 
				    'id_proforma' => $req->input('id_proforma'),
				    'id_usuario' => Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario, 
				    'comentario' => $req->input('comentario'),
				    'id_producto_ticket'=> $req->input('id_tipo_ticket'),
				    'precio_venta' => ($precio_venta != '') ? $precio_venta: null,
					'porcentaje_comision'=> ($porcentaje != '') ? $porcentaje: null]
				]);

		if($proforma->estado_id != '47'){
		//CAMBIAR A ESTADO SOLICITUD DE TICKET
		DB::table('proformas')
				->where('id',$req->input('id_proforma'))
				->update(['estado_id'=>47]);
			}	


		} catch(\Exception $e)
			{
				$err = false;
				DB::rollBack();
			}

			DB::commit();


		return response()->json(['operacion'=>$err]);	


	}//function



	public function getInfoEmisionTicket(Request $req){

		$id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

		$info = DB::table('solicitud_emision_ticket as st')
				->select('st.*','p.id_usuario as id_usuario_proforma')
				->leftJoin('proformas as p','p.id','=','st.id_proforma')
				->where('id_proforma', $req->id_proforma)
				->where('estado', 'true')
				->first();

		if(!empty($info)){	
				$info->propietario = false;
				$info->puede_editar = false;
			if($info->id_usuario_proforma == $id_usuario){
				$info->propietario = true;
			} else {
				$permiso_edicion = DB::select("SELECT COUNT(*) cant 
								               FROM persona_permiso_especiales 
								               WHERE id_permiso = 19 AND id_persona = ".$id_usuario);

				if($permiso_edicion[0]->cant > 0){
					//TIENE PERSMISO DE EDICION
					$info->puede_editar = true;
				}
			}
		}	


		return response()->json(['data'=>$info]);
	}//function


	public function setformaPago(Request $req){

			$option = $req->option;
			$id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			$fecha_actual = date('Y-m-d H:i:00');
			$err = false;
			$msj = '';
			$form = (object) $req->form;
			$id_proforma = $form->id_proforma;
			$form = $form->datos;
			$monto_proforma = (integer)DB::select('SELECT get_monto_proforma m FROM get_monto_proforma('.$id_proforma.')')[0]->m;

		if($option === '1'){

			if($monto_proforma > 0 && !empty($form) && count($form) > 0){

			try{

				DB::beginTransaction();
				$insert_fp = [];
				$id_cabecera = null;

				$id_cabecera_delete = DB::table('proforma_fp_cabecera')
									 ->where('id_proforma', $id_proforma )
									 ->first();
				if(!empty($id_cabecera_delete)){				 
				$id_cabecera_delete = $id_cabecera_delete->id;					 
				DB::table('proforma_fp_cabecera')->where('id_proforma', $id_proforma )->delete();
				DB::table('proforma_fp_detalle')->where('id_cabecera', $id_cabecera_delete )->delete();
				DB::table('proformas')->where('id', $id_proforma )->update(['id_fp'=>null]);
				 }	

					$id_cabecera = DB::table('proforma_fp_cabecera')->insertGetId ([
					'fecha_hora' => $fecha_actual, 
				    'id_proforma' => $id_proforma,
				    'id_usuario' => $id_usuario, 
				    'importe_total' => $monto_proforma
						]);
					//INSERTAR ID DE CABECERA EN PROFORMAS
					DB::table('proformas')->where('id', $id_proforma )->update(['id_fp'=>$id_cabecera]);


				foreach ($form as $key => $value) {
					$f = (object) $value;

					$importe = str_replace(',','.', str_replace('.','', $f->importe));
					$importe_pago = str_replace(',','.', str_replace('.','', $f->importePago));

					$insert_fp[] = [
					'id_forma_pago'=>$f->id_forma_pago,
					'documento'=>$f->documento,
					'importe'=>$importe,
					'id_moneda'=>$f->id_moneda_pago,
					'importe_pago'=>$importe_pago,
					'id_cabecera'=>$id_cabecera
					];
				}
		

				$err = true;
				DB::table('proforma_fp_detalle')->insert($insert_fp);


			} catch(\Exception $e){
				DB::rollBack();
			}
				DB::commit();

			} else {
				$msj = 'El monto de la proforma no puede ser cero "0"';
			}


		} else if($option === '2'){

				
				try{
				DB::beginTransaction();

				$id_cabecera_delete = DB::table('proforma_fp_cabecera')
									 ->where('id_proforma', $id_proforma )
									 ->first();
				if(!empty($id_cabecera_delete)){	
					$id_cabecera_delete = $id_cabecera_delete->id;		
					DB::table('proforma_fp_cabecera')->where('id_proforma', $id_proforma )->delete();
					DB::table('proforma_fp_detalle')->where('id_cabecera', $id_cabecera_delete )->delete();
					DB::table('proformas')->where('id', $id_proforma )->update(['id_fp'=>null]);
					$err = true;
				}
				
				} catch(\Exception $e){
				DB::rollBack();
			}
				DB::commit();	
		}	

		return response()->json(['err'=>$err,'msj'=>$msj]);
	}



	public function getformaPago(Request $req){
		
		// dd($req->all());
		if($req->option === '1'){
			$form = (object) $req->form;

			$importe = str_replace(',','.', str_replace('.','', $form->importe_pago));
			$linea_fp = DB::select("
				SELECT 
				get_monto_cotizado(".$importe.",".$form->id_moneda_pago.",id_moneda_venta) importe_cotizado,
			    get_monto_proforma(id) monto_proforma 
				FROM proformas
				WHERE id = ".$form->id_proforma);

			return response()->json(['linea_fp'=>$linea_fp]);
		}

		if($req->option === '2'){

		$fp_info =  new \StdClass;
		$fp_info->monto_proforma = DB::select('SELECT get_monto_proforma m FROM get_monto_proforma('.$req->id_proforma.')')[0]->m;
		$fp_info->get_fp = DB::select("SELECT fpd.id_forma_pago,
                                              fpd.documento,
                                              fpd.importe,
                                              fpd.importe_pago,
                                              fpd.id_moneda,
                                              cu.currency_code,
                                              fc.denominacion forma_pago_txt
						             FROM proforma_fp_cabecera fpc, proforma_fp_detalle fpd, currency cu, forma_pago_cliente fc
						             WHERE fpc.id = fpd.id_cabecera 
						             AND cu.currency_id = fpd.id_moneda
						             AND fc.id = fpd.id_forma_pago
						             AND fpc.id_proforma = ".$req->id_proforma);	

		return response()->json($fp_info);

		}

	
	}

	public function servicioDtp(Request $req){
		$currency = Currency::where('activo', 'S')->get();
		$reservaServicios = ReservasServicio::with('personas', 'usuarios', 'proveedor','id_agente_dtp')
											->where('id_estado_reserva', 2)
											->get();
		foreach($reservaServicios as $key=>$reservaServicio){
			$proveedor = Persona::where('usuario_amadeus', $reservaServicio->proveedor)
									   ->get(['nombre']);
			$reservaServicios[$key]->proveedor = $proveedor[0]->nombre."(".$reservaServicio->proveedor.")";

		}

 		/*echo "<pre>";
		print_r($reservaServicios);	*/	
		return view('pages.mc.proforma.serviciosDtp')->with([
															'reservaServicios'=>$reservaServicios,
															'monedas'=>$currency 
											                  ]);
		
	}	

    public function ajaxProforma(Request $req){
    		$personaLogin = $this->getDatosUsuario();
			$tipoPersona = $personaLogin->id_tipo_persona;
			$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

			$flag = 0;
			
			$proformas = DB::table( 
					DB::raw("( 
						SELECT 

						p.id,
						p.estado_id, 
						p.pasajero_online,  
						p.id_prioridad, 
						p.id_moneda_venta,
						estados.denominacion,
						f.nro_factura, 
						p.calificacion,
						p.comentario_calificacion,
						p.factura_id,
						p.fecha_alta, 
						p.check_out,
						to_char(p.fecha_alta , 'DD/MM/YYYY') as fecha_alta_format,
						p.check_in,
						to_char(p.check_in , 'DD/MM/YYYY') as check_in_format,
						p.cliente_id,
						concat(cliente.nombre,' ',cliente.apellido) cliente_n,
						p.vendedor_id,
					    concat(vendedor.nombre,' ',vendedor.apellido) vendedor_n, 
						p.pasajero_id,	
					    concat(pasajero.nombre,' ',pasajero.apellido) pasajero_n, 
						p.responsable_id,
						concat(operativo.nombre,' ',operativo.apellido) operativo_n, 
						p.id_usuario, 
						concat(usuario.nombre,' ',usuario.apellido) usuario_n,
					    usuario.id_sucursal_empresa id_sucursal_empresa ,
					    p.id_empresa,
					    (SELECT COUNT(*) 
					    FROM proformas_detalle 
					    WHERE activo = true and id_producto IN(1,9) and id_proforma = p.id ) as ticketCount
		FROM proformas p 
		LEFT JOIN personas as cliente
		ON cliente.id = p.cliente_id 
		LEFT JOIN personas as usuario 
		ON usuario.id = p.id_usuario
		LEFT JOIN personas as vendedor
		ON vendedor.id = p.vendedor_id
		LEFT JOIN personas as pasajero
		ON pasajero.id = p.pasajero_id
		LEFT JOIN personas as operativo 
		ON operativo.id = p.responsable_id
		LEFT JOIN estados
		ON estados.id = p.estado_id
		LEFT JOIN facturas as f
		ON f.id = p.factura_id ) as result")		
		);
		
		$proformas = $proformas->where('id_usuario', $idUsuario);

		$proformas = $proformas->where('estado_id', 1);

		$proformas = $proformas->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

		$proformas = $proformas->get();
    
    	return json_encode($proformas); 

	}

    public function asignarProformaReserva(Request $req){
		$idProducto = 0;
		switch ($req->input('idProducto')) {
			case 1://alojamiento
				$idProducto = 24384;
				break;
			case 2: //circuitos
				$idProducto = 24385;
				break;
			case 3: //traslado
				$idProducto = 24386;
				break;
			case 4: //actividades
				$idProducto = 24387;
				break;
		}
		if($req->input('idProforma') != 0){
			$validado = 0;
			$reservas = NemoReserva::where('id',$req->input('idReserva'))
									->first(['vendedor_id','agencia_id']); 

			$proforma = Proforma::where('id', $req->input('idProforma'))
									->first(['vendedor_id','cliente_id']);
			if($reservas->vendedor_id == $proforma->vendedor_id && $reservas->agencia_id == $proforma->cliente_id){					
				$validado = 1;
			}
		}else{
			$validado = 1;
		}

		$destino = $req->input('idDestinos');

		if($destino == ""){
			$destino = 0;
        }	

		if($validado == 1){
			$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			$reservaProforma = DB::select("SELECT * FROM generar_proforma_reserva(".$destino.",".$req->input('idProforma').",".$req->input('idReserva').",".$idProducto.",".$idUsuario.",".$this->getIdEmpresa().")");
			$respuesta = explode('_', $reservaProforma[0]->generar_proforma_reserva);
			
			$respuestaArray[] = [
				'p_cod_retorno_out'=>'OK',
				'proforma'=>$respuesta[0],
				'item'=>$respuesta[1] 
				];
		}else{
			$respuestaArray[] = [
				'p_cod_retorno_out'=>'Solo puedes tomar tus reservas',
				'proforma'=>0,
				'item'=>0
				];

		}	
	   return json_encode($respuestaArray); 
	}



    public function asignarProformaServicios(Request $req){
		$idProducto = "";
		switch ($req->input('idProducto')) {
			case 'Transfer':
				$idProducto = 10;
				break;
			case 'Tour':
				$idProducto = 11;
				break;
			case 'Activity':
				$idProducto = 23;
				break;
		}

		$destino = $req->input('idDestinos');

		if($destino == ""){
			$destino = 0;
        }	

        $resarvaServicios = DB::select("SELECT * FROM sp_registrar_reserva_servicios(".$destino.",".$req->input('idServicio').",".$req->input('idProforma').",".$idProducto.",".$this->getIdEmpresa().")");

		if($resarvaServicios[0]->p_cod_retorno_out == 0){
		    DB::table('reservas_servicios')
							    ->where('id',$req->input('idServicio'))
							    ->update([
									   		'numero_proforma'=>$resarvaServicios[0]->p_id_proforma_out,
									   		'codret_proforma'=>$resarvaServicios[0]->p_cod_retorno_out,
									   		'id_estado_reserva'=>2,
									   		'numero_de_item'=>$resarvaServicios[0]->p_item_detalle_numero_out
							   			 ]);
        }	
	   return json_encode($resarvaServicios); 
    
   } 

    public function buscarServicio(Request $req){
        /*echo "<pre>";
		print_r($req->all());*/
		if($req->input('tipo_servicio')!="" || $req->input('proforma')!="" || $req->input('localizador')!="" ||$req->input('pasajero')!="" || $req->input('divisa_id')!=""){
			$reservaServicios = ReservasServicio::with('personas', 'usuarios', 'proveedor','id_agente_dtp')->where(function($query) use($req)
	        {
	           	if(!empty($req->input('tipo_servicio'))){
	                    $query->where('tipo_servicio',$req->input('tipo_servicio'));
				}

				if(!empty($req->input('periodo'))){
					$fecha = explode(' - ', $req->input('periodo'));
					
					$desdeBusqueda = explode("/",$fecha[0]);
				    $hastaBusqueda = explode("/",$fecha[1]);

					$fechaDesde = $desdeBusqueda[2]."-".$desdeBusqueda[1]."-".$desdeBusqueda[0].' 00:00:00';
					$fechaHasta = $hastaBusqueda[2]."-".$hastaBusqueda[1]."-".($hastaBusqueda[0]). ' 23:59:59';

	 				$query->whereBetween('fecha_reserva', array(
																	$fechaDesde,
																	$fechaHasta
																	));		
	 			}
	 			if(!empty($req->input('proforma'))){
	                    $query->where('numero_proforma',$req->input('proforma'));
				}

				if(!empty($req->input('localizador'))){
	                    $query->where('localizador',$req->input('localizador'));
				}

				if(!empty($req->input('pasajero'))){
	                    $query->where('nombre_pasajero', 'like', '%'.$req->input('pasajero').'%');
				}

				if(!empty($req->input('divisa_id'))){
	                    $query->where('moneda_proveedor',$req->input('divisa_id'));
				}

	        })->get();
	    }else{   
			$reservaServicios = ReservasServicio::with('personas', 'usuarios', 'proveedor','id_agente_dtp')
											->where('id_estado_reserva', 2)

											->get();

		}

		foreach($reservaServicios as $key=>$reservaServicio){
			$proveedor = Persona::where('usuario_amadeus', $reservaServicio->proveedor)
									   ->get(['nombre']);
			$reservaServicios[$key]->proveedor = $proveedor[0]->nombre."(".$reservaServicio->proveedor.")";

		}

		return json_encode($reservaServicios);

	}	

    public function procesarPago(Request $req){
		$totalPago = 0;
		$codigo = "";
		foreach($req->input('datos') as $key=>$datos){
			if($datos['id_forma_pago'] == 2){
				$totalPago = floatval($totalPago) + floatval(str_replace(',','.', str_replace('.','', $datos['importe'])));
				if($key != 0) {
					$codigo .=", ";	
				}
				$codigo .=$datos['documento']; 
			}
		}

		$registrarGastos = DB::select("SELECT public.get_gastos_administrativos(".floatval($totalPago).",".$req->input('id_proforma').",".$this->getIdEmpresa().",'".$codigo."')");

		$respuesta = $registrarGastos[0]->get_gastos_administrativos;


		$detallesProformas = ProformasDetalle::where('id_proforma',$respuesta)
									 ->where('origen','A')
									 ->whereIn('id_producto', [39,38])
									 ->where('activo',true)
									 ->orderBy('item','ASC')
									 ->get();

   	    return json_encode($detallesProformas);

	}


   public function eliminarItemsPago(Request $req){

		$detallesProformas = ProformasDetalle::where('id_proforma',$req->input('idProforma'))
									 ->where('origen','A')
									 ->whereIn('id_producto', [39,38])
									 ->where('activo',true)
									 ->get(['id', 'item']);

		foreach($detallesProformas as $key=>$detalle){	
			DB::table('proformas_detalle')
						        ->where('id',$detalle['id'])
						        ->update([
						            	'activo'=>false,
						            	]);
		}			 
		
		$actualizarSenha = DB::select('SELECT public.actualizar_senha('.$req->input('idProforma').', '.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');
		return json_encode($detallesProformas);
   }	

   public function doGrupoCom(Request $req){
		   $idCliente = 0;
		   $err = true;
		//    dd($req->all());

		   if($req->input('idGrupo')){
			   
			   if($req->input('idCliente')){
				$grupo = DB::select('SELECT get_control_grupo('.$req->input('idCliente').','.$req->input('idGrupo').') as result');

				if($grupo[0]->result !== 'OK'){
				  $err = false;
				}

			   } else {
				 $err = false;
			   }
			   
			   
		   }
		//    dd($grupo[0]);
		// $grupo = Grupo::where('id',$req->input('idGrupo'))->get(['cliente_id']);
		// if(!empty($grupo[0])){
		// 	$idCliente = $grupo[0]->cliente_id;
  		// }

		return response()->json(['err'=>$err]);
   }//


   private function eventAviso($id_proforma,$id_usuario) {
	$id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	$activar = 0;
	$caracteres = 100;
	$acuerdo = '';
	$err = false;
	
	

	try{
			$proforma = Proforma::with('cliente')->where('id',$id_proforma)->whereNotIn('estado_id',array(5,4))->firstOrFail();
			$acuerdo = $proforma->cliente->acuerdos_especiales;
			$tipo_persona_autorizar = DB::select('
			SELECT COUNT(*) AS cant 
			FROM PERSONAS 
			WHERE id = '.$proforma->cliente_id.' 
			AND id_tipo_persona IN (
				SELECT id_tipo_persona 
				FROM tipo_persona_autorizacion
				WHERE requiere_autorizacion = true 
				AND id_empresa = '.$this->getIdEmpresa().')');

			$estado = DB::table('estados_proforma_control')
			->where('id_proforma',$id_proforma)
			->where('id_usuario',$id_usuario)
			->where('estado_id',51)->count();


			if($estado == 0 && $acuerdo != null){
				$acuerdo = strip_tags($acuerdo);
				$activar++;
			} 

			if($estado == 0 && $tipo_persona_autorizar[0]->cant > 0){
				$acuerdo .='     **Este cliente requiere autorización de Administración para facturar';
				$activar++;
			}

			if($activar > 0){ 
				$comentarioSub = substr($acuerdo, 0, $caracteres);
		
			$comentario = new HistoricoComentariosProforma;
			$comentario->fecha_hora = date('Y-m-d H:m:i');
			$comentario->id_proforma =$id_proforma;
			$comentario->id_usuario = $id_usuario;
			$comentario->id_empresa = $id_empresa;
			$comentario->comentario = strip_tags($comentarioSub).'...';
			$comentario->save();
			$idComentario =$comentario->id;

			DB::table('estados_proforma_control')->insert([
				['fecha' => date('Y-m-d H:i:00'), 
				'id_proforma' => $id_proforma,
				'id_usuario' => $id_usuario, 
				'id_comentario' => $idComentario,
				'estado_id' => '51']
			]);
		}
		
		
		} catch(\Exception $e){

			$err = false;
		}


		if($activar > 0){
			$err = true;
		}

		// DB::commit();
		

		$data =  new \StdClass;
		$data->err = $err;
		$data->acuerdo = $acuerdo;
		return $data;			

   }//

 	public function grupoProducto(Request $req){
		$productos = Producto::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							 ->where('id', '=', $req->input('dataPrducto'))
							 ->get(['id_grupos_producto','nemo']);

	    $nemoArray = ConfigNemo::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
								->where('id_producto_gestur', '=', $req->input('dataPrducto'))
								->first();
		$data_resp = new \stdClass();
		$data_resp->grupo_producto = 0;
		$data_resp->producto_nemo = 0;
		$data_resp->producto_cangoroo = 0;

		if(isset($productos[0]->id_grupos_producto)){
			if(isset($nemoArray->producto_nemo) && $nemoArray->producto_nemo){
				$data_resp->grupo_producto = $productos[0]->id_grupos_producto;
				$data_resp->producto_nemo = $nemoArray->producto_nemo;
			} else if(isset($nemoArray->producto_cangoroo) && $nemoArray->producto_cangoroo){
				$data_resp->grupo_producto = $productos[0]->id_grupos_producto;
				$data_resp->producto_cangoroo = $nemoArray->producto_cangoroo;
			} else {
				$data_resp->grupo_producto = $productos[0]->id_grupos_producto;
				$data_resp->producto_nemo = 0;
				$data_resp->producto_cangoroo = 0;
			}

		}

		return response()->json($data_resp);
	}
	   
	  
	public function verificarProforma(Request $req){
			$control_empresa = Empresa::where('id', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first(['control_de_rentabilidad']);
			if($control_empresa->control_de_rentabilidad == 'V'){
				$data = $this->checkProforma($req->input('idProforma'));
			}else{
				$data = $this->comprobarProforma($req->input('idProforma'));
			}	
		return response()->json($data);
	}//


	/**
	 * Verificar si la proforma debe ser autorizada segun
	 * las reglas establecidas aqui
	 */
	private function comprobarProforma($id_proforma){
		$authMarkup = false;
		$authFacturarAutorizado = false;
		$pendienteAuth = [];
		$err = true;
		$indicador = true;
		try{
			$proforma = Proforma::where('id',$id_proforma)->first(['cliente_id']);
			$autorizacionProforma = Autorizaciones::where('id_proforma',$id_proforma)->orderBy('id','DESC')->first();
			$pendienteAuth = [];
						//VERIFICAR SI REQUIERE AUTORIZACION PARA FACTURAR
			$tipo_persona_autorizar = DB::select('
						SELECT COUNT(*) AS cant 
						FROM PERSONAS 
						WHERE id = '.$proforma->cliente_id.' 
						AND id_tipo_persona IN (
							SELECT id_tipo_persona 
							FROM tipo_persona_autorizacion
							WHERE requiere_autorizacion = true 
							AND id_empresa = '.$this->getIdEmpresa().')');
			
		
			if(!empty($autorizacionProforma)){
			//!!!! PENDIENTE VALIDAR CLIENTE Y MARKUP CON MENSAJE PERSONALIZADO

					//VERIFICAR AUTORIZACION DE MARKUP

					if($autorizacionProforma->id_estado == 38 && $proforma->markup == $autorizacionProforma->valor){
						$authMarkup = true;
					} 

					//VERIFICAR AUTORIZACION DE TIPO PERSONA
					if($tipo_persona_autorizar[0]->cant > 0 && $autorizacionProforma->id_estado == 38 && $proforma->cliente_id == $autorizacionProforma->cliente_id){

					//VERIFICAR SI REQUIERE AUTORIZACION DE FACTURACION
					if($tipo_persona_autorizar[0]->cant > 0 && $authFacturarAutorizado == false){
						array_push($pendienteAuth,1);
					}
					$detallesProformas = ProformasDetalle::where('id_proforma',$id_proforma)
														->where('activo', true)
														->get(['cumple_rentabilidad_minima']);
					foreach($detallesProformas as $key=>$value){
						if($value->cumple_rentabilidad_minima == false){
							$indicador = false;
						}
					}
					$authFacturarAutorizado = true;
				}
			}else{
				if($tipo_persona_autorizar[0]->cant > 0 && $authFacturarAutorizado == false){
					array_push($pendienteAuth,1);
				}
				$detallesProformas = ProformasDetalle::where('id_proforma',$id_proforma)
													->where('activo', true)
													->get(['cumple_rentabilidad_minima']);
				foreach($detallesProformas as $key=>$value){
					if($value->cumple_rentabilidad_minima == false){
						$indicador = false;
					}
				}
				$authFacturarAutorizado = true;

			}

		//	$getNotificacion= DB::select("SELECT public.insert_notificacion(".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.",null, null, null,15,".$id_proforma.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");

			if($indicador == false){
				array_push($pendienteAuth,3);
			}
		} catch(\Exception $e) {
			$err = false;
		}

		return ['err'=>$err, 'pendienteAuth'=>$pendienteAuth];
	}

	/**
	 * Verificar si la proforma debe ser autorizada segun
	 * las reglas establecidas aqui
	 */
	private function checkProforma($id_proforma){

		// $id_proforma = $req->idProforma;
		$authMarkup = false;
		$authFacturarAutorizado = false;
		$pendienteAuth = [];
		$err = true;
		try{
			$proforma = Proforma::where('id',$id_proforma)->first(['porcentaje_ganancia','cliente_id']);
			$empresaMarkup = DB::select("SELECT public.get_markup_minimo_venta(".$this->getIdEmpresa().")");
			$autorizacionProforma = Autorizaciones::where('id_proforma',$id_proforma)->orderBy('id','DESC')->first();
			//VERIFICAR SI REQUIERE AUTORIZACION PARA FACTURAR
			$tipo_persona_autorizar = DB::select('
												SELECT COUNT(*) AS cant 
												FROM PERSONAS 
												WHERE id = '.$proforma->cliente_id.' 
												AND id_tipo_persona IN (
													SELECT id_tipo_persona 
													FROM tipo_persona_autorizacion
													WHERE requiere_autorizacion = true 
													AND id_empresa = '.$this->getIdEmpresa().')');

			if(!empty($autorizacionProforma)){
				//!!!! PENDIENTE VALIDAR CLIENTE Y MARKUP CON MENSAJE PERSONALIZADO

				//VERIFICAR AUTORIZACION DE MARKUP

					if($autorizacionProforma->id_estado == 38 && $proforma->markup == $autorizacionProforma->valor){
						$authMarkup = true;
					} 

				//VERIFICAR AUTORIZACION DE TIPO PERSONA
				if($tipo_persona_autorizar[0]->cant > 0 && $autorizacionProforma->id_estado == 38 && $proforma->cliente_id == $autorizacionProforma->cliente_id){
					$authFacturarAutorizado = true;
				}
			}

			//VERIFICAR SI REQUIERE AUTORIZACION DE FACTURACION
			if($tipo_persona_autorizar[0]->cant > 0 && $authFacturarAutorizado == false){
				array_push($pendienteAuth,1);
			}

			//VERIFICAR SI REQUIERE AUTORIZACION POR MARKUP
			$markup = (float)$proforma->porcentaje_ganancia;
			$markupEmpresa = (float)$empresaMarkup[0]->get_markup_minimo_venta;
			if($markupEmpresa > 0 && $markupEmpresa != null){
				if(($markup <= $markupEmpresa) && $authMarkup == false){
					array_push($pendienteAuth,2);
				}
			}
		} catch(\Exception $e) {
			$err = false;
		}
	//	$getNotificacion= DB::select("SELECT public.insert_notificacion(".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.",null, null, null,15,".$id_proforma.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");

		return ['err'=>$err, 'pendienteAuth'=>$pendienteAuth];
	}//


	/**
	 * Solicitud de verificacion de la proforma en la pagina de detalles proforma
	 */
	public function solicitarVerificacionProforma(Request $req)
	{
		$opcion = $req->input('option');
		$id_proforma = $req->idProforma;
		$err = "false";
		$idUsuario = $this->getIdUsuario();

		 	/*echo '<pre>';
			print_r('SELECT * FROM controlar_proforma('.$id_proforma.','.$idUsuario.')');
			die; */

			try
			{
				$resp = DB::select('SELECT * FROM controlar_proforma(?,?)',[$id_proforma,$idUsuario]);
				$mensaje = $resp[0]->controlar_proforma;

				if ($mensaje == 'OK') 
				{
					if($opcion == '1')
					{	//SOLICITAR AUTORIZACION
						$proforma = DB::table('proformas')
										->where('id',$id_proforma)
										->update(['estado_id'=>'24']);
					}
					else 
						if($opcion == '2')
						{
							$proforma = DB::table('proformas')
									->where('id',$id_proforma)
									->update(['estado_id'=>'3']);
						} 
					$err = "true";
				}
				$getNotificacion= DB::select("SELECT public.insert_notificacion(".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.",null, null, null,15,".$id_proforma.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");

			} 
			catch(\Exception $e)
			{
				$err = "false";
			}	
		

		return response()->json(array('err'=>$err, 'mensaje'=>$mensaje));
	}


		public function filtrarTicketTabla(Request $request){
		$tickets = Ticket::with('grupo', 'currency', 'persona')
						    ->where('id_tipo_ticket', '=', $request->input('dataTipoId'))
						    //->where('id_proforma', null)
							->where(
								function($query) {
								  return $query
						    ->where('id_proforma', null)
										 ->orWhere('id_proforma', 0);
								 })
						    ->where('id_estado','=','25')
						    //->where('id_currency_costo','=',$request->input('idMonedaProforma'))
						    ->where('id_empresa','=',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->get();
		
		$resultado = array();
		foreach($tickets as $key=>$ticket){
			if(!$tickets->isEmpty()){
				$getVenta = DB::select('SELECT public.get_venta_ticket('.$ticket->id.')');
				$precio_venta = $getVenta[0]->get_venta_ticket;
				$tickets[$key]->precio_venta = $precio_venta;
				$resultado[$key]['id'] = $ticket->id;
				$resultado[$key]['nro_ticket'] = $ticket->numero_amadeus;
				$resultado[$key]['pnr'] = $ticket->pnr;
				$resultado[$key]['pasajero'] = $ticket->pasajero;
				if(isset($ticket->currency->currency_code)){
				$resultado[$key]['moneda'] = $ticket->currency->currency_code;
				}else{
					$resultado[$key]['moneda'] = "";
				}
				$resultado[$key]['venta'] = $precio_venta;
				if($ticket->comision != 0){
					$comision = 'Comisionable';
				}else{
					$comision = 'Neto';
				}
				$resultado[$key]['tarifa'] = $comision;
				if(isset($ticket->persona->nombre)){
					$resultado[$key]['aerolinea'] = $ticket->persona->nombre;
				}else{
					$resultado[$key]['aerolinea'] = '';
				}
				if(!empty($ticket->grupo)){
					$resultado[$key]['grupo'] = $ticket->grupo->denominacion;
				}else{
					$resultado[$key]['grupo'] = "";
				}
			}
		}	

		/*echo '<pre>';
		print_r($tickets);		
		/*echo '<pre>';
		print_r($resultado);*/

		return json_encode($resultado);
	}	

		public function datosProformaDetalle(Request $request){
			$detallesProformas = ProformasDetalle::with('ticket', 'producto','prestador','proveedor')
												 ->where('id_proforma',$request->input('dataProforma'))
												 ->where('id',$request->input('dataDetalleProforma'))
									 			  ->get();
			return json_encode($detallesProformas);
		}



	public function getDatosNemo(Request $request){
		$reservaNemo = DB::select(" select 
									nr.id, 
									nr.hotel_booking_service_item_id, 
									p.denominacion, 
									nr.fecha_creacion, 
									nr.prestador_name, 
									nr.check_in ,
									nr.check_out,
									nr.det_client_name, 
									nr.hotel_supplier_desc, 
									nr.dest_city_desc, 
									nr.dest_country_desc, 
									c.currency_code, 
									nr.price_amount_global, 
									nr.email,
									nr.id_proforma
									from nemo_reservas nr, productos p, estados e, currency c
									where  nr.id_tipo_producto = p.id
									and nr.id_estado_res_dtp = e.id
									and nr.id_moneda = c.currency_id
									and nr.id_proforma IS NULL"); 

		return json_encode($reservaNemo);	
	}	

	public function guardarReservaNemo(Request $req)
	{
 		$mensaje = new \StdClass;
 		try
 		{
	 		$reservaServicios = ReservasNemo::where('id', $req->input('reservaNemo'))->get();

	 		$resarvaNemo = DB::select("SELECT * FROM sp_registrar_reserva_nemo(".$req->input('reservaNemo').",".$req->input('idProforma').",".$this->getIdUsuario().",".$this->getIdEmpresa().")");
			if($resarvaNemo[0]->p_cod_retorno_out == 0){
			    DB::table('nemo_reservas')  
								    ->where('id',$req->input('reservaNemo'))
								    ->update([
										   		'id_proforma'=>$resarvaNemo[0]->p_id_proforma_out,
										   		'id_detalle_proforma'=>$resarvaNemo[0]->p_id_proforma_detalle_out,
										   		'id_estado_res_dtp'=>2,
										   		'numero_item'=>$resarvaNemo[0]->p_item_detalle_numero_out
								   			 ]);
				if(isset($resarvaNemo[0]->p_id_proforma_detalle_out)){
					$detallesProformas = ProformasDetalle::with('producto','proveedor','prestador', 'currencyCosto')
									->where('activo',true)
									->where('id', $resarvaNemo[0]->p_id_proforma_detalle_out)
									->where('id_empresa', $this->getIdEmpresa())
									->orderBy('item', 'ASC')
									->get();
				}	
		        $mensaje->codigo = 'OK';
				$mensaje->id_proforma = $resarvaNemo[0]->p_id_proforma_out;
				$mensaje->id_detalle_proforma = $resarvaNemo[0]->p_id_proforma_detalle_out;
				$mensaje->item = $resarvaNemo[0]->p_item_detalle_numero_out;
				$mensaje->proveedor = $reservaServicios[0]->hotel_supplier_desc;
				$mensaje->detalle = $detallesProformas;
	        }
	   } 
		catch(\Exception $e)
		{
	        $mensaje->codigo = 'ERROR';
	        $mensaje->mensaje = 'Ocurrio un error! La reserva no fue guardada';
		}

		return response()->json($mensaje);
	}	

		public function getPasajeroPrincipal(Request $req)
	{

		$destinos = DB::select("SELECT concat(nombre,' ',apellido,' - '||documento_identidad) as pasajero_data, id 
							    FROM personas WHERE id_tipo_persona 
							    IN (SELECT id FROM tipo_persona WHERE puede_ser_pasajero = true) AND activo = true   
							    AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa
							);
		$dest =  json_encode($destinos);
		$destinos =  json_decode($dest);

		if($req->has('q')){
            $search = $req->q;
				foreach($destinos as $key=>$destino){
					$resultado = strpos(strtolower($destino->pasajero_data), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		return response()->json($destinos);
	}	

		public function getPrecioAsistencia(Request $request){
			$fechaPeriodo= explode('-', $request->input('dataPeriodo'));
			$check_in = explode('/', $fechaPeriodo[0]);
			$check_out = explode('/', $fechaPeriodo[1]);
			$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]);
			$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]);

			//$diff = abs(strtotime($checkIn) - strtotime($checkOut));
			$diff = abs((strtotime($checkIn) - strtotime($checkOut)));

			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24))+1;		
			$days =  $this->compararFechas(trim($fechaPeriodo[0]),trim($fechaPeriodo[1]));
			$getPrecio = DB::select('SELECT public.get_costo_asistencia('.$request->input('dataTipoAsistencia') .','.$days .','.$request->input('dataCantidad').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');
			$getPrecio = $getPrecio[0]->get_costo_asistencia;
			$asistencia = [];
			$asistencia['costo'] = $getPrecio;
			$asistencia['cant_personas'] = $request->input('dataCantidad');
			$asistencia['dias'] = $days;
			$asistencia['tipo_asistencia'] = $request->input('dataTipoAsistencia');
			return response()->json($asistencia);
		}

		public function getPrecioAsistenciaUA(Request $request){

		}	
		public function compararFechas($primera, $segunda)
		{
		 $valoresPrimera = explode ("/", $primera);   
		 $valoresSegunda = explode ("/", $segunda); 
		 $diaPrimera    = $valoresPrimera[0];  
		 $mesPrimera  = $valoresPrimera[1];  
		 $anyoPrimera   = $valoresPrimera[2]; 
		 $diaSegunda   = $valoresSegunda[0];  
		 $mesSegunda = $valoresSegunda[1];  
		 $anyoSegunda  = $valoresSegunda[2];
		 $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);  
		 $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     
		 if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
		   // "La fecha ".$primera." no es válida";
		   return 0;
		 }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
		   // "La fecha ".$segunda." no es válida";
		   return 0;
		 }else{
		   return  ($diasSegundaJuliano - $diasPrimeraJuliano)+1;
		 } 
	   }

		public function addVenta(Request $request){
			$cliente = DB::select("
			SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
			concat(p.documento_identidad ||' - ',p.nombre||' ',p.apellido) as full_data
			FROM personas p
			JOIN tipo_persona tp on tp.id = p.id_tipo_persona
			WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
			AND p.activo = true
			AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
			$productos = Producto::where('visible', true)
			->where('tipo_producto', 'V')
			->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
			->where('activo', '=', true)
			->get();
			$proveedor = Persona::where('id_tipo_persona', '=', '14')
							->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->where('activo', '=', true)
							->get();
			$formaPagos = FormaCobroCliente::where('activo', true)->get();
			$zonas = Zona::where('activo', true)->orderBy('id', 'asc')->get();
				
			$tipoFactura= TipoFactura::all();
			$comercioPersona= ComercioPersona::where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
			$tipo_facturacion = TipoFacturacion::all();
			$currency = Currency::where('activo', 'S')->get();
			$tipoIdentidad = TipoIdentidad::where('activo', true)->get();
			return view('pages.mc.ventas.add')->with(['clientes'=>$cliente,'formasPagos'=>$formaPagos, 'zonas'=>$zonas,'productos'=>$productos, 'tipoFacturas'=>$tipoFactura, 'tipoFacturacions'=>$tipo_facturacion, 'currencys'=>$currency, 'tipoDocumentos'=>$tipoIdentidad, 'proveedores'=>$proveedor, 'comercioPersonas'=>$comercioPersona]);
		}	

		public function getIvaProcudcto(Request $request){
			$productos = Producto::with('tipoImpuesto')->where('id', $request->input('idPorducto'))
			->first();
			if(empty($productos->id_proveedor)){
				$idProveedor = 0;
				$nombreProveedor = "";
			}else{
				$proveedors = Persona::where('id_tipo_persona', '=', '14')
									->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
									->where('id', $productos->id_proveedor)
									->first(); 
				$idProveedor = $productos->id_proveedor;
				if(empty($proveedors->nombre)){
					$nombreProveedor = "";
				}else{	
					$nombreProveedor = $proveedors->nombre." ".$proveedors->apellido;
				}	
			}
			$tipoImpuesto = new \StdClass;
			if(isset($productos->tipoImpuesto->valor)){
				$tipoImpuesto->id = $productos->tipoImpuesto->id;
				$tipoImpuesto->impuesto = $productos->tipoImpuesto->valor;
				$tipoImpuesto->denominacion = $productos->tipoImpuesto->valor;
			}else{
				$tipoImpuesto->id = 0;
				$tipoImpuesto->impuesto = 0;
				$tipoImpuesto->denominacion = 0;
			}
			$valorReal = $productos->precio_costo;
			$valorVenta = $productos->precio_venta;
			$input = 0;
			if($request->input('itemBase') == ""){
				$tipoImpuesto->item = $productos->frase_predeterminada;
			}else{
				$tipoImpuesto->item = $request->input('itemBase');
			}
			$tipoImpuesto->valorReal = $valorReal;
			$tipoImpuesto->valorVenta = $valorVenta;
			$tipoImpuesto->input = $input;
			$tipoImpuesto->proveedor_id = $idProveedor;
			$tipoImpuesto->proveedor_nombre = $nombreProveedor;
			$tipoImpuesto->producto = $productos->denominacion;
			$tipoImpuesto->producto_id = $productos->id;

			$tipoImpuesto->linea =$request->input('idLinea');

			return response()->json($tipoImpuesto);
		}
		public function doAddVenta(Request $request){
			 try{
				$venta = new VentasRapidasCabecera;
				if($request->input('fecha_venta')!== null){
				    $fecha = explode('/', $request->input('fecha_venta'));
					$fechaVenta = trim($fecha[2]).'-'.trim($fecha[1]).'-'.trim($fecha[0]);
				}else{
					$fechaVenta = date('Y-m-d');
				}
				$venta->fecha_venta = date('Y-m-d H:m:s');
				$venta->id_cliente = $request->input('cliente_id');
				$venta->id_vendedor = $this->getIdUsuario();
				$venta->id_empresa = $this->getIdEmpresa();
				$venta->id_estado = 71;
				$venta->fecha_vencimiento = $fechaVenta;
				$venta->id_tipo_facturacion = 1;
				$venta->zona_id = $request->input('zona');
				$venta->id_forma_pago = $request->input('id_forma_pago');
				$venta->id_comercio_empresa = $request->input('origen_comercio');
				$venta->nro_venta = str_replace('.','', str_replace(',','.',$request->input('nro_venta')));
				$venta->id_tipo_factura = $request->input('tipo_factura');
				$venta->activo = true;
				$venta->id_moneda = $request->input('moneda');
				$venta->comprobante = $request->input('comprobante');
				$venta->delivery_direccion = $request->input('delivery_direccion');
				$venta->delivery_referencia = $request->input('delivery_referencia');
				$venta->concepto_generico = $request->input('concepto');
				$venta->save();
				$idVenta =$venta->id;
				if(!empty($request->input('datos'))){
					foreach($request->input('datos') as $key=>$detalleVenta){
						$ventaDetalle = new VentasRapidasDetalle;
						$ventaDetalle->id_venta_cabecera = $idVenta;
						$ventaDetalle->id_producto = $detalleVenta['producto_id'];
						$ventaDetalle->precio_costo = str_replace(',','.', str_replace('.','',$detalleVenta['costo']));
						$ventaDetalle->precio_venta = str_replace(',','.', str_replace('.','',$detalleVenta['venta']));
						$ventaDetalle->id_proveedor = $detalleVenta['proveedor_id'];
						$ventaDetalle->item = $detalleVenta['item'];
						$ventaDetalle->monto_neto =  str_replace(',','.', str_replace('.','',$detalleVenta['venta']));
						$ventaDetalle->monto_bruto =  str_replace(',','.', str_replace('.','',$detalleVenta['venta']));
						$ventaDetalle->nro_factura_proveedor = $detalleVenta['nro_factura'];
						$ventaDetalle->cantidad = str_replace(',','.', str_replace('.','',$detalleVenta['cantidad']));
						$venta = str_replace(',','.', str_replace('.','',$detalleVenta['venta']));
						switch ($detalleVenta['tipo_impuesto']) {
							    case 0:
							        $ventaDetalle->exenta = (float)$venta;
							        break;
							    case 1:
							        $ventaDetalle->exenta = (float)$venta;
							        break;
							    case 2:
							    	$ventaDetalle->iva5 = (float)$venta * 0.21;
							        $ventaDetalle->gravada = (float)$venta;
							        break;
							    case 3:
							        $ventaDetalle->iva = (float)$venta / 11;
							    	$ventaDetalle->gravada = (float)$venta;
							   		break;
						}	    
						$ventaDetalle->activo = true;
						$ventaDetalle->save();
					}
				}	
			flash('La venta fue guardada exitosamente')->success();
            return redirect()->route('verVenta', $idVenta);
		} 
		catch(\Exception $e)
		{
	       flash('No se ha guardado la venta. Intentelo Nuevamente')->error();
	       return Redirect::back();
		}


		}	


		public function editVenta(Request $request, $id){

			$ventas = VentasRapidasCabecera::with('ventasDetalle','factura','estado', 'vendedor', 'vendedor_agencia', 'pedido','cliente')
											->where('id', $id)
											->where('id_empresa',$this->getIdEmpresa())
											->where('activo', true)->get();
			/*echo '<pre>';
			print_r($ventas);*/

			if(!isset($ventas[0]->id)){
				flash('La venta solicitada no existe. Intentelo nuevamente !!')->error();
				return redirect()->route('home');
			}
			if(!empty($ventas[0]->pedido)){
				$estados = EstadoPedidoWoo::where('name', $ventas[0]->pedido->status)
				->get();
				$status = $estados[0]->denominacion;	
			}else{
				$status = '';
			}

			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 5 && Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 6 && Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 2 && $ventas[0]->id_vendedor != Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario){
				flash('No tenes permisos para editar esta venta')->error();
				return Redirect::back();
			}

			$formaPagos = FormaCobroCliente::where('activo', true)->where('id_empresa','=',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
			$zonas = Zona::where('activo', true)->orderBy('id', 'asc')->get()	;		
			foreach($ventas[0]->ventasDetalle as $key=>$ventasDetalles){
				$productos = Producto::where('visible', true)
									->where('id', $ventasDetalles->id_producto)
									//->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
									->where('activo', true)
									->first(['denominacion']);
				if(isset($productos->denominacion)){
					$ventas[0]->ventasDetalle[$key]->producto_detalle = $productos->denominacion;
				}else{
					$ventas[0]->ventasDetalle[$key]->producto_detalle = "";
				}	
			}
			/*echo '<pre>';
			print_r($ventas); */
			$cliente = DB::select("
			SELECT p.id,p.nombre, p.apellido,p.dv, p.documento_identidad, tp.denominacion, p.activo,
			concat(p.documento_identidad ||' - ',p.nombre||' ',p.apellido) as full_data
			FROM personas p
			JOIN tipo_persona tp on tp.id = p.id_tipo_persona
			WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
			AND p.activo = true
			AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
		/*	$productosEdit = Producto::where('visible', true)
			->where('tipo_producto', 'V')
			->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
			->get();*/
			$proveedor = Persona::where('id_tipo_persona', '=', '14')
							->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->where('activo', '=', true)
							->get();
			$btn = $this->btnPermisos(array('id_venta'=>$id),'editVenta');
			/*echo '<pre>';
			print_r($btn);*/
			$tipoFactura= TipoFactura::all();
			$comercioPersona= ComercioPersona::where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();
			$tipo_facturacion = TipoFacturacion::all();
			$currency = Currency::where('activo', 'S')->get();
			$tipoIdentidad = TipoIdentidad::where('activo', true)->get();
			return view('pages.mc.ventas.edit')->with(['clientes'=>$cliente,'formasPagos'=>$formaPagos, 'zonas'=>$zonas, 'tipoFacturas'=>$tipoFactura, 'tipoFacturacions'=>$tipo_facturacion, 'currencys'=>$currency, 'tipoDocumentos'=>$tipoIdentidad, 'ventas'=>$ventas, 'proveedores'=>$proveedor, 'comercioPersonas'=>$comercioPersona, 'btn'=>$btn]);
		}	

		public function doEditVenta(Request $request){
			//die;
			try{
				//DB::beginTransaction();
				if($request->input('fecha_venta') !== null){
					$fecha = explode('/', $request->input('fecha_venta'));
					$fechaVenta = trim($fecha[2]).'-'.trim($fecha[1]).'-'.trim($fecha[0]);		
				}else{
					$ventas = VentasRapidasCabecera::where('id', $request->input('id'))->first();
					$fechaVenta = $ventas->fecha_vencimiento;
				}

				if($request->input('id_forma_pago') != ""){
					$forma_pago = $request->input('id_forma_pago');
				}else{
					$forma_pago = null;	
				}

				DB::table('ventas_rapidas_cabecera')  
								    ->where('id',$request->input('id'))
								    ->update([
										   		'id_cliente'=>$request->input('cliente_id'),
										   		'fecha_vencimiento'=>$fechaVenta,
										   		'id_tipo_factura'=>$request->input('tipo_factura'),
												'id_moneda'=>$request->input('moneda'),
												'zona_id'=>$request->input('zona'),
										   		'id_forma_pago'=>$forma_pago,
										   		'comprobante'=>$request->input('comprobante'),
										   		'id_comercio_empresa'=>$request->input('origen_comercio'),
										   		'delivery_direccion'=>$request->input('delivery_direccion'),
										   		'delivery_referencia'=>$request->input('delivery_referencia'),
												'concepto_generico'=>$request->input('concepto')
								   			 ]);
				DB::table('ventas_rapidas_detalle')->where('id_venta_cabecera', $request->input('id'))->delete();				    
				/*echo '<pre>';
				print_r($request->all())*/
				if(!empty($request->input('datos'))){
					foreach($request->input('datos') as $key=>$detalleVenta){
						/*echo '<pre>';
						print_r($detalleVenta);*/
						
						$ventaDetalle = new VentasRapidasDetalle;
						if($detalleVenta['proveedor_id'] == ""){
							$proveedor = null;
						}else{
							$proveedor = $detalleVenta['proveedor_id'];
						}
						$ventaDetalle->id_venta_cabecera = $request->input('id');
						$ventaDetalle->id_producto = $detalleVenta['producto_id'];
						$ventaDetalle->precio_costo = str_replace(',','.', str_replace('.','',$detalleVenta['costo']));
						$ventaDetalle->precio_venta = str_replace(',','.', str_replace('.','',$detalleVenta['venta']));
						$ventaDetalle->id_proveedor = $proveedor;
						$ventaDetalle->item = $detalleVenta['item'];
						$ventaDetalle->nro_factura_proveedor = $detalleVenta['nro_factura'];
						if(isset($detalleVenta['origen'])){
							$ventaDetalle->origen = $detalleVenta['origen'];
						}	
						$ventaDetalle->monto_neto = str_replace(',','.', str_replace('.','',$detalleVenta['venta']));
						$ventaDetalle->monto_bruto = str_replace(',','.', str_replace('.','',$detalleVenta['venta']));
						$ventaDetalle->cantidad = $detalleVenta['cantidad'];
						$ventax = str_replace(',','.', str_replace('.','',$detalleVenta['venta']));
						switch ($detalleVenta['tipo_impuesto']) {
							    case 0:
							        $ventaDetalle->exenta = (float)$ventax;
							        break;
							    case 1:
							        $ventaDetalle->exenta = (float)$ventax;
							        break;
							    case 2:
							    	$ventaDetalle->iva5 = (float)$ventax * 0.21;
							        $ventaDetalle->gravada = (float)$venta;
							        break;
							    case 3:
							        $ventaDetalle->iva = (float)$ventax / 11;
							    	$ventaDetalle->gravada = (float)$ventax;
							   		break;
						}	    
						
						$ventaDetalle->activo = true;
						$ventaDetalle->save();
					}
				}
				flash('La venta fue guardada exitosamente')->success();
				//DB::commit(); //CONFIRMAR CAMBIOS
            	return redirect()->route('verVenta', $request->input('id'));
			} 
			catch(\Exception $e)
			{ 
				flash('No se ha actualizado la venta. Intentelo Nuevamente')->error();
				DB::rollBack(); //DESHACER CAMBIOS
				return Redirect::back();

			}

		}	

		public function verVenta(Request $request, $id){
			$ventas = VentasRapidasCabecera::with('zona','formaPago','estado','ventasDetalle','factura', 'vendedor', 'vendedor_agencia', 'pedido', 'comercio','tipoVenta')
			->where('id', $id)
			->where('id_empresa',$this->getIdEmpresa())
			->where('activo', true)->get();
			if(!isset($ventas[0]->id)){
				flash('La venta solicitada no existe. Intentelo nuevamente !!')->error();
				return redirect()->route('home');
			}
			if(isset($ventas[0]->pedido->status)){
				$estados = EstadoPedidoWoo::where('name', $ventas[0]->pedido->status)
				->get();
				$status = $estados[0]->denominacion;	
			}else{
				$status = '';
			}
			/*if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 6 && Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 2 && $ventas[0]->id_vendedor != Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario){
				flash('No tenes permisos para editar esta venta')->error();
				return Redirect::back();
			}*/

			$idFactura = 0;
			$idPermisio = 0;
			$factura = Factura::where('id_venta_rapida', $id)->orderBy('id', 'DESC')->get(['id']);

			if(isset($factura[0]->id)){
				$idFactura = $factura[0]->id;
			}
			$btn = $this->btnPermiso(array('id_factura' =>$idFactura),'impFactPedido');
			$btns = $this->btnPermisosVenta(array('id_factura'=>$idFactura),'verVenta');
			if(!empty($btns)){
				$idPermisio = 1;
			}
			$cliente = DB::select("
			SELECT p.id,p.nombre,p.dv,p.apellido, p.documento_identidad, tp.denominacion, p.activo,
			concat(p.documento_identidad ||' - ',p.nombre||' ',p.apellido) as full_data
			FROM personas p
			JOIN tipo_persona tp on tp.id = p.id_tipo_persona
			WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
			AND p.activo = true
			AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
			$productos = Producto::where('visible', true)
			->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
			->get();
			$zonas = Zona::where('activo', true)->get();
			$proveedor = Persona::where('id_tipo_persona', '=', '14')
							->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->get();
			$tipoFactura= TipoFactura::all();
			$tipo_facturacion = TipoFacturacion::all();
			$currency = Currency::where('activo', 'S')->get();
			$tipoIdentidad = TipoIdentidad::where('activo', true)->get();
			return view('pages.mc.ventas.ver')->with(['clientes'=>$cliente, 'status'=>$status, 'productos'=>$productos, 'tipoFacturas'=>$tipoFactura, 'zonas'=>$zonas, 'tipoFacturacions'=>$tipo_facturacion, 'currencys'=>$currency, 'tipoDocumentos'=>$tipoIdentidad, 'ventas'=>$ventas, 'proveedores'=>$proveedor, 'btn'=>$btn, 'permiso'=>$btns]);			
		}	


		public function getAsignarVenta(Request $request){
			$mensaje = new \StdClass;
			try{
				DB::table('ventas_rapidas_cabecera')  
								    ->where('id',$request->input('documento'))
								    ->update([
										   		'id_vendedor'=>$this->getIdUsuario()
								   			 ]);
				$status = 'OK';				    
			} 
			catch(\Exception $e)
			{
				$status = 'ERROR';
			}
			return response()->json(['status'=>$status]);				    
		}

		
		public function getReactivarVenta(Request $request){
			$mensaje = new \StdClass;
			try{
				DB::table('ventas_rapidas_cabecera')  
								    ->where('id',$request->input('documento'))
								    ->update([
										   		'id_estado'=>71
								   			 ]);
				$status = 'OK';				    
			} 
			catch(\Exception $e)
			{
				$status = 'ERROR';
			}
			return response()->json(['status'=>$status]);				    
		}

		public function getAsignarZona(Request $request){
			$mensaje = new \StdClass;
			try{
				DB::table('ventas_rapidas_cabecera')  
								    ->where('id',$request->input('documento'))
								    ->update([
										   		'zona_id'=>$request->input('idZona')
								   			 ]);
				$status = 'OK';				    
			} 
			catch(\Exception $e)
			{
				$status = 'ERROR';
			}
			return response()->json(['status'=>$status]);				    
		}

		public function getImprimirNota(Request $request){
			$mensaje = new \StdClass;
			$contadorDetalles = DB::select("SELECT verificar_ventas_rapidas(".$request->input('idVenta').",".$this->getIdUsuario().")");
			if($contadorDetalles[0]->verificar_ventas_rapidas == 'OK'){
					try{
						DB::table('ventas_rapidas_cabecera')  
											->where('id',$request->input('idVenta'))
											->update([
														'id_estado'=>72
													]);

						$status = 'OK';	
						$mensajes = 'Se genero la Nota de Venta exitosamente';			    
					} 
					catch(\Exception $e)
					{
						$status = 'ERROR';
						$mensajes = "No pudo generarse la Nota de Venta, intentelo nuevamente";			    

					}
				}else{
					$status = 'ERROR';
					$mensajes = $contadorDetalles[0]->verificar_ventas_rapidas;
				}		
				$mensaje = [];
				$mensaje['status'] = $status;
				$mensaje['mensaje'] = $mensajes;
	
				return response()->json($mensaje);

		}
		public function getImprimirFactura(Request $request){
			//try{
				$contadorDetalles = DB::select("SELECT verificar_ventas_rapidas(".$request->input('idVenta').",".$this->getIdUsuario().")");
				if($contadorDetalles[0]->verificar_ventas_rapidas == 'OK'){
						$idFactura = DB::select("SELECT public.facturar_ventas_rapidas(".$request->input('idVenta').",".$this->getIdUsuario().")");
						
						if($idFactura[0]->facturar_ventas_rapidas == 'OK'){
							$facturas = VentasRapidasCabecera::where('id', $request->input('idVenta'))->where('activo', true)->get();
							$status = 'OK';
							$mensajes = $idFactura[0]->facturar_ventas_rapidas;
							$factura = $facturas[0]->id_factura;
							$numeroFactura = $facturas[0]->factura->nro_factura;
						}else{
							$status = 'ERROR';
							$mensajes = $idFactura[0]->facturar_ventas_rapidas;
							$factura = 0;
							$numeroFactura = "";
						}
					}else{
						$status = 'ERROR';
						$mensajes = $contadorDetalles[0]->verificar_ventas_rapidas;
						$factura = 0;
						$numeroFactura = "";
					}		
		/*	 } 
			catch(\Exception $e)
			{
					$status = 'ERROR';
					$mensajes = 'Error en la conexion intentelo nuevamente';
					$factura = 0;
					$numeroFactura = "";
			} */

			$mensaje = [];
			$mensaje['status'] = $status;
			$mensaje['factura'] = $factura;
			$mensaje['numeroFactura'] = $numeroFactura;
			$mensaje['mensaje'] = $mensajes;

			return response()->json($mensaje);
		}	
	public function getEmergencia(Request $req)
	{
		$mensaje = new \StdClass;
		$contador = 0; 	
				if($req->input('dataTipoEmergenciaId') == 2){
					$proformaEmergencia = 0;
				}else{
					$proformaBase = Proforma::where('id', $req->input('dataProformaEmergencia'))->get(['id']);
					$contador = count($proformaBase);
					$proformaEmergencia =$req->input('dataProformaEmergencia');
				}
				if($contador != 0 ){

					if($req->input('dataProformaEmergencia') > $req->input('dataProforma')){
						try{	
							$proformaEmergencia = DB::table('proformas')
													->where('id',$req->input('dataProforma'))
													->update([
																'emergencia'=> $req->input('dataTipoEmergencia'),
																'proforma_padre'=> $proformaEmergencia
															]);
							$comentario = new HistoricoComentariosProforma;
							$comentario->fecha_hora = date('Y-m-d H:m:i');
							$comentario->id_proforma = $req->input('dataProforma');
							$comentario->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
							$comentario->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
							$comentario->comentario = $req->input('dataTipoEmergencia')."Nro: ".$req->input('dataProformaEmergencia');
							$comentario->save();

							$mensaje->status = "OK";
							$mensaje->mensaje = 'Los datos fueron actualizados correctamente.';
							$mensaje->comentario = $comentario;

						}catch(\Exception $e)
							{
								$mensaje->status = "ERROR";
								$mensaje->mensaje = 'Los datos no fueron actualizados. Intentelo Nuevamente';
							}		
				}else{
					$mensaje->status = "ERROR";
					$mensaje->mensaje = 'El numero de la Proforma ingresada no puede ser menor a la actual.';

				}
			}else{
				if($req->input('dataTipoEmergenciaId') == '2'){
					try{	
							$proformaEmergencia = DB::table('proformas')
													->where('id',$req->input('dataProforma'))
													->update([
																'emergencia'=> $req->input('dataTipoEmergencia'),
																'proforma_padre'=> $proformaEmergencia
															]);
							$comentario = new HistoricoComentariosProforma;
							$comentario->fecha_hora = date('Y-m-d H:m:i');
							$comentario->id_proforma = $req->input('dataProforma');
							$comentario->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
							$comentario->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
							$comentario->comentario = $req->input('dataTipoEmergencia');
							$comentario->save();

							$mensaje->status = "OK";
							$mensaje->mensaje = 'Los datos fueron actualizados correctamente.';
							$mensaje->comentario = $comentario;
						}catch(\Exception $e)
							{
								$mensaje->status = "ERROR";
								$mensaje->mensaje = 'Los datos no fueron actualizados. Intentelo Nuevamente';
							}	
				}else{
					$mensaje->status = "ERROR";
					$mensaje->mensaje = 'La Proforma ingresada no existe. Verifiquelo.';
				}
			}	

	return response()->json($mensaje);

   }

    public function btnPermiso($parametros, $vista){

  	$btn = DB::select("SELECT pe.* 
                        FROM persona_permiso_especiales p, permisos_especiales pe
                        WHERE p.id_permiso = pe.id 
                        AND p.id_persona = ".$this->getIdUsuario() ." 
                        AND pe.url = '".$vista."' ");
  	//print_r($parametros['id_factura']);
	$htmlBtn = '';
  	if(!empty($btn)){
		 foreach ($btn as $key => $value){
		    $ruta = $value->accion;
		    $htmlBtn = '';
		    $htmlBtn = "<a href='../imp_fact_pedido/".$parametros['id_factura']."' class='".$value->clase."' data-btn='".$parametros['id_factura']."' title='".$value->titulo."'>".$value->titulo."</a>";
		    $boton = $htmlBtn;
		 }
	   		return $boton;
    } else {
    	$htmlBtn = '<a href="../imp_fact_pedido/'.$parametros['id_factura'].'" class="btn btn-danger" role="button">Imprimir Factura</a>';
    	$boton = $htmlBtn;
    	return $boton;
    }
}//function


    public function datosPermiso($vista, $permiso){

  	$btn = DB::select("SELECT pe.* 
                        FROM persona_permiso_especiales p, permisos_especiales pe
                        WHERE p.id_permiso = pe.id 
                        AND p.id_persona = ".$this->getIdUsuario() ." 
                        AND pe.url = '".$vista."' 
                        AND pe.id = ".$permiso);
  	//print_r($parametros['id_factura']);
  	if(!empty($btn)){
		$respuesta = 1;  		
   		return $respuesta;
    } else {
    	$respuesta = 0;
    	return $respuesta;
    }
}//function


    public function getCliente(Request $req)
	{
		$mensaje = new \StdClass;
		$cliente = Persona::where('id', $req->input('documento'))->get();
		if(!empty($cliente[0])){
			$mensaje->status = "OK";
			$mensaje->nombre = $cliente[0]->documento_identidad."- ".$cliente[0]->nombre." ".$cliente[0]->apellido; 
			$mensaje->email = $cliente[0]->email;
			$mensaje->telefono = $cliente[0]->telefono;
			$mensaje->celular = $cliente[0]->celular;
			$mensaje->direccion = $cliente[0]->direccion;
		}else{
			$mensaje->status = "ERROR";
		}
		return response()->json($mensaje);
	}	

    public function getEmailCliente(Request $req)
	{
		$mensaje = new \StdClass;
		$cliente = Persona::where('email', $req->input('email'))->get();

		if(empty($cliente[0])){
			$mensaje->status = "OK";
		}else{
			$mensaje->status = "ERROR";
		}
		return response()->json($mensaje);
	}	
	
    public function getDocumentoCliente(Request $req)
	{
		$mensaje = new \StdClass;
		$cliente = Persona::where('documento_identidad', $req->input('documento'))->get();

		if(empty($cliente[0])){
			$mensaje->status = "OK";
		}else{
			$mensaje->status = "ERROR";
		}
		return response()->json($mensaje);
	}	
 	
 	public function getLatLong(Request $request)
	{
		$mensaje = new \StdClass;
		$latlon = VentasRapidasCabecera::where('id', $request->input('documento'))->get();
		if(!empty($latlon[0])){
			$mensaje->status = "OK";
			$mensaje->latitud = $latlon[0]->delivery_lat;
			$mensaje->longitud = $latlon[0]->delivery_lng;
		}else{
			$mensaje->status = "ERROR";
			$mensaje->latitud = 0;
			$mensaje->longitud = 0;
		}
		return response()->json($mensaje);
	}	

 	public function guardarLatLong(Request $request){
 		$mensaje = new \StdClass;
		try{	
			$proformaEmergencia = DB::table('ventas_rapidas_cabecera')
									->where('id',$request->input('idVenta'))
									->update([
											'delivery_lat'=> $request->input('latitud'),
											'delivery_lng'=> $request->input('longitud')
											]);
			$mensaje->status = "OK";
			$mensaje->mensaje = 'Los datos fueron actualizados correctamente.';
		}catch(\Exception $e){
			$mensaje->status = "ERROR";
			$mensaje->mensaje = 'Los datos no fueron actualizados. Intentelo Nuevamente';
		}	
		return response()->json($mensaje);
	}
	public function getGuardarProveedor(Request $request){
		
		$mensaje = new \StdClass;
		try{	
			$productos = DB::table('productos')
									->where('id',$request->input('idProducto'))
									->update([
											'id_proveedor'=> $request->input('idProveedor'),
										]);
			$mensaje->status = "OK";
			$mensaje->mensaje = 'Los datos fueron actualizados correctamente.';
			$mensaje->id = $request->input('idProveedor');
		}catch(\Exception $e){
			$mensaje->status = "ERROR";
			$mensaje->mensaje = 'Los datos no fueron actualizados. Intentelo Nuevamente';
		}	
		return response()->json($mensaje);
	}	

	public function getProveedorVer(Request $request){
		$mensaje = new \StdClass;
		$producto = Producto::where('id', $req->input('productoData'))->get();

		if(empty($producto[0])){
			$mensaje->status = "OK";
			$mensaje->id = $producto[0]->id_proveedor;
		}else{
			$mensaje->status = "ERROR";
		}
		return response()->json($mensaje);
	}	

	public function doPasajeroCliente(Request $request){
		$mensaje = new \StdClass;
		$personas =  DB::select("select * from personas where id_empresa=".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa." AND documento_identidad LIKE '%".$request->input('dataDocumento')."%'");	
		if(!empty($personas[0])){
			$mensaje->status = "ERROR";
			$mensaje->id = $personas[0]->id;
		}else{
			$mensaje->status = "OK";
		}
		return response()->json($mensaje);
	}	

	public function getUsuario(Request $request){
		$usuarios = Persona::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->whereIn('id_tipo_persona',array(2, 4, 3, 5,6,7))
							->get(['id','nombre','nombre','apellido', 'email']);

		return response()->json($usuarios);					
	}						

	public function getProductos(Request $request){
		$productosEdit = Producto::where('visible', true)
								->where('tipo_producto', 'V')
								->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
								->get(['id','denominacion']);
		return response()->json($productosEdit);					
	}	
	
	public function getClienteVenta(Request $req){
		$clientes = DB::select("
							SELECT p.id,p.nombre, p.apellido, p.documento_identidad, tp.denominacion, p.activo,
							concat(p.documento_identidad ||' - ',p.nombre||' ',p.apellido) as full_data
							FROM personas p
							JOIN tipo_persona tp on tp.id = p.id_tipo_persona
							WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
							AND p.activo = true
							AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
		$client =  json_encode($clientes);
		$clientes =  json_decode($client);

		if($req->has('q')){
            $search = $req->q;
				foreach($clientes as $key=>$cliente){
					$resultado = strpos(strtolower($cliente->full_data), strtolower($search));
					if($resultado === FALSE){
						unset($clientes[$key]);
					}
				}	
		}
		return response()->json($clientes);
	}

	public function getProductoVenta(Request $req){
		$productos = Producto::where('visible', true)
						->where('tipo_producto', 'V')
						->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
						->where('activo', '=', true)
						->get(['id','denominacion']);
		if($req->has('q')){
			$search = $req->q;
			foreach($productos as $key=>$producto){
				$resultado = strpos(strtolower($producto->denominacion), strtolower($search));
				if($resultado === FALSE){
					unset($productos[$key]);
				}
			}	
		}
		return response()->json($productos);
	}

	public function getProveedorVenta(Request $req){
		$proveedors = Persona::where('id_tipo_persona', '=', '14')
							->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
							->where('activo', '=', true)
							->get(); 
		if($req->has('q')){
			$search = $req->q;
			foreach($proveedors as $key=>$proveedor){
				$resultado = strpos(strtolower($proveedor->nombre."".$proveedor->apellido), strtolower($search));
				if($resultado === FALSE){
					unset($proveedors[$key]);
				}
			}	
		}
		return response()->json($proveedors);
	}

    private function btnPermisosVenta($parametros, $vista){
		$btn = DB::select("SELECT pe.* 
							 FROM persona_permiso_especiales p, permisos_especiales pe
							 WHERE p.id_permiso = pe.id 
							 AND p.id_persona = ".$this->getIdUsuario()." 
							 AND pe.url = '".$vista."'");
   
	   $htmlBtn = '';
	   $boton =  0;
	   $idParametro = '';
   
	 // dd( $btn );
   
	 if(!empty($btn)){
	   $boton = 1;
	 }
	  return $boton;
	 }

	 public function reservasNemo(Request $req){
		$reservasNemos = DB::select("SELECT * FROM public.vw_reservas_nemo where tipo_reserva_nemo_id =".$req->input('dataTipo')." and estado_id in (2,3) and id_proforma is null and empresa_id =".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
		return response()->json($reservasNemos);
	}

	public function reservasNemoDatos(Request $req){

			$respuesta = new \StdClass;
			$reservas = NemoReserva::with('prestador', 'proveedor')->where('id', $req->input('dataId'))
		       						->first(); 
			$respuesta->codigo = $reservas->booking_reference;
			$respuesta->checkIn = date('d/m/Y', strtotime($reservas->booking_checkin));			   
			$respuesta->checkOut = date('d/m/Y', strtotime($reservas->booking_checkout));
			$respuesta->codigo = $reservas->booking_reference;
			$respuesta->proveedor = $reservas->supplier_id;
			$respuesta->prestador = $reservas->lender_id;
			if($reservas->supplier_id == 0 || $reservas->supplier_id == ""){
				$respuesta->proveedor_name = '';
			}else{
				$respuesta->proveedor_name = $reservas->proveedor->nombre;
			}
			$respuesta->prestador_name = $reservas->prestador->nombre;

			/*if($reservas->price_neto_marketer != null && $reservas->price_neto != null ){ 
				$fee = (float)$reservas->price_neto_marketer - (float)$reservas->price_neto;
			}else{ */
				$fee = (float)3;
			//}	
			$respuesta->fee = $fee;
			$respuesta->costo_base = (float)$reservas->price_neto;
			$respuesta->venta_base = (float)$reservas->price_bruto;
			$venta  = (float)$reservas->price_bruto - $fee;
			$respuesta->venta = $venta;
			$costo  =  (float)$reservas->price_bruto - $fee;

		 	if($costo > (float)$reservas->price_neto){
				$respuesta->costo = (float)$reservas->price_neto;
			}else{ 
				$respuesta->costo = $costo;
			} 

			$respuesta->costo = (float)$reservas->price_neto;

			if($reservas->cancelacion_begin_d != ""){
				$respuesta->vencimiento = date('d/m/Y', strtotime($reservas->cancelacion_begin_d));
			}else{ 
				$respuesta->vencimiento = "";
			}
			if($reservas->booking_deadline != ""){
				$respuesta->pago_proveedor = date('d/m/Y', strtotime($reservas->booking_deadline));
			}else{ 
				$respuesta->pago_proveedor = "";
			}

			$respuesta->moneda = $reservas->moneda_id;
			$respuesta->code_supplier = $reservas->code_supplier;
			$respuesta->monto_gasto = $reservas->monto_gasto;
			return response()->json($respuesta); 
	}

	public function obtenerIncentivoVenta(Request $req){

		$incentivoBase = DB::select("SELECT public.get_incentivo(".$req->input('idProforma').")");
		if($incentivoBase[0]->get_incentivo == 0 || $incentivoBase[0]->get_incentivo == null){
			if($req->input('idPersona') != 0){
				$incentivoPersona = Persona::where('id',$req->input('idPersona'))->first();
				if(isset($incentivoPersona->comision_pactada)){
					$incentivo = 0;
				}else{
					$incentivo = $incentivoPersona->comision_pactada;
				}
			}else{
				$incentivo = 0;
			}
		}else{ 
			$incentivo= $incentivoBase[0]->get_incentivo;
		}
		return response()->json($incentivo);
	}

	public function cuentasFactura(Request $req)
	{
		$cliente = DB::select("SELECT * FROM personas 
							  WHERE id_tipo_persona IN (SELECT id FROM tipo_persona 
							  							WHERE puede_facturar = true) 
							  AND id_empresa = ".$this->getIdEmpresa()." order by nombre ASC");

		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();

		return view('pages.mc.cuentaCorriente.cuentaVendedor',compact('currency','cliente'));
	}

	public function getResumenCliente(Request $req)
	{
		$exacto =  DB::table('vw_cuenta_corriente_vendedor_resumen');
		$exacto =  $exacto->where('id_usuario', $this->getIdUsuario());
		$exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
		$exacto =  $exacto->get();
		return response()->json(['data'=>$exacto]);
	}

	public function getCuentaCorrienteCliente(Request $req)
	{
		$exacto =  DB::table('vw_cuenta_corriente_vendedor_resumen');
		if($req->input("idCliente") != ''){
			$exacto =  $exacto->where('id_cliente',$req->input("idCliente"));
		}
		if($req->input("idMoneda") != ''){
			$exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
		}
		$exacto =  $exacto->where('id_usuario', $this->getIdUsuario());
		$exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
		$exacto =  $exacto->get();
		$resultado = [];
        $contador = 0;

        foreach($exacto as $key=>$valor){
			$extractoDetalles =  DB::table('vw_cuenta_corriente_vendedor');
			if($req->input("fecha_emision") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
			}
			if($req->input("fecha_vencimiento") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
			}
			$extractoDetalles =  $extractoDetalles->where('id_cliente',$valor->id_cliente);
			$extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
			//$extractoDetalles =  $extractoDetalles->where('id_usuario', $this->getIdUsuario());
			$extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
			$extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
			$extractoDetalles =  $extractoDetalles->get();
			if(isset($extractoDetalles[0]->nro_documento)){
				$resultado[$key] = $valor;
				$detalles = [];
				foreach($extractoDetalles as $keys=>$detalle){
					$detalles[$keys]['nro_documento'] = $detalle->nro_documento;
					$detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
					$detalles[$keys]['moneda'] = $detalle->moneda;
					$detalles[$keys]['fecha_emision'] = date('d/m/Y',strtotime($detalle->fecha_emision));
					$detalles[$keys]['fecha_vencimiento'] =date('d/m/Y',strtotime($detalle->fecha_vencimiento));
					$detalles[$keys]['check_in'] = date('d/m/Y',strtotime($detalle->check_in));
					$detalles[$keys]['atraso'] = $detalle->atraso;
					$detalles[$keys]['corte'] = $detalle->corte;
					$detalles[$keys]['importe'] = $detalle->importe;
					$detalles[$keys]['total_comision'] = $detalle->total_comision;
					$detalles[$keys]['importe_neto'] = $detalle->importe_neto;
					$detalles[$keys]['saldo_neto'] = $detalle->saldo_neto;
					$detalles[$keys]['proforma'] = $detalle->proforma;
					$detalles[$keys]['vendedor'] = $detalle->vendedor;
					$detalles[$keys]['pasajero'] = $detalle->pasajero_online;
				}
				$resultado[$key]->detalles = $detalles;
			}
		}
		$listado = array();
        foreach($resultado as $key=>$st){
            $listado['data'][] = $st;	
        }

        if(empty($listado)){
                $listado['data'][] = $listado;
        }
        return response()->json($listado);
	}

	private function formatMoney($num,$f)  {
		if($f != 111){
		return number_format($num, 2, ",", ".");  
		} 
		return number_format($num,0,",",".");
	
	  }//function


	public function getRevisarCotizacion(Request $request)
	{
		$validarCotizacion = DB::select('SELECT public.comprobar_cotizaciones('.$request->input('dataMonedaCompra').','.$request->input('dataCurrency_venta').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');
		return response()->json($validarCotizacion[0]->comprobar_cotizaciones);
	}

	public function getInfoProforma(Request $req){
		$proforma = Proforma::where('id', $req->input("id_proforma"))
								->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first();
		
		$pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id FROM personas WHERE id IN (SELECT pasajero FROM pasajeros_proformas WHERE id_proforma=".$req->input("id_proforma").")");

		if(empty($pasajero)){					
			$pasajero = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n, id 
						FROM personas WHERE personas.id = ".$proforma->pasajero_id);
		}
		
		return response()->json($pasajero);
	}

	public function getClienteListado(Request $req){
		$destinos = DB::select("
							SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
							concat(p.nombre||' ',p.apellido,' ','- '|| tp.denominacion) as pasajero_data
							FROM personas p
							JOIN tipo_persona tp on tp.id = p.id_tipo_persona
							WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
							AND p.activo = true
							AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);



		$dest =  json_encode($destinos);
		$destinos =  json_decode($dest);

		if($req->has('q')){
			$search = $req->q;
				foreach($destinos as $key=>$destino){
					$resultado = strpos(strtolower($destino->pasajero_data), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		return response()->json($destinos);

	}


	public function getUsuarioListado(Request $req){
		$destinos = DB::select("
							SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
							concat(p.nombre||' ',p.apellido,' ','- '|| tp.denominacion) as pasajero_data
							FROM personas p
							JOIN tipo_persona tp on tp.id = p.id_tipo_persona
							WHERE p.id_tipo_persona IN (2,4,3,5,6,7)
							AND p.activo = true
							AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);



		$dest =  json_encode($destinos);
		$destinos =  json_decode($dest);

		if($req->has('q')){
			$search = $req->q;
				foreach($destinos as $key=>$destino){
					$resultado = strpos(strtolower($destino->pasajero_data), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		return response()->json($destinos);

	}

	public function getSolicitarFactura(Request $req)
	{
		$mensaje = new \StdClass;
	 	try{
			$proformaClientes = ProformaCliente::with('cliente')->where('id_persona',$req->input('cliente_pedido'))->get();
			if(isset($proformaClientes[0]->id)){
				$hay = 1;
			}else{
				$hay = 0;
			}
			$proforma_solicitud = new SolicitudFacturaParcial;
			$proforma_solicitud->fecha_hora_pedido = date('Y-m-d H:m:i');
			$proforma_solicitud->id_proforma = $req->input('id_proforma_pedido');
			$proforma_solicitud->id_persona = $req->input('cliente_pedido');
			$proforma_solicitud->id_moneda = $req->input('moneda_factura_parcial');
			/*$proforma_solicitud->id_producto_proceso = $req->input('producto_pedido');
			$proforma_solicitud->concepto_proceso = $req->input('concepto_parcial');*/
			$proforma_solicitud->id_pasajero = $req->input('pasajero_pedido');
			$proforma_solicitud->monto = str_replace(',','.', str_replace('.','',$req->input('monto_factura_parcial'))) ;
			$proforma_solicitud->id_usuario_pedido = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			$proforma_solicitud->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
			$proforma_solicitud->id_estado = 80;
			$proforma_solicitud->save();
			if($hay == 0){
				$proformaCliente = new ProformaCliente;
				$proformaCliente->id_proforma = $req->input('id_proforma_pedido');
				$proformaCliente->id_persona =$req->input('cliente_pedido'); 
				$proformaCliente->fecha_hora_alta = date('Y-m-d h:m:s');
				$proformaCliente->id_persona_alta = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
				$proformaCliente->save();
			}

			$actualizar_saldo = DB::select("SELECT actualizar_saldo_proforma_parcial(".$req->input('id_proforma_pedido').",".$req->input('moneda_factura_parcial').",".str_replace(',','.', str_replace('.','',$req->input('monto_factura_parcial'))).",2)");
			$id_solicitud = $proforma_solicitud->id;
			$getNotificacion= DB::select("SELECT public.insert_notificacion(".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.",null, null, null,23,".$id_solicitud.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");


			
			
			$mensaje->status = "OK";
			$mensaje->mensaje = 'Se ha generado la solicitud de Facturacion Parcial';
	 	}catch(\Exception $e){
			$mensaje->status = "ERROR";
			$mensaje->mensaje = $e->getMessage();
		}	
		return response()->json($mensaje); 
	}
	public function getClienteAuxiliar(Request $req)
	{

		$destinos = DB::select("
							SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo,
							concat(p.nombre||' ',p.apellido,' ','- '|| tp.denominacion) as pasajero_data
							FROM personas p
							JOIN tipo_persona tp on tp.id = p.id_tipo_persona
							WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
							AND p.activo = true
							AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

		$dest =  json_encode($destinos);
		$destinos =  json_decode($dest);

		if($req->has('q')){
			$search = $req->q;
				foreach($destinos as $key=>$destino){
					$resultado = strpos(strtolower($destino->pasajero_data), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		return response()->json($destinos);
	}


	public function getGuardarClienteAuxiliar(Request $request)
	{
		$mensaje = new \StdClass;
		try{
			$proformaCliente = new ProformaCliente;
			$proformaCliente->id_proforma = $request->input('dataProforma');
			$proformaCliente->id_persona = $request->input('dataCliente'); 
			$proformaCliente->fecha_hora_alta = date('Y-m-d h:m:s');
			$proformaCliente->id_persona_alta = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
			$proformaCliente->save();
			$proforma = $proformaCliente->id;
			$mensaje->status = "OK";
			$mensaje->mensaje = 'Los datos fueron actualizados correctamente.';
			$mensaje->id = $proforma;
	    }catch(\Exception $e){
			$mensaje->status = "ERROR";
			$mensaje->mensaje = 'Los datos no fueron actualizados. Intentelo Nuevamente';
		}	 
		return response()->json($mensaje);
	}



	public function getGuardarDetalleFactura(Request $request){
		$mensaje = new \StdClass;
	    try{	
			DB::table('proformas_detalle_factura')->where('id_proforma', $request->input('proforma'))->delete();
			if(!empty($request->input('detalle'))){				    
				foreach($request->input('detalle') as $key=>$detalles){
					$detalles_proforma = new ProformaFacturaDetalle;
					$detalles_proforma->id_proforma = $detalles['id_proforma'];
					$detalles_proforma->cantidad = $detalles['cantidad']; 
					$detalles_proforma->descripcion = $detalles['descripcion'];
					$detalles_proforma->precio_venta = str_replace(',','.', str_replace('.','',$detalles['precio_unitario']));
					$detalles_proforma->exento = str_replace(',','.', str_replace('.','',$detalles['exenta']));
					$detalles_proforma->gravadas_5 = str_replace(',','.', str_replace('.','',$detalles['iva5']));
					$detalles_proforma->gravadas_10 =str_replace(',','.', str_replace('.','',$detalles['iva10']));
					$detalles_proforma->total = str_replace(',','.', str_replace('.','',$detalles['total']));
					$detalles_proforma->save();
				}
			}	
			$mensaje->status = "OK";
			$mensaje->mensaje = 'Los datos de detalles factura fueron cargados.';
	  	}catch(\Exception $e){
			$mensaje->status = "ERROR";
			$mensaje->mensaje = 'Los datos de detalles factura no fueron cargados. Intentelo Nuevamente';
		}  
		return response()->json($mensaje);
	}

	public function getDetalleFactura(Request $request){
		$detalles = ProformaFacturaDetalle::where('id_proforma', $request->input("dataProforma"))
											->get();
		return response()->json($detalles);
	}




	public function getDetalleProforma(Request $req){
		$detalles = ProformasDetalle::with('producto','proveedor','prestador', 'currencyVenta')
									->where('id_proforma', $req->input("dataProforma"))
									->where('id_detalle_proforma_padre', $req->input("dataDetallePadre"))
									->where('activo', true)
									->first();
		return response()->json($detalles);
	}

	public function calcularPrecioCosto(Request $request){
	//	echo '<pre>';
	//	print_r("SELECT public.calcular_precio_venta(".$request->input('dataTotalCosto').",".$request->input('dataBaseComisionable').",".$request->input('dataProducto').",".$request->input('dataMonedaCompra').",".$request->input('dataMonedaVenta').",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
	 	$precioVenta = DB::select("SELECT public.calcular_precio_venta(".$request->input('dataTotalCosto').",".$request->input('dataBaseComisionable').",".$request->input('dataProducto').",".$request->input('dataMonedaCompra').",".$request->input('dataMonedaVenta').",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
		return response()->json($precioVenta[0]->calcular_precio_venta);
	}


	public function getDetalleSolicitud(Request $request){
		$solicitudes = SolicitudFacturaParcial::with('pasajero','estado','currency', 'cliente', 'usuario')->where('id_proforma', $request->input("dataProforma"))->whereIn('id_estado', [80,81])->get();
		return response()->json($solicitudes);
	}

	public function getEliminrSolicitud(Request $request){
		$mensaje = new \StdClass;
		try{	
			$solicitudes = SolicitudFacturaParcial::where('id', $request->input("idPedido"))->first();

			$productos = DB::table('proformas_facturacion_parcial')
									->where('id',$request->input('idPedido'))
									->update([
											'id_estado'=> 82,
										]);

			$actualizar_saldo = DB::select("SELECT actualizar_saldo_proforma_parcial(".$solicitudes->id_proforma.",".$solicitudes->id_moneda.",".$solicitudes->monto.",1)");

			$mensaje->status = "OK";
			$mensaje->mensaje = 'Los datos fueron actualizados correctamente.';
			$mensaje->id = $request->input('idPedido');
		}catch(\Exception $e){
			$mensaje->status = "ERROR";
			$mensaje->mensaje = 'Los datos no fueron actualizados. Intentelo Nuevamente';
		}	
		return response()->json($mensaje);

	}	

	public function getMontoCotizado(Request $request){
		$montoCotizado = DB::select('SELECT public.get_monto_cotizado('.floatval($request->input('dataMonto')).','.$request->input('dataMonedaCompra').','.$request->input('dataCurrency_venta').','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');
		return response()->json($montoCotizado[0]->get_monto_cotizado);
	}

	public function controlarNC(Request $request){

		$proformas = Proforma::where('id',$request->input('dataProforma'))->first(['saldo_por_facturar']);
		
		return response()->json($proformas->saldo_por_facturar);
	}

	public function doControlNombre(Request $request){
		$respuesta  = 'OK';
		if($request->input('dataNombre') == ''){
			$nombre = '';
		}else{
			$nombre = $request->input('dataNombre');
		}

		if($request->input('dataApellido') == ''){
			$apellido = '';
		}else{
			$apellido = "".$request->input('dataApellido');
		}

		$nombre_apellido = $nombre.''.$apellido;

		$query ="SELECT * 
				FROM personas 
				WHERE concat_ws(' ', nombre, apellido) like '".$nombre_apellido."'
				AND id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_ser_pasajero = true) 
				AND activo = true 
				AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

		$pasajero = DB::select($query);

		if(!empty($pasajero)){
			$respuesta  = 'ERROR';
		}

		 return response()->json($respuesta);
	}

	public function getDatosClienteAux(Request $request)
	{
		$proformaClientes = ProformaCliente::with('cliente')->where('id_proforma',$request->input('dataProforma'))->get();
		$resultado = [];
		if(isset($proformaCliente->cliente->id_tipo_persona)){
			foreach($proformaClientes as $key=>$proformaCliente){
				if(isset($proformaCliente->cliente->id_tipo_persona)){
					$tipos_personas = TipoPersona::where('id',$proformaCliente->cliente->id_tipo_persona)->first();
					$baseTipo =  $tipos_personas->denominacion;
				}else{
					$baseTipo =  "";
				}
				$resultado[$key]['id'] = $proformaCliente->id;		
				$resultado[$key]['dv'] = $proformaCliente->cliente->dv;			
				$resultado[$key]['documento_identidad'] = $proformaCliente->cliente->documento_identidad;
				$resultado[$key]['nombre'] = $proformaCliente->cliente->nombre;
				$resultado[$key]['apellido'] = $proformaCliente->cliente->apellido;
				$resultado[$key]['tipo_persona'] = $tipos_personas->denominacion;
			}
		}
		return response()->json($resultado);
	}

	public function reporteProformas(){
		$estado = DB::table('vw_time_line')->select('denominacion')->distinct()->get();
		return view('pages.mc.proforma.reporteProformas')->with([
			'estados'=>$estado
			]);
	}

	public function verDatosProformas(Request $req){
		ini_set('memory_limit', '-1');
		set_time_limit(300);
		$formSearch = $req->formSearch;
		$draw = intval($req->draw);
		$start = intval($req->start);
		$length = intval($req->length);
		//ORDENAMIENTO POR COLUMNA
		$columna = $req->order[0]['column'];
		$orden = $req->order[0]['dir'];
		//Capturamos las fechas enviadas desde el filtro de criterios
		$formSearch = $req->formSearch;
		$periodo = $formSearch[0]['value'];
		$proformaId= $formSearch[1]['value'];
		$vendedor=$formSearch[2]['value'];
		$estado=$formSearch[3]['value'];
		$cambioEstado=$formSearch[4]['value'];

		if($periodo != ""){
				$fechaPeriodo = explode(' - ', $periodo);
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
			} else {
				$desde     = "";
				$hasta     = "";
			}  

		//Generamos los datos para la consulta
		$proformas=Proforma::where('proformas.id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
		->leftJoin('personas', 'personas.id', '=', 'proformas.id_usuario')
		->leftJoin('vw_time_line', 'vw_time_line.id_proforma', '=', 'proformas.id');
		//Verificamos si envio algun estado para filtrar
		if($estado != ''){
			$proformas = $proformas->where('vw_time_line.denominacion', '=', $estado);
		 }		
		 //Verificamos si envio algun vendedor para filtrar
		if($vendedor != ''){
			$proformas = $proformas->where('proformas.id_usuario', '=', $vendedor);
		 }
		//Verificamos si envio datos en el filtro de numero de proforma.
		if($proformaId != ''){

			$proformas = $proformas->where('proformas.id', '=', $proformaId);
			
		 }
		 //Verificamos si envio datos en el filtro de cambio de estado de proforma.
		if($cambioEstado != ''){

			$proformas = $proformas->where('vw_time_line.id_editor', '=', $cambioEstado);
			
		 }
		if($desde != '' && $hasta != ''){

			$proformas = $proformas->whereBetween('proformas.fecha_alta', array(
			date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
			date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
				));
		 }

		 switch ($columna) {
			case '0':
				$proformas= $proformas->orderBy('proformas.id',$orden);
				break;
			case '1':
				$proformas= $proformas->orderBy('personas.apellido',$orden);
				break;
			case '2':
				$proformas= $proformas->orderBy('proformas.fecha_alta',$orden);
				break;
			case '3':
				$proformas= $proformas->orderBy('vw_time_line.fecha',$orden);
				break;
			case '4':
				$proformas= $proformas->orderBy('vw_time_line.usuario_n',$orden);
				break;
			case '5':
				$proformas= $proformas->orderBy('vw_time_line.denominacion',$orden);
				break;
			default:
				$proformas= $proformas->orderBy('proformas.fecha_alta','desc');
				break;
		 }

		 $proformas = $proformas->offset($start)->limit($length)->get();
		 //Calculamos el total de proformas para el paginado sin el offset.
		 $proformasTotales=Proforma::where('proformas.id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
		 ->leftJoin('personas', 'personas.id', '=', 'proformas.id_usuario')
		 ->leftJoin('vw_time_line', 'vw_time_line.id_proforma', '=', 'proformas.id');
		//Calculamos el total de proformas para el paginado sin el offset si envio un numero de proforma
		 if($proformaId != ''){

			$proformasTotales = $proformasTotales->where('proformas.id', '=', $proformaId);
			
		 }
		//Calculamos el total de proformas para el paginado sin el offset si envio un rango de fechas
		 if($desde != '' && $hasta != ''){
			$proformasTotales = $proformasTotales->whereBetween('proformas.fecha_alta', array(
			date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
			date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
				));
		 }
		//Verificamos si envio algun estado para filtrar
		if($estado != ''){
			$proformasTotales = $proformasTotales->where('vw_time_line.denominacion', '=', $estado);
			
		 }
	   //Verificamos si envio algun vendedor para filtrar
		if($vendedor != ''){
			$proformasTotales = $proformasTotales->where('proformas.id_usuario', '=', $vendedor);
		}
		//Verificamos si envio datos en el filtro de cambio de estado de proforma.
		if($cambioEstado != ''){

		$proformasTotales = $proformasTotales->where('vw_time_line.id_editor', '=', $cambioEstado);
		
		}
		$proformasTotales = $proformasTotales->get();
		$lengtArray=$proformasTotales->count();
				
		return response()->json(array('data'=>$proformas,'draw'=>$draw,
		'recordsTotal'=>$lengtArray,
		'recordsFiltered'=>$lengtArray,));
	}

	public function prestadorProforma(Request $request){
		$prestadores = DB::select("select p.id, p.nombre, p.apellido, p.denominacion_comercial
								from personas p 
								where (p.id_tipo_persona = 15 or p.id_tipo_persona = 14 or p.id_tipo_persona = 3 or p.id_tipo_persona = 2 or p.id_tipo_persona= 8 or p.id_tipo_persona= 10) 
								and p.activo= true 
								and p.id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
		if(isset($_GET['q'])){
            $search = $_GET['q'];
				foreach($prestadores as $key=>$prestador){
					$resultado = strpos(strtolower($prestador->nombre.''.$prestador->apellido), strtolower($search));
					if($resultado === FALSE){
						unset($prestadores[$key]);
					}
				}	
		}
		$json = [];
		foreach($prestadores as $key=>$row){
			$denominacion = '';
			if($row->denominacion_comercial != ""){
				$denominacion =  ' - '.$row->denominacion_comercial;
			}
		    $json[$row->id]['text'] = $row->nombre.' '.$row->apellido.''.$denominacion;
		}
		echo json_encode($json);
	}

	public function proveedorProforma(Request $request){
		$grupoProducto = Producto::where('id',  '=', $request->input('producto'))
									->first();
		$id_grupo_producto = $grupoProducto->id_grupos_producto;
		if($id_grupo_producto == 1){
			$producto = $request->input('producto');
			$aerolineas1 = DB::select("SELECT id as out_id_persona,
										nombre as out_nombre,
									CASE 
										WHEN denominacion_comercial IS NULL THEN
											nombre
										ELSE
										nombre || ' - ' || denominacion_comercial
									END as out_nombre,	
									id_tipo_facturacion as out_tipo 
									from personas where id_empresa = ? and id in (select distinct(id_proveedor)
									from tickets where id_empresa = ?) ",[$this->getIdEmpresa(),$this->getIdEmpresa()]); 
			$aerolineas2 = DB::select('select * from get_producto_persona(?,?)', [$producto,$this->getIdEmpresa()]);
			$aerolineas = array_merge($aerolineas1, $aerolineas2);
			$resultado = [];
			foreach($aerolineas as $key=>$aerolinea){
				$resultado[$key]['id'] = $aerolinea->out_id_persona;
				$resultado[$key]['nombre'] = $aerolinea->out_nombre;
				$resultado[$key]['tipo'] =  $aerolinea->out_tipo;
			}
			usort($resultado, function($a, $b){
				return strcmp($a["nombre"], $b["nombre"]);
			});
		}else{
			$producto = $request->input('producto');
			$getProveedors =DB::select('SELECT public."get_producto_persona"('.$producto.','.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.')');

			$resultado = [];
			foreach($getProveedors as $key=>$getProveedor){
				$valor = str_replace(')', '',str_replace('"', '',str_replace('(', '', $getProveedor->get_producto_persona)));
				$proveedor = explode(',',$valor);
				$resultado[$key]['id'] = $proveedor[0];
				$resultado[$key]['nombre'] = $proveedor[1];
				$resultado[$key]['tipo'] = $proveedor[2];
			}
			usort($resultado, function($a, $b){
				return strcmp($a["nombre"], $b["nombre"]);
			});
		}
		$baseResultado = $resultado;
		if(isset($_GET['q'])){
            $search = $_GET['q'];
				foreach($baseResultado as $key=>$result){
					$resul= strpos(strtolower($result['nombre']), strtolower($search));
					if($resul === FALSE){
						unset($baseResultado[$key]);
					}
				}	
		}
		$json = [];
		foreach($baseResultado as $key=>$row){
		    $json[$row['id']]['text'] = $row['nombre'];
		}
		echo json_encode($json);
	}

	public function getProveedorProfoma(Request $request){
		$proveedores = DB::select('select id as out_id_persona, nombre as out_nombre, id_tipo_facturacion as out_tipo from personas where id ='.$request->input('dataProveedor')); 
		return json_encode($proveedores);	
	}

	public function doItem(Request $request){
		$item = DB::select('select item+1 as item_nuevo from proformas_detalle where id_proforma = ? order by item desc limit 1',[$request->input('dataProforma')]); 
		if(isset($item[0])){
			$item = $item[0]->item_nuevo;
		}
		$resp[] = ['item_nuevo' => 1];
		return response()->json($resp);	
	}

	public function reservasCangoroo(Request $req){
		$tipo_reserva = $req->input('id_tipo_reserva');
		$id_usuario = $req->input('user_id');
		$reservasCangoroos = DB::select("SELECT * FROM public.vw_reservas_cangoroo where id_tipo = ? and estado_gestur = ? and id_proforma is null and id_vendedor_empresa = ?",[$tipo_reserva, 'DISPONIBLE', $id_usuario]);
		return response()->json($reservasCangoroos);
	}

	public function pagarReservaDetalle(Request $request){ 
      
		$mensaje = new \StdClass; 
  
		try{
  
		  DB::beginTransaction();
			$id_proforma_detalle = $request->id_proforma_detalle;
			$nro_op = $this->generar_pago_tc(null, $id_proforma_detalle, $request);
  
			$mensaje->status = 'OK';
			$mensaje->mensaje = 'Se ha Pagado la reserva'; 
			$mensaje->id_op = $nro_op;
			DB::commit();
  
		  } catch( ExceptionCustom $e){
			Log::error($e);
  
			   $mensaje->status = 'ERROR';
			   $mensaje->mensaje = $e->getMessage(); 
	
			   DB::rollBack();
		  } catch(\Exception $e){
  
			Log::error($e);
 
			$mensaje->status = 'ERROR';
			$mensaje->mensaje = 'Ocurrio un error al intentar procesar su pedido'; 
 
			DB::rollBack();
		 }
  
  
		 return response()->json($mensaje);
	}

	public function reservasCangoroDatos(Request $req){

			$id_reserva = $req->id_reserva;
			$id_grupo_reserva = $req->id_grupo_reserva;

			$respuesta = new \StdClass;
			$reservas = DB::select("SELECT * FROM public.vw_reservas_cangoroo where grupo_reserva_id = ? and id_reserva = ?",[$id_grupo_reserva,$id_reserva ])[0];

			$respuesta->codigo = $reservas->booking_reference;
			$respuesta->checkIn = date('d/m/Y', strtotime($reservas->booking_checkin));			   
			$respuesta->checkOut = date('d/m/Y', strtotime($reservas->booking_checkout));
			$respuesta->codigo = $reservas->booking_reference;
			$respuesta->proveedor = $reservas->id_proveedor;
			$respuesta->prestador = $reservas->id_prestador;
			$respuesta->proveedor_name = $reservas->proveedor;
			$respuesta->prestador_name = $reservas->prestador;
			$respuesta->fee = 0;
			$respuesta->costo_base = (float)$reservas->precio_costo;
			$respuesta->venta_base = (float)$reservas->precio_venta;

			$respuesta->venta = (float)$reservas->precio_venta;
			$respuesta->costo = (float)$reservas->precio_costo;
			$respuesta->pago_proveedor = date('d/m/Y', strtotime($reservas->fecha_pago_proveedor));
			//Si no tiene fecha de gsato usamos el del proveedor
			$respuesta->vencimiento = $reservas->fecha_gasto == '0001-01-01 00:00:00' 
				? date('d/m/Y', strtotime($reservas->fecha_pago_proveedor . ' +14 days')) 
				: date('d/m/Y', strtotime($reservas->fecha_gasto . ' +14 days'));

			$respuesta->moneda = $reservas->moneda_costo;
			$respuesta->id_moneda_costo = $reservas->id_moneda_costo;
			$respuesta->code_supplier = $reservas->code_supplier;
			$respuesta->monto_gasto = $reservas->monto_gasto;
			$respuesta->tipo_reserva = $reservas->tipo_reserva;
			
			return response()->json($respuesta); 
	}

	public function repararCotizacionProforma(){

		$proformas_detalles = ProformasDetalle::where('currency_costo_id',143)
		->where('cotizacion_costo','<',7000)
		->where('activo',true)
		->where('id_empresa',17)
		->get();

		foreach ($proformas_detalles as $proformas_detalle) {

			Log::info('$proformas_detalle->'.$proformas_detalle->id);

			$cotizacion = \App\Cotizacion::whereDate('fecha', $proformas_detalle->created_at)
            ->where('id_empresa', $proformas_detalle->id_empresa)
            ->where('id_currency', $proformas_detalle->currency_costo_id)
            ->whereNotNull('venta')
            ->orderBy('id','DESC')
            ->limit(1)
            ->value('venta');

			if(!$cotizacion){
				$cotizacion = \App\Cotizacion::whereDate('fecha', '<', $proformas_detalle->created_at)
                ->where('id_empresa', $proformas_detalle->id_empresa)
                ->where('id_currency', $proformas_detalle->currency_costo_id)
                ->whereNotNull('venta')
                ->orderBy('fecha','DESC')
                ->limit(1)
                ->value('venta');
			}

			if($cotizacion){
				Log::info('$cotizacion->'.$cotizacion);
                $proformas_detalle->cotizacion_costo = $cotizacion;
                $proformas_detalle->save();
            } else {
				dd('No se encontro cotizacion');
			}
		}

        dd('--><--');

	}

	

}//clase
