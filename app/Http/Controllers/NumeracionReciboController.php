<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\NumeracionRecibo;
use App\SucursalEmpresa;
use App\Persona;

class NumeracionReciboController extends Controller
{
	private function getIdUsuario(){
        
        return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
  	}//function

	private function getIdEmpresa(){

	  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

	}//function

	public function index(Request $Request){


		return view('pages.mc.numeracion_recibo.index');
	}	

	public function create(Request $Request){

		$sucursalEmpresa = Persona::select('id','nombre')->where('id_tipo_persona','21')->where('activo', true)->where('id_empresa',$this->getIdEmpresa())->get();
        $centroCostos = DB::table('centro_costo')->where('id_empresa',$this->getIdEmpresa())->orderBy('nombre','ASC')->get();

		return view('pages.mc.numeracion_recibo.add')->with(['sucursalEmpresas'=>$sucursalEmpresa, 'centroCostos'=>$centroCostos]);
	}	

	public function store(Request $request){


	 	$numeracion = new NumeracionRecibo;
		$numeracion->id_sucursal_empresa= $request->input('sucursal');
		$numeracion->establecimiento= $request->input('Establecimiento');
		$numeracion->expedicion= $request->input('expedicion');
		$numeracion->id_empresa= $this->getIdEmpresa();
		$numeracion->id_usuario= $this->getIdUsuario();
		$numeracion->activo= true;
		$numeracion->created_at= date('Y-m-d h:m:s');
		$numeracion->id_sucursal_contable= $request->input('centroCosto');
		
		DB::beginTransaction();
		try{
		 	
			$numeracion_old = DB::table('numeracion_recibo')
			->where('id_sucursal_empresa', $request->input('sucursal'))
			->where('id_empresa', $this->getIdEmpresa())
			->where('activo',true)
			->update([
				'activo' => false
			]);

			$numeracion->save();

	    	flash('La numeración se guardo exitosamente!!')->success();
	    	DB::commit();
            return redirect()->route('numeracion_recibo.index');

		} catch(\Exception $e){
			DB::rollBack();
	    	flash('La numeración no se guardo. Intentelo nuevamente !!')->error();
            return redirect()->back();
		}
	}	


	public function getNumeracionesRecibo(Request $Request){	
		$numeracion = NumeracionRecibo::with('sucursal','centro_costo')
					  ->where('id_empresa',$this->getIdEmpresa())
					  ->get();
        return response()->json($numeracion);
	}
	





}	  
