<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\FacturarController;
use App\Empresa;
use App\VentasRapidasCabecera;
use App\VentasRapidasDetalle;
use App\Divisas;
use App\Persona;
use App\EstadoFactour;
use App\PlazoPago;
use App\HistoricoComentariosProforma;
use App\Voucher;
use App\TipoFactura;
use App\TipoPersona;
use App\PedidoWoo;
use App\Factura;
use App\FacturaDetalle;
use App\Producto;
use App\Currency;
use App\ComercioPersona;
use Session;
use Response;
use Image;
use DB;


class VentasRapidasController extends Controller
{


	public function indexPedido(Request $req)
	{	
		// MODIFICACIONES EN LA VISTA DE PEDIDOS... PARA PRUEBAS

		$web_empresa = DB::table('comercio_empresa')->where('id_empresa',$this->getIdEmpresa())->get();
		if(isset($web_empresa[0]->url_web)){
			$web_empresa = $web_empresa[0]->url_web;
		} else {
			$web_empresa = '#';
		}
		return view('pages.mc.ventas.indexPedido', compact('web_empresa'));

	}

	public function ajaxListadoPedido(Request $req)
	{

		$data = PedidoWoo::with('status')
						->where('id_empresa',$this->getIdEmpresa())	
						->get();

		return response()->json(['data'=>$data]);
	}


		public function nota_pedido_pdf($id_pedido)
		{


			// //===================DATOS DE LOGUEO=======================
				$id_usuario = $this->getIdUsuario();
				$id_empresa = $this->getIdEmpresa();
			// //=========================================================
	
	
			  $okFactura = 'OK';  
			  $mensajeErr = "Error Desconocido";  
			  $es_original = '0';
			  $reimprimirUser = 'true';
			  $imprimir = '0';
			  $empresa = '0';
			  $totalFactura = '0';
			  $NumeroALetras = 'Cero';
			  $factura = array('');
			  $err = 0;
			  $fact = new FacturarController;//INSTANCIA FACTURA CONTROLLER
	
	
			$venta_rapida_cab = VentasRapidasCabecera::with('cliente',
										'currency',
										'vendedor',
										'vendedor_agencia',
										'pedido',
										'formaPago')
			 ->where('id',$id_pedido)->get();
			 $venta_rapida_det = VentasRapidasDetalle::with('producto', 'proveedor')
			 ->where('id_venta_cabecera',$id_pedido)->get();

			
			 $empresa = Empresa::where('id',$id_empresa)->first();
			 $totalFactura = (float)$venta_rapida_cab[0]->total_venta;
			 $NumeroALetras = $fact->convertir($totalFactura);
	
	
			$imprimir = '1';
	
			$comercioPersona= ComercioPersona::where('id', $venta_rapida_cab[0]->id_comercio_empresa)->get(['logo']);

			if(isset($comercioPersona[0]->logo)){
				$logo = $comercioPersona[0]->logo;
			}else{
				$logo = asset("logoEmpresa/$empresa->logo");
			}

			$pdf = \PDF::loadView('pages.mc.ventas.nota_venta_pdf',
				 		compact('empresa','imprimir','venta_rapida_cab','venta_rapida_det','totalFactura','NumeroALetras','logo'));
			$pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true,'enable_css_float'=>true]);
	
			$pdf->setPaper('a4', 'letter')->setWarnings(false);
			return $pdf->download('Nota_Pedido'.$fact->getId4Log().'.pdf');
	
		}//function


			
    	/**
		 * //////////////////////////////////////////////////////////////////////////////////////////////
		 * 										VENTAS RAPIDAS FACTURA PDF
		 * //////////////////////////////////////////////////////////////////////////////////////////////
		 */

		 	
			//====================PARAMETROS DE ESTADO FACTURA======================
			private $facturaFacturada = 29;
			private $facturaAnulada =  30;
			private $facturaVerificado = 2;
			//===============================================================

  	     public function imp_fact_pedido($idFactura){

        //===================DATOS DE LOGUEO=======================
            $id_usuario = $this->getIdUsuario();
            $id_empresa = $this->getIdEmpresa();
        //=========================================================


          $okFactura = 'OK';  
          $mensajeErr = "Error Desconocido";  
          $es_original = '0';
          $reimprimirUser = 'true';
          $imprimir = '0';
          $empresa = '0';
          $totalFactura = '0';
          $NumeroALetras = 'Cero';
          $factura = array('');
          $err = 0;

                     
			if($id_empresa == 1){
				$reImprimirOriginal = DB::table('persona_permiso_especiales')
												->where('id_persona',$id_usuario)
												->where('id_permiso',12)->count();
				}else{
					$reImprimirOriginal = 1;
				}  
						  

            $factura = Factura::with('cliente',
                      'pasajero',
                      'vendedorAgencia',
                      'vendedorEmpresa',
                      'tipoFactura',
                      'timbrado',
                      'proforma',
                      'currency',
                      'ventaRapida')
					  ->where('id',$idFactura)
					  ->where('id_empresa',$this->getIdEmpresa())
                      ->orderBy('id','DESC')                      
					  ->get();
			if(!isset($factura[0]->id)){
				flash('La factura solicitada no existe. Intentelo nuevamente !!')->error();
				return redirect()->route('home');
			}

            $facturaDetalle = FacturaDetalle::with('producto')
                      ->where('id_factura',$idFactura)
                      ->get(); 

			$contadorDetalle= count($facturaDetalle);

            $vendedorEmpresa = Persona::where('id',$factura[0]->id_usuario)->get();  
                      
                
          //Los datos estan completos y no hay error entonces seguir          
        if(!empty($factura) && !empty($facturaDetalle)){


          $empresa = Empresa::where('id',$id_empresa)->first();

          $sucursalEmpresa =  Persona::where('id_empresa',$id_empresa)
                                     ->where('id_tipo_persona', 21)
                                     ->where('activo', true)
                                     ->get();


            //saber si es bruto o neto
            $totalFactura = DB::select('SELECT * FROM get_monto_factura('.$factura[0]->id.')');
            $totalFactura = $totalFactura[0]->get_monto_factura;
                 

            $NumeroALetras = $this->convertir($totalFactura);
            $estadoFactura = $factura[0]->id_estado_factura;
            $facturaImpresa = $factura[0]->factura_impresa;
            $id_factura = $factura[0]->id;


            //Posibilidad de imprimir copias de factura validas
            if($estadoFactura == $this->facturaVerificado || $estadoFactura == $this->facturaFacturada ){

              //Para reimprimir original en caso de tener permisos
              if($facturaImpresa == true && $reImprimirOriginal > 0){

                $imprimir = '1';

                //Para imprimir factura original  
              } else if($facturaImpresa == false){

                $imprimir = '1';

              //Actualizar los datos de factura luego del select
              $update = DB::table('facturas')
              ->where('id',$id_factura)
              ->update(['fecha_hora_impresion_factura'=>date('Y-m-d H:i:00'),
                    'factura_impresa'=>'true',
                    'id_usuario_impresion'=>$this->getIdUsuario()]);

              $update = DB::table('ventas_rapidas_cabecera')
              ->where('id',$factura[0]->id_venta_rapida)
              ->update(['fecha_hora_impresion_factura'=>date('Y-m-d H:i:00'),
                    	'id_usuario_impresion'=>$this->getIdUsuario()]);
              }//if

              //Para imprimir copia
              else if($facturaImpresa == true){

                $imprimir = '2';

              }//if
            }//if

            //Imprimir factura anulada
            else if($estadoFactura == $this->facturaAnulada) {
              $imprimir = '3';
              //dd($factura);
            } else{

              /*DEVOLVER ERROR O IMPRIMIR UNA PAGINA DE ERROR*/
               return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);

            } //else

                    } else {
                         return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);
                    }
                    
            if(isset($factura[0]->ventaRapida->id_comercio_empresa)){
              $idComercio = $factura[0]->ventaRapida->id_comercio_empresa; 
            }else{
              $idComercio = 0;
            }

            $comercioPersona= ComercioPersona::where('id', $idComercio)->get(['logo']);

            if(isset($comercioPersona[0]->logo)){
              $logo = $comercioPersona[0]->logo;
            }else{
              $logo = asset("logoEmpresa/$empresa->logo");
			}
			
           $pdf = \PDF::loadView('pages.mc.ventas.factura_pedido',
					  compact('factura','imprimir','facturaDetalle','empresa','totalFactura','NumeroALetras','vendedorEmpresa', 'sucursalEmpresa', 'logo', 'contadorDetalle'));
            $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);

            $pdf->setPaper(/*'a4', */'letter')->setWarnings(false);
               return $pdf->download('Factura Proforma'.$this->getId4Log().'.pdf');
        
		}//function


	  public function getId4Log(){
	    //Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
	      list($MiliSegundos, $Segundos) = explode(" ", microtime());
	      $id=substr(strval($MiliSegundos),2,4);
	      return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	  }
	
	  public function imp_fact_pedido_original($idFactura){

		//===================DATOS DE LOGUEO=======================
			$id_usuario = $this->getIdUsuario();
			$id_empresa = $this->getIdEmpresa();
		//=========================================================


		  $okFactura = 'OK';  
		  $mensajeErr = "Error Desconocido";  
		  $es_original = '0';
		  $reimprimirUser = 'true';
		  $imprimir = '0';
		  $empresa = '0';
		  $totalFactura = '0';
		  $NumeroALetras = 'Cero';
		  $factura = array('');
		  $err = 0;

		//BUSCAR PERMISO PARA REIMPRIMIR ORIGINAL
			/* $reImprimirOriginal = DB::table('persona_permiso_especiales')
									   ->where('id_persona',$id_usuario)
									   ->where('id_permiso',12)->count();*/
						
			if($id_empresa == 1){
				$reImprimirOriginal = DB::table('persona_permiso_especiales')
												->where('id_persona',$id_usuario)
												->where('id_permiso',12)->count();
				}else{
					$reImprimirOriginal = 1;
				}  
						  

			$factura = Factura::with('cliente',
					  'pasajero',
					  'vendedorAgencia',
					  'vendedorEmpresa',
					  'tipoFactura',
					  'timbrado',
					  'proforma',
					  'currency',
					  'ventaRapida')
					  ->where('id',$idFactura)
					  ->where('id_empresa',$this->getIdEmpresa())
					  ->orderBy('id','DESC')                      
					  ->get();
			if(!isset($factura[0]->id)){
				flash('La factura solicitada no existe. Intentelo nuevamente !!')->error();
				return redirect()->route('home');
			}

			$facturaDetalle = FacturaDetalle::with('producto')
					  ->where('id_factura',$idFactura)
					  ->get(); 

			$contadorDetalle= count($facturaDetalle);

			$vendedorEmpresa = Persona::where('id',$factura[0]->id_usuario)->get();  
					  
				
		  //Los datos estan completos y no hay error entonces seguir          
		if(!empty($factura) && !empty($facturaDetalle)){


		  $empresa = Empresa::where('id',$id_empresa)->first();

		  $sucursalEmpresa =  Persona::where('id_empresa',$id_empresa)
									 ->where('id_tipo_persona', 21)
									 ->where('activo', true)
									 ->get();


			//saber si es bruto o neto
			$totalFactura = DB::select('SELECT * FROM get_monto_factura('.$factura[0]->id.')');
			$totalFactura = $totalFactura[0]->get_monto_factura;
				 

			$NumeroALetras = $this->convertir($totalFactura);
			$estadoFactura = $factura[0]->id_estado_factura;
			$facturaImpresa = $factura[0]->factura_impresa;
			$id_factura = $factura[0]->id;


			//Posibilidad de imprimir copias de factura validas
			if($estadoFactura == $this->facturaVerificado || $estadoFactura == $this->facturaFacturada ){

			  //Para reimprimir original en caso de tener permisos
			  if($facturaImpresa == true && $reImprimirOriginal > 0){

				$imprimir = '1';

				//Para imprimir factura original  
			  } else if($facturaImpresa == false){

				$imprimir = '1';

			  //Actualizar los datos de factura luego del select
			  $update = DB::table('facturas')
			  ->where('id',$id_factura)
			  ->update(['fecha_hora_impresion_factura'=>date('Y-m-d H:i:00'),
					'factura_impresa'=>'true',
					'id_usuario_impresion'=>$this->getIdUsuario()]);

			  $update = DB::table('ventas_rapidas_cabecera')
			  ->where('id',$factura[0]->id_venta_rapida)
			  ->update(['fecha_hora_impresion_factura'=>date('Y-m-d H:i:00'),
						'id_usuario_impresion'=>$this->getIdUsuario()]);
			  }//if

			  //Para imprimir copia
			  else if($facturaImpresa == true){

				$imprimir = '2';

			  }//if
			}//if

			//Imprimir factura anulada
			else if($estadoFactura == $this->facturaAnulada) {
			  $imprimir = '3';
			  //dd($factura);
			} else{

			  /*DEVOLVER ERROR O IMPRIMIR UNA PAGINA DE ERROR*/
			   return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);

			} //else

					} else {
						 return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);
					}
					
			if(isset($factura[0]->ventaRapida->id_comercio_empresa)){
			  $idComercio = $factura[0]->ventaRapida->id_comercio_empresa; 
			}else{
			  $idComercio = 0;
			}

			$comercioPersona= ComercioPersona::where('id', $idComercio)->get(['logo']);

			if(isset($comercioPersona[0]->logo)){
			  $logo = $comercioPersona[0]->logo;
			}else{
			  $logo = asset("logoEmpresa/$empresa->logo");
			}
			$imprimir = '4';
			/*return view('pages.mc.ventas.factura_pedido',
			compact('factura','imprimir','facturaDetalle','empresa','totalFactura','NumeroALetras','vendedorEmpresa', 'sucursalEmpresa', 'logo', 'contadorDetalle')); */
		   $pdf = \PDF::loadView('pages.mc.ventas.factura_pedido',
					  compact('factura','imprimir','facturaDetalle','empresa','totalFactura','NumeroALetras','vendedorEmpresa', 'sucursalEmpresa', 'logo', 'contadorDetalle'));
			$pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);

			$pdf->setPaper(/*'a4', */'letter')->setWarnings(false);
			   return $pdf->download('Factura Proforma'.$this->getId4Log().'.pdf');
		
		}//function


		/**
		 * //////////////////////////////////////////////////////////////////////////////////////////////
		 * 										METODOS AUXILIARES
		 * //////////////////////////////////////////////////////////////////////////////////////////////
		 */
   
		private function getIdEmpresa()
		{
			return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		}

		
		private function getIdUsuario()
		{
			return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		}
	
	public function indexVentasRapidas(Request $request)
	{	
		$estados = EstadoFactour::where('id_tipo_estado','18')->where('visible',true)->orderBy('denominacion','ASC')->get();
		$currency = Currency::where('activo', 'S')->get();
		$vendedor_empresa = Persona::whereIn('id_tipo_persona',array(2, 4, 3,6))->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();
		$cliente = DB::select("
			SELECT 	p.id,p.nombre,p.dv,p.apellido, p.documento_identidad, tp.denominacion, p.activo,
					concat(p.nombre,' ',p.apellido,' ','- '|| trim(p.denominacion_comercial)) as full_data
			FROM personas p
			JOIN tipo_persona tp on tp.id = p.id_tipo_persona
			WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
			AND p.activo = true and p.id_empresa = ".$this->getIdEmpresa()." order by p.nombre asc");
		// dd($this->getIdEmpresa());
		$comercioPersona= ComercioPersona::where('id_empresa', $this->getIdEmpresa())->get();
		$pedidos = PedidoWoo::all();
		
		return view('pages.mc.ventas.index')->with(['estados'=>$estados, 
													'currencys'=>$currency,
													'vendedor_empresas'=>$vendedor_empresa,
													'clientes'=>$cliente,
													'comercioPersonas'=>$comercioPersona,
													'pedidos'=>$pedidos
												]);
	}	

	public function getVentasRapidas(Request $request)
	{
		 /* echo '<pre>';
		  print_r($request->all());*/
		  $ventas = VentasRapidasCabecera::query();
          $ventas = $ventas->with('ventasDetalle', 'factura', 'currency', 'vendedor', 'cliente', 'estado','vendedor_agencia','pedido', 'comercio','tipoVenta');

          	if(!empty($request->input('nro_venta')))
			{
				$ventas = $ventas->where('id',$request->input('nro_venta'));
			}
			
			if(!empty($request->input('id_estado')))
			{
				$ventas = $ventas->where('id_estado', $request->input('id_estado'));
			}

			if(!empty($request->input('id_moneda')))
			{
				$ventas = $ventas->where('id_moneda', $request->input('id_moneda'));
			}

			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 3)
			{
				$ventas = $ventas->where('id_vendedor', $this->getIdUsuario());
			}else{
				if(!empty($request->input('id_vendedor_empresa'))){
					$ventas = $ventas->where('id_vendedor', $request->input('id_vendedor_empresa'));
				}	
			}
			// dd($request->all());

			if(!empty($request->input('origen_negocio')))
			{
				$ventas = $ventas->where('id_comercio_empresa', $request->input('origen_negocio'));
			}

			if(!empty($request->input('id_woo_order')))
			{
				$ventas = $ventas->where('id_pedido', $request->input('id_woo_order'));
			}

			if(!empty($request->input('tipo_venta')))
			{
				if($request->input('tipo_venta') == "ONLINE"){
					$ventas = $ventas->where('id_pedido', '!=' ,null);
				}else{
					$ventas = $ventas->where('id_pedido', '=' ,null);
				}
			}

			if(!empty($request->input('cliente_id')))
			{
				$ventas = $ventas->where('id_cliente', $request->input('cliente_id'));
			}

			if(!empty($request->input('periodo')))
	        {
		        $fechaTrim = trim($request->input('periodo'));
		        $periodo = explode('-', trim($fechaTrim));
		        $desde = explode("/", trim($periodo[0]));
		        $hasta = explode("/", trim($periodo[1]));
		        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
		        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
		        $ventas = $ventas->whereBetween('fecha_venta', [$fecha_desde, $fecha_hasta]);
			}
			$ventas = $ventas->where('id_empresa', $this->getIdEmpresa());
			$ventas = $ventas->orderBy('id', 'desc');
			$ventas = $ventas->get();

			if(!empty($request->input('nro_factura')))
	        {
				$arrayVenta = [];
				foreach($ventas as $key=>$venta){
					if(isset($venta->factura->nro_factura)){
		 				$resultado = strpos(strtolower($venta->factura->nro_factura), strtolower($request->input('nro_factura')));
		 				if($resultado === FALSE){
							
						}else{
							$arrayVenta[] = $ventas[$key];
						}
					}
				}
				$ventas = [];
				$ventas = $arrayVenta;	
			}	

		return response()->json(['data'=>$ventas]);
	}

	public function anularFacturaVentaRapida(Request $req)
	{
		// dd($request->all());
  		$err = 'OK';
		$idUsuario = $this->getIdUsuario();		
		$id_factura = $req->input('id');

		$anular = DB::select("SELECT * FROM anular_factura_ventas_rapidas(".$id_factura.", ".$idUsuario.") limit 1");
		// dd($anular[0]->anular_ventas_rapidas);
		 
		if($anular[0]->anular_factura_ventas_rapidas !== 'OK')
		{
		    $err = 'false';
		}

  		return response()->json(array('err'=>$err,'response'=>$anular));
	}

	public function anularVentaRapida(Request $req)
	{
  		$err = 'OK';
  		$idUsuario = $this->getIdUsuario();		
		$id_venta = $req->input('id');

		$anular = DB::select("SELECT * FROM anular_ventas_rapidas(".$id_venta.", ".$idUsuario.") limit 1");
		// dd($anular[0]->anular_ventas_rapidas);
		 
		if($anular[0]->anular_ventas_rapidas !== 'OK')
		{
		    $err = 'false';
		}

  		return response()->json(array('err'=>$err,'response'=>$anular));

	}
 private static $UNIDADES = [
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
    ];
    private static $DECENAS = [
        'VENTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
    ];
    private static $CENTENAS = [
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
    ];
    public static function convertir($number, $moneda = '', $centimos = '', $forzarCentimos = false)
    {
        $converted = '';
        $decimales = '';
        if (($number < 0) || ($number > 999999999)) {
            return 'No es posible convertir el numero a letras';
        }
        $div_decimales = explode('.',$number);
        if(count($div_decimales) > 1){
            $number = $div_decimales[0];
            $decNumberStr = (string) $div_decimales[1];
            //SI VIENEN NUMERO SOLO LE RELLENA CON CERO
            $decNumberStr = str_pad($decNumberStr, 2, "0", STR_PAD_RIGHT);
            if(strlen($decNumberStr) == 2){
                $decNumberStrFill = str_pad($decNumberStr, 9, '0', STR_PAD_LEFT);
                $decCientos = substr($decNumberStrFill, 6);
                $decimales = self::convertGroup($decCientos);
            }
        }
        else if (count($div_decimales) == 1 && $forzarCentimos){
            $decimales = 'CERO ';
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', self::convertGroup($millones));
            }
        }
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', self::convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', self::convertGroup($cientos));
            }
        }
        if(empty($decimales)){
            $valor_convertido = $converted . strtoupper($moneda);
        } else {
            $valor_convertido = $converted . strtoupper($moneda) . ' CON ' . $decimales . ' ' . strtoupper($centimos);
        }
        return $valor_convertido;
    }
    private static function convertGroup($n)
    {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = self::$CENTENAS[$n[0] - 1];
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= self::$UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }

}
