<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class RecuperarController extends Controller
{
    public function recuperar(Request $req){
		$midReq = new \StdClass;
		$midReq->email = $req->input('email');
		
		$client = new Client();
	
		$midRsp = $client->post(Config::get('config.midRecuperarPass'), [
			'json' => $midReq
		]);
		
		$midObjRsp = json_decode($midRsp->getBody());
		
		return json_encode($midObjRsp);
	}
	
	public function actualizar(Request $req){
		$midReq = new \StdClass;
		$midReq->password = $req->input('password');
		$midReq->tokenEmail = $req->input('tokenEmail');
		
		$client = new Client();
		
		/*echo '<pre>';
		print_r($midReq);*/

		$midRsp = $client->post(Config::get('config.midActualizarPass'), [
			'json' => $midReq
		]);

		$midObjRsp = json_decode($midRsp->getBody());
		
		/*echo '<pre>';
		print_r($midObjRsp);*/

		return json_encode($midObjRsp);
		
	}
	
	
}
