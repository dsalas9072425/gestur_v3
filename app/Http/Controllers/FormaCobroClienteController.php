<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Response;
use Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\TipoTimbrado;
use App\Timbrado;
use App\SucursalEmpresa;
use App\FormaCobroCliente;
use App\CtaCtteFormaCobro;
use App\Currency;
use App\BancoCabecera;
use App\PlanCuenta;
use App\BancoDetalle;

class FormaCobroClienteController extends Controller
{

	private function getIdEmpresa(){

	  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

	}//function

	public function index(Request $Request){
		return view('pages.mc.formaCobroCliente.index');
	}	

	public function getFormaCobro(Request $Request){	

		$selectCuentaKey	= array_keys(FormaCobroCliente::SELECTCUENTA); //Abreviaturas

		$formaCobroCliente = new FormaCobroCliente;
		$formaCobroCliente = $formaCobroCliente->where('id_empresa',$this->getIdEmpresa());
		$formaCobroCliente = $formaCobroCliente->get()->map(function($fc) use($selectCuentaKey){
			$fc->select_cuenta = true;
		
			if(in_array( $fc->abreviatura, $selectCuentaKey)){
				$fc->select_cuenta = FormaCobroCliente::SELECTCUENTA[$fc->abreviatura];
			}

			return $fc;
		});



	    return response()->json($formaCobroCliente);
	}

	public function edit(Request $Request, $id){	
		$formaCobro = FormaCobroCliente::findOrFail($id);	

		return view('pages.mc.formaCobroCliente.edit', compact('formaCobro', 'abreviaturas'));
	}

	public function doEditFormaCobro(Request $request){	
		if($request->input('activo') == 1){
			$activo = true;
		}else{
			$activo = false;
		}

		if($request->input('visible') == 1){
			$visible = true;
		}else{
			$visible = false;
		}

		$id = $request->id;
		try{
			$formaCobro = FormaCobroCliente::findOrFail($id);	
			$formaCobro->denominacion = $request->input('denominacion');
			$formaCobro->visible = $visible;
			$formaCobro->activo = $activo;
			$formaCobro->save();


	    	flash('La Forma de Cobro Cliente se actualizo exitosamente!!')->success();
            return redirect()->route('indexFormaCobro');
		} catch(\Exception $e){
	    	flash('La Forma de Cobro Cliente no se actualizo. Intentelo nuevamente !!')->error();
            return redirect()->back();
		}
			
	}	


	public function asignarCuentaContable(Request $request, $id){

		$formaCobro = FormaCobroCliente::findOrFail($id);	
		$cttaCtte = CtaCtteFormaCobro::with('moneda','planCuenta','banco')
									->where('id_forma_cobro',$id)
									->where('activo',true)
									->get();
		$currency = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
		$data_plans = PlanCuenta::where('activo',true)->where('asentable', true)->where('id_empresa', $this->getIdEmpresa())->get();
		$bancos = DB::table('banco_detalle as bd')
						->select('bd.*','cu.hb_desc as currency_code','tcb.denominacion','bc.nombre AS banco_n')
						->leftJoin('currency as cu','cu.currency_id','=','bd.id_moneda')
						->leftJoin('tipo_cuenta_banco as tcb','tcb.id','=','bd.id_tipo_cuenta')
						->leftJoin('banco_cabecera as bc', 'bc.id','=','bd.id_banco')
						->where('bc.id_empresa',$this->getIdEmpresa())
						->get();
		return view('pages.mc.formaCobroCliente.asignarCuenta')->with(['cttaCtte'=>$cttaCtte,'formaCobro'=>$formaCobro,'currency'=>$currency,'bancos'=>$bancos,'data_plans'=>$data_plans]);

	}	

	public function getEliminarCtaCtte(Request $request){	
		$resp = new \StdClass; 
		try{
			DB::table('cta_ctb_forma_cobro')
			->where('id',$request->input('idCtta'))
			->update([
					'activo'=>false,
					]);
			$resp->status = 'OK';
			$resp->mensaje = 'Se ha eliminado el registro';
		} catch(\Exception $e){
			$resp->status = 'ERROR';
			$resp->mensaje = 'No se ha eliminado el registro';
		}
		return response()->json($resp);
	}

	public function getAsignarCtaCtte(Request $request){	
		$resp = new \StdClass; 

	 	 try{
			$cttaCtte = new CtaCtteFormaCobro;
			$cttaCtte->id_forma_cobro = $request->input('id');


			if($request->input('plan_contable')){
				$cttaCtte->id_cuenta_contable = $request->input('plan_contable');
			}else{
				throw new \Exception("Falta cuenta contable", 1);
				
			}

			if($request->input('banco')){
				$cttaCtte->id_cuenta_banco = $request->input('banco');
				$banco = BancoDetalle::findOrFail($request->input('banco'));
				$cttaCtte->id_moneda = $banco->id_moneda;

			}else{
				throw new \Exception("Falta cuenta de banco", 1);
				
			}

			$validar = CtaCtteFormaCobro::where('id_empresa',$this->getIdEmpresa())
					   ->where('id_forma_cobro', $request->input('id'))
					   ->where('id_moneda',  $banco->id_moneda)
					   ->where('activo', true)
					   ->first();
			if($validar){
				throw new \Exception("Ya existe cuenta en esa moneda", 1);
			}

			$cttaCtte->id_empresa = $this->getIdEmpresa();
			$cttaCtte->save();


			$id = $cttaCtte->id;
			$resp->status = 'OK';
			$resp->id = $id;
			$resp->mensaje = 'Se ha agregado el registro';

	 	 } catch(\Exception $e){
			Log::error($e);
			$resp->status = 'ERROR';
			$resp->mensaje = $e->getMessage();
		}  

		return response()->json($resp);
	}	

	public function add(Request $Request){	
		$abreviaturas = FormaCobroCliente::SELECTCUENTA;
		return view('pages.mc.formaCobroCliente.add',compact('abreviaturas'));
	}

	public function doAddFormaCobro(Request $request){	

		// dd(FormaCobroCliente::CUENTA_DETALLE[$request->input('abreviatura')],$request->input('abreviatura'));
		$depositable = FormaCobroCliente::CUENTA_DETALLE[$request->input('abreviatura')]['depositable'];
		$tipo = FormaCobroCliente::CUENTA_DETALLE[$request->input('abreviatura')]['tipo'];

		if($request->input('visible') == 1){
			$visible = true;
		}else{
			$visible = false;
		}

		if($request->input('activo') == 1){
			$activo = true;
		}else{
			$activo = false;
		}


	//  try	{
		$cheque = new FormaCobroCliente;
		$cheque->denominacion = $request->input('denominacion');
		$cheque->abreviatura = $request->input('abreviatura');
		$cheque->tipo = $tipo;
		$cheque->depositable = $depositable;
		$cheque->visible = $visible;
		$cheque->id_empresa = $this->getIdEmpresa();
		$cheque->activo = true;
		$cheque->save();
		flash('La Forma de Cobro Cliente se creo exitosamente!!')->success();
	  return redirect()->route('indexFormaCobro');
 	// } catch(\Exception $e){
	// 	Log::error($e);
	// 	flash('La Forma de Cobro Cliente no se creo. Intentelo nuevamente !!')->error();
	// 	return redirect()->back();
	// } 

	}

}
