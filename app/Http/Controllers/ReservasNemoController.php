<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Currency;
use \SimpleXMLElement;
class ReservasNemoController extends Controller
{
    //
    public function index(){
        return view('pages.mc.reservasNemo.index');
    }


    //muestra todos los datos de la vista vw_reservas_nemo_lista para estructurar la tabla y acceder mediante cada ID a ver los datos de respuesta de la plataforma NEMO. 
    public function verDatos(Request $req){

        ini_set('memory_limit', '-1');
		set_time_limit(300);
        $ajax=0;
        
        $draw = intval($req->draw);
        $start = intval($req->start);
        $length = intval($req->length);

        if(!empty($req->all()) && !is_null($req->all())){
        $ajax++;
        //ORDENAMIENTO POR COLUMNA
        $columna = $req->order[0]['column'];
        $orden = $req->order[0]['dir'];
        //capturamos el valor del campo search de la tabla para hacer busquedas con ese valor en la bd
        $buscar = $req->search['value'];
        $cont = 0;
        $filtrarMax = 0;
        //guardar valores provenientes del filtro
        $formSearch = $req->formSearch;
        $codigoReserva= $formSearch[1]['value'];
        $codigoProveedor= $formSearch[0]['value'];
        $fechaEmision = $formSearch[2]['value'];

        $reservas = DB::table('vw_reservas_nemo_lista');
        if($codigoReserva !== ""){
            $reservas= $reservas->where('booking_reference', '=', $codigoReserva);
        }       
        if($codigoProveedor !== ""){
            $reservas= $reservas->where('supplier_code', '=', $codigoProveedor);
        }   
        if($fechaEmision !== ""){
            $date = explode('/', $fechaEmision);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            $reservas= $reservas->where('booking_creation_date', '=', $fecha);
        }
        
        $count = $reservas->count();
        $reservas=$reservas->offset($start)->limit($length);
        $reservas=$reservas->get();
        
        return response()->json(['draw'=>$draw,
        'recordsTotal'=>$count,
        'recordsFiltered'=>$count,
        'data'=>$reservas]);
		}//IF REQUEST
		
		if($ajax == 0){
	        $count = DB::table('vw_reservas_nemo_lista')->count();
            $reservas = DB::table('vw_reservas_nemo_lista')->offset($start)->limit($length)->get();
                return response()->json(['draw'=>$draw,
                'recordsTotal'=>$count,
                'recordsFiltered'=>$count,
                'data'=>$reservas]);	
		}
        
    }

    //trae los datos segun el ID (codigo) de la reserva para mostrar en detalle la respuesta del sistema NEMO
    public function getRespuestaGDS(Request $req){
        $codigoReserva= $req->id;
        $tipo= $req->tipo; 
        if($tipo == 1){
            $busqueda="respuesta_gds";
        }else{
            $busqueda="mensaje_nemo";
        }
        
        $mensaje= DB::table('reservas_nemo')->select($busqueda)->where('booking_reference', '=', $codigoReserva)->get();
    //dd($mensaje);  
        $cadena_limpia = str_replace(array("\\n", "\\t", "\\r", "\\f", "\\v", "&lt;", "\\"), '', $mensaje);

 
        return response()->json(array('respuesta'=>$cadena_limpia) );
    }

    //estira los datos para visualizar la pantalla de edicion
    public function editarReservaNemo(Request $req){
       

        $codigoReserva=$req->id;
        $monedas=Currency::all();
        $tipoReservas=DB::table('tipos_reservas')->get();
        
        $datos= DB::table('vw_reservas_nemo_lista')->where('booking_reference', '=', $codigoReserva)->get();
        $monedaSelected=DB::table('currency')->where('currency_code','=',$datos[0]->moneda)->get();
        return view('pages.mc.reservasNemo.editarReserva')->with(['data'=>$datos,'moneda'=>$monedas,'tipoReserva'=>$tipoReservas,'monedaSelect'=>$monedaSelected]);
    }

    //recibe los parametros para editar los campos de la tabla reservas nemo
    public function guardarEditarNemo(Request $req){
        //echo '<pre>';
        //print_r($req->all());
        $codigoReserva=$req->codigoReserva;
        $cliente=$req->cliente;
        $tarifa= str_replace(',','.', str_replace('.','', $req->tarifa));
        $moneda=$req->moneda;
        $pais=$req->pais;
        $ciudad=$req->ciudad;
        $prestador=$req->prestador;
        $proveedor=$req->proveedor;
        $servicio=$req->servicio;
        $checkin=$req->checkin;
        $checkout=$req->checkout;
        $tktl=$req->tktl;
        $monedaSelected=DB::table('currency')->where('currency_id','=',$moneda)->get();
        DB::table('reservas_nemo')->where('booking_reference','=',$codigoReserva)
		->update(['client_name'=>$cliente,'precio_total'=>$tarifa,'moneda'=>$monedaSelected[0]->currency_code,'destino_pais'=>$pais,'destino_ciudad'=>$ciudad,'supplier_name'=>$proveedor,'lender_name'=>$prestador,'tipo_reserva_nemo_id'=>$servicio,'booking_checkin'=>$checkin,'booking_checkout'=>$checkout,'booking_deadline'=>$tktl]);
        return response()->json(array('estado'=>1));	
    }


public function getPrestadoresListado(Request $req){
    $destinos = DB::select("SELECT concat(personas.nombre,' ',personas.apellido,' - '||personas.documento_identidad) as pasajero_data, personas.id, tipo_persona.denominacion as tipo_persona
                            FROM personas, tipo_persona
                            WHERE personas.id_tipo_persona = tipo_persona.id 
                            AND personas.activo = true   
                            AND personas.id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa
                        );
    $dest =  json_encode($destinos);
    $destinos =  json_decode($dest);

    if($req->has('q')){
        $search = $req->q;
            foreach($destinos as $key=>$destino){
                $resultado = strpos(strtolower($destino->pasajero_data), strtolower($search));
                if($resultado === FALSE){
                    unset($destinos[$key]);
                }
            }	
    }
    return response()->json($destinos);

}

}
