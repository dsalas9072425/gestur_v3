-- View: public.vw_total_facturacion_renta

-- DROP VIEW public.vw_total_facturacion_renta;

CREATE OR REPLACE VIEW public.vw_total_facturacion_renta AS
 SELECT resultado.id_usuario,
    sum(resultado.total) AS total
   FROM ( SELECT renta.id_usuario,
            sum(renta.total) AS total
           FROM ( SELECT facturas.id_usuario,
                    sum(facturas.renta) AS total
                   FROM facturas
                  WHERE facturas.id_moneda_venta = 143 AND facturas.fecha_hora_facturacion >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) AND facturas.id_estado_factura = 29
                  GROUP BY facturas.id_usuario
                UNION ALL
                 SELECT facturas.id_usuario,
                    sum(facturas.renta / facturas.cotizacion_factura) AS total
                   FROM facturas
                  WHERE facturas.id_moneda_venta = 111 AND facturas.fecha_hora_facturacion >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) AND facturas.id_estado_factura = 29
                  GROUP BY facturas.id_usuario) renta
          GROUP BY renta.id_usuario) resultado
  GROUP BY resultado.id_usuario;

ALTER TABLE public.vw_total_facturacion_renta
    OWNER TO postgres;



-- View: public.vw_total_facturacion_mes
-- Total facturacion del mes por cada vendedor
-- DROP VIEW public.vw_total_facturacion_mes;

CREATE OR REPLACE VIEW public.vw_total_facturacion_mes AS
 SELECT resultado.id_usuario,
    sum(resultado.total) AS total
   FROM ( SELECT facturas.id_usuario,
            sum(facturas.total_neto_factura) AS total
           FROM facturas
          WHERE facturas.id_moneda_venta = 143 
     AND facturas.fecha_hora_facturacion >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) AND facturas.id_estado_factura = 29
          GROUP BY facturas.id_usuario
        UNION ALL
         SELECT facturas.id_usuario,
            sum(facturas.total_neto_factura / facturas.cotizacion_factura) AS total
           FROM facturas
          WHERE facturas.id_moneda_venta = 111 AND facturas.fecha_hora_facturacion >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) AND facturas.id_estado_factura = 29
          GROUP BY facturas.id_usuario) resultado
  GROUP BY resultado.id_usuario;

ALTER TABLE public.vw_total_facturacion_mes
    OWNER TO postgres;

 -- View: public.vw_periodo_facturacion_10dias

-- DROP VIEW public.vw_periodo_facturacion_10dias;

 SELECT resultado.fecha,
    sum(resultado.total) AS total
   FROM ( SELECT to_char(facturas.fecha_hora_facturacion, 'DD/MM/YYYY'::text) AS fecha,
            sum(facturas.total_neto_factura) AS total
           FROM facturas
          WHERE facturas.id_moneda_venta = 143 AND facturas.fecha_hora_facturacion >= (now()::timestamp(0) without time zone - '8 days'::interval)::timestamp(0) without time zone AND facturas.id_estado_factura = 29 AND facturas.id_empresa = 1
          GROUP BY (to_char(facturas.fecha_hora_facturacion, 'DD/MM/YYYY'::text)), facturas.id_usuario
        UNION ALL
         SELECT to_char(facturas.fecha_hora_facturacion, 'DD/MM/YYYY'::text) AS fecha,
            sum(facturas.total_neto_factura / facturas.cotizacion_factura) AS total
           FROM facturas
          WHERE facturas.id_moneda_venta = 111 AND facturas.fecha_hora_facturacion >= (now()::timestamp(0) without time zone - '8 days'::interval)::timestamp(0) without time zone AND facturas.id_estado_factura = 29 AND facturas.id_empresa = 1
          GROUP BY (to_char(facturas.fecha_hora_facturacion, 'DD/MM/YYYY'::text)), facturas.id_usuario) resultado
  GROUP BY resultado.fecha
  ORDER BY resultado.fecha;



 -- View: public.vw_proformas_pendientes

-- DROP VIEW public.vw_proformas_pendientes;

 SELECT total_facturacion.id_usuario,
    sum(total_facturacion.total) AS total
   FROM ( SELECT resultado.id_usuario,
            sum(resultado.total) AS total
           FROM ( SELECT proformas.id_usuario,
                    sum(proformas.total_neto_facturar) AS total
                   FROM proformas
                  WHERE proformas.id_moneda_venta = 143 AND proformas.fecha_alta >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) AND proformas.estado_id <> 4 AND proformas.estado_id <> 5
                  GROUP BY proformas.id_usuario
                UNION ALL
                 SELECT proformas.id_usuario,
                    sum(proformas.total_neto_facturar / proformas.cotizacion) AS total
                   FROM proformas
                  WHERE proformas.id_moneda_venta = 111 AND proformas.fecha_alta >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) AND proformas.estado_id <> 4 AND proformas.estado_id <> 5
                  GROUP BY proformas.id_usuario) resultado
          GROUP BY resultado.id_usuario) total_facturacion
  GROUP BY total_facturacion.id_usuario;


SELECT resultado.id_usuario, 
    resultado.vendedor_id, 
    sum(resultado.total_venta) as total, 
    (SELECT meta from meta_vendedores mv WHERE mv.id_usuario = resultado.id_usuario AND mv.id_vendedor = resultado.vendedor_id ),
    p.nombre as nombre_vendedor_agencia,
    ag.nombre as agencia_nombre
    FROM (
    SELECT facturas.id_usuario,facturas.vendedor_id,
    get_monto_factura(facturas.id) AS total_venta
           FROM facturas, meta_vendedores mv
          WHERE facturas.id_moneda_venta = 143 
      AND facturas.fecha_hora_facturacion >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) 
      AND facturas.id_estado_factura = 29 
      AND mv.id_usuario = facturas.id_usuario
      AND mv.id_vendedor = facturas.vendedor_id
      AND mv.activo = true
         
        UNION ALL
         SELECT facturas.id_usuario,facturas.vendedor_id,
            get_monto_factura(facturas.id) / facturas.cotizacion_factura AS total_venta
           FROM facturas, meta_vendedores mv
          WHERE facturas.id_moneda_venta = 111 
      AND facturas.fecha_hora_facturacion >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) 
      AND facturas.id_estado_factura = 29
      AND mv.id_usuario = facturas.id_usuario
      AND mv.id_vendedor = facturas.vendedor_id
      AND mv.activo = true
     ) resultado
     LEFT JOIN personas p
     ON p.id = resultado.vendedor_id
     LEFT JOIN personas ag
     ON ag.id = p.id_sucursal_empresa
     GROUP BY resultado.vendedor_id, resultado.id_usuario, p.nombre, ag.nombre
          










-- FUNCTION: public.get_porcentaje_comision(integer, double precision)

-- DROP FUNCTION public.get_porcentaje_comision(integer, double precision);

CREATE OR REPLACE FUNCTION public.get_porcentaje_comision(
  p_id_vendedor integer,
  p_renta double precision)

    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE
  v_porcentaje integer;

BEGIN
  begin
  
  v_porcentaje = 0;
  
  select ed.porcentaje 
  from escala_comision_detalle as ed, 
  escala_comision_cabecera as ec 
  where p_renta between desde and hasta  
  and ec.id_vendedor = p_id_vendedor
  INTO v_porcentaje;

  IF v_porcentaje IS NULL THEN

  v_porcentaje = 0;

  END IF;
  
  RETURN v_porcentaje;
  exception
    when OTHERS then
      raise exception 'get_porcentaje_comision: %', SQLERRM;
  end;
END;$BODY$;

ALTER FUNCTION public.get_porcentaje_comision(integer, double precision)
    OWNER TO postgres;






SELECT fecha,sum(total) as total FROM(
      SELECT to_char(facturas.fecha_hora_facturacion, 'YYYY-MM-DD'::text) AS fecha,
            sum(get_monto_factura(facturas.id)) AS total
           FROM facturas
          WHERE facturas.id_moneda_venta = 143 
      AND facturas.fecha_hora_facturacion >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) 
      AND facturas.id_estado_factura = 29 AND facturas.id_empresa = 1
          GROUP BY (to_char(facturas.fecha_hora_facturacion, 'YYYY-MM-DD'::text)), facturas.id_usuario
        UNION ALL
         SELECT to_char(facturas.fecha_hora_facturacion, 'YYYY-MM-DD'::text) AS fecha,
            sum(get_monto_factura(facturas.id) / facturas.cotizacion_factura) AS total
           FROM facturas
          WHERE facturas.id_moneda_venta = 111 
     AND facturas.fecha_hora_facturacion >= (( SELECT date_trunc('month'::text, 'now'::text::date::timestamp with time zone)::date AS date_trunc)) 
     AND facturas.id_estado_factura = 29 AND facturas.id_empresa = 1
          GROUP BY (to_char(facturas.fecha_hora_facturacion, 'YYYY-MM-DD'::text)), facturas.id_usuario
               ) as resultado GROUP BY fecha order by 1




-- View: public.vw_escritorio_datos

-- DROP VIEW public.vw_escritorio_datos;

CREATE OR REPLACE VIEW public.vw_escritorio_datos AS
 
 ------------QUERY MODIFICADO------------------

select id_usuario, sum(total) as total_facturacion, sum(renta) as renta from (

select id_usuario, 
sum(renta) renta,       
sum(get_monto_factura(id)) total                                     
from facturas
where id_moneda_venta = 143
and id_usuario = 470  and fecha_hora_facturacion >= (select date_trunc('month',current_date)::date) and id_estado_factura = 29
group by id_usuario
union all
select id_usuario, 
sum(renta) renta,
sum(get_monto_factura(id)/cotizacion_factura)  total
from facturas
where id_moneda_venta =  111
and id_usuario = 470 and fecha_hora_facturacion >= (select date_trunc('month',current_date)::date)  and id_estado_factura = 29
group by id_usuario)

 as total_facturacion
group by id_usuario 
--=================------------QUERY MODIFICADO------------------
UNION ALL

select id_usuario, sum(total) as renta from (
select id_usuario, sum(renta) total
from facturas
where id_moneda_venta = 143
and id_usuario = 470  and fecha_hora_facturacion >= (select date_trunc('month',current_date)::date) and id_estado_factura = 29
group by id_usuario
union all
select id_usuario, sum(renta/cotizacion_factura)  total
from facturas
where id_moneda_venta =  111
and id_usuario = 470  and fecha_hora_facturacion >= (select date_trunc('month',current_date)::date) and id_estado_factura = 29  
group by id_usuario) as renta
group by id_usuario
                       
UNION ALL
                       
select id_usuario, sum(total) as proformas_cobrar from (
select id_usuario, sum(get_monto_proforma(id)) total
from proformas
where id_moneda_venta = 143
and id_usuario = 470 and fecha_alta >= (select date_trunc('month',current_date)::date) and estado_id <> 4 and estado_id <> 5
group by id_usuario
union all
select id_usuario, sum(get_monto_proforma(id)/cotizacion)  total
from proformas
where id_moneda_venta =  111
and id_usuario = 470 and fecha_alta >= (select date_trunc('month',current_date)::date) and estado_id <> 4 and estado_id <> 5
group by id_usuario) as resultado
group by id_usuario



ALTER TABLE public.vw_escritorio_datos
    OWNER TO postgres;

-- select * from vw_escritorio_datos






-- FUNCTION: public.log_actualizar_fecha_pago_proveedor()

-- DROP FUNCTION public.log_actualizar_fecha_pago_proveedor();

CREATE FUNCTION public.log_actualizar_fecha_pago_proveedor()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS $BODY$
DECLARE
        v_id_proforma_detalle integer;
        v_fecha date;
        v_plazo_dia integer;
        v_fecha_op date;
        v_fecha_in date;
        v_semana_num integer;
        c_dia_semana integer;
        v_restar_dias integer;
        v_id_proveedor integer;

BEGIN

    --Obtener id proveedor de detalle proforma
    SELECT id_proveedor 
    FROM proformas_detalle
    WHERE id = NEW.id
    INTO v_id_proveedor;
    
 
    --OBTENER EL PLAZO DE DIAS  del proveedor                                          
    SELECT 
    COALESCE(pl.cantidad_dias,0)                                             
    FROM personas as p 
    LEFT JOIN plazos_pago as pl 
    ON pl.id = p.id_plazo_pago
    WHERE p.id = v_id_proveedor
    INTO v_plazo_dia  ;
                                                   
    IF v_plazo_dia <> 0 THEN
    
    v_fecha_op = CAST(NEW.fecha_in AS DATE) + CAST( v_plazo_dia||'days' AS INTERVAL);
    v_semana_num = extract(dow  from (CAST(NEW.fecha_in AS DATE) + CAST(v_plazo_dia||'days' AS INTERVAL)) );
    
    ELSE
    --Si no tiene plazo restar 30 dias                                
    v_fecha_op = CAST(NEW.fecha_in AS DATE) - CAST( '30 days' AS INTERVAL);
    v_semana_num = extract(dow  from (CAST(NEW.fecha_in AS DATE) - CAST('30 days' AS INTERVAL)) );                                
                                                                     
    --END OBTENER EL PLAZO DE DIAS  del proveedor                                          
    END IF;                
    
    -- SI NO ES IGUAL A VIERNES                              
    IF v_semana_num <> 5 THEN
    
    CASE 
      
      WHEN  v_semana_num = 6 THEN v_restar_dias = 1;
      WHEN  v_semana_num = 0 THEN v_restar_dias = 2;
      WHEN  v_semana_num = 1 THEN v_restar_dias = 3;
      WHEN  v_semana_num = 2 THEN v_restar_dias = 4;
      WHEN  v_semana_num = 3 THEN v_restar_dias = 5;
      WHEN  v_semana_num = 4 THEN v_restar_dias = 6;
                                                                  
     END CASE;
    --restar dias para llegar el viernes
     SELECT
    (CAST(v_fecha_op AS DATE) - CAST( v_restar_dias||'days' AS INTERVAL))
    INTO v_fecha_op;    
                             
    --END SI NO ES IGUAL A VIERNES                                                           
    END IF;
    
    --Si la fecha de pago es menor al dia actual , establecer vencimiento al dia actual                              
    IF v_fecha_op < CURRENT_DATE THEN
    v_fecha_op = CURRENT_DATE;
    END IF;                              
    
    --insertar fecha_op
    UPDATE proformas_detalle
    SET fecha_pago_proveedor = v_fecha_op
    WHERE id = NEW.id;

RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.log_actualizar_fecha_pago_proveedor()
    OWNER TO postgres;



---------------------------------------*******************************************--------------------------------*******************************




--PRUEBAS DE FACTURAS
delete from facturas  where id_proforma=90550;
delete from facturas_detalle  where id_proforma=90550;
update  proformas set estado_id = 3, fecha_facturacion = null,factura_id = null where id=90550;




--INSERTAR AGENCIA

INSERT INTO personas
    (id_tipo_persona,
     nombre,
     telefono,
     email,
     fecha_alta,
     fecha_validez,
     id_tipo_identidad,
     documento_identidad,
     id_empresa,
    id_pais,
    id_agencia,
    created_at,
    denominacion_comercial) 

    SELECT 
    8 as id_tipo_persona,
    razon_social as nombre,
    telefono,
     email,
    fecha_alta,
    fecha_alta + CAST('1 year' AS INTERVAL) as fecha_validez,
    1 as id_tipo_identidad,
    ruc as documento_identidad,
    1 as id_empresa,
    cod_pais as id_pais,
    id_agencia,
    to_date(to_char(now(), 'YYYY-MM-DD HH24:MI:SS'), 'YYYY-MM-DD HH24:MI:SS'),
    'migradoV2' as denominacion_comercial
    from agencias 
 
 
 --select * from personas where id_agencia = 261

--INSERTAR SUCURSAL AGENCIA
INSERT INTO personas
        (id_tipo_persona,
         nombre,
         telefono,
         email,
         fecha_alta,
         fecha_validez,
         id_tipo_identidad,
         documento_identidad,
         id_empresa,
        id_pais,
        id_persona,
        id_sucursal_agencia,
        created_at,
        denominacion_comercial
        ) 

        SELECT 
        9 as id_tipo_persona,
        suc.descripcion_agencia as nombreSucursal,
        suc.telefono,
        suc.email,
        pe.fecha_alta,
        pe.fecha_alta + CAST('1 year' AS INTERVAL) as fecha_validez,
        1 as id_tipo_identidad,
        pe.documento_identidad,
        1 as id_empresa,
        pe.id_pais,
        pe.id as id_persona,
        suc.id_sucursal_agencia,
        to_date(to_char(now(), 'YYYY-MM-DD HH24:MI:SS'), 'YYYY-MM-DD HH24:MI:SS')  as created_at,
        'migrado' as denominacion_comercial

        from sucursales as suc, personas as pe
         where pe.id_agencia = suc.id_agencia 
         and pe.id_agencia = suc.id_agencia
-- select * from personas where denominacion_comercial = 'migrado'
--select * from agencias where id_agencia = 261
--select * from sucursales where id_agencia = 261
--select * from personas where id_agencia = 261

FORMA CORRECTA DE GENERAR FECHAS ACTUALES
select to_char(now(), 'YYYY-MM-DD HH24:MI:SS')  


Migrar usuarios

INSERT INTO personas
    (id_tipo_persona,
     nombre,
     celular,
     id_pais,
     id_tipo_identidad,
     documento_identidad,
     fecha_alta,
     fecha_validez,
     fecha_nacimiento,
     email,
     usuario,
     password,
     id_sucursal_empresa,
     id_persona,
     id_empresa,
     created_at,
     denominacion_comercial) 
    
    SELECT
    
    10 as id_tipo_persona,
    usu.nombre_apellido as nombre,
    usu.celular,
    suc.id_pais,
    1 as id_tipo_identidad,
    usu.documento_identidad,
    usu.fecha_alta,
    usu.fecha_validez,
    usu.fecha_nacimiento,
    usu.usuario as email,
    usu.usuario,
    usu.password,
    suc.id as id_sucursal_empresa ,
    suc.id_persona ,
    1 as id_empresa,
     to_date(to_char(now(), 'YYYY-MM-DD HH24:MI:SS'), 'YYYY-MM-DD HH24:MI:SS')  as created_at,
    'migrado' as denominacion_comercial

    
    FROM usuarios as usu, personas suc
    WHERE suc.id_sucursal_agencia = usu.id_sucursal_agencia






























-- FUNCTION: public.get_monto_factura(integer)

-- DROP FUNCTION public.get_monto_factura(integer);

CREATE OR REPLACE FUNCTION public.get_monto_factura(
    p_id_factura integer)
    RETURNS double precision
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

    v_tipo_facturacion double precision;
    v_total_factura double precision;
    v_total_factura = 0;

BEGIN
  begin
    SELECT id_tipo_facturacion 
    FROM facturas
    WHERE id = p_id_factura
    INTO v_tipo_facturacion;

    IF v_tipo_facturacion = 1 THEN

        SELECT total_bruto_factura 
        FROM facturas 
        WHERE id = p_id_factura
        INTO v_total_factura;

    IF v_tipo_facturacion = 2 THEN

        SELECT total_neto_factura 
        FROM facturas 
        WHERE id = p_id_factura
        INTO v_total_factura;

    END IF;

  RETURN v_total_factura;
  exception
    when OTHERS then
      raise exception 'error_get_cotizacion: %', SQLERRM;
  end;
END;$BODY$;

ALTER FUNCTION public.get_monto_factura(integer)
    OWNER TO postgres;






































-- View: public.vw_tickets_pendientes

-- DROP VIEW public.vw_tickets_pendientes;

CREATE OR REPLACE VIEW public.vw_tickets_pendientes AS
 SELECT f.id_tipo_facturacion,
        (SELECT 
             CASE j.id_tipo_facturacion 
               WHEN 1 THEN j.total_bruto_factura
               WHEN 2 THEN j.total_neto_factura
               ELSE 0
        END 
          FROM facturas j WHERE j.id_proforma = f.id_proforma) AS f.total_factura,
        f.total_bruto_factura,
        f.saldo_factura,
        f.total_factura - f.saldo_factura AS total_cobrado,

        ( SELECT sum(proformas_detalle.precio_venta) AS sum
          FROM proformas_detalle
          WHERE (proformas_detalle.id_producto = ANY (ARRAY[1, 9])) 
          AND proformas_detalle.id_proforma = f.id_proforma) 
        AS total_ticket,



   FROM facturas f

   WHERE f.total_cobrado <= (( SELECT sum(fd.precio_venta) AS sum
                               FROM facturas_detalle AS fd
                               WHERE (fd.id_producto = ANY (ARRAY[1, 9])) 
                               AND fd.id_proforma = f.id_proforma));

ALTER TABLE public.vw_tickets_pendientes
    OWNER TO postgres;
















--VALIDAR SI LA PROFORMA SE PUEDE FACTURAR

CREATE OR REPLACE FUNCTION public.control_saldo (p_id_proforma integer)
    
    RETURNS varchar
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$DECLARE
    resultado varchar;
    v_saldo double precision;
    v_id_currency integer;
    v_tipo_factura integer;
    v_tipo_facturacion integer;
    v_monto_total_proforma double precision;
    v_id_cliente integer;
    v_resp varchar;
    v_total_calc double precision;
    v_cotizacion_actual double precision;


BEGIN
  begin

    
    SELECT tipo_factura_id
    FROM  proformas
    WHERE id = p_id_proforma
    INTO  v_tipo_factura; 


   
     v_resp = 'Valido para facturar';
     
     -- VALIDAR SI ES FACTURA CREDITO 
    IF v_tipo_factura = 1 THEN
    
    
    
    
            SELECT tipo_facturacion
            FROM  proformas
            WHERE id = p_id_proforma
            INTO   v_tipo_facturacion; 

            SELECT cliente_id
            FROM  proformas
            WHERE id = p_id_proforma
            INTO  v_id_cliente; 


            SELECT saldo
            FROM  estado_cuenta
            WHERE persona_id = v_id_cliente
            ORDER BY fecha DESC limit 1
            INTO  v_saldo;

            SELECT id_moneda_venta
            FROM  proformas
            WHERE id = p_id_proforma
            INTO  v_id_currency; 



    --bruto
        IF v_tipo_facturacion = 1 THEN

            SELECT total_bruto_facturar
            FROM proformas
            WHERE id = p_id_proforma
            INTO v_monto_total_proforma;


            END IF;
             --bruto

        
        --neto
        IF v_tipo_facturacion = 2 THEN

            SELECT total_neto_facturar
            FROM proformas
            WHERE id = p_id_proforma
            INTO v_monto_total_proforma;


            END IF;
            --neto


    --SI ES IGUAL A USD 
    IF v_id_currency = 143 THEN
     

            IF v_monto_total_proforma > v_saldo THEN

             v_resp = 'El monto de la proforma supera el saldo disponible';

            END IF;

    ELSE
    --SI ES GUARANIES 

                    SELECT * 
                    FROM get_cotizacion(143)
                    INTO v_cotizacion_actual;

            --SI COTIZACION ES DIFERENTE A CERO ENTONCES ES VALIDO
            IF v_cotizacion_actual != 0 THEN

             v_total_calc = v_monto_total_proforma / v_cotizacion_actual;
            
                     IF v_total_calc > v_saldo THEN

                     v_resp = 'El monto de la proforma supera el saldo disponible';
                     END IF;

             ELSE 

             v_resp = 'No tiene cotización actualizada no se puede verificar el saldo';

            END IF;


    END IF;
    
    
    
    END IF;
    -- END IF VALIDAR SI ES FACTURA CREDITO 


    
  RETURN v_resp;
   exception
    when OTHERS then
      raise exception 'control_saldo: %', SQLERRM;
  end;

END;$BODY$;

ALTER FUNCTION public.control_saldo(integer)
    OWNER TO postgres;









































CREATE TRIGGER insert_into_tickets   
ON grupos 
AFTER INSERT
AS

INSERT INTO tickets(fecha_emision, id_proveedor, facial, id_grupo, id_currency_costo, id_proforma, proforma_padre,
 fecha_pago_bsp,
usuario_amadeus, pasajero, pnr, iva, id_empresa, paquete, usuario_factour, tasas, tasas_yq, id_factura, 
comision, iva_comision, tipo_ticket,
fee, id_estado, id_tipo_ticket, fecha_alta, id_usuario, id_detalle_proforma, numero_amadeus)
VALUES (NEW.fecha_emision, NEW.proveedor_id, NEW.importe_facial, NEW.id, NEW.currency_compra_id, NULL, 
	NULL, NULL, NULL, NULL, NEW.pnr, NEW.importe_iva, NEW.empresa_id, NULL, NULL, NEW.importe_tasas, NEW.tasa_YQ, 
	NULL, NEW.comision, NULL, 'tktt', NULL, NEW.estado_id, NEW.tipo_ticket_id, NOW(), NULL, NULL, NULL);


-- TRIGGER GRUPOS
CREATE FUNCTION public.insert_into_tickets()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS $BODY$

DECLARE

cantidad_total INTEGER := 0; 
i INTEGER := 1; 

 

BEGIN

 IF(TG_OP = 'INSERT') THEN
 	SELECT (NEW.cantidad + NEW.cantidad_liberados) INTO cantidad_total;
 	
 	FOR i IN 1..cantidad_total LOOP 
 		IF()

 		END IF;
	END LOOP ; 
				
    RETURN NEW;
   
 END IF;
END;

 

$BODY$;

ALTER FUNCTION public.log_comentarios_proforma()
    OWNER TO postgres;











CREATE OR REPLACE FUNCTION public.insert_into_tickets(
    fecha_emision date,
    id_proveedor integer,
    facial numeric,
    id_grupo integer,
    id_currency_costo integer,
    id_proforma integer,
    proforma_padre integer,
    fecha_pago date,
    usuario_amadeus varchar,
    pasajero varchar,
    pnr varchar,
    iva numeric,
    id_empresa integer,
    usuario_factour integer,
    tasas numeric,
    tasas_yq numeric,
    id_factura integer,
    comision numeric,
    iva_comision numeric,
    tipo_ticket varchar,
    fee numeric,
    id_estado integer,
    id_tipo_ticket integer,
    fecha_alta date,
    id_usuario integer,
    id_detalle_proforma integer,
    nro_amadeus varchar
)
    
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$DECLARE
    

BEGIN
  begin

    INSERT INTO tickets(
        fecha_emision,
        id_proveedor,
        facial,
        id_grupo,
        id_currency_costo,
        id_proforma,
        proforma_padre,
        fecha_pago_bsp,
        usuario_amadeus,
        pasajero,
        pnr,
        iva,
        id_empresa,
        usuario_factour,
        tasas,
        tasas_yq,
        id_factura,
        iva_comision,
        tipo_ticket,
        fee,
        id_estado,
        id_tipo_ticket,
        fecha_alta,
        id_usuario,
        id_detalle_proforma,
        numero_amadeus
    )
   VALUES (
        fecha_emision,
        id_proveedor, 
        facial, 
        id_grupo,
        id_currency_costo,
        id_proforma,
        proforma_padre,
        fecha_pago,
        usuario_amadeus,
        pasajero,
        pnr,
        iva,
        id_empresa,
        usuario_factour,
        tasas,
        tasas_yq,
        id_factura,
        iva_comision, 
        tipo_ticket,
        fee,
        id_estado,
        id_tipo_ticket,
        fecha_alta,
        id_usuario,
        id_detalle_proforma,
        nro_amadeus 
    )

    
  RETURN void;
  exception
    when OTHERS then
      raise exception 'insert_into_tickets: %', SQLERRM;
  end;
END;$BODY$;

ALTER FUNCTION public.insert_into_tickets()
    OWNER TO postgres;










CREATE FUNCTION public.insert_into_tickets(
    fecha_emision date,
    id_proveedor integer,
    facial numeric,
    id_grupo integer,
    id_currency_costo integer,
    id_proforma integer,
    proforma_padre integer,
    fecha_pago date,
    usuario_amadeus varchar,
    pasajero varchar,
    pnr varchar,
    iva numeric,
    id_empresa integer,
    usuario_factour integer,
    tasas numeric,
    tasas_yq numeric,
    id_factura integer,
    comision numeric,
    iva_comision numeric,
    tipo_ticket varchar,
    fee numeric,
    id_estado integer,
    id_tipo_ticket integer,
    fecha_alta date,
    id_usuario integer,
    id_detalle_proforma integer,
    nro_amadeus varchar
)
    RETURNS VOID
    AS $$


    INSERT INTO tickets(
        fecha_emision,
        id_proveedor,
        facial,
        id_grupo,
        id_currency_costo,
        id_proforma,
        proforma_padre,
        fecha_pago_bsp,
        usuario_amadeus,
        pasajero,
        pnr,
        iva,
        id_empresa,
        usuario_factour,
        tasas,
        tasas_yq,
        id_factura,
        comision,
        iva_comision,
        tipo_ticket,
        fee,
        id_estado,
        id_tipo_ticket,
        fecha_alta,
        id_usuario,
        id_detalle_proforma,
        numero_amadeus
    )
   VALUES (
        fecha_emision,
        id_proveedor, 
        facial, 
        id_grupo,
        id_currency_costo,
        id_proforma,
        proforma_padre,
        fecha_pago,
        usuario_amadeus,
        pasajero,
        pnr,
        iva,
        id_empresa,
        usuario_factour,
        tasas,
        tasas_yq,
        id_factura,
        comision,
        iva_comision, 
        tipo_ticket,
        fee,
        id_estado,
        id_tipo_ticket,
        fecha_alta,
        id_usuario,
        id_detalle_proforma,
        nro_amadeus 
    )




    $$
    LANGUAGE SQL






SELECT insert_into_tickets(
        '2019-01-01',
       1, 
        100, 
        1,
        143,
        null,
        null,
        get_fecha_bsp('2019-01-01'),
        null,
        null,
        JHUYTRB,
        100,
        1,
        null,
        450,
        200,
        null,
        null, 
        'TKTT',
        null,
        1,
        3,
        '2019-02-07',
        null,
        null,
        null 
)




INSERT INTO public.masterbm_hotel_amenidad (presente, id, amenidad_code, hotel_id) VALUES
 (true, 2686003, 2, 3518044)

ON CONFLICT (id) DO UPDATE SET presente = EXCLUDED.presente, amenidad_code = EXCLUDED.amenidad_code, hotel_id = EXCLUDED.hotel_id;


INSERT INTO masterbm_tipo_hotel( id, name) VALUES (9, 'Condominios') ON CONFLICT (id) DO UPDATE SET name = 'TEST';






CREATE OR REPLACE FUNCTION public.insert_into_tickets(
    fecha_emision date,
    id_proveedor integer,
    facial numeric,
    id_grupo integer,
    id_currency_costo integer,
    id_proforma integer,
    proforma_padre integer,
    fecha_pago date,
    usuario_amadeus character varying,
    pasajero character varying,
    pnr character varying,
    iva numeric,
    id_empresa integer,
    usuario_factour integer,
    tasas numeric,
    tasas_yq numeric,
    id_factura integer,
    comision numeric,
    iva_comision numeric,
    tipo_ticket character varying,
    fee numeric,
    id_estado integer,
    id_tipo_ticket integer,
    fecha_alta date,
    id_usuario integer,
    id_detalle_proforma integer,
    nro_amadeus character varying)
    RETURNS void
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
AS $BODY$

    INSERT INTO tickets(
        fecha_emision,
        id_proveedor,
        facial,
        id_grupo,
        id_currency_costo,
        id_proforma,
        proforma_padre,
        fecha_pago_bsp,
        usuario_amadeus,
        pasajero,
        pnr,
        iva,
        id_empresa,
        usuario_factour,
        tasas,
        tasas_yq,
        id_factura,
        comision,
        iva_comision,
        tipo_ticket,
        fee,
        id_estado,
        id_tipo_ticket,
        fecha_alta,
        id_usuario,
        id_detalle_proforma,
        numero_amadeus
    )
   VALUES (
        fecha_emision,
        id_proveedor, 
        facial, 
        id_grupo,
        id_currency_costo,
        id_proforma,
        proforma_padre,
        fecha_pago,
        usuario_amadeus,
        pasajero,
        pnr,
        iva,
        id_empresa,
        usuario_factour,
        tasas,
        tasas_yq,
        id_factura,
        comision,
        iva_comision, 
        tipo_ticket,
        fee,
        id_estado,
        id_tipo_ticket,
        fecha_alta,
        id_usuario,
        id_detalle_proforma,
        nro_amadeus 
    )

ON CONFLICT (numero_amadeus) DO UPDATE SET tasas = EXCLUDED.tasas, tasas_yq = EXCLUDED.tasas_yq;
    $BODY$;




ALTER FUNCTION public.insert_into_tickets(date, integer, numeric, integer, integer, integer, integer, date, character varying, character varying, character varying, numeric, integer, integer, numeric, numeric, integer, numeric, numeric, character varying, numeric, integer, integer, date, integer, integer, character varying)
    OWNER TO postgres;





Realizar una función get_usuario_amadeus que reciba 
como parámetro el id de la variable de sesión y retorne el usuario amadeus.


CREATE OR REPLACE FUNCTION get_usuario_amadeus(p_id_variable_sesion integer)
    RETURNS varchar
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$DECLARE
    r_usuario_amadeus varchar;

BEGIN
  begin

    SELECT usuario_amadeus
    FROM   personas
    WHERE  id = p_id_variable_sesion
    INTO   r_usuario_amadeus;

    
  RETURN r_usuario_amadeus;
  exception
    when OTHERS then
      raise exception 'get_usuario_amadeus: %', SQLERRM;
  end;
END;$BODY$;

ALTER FUNCTION public.get_usuario_amadeus(integer)
    OWNER TO postgres;


--REGISTRAR MOVIMIENTO

CREATE OR REPLACE FUNCTION registrar_movimiento(
    documento varchar,
    importe numeric,
    id_currency integer,
    id_persona integer,
    id_tipo_documento integer
    )
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

    DECLARE
        v_tipo_operacion char;

    BEGIN
        SELECT * INTO v_tipo_operacion FROM get_tipo_operacion(id_tipo_documento);
        
        IF v_tipo_operacion = '+' THEN

            INSERT INTO estado_cuenta(
                fecha,
                documento,
                saldo,
                debe,
                haber,
                currency_id,
                persona_id,
                tipo_documento_id
            )
                VALUES (
                now(),
                documento,
                0,
                importe,
                0,
                id_currency,
                id_persona,
                id_tipo_documento
            );
        ELSIF v_tipo_operacion = '-' THEN
            INSERT INTO estado_cuenta(
                fecha,
                documento,
                saldo,
                debe,
                haber,
                currency_id,
                persona_id,
                tipo_documento_id
            )
                VALUES (
                now(),
                documento,
                0,
                0,
                importe,
                id_currency,
                id_persona,
                id_tipo_documento
            );
        END IF;
    END;
    $BODY$;

ALTER FUNCTION public.registrar_movimiento(varchar, numeric, integer, integer, integer)
    OWNER TO postgres;

select * from registrar_movimiento('2019-02-12', 'Documento prueba', 0, 1000, 0, 143, );


CREATE OR REPLACE FUNCTION get_tipo_operacion(p_id_tipo_documento integer)
    RETURNS char
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 

AS $BODY$

    DECLARE
        v_tipo_operacion char;
    BEGIN
        SELECT tipo_operacion
        INTO v_tipo_operacion
        FROM tipo_documento_ctacte
        WHERE id = p_id_tipo_documento;
        RETURN v_tipo_operacion;
    END;
    
$BODY$;




ALTER FUNCTION public.get_tipo_operacion(integer)
    OWNER TO postgres;




CREATE FUNCTION public.actualizar_estado()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS $BODY$
BEGIN

 IF(TG_OP = 'INSERT') THEN 


                INSERT INTO historico_comentarios_proforma(id_proforma,comentario,fecha_hora, id_usuario)
                VALUES(NEW.id,NEW.comentario,now(),NEW.id_usuario);
        RETURN NEW;
    ELSIF(TG_OP = 'UPDATE') THEN 
            IF NEW.comentario <> OLD.comentario  AND NEW.comentario  <> '' THEN
                INSERT INTO historico_comentarios_proforma(id_proforma,comentario,fecha_hora, id_usuario)
                VALUES(NEW.id,NEW.comentario,now(),NEW.id_usuario);
            END IF;
        RETURN NEW;
    END IF;
END;

 

$BODY$;

ALTER FUNCTION public.log_comentarios_proforma()
    OWNER TO postgres;




CREATE OR REPLACE FUNCTION log_actualizar_saldo()
    RETURNS trigger 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 

AS $BODY$
DECLARE
    v_tipo_operacion CHAR;

BEGIN
    SELECT tipo_operacion
    INTO v_tipo_operacion
    FROM tipo_documento_ctacte
    WHERE id = NEW.tipo_documento_id;
    
    IF v_tipo_operacion = '+' THEN
        UPDATE estado_cuenta
        SET saldo = saldo + NEW.debe
        WHERE id = NEW.id;
    ELSE
        UPDATE estado_cuenta
        SET saldo = saldo - NEW.haber
        WHERE id = NEW.id;
    END IF;
 
    RETURN NEW;
END;
$BODY$;


CREATE TRIGGER actualizar_saldo
  AFTER INSERT
  ON estado_cuenta
  FOR EACH ROW
  EXECUTE PROCEDURE log_actualizar_saldo();


select * from registrar_movimiento('prueba', 3000, 143, 422, 1)
--CONTROLAR SALDO
CREATE OR REPLACE FUNCTION controlar_saldo(p_id_persona integer, p_monto numeric, p_moneda integer)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 

AS $BODY$
    DECLARE
        v_monto_credito numeric;
        v_saldo numeric;
        v_saldo_real numeric;

    BEGIN
        SELECT monto 
        INTO v_monto_credito
        FROM linea_de_credito
        WHERE id_persona = p_id_persona;

        SELECT saldo
        INTO v_saldo
        FROM estado_cuenta
        WHERE persona_id = p_id_persona;

        v_saldo_real = v_monto_credito - v_saldo;

        IF p_monto <= v_saldo_real THEN
            v_saldo_real = v_saldo_real - p_monto;
        ELSE
            RAISE EXCEPTION 'Saldo insuficiente';
        END IF;

    END;

    $BODY$;

ALTER FUNCTION public.controlar_saldo(integer, numeric, integer)
    OWNER TO postgres;




CREATE OR REPLACE FUNCTION get_tipo_documento(
    documento varchar,
    importe numeric,
    id_currency integer,
    id_persona integer,
    id_tipo_documento integer
    )
    RETURNS void
    LANGUAGE 'sql'

    COST 100
    VOLATILE 
AS $BODY$

    INSERT INTO estado_cuenta(
        fecha,
        documento,
        saldo,
        debe,
        haber,
        currency_id,
        persona_id,
        tipo_documento_id
    )
   VALUES (
        now(),
        documento,
        0,
        debe,
        haber,
        id_currency,
        id_persona,
        id_tipo_documento
    )
    $BODY$;

ALTER FUNCTION public.registrar_movimiento(date, integer, numeric, numeric, numeric, integer, integer, integer)
    OWNER TO postgres;



       ELSIF v_tipo_operacion = '-' THEN





CREATE OR REPLACE FUNCTION public.controlar_saldo(
    p_id_persona integer,
    p_monto numeric,
    p_moneda integer)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
    DECLARE
        v_monto_credito numeric;
        v_saldo numeric;
        v_saldo_real numeric;

    BEGIN
        SELECT monto 
        INTO v_monto_credito
        FROM linea_de_credito
        WHERE id_persona = p_id_persona;

        SELECT saldo
        INTO v_saldo
        FROM estado_cuenta
        WHERE persona_id = p_id_persona;

        v_saldo_real = v_saldo - v_monto_credito;

        IF p_monto <= v_saldo_real THEN
            RAISE NOTICE 'OK. Se puede procesar';
        ELSE
            RAISE EXCEPTION 'Saldo insuficiente';
        END IF;

    END;

    $BODY$;

ALTER FUNCTION public.controlar_saldo(integer, numeric, integer)
    OWNER TO postgres;
select persona_id, saldo from estado_cuenta group by persona_id, saldo, fecha order by persona_id, fecha desc 



select c.persona_id, c.saldo, l.monto, c.fecha from estado_cuenta c, linea_de_credito l 
where c.persona_id = l.id_persona and l.activo = true
group by  c.persona_id, c.saldo, l.monto, c.fecha order by c.persona_id, c.fecha desc 

SELECT p.nombre, c.persona_id, c.saldo, l.monto, c.fecha 
                                FROM estado_cuenta c, linea_de_credito l, personas p 
                                WHERE c.persona_id = l.id_persona AND l.activo = true AND p.id = c.persona_id AND p.id_tipo_persona = 8
                                GROUP BY  p.nombre, c.persona_id, c.saldo, l.monto, c.fecha ORDER BY c.persona_id, c.fecha DESC


-- Fecha de Vencimiento:





-- En caso que tenga un solo item con fecha de gasto, menor a los días del punto 1. 
    -- La fecha de vencimiento de la factura es la fecha de gastos. Si tiene mas de uno va a seña.



-- Para combinaciones de servicios, se debe analizar si la suma de los servicios con vencimientos 
-- cercanos (tickets, otros y casos de gastos) superan el 85%, de ser así la fecha de 
-- vencimiento debe ser la mas cercana a HOY.

-- El vendedor solo pueden cambiar la fecha de vencimiento de la proforma/factura antes.

-- Los casos donde existen varios servicios, y entre ellos trenes/ticket/crucero estos deben ir como SEÑA







-- Check-In / Check-Out: Utilizando un trigger sobre detalles de la proforma 
-- actualizar las fechas de la cabecera. El Check-In de la proforma debe 
-- ser el menor Check-In de los items. El Check-Out de la proforma debe ser el mayor Check-Out de los items.

CREATE OR REPLACE FUNCTION log_actualizar_check_in_check_out()
    RETURNS trigger 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 

AS $BODY$
DECLARE
    v_fecha_max date;
    v_fecha_min date;

BEGIN
    SELECT pr.fecha_in
    INTO v_fecha_min
    FROM proformas p
    JOIN proformas_detalle pr ON p.id = pr.id_proforma
    WHERE p.id = NEW.pr.id_proforma
    ORDER BY pr.fecha_in ASC limit 1;

    SELECT pr.fecha_out
    INTO v_fecha_max
    FROM proformas p
    JOIN proformas_detalle pr ON p.id = pr.id_proforma
    WHERE p.id = NEW.pr.id_proforma
    ORDER BY pr.fecha_in DESC limit 1;

    UPDATE proformas
    SET check_in = v_fecha_min, check_out = v_fecha_max
    WHERE id = NEW.id_proforma;

    RETURN NEW;
END;
$BODY$;


CREATE TRIGGER actualizar_check_in_check_out
AFTER INSERT OR UPDATE
ON proformas_detalle
FOR EACH ROW
EXECUTE PROCEDURE log_actualizar_check_in_check_out();



--FECHA VENCIMIENTO
CREATE OR REPLACE FUNCTION log_actualizar_vencimiento() --ver caso de negociacion con agencias
    RETURNS trigger 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 

AS $BODY$
DECLARE
    v_parametro integer;
    v_valor numeric;
    v_valor_memo numeric;
    v_fecha_pago date;
    v_check_in date;


BEGIN
    SELECT p.parametro_id
    INTO v_parametro
    FROM proformas_detalle pd
    JOIN productos p on pd.id_producto = p.id
    WHERE pd.id_proforma = NEW.id_proforma;

    SELECT pa.valor
    INTO v_valor_memo
    JOIN parametros
    WHERE id = 5;

    SELECT pa.valor
    INTO v_valor
    JOIN parametros
    WHERE id = 4;

    SELECT fecha_pago_proveedor
    INTO v_fecha_pago
    FROM proformas_detalle 
    WHERE id_proforma = NEW.id_proforma;

    SELECT fecha_in
    INTO v_check_in
    FROM proformas_detalle
    WHERE id_proforma = NEW.id_proforma;

    -- En caso que exista un item con memo de pago validar que la fecha de vencimiento 
    --de la factura sea 2 días antes (configurable) a la fecha del memo de pago.
    IF v_fecha_pago IS NOT NULL THEN
        UPDATE proformas
        SET vencimiento = (date v_fecha_pago - (v_valor_memo * interval '1 day'))
        WHERE id = NEW.id_proforma;

    -- En caso que la factura tenga solo ticket/tren/crucero, la fecha de vencimiento es HOY.
    ELSE

    
    ELSEIF

    -- El vencimiento de la factura debe ser 20 días (configurable) antes del check-in. 
    --v_fecha_vencimiento = (v_check_in - interval '20 days'); --20 días antes del check in
    ELSE
        UPDATE proformas
        SET vencimiento = (date v_check_in - (v_valor * interval '1 day'))
        WHERE id = NEW.id_proforma;

    

    END IF;



   
    RETURN NEW;
END;
$BODY$;


CREATE TRIGGER actualizar_vencimiento 
AFTER INSERT OR UPDATE
ON proformas
FOR EACH ROW
EXECUTE PROCEDURE log_actualizar_vencimiento();













--Seña: El valor de la seña de la cabecera debe ser la suma de todos los items de la proforma
--cuyo producto sea: ticket, tren y crucero. Este campo de la cabecera debe actualizarse con 
--un trigger en el detalle. Teniendo en cuenta que el vendedor puede cambiar el valor de 
--la seña (hacia arriba), tenemos que agregar un trigger en la cabecera para validar que 
--la seña cargada no sea menor a la suma de los items ticket, tren y crucero.


CREATE OR REPLACE FUNCTION log_validar_senha()
    RETURNS trigger 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 

AS $BODY$
DECLARE
    v_suma double precision;


BEGIN
    SELECT sum(OLD.pd.precio_venta)
    INTO v_suma
    FROM productos p
    JOIN proformas_detalle pd ON pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE (p.parametro_id = 1 OR p.parametro_id = 2 OR p.parametro_id = 3) AND pr.id = OLD.id;

    IF NEW.senha < v_suma THEN
        RAISE EXCEPTION 'No se puede actualizar. El valor es menor a la seña % %', NEW.senha, v_suma;
       -- raise notice '% %', SQLCODE, SQLERRM;
    END IF;

    RETURN NEW;
END;
$BODY$;


CREATE TRIGGER validar_senha
AFTER INSERT OR UPDATE
ON proformas
FOR EACH ROW
EXECUTE PROCEDURE log_validar_senha();




--ACTUALIZAR SEÑA
CREATE OR REPLACE FUNCTION log_actualizar_senha()
    RETURNS trigger 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 

AS $BODY$
DECLARE
    v_suma double precision;
    v_parametro integer;



BEGIN

    SELECT p.parametro_id
    INTO v_parametro
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    WHERE pd.id_proforma = NEW.id_proforma;


    IF v_parametro IN (select * from get_parametro_ticket()) THEN
        SELECT sum(pd.precio_venta)
        INTO v_suma
        FROM productos p
        JOIN proformas_detalle pd on pd.id_producto = p.id
        WHERE pd.id_proforma = NEW.id_proforma AND pd.id = NEW.id
        AND p.parametro_id in (select * from get_parametro_ticket());

        UPDATE proformas
        SET senha = senha + v_suma
        WHERE id = NEW.id_proforma;
    END IF;


    RETURN NEW;
END;
$BODY$;


CREATE TRIGGER actualizar_senha
AFTER INSERT OR UPDATE
ON proformas_detalle
FOR EACH ROW
EXECUTE PROCEDURE log_actualizar_senha();









CREATE OR REPLACE FUNCTION get_parametro_ticket(OUT out_parametro_id integer)
    RETURNS SETOF integer 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 

AS $BODY$
    DECLARE
        r_id_parametro RECORD;

    BEGIN   
    FOR r_id_parametro IN 
        SELECT id
        FROM parametros 
        WHERE id IN (1,2,3)
    LOOP
       out_parametro_id  = r_id_parametro.id;
       RETURN NEXT;
    END LOOP;
    
    RETURN;

   END;

    $BODY$;

ALTER FUNCTION public.get_parametro_ticket()
    OWNER TO postgres;




--VALIDAR SEÑA
CREATE OR REPLACE FUNCTION log_validar_senha()
    RETURNS trigger 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 

AS $BODY$
DECLARE
    v_suma double precision;
    v_parametro integer;



BEGIN
    SELECT sum(pd.precio_venta)
    INTO v_suma
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE pr.id = NEW.id 
    AND p.parametro_id in (select * from get_parametro_ticket());
   

    IF NEW.senha < v_suma  THEN
        RAISE EXCEPTION 'No se puede actualizar';
    END IF;





    RETURN NEW;
END;
$BODY$;


CREATE TRIGGER validar_senha
AFTER UPDATE
ON proformas
FOR EACH ROW
EXECUTE PROCEDURE log_validar_senha();
















            --RAISE EXCEPTION '% %', v_senha, v_suma;           
         UPDATE proformas
         SET senha =  v_suma
         WHERE id = new.id;












         -- FUNCTION: public.log_validar_senha()

-- DROP FUNCTION public.log_validar_senha();

CREATE OR REPLACE FUNCTION public.log_validar_senha()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS $BODY$
DECLARE
    v_suma double precision;
    v_senha double precision;
    v_parametro integer;

BEGIN                      
    SELECT senha
    INTO v_senha
    FROM proformas 
    WHERE id = NEW.id;
    
    
--  IF v_senha <> '0' THEN

        SELECT COALESCE (sum(pd.precio_venta),  0)
        INTO v_suma
        FROM productos p
        JOIN proformas_detalle pd on pd.id_producto = p.id
        JOIN proformas pr on pr.id = pd.id_proforma
        WHERE pr.id = NEW.id 
        AND p.parametro_id in (select * from get_parametro_ticket());
        
        IF v_senha < v_suma THEN
            RAISE EXCEPTION 'No se puede actualizar % %', v_senha, v_suma;      
        END IF;
                               
--  END IF;

    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.log_validar_senha()
    OWNER TO postgres;





-- El vencimiento de la factura debe ser 20 días (configurable) antes del check-in.

-- En caso que exista un item con memo de pago, validar que la fecha de vencimiento de 
--la factura sea 2 días antes (configurable) a la fecha del memo de pago.

-- En caso que la factura tenga solo ticket/tren/crucero, la fecha de vencimiento es HOY.

-- En caso que tenga un solo item con fecha de gasto, menor a los días del punto 1. 
--La fecha de vencimiento de la factura es la fecha de gastos. Si tiene mas de uno va a seña. (A ANALIZAR)

-- Para combinaciones de servicios, se debe analizar si la suma de los servicios con vencimientos cercanos (tickets, otros y casos de gastos) superan el 85%, de ser así la fecha de vencimiento debe ser la mas cercana a HOY.

-- El vendedor solo pueden cambiar la fecha de vencimiento de la proforma/factura antes.

-- Los casos donde existen varios servicios, y entre ellos trenes/ticket/crucero estos deben ir como SEÑA.


CREATE OR REPLACE FUNCTION log_actualizar_vencimiento()
    RETURNS trigger 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 

AS $BODY$
DECLARE
    
    -- cursor_detalles CURSOR FOR 
    --     SELECT *
    --     FROM productos p
    --     JOIN proformas_detalle pd on pd.id_producto = p.id
    --     JOIN proformas pr on pr.id = pd.id_proforma
    --     WHERE id_proforma = NEW.id_proforma;

    -- registro_detalle proformas_detalle%ROWTYPE;
    v_check_in date;
    v_fecha_pago date;
    v_fecha_proxima date;
    v_valor integer;
    v_rows integer;
    v_cant_tickets integer;
    v_suma double precision;
    v_suma_tickets double precision;
    v_total double precision;

BEGIN
-------------------------------------------------------------------------------------------------
    --PUNTO A PONER EN EL ELSE

    -- SELECT fecha_in
    -- INTO v_check_in
    -- FROM proformas_detalle
    -- WHERE id_proforma = NEW.id_proforma;

    -- SELECT * INTO v_valor FROM get_valor_parametro(4);

    -- UPDATE proformas
    -- SET vencimiento = (date v_check_in - (v_valor * interval '1 day'))
    -- WHERE id = NEW.id_proforma;


--------------------------------------------------------------------------------------------------
    --MEMO DE PAGO

    -- SELECT fecha_pago_proveedor
    -- INTO v_fecha_pago
    -- FROM proformas_detalle 
    -- WHERE id_proforma = NEW.id_proforma;

    -- SELECT * INTO v_valor_memo FROM get_valor_parametro(5);

    -- IF v_fecha_pago IS NOT NULL THEN
    --     UPDATE proformas
    --     SET vencimiento = (date v_fecha_pago - (v_valor_memo * interval '1 day'))
    --     WHERE id = NEW.id_proforma;
    -- END IF; 

--------------------------------------------------------------------------------------------------
    --PUNTO C
        -- SELECT count(*)
        -- INTO v_rows
        -- FROM productos p
        -- JOIN proformas_detalle pd on pd.id_producto = p.id
        -- JOIN proformas pr on pr.id = pd.id_proforma
        -- WHERE pr.id = NEW.id_proforma

        -- SELECT count(*)
        -- INTO v_cant_tickets
        -- FROM productos p
        -- JOIN proformas_detalle pd on pd.id_producto = p.id
        -- JOIN proformas pr on pr.id = pd.id_proforma
        -- WHERE pr.id = NEW.id_proforma
        -- AND p.parametro_id in (select * from get_parametro_ticket());

        -- IF v_rows = v_cant_tickets THEN
        --     UPDATE proformas
        --     SET vencimiento = now()
        --     WHERE id = NEW.id_proforma;
        -- END IF; 

----------------------------------------------------------------------------------------------------

    --PUNTO E
        -- SELECT COALESCE(sum(pd.precio_venta),0)
        -- INTO v_suma_tickets
        -- FROM productos p
        -- JOIN proformas_detalle pd on pd.id_producto = p.id
        -- JOIN proformas pr on pr.id = pd.id_proforma
        -- WHERE pr.id = NEW.id_proforma
        -- AND p.parametro_id IN (select * from get_parametro_ticket());

        -- SELECT COALESCE(sum(pd.precio_venta),0)
        -- INTO v_suma
        -- FROM productos p
        -- JOIN proformas_detalle pd on pd.id_producto = p.id
        -- JOIN proformas pr on pr.id = pd.id_proforma
        -- WHERE pr.id = NEW.id_proforma

        -- v_porcentaje = (v_suma / v_suma_tickets) * 100;

        -- IF v_porcentaje > 85 THEN

        --     SELECT pd.fecha_pago_proveedor
        --     INTO v_fecha_proxima
        --     FROM productos p
        --     JOIN proformas_detalle pd on pd.id_producto = p.id
        --     JOIN proformas pr on pr.id = pd.id_proforma
        --     WHERE pr.id = 90218
        --     AND p.parametro_id  IN (select * from get_parametro_ticket())
        --     AND pd.fecha_pago_proveedor IS NOT NULL
        --     ORDER BY pd.fecha_pago_proveedor ASC LIMIT 1;

        --     UPDATE proformas
        --     SET vencimiento = v_fecha_proxima
        --     WHERE id = NEW.id_proforma;
        -- END IF;


----------------------------------------------------------------------------------------------------
    SELECT fecha_pago_proveedor
    INTO v_fecha_pago
    FROM proformas_detalle 
    WHERE id_proforma = NEW.id_proforma;

    SELECT * INTO v_valor_memo FROM get_valor_parametro(5);

    SELECT count(*)
    INTO v_rows
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE pr.id = NEW.id_proforma

    SELECT count(*)
    INTO v_cant_tickets
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE pr.id = NEW.id_proforma
    AND p.parametro_id in (select * from get_parametro_ticket());

    SELECT fecha_in
    INTO v_check_in
    FROM proformas_detalle
    WHERE id_proforma = NEW.id_proforma;

    SELECT * INTO v_valor FROM get_valor_parametro(4);

    SELECT COALESCE(sum(pd.precio_venta),0)
    INTO v_suma_tickets
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE pr.id = NEW.id_proforma
    AND p.parametro_id IN (select * from get_parametro_ticket());

    SELECT COALESCE(sum(pd.precio_venta),0)
    INTO v_suma
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE pr.id = NEW.id_proforma

    v_porcentaje = (v_suma / v_suma_tickets) * 100;

CASE 
    WHEN v_fecha_pago IS NOT NULL THEN 

        UPDATE proformas
        SET vencimiento = (date v_fecha_pago - (v_valor_memo * interval '1 day'))
        WHERE id = NEW.id_proforma;
    
    WHEN v_rows = v_cant_tickets THEN 

        UPDATE proformas
        SET vencimiento = now()
        WHERE id = NEW.id_proforma;

    WHEN v_porcentaje > 85 THEN

        SELECT pd.fecha_pago_proveedor
        INTO v_fecha_proxima
        FROM productos p
        JOIN proformas_detalle pd on pd.id_producto = p.id
        JOIN proformas pr on pr.id = pd.id_proforma
        WHERE pr.id = 90218
        AND p.parametro_id  IN (select * from get_parametro_ticket())
        AND pd.fecha_pago_proveedor IS NOT NULL
        ORDER BY pd.fecha_pago_proveedor ASC LIMIT 1;

        UPDATE proformas
        SET vencimiento = v_fecha_proxima
        WHERE id = NEW.id_proforma;

    ELSE 
        UPDATE proformas
        SET vencimiento = (date v_check_in - (v_valor * interval '1 day'))
        WHERE id = NEW.id_proforma; 
END
    RETURN;
END;
$BODY$;


CREATE TRIGGER actualizar_vencimiento
AFTER INSERT OR UPDATE
ON proformas_detalle
FOR EACH ROW
EXECUTE PROCEDURE log_actualizar_vencimiento();





CREATE OR REPLACE FUNCTION log_actualizar_vencimiento()
    RETURNS trigger 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 

AS $BODY$
DECLARE
    v_check_in date;
    v_fecha_pago date;
    v_fecha_proxima date;
    v_valor integer;
    v_valor_memo integer;
    v_rows integer;
    v_cant_tickets integer;
    v_suma double precision;
    v_suma_tickets double precision;
    v_total double precision;
    v_porcentaje double precision;

BEGIN
    SELECT fecha_pago_proveedor
    INTO v_fecha_pago
    FROM proformas_detalle 
    WHERE id_proforma = NEW.id_proforma;

    SELECT * INTO v_valor_memo FROM get_valor_parametro(5);

    SELECT count(*)
    INTO v_rows
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE pr.id = NEW.id_proforma;

    SELECT count(*)
    INTO v_cant_tickets
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE pr.id = NEW.id_proforma
    AND p.parametro_id in (select * from get_parametro_ticket());

    SELECT fecha_in
    INTO v_check_in
    FROM proformas_detalle
    WHERE id_proforma = NEW.id_proforma;

    SELECT * INTO v_valor FROM get_valor_parametro(4);

    SELECT COALESCE(sum(pd.precio_venta),0)
    INTO v_suma_tickets
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE pr.id = NEW.id_proforma
    AND p.parametro_id IN (select * from get_parametro_ticket());

    SELECT COALESCE(sum(pd.precio_venta),0)
    INTO v_suma
    FROM productos p
    JOIN proformas_detalle pd on pd.id_producto = p.id
    JOIN proformas pr on pr.id = pd.id_proforma
    WHERE pr.id = NEW.id_proforma;

    v_porcentaje = (v_suma / v_suma_tickets) * 100;

    IF v_fecha_pago IS NOT NULL THEN 
        UPDATE proformas
        SET vencimiento = (v_fecha_pago - (v_valor_memo * interval '1 day'))
        WHERE id = NEW.id_proforma;

    ELSEIF v_rows = v_cant_tickets THEN 
        UPDATE proformas
        SET vencimiento = now()
        WHERE id = NEW.id_proforma;

    ELSEIF v_porcentaje > 85 THEN
        SELECT pd.fecha_pago_proveedor
        INTO v_fecha_proxima
        FROM productos p
        JOIN proformas_detalle pd on pd.id_producto = p.id
        JOIN proformas pr on pr.id = pd.id_proforma
        WHERE pr.id = 90218
        AND p.parametro_id  IN (select * from get_parametro_ticket())
        AND pd.fecha_pago_proveedor IS NOT NULL
        ORDER BY pd.fecha_pago_proveedor ASC LIMIT 1;

        UPDATE proformas
        SET vencimiento = v_fecha_proxima
        WHERE id = NEW.id_proforma;

    ELSE 
        UPDATE proformas
        SET vencimiento = (v_check_in - (v_valor * interval '1 day'))
        WHERE id = NEW.id_proforma;
END IF;
RETURN NEW;
END;
$BODY$;



            IF id_currency = 111 THEN
                UPDATE estado_cuenta
                SET importe_gs = importe,
                    debe = ROUND((importe / v_cotizacion)::numeric, 2)
                WHERE documento = p_documento;
            END IF;

            IF id_currency = 111 THEN
                UPDATE estado_cuenta
                SET importe_gs = importe,
                    haber = ROUND((importe / v_cotizacion)::numeric, 2)
                WHERE documento = p_documento;
            END IF;

        IF id_tipo_documento IN (1,3,5,6) THEN
            v_cotizacion = get_cotizacion_factura(p_documento);
        ELSE
            IF id_tipo_documento = 2 THEN
                --v_cotizacion = get_cotizacion_nc(p_documento);
            END IF;
        END IF;     
        
            