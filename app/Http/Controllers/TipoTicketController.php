<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\TipoTicket;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Message;
class TipoTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        	    //utilizamos el index para poder listar los parametros tienen los filtros de busqued por clave
			if($request->buscar){
				$tipoTickets = TipoTicket::where('descripcion','like', '%'.$request->buscar.'%' )
									->orderBy("id", "desc")
									->paginate(10);
				return view('pages.mc.tipoticket.index')->with('tipoTickets', $tipoTickets);
			}else{
				$tipoTickets = TipoTicket::orderBy("id", "desc")->paginate(10);
				return view('pages.mc.tipoticket.index')->with('tipoTickets', $tipoTickets);
			}

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas = Empresa::orderBy('denominacion', 'asc')->get(); // Obtener todas las empresas ordenadas por denominación
        
        return view('pages.mc.tipoticket.nuevo', compact('empresas'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//dd($request->all());
        $this->validate($request, [
            'descripcion' => 'required',
  
        ]);
        $tipoTicket = new TipoTicket();
        $tipoTicket->descripcion = $request->get('descripcion');
        $tipoTicket->id_empresa = $request->get('id_empresa');
        $tipoTicket->save();

    return redirect()->route('tipoticket.index')->with('message', 'El registro fue exitoso');
       


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoTicket = TipoTicket::findOrFail($id);
        $empresas = Empresa::orderBy('denominacion', 'asc')->get(); // Obtener todas las empresas ordenadas por denominación
        
        return view('pages.mc.tipoticket.editar', compact('empresas','tipoTicket'));
    
        //return view('pages.mc.tipoticket.editar', compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipoTicket = TipoTicket::findOrFail($id);
        $tipoTicket->descripcion = $request->get('descripcion');
        $tipoTicket->id_empresa = $request->get('id_empresa');
        $tipoTicket->update();
        return redirect()->route('tipoticket.index')->with('mensaje', 'El registro fue exitoso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
