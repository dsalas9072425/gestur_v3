<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asistencia;
use Session;
use App\Currency;
use App\Empresa;
use DB;
class asistenciaViajeroController extends Controller
{
    public function index(Request $req){
        return view('pages.mc.asistenciaViajero.index');
    }
    public function addAsistencia(){
        $monedas=Currency::all();
        $empresas = Empresa::all();
        return view('pages.mc.asistenciaViajero.addAsistencia')->with(['cambio'=>$monedas,'empresas'=>$empresas]);
    }
    public function verAsistencias(Request $req){
        if($req->filtro == ""){
            $datos=Asistencia::with('moneda')->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->orderBy('tarifa_asistencia_nombre', 'asc')->get();  
            foreach($datos as $i=>$dato) { 
                $data  ['data'][] =  [
                    'nombre'=>$dato['tarifa_asistencia_nombre'],
                    'precio'=>$dato['tarifa_asistencia_precio'],
                    'exenta'=>$dato['tarifa_asistencia_exenta'],
                    'grabada'=>$dato['tarifa_asistencia_grabada'],
                    'moneda'=>$dato['moneda']['currency_code'],
                    'iva'=>$dato['tarifa_asistencia_iva'],
                    'estado'=>$dato['activo'],
                    'id'=>$dato['id'],
                    ];
            }
        }else{
            $filtro=$req->filtro;
            $datos=Asistencia:: with('moneda')->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->where('tarifa_asistencia_nombre', '=', $filtro)
            ->orderBy('tarifa_asistencia_nombre', 'asc')->get();
            foreach($datos as $i=>$dato) { 
                $data  ['data'][] =  [
                    'nombre'=>$dato['tarifa_asistencia_nombre'],
                    'precio'=>$dato['tarifa_asistencia_precio'],
                    'exenta'=>$dato['tarifa_asistencia_exenta'],
                    'grabada'=>$dato['tarifa_asistencia_grabada'],
                    'moneda'=>$dato['moneda']['currency_code'],
                    'iva'=>$dato['tarifa_asistencia_iva'],
                    'estado'=>$dato['activo'],
                    'id'=>$dato['id'],
                    ];
            }  
        }
        return response()->json($data);
    }
    public function addSolicitudAsistencia(Request $req){
        ini_set('memory_limit', '-1');
		set_time_limit(300);
        //variables recibidas para cargar nueva asistencia
        $nombre=$req -> nombre;
        $tarifa=$req->tarifa;
        $exenta=$req->exenta;
        $grabada=$req->grabada;
        $iva=$req->iva;
        $moneda=$req->moneda;
        $idEmpresa=$req->empresa;
        //$idEmpresa=Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
        //generamos la consulta para cargar el servicio nuevo
	    $nuevaAsistencia = new Asistencia;
		$nuevaAsistencia->tarifa_asistencia_nombre = $nombre;
		$nuevaAsistencia->tarifa_asistencia_precio =  str_replace(',','.', str_replace('.','', $tarifa));
		$nuevaAsistencia->tarifa_asistencia_exenta =  str_replace(',','.', str_replace('.','', $exenta));
		$nuevaAsistencia->tarifa_asistencia_grabada = str_replace(',','.', str_replace('.','', $grabada));
		$nuevaAsistencia->tarifa_asistencia_iva = $iva;
		$nuevaAsistencia->id_empresa = $idEmpresa;
		$nuevaAsistencia->id_moneda = $moneda;
		$nuevaAsistencia->save();

        return response()->json(array('estado'=>1));

    }
    public function editarAsistencia($id){
        //Capturamos los datos para la mascara de editar datos.
        $datos = Asistencia::find($id);
        $monedas= Currency::all();
        $empresas = Empresa::all();
        return view('pages.mc.asistenciaViajero.editarAsistencia')->with(['datos'=>$datos,'cambio'=>$monedas,'empresas'=>$empresas]);
    }
    public function guardarEditarAsistencia(Request $req){
        //Capturamos los datos para la mascara de editar datos.
      //recibimos todos las datos del fomulario de edicio para procesar y guardar cambios en la BD
      $id=$req->id;
      $nombre= $req -> descripcion;
      $precio= str_replace(',','.', str_replace('.','', $req->precio));
      $exenta= str_replace(',','.', str_replace('.','', $req -> exenta));
      $grabada= str_replace(',','.', str_replace('.','', $req -> grabada));
      $iva = $req -> iva;
      $moneda = $req -> moneda;
      $estado = $req -> estado;
      $empresa = $req -> empresa;
      DB::table('tarifas_assiscard')
          ->where('id', $id)
          ->update(['tarifa_asistencia_nombre' => $nombre, 'tarifa_asistencia_precio' => $precio, 'tarifa_asistencia_exenta' => $exenta, 'tarifa_asistencia_grabada' => $grabada, 'tarifa_asistencia_iva' => $iva, 'id_moneda' => $moneda, 'id_empresa' =>$empresa, 'activo' => $estado ]);

      return response()->json(array('estado'=>1));
    }
}
 
