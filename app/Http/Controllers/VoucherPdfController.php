<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Session;
use Redirect;
use App\State;
use App\Reserva;
use App\Usuario;
use App\Agencias;
use App\Proforma;
use App\ProformasDetalle;
use App\Voucher;
use App\Persona;
use App\Empresa;
use App\Factura;
use App\Ciudad;
use DB;


class VoucherPdfController extends Controller
{

	


/**
 * Funcion que contiene la logica para generar los vouchers
 * @param  id proforma
 * @return array con vouchers para imprimir
 */
private function imprimirVoucher($id,$multipleVoucher = null){


		$idProforma = $id;
		//$idEmpresa = $this->getIdEmpresa();


		$empresa = Empresa::where('id',1)->first();
		$proforma = Proforma::with('cliente',
									'vendedor',
									'pasajero',
									'estado')->where('id',$idProforma)->get();

		$estadoFactura = Factura::where('id_proforma',$idProforma)->get(['id_estado_factura']);
		if($estadoFactura == '30'){
			die;
		}

		$proformasDetalle = ProformasDetalle::with('producto',
												   'proveedor',
												   'voucher',
												   'proforma')
		                     ->where('id_proforma',$idProforma)
		                     ->where('activo',true)
		                     ->get();

		 $ciudades = Ciudad::all();

		 $update = array(); //Guarda id para luego actualizar estado
		 $voucherArray = array(); //Guarda los voucher para imprimir
		 $cont= 0;
		 $titulo = "<b>ERROR PRODUCTO NO ENCONTRADO</b>";
		 $txtProducto = null;
		 $icono = null;
		 $product = null;
	

		
		
        foreach($proformasDetalle as $key=>$detalles){
      

			$servicio = '';
			$comentario = '';
			$nombre = '';
			$nombreArray = array();
			$ciudProveedor = '';
			$paisProveedor = '';
			$contadorVaucher = 0;
			$datosPrestador = '';
			$fechaConfirmacionReserva = '';

			//DATOS OBTENIDOS DEL DETALLE
			 $fechaConfirmacionReserva =  (!empty($detalles->created_at)) ? $detalles->created_at : '00-00-00 00:00:00';


					//OBTENER DATOS DEL PRESTADOR DETALLE PROFORMA
					 $datosPrestador = DB::select('	SELECT pre.nombre prestador_n,
					 								   pre.direccion,
					 								   pre.telefono,	
													   pre.id_pais,
													   p.cod_pais, 
													   p.name_es pais,
													   c.denominacion ciudad
													   from personas pre
													   LEFT JOIN paises p ON p.cod_pais = pre.id_pais
													   LEFT JOIN ciudades c ON c.id_pais = p.cod_pais
													   WHERE pre.id = '.$detalles->id_prestador.' limit 1');


	

            if($detalles->producto['genera_voucher'] == '1'){


                /*==================================
               			 GENERA MULTIPLES VOUCHER
                ==================================*/
            
                if($multipleVoucher != null){

                	$optionMultiple = $multipleVoucher;
                	
                	if($detalles->producto->multiple_voucher == 'true'){
                		$optionMultiple = $detalles->producto->multiple_voucher;
                	}

                } else {
                		$optionMultiple = $detalles->producto->multiple_voucher;
                }

                    if($optionMultiple == 'true'){

                    $idVouchersArray = array();

				 		
					if(count($detalles->voucher) > 0){

		

			foreach ($detalles->voucher as $key => $voucherM) {
				if($voucherM->activo == '1'){

					 		if($voucherM->id_prestador != 0){

					 //OBTENER DATOS DEL PRESTADOR VOUCHER
					 $datosPrestador = DB::select('	SELECT pre.nombre prestador_n,
					 								   pre.direccion,
					 								   pre.telefono,	
													   pre.id_pais,
													   p.cod_pais, 
													   p.name_es pais,
													   c.denominacion ciudad
													   from personas pre
													   LEFT JOIN paises p ON p.cod_pais = pre.id_pais
													     LEFT JOIN ciudades c ON c.id = pre.id_ciudad
													   WHERE pre.id = '.$voucherM->id_prestador.' limit 1');

					 		}//if
					 		


							 	if($voucherM->producto_id != null){
								 	$product = DB::select('SELECT descrip_voucher,icono, id_grupos_producto 
												 		   FROM productos 
												 		   WHERE id = '.$voucherM->producto_id);	
								 	$txtProducto = $product[0]->descrip_voucher;
								 	$icono = $product[0]->icono;

							 	} else {
							 		$txtProducto = null;
							 		$icono = null;
							 	}		
							 
							 	if($txtProducto != null){
								 	$explode = explode('_', $txtProducto);	
								 	$titulo = $explode[0];
									$subTituloUno = $explode[1];
									$subTituloDos = $explode[2];	
							 	} else {
								 	$titulo = "<b>ERROR PRODUCTO NO ENCONTRADO</b>";
									$subTituloUno = ' - <b>Bono</b> - <b>ERROR</b> ';
									$subTituloDos = ' - <b>Voucher</b> - <b>ERROR</b> ';

							 	}

							 	//Guardar id para cambiar estado de voucher
							 	$update[] = $voucherM->id;
							 	$voucherDetalles = new \StdClass;

							 	//GUARDAR EL GRUPO PRODUCTO
							 	$voucherDetalles->grupo_producto = 	$product[0]->id_grupos_producto;
								
								//GUARDAR EL ID DEL PRODUCTO
								$voucherDetalles->producto_id = $detalles->producto->id;
	
							 	//LOGO
							 	$voucherDetalles->logoEmpresa = $empresa->logo;
							 	$voucherDetalles->logoAgencia = $proforma[0]->cliente['logo'];

							 	//ICONO
							 	$voucherDetalles->icono = $icono;


							 	//Titulo de cabecera	
							 	 $voucherDetalles->titulo = $titulo;
							 	 $voucherDetalles->subTituloUno = $subTituloUno;
							 	 $voucherDetalles->subTituloDos = $subTituloDos;

							 	 //Datos del pasajero - Cabecera
							 	$fechaConfirmacionReserva = explode(' ',$fechaConfirmacionReserva);
							 	$fechaConfirmacionReserva = trim($fechaConfirmacionReserva[0]);

							 	$voucherDetalles->nombre = $voucherM->pasajero_principal;
								$voucherDetalles->fechaConfirmacionReserva =  $this->formatoFechaSalida($fechaConfirmacionReserva);
								$voucherDetalles->id_proforma = $detalles->id_proforma;



								//convertir fecha hora
								$fechaIn = explode(' ',$voucherM->fecha_in);
								$fechaIn = trim($fechaIn[0]);
								$fechaOut = explode(' ',$voucherM->fecha_out);
								$fechaOut = trim($fechaOut[0]);
								$fechaGenerado = explode(' ',$voucherM->fecha);
								$fechaGenerado = trim($fechaGenerado[0]);
						
								//DESDE HASTA
								$voucherDetalles->desde = $this->formatoFechaSalida($fechaIn);
								$voucherDetalles->hasta = $this->formatoFechaSalida($fechaOut);

								//FECHA GENERADO
								$voucherDetalles->generado = $this->formatoFechaSalida($fechaGenerado);


								$noches  = ($voucherM->noches != null) ? $voucherM->noches : '0';

								//SERVICIOS DETALLES
								$servicio .= '<tr class="padding-td border-bottom-red-td" style="font-size: 12px;">';
								$servicio .= "<td>1x</td>"; 
								$servicio .= "<td>&nbsp; ".$voucherM->descripcion_servicio."</td>";
								$servicio .= "<td>".$voucherM->cant_adultos."</td>";
								$servicio .= "<td>".$voucherM->cant_child."</td>";
								$servicio .= ($product[0]->id_grupos_producto == 2) ? "<td>".$noches."</td>": '';
								$servicio .= "</tr>";

								$voucherDetalles->servicio = $servicio;
								$servicio = '';

								$voucherDetalles->codigoConfirmacion = $detalles->cod_confirmacion;

								//observaciones
								$voucherDetalles->comentario = $voucherM->comentario."<br>";

					
							
								//PRESTADOR
								$voucherDetalles->paisPrestador = $datosPrestador[0]->pais;
								$voucherDetalles->ciudadPrestador = $datosPrestador[0]->ciudad;
							    $voucherDetalles->nombrePrestador = $datosPrestador[0]->prestador_n;
								$voucherDetalles->direccionPrestador = $datosPrestador[0]->direccion;
								$voucherDetalles->telefonoPrestador = $datosPrestador[0]->telefono;

							  

								//PROVEEDOR
								$voucherDetalles->nombreProveedor = $detalles->proveedor->nombre;

								 $voucherArray[] =  $voucherDetalles;


						 } //VOUCHER ACTIVO

					 	}//foreach $detalles->voucher

				 } // count($detalles->voucher)
				 


        }	else {


                /*==================================
               			 GENERA UN VOUCHER
                ==================================*/

        	if(count($detalles->voucher) > 0){

                foreach ($detalles->voucher as $key => $voucherN_1){


                	//VALIDAR SI VOUCHER ACTIVO
                if( $voucherN_1->activo == '1'){

                	//VALIDAR SI TIENE PRESTADOR
        		if($voucherN_1->id_prestador != 0){
        			

        		//OBTENER DATOS DEL PRESTADOR
					 $datosPrestador = DB::select('	SELECT pre.nombre prestador_n,
					 								   pre.direccion,
					 								   pre.telefono,
													   pre.id_pais,
													   p.cod_pais,
													   p.name_es pais,
													   c.denominacion ciudad
													   from personas pre
													   LEFT JOIN paises p ON p.cod_pais = pre.id_pais
													   LEFT JOIN ciudades c ON c.id = pre.id_ciudad
													   WHERE pre.id = '.$voucherN_1->id_prestador.' limit 1');
					
        		}

        	//OBTENER DETALLES DE TABLA PRODUCTO
        	$txtProducto = $detalles->producto->descrip_voucher	;
        	$icono = $detalles->producto->icono;

		 	if($txtProducto != null){
		 	$explode = explode('_', $txtProducto);

		 	$titulo = $explode[0];
			$subTituloUno = $explode[1];
			$subTituloDos = $explode[2];

		 	} else {

		 	$titulo = "<b>ERROR PRODUCTO NO ENCONTRADO</b>";
			$subTituloUno = ' - <b>Bono</b> - <b>ERROR</b> ';
			$subTituloDos = ' - <b>Voucher</b> - <b>ERROR</b> ';

		 	}

        	$voucherDetalles = new \StdClass;


        	//GUARDAR EL GRUPO PRODUCTO
			$voucherDetalles->grupo_producto = $detalles->producto->id_grupos_producto;

			//GUARDAR EL ID DEL PRODUCTO
			$voucherDetalles->producto_id = $detalles->producto->id;

        	 //LOGO
		 	$voucherDetalles->logoEmpresa = $empresa->logo;
		 	$voucherDetalles->logoAgencia = $proforma[0]->cliente['logo'];

		 	//ICONO
		 	$voucherDetalles->icono = $icono;

        	//Titulo de cabecera
		 	 $voucherDetalles->titulo = $titulo;
		 	 $voucherDetalles->subTituloUno = $subTituloUno;
		 	 $voucherDetalles->subTituloDos = $subTituloDos;


			//convertir fecha hora
		 	$fechaGenerado = explode(' ',$voucherN_1->fecha);
			$fechaGenerado = trim($fechaGenerado[0]);
			$fechaIn = explode(' ',$voucherN_1->fecha_in);
			$fechaIn = trim($fechaIn[0]);
			$fechaOut = explode(' ',$voucherN_1->fecha_out);
			$fechaOut = trim($fechaOut[0]);


	    	//DESDE HASTA
			$voucherDetalles->desde = $this->formatoFechaSalida($fechaIn);
			$voucherDetalles->hasta = $this->formatoFechaSalida($fechaOut);

			//FECHA GENERADO
			$voucherDetalles->generado = $this->formatoFechaSalida($fechaGenerado);

			//FECHA CONFIRMACION
			$fechaConfirmacionReserva = explode(' ',$fechaConfirmacionReserva);
			$fechaConfirmacionReserva = trim($fechaConfirmacionReserva[0]);

			$voucherDetalles->fechaConfirmacionReserva = $this->formatoFechaSalida($fechaConfirmacionReserva);
			$voucherDetalles->id_proforma = $detalles->id_proforma;
			$voucherDetalles->codigoConfirmacion = $detalles->cod_confirmacion;


        	foreach ($detalles->voucher as $key => $voucherN_2) {


        		if($voucherN_2->activo == '1'){



				$noches  = ($voucherN_2->noches != null) ? $voucherN_2->noches : '0';

				$ok=0;

				foreach ($nombreArray as $value) {

					if($voucherN_2->pasajero_principal == $value){

						$ok++;
					}

				}


					if($ok == 0){
						array_push($nombreArray, trim($voucherN_2->pasajero_principal));
						$nombre .= " ".$voucherN_2->pasajero_principal;

					}



		 	//Cambiar estado de voucher
			$update[] = $voucherN_2->id;

			//SERVICIOS DETALLES
			$servicio .= '<tr class="padding-td border-bottom-red-td" style="font-size: 12px;">';
			$servicio .= "<td>1x</td>";
			$servicio .= "<td>&nbsp; ".$voucherN_2->descripcion_servicio."</td>";
			$servicio .= "<td>".$voucherN_2->cant_adultos."</td>";
			$servicio .= "<td>".$voucherN_2->cant_child."</td>";
			$servicio .= ($detalles->producto->id_grupos_producto == 2) ? "<td>".$noches."</td>" : '';
			$servicio .= "</tr>";
			$voucherDetalles->servicio = $servicio;

			//observaciones
			$comentario .= $voucherN_2->comentario."<br>";

				}//$voucher->activo == '1' 

		 	}//foreach


		 	 //Datos del pasajero - Cabecera
		 	$voucherDetalles->nombre =	$nombre;


		 	//COMENTARIO
		 	$voucherDetalles->comentario = $comentario;



			//PRESTADOR
			$voucherDetalles->paisPrestador = $datosPrestador[0]->pais;
			$voucherDetalles->ciudadPrestador = $datosPrestador[0]->ciudad;
			$voucherDetalles->nombrePrestador = $datosPrestador[0]->prestador_n;
			$voucherDetalles->direccionPrestador = $datosPrestador[0]->direccion;
			$voucherDetalles->telefonoPrestador = $datosPrestador[0]->telefono;


			//PROVEEDOR
			$voucherDetalles->nombreProveedor = $detalles->proveedor->nombre;


			 $voucherArray[] =  $voucherDetalles;
			 $servicio = '';

			 //FINALIZAR FOREACH
			 break;

                    		}// $voucher->activo

                      }//foreach ->detalles->voucher

			  	} //count($detalles->voucher)

        }//else multiple voucher







		 	

		 }  //else Genera Voucher
		 
        }//foreach

       
       //ACTUALIZAR ESTADO DE VOUCHER
       if(!empty($update)){
       	foreach ($update as $key => $value) {
       		$this->updateVoucher($value);
       	}
       
       }

  //      echo '///////////////////////////////////////////////////////////////////////////////';
		// echo '<pre>';
		// print_r($voucherArray);
     
       return $voucherArray;
		



}//function




		  /**
		 * Metodo que imprime voucher recibe parametro id proforma y id vendedor con hash
		 * @param  $hash
		 * @return archivo pdf
		 */
    	public function voucherDtp($hash){

    		
    		
    		
    		$err = 0;
    		$md5 = '';
    		$idProforma = '';
    		$idVendedorHash = '';
    		$hash = explode('&',$hash);
    		$voucherArray = array();
    		
    		//SI ESTA VACIO MARCAR COMO ERROR
    	 if(!isset($hash[1])){
    	 	$err++;
            } else {
          	 
            $idProforma = $hash[0];
            $idVendedorHash = $hash[1];

            try{
           $proforma = Proforma::with('cliente',
									'vendedor',
									'pasajero',
									'estado')->where('id',$idProforma)->get();

			$empresa = Empresa::where('id',1)->first();

            $md5 = md5($proforma[0]->vendedor['id']);

            } catch(\Exception $e) {
            	$md5 = ''; 
            	$err++;
            }

            //SI EL MD5 ES DISTINTO MARCAR COMO ERROR
            if($md5 != $idVendedorHash){ $err++;}     
                
          	}// else	

		 $voucherArray = $this->imprimirVoucher($idProforma);

		 //if($err == 0 && count($voucherArray) > 0){

		 $pdf = \PDF::loadView('pages.mc.voucher.voucherPdf',compact('voucherArray'));
		$pdf->setPaper('a4', 'letter')->setWarnings(false);

		  return $pdf->download('Voucher'.$this->getId4Log().'.pdf');

		/* } else {
		 	abort(404);
		 }//else*/

      

        

		
	}//function




	
/**
	 * Genera pdf de voucher segun el id de proforma que recibe
	 * @return documento archivo.pdf
	 */

	public function voucherpdfImprimir(Request $req){
		$voucherArray = array();

		if($req->input('id') != ''){
			$id_proforma = $req->input('id');
			$optionMultiple = null;
			
			if($req->input('multiple') != ''){
				$optionMultiple = $req->input('multiple');
			}

		$voucherArray =  $this->imprimirVoucher($id_proforma,$optionMultiple);
		}

		if(count($voucherArray) > 0){

		// return view('pages.mc.voucher.voucherPdf')->with(['voucherArray'=>$voucherArray]);	
			
		$pdf = \PDF::loadView('pages.mc.voucher.voucherPdf',compact('voucherArray'));
		$pdf->setPaper('a4', 'letter')->setWarnings(false);
		  return $pdf->download('Voucher'.$this->getId4Log().'.pdf');

 		

		} else {
			 abort(404);
		}//else
	
	}//function

 public function voucherpdfDtp($id){
		
		$voucherArray = $this->imprimirVoucher($id);

		if(count($voucherArray) > 0){
		$pdf = \PDF::loadView('pages.mc.voucher.voucherPdf',compact('voucherArray'));
		$pdf->setPaper('a4', 'letter')->setWarnings(false);
		  return $pdf->download('Voucher'.$this->getId4Log().'.pdf');
		} else {
			 abort(404);
		}//else

        

		
	}//function



	private function updateVoucher($data){

		try{
			DB::table('vouchers')
			->where('id',$data)
			->update(['id_estados'=> 7]);
       		} catch(\Exception $e) {
       			 abort(404);
       		}
				
	}


	private function getIdEmpresa(){

	 return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}


	private function validarData($data){

		if(!is_null($data) && !empty($data)){
			return $data;
		} else {
			return '';
		}
	}


	/**
     * Recibe una fecha 2017-05-02 y pasa a 02/05/2017 
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaSalida($date){
        if( $date != ''){

        $date = explode('-', $date);
            $fecha = $date[2]."/".$date[1]."/".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }



    /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }




    private function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}

	public function generarVoucherPdf(Request $req)
	{
		$datosVoucher = $req->session()->get('objResultado');
		$pdf = \PDF::loadView('pages.hoteles.voucherpdf', compact('datosVoucher'));
		return $pdf->download('Voucher Promo'.$this->getId4Log().'.pdf');
	}

	public function voucherpdfUser(Request $req, $id)
	{
		//$solicitados = DB::select("select * from dtp_plus, usuarios, agencias where dtp_plus.user_id = usuarios.id_usuario and usuarios.id_agencia = agencias.id_agencia and dtp_plus.id=".$id);	
		$solicitados = DB::select('select * from personas');
		$datosVoucher = $solicitados;
		$pdf = \PDF::loadView('pages.hoteles.voucherpdfUser', compact('datosVoucher'));
		//return $pdf->download('Voucher Promo'.$this->getId4Log().'.pdf');
		return view('pages.hoteles.voucherpdfUser',compact('datosVoucher'));
	}

	public function listadopdf(Request $req){
		$agencia = Agencias::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
								->get();	
		$agencias = $agencia[0]->razon_social;
		$resultadosAccesos = DB::select("select * from accesos_dtpmundo where agencia = '".$agencias."'");

		if(!empty($req->input('usuario_id'))){
			$usuario = Usuario::where('id_usuario', '=', $req->input('usuario_id'))
									->get();	
			$usuarios = $usuario[0]->nombre_apellido;
			$listadoAccesos = [];
			foreach($resultadosAccesos as $key=>$accesos){
				if($accesos->usuario == $usuarios){
					$listadoAccesos[] = $accesos;  
				}
			}
			$resultadosAccesos = $listadoAccesos;
		}else{
			$resultadosAccesos = $resultadosAccesos;
		}

		if(!empty($req->input('desde'))){
			$listadoAccesosDesde = [];
			$desde = date('Y-m-d', strtotime($req->input('desde')));
			foreach($resultadosAccesos as $keys=>$accesosDesde){
				if($accesosDesde->fecha == $desde){
					$listadoAccesosDesde[] = $accesosDesde;  
				}	
			}
			$resultadosAccesos = $listadoAccesosDesde;
		}else{
			$resultadosAccesos = $resultadosAccesos;
		}

		$datosVoucher = $resultadosAccesos;

		$pdf = \PDF::loadView('pages.listadopdf', compact('datosVoucher'));
		return $pdf->download('Voucher Promo'.$this->getId4Log().'.pdf');
	}

        public function pdfUrl(Request $req){
        		
                if ($req->id != null) { 
                  $file = $req->id;
                  $nombre = explode("/",$file);
                    $filename = $nombre[5]; // el nombre con el que se descargará, puede ser diferente al original
                    /*header("Content-Type: application/octet-stream");*/
                    header("Content-Type: application/force-download");
                    header("Content-Disposition: attachment; filename=\"$filename\"");
                    readfile($file);
                }

        } 




}