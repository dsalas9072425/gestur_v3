<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DtpPlus;
use App\DtpPlusDetalle;
use App\Persona;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Response;
use Image;
use Session;

class DtplusController extends Controller
{
    public function index()
    {
        if($this->getIdEmpresa() != 1){
            flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
        }

        $vendedores = Persona::with('agencia')->where('id_empresa', $this->getIdEmpresa())->where('id_tipo_persona', 10)->get();
        return view('pages.mc.dtplus.index',compact('vendedores'));
    }


    public function listado(Request $req) {

		$data = $this->responseFormatDatatable($req);
		$vouchers = DtpPlus::with('detalles.factura','detalles.nota_credito','vendedor.agencia')
        ->has('detalles');

        if($data->vendedor != '0'){
            $vouchers = $vouchers->where('user_id', $data->vendedor);
        }

        if($data->estado != '0'){
            $vouchers = $vouchers->where('estado_pedido', $data->estado);
        }

		$vouchers = $vouchers->orderBy('id','DESC')
        ->get();

		foreach ($vouchers as $voucher) {

			$facturas = '';
			foreach($voucher->detalles as $documento){
				if(isset($documento->factura)){
					$facturas.= $documento->factura->nro_factura. ', ';
				} else {
					$facturas.= $documento->nota_credito->nro_nota_credito. ', ';
				}
			}
			$voucher->facturas = $facturas;
		}

		return response()->json( ['data' => $vouchers]);	
	}


    public function detalle(Request $req) {

		// $id_usuario = $this->getUsuario();
		$detalles = DtpPlusDetalle::with('factura','nota_credito.factura')
        ->where('dtp_plus_id', $req->dtplus_id)
       ->orderBy('id','DESC')->get();

       foreach ($detalles as $detalle) {
                if(isset($detalle->factura)){
                    $detalle->tipo = 'Factura';
                    $detalle->nro_factura = $detalle->factura->nro_factura;
                    $detalle->fecha_documento = date('d/m/Y H:m:i', strtotime($detalle->factura->fecha_hora_facturacion));
                    $factura = $detalle->factura;
                } else {
                    $detalle->tipo = 'Nota Credito';
                    $detalle->nro_factura = $detalle->nota_credito->nro_nota_credito;
                    $detalle->fecha_documento = date('d/m/Y H:m:i', strtotime($detalle->nota_credito->fecha_hora_nota_credito));
                    $factura = $detalle->nota_credito->factura[0];
                }

                if($factura->id_moneda_venta == 111){
                    $detalle->dtplus = round(($factura->incentivo_vendedor_agencia/$factura->cotizacion_factura),2);
                }else{
                    $detalle->dtplus = $factura->incentivo_vendedor_agencia;
                }

        }
		

		return response()->json( ['data' => $detalles]);	
	}

    
    private function getIdEmpresa()
    {
        return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
    }

    private function responseFormatDatatable($req)
    {
        $datos = array();
        $data =  new \StdClass();

        foreach ($req->formSearch as $key => $value) {
            $n = $value['name'];
            $datos[$value['name']] = $value['value'];
            $data-> $n = $value['value'];
        }

        return $data;
    }


    public function voucherPdf(Request $req) {
		$id = $req->id;

		$datosVoucher = DtpPlus::where('id',$id)
					   ->with('detalles.factura','detalles.nota_credito','vendedor.agencia')
					   ->first();

        
		$pdf = \PDF::loadView('pages.mc.dtplus.voucherpdfUsers', compact('datosVoucher'));
        $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true,'enable_css_float'=>true]);
        
        // $pdf->setPaper('a4', 'letter')->setWarnings(false);
        // return response($pdf->output(), 200)->header('Content-Type', 'application/pdf');

		return $pdf->download('voucher_promo_'.$id.'.pdf');
	}

      /**
     * Asigna una factura a la cabecera de la liquidacion para el pago total de la comision
     */
    public function pagarComision(Request $request)
    {
        /**
         * Verificamos si ya se pago para no volver a cambiar
         * Faltaia verificar permisos de personas
         */

        $liquidacion = DtpPlus::where('id', $request->dtplus_id)->first();
 
        if ($liquidacion) {

            $liquidacion->fecha_cobro = date('Y-m-d H:i:s');
            $liquidacion->id_libros_compras = $request->id_libro_compra;
            $liquidacion->estado_pedido = 'C'; //Cobrado
            $liquidacion->save();

            return response()->json(['err' => false, 'msg' => 'Factura asignada']);
        }

        return response()->json(['err' => true, 'msg' => 'Error al intentar encontrar el dtplus']);
    }


}
