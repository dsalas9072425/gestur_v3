<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Session;
use Redirect;
use App\Cotizacion;
use App\Persona;
use App\LineaCredito;
use App\EstadoCuenta;
use DB;

class LineaCreditoController extends Controller
{

	private function getIdUsuario(){
		
		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

	}

	private function getIdEmpresa(){

	 return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

	}



	/**
	 * Recibe una fecha 02/05/2017 y pasa a 2017-05-02
	 * @param  string $date fecha
	 * @return string       fecha
	 */
	private function formatoFecha($date){
		if( $date != ''){

		$date = explode('-', $date);
		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
		 	return $fecha;
		} else {
			return null;
		}
	}


	public function index(){

		$id_empresa = $this->getIdEmpresa();

		$personas = DB::select("SELECT 
								p.nombre,
								p.apellido,
								p.id,
								p.dv,
								t.denominacion, 
								p.denominacion_comercial,
								COALESCE ((SELECT l.monto from linea_de_credito as l where l.id_persona = p.id and l.activo = true limit 1),0) as monto,
								COALESCE ((SELECT cu.currency_code from linea_de_credito as l,currency as cu where l.id_persona = p.id and l.activo = true and l.id_moneda = cu.currency_id limit 1),'N/A') as moneda,
								COALESCE ((SELECT l.saldo from linea_de_credito as l where l.id_persona = p.id and l.activo = true limit 1),0) as saldo,	
								 p.documento_identidad
							 	FROM personas as p ,tipo_persona as t 
								WHERE t.id = p.id_tipo_persona 
								 AND p.id_empresa = ".$id_empresa." 
								 AND p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
								 ORDER BY monto DESC");

		return view('pages.mc.lineaCredito.index')->with(['personas'=>$personas]);
	}


	public function add(Request $req,$id){

		$idPersona = $id;
		$idEmpresa = $this->getIdEmpresa();
		$personaLineaCredito = Persona::where('id',$id)->get();
		$personas = LineaCredito::where('id_persona',$idPersona)->where('id_empresa',$idEmpresa)->orderBy('activo','DESC')->get();

		$personaLc = LineaCredito::where('id_persona',$idPersona)->where('id_empresa',$idEmpresa)->where('activo',true)->get();

		foreach ($personas as  $persona) 
		{
			$fecha = $persona->fecha;
			$persona->fecha = $this->formatoFecha($fecha);
			$monto = $persona->monto;
			$persona->monto = number_format($monto);
			
			if($persona->id_estado == '23')
			{
				$persona->id_estado = 'Activo';
			}
			else 
			{
				$persona->id_estado = 'Inactivo';
			}
		}

		//dd($personas);


		return view('pages.mc.lineaCredito.add')->with(['lineaCredito'=>$personaLineaCredito,
														'selectPersona'=>$personas,
														'idPersona'=>$idPersona,
														'personaLc'=>$personaLc,
														'idEmpresa'=>$idEmpresa]);
	}


	public function guardarCredito (Request $req)
	{
		// dd($req->nuevoSaldo);
		$idPersona = $req->idPersona;
		$idEmpresa = $this->getIdEmpresa();
		$estadoInactivo = '22';
		$estadoActivo = '23';
		$activo = false;
		$response = 'true';
		$creditoNuevo  = str_replace(',','.', str_replace('.','', $req->input('lineaCredito')));
		$fechaHora = date('Y-m-d H:m:i');

		$creditoAnterior = LineaCredito::where('activo','true')->where('id_persona',$idPersona)
							->get(['monto']);

		// dd($creditoAnterior[0]->monto);
																		
		try{
			DB::update('update linea_de_credito set id_estado = ? , activo= ? where id_empresa = ? and id_persona = ?',[$estadoInactivo,$activo,$idEmpresa,$idPersona]);
		} 
		catch(\Exception $e){
				$response = 'false';
		}

		$monto = str_replace(',','.', str_replace('.','', $req->input('lineaCredito')));

		$lineaCredito = new LineaCredito;
		$lineaCredito->id_persona = $idPersona;
		$lineaCredito->activo = 'true';
		$lineaCredito->id_empresa = $this->getIdEmpresa();
		$lineaCredito->monto = $monto;
		$lineaCredito->saldo = $req->nuevoSaldo;
		$lineaCredito->id_moneda = '143';//dolar
		$lineaCredito->fecha = date('Y-m-d');
		$lineaCredito->id_usuario = $this->getIdUsuario();//Usuario que utiliza el sistema
		$lineaCredito->id_estado = $estadoActivo;

		try{

			$lineaCredito->save();
		} 
		catch(\Exception $e){
			$response = 'false';
		}



		

		if(isset($creditoAnterior[0]->monto))
		{
			$lineaCreditoAnterior = $creditoAnterior[0]->monto;

			if(($creditoNuevo - $lineaCreditoAnterior) > 0)
			{
				$credito = $creditoNuevo - $lineaCreditoAnterior;

			  	DB::select('select registrar_movimiento_cta_cte(?,?,?,?,?,?,?)', ['Modificación Línea de Crédito', $credito, 143, $idPersona, 9, 'C',$lineaCredito->id]);

			} 
			else 
			{
			  	$credito = ltrim($creditoNuevo - $lineaCreditoAnterior, "-");

			  	DB::select('select registrar_movimiento_cta_cte(?,?,?,?,?,?,?)', ['Modificación Línea de Crédito', $credito, 143, $idPersona, 10,'C',$lineaCredito->id]);
			}
			
		  
		}


		$personas = LineaCredito::with('persona')->where('id_persona',$idPersona)->orderBy('activo','DESC')->get();

		//22 Inactivo
		//23 Activo
		foreach ($personas as  $persona) {
			
			if($persona->id_estado == '23'){
				$persona->id_estado = 'Activo';
			}else {
				$persona->id_estado = 'Inactivo';
			}
		}

		return response()->json(array('err'=>$response,'lineaCredito'=>$personas));
	}
	

	public function consultaCredito(Request $req){

		$id_empresa = $this->getIdEmpresa();
		$id = $req->id;

		if($id == '0'){
					$personas = DB::select("SELECT 
									  p.nombre,
									   l.id_estado,
									   p.id,
									   t.denominacion, 
									   p.denominacion_comercial,
									   p.documento_identidad,
									   l.monto,
									   l.activo,
									   l.fecha
									   FROM personas as p 
									   join linea_de_credito as l ON p.id = l.id_persona  
									   join tipo_persona as t ON t.id = p.id_tipo_persona 
									   where l.activo = true and p.id_empresa = ".$id_empresa."	
									   and p.id_tipo_persona =".config('constants.personaAgencia'));
				} else {
					
				$personas = DB::select("SELECT
									   p.nombre,
									   l.id_estado,
									   p.id,
									   t.denominacion, 
									   p.denominacion_comercial,
									   p.documento_identidad,
									   l.monto,
									   l.activo,
									   l.fecha
									   FROM personas as p 
									   join linea_de_credito as l ON p.id = l.id_persona  
									   join tipo_persona as t ON t.id = p.id_tipo_persona 
									   where l.activo = true and p.id_empresa = ".$id_empresa."	
									   and p.id_tipo_persona =".config('constants.personaAgencia')."
									   and p.id =".$id);
				}


				//22 Inactivo
		//23 Activo
		foreach ($personas as  $persona) {

			$fecha = $persona->fecha;
			$persona->fecha = $this->formatoFecha($fecha);
			$monto = $persona->monto;
			$persona->monto = number_format($monto);
			
			if($persona->id_estado == '23'){
				$persona->id_estado = 'Activo';
			}else {
				$persona->id_estado = 'Inactivo';
			}
		}

		


	echo json_encode(array('datos'=>$personas,'err'=>'false'));
	}


}
