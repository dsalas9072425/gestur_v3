<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\DestinoDtpmundo;
use App\Currency;
use App\Aerolinea;
use App\EstadoFactour;
use App\Grupo;
use App\Divisas;
use App\Producto;
use App\Persona;
use App\TipoTicket;
use App\Ticket;
use App\Factura;
use App\TipoTransaccionTicket;
use App\TarjetaCredito;
use App\LibroCompra;
use App\FormaPagoOpDetalle; 
use App\FormaPagoOpCabecera;
use App\FormaPagoCliente;
use App\OpCabecera;
use App\TicketOffice;
use App\OpDetalle;
use Redirect;
use DB;
use Response;
use Image; 
use Session;
use App\TicketImpuesto;
use App\TicketFractal;
use App\TicketNdc;
use App\TarjetaPersona;
use Log;
use App\Traits\TicketsTrait;
use App\Traits\OpTrait;
use App\Exceptions\ExceptionCustom;
use DateTime;
use Illuminate\Support\Facades\Redis;


class TicketsController extends Controller
{

  use TicketsTrait, OpTrait;


  private function getIdEmpresa()
  {
    return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
  }

  /**
  * Recibe una fecha 2017-05-02 y pasa a 02/05/2017 
  * @param  string $date fecha
  * @return string       fecha
  */

  private function formatoFechaSalida($date)
  {
    if( $date != '')
    {
      $date = explode('-', $date);
      $fecha = $date[2]."/".$date[1]."/".$date[0];
      
      return $fecha;
    } 
    else 
    {
      return null;
    }
  }

  /**
   * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
   * @param  string $date fecha
   * @return string       fecha
   */

    private function formatoFechaEntrada($date)
    {
      if( $date != '')
      {
        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
      } 
      else 
      {
        return null;
      }
    }




/**
   * Recibe una fecha 02/05/2017 y pasa a 2017-05-02
   * @param  string $date fecha
   * @return string       fecha
   */
  private function formatoFecha($date)
  {
    if( $date != '')
    {
      $date = explode('/', $date);
      $fecha = $date[2]."-".$date[1]."-".$date[0];
      return $fecha;
    } 
    else 
    {
      return null;
    }
  }


  public function indexTicket(Request $req)
{ 

    $tipos_tickets = TipoTicket::where('id_empresa', null)->orderBy('descripcion')->get();   
    $tipo_tickets =  TipoTicket::where('id_empresa', $this->getIdEmpresa())->orderBy('descripcion')->get();
    $tipoTickets = [];
    $base = 0;
    foreach($tipos_tickets as $key=>$value){
      $tipoTickets[$key]['id'] = $value->id;
      $tipoTickets[$key]['descripcion'] = $value->descripcion;
    }
    $base = $key;
    foreach($tipo_tickets as $keys=>$item){
      $base = $base +1;
      $tipoTickets[$base]['id'] = $item->id;
      $tipoTickets[$base]['descripcion'] = $item->descripcion;
    }

    $estados = EstadoFactour::where('id_tipo_estado', '8')->where('activo',true)->orderBy('denominacion','ASC')->get();  
    $pasajeros = DB::select('SELECT DISTINCT pasajero, id_empresa  FROM tickets WHERE id_empresa = '.$this->getIdEmpresa().' ORDER BY pasajero ASC');
    $calendarioBsp = DB::select("SELECT * FROM vw_calendario_bcp");
    $monedas = Divisas::where('activo','S')->get();

    $grupos = Grupo::with('destino', 'estado')->where('empresa_id', $this->getIdEmpresa())->get();

    $tarjetas = TarjetaPersona::with('banco_detalle.currency','banco_detalle.banco_cabecera')
    ->where('id_persona',$this->getIdUsuario())
    ->get();

    $aerolineas = DB::select('
                        SELECT DISTINCT tk.id_proveedor,
                        p.nombre,
                        p.id 
                        FROM tickets tk
                        LEFT JOIN personas p ON p.id = tk.id_proveedor
                        ORDER BY p.nombre ASC');

    $vendedorProforma = DB::select("SELECT distinct(p.id_usuario),
                                             pe.nombre,
                                             pe.apellido,
                                             pe.id  
                                             FROM tickets t, proformas p, personas pe 
                                             WHERE p.id = t.id_proforma 
                                             and pe.id = p.id_usuario 
                                             and pe.id_empresa = ".$this->getIdEmpresa()."
                                             order  by pe.nombre ASC");
    /*===================
      PARAMETROS
      ===================*/
    //Recibe un id de grupo para realizar busqueda
      $id = '';
      $tipo_grupo ='';
      
      if($req->input('id') != ''){
        $id = $req->input('id');

        $grupo = Grupo::where('id', $req->input('id'))->get(); 
        $tipo_grupo = $grupo[0]->tipo_ticket_id;

      }
      //PARAMETROS DEL HOME PARA FILTRAR
       $id_estado = $req->input('id_estado');
       $id_tipo_ticket = $req->input('id_tipo_ticket');

      //BTN DE PERMISOS ESPECIALES
       $btn = $this->btnPermisos('','mostrarTicket');
      
    
        return view('pages.mc.tickets.index', compact('btn','estados','pasajeros','calendarioBsp','monedas','aerolineas','tipoTickets','vendedorProforma','grupos','id','tipo_grupo','id_estado','id_tipo_ticket','tarjetas'));


    }//function



public function ticketAjaxServerSide(Request $req)
{ 
  
  $ajax = 0;
  $formSearch = $req->dataString;
  $draw = intval($req->draw);
  $start = intval($req->start);
  $length = intval($req->length);
  $cont = 0;
  $filtrarMax = 0;
  $query = "SELECT to_char(fecha_pago_bsp,'DD/MM/YYYY') as fecha_pago_formateo,
                        to_char(fecha_emision,'DD/MM/YYYY') as fecha_emision_formateo,
                        * FROM vw_ticket WHERE id_empresa = ".$this->getIdEmpresa();

  if($formSearch['fecha_emision_desdeHasta'] != '')
  {
      $fecha = explode('-',$formSearch['fecha_emision_desdeHasta']);
      $desde = $this->formatoFechaEntrada(trim($fecha[0]));
      $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
      $query .= " AND fecha_emision BETWEEN '".$desde."' AND '".$hasta."' ";
  }
     
      if($formSearch['estado_id'] != ''){
        $query .= ' AND id_estado = '.$formSearch['estado_id'];
      }

      if($formSearch['pnr'] != ''){
        $query .= " AND pnr = '".$formSearch['pnr']."' ";
      }
      if($formSearch['nro_ticket'] != ''){
        $query .= " AND numero_amadeus = '".$formSearch['nro_ticket']."' ";
      }
      if($formSearch['pasajero'] != ''){
        $query .= " AND pasajero = '".$formSearch['pasajero']."' ";
      }
      if($formSearch['fecha_pago'] != ''){
          // $fecha     = $this->formatoFechaEntrada($formSearch['fecha_pago']);
        $query .= " AND fecha_pago_bsp BETWEEN '".$formSearch['fecha_pago']."' AND '".$formSearch['fecha_pago']."' ";
      }
      if($formSearch['moneda_id'] != ''){
        $query .= " AND id_currency_costo = ".$formSearch['moneda_id'];
      }
      if($formSearch['aerolinea_id'] != ''){
        $query .= " AND id_proveedor = ".$formSearch['aerolinea_id'];
      }
      if($formSearch['id_proforma'] != ''){
        $query .= " AND id_proforma = ".$formSearch['id_proforma'];
      }
       if($formSearch['nro_factura'] != ''){
        $query .= " AND nro_factura = '".$formSearch['nro_factura']."' ";
      }
      if($formSearch['tipo_ticket'] != '' && $formSearch['tipo_ticket'] != 'D'){
        $query .= " AND id_tipo_ticket = ".$formSearch['tipo_ticket'];
      }else if($formSearch['tipo_ticket'] == 'D'){
        $query .= ' AND id_tipo_ticket != 1';
      }


      if($formSearch['id_vendedor_empresa'] != ''){
        $query .= " AND id_usuario_proforma = ".$formSearch['id_vendedor_empresa'];
      }
      if($formSearch['grupo_id'] != ''){
        $query .= " AND id_grupo = ".$formSearch['grupo_id'];
      }

      $query .= ' AND id_empresa = '.$this->getIdEmpresa();

      $tickets = DB::select($query);

      $cantidadTicket = count($tickets);
      $resultTicket = array();
      $btn = '';

      foreach ($tickets as $key => $value) 
      {

        /*echo "</pre>";
        print_r(json_encode($value)); */

         //LIMPIAR BOTONES EN LA CARGA
           $btnEdit = ''; 
           $btn = '';


      if($cont == $length)
        {
          break;
        }

        if($key >= $start)
        {

         

          //BTN CON OPCIONES PARA AIR EUROPA Y QUE SE ENCUENRE EN ESTADO DISPONIBLE
          if($value->id_proveedor == 520 && $value->id_estado == 25)
          {
            $btn = '<a href="'. route('edit', $value->id) .'" class="btn btn-danger" title="Tasa YQ" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-plane"></i></a> ';
          } 
          else 
          {
              $btn = '<a class="btn btn-danger" title="Tasa YQ" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button" disabled="disabled"><i class="fa fa-fw fa-plane"></i></a> ';
          }


          //BTN PARA PARANAIR PARA MODIFICAR EL CALCULO DEL IVA
          if($value->id_proveedor == 505 && $value->id_estado == 25)
          {
            $btn = '<a onclick="cambiarIvaTicket('.$value->id.','.$value->porc_iva.')" title="Cambiar IVA" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-plane"></i></a> ';
          }  


          if(isset($value->pro_per_n)){ 
            $proveedor = $value->pro_per_n.' '.$value->pro_ape_a;
          }else{
            $proveedor ="";
          }

          //echo strlen ($value->numero_amadeus);

            if($value->id_estado == 25 && strlen ($value->numero_amadeus) >= 10){
                $btnx = '<a onclick="asignarTicket('.$value->id.')" title="Asignar Ticket" role="button" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;"><i class="fa fa-files-o"></i></a>  ';
            }else{
                $btnx = '';
            }


            $btns = '<a href="'.route('addUser', $value->id).'" title="Asignar a Vendedor" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-user"></i></a>';         
          

          //DISPONIBLE BTN PARA CAMBIAR ESTADO
          if($value->id_estado == 25){
            $n = 1;

            $btnEdit = '<a onclick="cambiarEstadoTicket('.$n.','.$value->id.')" title="Cambiar Estado" role="button" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;"><i class="fa fa-edit"></i></a>  ';
          }
           
          $coma = ($value->currency_code == 'PYG') ? 0 : 2;
          $vendedor = $value->vendedor_nombre." ".$value->vendedor_apellido;


          $resultTicket[] = [
                            $value->numero_amadeus, 
                            $value->nombre_proveedor,
                            $value->fecha_emision_formateo,
                            $value->usuario,
                            $value->id_proforma,
                            $proveedor,
                            $vendedor,
                            $value->fecha_pago_formateo,
                            $value->codigo_amadeus,
                            $value->descripcion,
                            $value->estado,
                            $value->nro_factura,
                            $value->pnr,
                            $value->currency_code,
                            number_format($value->total_costo,$coma,",","."), 
                            number_format($value->total_venta,$coma,",","."),
                            $btn,
                            $btns,
                            '<a href="'.route('verDetalleTicket', $value->id).'" title="Ver Ticket" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-search"></i></a>',
                            $btnx,
                            $btnEdit
                          ];
          $cont++;
        }
      }

        return response()->json(array('data'=>$resultTicket,
                                    'draw'=>$draw,
                                    'recordsTotal'=>$cantidadTicket,
                                    'recordsFiltered'=>$cantidadTicket));

    }//function





    public function cambiarEstadoTicket(Request $req){

      $estado = $req->estado;
      $idTicket = $req->id;
      $opcion = $req->opcion;
      $err = false;

      if($idTicket != '' && $opcion != '' && $estado != ''){
       try
      {
        DB::table('tickets')
                  ->where('id',$idTicket)
                  ->update(['id_estado'=>$estado]);
                  $err = true;
      } 
      catch(\Exception $e){
        $err = false;
      }

    } 
      


      return response()->json(['resp'=>$err]);
    }


    public function cambiarIva(Request $req){

      $idTicket = $req->id;
      $ivaTicket = (float)$req->iva;
      $err = false;

      if($idTicket != '' && $ivaTicket != ''){

       try{
        DB::table('tickets')
                  ->where('id',$idTicket)
                  ->update(['porc_iva'=>$ivaTicket]);
                  $err = true;
      } 
      catch(\Exception $e){
        $err = false;
      }

           }

       return response()->json(array('err'=>$err));
    }


    public function cambiarComision(Request $req){

      $idTicket = $req->id;
      $comisionTicket = (float)$req->comision;
      $codigoOvertactico = $req->codigo;
      $err = false;
      if($idTicket != ''){
          $ticket = Ticket::where('id', $idTicket)->get(['facial']);
          $comision = $ticket[0]->facial;
          $monto_comision = ($comision * $comisionTicket)/100;
          $iva_comision  = $monto_comision/10;
         try{
              DB::table('tickets')
                      ->where('id',$idTicket)
                      ->update(['comision'=>$monto_comision,
                                'iva_comision'=>$iva_comision,
                                'over_tactico'=>$codigoOvertactico
                                ]);
                      $err = true;
         } 
          catch(\Exception $e){
             $err = false;
          } 
      }
      return response()->json(array('err'=>$err));
    }



    public function create()
    {
      if($this->getIdEmpresa() == 1){
        $producto = 1;
      }
      if($this->getIdEmpresa() == 2){
        $producto = 8167;
      }
      if($this->getIdEmpresa() == 5){
        $producto = 14869;
      }

      if($this->getIdEmpresa() == 9){
       $producto = 29754;
       //$producto = 29573; 
      }

      if($this->getIdEmpresa() == 17){
        $producto = 37169;
      }

      if($this->getIdEmpresa() == 21){
        $producto = 38368;
      }

      if($this->getIdEmpresa() == 23){
        $producto = 109684;
      }


      $aerolineas1 = DB::select('select id as out_id_persona, nombre as out_nombre from personas where id_empresa = '.$this->getIdEmpresa().' and id in (select distinct(id_proveedor) from tickets where id_empresa = '.$this->getIdEmpresa().')');  
      $aerolineas2 = DB::select('select * from get_producto_persona(?,?)', [$producto,$this->getIdEmpresa()]);
      $aerolineas = array_merge($aerolineas1, $aerolineas2);

      $currencys = Currency::where('activo', 'S')->orderBy('currency_code')->get();    
      $estados = EstadoFactour::where('id_tipo_estado', '8')->orderBy('denominacion')->get();  
    //  $tipos_tickets = TipoTicket::where('id','1')->orwhere('id','4')->orwhere('id','5')->orderBy('descripcion')->get();
      $tipos_tickets = DB::select("select id, descripcion, id_empresa
                                      from tipos_ticket
                                      where id_empresa is null
                                      union all
                                      select id, descripcion, id_empresa
                                      from tipos_ticket
                                      where id_empresa = ".$this->getIdEmpresa());

      $grupos = Grupo::all();
      $tipo_transaccion_tickets = TipoTransaccionTicket::whereIn('tipo_amadeus', ['SALE', 'RFND','ACMA'])
                                                        ->where('activo', true) 
                                                        ->orderBy('descripcion')->get();
      $permiso = $this->btnPermisosTicket(array(),'ticket5');
      return view('pages.mc.tickets.create', compact('aerolineas','currencys','estados', 'tipos_tickets', 'grupos', 'tipo_transaccion_tickets','permiso'));
    }

    public function store(Request $request)
    {

     $rules = [
        'aerolinea' => 'required',
        'numero_de_ticket' => 'required',
        'tipo_ticket' => 'required',
        // 'estado' => 'required',
        'fecha_emision' => 'required',
        'PNR' => 'required',
        'pasajero' => 'required',
        'tarifa' => 'required',
        'facial' => 'required',
        'tasa' => 'required',
        'tipo_transaccion' => 'required',
        'moneda' => 'required'

      ];
 
      $this->validate($request, $rules);

      
      $fecha_alta = date('d/m/Y');
      $facial = str_replace(',','.', str_replace('.','', $request->input('facial')));
      $tasa = str_replace(',','.', str_replace('.','', $request->input('tasa')));
      $genera_comision = $request->tarifa == 1;
      $tarifa_comision_yq = $request->tarifa_comision_yq == 1;

  
     try {  

      DB::beginTransaction();

            /**
             * Funcion recibe fechas en formato d/m/Y
             * Los numeros ya deben estar formateados
             */
            $ticket_data = [  
              'fecha_emision' => $request->fecha_emision,
              'id_proveedor' => (int)$request->input('aerolinea'),
              'facial' => $facial,
              'id_currency_costo' => $request->moneda,
              'pasajero' => $request->pasajero,
              'pnr' => $request->PNR,
              'tasas' => (float)$tasa,
              'genera_iva' => true,
              'genera_comision' => $genera_comision,
              'tasas_yq' => (float)str_replace(',','.', str_replace('.','', $request->input('tasaYq'))),
              'id_tipo_ticket' => $request->tipo_ticket,
              'fecha_alta' => $fecha_alta,
              'id_usuario' => Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
              'nro_amadeus' => $request->numero_de_ticket,
              'id_tipo_transaccion' => $request->tipo_transaccion,
              'tarifa_comision_yq' => $tarifa_comision_yq
            ];


           // dd($ticket_data);
            $this->insert_into_ticket_manual($ticket_data);
            
          
            $tickets = Ticket::where('numero_amadeus', $request->numero_de_ticket)->first();  
            $obs = '';
            // if($request->input('tasaYq')){
            //           $validar_tasaYQ= DB::select('SELECT * FROM validar_tasaYQ('.(float)$request->input('tasaYq').','.$tickets->id.')');
            //           if($validar_tasaYQ[0]->validar_tasayq == 'OK'){
            //               DB::table('tickets')->where('id',$tickets->id)
            //                 ->update([
            //                           'tasas_yq'=> (float)str_replace(',','.', str_replace('.','', $request->input('tasaYq')))
            //                         ]);
                        
            //           }else{  
            //               $obs = '<br>'.$validar_tasaYQ[0]->validar_tasayq;
            //           }   
            //        }
                   
            DB::commit();       
          flash('¡El ticket se ha ingresado exitosamente!'.$obs)->success();
          return redirect()->route('indexTicket');
             
       } catch(\Exception $e) {

        DB::rollback();      

        Log::error($e);
        flash('Ha ocurrido un error, inténtelo nuevamente. Error: '.$e->getMessage())->error();
        return back()->withInput();
      } 
    }

    public function edit($id)
    {
      $tickets = DB::select('SELECT * FROM vw_ticket WHERE id_empresa = '.$this->getIdEmpresa().' AND id = '.$id.' LIMIT 1 ');
      if(!isset($tickets[0]->id)){
        flash('El ticket solicitado no existe. Intentelo nuevamente !!')->error();
        return redirect()->route('home');
      }
      $ticketes = Ticket::findOrFail($id);
      if($this->getIdEmpresa() == 1){
        $producto = 1;
      }
      if($this->getIdEmpresa() == 2){
        $producto = 8167;
      }
      if($this->getIdEmpresa() == 5){
        $producto = 14869;
      }

      if($this->getIdEmpresa() == 9){
       $producto = 29754;
       //$producto = 29573; 
      }

      if($this->getIdEmpresa() == 17){
        $producto = 37169;
      }

      if($this->getIdEmpresa() == 21){
        $producto = 38368;
      }
      
      if($this->getIdEmpresa() == 23){
        $producto = 109684;
      }

      $aerolineas1 = DB::select('select id as out_id_persona, nombre as out_nombre from personas where id_empresa = '.$this->getIdEmpresa().' and id in (select distinct(id_proveedor) from tickets where id_empresa = '.$this->getIdEmpresa().')');  
      $aerolineas2 = DB::select('select * from get_producto_persona(?,?)', [$producto,$this->getIdEmpresa()]);
      $aerolineas = array_merge($aerolineas1, $aerolineas2);
      $currencys = Currency::where('activo', 'S')->orderBy('currency_code')->get();    
      $estados = EstadoFactour::where('id_tipo_estado', '8')->orderBy('denominacion')->get();  
    //  $tipos_tickets = TipoTicket::where('id','1')->orwhere('id','4')->orwhere('id','5')->orderBy('descripcion')->get();
      $tipos_tickets = DB::select("select id, descripcion, id_empresa
                                      from tipos_ticket
                                      where id_empresa is null
                                      union all
                                      select id, descripcion, id_empresa
                                      from tipos_ticket
                                      where id_empresa = ".$this->getIdEmpresa());

      $grupos = Grupo::all();
      $tipo_transaccion_tickets = TipoTransaccionTicket::where('tipo_amadeus', 'SALE')
                                                        ->where('activo', true) 
                                                        ->orderBy('descripcion')->get();
      if($tickets[0]->fecha_pago_bsp)
      {
        $fecha_s = explode("-", $tickets[0]->fecha_pago_bsp);
        $fecha_pago = $fecha_s[2]."/".$fecha_s[1]."/".$fecha_s[0];

        $fecha_e = explode("-", $tickets[0]->fecha_emision);
        $fecha_emision = $fecha_e[2]."/".$fecha_e[1]."/".$fecha_e[0];
      } 
      else 
      {
        $fecha_pago =""; 
        $fecha_emision ="";     
      } 
      $permiso = $this->btnPermisosTicket(array(),'ticket5');
      return view('pages.mc.tickets.edit', compact('aerolineas','currencys','estados', 'tipos_tickets', 'grupos', 'tipo_transaccion_tickets','permiso','ticketes','tickets','fecha_emision','fecha_pago'));
    }



    public function editarTicket(Request $request)
    {

      DB::select("ALTER TABLE tickets DISABLE TRIGGER ALL");
      $ticket = Ticket::find($request->input('id'));
       try
        {  

          DB::table('tickets')
              ->where('id',$request->input('id'))
              ->update([
                      'id_proveedor'=>$request->input('aerolinea'),
                      'facial'=>(float)str_replace(',','.', str_replace('.','', $request->input('facial'))),
                      'tasas'=>(float)str_replace(',','.', str_replace('.','', $request->input('tasa'))),
                      'tasas_yq'=>(float)str_replace(',','.', str_replace('.','', $request->input('tasa_yq'))),
                      'iva'=>(float)str_replace(',','.', str_replace('.','', $request->input('iva'))),
                      'comision'=>(float)str_replace(',','.', str_replace('.','', $request->input('comision'))),
                      'iva_comision'=>(float)str_replace(',','.', str_replace('.','', $request->input('iva_comision'))),
                      'total_ticket'=>(float)str_replace(',','.', str_replace('.','', $request->input('total_ticket')))
                        ]);

         flash('¡Se han editado los datos del ticket exitosamente!')->success();
         DB::select("ALTER TABLE tickets ENABLE TRIGGER ALL");

          return redirect()->route('indexTicket');
      } 
        catch(\Exception $e)
        {
          flash('No se ha podido guardar. Intente nuevamente.')->error();
          return redirect()->back();
        }    
    }
  


    public function update(Request $request, $id)
    {
      $ticket = Ticket::find($id);

      try
      {
        $ticket->update([
        $ticket->tasas_yq = $request->input('tasa_yq'),
        ]);
        flash('¡Se ha actualizado exitosamente!')->success();
        return redirect()->route('indexTicket');
      } 
      catch(\Exception $e)
      {
        flash('No puede ser mayor a la tasa. Intente nuevamente.')->error();
        return redirect()->back();
      }      
        
    }


	public function getTicket(Request $request)
  {
    $tickets = Ticket::with('persona', 'estado', 'tipoTicket')->where(function($query) use($request)
    {
      if(!empty($request->input('fecha_emision'))){
        $fecha_e = explode("/", $request->fecha_emision);
        $fecha_emision = $fecha_e[2]."-".$fecha_e[1]."-".$fecha_e[0];
        $query->orwhereDate('fecha_emision','=', $fecha_emision);
      }
      if(!empty($request->input('fecha_pago'))){
        $fecha_s = explode("/", $request->fecha_pago);
        $fecha_pago = $fecha_s[2]."-".$fecha_s[1]."-".$fecha_s[0];
        $query->orwhereDate('fecha_pago_bsp','=', $fecha_pago);
      }
      if(!empty($request->input('PNR'))){
        $query->orwhere('pnr','=', $request->input('PNR'));
      }
      if(!empty($request->input('estado'))){
         $query->orwhere('id_estado','=', $request->input('estado'));
      }
    })->get();

   		return response()->json($tickets);		
	}

    public function ajaxTickets(Request $req)
  {
      
      $datos = $this->responseFormatDatatable($req);
      //dd($datos);
    /* echo "</pre>";
      print_r(json_encode($datos)); */
      $key = 'ajaxTickets:' . md5(serialize($req->input()));
      $tickets = unserialize(Redis::get($key)); // Deserializar los datos recuperados de Redis
      // $tickets = Redis::get($key);
      if ($tickets === false) { // Comprobar si los datos son false, no null
      // $query = "SELECT to_char(fecha_pago_bsp,'DD/MM/YYYY') as fecha_pago_formateo,
      //                   to_char(fecha_emision,'DD/MM/YYYY') as fecha_emision_formateo ,
      //                   to_char(check_in,'DD/MM/YYYY') as fecha_checkin_formateo, 
      //                   to_char(check_out,'DD/MM/YYYY') as fecha_checkout_formateo,
      //                   * FROM public.mv_ticket WHERE id_empresa = ".$this->getIdEmpresa();
      // $query = DB::table('public.mv_ticket')
      $query = DB::table('ticket_material')
        ->select(DB::raw("to_char(fecha_pago_bsp,'DD/MM/YYYY') as fecha_pago_formateo,
                    to_char(fecha_emision,'DD/MM/YYYY') as fecha_emision_formateo ,
                    to_char(check_in,'DD/MM/YYYY') as fecha_checkin_formateo, 
                    to_char(check_out,'DD/MM/YYYY') as fecha_checkout_formateo,
                    *"))
        ->where('id_empresa', $this->getIdEmpresa());
  
      // if($datos->fecha_emision_desdeHasta != '')
      // {
      //   $fecha = explode('-',$datos->fecha_emision_desdeHasta);
      //   $desde = $this->formatoFechaEntrada(trim($fecha[0]));
      //   $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
      //   $query .= " AND fecha_emision BETWEEN '".$desde."' AND '".$hasta."' ";
      // }
      if($datos->fecha_emision_desdeHasta != '')
      {
        $fecha = explode('-',$datos->fecha_emision_desdeHasta);
        $desde = $this->formatoFechaEntrada(trim($fecha[0]));
        $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
        $query->whereBetween('fecha_emision', [$desde, $hasta]);
      }
      if(isset($datos->fecha_checkIn_desdeHasta )){
        if($datos->fecha_checkIn_desdeHasta != ''){

          $fechaCheck = explode('-',$datos->fecha_checkIn_desdeHasta);
          $desdeC = $this->formatoFechaEntrada(trim($fechaCheck[0]));
          $hastaC = $this->formatoFechaEntrada(trim($fechaCheck[1]));
          // $query .= " AND check_in BETWEEN '".$desdeC."' AND '".$hastaC."' ";
          $query->whereBetween('check_in', [$desdeC, $hastaC]);
        }
     }   
     if($datos->estado_id != ''){
        // $query .= ' AND id_estado = '.$datos->estado_id;
        $query->where('id_estado', $datos->estado_id);
      }
      if($datos->pnr != ''){
        // $query .= " AND pnr = '".$datos->pnr."' ";
        $query->where('pnr', $datos->pnr);
      }
      if($datos->nro_ticket != ''){
        // $query .= " AND numero_amadeus LIKE '%".$datos->nro_ticket."%' ";
        $query->where('numero_amadeus', 'like', '%'.$datos->nro_ticket.'%');
      }
      if($datos->pasajero != ''){
        // $query .= " AND pasajero = '".$datos->pasajero."' ";
        $query->where('pasajero', $datos->pasajero);
      }
      if($datos->fecha_pago != ''){
          // $fecha     = $this->formatoFechaEntrada($formSearch['fecha_pago']);
        // $query .= " AND fecha_pago_bsp BETWEEN '".$datos->fecha_pago."' AND '".$datos->fecha_pago."' ";
        $query->whereBetween('fecha_pago_bsp', [$datos, $datos]);
      }
      if($datos->moneda_id != ''){
        // $query .= " AND id_currency_costo = ".$datos->moneda_id;
        $query->where('id_currency_costo', $datos->moneda_id);
      }
      if($datos->aerolinea_id != ''){
        // $query .= " AND id_proveedor = ".$datos->aerolinea_id;
        $query->where('id_proveedor', $datos->aerolinea_id);
      }
      if($datos->id_proforma != ''){
        // $query .= " AND id_proforma = ".$datos->id_proforma;
        $query->where('id_proforma', $datos->id_proforma);
      }
       if($datos->nro_factura != ''){
        // $query .= " AND nro_factura = '".$datos->nro_factura."' ";
        $query->where('nro_factura', $datos->nro_factura);
      }
      if($datos->tipo_ticket != ''){
        // $query .= " AND id_tipo_ticket = ".$datos->tipo_ticket;
        $query->where('id_tipo_ticket', $datos->tipo_ticket);
      }
      if($datos->id_vendedor_empresa != ''){
        // $query .= " AND id_usuario_proforma = ".$datos->id_vendedor_empresa;
        $query->where('id_usuario_proforma', $datos->id_vendedor_empresa);
      }
      if($datos->grupo_id != ''){
        // $query .= " AND id_grupo = ".$datos->grupo_id;
        $query->where('id_grupo', $datos->grupo_id);
      }
      // if($datos->mostrar_precio == 'N'){
      //   $query .= " AND total_venta <> 0";
      // }

      if($datos->asignado){
        $datos->asignado = $datos->asignado == '1' ? 'true' : 'false';
        // $query .= " AND asignado = ".$datos->asignado;
        $query->where('asignado', $datos);
      }
      $totalRecords = $query->count();

		$draw = $req->input('draw');
		$start = $req->input('start', 0);
		$length = $req->input('length', 10);
		$recordsFiltered = $totalRecords;
	
		$tickets = $query->offset($start)->limit($length)->get();

      //  $query .= " AND numero_amadeus =  '". $numeroAmadeus."'";
      // $query .= " LIMIT 300";
     /* echo '<pre>';
      print_r($query);
    die();*/
      // $tickets = DB::select($query); 
      // $tickets = DB::select($query);
      // $tickets = $query->paginate(10);
      // $currentPage = $req->input('page', 1);

      // // Ejecutar la consulta y paginar los resultados
      // $tickets = $query->paginate(10, ['*'], 'page', $currentPage);
      // podes quitar el foreach para probar
      // $tickets = $tickets->getCollection();

      // Recorrer los elementos y agregar los botones de permisos
      foreach ($tickets as $ticket) {
          $ticket->btn1 = $this->btnPermisosTicket(['id_ticket' => $ticket->id], 'ticket1');
          $ticket->btn2 = $this->btnPermisosTicket(['id_ticket' => $ticket->id], 'ticket3');
          $ticket->btn3 = $this->btnPermisosTicket(['id_estado' => 1, 'id_ticket' => $ticket->id], 'ticket4');
          $ticket->btn4 = $this->btnPermisosTicket(['id_ticket' => $ticket->id], 'ticket2');
          $ticket->btn5 = $this->btnPermisosTicket(['id_ticket' => $ticket->id, 'porc_iva' => $ticket->porc_iva], 'ticket5');
          $ticket->btn6 = $this->btnPermisosTicket(['id_ticket' => $ticket->id, 'porc_comision' => $ticket->comision], 'ticket6');
          $ticket->btn7 = $this->btnPermisosTicket(['id_ticket' => $ticket->id], 'ticket7');
      }
      
      // foreach($tickets as $key=>$ticket){
        // foreach($tickets->items() as $ticket){
 
        // // $tickets[$key]->btn1 = $this->btnPermisosTicket(array('id_ticket'=>$ticket->id),'ticket1');
        // // $tickets[$key]->btn2 = $this->btnPermisosTicket(array('id_ticket'=>$ticket->id),'ticket3');
        // // $tickets[$key]->btn3 = $this->btnPermisosTicket(array('id_estado'=>1,'id_ticket'=>$ticket->id),'ticket4');
        // // $tickets[$key]->btn4 = $this->btnPermisosTicket(array('id_ticket'=>$ticket->id),'ticket2');
        // // $tickets[$key]->btn5 = $this->btnPermisosTicket(array('id_ticket'=>$ticket->id, 'porc_iva'=>$ticket->porc_iva),'ticket5');
        // // $tickets[$key]->btn6 = $this->btnPermisosTicket(array('id_ticket'=>$ticket->id, 'porc_comision'=>$ticket->comision),'ticket6');
        // // $tickets[$key]->btn7 = $this->btnPermisosTicket(array('id_ticket'=>$ticket->id),'ticket7');
  
        // // dd($tickets);

        // }

        

         // Almacena el resultado en Redis para la próxima vez
        // Redis::set($key, $tickets);
        Redis::set($key, serialize($tickets)); // Serializar los datos antes de guardarlos en Redis
      }//if de redis
  
      return response()->json(['data'=>$tickets,
      'draw' => $draw,
			'recordsTotal' => $totalRecords,
			'recordsFiltered' => $recordsFiltered,]);

   }


  public function ajaxTicket(Request $req)
  {

      $ticket  = Ticket::findOrFail($req->id_ticket);
      $query = "SELECT to_char(fecha_pago_bsp,'DD/MM/YYYY') as fecha_pago_formateo,
                        to_char(fecha_emision,'DD/MM/YYYY') as fecha_emision_formateo ,
                        * FROM vw_ticket WHERE id_empresa = ".$this->getIdEmpresa();

      $fecha_fin = new DateTime($ticket->fecha_emision.' 00:00:00');
      $fecha_fin->modify('+11 months');// Sumar 11 meses a la fecha
      $fecha_fin = $fecha_fin->format('Y-m-d H:i:s');

      $desde = $ticket->fecha_emision;
      $hasta = $fecha_fin;
      $query .= " AND fecha_emision BETWEEN '".$desde."' AND '".$hasta."' ";

      //Grupo y salida garantizada, estado facturado y cobrado
      $query .= " AND id_tipo_ticket in (3,2) AND id_estado = 27 AND estado_cobro_ticket = 'SI' ";
      
     
      $tickets = DB::select($query);   
     
      $listadoTicket=array();

      foreach ($tickets as $key => $value) 
      {
          if($value->id_estado == 27 &&strlen ($value->numero_amadeus) <= 9){
              $ticket_asignado = Ticket::where('id_ticket_asignado', $value->id)->get(['id_ticket_asignado']); 
              if(!isset($ticket_asignado[0]->id_ticket_asignado)){
                  $listadoTicket[] = $value;
              }
           } 
      }

      return response()->json(['data'=>$listadoTicket]);
   }

  public function mostrarFacturas(Request $req)
  {
    
    $facturas = Factura::with('cliente', 'vendedorEmpresa', 'pasajero', 'facturaDetalle')
                       ->where('id_estado_factura', '29')
                       ->where('nro_factura', $req->input('factura'))
                       ->get();
    $indice = 0;
     foreach ($facturas as $key => $factura) 
    {
      $poseeTicket = 0;
      foreach ($factura->facturaDetalle as $key1 => $detalle) 
      {
        $idGrupo = $this->getGrupoProducto($detalle->id_producto);

        if ($idGrupo == 1)
        {
          $poseeTicket = 1;
        }
      }
      
      if ($poseeTicket == 0)
      {
        unset($facturas[$key]);
      }
    }
    $facturasTickets = [];
    foreach ($facturas as $key => $factura) 
    {
      $facturasTickets[$indice] = $factura;
      $indice = $indice + 1;
    }
    // dd($fac);
// dd(count($facturas));
    // echo "<pre>";
    // print_r($facturas->toArray());
    // die;
    // $facturas = $facturas->toArray();
    // ksort($facturas);
    // dd($facturas);
    return response()->json(['data'=>$facturasTickets]);
   }

        
   private function getGrupoProducto($idProducto){     
      $grupoProducto = Producto::where('id',  '=', $idProducto)->first();
      $id_grupo_producto = $grupoProducto->id_grupos_producto;
      return $id_grupo_producto;
    }


 private function  responseFormatDatatable($req){

    $datos = array();
      $data =  new \StdClass;
    // if(!isset($req->formSearch)){ 
    foreach ($req->formSearch as $key => $value) {
     $n = $value['name'];
         $datos[$value['name']] = $value['value'];
         $data-> $n = $value['value'];
    }
  // }

    return $data;

 } 

	public function verDetalleTicket($id)
  {
    $tickets = DB::select('SELECT * FROM vw_ticket WHERE id_empresa = '.$this->getIdEmpresa().' AND id = '.$id.' LIMIT 1 ');
    if(!isset($tickets[0]->id)){
      flash('El ticket solicitado no existe. Intentelo nuevamente !!')->error();
      return redirect()->route('home');
    }

    if($tickets[0]->fecha_pago_bsp)
    {
  		$fecha_s = explode("-", $tickets[0]->fecha_pago_bsp);
      $fecha_pago = $fecha_s[2]."/".$fecha_s[1]."/".$fecha_s[0];

      $fecha_e = explode("-", $tickets[0]->fecha_emision);
      $fecha_emision = $fecha_e[2]."/".$fecha_e[1]."/".$fecha_e[0];
    } 
    else 
    {
      $fecha_pago =""; 
      $fecha_emision ="";     
    }  

    $btn = $this->btnPermisos('','verDetalleTicket');
    $btns = $this->btnPermisos('','verEsitarTicket');

    if(empty($btns)){
      $permiso = 0;
    }else{
      $permiso = 1;
    }

    $op = null;
    
    //Ver si se pago con TC
    if($tickets[0]->pago_tc == 'TC'){
      $libro_compra = LibroCompra::where('id_tipo_documento_hechauka',13)->where('id_documento',$tickets[0]->id)->first();
      $op = OpDetalle::with('opCabecera')->where('id_libro_compra',$libro_compra->id)->first()->opCabecera;
    }
    

		return view('pages.mc.tickets.ver', compact('tickets', 'fecha_emision', 'fecha_pago','btn','permiso','op'));
	}

  public function addUser($id)
  {
    $personas = Persona::whereIn('id_tipo_persona', [3, 4])->where('id_empresa', $this->getIdEmpresa())->get();
    $ticket = Ticket::find($id);
    
    return view('pages.mc.tickets.user', compact('ticket','personas'));
  }

  public function saveUser(Request $request, $id_ticket)
  {
    $ticket = Ticket::find($id_ticket);
    
    try
      {
        $ticket->update([
          $ticket->id_vendedor = $request->input('id_vendedor')
        ]);

        flash('¡Se ha agregado el vendedor exitosamente!')->success();
        return redirect()->route('indexTicket');
      } 
      catch(\Exception $e)
      {
        flash('No se ha podido guardar. Intente nuevamente.')->error();
        return redirect()->back();
      }     
  }



    private function getIdUsuario(){
        
        return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
  }//function

    private function btnPermisosTicket($parametros, $vista){
     $btn = DB::select("SELECT pe.* 
                          FROM persona_permiso_especiales p, permisos_especiales pe
                          WHERE p.id_permiso = pe.id 
                          AND p.id_persona = ".$this->getIdUsuario() ." 
                          AND pe.url = '".$vista."'");

    $htmlBtn = '';
    $boton =  0;
    $idParametro = '';

  // dd( $btn );

  if(!empty($btn)){
    $boton = 1;
  }
   return $boton;
  }


    public function btnPermisos($parametros, $vista){
   /*echo "</pre>";
    print_r($parametros);
    echo "</pre>";
    print_r($vista);*/
    $btn = DB::select("SELECT pe.* 
                          FROM persona_permiso_especiales p, permisos_especiales pe
                          WHERE p.id_permiso = pe.id 
                          AND p.id_persona = ".$this->getIdUsuario() ." 
                          AND pe.url = '".$vista."'");

    $htmlBtn = '';
    $boton =  array();
    $idParametro = '';

  // dd( $btn );

  if(!empty($btn)){

  foreach ($btn as $key => $value) {
    $idParametro = '';
    $ruta = 'factour.'.$value->accion;
    $htmlBtn = '';

     //LLEVA PARAMETRO
    if($value->lleva_parametro == '1'){
   
    foreach ($parametros as $indice=>$valor) { 
       
      if($indice == $value->nombre_parametro){

        $idParametro = $valor;
      }
    }



    //PARAMETRO OCULTO EN DATA
    if($value->parametro_oculto == '1'){
       $htmlBtn = "<a role='button' class='".$value->clase."' data-btn='".$idParametro."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    } else {
       $htmlBtn = "<a role='button' href='".route($ruta,['id'=>$idParametro])."' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    }

    } else {
      $htmlBtn = "<a role='button' href='#' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i>".$value->texto_alternativo." </a>"; 
    }

    
    $boton[] = $htmlBtn;
  }



   return $boton;
 
   } else {
    return array();
   }



}//function


public function asignarFactura(Request $request)
{
   try
      {   
           $total_costo_ticket = 0;
           $ticket_asignados = Ticket::where('id_ticket_asignado', $request->input('id_ticket'))->get(); 

           foreach($ticket_asignados as $tickets){
               $ticket_costo = DB::select('SELECT get_costo_ticket(?)', [$tickets->id]);
               $total_costo_ticket = $total_costo_ticket + (float)$ticket_costo[0]->get_costo_ticket;
           }

           $ticket_costo = DB::select('SELECT get_costo_ticket(?)', [$request->input('id_ticket')]);
           $total_costo_ticket = $total_costo_ticket + (float)$ticket_costo[0]->get_costo_ticket;

           $total_costo_ticket_factura = DB::select("SELECT coalesce(sum(costo_proveedor), 0) AS total  
                                                    FROM facturas_detalle
                                                    WHERE id_factura = ?
                                                    AND activo = true 
                                                    AND id_producto in (SELECT id FROM productos WHERE id_grupos_producto = 1)",[$request->input('id_factura')])[0]->total;

           $total_tickets_factura = DB::select("SELECT coalesce(SUM(senha_paquete),0) as total
                                                FROM tickets 
                                                WHERE id_tipo_ticket = 2
                                                AND id_factura = ?",[$request->input('id_factura')])[0]->total;

           $total_costo_factura =  (float)$total_costo_ticket_factura + (float)$total_tickets_factura;

           if($total_costo_ticket <= $total_costo_factura){
                     DB::table('tickets')->where('id',$request->input('id_ticket'))
                       ->update([
                                 'id_factura'=>$request->input('id_factura'),
                                 'asignado'=>true,
                                 'id_estado' => 27
                               ]);
                     $mensaje = new \StdClass; 
                     $mensaje->id = $request->input('id_ticket');
                     $mensaje->status = 'OK';
                     $mensaje->mensaje = 'Se ha asignado la factura al ticket'; 
           }else{
               $mensaje = new \StdClass; 
               $mensaje->id = 0;
               $mensaje->status = 'ERROR';
               $mensaje->mensaje = "El monto del costo del ticket es mayor al costo de los productos aereos en la factura"; 
               $mensaje->tipo_facturacion = 0;
           }                    
      return json_encode($mensaje);
   }
    catch(\Exception $e)
    {     
      Log::error($e);
      $mensaje = new \StdClass; 
      $mensaje->id = 0;
      $mensaje->status = 'ERROR';
      $mensaje->mensaje = "Error al intentar asignar la factura al ticket"; 
      $mensaje->tipo_facturacion = 0;
      return json_encode($mensaje);
    }  
  } 


 public function guardarAsignacionTicket(Request $request)
  {
    try{     
       DB::table('tickets')
                    ->where('id',$request->input('ticket_asignar'))
                    ->update([
                          'id_ticket_asignado'=>$request->input('ticke_asignado'),
                          'asignado'=>true
                          ]);
        $mensaje = new \StdClass; 
        $mensaje->id = $request->input('ticke_asignado');
        $mensaje->status = 'OK';
        $mensaje->mensaje = 'Se ha asignado el Ticket'; 
        return json_encode($mensaje);
      }
      catch(\Exception $e)
      {     
          $mensaje = new \StdClass; 
          $mensaje->id = 0;
          $mensaje->status = 'ERROR';
          $mensaje->mensaje = "Error al intentar asignar el Ticket"; 
          $mensaje->tipo_facturacion = 0;
          return json_encode($mensaje);
      }  
  }//function

 public function getImpuestos(Request $request)
  {
    $impuestos = TicketImpuesto::where('id_ticket', $request->input('ticke_asignado'))
                       ->get();
    return json_encode($impuestos);                   
   }

  public function subir(Request $request){

    return view('pages.mc.tickets.subirAir');

  }

  public function recibirAir(Request $request){

    $procesado = false;
    Log::info('=========================<ticket> ==============================');
    try {

      //Primero vamos a guardar antes de todo 
      $ticket_ndc = new TicketNdc;
      $ticket_ndc->contenido = json_encode($request->all());
      $ticket_ndc->fecha_alta = date('Y-m-d H:i:00');
      $ticket_ndc->nombre_archivo = $request->input('nombre_archivo');
      $ticket_ndc->tipo = $request->input('tipo');
      $ticket_ndc->procesado = 'no';
      $nombreArchivo = explode('.',$request->input('nombre_archivo'));
      if(isset($nombreArchivo[1])){
          if($nombreArchivo[1] == 'AO'){
              $ticket_ndc->id_empresa = 21;
          } else {
            $ticket_ndc->id_empresa = $request->input('id_empresa');
          }

      }else{
        $ticket_ndc->id_empresa = $request->input('id_empresa');
      }
      $ticket_ndc->save();
      $id = $ticket_ndc->id;

      //Validaciones basicas
      if(!$request->input('id_empresa')){
        throw new \Exception('Falta id_empresa');
      }

      if(!$request->input('tipo')){
          throw new \Exception('Falta tipo de archivo air');
      }

      if(!$request->input('nombre_archivo')){
        throw new \Exception('Falta nombre del archivo air');
      }

      if(!$request->input('air')){
        throw new \Exception('Falta archivo air base_64');
      }

      Log::info('Ticket:'.$request->input('nombre_archivo'));



      if($request->input('tipo') == 'amadeus'){
        $procesado =  $this->recibirAirAmadeus($request);
      } 
      if($request->input('tipo') == 'sabre'){
       $procesado = $this->recibirAirSabre($request);
      }

      if($request->input('tipo') == 'copa'){
   
        $procesado = $this->recibirAirCopa($request);
      }
 
      //Solamente si el proceso retorna true ya sea por ser voideado o procesado
      if($procesado){
        $ticket_ndc = TicketNdc::find($id);
        $ticket_ndc->procesado = 'yes';
        $ticket_ndc->save();
      }
   
      
    } catch (Exception $e) {
      Log::error($e);
      Log::info('<!> =========================<ticket/> ============================== <!>');
      return response(500)->json(['status' => 'error' , 'msg' => $e->getMessage() ],500);
    }
    
    Log::info('=========================<ticket/> ==============================');
    return response()->json([ 'status' => 'success'],200);
  }

  private function recibirAirCopa(Request $request){
    
    Log::info('<==== Inicia AirCopa ==> archivo --> '.$request->input('nombre_archivo').' ===>');

    $contenidoAir = $request->input('air');
    $contenido = base64_decode($request->input('air'));
    $nombre_archivo = $request->input('nombre_archivo');

    $nombreArchivo = explode('.',$request->input('nombre_archivo'));


    /**
     * Identificar la empresa segun el archivo, los que no tienen extension son de DTP
     * Esto porque ahora al momento de este cambio esta implementacion de copa se ejecuta en los servidores de DTP
     */
    switch ($nombreArchivo[1]) {
      case 'AO': $id_empresa = 21; break; //Oais
      case 'BP': $id_empresa = 17; break; //Boarding Pass
      case 'GT': $id_empresa = 23; break; // Go Travel
      default: $id_empresa = $request->input('id_empresa'); break;
    }

 
    //Validamos para no recibir archivos vacios
    if(!$contenido){
      throw new \Exception('El contenido air no es valido o la codificación base 64 tiene errores');
    }

    //Convertimos en lineas para la lectura del regex
    $contenido  = str_replace("||","\r", $contenido); //Agregamos saltos de linea
    $contenido  = preg_replace("/(\\r)/","", $contenido); //Quitamos los espacios iniciales
    $contenido  = preg_replace("/(\\n)/","\r\n", $contenido); //Corregimos la falta de /r

    $error_proceso = false;
    $penalidad = false;
    $fecha_creacion = '';
    $proveedor_id = 0;
    $facial = 0.00;
    $moneda = null; 
    $id_usuario = null;
    $usuarioCopa= "";
    $pasajeroMedio= "";
    $precio =0.00;
    $marcadorSegmento = 0;
    $destino = null;
    $origen = null;
    $fechaOrigen ="";
    $fechaDestino ="";
    $pnr ="";
    $nroTicket ="";
    $tasaYq  = 0.00;
    $pasajero = [];
    $ticketNro = [];
    $bandera = 0;
    $estado = 0;
    $marcadorD = 0;
    $nroTicketVoid = "";
    $monedaFacial = null;
    $ttal = [];
    $tasa = 0.00;
    $cotizacion ="";
    $contador = 0;
    $iva= 0.00;
    $pasaj = null;
    $comisionTotal = 0.00;
    $comision = 0.00;
    $ivaComision = 0.00;
    $key = 0;
    $anho_emision = '';
    $mes_emision = '';
    $forma_pago = '';
    $personas = null;
    $id_tipo_transaccion = 4; //tipo normal ticket
    $codigo_iata_aerolinea = null; //Codigo iata de 3 digitos
    $identificacion_reemision = false;
    $office_id = null;
    $tipo_tarifa = 'Privada';

      /**
        * Identificar si es un ticket voideado
        * Identificar la cantidad de pasajeros para generar un ticket
        */

      //Preguntar si el ticket esta en estado voideado
    $nom_arch = $request->input('nombre_archivo');

    $buscar = "VOID";
    $resultado = strpos($contenido, $buscar);
    if($resultado !== FALSE){
          $estado = 1;
          $error_proceso = true;
    }else if(empty($estado)){
      $estado = 0;
    }

    $buscar = "CANX";
    $resultado = strpos($nom_arch, $buscar);
    if($resultado !== FALSE){
          $estado = 1;
          $error_proceso = true;
          $paraVoid = explode('-',$nom_arch);
          DB::table('tickets')->where('numero_amadeus',$paraVoid[1])
              ->update([
                        'id_estado'=>33
                    ]);

           return false;
    }

    if($estado == 0){
      /**
        * ============= Identificar el codigo de office =================================
        * En el caso de Copa el office se encuentra en el nombre del archivo, el office 
        * vendria a ser el codigo de la empresa que emite el ticket
        * ===============================================================================
        */

    /*  preg_match('/(.*)_AIR/', $nombre_archivo, $ouput_office);
      echo '<pre>';
      print_r($ouput_office);
      $office = $ouput_office[1];
      $office_data = TicketOffice::where('codigo', $office)->where('id_empresa',$id_empresa)->first();
      if($office_data){
        $office_id = $office_data->id;
      } else {
        Log::error('No se encuentra el office del ticket en BD - M0');
      }*/
      

      /**
        * Un ticket puede contener tickets normales como tickets EMD
        */

      //Identificar Ticket EMD
      if(preg_match('/^EMD/m',$contenido)){
          $ndc_proceso = $this->emdAirAmadeus($contenido,$request,$contenidoAir, $office_id);

          //Validar si continuar con el proceso de insert de ticket normal
          if(!preg_match_all('/^T\-K.*/m', $contenido)){
          return $ndc_proceso;
          }  
      }

      //------------TICKET NORMAL ---------------------
    
      //Busca la linea que empieza con T-K y termina con punto y coma en 2 o 4 espacios espacios
      //Busca el usuario copa
      preg_match('/(T\-K).*(;|\s{2,4})?/', $contenido, $output_array_usuario_copa);

      if(count($output_array_usuario_copa)){
        $codigo_iata_aerolinea = preg_replace('/T\-K|\s|\-\d.*/', '', $output_array_usuario_copa[0]); //Quitar las siglas TK-, espacios y saltos y guion con numeros

        $iata_aerolinea = Aerolinea::where('codigo3d',$codigo_iata_aerolinea)->first();

          if($iata_aerolinea){
            $personas = Persona::where('codigo_iata', $iata_aerolinea->codigo_iata)
            ->where('id_empresa', $id_empresa)
            ->first(['id']);
          } else {
            Log::info('No se pudo identificar al proveedor aerolinea en BD Aerolinea- Linea T-K =>'.$codigo_iata_aerolinea);
          }
        
        if(isset($personas->id)){
          $proveedor_id = $personas->id;
        }else{ 
          $proveedor_id = 0;
          Log::info('No se pudo identificar al proveedor aerolinea en BD - Linea T-K =>'.$codigo_iata_aerolinea);
        }
      } else {
      //  $error_proceso = true;
        Log::error('No se pudo identificar los numeros de tickets T-K');
      }

      //Ticket Number
      preg_match_all('/^T\-K.*/m', $contenido, $output_array_tickets);

      if(isset($output_array_tickets[0])){
        foreach ($output_array_tickets[0] as $ticket) {
          $nroTicketBase = explode('-', $ticket);

          if(!isset($nroTicketBase[2])){
            $error_proceso = true;
            Log::error('No se pudo identificar el numero de ticket en la linea T-K');
          } else {
            $nroTicket = trim($nroTicketBase[2]);
            $ticketNro[]['nro'] = $nroTicket;
          }

        }
      } else {
      //  $error_proceso = true;
        Log::error('No se pudo identificar los numeros de tickets - numero de ticket T-K');
      }

      //Buscar cuando inicia con D- y que luego tenga numeros y que pueda terminar en punto y coma o espacios de 2 a 4
      //Fecha de emision del ticket
      preg_match('/^D\-(.*)/m', $contenido, $output_array); 
      if(isset($output_array[1])){
        $filtro_fecha = preg_replace('/\s+/', '', $output_array[1]); //Eliminar todo lo que no sea numero
        $fechas = explode(';',$filtro_fecha);

        if(isset($fechas[2])){
            //Tomamos la tercera fecha 
            $fecha_emision = $fechas[2];

            $anho_emision = "20".substr($fecha_emision, 0, 2);
            $mes_emision = substr($fecha_emision, -4, 2);
            
            $fecha_creacion= "20".substr($fecha_emision, 0, 2)."-".substr($fecha_emision, -4, 2)."-".substr($fecha_emision, -2, 4);
          } else {
       //     $error_proceso = true;
            Log::error('No se pudo identificar la fecha de emision de ticket (2) D-');
          }
        } else {
     //     $error_proceso = true;
          Log::error('No se pudo identificar la fecha de emision de ticket (1) D-');
        }

        /**
         * ============================== IDENTIFICAR LAS TARIFAS ==============================
         * 
         * >Detalles de una tarifa publica:
         *   La primera emision  de un ticket se identifica con la letra F (first issue) que podria ser ejemplo K-F
         *   Cuando se realiza una reemision de una tarifa publica se identifica con la letra R (reissue) que podria ser ejemplo K-R
         *   El desglose de la tarifa para primera emision suele venir con las iniciales de KFTF
         *   El desglose de la tarifa para reemision suele venir con las iniciales de KFTR
         * >Detalles de una tarifa privada:
         *   La primera emision de un ticket se identifica con la letra I (issue) que podria ser ejemplo KS-I
         *   Cuando se realiza una reemision de una tarifa privada se identifica con la letra Y (reissue) que podria ser ejemplo KS-Y
         *   El desglose de la tarifa para reemision suele venir con las iniciales de KSTY
         *   El desglose de la tarifa para primer emision suele venir con las iniciales de KSTI
         * 
         * ============================== IDENTIFICAR LAS TARIFAS ==============================
         */
    
      if (!$error_proceso) {
          $tarifa_data = $this->identificarTarifasAmadeus($contenido);

          //Identificacion del tipo de ticket
          $identificacion_reemision = $tarifa_data['identificacion_reemision'];
          $id_tipo_transaccion = $tarifa_data['id_tipo_transaccion'];

          //Tarifas
          $moneda = $tarifa_data['moneda'];
          $precio = $tarifa_data['precio'];
          $facial = $tarifa_data['facial'];
          $tipo_tarifa = $tarifa_data['tipo_tarifa'];

      }

      if(!$error_proceso && $precio > 0.00){
        $impuestos_data = $this->identificarDesgloseImpuestosAmadeus($contenido);

        //Impuestos
        $error_proceso = $impuestos_data['error_proceso'];
        $iva_moneda = $impuestos_data['iva_moneda'];
        $iva = $impuestos_data['iva'];
        $tasaYq = $impuestos_data['tasaYq'];
        $tasa = $impuestos_data['tasa'];
      }

      /**
        * En la linea TAX podemos determinar si el total de impuestos ya esta pagado
        * El tercer valor es una parte del total 
        * Los PD que vamos encontrando vamos a estar de tasa
        * Si el total del ticket no es mayor a cero no vamos a calcular impuestos
        * 
        * 
        * Lo que tenemos que hacer es machear con el desglose , porque si el total tiene PD entonces hay que volver  a calcular en base a los 
        * valores que no tienen PD aca
        * El total de las tasa es siempre XT
        */
      preg_match('/^TAX\-.*/m', $contenido, $output_array_impuesto_tax);
      if(!empty($output_array_impuesto_tax) && $precio > 0.00){
        $valores_impuesto = explode(';',$output_array_impuesto_tax[0]);
        $total_cancelado = false;

        foreach ($valores_impuesto as  $impuesto) {
            //Si el total que seria XT y  tiene PD entonces hay que recalcular la tasa en base a las tasas que no tienen pd en esta linea
          preg_match('/(USD|EUR|PYG|GBP)((\d+)(\.)(\d+))/', $impuesto,$arr_precios); //Buscamos si hay valores con el patron moneda y valor
          $total = preg_match('/XT/', $impuesto); 
          $pd = preg_match('/PD/', $impuesto); 
          if($total && $pd){
            $iva_moneda = '';
            $iva = 0;
            $tasaYq = 0;
            $tasa = 0;
            $total_cancelado = true; //marcamos que el total de la linea ya esta cancelado tenemos que volver recalcular el total
          }
        }
        $arr_precios = [];

        //Vamos a ignorar siempre el ultimo valor porque es el total
        array_pop($valores_impuesto);
        $total_item_tax = count($valores_impuesto);
        $cant_vueltas = 0; //Iniciamos en uno porque son 3 elementos y un espacio al final 

        foreach ($valores_impuesto as  $impuesto) {
        

          //Si el total que seria XT y  tiene PD entonces hay que recalcular la tasa en base a las tasas que no tienen pd en esta linea
          preg_match('/(USD|EUR|PYG|GBP|PD)((\s+)?(\d+)(\.)(\d+))/', $impuesto,$arr_precios); //Buscamos si hay valores con el patron moneda y valor

          $py = preg_match('/PY/', $impuesto); //Verificamos si ese valor posee el impuesto PY
          $yq = preg_match('/YQ/', $impuesto); //Verificamos si ese valor posee el impuesto YQ
          $pd = preg_match('/PD/', $impuesto); //Verificamos si ese valor posee el impuesto PD
          

          if(empty($arr_precios) || !$arr_precios){
            continue;
          }

          if($total_cancelado){


            //PD es pagado
            if(!$pd){

            $tasa += (float)$arr_precios[2]; //marcamos que el total de la linea ya esta cancelado tenemos que volver recalcular el total

            //Impuesto PY
              if($py){
                $iva_moneda = $arr_precios[1];
                $iva += (float) $arr_precios[2];
              }

              //Impuesto YQ
              if($yq){
                $tasaYq += (float) $arr_precios[2]; 
              }

            }// PD


          } else {


          if($pd){
            //Impuesto PY
              if($py){
                $iva -= (float) $arr_precios[2];
              }
              //Impuesto YQ
              if($yq){
                $tasaYq -= (float) $arr_precios[2]; 
              }
              $tasa -= $arr_precios[2]; //marcamos que el total de la linea ya esta cancelado tenemos que volver recalcular el total
            }// PD
          } //total cancelado
        }//foreach



      } else {
        // $error_proceso = true;
        // Log::info('El ticket no posee la linea de impuestos TAX-');
      }
 
      //Linea de pasajeros
      preg_match_all('/^I\-.*/m', $contenido, $output_array_pasajero);
      if(!empty($output_array_pasajero)){
      foreach ($output_array_pasajero[0] as $linea_pasajeros) {
          $pasajeroBase = explode(';', $linea_pasajeros);
          if(isset($pasajeroBase[1])){  
            $pasajeros = substr($pasajeroBase[1], 2);
            $pasajeroMedio = str_replace('/', ' ',$pasajeros);
            $pasajero[]['nombre'] = $pasajeroMedio;
          }
        }
      } else {
      //  $error_proceso = true;
        Log::error('El ticket no posee pasajeros I-');
      }
   

      //PNR del ticket
      preg_match('/^MUC1A.*/m', $contenido, $output_array_pnr);
      if(!empty($output_array_pnr)){
          $baseBase = explode(';', $output_array_pnr[0]);
            $pnrBase = $baseBase[0];
            $baseB = explode(' ', $pnrBase);
            $cadena = substr($baseB[1], 0, -3);
            $pnr = $cadena;
      } else {
       // $error_proceso = true;
        Log::error('El ticket no posee PNR MUC1A');
      }

      /**
        * Segmentos del pasajero son los destino en escala
        * Aca vamos a tomar la primera fecha y luego la ultima fecha 
        * Cada segmento tiene dos fecha de embarque y desembarque con la hora pero sin el año
        * La venta de pasaje siempre es a futuro como minimo de 6 a 11 meses en adelante a la fecha de emision
        * 
        * 
        * Porque usamos /m
        * https://stackoverflow.com/questions/22438038/regex-expression-at-beginning-of-line-or-not
        * By default, PCRE treats the subject string as consisting of a single "line" of characters (even if it actually contains several newlines). The "start of line" metacharacter (^) matches only at the start of the string, while the "end of line" metacharacter ($) matches only at the end of the string, or before a terminating newline (unless D modifier is set). This is the same as Perl. When this modifier is set, the "start of line" and "end of line" constructs match immediately following or immediately before any newline in the subject string, respectively, as well as at the very start and end. This is equivalent to Perl's /m modifier. If there are no "\n" characters in a subject string, or no occurrences of ^ or $ in a pattern, setting this modifier has no effect.
        */

      preg_match_all('/^H\-.*/m', $contenido, $output_array_segemento);
      if(isset($output_array_segemento[0])){
        $last = count($output_array_segemento[0]) -1;
        foreach ($output_array_segemento[0] as $key => $segmento) {
          $voids = strpos($segmento, "VOID;");
          if(!$voids){ 

            if(!$fechaOrigen){
              $segmento_explode = explode(';', $segmento);
              $origen = substr($segmento_explode[1], 4, 3); //Ciudad origen
              /**
                * Vamos a encontrar las fechas de la linea que tienen este patron, nos interesa la fecha de embarque el primero
                * El patron dice que se busca que los primeros sean dos numeros seguido de alguna de las opciones del grupo, y se pide buscar solo una coincidencia
                * */
              preg_match('/(\d{2})(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)/', $segmento, $output_arr_seg);

              if(empty($output_arr_seg)){
               // $error_proceso = true;
                Log::error('No se pudo identificar la fecha del segmento de pasajero fecha origen');
              } else {
                $dia = $output_arr_seg[1];
                $mes = $this->mes($output_arr_seg[2]);
                

                $mes_vuelo = (int)$mes;
                $mes_emision_f = (int)$mes_emision;
                $anho = $anho_emision;

                if($mes_vuelo < $mes_emision_f){
                  $anho = $anho_emision + 1;
                }

                $fechaOrigen = $anho.'-'.$mes.'-'.$dia;
              }
              

        
            }
            
            //Leer ultimo segmento la segunda fecha
            if($last == $key){
            
              $segmento_explode = explode(';', $segmento);
              $destino = substr($segmento_explode[1], 4, 3);
              /**
                * Vamos a encontrar las fechas de la linea que tienen este patron, nos interesa la fecha de desembarque el segundo
                * El patron dice que se busca que los primeros sean dos numeros seguida de alguna de las opciones del grupo, y se pide buscar todas las coicidencias
                * */
              preg_match_all('/(\d{2})(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)/', $segmento, $output_arr_seg);

              if(empty($output_arr_seg)){
             //   $error_proceso = true;
                Log::error('No se pudo identificar la fecha del segmento de pasajero fecha destino');

              } else {
                    
                    $dia = $output_arr_seg[1][1];
                    $mes = $this->mes($output_arr_seg[2][1]);
                
                  
                    $mes_vuelo = (int)$mes;
                    $mes_emision_f = (int)$mes_emision;
                    $anho = $anho_emision;
                    /**
                      * Determinar el año: si el mes de embarque es mayor al mes  de emision es del año de emision
                      * Si el mes de embarque es menor al mes de emision es del año sgte
                      */
                    if($mes_vuelo < $mes_emision_f){
                      $anho = $anho_emision + 1;
                    }

                    $fechaDestino = $anho.'-'.$mes.'-'.$dia;
              }
            
            }
        
          }//void

        }//for

      } else {
      //  $error_proceso = true;
        Log::error('Error el ticket no posee segmento de pasajeros');
      }//output_array_segemento


      //Identificar forma de pago
      //Verificamos si la linea inicia con FP
      preg_match_all('/^FP.*/m', $contenido, $output_array_fp);
    
      if(isset($output_array_fp[0][0])){
        foreach ($output_array_fp[0] as $key =>  $fp) {
          $fp = preg_replace('/(FP)|\s|(FPO)|\//','',$fp);

          //Si tiene punto y coma vamos a dividir para buscar el la forma de pago
          if(preg_match('/;/',$fp)){
            $arr_fp = explode(';',$fp);
            $fp = $arr_fp[0];
          }

          if($fp == Ticket::FORMA_PAGO_CASH){
              $pasajero[$key]['forma_pago']= $fp; 
          } else {
            Log::error('Forma de pago no identificado <!>');
          }
          
        }
        
      } else {
        Log::info('No tiene forma de pago');
      }

      //Identificar al emisor del ticket de agencia
   //   preg_match('/^C\-.*/m', $contenido, $pos);
      preg_match('/^FS TICKET.*/m', $contenido, $pos);

      if(!empty($pos)){
      //  preg_match('/(\w{6})(\w{2})\-(\w{6})(\w{2})/',$pos[0],$usuario_copa);
        $usuario_copa = explode('AGENT:',$pos[0]);
        if(isset($usuario_copa[1])){
          $usuarioBase = trim($usuario_copa[1]);
          $base_usuario = explode(' ',$usuarioBase);
          $usuarioCopa = $base_usuario[0];
          $persona_copa = Persona::where('cod_copa',$usuarioCopa)->first();
          if($persona_copa){
            $id_usuario = $persona_copa->id;
          } else {
            Log::info('El usuario copa no se ecuentra en BD personas. =>'.$usuarioCopa);
          }
        }

      } else {
        Log::info('No posee linea de usuario Copa - Linea C-');
      }

      //Identificar la comision
      preg_match_all('/^FM\*\w+\*(\d+(\.00)?);?/m', $contenido, $pos);
      if(!empty($pos[1])){
        foreach ($pos[1] as $key => $fm) {
            $comision_porcentaje = (float)$fm;
            if($comision_porcentaje > 0.00){
              $pasajero[$key]['comision'] = $comision_porcentaje;
            }
        }
      }

      /**
        * Calcular Facial en base al total del ticket y las tasas
        * El total debe ser mayor a 0
        * 
        */
    
      if($identificacion_reemision && $precio > 0.00){
        $facial = $precio - $tasa;
      }
      
       if(empty($pasajero)){

        preg_match('/^A02.*/m', $contenido, $coincidencias);
        $coincidencias = (array)$coincidencias;
         foreach($coincidencias as $key=>$items){
              $base_pasajero = explode('  ',$items);
              $inicio_pasajero = explode(' ',$items);
              $pasajeros = str_replace('A02',' ', $base_pasajero[0]);
              $pasajero[]['nombre'] = $pasajeros;
              foreach($inicio_pasajero as $key=>$item_tickets){
                      if($item_tickets != ""){
                         if(strpos($item_tickets, "A02") === false && strlen($item_tickets) > 18){
                              $nroTicket = substr($item_tickets, 0, 10);
                             $ticketNro[]['nro'] = $nroTicket;
                         }else{
                              if(strlen($item_tickets) > 18){
                                $nro = strpos($item_tickets, "/");
                                if(!$nro){ 
                                    $ticketNro[]['nro'] = $nroTicket;
                                }
                              }
                         }
                    }
              }
          }

          /**
           * Obtiene la fecha del ticket, toma el ultimo elemento del array
           */
            preg_match_all('/^T51.*/m', $contenido, $fechasCreacion);
            if(isset($fechasCreacion[0][0])){
                 $base_fecha = explode('  ',$fechasCreacion[0][0]);
                 $ultimaFecha = end($base_fecha);
                 $baseFecha = substr(trim($ultimaFecha), 0, 7);
                 $fecha_emision = $baseFecha;
                 $fecha_creacion= "20".substr($fecha_emision, -2, 4)."-".$this->mes(substr($fecha_emision, -5, 3))."-".substr($fecha_emision, 0, 2);
            }else{
                 $fecha_creacion= date('Y-m-d');
            }  

            //DAD-1555 Se quita de nuevo la validacion de la fecha de emision
            // if($id_empresa == 17){
            //   //Para boarding pass que usa spark con copa vamos a modificar la fecha de emision por el error de fechas futuras
            //   $fecha_creacion= date('Y-m-d');
            // }


            preg_match_all('/^A04.*/m', $contenido, $output_array_segemento);
            if(isset($output_array_segemento[0])){
             $last = count($output_array_segemento[0]) -1;
             foreach ($output_array_segemento[0] as $key => $segmento) {
               $voids = strpos($segmento, "VOID;");
               if(!$voids){ 
                 if(!$fechaOrigen){
                   $segmento_explode = explode(' ', $segmento);
                   $origen = substr($segmento_explode[5], 4, 3); //Ciudad origen
                   /**
                     * Vamos a encontrar las fechas de la linea que tienen este patron, nos interesa la fecha de embarque el primero
                     * El patron dice que se busca que los primeros sean dos numeros seguido de alguna de las opciones del grupo, y se pide buscar solo una coincidencia
                     * */
                   preg_match('/(\d{2})(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)/', $segmento, $output_arr_seg);
   
                   if(empty($output_arr_seg)){
                    // $error_proceso = true;
                     Log::error('No se pudo identificar la fecha del segmento de pasaºjero fecha origen');
                   } else {
                     $dia = $output_arr_seg[1];
                     $mes = $this->mes($output_arr_seg[2]);
                     $mes_vuelo = (int)$mes;
                     $mes_emision_f = (int)$mes_emision;
                     $anho = date('Y');
   
                     if($mes_vuelo < $mes_emision_f){
                       $anho = $anho_emision + 1;
                     }
                     $fechaOrigen = $anho.'-'.$mes.'-'.$dia;
                   }
                 }
                 
                 //Leer ultimo segmento la segunda fecha
                 if($last == $key){
                 
                   $segmento_explode = explode(' ', $segmento);
  
                   $destino = substr($segmento_explode[5], 4, 3);
  
                   /**
                     * Vamos a encontrar las fechas de la linea que tienen este patron, nos interesa la fecha de desembarque el segundo
                     * El patron dice que se busca que los primeros sean dos numeros seguida de alguna de las opciones del grupo, y se pide buscar todas las coicidencias
                     * */
                   preg_match_all('/(\d{2})(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)/', $segmento, $output_arr_seg);
   
                   if(empty($output_arr_seg)){
                  //   $error_proceso = true;
                     Log::error('No se pudo identificar la fecha del segmento de pasajero fecha destino');
   
                   } else {
                         $dia = $output_arr_seg[1][0];
                         $mes = $this->mes($output_arr_seg[2][0]);
                         $mes_vuelo = (int)$mes;
                         $mes_emision_f = (int)$mes_emision;
                         $anho = date('Y');
                         /**
                           * Determinar el año: si el mes de embarque es mayor al mes  de emision es del año de emision
                           * Si el mes de embarque es menor al mes de emision es del año sgte
                           */
                         if($mes_vuelo < $mes_emision_f){
                           $anho = $anho_emision + 1;
                         }
                         $fechaDestino = $anho.'-'.$mes.'-'.$dia;
                   }
                 }
               }//void
   
             }//for
   
           } else {
           //  $error_proceso = true;
             Log::error('Error el ticket no posee segmento de pasajeros');
           }
         
           if(isset($base_fecha[0])){
               $base_proveedor = explode(' ',$base_fecha[0]);
               $codigo_iata = substr($base_proveedor[1], 0, 2);
           }else{
               preg_match_all('/^A\-.*/m', $contenido, $output_codigo);
               if(isset($output_codigo[0][0])){
                    $codigoBase = explode(';',$output_codigo[0][0]);  
                    $codigo_iata = substr($codigoBase[1], 0, 2);
               }else{
                    $codigo_iata = '';
               }
           }
  
           $persona_copa = Persona::where('codigo_iata',$codigo_iata)                 
                                   ->where('id_empresa', $id_empresa)
                                   ->first(['id']);
           if($persona_copa){
             $proveedor_id = $persona_copa->id;
           } else {
             Log::info('El usuario copa no se ecuentra en BD personas. =>'.$codigo_iata);
           }
  
           preg_match_all('/9050.*/m', $contenido, $pnr_base);
  
           if(!empty($pnr_base[0][0])){
              $pnrBase = explode(' ',$pnr_base[0][0]);
              $pnr = $pnrBase[1];
           }else{
                preg_match_all('/9059.*/m', $contenido, $pnr_base);
                if(!empty($pnr_base[0][0])){
                    $pnrBase = explode(' ',$pnr_base[0][0]);
                    $pnr = $pnrBase[1];
                }else{
                    $error_proceso = true;
                    Log::error('El ticket no posee PNR /9050');
                } 
           } 
           preg_match_all('/^A07.*/m', $contenido, $output_montos);

           $montosBase = explode(' ',$output_montos[0][0]);

           if($montosBase[5] == ''){
              if($montosBase[4] == ''){
                    $montosMoneda = substr($montosBase[0], -3);
                    $precio = $montosBase[12];
                    $facial = $precio;
              }else{
                $montosMoneda = substr($montosBase[4], -3);
                $precio = $montosBase[8];
                $facial = $precio; 
              }
            }else{
                 if($this->todosLosCaracteresSonNumeros($montosBase[10])) {
                    $montosMoneda = preg_replace('/[^A-Za-z]/', '', $montosBase[5]);
                    $precio =  $montosBase[10];
                    $facial =  preg_replace('/\D/', '', $montosBase[5]);;
                 }else{
                    $montosMoneda = preg_replace('/[^A-Za-z]/', '', $montosBase[5]);
                    $precio = $montosBase[10];
                    $facial =  preg_replace('/\D/', '', $montosBase[5]);;
               }
            }
  
           preg_match('/(USD|EUR|PYG|GBP)/', $montosMoneda,$moneda_base);

           $monedaBase =$moneda_base[1];
           if($monedaBase == 'PYG'){
               $moneda = 111;
           }elseif($monedaBase == 'USD'){
               $moneda = 143;
           }elseif($monedaBase == 'EUR'){
               $moneda = 43;
           }

     //      $facial = str_replace($monedaBase, '',$montosMoneda);

          preg_match_all('/^IT:.*/m', $contenido, $iva_base);
  
           $ivaBase = explode(' ',$iva_base[0][0]);
  
           foreach($ivaBase as $key=>$valor){
   
               if (strpos($valor, 'PY') !== false) {
                   $iva = str_replace('PY','',$valor);
                   $tasa = $tasa + (float)$iva;
               }else{
                   $valoresNumericos = preg_replace('/[^0-9.]/', '', $valor);
                   $tasa = $tasa + (float)$valoresNumericos;
               }
             }
  
               /**         $py = preg_match('/PY/', $impuesto); //Verificamos si ese valor posee el impuesto PY
  
                * Debe tener fecha de destino y origen
                * Debe tener una facial
                * Debe tener una comision
                */

              preg_match_all('/^FO230.*/m', $contenido, $output_fo230);
              if(!empty($output_fo230)){
                  preg_match_all('/^KFTF.*/m', $contenido, $output_array_impuestos);
                  if(!empty($output_array_impuestos[0])){
                        $impuesto = explode(';',$output_array_impuestos[0][0]);
                        $tas = 0;
                        foreach($impuesto as $key=>$tax){
                          if($tax != ""){
                              $valoresNumericos = preg_replace('/[^0-9.]/', '', $tax);
                              $tas = $tas + (float)$valoresNumericos;
                          }
                        }
                        $tasa = $tas;
                        $facial = $precio - $tas;
                       // $facial = $precio;
                  }else{
                        $impuestos_data = $this->identificarDesgloseImpuestosAmadeus($contenido);
                        //Impuestos
                        $error_proceso = $impuestos_data['error_proceso'];
                        $iva_moneda = $impuestos_data['iva_moneda'];
                        $tasaYq = $impuestos_data['tasaYq'];
                  }
              }
  
       }


    }//if estado


     if(!$error_proceso){
          if($estado == 0){
    
              if(count($pasajero)){

                foreach($pasajero as $key=>$pasaj){
                  $ivaComision = 0.00;
                  $comision = isset($pasaj['comision']) ? $pasaj['comision'] : 0.00;

                  //Validamos que tenga fechas
                if($facial > 0.00 && $comision > 0.00){

                    Log::info('Comision identificada > '. $comision_porcentaje);
                    Log::info('Facial identificada > '. $facial);


                    $comisionTotal = round(((float)$facial * (float)$comision)/100,2);
                    $ivaComision = round(($comisionTotal* 10)/100,2);
                    Log::info('Comision Calculada > '. $comisionTotal);
                }

                $forma_pago = isset($pasaj['forma_pago']) ? $pasaj['forma_pago'] : null;
                  
                $tickets = Ticket::where('numero_amadeus', trim($ticketNro[$key]['nro']))
                                    ->where('id_empresa',$id_empresa)
                                    ->first(['id']);

                if(!$tickets){  
                  if($moneda != 111){
                    $iva  = round($iva,2);
                    $tasaYq = round($tasaYq,2);
                    $facial = (float)$facial;
                    $tasa = round($tasa,2);
                  } else {
                    $iva  = round($iva);
                    $tasaYq = round($tasaYq);
                    $facial = round($facial);
                    $tasa = round($tasa);
                  }

                  $tasaComprobacion = (float)$precio - (float)$facial;

                  if((float)$tasaComprobacion != (float)$tasa){
                      $tasa = round((float)$tasaComprobacion);
                  }                  
                  
                  if($tasa < 0){
                    $tasa = 0;
                  }

                 if(substr($usuarioCopa, 0, 6) != 'CMTRAV'){  

                         $arrayInicio  =  
                                         [ 
                                           $fecha_creacion, //1
                                           $proveedor_id, //2
                                           $facial, //3
                                           0, // grupo 4
                                           $moneda, //int4, 5
                                           null,  // proforma 6
                                           substr($usuarioCopa, 0, 6), //7
                                           trim($pasaj['nombre']), //8
                                           $pnr, //9
                                           $iva,// float8, 10
                                           $id_empresa,//empresa 11
                                           $tasa,//"tasas",// float8, 12
                                           $tasaYq,// float8, 13
                                           0, // factura 14
                                           $comisionTotal,//"comision",// float8, 15
                                           $ivaComision,//"iva_comision",// float8, 16
                                           0,//"fee",// float8,  17
                                           25,//"id_estado",// int4,  18
                                           10,  //19
                                           date('Y-m-d'),//"fecha_alta",// date, 20
                                           $id_usuario,//"id_usuario",// int4, 21
                                           null, //"id_detalle_proforma" int4, 22
                                           trim($ticketNro[$key]['nro']), //23
                                           $precio,//"total_ticket",// float8, 24
                                           $id_tipo_transaccion,//"id_tipo_transaccion",// int4, 25
                                           $forma_pago,//"tipo_pago",// varchar, 26
                                           0,//"variacion_impuesto", //27
                                           0,//"senha_paquete", //28
                                           'A',//"origen del insert Automatico",// text, 29
                                           $destino,   // "destino",// text,30 
                                           $fechaOrigen,// date, 31
                                           $fechaDestino,// date,32 
                                           0,//"adultos",// int4,33
                                           0,//"ninhos",// int4,  34
                                           0,//"infantes",// int4, 35
                                           $contenidoAir, // int4, 36
                                           $request->input('nombre_archivo'),
                                           $request->input('tipo'), //String 
                                           $comision, //porcentaje_comision float
                                           $codigo_iata_aerolinea,
                                           $origen, //iniciio Origen
                                           $tipo_tarifa, //Tarifa publica, privada o EMD
                                           $office_id //Empresa que emite el ticket
                                   ];
                       
                           Log::info( json_encode($arrayInicio,JSON_PRETTY_PRINT));

                      

                           $inserto= DB::select('SELECT public."insert_into_tickets_air"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                           $arrayInicio); 

                            Log::info('<==================== El ticket ingreso a insert_into_tickets_air ====================>');   
                           }       
                                 
                         }  else {
                           Log::error('El ticket ya se encuentra en la BD y no sera actualizado');
                         }               
                                     
                     }//foreach  
   
                     //Fin del foreach de insert ticket
                     return true;
   
                   } else {
                        Log::error('No tiene pasajeros');
                   }
                     
               } else {  
   
                  Log::info('El ticket es voideado');
   
                 /**
                   * Buscamos que la linea inicie con T-K y con la variable multiline /m
                   */
                       preg_match_all('/^T\-K.*/m', $contenido, $output_array_tickets);

                       if(!empty($output_array_tickets[0])){
   
                         $total_ticket_file = count($output_array_tickets[0]);
                         $total_procesado = 0;
                 
                         foreach ($output_array_tickets[0] as $ticket) {
                         
                           $nroTicketBase = explode('-', $ticket);
                     
                           if(!isset($nroTicketBase[2])){
                             Log::error('El ticket no se pudo cancelar porque no se pudo identificar el numero de ticket en la linea T-K');
                           } else {
                               $nroTicket = trim($nroTicketBase[2]);
                               $tickets = Ticket::where('numero_amadeus', $nroTicket)
                               ->where('id_empresa',$request->input('id_empresa'))
                               ->first(['id']);
   
                               if(isset($tickets->id)){ 
                                     DB::table('tickets')->where('id',$tickets->id)
                                     ->update([
                                               'id_estado'=>33
                                             ]);
   
                                     Log::info('Ticket voideado '.$nroTicket);
                                     $total_procesado++;
                               } else {
                                 Log::error('El numero de ticket no se encuentra en la base de datos');
                               }
                           }
   
                     
                       }//foreach
   
                       if($total_procesado > 0 && $total_procesado == $total_ticket_file){
                         Log::info('Todos los tickets presentes fueron voideados');
                       } else {
                         Log::error('Solo fueron voideados '.$total_procesado .' DE '.$total_ticket_file);
                       }
   
   
                   } else {
                          preg_match('/^A02.*/m', $contenido, $output_array_tickets);
                         if(isset($output_array_tickets[0])){
                            $total_procesado = 0;
                            foreach( (array)$output_array_tickets[0] as $ticket) {
                                  $base_pasajero = explode('  ',$items);
                                  foreach($inicio_pasajero as $key=>$item_tickets){
                                        if(strpos($item_tickets, "A02") === false && strlen($item_tickets) > 18){
                                              $nroTicket = substr($item_tickets, 0, 10);
                                         }else{
                                              if(strlen($item_tickets) > 18){
                                                  $nroTicket = substr($item_tickets, 0, 10);
                                                }
                                        }
                                  }
                                  $tickets = Ticket::where('numero_amadeus', $nroTicket)
                                                    ->where('id_empresa',$request->input('id_empresa'))
                                                    ->first(['id']);
                                  if(isset($tickets->id)){ 
                                        DB::table('tickets')->where('id',$tickets->id)
                                            ->update([
                                                      'id_estado'=>33
                                                   ]);
                    
                                        Log::info('Ticket voideado '.$nroTicket);
                                        $total_procesado++;
                                  } else {
                                        Log::error('El numero de ticket no se encuentra en la base de datos');
                                  }
                            }
                        }
                   }
                     
               } //else 
   
           } else {
           Log::error('<==================== El ticket no fue procesado - '.$request->input('nombre_archivo').' ====================>');
           }
   
   
   
           return false;
     }

     private function todosLosCaracteresSonNumeros($cadena) {
      return preg_match('/^\d+$/', $cadena) === 1;
    }
    

  private function recibirAirSabre(Request $request){

    $contenidoAir = $request->input('air');
    $contenido = base64_decode($request->input('air'));

    // echo '<pre>';
    // print_r($request->input('nombre_archivo'));


    //Convertimos en lineas para la lectura del regex
    $contenido  = str_replace("||","\r", $contenido); //Agregamos saltos de linea
    $contenido  = preg_replace("/(\\r)/","", $contenido); //Quitamos los espacios iniciales
    $contenido_format  = preg_replace("/(\\n)/","\r\n", $contenido); //Corregimos la falta de /r

 

    $fecha_creacion = '';
    $fechaMes = '';
    $pasajeros = [];
    $fechaOrigen ="";
    $fechaDestino ="";
    $mes_emision = '';
    $anho_emision = '';
    

    $estado_cancelado = false;
    $error_proceso = false;
    $tipo_transaccion_ticket = '';
    $version_sistema_uir = '';
    $agency_city_code = '';
    $destino_code = '';
    $destino_name = '';
    $origen_code = '';
    $origen_name = '';
    $total_tickets = 0.00;
    $total_pasajeros = 0;

    $money_code = null;
    $moneda_id = null;
    $sabre_pnr="";
    $iva = 0.00;
    $tasaYq = 0.00;
    $tasas = 0.00;
    $facial = 0.00;
    $total = 0.00;

    $tipo_pasajero = '';
    $descripcion_tipo_pasajero = '';
    $tasa_1 = 0.00;
    $tasa_1_type = '';
    $tasa_2 = 0.00;
    $tasa_2_type = '';
    $tasa_3 = 0.00;
    $tasa_3_type = '';
    $total_tax_agency = 0.00;
    $aerolinea_iata = null; //Codigo iata de 3 lineas
    $numero_ticket = '';
    $comision = 0.00;
    $iva_comision = 0.00;
    $forma_pago = '';


    $passenger_names = '';
    $passenger_id = 0;
    $proveedor_id = 0;
    $usuario_sabre = null;
    $id_usuario = null;
    $tipo_emd = false;
    $tipo_tarifa = 'Publica';
    $office_id = null;
    $id_empresa = $request->input('id_empresa');


    // dd($contenido_format); 
    $estado_cancelado = preg_match('/.*M0.*VOID/',$contenido_format);

    preg_match('/.*M0.*/',$contenido_format,$data_m0);
          if(!empty($data_m0) && !$estado_cancelado){
            $tipo_transaccion_ticket = substr($data_m0[0],13,1);

            /**
             * F Ticket Enhanced Void IUR
             * G EMD Enhanced Void IUR
             * 5 VOID TICKET
             * 
             */
            if( $tipo_transaccion_ticket == 'F' || $tipo_transaccion_ticket == 'G' || $tipo_transaccion_ticket == '5'){
              $estado_cancelado = true;
            }
          }

    if($estado_cancelado){
            Log::info('=========================== TICKET VOIDEADO =============================='); 

            preg_match('/^M2.*/m',$contenido_format,$pos);
            if(!empty($pos)){
              $numero_ticket = trim(substr($pos[0],233,10));

              $tickets = Ticket::where('numero_amadeus', $numero_ticket)
              ->where('id_empresa',$id_empresa)
              ->first(['id']);
      
              if(isset($tickets->id)){ 
                    DB::table('tickets')->where('id',$tickets->id)
                    ->update([
                              'id_estado'=>33
                            ]);
      
      
                  return true;//procesado
      
      
              } else {
                Log::error('El numero de ticket no se encuentra en la base de datos - VOID');
              }

              
            } else {
              Log::error('No posee numero de ticket - VOID');
            }

        
   


    } else {

          // --------------------------- Linea M0 --------------------------------


           /**
           * Identificar la linea que contiene M0 y obtener fecha de creacion y PNR
           * 
           */
          preg_match('/.*M0.*/',$contenido_format,$data_m0);
          if(!empty($data_m0)){
            $prefijo_sistema = substr($data_m0[0],0,2);
            $dia = substr($data_m0[0],2,2);
            $mes = $this->mes(substr($data_m0[0],4,3));
            $hora = substr($data_m0[0],7,4);
            $mes_emision = (int)$mes;
            $anho_emision = (int)date('Y');
            $mes_actual = (int)date('m');

            /**
             * Restamos un año cuando el mes de emision es mayor al mes actual
             * */
            if($mes_emision > $mes_actual ){
              $anho_emision--;
            }


            $fecha_creacion = $anho_emision."-".$mes."-".$dia;

            $tipo_transaccion_ticket = substr($data_m0[0],13,1);
            $version_sistema_uir = substr($data_m0[0],14,2);
            $sabre_pnr = trim(substr($data_m0[0],53,8));
            $agency_city_code = trim(substr($data_m0[0],126,5));
            $usuario_sabre = trim(substr($data_m0[0],95,2));

             //Solo para DPT
             if($id_empresa == 1){
                $usuario_sabre = trim(substr($data_m0[0],133,2));
             }

            if($usuario_sabre){
              $persona_sabre = Persona::where('usuario_sabre',$usuario_sabre)
              ->where('id_empresa',$id_empresa)
              ->first();

              if($persona_sabre){
                $id_usuario = $persona_sabre->id;
              } else {
                Log::error('No se encuentra el usuario del ticket en BD - M0');
              }
            } else {
              Log::error('No se encuentra el usuario del ticket - M0');
            }


            $destino_code = trim(substr($data_m0[0],166,3));
            $destino_name = trim(substr($data_m0[0],169,17));
            $origen_code = trim(substr($data_m0[0],146,3));
            $origen_name = trim(substr($data_m0[0],149,17));
            $total_pasajeros = (int)trim(substr($data_m0[0],186,3));
            $total_tickets = (int)trim(substr($data_m0[0],189,3));
            $codigo_office = trim(substr($data_m0[0],88,4));

            //Solo para DPT
            if($id_empresa == 1){
              $codigo_office = trim(substr($data_m0[0],126,4));
            }
      
            //Obtener el id del office
            $office = TicketOffice::where('codigo',$codigo_office)
            ->where('id_empresa',$id_empresa)
            ->first(['id']);
            if($office){
              $office_id = $office->id;
            } else {
              Log::error('No se encuentra el office del ticket en BD - M0');
            }
          }

            // --------------------------- Linea M3 --------------------------------
            /**
             * Obtener las fechas de origen y destino
             */
            preg_match_all('/^M3.*/m',$contenido_format,$pos);
            if(isset($pos[0]) && !$error_proceso){
              foreach ($pos[0] as $m3) {
  
                //Buscar las fechas 
                preg_match('/(\d{2})(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)/', $m3, $fecha);
                $dia = $fecha[1];
                $mes_compare = (int)$this->mes($fecha[2]);
                $mes = $this->mes($fecha[2]);
                $anho = date('Y');
  
                  if(!$fechaOrigen){
  
                    if($mes_compare < $mes_emision){
                      $anho = $anho_emision + 1;
                    }
  
                    $fechaOrigen = $anho .'-'.$mes.'-'.$dia;
                  }
  
                  if($mes_compare < $mes_emision){
                    $anho = $anho_emision + 1;
                  }
  
                  $fechaDestino = $anho .'-'.$mes.'-'.$dia;
              }
            }

          if($tipo_transaccion_ticket == 'A'){
            $tipo_emd = true;
            Log::info('Es un ticket EMD '.$request->input('nombre_archivo'));
          }


          /**
           * Validar si es el tipo de ticket aceptado
           * 1 Invoice/Ticket
           * A Invoice/Ticket/EMD
           */
          if(!($tipo_transaccion_ticket == '1' || $tipo_transaccion_ticket == 'A')){
            $error_proceso = true;
            Log::error('Ticket rechazado porque no corresponde al tipo de transaccion aceptada - M0');

            if($tipo_transaccion_ticket == 'A'){
              $tipo_tarifa = 'EMD';
            }
          }
          
          if($total_pasajeros == 0){
            $error_proceso = true;
            Log::error('Ticket rechazado porque no contiene pasajeros - M0');
          }

          preg_match('/^M5.*/m',$contenido_format,$pos);
          if(!empty($pos) && !$error_proceso){
            preg_match('/\d+\/\w+\/(\w+)/',$pos[0],$pos);
            $forma_pago = $pos[1];
            if($forma_pago == 'CA' || $forma_pago == 'PCASH'){
              $forma_pago = Ticket::FORMA_PAGO_CASH;
            } else {
              Log::error('Forma de pago no encontrada - M5');
            }
          }

    

          // --------------------------- Linea M2 --------------------------------
          /**
           * Linea de tasas, codigo_iata y ticket
           * Puede venir mas de un ticket, se realiza foreach de los valores 
           */

           $data_passager_ticket = [];
          
          
          preg_match_all('/^M2.*/m',$contenido_format,$pos_m2); //Linea de tasas, codigo_iata y ticket
          preg_match_all('/^M1.*/m',$contenido_format,$pos_m1); //Nombre de los pasajeros

          if(!isset($pos_m2[0][0])){
            $error_proceso = true;
            Log::error('Error fatal al intentar obtener la linea M2');
            return false;
          }
          
          foreach ($pos_m2[0] as $key => $pos) {
            $avanzar_calculo_1 = true; //Avanzamos con este calculo si no es empresa DTP, ya que sabre es igual para todos
            $nro_pasajero_m2 = trim(substr($pos,2,2)); //Numero de pasajero

            //Solo para DPT
            if($id_empresa == 1){
                //Dividir las tarifas de MX segun los pasajeros (MX)
                /**
                 * El funcionamiento para encontrar la tarifa es tomar cada MX desde abajo para arriba y convertirlas en 
                 * lineas para luego aplicar expresiones para obtener los valores
                 */
                preg_match_all('/^MX.*/sm',$contenido_format,$mxs); //Nombre de los pasajeros
                $sgte_mx = true;
                $mx_lines_arr = [];
                $result_mx_string = $mxs[0][0];
                $avanzar_calculo_1 = false;
              
                while ($sgte_mx) {
                    //Tomar ultima MX
                    $sgte_mx = false;

                    $pattern = '/^MX.*?(?:\R(?!MX).*)*+(?:\R(?=MX).*)*+\Z/m';
                    if (preg_match($pattern, $result_mx_string, $matches)) {
                          //Por cada pasajero ya tenemos el desglose de impuestos y los totales
                        $data_mx = str_replace(array("\r\n", "\r", "\n","\r\n\f","\f"), '', trim(array_pop($matches)));
                        $nro_pasajero_mx = trim(substr($data_mx,7,2));
                        $mx_lines_arr[$nro_pasajero_mx][] = $data_mx;

                        //  dd($data_mx, $nro_pasajero_mx, $mx_lines_arr);
                        $sgte_mx = true;
                        $result_mx_string = preg_replace($pattern, '', $result_mx_string);
                    }
                }

                //Obtener las tarifas a partir de los MX filtrando por el numero de pasajero
                $mxs = $mx_lines_arr[$nro_pasajero_m2];
                //1. F = Fare, 2. X = Taxes, 3. T = Total
                preg_match_all('/((\d+)(\.)(\d+))(USD|EUR|PYG|GBP)/', $mxs[0],$arr_precios);
                $facial = $arr_precios[1][0];
                $money_code = $arr_precios[5][0]; //moneda de la facial
                $total_linea = $arr_precios[2][2];
                $tasas = $arr_precios[1][1];

                //Sacar con el foreach sobre los impuestos del MX[1]
                preg_match('/((\s+)?(\d+)(\.)(\d+))PY/', $mxs[1], $py); //Verificamos si ese valor posee el impuesto PY
                preg_match('/((\s+)?(\d+)(\.)(\d+))YQ/', $mxs[1], $yq); //Verificamos si ese valor posee el impuesto YQ
              
                if(isset($py[0])){
                  $iva += (float) trim($py[1]);
                }

                if(isset($yq[0])){
                  $tasaYq += (float) trim($yq[1]);
                }
            }
            


            //aquiiiii
            //------------------------------------------------------------------------------------------------


            if(!empty($pos) && !$error_proceso){
              $tipo_pasajero = trim(substr($pos,4,3));
              $descripcion_tipo_pasajero = $this->typePassenger($tipo_pasajero);
              $tipo_tarifa_tk = trim(substr($pos,217,1));
              $tipo_tarifa = $tipo_tarifa_tk ? 'Privada' : 'Publica';

              if($avanzar_calculo_1){
                  $money_code = trim(substr($pos,34,3));
                  $facial = (float)trim(substr($pos,37,8));
                  $total_linea = (float)trim(substr($pos,79,8));

      
                  //Equivalencia
                  $moneda_equivalente = trim(substr($pos,116,3));
                  $monto_equivalente = (float)trim(substr($pos,119,8));
      
                  if($moneda_equivalente && $monto_equivalente != '' ){
                    $money_code = $moneda_equivalente;
                    $facial = $monto_equivalente;
                  }
                
      
                  //identificar tasa 1 y sumar
                  $tasa_1 = (float)trim(substr($pos,46,7));
                  $tasa_1_type = trim(substr($pos,53,2));
      
                  $iva += $tasa_1_type == 'PY' ? $tasa_1 : 0;
                  $tasaYq += $tasa_1_type == 'YQ' ? $tasa_1 : 0;
                  
                  //identificar tasa 2 y sumar
                  $tasa_2 = (float)trim(substr($pos,56,7));
                  $tasa_2_type = trim(substr($pos,63,2));
      
                  $iva += $tasa_2_type == 'PY' ? $tasa_2 : 0;
                  $tasaYq += $tasa_2_type == 'YQ' ? $tasa_2 : 0;
      
                  //identificar tasa 3 y sumar
                  $tasa_3 = (float)trim(substr($pos,66,7));
                  $tasa_3_type = trim(substr($pos,73,2));
      
                  $iva += $tasa_3_type == 'PY' ? $tasa_3 : 0;
                  $tasaYq += $tasa_3_type == 'YQ' ? $tasa_3 : 0;
      
                  $tasas = $tasa_1 + $tasa_2 + $tasa_3;
      
                  $total_tax_agency = (float)trim(substr($pos,156,8));

              }


              $aerolinea_iata = trim(substr($pos,231,2));
           
                $proveedor = Persona::where('codigo_iata',$aerolinea_iata)
                ->where('id_empresa',$request->input('id_empresa'))->first();
                if($proveedor){
                  $proveedor_id = $proveedor->id;
                } else {
                  // $error_proceso = true;
                  Log::error('No se encuentra el proveedor en personas - M2');
                }
              
  
  
              $numero_ticket = trim(substr($pos,233,10));
                // dd($facial, $tasas);
              $comision = (float)trim(substr($pos,136,8));
  
              //Si el total no es mayor a cero el resto de los valores debe ser cero
              if($total_linea > 0.0){
  
                $total = $tasas + $facial;
  
              } else {
  
                //Si el total es cero vamos cerar todo
                $tasas = 0.0;
                $total = 0.0;
                $comision = 0.0;
                $iva = 0.0;
                $tasaYq = 0.0;
                $facial = 0.0;
                
              }
          
  
            
  
  
              // dd($facial, $total,$tasas);
            } 
  
            if(!$aerolinea_iata){
              $error_proceso = true;
              Log::error('No se encuentra aerolinea iata en linea - M2');
            }
            
            if(!$numero_ticket){
              $error_proceso = true;
              Log::error('No se identifico el numero de ticket - M2');
            }

            // --------------------------- Linea M1 --------------------------------

    
            if(!$error_proceso){
              $numero_pasajero = (int) $nro_pasajero_m2 - 1;
              $passenger_names = isset($pos_m1[0][$numero_pasajero]) ? $pos_m1[0][$numero_pasajero] : '' ;
              $passenger_names = trim(substr($passenger_names,4,64));
              $passenger_names = str_replace('/',' ',$passenger_names);
            }

              //----------------- PROCESO INSERT ------------------------------------


              if(!$error_proceso){

                $moneda_id = $this->getIdMoneda($money_code);
                if($comision){
                  $iva_comision = round(($comision * 10)/100);
                }
      
      
                    
                  $tickets = Ticket::where('numero_amadeus', $numero_ticket)
                                  ->where('id_empresa',$request->input('id_empresa'))
                                  ->first(['id']);

                  // echo '<pre>';
                  // print($tickets);
          
                  if(!isset($tickets->id)){  

                        $id_tipo_transaccion = $tipo_emd ? 14 : 4;

                        $arrayInicio  =  
                        [ 
                          $fecha_creacion, //1
                          $proveedor_id, //2
                          $facial, //3
                          null, // grupo 4
                          $moneda_id, //int4, 5
                          null,  // proforma 6
                          $usuario_sabre, //7
                          $passenger_names, //8 pasajero nombre
                          $sabre_pnr, //9
                          $iva,// float8, 10
                          $request->input('id_empresa'),//empresa 11
                          $tasas,//"tasas",// float8, 12
                          $tasaYq,// float8, 13
                          0, // factura 14
                          $comision,//"comision",// float8, 15
                          $iva_comision,//"iva_comision",// float8, 16
                          0,//"fee",// float8,  17
                          25,//"id_estado",// int4,  18
                          4,  //19
                          date('Y-m-d'),//"fecha_alta",// date, 20
                          $id_usuario,//"id_usuario",// int4, 21
                          null, //"id_detalle_proforma" int4, 22
                          $numero_ticket, //23
                          $total,//"total_ticket",// float8, 24
                          $id_tipo_transaccion,//"id_tipo_transaccion",// int4, 25
                          $forma_pago,//"tipo_pago",// varchar, 26
                          0,//"variacion_impuesto", //27
                          0,//"senha_paquete", //28
                          'A',//"origen del insert automatico",// text, 29
                          $destino_name,   // "destino",// text,30 
                          $fechaOrigen,// date, 31
                          $fechaDestino,// date,32 
                          null,//"adultos",// int4,33
                          null,//"ninhos",// int4,  34
                          null,//"infantes",// int4, 35
                          $contenidoAir, // int4, 36
                          $request->input('nombre_archivo'),
                          $request->input('tipo'), //tipo 
                          null, //porcentaje_comision float
                          $aerolinea_iata,
                          $origen_code, //iniciio Origen
                          $tipo_tarifa,
                          $office_id
                          ];

                          Log::info( json_encode($arrayInicio,JSON_PRETTY_PRINT));

                            // echo '<pre>';
                            // print_r($arrayInicio);

                            // echo '<pre>';
                            // print_r('SELECT public."insert_into_tickets_air"('.$fecha_creacion.','.$proveedor_id.','.$facial.',null,'.$moneda_id.',null,'.$usuario_sabre.','.$passenger_names.','.$sabre_pnr.','.$iva.','.$request->input('id_empresa').','.$tasas.','.$tasaYq.',0,'.$comision.','.$iva_comision.',0,25,4,'.date('Y-m-d').','.$id_usuario.',.null,'.$numero_ticket.','.$total.','.$id_tipo_transaccion.','.$forma_pago.',0,0,A,'.$destino_name.','.$fechaOrigen.','.$fechaDestino.',null,null,null,'.$contenidoAir.','.$request->input('nombre_archivo').','.$request->input('tipo').',null,'.$aerolinea_iata.','.$origen_code.','.$tipo_tarifa.','.$office_id);

                            $inserto= DB::select('SELECT public."insert_into_tickets_air"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                                          $arrayInicio);


                            Log::info('<==================== El ticket ingreso a insert_into_tickets_air : '.$numero_ticket.' ====================>');   
                          
          
      
                      } else {
                        $error_proceso = true;
                        Log::error('El ticket ya se encuentra registrado en la base de datos : '.$numero_ticket);
                      }
              } else {
                Log::error('Ticket rechazado por errores');
              }

              //Volver a resetear los valores
              $error_proceso = false;
              $money_code = null;
              $iva = 0.00;
              $tasaYq = 0.00;
              $tasas = 0.00;
              $facial = 0.00;
              $total = 0.00;
              $passenger_names = '';
              $numero_ticket = '';
              $comision = 0.00;
              $iva_comision = 0.00;
              $forma_pago = '';
              $money_code = null;
              $moneda_id = null;
          }

        
          

         
         
          
          return true;

    } //else estado

 


   
          
 
            
    return false;
    

  } 

  private function recibirAirAmadeus($request){
    Log::info('<==== Inicia AirAmadeus ==> archivo --> '.$request->input('nombre_archivo').' ===>');

    $contenidoAir = $request->input('air');
    $contenido = base64_decode($request->input('air'));
    $nombre_archivo = $request->input('nombre_archivo');
    //Validamos para no recibir archivos vacios
    if(!$contenido){
      throw new \Exception('El contenido air no es valido o la codificación base 64 tiene errores');
    }

    //Convertimos en lineas para la lectura del regex
    $contenido  = str_replace("||","\r", $contenido); //Agregamos saltos de linea
    $contenido  = preg_replace("/(\\r)/","", $contenido); //Quitamos los espacios iniciales
    $contenido  = preg_replace("/(\\n)/","\r\n", $contenido); //Corregimos la falta de /r



    $error_proceso = false;
    $penalidad = false;
    $fecha_creacion = '';
    $proveedor_id = 0;
    $facial = 0.00;
    $moneda = null; 
    $id_usuario = null;
    $usuarioAmadeus= "";
    $pasajeroMedio= "";
    $precio =0.00;
    $marcadorSegmento = 0;
    $destino = null;
    $origen = null;
    $fechaOrigen ="";
    $fechaDestino ="";
    $pnr ="";
    $nroTicket ="";
    $tasaYq  = 0.00;
    $pasajero = [];
    $ticketNro = [];
    $bandera = 0;
    $estado = 0;
    $marcadorD = 0;
    $nroTicketVoid = "";
    $monedaFacial = null;
    $ttal = [];
    $tasa = 0.00;
    $cotizacion ="";
    $contador = 0;
    $iva= 0.00;
    $pasaj = null;
    $comisionTotal = 0.00;
    $comision = 0.00;
    $ivaComision = 0.00;
    $key = 0;
    $anho_emision = '';
    $mes_emision = '';
    $forma_pago = '';
    $personas = null;
    $id_tipo_transaccion = 4; //tipo normal ticket
    $codigo_iata_aerolinea = null; //Codigo iata de 3 digitos
    $identificacion_reemision = false;
    $office_id = null;
    $tipo_tarifa = 'Publica';
    $id_empresa = (int)$request->input('id_empresa');


   
      /**
       * Identificar si es un ticket voideado
       * Identificar la cantidad de pasajeros para generar un ticket
       */

      //  dd($contenido);

     //Preguntar si el ticket esta en estado voideado
     $estado = !preg_match('/1\/1;VOID.*/',$contenido); //Buscamos si existe una linea que contenga esto y entonces asumimos que esta cancelado
     if(preg_match('/^EMD/m',$contenido)){
      $ndc_proceso = $this->emdAirAmadeus($contenido,$request,$contenidoAir, $office_id);

      //Validar si continuar con el proceso de insert de ticket normal
      if(!preg_match_all('/^T\-K.*/m', $contenido)){
        return $ndc_proceso;
      }  
    }


     //preg_match('/^A\-.*/m', $contenido, $indicador_amd_array);
     preg_match('/^A\-.*/m', $contenido, $indicador_amd_array);


     if(isset($indicador_amd_array[0])){
      $baseControl = explode('-', $indicador_amd_array[0]);

      /* if(strlen(trim($baseControl[1])) == 0){
            Log::error('<==================== El ticket no fue procesado - '.$request->input('nombre_archivo').' ====================>');
      }else {*/
      
          if($estado){

            /**
             * 
             * ============= Identificar el codigo de office =================================
             * En el caso de Amadeus el office se encuentra en el nombre del archivo, el office 
             * vendria a ser el codigo de la empresa que emite el ticket
             * ===============================================================================
             */
            preg_match('/(.*)_AIR/', $nombre_archivo, $ouput_office);
            
            if(isset($ouput_office[1]) && $ouput_office[1] == 'MIA1S2180'){
              $id_empresa = 21;
            }

            if(isset($ouput_office[1])){
              $office = $ouput_office[1];

              $office_data = TicketOffice::where('codigo', $office)->where('id_empresa',$id_empresa)->first();
              if($office_data){
                $office_id = $office_data->id;
              } else {
                Log::error('No se encuentra el office del ticket en BD - M0');
              }

            } else {
              Log::error('No se encuentra el office en el ticket- M0');
            }
            

            /**
             * Un ticket puede contener tickets normales como tickets EMD
             */


            //Identificar Ticket EMD

            //------------TICKET NORMAL ---------------------
          
            //Busca la linea que empieza con T-K y termina con punto y coma en 2 o 4 espacios espacios
            //Busca el usuario amadeus
            preg_match('/(T\-K).*(;|\s{2,4})?/', $contenido, $output_array_usuario_amadeus);

            if(count($output_array_usuario_amadeus) == 0){
              preg_match('/^A\-.*/m', $contenido, $output_array);
                  $baseUsuarioAmadeus = explode(';', $output_array[0]);
                  $output_array_usuario_amadeus =  explode(' ', $baseUsuarioAmadeus[1]);
            } 


            if(count($output_array_usuario_amadeus)){
              $codigo_iata_aerolinea = preg_replace('/T\-K|\s|\-\d.*/', '', $output_array_usuario_amadeus[0]); //Quitar las siglas TK-, espacios y saltos y guion con numeros

              $iata_aerolinea = Aerolinea::where('codigo3d',$codigo_iata_aerolinea)->first();

                if($iata_aerolinea){
                  $personas = Persona::where('codigo_iata', $iata_aerolinea->codigo_iata)
                                      ->where('id_empresa', $id_empresa)
                                      ->first(['id']);
                } else {
                  if(isset($output_array_usuario_amadeus[0])){
                    $personas = Persona::where('codigo_iata', $output_array_usuario_amadeus[0])
                                      ->where('id_empresa', $id_empresa)
                                      ->first(['id']);
                  }else{
                    Log::info('No se pudo identificar al proveedor aerolinea en BD Aerolinea- Linea T-K =>'.$codigo_iata_aerolinea);
                  }
              }

              if(isset($personas->id)){
                $proveedor_id = $personas->id;
              }else{ 
                $proveedor_id = 0;
                Log::info('No se pudo identificar al proveedor aerolinea en BD - Linea T-K =>'.$codigo_iata_aerolinea);
              }
            } else {
              $error_proceso = true;
              Log::error('No se pudo identificar los numeros de tickets T-K');
            }

            //Ticket Number
            preg_match_all('/^T\-K.*/m', $contenido, $output_array_tickets);

            if(empty($output_array_tickets[0])){
              preg_match_all('/^T\-E.*/m', $contenido, $output_array_tickets);
            }    

            if(isset($output_array_tickets[0])){
              foreach ($output_array_tickets[0] as $ticket) {
                $nroTicketBase = explode('-', $ticket);

                if(!isset($nroTicketBase[2])){
                  $error_proceso = true;
                  Log::error('No se pudo identificar el numero de ticket en la linea T-K');
                } else {
                  $nroTicket = trim($nroTicketBase[2]);
                  $ticketNro[]['nro'] = $nroTicket;
                }

              }
            } else {
              $error_proceso = true;
              Log::error('No se pudo identificar los numeros de tickets - numero de ticket T-K');
            }
            
            //Buscar cuando inicia con D- y que luego tenga numeros y que pueda terminar en punto y coma o espacios de 2 a 4
            //Fecha de emision del ticket
            preg_match('/^D\-(.*)/m', $contenido, $output_array); 
            if(isset($output_array[1])){
              $filtro_fecha = preg_replace('/\s+/', '', $output_array[1]); //Eliminar todo lo que no sea numero
              $fechas = explode(';',$filtro_fecha);

              if(isset($fechas[2])){
                  //Tomamos la tercera fecha 
                  $fecha_emision = $fechas[2];

                  $anho_emision = "20".substr($fecha_emision, 0, 2);
                  $mes_emision = substr($fecha_emision, -4, 2);
                  
                  $fecha_creacion= "20".substr($fecha_emision, 0, 2)."-".substr($fecha_emision, -4, 2)."-".substr($fecha_emision, -2, 4);
                } else {
                  $error_proceso = true;
                  Log::error('No se pudo identificar la fecha de emision de ticket (2) D-');
                }
              } else {
                $error_proceso = true;
                Log::error('No se pudo identificar la fecha de emision de ticket (1) D-');
              }


              /**
              * ============================== IDENTIFICAR LAS TARIFAS ==============================
              * 
              * >Detalles de una tarifa publica:
              *   La primera emision  de un ticket se identifica con la letra F (first issue) que podria ser ejemplo K-F
              *   Cuando se realiza una reemision de una tarifa publica se identifica con la letra R (reissue) que podria ser ejemplo K-R
              *   El desglose de la tarifa para primera emision suele venir con las iniciales de KFTF
              *   El desglose de la tarifa para reemision suele venir con las iniciales de KFTR
              * >Detalles de una tarifa privada:
              *   La primera emision de un ticket se identifica con la letra I (issue) que podria ser ejemplo KS-I
              *   Cuando se realiza una reemision de una tarifa privada se identifica con la letra Y (reissue) que podria ser ejemplo KS-Y
              *   El desglose de la tarifa para reemision suele venir con las iniciales de KSTY
              *   El desglose de la tarifa para primer emision suele venir con las iniciales de KSTI
              * 
              * ============================== IDENTIFICAR LAS TARIFAS ==============================
              */
            
            if (!$error_proceso) {
                $tarifa_data = $this->identificarTarifasAmadeus($contenido);

                //Identificacion del tipo de ticket
                $identificacion_reemision = $tarifa_data['identificacion_reemision'];
                $id_tipo_transaccion = $tarifa_data['id_tipo_transaccion'];

                //Tarifas
                $moneda = $tarifa_data['moneda'];
                $precio = $tarifa_data['precio'];
                $facial = $tarifa_data['facial'];
                $tipo_tarifa = $tarifa_data['tipo_tarifa'];

            }

            if(!$error_proceso && $precio > 0.00){
              $impuestos_data = $this->identificarDesgloseImpuestosAmadeus($contenido);

              //Impuestos
              $error_proceso = $impuestos_data['error_proceso'];
              $iva_moneda = $impuestos_data['iva_moneda'];
              $iva = $impuestos_data['iva'];
              $tasaYq = $impuestos_data['tasaYq'];
              $tasa = $impuestos_data['tasa'];
            }

            /**
             * En la linea TAX podemos determinar si el total de impuestos ya esta pagado
             * El tercer valor es una parte del total 
             * Los PD que vamos encontrando vamos a estar de tasa
             * Si el total del ticket no es mayor a cero no vamos a calcular impuestos
             * 
             * 
             * Lo que tenemos que hacer es machear con el desglose , porque si el total tiene PD entonces hay que volver  a calcular en base a los 
             * valores que no tienen PD aca
             * El total de las tasa es siempre XT
             */
            preg_match('/^TAX\-.*/m', $contenido, $output_array_impuesto_tax);
            if(!empty($output_array_impuesto_tax) && $precio > 0.00){
              $valores_impuesto = explode(';',$output_array_impuesto_tax[0]);
              $total_cancelado = false;

              foreach ($valores_impuesto as  $impuesto) {
                //Si el total que seria XT y  tiene PD entonces hay que recalcular la tasa en base a las tasas que no tienen pd en esta linea
                preg_match('/(USD|EUR|PYG|GBP)((\d+)(\.)(\d+))/', $impuesto,$arr_precios); //Buscamos si hay valores con el patron moneda y valor
                $total = preg_match('/XT/', $impuesto); 
                $pd = preg_match('/PD/', $impuesto); 
                if($total && $pd){
                  $iva_moneda = '';
                  $iva = 0;
                  $tasaYq = 0;
                  $tasa = 0;
                  $total_cancelado = true; //marcamos que el total de la linea ya esta cancelado tenemos que volver recalcular el total
                }
              }
              $arr_precios = [];

              //Vamos a ignorar siempre el ultimo valor porque es el total
              array_pop($valores_impuesto);
              $total_item_tax = count($valores_impuesto);
              $cant_vueltas = 0; //Iniciamos en uno porque son 3 elementos y un espacio al final 

              foreach ($valores_impuesto as  $impuesto) {
            

                //Si el total que seria XT y  tiene PD entonces hay que recalcular la tasa en base a las tasas que no tienen pd en esta linea
              preg_match('/(USD|EUR|PYG|GBP|PD)((\s+)?(\d+)(\.)(\d+))/', $impuesto,$arr_precios); //Buscamos si hay valores con el patron moneda y valor

              $py = preg_match('/PY/', $impuesto); //Verificamos si ese valor posee el impuesto PY
              $yq = preg_match('/YQ/', $impuesto); //Verificamos si ese valor posee el impuesto YQ
              $pd = preg_match('/PD/', $impuesto); //Verificamos si ese valor posee el impuesto PD
                

                if(empty($arr_precios) || !$arr_precios){
                  continue;
                }

              if($total_cancelado){


                  //PD es pagado
                if(!$pd){

                  $tasa += (float)$arr_precios[2]; //marcamos que el total de la linea ya esta cancelado tenemos que volver recalcular el total

                  //Impuesto PY
                    if($py){
                      $iva_moneda = $arr_precios[1];
                      $iva += (float) $arr_precios[2];
                    }
        
                    //Impuesto YQ
                    if($yq){
                      $tasaYq += (float) $arr_precios[2]; 
                    }
        
                }// PD


              } else {


                if($pd){
                  //Impuesto PY
                    if($py){
                      $iva -= (float) $arr_precios[2];
                    }
                    //Impuesto YQ
                    if($yq){
                      $tasaYq -= (float) $arr_precios[2]; 
                    }
                    $tasa -= $arr_precios[2]; //marcamos que el total de la linea ya esta cancelado tenemos que volver recalcular el total
                }// PD
              } //total cancelado
            }//foreach



            } else {
              // $error_proceso = true;
              // Log::info('El ticket no posee la linea de impuestos TAX-');
            }


          //Linea de pasajeros
            preg_match_all('/^I\-.*/m', $contenido, $output_array_pasajero);
            if(!empty($output_array_pasajero)){
            foreach ($output_array_pasajero[0] as $linea_pasajeros) {
                $pasajeroBase = explode(';', $linea_pasajeros);
                if(isset($pasajeroBase[1])){  
                  $pasajeros = substr($pasajeroBase[1], 2);
                  $pasajeroMedio = str_replace('/', ' ',$pasajeros);
                  $pasajero[]['nombre'] = $pasajeroMedio;
                }
              }
            } else {
              $error_proceso = true;
              Log::error('El ticket no posee pasajeros I-');
            }

            //PNR del ticket
            preg_match('/^MUC1A.*/m', $contenido, $output_array_pnr);
            if(!empty($output_array_pnr)){
                $baseBase = explode(';', $output_array_pnr[0]);
                  $pnrBase = $baseBase[0];
                  $baseB = explode(' ', $pnrBase);
                  $cadena = substr($baseB[1], 0, -3);
                  $pnr = $cadena;
            } else {
              $error_proceso = true;
              Log::error('El ticket no posee PNR MUC1A');
            }

            /**
             * Segmentos del pasajero son los destino en escala
             * Aca vamos a tomar la primera fecha y luego la ultima fecha 
             * Cada segmento tiene dos fecha de embarque y desembarque con la hora pero sin el año
             * La venta de pasaje siempre es a futuro como minimo de 6 a 11 meses en adelante a la fecha de emision
             * 
             * 
             * Porque usamos /m
             * https://stackoverflow.com/questions/22438038/regex-expression-at-beginning-of-line-or-not
             * By default, PCRE treats the subject string as consisting of a single "line" of characters (even if it actually contains several newlines). The "start of line" metacharacter (^) matches only at the start of the string, while the "end of line" metacharacter ($) matches only at the end of the string, or before a terminating newline (unless D modifier is set). This is the same as Perl. When this modifier is set, the "start of line" and "end of line" constructs match immediately following or immediately before any newline in the subject string, respectively, as well as at the very start and end. This is equivalent to Perl's /m modifier. If there are no "\n" characters in a subject string, or no occurrences of ^ or $ in a pattern, setting this modifier has no effect.
             */
        
            preg_match_all('/^H\-.*/m', $contenido, $output_array_segemento);
            if(isset($output_array_segemento[0])){
              $last = count($output_array_segemento[0]) -1;
              foreach ($output_array_segemento[0] as $key => $segmento) {
                $voids = strpos($segmento, "VOID;");
                if(!$voids){ 
                  if(!$fechaOrigen){
                    $segmento_explode = explode(';', $segmento);
                    $origen = substr($segmento_explode[1], 4, 3); //Ciudad origen
                    /**
                     * Vamos a encontrar las fechas de la linea que tienen este patron, nos interesa la fecha de embarque el primero
                     * El patron dice que se busca que los primeros sean dos numeros seguido de alguna de las opciones del grupo, y se pide buscar solo una coincidencia
                     * */
                    preg_match('/(\d{2})(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)/', $segmento, $output_arr_seg);

                    if(empty($output_arr_seg)){
                      $error_proceso = true;
                      Log::error('No se pudo identificar la fecha del segmento de pasajero fecha origen');
                    } else {
                      $dia = $output_arr_seg[1];
                      $mes = $this->mes($output_arr_seg[2]);
                    
        
                      $mes_vuelo = (int)$mes;
                      $mes_emision_f = (int)$mes_emision;
                      $anho = $anho_emision;

                      if($mes_vuelo < $mes_emision_f){
                        $anho = $anho_emision + 1;
                      }
        
                      $fechaOrigen = $anho.'-'.$mes.'-'.$dia;
                    }
                  }
                  
                  //Leer ultimo segmento la segunda fecha
                  if($last == $key){
                
                    $segmento_explode = explode(';', $segmento);
                    $destino = substr($segmento_explode[1], 4, 3);
                    /**
                     * Vamos a encontrar las fechas de la linea que tienen este patron, nos interesa la fecha de desembarque el segundo
                     * El patron dice que se busca que los primeros sean dos numeros seguida de alguna de las opciones del grupo, y se pide buscar todas las coicidencias
                     * */
                    preg_match_all('/(\d{2})(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)/', $segmento, $output_arr_seg);

                    if(empty($output_arr_seg)){
                      $error_proceso = true;
                      Log::error('No se pudo identificar la fecha del segmento de pasajero fecha destino');

                    } else {
                          /**
                           * Linea a descomponer
                           * H-010;008XPTY;PANAMA CITY PTY  ;ASU;ASUNCION         ;CM    0207 Q Q 07APR1538 2249 07APR;OK02;HK02;M ;0;738;;;1PC;;;ET;0611 ;N;2781;PA;PY;
                           * Con preg_match_all
                           * array:3 [▼
                            *    0 => array:2 [▼
                            *      0 => "07APR"
                            *      1 => "07APR"
                            *    ]
                            *    1 => array:2 [▼
                            *      0 => "07"
                            *      1 => "07"
                            *    ]
                            *    2 => array:2 [▼
                            *      0 => "APR"
                            *      1 => "APR"
                            *    ]
                            *  ]
                          */

                          
                          $dia = $output_arr_seg[1][1];
                          $mes = $this->mes($output_arr_seg[2][1]);
                      
                        
                          $mes_vuelo = (int)$mes;
                          $mes_emision_f = (int)$mes_emision;
                          $anho = $anho_emision;
                          /**
                           * Determinar el año: si el mes de embarque es mayor al mes  de emision es del año de emision
                           * Si el mes de embarque es menor al mes de emision es del año sgte
                           */
                          if($mes_vuelo < $mes_emision_f){
                            $anho = $anho_emision + 1;
                          }
                          $fechaDestino = $anho.'-'.$mes.'-'.$dia;
                    }

                  }
                }//void
              }//for

            } else {
              $error_proceso = true;
              Log::error('Error el ticket no posee segmento de pasajeros');
            }//output_array_segemento

            //Identificar forma de pago
            //Verificamos si la linea inicia con FP
            preg_match_all('/^FP.*/m', $contenido, $output_array_fp);
          
            if(isset($output_array_fp[0][0])){
              foreach ($output_array_fp[0] as $key =>  $fp) {
                $fp = preg_replace('/(FP)|\s|(FPO)|\//','',$fp);

                //Si tiene punto y coma vamos a dividir para buscar el la forma de pago
                if(preg_match('/;/',$fp)){
                  $arr_fp = explode(';',$fp);
                  $fp = $arr_fp[0];
                }

                if($fp == Ticket::FORMA_PAGO_CASH){
                    $pasajero[$key]['forma_pago']= $fp;
                } else {
                  Log::error('Forma de pago no identificado <!>');
                }
                
              }
              
            } else {
              Log::info('No tiene forma de pago');
            }

            //Identificar al emisor del ticket de agencia
            preg_match('/^C\-.*/m', $contenido, $pos);
            if(!empty($pos)){
              preg_match('/(\w{6})(\w{2})\-(\w{6})(\w{2})/',$pos[0],$usuario_amadeus);
          
              if(isset($usuario_amadeus[1])){
                $usuarioAmadeus = trim($usuario_amadeus[1]);
                $persona_amadeus = Persona::where('usuario_amadeus',$usuarioAmadeus)->first();
                if($persona_amadeus){
                  $id_usuario = $persona_amadeus->id;
                } else {
                  Log::info('El usuario amadeus no se ecuentra en BD personas. =>'.$usuarioAmadeus);
                }
              }
        
            } else {
              Log::info('No posee linea de usuario Amadeus - Linea C-');
            }

            //Identificar la comision
            preg_match_all('/^FM\*\w+\*(\d+(\.00)?);?/m', $contenido, $pos);
            if(!empty($pos[1])){
              foreach ($pos[1] as $key => $fm) {
                  $comision_porcentaje = (float)$fm;
                  if($comision_porcentaje > 0.00){
                    $pasajero[$key]['comision'] = $comision_porcentaje;
                  }
              }
            }
        
            /**
             * Calcular Facial en base al total del ticket y las tasas
             * El total debe ser mayor a 0
             * 
             */
          
            if($identificacion_reemision && $precio > 0.00){
              $facial = $precio - $tasa;
            }


          }//if estado


          /**
           * Debe tener fecha de destino y origen
           * Debe tener una facial
           * Debe tener una comision
           */

          // if(!$error_proceso){
                if($estado){
                  if(count($pasajero)){
                    foreach($pasajero as $key=>$pasaj){
                      $ivaComision = 0.00;
                      $comision = isset($pasaj['comision']) ? $pasaj['comision'] : 0.00;

                      //Validamos que tenga fechas
                    if($facial > 0.00 && $comision > 0.00){

                        Log::info('Comision identificada > '. $comision_porcentaje);
                        Log::info('Facial identificada > '. $facial);


                        $comisionTotal = round(((float)$facial * (float)$comision)/100,2);
                        $ivaComision = round(($comisionTotal* 10)/100,2);
                        Log::info('Comision Calculada > '. $comisionTotal);
                    }

                    $forma_pago = isset($pasaj['forma_pago']) ? $pasaj['forma_pago'] : null;
              
                
                    
                      $tickets = Ticket::where('numero_amadeus', trim($ticketNro[$key]['nro']))
                                        ->where('id_empresa',$request->input('id_empresa'))
                                        ->first(['id']);
                
                          
                                
                      if(!$tickets){  
                        if($moneda != 111){
                          $iva  = round($iva,2);
                          $tasaYq = round($tasaYq,2);
                          $facial = (float)number_format($facial,2,'.','');
                          $tasa = round($tasa,2);
                        } else {
                          $iva  = round($iva);
                          $tasaYq = round($tasaYq);
                          $facial = round($facial);
                          $tasa = round($tasa);
                        }
                        
                        
                        $arrayInicio  =  
                                        [ 
                                          $fecha_creacion, //1
                                          $proveedor_id, //2
                                          $facial, //3
                                          0, // grupo 4
                                          $moneda, //int4, 5
                                          null,  // proforma 6
                                          $usuarioAmadeus, //7
                                          $pasaj['nombre'], //8
                                          $pnr, //9
                                          $iva,// float8, 10
                                          $id_empresa,//empresa 11
                                          $tasa,//"tasas",// float8, 12
                                          $tasaYq,// float8, 13
                                          0, // factura 14
                                          $comisionTotal,//"comision",// float8, 15
                                          $ivaComision,//"iva_comision",// float8, 16
                                          0,//"fee",// float8,  17
                                          25,//"id_estado",// int4,  18
                                          1,  //19
                                          date('Y-m-d'),//"fecha_alta",// date, 20
                                          $id_usuario,//"id_usuario",// int4, 21
                                          null, //"id_detalle_proforma" int4, 22
                                          trim($ticketNro[$key]['nro']), //23
                                          $precio,//"total_ticket",// float8, 24
                                          $id_tipo_transaccion,//"id_tipo_transaccion",// int4, 25
                                          $forma_pago,//"tipo_pago",// varchar, 26
                                          0,//"variacion_impuesto", //27
                                          0,//"senha_paquete", //28
                                          'A',//"origen del insert Automatico",// text, 29
                                          $destino,   // "destino",// text,30 
                                          $fechaOrigen,// date, 31
                                          $fechaDestino,// date,32 
                                          0,//"adultos",// int4,33
                                          0,//"ninhos",// int4,  34
                                          0,//"infantes",// int4, 35
                                          $contenidoAir, // int4, 36
                                          $request->input('nombre_archivo'),
                                          $request->input('tipo'), //String 
                                          $comision, //porcentaje_comision float
                                          $codigo_iata_aerolinea,
                                          $origen, //iniciio Origen
                                          $tipo_tarifa, //Tarifa publica, privada o EMD
                                          $office_id //Empresa que emite el ticket
                                        ];

                          Log::info( json_encode($arrayInicio,JSON_PRETTY_PRINT));

                          $inserto= DB::select('SELECT public."insert_into_tickets_air"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                          $arrayInicio); 

                                            
                                  Log::info('<==================== El ticket ingreso a insert_into_tickets_air ====================>');   
                                  
                                  
                          }  else {
                            Log::error('El ticket ya se encuentra en la BD y no sera actualizado');
                          }               
                                      
                      }//foreach  

                      //Fin del foreach de insert ticket
                      return true;

                  } else {
                    Log::error('No tiene pasajeros');
                  }
                      
                } else { 

                  Log::info('El ticket es voideado');


                  /**
                   * Buscamos que la linea inicie con T-K y con la variable multiline /m
                   */
                        preg_match_all('/^T\-K.*/m', $contenido, $output_array_tickets);
                    
                        if(isset($output_array_tickets[0])){

                          $total_ticket_file = count($output_array_tickets[0]);
                          $total_procesado = 0;
                
                          foreach ($output_array_tickets[0] as $ticket) {
                          
                            $nroTicketBase = explode('-', $ticket);
                      
                            if(!isset($nroTicketBase[2])){
                              Log::error('El ticket no se pudo cancelar porque no se pudo identificar el numero de ticket en la linea T-K');
                            } else {
                              $nroTicket = trim($nroTicketBase[2]);
        
                            
                                $tickets = Ticket::where('numero_amadeus', $nroTicket)
                                ->where('id_empresa',$request->input('id_empresa'))
                                ->first(['id']);

                                if(isset($tickets->id)){ 
                                      DB::table('tickets')->where('id',$tickets->id)
                                      ->update([
                                                'id_estado'=>33
                                              ]);

                                      Log::info('Ticket voideado '.$nroTicket);
                                      $total_procesado++;
                                } else {
                                  Log::error('El numero de ticket no se encuentra en la base de datos');
                                }
                            }

                      
                        }//foreach

                        if($total_procesado > 0 && $total_procesado == $total_ticket_file){
                          Log::info('Todos los tickets presentes fueron voideados');
                        } else {
                          Log::error('Solo fueron voideados '.$total_procesado .' DE '.$total_ticket_file);
                        }


                    } else {
                      Log::error('El ticket no se pudo cancelar porque no posee numero de ticket T-K');
                    }
                      
                } //else 

          //} 
       }

      return false;
   
  } 

//////////////////////////////////////////////////////////////////////////////////////////////////
private function amdAirAmadeus($contenido,$request,$contenidoAir, $office_id){

  $error_proceso = false;
  $penalidad = false;
  $fecha_creacion = null;
  $proveedor_id = null;
  $facial = 0.00;
  $moneda = null; 
  $id_usuario = null;
  $usuarioAmadeus= "";
  $pasajeroMedio= "";
  $precio =0.00;
  $marcadorSegmento = 0;
  $destino = null;
  $origen = null;
  $fechaOrigen = null;
  $fechaDestino = null;
  $pnr =null;
  $nroTicket =null;
  $tasaYq  = 0;
  $pasajero = '';
  $ticketNro = [];
  $bandera = 0;
  $estado = 0;
  $marcadorD = 0;
  $nroTicketVoid = "";
  $monedaFacial = null;
  $ttal = [];
  $tasa = 0;
  $cotizacion ="";
  $contador = 0;
  $iva= 0.00;
  $pasaj = null;
  $comisionTotal = 0;
  $comision = 0.00;
  $ivaComision = 0.00;
  $key = 0;
  $anho_emision = '';
  $mes_emision = '';
  $forma_pago = '';
  $personas = null;
  $tipo_tarifa = 'AMD';

    //PNR del ticket
    preg_match('/^MUC1A.*/m', $contenido, $output_array_pnr);
    if(!empty($output_array_pnr)){
        $baseBase = explode(';', $output_array_pnr[0]);
          $pnrBase = $baseBase[0];
          $baseB = explode(' ', $pnrBase);
          $cadena = substr($baseB[1], 0, -3);
          $pnr = $cadena;
    } else {
      $error_proceso = true;
      Log::error('El ticket no posee PNR MUC1A');
    }

    //Identificar al emisor del ticket de agencia
    preg_match('/^C\-.*/m', $contenido, $pos);
    if(!empty($pos)){
      preg_match('/(\w{6})(\w{2})\-(\w{6})(\w{2})/',$pos[0],$usuario_amadeus);

      if(isset($usuario_amadeus[1])){
        $usuarioAmadeus = trim($usuario_amadeus[1]);
        $persona_amadeus = Persona::where('usuario_amadeus',$usuarioAmadeus)
        ->where('id_empresa', $request->input('id_empresa'))
        ->first();
        if($persona_amadeus){
          $id_usuario = $persona_amadeus->id;
        } else {
          Log::info('El usuario amadeus no se ecuentra en BD personas.  EMD =>'.$usuarioAmadeus);
        }
      }

    } else {
      Log::info('No posee linea de usuario Amadeus EMD - Linea C-');
    }


    //Buscar cuando inicia con D- y que luego tenga numeros y que pueda terminar en punto y coma o espacios de 2 a 4
    //Fecha de emision del ticket
    preg_match('/^D\-(.*)/m', $contenido, $output_array); 
    if(isset($output_array[1])){
      $filtro_fecha = preg_replace('/\s+/', '', $output_array[1]); //Eliminar todo lo que no sea numero
      $fechas = explode(';',$filtro_fecha);

      if(isset($fechas[2])){
          //Tomamos la tercera fecha 
          $fecha_emision = $fechas[2];

          $anho_emision = "20".substr($fecha_emision, 0, 2);
          $mes_emision = substr($fecha_emision, -4, 2);
          
          $fecha_creacion= "20".substr($fecha_emision, 0, 2)."-".substr($fecha_emision, -4, 2)."-".substr($fecha_emision, -2, 4);
        } else {
          $error_proceso = true;
          Log::error('No se pudo identificar la fecha de emision de ticket (2) D-');
        }
      } else {
        $error_proceso = true;
        Log::error('No se pudo identificar la fecha de emision de ticket (1) D-');
      }


    if($error_proceso){
      Log::error('<==================== El ticket no fue procesado - '.$request->input('nombre_archivo').' ====================>');
    }


    //Buscar cada linea EMD
    preg_match_all('/^AMD.*/m', $contenido, $emds);


    foreach ($emds[0] as $emd) {

      $nroTicket =null;
      $tasaYq  = 0.00;
      $pasajero = null;
      $ticketNro = null;
      $proveedor_id = 0;
      $facial = 0.00;
      $moneda = null; 
      $pasajeroMedio= "";
      $precio =0.00;
      $marcadorSegmento = 0;
      $destino = null;
      $origen = null;
      $fechaOrigen =null;
      $fechaDestino =null;
      $iva_moneda = null;
      $iva= 0.00;
      $linea_emd_identificatoria = '';
      $codigo_iata_aerolinea = null;


      //Identificar la linea EMD para machear contra el ticket
      $linea_emd_identificatoria = explode(';',$emd)[5];

      //-------------- VALORES DEL EMD ---------------------------

      //Buscar la facial del EMD
      preg_match_all('/F;(USD|EUR|PYG|GBP)(\s+)?(\d+\.\d+)/', $emd, $facial_emd_arr);

      if(isset($facial_emd_arr[3][0])){
        $moneda = isset($facial_emd_arr[1][0])   ? $this->getIdMoneda($facial_emd_arr[1][0]) : null;
        $facial = isset($facial_emd_arr[2][0])   ? (float) $facial_emd_arr[3][0] : 0.00;
      }

      //Buscar el total del EMD
      preg_match_all('/N;;(USD|EUR|PYG|GBP)(\s+)?(\d+\.\d+);/', $emd, $total_emd_arr);
      if(isset($total_emd_arr[3][0])){
        $moneda = isset($total_emd_arr[1][0])   ? $this->getIdMoneda($total_emd_arr[1][0]) : null;
        $precio = isset($total_emd_arr[3][0])   ? (float) $total_emd_arr[3][0] : 0.00;
      }

      //Buscar Impuestos del ticket
      //Validar que el total sea mayor a cero
      preg_match('/;T-.*/', $emd, $impuestos_ticket);
      if(isset($impuestos_ticket[0]) && !empty($impuestos_ticket) && $precio > 0.00){
    
        $py = preg_match('/PY/', $impuestos_ticket[0]); //Verificamos si ese valor posee el impuesto PY
        $yq = preg_match('/YQ/', $impuestos_ticket[0]); //Verificamos si ese valor posee el impuesto YQ
        preg_match('/(USD|EUR|PYG|GBP)\s+((\d+)(\.)(\d+))/', $impuestos_ticket[0],$arr_precios); //Buscamos si hay valores con el patron moneda y valor

        if(!empty($arr_precios)){
             //Impuesto PY
          if($py){
            $iva_moneda = $arr_precios[1];
            $iva += (float) $arr_precios[2];   
          }

          //Impuesto YQ
          if($yq){
            $tasaYq += (float) $arr_precios[2]; 
          }

          //Total impuestos
          $tasa+= (float) $arr_precios[2];
          $arr_precios = [];
        } //arr_precio emty
         
          
      }//impuesto ticket 


      //Si el total no es mayor a cero entonces vamos a cerar la facial
      if($precio <= 0.0){
        $facial = 0.0;
      }


      //----- BUSCAR PASAJERO
      
      //Obtener nombre de pasajeros y el ticket 
      preg_match_all('/^I\-.*|^(TMC).*/m',$contenido,$pos);

      if(!empty($pos[0])){
        foreach ($pos[0] as $linea) {

            if(preg_match('/^I\-/',$linea)){
              $pasajeroBase = explode(';', $linea);
              if(isset($pasajeroBase[1])){  
                $pasajeros = substr($pasajeroBase[1], 2);
                $pasajeroMedio = str_replace('/', ' ',$pasajeros);
                $pasajero = $pasajeroMedio;
              }
            }

            //Obtener el numero de ticket y asignar solamente si es de la misma linea EMD
            if(preg_match('/^TMCD/m',$linea)){
                $nroTicketBaseGuion = explode('-', $linea);
                $nroTicketBase = explode(';',$nroTicketBaseGuion[1]);
                $identificador_linea = preg_replace('/\s/','',$nroTicketBase[2]);


                if($identificador_linea == $linea_emd_identificatoria){
                  $nroTicket = $nroTicketBase[0];
                  //TODO: Verificar porque no estan todos los codigos en BD
                  $codigo_iata_aerolinea = preg_replace('/\D/', '', $nroTicketBaseGuion[0]); //Quitar las siglas TMCD, espacios y saltos y guion con numeros

                  $iata_aerolinea = Aerolinea::where('codigo3d',$codigo_iata_aerolinea)->first();
          
                  $personas = Persona::where('codigo_iata', $iata_aerolinea->codigo_iata)
                              ->where('id_empresa', $request->input('id_empresa'))
                              ->first(['id']);
                    if(isset($personas->id)){
                      $proveedor_id = $personas->id;
                    }else{ 
                      $proveedor_id = 0;
                      Log::info('No se pudo identificar al proveedor aerolinea en BD EMD - Linea TMCD =>'.$codigo_iata_aerolinea);
                    }
                


                  $comisionTotal = 0.00;    
                  // $comisionTotal = ((float)$facial * (float)$comision)/100;
                  // $ivaComision = ($comisionTotal* 10)/100;
                  // $forma_pago = isset($pasaj['forma_pago']) ? $pasaj['forma_pago'] : null;
        
                
                                      
                          $tickets = Ticket::where('numero_amadeus', trim($nroTicket))
                          ->where('id_empresa',$request->input('id_empresa'))
                          ->first(['id']);


                                  
                          if(!isset($tickets->id)){  
                         
                            $arrayInicio  =  
                            [ 
                              $fecha_creacion, //1
                              $proveedor_id, //2
                              (float)$facial, //3
                              null, // grupo 4
                              $moneda, //int4, 5
                              null,  // proforma 6
                              $usuarioAmadeus, //7
                              $pasajero, //8
                              $pnr, //9
                              $iva,// float8, 10
                              (int)$request->input('id_empresa'),//empresa 11
                              $tasa,//"tasas",// float8, 12
                              $tasaYq,// float8, 13
                              null, // factura 14
                              $comisionTotal,//"comision",// float8, 15
                              round($ivaComision,2),//"iva_comision",// float8, 16
                              0,//"fee",// float8,  17
                              25,//"id_estado",// int4,  18
                              1,  //19
                              date('Y-m-d'),//"fecha_alta",// date, 20
                              $id_usuario,//"id_usuario",// int4, 21
                              null, //"id_detalle_proforma" int4, 22
                              trim($nroTicket), //23
                              $precio,//"total_ticket",// float8, 24
                              14,//"id_tipo_transaccion",// int4, 25 EMDS
                              null,//"tipo_pago",// varchar, 26
                              0,//"variacion_impuesto", //27
                              0,//"senha_paquete", //28
                              'A',//"origen del insert Automatico",// text, 29
                              null,   // "destino",// text,30 
                              null,// date, 31
                              null,// date,32 
                              null,//"adultos",// int4,33
                              null,//"ninhos",// int4,  34
                              null,//"infantes",// int4, 35
                              $contenidoAir, // int4, 36
                              $request->input('nombre_archivo'),
                              $request->input('tipo'),
                              null, //porcentaje_comision float
                              $codigo_iata_aerolinea, //Codigo iata de 3 digitos
                              $origen, //inicio Origen
                              $tipo_tarifa,
                              $office_id
                            ];
                        
                            Log::info( json_encode($arrayInicio,JSON_PRETTY_PRINT));
                            
                        

                            $inserto= DB::select('SELECT public."insert_into_tickets_air"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                                          $arrayInicio); 

                                              // return true;//procesado
                                              
                                    Log::info('<==================== El ticket ingreso a insert_into_tickets_air ====================>');   
                                    
                                    break;
                            }  else {
                              Log::error('El ticket ya se encuentra en la BD y no sera actualizado EMD');
                            } 

                } //identificador


            }//if TMCD
        }//foreach I-TMC
      } //if I-
    }//foreach

    return true;

      
}
/////////////////////////////////////////////////////////////////////////////////////////////////

  private function emdAirAmadeus($contenido,$request,$contenidoAir, $office_id){

      $error_proceso = false;
      $penalidad = false;
      $fecha_creacion = null;
      $proveedor_id = null;
      $facial = 0.00;
      $moneda = null; 
      $id_usuario = null;
      $usuarioAmadeus= "";
      $pasajeroMedio= "";
      $precio =0.00;
      $marcadorSegmento = 0;
      $destino = null;
      $origen = null;
      $fechaOrigen = null;
      $fechaDestino = null;
      $pnr =null;
      $nroTicket =null;
      $tasaYq  = 0;
      $pasajero = '';
      $ticketNro = [];
      $bandera = 0;
      $estado = 0;
      $marcadorD = 0;
      $nroTicketVoid = "";
      $monedaFacial = null;
      $ttal = [];
      $tasa = 0;
      $cotizacion ="";
      $contador = 0;
      $iva= 0.00;
      $pasaj = null;
      $comisionTotal = 0;
      $comision = 0.00;
      $ivaComision = 0.00;
      $key = 0;
      $anho_emision = '';
      $mes_emision = '';
      $forma_pago = '';
      $personas = null;
      $tipo_tarifa = 'EMD';

        //PNR del ticket
        preg_match('/^MUC1A.*/m', $contenido, $output_array_pnr);
        if(!empty($output_array_pnr)){
            $baseBase = explode(';', $output_array_pnr[0]);
              $pnrBase = $baseBase[0];
              $baseB = explode(' ', $pnrBase);
              $cadena = substr($baseB[1], 0, -3);
              $pnr = $cadena;
        } else {
          $error_proceso = true;
          Log::error('El ticket no posee PNR MUC1A');
        }

        //Identificar al emisor del ticket de agencia
        preg_match('/^C\-.*/m', $contenido, $pos);
        if(!empty($pos)){
          preg_match('/(\w{6})(\w{2})\-(\w{6})(\w{2})/',$pos[0],$usuario_amadeus);

          if(isset($usuario_amadeus[1])){
            $usuarioAmadeus = trim($usuario_amadeus[1]);
            $persona_amadeus = Persona::where('usuario_amadeus',$usuarioAmadeus)
            ->where('id_empresa', $request->input('id_empresa'))
            ->first();
            if($persona_amadeus){
              $id_usuario = $persona_amadeus->id;
            } else {
              Log::info('El usuario amadeus no se ecuentra en BD personas.  EMD =>'.$usuarioAmadeus);
            }
          }
    
        } else {
          Log::info('No posee linea de usuario Amadeus EMD - Linea C-');
        }

   
        //Buscar cuando inicia con D- y que luego tenga numeros y que pueda terminar en punto y coma o espacios de 2 a 4
        //Fecha de emision del ticket
        preg_match('/^D\-(.*)/m', $contenido, $output_array); 
        if(isset($output_array[1])){
          $filtro_fecha = preg_replace('/\s+/', '', $output_array[1]); //Eliminar todo lo que no sea numero
          $fechas = explode(';',$filtro_fecha);

          if(isset($fechas[2])){
              //Tomamos la tercera fecha 
              $fecha_emision = $fechas[2];

              $anho_emision = "20".substr($fecha_emision, 0, 2);
              $mes_emision = substr($fecha_emision, -4, 2);
              
              $fecha_creacion= "20".substr($fecha_emision, 0, 2)."-".substr($fecha_emision, -4, 2)."-".substr($fecha_emision, -2, 4);
            } else {
              $error_proceso = true;
              Log::error('No se pudo identificar la fecha de emision de ticket (2) D-');
            }
          } else {
            $error_proceso = true;
            Log::error('No se pudo identificar la fecha de emision de ticket (1) D-');
          }


        if($error_proceso){
          Log::error('<==================== El ticket no fue procesado - '.$request->input('nombre_archivo').' ====================>');
        }

    
        //Buscar cada linea EMD
        preg_match_all('/^EMD.*/m', $contenido, $emds);
        foreach ($emds[0] as $emd) {

          $nroTicket =null;
          $tasaYq  = 0.00;
          $pasajero = null;
          $ticketNro = null;
          $proveedor_id = 0;
          $facial = 0.00;
          $moneda = null; 
          $pasajeroMedio= "";
          $precio =0.00;
          $marcadorSegmento = 0;
          $destino = null;
          $origen = null;
          $fechaOrigen =null;
          $fechaDestino =null;
          $iva_moneda = null;
          $iva= 0.00;
          $linea_emd_identificatoria = '';
          $codigo_iata_aerolinea = null;


          //Identificar la linea EMD para machear contra el ticket
          $linea_emd_identificatoria = explode(';',$emd)[5];

          //-------------- VALORES DEL EMD ---------------------------

          //Buscar la facial del EMD
          preg_match_all('/F;(USD|EUR|PYG|GBP)(\s+)?(\d+\.\d+)/', $emd, $facial_emd_arr);

          if(isset($facial_emd_arr[3][0])){
            $moneda = isset($facial_emd_arr[1][0])   ? $this->getIdMoneda($facial_emd_arr[1][0]) : null;
            $facial = isset($facial_emd_arr[2][0])   ? (float) $facial_emd_arr[3][0] : 0.00;
          }

          //Buscar el total del EMD
          preg_match_all('/N;;(USD|EUR|PYG|GBP)(\s+)?(\d+\.\d+);/', $emd, $total_emd_arr);
          if(isset($total_emd_arr[3][0])){
            $moneda = isset($total_emd_arr[1][0])   ? $this->getIdMoneda($total_emd_arr[1][0]) : null;
            $precio = isset($total_emd_arr[3][0])   ? (float) $total_emd_arr[3][0] : 0.00;
          }

          //Buscar Impuestos del ticket
          //Validar que el total sea mayor a cero
          preg_match('/;T-.*/', $emd, $impuestos_ticket);
          if(isset($impuestos_ticket[0]) && !empty($impuestos_ticket) && $precio > 0.00){
        
            $py = preg_match('/PY/', $impuestos_ticket[0]); //Verificamos si ese valor posee el impuesto PY
            $yq = preg_match('/YQ/', $impuestos_ticket[0]); //Verificamos si ese valor posee el impuesto YQ
            preg_match('/(USD|EUR|PYG|GBP)\s+((\d+)(\.)(\d+))/', $impuestos_ticket[0],$arr_precios); //Buscamos si hay valores con el patron moneda y valor

            if(!empty($arr_precios)){
                 //Impuesto PY
              if($py){
                $iva_moneda = $arr_precios[1];
                $iva += (float) $arr_precios[2];   
              }

              //Impuesto YQ
              if($yq){
                $tasaYq += (float) $arr_precios[2]; 
              }

              //Total impuestos
              $tasa+= (float) $arr_precios[2];
              $arr_precios = [];
            } //arr_precio emty
             
              
          }//impuesto ticket 


          //Si el total no es mayor a cero entonces vamos a cerar la facial
          if($precio <= 0.0){
            $facial = 0.0;
          }


          //----- BUSCAR PASAJERO
          
          //Obtener nombre de pasajeros y el ticket 
          preg_match_all('/^I\-.*|^(TMC).*/m',$contenido,$pos);
  
          if(!empty($pos[0])){
            foreach ($pos[0] as $linea) {

                if(preg_match('/^I\-/',$linea)){
                  $pasajeroBase = explode(';', $linea);
                  if(isset($pasajeroBase[1])){  
                    $pasajeros = substr($pasajeroBase[1], 2);
                    $pasajeroMedio = str_replace('/', ' ',$pasajeros);
                    $pasajero = $pasajeroMedio;
                  }
                }

                //Obtener el numero de ticket y asignar solamente si es de la misma linea EMD
                if(preg_match('/^TMCD/m',$linea)){
                    $nroTicketBaseGuion = explode('-', $linea);
                    $nroTicketBase = explode(';',$nroTicketBaseGuion[1]);
                    $identificador_linea = preg_replace('/\s/','',$nroTicketBase[2]);


                    if($identificador_linea == $linea_emd_identificatoria){
                      $nroTicket = $nroTicketBase[0];
                      //TODO: Verificar porque no estan todos los codigos en BD
                      $codigo_iata_aerolinea = preg_replace('/\D/', '', $nroTicketBaseGuion[0]); //Quitar las siglas TMCD, espacios y saltos y guion con numeros

                      $iata_aerolinea = Aerolinea::where('codigo3d',$codigo_iata_aerolinea)->first();
              
                      $personas = Persona::where('codigo_iata', $iata_aerolinea->codigo_iata)
                                  ->where('id_empresa', $request->input('id_empresa'))
                                  ->first(['id']);
                        if(isset($personas->id)){
                          $proveedor_id = $personas->id;
                        }else{ 
                          $proveedor_id = 0;
                          Log::info('No se pudo identificar al proveedor aerolinea en BD EMD - Linea TMCD =>'.$codigo_iata_aerolinea);
                        }
                    


                      $comisionTotal = 0.00;    
                      // $comisionTotal = ((float)$facial * (float)$comision)/100;
                      // $ivaComision = ($comisionTotal* 10)/100;
                      // $forma_pago = isset($pasaj['forma_pago']) ? $pasaj['forma_pago'] : null;
            
                    
                                          
                              $tickets = Ticket::where('numero_amadeus', trim($nroTicket))
                              ->where('id_empresa',$request->input('id_empresa'))
                              ->first(['id']);

  
                                      
                              if(!isset($tickets->id)){  
                             
                                $arrayInicio  =  
                                [ 
                                  $fecha_creacion, //1
                                  $proveedor_id, //2
                                  (float)$facial, //3
                                  null, // grupo 4
                                  $moneda, //int4, 5
                                  null,  // proforma 6
                                  $usuarioAmadeus, //7
                                  $pasajero, //8
                                  $pnr, //9
                                  $iva,// float8, 10
                                  (int)$request->input('id_empresa'),//empresa 11
                                  $tasa,//"tasas",// float8, 12
                                  $tasaYq,// float8, 13
                                  null, // factura 14
                                  $comisionTotal,//"comision",// float8, 15
                                  round($ivaComision,2),//"iva_comision",// float8, 16
                                  0,//"fee",// float8,  17
                                  25,//"id_estado",// int4,  18
                                  1,  //19
                                  date('Y-m-d'),//"fecha_alta",// date, 20
                                  $id_usuario,//"id_usuario",// int4, 21
                                  null, //"id_detalle_proforma" int4, 22
                                  trim($nroTicket), //23
                                  $precio,//"total_ticket",// float8, 24
                                  14,//"id_tipo_transaccion",// int4, 25 EMDS
                                  null,//"tipo_pago",// varchar, 26
                                  0,//"variacion_impuesto", //27
                                  0,//"senha_paquete", //28
                                  'A',//"origen del insert Automatico",// text, 29
                                  null,   // "destino",// text,30 
                                  null,// date, 31
                                  null,// date,32 
                                  null,//"adultos",// int4,33
                                  null,//"ninhos",// int4,  34
                                  null,//"infantes",// int4, 35
                                  $contenidoAir, // int4, 36
                                  $request->input('nombre_archivo'),
                                  $request->input('tipo'),
                                  null, //porcentaje_comision float
                                  $codigo_iata_aerolinea, //Codigo iata de 3 digitos
                                  $origen, //inicio Origen
                                  $tipo_tarifa,
                                  $office_id
                                ];
                            
                                Log::info( json_encode($arrayInicio,JSON_PRETTY_PRINT));
                                

                                $inserto= DB::select('SELECT public."insert_into_tickets_air"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                                              $arrayInicio); 

                                                  // return true;//procesado
                                                  
                                        Log::info('<==================== El ticket ingreso a insert_into_tickets_air ====================>');   
                                        
                                        break;
                                }  else {
                                  Log::error('El ticket ya se encuentra en la BD y no sera actualizado EMD');
                                } 

                    } //identificador


                }//if TMCD
            }//foreach I-TMC
          } //if I-
        }//foreach
  
        return true;

          
  }

  /**
   * Identifica las tarifas del ticket y determinar si es una reemision
   */
  private function identificarTarifasAmadeus($contenido){

        $identificacion_reemision = false;
        $id_tipo_transaccion = 4;
        $error_proceso = false;

        //Tarifas
        $moneda = null;
        $precio = 0.00;
        $facial = 0.00;
        $tipo_tarifa = 'Privada';
        
        
          /**
           * =============== Identificar si es una reemision y su tarifa ===============
           */
          $tarifa_reemision_publica = preg_match('/^K\-R.*/m', $contenido);
          $tarifa_reemision_privada = preg_match('/^KS\-Y.*/m', $contenido);

          $tarifa_primera_emision_publica = preg_match('/^K\-F.*/m', $contenido);
          $tarifa_primera_emision_privada = preg_match('/^KS\-I.*/m', $contenido);

          if ($tarifa_reemision_publica || $tarifa_reemision_privada) {
              $identificacion_reemision = true;

              if ($tarifa_reemision_privada) {
                  $tipo_tarifa = 'Privada';
                  preg_match('/^KS\-Y.*/m', $contenido, $output_array_precios); //Obtenemos la linea
                  Log::info('Se identifica una reemision de tarifa privada');
              } else {  
                  $tipo_tarifa = 'Publica';
                  preg_match('/^K\-R.*/m', $contenido, $output_array_precios); //Obtenemos la linea
                  Log::info('Se identifica una reemision de tarifa publica');
              }

              if(!isset( $output_array_precios[0])){
                $error_proceso = true;
                Log::error('La linea de la tarifa esta vacia');
              } else {
                     /**
                       * Quitamos en orden los valores
                       * 1. Facial
                       * 2. Total (En una reemision es la diferencia)
                       */
                      preg_match_all('/(USD|EUR|PYG|GBP)(\d+\.\d+)/', $output_array_precios[0], $arr_precios);

                      if (empty($arr_precios)) {
                          $error_proceso = true;
                          Log::error('No se pudo identificar el precio del ticket reemision');
                      } else {

                        /**
                           * Si tengo 3 valores en mi linea entonces etoy teniendo un equivalente de otra moneda
                           */

                           $primera_moneda = isset($arr_precios[1][0])   ? $arr_precios[1][0] : 'USD';
                           $segunda_moneda = isset($arr_precios[1][1])   ? $arr_precios[1][1] : 'USD';

                            if($primera_moneda != $segunda_moneda) {
                                //Moneda
                                $moneda = isset($arr_precios[1][1])   ? $this->getIdMoneda($arr_precios[1][1]) : null;

                                //Total ticket
                                $precio  = isset($arr_precios[2][1]) ? (float) $arr_precios[2][1] : '';

                                //En caso del que precio sea cero ya vamos cerar la facial
                                if ($precio <= 0.00 || !$precio) {
                                    $facial = 0.00;
                                }


                            } else {
                              //Moneda
                              $moneda = isset($arr_precios[1][0]) ? $this->getIdMoneda($arr_precios[1][0]) : '';

                              //Total ticket
                              $precio  = isset($arr_precios[2][1]) ? (float) $arr_precios[2][1] : '';

                              //En caso del que precio sea cero ya vamos cerar la facial
                              if ($precio <= 0.00 || !$precio) {
                                  $facial = 0.00;
                              }
                            }

                          //Cambiamos el tipo de ticket
                          $id_tipo_transaccion = 13;
                      }

              }


           


        



          } else if($tarifa_primera_emision_publica || $tarifa_primera_emision_privada) {

              if ($tarifa_primera_emision_privada) {
                  $tipo_tarifa = 'Privada';
                  preg_match('/^KS\-I.*/m', $contenido, $data_fare); //Obtenemos la linea
                  Log::info('Se identifica una primera emision de tarifa privada');
              } else {
                  $tipo_tarifa = 'Publica';
                  preg_match('/^K\-F.*/m', $contenido, $data_fare); //Obtenemos la linea
                  Log::info('Se identifica una primera emision de tarifa publica');
              }


        
              
              if(!isset( $data_fare[0])){
                $error_proceso = true;
                Log::error('La linea de la tarifa esta vacia');
              } else {
                  /**
                    * Quitamos en orden los valores
                    * 1. Facial
                    * 2. Total (En una reemision es la diferencia)
                    */
                  preg_match_all('/(USD|EUR|PYG|GBP)((\d+\.\d+)|(\d+))/', $data_fare[0],$arr_precios); 

                      
                  if(isset($arr_precios[0][0]) && !empty($arr_precios[0][0])){

                        /**
                           * Si tengo 3 valores en mi linea entonces etoy teniendo un equivalente de otra moneda
                           */

                        $primera_moneda = isset($arr_precios[1][0])   ? $arr_precios[1][0] : 'USD';
                        $segunda_moneda = isset($arr_precios[1][1])   ? $arr_precios[1][1] : 'USD';
                     
                        if($primera_moneda != $segunda_moneda){

                            $moneda = isset($arr_precios[1][1])   ? $this->getIdMoneda($arr_precios[1][1]) : null;
                            $facial = isset($arr_precios[2][1])   ? (float) $arr_precios[2][1] : 0.00;
                    
                            //Total ticket
                            $precio  = isset($arr_precios[2][2])  ? (float) $arr_precios[2][2] : 0.00;
                  
                            //En caso del que precio sea cero ya vamos cerar la facial
                            if($precio <= 0.00 || !$precio){
                              $facial = 0.00;
                            }
                        } else {
                            //Facial
                            $moneda = isset($arr_precios[1][0])   ? $this->getIdMoneda($arr_precios[1][0]) : null;
                            $facial = isset($arr_precios[2][0])   ? (float) $arr_precios[2][0] : 0.00;
                  
                            //Total ticket
                            $precio  = isset($arr_precios[2][1])  ? (float) $arr_precios[2][1] : 0.00;
                  
                            //En caso del que precio sea cero ya vamos cerar la facial
                            if($precio <= 0.00 || !$precio){
                              $facial = 0.00;
                            }
                        }
                  
                        
              
                  } else {
                    $error_proceso = true;
                    Log::error('No se pudo identificar ela tarifa del ticket');
                  }
              }


              


          } else {
              $error_proceso = true;
              Log::critical('<!> No se pudo identificar la tarifa de emision del ticket');
          }



      $data_response = [
          'error_proceso' => $error_proceso,
          'moneda' => $moneda,
          'precio' => $precio,
          'facial' => $facial,
          'id_tipo_transaccion' => $id_tipo_transaccion,
          'identificacion_reemision' => $identificacion_reemision,
          'tipo_tarifa' => $tipo_tarifa
      ];
     
      return $data_response;

  }

  /**
   * Obtener los impuestos del ticket sin importar si es tarifa publica o privada
   */
  private function identificarDesgloseImpuestosAmadeus($contenido){
 

        $error_proceso = false;
        $iva_moneda = null;
        $iva = 0.00;
        $tasaYq = 0.00;
        $tasa = 0.00;


              preg_match_all('/^((KFTR.*)|(KSTY.*)|(KSTI.*)|(KFTF.*))/m', $contenido, $output_array_impuestos);

              if (isset($output_array_impuestos[0][0])) {
                  $valores_impuesto = [];
                  //TEnemos que validar que encontramos al menos una linea que contenga valores monetarios
                  foreach ($output_array_impuestos[0] as $value) {
                      //Debe tener al menos un valor
                      if (preg_match('/(USD|EUR|PYG|GBP)((\d+\.\d+)|(\d+))/', $value)) {
                          $valores_impuesto = explode(';', $value);
                          break;
                      }
                  }


                  if (!empty($valores_impuesto)) {
                      foreach ($valores_impuesto as $precio_moneda) {
                          preg_match('/(USD|EUR|PYG|GBP)((\d+\.\d+)|(\d+))/', $precio_moneda, $arr_precios); //Buscamos si hay valores con el patron moneda y valor
                          $pagado = preg_match('/(O(USD|EUR|PYG|GBP))/', $precio_moneda); //Verificamos si ese valor posee el O{codigo_moneda} de pagado
                          $py = preg_match('/(\s+PY\s)/', $precio_moneda); //Verificamos si ese valor posee el impuesto PY
                          $yq = preg_match('/(\s+YQ\s)/', $precio_moneda); //Verificamos si ese valor posee el impuesto YQ


                          //Si no esta pagado entonces sumamos
                          if (!$pagado && !empty($arr_precios)) {
                              //Impuesto PY
                              if ($py) {
                                  $iva_moneda = $arr_precios[1];
                                  $iva += (float) $arr_precios[2];
                              }

                              //Impuesto YQ
                              if ($yq) {
                                  $tasaYq += (float) $arr_precios[2];
                              }

                              //Total impuestos
                              $tasa+= (float) $arr_precios[2];
                              $arr_precios = [];
                          }
                      }//foreach
                  } else {
                      Log::error('No se pudo identificar los impuestos de la linea KFTR,KSTY,KSTI,KFTF en foreach');
                  }
              } else {
                  Log::error('No se pudo identificar el desglose de impuestos');
              }


      $data_response = [
          'error_proceso' => $error_proceso,
          'iva_moneda' => $iva_moneda,
          'iva' => $iva,
          'tasaYq' => $tasaYq,
          'tasa' => $tasa,
      ];

      return $data_response;
    
  }


 


  public function convertirFecha($fecha){
    $dia = substr($fecha, 0, 2);
    $mes = substr($fecha, 2, 3);
    $anho = substr($fecha, -2, 4);
    $mesFormateado = $this->mes($mes);
    return "20".$anho.'-'.$mesFormateado.'-'.$dia;
  } 

  public function mes($mes){
      switch ($mes) {
        case 'JAN':
          return '01';
          break;
        case 'FEB':
          return '02';
          break;
        case 'MAR':
          return '03';
          break;
        case 'APR':
          return '04';
          break;
        case 'MAY':
          return '05';
          break;
        case 'JUN':
          return '06';
          break;
        case 'JUL':
          return '07';
          break;
        case 'AUG':
          return '08';
          break;
        case 'SEP':
          return '09';
          break;
        case 'OCT':
          return '10';
          break;
        case 'NOV':
          return '11';
          break;
        case 'DEC':
          return '12';
          break;
        }
   }
  public function anularTicket(Request $request)
  {
      $mensaje = new \StdClass; 
      try
      {
          $flight = Ticket::find($request->input('dataTicket'));
          $flight->delete();
          $mensaje->status = 'OK';
          $mensaje->mensaje = 'Se ha eliminado el Ticket'; 
      } 
      catch(\Exception $e){
        $mensaje->status = 'ERROR';
        $mensaje->mensaje = 'No se ha eliminado el Ticket'; 
      }
      return json_encode($mensaje);   
    }

    public function getPagoTc(Request $request){
      
      $mensaje = new \StdClass; 

      try{

        DB::beginTransaction();
          $id_ticket = $request->ticket;
          $nro_op = $this->generar_pago_tc($id_ticket, null, $request);

          $mensaje->status = 'OK';
          $mensaje->mensaje = 'Se ha Pagado el Ticket'; 
          $mensaje->id_op = $nro_op;
          DB::commit();

        }catch(\Exception $e){

           Log::error($e);

           $mensaje->status = 'ERROR';
           $mensaje->mensaje = 'No se pudo Pagar el Ticket'; 

           DB::rollBack();
        } catch( ExceptionCustom $e){
          Log::error($e);

             $mensaje->status = 'ERROR';
             $mensaje->mensaje = $e->getMessage(); 
  
             DB::rollBack();
        }


        return response()->json($mensaje);
      }



    public function pagoTicketRealizado(Request $request){
      $tickets = Ticket::where('id',$request->input('id_ticket'))->first();
      return json_encode($tickets);
    } 
    
     public function guardarTicket(Request $request){
      if($request->header('php-auth-user') == 'dtp' && $request->header('php-auth-pw') == '2CTc*9rcHR19'){
         try{
              $ticketFractal = new TicketFractal;
              $ticketFractal->payload = json_encode($request->all());
              $ticketFractal->fecha_alta = date('Y-m-d H:i:00');
              $ticketFractal->channel = null;
              $ticketFractal->datebook = null;
              $ticketFractal->event = null;
              $ticketFractal->technology = null;
              $ticketFractal->loc = null;
              $ticketFractal->save();
              $id = $ticketFractal->id;
              DB::select("SELECT procesar_ticket_fractal(".$id.")");  
           }catch(\Exception $e){
            $response['SaleInfoRS']['statusIntegracionField'] = 'ERROR';
            $response['SaleInfoRS']['messageIntegrationField'] = $e->getMessage();
  
          }  
          $response = [];
            if(isset($id)){
              $response['SaleInfoRS']['statusIntegracionField'] = 'OK';
              $response['SaleInfoRS']['messageIntegrationField'] = 'Mensaje de exito';
              $response['SaleInfoRS']['messageIntegrationFieldId'] = $id;
            }else{
              $response['SaleInfoRS']['statusIntegracionField'] = 'ERROR';
              $response['SaleInfoRS']['messageIntegrationField'] = 'Error al Guardar';
            } 
       }else{
          $response['SaleInfoRS']['statusIntegracionField'] = 'ERROR';
          $response['SaleInfoRS']['messageIntegrationField'] = 'El usuario o la contraseña son incorrectos';
       }
      return json_encode($response);

    }

    
    public function getIdMoneda($moneda){

      $moneda = trim($moneda);

      if($moneda == 'USD'){
           return 143; 
       }

       if($moneda == 'ADT'){
          return 143; 
       }
  
       if($moneda == 'PYG'){
            return 111; 
       } 
   
       if($moneda == 'GBP'){
            return 46; 
       } 
     
       if($moneda == 'EUR'){
            return 43; 
       }  

       Log::error('Moneda no identificada => '.$moneda);

       return null;
    }



    private function eliminar_acentos($cadena){
		
      //Reemplazamos la A y a
      $cadena = str_replace(
      array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
      array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
      $cadena
      );
  
      //Reemplazamos la E y e
      $cadena = str_replace(
      array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
      array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
      $cadena );
  
      //Reemplazamos la I y i
      $cadena = str_replace(
      array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
      array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
      $cadena );
  
      //Reemplazamos la O y o
      $cadena = str_replace(
      array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
      array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
      $cadena );
  
      //Reemplazamos la U y u
      $cadena = str_replace(
      array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
      array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
      $cadena );
  
      //Reemplazamos la N, n, C y c
      $cadena = str_replace(
      array('Ñ', 'ñ', 'Ç', 'ç'),
      array('N', 'n', 'C', 'c'),
      $cadena
      );
      
      return $cadena;
    }

    /**
     * Lista de códigos de tipo de pasajero (PTC)
     */
    private function typePassenger($type){
      $type_p = [
        'ACC'=>'ACCOMPANYING ADULT',
        'ADT'=>'ADULT',
        'AGT'=>'AGENT',
        'ANN'=>'COMPANION WITH AGE',
        'ASB'=>'ADULT STANDBY',
        'ASF'=>'AIR-SEA FARE',
        'AST'=>'AIRLINE STAFF STANDBY',
        'BAG'=>'EXCESS BAGGAGE',
        'BLD'=>'BLIND PASSENGER',
        'BRV'=>'BEREAVEMENT',
        'BUD'=>'AIRLINE BUDDY STANDBY FARE',
        'CCM'=>'CARD CARRYING MEMBER',
        'CLG'=>'CLERGY',
        'CMA'=>'PRIMARY COMPANION',
        'CMM'=>'COMMUTER',
        'CMP'=>'COMPANION',
        'CNN'=>'CHILD',
        'CNV'=>'CONVENTION',
        'CPN'=>'COUPON DISCOUNT PSGR',
        'CSB'=>'CHILD STANDBY',
        'CTZ'=>'CATEGORY Z PSGR',
        'DNN'=>'CNN EQUIVALENT',
        'DOD'=>'DEPARTMENT OF DEFENSE',
        'EMI'=>'EMIGRANT',
        'ENN'=>'GROUP INCLUSIVE TOUR CHILD',
        'FAM'=>'FAMILY MEMBER',
        'FFY'=>'FREQUENT FLYER',
        'ACC'=>'FAMILY PLAN CHILD',
        'FFN'=>'GOVERNMENT CONTRACT',
        'GCF'=>'CITY/COUNTY GOVERNMENT TRAVEL',
        'GCT'=>'GOVERNMENT EMPLOYEE DEPENDENT',
        'GDP'=>'GOVERNMENT EXCHANGE PSGR',
        'GEX'=>'GROUP INCLUSIVE TOUR',
        'GIT'=>'GOV / CT / MIL / CATZ',
        'GMZ'=>'GROUP CHILD',
        'GNN'=>'GROUP PSGR',
        'GRP'=>'GROUP STUDENT PARTY',
        'GSP'=>'STATE GOVERNMENT',
        'GST'=>'GOVERNMENT CHILD',
        'GTC'=>'GOVERNMENT TRAVEL',
        'GTF'=>'GOV / GOVCT / MIL',
        'GVM'=>'GOVERNMENT TRAVEL',
        'GVZ'=>'GOVERNMENT / GOVCT / CATZ',
        'HNN'=>'PRIVATE CHILD',
        'HOF'=>'HEAD OF FAMILY',
        'ICP'=>'INCENTIVE CERTIFICATES',
        'INF'=>'INFANT NOT OCCUPYING A SEAT',
        'INS'=>'INFANT WITH A SEAT',
        'IIT'=>'INDIVIDUAL INCLUSIVE TOUR ADULT',
        'INN'=>'ou ITS	INDIVIDUAL INCLUSIVE TOUR CHILD',
        'ITF'=>'INDIVIDUAL INCLUSIVE TOUR INFANT',
        'ITX'=>'INDIVIDUAL INCLUSIVE TOUR PSGR',
        'JOB'=>'JOB CORPS',
        'LBR'=>'LABORER/WORKER',
        'LIF'=>'LABORER/WORKER INFANT',
        'LNN'=>'LABORER/WORKER CHILD',
        'MBT'=>'MILITARY-BASIC TRAINING GRADUATE',
        'MCR'=>'MILITARY CHARTER',
        'MDP'=>'MILITARY DEPENDENT',
        'MED'=>'PATIENTS TRAVEL. FOR MEDICAL TREATMT',
        'MFM'=>'MILITARY IMMEDIATE FAMILY',
        'MIL'=>'MILITARY CONFIRMED PSGR',
        'MIR'=>'MILITARY RESERVE ON ACTIVE DUTY',
        'MIS'=>'MISSIONARY',
        'MLZ'=>'MILITARY / CATZ',
        'MNN'=>'MILITARY CHILD',
        'MPA'=>'MILITARY PARENTS/PARENTS IN LAW',
        'MRE'=>'MILITARY RETIRED AND DEPENDENTS',
        'MSB'=>'MILITARY STANDBY',
        'MSG'=>'MULTI STATE GOVERNMENT',
        'MUS'=>'MILITARY/DOD AND DEPENDENTS IN USA',
        'MXS'=>'MILITARY/DOD,DEPNDTS NOT BASED IN US',
        'NAT'=>'NATO',
        'NSB'=>'NONREVENUE STANDBY',
        'OTS'=>'PSGRS OCCUPYING TWO SEATS',
        'PCF'=>'PRIVATE CHILD',
        'PCR'=>'ADULT CHARTER',
        'PIL'=>'PILGRIM',
        'PMP'=>'NEGOTIATED',
        'PNN'=>'CHILDREN CHARTER',
        'PON'=>'NEGOTIATED',
        'REC'=>'MILITARY RECRUIT',
        'REF'=>'REFUGEE',
        'SDB'=>'STUDENT STANDBY',
        'SEA'=>'SEAMAN',
        'SPA'=>'ACCOMPANIED SPOUSE',
        'SPH'=>'HEAD OF FAMILY SPOUSE',
        'SPS'=>'SPOUSE',
        'SRC'=>'SENIOR CITIZEN',
        'STR'=>'STATE RESIDENT',
        'STU'=>'STUDENT',
        'TNN'=>'FREQUENT FLYER CHILD',
        'TUR'=>'TOUR CONDUCTOR',
        'UAM'=>'UNACCOMPANIED MINOR',
        'UNN'=>'UNACCOMPANIED CHILD',
        'VAC'=>'VISIT ANOTHER COUNTRY ADULT',
        'VAG'=>'VISIT ANOTHER COUNTRY GROUP ADULT',
        'VFR'=>'VISIT FRIENDS/RELATIVES',
        'VNN'=>'VISIT ANOTHER COUNTRY CHILD',
        'VUG'=>'VISIT ANOTHER COUNTRY ADULT',
        'VUS'=>'VISIT ANOTHER COUNTRY ADULT',
        'WEB'=>'INTERNET FARE',
        'YCB'=>'SENIOR CITIZEN STANDBY',
        'YCR'=>'YOUTH CHARTER',
        'YNN'=>'GOVERNMENT TRAVEL CHILD',
        'YSB'=>'YOUTH STANDBY',
        'YTH'=>'YOUTH CONFIRMED',
        'ZNN'=>'VISIT ANOTHER COUNTRY GROUP'
      ];

      return isset($type_p[$type]) ? $type_p[$type] : 'N/A';
        
    }




}//class

 


