<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use App\Dtp\Proveedor;
use App\Reserva;
use App\Usuario;
use App\Parametros;
use App\Sucursal;
use App\Agencias;
use App\Perfil;
use App\State;
use App\DestinosPromo;
use App\ActividadesPromo;
use App\DetallesPromo;
use App\DtpPlus;
use Session;
use Redirect;
use DB;

class UserController extends Controller
{
	/*
	Página de perfil de usuario
	-Inica la pagina  del perfil y datos de usuario
	*/
	private function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}

	public function perfilUsuario(Request $req){
		$client = new Client();
		$iibReq = new \StdClass;
		$iibReq->token = "ertetrytrey";
		$iibReq->idioma = "CAS";
		$iibReq->usuario = "dtpmundol2@dtpmundo.com";
		$datosUsuario = new \StdClass;
		$datos = Session::get('datos-loggeo');
		$datosUsuario = $datos;

		$usuarios = DB::select("SELECT * FROM usuarios where id_usuario = ". Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario);

		$usuario = json_encode($usuarios);

		$agentes = DB::select("SELECT * FROM usuarios where activo = 'S' AND id_perfil = 10  and id_agencia = ". Config::get('constants.agenciaDtp'));



		$arrayAgente = [];
		foreach($agentes as $key=>$agente){
			$arrayAgente[$key]['value']= $agente->id_usuario;
			$arrayAgente[$key]['label']= $agente->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agente->usuario;
		}	

		return view('pages.perfilUsuario')->with(['datosUsuario'=>$datosUsuario, 'usuario'=>$usuarios[0], 'selectAgentes'=>$arrayAgente]);
	}

	public function doEditPerfil(Request $req){
			$tipo= $req->input('tipo');
			$usuarioId= $req->input('id_usuario');
			switch ($tipo){
			case 1:
			    DB::table('usuarios')
					            ->where('id_usuario',$req->input('id_usuario'))
					            ->update([
					            			'nombre_apellido'=>$req->input('nombre_apellido'),
					            			]);
				flash('El nombre de su usuario fue modificado')->success();
				return Redirect::back();		            
			    break;
			case 2:
			    if(!ctype_space($req->input('pass'))){
		       		$password = md5(Config::get('constants.semilla_left') ."".$req->input('pass')."".Config::get('constants.semilla_right'));
		         	DB::table('usuarios')
						            ->where('id_usuario',$req->input('id_usuario'))
						            ->update([
						            			'password' => $password,
						            			]);
						$userData = Usuario::where('id_usuario', '=', $req->input('usuer_id'))->get();
						flash('Su contraseña fue grenerada y sera efectiva una vez reinicie la sessión')->success();
						return Redirect::back();
		    	}else{
					flash('La contraseña no puede estar compuesta por espacios en blanco')->error();
					return Redirect::back();
		    	}
			    break;
			case 3:
				//dd($req->all());
				DB::table('usuarios')
								->where('id_usuario',$req->input('id_usuario'))
								->update([
											'codigo_agente' => $req->input('codigo_agente'),
											]);

					$usuarios = DB::select("SELECT * FROM usuarios where id_usuario = ". Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario);
					foreach($usuarios as $agente){
						$nombreAgente = $agente->nombre_apellido;
					}

					$datos_loggeo = Session::get('datos-loggeo');
					$datosUsuarios = new \StdClass;		
					$datosUsuarios = Session::get('datos-loggeo')->datos->datosUsuarios; 
					$datosUsuarios->codAgente = $req->input('codigo_agente');
					$datosUsuarios->nomAgente = $nombreAgente;
					$datos = new \StdClass;	
					$datos->datosUsuarios = $datosUsuarios;
					$datos_loggeo->datos = $datos;

					Session::put('datos-loggeo',$datos_loggeo);	

					flash('Su vendedor DTP fue actualizado')->success();
					return Redirect::back();
					
					
			    //print_r($req->input('codigo_agente'));
			    break;
			}
	}	

	/*
	Listado de usuarios
	Da el listado de todos los usuarios registrados
	*/
	public function index(){
		$listaUsuarios =[];

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			$usuarios = Usuario::with('agencia','sucursal','perfil')->get();
		}else{
			$usuarios = Usuario::with('agencia','sucursal','perfil')
								->where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
								->get();	
		}
		/*
		foreach($usuarios as $key=>$usuario){
			
			$listaUsuarios[$key]['id'] = $usuario->id_usuario;
			$listaUsuarios[$key]['email'] = $usuario->email;
			$listaUsuarios[$key]['activo'] = $usuario->activo;
			$listaUsuarios[$key]['perfil'] = $usuario->perfil['des_perfil'];
			$listaUsuarios[$key]['caducidad'] = date('d/m/Y', strtotime($usuario->fecha_validez));
			$listaUsuarios[$key]['agencia'] = $usuario->agencia->razon_social;
			$listaUsuarios[$key]['sucursal'] = $usuario->sucursal->descripcion_agencia;
			$listaUsuarios[$key]['password'] = $usuario->password;
			
		}
		return view('pages.users.usuarios')->with('usuarios', json_decode(json_encode($listaUsuarios), FALSE));
		*/
		//dd($usuarios);
		return view('pages.users.usuarios')->with('usuarios', $usuarios, FALSE);
		
	}	

	/*
	Página para agregar usuarios
	*/
	public function add(Request $req){
		$arrayPerfil = [];
		$arrayAgencia = [];
		$arraySucursal = [];

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			$perfiles = Perfil::all();
			$agencias = Agencias::all();
			$sucursales = Sucursal::all();			
		}else{
			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
				$perfiles = Perfil::where('id_perfil', '=',Config::get('constants.vendedor'))->get();
				$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
				$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
			}else{
				if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('adminAgencia')){
					$perfiles = Perfil::where('id_perfil', '=',Config::get('constants.vendedor'))
										->orWhere('id_perfil', '=',Config::get('constants.adminOpAdmin'))
										->get();
					$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
					$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
				}else{
					$perfiles = Perfil::where('id_perfil', '=',10)->get();
					$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
					$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
				}	
			}	
		}

		foreach ($perfiles as $key=>$perfil){
			$arrayPerfil[$key]['value']= $perfil->id_perfil;
			$arrayPerfil[$key]['label']= $perfil->des_perfil;
		}

		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}

		foreach ($sucursales as $key=>$sucursal){
			$arraySucursal[$key]['value']= $sucursal->id_sucursal_agencia;
			$arraySucursal[$key]['label']= $sucursal->descripcion_agencia;
		}

		//Inicio de Generación de Listado de agentes DTP
		$agents = DB::select("SELECT * FROM usuarios where id_perfil = 10  or id_perfil = 3 and id_agencia = ". Config::get('constants.agenciaDtp')." and activo='S'");

		$listaAgente = [];
		foreach($agents as $key=>$agent){
			$listaAgente[$key]['value']=$agent->id_usuario;
			$listaAgente[$key]['label']= $agent->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agent->usuario;
		}

		return view('pages.users.usuariosAdd')->with(['listadoPerfil'=>$arrayPerfil, 'listadoAgencia'=>$arrayAgencia, 'listadoSucursal'=>$arraySucursal, 'listAgenteDtp'=>$listaAgente]);
	}

	/*
	Página para editar usuarios
	*/
	public function edit(Request $req, $id){
		$usuario = Usuario::where('id_usuario', '=',$id)->get();
		$arrayPerfil = [];
		$arrayAgencia = [];
		$arraySucursal = [];

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			$perfiles = Perfil::all();
			$agencias = Agencias::all();
			$sucursales = Sucursal::all();			
		}else{
			$perfiles = Perfil::where('id_perfil', '=',Config::get('constants.adminAgencia'))->get();
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
			$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
		}

		foreach ($perfiles as $key=>$perfil){
			$arrayPerfil[$key]['value']= $perfil->id_perfil;
			$arrayPerfil[$key]['label']= $perfil->des_perfil;
		}

		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}

		foreach ($sucursales as $key=>$sucursal){
			$arraySucursal[$key]['value']= $sucursal->id_sucursal_agencia;
			$arraySucursal[$key]['label']= $sucursal->descripcion_agencia;
		}

		//Inicio de Generación de Listado de agentes DTP
		$agents = DB::select("SELECT * FROM usuarios where id_perfil = 10 or id_perfil = 3 and id_agencia = ". Config::get('constants.agenciaDtp')." and activo='S'");

		$listaAgente = [];
		foreach($agents as $key=>$agent){
			$listaAgente[$key]['value']=$agent->id_usuario;
			$listaAgente[$key]['label']= $agent->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agent->usuario;
		}

		return view('pages.users.usuariosEdit')->with(['usuario'=>$usuario,'listadoPerfil'=>$arrayPerfil, 'listadoAgencia'=>$arrayAgencia, 'listadoSucursal'=>$arraySucursal, 'listAgenteDtp'=>$listaAgente]);
	}	

	/*
	Hacer Agregar
	Función para agregar los datos de usuario
	*/
	public function doAdd(Request $request){
		$usuario = Usuario::where('email', '=',$request->input('email'))->get();
		if(count($usuario)){
			flash('Ya existe un usuario con ese email');
		}
		else{
			$usuario = new Usuario;
			
			/* Se comprueba si los datos solo con espacios en blanco*/
			if(!ctype_space($request->input('nombreApellido'))){
				$usuario->nombre_apellido = $request->input('nombreApellido');
			}else{
				flash('El nombre esta formado por espacios vacios')->error();
				return Redirect::back();
			}
			$usuario->password = $request->input('email');
			$usuario->email = $request->input('email');
			$usuario->usuario = $usuario->email;
			if($request->input('activo') == 'true') $usuario->activo = 'S';
			else $usuario->activo = 'N';
			$usuario->id_agencia = $request->input('agencia_id');
			$usuario->id_sucursal_agencia = $request->input('sucursal_id');
			$usuario->id_perfil = $request->input('perfil_id');
			$usuario->codigo_agente = $request->input('codigo_agente');
			$usuario->fecha_ingreso_promo = null;
			try{
				$usuario->save();
				$id = Usuario::select('id_usuario')->max('id_usuario');
				//yaii parece que no modifica el fecha_promo por default en update! (pero hay que poner null en insert)
	         	DB::table('usuarios')
					            ->where('id_usuario',$id)
					            ->update([
					            			'id_sistema_facturacion'=>$id
					            			]);
				
				//actualizar en usuarios si es que se recibio respuesta positiva/????
				flash('Se ha ingresado el usuario exitosamente')->success();
			} catch(\Exception $e){
				flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			}
			
		}

		//return "deluxe";
		return redirect()->route('usuarios');
	}

	/*
	Hacer Editar
	Función para modificar los datos de usuario
	*/
	public function doEdit(Request $request){

 		$id_usuario = $request->input('idSucursalAgencia');

		if(!ctype_space($request->input('nombreApellido'))){
 			$nombre = $request->input('nombreApellido');
 		}else{
			flash('El nombre esta formado por espacios vacios')->error();
			return Redirect::back();
 		}

 		if($request->input('activo') == 'true'){
 			$activo = 'S';
 		}else{
 			$activo = 'N';
 		}
 		if($request->input('password') != ""){
			$password = md5(Config::get('constants.semilla_left') ."".$request->input('password')."".Config::get('constants.semilla_right'));
 		}else{
			$usuario = Usuario::where('id_usuario', '=',$id_usuario)->get();
			$password  = $usuario[0]['password'];
 		}

	    DB::table('usuarios')
					    ->where('id_usuario',$request->input('idSucursalAgencia'))
					    ->update([
					            'nombre_apellido'=>$nombre,
					            'email' =>$request->input('email'),
					            'password'=>$password,
					            'activo'=>$activo,
					            'id_agencia' => $request->input('agencia_id'),
					            'id_sucursal_agencia'=>$request->input('sucursal_id'),
					            'id_perfil'=>$request->input('perfil_id'),
					            'codigo_agente' => $request->input('codigo_agente')
					            ]);
		flash('Se ha editado el usuario exitosamente')->success();
		return redirect()->route('usuarios');
	}

		/*
	Recuperar Contraseña
	-Página para ingresar la contraseña en un usuario
	*/
	public function recuperarContrasenha(Request $request){

		$usuario = Usuario::where('email', '=',$request->input('recuperar-email'))->get();
		if(isset($usuario[0]->usuario)){
			$usuarioReg = $usuario[0]->usuario;
			$client = new Client([
						'headers' => [ 
										'Content-Type' => 'text/html',
										'usuario'=> $usuarioReg,
										]
					]);

			try{
				$iibRsp = $client->get(Config::get('config.iibRestContrasenha'));
			}
	 		catch(RequestException $e){
				$mensaje = 'Ha ocurrido un error en su pedido, Intentelo nuevamente';
			}
			catch(ClientException $e){
				$mensaje = 'Ha ocurrido un error en su pedido, Intentelo nuevamente';
			}
			$mensaje = '<b>El pedido se ha realizado exitosamente</b><br>Se le ha enviado un correo con los datos para recuperar su contraseña';
		}else{
			$mensaje = '<b class= "textRed">El usuario solicitado no existe, Intentelo nuevamente<b>';
		}
		return $mensaje;
	}

	/*
	Activar Cuenta
	-Página para ingresar la contraseña en un usuario
	*/
	public function activateAcount(Request $req){
		Session::forget('datos-loggeo');
		if($req->id !=""){
			$idUsuario = $req->id;
			$usuario = Usuario::where('codigo_activacion', '=',$idUsuario)->get();
			if(!isset($usuario[0]->id_usuario)){
				return view('pages.errorUsuario');
			}
			return view('pages.users.activateAcount')->with('usuario', $usuario);
		}else{
			return view('pages.errorUsuario');
		}	
	}

  /*
    Activar password
    - Se utiliza GET para evitar la exposicion directa de los datos fuera del Middleware
      autenticado       
    */
    public function activarPassword(Request $request){
        if(!empty($request->input('usuer_id'))){
        	if(!ctype_space($request->input('pass'))){
	       		$password = md5(Config::get('constants.semilla_left') ."".$request->input('pass')."".Config::get('constants.semilla_right'));
	         	DB::table('usuarios')
					            ->where('id_usuario',$request->input('usuer_id'))
					            ->update([
					            			'codigo_activacion'=>null,
					            			'password' => $password,
					            			'activo'=>"S"
					            			]);
	    			Session::flush();
					$userData = Usuario::where('id_usuario', '=', $request->input('usuer_id'))->get();
					flash('Su contraseña fue grenerada y su usuario activado')->success();
					return redirect()->route('login');
	    	}else{
				flash('La contraseña no puede estar compuesta por espacios en blanco')->error();
				return Redirect::back();
	    	}
		}
	}
	/*
	Consultar Factura
	- Pagina para ingresar los requerimientos de busqueda de facturas
	*/
    public function consultaFacturas(){
    	$arrayAgencia = [];
    	$arrayUsuario = [];
    	//Filtro para Administrador DTP
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
	    	$agencias = Agencias::where('activo', '=', 'S')->get();
			foreach ($agencias as $key=>$agencia){
				$arrayAgencia[$key]['value']= $agencia->id_sistema_facturacion;
				$arrayAgencia[$key]['label']= $agencia->razon_social;
			}
			$usuarios = Usuario::where('activo', '=', 'S')->get();
    	}
    	//Filtro para Administrador Agencia
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminAgencia')){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
				->where('activo', '=', 'S')
				->get();
			$usuarios = Usuario::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
			->where('activo', '=', 'S')
			->get();

    	}
    	//Filtro para Vendedor
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.vendedor')){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
			$usuarios = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
    	}
    	if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==10){
			$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
			$usuarios = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
    	}

    	//Se arman las listas para la página
		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_sistema_facturacion;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}

		foreach ($usuarios as $key=>$usuario){
			$arrayUsuario[$key]['value']= $usuario->id_sistema_facturacion;
			$arrayUsuario[$key]['labelUsuario']= $usuario->usuario;
			$arrayUsuario[$key]['labelApellidoNombre']= $usuario->nombre_apellido;
		}
		$usuario = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
		$agency = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();

		return view('pages.users.consultaFactura')->with(['listadoAgencia'=>$arrayAgencia,'listadoUsuarios'=>$arrayUsuario, 'usuario'=>$usuario, 'agency'=>$agency]);  
	}

	/*
	Hacer consulta de Factura
	-Se utiliza para realizar la busqueda de las facturas
	*/
    public function doHistoricoFacturaMc(Request $request){
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json' ]
		]);

		$cuerposFactura =[];
		$restDetallesFactIn = new \StdClass;
		$restDetallesFactIn->reqToken = "ertetrytrey";
		$restDetallesFactIn->ts= date('Y-m-d h:m:s');
		$detalles = new \StdClass;

		if(!empty($request->input('desde'))){
			$FechaDesde = explode("/", $request->input('desde'));
			$detalles->FechaDesde = $FechaDesde[2]."-".$FechaDesde[1]."-".$FechaDesde[0];
		}else{
			$detalles->FechaDesde = "";
		}

		if(!empty($request->input('hasta'))){
			$FechaHasta = explode("/", $request->input('hasta'));
			$detalles->FechaHasta = $FechaHasta[2]."-".$FechaHasta[1]."-".$FechaHasta[0];
		}else{
			$detalles->FechaHasta = "";
		}
		
		$detalles->VendedorId = $request->input('vendedor_id');
		
		if($request->input('vendedor_id') == 0){
			//y no se eligio agencia
			$detalles->AgenciaId = Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia;
		}else{
			$usuario = Usuario::where('id_usuario', '=',$request->input('vendedor_id'))->get();			
			$detalles->AgenciaId = $usuario[0]['id_agencia'];

		}

		$restDetallesFactIn->Detalles = $detalles;
		$iibReq = new \StdClass;
		$iibReq->RestDetallesFactIn = $restDetallesFactIn;

		try{
			$iibRsp = $client->post(Config::get('config.iibFacturasAdminAgencia'),
				['body' => json_encode($iibReq)]
			);
		}
		catch(RequestException $e){
			return view('pages.mc.timeErrorConexion');
		}
		catch(ClientException $e){
			return view('pages.mc.errorConexion');	
		}

		$iibObjRsp = json_decode($iibRsp->getBody());
		if(isset($iibObjRsp->RestDetallesFactOut->DatosFactura)){
			foreach($iibObjRsp->RestDetallesFactOut->DatosFactura as $key=>$datosFactura){
				if(!empty($request->input('estado'))){
					if(strtolower($datosFactura->FacturasEstado) == strtolower($request->input('estado'))){
						switch (strtolower($datosFactura->FacturasEstado)) {
							case 'pendiente':
								$color= "red";
							break;
							case 'anulado':
								$color= "blue";
							break;
							case 'cobrado':
								$color= "green";
							break;
						}						
						$fecha = $FechaDesde = explode("-", $datosFactura->FacturasFecha);
						$cuerposFactura[$key]['factura_id'] = $datosFactura->FacturasId;
						$cuerposFactura[$key]['factura_fecha'] = $fecha[2]."/".$fecha[1]."/".$fecha[0];
						$cuerposFactura[$key]['factura_numero'] = "<b>".$datosFactura->FacturasNumero."</b>";
						$cuerposFactura[$key]['cliente_id'] = $datosFactura->ClienteId;
						$cuerposFactura[$key]['ruc_cliente'] = $datosFactura->RucCliente;
						$cuerposFactura[$key]['nombre_cliente'] = $datosFactura->FacturasNombrePasajero;
						$cuerposFactura[$key]['factura_moneda'] = $datosFactura->FacturaMonedaNombre;
						$cuerposFactura[$key]['factura_destinacion'] = $datosFactura->FacturasDestinationName;
						$cuerposFactura[$key]['factura_total'] = $datosFactura->FacturasTotalDetalles;
						$cuerposFactura[$key]['factura_vendedor'] = $datosFactura->FacturasVendedor;
						$cuerposFactura[$key]['factura_vendedor_id'] = $datosFactura->FacturasVendedorId;
						$cuerposFactura[$key]['estado'] = '<b style="color: '.$color.';">'.$datosFactura->FacturasEstado.'</b>';
						$cuerposFactura[$key]['saldo'] = "<b>".$datosFactura->FacturasSaldoT."</b>";
						$cuerposFactura[$key]['url_factura'] = $datosFactura->UrlFactura;
						$cuerposFactura[$key]['url_voucher'] = $datosFactura->UrlVoucher;
					}	
				}else{
						switch (strtolower($datosFactura->FacturasEstado)) {
							case 'pendiente':
								$color= "red";
							break;
							case 'anulado':
								$color= "blue";
							break;
							case 'cobrado':
								$color= "green";
							break;
						}						
						$fecha = $FechaDesde = explode("-", $datosFactura->FacturasFecha);
						$cuerposFactura[$key]['factura_id'] = $datosFactura->FacturasId;
						$cuerposFactura[$key]['factura_fecha'] = $fecha[2]."/".$fecha[1]."/".$fecha[0];
						$cuerposFactura[$key]['factura_numero'] = "<b>".$datosFactura->FacturasNumero."</b>";
						$cuerposFactura[$key]['cliente_id'] = $datosFactura->ClienteId;
						$cuerposFactura[$key]['ruc_cliente'] = $datosFactura->RucCliente;
						$cuerposFactura[$key]['nombre_cliente'] = $datosFactura->FacturasNombrePasajero;
						$cuerposFactura[$key]['factura_moneda'] = $datosFactura->FacturaMonedaNombre;
						$cuerposFactura[$key]['factura_destinacion'] = $datosFactura->FacturasDestinationName;
						$cuerposFactura[$key]['factura_total'] = $datosFactura->FacturasTotalDetalles;
						$cuerposFactura[$key]['factura_vendedor'] = $datosFactura->FacturasVendedor;
						$cuerposFactura[$key]['factura_vendedor_id'] = $datosFactura->FacturasVendedorId;
						$cuerposFactura[$key]['estado'] = '<b style="color: '.$color.';">'.$datosFactura->FacturasEstado.'</b>';
						$cuerposFactura[$key]['saldo'] = "<b>".$datosFactura->FacturasSaldoT."</b>";
						$cuerposFactura[$key]['url_factura'] = $datosFactura->UrlFactura;				
						$cuerposFactura[$key]['url_voucher'] = $datosFactura->UrlVoucher;
					}	
			}	
		}
		$facturas =  json_decode(json_encode($cuerposFactura), FALSE);
		return response()->json($facturas);
	}

    public function doConsultaFactura(Request $request){
    	$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json' ]
		]);
		$cuerposFactura =[];
		$restDetallesFactIn = new \StdClass;
		$restDetallesFactIn->reqToken = "ertetrytrey";
		$restDetallesFactIn->ts= date('Y-m-d h:m:s');
		$detalles = new \StdClass;

		if(!empty($request->input('desde'))){
			$FechaDesde = explode("/", $request->input('desde'));
			$detalles->FechaDesde = $FechaDesde[2]."-".$FechaDesde[1]."-".$FechaDesde[0];
		}else{
			$detalles->FechaDesde = "";
		}

		if(!empty($request->input('hasta'))){
			$FechaHasta = explode("/", $request->input('hasta'));
			$detalles->FechaHasta = $FechaHasta[2]."-".$FechaHasta[1]."-".$FechaHasta[0];
		}else{
			$detalles->FechaHasta = "";
		}
		
		//si no se eligio agencia
		if($request->input('agencia_id') == 0){
			//y no se eligio vendedor
			if($request->input('vendedor_id') == 0){
				$agenciasLogueo = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
				//dd($agenciasLogueo[0]->idAgencia);
				foreach ($agenciasLogueo as $key=>$agencia){
					//$agenciaFacturacionId = $agencia->id_sistema_facturacion;
					$agenciaFacturacionId = $agencia->id_agencia;

				}
				//se elige la agencia que corresponde al usuario que hizo el pedido
				$detalles->AgenciaId = $agenciaFacturacionId;
			}
			//aca creo que nunca entra
			else{
				$detalles->AgenciaId = 0;
			}	
		}
		//sino, se elige la agencia seleccionada (supongo que solo si es DTP deberia de poder)
		else{
			$detalles->AgenciaId =  $request->input('agencia_id');
		}
		//$detalles->AgenciaId = 0;
		//si no se eligio vendedor
		if($request->input('vendedor_id') == 0){
			//y no se eligio agencia
			if($request->input('agencia_id') == 0){
				//$detalles->VendedorId = $request->session()->get('datos-loggeo')->datos->datosUsuarios->id_sistema_facturacion;
				//se elige el id de ese usuario
				$detalles->VendedorId = $request->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario;

			}
			//se elige el id 0 (supongo que corresponde a todos los vendedores)
			else{
				$detalles->VendedorId = 0;
			}
		}else{
			$detalles->VendedorId = $request->input('vendedor_id');
		}
		//$detalles->VendedorId = 7;
		$restDetallesFactIn->Detalles = $detalles;
		$iibReq = new \StdClass;
		$iibReq->RestDetallesFactIn = $restDetallesFactIn;

			$iibRsp = $client->post(Config::get('config.iibConFactura'),
				['body' => json_encode($iibReq)]
			);
		$iibObjRsp = json_decode($iibRsp->getBody());
		if(isset($iibObjRsp->RestDetallesFactOut->DatosFactura)){
			foreach($iibObjRsp->RestDetallesFactOut->DatosFactura as $key=>$datosFactura){
				$fecha = $FechaDesde = explode("-", $datosFactura->FacturasFecha);
				$cuerposFactura[$key]['factura_id'] = $datosFactura->FacturasId;
				$cuerposFactura[$key]['factura_fecha'] = $fecha[2]."/".$fecha[1]."/".$fecha[0];
				$cuerposFactura[$key]['factura_numero'] = $datosFactura->FacturasNumero;
				$cuerposFactura[$key]['cliente_id'] = $datosFactura->ClienteId;
				$cuerposFactura[$key]['ruc_cliente'] = $datosFactura->RucCliente;
				$cuerposFactura[$key]['nombre_cliente'] = $datosFactura->FacturasNombrePasajero;
				$cuerposFactura[$key]['factura_moneda'] = $datosFactura->FacturaMonedaNombre;
				$cuerposFactura[$key]['factura_destinacion'] = $datosFactura->FacturasDestinationName;
				$cuerposFactura[$key]['factura_total'] = $datosFactura->FacturasTotalDetalles;
				$cuerposFactura[$key]['factura_vendedor'] = $datosFactura->FacturasVendedor;
				$cuerposFactura[$key]['factura_vendedor_id'] = $datosFactura->FacturasVendedorId;
				$cuerposFactura[$key]['url_factura'] = $datosFactura->UrlFactura;
				$cuerposFactura[$key]['url_vaucher'] = $datosFactura->UrlVoucher;
			}	
		}
		$facturas =  json_decode(json_encode($cuerposFactura), FALSE);
		return response()->json($facturas);
    }

    public function getUsuarios(Request $req){
    	if(isset($req->dataSucursal)){
    		$usuarios = Usuario::where('id_usuario', '=',$req->dataUsuario)
    							->orWhere('id_sucursal_agencia', '=',$req->dataSucursal)
    							->get();
    	}else{
			$usuarios = Usuario::where('id_usuario', '=',$req->dataUsuario)->get();
    	}
		$arrayUsuario = [];
		foreach ($usuarios as $key=>$usuario){
			$arrayUsuario[$key]['value']= $usuario->id_usuario;
			$arrayUsuario[$key]['label']= $usuario->usuario." - ".$usuario->nombre_apellido;
		}
		return json_encode($arrayUsuario);
	}

	public function getUsuariosAgencia(Request $req){
    	if(isset($req->dataAgencia)){
    		$usuarios = Usuario::where('id_agencia', '=',$req->dataAgencia)
    							->get();
    	}else{
			$usuarios = Usuario::all();
    	}
		$arrayUsuario = [];
		foreach ($usuarios as $key=>$usuario){
			$arrayUsuario[$key]['value']= $usuario->id_usuario;
			$arrayUsuario[$key]['label']= $usuario->usuario." - ".$usuario->nombre_apellido;
		}
		return json_encode($arrayUsuario);
	}
	public function ventas(Request $req){
		return view('pages.ventas');
	}	

	public function formularioPromocion(Request $req){
		$usuarios = Usuario::where('id_usuario', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)
								->get();
		if($usuarios[0]->tiene_promo =="N"){
			return view('pages.formularioPromocion')->with(['usuario'=>$usuarios]);
		}else{
			flash('Ya estas suscripto a la promo!.')->success();
			return redirect('home');
		}							
	}	

	public function postPromocion(Request $req){

		if(isset($req->anho)&&isset($req->mes)&&isset($req->dia)){
			$fechaNac = $req->anho."-".$req->mes."-".$req->dia;
		}else{
			flash('Ingrese una fecha de nacimiento válida')->error();
			return Redirect::back();
		}
		for ($x = 1; $x < 3; $x++){			
			for ($i = 1; $i < 4; $i++){
				$destinos = new DestinosPromo;
				if($x == 1){
			   	 $destinos->destino_id = $req->input('destino_conocer'.$i);
				}else{
			   	 $destinos->destino_id = $req->input('destino_venta'.$i);
				}
				if($destinos->destino_id != ""){
				    $destinos->tipo_destino_id = $x;
				    $destinos->id_usuario = $req->input('id_usuario');
				    $destinos->orden = $i;

					$savet = $destinos->save();
					if(!$savet){
							flash('Uno o varios de los destinos no existen')->error();
							return Redirect::back();				
					}
				}	
			}	
		}	
		if(isset($req->actividades)){
			foreach($req->actividades as $key=>$actividad){
				$actividades = new ActividadesPromo;
				$actividades->id_usuario =  $req->input('id_usuario');
				$actividades->actividad = $actividad;
				$saveds = $actividades->save();
				if(!$saveds){
					flash('Una o varias de las actividades no existen')->error();
					return Redirect::back();				
				}
			}
		}
		$detalles = new DetallesPromo;
		$detalles->usuario_id = $req->input('id_usuario');
		$detalles->celular = $req->input('celular');
		$detalles->color = $req->input('color');
		$detalles->descripcion_varios = $req->input('descripcion_varios');
		$saved = $detalles->save();
		if(!$saved){
			flash('Uno de los datos no estan correctos')->error();
			return Redirect::back();				
		}

		$fecha = explode("/", $req->input('fechaNacimiento'));
	 	DB::table('usuarios')
					->where('id_usuario',$req->input('id_usuario'))
					->update([
					        'fecha_nacimiento'=>$fechaNac,
					        'celular' => $req->input('celular'),
					        'documento_identidad' => $req->input('documento_identidad'),
					        'tiene_promo' => 'S',
					        'fecha_ingreso_promo'=>'now()'
					        ]);
		Session::put('tiene_promo',"S");
		flash('Ya estas suscripto a la promo: TODOS GANAN CON DTP')->success();
		return view('pages.vistapromocion');
	}

	public function vistaPromocion(Request $req){
		if($req->session()->get('datos-loggeo')->datos->datosUsuarios->idPais == 2){
			return view('pages.vistapromocion');
		}else{
			flash('Esta promoción es valida solo para el territorio paraguayo')->warning();
			return Redirect::back();
		}
	}	

	public function getResultadosPromos(Request $req){
		include("../mes.php");
		/*
			cambiar la url, por mid.dtpmundo.com
			controlar si el estado es distinto a OK y no mostrar las pentañas como estoy y detalle
			poner el separador de miles
		*/
		$client = new Client();
		$iibRsp = $client->get('http://181.40.66.162:9292/ControlDTP/resources/estadisticas/promo?idVendedor='.$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario.'&detalle='.$req->input('dataTipo'));
		$iibObjRsp = json_decode($iibRsp->getBody());
		/*$iibRsp = $client->get('http://181.40.66.162:9292/ControlDTP/resources/estadisticas/promo?idVendedor=603&detalle='.$req->input('dataTipo'));*/
		if($req->input('dataTipo') =="N"){
			foreach($iibObjRsp->meses as $key=>$meses){
				$mesTiempo = explode("-",$meses->mes);
				if($mesTiempo[0] <= 9){
					$mes0 = "0".$mesTiempo[0];
				}else{
					$mes0 = $mesTiempo[0];
				}
				$iibObjRsp->meses[$key]->mes = $mes[$mes0]." - ".$mesTiempo[1];
			}
		}	

		return json_encode($iibObjRsp);
	}


	public function getResultadosPromo(Request $req){
		include("../mes.php");
		/*
			cambiar la url, por mid.dtpmundo.com
			controlar si el estado es distinto a OK y no mostrar las pentañas como estoy y detalle
			poner el separador de miles
		*/
		$client = new Client();
		$iibRsp = $client->get('http://181.40.66.162:9292/ControlDTP/resources/estadisticas/promo?idVendedor='.$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario.'&detalle='.$req->input('dataTipo'));

		/*$iibRsp = $client->get('http://181.40.66.162:9292/ControlDTP/resources/estadisticas/promo?idVendedor=603&detalle='.$req->input('dataTipo'));*/
		$iibObjRsp = json_decode($iibRsp->getBody());

		if($req->input('dataTipo') =="N"){
			foreach($iibObjRsp->meses as $key=>$meses){
				$mesTiempo = explode("-",$meses->mes);
				if($mesTiempo[0] <= 9){
					$mes0 = "0".$mesTiempo[0];
				}else{
					$mes0 = $mesTiempo[0];
				}
				$iibObjRsp->meses[$key]->mes = $mes[$mes0]." - ".$mesTiempo[1];
			}
		}	

		return json_encode($iibObjRsp);
	}

/*************************************************************************************************
					MC
*************************************************************************************************/
	public function indexMc(){
		$listaUsuarios =[];

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			$usuarios = Usuario::with('agencia','sucursal','perfil')->get();
		}else{
			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==2){
				$usuarios = Usuario::with('agencia','sucursal','perfil')
									->where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)
									->get();	
			}else{
				if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==7){
					$usuarios = Usuario::with('agencia','sucursal','perfil')
										/*->where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)*/
										->get();	
				}						
			}						
		}
		return view('pages.mc.users.usuarios')->with('usuarios', $usuarios, FALSE);
	}	
	/*
	Página para agregar usuarios
	*/
	public function addMc(Request $req){
		$arrayPerfil = [];
		$arrayAgencia = [];
		$arraySucursal = [];

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			$perfiles = Perfil::all();
			$agencias = Agencias::all();
			$sucursales = Sucursal::all();			
		}else{
			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminOpAdmin')){
				$perfiles = Perfil::where('id_perfil', '=',Config::get('constants.vendedor'))
									->orWhere('id_perfil', '=',Config::get('constants.adminOpAdmin'))
									->get();
				$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
				$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
			}else{
				if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminAgencia')){
					$perfiles = Perfil::where('id_perfil', '=',Config::get('constants.adminOpAdmin'))
										->orWhere('id_perfil', '=',Config::get('constants.adminAgencia'))
										->get();
					$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
					$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
				}else{
					$perfiles = Perfil::where('id_perfil', '=',Config::get('constants.adminOpAdmin'))
										->orWhere('id_perfil', '=',Config::get('constants.adminAgencia'))
										->orWhere('id_perfil', '=',Config::get('constants.vendedor'))
										->get();
					$agencias = Agencias::all();
					$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
				}	

			}	
		}

		foreach ($perfiles as $key=>$perfil){
			$arrayPerfil[$key]['value']= $perfil->id_perfil;
			$arrayPerfil[$key]['label']= $perfil->des_perfil;
		}

		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}

		foreach ($sucursales as $key=>$sucursal){
			$arraySucursal[$key]['value']= $sucursal->id_sucursal_agencia;
			$arraySucursal[$key]['label']= $sucursal->descripcion_agencia;
		}

		//Inicio de Generación de Listado de agentes DTP
		$agents = DB::select("SELECT * FROM usuarios where id_perfil = 10 or id_perfil = 3 and id_agencia = ". Config::get('constants.agenciaDtp')." and activo='S'");

		$listaAgente = [];
		foreach($agents as $key=>$agent){
			$listaAgente[$key]['value']=$agent->id_usuario;
			$listaAgente[$key]['label']= $agent->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agent->usuario;
		}

		return view('pages.mc.users.usuariosAdd')->with(['listadoPerfil'=>$arrayPerfil, 'listadoAgencia'=>$arrayAgencia, 'listadoSucursal'=>$arraySucursal, 'listAgenteDtp'=>$listaAgente]);
	}
	/*
	Página para editar usuarios
	*/
	public function editMc(Request $req, $id){
		$usuario = Usuario::where('id_usuario', '=',$id)->get();
		$arrayPerfil = [];
		$arrayAgencia = [];
		$arraySucursal = [];

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			$perfiles = Perfil::all();
			$agencias = Agencias::all();
			$sucursales = Sucursal::all();			
		}else{
			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminOpAdmin')){
				$perfiles = Perfil::where('id_perfil', '=',Config::get('constants.adminOpAdmin'))
									->get();
				$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
				$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
			}else{
				if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminAgencia')){
					$perfiles = Perfil::where('id_perfil', '=',Config::get('constants.adminOpAdmin'))
										->orWhere('id_perfil', '=',Config::get('constants.adminAgencia'))
										->get();
					$agencias = Agencias::where('id_agencia', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();
					$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
				}else{
					$perfiles = Perfil::where('id_perfil', '=',Config::get('constants.adminOpAdmin'))
										->orWhere('id_perfil', '=',Config::get('constants.adminAgencia'))
										->orWhere('id_perfil', '=',Config::get('constants.vendedor'))
										->get();
					$agencias = Agencias::all();
					$sucursales = Sucursal::where('id_agencia', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia)->get();	
				}	

			}	
		}

		foreach ($perfiles as $key=>$perfil){
			$arrayPerfil[$key]['value']= $perfil->id_perfil;
			$arrayPerfil[$key]['label']= $perfil->des_perfil;
		}

		foreach ($agencias as $key=>$agencia){
			$arrayAgencia[$key]['value']= $agencia->id_agencia;
			$arrayAgencia[$key]['label']= $agencia->razon_social;
		}

		foreach ($sucursales as $key=>$sucursal){
			$arraySucursal[$key]['value']= $sucursal->id_sucursal_agencia;
			$arraySucursal[$key]['label']= $sucursal->descripcion_agencia;
		}

		//Inicio de Generación de Listado de agentes DTP
		$agents = DB::select("SELECT * FROM usuarios where id_perfil = 10 or id_perfil = 3 and id_agencia = ". Config::get('constants.agenciaDtp')." and activo='S'");

		$listaAgente = [];
		foreach($agents as $key=>$agent){
			$listaAgente[$key]['value']=$agent->id_usuario;
			$listaAgente[$key]['label']= $agent->nombre_apellido."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;".$agent->usuario;
		}

		return view('pages.mc.users.usuariosEdit')->with(['usuario'=>$usuario,'listadoPerfil'=>$arrayPerfil, 'listadoAgencia'=>$arrayAgencia, 'listadoSucursal'=>$arraySucursal, 'listAgenteDtp'=>$listaAgente]);
	}	
	/*
	Hacer Agregar
	Función para agregar los datos de usuario
	*/
	public function doAddMc(Request $request){
		$usuario = Usuario::where('email', '=',$request->input('email'))->get();
		if(count($usuario)){
			flash('Ya existe un usuario con ese email');
		}
		else{
			$usuario = new Usuario;
			
			/* Se comprueba si los datos solo con espacios en blanco*/
			if(!ctype_space($request->input('nombreApellido'))){
				$usuario->nombre_apellido = $request->input('nombreApellido');
			}else{
				flash('El nombre esta formado por espacios vacios')->error();
				return Redirect::back();
			}

			$id = Usuario::select('id_usuario')->max('id_usuario');
			$maxInd  = $id+1;
			$usuario->id_usuario = $maxInd;
			$usuario->password = $request->input('email');
			$usuario->email = $request->input('email');
			$usuario->usuario = $usuario->email;
			if($request->input('activo') == 'true') $usuario->activo = 'S';
			else $usuario->activo = 'N';
			$usuario->id_agencia = $request->input('agencia_id');
			$usuario->id_sucursal_agencia = $request->input('sucursal_id');
			$usuario->id_perfil = $request->input('perfil_id');
			$usuario->codigo_agente = $request->input('codigo_agente');
			$usuario->fecha_ingreso_promo = null;
			try{
				$usuario->save();
				//yaii parece que no modifica el fecha_promo por default en update! (pero hay que poner null en insert)
	         	DB::table('usuarios')
					            ->where('id_usuario',$maxInd)
					            ->update([
					            			'id_sistema_facturacion'=>$maxInd
					            			]);
				
				//actualizar en usuarios si es que se recibio respuesta positiva/????
				flash('Se ha ingresado el usuario exitosamente')->success();
			} catch(\Exception $e){
				flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
			}
		}

		//return "deluxe";
		return redirect()->route('mc.usuarios');
	}
	/*
	Hacer Editar
	Función para modificar los datos de usuario
	*/
	public function doEditMc(Request $request){

	 		$id_usuario = $request->input('idSucursalAgencia');

			if(!ctype_space($request->input('nombreApellido'))){
	 			$nombre = $request->input('nombreApellido');
	 		}else{
				flash('El nombre esta formado por espacios vacios')->error();
				return Redirect::back();
	 		}

	 		if($request->input('activo') == 'true'){
	 			$activo = 'S';
	 		}else{
	 			$activo = 'N';
	 		}
	 		if($request->input('password') != ""){
				$password = md5(Config::get('constants.semilla_left') ."".$request->input('password')."".Config::get('constants.semilla_right'));
	 		}else{
				$usuario = Usuario::where('id_usuario', '=',$id_usuario)->get();
				$password  = $usuario[0]['password'];
	 		}
	 		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil ==Config::get('constants.adminDtp')){
			    DB::table('usuarios')
							    ->where('id_usuario',$request->input('idSucursalAgencia'))
							    ->update([
							            'nombre_apellido'=>$nombre,
							            'password'=>$password,
							            'activo'=>$activo,
							            'id_agencia' => $request->input('agencia_id'),
							            'id_perfil' => $request->input('perfil_id'),
							            'id_sucursal_agencia'=>$request->input('sucursal_id'),
							            'codigo_agente' => $request->input('codigo_agente')
							            ]);

	 		}else{
		   		DB::table('usuarios')
						    ->where('id_usuario',$request->input('idSucursalAgencia'))
						    ->update([
						            'nombre_apellido'=>$nombre,
						            'password'=>$password,
						            'activo'=>$activo,
						            'id_agencia' => $request->input('agencia_id'),
						            'id_sucursal_agencia'=>$request->input('sucursal_id'),
						            'codigo_agente' => $request->input('codigo_agente')
						            ]);
	 		}
			flash('Se ha editado el usuario exitosamente')->success();
			return redirect()->route('mc.usuarios');
	}

    public function doConsultaFacturaMc(Request $request){
    	$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json' ]
		]);

		$cuerposFactura =[];
		$restDetallesFactIn = new \StdClass;
		$restDetallesFactIn->reqToken = "ertetrytrey";
		$restDetallesFactIn->ts= date('Y-m-d h:m:s');
		$detalles = new \StdClass;
		$detalles->FechaDesde = "0000-00-00";
		$detalles->FechaHasta = "0000-00-00";
		if($request->input('vendedor_id') == 0){
			$detalles->AgenciaId = Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia;
			$detalles->VendedorId = 0;
		}else{
			$detalles->VendedorId = $request->input('vendedor_id');
			$usuario = Usuario::where('id_usuario', '=',$request->input('vendedor_id'))->get();
			$detalles->AgenciaId = $usuario[0]['id_agencia'];
		}

		$restDetallesFactIn->Detalles = $detalles;
		$iibReq = new \StdClass;
		$iibReq->RestDetallesFactIn = $restDetallesFactIn;
		try{
			$iibRsp = $client->post('http://factour.dtpmundo.com/dtp/rest/FacturasPendientes',
				['body' => json_encode($iibReq)]
			);
		}
		catch(RequestException $e){
			return view('pages.mc.timeErrorConexion');
		}
		catch(ClientException $e){
			return view('pages.mc.errorConexion');	
		}
		$iibObjRsp = json_decode($iibRsp->getBody());

		/*echo '<pre>';
		print_r(json_encode($iibObjRsp));
		die;*/
		$totalUsaVencer = 0;
		$totalGsaVencer = 0;
		$totalUsVencer = 0;
		$totalGsVencer = 0;

		if(isset($iibObjRsp->RestDetallesFactOut->DatosFactura)){
			foreach($iibObjRsp->RestDetallesFactOut->DatosFactura as $key=>$datosFactura){
				$fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
				$fecha_entrada = strtotime($datosFactura->FacturasVencimientoFecha." 23:59:59");
				if($fecha_actual > $fecha_entrada&& $datosFactura->FacturasSaldoT != 0){
					if($datosFactura->FacturaMonedaNombre == 'Guaranies'){
						$totalGsVencer = $totalGsVencer + floatval($datosFactura->FacturasSaldoT);
					}else{
						$totalUsVencer = $totalUsVencer + floatval($datosFactura->FacturasSaldoT);
					}
					if($request->input('estado') == 1 || $request->input('estado') == 3){ 
						$fecha = $FechaDesde = explode("-", $datosFactura->FacturasFecha);
						$vencimiento = explode("-", $datosFactura->FacturasVencimientoFecha);
						$cuerposFactura['resultado'][$key]['factura_id'] = $datosFactura->FacturasId;
						$cuerposFactura['resultado'][$key]['factura_fecha'] = $fecha[2]."/".$fecha[1]."/".$fecha[0];
						$cuerposFactura['resultado'][$key]['factura_numero'] = "<b>".$datosFactura->FacturasNumero."</b>";
						$cuerposFactura['resultado'][$key]['cliente_id'] = $datosFactura->ClienteId;
						$cuerposFactura['resultado'][$key]['ruc_cliente'] = $datosFactura->RucCliente;
						$cuerposFactura['resultado'][$key]['nombre_cliente'] = $datosFactura->FacturasNombrePasajero;
						$cuerposFactura['resultado'][$key]['factura_moneda'] = $datosFactura->FacturaMonedaNombre;
						$cuerposFactura['resultado'][$key]['factura_destinacion'] = $datosFactura->FacturasDestinationName;
						$cuerposFactura['resultado'][$key]['factura_total'] = number_format($datosFactura->FacturasTotalDetalles, 0, '', '.');
						$cuerposFactura['resultado'][$key]['factura_vendedor'] = $datosFactura->FacturasVendedor;
						$cuerposFactura['resultado'][$key]['factura_vendedor_id'] = $datosFactura->FacturasVendedorId;
						$cuerposFactura['resultado'][$key]['fecha_vencimiento'] = '<b style="color: red;">'.$vencimiento[2]."/".$vencimiento[1]."/".$vencimiento[0].'</b>';
						$cuerposFactura['resultado'][$key]['estado'] = '<b style="color: red;">Vencido</b>';
						$cuerposFactura['resultado'][$key]['saldo'] = "<b>".number_format($datosFactura->FacturasSaldoT, 0, '', '.')."</b>";
						$cuerposFactura['resultado'][$key]['url_factura'] = $datosFactura->UrlFactura;
						$cuerposFactura['resultado'][$key]['url_voucher'] = $datosFactura->UrlVoucher;
					}	
				}else{
					if($datosFactura->FacturasSaldoT != 0){
						if($datosFactura->FacturaMonedaNombre == 'Guaranies'){
							$totalGsaVencer = $totalGsaVencer + floatval($datosFactura->FacturasSaldoT);
						}else{
							$totalUsaVencer = $totalUsaVencer + floatval($datosFactura->FacturasSaldoT);
						}
						if($request->input('estado') == 2 || $request->input('estado') == 3){
							$fecha = $FechaDesde = explode("-", $datosFactura->FacturasFecha);
							$vencimiento = explode("-", $datosFactura->FacturasVencimientoFecha);
							$cuerposFactura['resultado'][$key]['factura_id'] = $datosFactura->FacturasId;
							$cuerposFactura['resultado'][$key]['factura_fecha'] = $fecha[2]."/".$fecha[1]."/".$fecha[0];
							$cuerposFactura['resultado'][$key]['factura_numero'] = "<b>".$datosFactura->FacturasNumero."</b>";
							$cuerposFactura['resultado'][$key]['cliente_id'] = $datosFactura->ClienteId;
							$cuerposFactura['resultado'][$key]['ruc_cliente'] = $datosFactura->RucCliente;
							$cuerposFactura['resultado'][$key]['nombre_cliente'] = $datosFactura->FacturasNombrePasajero;
							$cuerposFactura['resultado'][$key]['factura_moneda'] = $datosFactura->FacturaMonedaNombre;
							$cuerposFactura['resultado'][$key]['factura_destinacion'] = $datosFactura->FacturasDestinationName;
							$cuerposFactura['resultado'][$key]['factura_total'] = number_format($datosFactura->FacturasTotalDetalles, 0, '', '.');
							$cuerposFactura['resultado'][$key]['factura_vendedor'] = $datosFactura->FacturasVendedor;
							$cuerposFactura['resultado'][$key]['factura_vendedor_id'] = $datosFactura->FacturasVendedorId;
							$cuerposFactura['resultado'][$key]['fecha_vencimiento'] = '<b style="color: green;">'.$vencimiento[2]."/".$vencimiento[1]."/".$vencimiento[0].'</b>';
							$cuerposFactura['resultado'][$key]['estado'] = '<b style="color: green;">A Vencer</b>';
							$cuerposFactura['resultado'][$key]['saldo'] = "<b>".number_format($datosFactura->FacturasSaldoT, 0, '', '.')."</b>";
							$cuerposFactura['resultado'][$key]['url_factura'] = $datosFactura->UrlFactura;
							$cuerposFactura['resultado'][$key]['url_voucher'] = $datosFactura->UrlVoucher;
						}
					}	
				}	
			}	
		}

		$totalGs = $totalGsVencer + $totalGsaVencer;
		$totalUs = $totalUsVencer + $totalUsaVencer;
		if($totalGsVencer!= 0){
			$porcentajeGsVencer = ($totalGsVencer * 100)/$totalGs;
		}else{
			$porcentajeGsVencer = 0;
		}
		if($totalGsaVencer!= 0){
			$porcentajeGsaVencer = ($totalGsaVencer * 100)/$totalGs;
		}else{
			$porcentajeGsaVencer = 0;
		}
		if($totalUsVencer!= 0){
			$porcentajeUsVencer = ($totalUsVencer * 100)/$totalUs;
		}else{
			$porcentajeUsVencer = 0;
		}
		if($totalUsaVencer!= 0){
			$porcentajeUsaVencer = ($totalUsaVencer * 100)/$totalUs;
		}else{
			$porcentajeUsaVencer = 0;
		}

		if($porcentajeGsVencer != 0 && $porcentajeUsVencer != 0){
			$cuerposFactura['cabecera']['porcentajeVencido'] = ($porcentajeGsVencer + $porcentajeUsVencer)/2;
		}else{
			$cuerposFactura['cabecera']['porcentajeVencido'] = $porcentajeGsVencer + $porcentajeUsVencer;
		} 
		if($porcentajeGsaVencer != 0 && $porcentajeUsaVencer != 0){
			$cuerposFactura['cabecera']['porcentajeAVencer'] = ($porcentajeGsaVencer + $porcentajeUsaVencer)/2;
		}else{
			$cuerposFactura['cabecera']['porcentajeAVencer'] = $porcentajeGsaVencer + $porcentajeUsaVencer;
		}
		$cuerposFactura['cabecera']['totalGsVencer'] = $totalGsVencer;
		$cuerposFactura['cabecera']['totalUsVencer'] = $totalUsVencer;
		$cuerposFactura['cabecera']['totalGsaVencer'] = $totalGsaVencer;
		$cuerposFactura['cabecera']['totalUsaVencer'] = $totalUsaVencer;
		$facturas =  json_decode(json_encode($cuerposFactura), FALSE);
		return response()->json($facturas);
    }
    
	public function getUsuariosAgenciaMc(Request $req){
    	if(isset($req->dataAgencia)){
    		$usuarios = Usuario::where('id_agencia', '=',$req->dataAgencia)
    							->get();
    	}else{
			$usuarios = Usuario::all();
    	}
		$arrayUsuario = [];
		foreach ($usuarios as $key=>$usuario){
			$arrayUsuario[$key]['value']= $usuario->id_usuario;
			$arrayUsuario[$key]['label']= $usuario->usuario." - ".$usuario->nombre_apellido;
		}
		return json_encode($arrayUsuario);
	}

	public function comoPromocion(Request $req){
		return view('pages.comopromocion');	
	}	

	public function detalleCobro(Request $req, $id){
		$client = new Client();
		$iibRsp = $client->get('http://181.40.66.162:9292/ControlDTP/resources/estadisticas/promo?idVendedor='.$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario.'&detalle=C');
		$iibObjRsp = json_decode($iibRsp->getBody());
		$solicitados = DtpPlus::where('user_id', '=',$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario)
								->get();
		$facturas  = "";
		foreach($solicitados as $key=>$dtplus){
			if($key == "0"){
				$facturas .= $dtplus['facturas'];
			}else{
				$facturas .= ",".$dtplus['facturas'];
			}
		}

		$facturasNumeros = explode(",", $facturas);
		foreach($iibObjRsp->detallePromo as $key=>$detallePromo){
			foreach($facturasNumeros as $voucher){
				if($detallePromo->numeroFactura == $voucher){
					unset($iibObjRsp->detallePromo[$key]);	

				}
			}
		}	
		$arrayFacturas = [];
		$objResultado = new \StdClass; 
		$contadorPendiente = 0;
		$contadorMonto  =  0;
		$contador =  0;
		$facturas = [];
		$facturaString = "";
		$facturaIdString = "";
		foreach($iibObjRsp->detallePromo as $key=>$detallePromo){
			if($detallePromo->estadoFactura == 'C'){
				$contadorMonto = $contadorMonto + floatval($detallePromo->incentivo);
				$arrayFacturas[] = $detallePromo->idFactura;
				$facturas[] = $detallePromo->numeroFactura;
				$contadorPendiente++;
				if($contador == 0){
					$facturaString .=$detallePromo->numeroFactura;
					$facturaIdString .= $detallePromo->idFactura;
				}else{
					$facturaString .=",".$detallePromo->numeroFactura;
					$facturaIdString .=",".$detallePromo->idFactura;
				}
				$contador++;
			}
		}
		if($contador != 0){
			$ssl   = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on';
			$proto = strtolower($_SERVER['SERVER_PROTOCOL']);
			$proto = substr($proto, 0, strpos($proto, '/'));
			$usuario = Usuario::with('agencia')
								->where('id_usuario', '=',$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario)
								->get();
			$imagen = '../images/promociones/'.$id.'.png';
			$objResultado->usuario_id = $usuario[0]['id_usuario'];
			$objResultado->nombre_apellido = $usuario[0]['nombre_apellido'];
			$objResultado->agencia = $usuario[0]['agencia']['razon_social'];
			$objResultado->id_facturas = $arrayFacturas;
			$objResultado->facturas = $facturas;
			$objResultado->cantidad = $contadorPendiente;
			$objResultado->monto = floatval($contadorMonto);
			$objResultado->imagen = $imagen;
			$objResultado->imagenNombre = $id; 
			$dtpPlus = new DtpPlus;	
			$dtpPlus->codigo = $this->getId4Log();
			$dtpPlus->user_id = $usuario[0]['id_usuario'];
			$dtpPlus->fecha_pedido = date('Y-m-d h:m:s');
			$dtpPlus->opcion_cambio = $id;
			$dtpPlus->facturas = $facturaString;
			$dtpPlus->id_facturas = $facturaIdString;
			$dtpPlus->monto = floatval($contadorMonto);
			$dtpPlus->tipo = 2;
			$dtpPlus->save();
			$indice = $dtpPlus->id;
			$objResultado->idVoucher = $indice; 
		    Session::put('objResultado',$objResultado);
		}else{
			$objResultado = Session::get('objResultado');
			$imagen = Session::get('objResultado')->imagen;
			$indice = Session::get('objResultado')->idVoucher;
		}   
		return view('pages.detallepromocion')->with(['resultado'=>$objResultado, 'imagen'=>$imagen, 'indice'=>$indice]);
	}	

	public function getResultadosparaPromo(Request $req){
		include("../mes.php");
		/*
			cambiar la url, por mid.dtpmundo.com
			controlar si el estado es distinto a OK y no mostrar las pentañas como estoy y detalle
			poner el separador de miles
		*/
		$client = new Client();
		//$iibRsp = $client->get('http://mid.dtpmundo.com:9292/ControlDTP/resources/estadisticas/promo?idVendedor='.$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario.'&detalle='.$req->input('dataTipo'));

		$iibRsp = $client->get('http://181.40.66.162:9292/ControlDTP/resources/estadisticas/promo?idVendedor='.$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario.'&detalle='.$req->input('dataTipo'));
		$iibObjRsp = json_decode($iibRsp->getBody());
			$solicitados = DtpPlus::where('user_id', '=',$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario)
									->get();
			$facturas  = "";
			foreach($solicitados as $key=>$dtplus){
				if($key == "0"){
					$facturas .= $dtplus['id_facturas'];
				}else{
					$facturas .= ",".$dtplus['id_facturas'];
				}
			}

			$facturasNumeros = explode(",", $facturas);
			foreach($iibObjRsp->detallePromo as $key=>$detallePromo){
				foreach($facturasNumeros as $voucher){
					if($detallePromo->idFactura == $voucher){
						unset($iibObjRsp->detallePromo[$key]);	

					}
				}
			}	

			$resultado = [];
			$arrayResultado = new \StdClass; 
			$contadorTotal = 0;
			$contadorPendiente = 0;
			$contadorCobrado  =  0;
			$resultadoCobrado = [];
			$resultadoPendiente = [];
			foreach($iibObjRsp->detallePromo as $key=>$detallePromo){
				$contadorTotal++;
				if($detallePromo->estadoFactura == 'P'){
					$resultadoPendiente[] = $detallePromo;
					$contadorPendiente++;
				}else{
					$resultadoCobrado[] = $detallePromo;
					$contadorCobrado++;
				}
			}
			$arrayResultado->estado = $iibObjRsp->estado;
			$arrayResultado->total = $contadorTotal;
			$arrayResultado->cobrado = $contadorCobrado;
			$arrayResultado->pendiente = $contadorPendiente;
			$arrayResultado->resultadoPendiente = $resultadoPendiente;
			$arrayResultado->resultadoCobrado = $resultadoCobrado;
			return json_encode($arrayResultado);
	}

	public function reportePromocion(Request $req){
		$solicitados = DB::select("select * from dtp_plus, usuarios, agencias where dtp_plus.user_id = usuarios.id_usuario and usuarios.id_agencia = agencias.id_agencia");					
		return view('pages.mc.reportes.reportesPromo')->with(['promos'=>$solicitados]);

	}

	public function getDetalleLista(Request $req){
   		$facturas = DtpPlus::where('id', '=',$req->id)->get();
   		return json_encode($facturas[0]['facturas']);	
	}


	public function getDetalleUsuario(Request $req){
		$solicitados = DB::select("select * from dtp_plus, usuarios, agencias where dtp_plus.user_id = usuarios.id_usuario and usuarios.id_agencia = agencias.id_agencia and dtp_plus.user_id=".$req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario);	
		return json_encode($solicitados);	
	}

	public function getPagarPromo(Request $req){
   		$facturas = DtpPlus::where('id', '=',$req->idDtplus)->get();
		if($facturas[0]['estado_pedido'] == 'P'){
		    DB::table('dtp_plus')
						->where('id',$req->idDtplus)
						->update([
						        'estado_pedido'=>'C'
						        ]);
			$estado = 'true';			
		}else{
	    	DB::table('dtp_plus')
						->where('id',$req->idDtplus)
						->update([
						        'estado_pedido'=>'P'
						        ]);
			$estado = 'false';			

		}
		return $estado;
	}
}	

