<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Session;
use Redirect;
use App\Proforma;
use App\ProformasDetalle;
use App\Divisas;
use App\Persona;
use App\AdjuntoDocumento;
use App\HistoricoComentariosProforma;
use App\Voucher;
use App\TipoFactura;
use App\EstadoFactour;
use App\Factura;
use App\FacturaDetalle;
use App\Empresa;
use App\NotaCredito;
use App\NotaCreditoDetalle;
use App\Producto;
use Response;
use Image;
use DB;

class NotaCreditoController extends Controller {


//====================PARAMETROS DE ESTADO FACTURA======================
    private $facturaFacturada = 29;
    private $facturaAnulada =  30;
    private $facturaVerificado = 2;
//======================================================================
//
//====================PARAMETROS DE ESTADO NOTA CREDITO=================

//======================================================================

private function getIdUsuario(){
	
	return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
}

private function getIdEmpresa(){

 return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
}


public function notaCreditoImprimir($idNotaCredito){

  //generar_nota_credito(id_factura, id_usuario)

        //===================DATOS DE LOGUEO=======================
            $id_usuario = $this->getIdUsuario();
            $id_empresa = $this->getIdEmpresa();
        //=========================================================

         
     $empresa = Empresa::where('id',$id_empresa)->first();       
     $notaCredito = NotaCredito::with('cliente',
                      'proforma',
                      'currency',
                      'timbrado',
                      'factura',
                      'factura.vendedorEmpresa',
                      'notaCreditoDetalle',
                      'libroVentaSaldo0')
     ->where('id',$idNotaCredito)->get(); 

     if(!empty($notaCredito[0])){

     $totalFactura =  $notaCredito[0]->total_neto_nc;
     $NumeroALetras = $this->convertir($totalFactura);
     $notaCreditoDetalle = $notaCredito[0]->notaCreditoDetalle;


     $logoSucursal = DB::select('select s.logo from personas p, personas s where p.id_sucursal_empresa = s.id AND p.id ='.$notaCredito[0]->id_usuario);

     $sucursalEmpresa =  Persona::where('id_empresa',$id_empresa)
                                     ->where('id_tipo_persona', 21)
                                     ->where('activo','true')
                                     ->get();

      if($logoSucursal[0]->logo != "" && $logoSucursal[0]->logo != 'factour.png'){
        $baseLogo = $logoSucursal[0]->logo;
        $logo = asset("personasLogo/$baseLogo");
      }else{
        $logo = asset("logoEmpresa/$empresa->logo");
      }

     $pdf = \PDF::loadView('pages.mc.notaCredito.notaCredito',
              compact('notaCredito','notaCreditoDetalle','empresa','totalFactura','NumeroALetras', 'sucursalEmpresa','logo'));

             $pdf->setPaper('a4', 'letter')->setWarnings(false);
               return $pdf->download('N_C_'.$notaCredito[0]->nro_nota_credito.'.pdf');
             } else {
              abort('404');
             }




             // return view('pages.mc.notaCredito.notaCredito')->with([
             //                              'notaCredito'=>$notaCredito,
             // 														  'notaCreditoDetalle'=>$notaCreditoDetalle,
             // 														  'empresa'=>$empresa,
             // 														  'totalFactura'=>$totalFactura,
             // 														  'NumeroALetras'=>$NumeroALetras]);
  }
  

/**
     * Recibe una fecha 2017-05-02 y pasa a 02/05/2017 
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaSalida($date){
        if( $date != ''){

        $date = explode('-', $date);
            $fecha = $date[2]."/".$date[1]."/".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }


    /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }




      private function getId4Log(){
    //Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
      list($MiliSegundos, $Segundos) = explode(" ", microtime());
      $id=substr(strval($MiliSegundos),2,4);
      return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
  }




    private static $UNIDADES = [
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
    ];
    private static $DECENAS = [
        'VENTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
    ];
    private static $CENTENAS = [
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
    ];
    public static function convertir($number, $moneda = '', $centimos = '', $forzarCentimos = false)
    {
        $converted = '';
        $decimales = '';
        if (($number < 0) || ($number > 999999999)) {
            return 'No es posible convertir el numero a letras';
        }
        $div_decimales = explode('.',$number);
        if(count($div_decimales) > 1){
            $number = $div_decimales[0];
            $decNumberStr = (string) $div_decimales[1];
            if(strlen($decNumberStr) == 2){
                $decNumberStrFill = str_pad($decNumberStr, 9, '0', STR_PAD_LEFT);
                $decCientos = substr($decNumberStrFill, 6);
                $decimales = self::convertGroup($decCientos);
            }
        }
        else if (count($div_decimales) == 1 && $forzarCentimos){
            $decimales = 'CERO ';
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', self::convertGroup($millones));
            }
        }
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', self::convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', self::convertGroup($cientos));
            }
        }
        if(empty($decimales)){
            $valor_convertido = $converted . strtoupper($moneda);
        } else {
            $valor_convertido = $converted . strtoupper($moneda) . ' CON ' . $decimales . ' ' . strtoupper($centimos);
        }
        return $valor_convertido;
    }
    private static function convertGroup($n)
    {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = self::$CENTENAS[$n[0] - 1];
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= self::$UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }




}