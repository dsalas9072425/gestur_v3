<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Session;
use Redirect;
use Response;
use Image;
use Mail;
use DB;
use App\Grupos;

class NotificacionController extends Controller {



	public function getNotificacionTicket(){
		$id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$ticket_pendientes = DB::select("SELECT * FROM get_notificaciones(".$id_usuario.",'M')");
		$ticket_pendientes = $ticket_pendientes[0]->get_notificaciones;
		if($ticket_pendientes != '0'){ 

		return response()->json(['count'=>$ticket_pendientes]);
		} else {
		return response()->json(['count'=>0]);
		}


	}//FUNCTION



	public function getNotificacionProceso(){
		$id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$ticket_pendientes = DB::select("SELECT * FROM get_notificaciones(".$id_usuario.",'P')");
		$ticket_pendientes = $ticket_pendientes[0]->get_notificaciones;
		if($ticket_pendientes != '0'){ 
			$mensaje = explode('|',$ticket_pendientes);

		return response()->json(['count'=>$mensaje]);
		} else {
		return response()->json(['count'=>0]);
		}


	}//FUNCTION

    public function index(Request $request){
        /*echo '<pre>';
        print_r($request->all());
        die;*/
		$correos = DB::table('vw_notificacion_pendiente');
		$correos = $correos->where('estado','S');
		$correos = $correos->get();
		
        foreach($correos as $key=>$correo){
			/*echo '<pre>';
       		print_r($correo);*/
            $subject = $correo->asunto;
            $para = /*$correo->para*/'sergiopereira947@gmail.com';
            $de = $correo->de;
            $data = [
                'datos' => $correo->contenido,
                'titulo' => $correo->titulo,
                'logo' => $correo->logo
                ];
            $plantilla = $correo->plantilla;
			$pdf = "";
           /* if($correo->adjunto_documento!== null){ 
                $facturas = DB::table('vw_facturas');
                $facturas = $facturas->where('tid',$correo->adjunto_documento);
                $facturas = $facturas->get();
                $facturasDetalles = DB::table('vw_detalle_facturas');
                $facturasDetalles = $facturasDetalles->where('id_venta',$facturas[0]->tid);
                $facturasDetalles = $facturasDetalles->get();
                $entidad = DB::select('select * from entidad where entidad.tid = '.$facturas[0]->entidad);
                $total = $facturas[0]->total_venta;
                $NumeroALetras = $this->convertir($total);
                $timbrados = DB::select('select * from timbrados where tid = '.$facturas[0]->id_timbrado);
                $autorimpresor = $timbrados[0]->auto_impresor;
                $autorizacionAutoImpresor = $timbrados[0]->autorizacion_auto_impresor;
                $plantilla = 'plantillaCorreo';
                $view = view('pages.plantillas.'.$plantilla)->with(['facturas'=>$facturas, 'facturasDetalles'=>$facturasDetalles, 'autorimpresor'=>$autorimpresor,'entidad'=>$entidad,'NumeroALetras'=>$NumeroALetras,'timbrados'=>$timbrados,'autorizacionAutoImpresor'=>$autorizacionAutoImpresor])->render();
                $pdf = PDF::loadHTML($view);
                $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);
                $pdf->setPaper('letter')->setWarnings(false);
                $subject = $subject.' '.$entidad[0]->descripcion; 
            }else{ 
                $pdf = "";
            } */   
            //$para = 'edgargrunce@gmail.com';
           // try {    
                Mail::send('mail.email', $data, function($msj) use($subject,$de,$para,$pdf){
                    $msj->from($de,"DTP-GESTUR");
                    $msj->subject($subject);
                    $msj->bcc('notificaciones@dtp.com.py');
                    $msj->replyTo($de, 'Notificaciones Dtp');
                    $msj->to($para);
                    if($pdf !=""){
                        $msj->attachData($pdf->output(), $subject.".pdf");
                    }    
                });
                DB::table('notificaciones')
                ->where('id',$correo->id)
                ->update([
                        'enviado'=> 'S',
                        'fecha_hora_envio'=> date('Y-m-d H:m:s')
                        ]);
          /*  }
            catch (Exception $e) {
                        echo "Message: " . $e->getMessage();
                        echo "";
                        echo "getCode(): " . $e->getCode();
                        echo "";
                        echo "__toString(): " . $e->__toString();
            } */
                    
        }
       // if($request->input('notificacion')!== null){
          //  return Redirect::back();
       // }
       
    }    






}//CLASS
