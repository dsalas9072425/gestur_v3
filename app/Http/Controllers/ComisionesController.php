<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use \App\DestinoDtpmundo;
use App\Currency;
use App\Aerolinea;
use App\EstadoFactour;
use App\Grupo;
use App\Producto;
use App\Persona;
use App\TipoTicket;
use App\Ticket;
use App\Comision;
use App\ComisionDetalle;
use Carbon\Carbon;
use DB;
use Response;
use Image; 
use Session;

class ComisionesController extends Controller
{
    private function getIdEmpresa(){

        return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
      
      }//function
      
    public function index(Request $request)
    {
        $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'addLiq'");

	 	if(empty($permisos)){
	    	flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		} 

        $divisas = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
        $vendedores = Persona::whereIn('id_tipo_persona',[3,4])
                            ->where('activo',  true)
                            ->where('id_empresa',  Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
                            ->orderBy('nombre','asc')->get();


        return view('pages.mc.comisiones.index',compact('divisas','vendedores'));
    }

    public function add(Request $request)
    {
        $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$permisos = DB::select("SELECT pe.* 
							FROM persona_permiso_especiales p, permisos_especiales pe
							WHERE p.id_permiso = pe.id 
							AND p.id_persona = ".$idUsuario." 
							AND pe.url = 'addLiq'");

	 	if(empty($permisos)){
	    	flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		} 

        $divisas = Currency::where('activo', 'S')->orderBy('currency_id', 'DESC')->get();
        $vendedores = DB::select(" SELECT p.id,p.nombre, p.apellido, p.documento_identidad, tp.denominacion, p.activo, p.email
                                    FROM personas p
                                    JOIN tipo_persona tp on tp.id = p.id_tipo_persona
                                    WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE denominacion like '%_EMPRESA')
                                    AND p.activo = true
                                    AND p.id_empresa =".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

        return view('pages.mc.comisiones.add',compact('divisas','vendedores'));
    }

    public function doAdd(Request $request)
    {
        $respuesta = new \StdClass;
         try{
            DB::table('escala_comision_cabecera')
                    ->where('id_vendedor',$request->input('id_vendedor'))
                    ->update([
                                'activo'=>false
                                ]);
            $comision = new Comision;
            $comision->id_vendedor = $request->input('id_vendedor');
            $comision->fecha = date('Y-m-d');
            $comision->activo = true;
            $comision->tipo = 'R';
            $comision->id_moneda = $request->input('id_moneda');
            $comision->facturacion =str_replace(',','.', str_replace('.','', $request->input('facturacion')));
            $comision->porcentaje_extra = str_replace(',','.', str_replace('.','', $request->input('porcentaje_extra')));
            $comision->save();
            $comisionId =  $comision->id;
            foreach($request->input('detalles') as $key=>$detalle){
                $comisionDetalle = new ComisionDetalle;
                $comisionDetalle->id_cabecera = $comisionId;
                $comisionDetalle->desde = str_replace(',','.', str_replace('.','',$detalle['desde']));
                $comisionDetalle->hasta = str_replace(',','.', str_replace('.','',$detalle['hasta']));
                $comisionDetalle->porcentaje = str_replace(',','.', str_replace('.','',$detalle['porcentaje']));
                $comisionDetalle->save();
            }
            $respuesta->status = 'OK';
            $respuesta->mensaje = 'Se han guardado las comisiones.';
        } catch(\Exception $e){
            $respuesta->status = 'ERROR';        
            $respuesta->mensaje = 'Ocurrió un error al intentar generar la comisión.';
        } 
        return response()->json($respuesta);
    }   


    public function getComision(Request $req)
    {
		$id_empresa = $this->getIdEmpresa();

		$comision = DB::table('vw_comision_cabecera');
		$comision = $comision->where('id_empresa',$id_empresa);
		if($req->input('id_vendedor')){
			$comision = $comision->where('id_vendedor',$req->input('id_vendedor'));
		}
		if($req->input('id_moneda')){
			$comision = $comision->where('id_moneda',$req->input('id_moneda'));
		}
		if($req->input('tipo')){
			$comision = $comision->where('tipo',$req->input('tipo'));
		}
        if($req->input('activo')){
			$comision = $comision->where('activo',$req->input('activo'));
		}
		$comision = $comision->get();
		return response()->json(['data'=>$comision]);
    }

    public function getComisionDetalle(Request $req)
    {
        $comisionDetalle = ComisionDetalle::where('id_cabecera',$req->input('id_cabecera'))->get();
		return response()->json(['data'=>$comisionDetalle]);
    }
    
    
    public function delete(Request $req)
    {
        $respuesta = new \StdClass;
        try{
                DB::table('escala_comision_cabecera')
                    ->where('id',$req->input('id_comision'))
                    ->update([
                                'activo'=>false
                                ]);
                $respuesta->status = 'OK';
                $respuesta->msj = 'Se han anulado la escala de comisiones.';
        } catch(\Exception $e){
                $respuesta->status = 'ERROR';        
                $respuesta->msj = 'Ocurrió un error al intentar anular la comisión.';
        } 
        return response()->json($respuesta);
    }

    public function activar(Request $req)
    {
        $respuesta = new \StdClass;
        try{
                DB::table('escala_comision_cabecera')
                    ->where('id',$req->input('id_comision'))
                    ->update([
                                'activo'=>true
                                ]);
                $respuesta->status = 'OK';
                $respuesta->msj = 'Se han activado la escala de comisiones.';
        } catch(\Exception $e){
                $respuesta->status = 'ERROR';        
                $respuesta->msj = 'Ocurrió un error al intentar activado la comisión.';
        } 
        return response()->json($respuesta);
    }

    public function agregar(Request $req)
    {
        if($this->getIdEmpresa() == 1){
            $producto = 1;
          }
          if($this->getIdEmpresa() == 2){
            $producto = 8167;
          }
          if($this->getIdEmpresa() == 5){
            $producto = 14869;
          }
    
          if($this->getIdEmpresa() == 9){
           $producto = 29754;
           //$producto = 29573; 
          }
    
          if($this->getIdEmpresa() == 17){
            $producto = 37169;
          }
    
          if($this->getIdEmpresa() == 21){
            $producto = 38368;
          }
    
        $aerolineas1 = DB::select('select id as out_id_persona, nombre as out_nombre from personas where id_empresa = '.$this->getIdEmpresa().' and id in (select distinct(id_proveedor) from tickets where id_empresa = '.$this->getIdEmpresa().')');  
        $aerolineas2 = DB::select('select * from get_producto_persona(?,?)', [$producto,$this->getIdEmpresa()]);
        $aerolineas = array_merge($aerolineas1, $aerolineas2);
        return view('pages.mc.comisiones.agregar',compact('aerolineas'));
    }

    public function getDatos(Request $req)
    {
        $comision = Persona::where('id',$req->input('dataFile'))
                            ->where('id_empresa',  Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
                            ->orderBy('nombre','asc')->first(['comision_pactada']);

        $respuesta = null;
        if($comision){
            if($comision->comision_pactada != null){
                $respuesta = (float)$comision->comision_pactada;
            }
        }
        return response()->json(['comision' => $respuesta]);
    }


    public function guardarDatos(Request $req)
    {
            $respuesta = new \StdClass;
            try{
                DB::table('personas')->where('id',$req->input('dataAerolinea'))
                                    ->update(['comision_pactada'=>str_replace(',','.', str_replace('.','',$req->input('dataComision')))]);	
                $respuesta->status = 'OK';
                $respuesta->msj = 'Se ha editado la comision.';
             } catch(\Exception $e){
                $respuesta->status = 'ERROR';        
                $respuesta->msj = 'Ocurrió un error al intentar guardar la comisión.';
            }  
            return response()->json($respuesta);
                
       }





}