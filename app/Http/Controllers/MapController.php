<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Session;

class MapController extends Controller
{
	/*
	Página de los mapas de la ubicacion de los hoteles
	*/
	public function index(Request $req){
		//dd($req->all());
		//$hotelsArray = $req->session()->get('searchInfo');
		//return view('pages.hoteles.mapa')->with('hotels',$req->session()->get('searchInfo')->hotels)->with('hotelIndex',$req->input('hotelIndex'));
		//dd($req->session()->get('searchInfo')->hotels);
		//dd($req->input('codHotel'));
		//dd($this->waitUntilRead($req->input('sid')));
		return view('pages.hoteles.mapa')->with('hotels',$this->waitUntilRead($req->input('sid'))->hotels)->with('codHotel',$req->input('codHotel'))->with('codProveedor',$req->input('codProveedor'));
	}
	
	private function waitUntilRead($sid){
		//$fileName='searchids/' . Session::get('sid') . 'lock.txt';
		$fileName = 'searchids/' . $sid . '/lock.txt';
		if(!file_exists($fileName)){
			
			return null;
			
		}
		$fp = fopen($fileName, "r");
		
		while(!flock($fp, LOCK_EX)) {  // acquire an exclusive lock
			
		}
		//return json_decode(fread($fp));
		$oldSearchInfo;
		//var_dump(filesize($fileName));
		/*
		if(filesize($fileName) <= 0) $oldSearchInfo = "{}";
		else{
			//$oldSearchInfo = fread($fp,filesize($fileName));
			//$oldSearchInfo = fgets($fp);
			//var_dump($oldSearchInfo);
			//dd($oldSearchInfo);
		}
		*/
		$oldSearchInfo = fgets($fp);
		if($oldSearchInfo == '') $oldSearchInfo = '{}';
		flock($fp, LOCK_UN);    // release the lock
		fclose($fp);
		//dd(json_decode($oldSearchInfo));
		//dd(json_decode($oldSearchInfo));
		return json_decode($oldSearchInfo);
		//return $oldSearchInfo;
	}	
	
}
