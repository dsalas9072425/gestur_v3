<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use Redirect;
use App\Ticket;
use App\Producto;
use App\Persona;
use App\PlazoPago;
use App\Divisas;
use App\DestinoDtpmundo;
use App\Currency;
use App\Aerolinea;
use App\EstadoFactour;
use App\Grupo;
use App\TipoTicket;
use App\TipoTransaccionTicket;
use App\Office;
use Response;
use Image; 
use DB;


class ReporteBspController extends Controller
{

	private function getIdUsuario(){
	
	 return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

}

private function getIdEmpresa(){

  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

}


/**
     * Recibe una fecha 2017-05-02 y pasa a 02/05/2017 
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaSalida($date){
        if( $date != ''){

        $date = explode('-', $date);
            $fecha = $date[2]."/".$date[1]."/".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }


    /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }


	public function index(){

    if($this->getIdEmpresa() == 1){
      $producto = 1;
    }
    if($this->getIdEmpresa() == 2){
      $producto = 8167;
    }
    if($this->getIdEmpresa() == 5){
      $producto = 14869;
    }

    if($this->getIdEmpresa() == 9){
     $producto = 29754;
     //$producto = 29573; 
    }

    if($this->getIdEmpresa() == 17){
      $producto = 37169;
    }

		$aerolineas =DB::select('select id, nombre from personas where id_empresa = '.$this->getIdEmpresa().' and id in (select distinct(id_proveedor) from tickets where id_empresa = '.$this->getIdEmpresa().')');  

    $offices = Office::where('id_empresa',$this->getIdEmpresa())->get();
    
		$calendariobsp = DB::select("SELECT * FROM vw_calendario_bcp");
		$currency = Divisas::where('activo','S')->get();
		$total_porcentaje = 0;
    return view('pages.mc.bspReporte.index')->with(['aerolineas'=>$aerolineas,
                                                    'calendariobsp'=>$calendariobsp,
                                                    'currency'=>$currency,
                                                    'oficces'=>$offices
                                                    ]);
	}





  public function reporteBspCobranzasAjax(Request $req)
  { 
    // dd($req->all());

    $ajax = 0;
    // $formSearch = $req->dataString;
    // dd( $formSearch);
    $draw = intval($req->input('draw'));
    $start = intval($req->input('start'));
    $length = intval($req->input('length'));
    $cont = 0;
    $filtrarMax = 0;

    //ORDENAMIENTO POR COLUMNA
    $columna = $req->input('order')[0]['column'];
    $orden = $req->input('order')[0]['dir'];

    $query = "SELECT * FROM vw_ticket WHERE id_empresa = ".$this->getIdEmpresa();

       
  if($req->input('fecha_emision_desdeHasta'))
  {
      $fecha = explode('-',$req->input('fecha_emision_desdeHasta'));

      $desde = $this->formatoFechaEntrada(trim($fecha[0]));
      $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
      $query .= " AND fecha_emision BETWEEN '".$desde."' AND '".$hasta."' ";
  }
      if($req->input('estado_id')){
        $query .= ' AND id_estado = '.$req->input('estado_id');
      }
      if($req->input('fecha_pago')){
        $query .= " AND fecha_pago_bsp BETWEEN '".$req->input('fecha_pago')."' AND '".$req->input('fecha_pago')."' ";
      }
      if($req->input('moneda_id')){
        $query .= " AND id_currency_costo = ".$req->input('moneda_id');
      }
      if($req->input('aerolinea_id')){
        $query .= " AND id_proveedor = ".$req->input('aerolinea_id');
      }
      if($req->input('estado_cobro')){
        $query .= " AND estado_cobro_ticket = '".$req->input('estado_cobro')."' ";
      }
    

// dd($columna);
      switch ($columna) {
				case '0':
				$query .= ' order by estado_cobro_ticket '.$orden;
				break;

				case '1':
				$query .= ' order by nro_factura '.$orden;
				break;

				case '2':
				$query .= ' order by id_proforma '.$orden;
				break;

				case '3':
				$query .= ' order by cliente_n '.$orden;
				break;

				case '4':
				$query .= ' order by numero_amadeus '.$orden;
				break;

				case '5':
				$query .= ' order by pasajero '.$orden;
				break;
				
				case '6':
				$query .= ' order by total_costo '.$orden;
				break;
				case '7':
				$query .= ' order by vendedor_nombre '.$orden;
				break;
				case '8':
				$query .= ' order by usuario '.$orden;
				break;

			
			default:
				# code...
				break;
		}

      $tickets = DB::select($query);
      $cantidadTicket = count($tickets);
      $resultTicket = array();
      $nro_factura = '';

      // dd($tickets);

      foreach ($tickets as $key => $value) 
      {

      if($cont == $length)
        {
          break;
        }

        if($key >= $start)
        {

       	


        $coma = ($value->currency_code == 'PYG') ? 0 : 2;

        $icoEstado = "fa fa-exclamation-triangle icoRojo";        
        $pesoIcoEstado = 'NO';

        /**
         * ESTADO DE COBRO TICKET
         * SI = PENDIENTE
         * NO = PAGADO
         */

        if($value->estado_cobro_ticket == 'NO'){
        $icoEstado = "fa fa-thumbs-o-up icoVerde";;        
        $pesoIcoEstado = 'SI';
        }

   
/*
   if($value->diferencia == 'COBRADO'){
    $icoEstado = "fa fa-thumbs-o-up icoVerde";;        
    $pesoIcoEstado = 'SI';
    }*/
    /*$query = "SELECT senha123 - entrega FROM vw_ticket WHERE id_empresa = ".$this->getIdEmpresa();
    $sen = DB::select($query);*/
    $entrega =0;
     if($value->entrega>=$value->senha123){
      $entrega='0';
    }else{
     $entrega = $value->senha123 - $value->entrega;
    }
    /*if($entrega == 0){
      $icoEstado = "fa fa-thumbs-o-up icoVerde";;        
      $pesoIcoEstado = 'SI';
    }*/

    if($value->id_estado == 33 ){
      $ico = '<i style="color:red" class="'.$icoEstado.'"></i>
      <div style="display:none;">'. $pesoIcoEstado.'</div>';
      $factura = '<a href="verFactura/'.$value->id_factura.'" class="bgRed"><i class="fa fa-fw fa-search" style="color:red"></i><b>'.$value->nro_factura.'</b></a>';
      $proforma = '<a href="detallesProforma/'.$value->id_proforma.'" class="bgRed"><i class="fa fa-fw fa-search" style="color:red"></i><b>'.$value->id_proforma.'</b></a>';
      $resultTicket[] = [
                      $ico,
                      $factura,
                      $proforma,
                      '<b style="color:red">'.$value->cliente_n.' '.$value->cliente_a.'</b>',
                      '<b style="color:red">'.$value->numero_amadeus.'</b>',
                      '<b style="color:red">'.$value->pasajero.'</b>',
                      '<b style="color:red">'.number_format($value->total_costo,$coma,",",".").'</b>',
                      '<b style="color:red">'.$value->vendedor_nombre." ".$value->vendedor_apellido.'</b>',
                      '<b style="color:red">'.$value->usuario.'</b>',
                      '<b style="color:red">'.number_format($value->senha123,$coma,",",".").'</b>',
                      '<b style="color:red">'.number_format($entrega,$coma,",",".").'</b>',
                      '<b style="color:red">'.$value->usuario_amadeus_n." ".$value->usuario_amadeus_a.'</b>',
                    ];

  }else{
           $ico = '
           <i class="'.$icoEstado.'"></i>
           <div style="display:none;">'. $pesoIcoEstado.'</div>';
           $factura = '<a href="verFactura/'.$value->id_factura.'" class="bgRed"><i class="fa fa-fw fa-search"></i><b>'.$value->nro_factura.'</b></a>';
           $proforma = '<a href="detallesProforma/'.$value->id_proforma.'" class="bgRed"><i class="fa fa-fw fa-search"></i><b>'.$value->id_proforma.'</b></a>';
           $resultTicket[] = [
                            $ico,
                            $factura,
                            $proforma,
                            $value->cliente_n.' '.$value->cliente_a,
                            $value->numero_amadeus, 
                            $value->pasajero,
                            number_format($value->total_costo,$coma,",","."), 
                            $value->vendedor_nombre." ".$value->vendedor_apellido,
                            $value->usuario,
                            number_format($value->senha123,$coma,",","."),
                            number_format($entrega,$coma,",","."),
                            $value->usuario_amadeus_n." ".$value->usuario_amadeus_a,
                            //senha123-entrega // Agregar la diferencia aquí osea seria el estado si el estado es 40 si va a poner en cobrado 
                          ];
                        }
  
          $cont++;
        }
      }

      //dd($resultTicket);
        return response()->json(array('data'=>$resultTicket,
                                    'draw'=>$draw,
                                    'recordsTotal'=>$cantidadTicket,
                                    'recordsFiltered'=>$cantidadTicket));
      

  }


  public function reporteBspCobranzas(Request $req)
  {

  	$calendariobsp = DB::select("SELECT * FROM vw_calendario_bcp");
    $tipos_tickets = TipoTicket::orderBy('descripcion')->get();   
    $estados = EstadoFactour::where('id_tipo_estado', '8')->orderBy('denominacion','ASC')->get();  
    $monedas = Divisas::where('activo','S')->get();
    
    $aerolineas = DB::select('
                        SELECT DISTINCT tk.id_proveedor,
                        p.nombre,
                        p.id 
                        FROM tickets tk
                        LEFT JOIN personas p ON p.id = tk.id_proveedor
                        WHERE tk.id_empresa = '.$this->getIdEmpresa().'
                        AND  tk.id_proveedor <> 513
                        ORDER BY p.nombre ASC');

  

        return view('pages.mc.bspReporte.bcp_cobranzas', compact('estados','monedas','aerolineas','calendariobsp'));
	}//function

	public function consultaBsp(Request $req)
	{
      $query = "SELECT to_char(fecha_emision, 'DDMONYY') fecha_formateada,
                      CASE
                      WHEN comision > 0 THEN ROUND(((comision *100) / ( 
                        CASE 
                          WHEN facial > 0 THEN facial
                          ELSE 1
                          end
                          ) 
                          )::decimal, 2)
                          ELSE 0
                          END as porcentaje_comision, *
                      FROM vw_ticket
                      WHERE id_empresa = ".$this->getIdEmpresa()." 
                        AND id_proveedor != 8391
                        AND (id_grupo IS NULL OR id_grupo = 0)
                        AND id_proveedor <> 513";

		if($req->id_aerolinea != ''){
			$query .= " AND id_proveedor = ".$req->id_aerolinea;
		}
		if($req->fecha_pago != ''){
			$query .= " AND fecha_pago_bsp = '".$req->fecha_pago."' ";
		}
		if($req->id_currency !=''){
			$query .= " AND id_currency_costo = ".$req->id_currency."";
		}

    if($req->id_office !=''){
			$query .= " AND office_id = ".$req->id_office."";
		}

		if($req->periodo_desde_hasta !=''){
			 $fecha =  explode('-', $req->periodo_desde_hasta);

                $desde     = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta     = $this->formatoFechaEntrada(trim($fecha[1])); 
                $desde     = date('Y-m-d H:i:s',strtotime($desde.' 00:00:00'));
                $hasta     = date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'));
			
			$query .= " AND fecha_emision BETWEEN '".$desde."' AND '".$hasta."'";
		}
		$ticket = DB::select($query." ORDER BY numero_amadeus ASC");
		$total_porcentaje = 0;

	// foreach ($ticket as $key => $value) {
	
	// $value->porcentaje_comision = 0;
	
	// if($value->facial > 0){
	// $value->porcentaje_comision = ($value->comision * 100)/$value->facial;
	// $value->porcentaje_comision = round($value->porcentaje_comision,2);
	// 	}
	// }




		return response()->json(array('data'=>$ticket));
	}



}