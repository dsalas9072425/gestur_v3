<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Paquete;
use App\State;
use App\Reserva;
use App\Usuario;
use App\Currency;
use App\CategoriaPaquete;
use App\TipoPaquete;
use App\PaqueteDestinos;
use App\PaqueteCategoriaPaquete;
use App\ImagenPaquete;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use DB;


class PaqueteController extends Controller
{
	public function index(Request $req)
	{
		$paquetes = Paquete::with('destinos', 'categorias', 'imagen', 'usuario', 'moneda')->get();
		return view('pages.mc.paquetes.index')->with(['paquetes'=>$paquetes]);
	}

	public function add(Request $req){
		$datos = file_get_contents("../destination.json");
		$destinoJson =  json_decode($datos, true);
		$datos ="";
		$id_categoria = "";
		$id_destinos = "";
		$arrayDestino = [];
		if(session('data') !== null){
			$datos = session('data');
			$id_categoria = $datos['id_categoria'];
			$id_destinos = $datos['id_destino'];
			$paquetesDestinos = explode(",", $datos['id_destino']);
			foreach($paquetesDestinos as $key=>$destino){
				foreach($destinoJson as $key1=>$dest){
					if($destino == $dest['idDestino']){
						$arrayDestino[$key1]['id'] = $dest['idDestino'];
						$arrayDestino[$key1]['value'] = $dest['desDestino'];
					}
				}
			}
		}

		$currency = Currency::where('activo', 'S')->get();
		$categoria = CategoriaPaquete::where('activo', true)->get();
		$tipoPaquete = TipoPaquete::all();
		return view('pages.mc.paquetes.add')->with(['moneda'=>$currency,'categorias'=>$categoria, 'tipoPaquetes'=>$tipoPaquete, 'inputs'=>$datos, 'categoriaValue'=>$id_categoria, 'destinosValue'=>$id_destinos, 'valorDestinos'=>$arrayDestino]);
	}

	public function doAdd(Request $req){
			if($req->input('imagen') != ""){
				$paquete = new Paquete;
				$periodo_fecha_inicio =  explode("/", $req->input('periodo_fecha_inicio'));
				$periodo_fecha_final =  explode("/", $req->input('periodo_fecha_final'));
				$fecha_baja =  explode("/", $req->input('fecha_baja'));

				$paquete->titulo = $req->input('titulo');
				$paquete->detalle = rtrim($req->input('detalle'));

				if($req->input('fecha_compra_inicio') != ""){
					$fecha_compra_inicio = explode("/", $req->input('fecha_compra_inicio'));
					$paquete->fecha_compra_inicio = $fecha_compra_inicio[2]."-".$fecha_compra_inicio[1]."-".$fecha_compra_inicio[0];
				}
					
				if($req->input('fecha_compra_final') != ""){
					$fecha_compra_final = explode("/", $req->input('fecha_compra_final'));
					$paquete->fecha_compra_final = $fecha_compra_final[2]."-".$fecha_compra_final[1]."-".$fecha_compra_final[0];
				}
				
				if($req->input('fecha_viaje_final') != ""){
					$fecha_viaje_final =  explode("/", $req->input('fecha_viaje_final'));
					$paquete->fecha_viaje_final = $fecha_viaje_final[2]."-".$fecha_viaje_final[1]."-".$fecha_viaje_final[0];
				}

				if($req->input('fecha_viaje_inicio') != ""){
					$fecha_viaje_inicio =  explode("/", $req->input('fecha_viaje_inicio'));
					$paquete->fecha_viaje_inicio = $fecha_viaje_inicio[2]."-".$fecha_viaje_inicio[1]."-".$fecha_viaje_inicio[0];
				}
					
				$paquete->precio = str_replace("", $req->input('precio'));
				$paquete->id_cupo_factour = $req->input('id_cupo_factour');
				$paquete->periodo_fecha_inicio = $periodo_fecha_inicio[2]."-".$periodo_fecha_inicio[1]."-".$periodo_fecha_inicio[0];
				$paquete->periodo_fecha_final = $periodo_fecha_final[2]."-".$periodo_fecha_final[1]."-".$periodo_fecha_final[0];
				$paquete->fecha_baja = $fecha_baja[2]."-".$fecha_baja[1]."-".$fecha_baja[0];
				$paquete->promocion =  $req->input('promo');
				$paquete->activo = 'true';
				$paquete->id_tipo_paquete = $req->input('tipo_paquete_id');
				$paquete->usuario_id = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
				$paquete->currency_id = $req->input('currency_id');
				$paquete->created_at = date('Y-m-d H:m:s');
				$paquete->updated_at = date('Y-m-d H:m:s');
				try{
					$paquete->save();
					$id = Paquete::select('id')->max('id');
					$destinos = explode(",", $req->input('id_destino'));
					$categorias = explode(",", $req->input('id_categoria'));
					foreach($destinos as $key=>$destino){
						$paquete_destinos = new PaqueteDestinos;      
						$paquete_destinos->paquete_id = $id;
						$paquete_destinos->destino_id = $destino;
						$paquete_destinos->save();
					}
					foreach($categorias as $key=>$categoria){
						$paquete_categoria = new PaqueteCategoriaPaquete;      
						$paquete_categoria->id_paquete = $id;
						$paquete_categoria->id_categoria = $categoria;
						$paquete_categoria->save();
					}
					$imagen_paquete = new ImagenPaquete;	
					$imagen_paquete->id_paquete = $id;
					$imagen_paquete->id_tipo_imagen = 1;
					$imagen_paquete->url = $req->input('imagen');
					$imagen_paquete->save();
					flash('Se ha ingresado el usuario exitosamente')->success();
					return redirect()->route('mc.paquetes');
				} catch(\Exception $e){
					flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
					return redirect()->route('mc.generarPaquete');
				}
			}else{
				flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
				return redirect()->back()->with('data', $req->all());
			}	

	}	
	public function edit(Request $req, $id){
		$datos = file_get_contents("../destination.json");
		$destinoJson =  json_decode($datos, true);
		$currency = Currency::where('activo', 'S')->get();
		$categoria = CategoriaPaquete::all();
		$tipoPaquete = TipoPaquete::all();
		$paquetes = Paquete::with('destinos', 'categorias', 'imagen')
							->where('id', '=',$id)->get();
		$arrayDestino = [];
		$sid = "";
		$contador= 0;
		/*		echo '<pre>';
		print_r($paquetes);*/

		foreach($paquetes[0]->destinos as $key=>$destino){
			foreach($destinoJson as $key1=>$dest){
				if($destino->destino_id == $dest['idDestino']&& $destino->activo=true){
					$arrayDestino[$key1]['id'] = $dest['idDestino'];
					$arrayDestino[$key1]['value'] = $dest['desDestino'];
					if($contador == 0){
						$sid .= $dest['idDestino'];
					}else{
						$sid .= ", ".$dest['idDestino'];
					}
					$contador++;
				}
			}
		}
		return view('pages.mc.paquetes.edit')->with(['paquetes'=>$paquetes,'moneda'=>$currency,'categorias'=>$categoria, 'tipoPaquetes'=>$tipoPaquete, 'destinos'=>$arrayDestino, 'valorSelect'=>$sid]);
	}

	public function doEdit(Request $req){
		if($req->input('imagen') != "" && $req->input('tipo_paquete_id') != "" ){
			$periodo_fecha_inicio =  explode("/", $req->input('periodo_fecha_inicio'));
			$periodo_fecha_final =  explode("/", $req->input('periodo_fecha_final'));
			$fecha_baja =  explode("/", $req->input('fecha_baja'));
			if($req->input('fecha_compra_inicio') != ""){
				$fecha_compra_inicio = explode("/", $req->input('fecha_compra_inicio'));
				$fecha_compra_inicios = $fecha_compra_inicio[2]."-".$fecha_compra_inicio[1]."-".$fecha_compra_inicio[0];
			}else{
				$fecha_compra_inicios =null;
			}
					
			if($req->input('fecha_compra_final') != ""){
				$fecha_compra_final = explode("/", $req->input('fecha_compra_final'));
				$fecha_compra_finals = $fecha_compra_final[2]."-".$fecha_compra_final[1]."-".$fecha_compra_final[0];
			}else{
				$fecha_compra_finals = null;
			}		
			
			if($req->input('fecha_viaje_final') != ""){
				$fecha_viaje_final =  explode("/", $req->input('fecha_viaje_final'));
				$fecha_viaje_finals = $fecha_viaje_final[2]."-".$fecha_viaje_final[1]."-".$fecha_viaje_final[0];
			}else{
				$fecha_viaje_finals = null;
			}

			if($req->input('fecha_viaje_inicio') != ""){
				$fecha_viaje_inicio =  explode("/", $req->input('fecha_viaje_inicio'));
				$fecha_viaje_inicios = $fecha_viaje_inicio[2]."-".$fecha_viaje_inicio[1]."-".$fecha_viaje_inicio[0];
			}else{
				$fecha_viaje_inicios = null;
			}
			DB::table('paquetes')
						    ->where('id',$req->input('paquete_id'))
						    ->update([
						            'titulo'=>$req->input('titulo'),
						            'detalle'=>$req->input('detalle'),
						            'precio'=>$req->input('precio'),
						            'periodo_fecha_inicio'=>$periodo_fecha_inicio[2]."-".$periodo_fecha_inicio[1]."-".$periodo_fecha_inicio[0],
						            'periodo_fecha_final'=>$periodo_fecha_final[2]."-".$periodo_fecha_final[1]."-".$periodo_fecha_final[0],
						            'fecha_baja'=> $fecha_baja[2]."-".$fecha_baja[1]."-".$fecha_baja[0],
						            'promocion'=>$req->input('promo'),
						            'id_tipo_paquete'=>$req->input('tipo_paquete_id'),
						            'usuario_id'=>Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario,
						            'fecha_compra_inicio'=>$fecha_compra_inicios,
						            'fecha_compra_final'=>$fecha_compra_finals,
						            'fecha_viaje_final'=>$fecha_viaje_finals,
						            'fecha_viaje_inicio'=>$fecha_viaje_inicios,
						            'currency_id'=>$req->input('currency_id'),
						            'updated_at'=>date('Y-m-d H:m:s'),
						            ]);

			$categorias = explode(",", $req->input('id_categoria'));
			$destinos = explode(",", $req->input('id_destino'));

			$paquete_categoria_paquete = PaqueteCategoriaPaquete::where('id_paquete', $req->input('paquete_id'));
			$paquete_categoria_paquete->delete();	

			$paquete_destinos = PaqueteDestinos::where('paquete_id', $req->input('paquete_id'));
			$paquete_destinos->delete();				

			/*DB::table('paquete_categoria_paquete')
						    ->where('id_paquete',$req->input('paquete_id'))
						    ->update([
									'activo'=>false,
						            ]);
			DB::table('paquete_destinos')
						    ->where('paquete_id',$req->input('paquete_id'))
						    ->update([
									'activo'=>false,
						            ]);*/

					foreach($destinos as $key=>$destino){
						$paquete_destinos = new PaqueteDestinos;      
						$paquete_destinos->paquete_id = $req->input('paquete_id');
						$paquete_destinos->destino_id = $destino;
						$paquete_destinos->created_at = date('Y-m-d H:m:s');
						$paquete_destinos->updated_at = date('Y-m-d H:m:s'); 
						$paquete_destinos->save();
					}

					foreach($categorias as $key=>$categoria){
						$paquete_categoria = new PaqueteCategoriaPaquete;      
						$paquete_categoria->id_paquete = $req->input('paquete_id');
						$paquete_categoria->id_categoria = $categoria;
						$paquete_categoria->save();
					}

			$user = ImagenPaquete::where('id_paquete', $req->input('paquete_id'));
			$user->delete();				

			$imagen_paquete = new ImagenPaquete;	
			$imagen_paquete->id_paquete = $req->input('paquete_id');
			$imagen_paquete->id_tipo_imagen = 1;
			$imagen_paquete->url = $req->input('imagen');
			$imagen_paquete->save();

			flash('Se ha editado el paquete exitosamente')->success();
			return redirect()->route('mc.paquetes');
		}else{
				flash('Por favor suba una imagen del Flyer, Intentelo nuevamente')->error();
				return redirect()->back();
			}		
		}



	public function destinos(Request $req){
		
		$datos = file_get_contents("../destination.json");
		$destinos =  json_decode($datos, true);
		if(isset($_GET['q'])){
            $search = $_GET['q'];
				foreach($destinos as $key=>$destino){
					$resultado = strpos(strtolower($destino['desDestino']), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		$json = [];
		foreach($destinos as $key=>$row){
		    $json[$row['idDestino']]['text'] = $row['desDestino'];
		}
		echo json_encode($json);
	}	
	public function buscarPaquete(Request $req){

		return view('pages.paquetes.buscar');
	}

	public function doBuscarPaquete(Request $req){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
							 ]
		]);
		$iibReq = new \StdClass; 
		$iibReq->token = Session::get('datos-loggeo')->token;
		$iibRsp = $client->post(Config::get('config.mostrarPaquetes'), [
				'json' => $iibReq
			]);
		$paqueteJson = json_decode($iibRsp->getBody());
		return json_encode($paqueteJson);
	}

	public function doFiltroPaquete(Request $req){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
							 ]
		]);
		$iibReq = new \StdClass; 
		$iibReq->token = Session::get('datos-loggeo')->token;
		$iibRsp = $client->post(Config::get('config.mostrarPaquetes'), [
				'json' => $iibReq
			]);
		$paqueteJson = json_decode($iibRsp->getBody());
		$paquetesBase  = $paqueteJson;

		if($req->input('filtroCategoriaInput')){
			$filtroCategoria = explode(",", $req->input('filtroCategoriaInput'));
			foreach($paquetesBase->paquetes as $keys=>$paquetes){
				foreach($filtroCategoria as $key=>$categoria){
					$contador = count($paquetes->categorias);
					$indice = 0;
					for ($x = 0; $x < $contador; $x++){
						if($paquetes->categorias[$x]->id == $categoria){
							$indice = 1;
						}	
					}
					if($indice == 0){
						unset($paquetesBase->paquetes[$keys]);
					}
				}	
			}	
		}

		if($req->input('filtroDestinoInput')){
			$arraySalida= new \StdClass;
			$filtroDestino = explode(",", $req->input('filtroDestinoInput'));
			foreach($paquetesBase->paquetes as $keys=>$paquetes){
				foreach($filtroDestino as $key=>$destino){
					$contador = count($paquetes->destinos);
					$indice = 0;
					for ($x = 0; $x < $contador; $x++){
						if($paquetes->destinos[$x]->id == $destino){
							$indice = 1;
						}	
					}
					if($indice == 0){
						unset($paquetesBase->paquetes[$keys]);
					}
				}	
			}	
		}

		if($req->input('filtroPaisesInput')){
			$arraySalida= new \StdClass;
			$filtroPaises = explode(",", $req->input('filtroPaisesInput'));
			foreach($paquetesBase->paquetes as $keys=>$paquetes){
				foreach($filtroPaises as $key=>$pais){
					$contador = count($paquetes->destinos);
					$indice = 0;
					for ($x = 0; $x < $contador; $x++){
						if($paquetes->destinos[$x]->idPais == $pais){
							$indice = 1;
						}	
					}
					if($indice == 0){
						unset($paquetesBase->paquetes[$keys]);
					}
				}	
			}	
		}
			
		if($req->input('filtroPeriodosInput')){
			$arraySalida= new \StdClass;
			$filtroPeriodo = $req->input('filtroPeriodosInput');
			foreach($paquetesBase->paquetes as $key=>$paquetes){
				$contador = count($paquetes->periodo);
				for ($x = 0; $x < $contador; $x++){
					if($paquetes->periodo[$x]->periodo == $filtroPeriodo){
						$arraySalida->paquetes[$key]= $paquetesBase->paquetes[$key];
					}
				}
			}
			$paquetesBase = $arraySalida;
		}else{
			$paquetesBase = $paquetesBase;
		}

		if($req->input('promociones')){
			$arraySalida= new \StdClass;
			$filtroPromociones = $req->input('promociones');
			foreach($paquetesBase->paquetes as $key=>$paquetes){
				if($paquetes->promocion == true){
					$arraySalida->paquetes[$key]= $paquetesBase->paquetes[$key];
				}
			}
			$paquetesBase = $arraySalida;
		}else{
			$paquetesBase = $paquetesBase;
		}
		if($req->input('sugeridos')){
			$arraySalida= new \StdClass;
			$filtroSugeridos = $req->input('sugeridos');
			foreach($paquetesBase->paquetes as $key=>$paquetes){
				if($paquetes->sugerido == true){
					$arraySalida->paquetes[$key]= $paquetesBase->paquetes[$key];
				}
			}
			$paquetesBase = $arraySalida;
		}else{
			$paquetesBase = $paquetesBase;
		}

		return json_encode($paquetesBase);
	}

	public function paqueteDetalles(Request $req , $id){
		$agenciaId  =$req->session()->get('datos-loggeo')->datos->datosUsuarios->idAgencia;
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
							 ]
		]);
		$iibReq = new \StdClass; 
		$iibReq->token = Session::get('datos-loggeo')->token;
		$iibReq->id = $id;
		$iibRsp = $client->post(Config::get('config.mostrarPaquetes'), [
				'json' => $iibReq
			]);
		$paqueteJson = json_decode($iibRsp->getBody());
		return view('pages.paquetes.paqueteDetalles')->with(['paquetes'=>$paqueteJson, 'agenciaId'=>$agenciaId]);
	}

	public function destinosSelect(Request $req){
		$datos = file_get_contents("../destination.json");
		$destinoJson =  json_decode($datos, true);

		$paquetes = Paquete::with('destinos', 'categorias')
							->where('id', '=',$req->input('file'))->get();
		$destinys  = $paquetes[0]->destinos;
		$arrayDestino = [];
		foreach($destinys as $key=>$destino){
			foreach($destinoJson as $key1=>$dest){
				if($destino->destino_id == $dest['idDestino']){
					$arrayDestino[$key1]['id'] = $dest['idDestino'];
					$arrayDestino[$key1]['value'] = $dest['desDestino'];
				}
			}
		}
		return json_encode($arrayDestino);
	}	
	public function categoriaSelect(Request $req){
		$paquetes = Paquete::with('destinos', 'categorias')
							->where('id', '=',$req->input('file'))
							->get();
		return json_encode($paquetes[0]->categorias);

	}	

	public function indexCategoria(Request $req){
		$categoria = CategoriaPaquete::where('activo', true)->get();
		return view('pages.mc.paquetes.indexCategoria')->with(['categorias'=>$categoria]);
	}

	public function addCategoria(Request $req){
		return view('pages.mc.paquetes.addCategoria');
	}	

	public function doAddCategoria(Request $req){
		$categoria = new CategoriaPaquete;
		$categoria->denominacion = $req->input('denominacion');
		$categoria->activo = 'true';
		$categoria->save();
		try{
				$categoria->save();
				flash('Se ha ingresado el usuario exitosamente')->success();
				return redirect()->route('mc.indexCategoria');
			} catch(\Exception $e){
				flash('Ha ocurrido un error en su pedido, Intentelo nuevamente')->error();
				return redirect()->route('mc.generarCategoria');
			}
	}
	public function editarCategoria(Request $req, $id){
		$categoria = CategoriaPaquete::where('id', '=' ,$id )->get();
		return view('pages.mc.paquetes.editCategoria')->with(['categorias'=>$categoria]);
	}	

	public function doEditCategoria(Request $req){
		DB::table('categoria_paquete')
					    ->where('id',$req->input('id'))
					    ->update([
								'denominacion'=>$req->input('denominacion'),
					            ]);
		flash('Se ha editado la categoria exitosamente')->success();
		return redirect()->route('mc.indexCategoria');

	}	
	public function eliminarCategoria(Request $req){
		DB::table('categoria_paquete')
					    ->where('id',$req->input('id'))
					    ->update([
								'activo'=>false,
					            ]);
		flash('Se ha eliminado la categoria exitosamente')->success();
		return redirect()->route('mc.indexCategoria');
    }
	
	public function getCancelarPaquete(Request $req){
		$paquetes = Paquete::where('id', '=',$req->input('idReserva'))->get();		
		if($paquetes[0]->activo == 1){
			$estado  = false;
			$respuesta = 'Se desactivo el Paquete';
		}else{
			$estado  = true;
			$respuesta = 'Se activo el Paquete';
		}
		DB::table('paquetes')
					    ->where('id',$req->input('idReserva'))
					    ->update([
								'activo'=>$estado,
					            ]);
		$arrayRsp= new \StdClass;
		$arrayRsp= $estado;
		return json_encode($arrayRsp);			    
	
	}

	public function getSugeridoPaquete(Request $req){
		$paquetes = Paquete::where('id', '=',$req->input('idReserva'))->get();		
		if($paquetes[0]->sugerido == 1){
			$estado  = false;
			$respuesta = 'No se sugirio el Paquete';
		}else{
			$estado  = true;
			$respuesta = 'Se sugirio el Paquete';
		}
		DB::table('paquetes')
					    ->where('id',$req->input('idReserva'))
					    ->update([
								'sugerido'=>$estado,
					            ]);
		$arrayRsp= new \StdClass;
		$arrayRsp= $estado;
		return json_encode($arrayRsp);			    
	
	}

}