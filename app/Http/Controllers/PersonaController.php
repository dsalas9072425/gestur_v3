<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use Redirect;
use App\Persona;
use App\TipoPersona;
use App\TipoIdentidad;
use App\SucursalEmpresa;
use App\Proforma;
use App\CentroCosto;
use App\EmpresaPersona;
use App\Pais;
use App\Ciudad;
use App\PlazoCobro;
use App\TipoFacturacion;
use App\TipoPersoneria;
use App\LineaCredito;
use App\PlazoPago;
use App\BancoPlaza;
use App\Empresa;
use App\TipoProveedor;
use App\BancoCabecera;
use App\PersonaSet;
use Illuminate\Support\Facades\Log;
use Response;
use Image; 
use DB;
use Maatwebsite\Excel\Facades\Excel;


class PersonaController extends Controller
{
	/**
	 * Tipo Personas
	 */
	private $idSucursalAgencia = '9';
	private $idSucursalEmpresa = '21';
	private $idAgencia = '8';
	private $idTipoEmpresa = '20';

	private $idSupervisorEmpresa = '4'; 


	private $idVendedorEmpresa = '3';
	private $idVendedorAgencia = '10';

	/**
	 * Estado de Linea Credito
	 */
	private $idEstadoCredito = '23';
	private $idEmpresa = '1';

	private function getIdUsuario(){
		
		 return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

	}

	private function getIdEmpresa(){

	  return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

	}

    /**
     * Recibe una fecha 2017-05-02 y pasa a 02/05/2017 
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaSalida($date){
        if( $date != ''){

        $date = explode('-', $date);
            $fecha = $date[2]."/".$date[1]."/".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }


    /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }

	private function verAgenciaEmpresa($id_tipo_persona){

		 $tipoEmpresa = [2,3,4,5,6,21,9];
		 $tipoAgencia = [22,9,10,12];


		foreach ($tipoEmpresa as $key => $value) {
			if($id_tipo_persona == $value)
				return 'empresa';
		}

		foreach ($tipoAgencia as $key => $value) {
			if($id_tipo_persona == $value)
				return 'agencia';
		}

	}


	public function modificarMarkupAgencia(Request $req){
		// dd($req->data);
		$err = false;

		try{
			DB::beginTransaction();

			$this->updateMarkup($req->data);
			$err = true;

		} catch(\Exception $e){
			DB::rollBack();
		}//try

		DB::commit();

		return response()->json(['resp'=>$err]);
	}//function
	
	private function updateMarkup($data){
			$idUsuario = $this->getIdUsuario();

			foreach ($data as $key => $value) {
			DB::table('personas')->where('id',$value['idAgencia'])
					->update(['comision_pactada'=>$value['markup'],
							  'id_usuario'=>$idUsuario,
							  'updated_at'=> date('Y-m-d H:i:00')]);	
			
			}//function

	}//function


	public function incentivoVendedor(Request $req){

		$permiso = DB::table('persona_permiso_especiales')->where('id_persona',$this->getIdUsuario())->where('id_permiso',50)->count();
		if($permiso == 0){
	    	flash('No tiene los permisos necesarios para ingresar a esta dirección')->error();
            return redirect()->route('home');
		} 
		$ajax = 0;
		$id_empresa = $this->getIdEmpresa();

		$personas = Persona::where('id_tipo_persona',10)
					->where('id_empresa',$id_empresa)
					->where('activo','true')
					->orderBy('nombre', 'ASC')->get();

		$empresaAgencia = Persona::where('id_tipo_persona',8)
						->where('id_empresa',$id_empresa)
						->where('activo','true')
						->orderBy('nombre', 'ASC')->get();


		if(!empty($req->all()) && !is_null($req->all())){

			$ajax++;
			$formSearch = $req->formSearch;
			$draw = intval($req->draw);
			$start = intval($req->start);
			$length = intval($req->length);


            //ORDENAMIENTO POR COLUMNA
            $columna = $req->order[0]['column'];
            $orden = $req->order[0]['dir'];



			$cont = 0;
		$filtrarMax = 0;
		
		$idEmpresaAgencia = $formSearch[0]['value'];
		$idPersona  = $formSearch[1]['value'];
		$cedulaRuc =  $formSearch[2]['value'];
		$email = $formSearch[3]['value'];

		$query = " SELECT 
				   p.id,
				   p.comision_pactada, 
				   ag.nombre as nombreAgencia , 
				   p.nombre as nombrePersona,
				   P.apellido as apellidoPersona, 
				   p.email,
				   p.activo,
				   p.denominacion_comercial, 
				   p.documento_identidad, 
				   to_char(p.fecha_alta,'DD/MM/YYYY') as fecha_alta
				   FROM personas AS p
				   LEFT JOIN personas AS ag ON p.id_persona = ag.id
				   WHERE p.id_empresa = ".$this->getIdEmpresa()." 
				   AND p.activo = true AND p.id_tipo_persona = 10 ";

		if($idPersona != ''){
			$query .= ' AND p.id = '.intval($idPersona);
		} 

		if($cedulaRuc != ''){
		$query .= " AND p.documento_identidad LIKE  '%".$cedulaRuc."%' ";
		}

		if($email != ''){
		$email = trim(strtoupper($email));	
		$query .= " AND UPPER(p.email)  = '".$email."' ";
		}

		if($idEmpresaAgencia != ''){
		$query .= ' AND p.id_persona = '.intval($idEmpresaAgencia).' ';
		}

		// dd($columna);
			switch ($columna) {
			case '0':
				$query .= ' order by nombreagencia '.$orden;
				break;

				case '1':
				$query .= ' order by nombrepersona '.$orden;
				break;

				case '2':
				$query .= ' order by email '.$orden;
				break;

				case '3':
				$query .=  ' order by documento_identidad '.$orden;
				break;

			
			default:
				# code...
				break;
		}
		// dd($query);
		$personas = DB::select($query);

		$cantidadPersonas = count($personas);
		$lengtArray =$cantidadPersonas;

		$resultPersonas = array();


		// dd($length);
		foreach ($personas as $key => $value) {
			//dd($value);			
			if($cont == $length){
				break;
			}

			if($key >= $start){


				$value->btn = "<button  class='btn btn-info' onclick='modalEdit($value->id)'  style='padding-left: 6px;padding-right: 6px;' ><i class='fa fa-fw fa-edit'></i></button>";

			
				$resultPersonas[] =    [
										$value->nombreagencia, 
										$value->nombrepersona.' '.$value->apellidopersona,
										$value->email,
										$value->documento_identidad,
										$value->comision_pactada,
									    $value->btn];
				
									    $cont++;
				}

			}//foreach


		}//IF REQUEST


		if($ajax > 0){
		return response()->json(['draw'=>$draw,
								 'recordsTotal'=>$lengtArray,
								 'recordsFiltered'=>$lengtArray,
								 'data'=>$resultPersonas]);	
		} else {	
			return view('pages.mc.persona.incentivo')->with(['selectPersona'=>$personas,'empresaAgencia'=>$empresaAgencia]);
		}


	}//function


	public function modificarIncentivo(Request $req){

		$idPersona = $req->idPersona;
		$incentivo = $req->newIncentivo;
		$err = false;

		try{
		DB::table('personas')->where('id',$idPersona)
		->update(['comision_pactada'=>$incentivo,
				 'id_usuario'=>$this->getIdUsuario(),
				 'updated_at'=> date('Y-m-d H:i:00')]);	
		$err = true;
		} catch(\Exception $e){	
		$err = false;
		}//try


		return response()->json(['resp'=>$err]);

	}//function


	public function obtenerIncentivo(Request $req){
		$incentivo = '';
		$idPersona = $req->idPersona;
		if($idPersona != ''){
		$incentivo = Persona::where('id',$idPersona)->first();
		} 

		return response()->json(['incentivo'=>$incentivo]);

	}//function


	public function markupAgencia(Request $req){
		$ajax = 0;

		if(!empty($req->all()) && !is_null($req->all())){
			$personas = Persona::with('tipoPersona','agencia')
			->where('id_tipo_persona','8')
						->where('id_empresa',$this->getIdEmpresa())
						->where('activo','true')
						->orderBy('nombre', 'ASC')->get();
			$ajax++;
			}

		if($ajax > 0){
		return response()->json(['data'=>$personas]);	
		} else {	
		return view('pages.mc.persona.markupAgencia');
		}

	}//function

	/**
	 * Retorna datos a la vista de Personas Index, y recibe request ajax de dataTable para enviar datos 
	 * en forma de serverSide
	 * @return Array Json
	 */
	public function index(Request $req){
		//dd($req->search['value']);

		ini_set('memory_limit', '-1');
		set_time_limit(300);
		$draw = intval($req->draw);
		$start = intval($req->start);
		$length = intval($req->length);
		$ajax = 0;
		$id_empresa = $this->getIdEmpresa();
		if($id_empresa == 1 || $id_empresa == 4){
			$tipoPersonas = TipoPersona::orderBy('denominacion', 'ASC')->get();
		}else{
			$tipoPersonas = TipoPersona::where('ver_agencia','false')->orderBy('denominacion', 'ASC')->get();
		}
		$personas = Persona::with('tipoPersona','agencia')->orderBy('nombre', 'ASC')->where('id_empresa',$id_empresa)->where('activo','true')->offset($start)->limit($length)->get();
		$empresaAgencia = Persona::whereIn('id_tipo_persona',[8,20])->where('id_empresa',$id_empresa)->orderBy('nombre', 'ASC')->get();


		if(!empty($req->all()) && !is_null($req->all())){

		$ajax++;
		$formSearch = $req->formSearch;

		//ORDENAMIENTO POR COLUMNA
		$columna = $req->order[0]['column'];
		$orden = $req->order[0]['dir'];

		
		$filtrarMax = 0;

		$idPersona = $formSearch[0]['value'];
		$estado = $formSearch[1]['value'];
		$idTipoPersona = $formSearch[2]['value'];
		$idEmpresaAgencia  = $formSearch[3]['value'];
		$cedulaRuc =  trim($formSearch[4]['value']);
		if(isset($formSearch[5]['value'])){
			$email = trim($formSearch[5]['value']);
		}else{
			$email = "";
		}

		$query = " SELECT 
				   ag.nombre as nombreAgencia , 
				   p.nombre as nombrePersona,
				   P.apellido as apellidoPersona, 
				   p.email,
				   p.dv, 
				   tp.denominacion, 
				   p.activo,
				   p.activo_dtpmundo,
				   p.denominacion_comercial, 
				   p.documento_identidad, 
				   to_char(p.fecha_alta,'DD/MM/YYYY') as fecha_alta,p.id 
				   FROM personas AS p
				   LEFT JOIN personas AS ag ON p.id_persona = ag.id
				   LEFT JOIN tipo_persona AS tp ON p.id_tipo_persona = tp.id
				   WHERE p.id_empresa = ".$this->getIdEmpresa();

		if ($req->search['value']) {
			$searchValue = strtoupper($req->search['value']); // Convertir a mayúsculas
			$query .= " AND (
				UPPER(p.email) LIKE '%$searchValue%' OR
				UPPER(ag.nombre) LIKE '%$searchValue%' OR
				UPPER(p.nombre) LIKE '%$searchValue%' OR
				UPPER(p.apellido) LIKE '%$searchValue%' OR
				UPPER(p.denominacion_comercial) LIKE '%$searchValue%' OR
				UPPER(p.documento_identidad) LIKE '%$searchValue%' OR
				UPPER(tp.denominacion) LIKE '%$searchValue%' 
			)";
		}
			/*if ($req->has('apellido')) {
				$query->where('apellido', 'like', "%" . $req->apellido . "%");
			}*/

		if($idTipoPersona != ''){	

			$query .= 'AND p.id_tipo_persona = '.intval($idTipoPersona);
		}
		if($idPersona != ''){
			$query .= ' AND p.id = '.intval($idPersona);

		} 
		if($estado != ''){
			$query .= ' AND p.activo = '.$estado.' ';
		}

		if($cedulaRuc != ''){
		$query .= " AND p.documento_identidad LIKE  '%".$cedulaRuc."%' ";
		}
		if($email != ''){
		$email = trim(strtoupper($email));	
		$query .= " AND UPPER(p.email)  LIKE '%".$email."%' ";

		}
		if($idEmpresaAgencia != ''){
			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 1){
				$query .= ' AND (p.id_persona = '.intval($idEmpresaAgencia).' OR   p.id = '.intval($idEmpresaAgencia).') ';
			}
		}
		$personasTotal= DB::select($query);
		// dd($columna);
			switch ($columna) {
			case '0':
				$query .= ' order by nombreagencia '.$orden;
				break;

				case '1':
				$query .= ' order by nombrepersona '.$orden;
				break;

				case '2':
				$query .= ' order by denominacion '.$orden;
				break;

				case '3':
				$query .= ' order by email '.$orden;
				break;

				case '4':
				$query .= ' order by denominacion_comercial '.$orden;
				break;

				case '5 ':
				$query .= ' order by documento_identidad '.$orden;
				break;
			
			default:
				# code...
				break;
		}
		$query .= ' offset('.$start.') limit('.$length.')';

		$personas = DB::select($query);
		//dd($personas,$query);
		$cantidadPersonas = count($personasTotal);
		$lengtArray =$cantidadPersonas;

		$resultPersonas = array();

		// dd($length);


		/*
		$permiso = DB::table('persona_permiso_especiales')->where('id_persona',$this->getIdUsuario())->where('id_permiso',63)->count();
		if($permiso == 0){
	    	flash('No tiene los permisos necesarios para ingresar a esta dirección')->error();
            return redirect()->route('home');
		} */
		foreach ($personas as $key => $value) {
			//dd($value);			
				$ruta = route('personaEdit',['id' =>$value->id]);
				$G = $value->id;
				$rutaau = route('indexAuditoriaPersonas').'?id='.$value->id;
				$value->id = "<a href='".$ruta."' class='btn btn-info' style='padding-left: 6px;padding-right: 6px;' role='button'><i class='fa fa-fw fa-edit'></i></a>";
				$permiso = DB::table('persona_permiso_especiales')->where('id_persona', $this->getIdUsuario())->where('id_permiso', 63)->count();

				if($permiso>0){
					$value->btn_auditoria = "<a href='".$rutaau."' class='btn btn-success' style='padding-left: 6px;padding-right: 6px;' role='button'><i class='fa fa-fw fa-user-secret'></i></a>";
				}
				else{
					$value->btn_auditoria = "Sin permisos";
				}

				
				//$value->btn_auditoria = "<a href='".$rutaau."' class='btn btn-success' style='padding-left: 6px;padding-right: 6px;' role='button'><i class='fa fa-fw fa-user-secret'></i></a>";
				if($value->activo == true){
					$iconoG = '<i class="fa fa-fw fa-circle  verde " title="Activo Gestur" style="color: green;"></i>';
				}else{
					$iconoG = '<i class="fa fa-fw fa-circle  rojo " title="Inactivo Gestur" style="color: red;"></i>';
				}
				if($value->activo_dtpmundo == true){
					$iconoO = '<i class="fa fa-fw fa-circle  verde " title="Activo Online" style="color: green;"></i>';
				}else{
					$iconoO = '<i class="fa fa-fw fa-circle  rojo " title="Inactivo Online" style="color: red;"></i>';
				}
				if(Session::get('datos-loggeo')->datos->datosUsuarios->id_plan_sistema == 3){
					$ruc = $value->documento_identidad;
					if($value->dv){
						$ruc = $ruc."-".$value->dv;
					}
	

	
					$resultPersonas[] =    [
											$G,
											$value->nombreagencia, 
											$value->nombrepersona.' '.$value->apellidopersona,
											$value->denominacion,
											$value->email,
											$value->denominacion_comercial,
										//	$G ,
											$ruc,
											$iconoG,
											$iconoO,
										    $value->id,
											$value->btn_auditoria
										    ];
			
				}else{
					$ruc = $value->documento_identidad;
					if($value->dv){
						$ruc .= $ruc."-".$value->dv;
					}

					$resultPersonas[] =    [
											$G,
											$value->nombreagencia, 
											$value->nombrepersona.' '.$value->apellidopersona,
											$value->denominacion,
											$value->email,
											$value->denominacion_comercial,
											//$G ,
											$ruc,
											$iconoG,
										    $value->id,
											$value->btn_auditoria
										    ];
	
				}
			
		}//foreach


		}//IF REQUEST

		
		

		if($ajax > 0){
		return response()->json(['draw'=>$draw,
								 'recordsTotal'=>$lengtArray,
								 'recordsFiltered'=>$lengtArray,
								 'data'=>$resultPersonas]);	
		} else {	
			return view('pages.mc.persona.index')->with(['selectPersona'=>$personas,'tipoPersonas'=>$tipoPersonas,'empresaAgencia'=>$empresaAgencia]);
		}
	}


	public function validarCorreo(Request $req){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 1){
			$correo = trim(strtoupper($req->e));
			$idTipoPersona = $req->input('idTipoPersona');
			$err = DB::select("SELECT count(p.*) as c 
							FROM personas p
							LEFT JOIN tipo_persona tp ON tp.id = p.id_tipo_persona
							WHERE UPPER(p.email) = '".$correo."' AND tp.puede_loguearse = true AND p.activo = true
							AND p.id_empresa = ".$this->getIdEmpresa());
			$err = $err[0]->c;
		}else{
			$err = 0;
		}
		return response()->json(array('resp'=>$err));

	}

	/**
	 * Recibe el Correo de personas y envia en hash MD5
	 * @param  Request $req->correo
	 * @return String md5
	 */
	public function generarHash(Request $req){
		$email = trim($req->online);
		if($req->indice == 0){
			$rsp = $this->limitar_cadena(md5($email),14, "N");
		}else{
			if($req->indice == ""|| is_null($req->indice)){
				$rsp = $this->limitar_cadena(md5($email),14, "N");
			}else{
				$rsp = $this->limitar_cadena(md5($email),(int)$req->indice, "N");

			}
		}
		return response()->json(array('resp'=>$rsp));
	}

	public function generarHashPrueba(Request $req){
		$email = trim($req->online);
		if($req->indice == 0){
			$rsp = $this->limitar_cadena(md5($email),14, "N");
		}else{
			if($req->indice == ""|| is_null($req->indice)){
				$rsp = md5($email.''.$req->indice);
			}else{

				$rsp = $this->limitar_cadena(md5($email),((int)$req->indice), "N");
			}
		}
		return response()->json(array('resp'=>$rsp));
	}

	public function limitar_cadena($cadena, $limite, $sufijo){
		// Si la longitud es mayor que el límite...
		if(strlen($cadena) > $limite){
			// Entonces corta la cadena y ponle el sufijo
			return $sufijo.''.substr($cadena, 0, $limite);
		}
		
		// Si no, entonces devuelve la cadena normal
		return $cadena;
	}
	public function crearCiudadPais(Request $req){


		// dd($req->all());

		$paisControl = trim(strtoupper($req->pais));
		$ciudadControl = trim(strtoupper($req->ciudad));
		$codPais = trim(strtoupper($req->codPais));
		$pais = trim($req->pais);
		$ciudad = trim($req->ciudad);
		$paisRelacion = $req->paisRelacion;
		$opcion = $req->opcion;
		$operacion = $req->operacion;
		$err = false;
		$pais_id = '';
		$m = '';



		//OPERACION PARA CREAR PAIS O CIUDAD
		if($operacion == 1){
			 $tablaPais = new Pais;
			 $ciudades = new Ciudad;

			  if($opcion == 1){
				 $ciudades->denominacion = $ciudad;	
				 $ciudades->id_pais = $paisRelacion;
				 $ciudV = Ciudad::whereRaw("UPPER(denominacion) = '".$ciudadControl."'")->get(); 
				 if($ciudV->count() > 0){$m .= ' Nombre de ciudad ya existe ';}	
			}


			  if($opcion == 2){
			 $paisV = Pais::whereRaw("UPPER(name_es) = '".$paisControl ."' OR UPPER(code) = '".$codPais."'")->get(); 
			 if($paisV->count() > 0){$m .= ' Nombre o codigo pais ya existe ';}

			 $tablaPais->name_es = $pais;
			 $tablaPais->name_en = $pais;
			 $tablaPais->name_pt = $pais;
			 $tablaPais->des_pais = $pais;
			 $tablaPais->habilitado = true;
			 $tablaPais->code = $codPais;

			}

		

			if($m == ''){


		 try{

		if($opcion == 1){
			//CREAR CIUDAD 
			$ciudades->id_pais = $paisRelacion;
			$ciudades->save();
			$err = true;
		}

		if($opcion == 2){
			//CREAR PAIS 
			$tablaPais->save();	
			$pais_id = $tablaPais->cod_pais;
			$err = true;
		
		}

	} catch(\Exception $e){
		$err = false;
		$m = 'Ocurrio un error al intentar realizar la operación';
	}//try


	}//mensaje

	return response()->json(array('resp'=>$err,'m'=>$m,'id'=>$pais_id));

		} else {
		//OPERACION PARA RETORNAR PAISES EN EL COMBO	


			$paises = Pais::where('habilitado',true)->orderBy('name_es','ASC')->get();

			return response()->json(array('p'=>$paises));

		}

		
	}//function





	/**
	 * DESHABILITADO
	 * Ajax para consutla de personas en Index Persona
	 * @param  Request $req idPersona idTipoPersona
	 * @return array de datos
	 */
	public function consultaPersona(Request $req){


		if(!is_null($req->all()) && !empty($req->all())){



			$personas = Persona::with('tipoPersona','agencia')->where(function($query) use($req)
        {
            if(!empty($req->input('idPersona'))){
                    $query->where('id',$req->input('idPersona'));
            }
            if(!empty($req->input('idTipoPersona'))){
                     $query->where('id_tipo_persona',$req->input('idTipoPersona'));
            }
            if(!empty($req->input('idEmpresaAgencia'))){
                     $query->where('id_persona',$req->input('idEmpresaAgencia'));
            }

        })->get();

		} else {
			$personas = Persona::with('tipoPersona','agencia')->where('id_empresa',$this->getIdEmpresa())->get();
		}

		//dd($personas );

		foreach ($personas as $persona) {
			$fecha = $persona->fecha_alta;

			//dd($fecha = $persona->datefecha_alta);
			if($fecha){
				$persona->fecha_alta = date('d/m/Y',strtotime($fecha));
			} else {
				$persona->fecha_alta = "N/A";
			}
		}//foreach

		return json_encode(['datos'=>$personas]);

	}//function

	private function validar($var){

		if($var !== NULL && $var != ''){
			return $var;
		} else {
			return NULL;
		}

	}


	/**
	 * Para poblar la tabla en el formulario de editar
	 * @param  Request $req [description]
	 * @param  [type]  $id  id de la perona
	 * @return [type]       Retorna los datos de la persona
	 */
	public function edit(Request $req,$id)
	{
		$agencia = null;
		$sucursalAgencia = null;
		$id_empresa = $this->getIdEmpresa();
		$monto = 0;
		$empresa_persona = EmpresaPersona::with('empresas','sucursal')
										->where('id_persona',$id)
										->where('id_empresa',$this->getIdEmpresa())
										->get();
		$empresas = Persona::where('activo',true)->where('id_tipo_persona','20')->get(); //EMPRESAS
		//$persona = Persona::where('id',$id)->first();
		$persona = Persona::with('agencia','sucursalAgencia','sucursalEmpresa','usuarioAuditoria')
							->where('id_empresa',$this->getIdEmpresa())
							->where('id',$id)
							->first();

		if(!isset($persona->id)){
			flash('El registro solicitado no existe. Intentelo nuevamente!!')->error();
			return redirect()->route('home');
		}

		$tipoProveedores = TipoProveedor::whereIn('id', array(1,2))->get();
					
		// dd($persona);
		$lineaCredito = LineaCredito::where('id_persona',$id)->where('id_estado','23')->get();
		$sucursalCartera = Persona::where('activo',true)->where('id_tipo_persona','21')->where('id_empresa',$this->getIdEmpresa())->get();
		$plazoCobro = PlazoCobro::where('id_empresa',$this->getIdEmpresa())->get();
		//PERMISO PARA EDITAR DOCUMENTO DE IDENTIDAD
		$permiso_doc = DB::table('persona_permiso_especiales')->where('id_persona',$this->getIdUsuario())->where('id_permiso',21)->count();

		if($permiso_doc > 0){
			$permiso_doc = true;
		} else {
			$permiso_doc = false;
		}

		//PERMISO PARA EDITAR TIP PERSONA
		$permiso_tp = DB::table('persona_permiso_especiales')->where('id_persona',$this->getIdUsuario())->where('id_permiso',68)->count();
		$cambiar_tipo_persona = $permiso_tp ? true : false;

		if(!empty($lineaCredito[0]) && !is_null($lineaCredito[0])){
			$monto = $lineaCredito[0]->monto;
		} 

		if(!empty($persona->fecha_nacimiento) && !is_null($persona->fecha_nacimiento)){
			$dateN = $persona->fecha_nacimiento;
		}else{
			$dateN = "";
		}


		 if($dateN != '' && $dateN != NULL){
		 	$fecha = $this->formatoFechaSalida($dateN);
		 } else {
		 	$fecha = Null;
		 }

		 $id_empresa = $this->getIdEmpresa();

		 //TODO: corregir de fondo el problema
        // if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 1){
			$tipoPersona = TipoPersona::orderBy('denominacion', 'ASC')->where('activo', true)->get();
		// }else{
		// 	$tipoPersona = TipoPersona::orderBy('denominacion', 'ASC')->whereNotIn('id', array(20, 2, 1))->where('activo', true)->get();
		// }


		//	$tipoPersona = TipoPersona::orderBy('denominacion', 'ASC')->where('activo', true)->get();

		$tipoIdentidad = TipoIdentidad::where('activo', true)->get();

		$pais = Pais::where('habilitado', true)->get();

		$ciudad = Ciudad::get();

		$plazosPago = PlazoPago::all();

		if(!empty($persona->agencia)){
		//$sucursal = $persona->agencia->id;

		$agencia = Persona::query();
		$agencia = $agencia->where('id_empresa',$id_empresa);

		$iDtipoPersona = $this->verAgenciaEmpresa($persona->id_tipo_persona);
		// echo $iDtipoPersona;

		if($iDtipoPersona == "empresa"){
			
			$agencia = $agencia->where('id_tipo_persona',$this->idTipoEmpresa);
		}

		else if($iDtipoPersona == "agencia" ){
			
			$agencia = $agencia->where('id_tipo_persona',$this->idAgencia);
		}

		$agencia = $agencia->get();

		}

 				// dd($agencia);

		if(!empty($persona->sucursalAgencia)){

			$sucursalAgencia  = Persona::query();
			$sucursalAgencia  = $sucursalAgencia->where('id_empresa',$id_empresa);

			$iDtipoPersona = $this->verAgenciaEmpresa($persona->id_tipo_persona);

			if($iDtipoPersona == 'empresa'){
				//echo "string";
				$sucursalAgencia = $sucursalAgencia->where('id_tipo_persona',$this->idSucursalEmpresa);
			}

			if($iDtipoPersona == 'agencia' ){
				$sucursalAgencia = $sucursalAgencia->where('id_tipo_persona',$this->idSucursalAgencia);
			}

			$sucursalAgencia = $sucursalAgencia->get();

		}

		$tipoFacturacion = TipoFacturacion::get();

		$tipoPersoneria = TipoPersoneria::where('activo', true)->get();
		$agente = Persona::whereIn('id_tipo_persona',array(2,3,4,5,7,6))
							->where('id_empresa',$id_empresa)
							->where('activo',true)
							->get();

		$sucursal_contable = CentroCosto::where('activo','true')->where('id_empresa',$id_empresa)->get();					

		$bancos = BancoPlaza::where('activo','S')/*->where('id_empresa',$this->getIdEmpresa())->where('cuenta_bancaria',true)*/->get();

		return view('pages.mc.persona.edit')->with(['persona'=>$persona,
													'tipoPersona'=>$tipoPersona, 
													'tipoIdentidad'=>$tipoIdentidad, 
													'sucursalAgencias'=>$sucursalAgencia,
													'pais'=>$pais,
													'ciudades'=>$ciudad,
													'sucursalCartera'=>$sucursalCartera,
													'agencias'=>$agencia, 
													'tipoFacturacion'=>$tipoFacturacion, 
													'tipoPersoneria'=>$tipoPersoneria, 
													'agente'=>$agente,
													'plazoCobro'=>$plazoCobro,
													'idPersona'=>$id,
													'fecha'=>$fecha,
													'plazosPago'=>$plazosPago,
													'monto'=>$monto,
													'sucursal_contable'=>$sucursal_contable,
													'permiso_doc'=>$permiso_doc,
													'empresas' => $empresas,
													'empresa_persona'=>$empresa_persona,
													'idEmpresa'=>$id_empresa,
													'bancos'=>$bancos,
													'ciudad'=>$persona->id_ciudad,
													'tipoProveedores'=>$tipoProveedores,
													'cambiar_tipo_persona' => $cambiar_tipo_persona
													]);
	}

	/**
	 * Para guardar los cambios en el formulario de editar
	 * @param  Request $req [description]
	 * @return [type]       [description]
	 */
	public function actualizarPersona(Request $req)
	{
		 $idPersona = $req->input('idPersona');
		 $results = 'true';
		 $password  = $req->input('password');
		 $err = 0;
		 $update = '';
		 $e = '';
		 $data_ep = [];
		 $pasosCompletados = [];

		 $date = $req->input('fechaNacimiento');
		 if($date != ''){
		 	$date = $this->formatoFechaEntrada($date);
		 }else {
		 	$date = NULL;
		 }

		 $idTipoPersona = $req->input('tipoPersona');
		
		$datosPersona = [
		"activo"					=> ($req->input('activoPersona') != '' ) ? $req->input('activoPersona') : true,
		"nombre"					=> $this->validar(strtoupper($req->input('nombres'))),
		"apellido"					=> $this->validar(strtoupper($req->input('apellido'))),
		"denominacion_comercial" 	=> $this->validar(strtoupper($req->input('denominacionComercial'))),
		"usuario" 					=> $this->validar(strtolower($req->input('correo'))),
		"email" 					=> $this->validar(strtolower($req->input('correo'))),
		"notificacion_email" 		=> $this->validar($req->input('notificacionCorreo')), //true or false
		"id_sucursal_empresa" 		=> $this->validar($req->input('sucursalEmpresa')),
		"celular" 					=> $this->validar($req->input('celular')),
		"id_sucursal_cartera"       => $this->validar($req->input('sucursalCarteraId')),
		"fecha_nacimiento" 			=>  $date,//date
		"telefono" 					=> $this->validar($req->input('telefono')),
		"logo" 						=> ($req->input('nombreImagen') !='') ? $req->input('nombreImagen') : 'factour.png', //direccion de la imagen a subir
		"id_personeria" 			=> $this->validar($req->input('tipoPersoneria')), ////se asocia id de tabla tipo_personeria
		"id_tipo_facturacion" 		=> $this->validar($req->input('tipoFacturacion')),
		"id_pais" 					=> $this->validar($req->input('pais')),
		"id_persona" 				=> $this->validar($req->input('empresa')), //asocia una persona de tipo agencia
		"iva_de_comisiones" 		=> $this->validar($req->input('ivaComisiones')),//true or false
		"direccion"					=> $this->validar($req->input('direccion')),
		"id_ciudad" 				=> $this->validar($req->input('ciudad')),
		// "iva_redondeo" 				=> $this->validar($req->input('redondeo')), //true or false
			"id_plazo_pago"				=> $this->validar($req->input('plazoPago')),
			"id_plazo_cobro"				=> $this->validar($req->input('plazoCobro')),
			"anular_factura"			=> $this->validar($req->input('anularFactura')), //true or false
		"reimprimir_factura_original" 	=> $this->validar($req->input('reimprimirFactura')), //true or false
			"crear_nota_credito"	    => $this->validar($req->input('notaCredito')), //true or false
			"updated_at"				=> date('Y-m-d H:i:00'),
			"activo_dtpmundo"		    => $this->validar($req->input('activoDtpMundo')),
			"correo_administrativo"     => $this->validar($req->input('correo_administrativo_array')),
			"correo_comerciales"        => $this->validar($req->input('correo_comercial_array')),
			"usuario_amadeus"           => $this->validar($req->input('CodigoAmadeus')),
			"proveedor_online"          => $this->validar($req->input('activoProveedorOnline')),
			"id_usuario"				=> $this->getIdUsuario(),
			"migrar"					=> $this->validar($req->input('migrar')),
			"imprimir_voucher"			=> $this->validar($req->input('voucher')),
			"puede_reservar"			=> $this->validar($req->input('puedeReservar')),
			"puede_reservar_en_gastos"	=> $this->validar($req->input('puedeReservarEnGastos')),
			"tipo_proveedor"			=> $req->input('tipoProveedor'),
			"id_banco_cabecera"			=> $this->validar($req->input('banco')),
			"nro_cuenta"				=> $this->validar($req->input('nroCuenta')),
				"acuerdos_especiales"	=> $this->validar($req->input('acuerdo')),
	"facturacion_automatica_en_gastos"  => $this->validar($req->input('facturacionAutomaticaEnGastos')),
					"cheque_emisor_txt" => $this->validar($req->input('emisorCheque')),
					"agente_retentor"   => $this->validar($req->input('agenteRetentor')),
					   "retener_iva"    => $req->input('retenerIva'),
					   			"dv"    => $req->input('dv'),
					   "prefijo_nemo"   => 14,
					   "token_nemo"     => $req->input('token_nemo'),
					   "cod_nemo"       => $req->input('codigoNemo'),
					   "cod_cangoroo"   => $this->validar($req->input('codigoCangoro')),
					   "usuario_sabre"  => $this->validar($req->input('usuario_sabre')),
					   "usuario_amadeus"=> $this->validar($req->input('usuario_amadeus')),
					   "cod_copa" 		=> $this->validar($req->input('cod_copa'))
			];

			//PERMISO PARA EDITAR TIP PERSONA
		   $permiso_tp = DB::table('persona_permiso_especiales')->where('id_persona',$this->getIdUsuario())->where('id_permiso',68)->count();
		   if($permiso_tp){
			   $datosPersona['id_tipo_persona'] = $this->validar($req->input('tipoPersona'));
		   }

			//VERIFICAR PERMISO PARA EDITAR DOCUMENTO DE IDENTIDAD
			$permiso_doc = DB::table('persona_permiso_especiales')->where('id_persona',$this->getIdUsuario())->where('id_permiso',21)->count();
			if($permiso_doc > 0){
				$datosPersona["documento_identidad"] =  str_replace(array(' ','.','/'),'',$this->validar($req->input('documentoIdentidad')));
				$datosPersona["id_tipo_identidad"] = $this->validar($req->input('tipoDocumento')); //se asocia id de tabla tipo_documento
			} 

		/* echo '<pre>';	
		 print_r($datosPersona);
		 die;*/

			//VALIDAR CORREO ANTES DE ACTIVAR UNA PERSONA
			$activoPersona  = ($req->input('activoPersona') != '' ) ? $req->input('activoPersona') : true;
			$activo_dtpmundo = $req->input('activoDtpMundo');
			//Validar que el usuario a actualizar puede_loguearse = true para verificar
			$puedeLoguearse = DB::select("SELECT tp.puede_loguearse
							FROM personas p
							LEFT JOIN tipo_persona tp ON tp.id = p.id_tipo_persona
							WHERE p.id = ". $idPersona);
			$puedeLoguearse = $puedeLoguearse[0]->puede_loguearse;
			// echo $activo_dtpmundo;
			/*if(($activoPersona == '1' || $activo_dtpmundo == '1') && $puedeLoguearse == 'true'){
			$correo = trim(strtoupper($req->input('correo')));
			$err = DB::select("SELECT count(p.*) as c 
							FROM personas p
							LEFT JOIN tipo_persona tp ON tp.id = p.id_tipo_persona
							WHERE UPPER(email) = '".$correo."' 
							AND tp.puede_loguearse = true
							AND p.activo = true 
							AND p.id !=".$idPersona);

			$err = $err[0]->c;
			if($err > 0){
				$results = 'El correo principal ya existe y esta activo.';
			*/
			//}


		if($err == 0){
			try{
				DB::beginTransaction();

				if($req->input('agente') != NULL && $req->input('agente') != ''){
					$datosPersona['id_vendedor_empresa'] = $req->input('agente');
				}

				$password = trim($password);
				$passActual = Persona::where('id', $idPersona)->get(['password']);

				if($passActual[0]->password != $password){
					DB::select('select get_password_hash(?,?)', [$password,$idPersona]);
					$pasosCompletados[] = ['UPDATE PASSWORD OK'];
				}
				
				//ACTUALIZAR PERSONAS
				$update = DB::table('personas')
				->where('id',$idPersona)
				->update($datosPersona);
				$pasosCompletados[] = ['UPDATE PERSONA OK'];
				
				//TIPO PERSONA SUCURSAL EMPRESA 
				//ACTUALIZAR SUCURSAL
				if($idTipoPersona  == '21'){
					$sucursalEmpresa = [
						'activo'=> $activoPersona,
						'denominacion' => $this->validar(strtoupper($req->input('denominacionComercial'))),
						'id_sucursal_contable' => $this->validar($req->input('id_sucursal_contable'))
					];

					$sucursal_contable = Persona::where('id',$idPersona)->first();

					DB::table('sucursales_empresa')
					->where('id',$sucursal_contable->id_sucursal_contable)
					->update($sucursalEmpresa);
					$pasosCompletados[] = ['UPDATE SUCURSAL PERSONA OK'];
				}
				
				if($req->input('data_empresa')){
					//CREAR ARRAY EMPRESA PERSONA
					foreach($req->input('data_empresa') as $value){
						$empresa_id = Empresa::where('id_persona', $value['empresa_id'])->get(['id']);
						$data_ep[] = [
									'id_persona' => $req->input('idPersona'),
									'id_empresa' => $empresa_id[0]->id,
									'id_sucursal_empresa'=> $value['sucursal_id'],
									'created' => date('Y-m-d H:i:00'),
									'activo' => 'true'];
					}
					// dd($data_ep);
					//ACTUALIZAR PERSONA EMPRESA
					EmpresaPersona::where('id_persona',$req->input('idPersona'))->delete();
					DB::table('empresa_persona')->insert($data_ep);
					$pasosCompletados[] = ['INSERT EMPRESA PERSONA OK'];
				}
				

				DB::commit();
				// DB::rollBack();
                $pasosCompletados[] = ['COMPLETADO'];

				} catch(\Exception $e){
					Log::error($e);
					if(env('APP_DEBUG')){
					$e .= $e;
					}
					$results = "No se realizaron las modificaciones en el registro ";
					$pasosCompletados[] = ['ROLLBACK'];
					DB::rollBack();
				}
				
		}

			return response()->json(array('dato'=>$results,'pasos_debug'=>$pasosCompletados, 'userId'=>$update,'e'=>$e));
			
			


	}//function



	public function getDatosAgencia(Request $req){

		$id = $req->idAgencia;
		$flag = $req->flag;
		$id_empresa = $this->getIdEmpresa();

		if($flag == 1){

			 $personas = Persona::query();
            $personas = $personas->with('tipoPersona','agencia');
            $personas = $personas->where('id_tipo_persona','10');

			if($id != ''){
				$personas = $personas->where('id_persona',$id);
			}
		
		$personas = $personas->where('id_empresa',$id_empresa);
		$personas = $personas->orderBy('nombre', 'ASC')->get();

				
			return response()->json(['resp'=>$personas]);

		}else {

			$personas = Persona::query();
            $personas = $personas->with('tipoPersona','agencia');

			if($id != ''){
				$personas = $personas->where('id_persona',$id);
			}
		
			$personas = $personas->where('id_empresa',$id_empresa);
			$personas = $personas->orderBy('nombre', 'ASC')->get();
	
			return response()->json(['resp'=>$personas]);

		}
   

	}//function


		public function getDatosVendedores(Request $req)
		{

			$id = $req->idAgencia;
			$id_empresa = $this->getIdEmpresa();

			

				if($id != ''){
					$personas = Persona::with('tipoPersona','agencia')
					->where('id_persona',$id)
					->where('id_tipo_persona','10')
					->where('id_empresa',$id_empresa)
					->orderBy('nombre', 'ASC')->get();

				} else {
					$personas = Persona::with('tipoPersona','agencia')
					->where('id_empresa',$id_empresa)
					->where('id_tipo_persona','10')
					->orderBy('nombre', 'ASC')->get();

				}
						
				return response()->json(['resp'=>$personas]);
		}

		public function getDatoPersonaEstado(Request $req)
		{

			$estado = $req->idEstado;
			$idEmpresa = $req->idEmpresa;
			$id_empresa = $this->getIdEmpresa();
			$idPersona = $req->id_persona;

			$personas = DB::table('v_personas_combo');

			$personas = $personas->where('id_empresa',$this->getIdEmpresa());
					
			if($estado != ''){
				$personas = $personas->where('activo',$estado);
			}
			if($idEmpresa){
				$personas = $personas->where('id_persona',$idEmpresa);
			}
			if($idPersona != ''){
				$personas = $personas->where('id',$idPersona);
			}
			$personas = $personas->orderBy('nombre', 'ASC');  
			$personas = $personas->get();

			return response()->json(['resp'=>$personas]);

		}

	/**
	 * Agregar Persona, recibe los datos del formulario
	 * @param Request $req [description]
	 */
	public function add(Request $req)
	{
		ini_set('max_execution_time', 35); 
		 $date = $req->input('fechaNacimiento');
		 $PersonaTipo = $req->input('tipoPersona');
		 $pasosCompletados = [];
		 $idPersonaAdd = 0;
		
		 if($date != '' && $date != NULL){
		 	
		 	$fecha = $this->formatoFechaEntrada($date);
		 } else {
		 	$fecha = Null;
		 }

		 $persona = new Persona;
		 $results = 'true';
		// //Campo vista
			   $persona->id_tipo_persona = $this->validar($req->input('tipoPersona'));
			   			$persona->activo = ($req->input('activoPersona') != '' ) ? $req->input('activoPersona') : true;
						$persona->nombre = $this->validar(strtoupper($req->input('nombres')));
					  $persona->apellido = $this->validar(strtoupper($req->input('apellido')));
				$persona->denominacion_comercial = $this->validar(strtoupper($req->input('denominacionComercial')));
							$persona->usuario = $this->validar(strtolower($req->input('correo')));
					$persona->usuario_amadeus = $this->validar($req->input('CodigoAmadeus'));
					$persona->id_plazo_pago   = $this->validar($req->input('plazoPago'));
					$persona->id_plazo_cobro   = $this->validar($req->input('plazoCobro'));
				$persona->id_sucursal_cartera = $this->validar($req->input('sucursalCarteraId'));
							// $persona->password = null;
								$persona->email = $this->validar(strtolower($req->input('correo')));
					$persona->notificacion_email = (!empty($req->input('notificacionCorreo'))) ? $req->input('notificacionCorreo') : 'false' ; //true or false
							$persona->id_perfil = $this->validar($req->input('rolesPerfil')); //rol del usuario
				$persona->id_sucursal_empresa = $this->validar($req->input('sucursalEmpresa'));
				$persona->id_vendedor_empresa = $this->validar($req->input('agente'));
							$persona->celular = $this->validar($req->input('celular')); 
							$persona->dv = $this->validar($req->input('dv')); 
					$persona->fecha_nacimiento = $fecha; //date
				$persona->documento_identidad = str_replace(array(' ','.','/'),'',$this->validar($req->input('documentoIdentidad')));
							$persona->telefono = $this->validar($req->input('telefono'));
								$persona->logo = ($req->input('nombreImagen') !='') ? $req->input('nombreImagen') : 'factour.png'; //direccion de la imagen a subir
						$persona->id_personeria = $this->validar($req->input('tipoPersoneria')); ////se asocia id de tabla tipo_personeria
					$persona->id_tipo_identidad = $this->validar($req->input('tipoDocumento')); //se asocia id de tabla tipo_documento
					// $persona->id_linea_credito = null ;//$this->validar($req->input('lineaCredito')); //Se guarda en tabla linea credito y el id se asocia con update
				$persona->id_tipo_facturacion = $this->validar($req->input('tipoFacturacion'));
							$persona->id_pais = $this->validar($req->input('pais'));
						if($req->input('tipoPersona') == 2){
							$denominacionEmpresa = Empresa::where('id', $req->input('empresa'))->get(['id_persona']);	
							$persona->id_persona = $denominacionEmpresa[0]->id_persona;
						}else{
							$persona->id_persona = $this->validar($req->input('empresa')); //asocia una persona de tipo agencia
						}

					$persona->iva_de_comisiones = $this->validar($req->input('ivaComisiones'));//true or false
							$persona->direccion = $this->validar($req->input('direccion'));
							$persona->id_ciudad = $this->validar($req->input('ciudad'));
								$persona->migrar = $this->validar($req->input('migrar'));
					$persona->imprimir_voucher = $this->validar($req->input('voucher'));
						// $persona->iva_redondeo = $this->validar($req->input('redondeo')); //true or false

						//OBTENER EL ID EMPRESA POR LA RELACION DE LA PESONA EMPRESA
						$denominacionEmpresa = Empresa::where('id_persona', $req->input('empresa'))->get(['id']);	

							    if($req->input('empresa') != ''){ 	
									if($req->input('tipoPersona') == 2){
										if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 1){
											$persona->id_empresa = $req->input('empresa');	
										}else{
											$denominacionEmpresa = Empresa::where('id', $req->input('empresa'))->get(['id']);	
											$persona->id_empresa = $this->getIdEmpresa();
										}
									}else{
										$persona->id_empresa = $this->getIdEmpresa();
									}
								}else{
									$persona->id_empresa = $this->getIdEmpresa();
								}
								//die;
								$persona->activo = true; //por default
							$fechaActual = (date('Y-m-d H:i:00'));
							$persona->fecha_alta = $fechaActual;
						$persona->fecha_validez = date('Y-m-d H:i:s',strtotime($fechaActual."1 year"));
					$persona->activo_dtpmundo = $this->validar($req->input('activoDtpMundo'));
					//die;
		// $persona->reimprimir_factura_original = $this->validar($req->input('reimprimirFactura')); //true or false
						// $persona->anular_factura = $this->validar($req->input('anularFactura')); //true or false
					// $persona->crear_nota_credito = $this->validar($req->input('notaCredito')); //true or false
				$persona->correo_administrativo  = $this->validar($req->input('correo_administrativo_array'));
				$persona->correo_comerciales     = $this->validar($req->input('correo_comercial_array'));
					$persona->proveedor_online  = $this->validar($req->input('activoProveedorOnline'));
				$persona->cod_cliente_factour = '0';
					$persona->saldo_disponible = '0.00'; //No se muestra en index
							$persona->created_at = date('Y-m-d H:i:00');
							$persona->updated_at = date('Y-m-d H:i:00');
							$persona->id_usuario = $this->getIdUsuario();
						$persona->puede_reservar = $this->validar($req->input('puedeReservar'));
			$persona->puede_reservar_en_gastos = $this->validar($req->input('puedeReservarEnGastos'));
		$persona->facturacion_automatica_en_gastos = $this->validar($req->input('facturacionAutomaticaEnGastos'));
				$persona->acuerdos_especiales = $this->validar($req->input('acuerdo'));
				 $persona->cheque_emisor_txt = $this->validar($req->input('emisorCheque'));
				 $persona->agente_retentor = $this->validar($req->input('agenteRetentor'));
				 $persona->id_banco_cabecera = $this->validar($req->input('banco'));
				 $persona->nro_cuenta = $this->validar($req->input('nroCuenta'));
				 $persona->activo_dtpmundo = true;
				 $persona->retener_iva = $req->input('retenerIva');
				 $persona->token_nemo = $req->input('token_nemo');
				 $persona->tipo_proveedor =$req->input('tipoProveedor');
				 $persona->cod_nemo = $req->input('codigoNemo');
				 $persona->cod_cangoroo = $this->validar($req->input('codigoCangoro'));
				 $persona->usuario_sabre =$req->input('usuario_sabre');
				 $persona->usuario_amadeus =$req->input('usuario_amadeus');
				 $persona->cod_copa =$req->input('cod_copa');

	 	try{
			 DB::beginTransaction();
			 $persona->save();
			 $idPersonaAdd = $persona->id;
			 $pasosCompletados[] = ['SAVE PERSONA OK'];

			 //TIPO PERSONA EMPRESA
 			 if($PersonaTipo == '20'){
			 	$empresa = new Empresa;
			 	$empresa->denominacion = $this->validar(strtoupper($req->input('denominacionComercial')));
			 	$empresa->logo = ($req->input('nombreImagen') !='') ? $req->input('nombreImagen') : 'factour.png';
			 	$empresa->telefono = $this->validar($req->input('telefono'));
			 	$empresa->direccion = $this->validar($req->input('direccion'));
			 	$empresa->ruc =  str_replace(array(' ','.','/'),'',$this->validar($req->input('documentoIdentidad')));
			 	$empresa->email = $this->validar(strtolower($req->input('correo')));
			 	$empresa->id_persona = $idPersonaAdd;
			 	$empresa->activo = true;
			 	$empresa->save();
			 	$idEmpresaAdd = $empresa->id;
			 	DB::table('personas')
					->where('id',$idPersonaAdd)
					->update(['id_empresa'=>$idEmpresaAdd]);

			$pasosCompletados[] = ['SAVE EMPRESA OK'];	

			 } else if($PersonaTipo == '21'){
				 //TIPO PERSONA SUCURSAL EMPRESA
				 $sucursalEmpresa = new SucursalEmpresa;
				 $sucursalEmpresa->activo = true;
				 $sucursalEmpresa->denominacion =  $this->validar(strtoupper($req->input('denominacionComercial')));
				 $sucursalEmpresa->id_empresa = $req->input('empresa'); 
				 $sucursalEmpresa->id_sucursal_contable = $this->validar($req->input('id_sucursal_contable'));
				 $sucursalEmpresa->save();
				 $idSucursalEmpresaAdd = $sucursalEmpresa->id;

				 $pasosCompletados[] = ['SAVE SUCURSAL OK'];		
				 DB::table('personas')
				 ->where('id',$idPersonaAdd)
				 ->update([
				 			'id_sucursal_contable'=>$idSucursalEmpresaAdd,
				 			'id_empresa'=>$req->input('empresa')
				 			]);

				 $pasosCompletados[] = ['UPDATE SUCURSAL OK'];	
			 }

			if($req->input('data_empresa')){
				foreach($req->input('data_empresa') as $value){
					// dd($value);
					$empresa_id = Empresa::where('id_persona', $value['empresa_id'])->get(['id']);
					$data_ep[] = [
								'id_persona' => $idPersonaAdd,
								'id_empresa' => $req->input('empresa'),
								'id_sucursal_empresa'=> $value['sucursal_id'],
								'created' => date('Y-m-d H:i:00'),
								'activo' => 'true'];
				}
					//INSERT EMPRESA PERSONA
					DB::table('empresa_persona')->insert($data_ep);
					$pasosCompletados[] = ['SAVE EMPRESA PERSONA OK'];
			}
	
			 

			

			 //CREAR PASSWORD
			 $password = ($req->input('password') != '' && $req->input('password') != NULL) ? $req->input('password') : false;
			if($password){
					DB::select('select get_password_hash(?,?)', [$password,$idPersonaAdd]);
					$pasosCompletados[] = ['NEW PASSWORD OK'];
			}

			if($PersonaTipo == '8' && $req->input('lineaCredito') != ''){

				//Id linea credito
				 $lineaCredito = new LineaCredito;
				$lineaCredito->monto = (double)str_replace(array(',','.'), '', $this->validar($req->input('lineaCredito')));
				$lineaCredito->id_moneda = 143; //Dolar
				$lineaCredito->id_persona = $idPersonaAdd; 
				$lineaCredito->id_empresa = $denominacionEmpresa[0]->id; //Concepto multi Empresa
				$lineaCredito->activo = true;
				$lineaCredito->id_estado = $this->idEstadoCredito; //Estado Activo
				$lineaCredito->fecha = date('Y-m-d');
				$lineaCredito->id_usuario = $this->getIdUsuario();//Usuario que utiliza el sistema
				$lineaCredito->save();
				$lineaCreditoAdd = $lineaCredito->id;
				$pasosCompletados[] = ['SAVE LINEA CREDITO OK'];

					DB::table('personas')
					->where('id',$idPersonaAdd)
					->update(['id_linea_credito'=>$lineaCreditoAdd,
							 'id_usuario'=>$this->getIdUsuario(),
							 'updated_at'=> date('Y-m-d H:i:00')]);

					$pasosCompletados[] = ['UPDATE LINEA CREDITO OK'];		 
		
				}

			
			 DB::commit();
			//  DB::rollBack(); 
			 $pasosCompletados[] = ['COMPLETADO'];

	  } catch(\Exception $e){
			//echo json_encode($e);
			Log::error($e);
			$results = 'Personas';
			$pasosCompletados[] = ['ROLLBACK'];
			DB::rollBack(); 
		} 


		return response()->json(['dato'=>$results,'userId'=> $idPersonaAdd,'complete_debug'=>$pasosCompletados]);
			

	}


	/**
	 * Poblar formulario de PersonaAdd
	 * @return [type] [description]
	 */
	public function getDatosPersona(){
		
		//flash('Se ha editado el paquete exitosamente')->success();
		$id_empresa = $this->getIdEmpresa();
		$empresasPlan = Empresa::where('id',$this->getIdEmpresa())->first(['id_plan_sistema','id_persona']);
		$empPlan = $empresasPlan->id_plan_sistema;
		$personaEmpresa = $empresasPlan->id_persona;

		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 1){
			$tipoPersona = TipoPersona::orderBy('denominacion', 'ASC')->where('activo', true)->get();
		}else{
			$tipoPersona = TipoPersona::orderBy('denominacion', 'ASC')->whereNotIn('id', array(20, 2, 1))->where('activo', true)->get();
		}

		$tipoProveedores = TipoProveedor::whereIn('id', array(1,2))->get();

		$sucursalCartera = Persona::where('activo',true)->where('id_tipo_persona','21')->where('id_empresa',$this->getIdEmpresa())->get();

		$tipoIdentidad = TipoIdentidad::where('activo', true)->get();

		$pais = Pais::where('habilitado', true)->get();

		$plazosPago = PlazoPago::where('id_empresa',$this->idEmpresa)
					  ->get();

		$agencia = Persona::where('id_tipo_persona',$this->idAgencia)
								->where('id_empresa',$id_empresa)
								->get();
		$plazoCobro = PlazoCobro::all();
		$tipoFacturacion = TipoFacturacion::get();
		$tipoPersoneria = TipoPersoneria::where('activo', true)->get();

		$agenteDtp = Persona::whereIn('id_tipo_persona',array(2,3,4,5,7,6))
							->where('id_empresa',$id_empresa)
							->where('activo',true)
							->get();

		$bancos = BancoPlaza::where('activo','S')/*->where('id_empresa',$this->getIdEmpresa())->where('cuenta_bancaria',true)*/->get();

		$sucursal_contable = CentroCosto::where('activo','true')->where('id_empresa',$id_empresa)->get();			
		$empresas = Persona::where('activo',true)->where('id_tipo_persona','20')->where('id', $personaEmpresa)->get(); //EMPRESAS	

		return view('pages.mc.persona.add')->with(['tipoPersona'=>$tipoPersona, 
												   'tipoIdentidad'=>$tipoIdentidad, 
												   'pais'=>$pais,
												   'agencia'=>$agencia, 
												   'tipoFacturacion'=>$tipoFacturacion, 
												   'tipoPersoneria'=>$tipoPersoneria, 
												   'agente'=>$agenteDtp,
												   'plazosPago'=>$plazosPago,
												   'plazoCobro'=>$plazoCobro,
												   'sucursalCartera'=>$sucursalCartera,
												   'sucursal_contable'=>$sucursal_contable,
												   'empresas'=>$empresas,
												   'idEmpresa'=>$id_empresa,
												   'empresasPlan'=>$empPlan,
												   'bancos'=>$bancos,
												   'tipoProveedores'=>$tipoProveedores
												   ]);
	}

	/**
	 * Sirve para obtener la ciudad de un pais seleccionado en un select pais
	 * @param  Request $req [description]
	 * @return [type]       [description]
	 */
	public function obtenerCiudad(Request $req){

		$idPais = $req->idPais;
		$ciudades = [];
		if($idPais != ""){
			$ciudades = Ciudad::where('id_pais',$idPais)->get();
		}
		return response()->json(array('ciudades'=>$ciudades));
	}


	/**
	 * Devuelve las Sucursales de la agencia seleccionada en el select agencia
	 * @param  Request $req [description]
	 * @return [type]       [description]
	 */
	public function obtenerSucursalAgencia(Request $req){
		$id_empresa = $req->input('idEmpresa');
		$tipoSucursal = $req->input('tipoSucursal');
		$idAgenciaEmpresa = $req->input('idAgencia');
		$sucursal= '';
		//Estirar Sucursal del tipo agencia
		if($tipoSucursal == '9'){
			$id_empresa = $this->getIdEmpresa(); 
			$sucursal = Persona::with('sucursalAgencia')
									->where('activo',true)
									->where('id_empresa',$id_empresa)
									->where('id_persona',$idAgenciaEmpresa)
									->where('id_tipo_persona',$this->idSucursalAgencia)
									->get();
		} 
		//Estirar Sucursal del tipo Empresa
		if($tipoSucursal == '21'){
			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 1){
				$sucursal = Persona::with('sucursalAgencia')
										->where('activo',true)
										->where('id_empresa',$id_empresa)
										->where('id_tipo_persona',$tipoSucursal)
										->get();
			}else{
				$id_empresa = $this->getIdEmpresa(); 
				$sucursal = Persona::with('sucursalAgencia')
										->where('activo',true)
										->where('id_empresa',$id_empresa)
										->where('id_tipo_persona',$this->idSucursalEmpresa)
										->get();
			}

		}

		return response()->json(['sucursalAgencia'=>$sucursal]);

	}


	public function obtenerEmpresaAgencia(Request $req){

		$id_empresa = $this->getIdEmpresa(); 
		$tipoPersona = $req->id;
		$personaAgencia = '8';
		$personaEmpresa = 20; 
		$personas = array();
		//Vendedor agencia y Sucursal Agencia
		if($tipoPersona == '1'){
			$id_Empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil;
			$personas = Persona::where('id_empresa',$id_empresa)->where('id_tipo_persona',$personaAgencia)->where('activo',true)->get();
		}

		//vendedor empresa y Sucursal Empresa
		if($tipoPersona == 2){
			/*$personas = Persona::where('id_empresa',$id_empresa)->where('id_tipo_persona',$personaEmpresa)->where('activo',true)->get();*/
			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2 ){
				$empresa = Empresa::where('id',$id_empresa)->get();
				$personas = [];
				foreach($empresa as $key=>$value){
					$personas[$key]['id'] = $value->id_persona;
					$personas[$key]['nombre'] = $value->denominacion;
				}
			}else{
				//TODO: Pendiente mejorar el control aqui, esto es un parche
				// if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 1){
					$empresa = Empresa::all();
					$personas = [];
					foreach($empresa as $key=>$value){
						$personas[$key]['id'] = $value->id;
						$personas[$key]['nombre'] = $value->denominacion;
					}
				// 	}else{
				// 	$personas = Persona::where('id_empresa',$id_empresa)->where('id_tipo_persona',$personaEmpresa)->where('activo',true)->get();
				// }
			}
		}

		if($tipoPersona == '3'){
			$persona = Persona::where('id_empresa',$id_empresa)->where('id_tipo_persona',$personaEmpresa)->where('activo',true)->get();
			$empresa = Empresa::all();
			$personas = [];
			foreach($empresa as $key=>$value){
				$personas[$key]['id'] = $value->id;
				$personas[$key]['nombre'] = $value->denominacion;
			}
		}

		return response()->json(array('response'=>$personas));

	}


	/**
	 * Funcion que llama a la vista cambiar Clave y recibe el ajax para realizar el cambio
	 * @param  Request $req [description]
	 * @return [type]       [description]
	 */
		public function cambiarClave(Request $req){

			if(!empty($req->all()) && !is_null($req->all())){

				$password_anterior = $req->clave_anterior;
				$password_nueva = $req->clave_nueva;
				$idPersona = $this->getIdUsuario();
				$err = 0;
				$ok = true;
			

		$passActual = Persona::where('id', $idPersona)->get(['password']);

           //Obtener hash del password anterior
          try{
            $password_anterior = DB::select('select get_password_hash(?,?)', [$password_anterior,0]);
        } catch(\Exception $e){
			Log::error($e);
           $err++;
           return response()->json(['response'=>false,'err'=>$err]);
           die;
        }


        if($passActual[0]->password == $password_anterior[0]->get_password_hash && $password_anterior != '' && $password_nueva != ''){

        	//Cambio de password
        	 try{
            $password_anterior = DB::select('select get_password_hash(?,?)', [$password_nueva,$idPersona]);
            $ok=true;
        } catch(\Exception $e){
			Log::error($e);
           $err++;
           return response()->json(['response'=>false,'err'=>$err]);
           die;
        }
        	
        } else {
        	$ok = false;
        }
		
        return response()->json(['response'=>$ok,'err'=>$err]);
		} else {
		return view('pages.mc.persona.cambiarClave')->with([]);

		} 


	}//function

	/**
	 * Valida que la cedula de identidad no exista repetido
	 * Pero no aplica para usuarios que sean Administrador de Gestur
	 */
	public function validarCedula(Request $req){
		// dd($req->all());
		 $err = true;
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 1){
			$consultar = DB::select("SELECT COUNT(*) cant FROM personas WHERE activo = true AND documento_identidad = '".$req->documento."' AND id_empresa =".$this->getIdEmpresa());
			$consultar = $consultar[0]->cant;

			if($consultar != 0){
				$err = false;
			}
		 }
		return response()->json(['err'=>$err]);

	}//function


	public function getAjaxAcuerdo(Request $req) {

		$idPersona = $req->input('id_persona');

		$data = Persona::where('id',$idPersona)->first();

		return response()->json(['data'=>$data]);

	}//

	public function getSucursalEmpresaPersona(Request $req)
	{
		$data = Persona::where('activo',true)
				->where('id_persona',$req->input('empresa_id'))
				->where('id_tipo_persona',$this->idSucursalEmpresa)
				->get();

		return response()->json(['data'=>$data]);
	}

	/*
	* ==============================================================================================
	* 									ABM CENTRO COSTO
	* ==============================================================================================
	*/
   
	   public function listadoCentroCosto()
	   {
		   return view('pages.mc.persona.listadoCentroCosto');
	   }
   
	   public function ajaxListadoCentroCosto(Request $req)
	   {
		   $data = new CentroCosto;
		   $data = $data->with('usuario');
		   $data = $data->where('id_empresa',$this->getIdEmpresa());
		   $data = $data->selectRaw("*,to_char(fecha_creacion, 'DD/MM/YYYY') AS fecha_creacion_format");
   
		   if($req->input('activo')){
			   $data = $data->where('activo',$req->input('activo'));
		   }
		   $data = $data->get();
   
		   return response()->json(['data'=>$data]);
	   }
   
	   public function getDataCentroCosto(Request $req)
	   {	
		   $data = CentroCosto::with('usuario')
				   ->where('id',$req->input('id'))
				   ->get();
   
		   return response()->json(['data'=>$data]);
	   }
   
 		public function getVerCliente(Request $req)
	   {	
		   $data = Persona::/*with('usuario')
				   ->*/where('id',$req->input('id'))
				   ->get();
   
		   return response()->json($data);
	   }

	   public function setDataCentroCosto(Request $req)
	   {	
		   $resp = new \StdClass;
		   $resp->err = true;
   
		   // dd($req->all());
   
			   try{
				   $data = new CentroCosto;
   
				   if($req->input('id')){
   
					   $update = [
						   "activo" 	=> $req->input('activo'),
						   "nombre"	    => $req->input('nombre'),
						   "codigo"	    => $req->input('codigo'),
					   ];
   
					   $data = $data->where('id',$req->input('id'));
					   $data = $data->update($update);
   
				   } else {
					   $data->id_usuario = $this->getIdUsuario();
					   $data->id_empresa = $this->getIdEmpresa();
					   $data->fecha_creacion = date('Y-m-d H:i:00');
					   $data->nombre = $req->input('nombre');
					   $data->codigo = $req->input('codigo');
					   $data->activo = true;
					   $data->save();	
				   }
   
			   } catch(\Exception $e){
				   $resp->err = false;
				   if(env('APP_DEBUG')){
					   $resp->e = $e;
				   }
			   }
   
			return response()->json($resp);
	   }
	   public function getGuardarCliente(Request $req){
			$resp = new \StdClass; 
			//try{
				$data = new Persona;
				$data->id_empresa = $this->getIdEmpresa();
				$data->fecha_alta = date('Y-m-d H:i:00');
				$data->nombre = $req->input('nombre');
				$data->apellido = $req->input('apellido');
				$data->id_tipo_identidad = $req->input('tipo_identidad');
				$data->documento_identidad = $req->input('documento');
				$data->telefono = $req->input('telefono');
				$data->direccion = $req->input('direccion');
				$data->logo = 'factour.png';
				$data->id_pais = 1;
				$data->id_tipo_persona = 11;
				$data->id_tipo_facturacion = 2;
				$data->activo = true;
				$data->save();	
				$resp->status = 'Ok';
				$resp->id = $data->id;
				$resp->nombre = $req->input('nombre');
				$resp->apellido = $req->input('apellido');
		/*	} catch(\Exception $e){
				$resp->status = 'Error';
				if(env('APP_DEBUG')){
					$resp->e = $e;
				}
			}*/
			return response()->json($resp);
	   }

	   public function listadoCliente(Request $req){
		ini_set('memory_limit', '-1');
		set_time_limit(300);

		$ajax = 0;
		$id_empresa = $this->getIdEmpresa();
		if($id_empresa == 1 || $id_empresa == 4){
			$tipoPersonas = TipoPersona::where('puede_facturar',true)->orderBy('denominacion', 'ASC')->get();
		}else{
			$tipoPersonas = TipoPersona::where('ver_agencia','false')->where('puede_facturar',true)->orderBy('denominacion', 'ASC')->get();
		}
		$personas = DB::select("
							SELECT p.id,p.nombre,p.dv, p.apellido, p.documento_identidad, tp.denominacion, p.activo,
							concat(p.nombre||' ',p.apellido,' ','- '|| tp.denominacion,' ','- '|| p.id) as full_data
							FROM personas p
							JOIN tipo_persona tp on tp.id = p.id_tipo_persona
							WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
							AND p.activo = true
							AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

		$empresaAgencia = Persona::whereIn('id_tipo_persona',[8,20])->where('id_empresa',$id_empresa)->orderBy('nombre', 'ASC')->get();


		if(!empty($req->all()) && !is_null($req->all())){

			$ajax++;
			$formSearch = $req->formSearch;
			$draw = intval($req->draw);
			$start = intval($req->start);
			$length = intval($req->length);


            //ORDENAMIENTO POR COLUMNA
            $columna = $req->order[0]['column'];
            $orden = $req->order[0]['dir'];

   

		$cont = 0;
		$filtrarMax = 0;

		$idPersona = $formSearch[0]['value'];
		$idTipoPersona = $formSearch[1]['value'];
		$inputCod = $formSearch[2]['value'];
		$idEmpresaAgencia  = $formSearch[3]['value'];
		$cedulaRuc =  trim($formSearch[4]['value']);
		$email = trim($formSearch[5]['value']);

		$query = " SELECT *
				   FROM vw_clientes_empresa
				   WHERE id_empresa = ".$this->getIdEmpresa();
		
		if($idTipoPersona != ''){	

			$query .= 'AND id_tipo_persona = '.intval($idTipoPersona);
		}
		if($idPersona != ''){
			$query .= ' AND id = '.intval($idPersona);

		} 
		if($inputCod != ''){
			$query .= ' AND id = '.$inputCod.' ';
		}

		if($cedulaRuc != ''){
		$query .= " AND documento_identidad LIKE  '%".$cedulaRuc."%' ";
		}
		if($email != ''){
		$email = trim(strtoupper($email));	
		$query .= " AND UPPER(email)  LIKE '%".$email."%' ";

		}
		if($idEmpresaAgencia != ''){
			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 1){
				$query .= ' AND (id_persona = '.intval($idEmpresaAgencia).' OR id = '.intval($idEmpresaAgencia).') ';
			}
		}
		// dd($columna);
			switch ($columna) {
			case '0':
				$query .= 'order by nombreagencia '.$orden;
				break;

				case '1':
				$query .= 'order by nombrepersona '.$orden;
				break;

				case '2':
				$query .= 'order by denominacion '.$orden;
				break;

				case '3':
				$query .= 'order by email '.$orden;
				break;

				case '4':
				$query .= 'order by denominacion_comercial '.$orden;
				break;

				case '5 ':
				$query .= 'order by documento_identidad '.$orden;
				break;
			
			default:
				# code...
				break;
		}

		$personas = DB::select($query);

		/*echo '<pre>';	
		print_r($personas);*/

		$cantidadPersonas = count($personas);
		$lengtArray =$cantidadPersonas;

		$resultPersonas = array();


		// dd($length);
		foreach ($personas as $key => $value) {
			//dd($value);			
			if($cont == $length){
				break;
			}

			if($key >= $start){

					$G = $value->id;
					$ruc =$value->documento_identidad;
					if($value->dv){
						$ruc .= $ruc."-".$value->dv;
					}

					$resultPersonas[] =    [
											$G,
											$value->nombreagencia, 
											$value->nombrepersona.' '.$value->apellidopersona,
											$value->denominacion,
											$value->email,
											$ruc,
										    ];
					$cont++;
			}
		


		}//foreach


		}//IF REQUEST

		
		

		if($ajax > 0){
		return response()->json(['draw'=>$draw,
								 'recordsTotal'=>$lengtArray,
								 'recordsFiltered'=>$lengtArray,
								 'data'=>$resultPersonas]);	
		} else {	
		return view('pages.mc.persona.listadoCliente')->with(['selectPersona'=>$personas,'tipoPersonas'=>$tipoPersonas,'empresaAgencia'=>$empresaAgencia]);
		}
	   }

	public function getPersonaListado(Request $req){
		$destinos = DB::select("SELECT concat(personas.nombre,' ',personas.apellido,' - '||personas.documento_identidad) as pasajero_data, personas.id, tipo_persona.denominacion as tipo_persona
								FROM personas, tipo_persona
								WHERE personas.id_tipo_persona = tipo_persona.id 
								AND personas.activo = true   
								AND personas.id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa
							);
		$dest =  json_encode($destinos);
		$destinos =  json_decode($dest);

		if($req->has('q')){
            $search = $req->q;
				foreach($destinos as $key=>$destino){
					$resultado = strpos(strtolower($destino->pasajero_data), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		return response()->json($destinos);

	}

	public function agregarDv(Request $req){

		$personas = PersonaSet::where('ruc', $req->documento)->get();

		return response()->json($personas);
	}

	public function datosPrestadores(Request $req){
		$personas = DB::select("SELECT id
								FROM personas 
								WHERE nombre = '".$req->input('dataFile')."'
								AND id_tipo_persona = 15
								AND id_empresa = ".$this->getIdEmpresa()."
								AND activo=true");
		if(isset($personas[0])){
			$resultado = false;
		}else{
			$resultado = true;
		}
		return response()->json($resultado);
    }

// para las personas duplicada
	public function personasDuplicada(Request $request){
		$id_empresa = $this->getIdEmpresa();
		$persona = strtoupper($request->input('nombre'));
		$denominacion = strtoupper($request->input('denominacion_comercial'));
		$email = $request->input('correo');
		$ruc = $request->input('ruc');
		$buscar = $request->input('buscar');
		
		//dd($inputPrestador);

		$query = DB::table('personas AS p')
		->select([
			'p.id',
			DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre"),
			'p.denominacion_comercial',
			'p.documento_identidad',
			'p.email',
			DB::raw("CASE WHEN p.activo THEN 'ACTIVO' ELSE 'INACTIVO' END AS estado"),
			DB::raw("COALESCE(facturas.cantidad, 0) AS cantidad_facturas"),
			DB::raw("COALESCE(proformas.cantidad, 0) AS cantidad_proformas"),
			'tipo.denominacion AS tipo_per'
		])
		->leftJoin('tipo_persona AS tipo', 'p.id_tipo_persona', '=', 'tipo.id')
		->leftJoin(DB::raw('(SELECT id_usuario, COUNT(id) AS cantidad FROM facturas GROUP BY id_usuario) AS facturas'), 'p.id', '=', 'facturas.id_usuario')
		->leftJoin(DB::raw('(SELECT id_usuario, COUNT(id) AS cantidad FROM proformas GROUP BY id_usuario) AS proformas'), 'p.id', '=', 'proformas.id_usuario')
    				->where('p.id_empresa', $id_empresa);
				
	
	
			if ($buscar) {
				$query->where(function ($query) use ($buscar) {
					$query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'like', '%' . $buscar . '%')
							->orWhere('p.documento_identidad', 'LIKE', '%' . $ruc . '%');;
				});
			}
			
			
		if ($persona) {
				$query->whereRaw("CONCAT(p.nombre, ' ', p.apellido) LIKE ?", ['%' . $persona . '%']);
		}

		if ($denominacion) {
			$query->where('p.denominacion_comercial', 'LIKE', '%' . $denominacion . '%');
		}

		if ($ruc) {
			$query->where('p.documento_identidad', 'LIKE', '%' . $ruc . '%');
		}

		if ($email) {
			$query->where('p.email', 'LIKE', '%' . $email . '%');
		}
	
		$personas = $query->orderBy('p.nombre', 'desc')->paginate(10);

		return view('pages.mc.persona.persona_duplicada')->with(['personas' => $personas]);
		
	}


	public function personasDuplicadaAjax(Request $request)
{
    $id_empresa = $this->getIdEmpresa();
    $persona = strtoupper($request->input('nombre'));
	$denominacion = strtoupper($request->input('denominacion_comercial'));
    $ruc = $request->input('ruc');
	$email = $request->input('correo');
    $buscar = $request->input('buscar');

    $query = DB::table('personas AS p')
						->select([
							'p.id',
							DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre"),
							'p.denominacion_comercial',
							'p.documento_identidad',
							'p.email',
							DB::raw("CASE WHEN p.activo THEN 'ACTIVO' ELSE 'INACTIVO' END AS estado"),
							DB::raw("COALESCE(facturas.cantidad, 0) AS cantidad_facturas"),
							DB::raw("COALESCE(facturas_clientes.cantidad, 0) AS cantidad_facturas_cliente"),
							DB::raw("COALESCE(proformas.cantidad, 0) AS cantidad_proformas"),
							DB::raw("COALESCE(proformas_clientes.cantidad, 0) AS cantidad_proformas_cliente"),
							'tipo.denominacion AS tipo_per'
						])
			->leftJoin('tipo_persona AS tipo', 'p.id_tipo_persona', '=', 'tipo.id')
			->leftJoin(DB::raw('(SELECT id_usuario, COUNT(id) AS cantidad FROM facturas GROUP BY id_usuario) AS facturas'), 'p.id', '=', 'facturas.id_usuario')
			->leftJoin(DB::raw('(SELECT cliente_id, COUNT(id) AS cantidad FROM facturas GROUP BY cliente_id) AS facturas_clientes'), 'p.id', '=', 'facturas_clientes.cliente_id')
			->leftJoin(DB::raw('(SELECT id_usuario, COUNT(id) AS cantidad FROM proformas GROUP BY id_usuario) AS proformas'), 'p.id', '=', 'proformas.id_usuario')
			->leftJoin(DB::raw('(SELECT cliente_id, COUNT(id) AS cantidad FROM proformas GROUP BY cliente_id) AS proformas_clientes'), 'p.id', '=', 'proformas_clientes.cliente_id')
			->where('p.id_empresa', $id_empresa);

    if ($request->input('search.value')) {
        $searchValue = $request->input('search.value');
        $query->where(function ($query) use ($searchValue) {
            $query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'LIKE', '%' . $searchValue . '%')
                ->orWhere('p.documento_identidad', 'LIKE', '%' . $searchValue . '%');
        });
    }

	if ($persona) {
		$query->whereRaw("CONCAT(p.nombre, ' ', p.apellido) LIKE ?", ['%' . $persona . '%']);
	}

	if ($denominacion) {
        $query->where('p.denominacion_comercial', 'LIKE', '%' . $denominacion . '%');
    }

    if ($ruc) {
        $query->where('p.documento_identidad', 'LIKE', '%' . $ruc . '%');
    }

	if ($email) {
		$query->where('p.email', 'LIKE', '%' . $email . '%');
	}
	$query->get();

    $totalRecords = $query->count();

    $draw = $request->input('draw');
    $start = $request->input('start', 0);
    $length = $request->input('length', 10);
    $recordsFiltered = $totalRecords;

    $personas = $query->orderBy('p.id', 'asc')->offset($start)->limit($length)->get();

    $data = [];
    foreach ($personas as $persona) {
        $botones = '<a href="' . route('personasDuplicadaEdit.edit', $persona->id) . '" class="btn btn-warning ml-2"><span class="fa fa-edit"></span></a>';

        $data[] = [
            'id' => $persona->id,
            'nombre' => $persona->nombre,
            'denominacion_comercial' => $persona->denominacion_comercial,
            'documento_identidad' => $persona->documento_identidad,
            'email' => $persona->email,
			'cantidad_facturas' => $persona->cantidad_facturas,
			'cantidad_proformas' => $persona->cantidad_proformas,
			'cantidad_facturas_cliente' => $persona->cantidad_facturas_cliente,
			'cantidad_proformas_cliente' => $persona->cantidad_proformas_cliente,
			'tipo_per' => $persona->tipo_per,
            'estado' => $persona->estado,
            'accion' => $botones,
        ];
    }

    return response()->json([
        'data' => $data,
        'draw' => $draw,
        'recordsTotal' => $totalRecords,
        'recordsFiltered' => $recordsFiltered,
    ]);
}
/*	public function personasDuplicadaEdit(Request $request, $id)
	{
		$persona = Persona::findOrFail($id);
		$persona->activo = false;
		$persona->save();
		return back()->with('success', 'Estado de la persona actualizado exitosamente');
	}*/
	public function personasDuplicadaEdit(Request $request, $id)
	{
    $persona = Persona::findOrFail($id);

    // Cambiar el estado
    $persona->activo = !$persona->activo; // Cambiar el estado opuesto

    $persona->save();

    $estadoActualizado = ($persona->activo) ? 'ACTIVO' : 'INACTIVO';

    return back()->with('success', 'Estado de la persona actualizado exitosamente. Nuevo estado: ' . $estadoActualizado);
	}

	public function masivoCreate()
	{
		
		$id_usuario = $this->getIdUsuario();
		$usuario = Persona::where('id_empresa',1)->find($id_usuario);
		$arr = [28566,89299,469,52163,90269];
		$permiso = false;
		if(isset($usuario->id) && in_array($usuario->id, $arr)){
			$permiso = true;
		}

		if(isset($usuario->id) && $usuario->id_tipo_persona == 1){
			$permiso = true;
		}


		if(!$permiso){
			flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}
		return view('pages.mc.persona.masivo', compact('personas'));
	}

	public function masivoStore(Request $request)
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 1640); // 240 segundos = 4 minutos
	
		$path = $request->file('file')->getRealPath();
		$personas = Excel::load($path)->get();
		
		foreach ($personas as $personaData) {
			$persona = new Persona;
			$persona->id_empresa = $personaData->id_empresa;
			$persona->fecha_alta = date('Y-m-d H:i:00');
			$persona->activo = true;
			$persona->nombre = $personaData->nombre;
			$persona->email = $personaData->email;
			$persona->celular = $personaData->celular;
			$persona->fecha_nacimiento = $personaData->fecha_nacimiento;
			$persona->telefono = $personaData->telefono;
			$persona->id_tipo_persona = $personaData->id_tipo_persona;
			$persona->id_tipo_identidad = $personaData->id_tipo_identidad;
			$persona->id_pais = $personaData->id_pais;
			$persona->direccion = $personaData->direccion;
			$persona->denominacion_comercial = $personaData->denominacion_comercial;
			$persona->apellido = $personaData->apellido;
			$persona->id_ciudad = $personaData->id_ciudad;
			$persona->documento_identidad = $personaData->documento_identidad;
			$persona->id_plazo_pago = $personaData->id_plazo_pago;
			$persona->correo_comerciales = $personaData->correo_comerciales;
			$persona->correo_administrativo = $personaData->correo_administrativo;
			$persona->agente_retentor = $personaData->agente_retentor;
			$persona->cheque_emisor_txt = $personaData->cheque_emisor_txt;
			$persona->retentor = $personaData->retentor;
			$persona->porcentaje_retencion = $personaData->porcentaje_retencion;
			$persona->retener_iva = $personaData->retener_iva;
			$persona->dv = $personaData->dv;
			$persona->aplica_iva = $personaData->aplica_iva;
			$persona->save();
		}
	
			flash('Personas creadas exitosamente')->success();
	
			return back();
		
	}
}
