<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use \App\Dtp\Proveedor;
use \App\Agencias;
use \App\Image;
use Session;
use Redirect;

class AgenciaController extends Controller
{
	/*
	Listado de agencias
	- Muestra el listado de todas las agencias habilitadas
	*/
	public function index(Request $req){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminDtp')){
			$agencia = Agencias::all();	
			return view('pages.agencias.agencias')->with('agencias',$agencia);
		}else{
			flash('No tienes los permisos para acceder a esta dirección')->error();
            return Redirect::back();
		}	
	}	

	/*
	Página de editar las agencias
	*/
	public function edit(Request $request, $id){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == Config::get('constants.adminDtp')){
			$agencias = Agencias::where('id_agencia', '=',$id)->get();
			return view('pages.agencias.agenciasEdit')->with('agencias',$agencias);
		}else{
			flash('No tienes los permisos para acceder a esta dirección')->error();
            return Redirect::back();
		}		
	}	


	/*
	Editar
	- Función de editar los datos de agencias
	*/
	public function doEdit(Request $request){
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json' ]
		]);

		$iibReq = new \StdClass;
		$iibReq->reqToken = "ertetrytrey"; 
 		$iibReq->ts= date('Y-m-d h:m:s');
 		$agencia = new \StdClass;
 		$agencia->id = (int)$request->input('idAgencia');
 		if(!ctype_space($request->input('razonSocial'))){
 			$agencia->razonSocial = $request->input('razonSocial');
 		}else{
			flash('La razon social esta formada por espacios vacios')->error();
			return Redirect::back();
 		}
 		if($request->input('activo')== true){
 			$activo  = "S";
 		}else{
 			$activo  = "N";
 		}
 		$agencia->email = $request->input('email');
 		$agencia->telefono = $request->input('telefono');
 		$agencia->activo = $activo;
 		$agencia->lineaCredito = $request->input('lineaCredito');
 		$agencia->comision = $request->input('comision');
 		$agencia->logo = $request->input('logo');
 		$iibReq->agencia = $agencia;
 		try{
	 		$iibRsp = $client->post(Config::get('config.iibActAgencia'),
				['body' => json_encode($iibReq)]
			);
 		}
 		catch(RequestException $e){
			return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			return view('pages.errorConexion');	
		}	

		$iibObjRsp = json_decode($iibRsp->getBody());
		//print_r(json_encode($iibObjRsp));

		if($iibObjRsp->codRetorno !=0){
			//TODO: armar template para mensaje de error
			flash($iibObjRsp->desRetorno)->error();
			return Redirect::back();

		}
		flash('Se ha editado la agencia exitosamente')->success();
		return redirect()->route('agencias');
	}	
}