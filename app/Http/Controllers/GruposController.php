<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use \App\DestinoDtpmundo;
use App\Currency;
use App\Aerolinea;
use App\EstadoFactour;
use App\Grupo;
use App\Producto;
use App\Persona;
use App\TipoTicket;
use App\Ticket;
use App\Penalidad;
use App\GruposPago;
use App\GruposSenha;
use App\GrupoAnticipo;
use App\FormaPagoCliente;
use App\Traits\TicketsTrait;
use DB;
use Response;
use Image; 
use Session;
use Log;

class GruposController extends Controller
{

	use TicketsTrait;


    public function addGrupo(Request $request)
    {
    	$datos = file_get_contents("../destination_actual.json");
		$destinoJson =  json_decode($datos, true);
		$datos ="";
		$id_categoria = "";
		$id_destinos = "";
		$arrayDestino = [];

		if(session('data') !== null)
		{
			$datos = session('data');
			$id_categoria = $datos['id_categoria'];
			$id_destinos = $datos['id_destino'];
			$paquetesDestinos = explode(",", $datos['id_destino']);

			foreach($paquetesDestinos as $key=>$destino)
			{
				foreach($destinoJson as $key1=>$dest)
				{
					if($destino == $dest['idDestino'])
					{
						$arrayDestino[$key1]['id'] = $dest['idDestino'];
						$arrayDestino[$key1]['value'] = $dest['desDestino'];
					}
				}
			}
		}
		$cliente = DB::select("
			SELECT p.id,p.nombre, p.apellido, p.documento_identidad,p.dv,tp.denominacion, p.activo
			FROM personas p
			JOIN tipo_persona tp on tp.id = p.id_tipo_persona
			WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
			AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa."
			AND p.activo = true
			");

		$aerolineas =DB::select('select id, nombre from personas where id_empresa = '.$this->getIdEmpresa().' and id in (select distinct(id_proveedor) from tickets where id_empresa = '.$this->getIdEmpresa().')');  
		
		$currency = Currency::where('activo', 'S')->orderBy('currency_code')->get();		
		// $estados = EstadoFactour::where('id_tipo_estado', '3')->get();	
		$tipos_tickets = TipoTicket::where('id','2')->orwhere('id','3')->orderBy('descripcion')->get();

/*
		$proveedor = Persona::where('id_tipo_persona',14)->orwhere('id_tipo_persona',15)->get();*/
		$usuario = DB::select(" SELECT id, CONCAT(nombre,' ',apellido,' - ',documento_identidad) as usuario_n 
								FROM personas 
								WHERE id_tipo_persona = 3 AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

		$proveedor = Persona::whereIn('id_tipo_persona',[14,15])->where('id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();

    	return view('pages.mc.grupos.add')->with(['valorDestinos'=>$arrayDestino, 'currencys'=>$currency, 'aerolineas'=>$aerolineas, 'tipos_tickets'=>$tipos_tickets, 'proveedores'=>$proveedor, 'clientes'=>$cliente, 'usuario'=>$usuario]);
    }

    public function storeGrupo(Request $request)
    {
    	/*echo "</pre>";
      	print_r(json_encode($request->all()));*/
    	// VALIDACIONES
    	if($request->input('posse_ticket') == "true"){
	    	$rules = [
	    		'denominacion' => 'required',
				'cantidad_bloqueos' => 'required',
				'cantidad_liberados' => 'required',
				'codigo_bloqueo' => 'required',
				'destino' => 'required',
				'fecha_emision' => 'required',
				'fecha_salida' => 'required',
				'fecha_seña' => 'required',
				'moneda' => 'required',
				'aerolinea' => 'required',
				'comision' => 'required',
				'PNR' => 'required',
				'facial' => 'required',
				'tasa' => 'required',
				'variacion_impuesto' => 'required',
				'monto_seña' => 'required',
				'monto_paquete' => 'required'
			];
    	}else{
	    	$rules = [
	    		'denominacion' => 'required',
				'destino' => 'required',
				'moneda' => 'required',
				'monto_paquete' => 'required'
			];
    	}

		
		$this->validate($request, $rules);

		$facial = str_replace(',','.', str_replace('.','', $request->input('facial')));
		$tasa = str_replace(',','.', str_replace('.','', $request->input('tasa')));
		$variacion_impuesto = str_replace(',','.', str_replace('.','', $request->input('variacion_impuesto')));
		$monto_seña = str_replace(',','.', str_replace('.','', $request->input('monto_seña')));
		$monto_paquete = str_replace(',','.', str_replace('.','', $request->input('monto_paquete')));

    	$fecha_creacion = date('Y-m-d');
    	if($request->input('fecha_salida') !=""){
	    	$fecha_s = explode("/", $request->input('fecha_salida'));
	    	$fecha_salida = $fecha_s[2]."-".$fecha_s[1]."-".$fecha_s[0];
	    }else{
	    	$fecha_salida = null;
	    }	

    	if($request->input('fecha_emision') !=""){
	    	$fecha_e = explode("/", $request->input('fecha_emision'));
	    	$fecha_emision = $fecha_e[2]."-".$fecha_e[1]."-".$fecha_e[0];
	    }else{
	    	$fecha_emision = null;
	    }	

    	if($request->input('fecha_seña') !=""){
	    	$fecha_se = explode("/", $request->input('fecha_seña'));
	    	$fecha_seña = $fecha_se[2]."-".$fecha_se[1]."-".$fecha_se[0];
	    }else{
	    	$fecha_seña = null;
	    }	
	    if($request->input('aerolinea')!= ""){
	    	$com_pactada = Persona::where('id', '=', $request->input('aerolinea'))->get(['comision_pactada']);
	      	$comision_pactada = (float) ($com_pactada[0]->comision_pactada);
	    }else{
	    	$comision_pactada = 0;
	    }  	
    	$aerolinea = $request->aerolinea; 

    	if ($request->comision == 1) 
      	{
        	$comision = round((($facial * (float) $comision_pactada) / 100), 2);
      	}
      	else
      	{
        	$comision = 0;
      	}

		$grupo = new Grupo;
		$grupo->denominacion = $request->input('denominacion');
		$grupo->cantidad = $request->input('cantidad_bloqueos');
		$grupo->cantidad_liberados = $request->input('cantidad_liberados');
		$grupo->codigo_bloqueo = $request->input('codigo_bloqueo');
		$grupo->destino_id = $request->input('destino');
		$grupo->fecha_emision = $fecha_emision;
		$grupo->fecha_creacion = $fecha_creacion;
		$grupo->fecha_salida = $fecha_salida;
		$grupo->senha_paquete = $monto_paquete;
		$grupo->fecha_seña = $fecha_seña;
		$grupo->proveedor_id = $aerolinea;
		$grupo->comision = $comision;
		$grupo->currency_compra_id = $request->input('moneda');
		$grupo->estado_id = 11; //ESTADO ABIERTO GRUPO
		$grupo->pnr = $request->input('PNR');
		$grupo->importe_facial = (float) $facial;
		$grupo->importe_tasas =(float) $tasa;
		$grupo->variacion_impuesto = (float) $variacion_impuesto;
		$grupo->monto_senha =(float) $monto_seña;
		$grupo->id_usuario = ($request->input('id_usuario') != '') ? $request->input('id_usuario') : NULL;
		$grupo->empresa_id = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		$grupo->cliente_id = ($request->input('cliente_id') != '') ? $request->input('cliente_id') : NULL;
		$grupo->imagen_documento = ($request->input('imagen_documento') != '') ? $request->input('imagen_documento') : NULL ;

		$grupo->tipo_ticket_id = $request->input('tipo_ticket');
       try
		{  
			 DB::beginTransaction();
			$grupo->save();
			$grupo_id = $grupo->id;

			if($request->input('posse_ticket') == "true"){
				$this->insertTickets($request, $grupo_id);
			}
			if($request->input('penalidad') !== null){
				foreach($request->input('penalidad') as $key=>$data){
		      		$penalidad = new Penalidad;
		      		$penalidad->id_grupo = $grupo_id;

		      		$fecha_pe = explode("/", $data['fecha']);
	    			$fecha_salida = $fecha_pe[2]."-".$fecha_pe[1]."-".$fecha_pe[0];

		      		$penalidad->fecha =  $fecha_salida;
		      		$penalidad->monto = str_replace(',','.', str_replace('.','',$data['monto']));
		      		$penalidad->save();
	      		}
	      	}	

			if($request->input('datos') !== null){
				foreach($request->input('datos') as $key=>$datos){
		      		$pago = new GruposPago;
		      		$pago->id_grupo = $grupo_id;

		      		$fecha_pe = explode("/", $datos['fecha']);
	    			$fecha_salida = $fecha_pe[2]."-".$fecha_pe[1]."-".$fecha_pe[0];

		      		$pago->fecha =  $fecha_salida;
		      		$pago->monto = str_replace(',','.', str_replace('.','',$datos['monto']));
		      		$pago->proveedor_id =  $datos['proveedor'];
		      		$pago->save();
	      		}
	      	}	

			if($request->input('senha') !== null){
				foreach($request->input('senha') as $key=>$senha){
		      		$gruposSenha = new GruposSenha;
		      		$gruposSenha->grupo_id = $grupo_id;

		      		$fecha_pe = explode("/", $senha['fecha']);
	    			$fecha_salida = $fecha_pe[2]."-".$fecha_pe[1]."-".$fecha_pe[0];

		      		$gruposSenha->fecha =  $fecha_salida;
		      		$gruposSenha->monto = str_replace(',','.', str_replace('.','',$senha['monto']));
		      		$gruposSenha->save();
	      		}
	      	}	
		    DB::commit();
	    	flash('¡Se ha ingresado exitosamente!')->success();
	 		 return redirect()->route('indexGrupo');
	    } 
		catch(\Exception $e)
		{	
			Log::error($e);
			DB::rollBack();
			flash('Ha ocurrido un error, inténtelo nuevamente, '.$e->getMessage())->error();
			return back()->withInput();
		}  
    }

    public  function insertTickets(Request $request, $id_grupo)
    {
     	$cantidad_bloqueados = $request->cantidad_bloqueos;
    	$cantidad_liberados =$request->cantidad_liberados; 

    	$fecha_alta = date('d/m/Y');
    	$aerolinea = $request->aerolinea; 
    	$facial = str_replace(',','.', str_replace('.','', $request->input('facial')));
		$tasa = str_replace(',','.', str_replace('.','', $request->input('tasa')));
		$variacion_impuesto = str_replace(',','.', str_replace('.','', $request->input('variacion_impuesto')));
		$monto_seña = str_replace(',','.', str_replace('.','', $request->input('monto_seña')));
		$monto_paquete = str_replace(',','.', str_replace('.','', $request->input('monto_paquete')));
		$genera_comision = $request->comision == 1;
		
		$u =DB::select("SELECT get_usuario_amadeus as u FROM get_usuario_amadeus(".$this->getIdUsuario().")");
        

      	if ($cantidad_bloqueados > 0) 
    	{
    		for ($i=0; $i < $cantidad_bloqueados; $i++) { 
    			$n = DB::select("select nextval as n from nextval('tickets_numero_amadeus_seq')");
				
				/**
				 * Funcion recibe fechas en formato d/m/Y
				 * Los numeros ya deben estar formateados
				 * Tira exepciones
				 */

				$ticket_data = [  
					'fecha_emision' => $request->fecha_emision,
					'id_proveedor' => $aerolinea,
					'facial' => (float) $facial,
					'id_grupo' =>  $id_grupo,
					'id_currency_costo' => $request->moneda,
					'usuario_amadeus' => $u[0]->u, //FUNCION INSERT 
					'pnr' => $request->PNR,
					'genera_iva' => true,
					'tasas' => (float)$tasa,
					'genera_comision' => $genera_comision,
					'id_tipo_ticket' => $request->tipo_ticket,
					'fecha_alta' => $fecha_alta,
					'id_usuario' => $this->getIdUsuario(),
					'nro_amadeus' => $n[0]->n,
					'id_tipo_transaccion' => 4,
					'variacion_impuesto' => $variacion_impuesto,
					'senha_paquete' => $monto_paquete
				  ];
				  $this->insert_into_ticket_manual($ticket_data);

		    }  
		}

      	if ($cantidad_liberados > 0) 
    	{
    		for ($i=0; $i < $cantidad_liberados; $i++) {
    			$n = DB::select("select nextval as n from nextval('tickets_numero_amadeus_seq')");

					$ticket_data = [  
						'fecha_emision' => $request->fecha_emision,
						'id_proveedor' => $aerolinea,
						'facial' => 0,
						'id_grupo' =>  $id_grupo,
						'id_currency_costo' => $request->moneda,
						'usuario_amadeus' => $u[0]->u, //FUNCION INSERT 
						'pnr' => $request->PNR,
						'genera_iva' => false,
						'tasas' => (float)$tasa,
						'genera_comision' => false,
						'id_tipo_ticket' => $request->tipo_ticket,
						'fecha_alta' => $fecha_alta,
						'id_usuario' => $this->getIdUsuario(),
						'nro_amadeus' => $n[0]->n,
						'id_tipo_transaccion' => 4,
						'variacion_impuesto' => $variacion_impuesto,
						'senha_paquete' => $monto_paquete
					  ];
					  $this->insert_into_ticket_manual($ticket_data);
			     
		    }  
			
		}  
    }

    public function destinos(Request $req)
    {
		$datos = file_get_contents("../destination_actual.json");
		$destinos =  json_decode($datos, true);
		
		if(isset($_GET['q']))
		{
            $search = $_GET['q'];
				foreach($destinos as $key=>$destino)
				{
					$resultado = strpos(strtolower($destino['desDestino']), strtolower($search));
					
					if($resultado === FALSE)
					{
						unset($destinos[$key]);
					}
				}	
		}
		
		$json = [];

		foreach($destinos as $key=>$row)
		{
		    $json[$row['idDestino']]['text'] = $row['desDestino'];
		}
		
		echo json_encode($json);
	}

	private function getId4Log()
	{
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}

	public function uploadDocument()
	{

		ini_set("gd.jpeg_ignore_warning", 1);
		  
        $file = Input::file('image');
        $input = array('image' => $file);
        $rules = array(
            'image' => 'image',
            'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png|max:20000'
        );
        $validator = Validator::make($input, $rules);
        
        if ( $validator->fails() )
        {
            return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);
        } 
        else 
        {
            $filename = $file->getClientOriginalName();            
            $base =  explode('.', $filename);
            $files = $this->getId4Log().'.'.$base[1];
            $indice = 0;

            if($base[1]== 'pdf'||$base[1]== 'xls'||$base[1]== 'doc'||$base[1]== 'docx'||$base[1]== 'pptx'||$base[1]== 'pps'||$base[1]== 'xlsx')
            {
                $indice = 1;
            }

            Storage::disk('grupos')->put($files, \File::get($file));

            return Response::json(['success' => true, 'archivo' => $files , 'file' => asset('grupos/' . $files), 'indice' =>$indice]);
        }

	}	

	public function index(Request $request)
	{
		$datos = file_get_contents("../destination_actual.json");
		$destinoJson =  json_decode($datos, true);
		$datos ="";
		$id_categoria = "";
		$id_destinos = "";
		$arrayDestino = [];
		
		if(session('data') !== null)
		{
			$datos = session('data');
			$id_categoria = $datos['id_categoria'];
			$id_destinos = $datos['id_destino'];
			$paquetesDestinos = explode(",", $datos['id_destino']);
			
			foreach($paquetesDestinos as $key=>$destino)
			{
				foreach($destinoJson as $key1=>$dest)
				{
					if($destino == $dest['idDestino'])
					{
						$arrayDestino[$key1]['id'] = $dest['idDestino'];
						$arrayDestino[$key1]['value'] = $dest['desDestino'];
					}
				}
			}
		}
					
		$grupos = Grupo::with('estado')
					   ->where('empresa_id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
		               ->get();
	
		$estados = EstadoFactour::where('id_tipo_estado', '3')->get();	
		$aerolineas = DB::select('select * from get_producto_persona(?,?)', [1, Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa]);
		$fechas_de_salida = [];
		$fechas_de_emision = [];
		$fechas_de_seña = [];


		foreach ($grupos as $key => $grupo) 
		{
			if($grupo->fecha_seña != ""){
	    		$fecha_s = explode("-", $grupo->fecha_salida);
	    		$fechas_de_salida[] = $fecha_s[2]."/".$fecha_s[1]."/".$fecha_s[0];
	    	}
	    	if($grupo->fecha_emision != ""){	
	    		$fecha_e = explode("-", $grupo->fecha_emision);
	    		$fechas_de_emision[] = $fecha_e[2]."/".$fecha_e[1]."/".$fecha_e[0];
    		}
    		if($grupo->fecha_seña != ""){
	    		$fecha_se = explode("-", $grupo->fecha_seña);
	    		$fechas_de_seña[] = $fecha_se[2]."/".$fecha_se[1]."/".$fecha_se[0];    	
        	}
        }	

        $cliente = DB::select("
			SELECT p.id,p.nombre, p.apellido,p.dv,p.documento_identidad, tp.denominacion, p.activo
			FROM personas p
			JOIN tipo_persona tp on tp.id = p.id_tipo_persona
			WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
			AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa."
			AND p.activo = true
			");


		return view('pages.mc.grupos.index')->with(['valorDestinos'=>$arrayDestino, 'estados'=>$estados, 'grupos'=>$grupos, 'aerolineas'=>$aerolineas, 'fechas_de_salida'=>$fechas_de_salida, 'fechas_de_emision'=>$fechas_de_emision, 'fechas_de_seña'=>$fechas_de_seña, 'clientes'=>$cliente]);
	}	

	public function getGrupo(Request $request){
		// dd($request->all());
		$grupos = Grupo::with('destino', 'estado', 'tickets', 'cliente')
						->where('empresa_id', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
		                ->get();

		foreach($grupos as $key=>$grupo){
			/*if(!empty($request->input('fecha_emision')))
			{
				$fecha_e = explode("/", $request->fecha_emision);
				$fecha_emision = $fecha_e[2]."-".$fecha_e[1]."-".$fecha_e[0];
				if($grupo->fecha_emision != $fecha_emision){
					unset($grupos[$key]);
				}
			}*/

			if(!empty($request->input('PNR')))
			{
				if($grupo->pnr != $request->input('PNR')){
					unset($grupos[$key]);
				}

			}

			if(!empty($request->input('destino')))
			{
				if($grupo->destino_id != $request->input('destino')){
					unset($grupos[$key]);
				}

			}

			if(!empty($request->input('cliente_id')))
			{
				if($grupo->cliente_id != $request->input('cliente_id')){
					unset($grupos[$key]);
				}

			}
			if(!empty($request->input('estado')))
			{
				if($grupo->estado_id != $request->input('estado')){
					unset($grupos[$key]);
				}

			}

			if(!empty($request->input('denominacion')))
			{

				$resultado = strpos(strtolower($grupo->denominacion), strtolower($request->input('denominacion')));
				if($resultado === FALSE){
					unset($grupos[$key]);
				}

			}

		}

		foreach($grupos as $key=>$grupo){
			$t = 0;
			$p = 0;
			$f = 0;
			$d = 0;
			foreach($grupo->tickets as $key1=>$ticket){
				/*echo '<pre>';
				print_r($ticket);*/

					switch ($ticket->id_estado) {
					    case 25:
					        $d = $d + 1;
					        break;
					    case 26:
					        $p = $p + 1;
					        break;
					    case 27:
					        $f = $f + 1;
					        break;
					}

			}
			$t = count($grupo->tickets);
			$grupos[$key]->t = $t;
			$grupos[$key]->d = $d;
			$grupos[$key]->p = $p;
			$grupos[$key]->f = $f;
			unset($grupos[$key]->tickets);

		}

   		return response()->json($grupos);
   		
	}

	private function getIdUsuario()
	{	
	 	$idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	 	return  $idUsuario;

	}

	public function editGrupo(Request $req, $id)
	{
		$grupo = Grupo::findOrFail($id);
		$estados = EstadoFactour::where('id_tipo_estado', '3')->get();	
		return view('pages.mc.grupos.edit', compact('grupo', 'estados'));
	}

	public function updateGrupo(Request $request, $id)
	{
		$grupo = Grupo::find($id);
		$tickets = Ticket::where('id_grupo', $id)->get();
		$cont = 0;
		// dd($tickets);
        try
        {
        	$grupo->update([
        		$grupo->pnr = $request->input('PNR'),
        		$grupo->estado_id = $request->input('estado'),
        		$grupo->codigo_bloqueo = $request->input('cod_bloqueo'),
        	]);

        	if (isset($tickets)) 
        	{
        		foreach ($tickets as $key => $ticket) 
        		{
        			if ($ticket->id_estado != 25) 
        			{
        				$cont = $cont + 1;
        			}
        		}

        		if ($cont == 0) 
        		{
        			foreach ($tickets as $key => $ticket) 	
        			{
        				$ticket->update([
        					$ticket->id_estado = 33
        				]);
        			}
        		}  
        		else
        		{
        			flash('No se puede actualizar a Cancelado. Alguno de los tickets asociados al grupo ya no están disponibles.')->error();
					return redirect()->back();
        		}   		        			
        	}

			flash('¡Se ha actualizado exitosamente!')->success();
			return redirect()->route('indexGrupo');

		} 
		catch(\Exception $e)
		{
			flash('Ha ocurrido un error, inténtelo nuevamente')->error();
			return redirect()->back();
		}	
	}

	public function verDetalle($id)
	{		
		$grupo = Grupo::with('penalidad', 'pagos', 'senha','usuario')->findOrFail($id);
		// dd($grupo);
		$fecha_salida = "";	
		if(isset($grupo->fecha_salida)){
			$fecha_s = explode("-", $grupo->fecha_salida);
	    	$fecha_salida = $fecha_s[2]."/".$fecha_s[1]."/".$fecha_s[0];
	    }
	    
	    $fecha_emision = "";	
	    if(isset($grupo->fecha_emision)){	
	    	$fecha_e = explode("-", $grupo->fecha_emision);
	    	$fecha_emision = $fecha_e[2]."/".$fecha_e[1]."/".$fecha_e[0];
	    }	

	    $fecha_seña = "";
	    if(isset($grupo->fecha_seña)){	
	    	$fecha_se = explode("-", $grupo->fecha_seña);
		    $fecha_seña = $fecha_se[2]."/".$fecha_se[1]."/".$fecha_se[0];
		}
		    	
    	foreach ($grupo->penalidad as $key => $value) {
    		$fecha_se = explode("-", $value->fecha);
    		$value->fecha = $fecha_se[2]."/".$fecha_se[1]."/".$fecha_se[0];
       	}
    
        foreach ($grupo->pagos as $key => $valor) {
			$proveedor = Persona::where('id',$valor->proveedor_id)->get();
			$valor->proveedor_id = $proveedor[0]->nombre;

       	}

		/*echo '<pre>';
		print_r($anticiposGrupos);*/

		return view('pages.mc.grupos.ver', compact('grupo', 'fecha_salida', 'fecha_emision', 'fecha_seña'));
	}

	private function getIdEmpresa()
	{
		return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}

	public function getAdelanto(Request $request){

	    $grupoAnterios = GrupoAnticipo::where('activo', true)							
						->get();
		$flag = 0;
		if($request->input('comprobante') != ''){
			foreach($grupoAnterios as $key=>$grupoAnt){
				if($grupoAnt->nro_comprobante == $request->input('comprobante')){
				  $flag = 1;
				}
			}
		}
		if($flag == 0){
	   		/*try
	        { */
				DB::beginTransaction();
				$grupoAnticipo = new GrupoAnticipo;
				$fecha_se = explode("/", $request->input('fecha'));
		    	$fecha_seña = $fecha_se[2]."-".$fecha_se[1]."-".$fecha_se[0];
		    	$grupoAnticipo->fecha =  $fecha_seña;
				$grupoAnticipo->forma_pago_id = $request->input('formaPago');
				$grupoAnticipo->moneda_id = $request->input('moneda');
				$grupoAnticipo->monto = str_replace(',','.', str_replace('.','', $request->input('monto')));
				$grupoAnticipo->nro_comprobante = $request->input('comprobante');
				$grupoAnticipo->nro_recibo = $request->input('recibo');
				$grupoAnticipo->id_usuario = $this->getIdUsuario();

				if($request->input('tipo') == 1){
					$grupoAnticipo->id_grupo = $request->input('id_grupo');
				}else{
					$grupoAnticipo->id_proforma = $request->input('id_proforma');
				}

				$grupoAnticipo->save();
				$grupoAnticipo->id = $grupoAnticipo->id;
				DB::commit();

				if($request->input('tipo') == 1){
		 			$lista = DB::select('SELECT grupos.id, grupos.denominacion as descripcion from grupos where grupos.estado_id != 12 and grupos.id ='.$request->input('id_grupo'));
		 			$grupoAnticipo->tipo = 'Grupo';
				}else{
					$lista = DB::select('SELECT proformas.id, proformas.id as descripcion from proformas, personas where proformas.estado_id != 5 and proformas.pasajero_id = personas.id and proformas.id = '.$request->input('id_proforma'));
		 			$grupoAnticipo->tipo = 'Proforma';
				}

				if(isset($lista[0]->descripcion)){
					$grupoAnticipo->lista = $lista[0]->descripcion;
				}else{
					$grupoAnticipo->lista = '';
				}

				$formaPagos = FormaPagoCliente::where('id', $request->input('formaPago'))->get();
				if(isset($formaPagos[0]['denominacion'])){
					$grupoAnticipo->forma_pago = $formaPagos[0]['denominacion'];
				}else{
					$grupoAnticipo->forma_pago = '';
				}	
				$currency = Currency::where('currency_id', $request->input('moneda'))->get();	
				if(isset($currency[0]['currency_code'])){
					$grupoAnticipo->denominacionMoneda = $currency[0]['currency_code'];
				}else{
					$grupoAnticipo->denominacionMoneda = '';
				}
				$grupoAnticipo->mensaje = 'OK';

				return json_encode($grupoAnticipo);					
			/*} 
			catch(\Exception $e)
			{
				flash('Ha ocurrido un error, inténtelo nuevamente')->error();
				return redirect()->back();
			}*/
		}else{
				$grupoAnticipo = new \StdClass; 
				$grupoAnticipo->mensaje = 'ERROR, el comprobante que esta tratando de ingresar ya fue registrado';
				return json_encode($grupoAnticipo);
		}
	}	

	public function getEliminarAdelanto(Request $request){

		$grupoAnterios = GrupoAnticipo::where('id', $request->input('id_fila'))->get();	
		$fecha_actual = strtotime(date("d-m-Y"));
        $fecha_entrada = strtotime($grupoAnterios[0]['fecha']);

		if($fecha_actual > $fecha_entrada)
		{
			$mensaje = new \StdClass; 
			$mensaje->status ='ERROR';
			$mensaje->mensaje ='No se puede eliminar un Pago, Fecha anterior a la actual';
			return json_encode($mensaje);			        
		}else{
			DB::table('grupos_anticipo')
					->where('id',$request->input('id_fila'))
						        ->update([
						            	'activo'=>false,
						            	'usuario_delete'=>$this->getIdUsuario(),
						            	'fecha_delete'=>date('Y-m-d'),
						            	'hora_delete'=>date('H:i:s')
						            	]);
			$mensaje = new \StdClass; 
			$mensaje->status ='OK';
			$mensaje->mensaje ='Se a eliminado el Pago con exito';
			$mensaje->monto = $grupoAnterios[0]['monto'];
			return json_encode($mensaje);			        

		}

	}	

}