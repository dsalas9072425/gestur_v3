<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Redirect;
use App\OpCabecera;
use App\Destination;
use App\Usuario;
use App\Persona;
use App\AdjuntoDocumento;
use App\ReciboAdjunto;
use App\TransferenciasCuentas;
use App\Timbrado;
use App\LibroVenta;
use App\LibroCompra;
use Response;
use Image; 
use App\IntermediaAdjuntoOp;


class UploadController extends Controller {
	public $restful=true;

	private function getIdUsuario(){
	
	 	return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;

	}//function
    private function getIdEmpresa()
      {
    
       return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
      }

	private function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}

	public function index(){
		$file = Input::file('image');
		$input = array('image' => $file);
		$rules = array(
			'image' => 'image',
			'image' => 'required|mimes:png'
		);
		$validator = Validator::make($input, $rules);
		if ( $validator->fails() )
		{
			return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);

		} else {

			$filename = $this->getId4Log().'.png';

			$path = public_path('uploads/' . $filename);

			Image::make($file->getRealPath())->resize(580, 880)->save($path);

			return Response::json(['success' => true, 'archivo' => $filename , 'file' => asset('uploads/' . $filename)]);
		}

	}

	public function indexDtplus(){
		ini_set("gd.jpeg_ignore_warning", 1);

		$file = Input::file('image');
		$input = array('image' => $file);
		$rules = array(
			'image' => 'image',
			'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png|max:20000'
		);
		$validator = Validator::make($input, $rules);
		if ( $validator->fails() )
		{
			return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);

		} else {

			$filename = $this->getId4Log().'.png';

			$path = public_path('uploads/' . $filename);

			Image::make($file->getRealPath())->resize(580, 880)->save($path);

			return Response::json(['success' => true, 'archivo' => $filename , 'file' => asset('uploads/' . $filename)]);
		}

	}

	public function uploadDocumentosBancario(){
			ini_set("gd.jpeg_ignore_warning", 1);
			$file = Input::file('image');
			$input = array('image' => $file);
			$rules = array(
				'image' => 'image',
				'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png,jpg|max:20000'
			);
			$validator = Validator::make($input, $rules);
		 	if ( $validator->fails() )
			{
				return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);

			} else { 

				$filename = $file->getClientOriginalName();

				$base =  explode('.', $file->getClientOriginalName());

				$files = $this->getId4Log().'_'.$filename;

				$indice = 0;
				if($base[1]== 'pdf'||$base[1]== 'xls'||$base[1]== 'doc'||$base[1]== 'docx'||$base[1]== 'pptx'||$base[1]== 'pps'||$base[1]== 'xlsx'){
					$indice = 1;
				}

				Storage::disk('uploadDocumento')->put($files, \File::get($file));

				$update = [
					"adjunto" => $files,
				];
				$transaccion = new TransferenciasCuentas;
				$transaccion = $transaccion->where('id',Input::get('id_transaccion'));
				$transaccion = $transaccion->update($update);
				/*return Response::json(['success' => true, 'archivo' => $files , 'file' => asset('uploadDocumento/' . $files), 'indice' =>$indice, 'id'=>$id]);*/
				return Response::json(['success' => true, 'archivo' => $files , 'file' => asset('uploadDocumento/' . $files)]);
			 }


	}

	public function uploadDocumentosRecibos(Request $req){
		ini_set("gd.jpeg_ignore_warning", 1);
		$file = Input::file('image');
		$input = array('image' => $file);
		$rules = array(
			'image' => 'image',
			'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png,jpg|max:20000'
		);
		$validator = Validator::make($input, $rules);
		 if ( $validator->fails() )
		{
			return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);

		} else { 

			$filename = $file->getClientOriginalName();

			$base =  explode('.', $file->getClientOriginalName());

			$files = $this->getId4Log().'_'.$filename;

			$indice = 0;
			if($base[1]== 'pdf'||$base[1]== 'xls'||$base[1]== 'doc'||$base[1]== 'docx'||$base[1]== 'pptx'||$base[1]== 'pps'||$base[1]== 'xlsx'){
				$indice = 1;
			}

			Storage::disk('uploadComprobanteRecibo')->put($files, \File::get($file));

			$reciboAdjunto = new ReciboAdjunto;
			$reciboAdjunto->id_recibo =$req->input('id_recibo');
			$reciboAdjunto->fecha_hora = date('Y-m-d H:i:s');
			$reciboAdjunto->id_usuario = $this->getIdUsuario();
			$reciboAdjunto->comprobante = $files;
			$reciboAdjunto->save();
			$idCarga = $reciboAdjunto->id;

			return Response::json(['success' => true, 'archivo' => $files , 'numero'=>$idCarga, 'file' => asset('uploadComprobanteRecibo/' . $files)]);
		 }
	}




	public function indexDtp_lus(){
		ini_set("gd.jpeg_ignore_warning", 1);

		$file = Input::file('image');
		$input = array('image' => $file);
		$rules = array(
			'image' => 'image',
			'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png|max:20000'
		);
		$validator = Validator::make($input, $rules);
		if ( $validator->fails() )
		{
			return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);

		} else {

			$filename = $this->getId4Log().'.png';

			$path = public_path('uploads/' . $filename);

			Image::make($file->getRealPath())->resize(580, 880)->save($path);

			return Response::json(['success' => true, 'archivo' => $filename , 'file' => asset('uploads/' . $filename)]);
		}

	}



		public function uploadAdjunto(Request $req){
			ini_set("gd.jpeg_ignore_warning", 1);
			$files = Input::file('image');
			$input = array('image' => $files);
			$rules = array(
				'image.*' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png,jpg|max:20000'
			);
			$validator = Validator::make($input, $rules);
			if ( $validator->fails() )
			{
				return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);

			} else {

				$info_adjunto = [];

				foreach ($files as $file) {
					$filename = $file->getClientOriginalName();

					$base =  explode('.', $file->getClientOriginalName());

					$file_name = $this->getId4Log().'_'.$filename;

					$indice = 0;
					if($base[1]== 'pdf'||$base[1]== 'xls'||$base[1]== 'doc'||$base[1]== 'docx'||$base[1]== 'pptx'||$base[1]== 'pps'||$base[1]== 'xlsx'){
						$indice = 1;
					}

					Storage::disk('adjuntoDetalle')->put($file_name, \File::get($file));

					$voucher = new AdjuntoDocumento;
					$voucher->proforma_id = $req->input('proforma_id');
					$voucher->nombre_adjunto = $file_name;
					$voucher->fecha = date('Y-m-d H:m:i');
					$voucher->save();

					$info_adjunto[] = [
						'id' => $voucher->id,
						'archivo' => $file_name,
						'file' => asset('adjuntoDetalle/' . $file_name),
						'indice' => $indice
					];
					
				}
				
				return Response::json(['success' => true, 'data' => $info_adjunto]);
			}

		}


				/**
	 * PARA ELIMINAR LAS IMAGENES DE DOCUMENTOS OP
	 * @param  Request $req RECIBE LA DIRECCION
	 * @return  RETORNA TRUE O FALSE 
	 */
	public function fileDeleteOpCopia(Request $req)
{
	//dd($req->all());
    $mensaje = array('rsp' => true);

    try {
        $archivoAEliminar = $req->input('dataFile');
        $adjunto = IntermediaAdjuntoOp::where('archivo', $archivoAEliminar)
		->where('op_cabecera_id',$req->input('id_op'))
		->firstOrFail();
       // dd($adjunto);
        if ($adjunto) {
            $adjunto->delete(); 
            unlink('adjuntoDetalle/documentos_op/' . $archivoAEliminar); 
        } else {
            $mensaje = array('rsp' => false);
        }
    } catch (\Exception $e) {
        $mensaje = array('rsp' => false);
    }

    return response()->json($mensaje);
}

//cuidado se elimino arturo capo	
	public function fileDeleteOp(Request $req){
		$directorioImagen = 'adjuntoDetalle/documentos_op/';
		$mensaje = array('rsp'=>true);

		try{
			unlink($directorioImagen.$req->input('dataFile'));
			OpCabecera::where('id',$req->input('id_op'))->update(['adjunto'=>null]);
		} catch(\Exception $e){
			$mensaje = array('rsp'=>false);
		}

		return response()->json($mensaje);
	}

	public function fileAdjuntoRecibo(Request $req){
		$directorioImagen = 'uploadComprobanteRecibo/';
		$mensaje = array('rsp'=>true);
	 //	try{
			$adjunto = ReciboAdjunto::where('id',$req->input('file'))->first();
			unlink($directorioImagen.$adjunto->comprobante);
			ReciboAdjunto::where('id',$req->input('file'))->delete();

	/*	} catch(\Exception $e){
			$mensaje = array('rsp'=>false);
		}*/

		return response()->json($mensaje);
	}

	public function fileAdjuntoEliminarDocumento(Request $req){
		$directorioImagen = 'uploadDocumento/';
		$mensaje = array('rsp'=>true);

		try{
			unlink($directorioImagen.$req->input('dataFile'));
		} catch(\Exception $e){
			$mensaje = array('rsp'=>false);
		}

		return response()->json($mensaje);
	}

	public function fileAdjuntoEliminarLC(Request $req){
		$directorioImagen = 'adjuntoLc';
		$mensaje = array('rsp'=>true);
		try{
			unlink($directorioImagen.'/'.$req->input('dataFile'));
			if($req->input('dataId')!==null){
			LibroCompra::where('id',$req->input('dataId'))->update(['adjunto' => '']); 
			}
		} catch(\Exception $e){
			$mensaje = array('rsp'=>false);
		}

		return response()->json($mensaje);
	}

	public function adjuntoDocumentosOp(Request $req)
	{
		$name = '';
		$imagen = false;
		$directorioImagen = 'adjuntoDetalle/documentos_op/';
		$resp = new \StdClass;
		$resp->err = true;
	
	
		 ini_set("gd.jpeg_ignore_warning", 1);
		 $file = Input::file('image');
		 $input = array('image' => $file);
		 $rules = array(
			 'image' => 'image',
			 'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png,jpg|max:20000'
		 );
		 $validator = Validator::make($input, $rules);
		 if ( $validator->fails() )
		 {	$resp->err = false;
			$resp->errors = $validator->getMessageBag()->toArray();
			 return Response::json($resp);
	
		 } else {
	
			//try{
			 $filename = $file->getClientOriginalName();
	
			 $extension =  explode('.', substr(strval($file->getClientOriginalName()),-4));
			 if($extension == 'jpg'||$extension == 'jpeg'||$extension == 'bmp'||$extension == 'png'){
				$imagen = true;
			}
			//NUMERO ALEATIRIO CON FECHA
			 $files = $this->getId4Log().'_'.$filename;
	
			 //CONFIGURADO EN EL APP/ FILE SYTEM
			 Storage::disk('adjuntoOp')->put($files, \File::get($file));
	
			 OpCabecera::where('id',$req->id_op)->update(['adjunto' => $files]); 
	
			 $resp->archivo = $files;
			 $resp->imagen = $imagen;
	
			 return Response::json($resp);
		 }
	}
	

	/**
	 * PARA SUBIR LAS IMAGENES DE OP
	 */
	public function adjuntoDocumentosOpCopia(Request $req)
{
    $name = '';
    $imagen = false;
    //$directorioImagen = 'adjuntoDetalle/documentos_op/';
    $resp = new \StdClass;
    $resp->err = true;

    ini_set("gd.jpeg_ignore_warning", 1);
    $files = Input::file('image');
    $input = array('image' => $files);
    $rules = array(
        'image.*' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png,jpg|max:20000'
    );
    $validator = Validator::make($input, $rules);

    if ($validator->fails()) {
        $resp->err = false;
        $resp->errors = $validator->getMessageBag()->toArray();
        return Response::json($resp);
    } else {
        $uploadedFiles = [];

        foreach ($files as $file) {
            $filename = $file->getClientOriginalName();
			$filename=str_replace(' ','', $filename);
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            $imagen = in_array(strtolower($extension), ['jpg', 'jpeg', 'bmp', 'png', 'pdf', 'doc', 'docx']);
            $fileWithRandomName = $this->getId4Log() . '_' . $filename;
			//dd($fileWithRandomName,$file);
           // Storage::disk('adjuntoOp')->put($fileWithRandomName,$file);
			Storage::disk('adjuntoOp')->put($fileWithRandomName, \File::get($file));
            $uploadedFiles[] = [
                'archivo' => $fileWithRandomName,
                'imagen' => $imagen
            ];
			$intermediaAdjunto = new IntermediaAdjuntoOp();
            $intermediaAdjunto->op_cabecera_id = $req->id_op;
            $intermediaAdjunto->archivo = $fileWithRandomName;
            //$intermediaAdjunto->imagen = $imagen;
            $intermediaAdjunto->save();
        }

      //  OpCabecera::where('id', $req->id_op)->update(['adjunto' => $uploadedFiles]);

		//OpCabecera::where('id', $req->id_op)->update(['adjunto' => $uploadedFiles[0]['archivo']]);

        $resp->archivos = $uploadedFiles;
        return Response::json($resp);
    }
}



	




		/**
	 * PARA SUBIR LAS IMAGENES DE OP
	 */
	public function lcUpload(Request $req){

		$name = '';
		$imagen = false;
		$directorioImagen = 'adjuntoLc';
		$resp = new \StdClass;
		$resp->err = true;


		 ini_set("gd.jpeg_ignore_warning", 1);
		 $file = Input::file('image');
		 $input = array('image' => $file);
		 $rules = array(
			 'image' => 'image',
			 'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png,jpg|max:20000'
		 );
		 $validator = Validator::make($input, $rules);
		 if ( $validator->fails() )
		 {	$resp->err = false;
			$resp->errors = $validator->getMessageBag()->toArray();
			 return Response::json($resp);

		 } else {

			//try{
			 $filename = $file->getClientOriginalName();

			 $extension =  explode('.', substr(strval($file->getClientOriginalName()),-4));
			 if($extension == 'jpg'||$extension == 'jpeg'||$extension == 'bmp'||$extension == 'png'){
				$imagen = true;
			}
			//NUMERO ALEATIRIO CON FECHA
			 $files = $this->getId4Log().'_'.$filename;

			 //CONFIGURADO EN EL APP/ FILE SYTEM
			 Storage::disk('adjuntoLc')->put($files, \File::get($file));

			 OpCabecera::where('id',$req->id_op)->update(['adjunto' => $files]); 
			 $resp->archivo = $files;
			 $resp->imagen = $imagen;

			 return Response::json($resp);
		 }

	}//function

//metodo para cargar item 
public function lcUploadItem(Request $req){

	$name = '';
	$imagen = false;
	$directorioImagen = 'adjuntoLc';
	$resp = new \StdClass;
	$resp->err = true;


	 ini_set("gd.jpeg_ignore_warning", 1);
	 $file = Input::file('image');
	 $input = array('image' => $file);
	 $rules = array(
		 'image' => 'image',
		 'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png,jpg|max:20000'
	 );
	 $validator = Validator::make($input, $rules);
	 if ( $validator->fails() )
	 {	$resp->err = false;
		$resp->errors = $validator->getMessageBag()->toArray();
		 return Response::json($resp);

	 } else {

		//try{
		 $filename = $file->getClientOriginalName();

		 $extension =  explode('.', substr(strval($file->getClientOriginalName()),-4));
		 if($extension == 'jpg'||$extension == 'jpeg'||$extension == 'bmp'||$extension == 'png'){
			$imagen = true;
		}
		//NUMERO ALEATIRIO CON FECHA
		 $files = $this->getId4Log().'_'.$filename;

		 //CONFIGURADO EN EL APP/ FILE SYTEM
		 Storage::disk('adjuntoLc')->put($files, \File::get($file));

		 LibroCompra::where('id',$req->id_libro_compra)->update(['adjunto' => $files]); 
		 $resp->archivo = $files;
		 $resp->imagen = $imagen;

		 return Response::json($resp);
	 }

}


 	public function getDownloadLv(Request $request){
             set_time_limit(600);
             $file= public_path(). "/reportLv.txt";
             $fp = fopen($file, 'w');
		      $cabecerahechauka = DB::table('vw_hechauka_lv_cabecera');
		      $cabecerahechauka = $cabecerahechauka->where('id_empresa',$this->getIdEmpresa());
		      if($request->input('anho') != ''){
		         if($request->input('mes')){
		            $cabecerahechauka = $cabecerahechauka->where('periodo',$request->input('anho')."".$request->input('mes'));
		    	 }
		      }
		      $cabecerahechauka = $cabecerahechauka->get();
 			  foreach ($cabecerahechauka as $dato) {
                fwrite($fp, $dato->tipo_registro."\t".$dato->periodo."\t".$dato->tipo_reporte."\t".$dato->cod_obligacion."\t".$dato->cod_formulario."\t".$dato->ruc_agente."\t".$dato->dv_agente."\t".strtoupper($dato->nombre_agente)."\t".$dato->ruc_representante."\t".$dato->dv_representante."\t".strtoupper($dato->representante_legal)."\t".$dato->cant_registros."\t".number_format($dato->monto_reportado,0,",","")."\t\r\n");
             }
		      $detallehechauka = DB::table('vw_hechauka_lv_detalle');
		      $detallehechauka = $detallehechauka->where('id_empresa',$this->getIdEmpresa());
		      if($request->input('id_proveedor') != ''){
		          $cabecerahechauka = $detallehechauka->where('id_proveedor',$request->input('id_proveedor'));
		      }
		      if($request->input('mes') != ''){
		         if($request->input('anho')){
		            $detallehechauka = $detallehechauka->where('fecha_documento','like', '%' .trim($request->input('mes')."/".$request->input('anho')).'%');
		         }
		      }      
		      $detallehechauka = $detallehechauka->orderBy('fecha_documento', 'asc');
		      $detallehechauka = $detallehechauka->get();
		      foreach ($detallehechauka as $datos) {
                fwrite($fp, $datos->tipo_registro."\t".$datos->ruc_cliente."\t".$datos->dv_cliente."\t".strtoupper($datos->cliente)."\t".$datos->tipo_documento."\t".$datos->nro_documento."\t".$datos->fecha_documento."\t".number_format($datos->monto_venta_10,0,",","")."\t".number_format($datos->iva_debito_10,0,",","")."\t".number_format($datos->monto_venta_5,0,",","")."\t".number_format($datos->iva_debito_5,0,",","")."\t".number_format($datos->monto_venta_exenta,0,",","")."\t".number_format($datos->monto_ingreso,0,",","")."\t".$datos->condicion_venta."\t".$datos->cant_cuota."\t".$datos->nro_timbrado/*."\t".$datos->tipo_registro.*/."\t\r\n");
             }
////////////////////////////////////////////////////////////////////////////////////////////////////
            // echo $file;
             $headers = array(
              '"Content-Type:text/plain"',
            );
	        return Response::download($file, 'reportLv.txt', $headers);
	}

	public function getDownloadTesaka(Request $request)
	{
		set_time_limit(600);
        $file= public_path(). "/tesaka.txt";
        $fp = fopen($file, 'w');

		$tesaka = DB::table('vw_tesaka');

			// dd($request->all());
		if($request->datos !== null)
		{
			$arrayId= [];

			foreach ($request->datos as $key => $dato) 
			{
	            $arrayId[] = $key;
			}


			$tesaka = $tesaka->whereIn('id', $arrayId);		
		}

		$tesaka = $tesaka->get();

		$tesakaArray = '';
		$tesakaArray .= "[ \t\r\n"; 
		$total = count($tesaka);
		$contador = 0;
		foreach ($tesaka as $key => $item) 
        {
			$contador++;
			$tesakaArray .= "\t { \t\r\n"; 
			$tesakaArray .= "\t \"detalle\":\t\r\n"; 
			$tesakaArray .= "\t\t[ \t\r\n"; 
			$tesakaArray .= "\t { \t\r\n"; 
			$tesakaArray .= "\t\t\"cantidad\":".$item->cantidad.",\t\r\n"; 
			$tesakaArray .= "\t\t\"tasaAplica\":\"".$item->tasa_aplica."\",\t\r\n"; 
			$tesakaArray .= "\t\t\"precioUnitario\":".$item->importe.",\t\r\n";
			$tesakaArray .= "\t\t\"descripcion\":\"".$item->concepto."\"\t\r\n"; 
			$tesakaArray .= "\t } \t\r\n"; 
			$tesakaArray .= "\t\t], \t\r\n"; 
			$tesakaArray .= "\t \"retencion\":\t\r\n"; 
			$tesakaArray .= "\t { \t\r\n"; 
			$tesakaArray .= "\t\t\"fecha\":\"".$item->fecha_retencion."\",\t\r\n";
			$tesakaArray .= "\t\t\"moneda\":\"".$item->currency_code."\",\t\r\n"; 
			if($item->retencion_renta == ""){
				$retencion = "false";
			}else{
				$retencion = "true";
			}
			$tesakaArray .= "\t\t\"retencionRenta\":".$retencion.",\t\r\n";
			$tesakaArray .= "\t\t\"conceptoRenta\":\"".$item->concepto_renta."\",\t\r\n";
			$tesakaArray .= "\t\t\"ivaPorcentaje5\":".$item->iva_porcentaje_5.",\t\r\n";
			$tesakaArray .= "\t\t\"ivaPorcentaje10\":".$item->iva_porcentaje_10.",\t\r\n";
			$tesakaArray .= "\t\t\"rentaCabezasBase\":".$item->renta_cabezas_base.",\t\r\n";
			$tesakaArray .= "\t\t\"rentaCabezasCantidad\":".$item->renta_cabezas_cantidad.",\t\r\n"; 
			$tesakaArray .= "\t\t\"rentaToneladasBase\":".$item->renta_toneladas_base.",\t\r\n"; 
			$tesakaArray .= "\t\t\"rentaToneladasCantidad\":".$item->renta_toneladas_cantidad.",\t\r\n"; 
			$tesakaArray .= "\t\t\"rentaPorcentaje\":".$item->renta_porcentaje.",\t\r\n";
			$tesakaArray .= "\t\t\"retencionIva\":".$item->retencion_iva.",\t\r\n";
			$tesakaArray .= "\t\t\"conceptoIva\":\"".$item->concepto_iva."\"\t\r\n";
			$tesakaArray .= "\t }, \t\r\n"; 
			$tesakaArray .= "\t \"informado\":\t\r\n"; 
			$tesakaArray .= "\t { \t\r\n"; 
			$tesakaArray .= "\t\t\"situacion\":\"".$item->situacion."\",\t\r\n"; 
			$tesakaArray .= "\t\t\"nombre\":\"".$item->nombre."\",\t\r\n";
			$tesakaArray .= "\t\t\"ruc\":\"".$item->ruc."\",\t\r\n";
			$tesakaArray .= "\t\t\"dv\":\"".$item->dv."\",\t\r\n";
			$tesakaArray .= "\t\t\"domicilio\":\"".$item->domicilio."\",\t\r\n"; 
			$tesakaArray .= "\t\t\"tipoIdentificacion\":\"".$item->tipo_identificacion."\",\t\r\n";
			$tesakaArray .= "\t\t\"identificacion\":\"".$item->identificacion."\",\t\r\n";
			$tesakaArray .= "\t\t\"direccion\":\"".$item->direccion."\",\t\r\n";
			$tesakaArray .= "\t\t\"correoElectronico\":\"".$item->correo_electronico."\",\t\r\n";
			$tesakaArray .= "\t\t\"pais\":\"".$item->pais."\",\t\r\n";
			$tesakaArray .= "\t\t\"telefono\":\"".$item->telefono."\"\t\r\n";
			$tesakaArray .= "\t }, \t\r\n"; 
			$tesakaArray .= "\t \"transaccion\":\t\r\n"; 
			$tesakaArray .= "\t { \t\r\n"; 
			$tesakaArray .= "\t\t\"numeroComprobanteVenta\":\"".$item->numero_comprobante_venta."\",\t\r\n";  
			$tesakaArray .= "\t\t\"condicionCompra\":\"".$item->condicion_compra."\",\t\r\n"; 
			$tesakaArray .= "\t\t\"cuotas\":".$item->cuotas.",\t\r\n"; 
			$tesakaArray .= "\t\t\"tipoComprobante\":".$item->tipo_comprobante.",\t\r\n";  
			$tesakaArray .= "\t\t\"fecha\":\"".$item->fecha."\",\t\r\n";  
			$tesakaArray .= "\t\t\"numeroTimbrado\":\"".$item->numero_timbrado."\"\t\r\n";  
			$tesakaArray .= "\t }, \t\r\n"; 
			$tesakaArray .= "\t \"atributos\":\t\r\n"; 
			$tesakaArray .= "\t { \t\r\n"; 
			$tesakaArray .= "\t\t\"fechaCreacion\":\"".$item->fecha_creacion."\",\t\r\n";
			$tesakaArray .= "\t\t\"fechaHoraCreacion\":\"".$item->fecha_hora_creacion."\"\t\r\n"; 
			$tesakaArray .= "\t } \t\r\n"; 
			if($total == $contador){
				$tesakaArray .= "\t } \t\r\n"; 
			}else{
				$tesakaArray .= "\t }, \t\r\n"; 
			}
		}   	
		$tesakaArray .= "] \t\r\n"; 

		fwrite($fp, $tesakaArray);

		 $headers = array(
              '"Content-Type:text/plain"',
            );
	    
	    return Response::download($file, 'tesaka.txt', $headers);

	}

	public function getDownloadLc(Request $request){ 
             set_time_limit(600);
             $file= public_path(). "/reportLc.txt";
             $fp = fopen($file, 'w');
             $usuarios = Persona::all();
			 $cabecerahechauka = DB::table('vw_hechauka_lc_cabecera');
		      $cabecerahechauka = $cabecerahechauka->where('id_empresa',$this->getIdEmpresa());
			  $cabecerahechauka = $cabecerahechauka->get();
			  $periodo=$request->input('anho')."".$request->input('mes');
 			  foreach ($cabecerahechauka as $dato) {
                fwrite($fp, $dato->tipo_registro."\t".$periodo."\t".$dato->tipo_reporte."\t".$dato->cod_obligacion."\t".$dato->cod_formulario."\t".$dato->ruc_agente."\t".$dato->dv_agente."\t".strtoupper($dato->nombre_agente)."\t".$dato->ruc_representante."\t".$dato->dv_representante."\t".strtoupper($dato->representante_legal)."\t".$dato->version."\t".$dato->cant_compras."\t".number_format($dato->monto_reportado,0,",","")."\t".$dato->exportador."\t\r\n");
             }
		      $detallehechauka = DB::table('vw_hechauka_lc_detalle');
		      $detallehechauka = $detallehechauka->where('id_empresa',$this->getIdEmpresa());
		      if($request->input('id_proveedor') != ''){
		          $cabecerahechauka = $detallehechauka->where('id_proveedor',$request->input('id_proveedor'));
		      }
		      if($request->input('mes') != ''){
		         if($request->input('anho')){
		            $detallehechauka = $detallehechauka->where('mes', trim($request->input('mes')))->where('anho', $request->input('anho'));
		         }
		      }      
		      $detallehechauka = $detallehechauka->get();
		       foreach ($detallehechauka as $datos) {
                fwrite($fp, $datos->tipo_registro."\t".$datos->ruc_proveedor."\t".$datos->dv_proveedor."\t".strtoupper($datos->proveedor)."\t".$datos->nro_timbrado."\t".$datos->tipo_documento."\t".$datos->nro_documento."\t".$datos->fecha_documento."\t".$datos->monto_compra_10."\t".$datos->iva_credito_10."\t".$datos->monto_compra_5."\t".$datos->iva_credito_5."\t".$datos->monto_compra_exenta."\t0\t".$datos->condicion_compra."\t".$datos->cant_cuota."\t\r\n");
             }
            // echo $file;
             $headers = array(
              '"Content-Type:text/plain"',
            );
	        return Response::download($file, 'reportLc.txt', $headers);
	}

	/**
	 * PARA SUBIR LAS IMAGENES DE PERSONAS 
	 */
	public function addPersonaImagen(){

	$crop = 0;	
	$type = '';	
	$name = '';
	$ratio = 0;
	//ALTO Y ANCHO DEFINIDO 
	 $width  = 200;
	 $height = 200;

  //FILTRAR POR FORMATO DE LA IMAGEN	
  $file = Input::file('image');
		$input = array('image' => $file);
		$rules = array(
			'image' => 'image',
			'image' => 'mimes:png,jpeg,jpg,bmp|max:3000'
		);
		$validator = Validator::make($input, $rules);

		if ( $validator->fails() )
		{
			return Response::json(['success' => false, 'errors' => 'El formato de la imagen no es soportado.']);

		}else {
			//OBTENER EXTENSION DE ARCHIVO
			$mime = Input::file('image')->getMimeType();
			$type = explode('/',$mime);
			$type = $type[1];
		}

	//GUARDAR Y OBTENER LA DIRECCION DEL ARCHIVO
  	$filename = $this->getId4Log().'.'.$type;
  	$src = public_path('personasLogo/' . $filename);
  	Image::make($file->getRealPath())->save($src);

  	if(Input::get('tiposPersonas') == 20){
  		$srcs = public_path('logoEmpresa/' . $filename);
  		Image::make($file->getRealPath())->save($srcs);
  	}
  	
   //OBTENER ANCHO Y ALTO		
  if(!list($w, $h) = getimagesize($src)) return response()->json(['success' => false, 'errors' => 'El formato de la imagen no es soportado']);

  

	//SI LA IMAGEN ES PEQUEÑA NO PASA POR FILTRO PARA REDIMENSION
	if($w < $width && $h < $height){

		 return response()->json(['success'=>true,'archivo'=>$filename,'file' => asset('personasLogo/' . $filename)]);
  
	} else {

	 //CONVERTIR EN MINUSCULA LA EXTENSION
  $type = strtolower($type);

  if($type == 'jpeg') $type = 'jpg';
  switch($type){
    case 'bmp': $img = imagecreatefromwbmp($src); break;
    case 'gif': $img = imagecreatefromgif($src); break;
    case 'jpg': $img = imagecreatefromjpeg($src); break;
    case 'png': $img = imagecreatefrompng($src); break;
    default : return response()->json(['success' => false, 'errors' => 'El formato de la imagen no es soportado']);
  }

  if($img == false){
  	return response()->json(['success' => false, 'errors' => 'Ocurrio un error al procesar la imagen.']);
  }

   // SI ES 1 RECORTAR LA IMAGEN SI NO REDIMENSIONAR
  if($crop){

    // if($w < $width or $h < $height) return "ERROR IMAGEN PEQUEÑA";

    $ratio = max($width/$w, $height/$h);
    $h = $height / $ratio;
    $x = ($w - $width / $ratio) / 2;
    $w = $width / $ratio;

  } else {

    //if($w < $width and $h < $height) return "ERROR IMAGEN PEQUEÑA";

    $ratio = min($width/$w, $height/$h);
    $width = $w * $ratio;
    $height = $h * $ratio;
    $x = 0;

  }

  $new = imagecreatetruecolor($width, $height);

  // PRESERVAR TRANSPARENCIA DE LAS IMAGENES GIF O PNG			
  if($type == "gif" or $type == "png"){
    imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
    imagealphablending($new, false);
    imagesavealpha($new, true);
  }

  imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);



  //GUARDAR LA IMAGEN, SI LA OPERACION TUVO EXITO RETORNA TRUE
  switch($type){
    case 'bmp': $b = imagewbmp($new, $src); break;
    case 'gif': $b = imagegif($new, $src); break;
    case 'jpg': $b = imagejpeg($new, $src); break;
    case 'png': $b = imagepng($new, $src); break;
  }

  
  if($b == false){
  	//SI NO TUVO EXITO SE DEBE BORRAR LA IMAGEN
  	try{
			unlink('personasLogo/'.$filename);

		} catch(\Exception $e){
			return response()->json(['success' => false, 'errors' => 'Ocurrio un error al procesar la imagen.']);
		}

		return response()->json(['success' => false, 'errors' => 'Ocurrio un error al intentar guardar la imagen']);

  } else {
  	//RESPUESTA DE EXITO 		
  return response()->json(['success'=>true,'archivo'=>$filename,'file' => asset('personasLogo/' . $filename)]);
  } 
	


	}//else

  



	}//function







public function productoIconUpload(){



	//ALTO Y ANCHO DEFINIDO 
	 $width  = 512;
	 $height = 512;
	 $ok = 0;
  //FILTRAR POR FORMATO DE LA IMAGEN	
  $file = Input::file('image');
		$input = array('image' => $file);
		$rules = array(
			'image' => 'image',
			'image' => 'mimes:png,jpeg|max:1000'
		);
		$validator = Validator::make($input, $rules);

		if ( $validator->fails() )
		{
			return Response::json(['success' => false, 'errors' => 'El formato de la imagen no es soportado.']);

		}else {
			//OBTENER EXTENSION DE ARCHIVO
			$mime = Input::file('image')->getMimeType();
			$type = explode('/',$mime);
			$type = $type[1];
		}


	//GUARDAR Y OBTENER LA DIRECCION DEL ARCHIVO
  	$filename = $this->getId4Log().'.'.$type;
  	$src = public_path('iconosVoucher/' . $filename);
  	Image::make($file->getRealPath())->save($src);
  	


   //OBTENER ANCHO Y ALTO		
  if(!list($w, $h) = getimagesize($src)){
  	$ok++;
  } 
 

  

	//SI LA IMAGEN NO DEBE SER MAYOR A LAS DIMENSIONES ESTABLECIDAS
	if($w > $width || $h > $height){
		 $ok++;
	}




  if($ok > 0){
  	//SI NO TUVO EXITO SE DEBE BORRAR LA IMAGEN
  	try{
			unlink('iconosVoucher/'.$filename);

		} catch(\Exception $e){
			return response()->json(['success' => false, 'errors' => 'Ocurrio un error al procesar la imagen.']);
		}

		return response()->json(['success' => false, 'errors' => 'Ocurrio un error al intentar guardar la imagen']);

  } else {
  	//RESPUESTA DE EXITO 		
  return response()->json(['success'=>true,'archivo'=>$filename,'file' => asset('iconosVoucher/' . $filename)]);
  } 



}

public function depositoUploadComprobante(Request $request){

	$name = '';
	$imagen = false;
	$directorioImagen = 'comprobante_depositos';
	$resp = new \StdClass;
	$resp->err = true;


	 ini_set("gd.jpeg_ignore_warning", 1);
	 $adjunto = $request->file('image');
	 $input = array('image' => $adjunto);
	 $rules = array(
		 'image' => 'image',
		 'image' => 'mimes:pdf,xls,doc,docx,pptx,pps,jpeg,bmp,png,jpg|max:20000'
	 );
	 $validator = Validator::make($input, $rules);
	 if ( $validator->fails() ){	
		throw new ExceptionCustom("Ocurrio un error al intentar guardar la imagen");
	 } 

	 $filename = $adjunto->getClientOriginalName();

	//NUMERO ALEATIRIO CON FECHA
	 $files = $this->getId4Log().'_'.$filename;

	 //CONFIGURADO EN EL APP/ FILE SYTEM
	 Storage::disk('adjuntoDeposito')->put($files, \File::get($adjunto));


	 $file = url('comprobante_depositos/'.$files);

	 return response()->json(['success' => true, 'archivo' => $file , 'numero'=> 123, 'file' => $files]);
		

}//function

public function depositoUploadComprobanteDelete(Request $req){
	$mensaje = array('rsp'=>true);

		try{
	
			unlink('comprobante_depositos/'.$req->adjunto);
		} catch(\Exception $e){
			$mensaje = array('rsp'=>false);
		}

		return response()->json($mensaje);
}

public function deleteIconProducto(Request $req){

	$mensaje = array('rsp'=>true);

	try{
			if($req->input('idProducto') != '')
		    DB::table('productos')->where('id',$req->input('idProducto'))->update(['icono'=> null]);

			unlink('iconosVoucher/'.$req->input('dataFile'));
		} catch(\Exception $e){
			$mensaje = array('rsp'=>false);
		}

		return response()->json($mensaje);


}





	/**
	 * PARA ELIMINAR LAS IMAGENES DE PERSONAS CON UPDATE EN TABLA PERSONAS
	 * @param  Request $req RECIBE LA DIRECCION
	 * @return  RETORNA TRUE O FALSE 
	 */
	public function fileDeletePersona(Request $req){

		$mensaje = array('rsp'=>true);
		try{
			if($req->input('idPersona') != '')
		    DB::table('personas')
				->where('id',$req->input('idPersona'))
				->update(['logo'=> 'factour.png',
						  'id_usuario'=>$this->getIdUsuario()]);

			unlink('personasLogo/'.$req->input('dataFile'));
		} catch(\Exception $e){
			$mensaje = array('rsp'=>false);
		}

		return response()->json($mensaje);
	}

	public function fileDelete(Request $req){
		unlink('uploads/'.$req->input('file'));
		$mensaje = "La imagen fue eliminada exitosamente";
		return json_encode($mensaje);
	}

	public function fileDelDTPlus(Request $req){
		AdjuntoDocumento::destroy($req->input('dataId'));
		unlink('adjuntoDetalle/'.$req->input('dataFile'));
		$id = $req->input('dataId');

		return json_encode($id);
	}

	public function fileEliminar(Request $req){
		unlink('grupos/'.$req->input('dataFile'));
		$mensaje = true;
		return json_encode(['msg'=>$mensaje]);
	}

	public function fileImageAgencia(Request $req){
		unlink($_SERVER['DOCUMENT_ROOT']."/dtp_web/public/agencias/".$req->input('file'));
		$mensaje = "La imagen fue eliminada exitosamente";
		return json_encode($mensaje);
	}

	public function doGenenarArchivo(Request $req){
		echo '<pre>';
		print_r(public_path(). "/proveedoresHoteles.php");
		$archivo = fopen(public_path(). "/proveedoresHoteles.php", "w") or die("No se pudo abrir el archivo!");

		$usuarios = Usuario::where('id_usuario', '=',Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario)->get();
		
		foreach ($usuarios as $key=>$usuario){
			echo '<pre>';
			print_r($usuario->id_sistema_facturacion);
			fwrite($archivo,$usuario->id_sistema_facturacion);
			fwrite($archivo,$usuario->usuario);
			fwrite($archivo,$usuario->nombre_apellido);
		}

    	fclose($archivo);
    	/*$file= public_path(). "/proveedoresHoteles.php";
 // the file to change
		$search = 'Hi 2'; // the content after which you want to insert new stuff
		$insert = 'Hi 3'; // your new stuff

		$replace = $search. "\n". $insert;

		file_put_contents($file, str_replace($search, $replace, file_get_contents($file)));*/
	}	


	public function subir(Request $req){
		if($req->input('agencia') != "0"){	
			$file = Input::file('image');
			$input = array('image' => $file);
			$rules = array(
				'image' => 'image',
				'image' => 'required|mimes:png',
				'image' => 'dimensions:width=118,height=70'
			);
			$validator = Validator::make($input, $rules);
			if ( $validator->fails() )
			{
				return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);

			} else {

				$filename = $req->input('agencia').'.png';

				$path = public_path('agencias/' . $filename);

				Image::make($file->getRealPath())->save($path);

				return Response::json(['success' => true, 'archivo' => $filename , 'file' => asset('agencias/' . $filename)]);
			}
		}else{
				return Response::json(['success' => false, 'errors' => 'Seleccione una agencia']);
		}	
	}	

	public function reporteCierrePdf(Request $req, $id){
		$cierreCajas = DB::table('vw_cierre_caja');
		$cierreCajas = $cierreCajas->where('id',$id);
		$cierreCajas = $cierreCajas->where('activo',true);
		$cierreCajas = $cierreCajas->get();
		/*echo '<pre>';
		print_r($cierreCajas);*/
		$idPersona = isset($cierreCajas[0]) ? $cierreCajas[0]->persona_id : null;

		$cierreCajaDetalles = DB::table('vw_resumen_cierre_caja');
		$cierreCajaDetalles = $cierreCajaDetalles->where('id_cierre',$cierreCajas[0]->id);
		$cierreCajaDetalles = $cierreCajaDetalles->get();

		//REPORTE ANTICIPOS
		$cierreCajasAnticipo = DB::table('vw_cierre_caja_anticipos');
		$cierreCajasAnticipo = $cierreCajasAnticipo->where('id_usuario_aplicado',$idPersona);
		$cierreCajasAnticipo = $cierreCajasAnticipo->where('id_cierre',$cierreCajas[0]->id);
		$cierreCajasAnticipo = $cierreCajasAnticipo->get();

		//RECIBOS ANULADOS
		$cierreCajasRecibo_anulados = DB::table('vw_cierre_caja_recibos_anulados');
		$cierreCajasRecibo_anulados = $cierreCajasRecibo_anulados->where('id_usuario_aplicado',$idPersona);
		$cierreCajasRecibo_anulados = $cierreCajasRecibo_anulados->where('id_cierre',$cierreCajas[0]->id);
		$cierreCajasRecibo_anulados = $cierreCajasRecibo_anulados->get();

		 //return view('pages.mc.cobranzas.resumenCaja',compact('cierreCajas', 'cierreCajaDetalles'));
		$pdf = \PDF::loadView('pages.mc.cobranzas.resumenCaja',compact('cierreCajas', 'cierreCajaDetalles','cierreCajasAnticipo','cierreCajasRecibo_anulados'));
		$pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true,'enable_css_float'=>true]);
		$pdf->setPaper('a4', 'letter')->setWarnings(false);
		return $pdf->download('Resumen Cierre Caja.pdf');

	}	


	public function migracionDatos(Request $request){
		return view('pages.mc.voucher.migrar');
	}

	public function doASubir(Request $request){
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 1640); //240 segundos = 4 minutos

		if($request->input('id_dato') == 'PRV'){
			$path = $request->file('file')->getRealPath();
			$data = Excel::load($path, function($reader) {
            									})->get();

			$data = $data->toArray();
			foreach($data as $key=>$proveedor){
				$nombre = $proveedor['nombre'];
				$denominacion = $proveedor['denominacion_comercial'];
				$ruc = $proveedor['ruc'];
				$email = $proveedor['email'];
				$celular = $proveedor['celular'];
				$telefono = $proveedor['telefono'];
				$personeria = $proveedor['personeria_fisica1juridica2'];
				$tipo_documento = $proveedor['tip_docuento_c.i1_ruc2pasaporte3_di4'];
				$facturacion = $proveedor['facturacion_neto2_bruta_1'];
				$pais = $proveedor['pais'];
				$direccion = $proveedor['direccion'];
				$ciudad = $proveedor['ciudad'];
				$plazo_pago = $proveedor['plazo_pago'];
				$plazo = $proveedor['plazo'];
				$correos_comerciales = $proveedor['correos_comerciales'];
				$correos_administrativos = $proveedor['correos_administrativos'];

				$ciudad = DB::select("SELECT id FROM ciudades WHERE LOWER(denominacion) = LOWER('".$ciudad."')");

				if(isset($ciudad[0]->id)){
					$id_ciudad = $ciudad[0]->id;
				}else{
					$id_ciudad = 0;
				}

				$pais = DB::select("SELECT cod_pais FROM paises WHERE LOWER(denominacion) LIKE LOWER('".$pais."')");

				if(isset($pais[0]->cod_pais)){
					$id_pais = $pais[0]->cod_pais;
				}else{
					$id_pais = null;
				}

				if($plazo_pago == 'PRE-PAGO'){
					$id_plazo_pago = 6;

				}
				if($plazo_pago == 'CREDITO'){
					$id_plazo_pago = 16;
				}

				$persona = new Persona;
				$persona->nombre =$nombre;
				$persona->denominacion_comercial = $denominacion;
				$persona->documento_identidad = $ruc;
				$persona->email = $email;
				$persona->celular = $celular;
				$persona->telefono = $telefono;
				$persona->id_personeria = $personeria;
				$persona->id_tipo_facturacion = $tipo_documento;
				$persona->id_tipo_facturacion = $facturacion;
				$persona->id_pais = $id_pais;
				$persona->direccion = $direccion;
				$persona->id_ciudad = $id_ciudad;
				$persona->id_plazo_pago = $id_plazo_pago;
				$persona->id_plazo_cobro = null;
				$persona->correo_comerciales = $correos_comerciales;
				$persona->correo_administrativo = $correos_administrativos;
				$persona->id_tipo_persona = 14;
				$persona->activo = true;
				$persona->id_sucursal_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia;
				$persona->id_persona = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
				$persona->id_empresa = $this->getIdEmpresa();
			    $persona->save();
			}
			flash('Se han migrado los datos de los proveedores exitosamente')->success();
			return redirect()->route('personaIndex');


		}

		if($request->input('id_dato') == 'CLI'){
			$path = $request->file('file')->getRealPath();
			$data = Excel::load($path, function($reader) {
            									})->get();
			$data = $data->toArray();
			foreach($data as $key=>$cliente){
				$nombre = $cliente['nombre'];
				$apellido = $cliente['apellido'];
				$ruc = $cliente['ruc'];
				$vendedor = $cliente['vendedor'];
				$vendedor_id = Persona::where('email', $vendedor)->first(['id']);
				if(isset($vendedor_id->id)){
					$id_vendedor = $vendedor_id->id;
				}else{
					$id_vendedor = 0;
				}
				$email = $cliente['email'];
				$celular = $cliente['celular'];
				$telefono = $cliente['telefono'];
				$personeria = $cliente['personeria_fisica1juridica2'];
				$facturacion = $cliente['facturacion_neto2_bruta_1'];
				$direccion = $cliente['direccion'];

				$persona = new Persona;
				$persona->nombre = $nombre;
				$persona->apellido = $apellido;
				$persona->documento_identidad = $ruc;
				$persona->id_vendedor_empresa = $id_vendedor;
				$persona->email = $email;
				$persona->celular = $celular;
				$persona->telefono = $telefono;
				$persona->id_personeria = $personeria;
				$persona->id_tipo_facturacion = $facturacion;
				$persona->direccion = $direccion;
				$persona->id_tipo_persona = 11;
				$persona->activo = true;
				$persona->id_sucursal_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia;
				$persona->id_persona = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
				$persona->id_empresa = $this->getIdEmpresa();
				$persona->save();
			}	
			flash('Se han migrado los datos de los proveedores exitosamente')->success();
			return redirect()->route('personaIndex');

		}

		if($request->input('id_dato') == 'LC'){
			$path = $request->file('file')->getRealPath();
			$data = Excel::load($path, function($reader) {
            									})->get();
			$data = $data->toArray();
			foreach($data as $key=>$lc){
				$numero_documento = $lc['numero_documento'];
				 $listados = LibroCompra::where('nro_documento',$numero_documento)	
										->where('id_empresa',$this->getIdEmpresa())
										->first(); 
				
				if(!isset($listados->nro_documento)){
					$tipo_documento = $lc['tipo_documento_5_factura_contado_22_factura_credito_32nota_de_credito'];
					$numero_documento = $lc['numero_documento'];
					$importe = $lc['importe'];
					if($lc['importe'] == ""){
						$importe = 0;
					}else{
						$importe = $lc['importe'];
					}
					$tipo_factura = $lc['tipo_factura1factura_credito2nota_de_credito_5factura_contado'];
					$origen = $lc['origen_mmanual_aautomatico'];
					if($lc['exenta'] == ""){
						$exenta = 0;
					}else{
						$exenta = $lc['exenta'];
					}

					if($lc['gravadas_5'] == ""){
						$gravadas_5 = 0;
					}else{
						$gravadas_5 = $lc['gravadas_5'];
					}

					if($lc['gravadas_10'] == ""){
						$gravadas_10 = 0;
					}else{
						$gravadas_10 = $lc['gravadas_10'];
					}
					$sucursal = $lc['sucursal_matriz'];
					$iva_10 = $lc['iva_10'];
					$iva_5 = $lc['iva_5'];
					$moneda = $lc['moneda_usd_pyg']; 
					if($lc['fecha_retencion'] !== null){
						$fecha_retencion = $lc['fecha_retencion']->format('Y-m-d');
					}else{
						$fecha_retencion = null;
					}
					if($lc['iva_retencion'] == ""){
						$iva_retencion = 0;
					}else{
						$iva_retencion = $lc['iva_retencion'];
					}

					if($lc['cambio'] == ""){
						$cambio = 0;
					}else{
						$cambio = $lc['cambio'];
					}

					$concepto = $lc['concepto'];
					if($lc['fecha_vencimiento'] !== null){
						$fecha_vencimiento = $lc['fecha_vencimiento']->format('Y-m-d');
					}else{
						$fecha_vencimiento = null;
					}
					$proveedor = $lc['proveedor'];
					$cod_confirmacion = $lc['cod._confirmacion'];
					$producto = $lc['producto'];
					$numero_timbrado = $lc['numero_timbrado'];
					$desde_timbrado = $lc['desde_timbrado'];
					$hasta_timbrado = $lc['hasta_timbrado'];
					if($lc['saldo'] == ""){
						$saldo = 0;
					}else{
						$saldo = $lc['saldo'];
					}
					$id_proforma = $lc['id_proforma'];
					$cuenta_gravada_10 = $lc['cuenta_gravada_10'];
					$cuenta_gravada_5 = $lc['cuenta_gravada_5'];
					$cuenta_exenta = $lc['cuenta_exenta'];
					$centro_costo = $lc['centro_costo'];
					$fecha_alta = $lc['fecha_alta'];
					if($lc['total_retencion'] == ""){
						$total_retencion = 0;
					}else{
						$total_retencion = $lc['total_retencion'];
					}

					$genero_retencion = $lc['genero_retencion_true_false'];
					if($lc['fecha_hora_facturacion'] !== null){
						$fecha_hora_facturacion =  $lc['fecha_hora_facturacion']->format('Y-m-d');
					}else{
						$fecha_hora_facturacion = null;
					}

					$ruc = $lc['ruc'];
					if($lc['costo_exenta'] == ""){
						$costo_exenta = 0;
					}else{
						$costo_exenta = $lc['costo_exenta'];
					}
					if($lc['costo_gravada'] == ""){
						$costo_gravada = 0;
					}else{
						$costo_gravada = $lc['costo_gravada'];
					}

					$fondo_fijo = $lc['fondo_fijo_true_false'];
					$documento_hechauka = $lc['documento_hechauka_14compras_15ventas_12anulacion_pago_proveedores_nc'];
					$migrado_factura = $lc['migrado_factura_false_por_defecto'];
					$pasajero = $lc['pasajero'];
					if($sucursal == 'BP'){
						$id_sucursal = 58824;
					}

					if(trim($moneda) == 'PYG'){
						$id_moneda = 111;
					}
					if(trim($moneda) == 'USD'){
						$id_moneda = 143;
					}
					if(trim($moneda) == 'BRL'){
						$id_moneda = 21;
					}
				    
     				$base_documento = explode('-',$ruc);
					$proveedores = Persona::where('documento_identidad',$base_documento[0])->where('id_empresa',$this->getIdEmpresa())->first();

					if(isset($proveedores->id)){
						$id_proveedor = $proveedores->id;
					}else{
						$base_documento = explode('-',$ruc);
						if(isset($base_documento[1])){
							$dv = $base_documento[1];
						}else{
							$dv = null;
						}
						$personaProveedor = new Persona;
						$personaProveedor->nombre = $proveedor;
						$personaProveedor->dv = $dv;
						$personaProveedor->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
						$personaProveedor->documento_identidad = $base_documento[0];
						$personaProveedor->id_tipo_facturacion = 1;
						$personaProveedor->id_tipo_persona = 14;
						$personaProveedor->id_personeria = 1;
						$personaProveedor->save();
						$id_proveedor = $personaProveedor->id; 
						$id_proveedor = 0;
					}

					if($numero_timbrado != ""){
						$timbradoB = Timbrado::where('numero', $numero_timbrado)->where('id_empresa',$this->getIdEmpresa())->first(['id']);
						if(isset($timbradoB->id)){
							$id_timbrado = $timbradoB->id;
						}else{
							if($lc['desde_timbrado'] !== null){
								$desde_timbrado = $lc['desde_timbrado']->format('Y-m-d');
							}
							if($lc['hasta_timbrado'] !== null){
								$hasta_timbrado = $lc['hasta_timbrado']->format('Y-m-d');
							}
							$timbradoSave = new Timbrado;
							$timbradoSave->numero = $timbrado;
							$timbradoSave->fecha_inicio = $desde_timbrado;
							$timbradoSave->vencimiento = $hasta_timbrado;
							$timbradoSave->id_tipo_timbrado = 1;
							$timbradoSave->activo = true;
							$timbradoSave->id_sucursal_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia;
							$timbradoSave->usuario_id = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
							$timbradoSave->id_empresa = $this->getIdEmpresa();
							$timbradoSave->save();
							$id_timbrado = $timbradoSave->id;
						}
					}else{
						$id_timbrado = null;
					}

					$libroCompra = new LibroCompra;
					$libroCompra->id_tipo_documento = $tipo_documento;
					$libroCompra->nro_documento = $numero_documento;
					$libroCompra->importe = $importe;
					$libroCompra->id_tipo_factura = $tipo_factura;
					$libroCompra->origen = $origen;
					$libroCompra->exenta = floatval(str_replace(',','.',$exenta));
					$libroCompra->gravadas5 = floatval(str_replace(',','.', $gravadas_5));
					$libroCompra->gravadas10 = floatval(str_replace(',','.', $gravadas_10));
					$libroCompra->id_sucursal = $id_sucursal;
					$libroCompra->iva10 = floatval(str_replace(',','.',$iva_10));
					$libroCompra->iva5 = floatval(str_replace(',','.', $iva_5));
					$libroCompra->id_moneda = $id_moneda;
					$libroCompra->fecha_retencion = $fecha_retencion;
					$libroCompra->iva_retencion = floatval(str_replace(',','.',$iva_retencion));
					$libroCompra->cambio = floatval(str_replace(',','.', $cambio));
					$libroCompra->concepto = $concepto;
					$libroCompra->fecha_prim_ven = $fecha_vencimiento;
					$libroCompra->id_persona = $id_proveedor;
					$libroCompra->cod_confirmacion = $cod_confirmacion;
					$libroCompra->id_timbrado = $id_timbrado;
					$libroCompra->saldo = $saldo;
					$libroCompra->id_proforma = $id_proforma;
					$libroCompra->id_cuenta_gravada_10 = $cuenta_gravada_10;
					$libroCompra->id_cuenta_gravada_5 = $cuenta_gravada_5;
					$libroCompra->id_cuenta_exenta = $cuenta_exenta;
					$libroCompra->id_centro_costo = $centro_costo;
					$libroCompra->fecha = date('Y-m-d');
					$libroCompra->fecha_hora_facturacion = $fecha_hora_facturacion;
					$libroCompra->total_retencion = floatval(str_replace(',','.', $total_retencion));
					$libroCompra->genero_retencion = $genero_retencion;
					$libroCompra->ruc = $ruc;
					$libroCompra->costo_exenta = floatval(str_replace(',','.', $costo_exenta));
					$libroCompra->costo_gravada =floatval(str_replace(',','.', $costo_gravada));
					$libroCompra->cotizacion_contable_venta = floatval(str_replace(',','.', $cambio));
					$libroCompra->fondo_fijo = $fondo_fijo;
					$libroCompra->id_tipo_documento_hechauka = $documento_hechauka;
					$libroCompra->migrado_factura = $migrado_factura;
					$libroCompra->pasajero = $pasajero;
					$libroCompra->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa; 
					$libroCompra->save();
				}else{
					echo '<pre>';
					print_r('Duplicado');
				}
			}
			flash('Se han migrado los datos de los libros de compras exitosamente')->success();
			return redirect()->route('indexLibrosCompras');

		}
		if($request->input('id_dato') == 'LV'){
			$path = $request->file('file')->getRealPath();
			$data = Excel::load($path, function($reader) {
            									})->get();

			$data = $data->toArray();
			foreach($data as $key=>$lv){
				$numero_documento = $lv['numero_documento'];
				$documento_cliente = $lv['documento_cliente'];
				$base_documento = explode('-',$documento_cliente);
				$clienteBase = Persona::where('documento_identidad', $base_documento[0])->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first(['id']);
				if(isset($clienteBase->id)){
					$idControl = $clienteBase->id;
				}else{
					$idControl = 0;
				}
				$baseLibroVenta = LibroVenta::where('nro_documento', $numero_documento)->where('cliente_id', $idControl)->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first(['id']);

				if(!isset($baseLibroVenta->id)){
					/*echo '<pre>';
					print_r($lv);*/
					$timbrado = $lv['timbrado'];
					$timbradoB = Timbrado::where('numero', $timbrado)->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first(['id']);
					if(isset($timbradoB->id)){
						$id_timbrado = $timbradoB->id;
					}else{
						if($lv['desde_timbrado'] !== null){
							$desde_timbrado = $lv['desde_timbrado']->format('Y-m-d');
						}else{
							$desde_timbrado = null;
						}
						if($lv['hasta_timbrado'] !== null){
							$hasta_timbrado = $lv['hasta_timbrado']->format('Y-m-d');
						}else{
							$hasta_timbrado = null;
						}
						$timbradoSave = new Timbrado;
						$timbradoSave->numero = $timbrado;
						$timbradoSave->fecha_inicio = $desde_timbrado;
						$timbradoSave->vencimiento = $hasta_timbrado;
						$timbradoSave->id_tipo_timbrado = 1;
						$timbradoSave->activo = true;
						$timbradoSave->id_sucursal_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia;
						$timbradoSave->usuario_id = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
						$timbradoSave->id_empresa = $this->getIdEmpresa();
						$timbradoSave->save();
						$id_timbrado = $timbradoSave->id;
					}
					$fecha_documento = $lv['fecha_documento']->format('Y-m-d');
					$estado_cobro = $lv['estado_cobro_31_pendiente_40_cobrado'];
					$estado_documento = $lv['estado_documento_29_facturado36generado'];
					if($lv['check_in'] !== null){
						$check_in = $lv['check_in']->format('Y-m-d');
					}else{
						$check_in = null;
					}
					if($lv['check_out'] !== null){
						$check_out = $lv['check_out']->format('Y-m-d');
					}else{
						$check_out = null;
					}
					$moneda = $lv['moneda_venta_pyg_usd'];

					if(trim($moneda) == 'PYG'){
						$id_moneda = 111;
					}
					if(trim($moneda) == 'USD'){
						$id_moneda = 143;
					}
					if(trim($moneda) == 'BRL'){
						$id_moneda = 21;
					}

					$cotizacion = $lv['cotizacion'];
					if($lv['vencimiento'] !== null){
						$vencimiento = $lv['vencimiento']->format('Y-m-d');
					}
					$destino = $lv['destino'];
					$documento_cliente = $lv['documento_cliente'];
					$cliente = $lv['cliente'];
					$base_documento = explode('-',$documento_cliente);
					$clienteBase = Persona::where('documento_identidad', $base_documento[0])->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first(['id']);
					if(isset($clienteBase->id)){
						$id_cliente = $clienteBase->id; 
					}else{
						if(isset($base_documento[1])){
							$dv = $base_documento[1];
						}else{
							$dv = null;
						}
						$personaCliente = new Persona;
						$personaCliente->nombre = $cliente;
						$personaCliente->dv = $dv;
						$personaCliente->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
						$personaCliente->documento_identidad = $base_documento[0];
						$personaCliente->id_tipo_facturacion = 1;
						$personaCliente->id_tipo_persona = 11;
						$personaCliente->id_personeria = 1;
						$personaCliente->save();
						$id_cliente = $personaCliente->id;
					}
					$total_bruto = $lv['total_bruto'];
					$total_neto = $lv['total_neto'];
					$facturacion = $lv['facturacion_neto2_bruta_1'];
					$factura_impresa = $lv['factura_impresa_true'];
					$vendedor = $lv['vendedor'];

					$vendedor_id = Persona::where('email', $vendedor)->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->first(['id']);
					if(isset($vendedor_id->id)){
						$id_vendedor = $vendedor_id->id;
					}else{
						$id_vendedor = 0;
					}
					$total_exentas = $lv['total_exentas'];
					$total_gravadas = $lv['total_gravadas'];
					$total_iva = $lv['total_iva'];
					$numero_proforma = $lv['numero_proforma'];
					$tipo_timbrado = $lv['tipo_timbrado_1_factura_2nota_credito'];
					$tipo_factura = $lv['tipo_factura_1credito_2contado'];
					$renta = $lv['renta'];
					$pasajero = $lv['pasajero'];
					$total_costo = $lv['total_costo'];
					$fecha_gasto = $lv['fecha_gasto'];
					$comentario = $lv['comentario'];
					$tarifa = $lv['tarifa_1comisionable2neto'];
					$tipo_factura_nc = $lv['tipo_fafactura_nc_nota_credito'];
					$importe = $lv['importe'];
					$saldo = $lv['saldo'];

					$libroVenta = new LibroVenta;
					$libroVenta->nro_documento = $numero_documento;
					$libroVenta->id_timbrado = $id_timbrado;
					$libroVenta->fecha_hora_documento = $fecha_documento;
					$libroVenta->id_estado_cobro = $estado_cobro;
					$libroVenta->id_estado_documento = $estado_documento;
					$libroVenta->check_in = $check_in;
					$libroVenta->check_out = $check_out;
					$libroVenta->id_moneda_venta = $id_moneda;
					$libroVenta->cotizacion_documento = $cotizacion;
					$libroVenta->vencimiento = $vencimiento;
					$libroVenta->destino_id = $destino;
					$libroVenta->cliente_id = $id_cliente;
					//$libroVenta->total_bruto_documento =  floatval(str_replace(',','.', str_replace('.','',$total_bruto)));
					//$libroVenta->total_neto_documento =  floatval(str_replace(',','.', str_replace('.','',$total_neto)));
					$libroVenta->total_bruto_documento = $total_bruto;
					$libroVenta->total_neto_documento =  $total_neto;
					$libroVenta->id_tipo_facturacion = $facturacion;
					$libroVenta->factura_impresa = $factura_impresa;
					$libroVenta->vendedor_id = $id_vendedor;
					/*$libroVenta->total_exentas =  floatval(str_replace(',','.', str_replace('.','',$total_exentas)));
					$libroVenta->total_gravadas =  floatval(str_replace(',','.', str_replace('.','',$total_gravadas)));
					$libroVenta->total_iva =  floatval(str_replace(',','.', str_replace('.','',$total_iva)));*/

					$libroVenta->total_exentas =  $total_exentas;
					$libroVenta->total_gravadas =  $total_gravadas;
					$libroVenta->total_iva =  $total_iva;
					$libroVenta->id_proforma = $numero_proforma;
					$libroVenta->id_tipo_timbrado = $tipo_timbrado;
					$libroVenta->id_tipo_factura = $tipo_factura;
					//$libroVenta->renta =  floatval(str_replace(',','.', str_replace('.','',$renta)));
					$libroVenta->renta = $renta;
					$libroVenta->pasajero_online = $pasajero;
					//$libroVenta->total_costos =  floatval(str_replace(',','.', str_replace('.','',$total_costo)));
					$libroVenta->total_costos = $total_costo;
					$libroVenta->fecha_gasto = $fecha_gasto;
					$libroVenta->comentario = $comentario;
					$libroVenta->id_tarifa = $tarifa;
					$libroVenta->tipo = $tipo_factura_nc;
					/*$libroVenta->importe = floatval(str_replace(',','.', str_replace('.','',$importe)));
					$libroVenta->saldo =  floatval(str_replace(',','.', str_replace('.','',$saldo)));*/
					$libroVenta->importe = $importe;
					$libroVenta->saldo =  $saldo;

					$libroVenta->senha = 0;
					if($tipo_factura_nc == 'FA'){
						$libroVenta->id_tipo_documento = 1;
						$libroVenta->id_tipo_documento_hechauka = 1;
					}
					if($tipo_factura_nc == 'NC'){
						$libroVenta->id_tipo_documento = 2;
						$libroVenta->id_tipo_documento_hechauka = 3;
					}

					$libroVenta->usuario_documento_id= Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
					$libroVenta->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario; 
					$libroVenta->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa; 
					$libroVenta->cotizacion_contable_compra = $cotizacion; 
				 	$libroVenta->save();
				}
			}	
			flash('Se han migrado los datos de los libros de ventas exitosamente')->success();
			return redirect()->route('indexLibrosVentas');
		}
	}

	public function subirBsp(Request $request){
		if($this->getIdEmpresa() == 1){
			return view('pages.mc.tickets.subirBsp');
		}else{
			flash('No tienes los permisos para acceder a esta dirección')->error();
            return Redirect::back();

		}
	}

	public function doSubirBsp(Request $request)
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', 1640); // 240 segundos = 4 minutos

		$path = $request->file('file')->getRealPath();
		$data = Excel::load($path)->get();
		$data = $data->toArray();
		$resultado = array();
		if (isset($data[0]['numero_tickt']) && $data[0]['numero_tickt'] !== "") {
			foreach ($data as $key => $datos) {
				$resultado[$key] = $datos;
				$getIdTicket = DB::select("SELECT public.get_ticket_id('".$datos['numero_tickt']."', ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
				if ($getIdTicket[0]->get_ticket_id != 0) {
					$tickets = DB::select("SELECT * FROM vw_ticket WHERE id = ".$getIdTicket[0]->get_ticket_id);
					if (!empty($tickets)) {
						$resultado[$key]['detalles'] = $tickets;
					}
				}
			}
		} else {
			flash('El formato del archivo no es correcto. ¡Inténtelo nuevamente!')->error();
			return Redirect::back();
		}

		return view('pages.mc.tickets.conciliacion', compact('resultado'));
	}

}
