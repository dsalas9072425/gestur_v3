<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Session;
use Redirect;
use App\Proforma;
use App\ProformasDetalle;
use App\Divisas;
use App\Persona;
use App\PlazoPago;
use App\HistoricoComentariosProforma;
use App\Voucher;
use App\TipoFactura;
use App\EstadoFactour;
use App\TipoPersona;
use App\Factura;
use App\FacturaDetalle;
use App\AdjuntoDocumento;
use App\Empresa;
use App\Producto;
use App\ComercioPersona;
use App\Negocio;
use App\LiquidacionComision;
use App\TipoTimbrado;
use Response;
use Image;
use DB;
use App\Grupos;
use App\SolicitudFacturaParcial;
use App\ProformaFacturaDetalle;
use App\Equipo;
use App\DestinoDtpmundo;
use Illuminate\Support\Facades\Cache;
use RobRichards\XMLSecLibs\XMLSecurityDSig;
use phpseclib\File\X509;
use phpseclib\File\Pkcs12;
use Log;
use Carbon\Carbon;
use App\DocumentosEcos;
use App\NotaCredito;
use App\Pais;
use App\Timbrado;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Support\Facades\Redis;

class FacturarController extends Controller {
  
  private $tiempo_cache = 0;
  private $id_empresa = 0;
  private $id_usuario = 0;

  function __construct(){
    /**
     * Vamos a fijar un cache de 1 hora para los datos de la empresa
     */
    $this->tiempo_cache = 60;
  }

  /**
   * Cuando se recupere el dato se va recuperar segun la empresa y el usuario si asi lo requiere, su uso es mejor cuando es para cargar las opciones
   * de las listas desplegables de los formularios que no sean muy grandes.
   * 
   * La key es el nombre que le damos a esa funcionalidad, la idea es poder reutilizar la key en consultas similares en el controaldor
   * El $variante es por ejemplo cuando la consulta implican datos de un usuario, entonces el $variante seria el id del usuario
   * 
   */
  private function idCacheUnique($key, $variante = ''){

    return $key . $variante .'_FacturarController_'. $this->getIdEmpresa();
   }
   

//====================PARAMETROS DE ESTADO FACTURA======================
    private $facturaFacturada = 29;
    private $facturaAnulada =  30;
    private $facturaVerificado = 2;
//===============================================================


//===========================ELIMINAR -  FECHA DE CREACION 26/06/19====================================



public function imprimirFacturaPrueba($idFactura){


          //===================DATOS DE LOGUEO=======================
          $id_usuario = $this->getIdUsuario();
          $id_empresa = $this->getIdEmpresa();
          //=========================================================


          $okFactura = 'OK';  
          $mensajeErr = "Error Desconocido";  
          $es_original = '0';
          $reimprimirUser = 'true';
          $imprimir = '0';
          $empresa = '0';
          $totalFactura = '0';
          $NumeroALetras = 'Cero';
          $factura = array('');
          $err = 0;

          //BUSCAR PERMISO PARA REIMPRIMIR ORIGINAL
          /*echo '<pre>';
          print_r($id_empresa);*/
          if($id_empresa == 1){
            $reImprimirOriginal = DB::table('persona_permiso_especiales')
                                      ->where('id_persona',$id_usuario)
                                      ->where('id_permiso',12)->count();
          }else{
            $reImprimirOriginal = 1;
          }  
          $factura = Factura::with('cliente',
                    'pasajero',
                    'vendedorAgencia',
                    'vendedorEmpresa',
                    'tipoFactura',
                    'timbrado',
                    'proforma',
                    'currency',
                    'ventaRapida')
                    ->where('id',$idFactura)->get();

          $facturaDetalle = FacturaDetalle::with('producto')
                    ->where('id_factura',$idFactura)
                    ->get(); 

          $contadorDetalle= count($facturaDetalle); 
                  
          $vendedorEmpresa = Persona::where('id',$factura[0]->id_usuario)->get();  
                    
              
          //Los datos estan completos y no hay error entonces seguir          
          if(!empty($factura) && !empty($facturaDetalle)){

          $empresa = Empresa::where('id',$id_empresa)->first();

          $sucursalEmpresa =  Persona::where('id_empresa',$id_empresa)
                                    ->where('id_tipo_persona', 21)
                                    ->where('activo', true)
                                    ->get();

          $pasajeroNombre = $this->getPasajero($factura[0]->id, null,$factura[0]->parcial );
          //saber si es bruto o neto
          $totalFactura = DB::select('SELECT * FROM get_monto_factura('.$factura[0]->id.')');
          $totalFactura = $totalFactura[0]->get_monto_factura;
                

            $NumeroALetras = $this->convertir($totalFactura);
          $estadoFactura = $factura[0]->id_estado_factura;
          $facturaImpresa = $factura[0]->factura_impresa;
              $id_factura = $factura[0]->id;


          //Posibilidad de imprimir copias de factura validas
          if($estadoFactura == $this->facturaVerificado || $estadoFactura == $this->facturaFacturada ){

            //Para reimprimir original en caso de tener permisos
            if($facturaImpresa == true && $reImprimirOriginal > 0){

              $imprimir = '1';

              //Para imprimir factura original  
            } else if($facturaImpresa == false){

              $imprimir = '1';

            //Actualizar los datos de factura luego del select
            $update = DB::table('facturas')
            ->where('id',$id_factura)
            ->update(['fecha_hora_impresion_factura'=>date('Y-m-d H:i:00'),
                  'factura_impresa'=>'true',
                  'id_usuario_impresion'=>$this->getIdUsuario()]);

            }//if

            //Para imprimir copia
            else if($facturaImpresa == true){

              $imprimir = '2';

            }//if

          }//if

          //Imprimir factura anulada
          else if($estadoFactura == $this->facturaAnulada) {
            $imprimir = '3';
            //dd($factura);
          } else{

            /*DEVOLVER ERROR O IMPRIMIR UNA PAGINA DE ERROR*/
              return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);

          } //else

                  } else {
                        return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);
                  }
                  
          if(isset($factura[0]->ventaRapida->id_comercio_empresa)){
            $idComercio = $factura[0]->ventaRapida->id_comercio_empresa; 
          }else{
            $idComercio = 0;
          }

          $comercioPersona= ComercioPersona::where('id', $idComercio)->get(['logo']);

          if(isset($comercioPersona[0]->logo)){
            $logo = $comercioPersona[0]->logo;
          }else{
            $logo = asset("logoEmpresa/$empresa->logo");
          }

          return view('pages.mc.factura.pruebaFactura')->with([
                                           'factura'=>$factura,
                                           'imprimir'=>$imprimir,
                                           'facturaDetalle'=>$facturaDetalle,
                                           'empresa'=>$empresa,
                                           'totalFactura'=>$totalFactura,
                                           'NumeroALetras'=>$NumeroALetras,
                                           'vendedorEmpresa'=>$vendedorEmpresa,
                                           'logo'=>$logo,
                                           'sucursalEmpresa'=>$sucursalEmpresa,
                                           'contadorDetalle'=>$contadorDetalle,
                                           'pasajeroNombre'=>$pasajeroNombre
                                           ]);
  }//function




//=====================================================================================================

  /**
   * Genera un pdf de vista previa de como seria los datos de la proforma en una factura
   * @param  idProforma
   * @return pdf de factura
   */
public function previewFactura($idProforma){

          $mensajeErr = "Probablemente no tenga cargado datos en la linea de proforma, si no es asi pongase en contacto con soporte técnico"; 
          $totalFactura = '0';
          $NumeroALetras = 'Cero';
          $indicadorProducto = 0;
          $id_empresa = $this->getIdEmpresa();

            $proformas = Proforma::with('cliente',
                      'pasajero',
                      'vendedor',
                      'tipoFacturacion',
                      'usuario',
                      'currency')->where('id',$idProforma)->get();

            $proformasDetalle = ProformaFacturaDetalle::where('id_proforma', $idProforma)
                      ->get();

            if(!isset($proformasDetalle[0])){
              $proformasDetalle = ProformasDetalle::with('producto')
                                                  ->where('id_proforma',$idProforma)
                                                  ->where('activo', '=' ,true) 
                                                  ->get();
              if(isset($proformasDetalle)){
                    foreach($proformasDetalle as $key =>$proformaDetalle){
                        if($proformaDetalle->producto->imprimir_en_factura == ''){
                           $indicadorProducto = $indicadorProducto + 1;
                        }
                    }
              }
             }else{
              $indicadorProducto = 1;
            }       
                      
                // dd($proformas[0]->id_moneda_venta);
          //Los datos estan completos y no hay error entonces seguir          
        if(!$proformas->isEmpty() && !$proformasDetalle->isEmpty()){

            $empresa = Empresa::where('id',$id_empresa)->first();
            $indicadorProducto = 0;
            $pasajero = $this->getPasajero(null, $idProforma);
            $totalFactura = DB::select('SELECT * FROM get_monto_proforma('.$idProforma.')');
            $totalFactura = $totalFactura[0]->get_monto_proforma;
            $NumeroALetras = $this->convertir($totalFactura);

                    } else {
                         return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);
                    }

                
             
                    // return view('pages.mc.factura.previewFactura',compact('proformas','proformasDetalle','totalFactura','NumeroALetras'));

            $pdf = \PDF::loadView('pages.mc.factura.previewFactura',
              compact('proformas','proformasDetalle','totalFactura','NumeroALetras', 'empresa','pasajero','indicadorProducto'));

             $pdf->setPaper('a4', 'letter')->setWarnings(false);
               return $pdf->download('Factura Proforma'.$this->getId4Log().'.pdf');


}//FUNCTION



/**
 * Metodo que imprime solo copia de factura recibe parametro id factura y id vendedor con hash
 * @param  $hash
 * @return archivo pdf
 */
public function facturaDtp($hash){

   // dd($hash);

          $id_empresa = 1;
          $mensajeErr = "Error Desconocido";  
          $imprimir = '2'; //Por defecto imprimir copias
          $empresa = '0';
          $totalFactura = '0';
          $NumeroALetras = 'Cero';
          $factura = array('');
          $err = 0;
          $idVendedorHash = '';
          $idFactura = 0;
          $md5 = '';
          $hash = explode('&',$hash);
        //  dd(isset($hash[1]));
          if(isset($hash[1])){
            // $hash = explode('&',$hash);

            $idFactura = $hash[0];
            $idVendedorHash = $hash[1];

            $empresa = Empresa::where('id',$id_empresa)->first();

            $sucursalEmpresa =  Persona::where('id_empresa',$id_empresa)
                                     ->where('id_tipo_persona', 21)
                                     ->where('activo', true)
                                     ->get();


            $factura = Factura::with('cliente',
                      'pasajero',
                      'vendedorAgencia',
                      'vendedorEmpresa',
                      'tipoFactura',
                      'timbrado',
                      'proforma',
                      'currency')
                      ->where('id',$idFactura)->get();
            
            /*$facturaDetalle = FacturaDetalle::with('producto')
                      ->where('id_factura',$idFactura)
                      ->get(); 

            $contadorDetalle= count($facturaDetalle); */

            $md5 = md5($factura[0]->vendedorEmpresa['id']);
                      // dd($md5,$idVendedorHash);

             if($md5 != $idVendedorHash){$err++;}            

          } else {$err++;}
          $pasajeroNombre = $this->getPasajero($factura[0]->id, null, $factura[0]->parcial);

           //Los datos estan completos y no hay error entonces seguir          
        if(!empty($factura) && $err == 0){

          $facturaDetalle = FacturaDetalle::with('producto')
                      ->where('id_factura',$idFactura)
                      ->get();  
          $contadorDetalle= count($facturaDetalle); 
          $vendedorEmpresa = Persona::where('id',$factura[0]->id_usuario)->get();    


          $empresa = Empresa::where('id',1)->first();


            //saber si es bruto o neto
            $totalFactura = DB::select('SELECT * FROM get_monto_factura('.$factura[0]->id.')');
            $totalFactura = $totalFactura[0]->get_monto_factura;
                 

            $NumeroALetras = $this->convertir($totalFactura);
            $estadoFactura = $factura[0]->id_estado_factura;


            //Imprimir factura anulada
             if($estadoFactura == $this->facturaAnulada) {
              $imprimir = '3';
          
            } 

            } else {

               // DEVOLVER ERROR O IMPRIMIR UNA PAGINA DE ERROR*/
              abort(404);
            }

            if(isset($factura[0]->ventaRapida->id_comercio_empresa)){
              $idComercio = $factura[0]->ventaRapida->id_comercio_empresa; 
            }else{
              $idComercio = 0;
            }

            $comercioPersona= ComercioPersona::where('id', $idComercio)->get(['logo']);

            if(isset($comercioPersona[0]->logo)){
              $logo = $comercioPersona[0]->logo;
            }else{
              $logo = asset("logoEmpresa/$empresa->logo");
            }

           /* if($id_empresa == 17){
              $this->generarJsonFactura($idFactura);
            }*/

            $pdf = \PDF::loadView('pages.mc.factura.facturaEsqueleto',
              compact('factura','imprimir','facturaDetalle','empresa','totalFactura','NumeroALetras','vendedorEmpresa','sucursalEmpresa','pasajeroNombre', 'logo','contadorDetalle'));

             $pdf->setPaper('a4', 'letter')->setWarnings(false);
               return $pdf->download('Factura Proforma'.$this->getId4Log().'.pdf');   



}

public function imprimirFactura($idFactura){
  //===================DATOS DE LOGUEO=======================
      $id_usuario = $this->getIdUsuario();
      $id_empresa = $this->getIdEmpresa();
  //=========================================================


    $okFactura = 'OK';  
    $mensajeErr = "Error Desconocido";  
    $es_original = '0';
    $reimprimirUser = 'true';
    $imprimir = '0';
    $empresa = '0';
    $totalFactura = '0';
    $NumeroALetras = 'Cero';
    $factura = array('');
    $err = 0;

    //BUSCAR PERMISO PARA REIMPRIMIR ORIGINAL
      /*echo '<pre>';
      print_r($id_empresa);*/
      if($id_empresa == 1){
        $reImprimirOriginal = DB::table('persona_permiso_especiales')
                                  ->where('id_persona',$id_usuario)
                                  ->where('id_permiso',12)->count();
        $reImprimirOriginal = 0;
      }else{
        $reImprimirOriginal = 0;
      }  
    /* echo '<pre>';
      print_r($reImprimirOriginal);
      die;*/

      $factura = Factura::with('cliente',
                'pasajero',
                'vendedorAgencia',
                'vendedorEmpresa',
                'tipoFactura',
                'timbrado',
                'proforma',
                'currency',
                'ventaRapida',
                'documentosEcos'
                )
                ->where('id',$idFactura)->get();
                
     /*echo '<pre>';
      print_r($factura); */
      $proformaFacturaDetalle = ProformaFacturaDetalle::where('id_proforma', $factura[0]->id_proforma)
                                              ->orderBy('id', 'asc')
                                              ->get();
      
      if(!isset($proformaFacturaDetalle[0])){
            $facturaDetalle = FacturaDetalle::with('producto')
                      ->where('id_factura',$idFactura)
                      ->orderBy('id', 'asc')
                      ->get(); 
            $indicadorProducto = 0;
              if(isset($facturaDetalle)){
                foreach($facturaDetalle as $key =>$detallesFacturas){
                //  echo '<pre>';
                //  print_r($detallesFacturas->producto->imprimir_en_factura.' V '.$detallesFacturas->precio_venta);
                  if($detallesFacturas->producto->imprimir_en_factura == '' && $detallesFacturas->precio_venta <= 0){
                    $indicadorProducto = $indicadorProducto + 1;
                  }
                }
              }
      }else{
        $facturaDetalle = $proformaFacturaDetalle;
        $indicadorProducto = 0;
      }        
      
      $contadorDetalle= count($facturaDetalle); 
              
      $vendedorEmpresa = Persona::where('id',$factura[0]->id_usuario)->get();  
                
          
    //Los datos estan completos y no hay error entonces seguir          
  if(!empty($factura) && !empty($facturaDetalle)){



    
    $empresa = Empresa::where('id',$id_empresa)->first();

    $sucursalEmpresa =  Persona::where('id_empresa',$id_empresa)
                              ->where('id_tipo_persona', 21)
                              ->where('activo', true)
                              ->get();

        $pasajeroNombre = $this->getPasajero($factura[0]->id, null, $factura[0]->parcial);
        $totalFactura = DB::select('SELECT * FROM get_monto_factura('.$factura[0]->id.')');
        $totalFactura = $totalFactura[0]->get_monto_factura;
          

        $NumeroALetras = $this->convertir($totalFactura);
        $estadoFactura = $factura[0]->id_estado_factura;
        $facturaImpresa = $factura[0]->factura_impresa;
          $id_factura = $factura[0]->id;


      //Posibilidad de imprimir copias de factura validas
      if($estadoFactura == $this->facturaVerificado || $estadoFactura == $this->facturaFacturada ){

        //Para reimprimir original en caso de tener permisos
        if($facturaImpresa == true && $reImprimirOriginal > 0){

          $imprimir = '1';

          //Para imprimir factura original  
        } else if($facturaImpresa == false){

          $imprimir = '1';

        //Actualizar los datos de factura luego del select
        $update = DB::table('facturas')
              ->where('id',$id_factura)
              ->update(['fecha_hora_impresion_factura'=>date('Y-m-d H:i:00'),
                    'factura_impresa'=>'true',
                    'id_usuario_impresion'=>$this->getIdUsuario()]);

        }//if

        //Para imprimir copia
        else if($facturaImpresa == true){

          $imprimir = '2';

        }//if

    }//if

      //Imprimir factura anulada
      else if($estadoFactura == $this->facturaAnulada) {
        $imprimir = '3';
        //dd($factura);
      } else{

        /*DEVOLVER ERROR O IMPRIMIR UNA PAGINA DE ERROR*/
        return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);

      } //else

              } else {
                  return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);
              }
              
      if(isset($factura[0]->ventaRapida->id_comercio_empresa)){
        $idComercio = $factura[0]->ventaRapida->id_comercio_empresa; 
      }else{
        $idComercio = 0;
      }

      $comercioPersona= ComercioPersona::where('id', $idComercio)->get(['logo']);

    //  $logoSucursal = DB::select('SELECT s.logo FROM personas p, personas s WHERE p.id = '.$factura[0]->id_usuario.' AND p.id_sucursal_empresa = s.id');

      $logoSucursal = DB::select('select personas.logo
                                  from personas, timbrados
                                  where personas.id = timbrados.id_sucursal_empresa
                                  and timbrados.id ='.$factura[0]->id_timbrado);

      
      if($logoSucursal[0]->logo != "" && $logoSucursal[0]->logo != 'factour.png'){
        $baseLogo = $logoSucursal[0]->logo;
        $logo = asset("personasLogo/$baseLogo");
      }else{
        if(isset($comercioPersona[0]->logo)){
          $logo = $comercioPersona[0]->logo;
        }else{
          $logo = asset("logoEmpresa/$empresa->logo");
        }
      }

      if(isset($factura[0]->nro_factura)){
        $nroFactura = $factura[0]->nro_factura;
      }else{
        $nroFactura = $this->getId4Log();
      }  

      $tipoFacturas = TipoTimbrado::where('id',$factura[0]->timbrado->id_tipo_timbrado)->first();  
      if(isset($tipoFacturas->denominacion)){
          $invoceFactura = strtoupper($tipoFacturas->denominacion);
      }else{
          $invoceFactura = "";
      }

    //  return view('pages.mc.factura.facturaEsqueleto',compact('factura','imprimir','facturaDetalle','empresa','totalFactura','NumeroALetras','vendedorEmpresa', 'sucursalEmpresa', 'logo','contadorDetalle','pasajeroNombre','indicadorProducto','reImprimirOriginal'));

      $fecha1 = new \DateTime(date('Y-m-d', strtotime($factura[0]->fecha_hora_facturacion)));
      $fecha2 = new \DateTime('2023-12-31');
      
      if ($fecha1 < $fecha2) {
         $plantilla = 'facturaEsqueleto';
      } else {
          // La fecha1 no es anterior (mayor o igual) que la fecha2
          $plantilla = $empresa->plantilla;
      }

      $pdf = \PDF::loadView('pages.mc.factura.'.$plantilla,
        compact('factura','imprimir','facturaDetalle','empresa','totalFactura','NumeroALetras','vendedorEmpresa', 'sucursalEmpresa', 'logo','contadorDetalle','pasajeroNombre','indicadorProducto','reImprimirOriginal','invoceFactura'));
      $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);

      $pdf->setPaper('a4', 'letter')->setWarnings(false);
      return $pdf->download('FACTURA_'.$nroFactura.'.pdf');
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public function imprimirFacturaOriginal($idFactura){
          
  //===================DATOS DE LOGUEO=======================
      $id_usuario = $this->getIdUsuario();
      $id_empresa = $this->getIdEmpresa();
  //=========================================================
    $okFactura = 'OK';  
    $mensajeErr = "Error Desconocido";  
    $es_original = '0';
    $reimprimirUser = 'true';
    $imprimir = '0';
    $empresa = '0';
    $totalFactura = '0';
    $NumeroALetras = 'Cero';
    $factura = array('');
    $err = 0;

    //BUSCAR PERMISO PARA REIMPRIMIR ORIGINAL
     $reImprimirOriginal = 1;
     $factura = Factura::with('cliente',
                          'pasajero',
                          'vendedorAgencia',
                          'vendedorEmpresa',
                          'tipoFactura',
                          'timbrado',
                          'proforma',
                          'currency',
                          'ventaRapida',
                          'documentosEcos'
                          )
     ->where('id',$idFactura)->get();
     
    /* echo '<pre>';
      print_r($factura);*/
      $proformaFacturaDetalle = ProformaFacturaDetalle::where('id_proforma', $factura[0]->id_proforma)
                                              ->orderBy('id', 'asc')
                                              ->get();
      
      if(!isset($proformaFacturaDetalle[0])){
            $facturaDetalle = FacturaDetalle::with('producto')
                      ->where('id_factura',$idFactura)
                      ->orderBy('id', 'asc')
                      ->get(); 
            $indicadorProducto = 0;
              if(isset($facturaDetalle)){
                foreach($facturaDetalle as $key =>$detallesFacturas){
              //     echo '<pre>';
              //    print_r($detallesFacturas->producto->imprimir_en_factura ." - ".$detallesFacturas->producto->id ); 
                  
                  if($detallesFacturas->producto->imprimir_en_factura == ''){
                    $indicadorProducto = $indicadorProducto + 1;
                  }
                }
              }
      }else{
        $facturaDetalle = $proformaFacturaDetalle;
        $indicadorProducto = 0;
      }        
      
      $contadorDetalle= count($facturaDetalle); 
              
      $vendedorEmpresa = Persona::where('id',$factura[0]->id_usuario)->get();  
                
          
    //Los datos estan completos y no hay error entonces seguir          
  if(!empty($factura) && !empty($facturaDetalle)){

    $empresa = Empresa::where('id',$id_empresa)->first();

    $sucursalEmpresa =  Persona::where('id_empresa',$id_empresa)
                              ->where('id_tipo_persona', 21)
                              ->where('activo', true)
                              ->get();

        $pasajeroNombre = $this->getPasajero($factura[0]->id, null, $factura[0]->parcial);
        $totalFactura = DB::select('SELECT * FROM get_monto_factura('.$factura[0]->id.')');
        $totalFactura = $totalFactura[0]->get_monto_factura;
          

        $NumeroALetras = $this->convertir($totalFactura);
        $estadoFactura = $factura[0]->id_estado_factura;
        $facturaImpresa = $factura[0]->factura_impresa;
          $id_factura = $factura[0]->id;


      //Posibilidad de imprimir copias de factura validas
      if($estadoFactura == $this->facturaVerificado || $estadoFactura == $this->facturaFacturada ){

        //Para reimprimir original en caso de tener permisos
        if($facturaImpresa == true && $reImprimirOriginal > 0){

          $imprimir = '1';

          //Para imprimir factura original  
        } else if($facturaImpresa == false){

          $imprimir = '1';

        //Actualizar los datos de factura luego del select
        $update = DB::table('facturas')
        ->where('id',$id_factura)
        ->update(['fecha_hora_impresion_factura'=>date('Y-m-d H:i:00'),
              'factura_impresa'=>'true',
              'id_usuario_impresion'=>$this->getIdUsuario()]);

        }//if

        //Para imprimir copia
        else if($facturaImpresa == true){

          $imprimir = '2';

        }//if

    }//if

      //Imprimir factura anulada
      else if($estadoFactura == $this->facturaAnulada) {
        $imprimir = '3';
        //dd($factura);
      } else{

        /*DEVOLVER ERROR O IMPRIMIR UNA PAGINA DE ERROR*/
        return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);

      } //else

              } else {
                  return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);
              }
              
      if(isset($factura[0]->ventaRapida->id_comercio_empresa)){
        $idComercio = $factura[0]->ventaRapida->id_comercio_empresa; 
      }else{
        $idComercio = 0;
      }

      $comercioPersona= ComercioPersona::where('id', $idComercio)->get(['logo']);

      $logoSucursal = DB::select('select personas.logo
                                  from personas, timbrados
                                  where personas.id = timbrados.id_sucursal_empresa
                                  and timbrados.id ='.$factura[0]->id_timbrado);

      
      if($logoSucursal[0]->logo != "" && $logoSucursal[0]->logo != 'factour.png'){
        $baseLogo = $logoSucursal[0]->logo;
        $logo = asset("personasLogo/$baseLogo");
      }else{
        if(isset($comercioPersona[0]->logo)){
          $logo = $comercioPersona[0]->logo;
        }else{
          $logo = asset("logoEmpresa/$empresa->logo");
        }
      }
    
      if(isset($factura[0]->nro_factura)){
        $nroFactura = $factura[0]->nro_factura;
      }else{
        $nroFactura = $this->getId4Log();
      }  
      $tipoFacturas = TipoTimbrado::where('id',$factura[0]->timbrado->id_tipo_timbrado)->first();  
      if(isset($tipoFacturas->denominacion)){
          $invoceFactura = strtoupper($tipoFacturas->denominacion);
      }else{
          $invoceFactura = "";
      }

      $fecha1 = new \DateTime(date('Y-m-d', strtotime($factura[0]->fecha_hora_facturacion)));
      $fecha2 = new \DateTime('2023-12-31');
      
      if ($fecha1 < $fecha2) {
         $plantilla = 'facturaEsqueleto';
      } else {
          // La fecha1 no es anterior (mayor o igual) que la fecha2
          $plantilla = $empresa->plantilla;
      }

      $pdf = \PDF::loadView('pages.mc.factura.'.$plantilla,        
      compact('factura','imprimir','facturaDetalle','empresa','totalFactura','invoceFactura','NumeroALetras',
                'vendedorEmpresa', 'sucursalEmpresa', 'logo','contadorDetalle','pasajeroNombre','indicadorProducto',
                'reImprimirOriginal'));
      $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true]);

      $pdf->setPaper('a4', 'letter')->setWarnings(false);
        return $pdf->download('FACTURA_'.$nroFactura.'.pdf');
}//function

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


public function facturarDtp($idProforma){



        //===================DATOS DE LOGUEO=======================
            $id_usuario = $this->getIdUsuario();
            $id_empresa = $this->getIdEmpresa();
        //=========================================================

        // dd((integer)$idProforma,$id_usuario);
          $okFactura = 'OK';  
          $mensajeErr = "Error Desconocido";  
          $id_factura = 2;
          $es_original = '0';
          $reimprimirUser = 'true';
          $imprimir = '0';
          $empresa = '0';
          $totalFactura = '0';
          $NumeroALetras = 'Cero';
          $factura = array('');
          $err = 0;



            $pos = strpos($idProforma, '_');

            if($pos === true){
            $Dato = explode('_', $idProforma);
            $idProforma = $Dato[0];
            $es_original = $Dato[1];
            }


            $reImprimirOriginal = Persona::where('id',$id_usuario)->get(['reimprimir_factura_original']);
            $reImprimirOriginal = (String)$reImprimirOriginal[0]->reimprimir_factura_original;
            $tieneFactura = Proforma::where('id',$idProforma)->get(['factura_id']);

            $sucursalEmpresa =  Persona::where('id_empresa',$id_empresa)
                                ->where('id_tipo_persona', 21)
                                ->where('activo', true)
                                ->get();


              //Proforma  esta facturado ?
            if($tieneFactura[0]->factura_id == null){
            try {   //Solicitar facturacion
                   $cargarFactura =  DB::select('select * from facturar(?,?)',[$idProforma,$id_usuario]);
                   $okFactura = (String)$cargarFactura[0]->facturar;
                  //dd($okFactura);
                 
                } catch(\Exception $e){ 
                  Log::error($e);
                  $err++;
                }


                }//Confirmacion de facturacion


            if($okFactura === 'OK' && $err == 0 ){

            $factura = Factura::with('cliente',
                      'pasajero',
                      'vendedorAgencia',
                      'vendedorEmpresa',
                      'tipoFactura',
                      'timbrado',
                      'proforma',
                      'currency')
                      ->where('id_proforma',$idProforma)->get();
                
            $facturaDetalle = FacturaDetalle::with('producto')
                      ->where('id_proforma',$idProforma)
                      ->get(); 

           // $vendedorEmpresa = Persona::with('vendedorEmpresa')->where('id',$factura[0]->cliente_id)->get();;
            $vendedorEmpresa = Persona::where('id',$factura[0]->id_usuario)->get();  
                      
                    } else {
                      //En caso de error
                      $mensajeErr = $okFactura;
                      $err++;
                      DB::table('proformas')->update(['estado_id'=>'3']);
                    }




              // dd($factura,$facturaDetalle);        
          //Los datos estan completos y no hay error entonces seguir          
        if(!empty($factura) && !empty($facturaDetalle) && $err == 0){


          $empresa = Empresa::where('id',$id_empresa)->first();


            //saber si es bruto o neto
            $totalFactura = DB::select('SELECT * FROM get_monto_factura('.$factura[0]->id.')');
            $totalFactura = $totalFactura[0]->get_monto_factura;
                 

              $NumeroALetras = $this->convertir($totalFactura);
            $estadoFactura = $factura[0]->id_estado_factura;
            $facturaImpresa = $factura[0]->factura_impresa;
                $id_factura = $factura[0]->id;


            //Posibilidad de imprimir copias de factura validas
            if($estadoFactura == $this->facturaVerificado || $estadoFactura == $this->facturaFacturada ){

              //Para reimprimir original en caso de tener permisos
              if($facturaImpresa == true && $reImprimirOriginal == '1'){

                $imprimir = '1';

              }//if

              //Para imprimir factura original
              else if($facturaImpresa == false){

                $imprimir = '1';

               //Actualizar los datos de factura luego del select
              $update = DB::table('facturas')
              ->where('id',$id_factura)
              ->update(['fecha_hora_impresion_factura'=>date('Y-m-d H:i:00'),
                    'factura_impresa'=>'true']);

              }//if

              //Para imprimir copia
              else if($facturaImpresa == true){

                $imprimir = '2';

              }//if

              



            }//if

            //Imprimir factura anulada
            else if($estadoFactura == $this->facturaAnulada) {
              $imprimir = '3';
              //dd($factura);
            } else{

              /*DEVOLVER ERROR O IMPRIMIR UNA PAGINA DE ERROR*/
               return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);

            } //else

                    } else {
                         return view('pages.mc.factura.error')->with(['mensajeErr'=>$mensajeErr]);
                    }

            $pdf = \PDF::loadView('pages.mc.factura.facturaEsqueleto',
              compact('factura','imprimir','facturaDetalle','empresa','totalFactura','NumeroALetras','vendedorEmpresa', 'sucursalEmpresa'));

             $pdf->setPaper('a4', 'letter')->setWarnings(false);
               return $pdf->download('Factura Proforma'.$this->getId4Log().'.pdf');


  
          


             // return view('pages.mc.facturar.facturaEsqueleto')->with(['factura'=>$factura,
             //                               'imprimir'=>$imprimir,
             //                               'facturaDetalle'=>$facturaDetalle,
             //                               'empresa'=>$empresa,
             //                               'totalFactura'=>$totalFactura,
             //                               'NumeroALetras'=>$NumeroALetras]);

}









    /**
     * Por defecto envia los datos al Reporte Detalle Proveedor 
     */
    public function reporteDetalleProveedor()
    {

       $getDivisa = Divisas::where('activo','S')->get();
       $getProveedor = Persona::where('id_tipo_persona','14')->where('id_empresa',$this->getIdEmpresa())->get();
       $plazoPago = DB::select("SELECT distinct tipo_plazo as n FROM plazos_pago ");
    
      return view('pages.mc.factura.reporteDetalleProveedor')->with(['monedas'=>$getDivisa,
                                                                     'getProveedor'=>$getProveedor,
                                                                     'plazoPago'=>$plazoPago]); 
    
    }//FUNCTION


   


    public function cancelarOperacion(Request $req){


      $arrayId = $req->listaId;
      $numOperacion = $req->numOp;
      $err = 0;


       try {   
            
            $this->actualizarFacturaDetallePagoProveedor($arrayId,$numOperacion);
                } catch(\Exception $e){
                 $err++;
               }
    

      return response()->json(['resp'=>$err]);
    }



    private function actualizarFacturaDetallePagoProveedor($arrayId, $numOp){

      $num_operacion = $numOp;

        foreach ($arrayId as $key => $value) {
          // echo $value[$key];

      $FacturaDetalle =  FacturaDetalle::find($value);
      $FacturaDetalle->nro_op = $num_operacion;
      $FacturaDetalle->pagado_proveedor = true;
      $FacturaDetalle->fecha_pagado_al_proveedor = date('Y-m-d');


         DB::transaction(function () use ($FacturaDetalle) {
          $FacturaDetalle->save();
       });

      }//foreach


    }//function



    public function comprobarOP(Request $req){

     
      $lineasFacturaDetalle = $req->lista;
      $monto = 0;
      $result = 0;
      $cantidadProcesada = 0;
      $cantidadSeleccionada = 0;

      $facturasDetalleProcesado = FacturaDetalle::whereIn('id',$lineasFacturaDetalle)->get();
      $facturasDetalleConfirmado = FacturaDetalle::with('factura')->whereIn('id',$lineasFacturaDetalle)->where('pagado_proveedor',true)->get();
     
      $lineas =  new \StdClass;
      $lineas->cantidadSeleccionada = (String)$facturasDetalleProcesado->count();
      $lineas->cantidadProcesada = (String)$facturasDetalleConfirmado->count();

      foreach ($facturasDetalleConfirmado as $key => $value) {

        if($value->factura->id_estado_factura == 29){

              $proforma = ProformasDetalle::where('id_proforma', $value->id_proforma)
                                            ->where('item', $value->item)
                                            ->first();

              $result = $proforma->costo_proveedor;
  
            }
              $monto = $result + $monto;

      }

     

       $lineas->monto = $monto;


      return response()->json(['resp'=>$lineas]);

    }//function



    /**
     * Recibe una fecha 2017-05-02 y pasa a 02/05/2017 
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaSalida($date){
        if( $date != ''){

        $date = explode('-', $date);
            $fecha = $date[2]."/".$date[1]."/".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }


    /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }

    private function getDatosUsuario(){
  
   $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
   return $persona = Persona::where('id',$idUsuario)->first();

  }


  private function getIdUsuario(){
    
    if(!$this->id_usuario){
      $this->id_usuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
    }
  
  return $this->id_usuario;
}

private function getIdEmpresa(){

  if(!$this->id_empresa){
     $this->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
  }
 return $this->id_empresa;
}
 

public function facturarOperativo(Request $req){ 

  //===================DATOS DE LOGUEO=======================
      $id_usuario = $this->getIdUsuario();
      $id_empresa = $this->getIdEmpresa();
  //=========================================================
      if($id_usuario == 59049 || $id_usuario == 60517 || $id_usuario == 58823|| $id_usuario == 59015 || $id_usuario == 59047 || $id_usuario == 60512 ){ 
        $caracter = '-';
        $posicion = strpos($req->input('rucSet'), $caracter);
        if ($posicion !== false) {
              $clienteRuc = explode('-',$req->input('rucSet'));
              $idPersona = $req->input('idPersona'); 

              DB::table('personas')->where('id',$idPersona)
                                  ->update(
                                            [
                                              'documento_identidad'=>$clienteRuc[0],
                                              'dv'=>$clienteRuc[1],
                                              'email'=>$req->input('mailSet')
                                            ]
                                          );    
        }else{
              $idPersona = $req->input('idPersona'); 
              DB::table('personas')->where('id',$idPersona)
                  ->update(
                            [
                              'documento_identidad'=>$req->input('rucSet'),
                              'dv'=>null,
                              'email'=>$req->input('mailSet')
                            ]
                          );    
        }
    }                           
    $okFactura = 'Error desconocido al intentar facturar.';  
    $resp = new \StdClass;
    $resp->e = '';
    $idProforma = $req->idProforma;
    $idPunto = $req->idPunto;
    $err = 0;
    $tieneFactura = Proforma::where('id',$idProforma)->get();

    //ACTUALIZAR SI NO ESTA EN ESTADO VERIFICADO Y FACTURADO
    if($tieneFactura[0]->estado_id != 2 && $tieneFactura[0]->factura_id == null){
          try{
            DB::table('proformas')->where('id',$idProforma)->update(['estado_id'=>2]);
          } catch(\Exception $e){
            Log::error($e);
              return response()->json(['err'=>false,'response'=> 'Factura ya existe o ocurrio un error en el cambio de estado.', 'e'=> $resp->e = '']);
          }
     }

        //PROFORMA NO DEBE TENER NUMERO DE FACTURA Y EL CAMBIO A VERIFICADO DEBE PODER REALIZARSE
      if($tieneFactura[0]->factura_id == null){

       try {   
             //Solicitar facturacion
             $cargarFactura =  DB::select('select * from facturar(?,?,?)',[$idProforma,$idPunto,$id_usuario]);
             $okFactura = (String)$cargarFactura[0]->facturar;


        } catch(\Exception $e){
          Log::error($e);

            $okFactura = $e->getMessage();
            return response()->json(['err'=>false, 'response'=>$okFactura, 'e'=>'']);             
       } 

          //VERIFICAR RESPUESTA DE LA FUNCION FACTURAR
           if($okFactura === 'OK'){
                $facturaId = Factura::where('id_proforma',$idProforma)->where('id_estado_factura','29')->first(['id']);
                $facturaId = $facturaId->id;
                return response()->json(['err'=>true,'idFactura'=>$facturaId]);

           } else {
               try { 
                  //CAMBIA A ESTADO A VERIFICADO SI OCURRE ERROR AL GENERAR
                    DB::table('proformas')
                    ->where('id',$idProforma)
                    ->update(['estado_id'=>2]);
                  } catch(\Exception $e){
                    Log::error($e);
                    if(env('APP_DEBUG')){
                      $resp->e = $e;
                    }

                  return response()->json(['err'=>false, 'response'=>'Ocurrio un error al generar la factura y cambiar el estado.', 'e'=>'']);
               }
               return response()->json(['err'=>false, 'response'=>$okFactura, 'e'=>'']);
           }//else
      }



}//function



/**
 * Listado de facturas
 * @return [type] [description]
 */
public function index(){

    $idEmpresa = $this->getIdEmpresa();

    /**
     * CACHE DE CONSULTAS, DETERMINAMOS UN TIEMPO MAXIMO DE 24 HORAS
     */
    $grupos = Cache::remember($this->idCacheUnique('grupos_factura'), $this->tiempo_cache, function () use ($idEmpresa) { 
            return  Grupos::where('estado_id', 11)
                    ->where('empresa_id',$idEmpresa)
                     ->get(['id','denominacion']);
        });

    $getTipoEstado = Cache::remember($this->idCacheUnique('estados_factura'), $this->tiempo_cache, function () use ($idEmpresa) { 
      return  EstadoFactour::where('id_tipo_estado','9')->get();
    });

    $getFacturaVendedorEmpresa = Cache::remember($this->idCacheUnique('factura_vendedor_factura'), $this->tiempo_cache, function () use ($idEmpresa) { 
      return  Factura::select('id_empresa','vendedor_id')->with(['vendedorEmpresa' => function($query) {
                  $query->select('id', 'nombre', 'apellido', 'denominacion_comercial');
              }])
            ->where('id_empresa', $idEmpresa)
            ->distinct()
            ->get();
    });


    $getFacturaUsuario = Cache::remember($this->idCacheUnique('factura_usuario_factura'), $this->tiempo_cache, function () use ($idEmpresa) { 
      return  Factura::select('id_usuario')
            ->where('id_empresa', $idEmpresa)
            ->whereHas('usuario')
            ->with('usuario:id,nombre,apellido') // selecciona los campos necesarios de la relación
            ->distinct()
            ->pluck('id_usuario');
    });

    $sucursalEmpresa = Persona::where('id_tipo_persona','21')->where('id_empresa',$idEmpresa)->where('activo','true')->orderBy('nombre','ASC')->get();
    $getDivisa = Divisas::where('activo','S')->get();
    $getTipoFactura = TipoFactura::get();


    if($idEmpresa == 1 || $idEmpresa == 4){
      $tipoPersona = TipoPersona::where('puede_facturar',true)->get();
    }else{
      $tipoPersona = TipoPersona::where('puede_facturar',true)->where('ver_agencia','false')->get();
    }

		$negocios = Negocio::where('id_empresa', '=', $idEmpresa)->get();

		$asistentes = Persona::select('id', 'nombre', 'apellido')
                ->where('id_empresa', '=', $idEmpresa)
                ->whereIn('id_tipo_persona', array(2, 3, 4, 5, 6, 7))
                ->get();
    
     $equipos = Equipo::where('activo', true)->where('id_empresa', '=', $idEmpresa)->get();
     $comercioPersona= ComercioPersona::where('id_empresa', $this->getIdEmpresa())->get();//se utiliza para empresas con ventas rapidas
     //$venta_rapida = Empresa::where('id_empresa', $this->getIdEmpresa());
     $venta_rapida = DB::select('select * from empresas where id = ?', [$this->getIdEmpresa()]);
    //  $usuarios = DB::table('vw_listado_factura')
    //  ->select('vw_listado_factura.id_usuario', 'personas.nombre', 'personas.apellido')
    //  ->where('vw_listado_factura.id_empresa', '=', $idEmpresa)
    //  ->leftJoin('personas', 'vw_listado_factura.id_usuario', '=', 'personas.id')
    //  ->distinct('vw_listado_factura.id_usuario')
    //  ->get();
  
  
    return view('pages.mc.factura.index')->with([
                                                 'monedas'=>$getDivisa,
                                                 'tipoFactura'=>$getTipoFactura,
                                                 'vendedorEmpresa'=>$getFacturaVendedorEmpresa,
                                                 'tipoEstado'=>$getTipoEstado,
                                                 'sucursals'=>$sucursalEmpresa,
                                                //  'getFacturaUsuario'=>$usuarios,
                                                 'grupos'=>$grupos,
                                                 'tipoPersona'=>$tipoPersona,
                                                 'negocios'=>$negocios,
                                                 'asistentes'=>$asistentes,
                                                 'equipos'=>$equipos,
                                                 'comercioPersonas'=>$comercioPersona,
                                                 'venta_rapida'=>$venta_rapida
                                                 ]);
}


/**
 * Si no recibe Ajax carga la vista de Reporte Factura y si recibe Ajax retorna
 * las consultas de Reporte Factura por Json
 * 
 */
public function reporteFactura(Request $req){

    // dd($req->all());
    $ajax = 0;
    $idUsuario = $this->getIdUsuario();
    $fecha_actual = date("Y-m-d H:i:s");
    $fecha_atras =  date("Y-m-d H:i:s",strtotime($fecha_actual."- 30 days")); 
    $sucursalFiltro = array();

    if(!is_null($req->all()) && !empty($req->all())){
          //Si request esta cargado realizar consultas
          $ajax++;
          $clienteId = $req->input('cliente_id');
          $monedaId  = $req->input('moneda_id');
          $tipoFactura = $req->input('tipoFactura');
          $tipoEstado = $req->input('tipoEstado');
          $vendedorId = $req->input('vendedor_id');
          $numFactura = $req->input('numFactura');
          $numProforma = $req->input('numProforma');
          $factura = Factura::query();
          $factura = $factura->with('cliente',
                                    'tipoFactura',
                                    'estado',
                                    'currency',
                                    'estadoCobro',
                                    'vendedorEmpresa',
                                    'tipoFacturacion');
          if($tipoFactura != ''){
               $factura = $factura->where('id_tipo_facturacion',$tipoFactura);  
           }
           if($clienteId != ''){
               $factura = $factura->where('cliente_id',$clienteId);  
           }
            if($monedaId != ''){
               $factura = $factura->where('id_moneda_venta',$monedaId);  
           }
            if($vendedorId != ''){
               $factura = $factura->where('vendedor_id',$vendedorId);  
           }
            if($numFactura != ''){
               $factura = $factura->where('nro_factura',$numFactura);  
           }
            if($numProforma != ''){
               $factura = $factura->where('id_proforma',$numProforma);  
           }
           if($tipoEstado != ''){
               $factura = $factura->where('id_estado_factura',$tipoEstado);  
           }

          if($req->input('facturacion_desde_hasta') != ''){
              $fecha = explode('-', $req->input('facturacion_desde_hasta'));
                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));

               $factura = $factura->whereBetween('fecha_hora_facturacion', array(
              date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
              date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                  ));
           }
            if($req->input('vencimiento_desde_hasta') != ''){
              $fecha = explode('-', $req->input('vencimiento_desde_hasta'));
                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));

               $factura = $factura->whereBetween('vencimiento', array(
              date('Y-m-d H:i:s',strtotime($desde)),
              date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                  ));
           }
           if($req->input('ultimo_pago_desde_hasta') != ''){
            $fecha = explode('-', $req->input('ultimo_pago_desde_hasta'));
              $desde = $this->formatoFechaEntrada(trim($fecha[0]));
              $hasta = $this->formatoFechaEntrada(trim($fecha[1]));

             $factura = $factura->whereBetween('fecha_ultimo_pago', array(
            date('Y-m-d H:i:s',strtotime($desde)),
            date('Y-m-d H:i:s',strtotime($hasta.' +1 day'))
                ));
         }
        $factura = $factura->where('id_empresa',$this->getIdEmpresa());
        $factura = $factura->get();
         // dd($factura->toArray());
          // if (condition) {
          //   # code...
          // }

    } else {

      //Si request esta vacio entonces realizar consultas para poblar vista de reporte factura
          $factura = Factura::with('cliente',
                                      'tipoFactura',
                                      'estado',
                                      'vendedorEmpresa',
                                      'tipoFacturacion',
                                      'estadoCobro')
                        ->where('id_empresa',$this->getIdEmpresa())
                        ->whereBetween('fecha_hora_facturacion',array($fecha_atras,$fecha_actual))->get();

          $getTipoEstado = EstadoFactour::where('id_tipo_estado','9')->get();
          $getTipoCobro = EstadoFactour::where('id_tipo_estado','10')->get();
          $getFacturaClientes = Factura::with('cliente')->where('id_empresa',$this->getIdEmpresa())->distinct()->get(['cliente_id']);
          $getFacturaVendedorEmpresa = Factura::with('vendedorEmpresa')->where('id_empresa',$this->getIdEmpresa())->distinct()->get(['vendedor_id']);
          $getDivisa = Divisas::where('activo','S')->get();
          $getTipoFactura = TipoFactura::get();
          $usuarioSistema = Persona::where('id', $idUsuario)->first();

          }//else
         
          
          // dd($factura);


          foreach ($factura as  $value) {
               $value->icoSaldo = "fa fa-exclamation-triangle icoRojo";
               $value->icoVenc = "fa fa-exclamation-triangle icoRojo";
               $value->pesoIcoSaldo = '0';
               $value->pesoIcoVenc = '0';

               if($value->fecha_ultimo_pago != null){
                $date = explode(' ',$value->fecha_ultimo_pago);   
                $hora =  date('H:i:s',strtotime($date[1]));
                $fecha = $this->formatoFechaSalida($date[0]);
                $value->fecha_ultimo_pago = $fecha;
                $date = null; $hora= null; $fecha = null;
                }


                      if($value->fecha_hora_facturacion != null){
                      $date = explode(' ',$value->fecha_hora_facturacion);   
                      $hora =  date('H:i:s',strtotime($date[1]));
                      $fecha = $this->formatoFechaSalida($date[0]);
                      $value->fecha_hora_facturacion = $fecha;
                      }

                      if($value->vendedor_id != null && $value->vendedor_id != ''){

                          $persona = Persona::with('sucursalAgencia')->where('id',$value->vendedor_id )->get();

                          if(isset($persona[0]->sucursalAgencia['nombre'])){
                            //
                         $value->sucursal = $persona[0]->sucursalAgencia['nombre'];
                         }
                      }

                      if($value->id_tipo_facturacion == '1'){
                          $value->monto = $value->total_bruto_factura;
                      } else {
                           $value->monto = $value->total_neto_factura;
                      }

                      if($value->vencimiento != null){
         
                          $vencimiento = $value->vencimiento;
                          $time = strtotime($vencimiento);
                          $fecha_actual = date("Y-m-d");
                          $fechaActual = strtotime($fecha_actual);
                          $fecha20Mas = strtotime($fecha_actual."+ 20 days");
                          $fecha8Mas = strtotime($fecha_actual."+ 8 days");

                          if($time >= $fechaActual){
                              if($time >= $fecha20Mas){
                                 $value->pesoIcoVenc = '2';
                                   $value->icoVenc = "fa fa-thumbs-o-up icoVerde";
                              } else if($time >= $fecha8Mas){
                                 $value->pesoIcoVenc = '1';
                                  $value->icoVenc = "fa fa-exclamation-triangle icoAmarillo";
                              } else {
                                 $value->pesoIcoVenc = '0';
                                  $value->icoVenc = "fa fa-exclamation-triangle icoRojo";
                              }

                          } else {
                            $value->pesoIcoVenc = '0';
                              $value->icoVenc = "fa fa-exclamation-triangle icoRojo";
                          } 
                          

                      }

                       if($value->vencimiento != null){
                      $fecha = $this->formatoFechaSalida($value->vencimiento);
                      $value->vencimiento = $fecha;
                      }


                      if($value->saldo_factura != null && $value->saldo_factura >= 0 && $value->monto != null && $value->monto > 0){
                          $saldoFactura = (Integer)$value->saldo_factura;
                          $totalFactura = $value->monto;


                          if($saldoFactura == 0){
                            $value->pesoIcoSaldo = '0';
                               $value->icoSaldo = "fa fa-thumbs-o-up icoVerde";
                               $value->icoVenc = "fa fa-thumbs-o-up icoVerde";
                           } else {

                           
                           if($saldoFactura > 0){

                          $value->pesoIcoSaldo = '1';
                           $value->icoSaldo = "fa fa-exclamation-triangle icoAmarillo";
                    
                          $total = round(($saldoFactura * 100) / $totalFactura );


                           } 
                          if($total >= 50 && $total <= 100 ){
                            $value->pesoIcoSaldo = '2';
                            $value->pesoIcoVenc = '2';
                            $value->icoSaldo = "fa fa-exclamation-triangle icoRojo";
                          
                           
                             
                          } else if ($total < 50 && $total != 0){
                              $value->pesoIcoSaldo = '1';
                              $value->icoSaldo = "fa fa-exclamation-triangle icoAmarillo";
                           
                            
                            
                          } 

                      }//else

                      }//if
       

          }//foreach

  if($ajax > 0){
      return response()->json(array('response'=>$factura));
    } else {

  return view('pages.mc.factura.reporteFactura')->with(['facturas'=>$factura,
                                                 'monedas'=>$getDivisa,
                                                 'tipoFactura'=>$getTipoFactura,
                                                 'clienteFactura'=>$getFacturaClientes,
                                                 'vendedorEmpresa'=>$getFacturaVendedorEmpresa,
                                                 'userSystem'=>$usuarioSistema,
                                                 'tipoEstado'=>$getTipoEstado,
                                                 'tipoCobro'=>$getTipoCobro]);  
}//else


}//


public function envioFactura(Request $req){
  $idUsuario = $this->getIdUsuario();
  $facturaId = $req->input("idFactura");
  if($idUsuario == 59049 || $idUsuario == 60517 || $idUsuario == 58823|| $idUsuario == 59015 || $idUsuario == 59047 || $idUsuario == 60512 ){ 
        $this->generarJsonFactura($req, $facturaId);
  }  
  return response()->json(array('response'=>'OK'));
}


public function envioNC(Request $req){
  $idUsuario = $this->getIdUsuario();
  $idNC = $req->input("idNC");
  if($idUsuario == 59049 || $idUsuario == 60517 || $idUsuario == 58823|| $idUsuario == 59015 || $idUsuario == 59047 || $idUsuario == 60512 ){ 
    $this->generarJsonNC($req, $idNC);
  }  
  return response()->json(array('response'=>'OK'));
}

/**
 * Listado de facturas
 * @return [type] [description]
 */
public function autorizadas(){
  

  $personaLogin = $this->getDatosUsuario();
  $idUsuario = $this->getIdUsuario();

  $grupos = Grupos::where('estado_id', 11)
                  ->where('empresa_id',$this->getIdEmpresa())
                   ->get(['id','denominacion']);
    
  $getTipoEstado = EstadoFactour::where('id_tipo_estado','9')->get();
  $cliente = DB::select("
                        SELECT p.id,p.nombre, p.apellido, p.documento_identidad,p.dv, tp.denominacion, p.activo,
                        concat(p.nombre,' ',p.apellido,' ','- '|| trim(p.denominacion_comercial)) as full_data
                        FROM personas p
                        JOIN tipo_persona tp on tp.id = p.id_tipo_persona
                        WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
                        AND p.activo = true and p.id_empresa = ".$this->getIdEmpresa()." order by p.nombre asc");

  $getFacturaVendedorEmpresa = Factura::with('vendedorEmpresa')->where('id_empresa',$this->getIdEmpresa())->distinct()->get(['vendedor_id']);
  $getFacturaUsuario = Factura::with('usuario')->where('id_empresa',$this->getIdEmpresa())->distinct()->get(['id_usuario']);

  $sucursalEmpresa = Persona::select('id','nombre')->where('id_tipo_persona','21')->where('id_empresa',$this->getIdEmpresa())->where('activo','true')->orderBy('nombre','ASC')->get();

  $getDivisa = Divisas::select('currency_id','currency_code')::where('activo','S')->get();
  $getTipoFactura = TipoFactura::get();
  $usuarioSistema = Persona::where('id', $idUsuario)->first();

  $arrayDestino = DB::select("SELECT
                id_destino_dtpmundo, desc_destino
              FROM
                destinos_dtpmundo 
              WHERE
                id_destino_dtpmundo IN ( SELECT DISTINCT ( destino_id ) FROM proformas WHERE id_empresa = ".$this->getIdEmpresa()."AND destino_id IS NOT NULL );");

  $sucursalFiltro = array();
  $id_empresa = $this->getIdEmpresa();
  if($id_empresa == 1 || $id_empresa == 4){
    $tipoPersona = TipoPersona::where('puede_facturar',true)->get();
  }else{
    $tipoPersona = TipoPersona::where('puede_facturar',true)->where('ver_agencia','false')->get();
  }

  $negocios = Negocio::where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)->get();

  $comentarios = [];

  return view('pages.mc.factura.facturasAutorizadas')->with([
                                               'monedas'=>$getDivisa,
                                               'tipoFactura'=>$getTipoFactura,
                                               'clientes'=>$cliente,
                                               'vendedorEmpresa'=>$getFacturaVendedorEmpresa,
                                               'userSystem'=>$usuarioSistema,
                                               'tipoEstado'=>$getTipoEstado,
                                               'sucursals'=>$sucursalEmpresa,
                                               'getFacturaUsuario'=>$getFacturaUsuario,
                                               'grupos'=>$grupos,
                                               'tipoPersona'=>$tipoPersona,
                                               'valorDestinos'=>$arrayDestino,
                                               'negocios'=>$negocios,
                                               'comentario'=>$comentarios,
                                               'logo'=>Session::get('datos-loggeo')->datos->datosUsuarios->logoAgencia
                                               ]);
}



public function consultaFacturaServerSide(Request $req){

    // dd($req->all());
     // Generar una clave única para esta consulta
     $redisKey = 'facturas:' . md5($req->fullUrl());

     if (Redis::exists($redisKey)) {
      // Recuperar los Connection to 172.17.0.2 6379 port [tcp/*] succeeded!resultados de la consulta de Redis
      $resultFacturas = json_decode(Redis::get($redisKey));
 } else {//del redis
      $personaLogin = $this->getDatosUsuario();
            //DATOS DE USUARIO
          $tipoPersona = $personaLogin->id_tipo_persona;
          $idUsuarioSistema = $personaLogin->id;
          $sucursalEmpresa = $personaLogin->id_sucursal_empresa;
          $flag = 0;

          $desde     = "";
          $hasta     = "";
          $desdecheck = "";
          $hastacheck = "";
          // dd($req->all());
          // $formSearch = $req->formSearch;
          $draw = intval($req->draw);
          $start = intval($req->start);
          $length = intval($req->length);
  
       
            

           $periodo = $req->input('periodo');
           if($periodo != ""){
              $fechaPeriodo = explode(' - ', $periodo);
              $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
              $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
            } else {
              $desde     = "";
              $hasta     = "";
            } 
            if($req->input('checkin') != ""){
              $fechaPeriodo = explode(' - ', $req->input('checkin'));
              $desdecheck     = $this->formatoFechaEntrada($fechaPeriodo[0]);
              $hastacheck     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
            } 

            //ORDENAMIENTO POR COLUMNA
            $columna = $req->order[0]['column'];
            $orden = $req->order[0]['dir'];

   
      $cont = 0;
     $filtrarMax = 0;

          $clienteId = $req->input('cliente_id');
          $monedaId  = $req->input('idMoneda');
          $tipoFactura = $req->input('tipoFactura');
          $tipoEstado = $req->input('tipoEstado');
          $vendedorId = $req->input('vendedor_id');
          $numFactura = $req->input('numFactura');
          $numProforma = $req->input('numProforma');
          $impreso = $req->input('impreso');
          $idUsuarioBusqueda = $req->input('id_usuario');
          $sucursalFacturacion = $req->input('id_sucursal');
          $grupo = $req->input('grupo_id');
          $forma_pago = $req->input('forma_pago');
          $idTipoPersona = $req->input('tipo_persona_id');
          $tipo_venta = $req->input('tipo_venta');
          $destino = $req->input('destino');
          $negocio = $req->input('negocio');
          $id_asistente = $req->input('id_asistente');
          $id_equipo = $req->input('id_equipo');
        //  $origen_negocio = $req->input('id_comercio');
          // dd($idTipoPersona);
          

          $factura = DB::table('vw_listado_factura');
          $factura = $factura->where('id_empresa',$this->getIdEmpresa());

       
          //OPERATIVO EMPRESA OR ADMINISTRATIVO_EMPRESA OR ADMIN_EMPRESA
      if($tipoPersona == '6' || $tipoPersona == '5' || $tipoPersona == '2'){
        if($req->id_estado_proforma == ''){ 
          //PUEDE VER TODAS LAS FACTURAS
        }
          //SUPERVISOR EMPRESA
      } else if($tipoPersona == '4'){
          $factura = $factura->where('id_sucursal_empresa_usuario', $sucursalEmpresa);
      } else {
          //LOS DEMAS TIPO SOLO PUEDE VER SU PROFORMA
          $flag = 1;
                //$factura = $factura->where('id_usuario',  $idUsuarioSistema);
          $factura = $factura->where(function($factura) use ($idUsuarioSistema) {
                  $factura->where('id_usuario',$idUsuarioSistema)
                      ->orWhere('id_asistente',$idUsuarioSistema);
                });
      }


           if($tipoFactura != ''){
               $factura = $factura->where('id_tipo_factura',$tipoFactura);  
           }
           if($clienteId != ''){
               $factura = $factura->where('cliente_id',$clienteId);  
           }
            if($monedaId != ''){
               $factura = $factura->where('id_moneda_venta',$monedaId);  
           }
           if($id_asistente != ''){
            $factura = $factura->where('id_asistente',$id_asistente);  
           }
            if($vendedorId != ''){
               $factura = $factura->where('vendedor_id',$vendedorId);  
           }
            if($numFactura != ''){
               $factura = $factura->where('nro_factura', 'like', '%' .trim($numFactura).'%');  
           }
            if($numProforma != ''){
               $factura = $factura->where('id_proforma',trim($numProforma));  
           }
           if($tipoEstado != ''){
               $factura = $factura->where('id_estado_factura',$tipoEstado);  
           }
           if($impreso != ''){
               $factura = $factura->where('factura_impresa',$impreso);  
           }
           if( $idUsuarioBusqueda != '' && $flag == 0){
             $factura = $factura->where('id_usuario',$idUsuarioBusqueda); 
           }
           if($destino != ''){
            $factura = $factura->where('destino_id',$destino); 
          }
           if( $forma_pago != ''){
              if($forma_pago  === 'true')
             $factura = $factura->whereNotNull('id_fp'); 

           if($forma_pago  === 'false')
             $factura = $factura->whereNull('id_fp'); 
           }  
           if($negocio != ''){
               $factura = $factura->where('id_unidad_negocio',$negocio);  
           }

           if($tipo_venta  != ''){ 
             if($tipo_venta == '1'){ $factura = $factura->whereNull('id_venta_rapida'); }//PROFORMA
             if($tipo_venta == '2'){$factura = $factura->whereNotNull('id_venta_rapida');}//VENTA RAPIDA
          }  
          
          if($id_equipo != ''){
            $factura = $factura->where('id_equipo',$id_equipo);  
          }
/*
          if($origen_negocio != ''){
            $factura = $factura->where('id_comercio',$origen_negocio);  
          }*/
            
            if($desde != '' && $hasta != ''){

              $factura = $factura->whereBetween('fecha_hora_facturacion', array(
              date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
              date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                  ));
           }

           if($grupo != ''){
             $factura = $factura->where('id_grupo',$grupo);
           }


           if($sucursalFacturacion != '' ){
            $factura = $factura->where('id_sucursal_timbrado',$sucursalFacturacion); 
           }
            
          if($req->input('file_codigo') != ''){
              //$proformas = $proformas->where('file_codigo', trim($req->input('file_codigo')));
              $nombre = "%".trim($req->input('file_codigo'))."%";
              $factura = $factura->where('file_codigo','LIKE',$nombre); 
            } 

//para checkin
          if ($desdecheck && $hastacheck) {
            $factura = $factura->whereBetween('check_in', [$desdecheck. ' 00:00:00', $hastacheck.' 23:59:59']);
          }


            // $factura = $factura->get();

            $cantidadFacturas = $factura->count();

            /**
             * MANTENER EL ORDENAMIENTO ANTES DEL COUNT PARA EVITAR ERRORES
             */

            //ORDENAMIENTO SEGUN EL ORDEN DE LA COLUMNA
          switch ($columna) {
            case '0':
              $factura = $factura->orderBy('id',$orden);
              break;

              case '1':
              $factura = $factura->orderBy('fecha_hora_facturacion',$orden);
              break;

               case '2':
              $factura = $factura->orderBy('check_in',$orden);
              break;

              case '3':
              $factura = $factura->orderBy('id_proforma',$orden);
              break;

              case '4':
               $factura = $factura->orderBy('denominacion',$orden);
              break;

              case '5':
               $factura = $factura->orderBy('monto',$orden);
              break;

              case '6':
              $factura = $factura->orderBy('currency_code',$orden);
              break;

              case '7':
               $factura = $factura->orderBy('cliente_n',$orden);
              break;

              case '8':
              $factura = $factura->orderBy('suc_n',$orden);
              break;

              case '9':
              $factura = $factura->orderBy('vendedor_n',$orden);
              break;

              case '10':
              $factura = $factura->orderBy('usuario_n',$orden);
              break;
            
            default:
              # code...
              break;
          }
            
            $factura = $factura->skip($start)->take($length)->get();  
   
            $lengtArray = $cantidadFacturas; 
            $resultFacturas = array();
            $boton = '';

    foreach ($factura as $key => $value) {
      $num_factura = '';
      $boton = '';
      
    

 
        if($value->id_proforma != null){
          $autorizado = DB::select("SELECT * FROM get_proforma_autorizada(".$value->id_proforma.")");	
          $proforma_autorizada = $autorizado[0]->get_proforma_autorizada;
        }else{
          $proforma_autorizada = '';
        }
        $renta_neta = (float)$value->ganancia_venta - ((float)$value->renta_extra + (float)$value->incentivo_vendedor_agencia);
        $value->renta = $this->formatMoney($renta_neta,$value->id_moneda_venta);
        $value->monto_factura =  $this->formatMoney($value->monto_factura,$value->id_moneda_venta);   
        $value->ganancia_venta = $this->formatMoney($value->ganancia_venta,$value->id_moneda_venta);
        $value->renta_extra = $this->formatMoney($value->renta_extra,$value->id_moneda_venta);
        $value->incentivo_vendedor_agencia = $this->formatMoney($value->incentivo_vendedor_agencia,$value->id_moneda_venta);
        $value->total_comision =  $this->formatMoney($value->total_comision,$value->id_moneda_venta);
        $value->proforma_autorizada = $proforma_autorizada;
        $value->btn = $this->btnPermisos(array('id_factura' =>$value->id),'verFactura');
        $resultFacturas[] = $value;
      
      
    


    }//foreach

    // dd($resultFacturas);


    Redis::set($redisKey, json_encode($resultFacturas));
  }//fin else redis

// dd( $resultFacturas);

return response()->json(['draw'=>$draw,
                 'recordsTotal'=>$lengtArray,
                 'recordsFiltered'=>$lengtArray,
                 'data'=>$resultFacturas]); 

}//function


public function consultaFacturaServerSideAutorizado(Request $req){

  // dd($req->all());

    $personaLogin = $this->getDatosUsuario();
          //DATOS DE USUARIO
        $tipoPersona = $personaLogin->id_tipo_persona;
        $idUsuarioSistema = $personaLogin->id;
        $sucursalEmpresa = $personaLogin->id_sucursal_empresa;
        $flag = 0;

        $desde     = "";
        $hasta     = "";
        // $formSearch = $req->formSearch;
        $draw = intval($req->draw);
        $start = intval($req->start);
        $length = intval($req->length);

     
          

         $periodo = $req->input('periodo');
         if($periodo != ""){
            $fechaPeriodo = explode(' - ', $periodo);
            $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
            $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
          } else {
            $desde     = "";
            $hasta     = "";
          }  

          //ORDENAMIENTO POR COLUMNA
          $columna = $req->order[0]['column'];
          $orden = $req->order[0]['dir'];

 
    $cont = 0;
   $filtrarMax = 0;

        $clienteId = $req->input('cliente_id');
        $monedaId  = $req->input('idMoneda');
        $tipoFactura = $req->input('tipoFactura');
        $tipoEstado = $req->input('tipoEstado');
        $vendedorId = $req->input('vendedor_id');
        $numFactura = $req->input('numFactura');
        $numProforma = $req->input('numProforma');
        $impreso = $req->input('impreso');
        $idUsuarioBusqueda = $req->input('id_usuario');
        $sucursalFacturacion = $req->input('id_sucursal');
        $grupo = $req->input('grupo_id');
        $forma_pago = $req->input('forma_pago');
        $idTipoPersona = $req->input('tipo_persona_id');
        $tipo_venta = $req->input('tipo_venta');
        $destino = $req->input('destino');
        $negocio = $req->input('negocio');
        // dd($idTipoPersona);
        

        $factura = DB::table('vw_listado_factura');
        $factura = $factura->where('id_empresa',$this->getIdEmpresa());
        $factura = $factura->where('cant_solicitud','>',0);
     
             //ORDENAMIENTO SEGUN EL ORDEN DE LA COLUMNA
        switch ($columna) {
          case '0':
            $factura = $factura->orderBy('id',$orden);
            break;

            case '1':
            $factura = $factura->orderBy('fecha_hora_facturacion',$orden);
            break;

             case '2':
            $factura = $factura->orderBy('check_in',$orden);
            break;

            case '3':
            $factura = $factura->orderBy('id_proforma',$orden);
            break;

            case '4':
             $factura = $factura->orderBy('denominacion',$orden);
            break;

            case '5':
             $factura = $factura->orderBy('monto',$orden);
            break;

            case '6':
            $factura = $factura->orderBy('currency_code',$orden);
            break;

            case '7':
             $factura = $factura->orderBy('cliente_n',$orden);
            break;

            case '8':
            $factura = $factura->orderBy('suc_n',$orden);
            break;

            case '9':
            $factura = $factura->orderBy('vendedor_n',$orden);
            break;

            case '10':
            $factura = $factura->orderBy('usuario_n',$orden);
            break;
          
          default:
            # code...
            break;
        }
      

        
  
        //OPERATIVO EMPRESA OR ADMINISTRATIVO_EMPRESA OR ADMIN_EMPRESA
    if($tipoPersona == '6' || $tipoPersona == '5' || $tipoPersona == '2'){
      if($req->id_estado_proforma == ''){ 
        //PUEDE VER TODAS LAS FACTURAS
      }
        //SUPERVISOR EMPRESA
    } else if($tipoPersona == '4'){
        $factura = $factura->where('id_sucursal_empresa_usuario', $sucursalEmpresa);
    } else {
        //LOS DEMAS TIPO SOLO PUEDE VER SU PROFORMA
        $flag = 1;
        $factura = $factura->where('id_usuario',  $idUsuarioSistema);
    }


         if($tipoFactura != ''){
             $factura = $factura->where('id_tipo_factura',$tipoFactura);  
         }
         if($clienteId != ''){
             $factura = $factura->where('cliente_id',$clienteId);  
         }
          if($monedaId != ''){
             $factura = $factura->where('id_moneda_venta',$monedaId);  
         }
          if($vendedorId != ''){
             $factura = $factura->where('vendedor_id',$vendedorId);  
         }
          if($numFactura != ''){
             $factura = $factura->where('nro_factura', 'like', '%' .trim($numFactura).'%');  
         }
          if($numProforma != ''){
             $factura = $factura->where('id_proforma',trim($numProforma));  
         }
         if($tipoEstado != ''){
             $factura = $factura->where('id_estado_factura',$tipoEstado);  
         }
         if($impreso != ''){
             $factura = $factura->where('factura_impresa',$impreso);  
         }
         if( $idUsuarioBusqueda != '' && $flag == 0){
           $factura = $factura->where('id_usuario',$idUsuarioBusqueda); 
         }
         if($destino != ''){
          $factura = $factura->where('destino_id',$destino); 
        }
         if( $forma_pago != ''){
            if($forma_pago  === 'true')
           $factura = $factura->whereNotNull('id_fp'); 

         if($forma_pago  === 'false')
           $factura = $factura->whereNull('id_fp'); 
         }  
         if($negocio != ''){
             $factura = $factura->where('id_unidad_negocio',$negocio);  
         }

         if($tipo_venta  != ''){ 
           if($tipo_venta == '1'){ $factura = $factura->whereNull('id_venta_rapida'); }//PROFORMA
           if($tipo_venta == '2'){$factura = $factura->whereNotNull('id_venta_rapida');}//VENTA RAPIDA
        }  
         
          
          if($desde != '' && $hasta != ''){

            $factura = $factura->whereBetween('fecha_hora_facturacion', array(
            date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
            date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                ));
         }

         if($grupo != ''){
           $factura = $factura->where('id_grupo',$grupo);
         }

         if($sucursalFacturacion != '' ){
          $factura = $factura->where('id_sucursal_timbrado',$sucursalFacturacion); 
         }
          
        if($req->input('file_codigo') != ''){
            //$proformas = $proformas->where('file_codigo', trim($req->input('file_codigo')));
            $nombre = "%".trim($req->input('file_codigo'))."%";
            $factura = $factura->where('file_codigo','LIKE',$nombre); 
          } 


          $factura = $factura->get();

          $cantidadFacturas = count($factura);
          $lengtArray = $cantidadFacturas; 
          $resultFacturas = array();
          $boton = '';

  foreach ($factura as $key => $value) {
    $num_factura = '';
    $boton = '';
    
    if($cont == $length){
      break;
    }

    if($key >= $start){
      $renta_neta = (float)$value->ganancia_venta - ((float)$value->renta_extra + (float)$value->incentivo_vendedor_agencia);
      $value->renta = $this->formatMoney($renta_neta,$value->id_moneda_venta);
      $value->monto_factura =  $this->formatMoney($value->monto_factura,$value->id_moneda_venta);   
      $value->ganancia_venta = $this->formatMoney($value->ganancia_venta,$value->id_moneda_venta);
      $value->renta_extra = $this->formatMoney($value->renta_extra,$value->id_moneda_venta);
      $value->incentivo_vendedor_agencia = $this->formatMoney($value->incentivo_vendedor_agencia,$value->id_moneda_venta);
      $value->total_comision =  $this->formatMoney($value->total_comision,$value->id_moneda_venta);
      $value->saldo_factura =  $this->formatMoney($value->saldo_factura,$value->id_moneda_venta);
      $value->btn = $this->btnPermisos(array('id_factura' =>$value->id),'verFactura');
      $resultFacturas[] = $value;
       $cont++;
    }
  


  }//foreach

  // dd($resultFacturas);




// dd( $resultFacturas);

return response()->json(['draw'=>$draw,
               'recordsTotal'=>$lengtArray,
               'recordsFiltered'=>$lengtArray,
               'data'=>$resultFacturas]); 

}//function



public function consultaFactura(Request $req){
     


          $personaLogin = $this->getDatosUsuario();
            //DATOS DE USUARIO
          $tipoPersona = $personaLogin->id_tipo_persona;
          $idUsuarioSistema = $personaLogin->id;
          $sucursalEmpresa = $personaLogin->id_sucursal_empresa;
        
          $desde     = "";
          $hasta     = "";
          $cont = 0;
          $filtrarMax = 0;

          $periodo = $req->input('periodo');
           if($periodo != ""){
              $fechaPeriodo = explode(' - ', $periodo);
              $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
              $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
            } else {
              $desde     = "";
              $hasta     = "";
            }  

          //DATOS INPUT
          $clienteId = $req->input('cliente_id');
          $monedaId  = $req->input('idMoneda');
          $tipoFactura = $req->input('tipoFactura');
          $tipoEstado = $req->input('tipoEstado');
          $vendedorId = $req->input('vendedor_id');
          $numFactura = $req->input('numFactura');
          $numProforma = $req->input('numProforma');
          $impreso = $req->input('impreso');
          $idUsuarioBusqueda = $req->input('id_usuario');
          $sucursal = $req->input('id_sucursal');
          /*
            2 "ADMIN_EMPRESA"
            3 "VENDEDOR_EMPRESA"
            4 "SUPERVISOR_EMPRESA"
            5 "ADMINISTRATIVO_EMPRESA"
            6 "OPERATIVO_EMPRESA"
           */
          //VALIDAR SI ES EL TIPO DE PERSONA QUE PUEDE VER FACTURAS
          if($tipoPersona == 3 || $tipoPersona == 4 || $tipoPersona == 2 || $tipoPersona == 5 || $tipoPersona == 6){



          //CONSULTA BD
          $factura = Factura::query();
          $factura = $factura->with('cliente',
                                    'tipoFactura',
                                    'estado',
                                    'usuario',
                                    'currency',
                                    'vendedorEmpresa',
                                    'tipoFacturacion');


        //VENDEDOR EMPRESA
      if($tipoPersona == '3'){
      $factura = $factura->where('id_usuario',$idUsuarioSistema);  
      }//if

           if($tipoFactura != ''){
               $factura = $factura->where('id_tipo_facturacion',$tipoFactura);  
           }
           if($clienteId != ''){
               $factura = $factura->where('cliente_id',$clienteId);  
           }
            if($monedaId != ''){
               $factura = $factura->where('id_moneda_venta',$monedaId);  
           }
            if($vendedorId != ''){
               $factura = $factura->where('vendedor_id',$vendedorId);  
           }
            if($numFactura != ''){
               $factura = $factura->where('nro_factura', 'like', '%' .$numFactura.'%');  
           }
            if($numProforma != ''){
               $factura = $factura->where('id_proforma',$numProforma);  
           }
           if($tipoEstado != ''){
               $factura = $factura->where('id_estado_factura',$tipoEstado);  
           }
           if($impreso != ''){
               $factura = $factura->where('factura_impresa',$impreso);  
           }
           if( $idUsuarioBusqueda != '' ){
             $factura = $factura->where('id_usuario',$idUsuarioBusqueda); 
           } 

            if($desde != '' && $hasta != ''){
           


              $factura = $factura->whereBetween('fecha_hora_facturacion', array(
              date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
              date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                  ));
           }
            
            $factura = $factura->where('id_empresa',$this->getIdEmpresa());
            $factura = $factura->get();

              
          // dd($factura);
            
        


             foreach ($factura as  $key=>$value) {  
                
                  //SUPERVISOR EMPRESA
                   if($tipoPersona == '4'){
                    if(isset($value->usuario->id_sucursal_empresa)){
                    if($value->usuario->id_sucursal_empresa != $sucursalEmpresa ){
                        unset($value);
                        continue;
                        }//if 
                      } else {
                        unset($value);
                        continue;
                      }
                    }




                    
                    if($value->fecha_hora_facturacion != null){
                    $date = explode(' ',$value->fecha_hora_facturacion);   
                    $hora =  date('H:i:s',strtotime($date[1]));
                    $fecha = $this->formatoFechaSalida($date[0]);
                    $value->fecha_hora_facturacion = $fecha;
                    }

                    if($value->vendedor_id != 'null' && $value->vendedor_id != ''){

                        $persona = Persona::with('sucursalAgencia')->where('id',$value->vendedor_id )->get();

                        if(isset($persona[0]->sucursalAgencia['nombre'])){

                           // dd($persona);
                        $value->sucursal = $persona[0]->sucursalAgencia['nombre'];
                        }

                    }

                    if($value->id_tipo_facturacion == '1'){
                        $value->monto = $value->total_bruto_factura;
                    } else {
                         $value->monto = $value->total_neto_factura;
                    }
                     /* echo "</pre>"; 
                      print_r($value->id);
                      echo "-----------------"; */
                    if($value->id >= 90000){
                     /* echo "</pre>"; 
                      print_r($value->id);*/
                      $btnVer = '<a href="../verFactura/'.$value->id.'" class="btn btn-danger" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-search"></i></a>';
                    }else{
                      $btnVer = '<a href="#" disabled="disabled" class="btn btn-danger" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-search"></i></a>';
                    }
                     $value->boton = $btnVer;
                  } //foreach



            if($sucursal != ""){
              foreach ($factura as  $key=>$value){  
                if((int)$value->usuario->id_sucursal_empresa != (int)$sucursal){
                    unset($factura[$key]);
                } 
              }  
            }



       } else {
            //RESPUESTA VACIA EN CASO DE NO SER PERSONA CON PERMISOS
             $factura = array();
          }


        return response()->json(array('response'=>$factura));

  }



 /**
   * Poblar el reporte facturas pendientes
   */
  public function reporteFacturasPendientes(){ 
    $moneda = '';
    $cliente = DB::select("SELECT  DISTINCT c.id, 
                          c.nombre nombre_c, 
                          c.denominacion_comercial,
                          c.documento_identidad,
                          c.dv   
                          FROM facturas f, personas c 
                          WHERE  c.id = f.cliente_id
                          AND f.id_estado_factura = 29  
                          AND id_tipo_factura = 1
                          AND f.id_empresa = ".$this->getIdEmpresa()."
                          AND f.id_tipo_factura = 1 
                          AND f.id_estado_cobro = 31 
                          AND f.saldo_factura > 0 
                          ORDER BY nombre_c ASC");


    $moneda = Divisas::where('activo','S')->get();

    $sucursalCartera = Persona::where('id_tipo_persona','21')
                               ->where('id_empresa',$this->getIdEmpresa())
                               ->get();

    //$tipoPersona = TipoPersona::where('puede_facturar',true)->get();
    $id_empresa = $this->getIdEmpresa();
    if($id_empresa == 1 || $id_empresa == 4){
      $tipoPersona = TipoPersona::where('puede_facturar',true)->get();
    }else{
      $tipoPersona = TipoPersona::where('puede_facturar',true)->where('ver_agencia','false')->get();
    }

    $facturas = DB::table('vw_facturas_pendientes');
    $facturas = $facturas->where('id_estado_factura',29);
    $facturas = $facturas->where('id_empresa',$this->getIdEmpresa());
    $facturas = $facturas->where('id_estado_cobro',31);
    $facturas = $facturas->where('saldo','>', 0);
    $facturas = $facturas->get();

    $usuarios = Persona::whereIn('id_tipo_persona', ['3', '4', '10'])->where('id_empresa',$this->getIdEmpresa())->get(['nombre', 'apellido', 'id']);
      
    return view('pages.mc.factura.facturasPendientes')->with([
                                                          'facturas'=>$facturas,
                                                          'clientes'=>$cliente,
                                                          'sucursalCartera'=>$sucursalCartera,
                                                          'tipoPersona'=>$tipoPersona,
                                                          'monedas'=>$moneda,
                                                          'usuarios'=>$usuarios
                                                          ]);
  }//function

  public function consultaFacturasPendientes(Request $req){
        $facturas = DB::table('vw_facturas_pendientes');
        $facturas = $facturas->where('id_estado_factura',29);
        $facturas = $facturas->where('id_empresa',$this->getIdEmpresa());
        $facturas = $facturas->where('id_estado_cobro',31);
        $facturas = $facturas->where('saldo','>', 0);
        if($req->input("cliente_id") != ''){
          $facturas = $facturas->where('cliente_id',$req->input("cliente_id"));
        }
        if($req->input("cliente_id") != ''){
          $facturas = $facturas->where('cliente_id',$req->input("cliente_id"));
        }
        if($req->input("moneda_id") != ''){
          $facturas = $facturas->where('currency_id',$req->input("moneda_id"));
        }
        if($req->input("estado") != ''){
          $facturas = $facturas->where('venc_estado',$req->input("estado"));
        }
        if($req->input("sucursal_id") != ''){
          $facturas = $facturas->where('id_sucursal_cartera',$req->input("sucursal_id"));
        }
        if($req->input("usuario_id") != ''){
          $facturas = $facturas->where('id_usuario',$req->input("usuario_id"));
        }
        if($req->input("tipo_persona_id") != ''){
          $facturas = $facturas->where('id_tipo_persona',$req->input("tipo_persona_id"));
        }
        if($req->input("checkin_desde_hasta") != ''){
          $fechaPeriodos = explode(' - ', $req->input('checkin_desde_hasta'));
          $date1 = explode('/', $fechaPeriodos[0]);
          $desde = $date1[2]."-".$date1[1]."-".$date1[0];
          $date2 = explode('/', $fechaPeriodos[1]);
          $hasta = $date2[2]."-".$date2[1]."-".$date2[0];
          $data = $data->whereBetween('checkin_format', array($desde,$hasta));
        }

        if($req->input("vencimiento_desde_hasta") != ''){
          $fechaPeriodos = explode(' - ', $req->input('vencimiento_desde_hasta'));
          $date1 = explode('/', $fechaPeriodos[0]);
          $desde = $date1[2]."-".$date1[1]."-".$date1[0];
          $date2 = explode('/', $fechaPeriodos[1]);
          $hasta = $date2[2]."-".$date2[1]."-".$date2[0];
          $data = $data->whereBetween('vencimienvencimiento_formatto', array($desde,$hasta));
          /*echo '<pre>';
          print_r($desde);*/
        }
        $facturas = $facturas->get();
     
        return response()->json(array('facturas'=>$facturas));      

}

public function getAdjuntosVoucher(Request $req){
    
  $adjunto = AdjuntoDocumento::where('proforma_id', $req->input("dataProforma"))->get();

  return json_encode($adjunto); 

}


public function errorFactura(){
    return view('pages.mc.factura.error');
}


public function anularFactura(Request $req){

  $idUsuario = $this->getIdUsuario();
  $idFactura = $req->input('idFactura');
  $err = 'OK';


  $anular = DB::select("SELECT * from anular_factura(".$idFactura.", ".$idUsuario.") limit 1");
  if($anular[0]->anular_factura !== 'OK'){
    $err = 'false';
  }
   DB::table('proformas')
                  ->where('factura_id',$idFactura)
                  ->update([
                        'estado_id'=>1
                        ]);

  return response()->json(array('err'=>$err,'response'=>$anular));

}


  public function getId4Log(){
    //Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
      list($MiliSegundos, $Segundos) = explode(" ", microtime());
      $id=substr(strval($MiliSegundos),2,4);
      return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
  }




    private static $UNIDADES = [
        '',
        'UN ',
        'DOS ',
        'TRES ',
        'CUATRO ',
        'CINCO ',
        'SEIS ',
        'SIETE ',
        'OCHO ',
        'NUEVE ',
        'DIEZ ',
        'ONCE ',
        'DOCE ',
        'TRECE ',
        'CATORCE ',
        'QUINCE ',
        'DIECISEIS ',
        'DIECISIETE ',
        'DIECIOCHO ',
        'DIECINUEVE ',
        'VEINTE '
    ];
    private static $DECENAS = [
        'VEINTI',
        'TREINTA ',
        'CUARENTA ',
        'CINCUENTA ',
        'SESENTA ',
        'SETENTA ',
        'OCHENTA ',
        'NOVENTA ',
        'CIEN '
    ];
    private static $CENTENAS = [
        'CIENTO ',
        'DOSCIENTOS ',
        'TRESCIENTOS ',
        'CUATROCIENTOS ',
        'QUINIENTOS ',
        'SEISCIENTOS ',
        'SETECIENTOS ',
        'OCHOCIENTOS ',
        'NOVECIENTOS '
    ];
    public static function convertir($number, $moneda = '', $centimos = '', $forzarCentimos = false)
    {
        $converted = '';
        $decimales = '';
        if (($number < 0) || ($number > 999999999)) {
            return 'No es posible convertir el numero a letras';
        }
        $div_decimales = explode('.',$number);
        if(count($div_decimales) > 1){
            $number = $div_decimales[0];
            $decNumberStr = (string) $div_decimales[1];
            //SI VIENEN NUMERO SOLO LE RELLENA CON CERO
            $decNumberStr = str_pad($decNumberStr, 2, "0", STR_PAD_RIGHT);
            if(strlen($decNumberStr) == 2){
                $decNumberStrFill = str_pad($decNumberStr, 9, '0', STR_PAD_LEFT);
                $decCientos = substr($decNumberStrFill, 6);
                $decimales = self::convertGroup($decCientos);
            }
        }
        else if (count($div_decimales) == 1 && $forzarCentimos){
            $decimales = 'CERO ';
        }
        $numberStr = (string) $number;
        $numberStrFill = str_pad($numberStr, 9, '0', STR_PAD_LEFT);
        $millones = substr($numberStrFill, 0, 3);
        $miles = substr($numberStrFill, 3, 3);
        $cientos = substr($numberStrFill, 6);
        if (intval($millones) > 0) {
            if ($millones == '001') {
                $converted .= 'UN MILLON ';
            } else if (intval($millones) > 0) {
                $converted .= sprintf('%sMILLONES ', self::convertGroup($millones));
            }
        }
        if (intval($miles) > 0) {
            if ($miles == '001') {
                $converted .= 'MIL ';
            } else if (intval($miles) > 0) {
                $converted .= sprintf('%sMIL ', self::convertGroup($miles));
            }
        }
        if (intval($cientos) > 0) {
            if ($cientos == '001') {
                $converted .= 'UN ';
            } else if (intval($cientos) > 0) {
                $converted .= sprintf('%s ', self::convertGroup($cientos));
            }
        }
        if(empty($decimales)){
            $valor_convertido = $converted . strtoupper($moneda);
        } else {
            $valor_convertido = $converted . strtoupper($moneda) . ' CON ' . $decimales . ' ' . strtoupper($centimos);
        }
        return $valor_convertido;
    }
    private static function convertGroup($n)
    {
        $output = '';
        if ($n == '100') {
            $output = "CIEN ";
        } else if ($n[0] !== '0') {
            $output = self::$CENTENAS[$n[0] - 1];
        }
        $k = intval(substr($n,1));
        if ($k <= 20) {
            $output .= self::$UNIDADES[$k];
        } else {
            if(($k > 30) && ($n[2] !== '0')) {
                $output .= sprintf('%sY %s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            } else {
                $output .= sprintf('%s%s', self::$DECENAS[intval($n[1]) - 2], self::$UNIDADES[intval($n[2])]);
            }
        }
        return $output;
    }


    public function verFactura(Request $request, $id){

      $grupos = Grupos::where('estado_id', 11)->get(['id','denominacion']);

      $factura = Factura::with('cliente',
                      'pasajero',
                      'vendedorAgencia',
                      'vendedorEmpresa',
                      'tipoFactura',
                      'tipoFacturacion',
                      'proforma',
                      'currency',
                      'facturaDetalle',
                      'estado',
                      'usuario',
                      'libroventa',
                      'negocio'
                      )
                      ->where('id_empresa',$this->getIdEmpresa())
                      ->where('id',$id)
                      ->get();



          $facturas = DB::select("SELECT * FROM public.vw_facturas WHERE id = ".$id);    
          $detalleFacturas = DB::select("SELECT * FROM public.vw_detalle_facturas WHERE id_factura = ".$id);

      
        if(!isset($factura[0]->id)){
          flash('La factura solicitada no existe. Intentelo nuevamente !!')->error();
              return redirect()->route('home');
        }


         $btn = $this->btnPermisos(array('id_factura' =>$id,'id_proforma'=>$factura[0]->id_proforma),'verFactura');
 
         $voucher = DB::select('SELECT COUNT(*) as contador from vouchers where id_proforma = '.$factura[0]->id_proforma .' AND activo = true' ) ;     
         $voucher = $voucher[0]->contador;   
         
        $pasajeroNombre = $this->getPasajero($factura[0]->id, null, $factura[0]->parcial);

        $cdc = $factura[0]->cdc;
  
        $fecha = '';
        if($factura[0]->fecha_hora_impresion_factura != ''){
                                    $r = explode(' ', $factura[0]->fecha_hora_impresion_factura);  
                                    $date = explode('-', $r[0]);
                                      $fecha = $date[2]."/".$date[1]."/".$date[0];
                                  } 
        $currency = Divisas::all();
        $totalFactura = DB::select('select get_monto_factura as total from get_monto_factura('. $factura[0]->id.')');
        // dd($totalFactura);
        $liquidacion =  new \StdClass;
        if($factura[0]->id_liquidacion_comision != "" && $factura[0]->id_liquidacion_comision != 0){
              $liquidacionBase = LiquidacionComision::where('id', $factura[0]->id_liquidacion_comision)->first();
              if(isset($liquidacionBase->periodo)){
                  $liquidacion->id = $factura[0]->id_usuario."_".substr($liquidacionBase->periodo, 0, -4)."_".substr($liquidacionBase->periodo, -4)."_".$factura[0]->id_liquidacion_comision;
                  $liquidacion->periodo = $this->getFecha(substr($liquidacionBase->periodo, 0, -4),substr($liquidacionBase->periodo, -4));
                }
        }
        $solicitudes = SolicitudFacturaParcial::where('id_proforma', $factura[0]->id_proforma)->get();
        $wordCount = $solicitudes->count();

       return view('pages.mc.factura.verFactura')->with(['facturas'=>$facturas,
                                                        'facturaDetalles'=>$detalleFacturas,
                                                        'voucher'=>$voucher,
                                                        'currency'=>$currency,
                                                        'totalFactura'=>$totalFactura,
                                                        'btn'=> $btn,
                                                        'fecha'=>$fecha,
                                                        "grupos"=>$grupos,
                                                        "pasajeroNombre"=>$pasajeroNombre,
                                                        "contador"=>$wordCount,
                                                        "liquidacion"=>$liquidacion,
                                                        "cdc"=>$cdc
                                                        ]);
 
    }
  
  private function getFecha($mes, $anho)      
  	{
  		$month = $mes; //Reemplazable por número del 1 a 12
		$year = $anho; //Reemplazable por un año valido
		
		switch(date('n',mktime(0, 0, 0, $month, 1, $year)))
		{
			case 1: $Mes = "Enero"; break;
			case 2: $Mes = "Febrero"; break;
			case 3: $Mes = "Marzo"; break;
			case 4: $Mes = "Abril"; break;
			case 5: $Mes = "Mayo"; break;
			case 6: $Mes = "Junio"; break;
			case 7: $Mes = "Julio"; break;
			case 8: $Mes = "Agosto"; break;
			case 9: $Mes = "Septiembre"; break;
			case 10: $Mes = "Octubre"; break;
			case 11: $Mes = "Noviembre"; break;
			case 12: $Mes = "Diciembre"; break;
		}
		
		return $Mes.'/'.date('Y',mktime(0, 0, 0, $month, 1, $year));
	}

public function ajaxDetalleProveedor(Request $req){ 

       $formSearch =$req->input('dataString');
       $idProveedor =  $formSearch['idProveedor'];
       $idProforma =  $formSearch['idProforma'];
       $nroFactura =  $formSearch['nroFactura'];
       $codigoConfirmacion =  $formSearch['codigoConfirmacion'];
       $checkin_desde_hasta =  $formSearch['checkin_desde_hasta'];
       $proveedor_desde_hasta =  $formSearch['proveedor_desde_hasta'];
       $idMoneda =  $formSearch['idMoneda'];
       $tieneFechaProveedor =  $formSearch['tieneFechaProveedor'];
       $tipo_negociacion =  $formSearch['tipo_negociacion'];
       $numOperacion =  $formSearch['numOperacion'];
       $idProformaNroFact = '';
       $columna = $req->order[0]['column'];
       $orden = $req->order[0]['dir'];
        $facturaDetalle = FacturaDetalle::query();
        $facturaDetalle =  $facturaDetalle->with('proveedor',
                                                 'prestador',
                                                 'producto',
                                                 'factura',
                                                 'vendedor',
                                                 'currencyVenta',
                                                 'voucher'
                                                 );
        $facturaDetalle =  DB::table(
          DB::raw("
                (SELECT 
                 fd.*,
                 f.nro_factura,
                 CONCAT(v.nombre,' ',v.apellido) vendedor_n,
                 prov.nombre as proveedor_n,
                  f.fecha_hora_facturacion,
                 to_char(f.fecha_hora_facturacion,'DD/MM/YYYY') as fecha_hora_facturacion_format,
                  f.id_pasajero_principal,
                  cu.currency_code as currency_code_venta,
                  prod.denominacion as producto_denominacion,
                  f.saldo_factura

                  FROM facturas_detalle fd 

                   LEFT JOIN personas prov ON prov.id = fd.id_proveedor
                   LEFT JOIN personas prest ON prest.id = fd.id_prestador
                   LEFT JOIN productos prod ON prod.id = fd.id_producto
                   LEFT JOIN facturas  f ON f.id = fd.id_factura
                   LEFT JOIN personas v ON v.id = fd.id_usuario
                   LEFT JOIN currency cu ON cu.currency_id = fd.currency_venta_id
                   LEFT JOIN proformas_detalle pd ON (pd.id_proforma = fd.id_proforma AND pd.item = fd.item AND pd.activo = true)

                  --No incluir productos del tipo TICKET
                  WHERE fd.id_producto NOT IN (SELECT id FROM productos WHERE id_grupos_producto = 1)

            ) as result
           
            
          
            ")
        );

            // $facturaDetalle =  $facturaDetalle->get();
            // dd($facturaDetalle);

        //EMPRESA DTP
        $facturaDetalle = $facturaDetalle->where('id_empresa',$this->getIdEmpresa());
        $facturaDetalle = $facturaDetalle->where('activo','=', true);

      if($nroFactura != ''){
        // $getFactura = Factura::where('nro_factura',$nroFactura)->first(); 
        //  if(!empty($getFactura) && !is_null($getFactura)){
            // $idProformaNroFact = $getFactura->id_proforma;
            $facturaDetalle = $facturaDetalle->where('nro_factura', $nroFactura);
         // }                     
      } //input nro_factura

         if($idProforma != ''){
          // $idProformaNroFact = ($idProforma != '') ? $idProforma : $idProformaNroFact ;
          $facturaDetalle = $facturaDetalle->where('id_proforma', $idProforma);
        }

        if($codigoConfirmacion!= ''){
        $facturaDetalle = $facturaDetalle->where('cod_confirmacion',$codigoConfirmacion);  
        }

        if($proveedor_desde_hasta != ''){
           $fecha = explode('-', $proveedor_desde_hasta);
                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));

        $facturaDetalle = $facturaDetalle->whereBetween('fecha_pago_proveedor',
                  array($desde.' 00:00:00',$hasta.' 23:59:59'));  
        }

        if($checkin_desde_hasta != ''){
          $fecha = explode('-', $checkin_desde_hasta);
          $desde = $this->formatoFechaEntrada(trim($fecha[0]));
          $hasta = $this->formatoFechaEntrada(trim($fecha[1]));

        $facturaDetalle = $facturaDetalle->whereBetween('fecha_in',array($desde,$hasta));  

        } 

        if($idProveedor != 0){
          $facturaDetalle = $facturaDetalle->where('id_proveedor',$idProveedor);
        }
        if($idMoneda != ''){
          $facturaDetalle = $facturaDetalle->where('currency_costo_id',$idMoneda);
        }


         if($tieneFechaProveedor != ''){
            $facturaDetalle = $facturaDetalle->where('pagado_proveedor',$tieneFechaProveedor);
        }  


        if($numOperacion != '') {
          $facturaDetalle = $facturaDetalle->where('nro_op',$numOperacion);
        }


         // dd($columna);
          switch ($columna) {
            case '2':
             // echo $columna." - ".$orden;
            $facturaDetalle = $facturaDetalle->orderBy('id', $orden);
            break;
            case '3':
            $facturaDetalle = $facturaDetalle->orderBy('cod_confirmacion', $orden);
            break;
            case '4':
            $facturaDetalle = $facturaDetalle->orderBy('currency_venta_id', $orden);
            break;
            case '5':
            $facturaDetalle = $facturaDetalle->orderBy('costo_proveedor', $orden);
            break;
            case '6':
            $facturaDetalle = $facturaDetalle->orderBy('id_producto', $orden);
            break;
            case '7':
            $facturaDetalle = $facturaDetalle->orderBy('id_proforma', $orden);
            break;
            case '8':
            $facturaDetalle = $facturaDetalle->orderBy('nro_factura', $orden);
            break;
            case '9':
          $facturaDetalle = $facturaDetalle->orderBy('fecha_hora_facturacion', $orden);
            break;
            case '10':
            $facturaDetalle = $facturaDetalle->orderBy('fecha_in', $orden);
            break;
            case '11':
            $facturaDetalle = $facturaDetalle->orderBy('id_usuario', $orden);
            break;
            case '12':
            $facturaDetalle = $facturaDetalle->orderBy('id_pasajero_principal', $orden);
            break;
            case '13':
           $facturaDetalle = $facturaDetalle->orderBy('fecha_pago_proveedor', $orden);
            break;
            case '14':
           $facturaDetalle = $facturaDetalle->orderBy('pagado_proveedor', $orden);
            break;
            case '15':
            $facturaDetalle = $facturaDetalle->orderBy('fecha_pagado_al_proveedor', $orden);
            break;
            case '16':
           $facturaDetalle = $facturaDetalle->orderBy('nro_op', $orden);
            break;
          default:
            # code...
            break;
        } 

        $facturaDetalle =  $facturaDetalle->get();
        $datosFact  = array(); 
        $draw = intval($req->draw);
        $start = intval($req->start);
        $length = intval($req->length);
        $lengtArray = count($facturaDetalle);
        $cont = 0;
        $base = 0;


       foreach ($facturaDetalle as $key => $detalle) {

        
      if($cont == $length){
        break;
      }
      


      if($key >= $start){
                        if($tipo_negociacion != ''){

                              $proveedor = Persona::with('plazoPago')->where('id',$detalle->id_proveedor)->get();
                              if(isset($proveedor[0]->plazoPago['cantidad_dias'])){ 
                                $mystring = $proveedor[0]->plazoPago['cantidad_dias'];
                                $findme   = '-';
                                if($tipo_negociacion === 'CONTADO'){
                                    if(intval($mystring) != 0){
                                      unset($facturaDetalle[$key]);
                                      continue;
                                    }  
                                }else if($tipo_negociacion === 'PREPAGO'){
                                    $pos = strpos($mystring, $findme);
                                    if($pos === false){
                                      unset($facturaDetalle[$key]);
                                      continue;
                                    }  
                                 }else if($tipo_negociacion === 'CREDITO'){
                                    $pos = strpos($mystring, $findme);
                                    if($pos !== false || $mystring == 0){
                                        unset($facturaDetalle[$key]);
                                        continue;
                                    }  
                                 }

                               }else{
                                    unset($facturaDetalle[$key]);
                                    continue;
                               
                               }

                           }//if*/


                          $facturaBase = Factura::where('id',$detalle->id_factura)
                                                  ->with('vendedorEmpresa')
                                                  ->first(); 
                                                  
                          if($detalle->id_usuario != 4987){
                              // $detalle->vendedor = $detalle->vendedor->nombre ." ".$detalle->vendedor->apellido;
                              $detalle->vendedor = $detalle->vendedor_n;
                          }else{
                              $detalle->vendedor =$facturaBase->usuario->nombre ." ".$facturaBase->usuario->apellido;
                          }                                            
                 
                         // $detalle->fecha_hora_facturacion = date("d/m/Y",strtotime($detalle->factura['fecha_hora_facturacion']));
                         $detalle->fecha_hora_facturacion =  $detalle->fecha_hora_facturacion_format;

                         if($facturaBase->id_estado_factura == 29){

                              $proforma = ProformasDetalle::where('id_proforma', $detalle->id_proforma)
                                                            ->with('voucher')
                                                            ->where('item', $detalle->item)
                                                            ->first();
                                                            
                              if(isset($proforma->costo_proveedor)){                                
                                   $detalle->monto = $proforma->costo_proveedor;
                              }else{
                                   $detalle->monto = 0;
                              }     
                              $idProforma = $detalle->id_proforma;

                              if($detalle->pagado_proveedor == true){

                              $detalle->pago_proveedor_b = 'SI';
                              } else {
                                 $detalle->pago_proveedor_b = 'NO';
                              }


                              if(!is_null($idProforma) && !empty($idProforma)){

                               $factura = Factura::with('pasajero')->where('id_proforma',$idProforma)->first();
                             

                               if(!is_null($factura) && !empty($factura)){

                                  $estadoFactura = $factura->id_estado_factura;

                                  $detalle->pasajeroPrincipal = "";

                                  if($factura->id_pasajero_principal != 4994){
                                      if(isset($factura->pasajero->nombre)){
                                        $detalle->pasajeroPrincipal = $factura->pasajero->nombre;  
                                      }else{
                                        $detalle->pasajeroPrincipal = "";
                                      }
                                     } else {
                                      if(isset($proforma->voucher[0]->pasajero_principal)){
                                        $detalle->pasajeroPrincipal = $proforma->voucher[0]->pasajero_principal;  
                                      }else{
                                        $detalle->pasajeroPrincipal = "";
                                      }
                                  }    


                                }else{
                                  $detalle->pasajeroPrincipal = "";
                                }

                                if(!is_null($detalle->fecha_in) && !empty($detalle->fecha_in)){
                                  $fecha = explode(' ',$detalle->fecha_in);
                                  $detalle->fecha_in = $this->formatoFechaSalida($fecha[0]);
                                }//if
                                 if(!is_null($detalle->fecha_out) && !empty($detalle->fecha_out)){
                                  $detalle->fecha_out = $this->formatoFechaSalida($detalle->fecha_out);
                                }//if
                                 if(!is_null($detalle->fecha_pago_proveedor) && !empty($detalle->fecha_pago_proveedor)){
                                  $detalle->fecha_pago_proveedor = $this->formatoFechaSalida($detalle->fecha_pago_proveedor);
                                }//if

                              }//if

                              $pendiente_ico = '';
                              if($detalle->saldo_factura > 0){
                                $pendiente_ico = '<i class="fa fa-exclamation-triangle" style="color:red"></i>';
                              }

                              

                              if(isset($detalle->fecha_pagado_al_proveedor)){
                                $fechaPago = date('d/m/Y', strtotime($detalle->fecha_pagado_al_proveedor));
                              }else{
                                $fechaPago = "";
                              }
                              $datosFact[] =    [
                                          '<input type="hidden" value="'.$detalle->id.'" id="moneda_'.$detalle->id.'" class="oculto">',
                                          $detalle->id,
                                          $pendiente_ico,
                                          $detalle->id_proforma,
                                          $detalle->nro_factura,
                                          $detalle->fecha_hora_facturacion,
                                          $detalle->fecha_in,
                                          $detalle->vendedor,
                                          $detalle->pasajeroPrincipal,
                                          $detalle->monto,
                                          $detalle->cod_confirmacion,
                                          // $detalle->proveedor->nombre,
                                          $detalle->proveedor_n,
                                          // $detalle->currencyVenta['currency_code'],
                                          $detalle->currency_code_venta,
                                          // $detalle->producto->denominacion,
                                          $detalle->producto_denominacion,
                                          // $detalle->factura->nro_factura,
                                          $detalle->fecha_pago_proveedor,
                                          $detalle->pago_proveedor_b,
                                          $fechaPago, 
                                          $detalle->nro_op,
                                          '<a class="btn btn-success" onclick="verVoucher('.$detalle->id_proforma.','.$detalle->item.')" data-toggle="modal" data-target="#modalAdjunto" style="padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-files-o fa-lg"></i></a>'
                                   ];
                        
                                      $cont++;
                                      $base++;
                // array_push($datosFact, $detalle);
      }

    }
      
    }//foreach
    $lengtArray = count($facturaDetalle);
    return response()->json(['draw'=>$draw,
                 'recordsTotal'=>$lengtArray,
                 'recordsFiltered'=>$lengtArray,
                 'data'=>$datosFact]); 



      // $fact = array('facturaDetalle'=>$datosFact);
      //   return response()->json($fact);
   
    }//FUNCTION




 private function formatMoney($num,$f)  {
    if($f != 111){
    return number_format($num, 2, ",", ".");  
    } 
    return number_format($num,0,",",".");

  }//function


  public function btnPermisos($parametros, $vista){

  $btn = DB::select("SELECT pe.* 
                        FROM persona_permiso_especiales p, permisos_especiales pe
                        WHERE p.id_permiso = pe.id 
                        AND p.id_persona = ".$this->getIdUsuario() ." 
                        AND pe.url = '".$vista."' ");

  $htmlBtn = '';
  $boton =  array();
  $idParametro = '';

  // dd( $btn );

  if(!empty($btn)){

  foreach ($btn as $key => $value) {

    $idParametro = '';
    $ruta = $value->accion;
     $htmlBtn = '';



     //LLEVA PARAMETRO
    if($value->lleva_parametro == '1'){

    foreach ($parametros as $indice=>$valor) { 

      if($indice == $value->nombre_parametro){
        $idParametro = $valor;
      }
    }

    //PARAMETRO OCULTO EN DATA
    if($value->parametro_oculto == '1'){
       $htmlBtn = "<a role='button' class='".$value->clase."' data-btn='".$idParametro."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    } else {
       $htmlBtn = "<a role='button' href='".route($ruta,['id'=>$idParametro])."' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i> ".$value->texto_alternativo." </a>";  
    }

    } else {
      $htmlBtn = "<a role='button' href='#' class='".$value->clase."'   title='".$value->titulo."'><i class='".$value->icono."'></i>".$value->texto_alternativo." </a>"; 
    }

    
    $boton[] = $htmlBtn;
  }
   return $boton;
 
   } else {
    return array();
   }



}//function

 private function  responseFormatDatatable($req){

    $datos = array();
      $data =  new \StdClass;
    // if(!isset($req->formSearch)){ 
    foreach ($req->formSearch as $key => $value) {

     $n = $value['name'];
         $datos[$value['name']] = $value['value'];
         $data-> $n = $value['value'];
   }
  // }

    return $data;

 } 



  public function reporteRiesgo(){


    $persona = DB::table('personas as p')
    ->select('p.id','p.nombre', 'p.denominacion_comercial','tp.denominacion')
    ->leftJoin('tipo_persona as tp','tp.id','=','p.id_tipo_persona')
    ->where('id_empresa',$this->getIdEmpresa())
    ->where('tp.puede_facturar','true')
    ->orderBy('nombre','ASC')->get();
    // dd();
    $id_empresa = $this->getIdEmpresa();
    if($id_empresa == 1 || $id_empresa == 4){
      $tipoPersona = TipoPersona::where('puede_facturar','true')->orderBy('denominacion','ASC')->get();
    }else{
      $tipoPersona = TipoPersona::where('puede_facturar',true)->where('ver_agencia','false')->orderBy('denominacion','ASC')->get();
    }
   // $tipoPersona = TipoPersona::where('puede_facturar','true')->orderBy('denominacion','ASC')->get();

    return view('pages.mc.factura.reporteRiesgo')->with(['persona'=>$persona, 'tipoPersona'=>$tipoPersona]);



  }


  public function ajaxReporteRiesgo(Request $req){
 
    $datos = $this->responseFormatDatatable($req);
    $vistaRiesgo = DB::table('vw_riesgo_cliente');
 
    if($datos->id_persona != ''){
     $vistaRiesgo =  $vistaRiesgo->where('id_cliente',$datos->id_persona);
    }
    if($datos->id_tipo_persona != ''){
      $vistaRiesgo =  $vistaRiesgo->where('id_tipo_persona',$datos->id_tipo_persona);
    }
    if($datos->ruc != ''){
       $vistaRiesgo =  $vistaRiesgo->where('documento_identidad','LIKE','%'.trim($datos->ruc).'%');
    }
     if($datos->id_estado_saldo != ''){
       $vistaRiesgo =  $vistaRiesgo->where('id_estado_saldo',$datos->id_estado_saldo);
    }
      $vistaRiesgo =  $vistaRiesgo->where('id_empresa',$this->getIdEmpresa());

      $vistaRiesgo =  $vistaRiesgo->get();
    

    return response()->json(['data'=>$vistaRiesgo]);


  }



   public function verVentaFactura(Request $request, $id){
      ini_set('memory_limit', '-1');
      set_time_limit(300);
      $facturas = Factura::with('cliente',
                              'pasajero',
                              'vendedorAgencia',
                              'vendedorEmpresa',
                              'tipoFactura',
                              'tipoFacturacion',
                              'proforma',
                              'currency',
                              'facturaDetalle',
                              'estado',
                              'ventaRapida',
                              'usuario'
                              )
                              ->where('id',$id)->get();
      if(isset($facturas[0]->ventaRapida->id_comercio_empresa)){
        $comercio= ComercioPersona::where('id',$facturas[0]->ventaRapida->id_comercio_empresa)->first();
      }else{
        $comercio=[];
      }
      $usuarioImpresion = Persona::where('id',$facturas[0]->usuario_facturacion_id)->first();
      foreach($facturas[0]->facturaDetalle as $key=>$facturaDetalle){
          $producto = Producto::where('id',$facturaDetalle->id_producto)->first();
          $proveedor = Persona::where('id',$facturaDetalle->id_proveedor)->first();
          /*echo "</pre>"; 
          print_r($producto);*/
          $facturas[0]->facturaDetalle->producto = $producto;
          $facturas[0]->facturaDetalle->proveedor = $proveedor;
                 
      }

      $btn = $this->btnPermisoVenta(array('id_factura' =>$id),'verFactura',$facturas[0]->id_estado_factura);
      $persona = Persona::all();
      $totalFactura = DB::select('select get_monto_factura as total from get_monto_factura('. $facturas[0]->id.')');
      return view('pages.mc.ventas.verFactura')->with(['facturas'=>$facturas, 'usuarioImpresion'=>$usuarioImpresion, 'comercio'=>$comercio, 'totalFactura'=>$totalFactura, 'personas'=>$persona, 'btn'=>$btn]);
  }

    public function getExtracto(Request $req){
      $exacto =  DB::table('vw_extracto_factura');
      $exacto =  $exacto->where('id_factura',$req->input("idFactura"));
                        //->whereNotNull('id_factura_anterior') ;
      $exacto =  $exacto->get();
      return response()->json(['exacto'=>$exacto]);
    }

    public function btnPermiso($parametros, $vista){

      $btn = DB::select("SELECT pe.* 
                          FROM persona_permiso_especiales p, permisos_especiales pe
                          WHERE p.id_permiso = pe.id 
                          AND p.id_persona = ".$this->getIdUsuario() ." 
                          AND pe.url = '".$vista."' ");
      //print_r($parametros['id_factura']);
      $htmlBtn = '';
        if(!empty($btn)){
        foreach ($btn as $key => $value){
            $ruta = $value->accion;
            $htmlBtn = '';
            $htmlBtn = "<a href='../imp_fact_pedido/".$parametros['id_factura']."' class='".$value->clase."' data-btn='".$parametros['id_factura']."' title='".$value->titulo."'>".$value->titulo."</a>";
            $boton = $htmlBtn;
        }
            return $boton;
        } else {
          $htmlBtn = '<a href="../imp_fact_pedido/'.$parametros['id_factura'].'" class="btn btn-danger" role="button">Imprimir Factura</a>';
          $boton = $htmlBtn;
          return $boton;
      }
  }//function

      public function btnPermisoVenta($parametros, $vista, $estado){

        $btn = DB::select("SELECT pe.* 
                            FROM persona_permiso_especiales p, permisos_especiales pe
                            WHERE p.id_permiso = pe.id 
                            AND p.id_persona = ".$this->getIdUsuario() ." 
                            AND pe.url = '".$vista."' ");


        $htmlBtn = '';
        $boton = [];
        if(!empty($btn)){
          foreach ($btn as $key => $value){
            $ruta = $value->accion;
              if($ruta == 'imprimirFactura'){
                $htmlBtn = '';
                $htmlBtn = "<a href='../imp_fact_pedido/".$parametros['id_factura']."' class='".$value->clase."' data-btn='".$parametros['id_factura']."' title='".$value->titulo."'><i class='fa fa-print fa-lg'></i></a>";
                $boton[] = $htmlBtn;
              }
              if($estado != 30){
                if($ruta == 'notaCredito'){
                  $htmlBtn = '';
                  $htmlBtn = "<a href='../notaCreditoVenta/".$parametros['id_factura']."' class='".$value->clase."' data-btn='".$parametros['id_factura']."' title='".$value->titulo."'><i class='fa fa-file fa-lg'></i></a>";
                  $boton[] = $htmlBtn;
                }
              }
        }
      }
      return $boton;
    }
    
    public function enviarFactura(Request $request, $id){
        $factura = Factura::with('cliente',
                                  'vendedorAgencia',
                                  'vendedorEmpresa')
                                  ->where('id',$id)->first();
                                  
        $correoAdministrativo = explode(',', $factura->cliente->correo_administrativo);
        foreach($correoAdministrativo as $key=>$correo){
            $getNotificacion= DB::select("SELECT public.insert_notificacion(".$factura->id_usuario.",'".$correo."', null, null,21,".$id.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
        }

        if(isset($factura->cliente->email)){
            $getNotificacion= DB::select("SELECT public.insert_notificacion(".$factura->id_usuario.",'".$factura->cliente->email."', null, null,21,".$id.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
        }

        if(isset($factura->vendedorAgencia->email)){
          $getNotificacion= DB::select("SELECT public.insert_notificacion(".$factura->id_usuario.",'".$factura->vendedorAgencia->email."', null, null,21,".$id.",".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");

        }
        flash('Enviamos un correo con la factura.')->success();
        return redirect()->route('verFactura',['id'=>$id]);
      }


      public function generarFacturaParcial(Request $req){
          $solicitud = SolicitudFacturaParcial::where('id', $req->input("id"))->first();
          $getFacturacionParcial= DB::select("SELECT public.facturar_parcial(".$req->input("dataPuntoVenta").",".$solicitud->id_proforma.",".$this->getIdUsuario().",".$req->input("id").")");
          $okFactura = $getFacturacionParcial[0]->facturar_parcial;
          $mensaje = new \StdClass; 
          if(isset($getFacturacionParcial[0]->facturar_parcial)){
            $respuesta = explode ('_',$getFacturacionParcial[0]->facturar_parcial);
            if($respuesta[0] === 'OK'){
                $facturaId = Factura::where('id_proforma',$solicitud->id_proforma)->where('id_estado_factura','29')->first(['id']);
                $mensaje->id =  $respuesta[1];
                $mensaje->status = 'OK';
                $mensaje->mensaje = 'La Factura Parcial se genero exitosamente';
            }else{
              $mensaje->id = 0;
              $mensaje->status = 'ERROR';
              $mensaje->mensaje = $getFacturacionParcial[0]->facturar_parcial;
            }
        }else{ 
          $mensaje->status = 'ERROR';
          $mensaje->mensaje = 'No se pudo generar la Factura Parcial ';
        }
        return json_encode($mensaje);
    }


    public function comentarios(Request $req){
      $comentarios = HistoricoComentariosProforma::with('persona')
                                                ->where('id_proforma',$req->input("dataProforma"))
                                                ->orderBy('fecha_hora','DESC')
                                                ->get();

      return json_encode($comentarios);
    }

    public function anularAnticipo(Request $req){
          $mensaje = new \StdClass;
         // try{
              $getAnularAnticipo= DB::select("SELECT public.anular_aplicacion_anticipo(".$req->input("idAnticipo").",".$this->getIdUsuario().")");
              $okFactura = $getAnularAnticipo[0]->anular_aplicacion_anticipo;
              $mensaje = new \StdClass; 
              if($okFactura === 'OK'){
                    $mensaje->status = $okFactura;
                    $mensaje->mensaje = 'La Aplicación de Anticipo fue anulada exitosamente.';
              }else{
                  $mensaje->status = 'ERROR';
              }
         /* } catch(\Exception $e){
            $mensaje->status = 'ERROR';
          }*/
         return json_encode($mensaje);
    }
    /**
     * Segun parametro empresa obtiene el pasajero principal o todos los pasajeros
     */
    public function getPasajero($id_factura = null, $id_proforma = null, $facturacion_parcial = false){

      $pasajero_nombre = '';
      $empresa = Empresa::find($this->getIdEmpresa());
      $factura = Factura::find($id_factura);

      //Si es preview proforma generamos un modelo para no cambiar nada abajo
      if($id_proforma){
        $proforma = Proforma::select('pasajero_id')->find($id_proforma);
        $factura = new Factura;
        $factura->id_proforma = $id_proforma;
        $factura->id_pasajero_principal = $proforma->pasajero_id;
        $factura->pasajero_online = '';
      }

      if($facturacion_parcial){
        if($factura->id_pasajero_principal){
          $pasajero_nombres= DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n
                      FROM personas WHERE personas.id = ".$factura->id_pasajero_principal);

          if(isset($pasajero_nombres[0]->pasajero_n)){
              $pasajero_nombre = $pasajero_nombres[0]->pasajero_n;
          }
          
        }
      }else if($empresa->pasajero_factura == 'TODOS'){

        $pasajeros = DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n
                                FROM personas 
                                WHERE id IN (
                                              SELECT pasajero FROM pasajeros_proformas WHERE id_proforma=".$factura->id_proforma."
                                              ) LIMIT 3");
        foreach ($pasajeros as $pasajero) {
          $pasajero_nombre.= $pasajero->pasajero_n.' ,';
        }

      } else if($empresa->pasajero_factura == 'PRINCIPAL') {
        if($factura->id_pasajero_principal){
          $pasajero_nombres= DB::select("SELECT concat(nombre,' ',apellido) as pasajero_n
                      FROM personas WHERE personas.id = ".$factura->id_pasajero_principal);

          if(isset($pasajero_nombres[0]->pasajero_n)){
              $pasajero_nombre = $pasajero_nombres[0]->pasajero_n;
          }
          
        }

      }
      
      $pasajero_nombre = rtrim($pasajero_nombre, ',');
      return $pasajero_nombre;

    }

    public function securityCode($numeroBase){
        while (strlen($numeroBase) < 9) {
            $digitoAleatorio = rand(0, 9); // Generar un dígito aleatorio entre 0 y 9
            $numeroBase .= $digitoAleatorio; // Agregar el dígito al número base
        }
        
        return $numeroBase;  
    }

  public function generarJsonFactura(Request $req, $id){

    if($this->getIdEmpresa() == 1){
        $username = 'admin@dtparaguayo.com.py';
       // $password = 'TGru4HuxiE';
        $password = 'ILZXg16OE';
    }else{
        $username = 'admin@boardingpass.com.py';
        //  $password = 'tiRRmrHQce';
        $password = 'RA3mAlRUEF';   
    }

    $facturas = DB::select("SELECT * FROM public.vw_facturas WHERE id = ".$id); 

    $detalleFacturas = DB::select("SELECT * FROM public.vw_detalle_facturas WHERE id_factura = ".$id);

    $securityCode = $this->securityCode($id);

    $fechaEmision = $facturas[0]->fecha_hora_facturacion;    

    $format = 'Y-m-d H:i:s';

    $date = \DateTime::createFromFormat($format, $fechaEmision);
    if ($date) {
        $formattedDate = $date->format('Y-m-d\TH:i:s');
    } else {
        echo 'Fecha inválida';
    }

    $moneda_factura = $facturas[0]->moneda_venta;

    $nombre_cliente = $facturas[0]->cliente_nombre;
    if($facturas[0]->cliente_apellido != ""){
        $nombre_cliente = $nombre_cliente." ".$facturas[0]->cliente_apellido;
    }
    $tipo_facturacion = 0;
    if($facturas[0]->id_tipo_facturacion == 2){
      $tipo_facturacion = 1;
    }

    $direccion = $facturas[0]->direccion_cliente;
    if($facturas[0]->nombre_pais == ""){
      $pais = 'Paraguay'; 
      $cod_pais = 107; 
    }else{
      $pais = $facturas[0]->nombre_pais; 
      $cod_pais = $facturas[0]->cod_pais; 
    }

    $idEmpresa = $facturas[0]->id_empresa;
    if($facturas[0]->dv_cliente == ""){
       $ruc = $facturas[0]->documento_cliente;
    }else{
       $ruc = $facturas[0]->documento_cliente."-".$facturas[0]->dv_cliente;

    }
    $personeria = $facturas[0]->id_personeria;
    $correo_cliente = $facturas[0]->cliente_email;
    if($personeria == 0 || $personeria == 0){
      $personeria = 1;
    }
    $base_factura = explode('-',$facturas[0]->nro_factura);
    $numbero = (int)$base_factura[2];
    if($this->getIdEmpresa() == 1){
        $totalFactura = $facturas[0]->total_neto_factura;
    }else{
        $total_Factura = DB::select('SELECT * FROM get_monto_factura('.$facturas[0]->id.')');
        $totalFactura = $total_Factura[0]->get_monto_factura;
    }
    $empresa_nombre = $facturas[0]->empresa_nombre; 
    $empresa_direccion = $facturas[0]->empresa_direccion; 
    $empresa_telefono = str_replace('+ 595 ','0',$facturas[0]->empresa_telefono);
    $empresa_correo = $facturas[0]->empresa_correo;
    $timbrado_establecimiento = $facturas[0]->timbrado_establecimiento;
    $timbrado_expedicion = $facturas[0]->timbrado_expedicion;
    $timbrado_fecha = $facturas[0]->timbrado_autorizacion;
    if($this->getIdEmpresa() == 1){
        $iva = round((($totalFactura * 11)/100), 2);
    }else{
        $iva = $facturas[0]->total_iva;
    }

    $sub_total = (float)$totalFactura - (float)$iva;
    $sub_total =number_format($sub_total,2,".","");
    $empresa_ruc = $facturas[0]->empresa_ruc;
    $timbrado_numero = $facturas[0]->timbrado_numero;

    if($moneda_factura == 'PYG'){
      $cotizacion = 0;
    }else{
      $cotizacion = $facturas[0]->cotizacion_factura;
    }
    if($this->getIdEmpresa() == 1){
      $logo = 3;
   }else{
      $logo =  $facturas[0]->logo;
    }

    $json = [
            'ElectronicDocuments' => [
                [
                    'issuingType' => 0,
                    'securityCode' => $securityCode,
                    'aditionalInformation' => '',
                    'aditionalTreasuryInformation' => '',
                    'documentTypeCode' => 1,
                    'number' => $numbero,
                    'series' => 'AA',
                    'issuedDate' => $formattedDate,
                    'transactionTypeCode' => 2,
                    'taxTypeCode' => 1,
                    'tenantLogoId' => $logo ,
                    'currencyTypeCode' => $moneda_factura,
                    'exchangeRate' => $cotizacion,
                    'client' => [
                        'nature' => 1,
                        'operationType' => 1,
                        'countryCode' => $cod_pais,
                        'countryName' => $pais,
                        'contributorType' => $personeria, 
                        'ruc' => $ruc,
                        'businessName' => $nombre_cliente,
                        'fantasyName' => $nombre_cliente,
                        'address' => $direccion,
                        'email' => $correo_cliente,
                    ],
                    'operationCondition' => $tipo_facturacion,
                    'subTotal' => $sub_total,
                    'total' => $totalFactura,
                    'tax' => $iva,
                    'electronicDocumentItems' => [],
                    'branch' => [
                        'branchDocumentTypes' => [
                            [
                                'stablishment' =>   $timbrado_establecimiento ,
                                'expeditionPoint' =>  $timbrado_expedicion ,
                                'stampNumber' => $timbrado_numero,
                                'stampDate' => $timbrado_fecha,
                            ],
                        ],
                        'address' => $empresa_direccion,
                        'houseNumber' => 1078,
                        'addressComplement1' => '',
                        'addressComplement2' => '',
                        'cityCode' => 1,
                        'phone' => $empresa_telefono,
                        'email' => $empresa_correo,
                       // 'name' => 'Matriz',
                    ],
                    'PresenceIndicatorCode' => 1,
                    'payments' => [
                        [
                            'paymentMethodCode' => 1,
                            'ammount' => $totalFactura,
                        ],
                    ],
                    'tenant' => [
                        'stampNumber' => $timbrado_numero,
                        'stampDate' => $timbrado_fecha,
                        'ruc' => $empresa_ruc,
                        'taxpayerType' => 2,
                        'name' => $empresa_nombre,
                        'activities' => [
                            [
                                'name' => 'ACTIVIDADES DE LOS OPERADORES TURÍSTICOS',
                                'identifier' => '79120',
                            ],
                        ],
                    ],
                ],
            ],
        ];
        
        $items = [];

        foreach ($detalleFacturas as $detalleFactura) {
            if($this->getIdEmpresa() == 1){
                $precio_venta = $detalleFactura->total_neto;
                $gravadas_10 = $detalleFactura->gravada_10_neta;
                $exento = $detalleFactura->exento;
            }else{
                $precio_venta = $detalleFactura->precio_venta;
                $gravadas_10 = $detalleFactura->gravadas_10;
                $exento = $detalleFactura->exento;
            }

            if($exento != 0 && $gravadas_10 == 0){
                $taxImpact = 2;
                $porcentaje = 0;
                $taxRate = 0;
            }

            if($exento == 0 && $gravadas_10 != 0){
              $taxImpact = 0;
              $porcentaje = 100;
              $taxRate = 10;
            }

            if($exento != 0 && $gravadas_10 != 0){
              $taxImpact = 3;
              $porcentaje = round((((float)$gravadas_10 * 100)/(float)$precio_venta), 2);
              if($porcentaje > 99){
                $porcentaje = 99;
              }
              $taxRate = 10;
            }

            if($exento == 0 && $gravadas_10 == 0){
              $taxImpact = 2;
              $porcentaje = 0;
              $taxRate = 0;
            }

            $item = [
                      'internalCode' => $detalleFactura->cod_confirmacion,
                      'description' => $detalleFactura->descripcion,
                      'measurementUnitCode' => 4,
                      'quantity' => $detalleFactura->cantidad,
                      'informationOfInterest' => 'info opcional',
                      'unitPriceWithTax' => $precio_venta,
                      'itemExchangeRate' => 1,
                      'itemUnitPriceDiscountWithTax' => 0,
                      'itemDiscountPercentage' => 0,
                      'itemUnitPriceGlobalDiscountWithTax' => 0,
                      'itemUnitPriceAdvanceWithTax' => 0,
                      'itemUnitPriceGlobalAdvanceWithTax' => 0,
                      'taxImpact' => $taxImpact,
                      'taxedProportion' => $porcentaje,
                      'taxRate' => $taxRate,
                    ];
        
            $items[] = $item;
        }
        
        $json['ElectronicDocuments'][0]['electronicDocumentItems'] = $items;

        if($facturas[0]->id_tipo_facturacion == 2){
            $fecha1 = new \DateTime(date('Y-m-d', strtotime($fechaEmision)));
            $fecha2 = new \DateTime($facturas[0]->vencimiento);
  
            $diferencia = $fecha1->diff($fecha2);
            $dias = $diferencia->days;

            $credit = [
                      'creditOperationCondition' => 0,
                      'creditDeadline' => $dias.' dias',
                      'initialDeliveryAmmount' => 0,
                    ];

            $json['ElectronicDocuments'][0]['credit'] = $credit;
        }

        if($cod_pais != 107 ){
            $facturaDetalle = Persona::with('pais')
                                      ->where('id',$facturas[0]->cliente_id)
                                      ->first(); 
           $nombre_cliente = $facturaDetalle->nombre;
           if($facturaDetalle->apellido!= ""){
                   $nombre_cliente = $facturaDetalle->nombre." ".$facturaDetalle->apellido;
            }
                                      
            if(isset($facturaDetalle->pais->id_set)){
                $codigo_pais = $facturaDetalle->pais->id_set;
            }else{
                $codigo_pais = 249;
            } 
            $json['ElectronicDocuments'][0]['client'] = [
              'nature' => 2,
              'ruc' => $facturaDetalle->documento_identidad,
              'businessName' => $nombre_cliente,
              'fantasyName' => $nombre_cliente,
              'contributorType' => 1,
              'identityDocumentTypeCode' => '3',
              'operationType' => '4',
              'countryCode' => $codigo_pais,
              'email' => $facturaDetalle->email
            ];
        }elseif($facturas[0]->dv_cliente == ""){
            $facturaDetalle = Persona::with('pais')
                                  ->where('id',$facturas[0]->cliente_id)
                                  ->first(); 

            $nombre_cliente = $facturaDetalle->nombre;
            if($facturaDetalle->apellido!= ""){
                   $nombre_cliente = $facturaDetalle->nombre." ".$facturaDetalle->apellido;
            }
                              
            if(isset($facturaDetalle->pais->id_set)){
              $codigo_pais = $facturaDetalle->pais->id_set;
            }else{
              $codigo_pais = 249;
            }  
            $json['ElectronicDocuments'][0]['client'] = [
                                                        'nature' => 2,
                                                        'operationType' => 2,
                                                        'identityDocumentNumber' => $facturaDetalle->documento_identidad,
                                                        'businessName' => $nombre_cliente,
                                                        'fantasyName' => $nombre_cliente,
                                                        'contributorType' => 1,
                                                        'identityDocumentTypeCode' => '1',
                                                        'countryCode' => $codigo_pais,
                                                        'email' => $correo_cliente
                                                        ];
        }

        $client = new Client();
      //  $response = $client->post('http://3.140.122.68:8080/Token', [
        $response = $client->post('http://3.140.122.68/Token', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                'username' => $username,
                'password' => $password,
                'grant_type' => 'password',
            ],
        ]);
        
        $body = $response->getBody();
        $data = json_decode($body, true);
        
        // Accede a los datos de la respuesta
        $accessToken = $data['access_token'];

       /* echo '<pre>';
        print_r(json_encode($accessToken));*/

       // $response = $client->post('http://3.140.122.68:8080/api/electronicDocument/Bulk', [
        $response = $client->post('http://3.140.122.68/api/electronicDocument/Bulk', [

            'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$accessToken,
                        ],
            'json' => $json
        ]);
        
        $responseData = json_decode($response->getBody(), true);

       /* echo '<pre>';
        print_r($responseData); */

        $cdc = $responseData['Items'][0]['CDC'];
        $url = $responseData['Items'][0]['DCarQR'];
        $id_impresion = $responseData['Items'][0]['Id'];
        $id_api = $responseData['Id'];

        $documento = new DocumentosEcos;
        $documento->id_documento = $id;
        $documento->id_api = $id_api;
        $documento->id_impresion = $id_impresion;
        $documento->tipo_documento = 1;
        $documento->cdc = $cdc;
        $documento->dcarqr = $url;
        $documento->request = json_encode($json);
        $documento->response = json_encode($responseData);
        $documento->fecha_envio = date('Y-m-d H:m:s'); 
        $documento->id_empresa = $idEmpresa; 
        $documento->save();
        $id_eco = $documento->id;
        
        $update = DB::table('facturas')
                    ->where('id',$id)
                    ->update(['cdc'=>$cdc]);
        
       $getNotificacion= DB::select("SELECT public.insert_notificacion(".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.",'".$correo_cliente."', null, null,27,".$id_eco.",".$idEmpresa.")");

        return 'OK';
  }

  public function generarJsonNC(Request $req, $id){

    if($this->getIdEmpresa() == 1){
        $username = 'admin@dtparaguayo.com.py';
       // $password = 'ILZXg16OE';
        $password = 'TGru4HuxiE';
    }else{
        $username = 'admin@boardingpass.com.py';
        //$password = 'tiRRmrHQce';
        $password = 'RA3mAlRUEF';   
    }

    $notaCredito = NotaCredito::with('cliente',
                                    'proforma',
                                    'currency',
                                    'timbrado',
                                    'factura',
                                    'factura.vendedorEmpresa',
                                    'notaCreditoDetalle',
                                    'libroVentaSaldo0')
                               ->where('id',$id)->get(); 

    $securityCode = $this->securityCode($id);

    $fechaEmision = $notaCredito[0]->fecha_hora_nota_credito;    

    $format = 'Y-m-d H:i:s';

    $date = \DateTime::createFromFormat($format, $fechaEmision);
    if ($date) {
        $formattedDate = $date->format('Y-m-d\TH:i:s');
    } else {
        echo 'Fecha inválida';
    }

    $moneda_factura = $notaCredito[0]->currency->currency_code;
    $nombre_cliente = $notaCredito[0]->cliente->nombre;
    $correo_cliente =$notaCredito[0]->cliente->email;
    if($notaCredito[0]->cliente->apellido != ""){
        $nombre_cliente = $nombre_cliente." ".$notaCredito[0]->cliente->apellido;
    }
    $id_empresa = $notaCredito[0]->id_empresa;
    $direccion = $notaCredito[0]->cliente->direccion;
    $code_pais = $notaCredito[0]->cliente->id_pais;
    if($code_pais == ""){
        $pais = 'Paraguay'; 
        $cod_pais = 107; 
    }else{
        $paises = Pais::where('cod_pais', $code_pais)->first();
        $pais = $paises->name_es; 
        $cod_pais = $paises->id_set; 
    }

    $ruc = $notaCredito[0]->cliente->documento_identidad."-".$notaCredito[0]->cliente->dv;
    $personeria = $notaCredito[0]->cliente->id_personeria;
    
    if($personeria == ''){
        $personeria = 1;
    }
    $base_factura = explode('-',$notaCredito[0]->nro_nota_credito);
    $numbero = (int)$base_factura[2];
    $totalFactura =$notaCredito[0]->total_neto_nc;
    $empresa = Empresa::where('id',$id_empresa)->first();   
    $empresa_nombre = $empresa->denominacion; 
    $empresa_direccion = $empresa->direccion; 
    $empresa_telefono = str_replace('+ 595 ','0',$empresa->telefono);
    $empresa_correo = $empresa->correo_set;
    $timbrado_establecimiento = $notaCredito[0]->timbrado->establecimiento;
    $timbrado_expedicion =$notaCredito[0]->timbrado->expedicion;
    $timbrado_fecha = $notaCredito[0]->timbrado->fecha_autorizacion;
    $iva = $notaCredito[0]->total_iva;
    $sub_total = (float)$totalFactura - (float)$iva;
    $sub_total =number_format($sub_total,2,".","");
    $empresa_ruc = $empresa->ruc_set;

    $base = explode('-', $empresa->ruc_set);

    $timbrado_numero =$notaCredito[0]->timbrado->numero;

    if($moneda_factura == 'PYG'){
      $cotizacion = 0;
    }else{
      $cotizacion = $notaCredito[0]->cotizacion;
    }
    $base_factura = explode('-', $notaCredito[0]->factura[0]->nro_factura);
    $cdc = $notaCredito[0]->factura[0]->cdc;
    $stamp_number = $notaCredito[0]->factura[0]->stamp_number;
    $fechaEmisionF = $notaCredito[0]->factura[0]->fecha_hora_facturacion;    

    $idTimbradoFactura = $notaCredito[0]->factura[0]->id_timbrado;

    $timbradoFactura = Timbrado::findOrFail($idTimbradoFactura);	

    $stampNumber = $timbradoFactura->numero;

    $format = 'Y-m-d H:i:s';

    $date = \DateTime::createFromFormat($format, $fechaEmisionF);
    if ($date) {
        $formattedDateF = $date->format('Y-m-d\TH:i:s');
    } else {
        echo 'Fecha inválida';
    }

    if($notaCredito[0]->factura[0]->cdc == '' ){
        $associatedDocumentType = 1;
    }else{
        $associatedDocumentType = 0;
    }


    $json = [
              'ElectronicDocuments' => [
                                  [
                                      'issuingType' => 0,
                                      'securityCode' => $securityCode,
                                      'aditionalInformation' => '',
                                      'aditionalTreasuryInformation' => '',
                                      'documentTypeCode' => 5,
                                      'number' => $numbero,
                                      'series' => 'AA',
                                      'issuedDate' => $formattedDate,
                                      'transactionTypeCode' => 1,
                                      'taxTypeCode' => 1,
                                      'currencyTypeCode' => $moneda_factura,
                                      'exchangeRate' => $cotizacion,
                                      'client' => [
                                          'nature' => 1,
                                          'operationType' => 1,
                                          'countryCode' => $cod_pais,
                                          'countryName' => $pais,
                                          'contributorType' => $personeria,
                                          'ruc' => $ruc,
                                          'businessName' => $nombre_cliente,
                                          'fantasyName' => $nombre_cliente,
                                          'address' => $direccion,
                                      ],
                                      'operationCondition' => 0,
                                      'subTotal' => $sub_total,
                                      'total' => $totalFactura,
                                      'tax' => $iva,
                                      'electronicDocumentItems' => [],
                                      'branch' => [
                                          'branchDocumentTypes' => [
                                              [
                                                  'stablishment' => $timbrado_establecimiento,
                                                  'expeditionPoint' => $timbrado_expedicion,
                                                  'stampNumber' => $timbrado_numero,
                                                  'stampDate' => $timbrado_fecha,
                                              ],
                                          ],
                                          'address' => $empresa_direccion,
                                          'houseNumber' => 1078,
                                          'addressComplement1' => '',
                                          'addressComplement2' => '',
                                          'cityCode' => 1,
                                          'phone' => $empresa_telefono,
                                          'email' => $empresa_correo,
                                          'name' => 'Matriz',
                                      ],
                                      'PresenceIndicatorCode' => 1,
                                      'payments' => [
                                          [
                                              'paymentMethodCode' => 1,
                                              'ammount' => $totalFactura,
                                          ],
                                      ],
                                      'tenant' => [
                                          'stampNumber' => $timbrado_numero,
                                          'stampDate' => $timbrado_fecha,
                                          'ruc' => $empresa_ruc,
                                          'taxpayerType' => 2,
                                          'name' => $empresa_nombre,
                                          'activities' => [
                                              [
                                                  'name' => 'ACTIVIDADES DE LOS OPERADORES TURÍSTICOS',
                                                  'identifier' => '79120',
                                              ],
                                          ],
                                      ],
                                      'associatedDocuments' => [
                                          [
                                              'associatedDocumentType' => $associatedDocumentType, //Obtener API GeneralCodes/Get -> AssociatedDocumentTypes.Identifier
                                              'cdc' => $notaCredito[0]->factura[0]->cdc, //'cdc' => $cdc,
                                              'stampNumber' =>$stampNumber ,//'stampNumber' => $stamp_number ,
                                              /*'stablishment' => '001',*/'stablishment' => $base_factura[0],
                                              /*'expeditionPoint' => '001', */'expeditionPoint' => $base_factura[1], 
                                              /*'documentNumber' => '0000091',*/ 'documentNumber' => $base_factura[2],
                                              'printedDocumentType' => 0,//Obtener API GeneralCodes/Get -> PrintedDocumentTypes.Identifier
                                              'issuingDate' => $formattedDateF,
                                              /* 'withholdingVoucherNumber' => 'string',
                                              'taxCreditResolutionNumber' => 'string',
                                              'recordType' => 1,
                                              'recordNumber' => 0,
                                              'recordControlNumber' => 'string', */
                                          ],
                                      ],
                                  ],
                              ],
                          ];
        
        $items = [];
        foreach ($notaCredito[0]->notaCreditoDetalle as $notaCreditoDetalle) {
            /*echo '<pre>';
            print_r($notaCreditoDetalle);*/
            if($notaCreditoDetalle->activo == true){
                  if($notaCreditoDetalle->total_exenta != 0 && $notaCreditoDetalle->total_gravada == 0){
                      $taxImpact = 2;
                      $porcentaje = 0;
                      $taxRate = 0;
                  }

                  if($notaCreditoDetalle->total_exenta == 0 && $notaCreditoDetalle->total_gravada != 0){
                    $taxImpact = 0;
                    $porcentaje = 100;
                    $taxRate = 10;
                  }

                  if($notaCreditoDetalle->total_exenta != 0 && $notaCreditoDetalle->total_gravada != 0){
                    $taxImpact = 3;
                    $porcentaje = round((((float)$notaCreditoDetalle->total_gravada * 100)/(float)$notaCreditoDetalle->precio_venta), 2);
                    $taxRate = 10;
                  }

                  if($notaCreditoDetalle->total_exenta == 0 && $notaCreditoDetalle->total_gravada == 0){
                    $taxImpact = 2;
                    $porcentaje = 0;
                    $taxRate = 0;
                  }

                  $item = [
                          'internalCode' => $notaCreditoDetalle->cod_confirmacion,
                          'description' => $notaCreditoDetalle->descripcion,
                          'measurementUnitCode' => 4,
                          'quantity' => $notaCreditoDetalle->cantidad,
                          'informationOfInterest' => 'info opcional',
                          'unitPriceWithTax' => $notaCreditoDetalle->precio_venta,
                          'itemExchangeRate' => 1,
                          'itemUnitPriceDiscountWithTax' => 0,
                          'itemDiscountPercentage' => 0,
                          'itemUnitPriceGlobalDiscountWithTax' => 0,
                          'itemUnitPriceAdvanceWithTax' => 0,
                          'itemUnitPriceGlobalAdvanceWithTax' => 0,
                          'taxImpact' => $taxImpact,
                          'taxedProportion' => $porcentaje,
                          'taxRate' => $taxRate
                      ];
              
                  $items[] = $item;
            }
        }
        
        $json['ElectronicDocuments'][0]['electronicDocumentItems'] = $items;

        if($cod_pais != 107 ){
            $facturaDetalle = Persona::with('pais')
                                      ->where('id',$notaCredito[0]->cliente_id)
                                      ->first(); 
            if(isset($facturaDetalle->pais->id_set)){
                $codigo_pais = $facturaDetalle->pais->id_set;
            }else{
                $codigo_pais = 249;
            } 
            $json['ElectronicDocuments'][0]['client'] = [
              'nature' => 2,
              'ruc' => $facturaDetalle->documento_identidad,
              'businessName' => $facturaDetalle->nombre,
              'fantasyName' => $facturaDetalle->nombre,
              'contributorType' => 1,
              'identityDocumentTypeCode' => '3',
              'operationType' => '4',
              'countryCode' => $codigo_pais,
              'email' => $facturaDetalle->email
            ];
        }elseif($notaCredito[0]->cliente->dv == ""){
            $facturaDetalle = Persona::with('pais')
                                      ->where('id',$notaCredito[0]->cliente_id)
                                      ->first(); 
            if(isset($facturaDetalle->pais->id_set)){
              $codigo_pais = $facturaDetalle->pais->id_set;
            }else{
              $codigo_pais = 249;
            }  
            $json['ElectronicDocuments'][0]['client'] = [
                                                        'nature' => 2,
                                                        'operationType' => 2,
                                                        'identityDocumentNumber' => $facturaDetalle->documento_identidad,
                                                        'businessName' => $nombre_cliente,
                                                        'fantasyName' => $nombre_cliente,
                                                        'contributorType' => 1,
                                                        'identityDocumentTypeCode' => '1',
                                                        'countryCode' => $codigo_pais,
                                                        'email' => $correo_cliente
                                                        ];
        }

      /*  echo '<pre>';
        print_r(json_encode($json));  
       die; */
        $client = new Client();
        // $response = $client->post('http://3.140.122.68:8080/Token', [
        $response = $client->post('http://3.140.122.68/Token', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                              'username' => $username,
                              'password' => $password,
                              'grant_type' => 'password',
              ],
        ]);
        
        $body = $response->getBody();
        $data = json_decode($body, true);
        
        // Accede a los datos de la respuesta
        $accessToken = $data['access_token'];

       /* echo '<pre>';
        print_r(json_encode($accessToken));*/
         // $response = $client->post('http://3.140.122.68:8080/api/electronicDocument/Bulk', [
        $response = $client->post('http://3.140.122.68/api/electronicDocument/Bulk', [
            'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer '.$accessToken,
                        ],
            'json' => $json
        ]);
        
        $responseData = json_decode($response->getBody(), true);

        $cdc = $responseData['Items'][0]['CDC'];
        $url = $responseData['Items'][0]['DCarQR'];
        $id_impresion = $responseData['Items'][0]['Id'];
        $id_api = $responseData['Id'];

        $documento = new DocumentosEcos;
        $documento->id_documento = $id;
        $documento->id_api = $id_api;
        $documento->id_impresion = $id_impresion;
        $documento->tipo_documento = 5;
        $documento->cdc = $cdc;
        $documento->dcarqr = $url;
        $documento->request = json_encode($json);
        $documento->response = json_encode($responseData);
        $documento->fecha_envio = date('Y-m-d H:m:s'); 
        $documento->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa; 
        $documento->save();
        $id_eco = $documento->id;

        $update = DB::table('nota_credito')
                    ->where('id',$id)
                    ->update(['cdc'=>$cdc]);
        
       // $getNotificacion= DB::select("SELECT public.insert_notificacion(".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.",'".$correo_cliente."', null, null,27,".$id_eco.",".$idEmpresa.")");

        return 'OK';  
    }


  public function documentosElectronicos(Request $req){

    return view('pages.mc.documentos_ecos.index');
  }


  public function consultaDocumentosEca(Request $req){

		$documento_eca = DB::table('vw_documento');
    if($req->input('documento') !=""){
      $documento_eca = $documento_eca->where('tipo_documento',$req->input('documento'));
    }
    $documento_eca = $documento_eca->where('id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
    if($req->input('periodo')){
      $fechaPeriodo = explode(' - ', $req->input('periodo'));
      $desde     = $this->formatoFechaEntrada(trim($fechaPeriodo[0])).' 00:00:00';
      $hasta     = $this->formatoFechaEntrada(trim($fechaPeriodo[1])).' 23:59:59';
      $documento_eca = $documento_eca->whereBetween('fecha_envio',array($desde,$hasta));
    }
		$documento_eca = $documento_eca->get();
    foreach($documento_eca as $key=>$documento){
      //if(isset($documento->estado)){
         // if($documento->estado == ""){
            $resultado = $this->resultadoBulk($documento->id_api);
         // }else{
           // $resultado = $documento->estado;
        //  }
    /*  }else{
        $resultado = '';
      }*/
        $documento_eca[$key]->estado_documento= $resultado;
    }

     return response()->json($documento_eca);

  }

  
  public function resultadoBulk($id_api){
    $respuestaJson = '';
    if($id_api != ""){
        if($this->getIdEmpresa() == 1){
            $username = 'admin@dtparaguayo.com.py';
            $password = 'TGru4HuxiE';
           // $password = 'ILZXg16OE';
        }else{
            $username = 'admin@boardingpass.com.py';
            //  $password = 'tiRRmrHQce';
            $password = 'RA3mAlRUEF';
        }

        $client = new Client();
       // $response = $client->post('http://3.140.122.68:8080/Token', [
        $response = $client->post('http://3.140.122.68/Token', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                'username' => $username,
                'password' => $password,
                'grant_type' => 'password',
            ],
        ]);
        
        $body = $response->getBody();
        $data = json_decode($body, true);
        
        // Accede a los datos de la respuesta
        $accessToken = $data['access_token'];


       // $response = $client->get('http://3.140.122.68:8080/api/electronicDocument/getBulk/'.$id_api, [
        try{
            $response = $client->get('http://3.140.122.68/api/electronicDocument/getBulk/'.$id_api, [
              'headers' => [
                          'Content-Type' => 'application/json',
                          'Authorization' => 'Bearer '.$accessToken,
                          ],
            ]);

          $responseEstado = json_decode($response->getBody(), true);
          if(isset($responseEstado['SifenBulkResult']['rResEnviConsLoteDe']['gResProcLoteField'][0]['dEstResField'])){
              $update = DB::table('documentos_electronicos')
                          ->where('id_api',$id_api)
                          ->update(['estado'=>$responseEstado['SifenBulkResult']['rResEnviConsLoteDe']['gResProcLoteField'][0]['dEstResField']."  /  ".$responseEstado['SifenBulkResult']['rResEnviConsLoteDe']['gResProcLoteField'][0]['gResProcField'][0]['dMsgResField']]);
              return $responseEstado['SifenBulkResult']['rResEnviConsLoteDe']['gResProcLoteField'][0]['dEstResField']."  /  ".$responseEstado['SifenBulkResult']['rResEnviConsLoteDe']['gResProcLoteField'][0]['gResProcField'][0]['dMsgResField'];  
          }else{
              return 'Esperando Respuesta';
          }
      } catch(\Exception $e){

        return 'Test / Esperando Respuesta';
      }
    }
    return $respuestaJson;  
  }


  public function generarKudePhp(Request $req)
  {
      if ($this->getIdEmpresa() == 1) {
          $username = 'admin@dtparaguayo.com.py';
          $password = 'TGru4HuxiE';
         // $password = 'ILZXg16OE';
      } else {
          $username = 'admin@boardingpass.com.py';
            //  $password = 'tiRRmrHQce';
            $password = 'RA3mAlRUEF';     
     }

      $client = new Client();
      //$response = $client->post('http://3.140.122.68:8080/Token', [
      $response = $client->post('http://3.140.122.68/Token', [
          'headers' => [
              'Content-Type' => 'application/x-www-form-urlencoded',
          ],
          'form_params' => [
              'username' => $username,
              'password' => $password,
              'grant_type' => 'password',
          ],
      ]);

      $body = $response->getBody();
      $data = json_decode($body, true);

      // Accede a los datos de la respuesta
      $accessToken = $data['access_token'];

   //   $response = $client->get('http://3.140.122.68:8080/api/electronicdocument/getkude/'.$req->input('cdc'), [
      $response = $client->get('http://3.140.122.68/api/electronicdocument/getkude/'.$req->input('cdc'), [
          'headers' => [
              'Authorization' => 'Bearer ' . $accessToken,
          ],
      ]);

      if ($response->getStatusCode() === 200) {
          $contentDisposition = $response->getHeaderLine('Content-Disposition');
          $filename = '';
          if (preg_match('/filename="(.+)"/', $contentDisposition, $matches)) {
              $filename = $matches[1];
          }

          $fileContents = $response->getBody()->getContents();
          // Guardar el archivo en el disco "facturaElectronica" de Laravel
          Storage::disk('facturaElectronica')->put($filename, $fileContents);

          // Generar la URL de descarga del archivo almacenado en Laravel
          $downloadUrl = asset('factura_eca/' . $filename);

          return response()->json([
                                  'success' => true,
                                  'url' => $downloadUrl,
                                  'filename' => $filename,
                              ]);
      } else {
          // Manejar el código de estado diferente a 200
          // ...
      } 
  }

  function anularDE(Request $request){

        $fechaEmision = date('Y-m-d H:m:s');    

        $format = 'Y-m-d H:i:s';
    
        $date = \DateTime::createFromFormat($format, $fechaEmision);
        if ($date) {
            $formattedDate = $date->format('Y-m-d\TH:i:s');
        } else {
            echo 'Fecha inválida';
        }
    
        $json = [
          'eventDetails' => [
              [
                  'typeCode' => 1,
                  'reason' => $request->input('motivo'),
                  'documentId' => $request->input('cdc'),
                  'signDate' => $formattedDate
              ]
          ]
        ];
     
      if ($this->getIdEmpresa() == 1) {
          $username = 'admin@dtparaguayo.com.py';
          $password = 'TGru4HuxiE';
          // $password = 'ILZXg16OE';
      } else {
          $username = 'admin@boardingpass.com.py';
          $password = 'RA3mAlRUEF';     
          //  $password = 'tiRRmrHQce';
      }

      $client = new Client();
     // $response = $client->post('http://3.140.122.68:8080/Token', [
      $response = $client->post('http://3.140.122.68/Token', [
          'headers' => [
              'Content-Type' => 'application/x-www-form-urlencoded',
          ],
          'form_params' => [
              'username' => $username,
              'password' => $password,
              'grant_type' => 'password',
          ],
      ]);
      
      $body = $response->getBody();
      $data = json_decode($body, true);
      
      // Accede a los datos de la respuesta
      $accessToken = $data['access_token'];
      /*echo '<pre>';
      print_r(json_encode($json));*/
     // $response = $client->post('http://3.140.122.68:8080/api/electronicDocument/event', [
      $response = $client->post('http://3.140.122.68/api/electronicDocument/event', [
                                    'headers' => [
                                                'Content-Type' => 'application/json',
                                                'Authorization' => 'Bearer '.$accessToken,
                                                ],
                                    'json' => $json
                              ]);
        
      $responseData = json_decode($response->getBody(), true);

      $mensaje = new \StdClass; 

      DB::table('documentos_electronicos')
        ->where('cdc',$request->input('cdc'))
        ->update([
                'fecha_anulacion'=> date('Y-m-d H:m:s'),
                'request_anulacion'=>json_encode($json),
                'response_anulacion'=>json_encode($responseData),
                'id_usuario_anulacion'=>$this->getIdUsuario()
                ]);
       
      $error = $responseData['Error'];

      if($error == ""){
          $idUsuario = $this->getIdUsuario(); 
          $err = 'OK';
        
          if($request->input('idFactura') !== null){
                $idFactura = $request->input('idFactura');
                $anular = DB::select("SELECT * from anular_factura(".$idFactura.", ".$idUsuario.") limit 1");
                if($anular[0]->anular_factura !== 'OK'){
                  $err = 'false';
                }

                DB::table('proformas')
                                ->where('factura_id',$idFactura)
                                ->update([
                                      'estado_id'=>1
                                      ]);
          }

          if($request->input('idNota') !== null){
                $idNota = $request->input('idNota');
                $anular = DB::select('SELECT public."anular_nota_credito"('.$idNota.','.$idUsuario.')');
          }

          $mensaje->status = 'OK';
          $mensaje->mensaje = 'El Documento Electronico fue anulado.';
      }else{
          $mensaje->status = 'ERROR';
          $mensaje->mensaje = $error;
      }
      
      return json_encode($mensaje);	
  }


  public function generarJsonFacturaURL(Request $req, $id){

    if($this->getIdEmpresa() == 1){
        $username = 'admin@dtparaguayo.com.py';
        $password = 'TGru4HuxiE';
      //  $password = 'ILZXg16OE';
    }else{
        $username = 'admin@boardingpass.com.py';
        //  $password = 'tiRRmrHQce';
        $password = 'RA3mAlRUEF';   
    }

    $facturas = DB::select("SELECT * FROM public.vw_facturas WHERE id = ".$id); 

    $detalleFacturas = DB::select("SELECT * FROM public.vw_detalle_facturas WHERE id_factura = ".$id);
   /* echo '<pre>';
    print_r(json_encode($detalleFacturas));*/

    $securityCode = $this->securityCode($id);

    $fechaEmision = $facturas[0]->fecha_hora_facturacion;    

    $format = 'Y-m-d H:i:s';

    $date = \DateTime::createFromFormat($format, $fechaEmision);
    if ($date) {
        $formattedDate = $date->format('Y-m-d\TH:i:s');
    } else {
        echo 'Fecha inválida';
    }

    $moneda_factura = $facturas[0]->moneda_venta;

    $nombre_cliente = $facturas[0]->cliente_nombre;
    if($facturas[0]->cliente_apellido != ""){
        $nombre_cliente = $nombre_cliente." ".$facturas[0]->cliente_apellido;
    }
    $tipo_facturacion = 0;
    if($facturas[0]->id_tipo_facturacion == 2){
      $tipo_facturacion = 1;
    }

    $direccion = $facturas[0]->direccion_cliente;
    if($facturas[0]->nombre_pais == ""){
      $pais = 'Paraguay'; 
      $cod_pais = 107; 
    }else{
      $pais = $facturas[0]->nombre_pais; 
      $cod_pais = $facturas[0]->cod_pais; 
    }

    $idEmpresa = $facturas[0]->id_empresa;
    if($facturas[0]->dv_cliente == ""){
       $ruc = $facturas[0]->documento_cliente;
    }else{
       $ruc = $facturas[0]->documento_cliente."-".$facturas[0]->dv_cliente;

    }
    $personeria = $facturas[0]->id_personeria;
    $correo_cliente = $facturas[0]->cliente_email;
    if($personeria == 0 || $personeria == 0){
      $personeria = 1;
    }
    $base_factura = explode('-',$facturas[0]->nro_factura);
    $numbero = (int)$base_factura[2];
    $total_Factura = DB::select('SELECT * FROM get_monto_factura('.$facturas[0]->id.')');
    $totalFactura = $total_Factura[0]->get_monto_factura;
    $empresa_nombre = $facturas[0]->empresa_nombre; 
    $empresa_direccion = $facturas[0]->empresa_direccion; 
    $empresa_telefono = str_replace('+ 595 ','0',$facturas[0]->empresa_telefono);
    $empresa_correo = $facturas[0]->empresa_correo;
 //   $timbrado_fecha = '2023-10-24';
    $timbrado_establecimiento = $facturas[0]->timbrado_establecimiento;
    $timbrado_expedicion = $facturas[0]->timbrado_expedicion;
    $timbrado_fecha = $facturas[0]->timbrado_autorizacion;
    $iva = $facturas[0]->total_iva;
    $sub_total = (float)$totalFactura - (float)$iva;
    $sub_total =number_format($sub_total,2,".","");
    $empresa_ruc = $facturas[0]->empresa_ruc;
   // $base = explode('-', $facturas[0]->empresa_ruc);
    $timbrado_numero = $facturas[0]->timbrado_numero;
   // $timbrado_numero = $base[0];
    if($moneda_factura == 'PYG'){
      $cotizacion = 0;
    }else{
      $cotizacion = $facturas[0]->cotizacion_factura;
    }
    if($this->getIdEmpresa() == 1){
      $logo = 3;
   }else{
      $logo =  $facturas[0]->logo;
    }


    $json = [
            'ElectronicDocuments' => [
                [
                    'issuingType' => 0,
                    'securityCode' => $securityCode,
                    'aditionalInformation' => '',
                    'aditionalTreasuryInformation' => '',
                    'documentTypeCode' => 1,
                    'number' => $numbero,
                    'series' => 'AA',
                    'issuedDate' => $formattedDate,
                    'tenantLogoId' => $logo ,
                    'transactionTypeCode' => 2,
                    'taxTypeCode' => 1,
                    'currencyTypeCode' => $moneda_factura,
                    'exchangeRate' => $cotizacion,
                    'client' => [
                        'nature' => 1,
                        'operationType' => 1,
                        'countryCode' => $cod_pais,
                        'countryName' => $pais,
                        'contributorType' => $personeria, 
                        'ruc' => $ruc,
                        'businessName' => $nombre_cliente,
                        'fantasyName' => $nombre_cliente,
                        'address' => $direccion,
                        'email' => $correo_cliente,
                    ],
                    'operationCondition' => $tipo_facturacion,
                    'subTotal' => $sub_total,
                    'total' => $totalFactura,
                    'tax' => $iva,
                    'electronicDocumentItems' => [],
                    'branch' => [
                        'branchDocumentTypes' => [
                            [
                                'stablishment' => $timbrado_establecimiento,
                                'expeditionPoint' => $timbrado_expedicion,
                                'stampNumber' => $timbrado_numero,
                                'stampDate' => $timbrado_fecha,
                            ],
                        ],
                        'address' => $empresa_direccion,
                        'houseNumber' => 1078,
                        'addressComplement1' => '',
                        'addressComplement2' => '',
                        'cityCode' => 1,
                        'phone' => $empresa_telefono,
                        'email' => $empresa_correo,
                       // 'name' => 'Matriz',
                    ],
                    'PresenceIndicatorCode' => 1,
                    'payments' => [
                        [
                            'paymentMethodCode' => 1,
                            'ammount' => $totalFactura,
                        ],
                    ],
                    'tenant' => [
                        'stampNumber' => $timbrado_numero,
                        'stampDate' => $timbrado_fecha,
                        'ruc' => $empresa_ruc,
                        'taxpayerType' => 2,
                        'name' => $empresa_nombre,
                        'activities' => [
                            [
                                'name' => 'ACTIVIDADES DE LOS OPERADORES TURÍSTICOS',
                                'identifier' => '79120',
                            ],
                        ],
                    ],
                ],
            ],
        ];
        
        $items = [];
        foreach ($detalleFacturas as $detalleFactura) {
            if($detalleFactura->exento != 0 && $detalleFactura->gravadas_10 == 0){
                $taxImpact = 2;
                $porcentaje = 0;
                $taxRate = 0;
            }

            if($detalleFactura->exento == 0 && $detalleFactura->gravadas_10 != 0){
              $taxImpact = 0;
              $porcentaje = 100;
              $taxRate = 10;
            }

            if($detalleFactura->exento != 0 && $detalleFactura->gravadas_10 != 0){
              $taxImpact = 3;
              $porcentaje = round((((float)$detalleFactura->gravadas_10 * 100)/(float)$detalleFactura->precio_venta), 2);
              if($porcentaje > 99){
                $porcentaje = 99;
              }
              $taxRate = 10;
            }

            if($detalleFactura->exento == 0 && $detalleFactura->gravadas_10 == 0){
              $taxImpact = 2;
              $porcentaje = 0;
              $taxRate = 0;
          }

            $item = [
                'internalCode' => $detalleFactura->cod_confirmacion,
                'description' => $detalleFactura->descripcion,
                'measurementUnitCode' => 4,
                'quantity' => $detalleFactura->cantidad,
                'informationOfInterest' => 'info opcional',
                'unitPriceWithTax' => $detalleFactura->precio_venta,
                'itemExchangeRate' => 1,
                'itemUnitPriceDiscountWithTax' => 0,
                'itemDiscountPercentage' => 0,
                'itemUnitPriceGlobalDiscountWithTax' => 0,
                'itemUnitPriceAdvanceWithTax' => 0,
                'itemUnitPriceGlobalAdvanceWithTax' => 0,
                'taxImpact' => $taxImpact,
                'taxedProportion' => $porcentaje,
                'taxRate' => $taxRate,
            ];
        
            $items[] = $item;
        }
        
        $json['ElectronicDocuments'][0]['electronicDocumentItems'] = $items;

        if($facturas[0]->id_tipo_facturacion == 2){
            $fecha1 = new \DateTime(date('Y-m-d', strtotime($fechaEmision)));
            $fecha2 = new \DateTime($facturas[0]->vencimiento);
  
            $diferencia = $fecha1->diff($fecha2);
            $dias = $diferencia->days;

            $credit = [
                      'creditOperationCondition' => 0,
                      'creditDeadline' => $dias.' dias',
                      'initialDeliveryAmmount' => 0,
                    ];

            $json['ElectronicDocuments'][0]['credit'] = $credit;
        }

        if($cod_pais != 107 ){
            $facturaDetalle = Persona::with('pais')
                                      ->where('id',$facturas[0]->cliente_id)
                                      ->first(); 
           $nombre_cliente = $facturaDetalle->nombre;
           if($facturaDetalle->apellido!= ""){
                   $nombre_cliente = $facturaDetalle->nombre." ".$facturaDetalle->apellido;
            }
                                      
            if(isset($facturaDetalle->pais->id_set)){
                $codigo_pais = $facturaDetalle->pais->id_set;
            }else{
                $codigo_pais = 249;
            } 
            $json['ElectronicDocuments'][0]['client'] = [
              'nature' => 2,
              'ruc' => $facturaDetalle->documento_identidad,
              'businessName' => $nombre_cliente,
              'fantasyName' => $nombre_cliente,
              'contributorType' => 1,
              'identityDocumentTypeCode' => '3',
              'operationType' => '4',
              'countryCode' => $codigo_pais,
              'email' => $facturaDetalle->email
            ];
        }elseif($facturas[0]->dv_cliente == ""){
            $facturaDetalle = Persona::with('pais')
                                  ->where('id',$facturas[0]->cliente_id)
                                  ->first(); 

            $nombre_cliente = $facturaDetalle->nombre;
            if($facturaDetalle->apellido!= ""){
                   $nombre_cliente = $facturaDetalle->nombre." ".$facturaDetalle->apellido;
            }
                              
            if(isset($facturaDetalle->pais->id_set)){
              $codigo_pais = $facturaDetalle->pais->id_set;
            }else{
              $codigo_pais = 249;
            }  
            $json['ElectronicDocuments'][0]['client'] = [
                                                        'nature' => 2,
                                                        'operationType' => 2,
                                                        'identityDocumentNumber' => $facturaDetalle->documento_identidad,
                                                        'businessName' => $nombre_cliente,
                                                        'fantasyName' => $nombre_cliente,
                                                        'contributorType' => 1,
                                                        'identityDocumentTypeCode' => '1',
                                                        'countryCode' => $codigo_pais,
                                                        'email' => $correo_cliente
                                                        ];
        }

        $client = new Client();
       // $response = $client->post('http://3.140.122.68:8080/Token', [
        $response = $client->post('http://3.140.122.68/Token', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                'username' => $username,
                'password' => $password,
                'grant_type' => 'password',
            ],
        ]);
        
        $body = $response->getBody();
        $data = json_decode($body, true);
        
        // Accede a los datos de la respuesta
        $accessToken = $data['access_token'];

        try{ 
                  //$response = $client->post('http://3.140.122.68:8080/api/electronicDocument/Bulk', [
                $response = $client->post('http://3.140.122.68/api/electronicDocument/Bulk', [

                      'headers' => [
                                  'Content-Type' => 'application/json',
                                  'Authorization' => 'Bearer '.$accessToken,
                                  ],
                      'json' => $json
                ]);
                $responseData = json_decode($response->getBody(), true);

                $cdc = $responseData['Items'][0]['CDC'];
                $url = $responseData['Items'][0]['DCarQR'];
                $id_impresion = $responseData['Items'][0]['Id'];
                $id_api = $responseData['Id'];

                $documento = new DocumentosEcos;
                $documento->id_documento = $id;
                $documento->id_api = $id_api;
                $documento->id_impresion = $id_impresion;
                $documento->tipo_documento = 1;
                $documento->cdc = $cdc;
                $documento->dcarqr = $url;
                $documento->request = json_encode($json);
                $documento->response = json_encode($responseData);
                $documento->fecha_envio = date('Y-m-d H:m:s'); 
                $documento->id_empresa = $idEmpresa; 
                $documento->save();
                $id_eco = $documento->id;
                 
                $update = DB::table('facturas')
                              ->where('id',$id)
                              ->update(['cdc'=>$cdc]);
                  
                $getNotificacion= DB::select("SELECT public.insert_notificacion(".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.",'".$correo_cliente."', null, null,27,".$id_eco.",".$idEmpresa.")");
                flash('Se ha enviado el DE exitosamente')->success();
                return redirect()->back();

          } catch(\Exception $e){

                $mensajeError = explode('":"',$e->getMessage());
                $mensajeErr = str_replace('"}','',$mensajeError[1]);
                flash('<b>'.$mensajeErr.'</b>')->error();
                return redirect()->back();

          }
    }


    public function generarJsonNCURL(Request $req, $id){

      if($this->getIdEmpresa() == 1){
          $username = 'admin@dtparaguayo.com.py';
         // $password = 'ILZXg16OE';
          $password = 'TGru4HuxiE';
      }else{
          $username = 'admin@boardingpass.com.py';
          //$password = 'tiRRmrHQce';
          $password = 'RA3mAlRUEF';   
      }
  
      $notaCredito = NotaCredito::with('cliente',
                                      'proforma',
                                      'currency',
                                      'timbrado',
                                      'factura',
                                      'factura.vendedorEmpresa',
                                      'notaCreditoDetalle',
                                      'libroVentaSaldo0')
                                 ->where('id',$id)->get(); 
  
      $securityCode = $this->securityCode($id);
  
      $fechaEmision = $notaCredito[0]->fecha_hora_nota_credito;    
  
      $format = 'Y-m-d H:i:s';
  
      $date = \DateTime::createFromFormat($format, $fechaEmision);
      if ($date) {
          $formattedDate = $date->format('Y-m-d\TH:i:s');
      } else {
          echo 'Fecha inválida';
      }
  
      $moneda_factura = $notaCredito[0]->currency->currency_code;
      $nombre_cliente = $notaCredito[0]->cliente->nombre;
      $correo_cliente =$notaCredito[0]->cliente->email;
      if($notaCredito[0]->cliente->apellido != ""){
          $nombre_cliente = $nombre_cliente." ".$notaCredito[0]->cliente->apellido;
      }
      $id_empresa = $notaCredito[0]->id_empresa;
      $direccion = $notaCredito[0]->cliente->direccion;
      $code_pais = $notaCredito[0]->cliente->id_pais;
      if($code_pais == ""){
          $pais = 'Paraguay'; 
          $cod_pais = 107; 
      }else{
          $paises = Pais::where('cod_pais', $code_pais)->first();
          $pais = $paises->name_es; 
          $cod_pais = $paises->id_set; 
      }
  
      $ruc = $notaCredito[0]->cliente->documento_identidad."-".$notaCredito[0]->cliente->dv;
      $personeria = $notaCredito[0]->cliente->id_personeria;
      
      if($personeria == ''){
          $personeria = 1;
      }
      $base_factura = explode('-',$notaCredito[0]->nro_nota_credito);
      $numbero = (int)$base_factura[2];
      $totalFactura =$notaCredito[0]->total_neto_nc;
      $empresa = Empresa::where('id',$id_empresa)->first();   
      $empresa_nombre = $empresa->denominacion; 
      $empresa_direccion = $empresa->direccion; 
      $empresa_telefono = str_replace('+ 595 ','0',$empresa->telefono);
      $empresa_correo = $empresa->correo_set;
      $timbrado_establecimiento = $notaCredito[0]->timbrado->establecimiento;
      $timbrado_expedicion =$notaCredito[0]->timbrado->expedicion;
      $timbrado_fecha = $notaCredito[0]->timbrado->fecha_autorizacion;
      $iva = $notaCredito[0]->total_iva;
      $sub_total = (float)$totalFactura - (float)$iva;
      $sub_total =number_format($sub_total,2,".","");
      $empresa_ruc = $empresa->ruc_set;
  
      $base = explode('-', $empresa->ruc_set);
  
      $timbrado_numero =$notaCredito[0]->timbrado->numero;
  
      if($moneda_factura == 'PYG'){
        $cotizacion = 0;
      }else{
        $cotizacion = $notaCredito[0]->cotizacion;
      }
      $base_factura = explode('-', $notaCredito[0]->factura[0]->nro_factura);
      $cdc = $notaCredito[0]->factura[0]->cdc;
      $stamp_number = $notaCredito[0]->factura[0]->stamp_number;
      $fechaEmisionF = $notaCredito[0]->factura[0]->fecha_hora_facturacion;    
  
      $idTimbradoFactura = $notaCredito[0]->factura[0]->id_timbrado;
  
      $timbradoFactura = Timbrado::findOrFail($idTimbradoFactura);	
  
      $stampNumber = $timbradoFactura->numero;
  
      $format = 'Y-m-d H:i:s';
  
      $date = \DateTime::createFromFormat($format, $fechaEmisionF);
      if ($date) {
          $formattedDateF = $date->format('Y-m-d\TH:i:s');
      } else {
          echo 'Fecha inválida';
      }
  
      if($notaCredito[0]->factura[0]->cdc == '' ){
          $associatedDocumentType = 1;
      }else{
          $associatedDocumentType = 0;
      }
  
      $json = [
                'ElectronicDocuments' => [
                                    [
                                        'issuingType' => 0,
                                        'securityCode' => $securityCode,
                                        'aditionalInformation' => '',
                                        'aditionalTreasuryInformation' => '',
                                        'documentTypeCode' => 5,
                                        'number' => $numbero,
                                        'series' => 'AA',
                                        'issuedDate' => $formattedDate,
                                        'transactionTypeCode' => 1,
                                        'taxTypeCode' => 1,
                                        'currencyTypeCode' => $moneda_factura,
                                        'exchangeRate' => $cotizacion,
                                        'client' => [
                                            'nature' => 1,
                                            'operationType' => 1,
                                            'countryCode' => $cod_pais,
                                            'countryName' => $pais,
                                            'contributorType' => $personeria,
                                            'ruc' => $ruc,
                                            'businessName' => $nombre_cliente,
                                            'fantasyName' => $nombre_cliente,
                                            'address' => $direccion,
                                        ],
                                        'operationCondition' => 0,
                                        'subTotal' => $sub_total,
                                        'total' => $totalFactura,
                                        'tax' => $iva,
                                        'electronicDocumentItems' => [],
                                        'branch' => [
                                            'branchDocumentTypes' => [
                                                [
                                                    'stablishment' => $timbrado_establecimiento,
                                                    'expeditionPoint' => $timbrado_expedicion,
                                                    'stampNumber' => $timbrado_numero,
                                                    'stampDate' => $timbrado_fecha,
                                                ],
                                            ],
                                            'address' => $empresa_direccion,
                                            'houseNumber' => 1078,
                                            'addressComplement1' => '',
                                            'addressComplement2' => '',
                                            'cityCode' => 1,
                                            'phone' => $empresa_telefono,
                                            'email' => $empresa_correo,
                                            'name' => 'Matriz',
                                        ],
                                        'PresenceIndicatorCode' => 1,
                                        'payments' => [
                                            [
                                                'paymentMethodCode' => 1,
                                                'ammount' => $totalFactura,
                                            ],
                                        ],
                                        'tenant' => [
                                            'stampNumber' => $timbrado_numero,
                                            'stampDate' => $timbrado_fecha,
                                            'ruc' => $empresa_ruc,
                                            'taxpayerType' => 2,
                                            'name' => $empresa_nombre,
                                            'activities' => [
                                                [
                                                    'name' => 'ACTIVIDADES DE LOS OPERADORES TURÍSTICOS',
                                                    'identifier' => '79120',
                                                ],
                                            ],
                                        ],
                                        'associatedDocuments' => [
                                            [
                                                'associatedDocumentType' => $associatedDocumentType, //Obtener API GeneralCodes/Get -> AssociatedDocumentTypes.Identifier
                                                'cdc' => $notaCredito[0]->factura[0]->cdc, //'cdc' => $cdc,
                                                'stampNumber' =>$stampNumber ,//'stampNumber' => $stamp_number ,
                                                /*'stablishment' => '001',*/'stablishment' => $base_factura[0],
                                                /*'expeditionPoint' => '001', */'expeditionPoint' => $base_factura[1], 
                                                /*'documentNumber' => '0000091',*/ 'documentNumber' => $base_factura[2],
                                                'printedDocumentType' => 0,//Obtener API GeneralCodes/Get -> PrintedDocumentTypes.Identifier
                                                'issuingDate' => $formattedDateF,
                                                /* 'withholdingVoucherNumber' => 'string',
                                                'taxCreditResolutionNumber' => 'string',
                                                'recordType' => 1,
                                                'recordNumber' => 0,
                                                'recordControlNumber' => 'string', */
                                            ],
                                        ],
                                    ],
                                ],
                            ];
          
          $items = [];
          foreach ($notaCredito[0]->notaCreditoDetalle as $notaCreditoDetalle) {

              if($notaCreditoDetalle->total_exenta != 0 && $notaCreditoDetalle->total_gravada == 0){
                  $taxImpact = 2;
                  $porcentaje = 0;
                  $taxRate = 0;
              }
  
              if($notaCreditoDetalle->total_exenta == 0 && $notaCreditoDetalle->total_gravada != 0){
                $taxImpact = 0;
                $porcentaje = 100;
                $taxRate = 10;
              }
  
              if($notaCreditoDetalle->total_exenta != 0 && $notaCreditoDetalle->total_gravada != 0){
                $taxImpact = 3;
                $porcentaje = round((((float)$notaCreditoDetalle->total_gravada * 100)/(float)$notaCreditoDetalle->precio_venta), 2);
                $taxRate = 10;
              }
  
              if($notaCreditoDetalle->total_exenta == 0 && $notaCreditoDetalle->total_gravada == 0){
                $taxImpact = 2;
                $porcentaje = 0;
                $taxRate = 0;
              }
  
              $item = [
                      'internalCode' => $notaCreditoDetalle->cod_confirmacion,
                      'description' => $notaCreditoDetalle->descripcion,
                      'measurementUnitCode' => 4,
                      'quantity' => $notaCreditoDetalle->cantidad,
                      'informationOfInterest' => 'info opcional',
                      'unitPriceWithTax' => $notaCreditoDetalle->precio_venta,
                      'itemExchangeRate' => 1,
                      'itemUnitPriceDiscountWithTax' => 0,
                      'itemDiscountPercentage' => 0,
                      'itemUnitPriceGlobalDiscountWithTax' => 0,
                      'itemUnitPriceAdvanceWithTax' => 0,
                      'itemUnitPriceGlobalAdvanceWithTax' => 0,
                      'taxImpact' => $taxImpact,
                      'taxedProportion' => $porcentaje,
                      'taxRate' => $taxRate
                  ];
          
              $items[] = $item;
          }
          
          $json['ElectronicDocuments'][0]['electronicDocumentItems'] = $items;
  
          if($cod_pais != 107 ){
              $facturaDetalle = Persona::with('pais')
                                        ->where('id',$notaCredito[0]->cliente_id)
                                        ->first(); 
              if(isset($facturaDetalle->pais->id_set)){
                  $codigo_pais = $facturaDetalle->pais->id_set;
              }else{
                  $codigo_pais = 249;
              } 
              $json['ElectronicDocuments'][0]['client'] = [
                'nature' => 2,
                'ruc' => $facturaDetalle->documento_identidad,
                'businessName' => $facturaDetalle->nombre,
                'fantasyName' => $facturaDetalle->nombre,
                'contributorType' => 1,
                'identityDocumentTypeCode' => '3',
                'operationType' => '4',
                'countryCode' => $codigo_pais,
                'email' => $facturaDetalle->email
              ];
          }elseif($notaCredito[0]->cliente->dv == ""){
              $facturaDetalle = Persona::with('pais')
                                        ->where('id',$notaCredito[0]->cliente_id)
                                        ->first(); 
              if(isset($facturaDetalle->pais->id_set)){
                $codigo_pais = $facturaDetalle->pais->id_set;
              }else{
                $codigo_pais = 249;
              }  
              $json['ElectronicDocuments'][0]['client'] = [
                                                          'nature' => 2,
                                                          'operationType' => 2,
                                                          'identityDocumentNumber' => $facturaDetalle->documento_identidad,
                                                          'businessName' => $nombre_cliente,
                                                          'fantasyName' => $nombre_cliente,
                                                          'contributorType' => 1,
                                                          'identityDocumentTypeCode' => '1',
                                                          'countryCode' => $codigo_pais,
                                                          'email' => $correo_cliente
                                                          ];
          }
  
          $client = new Client();
          // $response = $client->post('http://3.140.122.68:8080/Token', [
          $response = $client->post('http://3.140.122.68/Token', [
              'headers' => [
                  'Content-Type' => 'application/x-www-form-urlencoded',
              ],
              'form_params' => [
                                'username' => $username,
                                'password' => $password,
                                'grant_type' => 'password',
                ],
          ]);
          
          $body = $response->getBody();
          $data = json_decode($body, true);
          
          // Accede a los datos de la respuesta
          $accessToken = $data['access_token'];
     try{ 
           // $response = $client->post('http://3.140.122.68:8080/api/electronicDocument/Bulk', [
          $response = $client->post('http://3.140.122.68/api/electronicDocument/Bulk', [
              'headers' => [
                          'Content-Type' => 'application/json',
                          'Authorization' => 'Bearer '.$accessToken,
                          ],
              'json' => $json
          ]);
          
          $responseData = json_decode($response->getBody(), true);
          $cdc = $responseData['Items'][0]['CDC'];
          $url = $responseData['Items'][0]['DCarQR'];
          $id_impresion = $responseData['Items'][0]['Id'];
          $id_api = $responseData['Id'];
  
          $documento = new DocumentosEcos;
          $documento->id_documento = $id;
          $documento->id_api = $id_api;
          $documento->id_impresion = $id_impresion;
          $documento->tipo_documento = 5;
          $documento->cdc = $cdc;
          $documento->dcarqr = $url;
          $documento->request = json_encode($json);
          $documento->response = json_encode($responseData);
          $documento->fecha_envio = date('Y-m-d H:m:s'); 
          $documento->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa; 
          $documento->save();
          $id_eco = $documento->id;
  
          $update = DB::table('nota_credito')
                      ->where('id',$id)
                      ->update(['cdc'=>$cdc]);
          
         // $getNotificacion= DB::select("SELECT public.insert_notificacion(".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.",'".$correo_cliente."', null, null,27,".$id_eco.",".$idEmpresa.")");
         flash('Se ha enviado el DE exitosamente')->success();
         return redirect()->back();
      } catch(\Exception $e){

            $mensajeError = explode('":"',$e->getMessage());
            $mensajeErr = str_replace('"}','',$mensajeError[1]);
            flash('<b>'.$mensajeErr.'</b>')->error();
            return redirect()->back();
      }
   }

   public function generarJsonFacturaLote(Request $req) {
    $ids = array(
                  176528,
                  176483,
                  176620,
                  176773,
                  176597,
                  177047,
                  176598,
                  177049,
                  177051,
                  176601,
                  176486,
                  176928, 
                  176454,
                  176490,
                  176901,
                  177062,
                  177028,
                  176516,
                  176672,
                  176533,
                  176493,
                  176448,
                  176494,
                  176627,
                  176615,
                  177065,
                  176479,
                  176895,
                  177072,
                  176678,
                  176455,
                  177073,
                  176508,
                  176497,
                  176630,
                  177080,
                  176633,
                  176579,
                  177083,
                  176632,
                  176457,
                  176487,
                  176502,
                  176906,
                  176637,
                  176908,
                  176390,
                  176917,
                  177098,
                  177099,
                  177101,
                  176776,
                  176507,
                  177025,
                  177100,
                  176921,
                  177108,
                  177107,
                  177116,
                  176983,
                  176834,
                  176613,
                  176492
              );
    foreach ($ids as $elemento) {
        // Llamar a la función generarJsonFactura con cada elemento del array
    //  $estado = $this->generarJsonFactura($req, $elemento);
      echo '<pre>';
      print_r($elemento);
    }
}

public function generarJsonNCActualizacion(Request $req) {
  $data = [
            176379=>'01800267400001001000091222024013111763799565',
            176380=>'01800267400001001000091322024013111763801578',
            176390=>'01800267400001001000091422024013111763901696',
            176395=>'01800267400001001000091522024013111763953963',
            176398=>'01800267400001001000091622024013111763986900',
            176399=>'01800267400001001000091722024013111763998991',
            176432=>'01800267400001001000091822024020111764320586',
            176434=>'01800267400001001000091922024020111764349258',
            176436=>'01800267400001001000092022024020111764361035',
            176440=>'01800267400001001000092122024020111764404117',
            176441=>'01800267400001001000092222024020111764412888',
            176443=>'01800267400001001000092322024020111764434270',
            176444=>'01800267400001001000092422024020111764444593',
            176448=>'01800267400001001000092522024020111764481723',
            176450=>'01800267400001001000092622024020111764509598',
            176454=>'01800267400001001000092722024020111764549905',
            176455=>'01800267400001001000092822024020111764551250',
            176457=>'01800267400001001000092922024020111764574616',
            176461=>'01800267400001001000093022024020111764615620',
            176462=>'01800267400001001000093122024020111764625154',
            176479=>'01800267400001001000093222024020211764792300',
            176482=>'01800267400001001000093322024020211764821423',
            176483=>'01800267400001001000093422024020211764838571',
            176485=>'01800267400001001000093522024020211764857819',
            176486=>'01800267400001001000093622024020211764867539',
            176487=>'01800267400001001000093722024020211764873695',
            176488=>'01800267400001001000093822024020211764882783',
            176490=>'01800267400001001000093922024020211764908480',
            176492=>'01800267400001001000094022024020211764924824',
            176493=>'01800267400001001000094122024020211764933270',
            176494=>'01800267400001001000094222024020211764943772',
            176496=>'01800267400001001000094322024020211764964621',
            176497=>'01800267400001001000094422024020211764977979',
            176502=>'01800267400001001000094522024020211765020953',
            176503=>'01800267400001001000094622024020211765037929',
            176504=>'01800267400001001000094722024020211765040105',
            176507=>'01800267400001001000094822024020211765079095',
            176508=>'01800267400001001000094922024020211765088914',
            176511=>'01800267400001001000095022024020211765117254',
            176512=>'01800267400001001000095122024020211765127780',
            176513=>'01800267400001006000095222024020211765139952',
            176515=>'01800267400001001000095322024020211765156586',
            176516=>'01800267400001001000095422024020211765161725',
            176526=>'01800267400001001000095522024020211765265520',
            176528=>'01800267400001001000095622024020211765280740',
            176533=>'01800267400001001000095722024020211765336398',
            176541=>'01800267400001001000095822024020211765418440',
            176542=>'01800267400001001000095922024020211765422677',
            176548=>'01800267400001001000096122024020211765483100',
            176550=>'01800267400001001000096222024020211765506429',
            176552=>'01800267400001001000096322024020211765524281',
            176553=>'01800267400001001000096422024020211765530958',
            176555=>'01800267400001001000096522024020211765557317',
            176557=>'01800267400001001000096622024020211765579736',
            176561=>'01800267400001001000096722024020211765611141',
            176565=>'01800267400001001000096822024020211765656943',
            176569=>'01800267400001001000096922024020211765692559',
            176579=>'01800267400001001000097122024020211765797359',
            176587=>'01800267400001001000097222024020311765871298',
            176588=>'01800267400001001000097322024020311765884241',
            176589=>'01800267400001001000097422024020311765891124',
            176591=>'01800267400001001000097522024020311765910595',
            176593=>'01800267400001001000097622024020311765936500',
            176594=>'01800267400001001000097722024020311765941326',
            176597=>'01800267400001001000097822024020311765978289',
            176598=>'01800267400001001000097922024020311765983193',
            176600=>'01800267400001001000098022024020311766005240',
            176601=>'01800267400001001000098122024020311766011606',
            176613=>'01800267400001001000098222024020511766135492',
            176614=>'01800267400001001000098322024020511766144351',
            176615=>'01800267400001001000098422024020511766155914',
            176619=>'01800267400001001000098522024020511766191473',
            176620=>'01800267400001001000098622024020511766200391',
            176621=>'01800267400001001000098722024020511766213175',
            176626=>'01800267400001001000098822024020511766267402',
            176627=>'01800267400001001000098922024020511766276070',
            176630=>'01800267400001001000099022024020511766302028',
            176632=>'01800267400001001000099122024020511766321707',
            176633=>'01800267400001001000099222024020511766335546',
            176637=>'01800267400001001000099322024020511766370783',
            176644=>'01800267400001001000099422024020511766446917',
            176665=>'01800267400001001000099522024020511766658191',
            176672=>'01800267400001001000099622024020511766728424',
            176676=>'01800267400001001000099722024020511766764650',
            176678=>'01800267400001001000099822024020511766781783',
            176680=>'01800267400001001000099922024020511766809491',
            176685=>'01800267400001001000100122024020511766858929',
            176687=>'01800267400001001000100222024020511766876110',
            176689=>'01800267400001001000100322024020511766892639',
            176691=>'01800267400001001000100422024020511766914110',
            176692=>'01800267400001001000100522024020511766928641',
            176696=>'01800267400001001000100622024020511766960774',
            176699=>'01800267400001001000100722024020511766997490',
            176707=>'01800267400001001000100822024020511767077637',
            176708=>'01800267400001001000100922024020511767081596',
            176729=>'01800267400001001000101022024020511767298852',
            176730=>'01800267400001001000101122024020511767303635',
            176736=>'01800267400001001000101222024020511767368451',
            176737=>'01800267400001001000101322024020511767375610',
            176753=>'01800267400001001000101422024020511767530321',
            176756=>'01800267400001001000101522024020511767563734',
            176757=>'01800267400001001000101622024020511767574850',
            176769=>'01800267400001001000101822024020611767693385',
            176770=>'01800267400001001000101922024020611767704441',
            176771=>'01800267400001001000102022024020611767714992',
            176772=>'01800267400001001000102122024020611767729248',
            176773=>'01800267400001001000102222024020611767739928',
            176776=>'01800267400001001000102322024020611767767980',
            176802=>'01800267400001001000102422024020611768025112',
            176803=>'01800267400001001000102522024020611768030426',
            176807=>'01800267400001001000102622024020611768079093',
            176814=>'01800267400001001000102722024020611768146904',
            176825=>'01800267400001001000102822024020611768251961',
            176834=>'01800267400001001000102922024020611768343599',
            176838=>'01800267400001001000103022024020611768387241',
            176840=>'01800267400001001000103122024020611768406475',
            176843=>'01800267400001001000103222024020611768432786',
            176844=>'01800267400001001000103322024020611768447252',
            176846=>'01800267400001001000103422024020611768465188',
            176848=>'01800267400001001000103522024020611768484549',
            176850=>'01800267400001001000103622024020611768507689',
            176856=>'01800267400001001000103722024020611768569609',
            176857=>'01800267400001006000103822024020611768575181',
            176858=>'01800267400001001000103922024020611768585558',
            176859=>'01800267400001001000104022024020611768598658',
            176865=>'01800267400001001000104222024020611768654256',
            176874=>'01800267400001001000104322024020611768749818',
            176875=>'01800267400001001000104422024020611768753866',
            176879=>'01800267400001001000104522024020611768799459',
            176880=>'01800267400001001000104622024020611768800341',
            176881=>'01800267400001001000104722024020611768818801',
            176882=>'01800267400001001000104822024020611768829374',
            176889=>'01800267400001001000104922024020711768893828',
            176891=>'01800267400001001000105022024020711768915439',
            176892=>'01800267400001001000105122024020711768924276',
            176893=>'01800267400001001000105222024020711768932252',
            176895=>'01800267400001001000105322024020711768950404',
            176897=>'01800267400001001000105422024020711768971193',
            176901=>'01800267400001001000105522024020711769010880',
            176903=>'01800267400001001000105622024020711769032085',
            176905=>'01800267400001006000105722024020711769055330',
            176906=>'01800267400001001000105822024020711769068977',
            176908=>'01800267400001001000105922024020711769087874',
            176912=>'01800267400001001000106022024020711769128480',
            176917=>'01800267400001001000106122024020711769172047',
            176919=>'01800267400001001000106222024020711769190339',
            176921=>'01800267400001001000106322024020711769213282',
            176926=>'01800267400001001000106422024020711769260850',
            176928=>'01800267400001001000106522024020711769281466',
            176929=>'01800267400001001000106622024020711769291224',
            176931=>'01800267400001001000106722024020711769317010',
            176937=>'01800267400001001000106822024020711769378133',
            176953=>'01800267400001001000107022024020711769537523',
            176956=>'01800267400001001000107122024020711769569565',
            176961=>'01800267400001001000107222024020711769616601',
            176963=>'01800267400001001000107322024020711769639954',
            176967=>'01800267400001001000107422024020711769678070',
            176970=>'01800267400001001000107522024020711769706708',
            176971=>'01800267400001001000107622024020711769718854',
            176975=>'01800267400001001000107722024020711769752734',
            176978=>'01800267400001001000107822024020711769786663',
            176981=>'01800267400001001000107922024020711769816589',
            176982=>'01800267400001001000108022024020711769822943',
            176983=>'01800267400001001000108122024020711769835093',
            176985=>'01800267400001001000108222024020711769855779',
            177003=>'01800267400001001000108422024020711770036271',
            177006=>'01800267400001001000108522024020711770064054',
            177007=>'01800267400001001000108622024020711770074858',
            177008=>'01800267400001001000108722024020711770083695',
            177009=>'01800267400001001000108822024020711770095847',
            177012=>'01800267400001001000108922024020711770127331',
            177017=>'01800267400001001000109022024020811770179184',
            177025=>'01800267400001001000109122024020811770255298',
            177026=>'01800267400001001000109222024020811770260445',
            177028=>'01800267400001001000109322024020811770284352',
            177029=>'01800267400001001000109422024020811770290263',
            177042=>'01800267400001001000109622024020811770424790',
            177047=>'01800267400001001000109722024020811770474437',
            177049=>'01800267400001001000109822024020811770496724',
            177051=>'01800267400001001000109922024020811770517969',
            177062=>'01800267400001001000110022024020811770629280',
            177065=>'01800267400001001000110122024020811770654269',
            177071=>'01800267400001001000110222024020811770716256',
            177072=>'01800267400001001000110322024020811770720725',
            177073=>'01800267400001001000110422024020811770735285',
            177080=>'01800267400001001000110522024020811770806581',
            177083=>'01800267400001001000110622024020811770831470',
            177098=>'01800267400001001000110722024020811770985702',
            177099=>'01800267400001001000110822024020811770990307',
            177100=>'01800267400001001000110922024020811771006961',
            177101=>'01800267400001001000111022024020811771015381',
            177107=>'01800267400001001000111122024020811771079037',
            177108=>'01800267400001001000111222024020811771088532',
            177116=>'01800267400001001000111322024020811771166827',
            177119=>'01800267400001001000111422024020811771199903',
            177123=>'01800267400001001000111522024020811771239204',
            177127=>'01800267400001001000111622024020811771274727',
            176518=>'01800267400001001000017422024020211765186904',
            176520=>'01800267400001001000017522024020211765208975',
            176522=>'01800267400001001000017622024020211765220339',
            176524=>'01800267400001001000017822024020211765240968',
            176525=>'01800267400001001000017922024020211765256007',
            176559=>'01800267400001001000018022024020211765596485',
            176562=>'01800267400001001000018122024020211765622222',
            176596=>'01800267400001001000018222024020311765966540',
            176599=>'01800267400001001000018322024020311765991170',
            176634=>'01800267400001001000018422024020511766346592',
            176671=>'01800267400001001000018522024020511766711517',
            176725=>'01800267400001001000018622024020511767251080',
            176767=>'01800267400001001000018722024020611767671746',
            176783=>'01800267400001001000018822024020611767835986',
            176815=>'01800267400001001000019022024020611768154123',
            176852=>'01800267400001001000019122024020611768521797',
            176867=>'01800267400001001000019222024020611768672477',
            176870=>'01800267400001001000019322024020611768702759',
            176888=>'01800267400001001000019422024020711768883719',
            176923=>'01800267400001001000019522024020711769235125',
            176944=>'01800267400001001000019622024020711769448714',
            176945=>'01800267400001001000019722024020711769454730',
            176954=>'01800267400001001000019822024020711769547577',
            176966=>'01800267400001001000019922024020711769666053',
            176974=>'01800267400001001000020022024020711769744678',
            177001=>'01800267400001001000020122024020711770016869',
            177005=>'01800267400001001000020222024020711770052504',
            177019=>'01800267400001001000020322024020811770194560',
            177024=>'01800267400001001000020422024020811770243960',
            177044=>'01800267400001001000020522024020811770446853',
            177052=>'01800267400001001000020622024020811770522380',
            177055=>'01800267400001001000020722024020811770553765',
            177059=>'01800267400001001000020822024020811770595557',
            177093=>'01800267400001001000020922024020811770938071',
            177094=>'01800267400001001000021022024020811770941224',
            177103=>'01800267400001001000021122024020811771031973',
            177111=>'01800267400001001000021222024020811771112191',
            177117=>'01800267400001001000021322024020811771179300',
            177126=>'01800267400001001000021422024020811771266393'          
          ];

  $resultado = $this->generarJsonNCFactura($data);
}

public function generarJsonNCFactura($data){

  if($this->getIdEmpresa() == 1){
      $username = 'admin@dtparaguayo.com.py';
    // $password = 'ILZXg16OE';
      $password = 'TGru4HuxiE';
  }else{
      $username = 'admin@boardingpass.com.py';
      //$password = 'tiRRmrHQce';
      $password = 'RA3mAlRUEF';   
  }
  $contador = 0;
  foreach($data as $key=>$elemento){
            $contador = $contador + 1;
            $facturas = DB::select("SELECT * FROM public.vw_facturas WHERE id = ".$key); 
            $detalleFacturas = DB::select("SELECT * FROM public.vw_detalle_facturas WHERE id_factura = ".$key);

            $securityCode = $this->securityCode($key);

            $fechaEmision = date('Y-m-d H:i:s');  

            $format = 'Y-m-d H:i:s';

            $date = \DateTime::createFromFormat($format, $fechaEmision);
            if ($date) {
                $formattedDate = $date->format('Y-m-d\TH:i:s');
                $formattedDateF = $date->format('Y-m-d\TH:i:s');
            } else {
                echo 'Fecha inválida';
            }

            $moneda_factura = $facturas[0]->moneda_venta;

            $nombre_cliente = $facturas[0]->cliente_nombre;
            if($facturas[0]->cliente_apellido != ""){
                $nombre_cliente = $nombre_cliente." ".$facturas[0]->cliente_apellido;
            }
            $tipo_facturacion = 0;
            if($facturas[0]->id_tipo_facturacion == 2){
              $tipo_facturacion = 1;
            }

            $direccion = $facturas[0]->direccion_cliente;
            if($facturas[0]->nombre_pais == ""){
              $pais = 'Paraguay'; 
              $cod_pais = 107; 
            }else{
              $pais = $facturas[0]->nombre_pais; 
              $cod_pais = $facturas[0]->cod_pais; 
            }

            $idEmpresa = $facturas[0]->id_empresa;
            if($facturas[0]->dv_cliente == ""){
              $ruc = $facturas[0]->documento_cliente;
            }else{
              $ruc = $facturas[0]->documento_cliente."-".$facturas[0]->dv_cliente;

            }
            $personeria = $facturas[0]->id_personeria;
            $correo_cliente = $facturas[0]->cliente_email;
            if($personeria == 0 || $personeria == 0){
              $personeria = 1;
            }
            $base_factura = explode('-',$facturas[0]->nro_factura);
            $numbero = (int)$base_factura[2];
            $total_Factura = DB::select('SELECT * FROM get_monto_factura('.$facturas[0]->id.')');
            $totalFactura = $total_Factura[0]->get_monto_factura;
            $empresa_nombre = $facturas[0]->empresa_nombre; 
            $empresa_direccion = $facturas[0]->empresa_direccion; 
            $empresa_telefono = str_replace('+ 595 ','0',$facturas[0]->empresa_telefono);
            $empresa_correo = $facturas[0]->empresa_correo;
            //   $timbrado_fecha = '2023-10-24';
            $timbrado_establecimiento = $facturas[0]->timbrado_establecimiento;
            $timbrado_expedicion = $facturas[0]->timbrado_expedicion;
            $timbrado_fecha = $facturas[0]->timbrado_autorizacion;
            $iva = $facturas[0]->total_iva;
            $sub_total = (float)$totalFactura - (float)$iva;
            $sub_total =number_format($sub_total,2,".","");
            $empresa_ruc = $facturas[0]->empresa_ruc;
            // $base = explode('-', $facturas[0]->empresa_ruc);
            $timbrado_numero = $facturas[0]->timbrado_numero;
            // $timbrado_numero = $base[0];
            if($moneda_factura == 'PYG'){
              $cotizacion = 0;
            }else{
              $cotizacion = $facturas[0]->cotizacion_factura;
            }

            $cdc = $elemento;

            if($cdc == '' ){
                $associatedDocumentType = 1;
            }else{
                $associatedDocumentType = 0;
            }

            $idTimbradoFactura = $facturas[0]->id_timbrado;

            $timbradoFactura = Timbrado::findOrFail($idTimbradoFactura);	

            $stampNumber = $timbradoFactura->numero;

            $timbrado_establecimiento = '001';

            $timbrado_expedicion = '001';

            if($contador > 9){
              $numbero = '0000'.$contador;
            }else{
              $numbero = '00000'.$contador;
            }

            
            $json = [
                    'ElectronicDocuments' => [
                                        [
                                            'issuingType' => 0,
                                            'securityCode' => $securityCode,
                                            'aditionalInformation' => '',
                                            'aditionalTreasuryInformation' => '',
                                            'documentTypeCode' => 5,
                                            'number' => $numbero,
                                            'series' => 'AA',
                                            'issuedDate' => $formattedDate,
                                            'transactionTypeCode' => 1,
                                            'taxTypeCode' => 1,
                                            'currencyTypeCode' => $moneda_factura,
                                            'exchangeRate' => $cotizacion,
                                            'client' => [
                                                'nature' => 1,
                                                'operationType' => 1,
                                                'countryCode' => $cod_pais,
                                                'countryName' => $pais,
                                                'contributorType' => $personeria,
                                                'ruc' => $ruc,
                                                'businessName' => $nombre_cliente,
                                                'fantasyName' => $nombre_cliente,
                                                'address' => $direccion,
                                            ],
                                            'operationCondition' => 0,
                                            'subTotal' => $sub_total,
                                            'total' => $totalFactura,
                                            'tax' => $iva,
                                            'electronicDocumentItems' => [],
                                            'branch' => [
                                                'branchDocumentTypes' => [
                                                    [
                                                        'stablishment' => $timbrado_establecimiento,
                                                        'expeditionPoint' => $timbrado_expedicion,
                                                        'stampNumber' => $timbrado_numero,
                                                        'stampDate' => $timbrado_fecha,
                                                    ],
                                                ],
                                                'address' => $empresa_direccion,
                                                'houseNumber' => 1078,
                                                'addressComplement1' => '',
                                                'addressComplement2' => '',
                                                'cityCode' => 1,
                                                'phone' => $empresa_telefono,
                                                'email' => $empresa_correo,
                                                'name' => 'Matriz',
                                            ],
                                            'PresenceIndicatorCode' => 1,
                                            'payments' => [
                                                [
                                                    'paymentMethodCode' => 1,
                                                    'ammount' => $totalFactura,
                                                ],
                                            ],
                                            'tenant' => [
                                                'stampNumber' => $timbrado_numero,
                                                'stampDate' => $timbrado_fecha,
                                                'ruc' => $empresa_ruc,
                                                'taxpayerType' => 2,
                                                'name' => $empresa_nombre,
                                                'activities' => [
                                                    [
                                                        'name' => 'ACTIVIDADES DE LOS OPERADORES TURÍSTICOS',
                                                        'identifier' => '79120',
                                                    ],
                                                ],
                                            ],
                                            'associatedDocuments' => [
                                                [
                                                    'associatedDocumentType' => $associatedDocumentType, //Obtener API GeneralCodes/Get -> AssociatedDocumentTypes.Identifier
                                                    'cdc' => $cdc, //'cdc' => $cdc,
                                                    'stampNumber' =>$stampNumber ,//'stampNumber' => $stamp_number ,
                                                    'printedDocumentType' => 0,//Obtener API GeneralCodes/Get -> PrintedDocumentTypes.Identifier
                                                    'issuingDate' => $formattedDateF,
                                                ],
                                            ],
                                        ],
                                    ],
                                ];

                                $items = [];
                                foreach ($detalleFacturas as $notaCreditoDetalle) {
                                    if($notaCreditoDetalle->exento != 0 && $notaCreditoDetalle->gravadas_10 == 0){
                                        $taxImpact = 2;
                                        $porcentaje = 0;
                                        $taxRate = 0;
                                    }

                                    if($notaCreditoDetalle->exento == 0 && $notaCreditoDetalle->gravadas_10 != 0){
                                      $taxImpact = 0;
                                      $porcentaje = 100;
                                      $taxRate = 10;
                                    }

                                    if($notaCreditoDetalle->exento != 0 && $notaCreditoDetalle->gravadas_10 != 0){
                                      $taxImpact = 3;
                                      $porcentaje = round((((float)$notaCreditoDetalle->gravadas_10 * 100)/(float)$notaCreditoDetalle->precio_venta), 2);
                                      $taxRate = 10;
                                    }

                                    if($notaCreditoDetalle->exento == 0 && $notaCreditoDetalle->gravadas_10 == 0){
                                      $taxImpact = 2;
                                      $porcentaje = 0;
                                      $taxRate = 0;
                                    }

                                    $item = [
                                            'internalCode' => $notaCreditoDetalle->cod_confirmacion,
                                            'description' => $notaCreditoDetalle->descripcion,
                                            'measurementUnitCode' => 4,
                                            'quantity' => $notaCreditoDetalle->cantidad,
                                            'informationOfInterest' => 'info opcional',
                                            'unitPriceWithTax' => $notaCreditoDetalle->precio_venta,
                                            'itemExchangeRate' => 1,
                                            'itemUnitPriceDiscountWithTax' => 0,
                                            'itemDiscountPercentage' => 0,
                                            'itemUnitPriceGlobalDiscountWithTax' => 0,
                                            'itemUnitPriceAdvanceWithTax' => 0,
                                            'itemUnitPriceGlobalAdvanceWithTax' => 0,
                                            'taxImpact' => $taxImpact,
                                            'taxedProportion' => $porcentaje,
                                            'taxRate' => $taxRate
                                        ];

                                    $items[] = $item;
                                }
                                $json['ElectronicDocuments'][0]['electronicDocumentItems'] = $items;

                          if($cod_pais != 107 ){
                            $facturaDetalle = Persona::with('pais')
                                                      ->where('id',$facturas[0]->cliente_id)
                                                      ->first(); 
                            if(isset($facturaDetalle->pais->id_set)){
                                $codigo_pais = $facturaDetalle->pais->id_set;
                            }else{
                                $codigo_pais = 249;
                            } 
                            $json['ElectronicDocuments'][0]['client'] = [
                              'nature' => 2,
                              'ruc' => $facturaDetalle->documento_identidad,
                              'businessName' => $facturaDetalle->nombre,
                              'fantasyName' => $facturaDetalle->nombre,
                              'contributorType' => 1,
                              'identityDocumentTypeCode' => '3',
                              'operationType' => '4',
                              'countryCode' => $codigo_pais,
                              'email' => $facturaDetalle->email
                            ];
                        }elseif($facturas[0]->dv_cliente == ""){
                            $facturaDetalle = Persona::with('pais')
                                                      ->where('id',$facturas[0]->cliente_id)
                                                      ->first(); 
                            if(isset($facturaDetalle->pais->id_set)){
                              $codigo_pais = $facturaDetalle->pais->id_set;
                            }else{
                              $codigo_pais = 249;
                            }  
                            $json['ElectronicDocuments'][0]['client'] = [
                                                                        'nature' => 2,
                                                                        'operationType' => 2,
                                                                        'identityDocumentNumber' => $facturaDetalle->documento_identidad,
                                                                        'businessName' => $nombre_cliente,
                                                                        'fantasyName' => $nombre_cliente,
                                                                        'contributorType' => 1,
                                                                        'identityDocumentTypeCode' => '1',
                                                                        'countryCode' => $codigo_pais,
                                                                        'email' => $correo_cliente
                                                                        ];
                        }

                        echo '<pre>';
                        print_r(json_encode($json)); 

                        /*die;*/
                      $client = new Client();
                        // $response = $client->post('http://3.140.122.68:8080/Token', [
                        $response = $client->post('http://3.140.122.68/Token', [
                            'headers' => [
                                'Content-Type' => 'application/x-www-form-urlencoded',
                            ],
                            'form_params' => [
                                              'username' => $username,
                                              'password' => $password,
                                              'grant_type' => 'password',
                              ],
                        ]);
                        
                        $body = $response->getBody();
                        $data = json_decode($body, true);
                        
                        // Accede a los datos de la respuesta
                        $accessToken = $data['access_token'];

                        /* echo '<pre>';
                        print_r(json_encode($accessToken));*/
                          // $response = $client->post('http://3.140.122.68:8080/api/electronicDocument/Bulk', [
                        $response = $client->post('http://3.140.122.68/api/electronicDocument/Bulk', [
                            'headers' => [
                                        'Content-Type' => 'application/json',
                                        'Authorization' => 'Bearer '.$accessToken,
                                        ],
                            'json' => $json
                        ]);
                        
                        $responseData = json_decode($response->getBody(), true);

                        $cdc = $responseData['Items'][0]['CDC'];
                        $url = $responseData['Items'][0]['DCarQR'];
                        $id_impresion = $responseData['Items'][0]['Id'];
                        $id_api = $responseData['Id'];

                        $documento = new DocumentosEcos;
                        $documento->id_documento = $key;
                        $documento->id_api = $id_api;
                        $documento->id_impresion = $id_impresion;
                        $documento->tipo_documento = 5;
                        $documento->cdc = $cdc;
                        $documento->dcarqr = $url;
                        $documento->request = json_encode($json);
                        $documento->response = json_encode($responseData);
                        $documento->fecha_envio = date('Y-m-d H:m:s'); 
                        $documento->id_empresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa; 
                        $documento->save();
                        $id_eco = $documento->id;

                        $update = DB::table('nota_credito')
                                    ->where('id',$key)
                                    ->update(['cdc'=>$cdc]);
                        
                        // $getNotificacion= DB::select("SELECT public.insert_notificacion(".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.",'".$correo_cliente."', null, null,27,".$id_eco.",".$idEmpresa.")");
                        echo '<pre>';
                        echo 'OK'; 
      }
}






}//clase
