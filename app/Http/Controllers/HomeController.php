<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use	Illuminate\Support\facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;
use \App\DestinoDtpmundo;
use \App\Agencias;
use \App\Usuario;
use \App\Sucursal;
use \App\Currency;
use \App\Destinationdtp;
use \App\Destination;
use \App\FileReserva;
use \App\Infoprovider;
use \App\Paymentmethod;
use \App\Perfil;
use \App\PerfilPaymentmethod;
use \App\State;
use \App\AccesosToken;
use App\ReservasServicio;
use App\ReservasVuelos;
use App\Reserva;
use App\Producto;
use App\FormaCobroReciboDetalle;
use App\CuentaCorrienteEmpresa;
use App\CtaCtteFormaCobro;


use Session;
use DB;
use Cookie;
use App\Traits\RecibosTrait;


class HomeController extends Controller

{

	use RecibosTrait;
	/*
	Página de inicio 
	*/

	private function getId4Log(){
		//Se obtiene el ID para identificar al proceso en el log-->dia+mes+año+hora+min+seg+miliseg(exactitud 4 puntos decimales.)
		  list($MiliSegundos, $Segundos) = explode(" ", microtime());
		  $id=substr(strval($MiliSegundos),2,4);
		  return strval(date("d").date("m").date("y").date("H").date("i").date("s").$id);
	}

    public function index(Request $req){
		
	   	Session::forget('ocupancia');
        Session::forget('hotelsSession');
        Session::forget('datosPreReservaRQ');
        Session::forget('datosPreReservaRP');
        Session::forget('totalReservaPrecioNeto');
        Session::forget('totalReservaSinImpuestos');
        Session::forget('totalReservaConComision');
        Session::forget('totalPrecioNetoConvertido');
        Session::forget('token');
        Session::forget('urlFull');
        Session::forget('urlTotal');
        Session::forget('codRetorno');
        Session::forget('desRetorno');
        Session::forget('iibObjRsp');
        Session::forget('marcador');
        Session::forget('sid');
        Cookie::forget('marcador');
		$user = new \StdClass;
		$user->usuario = $req->session()->get('datos-loggeo')->datos->datosUsuarios->usuario;
		$user->contrasenha = $req->session()->get('datos-loggeo')->datos->datosUsuarios->tokenBk;
		return view('pages.home')->with('user', $user,'total_facturacion_mes',$total_facturacion_mes);
	}

	/**
	 * Responde a solicitud de ajax de la pagina de home para enviar datos al dashboard, 
	 * filtra por tipo de persona 
	 * logueada si es vendedor_empresa = 3 o supervisor empresa = 4
	 */
	public function indexGraphAjax(Request $req){
		$origen =  new \StdClass;
		$meta_mes = 0;
		$totalOrigen = 0;
		$idUsuario = $req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$idUsuario = $req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$idSucursalEmpresa = DB::select("SELECT id_sucursal_empresa FROM personas WHERE id = ".$idUsuario);
		$idSucursalEmpresa = $idSucursalEmpresa[0]->id_sucursal_empresa;
		$tipoUser = DB::select("SELECT id_tipo_persona from personas where id = ".$idUsuario);


		//VENDEDOR EMPRESA
		if($tipoUser[0]->id_tipo_persona == 3){
		$periodoFacturacion = DB::select("SELECT id_usuario, fecha,COALESCE (total,0) as total 
										  FROM public.vw_periodo_facturacion 
										  WHERE id_usuario = ".$idUsuario." AND id_empresa = ".$this->getIdEmpresa());
		
		$origen_detalles_proforma = DB::select("SELECT origen,COALESCE (total,0) as total FROM public.vw_origen_proformas_detalle where id_usuario = ".$idUsuario);

		$total_facturacion_mes = DB::select("SELECT SUM(neto_factura) as total FROM public.vw_total_facturacion_mes where id_usuario = ".$idUsuario);

		$total_renta_mes = DB::select("SELECT id_usuario, COALESCE (total,0) as total FROM public.vw_total_facturacion_renta where id_usuario = ".$idUsuario);
		$total_proforma_pendiente = DB::select("SELECT id_usuario, COALESCE (total,0) as total FROM public.vw_proformas_pendientes where id_usuario = ".$idUsuario);

		$renta = (!isset($total_renta_mes)) ? $total_renta_mes[0]->total : 0;
		$meta_mes = DB::select("SELECT facturacion FROM public.vw_meta_facturacion where id_vendedor = ".$idUsuario);
		$comision_vendedor = DB::select("SELECT * FROM public.get_porcentaje_comision(".$idUsuario.",".$renta.")");
		//
		
		}
		
		//SUPERVISOR EMPRESA
		 if($tipoUser[0]->id_tipo_persona == 4){
			$periodoFacturacion = DB::select("SELECT *,COALESCE (total,0) as total 
										  FROM public.vw_periodo_facturacion 
										  WHERE id_usuario = ".$idUsuario);
			$origen_detalles_proforma = DB::select("SELECT vw.origen, sum(vw.total) as total FROM public.vw_origen_proformas_detalle as vw , personas as p 
												where p.id = vw.id_usuario
												and p.id_sucursal_empresa = (select id_sucursal_empresa from personas where id = ".$idUsuario." limit 1) group by vw.origen");

			$total_facturacion_mes  = DB::select("SELECT id_sucursal_empresa,SUM(neto_factura) AS total FROM vw_total_facturacion_mes
											where id_sucursal_empresa =".$idSucursalEmpresa." GROUP BY id_sucursal_empresa");

		$total_renta_mes = DB::select("SELECT COALESCE (sum(total),0) as total  from (select vw.*,p.id_sucursal_empresa 
									  from vw_total_facturacion_renta as vw, personas p
									  where p.id = vw.id_usuario	
									  and p.id_sucursal_empresa = (select id_sucursal_empresa from personas where id = ".$idUsuario." limit 1) ) as total");
		$total_proforma_pendiente = DB::select("SELECT COALESCE (sum(total),0) as total  from (
									SELECT  COALESCE (total,0) as total FROM public.vw_proformas_pendientes as vw, personas p
									where p.id = vw.id_usuario
									and p.id_sucursal_empresa = (select id_sucursal_empresa from personas where id = ".$idUsuario." limit 1) ) as total");	
		$comision_vendedor = 0;

		} 

	



		$json = array();
		$q = 0;
		$totalVenta = 0;
		$json[] = array('y'=>date('Y-m-01'),'item1'=>0);
		foreach ($periodoFacturacion as  $value) {
			$totalVenta = $value->total + $totalVenta;
			$json[] = array('y'=>$value->fecha,'item1'=>$totalVenta);

		}

		if(!empty($origen_detalles_proforma)){

			$origen->total = 0;
			$origen->manualTotal = 0;
			$origen->autoTotal = 0;


			foreach ($origen_detalles_proforma as $key => $value) {
				$totalOrigen = $value->total + $totalOrigen;
				if($value->origen == 'M'){
					$origen->manualTotal = $value->total;
				} 
				if($value->origen == 'A'){
					$origen->autoTotal = $value->total;
				} 
			}			
			$origen->total = $totalOrigen;


		} else {
			$origen->total = 0;
			$origen->manualTotal = 0;
			$origen->autoTotal = 0;

		}
		// dd($json);

		return response()->json(['grafico1'=>$json,
								 'grafico2'=>$origen,
								 'facturacionTotal'=>$total_facturacion_mes,
								 'facturacionRenta'=>$total_renta_mes,
								 'totalProformaPendiente'=>$total_proforma_pendiente,
								 'comisionVendedor'=>$comision_vendedor,
								 'metaMes'=>$meta_mes,
								 'totalVenta'=>$totalVenta
								]);
	}

	private function getIdEmpresa(){
        return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
    }

	public function ajaxReserva(){
		$resultados = DB::select("SELECT * FROM public.vw_busqueda_reserva where nemo='true'and vendedor_id = ".Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario." and id_proforma is null and estado_id = 2");
		return response()->json($resultados);
	}
	/**
	 * Responde a solicitud de ajax de la pagina de home para enviar datos al dashboard,
	 * Datos para ser visto por el ADMIN_EMPRESA
	 */
	public function indexHighchartsAjax(){
		$grafico2 = new \StdClass;
		//DATATABLE DE TIPO TICKET ESTADO DISPONIBLE Y EN PROFORMA
		$tickets_emitidos_facturados = DB::select("SELECT * FROM public.vw_tickets_emitidos_facturados where id_empresa =".$this->getIdEmpresa() );

		// $ticket_disponibles_utilizados_amadeus = DB::select("SELECT * FROM public.vw_ticket_disponibles_utilizados_amadeus");
		// $ticket_disponibles_utilizados_proforma = DB::select("SELECT * FROM public.vw_ticket_disponibles_utilizados_proforma");	

		$tableTicket = DB::select("SELECT * FROM public.vw_ticket_tipo_estado WHERE id_empresa = ".$this->getIdEmpresa());
		
	
		if($this->getIdEmpresa() == 1){
			$cantidadLibre = DB::select("SELECT count(*) as cantidad FROM public.vw_reservas_cangoroo where  estado_gestur = 'DISPONIBLE'");
			$cantidadProforma = DB::select("SELECT count(*) as cantidad  FROM public.vw_reservas_cangoroo where estado_gestur = 'EN PROFORMA'");
			$key = count($tableTicket);
			$arrayRsp= new \StdClass;
			$arrayRsp->cant_ticket_disponible= $cantidadLibre[0]->cantidad;
			$arrayRsp->cant_ticket_en_proforma = $cantidadProforma[0]->cantidad;
			$arrayRsp->descripcion = 'Reservas Cangoroo';
			$arrayRsp->id_tipo_ticket = 0;
			$arrayRsp->id_empresa = 1;
			$tableTicket[$key] = $arrayRsp;
		}
	

		$facturacion_10dias_control = DB::select("SELECT * FROM vw_periodo_facturacion_10dias WHERE id_empresa = ".$this->getIdEmpresa());

		 if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 4){
		 	$facturacion_10dias = DB::select("SELECT round(total::decimal,2)::text as redondeo_total FROM vw_periodo_facturacion_10dias WHERE id_empresa = ".$this->getIdEmpresa()." AND id_sucursal_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia);
		 }else{
		 	$facturacion_10dias = DB::select("SELECT sum(total) as redondeo_total, fecha FROM vw_periodo_facturacion_10dias WHERE id_empresa = ".$this->getIdEmpresa()." GROUP BY  fecha");
		 }

		$total_proforma_pendiente = DB::select("SELECT  COALESCE (sum(total),0) as total FROM public.vw_proformas_pendientes WHERE id_empresa = ".$this->getIdEmpresa());

		$total_proforma_pendiente = number_format($total_proforma_pendiente[0]->total, 2, ",", ".");

		return response()->json(array('tableTicket'=> $tableTicket,
									  'grafico1'=>$tickets_emitidos_facturados,
									  'grafico3'=>$facturacion_10dias,
									  'totalProformaPendiente'=>$total_proforma_pendiente));

	}
	
	public function ajaxTable(Request $req)
	{
	$idUsuario = $req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario;	
	$tipoUser = DB::select("SELECT id_tipo_persona,id_sucursal_empresa from personas where id = ".$idUsuario."AND id_empresa =".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

	//SUPERVISOR EMPRESA
	if ($tipoUser[0]->id_tipo_persona == 4){
		$comision = DB::select("SELECT * 
							FROM vw_meta_vendedores_agentes vw
							WHERE id_suc_agente = ".$tipoUser[0]->id_sucursal_empresa."
							AND id_empresa =".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa
							);	
	}
	//VENDEDOR EMPRESA
	if($tipoUser[0]->id_tipo_persona == 3){
	   $comision = DB::select("SELECT * 
	   			FROM vw_meta_vendedores_agentes 
	   			WHERE id_usuario = ".$idUsuario."
				AND id_empresa =".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
		
	}

	foreach ($comision as  $value) {
		
		$value->peso = 0;
		$value->ico = "fa fa-exclamation-triangle icoRojo";

		if($value->total_venta >= $value->meta ){
			//BIEN		
			$value->ico = "fa fa-thumbs-o-up icoVerde";		
	} else if($value->meta > $value->total_venta){
		//calcular porcentaje
		$resp = $value->total_venta * 100;
		$resp = $resp / $value->meta;


		if($resp == 100){
			$value->ico = "fa fa-thumbs-o-up icoVerde";
			$value->peso = 2;
		} else if($resp < 100 && $resp >= 50){
			$value->ico = "fa fa-exclamation-triangle icoAmarillo";
			$value->peso = 1;
		} else if($resp < 50 && $resp != 0){
			$value->ico = "fa fa-exclamation-triangle icoRojo";
			$value->peso = 0;
		}	

	} else {
		//algo mal
	}


	}

	// dd($comision);

	return response()->json(['comision'=>$comision]);
}

	




    public function indexMc(Request $req){
    	$totalOrigen = 0;
    	$origen =  new \StdClass;
		$user = new \StdClass;
		// dd($req->session()->get('datos-loggeo'));
		if(!$req->session()->has('datos-loggeo')){
            flash('<b>Error de Sessión</b></br> No posee los permisos o su sesión ha caducado.')->error();
    			return redirect()->route('login');
		}
		$user->usuario = $req->session()->get('datos-loggeo')->datos->datosUsuarios->usuario;
		$user->empresa = $req->session()->get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		$idUsuario = $req->session()->get('datos-loggeo')->datos->datosUsuarios->idUsuario;
		$tipoUser = DB::select("SELECT id_tipo_persona from personas where id = ".$idUsuario);


		
		return view('pages.mc.home')->with(['user'=>$user,'tipoUser'=>$tipoUser]);
			
	}

	/*
	Ir destinos
	-Carga los detinos en el listado que aparece en la página principal o busqueda
	*/
	public function getDestinationsFligths(Request $req){
		include("../aeropuerto-destino.php");
		if($req->has('term')){
            $search = $req->term;
			foreach($aeropuerto as $key=>$destino){
				$optionExplode =  explode("|",$destino);
				/*echo '<pre>';
				print_r($optionExplode[2]);*/
				$option = $key."".$optionExplode[0]."".$optionExplode[1]."".$optionExplode[2];
				$resultado = strpos(strtolower($option), strtolower($search));
				if($resultado === FALSE){
					unset($aeropuerto[$key]);
				}
			}
		}
		$resultados = [];
		foreach($aeropuerto as $key=>$resultado){
			$option =  explode("|",$resultado);
			$aux[$key] = $option[4];
			$resultados[$key] =ucwords(strtolower($option[0]).", ".strtolower($option[1])." - ".strtolower($option[2]))."||".$option[3];
		}
		array_multisort($aux, SORT_DESC, $resultados); 
        return response()->json($resultados);
	}

	public function rtest(Request $req){
		$req->session()->flash('codRetorno',666);
		$req->session()->flash('desRetorno', 'Error Critico');
		return redirect()->route('home');
	}

	public function destacados(Request $req){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
						  	'api-key' => 'IPu,Rv=]tdT2'
							 ]
		]);
		$res = $client->request('GET', 'http://info.dtpmundo.com/paquetes/destacados');
		$respuesta = $res->getBody();
		return $respuesta;

	}

	public function testing(Request $req){
		//dd(env('PROVIDERS'));
		//dd(explode(',', env('PROVIDERS')));
		//dd(route('getCurrentHotels'));
		//dd(json_encode('{}'));
		//dd($req->getQueryString());
		//if(  (\DateTime::createFromFormat("m-d-Y" , '05-24-2019'))  > new \DateTime() ) dd('hallo');
		//else dd('hi');
		//dd((\DateTime::createFromFormat("m-d-Y" , '05-24-1993'))->format('d-m-Y'));
		//dd(\App::environment());
		//dd(Config::get('config.facInsertarUsuario'));
		//$client = new Client();
		//$email = 'g@g.com';
		//$midRsp = $client->get('http://httpbin.org/get?'.$email);
		//die($midRsp->getBody());
		//$result = DB::select('Select f_encrypt_user_password_by_md5(?) as result',array("b123456"));
		//dd($result[0]->result);
		//dd(Session::get('datos-loggeo'));
		//return json_encode($req->session()->get('token'));
		//return json_encode(Session::get('searchData'));
		//return json_encode($req->session()->get('filterInfo'));
		echo '<pre>';
		print_r($req->session());
		//dd(Session::get('datos-loggeo'));
		
		//dd(url('detallesReserva'));
		
		
		
		//$req->session->set('token',666);
		
		$iibObjRsp = json_decode('{
					"codRetorno" : "0",
					"desRetorno" : "PROCESADO EXITOSAMENTE RESERVA BKM",
					"datos" : {
						"datosReserva" : {
							"codReserva" : "13201",
							"nombreReferencia" : "DTP MUNDO PARAGUAY",
							"estado" : "OK",
							"totalReserva" : 61,
							"codMoneda" : "USD",
							"fechaInicioReserva" : "2018-07-19",
							"fechaFinReserva" : "2018-07-20",
							"politicasModificacion" : {
								"modificable" : false,
								"cancelable" : false,
								"additionalProperties" : {}
							},
							"infoAdicional" : "",
							"titular" : {
								"nombreTitular" : "Federico",
								"apellidoTitular" : "Gaona",
								"additionalProperties" : {}
							},
							"hotel" : {
								"destinoBusqueda" : "150001",
								"nombreHoteles" : [],
								"nombreZonas" : [],
								"hotel" : [{
										"titulo" : {
											"nomHotel" : "PALMAS DEL SOL",
											"codHotel" : "7263386",
											"codProveedor" : "11",
											"desProveedor" : "Restel",
											"additionalProperties" : {}
										},
										"dirHotel" : "Paraguay - Asuncion - AV. ESPA\u00d1A 202",
										"webHotel" : [],
										"mailHotel" : [],
										"habitacion" : [{
												"codHabitacion" : "-",
												"desHabitacion" : "Doble-twin",
												"estadoHabitacion" : "DISP",
												"tarifa" : [{
														"claseTarifa" : "-",
														"precioNeto" : "61.0",
														"tipoPago" : [],
														"utilizableEnPaquete" : "false",
														"codConsumision" : "Alojamiento Y Desayuno",
														"desConsumision" : "Alojamiento Y Desayuno",
														"politicaDeCancelacion" : [{
																"monto" : 59.642,
																"desdeFecha" : "2018-07-09T00:00:00",
																"additionalProperties" : {}
															}
														],
														"habitaciones" : 1,
														"adultos" : 2,
														"ninos" : 0,
														"edadNinos" : [],
														"promociones" : [],
														"ofertas" : [],
														"detallesExtrasHabitacion" : [],
														"comision" : [],
														"impuestos" : [],
														"additionalProperties" : {}
													}
												],
												"ocupancia" : [{
														"detalle" : [{
																"tipo" : "AD",
																"nombre" : "Federico",
																"apellido" : "Gaona",
																"additionalProperties" : {}
															}
														],
														"additionalProperties" : {}
													}, {
														"detalle" : [{
																"tipo" : "AD",
																"nombre" : "Federico",
																"apellido" : "Gaona",
																"additionalProperties" : {}
															}
														],
														"additionalProperties" : {}
													}
												],
												"comodidadesHabitacion" : [],
												"imagenHabitacion" : [],
												"additionalProperties" : {}
											}
										],
										"coordenadasHotel" : {
											"latitud" : 0,
											"longitud" : 0,
											"additionalProperties" : {}
										},
										"imagenHotel" : [],
										"telefonoHotel" : [],
										"comodidadHotel" : [],
										"detalleExtraHotel" : [],
										"comodidadesHotel" : [],
										"additionalProperties" : {}
									}
								],
								"additionalProperties" : {}
							},
							"additionalProperties" : {}
						},
						"additionalProperties" : {}
					},
					"additionalProperties" : {},
					"selectedRates" : [{
							"roomCode" : "-",
							"roomName" : "Doble-twin",
							"adults" : 2,
							"children" : 0,
							"desConsumision" : "Alojamiento Y Desayuno"
						}
					],
					"precioCan" : "59.642",
					"fechaCan" : "2018-07-07",
					"fechaReserva" : "24\/04\/2018"
				}');
		
		return view('pages.hoteles.booking')->with('confRsp', $iibObjRsp);
	}

	public function getDestinations(Request $req){
		ini_set('memory_limit', '256M');
		$datos = file_get_contents("../destination_actual.json");
		$destinos =  json_decode($datos);
		if($req->has('term')){
            $search = $req->term;
				foreach($destinos as $key=>$destino){
					$resultado = strpos(strtolower($destino->desDestino), strtolower($search));
					if($resultado === FALSE){
						unset($destinos[$key]);
					}
				}	
		}
		foreach ($destinos as $key => $row) {
   			 $aux[$key] = $row->prioridad;
		}

		array_multisort($aux, SORT_ASC, $destinos);

        return response()->json($destinos);
	}

	public function vuelos(){
		return view('pages.vuelos');
	}

	public function circuitos(){
		return view('pages.circuitos');
	}

	public function afiliados(){
		return view('pages.afiliados');
	}


	public function paquetesDestino(Request $req, $id, $id2){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
						  	'api-key' => 'IPu,Rv=]tdT2'
							 ]
		]);
		$paquete = $client->request('GET', 'http://info.dtpmundo.com/paquetes');
		$paqueteJson = json_decode($paquete->getBody());
		foreach($paqueteJson as $keys=>$paquetes){
			if($paquetes->destino != $id){
				unset($paqueteJson[$keys]);
			}	
		}
		$destinos = [];
		foreach($paqueteJson as $key=>$respuesta){
			$destinoDtpmundo = DestinoDtpmundo::where('id_destino_dtpmundo', '=',$respuesta->destino )
								->get(['desc_destino']);
			if(isset($destinoDtpmundo[0]['desc_destino'])){
				$posicion_coincidencia = strpos($destinoDtpmundo[0]['desc_destino'], "_");
				if ($posicion_coincidencia === false) {
					$nombre = explode("-", $destinoDtpmundo[0]['desc_destino']);
				}else{
					$nombre = explode("_", $destinoDtpmundo[0]['desc_destino']);
				}	
				$destinos[$respuesta->destino]['nombre'] = $nombre[0];
			}
		}
		return view('pages.paqueteDestino')->with(['paquetesDestinos'=>$paqueteJson, 'categorias'=>$destinos, 'agencia'=>$id2]);
	}


	public function paquetesDestinos(Request $req){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
						  	'api-key' => 'IPu,Rv=]tdT2'
							 ]
		]);
		$res = $client->request('GET', 'http://info.dtpmundo.com/paquetes');
		$resJson = json_decode($res->getBody());
		$destinos = [];
		foreach($resJson as $key=>$respuesta){
			$destinoDtpmundo = DestinoDtpmundo::where('id_destino_dtpmundo', '=',$respuesta->destino )
								->get(['desc_destino']);
			if(isset($destinoDtpmundo[0]['desc_destino'])){
				$posicion_coincidencia = strpos($destinoDtpmundo[0]['desc_destino'], "_");
				if ($posicion_coincidencia === false) {
					$nombre = explode("-", $destinoDtpmundo[0]['desc_destino']);
				}else{
					$nombre = explode("_", $destinoDtpmundo[0]['desc_destino']);
				}	
				$destinos[$respuesta->destino]['nombre'] = $nombre[0];
			}
		}
		return view('pages.paquetesDestinos')->with(['paquetesDestinos'=>$resJson, 'categorias'=>$destinos, 'agencia'=>Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia]);
	}

	public function paquetes(){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
						  	'api-key' => 'IPu,Rv=]tdT2'
							 ]
		]);
		$res = $client->request('GET', 'http://info.dtpmundo.com/paquetes/categorias');
		$resJson = json_decode($res->getBody());

		return view('pages.paquetes')->with('paquetesCategorias', $resJson);
	}

	public function subPaquetes(Request $req, $id, $id2){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
						  	'api-key' => 'IPu,Rv=]tdT2'
							 ]
		]);
		$categoria = $client->request('GET', 'http://info.dtpmundo.com/paquetes/categorias/'.$id);
		$categoriaJson = json_decode($categoria->getBody());
		return view('pages.sub_paquetes')->with(['categorias'=>$categoriaJson, 'agencia'=>$id2]);
	}	
	public function paqueteDetalle(Request $req, $id, $id2){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
						  	'api-key' => 'IPu,Rv=]tdT2'
							 ]
		]);
		$paquete = $client->request('GET', 'http://info.dtpmundo.com/paquetes/paquete/'.$id);
		$paqueteJson = json_decode($paquete->getBody());
		$res = $client->request('GET', 'http://info.dtpmundo.com/paquetes/categorias');
		$resJson = json_decode($res->getBody());		
		return view('pages.paqueteDetalle')->with(['paquete'=>$paqueteJson, 'categorias'=>$resJson, 'agencia'=>$id2]);
	}

	public function getPaquetesDestinos(Request $req){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
						  	'api-key' => 'IPu,Rv=]tdT2'
							 ]
		]);
		$res = $client->request('GET', 'http://info.dtpmundo.com/paquetes');
		$resJson = json_decode($res->getBody());
		 return response()->json($resJson);
	}	

	public function getPaquetesDestinosMenu(Request $req){
		$client = new Client([
			'headers' => [ 
							'Cache-Control' => 'no-cache',
						  	'api-key' => 'IPu,Rv=]tdT2'
							 ]
		]);
		$res = $client->request('GET', 'http://info.dtpmundo.com/paquetes');
		$resJson = json_decode($res->getBody());
		$respuestJason =[];
		$destinos = [];
		foreach($resJson as $key=>$respuesta){
			$destinoDtpmundo = DestinoDtpmundo::where('id_destino_dtpmundo', '=',$respuesta->destino )
								->get(['desc_destino']);
			if(isset($destinoDtpmundo[0]['desc_destino'])){
				$posicion_coincidencia = strpos($destinoDtpmundo[0]['desc_destino'], "_");
				if ($posicion_coincidencia === false) {
					$nombre = explode("-", $destinoDtpmundo[0]['desc_destino']);
				}else{
					$nombre = explode("_", $destinoDtpmundo[0]['desc_destino']);
				}	
				$destinos[$respuesta->destino]['nombre'] = $nombre[0];
			}
		}
		$respuestJason['menu'] = $destinos;
		$respuestJason['detalles'] = $resJson;
		return response()->json($respuestJason);
	}	

	/*
	Página de Terminos y Condiciones
	*/
	public function terminosCondiciones(){
		return view('pages.terminosCondiciones');
	}

	public function generarArchivoProveedor(){
		$proveedor = Infoprovider::all();
		return view('pages.mc.users.generarArchivoProveedor')->with(['proveedors'=>$proveedor]);
	}

	public function getCancelarProveedor(Request $req){
		$proveedor = Infoprovider::where('infoprovider_id', '=',$req->input('idProveedor'))->get();		
		if($proveedor[0]->habilitado == 1){
			$estado  = false;
			$respuesta = 'Se desactivo el Proveedor';
		}else{
			$estado  = true;
			$respuesta = 'Se activo el Proveedor';
		}
		DB::table('infoprovider')
					    ->where('infoprovider_id',$req->input('idProveedor'))
					    ->update([
								'habilitado'=>$estado,
					            ]);
		$arrayRsp= new \StdClass;
		$arrayRsp= $estado;
		return json_encode($arrayRsp);			    
	}	
	public function getActualizacion(Request $req){
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json',
						   'Cache-Control'=>'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
						   ]
		]);
		$iibReq = new \StdClass;
		$iibReq->token = Session::get('datos-loggeo')->token;
		try{
			$iibRsp = $client->post(Config::get('config.iibRecargar'),
						[
							'body' => json_encode($iibReq),
							]
				);
		}	
		catch(RequestException $e){
			//return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			//return view('pages.errorConexion');	
		}	
		$iibObjRsp = json_decode($iibRsp->getBody());
		return response()->json($iibObjRsp);

	}	

	public function getGenerarArchivo(Request $req){
		$proveedor = Infoprovider::all();
		$miArchivo = fopen("proveedoresHotel.php", "w") or die("No se puede abrir/crear el archivo!");
		$phps = "<?php  
				"; 
		foreach($proveedor as $key=>$provider){
			if($provider["habilitado"] == 1){
				$phps .= "\$providers['".$provider["infoprovider_id"]."'] = '".$provider["infoprovider_name"]."';
						";
			}
		}
		$phps .= "?>";
		fwrite($miArchivo, $phps);	
		fclose($miArchivo);
		return 'Se genero el archivo de configuración';
	}


	public function sincronizarSaldo(Request $req){
		return view('pages.mc.sincronizar');
	}	

	public function doSincronizarSaldo(Request $req){
		$client = new Client();
		$iibRsp = $client->get('http://mid.dtpmundo.com:9292/ControlDTP/resources/sincronizarSigaFactour');
		$iibObjRsp = json_decode($iibRsp->getBody());
		$respuestJason =[];
		$respuestJason['status'] = 'OK';
		return json_encode($respuestJason);
	}	


	public function sincronizarSaldoG(Request $req){
		return view('pages.mc.sincronizarG');
	}	

	public function doSincronizarSaldoG(Request $req){
		
		$idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
		if($idEmpresa == 1){
			$client = new Client();
			$iibRsp = $client->get('http://mid.dtpmundo.com:9292/ControlDTP/resources/sincronizarSigaGestur');
			$iibObjRsp = json_decode($iibRsp->getBody());
			$respuestJason =[];
			$respuestJason['status'] = 'OK';
			return json_encode($respuestJason);
		} else {
			return json_encode('');
		}
		
	}	

	public function doCotizacion(){
			$cotizacion = DB::select("select y.currency_id, y.currency_code, c.venta from cotizaciones c, currency y where c.id_currency = y.currency_id and fecha >= current_date and c.id = (select max(id) from cotizaciones x where y.currency_id = x.id_currency and id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.") order by 2 asc;"); 

			$indice_cotizcion = DB::select("SELECT 
			c_o.currency_code as currency_origen, 
			c_d.currency_code as currency_destino, 
			ci.indice, 
			ci.created_at
			FROM cotizaciones_indice ci
			JOIN  currency c_o ON c_o.currency_id = ci.id_moneda_origen
			JOIN  currency c_d ON c_d.currency_id = ci.id_moneda_destino
			INNER JOIN (
				SELECT id_moneda_origen, id_moneda_destino, MAX(created_at) AS last_loaded, id_empresa
				FROM cotizaciones_indice
				GROUP BY id_moneda_origen, id_moneda_destino,id_empresa
			) AS last_combinations ON ci.id_moneda_origen = last_combinations.id_moneda_origen
				AND ci.id_moneda_destino = last_combinations.id_moneda_destino
				AND ci.created_at = last_combinations.last_loaded
				AND ci.created_at >= current_date
				AND ci.id_empresa = ?",[ Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa ]);


			return json_encode(['cotizacion' => $cotizacion, 'indice_cotizacion' => $indice_cotizcion ]);		

	}

	public function actualizarProveedor(Request $req){
		
		$client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $jsonRequest = new \StdClass;
        $jsonRequest->token = Session::get('datos-loggeo')->token;

        try{
        	$iibRsp = $client->post('http://mid.dtpmundo.com:9292/DTPAPI/resources/configuracion/recargar_proveedores',
                                    ['body' => json_encode($jsonRequest)]
                                );

		}

        catch(RequestException $e){
           		return view('pages.mc.timeErrorConexion');
        } 
       catch(ClientException $e){
                return view('pages.mc.errorConexion');  
        }
 
        $jsonResponse = json_decode($iibRsp->getBody());

	    $resultado = DB::select("select id, logo , nombre from personas where proveedor_online = true and usuario_amadeus is not null and activo_dtpmundo = true");

        $miArchivo = fopen("proveedoresOnline.php", "w") or die("No se puede abrir/crear el archivo!");
        $textoArray = "";
        $textoArray = "<?php  
		    "; 
        foreach($resultado as $key=> $results){
        	$id = $results->id;
        	$logo = $results->logo;
        	$nombre = $results->nombre;
         	$textoArray .= "\$proveedor[".$id."] = '".$logo."_".$nombre."';
			";

        }
        $textoArray .= "?>";

        fwrite($miArchivo, $textoArray);
		fclose($miArchivo);

		return view('pages.mc.reportes.actualizarProveedor');
	}

	public function listarProveedor(Request $req){
		$client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $jsonRequest = new \StdClass;
        $jsonRequest->token = Session::get('datos-loggeo')->token;

       /* echo '<pre>';
        print_r(json_encode($jsonRequest));
*/

        try{
        	$iibRsp = $client->post('http://mid.dtpmundo.com:9292/DTPAPI/resources/configuracion/provedores_bkm',
                                    ['body' => json_encode($jsonRequest)]
                                );
		}

        catch(RequestException $e){
           		return view('pages.mc.timeErrorConexion');
        }
       catch(ClientException $e){
                return view('pages.mc.errorConexion');  
        }

        $jsonResponse = json_decode($iibRsp->getBody());

        $listado = [];

	       return view('pages.mc.reportes.listadoProveedor')->with('listados', $jsonResponse);
     }   



    public function VentaNetoSucursal(Request $req){
     	$dato = DB::select("  SELECT DISTINCT f.fecha_factura,f.id_empresa, *
								from vw_totales_fecha_factura f
								where f.dia_semana = 5 
								OR f.fecha_factura = (( SELECT date_trunc('month'::text, now())::date AS date_trunc))
								OR f.fecha_factura::date = CURRENT_DATE
								ORDER BY id_sucursal_empresa
							");
     	$datos = [];
     	foreach($dato as $key=>$value){
     		if($value->id_empresa == Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa){
     			if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 4){
     				if(Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia == $value->id_sucursal_empresa){
     					$datos[] = $value;
     				}
     			}else{
     				$datos[] = $value;
     			}
     		}
     	}

     return response()->json(['data'=>$datos]);

     }

    public function generarProducto(Request $req){
    		$totalProductos =Producto::where('id_proveedor', 7533)->get();
 
 				$client = new Client([
                                        'headers' => [ 
                                                    'Authorization' => 'Basic Y2tfYTUyOTFhMDY5MDBjOTQwZGEwMjVhNWEwOGUwNDBkYmY2YmFmOThiZTpjc18yZDA1Nzg0YjI1ZDMzNGQ2M2MyNTU5ZTY3MzNjZjc3MGM3OWRhNTY0',
                                                    'Content-Type' => 'application/json' 
                                                    ]
                                        ]);
	            $iibRsp = $client->get('https://beta.atukasa.com.py/wp-json/wc/v3/products/11396');
	                    $iibObjRsp = json_decode($iibRsp->getBody()); 

	                   	echo '<pre>';
						print_r($iibObjRsp);
            foreach($totalProductos as $baseProducto){
            	 echo '<pre>';
	            print_r($baseProducto->id_producto_woo);  
                if(!empty($baseProducto->id_producto_woo)){
                   
                    try{
	                    $iibRsp = $client->get('https://beta.atukasa.com.py/wp-json/wc/v3/products/'.$baseProducto->id_producto_woo);
	                    $iibObjRsp = json_decode($iibRsp->getBody()); 

	                   	echo '<pre>';
						print_r('manage_stock:'.$iibObjRsp->manage_stock);
						$prodArray = array();
						if($iibObjRsp->manage_stock !=""){
							$prodArray['update'][0]['id']= $baseProducto->id_producto_woo;
							if($baseProducto->stock == 0){
								$prodArray['update'][0]['stock_status']= "outofstock";
							}
							$iibRsp = $client->post('https://beta.atukasa.com.py/wp-json/wc/v3/products/batch',['body' => json_encode($prodArray)]);
			                $iibObjRsps = json_decode($iibRsp->getBody()); 
			                echo '<pre>';
							print_r($iibObjRsps);
						}else{
							$prodArray['update'][0]['id']= $baseProducto->stock;
							$prodArray['update'][0]['stock_quantity']= "outofstock";
							$iibRsp = $client->post('https://beta.atukasa.com.py/wp-json/wc/v3/products/batch',['body' => json_encode($prodArray)]);
			                $iibObjRsps = json_decode($iibRsp->getBody()); 

						} 
					} catch(\Exception $e){


					}	




                }
            }
		}

    public function homeDtp(Request $req){
		return view('pages.homeDtp');
	}

	/**
	 * Realizan los ajustes en los campos que tengan null en su forma de cobro cuenta contable
	 */
	public function ajusteErrorAsiento(){
		/**
		 * 1. Buscar formas de cobros sin su cuenta contable
		 * 2. Los que son depositable estirar de los parametros
		 * 3. Lo que no estirar de forma de cobro cliente
		 */

		 dd('No habilitado');
		 $errores = DB::select("Select 
		 vw.id as id_forma_cobro_recibo_detalle,
		 vw.id_moneda,
		 vw.id_tipo_pago,
		 vw.abreviatura,
		 e.denominacion as empresa,
		 vw.id_empresa
		 from vw_desarrollo_formas_cobro_recibo vw
		 join empresas e ON e.id = vw.id_empresa and e.activo = true
		 where vw.id_cta_ctb is null
		 order by e.denominacion,vw.abreviatura, vw.id_moneda");

		//  dd($errores);

		 foreach ($errores as $value) {

			$fp_detalle = FormaCobroReciboDetalle::findOrFail($value->id_forma_cobro_recibo_detalle);
			// dd($value, $fp_detalle);

			switch ($value->abreviatura) {
				case 'EFECT':

					$forma_cobro_data = CuentaCorrienteEmpresa::where('id_empresa', $value->id_empresa)
					->where('parametro', 'EFECTIVO_A_DEPOSITAR')
					->where('id_moneda',$value->id_moneda)
					->first();


					if(!$forma_cobro_data){
						// throw new \Exception('Falta configurar parametros de cobro EFECTIVO_A_DEPOSITAR '.$value->id_moneda.' '.$value->empresa);

						echo 'Falta configurar parametros de cobro EFECTIVO_A_DEPOSITAR '.$value->id_moneda.' '.$value->empresa;
						echo "<br>";
						//continue;
					}

					$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 
					$fp_detalle->save();

					break;
					
				case 'FACT':
					$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$value->id_tipo_pago)
					->where('activo',true)
					->where('id_moneda',$value->id_moneda)
					->first();

					if(!$forma_cobro_data){
						// throw new \Exception('Falta configurar parametros de cobro FACT '.$value->id_moneda.' '.$value->empresa);

						echo 'Falta configurar parametros de cobro FACT '.$value->id_moneda.' '.$value->empresa;
						echo "<br>";
						//continue;
					}

					$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable;
					$fp_detalle->save();

					break;

				case 'CHEQ':

						$forma_cobro_data = CuentaCorrienteEmpresa::where('id_empresa', $value->id_empresa)
						->where('parametro', 'CHEQUE_A_DEPOSITAR')
						->where('id_moneda', $value->id_moneda)
						->first();

						if(!$forma_cobro_data){
							// throw new \Exception('Falta configurar parametros de cobro CHEQUE_A_DEPOSITAR '.$value->id_moneda.' '.$value->empresa);

							echo 'Falta configurar parametros de cobro CHEQUE_A_DEPOSITAR '.$value->id_moneda.' '.$value->empresa;
							echo "<br>";

							//continue;
						}

						$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable;
						$fp_detalle->save();

				
					break;						
					
					
				case 'CHEQ_DIF':


						$forma_cobro_data = CuentaCorrienteEmpresa::where('id_empresa', $value->id_empresa)
						->where('parametro', 'CHEQUE_DIFERIDO_A_DEPOSITAR')
						->where('id_moneda', $value->id_moneda)
						->first();

						if(!$forma_cobro_data){
							// throw new \Exception('Falta configurar parametros de cobro CHEQUE_DIFERIDO_A_DEPOSITAR '.$value->id_moneda.' '.$value->empresa);

							echo 'Falta configurar parametros de cobro CHEQUE_DIFERIDO_A_DEPOSITAR '.$value->id_moneda.' '.$value->empresa;
							echo "<br>";
						//	continue;
						}

						$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable;
						$fp_detalle->save();




				
					break;

				case 'DEP':
					// throw new \Exception('Error DEP '.$value->id_moneda.' '.$value->empresa);

					echo 'Error DEP '.$value->id_moneda.' '.$value->empresa;
					echo "<br>";
				//	continue;

				
					break;

				case 'TRANSF':
						// throw new \Exception('Error transferencia '.$value->id_moneda.' '.$value->empresa);
						echo 'Error transferencia '.$value->id_moneda.' '.$value->empresa;
						echo "<br>";
					//	continue;
				
					break;	

				case 'TC':
					$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$value->id_tipo_pago)
					->where('activo',true)
					->where('id_moneda',$value->id_moneda)
					->first();

					if(!$forma_cobro_data){
						// throw new \Exception('Falta configurar parametros de cobro TC '.$value->id_moneda.' '.$value->empresa);
						echo 'Falta configurar parametros de cobro TC '.$value->id_moneda.' '.$value->empresa;
						echo "<br>";
					//	continue;
					}

					$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 
					$fp_detalle->save();

				
					break;

				case 'TD':
					$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$value->id_tipo_pago)
					->where('activo',true)
					->where('id_moneda',$value->id_moneda)
					->first();

					if(!$forma_cobro_data){
						// throw new \Exception('Falta configurar parametros de cobro TD '.$value->id_moneda.' '.$value->empresa);

						echo 'Falta configurar parametros de cobro TD '.$value->id_moneda.' '.$value->empresa;
						echo "<br>";
					//	continue;
					}
					
					
					$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 
					$fp_detalle->save();

				
					break;
					
				case 'VO':
					$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$value->id_tipo_pago)
					->where('activo',true)
					->where('id_moneda',$value->id_moneda)
					->first();

					if(!$forma_cobro_data){
						// throw new \Exception('Falta configurar parametros de cobro VO '.$value->id_moneda.' '.$value->empresa);

						echo 'Falta configurar parametros de cobro VO '.$value->id_moneda.' '.$value->empresa;
						echo "<br>";
					//	continue;
					}

					$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 
					$fp_detalle->save();

				
					break;

				case 'RETEN':
					$forma_cobro_data = CtaCtteFormaCobro::where('id_forma_cobro',$value->id_tipo_pago)
					->where('activo',true)
					->where('id_moneda',$value->id_moneda)
					->first();

					if(!$forma_cobro_data){
						// throw new \Exception('Falta configurar parametros de cobro RETEN '.$value->id_moneda.' '.$value->empresa);

						echo 'Falta configurar parametros de cobro RETEN '.$value->id_moneda.' '.$value->empresa;
						echo "<br>";
						// continue;
					}

					$fp_detalle->id_cta_ctb     = $forma_cobro_data->id_cuenta_contable; 
					$fp_detalle->save();
				
					break;

		

			}

			
			// dd('Finalizo el proceso');

		 }
	}


}//class
