<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use App\DestinoDtpmundo;
use App\Currency;
use App\Aerolinea;
use App\EstadoFactour;
use App\Grupo;
use App\Producto;
use App\Persona;
use App\Factura;
use App\FacturaDetalle;
use DB;
use Response;
use Image; 

class MigracionVentaController extends Controller
{

    private function getIdEmpresa()
      {
    
       return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
      }

    public function indexReporteCompra()
    {
      // 
    }

    public function createMigracionVentas()
    {      
      $ventasMigradas = DB::select("
        SELECT distinct f.nro_factura, tf.denominacion, p.nombre
        FROM facturas f 
        JOIN tipo_factura tf ON tf.id = f.id_tipo_factura
        JOIN personas p ON p.id = f.cliente_id
        WHERE f.migrado = false
        AND f.id_empresa = ". Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

      // dd($ventasMigradas);

      return view('pages.mc.reportes.migracionVentas', compact('ventasMigradas')); 
    }

    public function getMigracionVenta(Request $request)
    {
       
       $query = "SELECT distinct f.nro_factura, tf.denominacion, p.nombre
        FROM facturas f 
        JOIN tipo_factura tf ON tf.id = f.id_tipo_factura
        JOIN personas p ON p.id = f.cliente_id ";
 
      if(!empty($request->input('periodo')))
        {
          $fechaTrim = trim($request->input('periodo'));
          $periodo = explode('-', trim($fechaTrim));
          $desde = explode("/", trim($periodo[0]));
          $hasta = explode("/", trim($periodo[1]));
          $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
          $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
          $query .= "WHERE f.fecha_hora_facturacion BETWEEN '".$fecha_desde."' AND '".$fecha_hasta."' AND f.id_empresa = ". Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
        }else{
          $query .= "WHERE f.id_empresa = ". Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
        }

        if(!empty($request->input('operacion')))
        {
         $query .= "AND f.migrado = ".$request->input('operacion');
        }
     
      $facturas = DB::select($query);

      return response()->json($facturas);
    }


    public function edit($id)
    {
      // $ticket = Ticket::findOrFail($id);
      // $estados = EstadoFactour::where('id_tipo_estado', '8')->get();  
        
      // return view('pages.mc.tickets.edit', compact('ticket', 'estados'));
    }

    public function update(Request $request, $id)
    {
        // $ticket = Ticket::find($id);

        // try{
        //     $ticket->update([
        //     $ticket->pnr = $request->input('PNR'),
        //     $ticket->id_estado = $request->input('estado'),
        //     $ticket->numero_amadeus = $request->input('amadeus'),
        //   ]);

        //   flash('¡Se ha actualizado exitosamente!')->success();
        //   return redirect()->route('factour.indexTicket');

        // } catch(\Exception $e){
        //   flash('Ha ocurrido un error, inténtelo nuevamente')->error();
        //   return redirect()->back();

    }      
        
    
    public function hechaukaLv(Request $request)
    {
      $proveedores =DB::select("
      SELECT p.id,p.nombre, p.apellido, p.documento_identidad, p.dv, tp.denominacion, p.activo,
      concat(p.documento_identidad ||' - ',p.nombre||' ',p.apellido,' ','- '|| tp.denominacion) as full_data
      FROM personas p
      JOIN tipo_persona tp on tp.id = p.id_tipo_persona
      WHERE p.id_tipo_persona IN (SELECT id FROM tipo_persona WHERE puede_facturar = true)
      AND p.activo = true
      AND id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);


      return view('pages.mc.contabilidad.hechaukaLv', compact('proveedores'));
    }  

	    public function cabecerahechaukaLv(Request $request)
    {
      /*echo '<pre>';
      print_r($request->input('dataAnho')."".$request->input('dataMes'));*/
      $cabecerahechauka = DB::table('vw_hechauka_lv_cabecera');
      $cabecerahechauka = $cabecerahechauka->where('id_empresa',$this->getIdEmpresa());
      if($request->input('dataMes') != ''){
         if($request->input('dataAnho')){
            $cabecerahechauka = $cabecerahechauka->where('periodo',$request->input('dataAnho')."".$request->input('dataMes'));
         }
      }
      $cabecerahechauka = $cabecerahechauka->get();
      return response()->json($cabecerahechauka);
    }  

      public function detallehechaukaLv(Request $request)
    {
     /* echo '<pre>';
      print_r($request->all()); */
      $cabecerahechauka = DB::table('vw_hechauka_lv_detalle');
      $cabecerahechauka = $cabecerahechauka->where('id_empresa',$this->getIdEmpresa());
      if($request->input('id_proveedor') != ''){
          $cabecerahechauka = $cabecerahechauka->where('id_proveedor',$request->input('id_proveedor'));
      }
      if($request->input('mes') != ''){
         if($request->input('anho')){
            $cabecerahechauka = $cabecerahechauka->where('fecha_documento','like', '%' .trim($request->input('mes')."/".$request->input('anho')).'%');
         }
      }      
      $cabecerahechauka = $cabecerahechauka->get();
      return response()->json(['data'=>$cabecerahechauka]);
    }  
}
