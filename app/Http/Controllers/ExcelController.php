<?php 

namespace App\Http\Controllers;
 
use App\Http\Requests;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use App\Proforma;
use App\State;
use App\Reserva;
use App\Usuario;
use App\Agencias;
use App\Infoprovider;
use App\ExtractoProveedor;
use App\ExtractoDetalles;
use App\DestinoDtpmundo;
use App\Currency;
use App\Aerolinea;
use App\EstadoFactour;
use App\Grupo;
use App\Producto;
use App\Persona;
use App\Factura;
use App\FacturaDetalle;
use App\ProformasDetalle;
use App\Timbrado;
use App\NotaCredito;
use App\Divisas;
use App\TipoTicket;
use App\VentasRapidasCabecera;
use App\PlanCuenta;
use App\ComercioPersona;
use App\CierreRecibo;
use App\BancoDetalle;
use App\Recibo;
use App\Anticipo;
use App\SucursalEmpresa;
use App\FormaCobroCliente;
use Session;
use Exception;
use DB;
use DateTime;
use Carbon\Carbon;
use App\Exports\PagoProveedorExport;
use App\Exports\ReporteOpExport;
use App\Exports\ComprasExport;
use App\Exports\HechaukaCompraExport;
use App\Exports\HechaukaVentasExport;
use App\Exports\CostoVentaPendientes;
use App\Exports\MigracionCompraExport;
use App\Exports\LibrosComprasFondoFijoExport;
use App\Exports\ReporteCorteProveedoresExport;
use App\Exports\MigracionAsientosExport;
use App\Exports\MigracionVentasExport;
use App\Exports\ReporteFacturasExport;
use App\Exports\ReporteTicketsPendientesExport;
use App\Exports\ReporteDetallesPagoProveedorExport;
use App\Exports\ReporteTicketsExport;
use App\Exports\ReporteRecibosExport;
use App\Exports\ReporteRecibosComprobanteExport;
use App\Exports\ReporteFacturasPendientesExport;
use App\Exports\ReporteBSPCobranzaExport;
use App\Exports\GestionCobrosExport;
use App\Exports\ReporteBSPExport;
use App\Exports\ListadoVentasRapidasExport;
use App\Exports\ListadoNotaDeCreditoExport;
use App\Exports\ListadoFacturasExport;
use App\Exports\ListadoProformasExport;
use App\Exports\ListaVendedoresExport;
use App\Exports\ReporteClienteTipoFacturacionExport;
use App\Exports\ReporteClienteRiesgoExport;
use App\Exports\ReporteTicketsPendientes_2Export;
use App\Exports\ReporteVendedorIncentivoDetalleExport;
use App\Exports\ReporteClienteFacturaExport;
use App\Exports\ReporteLibroVentasExport;
use App\Exports\ReporteMovimientoCuentaExport;
use App\Exports\ReporteResumenCuentaExport;
use App\Exports\ReporteMasivoPersonasExport;
use App\Exports\ReporteListadoDetalleCierreCajasExport;
use App\Exports\ReporteListadoFacturasNCExport;
use App\Exports\ReporteListadoPlanCuentasExport;
use App\Exports\ReporteListadoResumenCobrosExport;
use App\Exports\ReporteListadoAgenciasExport;
use App\Exports\ReporteMetaAgenciasExport;
use App\Exports\ReporteVentaRapidaExport;
use App\Exports\ReporteVentaPrestadorExport;
use App\Exports\ReporteHistoricoProformasExport;
use App\Exports\ReporteLiquidacionesExport;
use App\Exports\ReporteAsientoContableExport;
use App\Exports\AnticiposProveedorExport;
use App\Exports\AnticiposClienteExport;
use App\Exports\ReporteProductosExport;
use App\Exports\ReportePersonasExport;
use App\Exports\ReporteAnticiposExport;
use App\Exports\ReporteAnticiposV2Export;
use App\Exports\ReporteMayorCuentasExport;
use App\Exports\ReporteResumenCuentaProveedorExport;
use App\Exports\ReporteResumenCuentaClienteExport;
use App\Exports\ReporteAnticipoProveedorExport;
use App\Exports\ReporteFormaCobroExport;
use App\Exports\ReportePlanCuentasExport;
use App\Exports\ReporteDetalleV1Export;
use App\Exports\ReporteDetalleV2Export;
use App\Exports\ReporteDetalleGenericoExport;



class ExcelController extends Controller {


    /**
     * Recibe una fecha 2017-05-02 y pasa a 02/05/2017 
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaSalida($date){
        if( $date != ''){

        $date = explode('-', $date);
            $fecha = $date[2]."/".$date[1]."/".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }


    /**
     * Recibe una fecha 02/05/2017  y pasa a 2017-05-02
     * @param  string $date fecha
     * @return string       fecha
     */
    private function formatoFechaEntrada($date){
        if( $date != ''){

        $date = explode('/', $date);
            $fecha = $date[2]."-".$date[1]."-".$date[0];
            return $fecha;
        } else {
            return null;
        }
    }



        private function formatMoney($num,$f)
      {


    
    if($f != 111){
    return number_format($num, 2, ",", ".");  
    } 
    return number_format($num,0,",",".");

  }

public function generarExcelPagoProv(Request $req)
{
///////////////////////////////////////ANTICIPO//////////////////////////////////////////////
$libroCompra = DB::table('vw_libro_compra_anticipos');
$libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
$libroCompra = $libroCompra->where('id_tipo_documento',20);
if ($req->input('extranjero') == 1) {
    $libroCompra = $libroCompra->where('id_pais', '<>', 1455);// Filtro para Extranjeros (SI)
  } else if ($req->input('extranjero') == 1455) {
      $libroCompra = $libroCompra->where('id_pais', 1455); // Filtro para No Extranjeros (NO)
  }
if($req->input('idProveedor'))
      $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
  if($req->input('idProforma'))
      $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
  if($req->input('codigoConfirmacion'))
      $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
  if($req->input('nroFactura'))
      $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
  if($req->input('idMoneda'))
      $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
  if($req->input('tieneFechaProveedor'))
      $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
  if($req->input('reprice'))
    $libroCompra = $libroCompra->where('reprice_pago_pro',$req->input('reprice'));
  if($req->input('proveedor_desde_hasta:p_date')){
      $periodo =  explode(' - ', $req->input('proveedor_desde_hasta:p_date'));
      $desde = explode("/", trim($periodo[0]));
      $hasta = explode("/", trim($periodo[1]));
      $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
      $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
      $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
    }
  if($req->input('gasto_desde_hasta:p_date')){
      $fecha = explode(' ', $req->input('gasto_desde_hasta:p_date'));
      $periodo =  explode(' - ', $req->input('gasto_desde_hasta:p_date'));
      $desde = explode("/", trim($periodo[0]));
      $hasta = explode("/", trim($periodo[1]));
      $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
      $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';

      $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($fecha_desde,$fecha_hasta));   
    }
  if($req->input('pendiente')){
    $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
  }   
  if($req->input('fecha_pago')){
    $desde     = $req->input('fecha_pago').' 00:00:00';
    $hasta     = $req->input('fecha_pago').' 23:59:59';

    $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($desde,$hasta));
  }    
    
  if($req->input('periodo_desde_hasta')){
    $periodo =  explode(' - ', $req->input('periodo_desde_hasta'));
    $desde = explode("/", trim($periodo[0]));
    $hasta = explode("/", trim($periodo[1]));
    $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
    $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
    $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
  } 
  $libroCompra = $libroCompra->where('saldo','>',0);
  $libroCompra = $libroCompra->get();
  $resultado = $libroCompra;
  $contador = count($resultado);
  ///////////////////////////////////////NOTA DE CREDITO//////////////////////////////////////////////
  $libroCompra = DB::table('vw_libro_compra_anticipos');
  $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
  $libroCompra = $libroCompra->whereIn('id_tipo_documento',array(2,32));
  if ($req->input('extranjero') == 1) {
    $libroCompra = $libroCompra->where('id_pais', '<>', 1455);// Filtro para Extranjeros (SI)
  } else if ($req->input('extranjero') == 1455) {
      $libroCompra = $libroCompra->where('id_pais', 1455); // Filtro para No Extranjeros (NO)
  }
    if($req->input('idProveedor'))
        $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
    if($req->input('idProforma'))
        $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
    if($req->input('codigoConfirmacion'))
        $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
    if($req->input('nroFactura'))
        $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
    if($req->input('idMoneda'))
        $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
    if($req->input('tieneFechaProveedor'))
        $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
        if($req->input('reprice'))
        $libroCompra = $libroCompra->where('reprice_pago_pro',$req->input('reprice'));
    if($req->input('pendiente')){
      $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
    }   
    if($req->input('proveedor_desde_hasta:p_date')){
        $periodo =  explode(' - ', $req->input('proveedor_desde_hasta:p_date'));
        $desde = explode("/", trim($periodo[0]));
        $hasta = explode("/", trim($periodo[1]));
        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
        $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
      }
    if($req->input('gasto_desde_hasta:p_date')){
        $fecha = explode(' ', $req->input('gasto_desde_hasta:p_date'));
        $periodo =  explode(' - ', $req->input('gasto_desde_hasta:p_date'));
        $desde = explode("/", trim($periodo[0]));
        $hasta = explode("/", trim($periodo[1]));
        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
  
        $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($fecha_desde,$fecha_hasta));   
      }
      if($req->input('pendiente')){
      $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
    }      
    if($req->input('fecha_pago')){
      $libroCompra = $libroCompra->where('fecha_de_gasto',$req->input('fecha_pago')); 
    }    
      
    if($req->input('periodo_desde_hasta')){
      $periodo =  explode(' - ', $req->input('periodo_desde_hasta'));
      $desde = explode("/", trim($periodo[0]));
      $hasta = explode("/", trim($periodo[1]));
      $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
      $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
      $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
    }   
    $libroCompra = $libroCompra->where('saldo','>',0);
    $libroCompra = $libroCompra->get();
    foreach($libroCompra as $key=>$lc){
      $resultado[$contador] = $lc;
      $contador++;
    }
  ///////////////////////////////////////FACTURA CREDITO//////////////////////////////////////////////
  $libroCompra = DB::table('vw_libro_compra_anticipos');
  $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
  $libroCompra = $libroCompra->whereIn('id_tipo_documento',array(1,5,33));
  if ($req->input('extranjero') == 1) {
    $libroCompra = $libroCompra->where('id_pais', '<>', 1455);// Filtro para Extranjeros (SI)
  } else if ($req->input('extranjero') == 1455) {
      $libroCompra = $libroCompra->where('id_pais', 1455); // Filtro para No Extranjeros (NO)
  }
    if($req->input('idProveedor'))
        $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
    if($req->input('idProforma'))
        $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
    if($req->input('codigoConfirmacion'))
        $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
    if($req->input('nroFactura'))
        $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
    if($req->input('idMoneda'))
        $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
    if($req->input('tieneFechaProveedor'))
        $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
        if($req->input('reprice'))
        $libroCompra = $libroCompra->where('reprice_pago_pro',$req->input('reprice'));
    if($req->input('pendiente')){
      $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
    }      
    if($req->input('checkin_desde_hasta:p_date')){
        $periodo =  explode(' - ', $req->input('checkin_desde_hasta:p_date'));
        $desde = explode("/", trim($periodo[0]));
        $hasta = explode("/", trim($periodo[1]));
        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
  
        $libroCompra = $libroCompra->whereBetween('check_in',array($fecha_desde,$fecha_hasta));   
      }
    if($req->input('proveedor_desde_hasta:p_date')){
        $periodo =  explode(' - ', $req->input('proveedor_desde_hasta:p_date'));
        $desde = explode("/", trim($periodo[0]));
        $hasta = explode("/", trim($periodo[1]));
        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
        $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
      }
    if($req->input('gasto_desde_hasta:p_date')){
        $fecha = explode(' ', $req->input('gasto_desde_hasta:p_date'));
        $periodo =  explode(' - ', $req->input('gasto_desde_hasta:p_date'));
        $desde = explode("/", trim($periodo[0]));
        $hasta = explode("/", trim($periodo[1]));
        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
  
        $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($fecha_desde,$fecha_hasta));   
      }
      if($req->input('fecha_pago')){
      $libroCompra = $libroCompra->where('fecha_de_gasto',$req->input('fecha_pago')); 
    }    
      
    if($req->input('periodo_desde_hasta')){
      $periodo =  explode(' - ', $req->input('periodo_desde_hasta'));
      $desde = explode("/", trim($periodo[0]));
      $hasta = explode("/", trim($periodo[1]));
      $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
      $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
      $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
    }   
    $libroCompra = $libroCompra->where('saldo','>',0);
    $libroCompra = $libroCompra->get();
    foreach($libroCompra as $key=>$lc){
      $resultado[$contador] = $lc;
      $contador++;
    }
  //////////////////////////////////FACTURA//////////////////////////////////////
  $libroCompra = DB::table('vw_libro_compra_anticipos');
  $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
  $libroCompra = $libroCompra->where('id_tipo_documento',22);
  if ($req->input('extranjero') == 1) {
    $libroCompra = $libroCompra->where('id_pais', '<>', 1455);// Filtro para Extranjeros (SI)
  } else if ($req->input('extranjero') == 1455) {
      $libroCompra = $libroCompra->where('id_pais', 1455); // Filtro para No Extranjeros (NO)
  }

    if($req->input('idProveedor'))
        $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
    if($req->input('idProforma'))
        $libroCompra = $libroCompra->where('id_proforma',$req->input('idProforma'));
    if($req->input('codigoConfirmacion'))
        $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
    if($req->input('nroFactura'))
        $libroCompra = $libroCompra->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
    if($req->input('idMoneda'))
        $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));
    if($req->input('tieneFechaProveedor'))
        $libroCompra = $libroCompra->where('pagado_proveedor_txt',$req->input('tieneFechaProveedor'));
        if($req->input('reprice'))
        $libroCompra = $libroCompra->where('reprice_pago_pro',$req->input('reprice'));
    if($req->input('pendiente')){
      $libroCompra = $libroCompra->where('pendiente',$req->input('pendiente')); 
    }    
    if($req->input('checkin_desde_hasta:p_date')){
        $periodo =  explode(' - ', $req->input('checkin_desde_hasta:p_date'));
        $desde = explode("/", trim($periodo[0]));
        $hasta = explode("/", trim($periodo[1]));
        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
  
        $libroCompra = $libroCompra->whereBetween('check_in',array($fecha_desde,$fecha_hasta));   
      }
    if($req->input('proveedor_desde_hasta:p_date')){
        $periodo =  explode(' - ', $req->input('proveedor_desde_hasta:p_date'));
        $desde = explode("/", trim($periodo[0]));
        $hasta = explode("/", trim($periodo[1]));
        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
        $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
      }
    if($req->input('gasto_desde_hasta:p_date')){
        $fecha = explode(' ', $req->input('gasto_desde_hasta:p_date'));
        $periodo =  explode(' - ', $req->input('gasto_desde_hasta:p_date'));
        $desde = explode("/", trim($periodo[0]));
        $hasta = explode("/", trim($periodo[1]));
        $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
        $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
  
        $libroCompra = $libroCompra->whereBetween('fecha_de_gasto',array($fecha_desde,$fecha_hasta));   
      }
      if($req->input('fecha_pago')){
        $libroCompra = $libroCompra->where('fecha_de_gasto',$req->input('fecha_pago')); 
      }    
        
    if($req->input('periodo_desde_hasta')){
      $periodo =  explode(' - ', $req->input('periodo_desde_hasta'));
      $desde = explode("/", trim($periodo[0]));
      $hasta = explode("/", trim($periodo[1]));
      $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
      $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
      $libroCompra = $libroCompra->whereBetween('fecha_pago_proveedor',array($fecha_desde,$fecha_hasta));   
    }   
    $libroCompra = $libroCompra->where('saldo','>',0);
    $libroCompra = $libroCompra->get(); 
    foreach($libroCompra as $key=>$lc){
      $resultado[$contador] = $lc;
      $contador++;
    }

    $cuerpo = array();

    foreach($resultado as $key=>$lista)
    {
        $pais = ($lista->id_pais === 1455) ? "No es extranjero" : "Extranjero";
           array_push($cuerpo, 
                            array($lista->proveedor_n,
                                  $lista->fecha_proveedor_format,
                                  date("d/m/Y",strtotime($lista->fecha_de_gasto)),
                                  $lista->nro_factura,
                                  $lista->fecha_in_format,
                                  $lista->tipo_doc_n,
                                  $lista->pagado_proveedor_txt,
                                  $lista->pasajero_n,
                                  $lista->currency_code,
                                  $lista->saldo,
                                  $lista->cod_confirmacion,
                                  $lista->denominacion_producto,
                                  $lista->importe,
                                  $lista->saldo_documento,
                                  $lista->id_proforma,
                                  $lista->fecha_facturacion_format,
                                  $lista->vendedor_n,
                                  $lista->origen,
                                  $pais // Reemplazamos el valor de $lista->id_pais por el valor de $pais según la condición
                                )
                  );
    }    


    $export = new PagoProveedorExport($cuerpo, $req);

    return Excel::download($export, 'Migración Pago Proveedor-' . date('d-m-Y') . '.xls');

}


        public function generarExcelHechaukaLc(Request $request)
        {
              $usuarios = Persona::all();
              $cabecerahechauka = DB::table('vw_hechauka_lc_cabecera');
              $cabecerahechauka = $cabecerahechauka->where('id_empresa',$this->getIdEmpresa());
              $cabecerahechauka = $cabecerahechauka->get();

              $cabeceras = array();
              foreach ($cabecerahechauka as $dato) {
                array_push($cabeceras, array($dato->tipo_registro,trim($request->input('anho')."".$request->input('mes')),$dato->tipo_reporte,$dato->cod_obligacion,$dato->cod_formulario,$dato->ruc_agente,$dato->dv_agente,strtoupper($dato->nombre_agente),$dato->ruc_representante,$dato->dv_representante,strtoupper($dato->representante_legal),$dato->version,$dato->cant_compras,number_format($dato->monto_reportado,0,",",""),$dato->exportador));
              }

              $detallehechauka = DB::table('vw_hechauka_lc_detalle');
              $detallehechauka = $detallehechauka->where('id_empresa',$this->getIdEmpresa());
              if($request->input('id_proveedor') != ''){
                  $cabecerahechauka = $detallehechauka->where('id_proveedor',$request->input('id_proveedor'));
              }
              if($request->input('mes') != ''){
                 if($request->input('anho')){
                    $detallehechauka = $detallehechauka->where('fecha_documento','like', '%' .trim($request->input('mes')."/".$request->input('anho')).'%');
                 }
              }      
              $detallehechauka = $detallehechauka->get();
               $cuerpo = array();
            foreach ($detallehechauka as $datos) {
                array_push($cuerpo, 
                            array($datos->tipo_registro,
                                  $datos->ruc_proveedor,
                                  $datos->dv_proveedor,
                                  strtoupper($datos->proveedor),
                                  $datos->nro_timbrado,
                                  $datos->tipo_documento,
                                  $datos->nro_documento,
                                  $datos->fecha_documento,
                                  number_format($datos->monto_compra_10,0,",",""),
                                  number_format($datos->iva_credito_10,0,",",""),
                                  number_format($datos->monto_compra_5,0,",",""),
                                  number_format($datos->iva_credito_5,0,",",""),
                                  number_format($datos->monto_compra_exenta,0,",",""),
                                  0,
                                  $datos->condicion_compra,
                                  $datos->cant_cuota
                                )
                  );


             }

             
             return Excel::download(new HechaukaCompraExport($cuerpo, $cabeceras), 'Hechauka Compras-' . date('d-m-Y') . '.xls');

        }



        public function generarExcelHechaukaLv(Request $request)
        {
              $usuarios = Persona::all();
              $cabecerahechauka = DB::table('vw_hechauka_lv_cabecera');
              $cabecerahechauka = $cabecerahechauka->where('id_empresa',$this->getIdEmpresa());
              if($request->input('anho') != ''){
                 if($request->input('mes')){
                    $cabecerahechauka = $cabecerahechauka->where('periodo',$request->input('anho')."".$request->input('mes'));
                 }
              }
              $cabecerahechauka = $cabecerahechauka->get();

              $cabeceras = array();
              foreach ($cabecerahechauka as $dato) {
                array_push($cabeceras, array($dato->tipo_registro,$dato->periodo,$dato->tipo_reporte,$dato->cod_obligacion,$dato->cod_formulario,$dato->ruc_agente,$dato->dv_agente,strtoupper($dato->nombre_agente),$dato->ruc_representante,$dato->dv_representante,strtoupper($dato->representante_legal),$dato->version,$dato->cant_registros,number_format($dato->monto_reportado,0,",","")));
              }
              
              $detallehechauka = DB::table('vw_hechauka_lv_detalle');
              $detallehechauka = $detallehechauka->where('id_empresa',$this->getIdEmpresa());
              if($request->input('id_proveedor') != ''){
                  $cabecerahechauka = $detallehechauka->where('id_proveedor',$request->input('id_proveedor'));
              }
              if($request->input('mes') != ''){
                 if($request->input('anho')){
                    $detallehechauka = $detallehechauka->where('fecha_documento','like', '%' .trim($request->input('mes')."/".$request->input('anho')).'%');
                 }
              } 
              $detallehechauka = $detallehechauka->orderBy('fecha_documento', 'asc');     
              $detallehechauka = $detallehechauka->get();
              $cuerpo = array();
              foreach ($detallehechauka as $datos) {
                array_push($cuerpo, 
                            array(
                                  $datos->tipo_registro,
                                  $datos->ruc_cliente,
                                  $datos->dv_cliente,
                                  strtoupper($datos->cliente),
                                  $datos->tipo_documento,
                                  $datos->nro_documento,
                                  $datos->fecha_documento,
                                  number_format($datos->monto_venta_10,0,",",""),
                                  number_format($datos->iva_debito_10,0,",",""),
                                  number_format($datos->monto_venta_5,0,",",""),
                                  number_format($datos->iva_debito_5,0,",",""),
                                  number_format($datos->monto_venta_exenta,0,",",""),
                                  number_format($datos->monto_ingreso,0,",",""),
                                  $datos->condicion_venta,
                                  $datos->cant_cuota,
                                  $datos->nro_timbrado
                                )
                  );


             }
            
             return Excel::download(new HechaukaVentasExport($cuerpo, $cabeceras), 'Hechauka Ventas-' . date('d-m-Y') . '.xls');

        }

public function generarExcelCompra(Request $request)
{
    Session::put('req',$request); 

    $librosCompras = DB::table('v_migracion_compras');
    $librosCompras = $librosCompras->where('id_empresa',$this->getIdEmpresa());

    if(!empty($request->input('periodo')))
    {
      $fechaTrim = trim($request->input('periodo'));
      $periodo = explode('-', trim($fechaTrim));
      $desde = explode("/", trim($periodo[0]));
      $hasta = explode("/", trim($periodo[1]));
      $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
      $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
      $librosCompras = $librosCompras->whereBetween('fecha_hora_facturacion', [$fecha_desde, $fecha_hasta]);

    }

    if(!empty($request->input('operacion')))
    {
      $librosCompras = $librosCompras->where('migrado_detalle','=', $request->input('operacion'));
    }

    $librosCompras = $librosCompras->get();
  // $item = $librosCompras->where('com_numero', '0010050004870-2');
   // 0010050005142-1
    //dd($item);
    //dd($librosCompras->slice(0, 10));
   //0020020001009-1

    $listado = array();

    foreach ($librosCompras as $key => $lc) 
    {
       $lista = new \StdClass;
       $lista->id = $lc->id;
       $lista->id_detalle = $lc->id_detalle;
       $lista->migrado_detalle = $lc->migrado_detalle;
       $lista->migrado = $lc->migrado;
       $lista->com_numero = $lc->com_numero;
       $lista->generar = $lc->generar;
       $lista->cta_iva = $lc->cta_iva;
       $lista->cta_caja = $lc->cta_caja;
       $lista->form_pag = $lc->form_pag;
       $lista->com_imputa = $lc->com_imputa;
       $lista->com_sucurs = $lc->com_sucurs; 
       $lista->com_centro = $lc->com_centro;
       $lista->com_provee = $lc->com_provee;
       $lista->com_cuenta = $lc->com_cuenta;
       $lista->com_prvnom = $lc->com_prvnom;
       $lista->com_tipofa = $lc->com_tipofa;
       $lista->com_fecha = $lc->com_fecha; 
       $lista->com_totfac =  $lc->com_totfac;
       $lista->com_exenta =  $lc->com_exenta;
       if($lc->com_tipofa == 'Nota Credito'){
            $lista->com_gravad = 0;
        }else{
		if($lc->com_gra05 != 0 ){
            if($lc->com_gravad == 0){
                $lista->com_gravad = 0;
            }else{
                $lista->com_gravad = $lc->com_gravad ;
            }
		}else{
            $lista->com_gravad = floatval($lc->com_totfac)-(floatval($lc->com_exenta)+floatval($lc->com_iva)) ;
		}
       }    
       $lista->com_iva = $lc->com_iva;
       $lista->com_import = $lc->com_import;
       $lista->com_aux = $lc->com_aux;
       $lista->com_con = $lc->com_con;
       $lista->com_cuota = $lc->com_cuota;
       $lista->com_fecven = $lc->com_fecven; 
       $lista->com_ordenp = $lc->com_ordenp;
       $lista->cant_dias = $lc->cant_dias;
       $lista->concepto = $lc->concepto;
       $lista->moneda = $lc->moneda;
       $lista->cambio = $lc->cambio;
       $lista->valor = $lc->valor;
       $lista->origen = $lc->origen;
      
        $lista->exen_dolar = $lc->exen_dolar;
       //$lista->exen_dolar = $lc->exen_dolar;
       $lista->com_disexe = $lc->com_disexe;
       $lista->com_disgra = $lc->com_disgra;
       $lista->forma_devo = $lc->forma_devo;
       $lista->retencion = $lc->retencion;
       $lista->porcentaje = $lc->porcentaje;
       $lista->reproceso = $lc->reproceso;
       $lista->cuenta_exe = $lc->cuenta_exe;
       $lista->com_tipimp = $lc->com_tipimp;
       $lista->com_gra05 = $lc->com_gra05;
       $lista->com_iva05 = $lc->com_iva05;
       $lista->com_disg05 = $lc->com_disg05;
       $lista->cta_iva05 = $lc->cta_iva05;
       $lista->com_rubgra = $lc->com_rubgra;
       $lista->com_rubg05 = $lc->com_rubg05;
       $lista->com_ctag05 = $lc->com_ctag05;
       $lista->com_rubexe = $lc->com_rubexe;
       $lista->com_saldo = $lc->com_saldo;
       $lista->timbrado = $lc->timbrado;

       $lista->com_gasinv = $lc->com_gasinv;
       $lista->com_gasinv = $lc->com_gasinv;
       $lista->com_rubro = $lc->com_rubro;
       $lista->com_gdc = $lc->com_gdc;
       $lista->com_irpc = $lc->com_irpc;
       $lista->retivacon = $lc->retivacon;
       $lista->retretcon = $lc->retretcon;
       $lista->tipo_export = $lc->tipo_export;
       $lista->retiva05 = $lc->retiva05;
       $lista->irptip = $lc->irptip;
       $lista->irpinc = $lc->irpinc;
       $lista->irpmon = $lc->irpmon;
       $lista->irptip2 = $lc->irptip2;
       $lista->irpinc2 = $lc->irpinc2;
       $lista->irpmon2 = $lc->irpmon2;
       $lista->irptip3 = $lc->irptip3;
       $lista->irpinc3 = $lc->irpinc3;
       $lista->irpmon3 = $lc->irpmon3;
       $lista->irptip4 = $lc->irptip4;
       $lista->irpinc4 = $lc->irpinc4;
       $lista->irpmon4 = $lc->irpmon4; 
       $lista->irptip5 = $lc->irptip5;
       $lista->irpinc5 = $lc->irpinc5;
       $lista->irpmon5 = $lc->irpmon5;
       $lista->ire = $lc->ire;
       $lista->ivasimplific = $lc->ivasimplific;
       $lista->irprygc = $lc->irprygc;
       $lista->nrofactnc = $lc->nrofactnc;
       $lista->nrotimfactnc = $lc->nrotimfactnc;
       $lista->nombco = $lc->nombco;
       $lista->bcoctacte = $lc->bcoctacte;
       $lista->tipodoc = $lc->tipodoc;
      
       $lista->id_lc = $lc->id_lc;
       $lista->id_moneda=$lc->id_moneda;
       $lista->costito=$lc->costito;
       $lista->comtota=$lc->comtota;
       $listado[] =  $lista;      
    }

    $cuerpo = array();
        
    foreach($listado as $key=>$lista)
    {/*
        //$exen_dolar = ($lista->exen_dolar === null || $lista->exen_dolar === '') ? 0 : round($lista->exen_dolar,2);
       // $lista->exen_dolar = $lista->exen_dolar ? $lista->exen_dolar : 0;
       if ($lista->exen_dolar === null || trim($lista->exen_dolar) === '') {
        $exen_dolar = '0'; 
    } else {
        $exen_dolar = (string) $lista->exen_dolar; 
    }*/

    if ($lista->id_moneda === 143) {
        if ($lista->cambio != 0) {
           /* $total = $lista->com_exenta / $lista->cambio;
            $com_import = $total * $lista->cambio;*/
            $com_import = $lista->costito * $lista->cambio;
           // $com_import = $total * $lista->cambio;
        } else {
            $com_import = 0; 
        }
    } else {
        $com_import = $lista->com_exenta;
    }
    if ($lista->id_moneda === 143) {
        if ($lista->cambio != 0) {
         
            $com_totfactotal = $lista->costito + $lista->comtota;
            $com_totfac=$com_totfactotal * $lista->cambio;
        } else {
            $com_totfac = 0; 
        }
    } else {
        $com_totfac = $lista->com_totfac;
    }
    
        array_push($cuerpo, array($lista->com_numero,$lista->generar,$lista->cta_iva,$lista->cta_caja,
        $lista->form_pag,$lista->com_imputa,$lista->com_sucurs,$lista->com_centro,$lista->com_provee,
        $lista->com_cuenta,$lista->com_prvnom,$lista->com_tipofa,$lista->com_fecha,round($com_totfac,0),
        round($com_import,0),floatval($lista->com_gravad),floatval($lista->com_iva),round($com_import,0),
        $lista->com_aux,$lista->com_con,$lista->com_cuota,$lista->com_fecven,$lista->com_ordenp,$lista->cant_dias,
        $lista->concepto,$lista->moneda,floatval($lista->cambio),$lista->valor,$lista->origen,$lista->exen_dolar,
        $lista->com_disexe,$lista->com_disgra,$lista->forma_devo,$lista->retencion,$lista->porcentaje,$lista->reproceso,
        $lista->cuenta_exe,$lista->com_tipimp, floatval($lista->com_gra05),floatval($lista->com_iva05),$lista->com_disg05,$lista->cta_iva05,
        $lista->com_rubgra,$lista->com_rubg05,$lista->com_ctag05,$lista->com_rubexe,floatval($lista->com_saldo),$lista->timbrado,
        $lista->com_gasinv,$lista->com_gasinv,$lista->com_rubro,$lista->com_gdc,$lista->irpmon,$lista->irpinc,$lista->retivacon,
        $lista->irpmon,$lista->irpinc5,$lista->nrofactnc,$lista->nrotimfactnc,$lista->bcoctacte,$lista->nombco));

        if ($lista->migrado_detalle != true)
        {
          DB::table('libros_compras')->where('id',$lista->id_lc)->update(['migrado_factura'=>true]);
        }
    }  
    return Excel::download(new MigracionCompraExport($cuerpo, $request), 'Migración Compras-' . date('d-m-Y') . '.xls');
}


public function generarExcelLibroCompra(Request $req){

    $libroCompra = DB::table('vw_libro_compra');
    $libroCompra = $libroCompra->where('id_empresa',$this->getIdEmpresa());
     if($req->input('idProveedor')!= '')
         $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
    
             if($req->input('nroFactura') != '')
             {
                 $libroCompra = $libroCompra->where('nro_documento',$req->input('nroFactura'));
                 
             }

             if($req->input('idProducto') != '')
             {
                 $libroCompra = $libroCompra->where('id_producto',$req->input('idProducto'));
             }

             if($req->input('codigoConfirmacion') != '')
             {
                 $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
             }

             if($req->input('idMoneda') != '')
             {

                 $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));

             }
             
             if($req->input('idLcv') != '')
             {

                 $libroCompra = $libroCompra->where('id',$req->input('idLcv'));

             }

             if($req->input('estado_lc')!= '')
             {
               if ($req->input('estado_lc') == 1) 
               {
                 $libroCompra = $libroCompra->where('activo', true);
                 
               }
               else
               {
                 $libroCompra = $libroCompra->where('activo', false);
               }
             }
             if ($req->input('tipo_proveedor')!= '')
             {
                 $libroCompra = $libroCompra->where('tipo_proveedor', $req->input('tipo_proveedor'));
             }


              if($req->input('proveedor_desde_hasta') != '')
               {
                 $fechaTrim = trim($req->input('proveedor_desde_hasta'));
                 $periodo = explode('-', trim($fechaTrim));
                 $desde = explode("/", trim($periodo[0]));
                 $hasta = explode("/", trim($periodo[1]));
                 $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
                 $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
                 $libroCompra = $libroCompra->whereBetween('fecha_creacion', [$fecha_desde, $fecha_hasta]);
               }
           
             if($req->input('creacion_desde_hasta') != '')
               {
               $fechaTrim = trim($req->input('creacion_desde_hasta'));
               $periodo = explode('-', trim($fechaTrim));
               $desde = explode("/", trim($periodo[0]));
               $hasta = explode("/", trim($periodo[1]));
               $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
               $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
                   $libroCompra = $libroCompra->whereBetween('fecha', [$fecha_desde, $fecha_hasta]);
               }

                  if($req->input('tiene_diferencia') != '')
                  {
                      if($req->input('tiene_diferencia')== 'S'){
                          $libroCompra = $libroCompra->where('diferencia', '<>' ,0);
                      }else{
                          $libroCompra = $libroCompra->where('diferencia', 0);
                      }
                  } 
  
            $libroCompra = $libroCompra->orderBy('fecha', 'asc'); 
            $libroCompra = $libroCompra->get();
            
            if($req->input('id_cuenta_exenta'))
              {
                $arrayLC = array();
                foreach($libroCompra as $lc){
                if($lc->id_cuenta_exenta == $req->input('id_cuenta_exenta') || $lc->id_cuenta_gravada_10 == $req->input('id_cuenta_exenta') || $lc->id_cuenta_gravada_5 == $req->input('id_cuenta_exenta')){
                    $arrayLC[] = $lc;
                }
                }
                $libroCompra = $arrayLC;
            }

            $cuerpo = array();
                
            foreach($libroCompra as $key=>$lista)
            {
                if($this->getIdEmpresa() == 1){
                    $fecha_nemo = $lista->fecha_notificado_nemo_format;
                }else{   
                    $fecha_nemo = ''; 
                }
                $ruc =$lista->documento_identidad;
                if($lista->dv_proveedor != ""){
                    $ruc = $ruc."-".$lista->dv_proveedor;
                }


                array_push ($cuerpo, array(
                                    date('d/m/Y',strtotime($lista->fecha_creacion)), 
                                    $lista->dia, 
                                    $lista->nro_documento,
                                    $lista->proveedor, 
                                    $ruc, 
                                    $lista->monto_compra_10, 
                                    $lista->monto_compra_5,
                                    $lista->iva_credito_10,
                                    $lista->iva_credito_5,
                                    $lista->monto_compra_exenta, 
                                    $lista->total,
                                    $lista->diferencia,
                                    $lista->nro_timbrado, 
                                    $lista->id_tipo_documento_hechauka_detalle,
                                    $lista->tipo_proveedor_nombre,
                                    $fecha_nemo,
                                    $lista->tipo_factura_nombre,
                                    $lista->plan_cuenta,
                                    $lista->cuenta_retencion,
                                    $lista->cuenta_exenta,
                                    $lista->cuenta_gravada_5,
                                    $lista->cuenta_gravada_10,
                                    $lista->comprobante
                                    
                            ));
           }   
           // dd($cuerpo);

 
           $idEmpresa = $this->getIdEmpresa(); // Define el ID de la empresa, puedes obtenerlo de alguna manera
       
           return Excel::download(new ComprasExport($cuerpo, $idEmpresa), 'LIBROS COMPRAS ' . date('d-m-Y') . '.xls');

        }

      public function generarExcelVentaPendiente(Request $req){
          
        $ventas = DB::table('vw_ventas_pendientes');
		$ventas = $ventas->where('id_empresa',$this->getIdEmpresa());
		if($req->input('idProveedor') != '')
		{
			$ventas = $ventas->where('id_proveedor',$req->input('idProveedor'));
			
		}
		if($req->input('idProforma') != '')
		{
			$ventas = $ventas->where('id_proforma',$req->input('idProforma'));
			
		}
		if($req->input('nroFactura') != '')
		{
			$ventas = $ventas->where('nro_factura','like', '%' .trim($req->input('nroFactura')).'%');
			
		}
		if($req->input('codigoConfirmacion') != '')
		{
			$ventas = $ventas->where('cod_confirmacion',$req->input('codigoConfirmacion'));
		}
		if($req->input('asociado') != '')
		{
			$ventas = $ventas->where('asociado',$req->input('asociado') );
		}
		if($req->input('idMoneda') != '')
		{
			$ventas = $ventas->where('id_moneda',$req->input('idMoneda'));
		}
		if($req->input('mes') != '')
		{
			if($req->input('anho'))
			{
				$ventas = $ventas->where('mes', trim($req->input('mes')))->where('anho',$req->input('anho'));
			 }
		  }   
		  if($req->input('proveedor_desde_hasta') != '')
		  {
			  $fechaTrim = trim($req->input('proveedor_desde_hasta'));
			  $periodo = explode('-', trim($fechaTrim));
			  $desde = explode("/", trim($periodo[0]));
			  $hasta = explode("/", trim($periodo[1]));
			  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
			  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
			  $ventas = $ventas->whereBetween('created_at', [$fecha_desde, $fecha_hasta]);
		  }

		  if($req->input('creacion_desde_hasta') != '')
		  {
			  $fechaTrim = trim($req->input('creacion_desde_hasta'));
			  $periodo = explode('-', trim($fechaTrim));
			  $desde = explode("/", trim($periodo[0]));
			  $hasta = explode("/", trim($periodo[1]));
			  $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
			  $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
			  $ventas = $ventas->whereBetween('fecha_hora_facturacion', [$fecha_desde, $fecha_hasta]);
		  }

		$ventas = $ventas->get();
        $cuerpo = array();
                
            foreach($ventas as $key=>$lista)
            {
                array_push ($cuerpo, array(
                                    $lista->fecha_hora_facturacion_format, 
                                    $lista->dia,
                                    $lista->nro_factura, 
                                    $lista->documento_identidad, 
                                    $lista->proveedor, 
                                    $lista->gravadas_10,
                                    $lista->gravadas_5,
                                    $lista->iva10pago,
                                    $lista->iva05pago, 
                                    $lista->exento,
                                    $lista->total, 
                                    $lista->numero,
                                    $lista->tipo,
                                    $lista->id_proforma,
                                    $lista->origen
                            ));
           }   
           // dd($cuerpo);

           return Excel::download(new CostoVentaPendientes($cuerpo), 'Costo de Ventas Pendientes-' . date('d-m-Y') . '.xls');

    
    }



      public function generarExcelLibroCompraFondoFijo(Request $req){

        $libroCompra = DB::table('v_libros_compras');
        $libroCompra = $libroCompra->where('fondo_fijo',true);


        if($req->input('idProveedor')!= '')
            $libroCompra = $libroCompra->where('id_proveedor',$req->input('idProveedor'));
       

                if($req->input('nroFactura') != '')
                {
                    $libroCompra = $libroCompra->where('nro_documento',$req->input('nroFactura'));
                    
                }

                if($req->input('codigoConfirmacion') != '')
                {
                    $libroCompra = $libroCompra->where('cod_confirmacion',$req->input('codigoConfirmacion'));
                }

                if($req->input('idMoneda') != '')
                {

                    $libroCompra = $libroCompra->where('id_moneda',$req->input('idMoneda'));

                }
                
                if($req->input('estado_lc')!= '')
                {
                  if ($req->input('estado_lc') == 1) 
                  {
                    $libroCompra = $libroCompra->where('activo', true);
                    
                  }
                  else
                  {
                    $libroCompra = $libroCompra->where('activo', false);
                  }
                }

                if($req->input('id_cuenta_exenta')!= '')
                {

                    $libroCompra = $libroCompra->where('id_cuenta_exenta', $req->input('id_cuenta_exenta'));
                    $libroCompra = $libroCompra->orWhere('id_cuenta_gravada_10', $req->input('id_cuenta_exenta'));
                    $libroCompra = $libroCompra->orWhere('id_cuenta_gravada_5', $req->input('id_cuenta_exenta'));
                }

                 if($req->input('proveedor_desde_hasta') != '')
                  {
                    $fechaTrim = trim($req->input('proveedor_desde_hasta'));
                    $periodo = explode('-', trim($fechaTrim));
                    $desde = explode("/", trim($periodo[0]));
                    $hasta = explode("/", trim($periodo[1]));
                    $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0];
                    $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0];
                    $libroCompra = $libroCompra->whereBetween('fecha', [$fecha_desde, $fecha_hasta]);
                  }
              
           
            $libroCompra = $libroCompra->get();
            // dd($periodo);
            $cuerpo = array();
                
            foreach($libroCompra as $key=>$lista)
            {
                array_push ($cuerpo, array($lista->dia, $lista->nro_documento, $lista->proveedor, $lista->documento_identidad, $lista->gravadas_10, $lista->gravadas_5,$lista->iva_10,$lista->iva_5,$lista->exentas, $lista->total
                ));

           }   
           // dd($cuerpo);

           $nombreArchivo = 'LIBROS COMPRAS FONDO FIJO-' . date('d-m-Y') . '.xlsx';

           return Excel::download(new LibrosComprasFondoFijoExport($cuerpo, $titulo), $nombreArchivo);

    }

     public function generarExcelMigracionAsiento(Request $req)
     {
        if ($req->input('tipo_doc') == 1) //resumen
        {
            $migracionAsiento = DB::table('vw_migracion_asiento_resumen');
            $migracionAsiento = $migracionAsiento->where('id_empresa',$this->getIdEmpresa());

            if($req->input('desde_hasta') != '')
            {
                $fechaTrim = trim($req->input('desde_hasta'));
                $periodo = explode('-', trim($fechaTrim));
                $desde = explode("/", trim($periodo[0]));
                $hasta = explode("/", trim($periodo[1]));
                $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
                $migracionAsiento = $migracionAsiento->whereBetween('fecha_asiento', [$fecha_desde, $fecha_hasta]);
            }

            if ($req->input('id_origen_asiento') != '') 
            {
                $migracionAsiento = $migracionAsiento->where('id_origen_asiento', $req->input('id_origen_asiento'));
            }

            if ($req->input('asiento_anulado') == '0') 
            {
              $migracionAsiento = $migracionAsiento->where('activo', true);
            }
            else
            {
              $migracionAsiento = $migracionAsiento->where('activo', false);
            }

            $migracionAsiento = $migracionAsiento->orderBy('id_asiento','ASC');

            $migracionAsiento = $migracionAsiento->get();

            $cuerpo = array();

            $cont_linea_asiento=0;
                
            $anterior = 0;
            $contador = 0;
            $id_asiento =  intval($req->input('ultimo_asiento'));
            foreach($migracionAsiento as $key=>$lista)
            {
                $id_asiento =  $id_asiento + 1;
                array_push($cuerpo, array(
                                            $id_asiento, 
                                            ' ', 
                                            $lista->concepto,
                                            date('d/m/Y',strtotime($lista->fecha_asiento)), 
                                            $lista->sucursal_siga, 
                                            $lista->centro_costo_siga, 
                                            $lista->nro_documento, 
                                            $lista->cuenta_contable, 
                                            $lista->tipo_debe_haber, 
                                            $lista->codigo_cuenta_contable, 
                                            '0', 
                                            $lista->debe, 
                                            $lista->haber, 
                                            $lista->id_moneda, 
                                            $lista->cotizacion, 
                                            $lista->monto_original, 
                                            $lista->nro_asiento, 
                                            ' ', 
                                            ' ', 
                                            ' ',
                                            $lista->origen_asiento, 
                                            ' ', 
                                            ' ', 
                                            ' ', 
                                            ' ', 
                                            65));

            }  
        }
        else
        {
            $migracionAsiento = DB::table('vw_migracion_asiento_detalle');
			$migracionAsiento = $migracionAsiento->where('id_empresa',$this->getIdEmpresa());

			 if($req->input('id_origen_asiento') != '') 
			{
				$migracionAsiento = $migracionAsiento->where('id_origen_asiento', $req->input('id_origen_asiento'));
			} 

			if ($req->input('asiento_anulado') == '0') 
			{
				$migracionAsiento = $migracionAsiento->where('activo', true);
			}
			else
			{
				$migracionAsiento = $migracionAsiento->where('activo', false);
			}
			
			if($req->input('desde_hasta') != '')
			{
				$fechaTrim = trim($req->input('desde_hasta'));
				$periodo = explode('-', trim($fechaTrim));
				$desde = explode("/", trim($periodo[0]));
				$hasta = explode("/", trim($periodo[1]));
				$fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
				$fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
				$migracionAsiento = $migracionAsiento->whereBetween('fecha_asiento', [$fecha_desde, $fecha_hasta]);

			}

            $migracionAsiento = $migracionAsiento->orderBy('id_asiento','DESC'); //El orden influye en la sumatoria para generar el id asiento para el excel
			$migracionAsiento = $migracionAsiento->get();
            $cuerpo = array();

            $cont_linea_asiento=0;
                    
            $anterior = 0;
            $contador = 0;
            $id_asiento =  intval($req->input('ultimo_asiento'));

            foreach($migracionAsiento as $key=>$lista)
            {
                if($lista->id_asiento != $anterior)
                {
                    $contador = 0;   
                    $id_asiento = $id_asiento + 1;
                }
                $contador ++;
                array_push($cuerpo, array(
                                            $id_asiento, 
                                            $contador, 
                                            $lista->concepto, 
                                            date('d/m/Y',strtotime($lista->fecha_asiento)),
                                            $lista->sucursal_siga, 
                                            $lista->centro_costo_siga, 
                                            $lista->nro_documento, 
                                            $lista->cuenta_contable,
                                            $lista->tipo_debe_haber, 
                                            $lista->codigo_cuenta_contable, 
                                            '0', 
                                            $lista->debe, 
                                            $lista->haber, 
                                            $lista->id_moneda, 
                                            intval($lista->cotizacion), 
                                            round($lista->monto_original, 2), 
                                            $lista->nro_asiento, 
                                            ' ', 
                                            ' ', 
                                            ' ', 
                                            $lista->origen_asiento, 
                                            ' ', 
                                            ' ', 
                                            ' ', 
                                            ' ', 
                                            65));

                $anterior = $lista->id_asiento; 
            } 
        }

        $nombreArchivo = 'Migracion de asientos-' . date('d-m-Y') . '.xlsx';

        return Excel::download(new MigracionAsientosExport($cuerpo), $nombreArchivo);  
    }

    public function generarExcelCostoProveedor(Request $request)
    {

        $query = "select to_char(c.fecha_venta,
                             'dd/mm/yyyy') fecha_venta,
                             to_char(c.fecha_venta, 'dd') hora_corte,
                             d.id_proveedor, d.id_producto, 
                             'fecha_limite_entraga' fecha_limite_entraga,
                             w.id_woo_order numero_pedido, 
                             'numero_oc' numero_oc, 
                             p.nombre proveedor,
                             r.denominacion producto, 
                             d.nro_factura_proveedor sku, 
                             d.cantidad, d.precio_costo,
                             'dia_pedido_corte' dia_pedido_corte,
                             c.id numero_venta, 
                             e.denominacion estado_venta
                             from ventas_rapidas_cabecera c, ventas_rapidas_detalle d, estados e,
                                    personas p, productos r, comercio_empresa m, pedidos_woo w
                                    where c.id = d.id_venta_cabecera
                                    and c.id_estado = e.id
                                    and d.id_proveedor = p.id
                                    and d.id_producto = r.id
                                    and c.id_comercio_empresa = m.id
                                    and c.id_pedido = w.id";

        if($request->input("id_vendedor_empresa") != ''){
            $query.= ' AND d.id_proveedor = '.$request->input("id_vendedor_empresa");
        }

        if($request->input('periodo') != ""){
            $fechaPeriodo = explode(' - ', $request->input('periodo'));
            $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
            $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
            $query .= " AND c.fecha_venta BETWEEN '".$desde."' AND '".$hasta."'";
        } 
        
        if($request->input("id_estado") != ''){
            $query.= " AND to_char(c.fecha_venta, 'dd') = '".$request->input("id_estado")."'";
        }                       

        $corte = DB::select($query);
  
        $cuerpo = array();
        
        foreach($corte as $key=>$lista){
            array_push($cuerpo, array(
            $lista->fecha_venta, $lista->hora_corte,$lista->numero_pedido,$lista->proveedor,$lista->producto,$lista->sku,$lista->cantidad,$lista->precio_costo,$lista->numero_venta,$lista->estado_venta));
        }
       
        $nombreArchivo = 'Reporte Corte Proveedores-' . date('d-m-Y') . '.xlsx';

        return Excel::download(new ReporteCorteProveedoresExport($cuerpo), $nombreArchivo);

    }    
    public function generarExcelVenta(Request $request)
    {
        Session::put('req',$request);

        $facturas = Factura::with('facturaDetalle', 'tipoFactura', 'timbrado','cliente')->where(function($query) use($request) 
        {
            $query->where('id_estado_factura', '!=', '30');
            
            if(!empty($request->input('operacion')))
            {
              $query->where('migrado','=', $request->input('operacion'));
            }
            
            $query->where('id_empresa', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

            if(!empty($request->input('periodo')))
            {
              $fechaTrim = trim($request->input('periodo'));
              $periodo = explode('-', trim($fechaTrim));
              $desde = explode("/", trim($periodo[0]));
              $hasta = explode("/", trim($periodo[1]));
              $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
              $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
              $query->whereBetween('fecha_hora_facturacion', [$fecha_desde, $fecha_hasta]);
            }
      })->get();

    $notaCreditos =  NotaCredito::with('detalleNotaCredito', 'tipoFactura', 'timbrado', 'cliente','factura')
      ->where('id_empresa',$this->getIdEmpresa())
      ->where(function($query) use($request) 
        {
            $query->where('id_estado_nc', '!=', '35');
            
            if(!empty($request->input('operacion')))
            {
              $query->where('migrado','=', $request->input('operacion'));
            }
            if(!empty($request->input('periodo')))
            {
              $fechaTrim = trim($request->input('periodo'));
              $periodo = explode('-', trim($fechaTrim));
              $desde = explode("/", trim($periodo[0]));
              $hasta = explode("/", trim($periodo[1]));
              $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0]." 00:00:00";
              $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0]." 23:59:59";
              $query->whereBetween('fecha_hora_nota_credito', [$fecha_desde, $fecha_hasta]);
        }
      })->get();


    $listado = array();


  foreach($notaCreditos as $key=>$notaCredito)
    {
        /*
        echo '<pre>';
        print_r($notaCredito); 
        die();*/
        $lista = new \StdClass;

        if (isset($notaCredito->tipoFactura->denominacion)) {
            $denominacion = $notaCredito->tipoFactura->denominacion;
        }else{
            $denominacion = null;
        }

        if ((int)$notaCredito->id_tipo_facturacion == 1) //bruta
        {
            $tot_not = $notaCredito->total_bruto_nc;
        }
        else
        {
            $tot_not = $notaCredito->total_neto_nc;
        }

           switch ($notaCredito->id_moneda_venta) 
        {
            case '143':
                $currency = '01';
                $total_nota = (int) round($tot_not * $notaCredito->cotizacion_contable_compra,0);
                $valor = $tot_not;
                $cambio = $notaCredito->cotizacion_contable_compra;
                $exenta =  ($notaCredito->total_exentas);
                $exentaGs = (int) round(($notaCredito->total_exentas * $notaCredito->cotizacion_contable_compra),0);
                $gravada = $notaCredito->total_gravadas;
                $gravadaGs = $notaCredito->total_gravadas * $notaCredito->cotizacion_contable_compra;     
                $venGrav = ($notaCredito->total_gravadas - $notaCredito->total_iva) * $notaCredito->cotizacion_contable_compra;
                $auxiliar_gravada = (int) round( ($total_nota - $exentaGs),0);
                if($auxiliar_gravada == 0){
                    $iva = (int)0;
                }else{
                    $iva = (int) round(($auxiliar_gravada / 11),0);
                }
                if($auxiliar_gravada == 0){
                    $venta_gravada = (int)0;
                }else{
                    $venta_gravada = (int) round(($auxiliar_gravada / 1.1),0);
                }
                $diferencia = (int) round(($total_nota - ($exentaGs + $venta_gravada + $iva)),0);

                if ($diferencia != 0) 
                {
                    switch ($exentaGs) 
                    {
                        case ($exentaGs > 0):
                            $exentaGs = $exentaGs + $diferencia;
                            break;
                        
                        case ($exentaGs == 0):
                            $venta_gravada = $venta_gravada + $diferencia;
                            break;
                    }
                }

                break;

            case '111':
                $currency = '';
                $total_nota =  (int) round($tot_not,0);
                $valor = 0;
                $cambio = 0;
                if($notaCredito->cotizacion_contable_compra != 0 ){
                    $gravada = (int) round(($notaCredito->total_gravadas / $notaCredito->cotizacion_contable_compra),0);
                }else{
                    $gravada = 0;
                }
                $gravadaGs = (int) round($notaCredito->total_gravadas,0);
                $exenta = 0;
                $exentaGs = (int) round($notaCredito->total_exentas,0);
                $venGrav = (int) round(($notaCredito->total_gravadas - $notaCredito->total_iva),0);
                $auxiliar_gravada = (int) round(($total_nota - $exentaGs),0);
                if($auxiliar_gravada == 0){
                    $iva = (int)0;
                }else{
                    $iva = (int) round(($auxiliar_gravada / 11),0);
                }
                if($auxiliar_gravada == 0){
                    $venta_gravada = (int)0;
                }else{
                    $venta_gravada = (int) round(($auxiliar_gravada / 1.1),0);
                }
                $diferencia = (int) round(($total_nota - ($exentaGs + $venta_gravada + $iva)),0);

                if ($diferencia != 0) 
                {
                    switch ($exentaGs) 
                    {
                        case ($exentaGs > 0):
                            $exentaGs = $exentaGs + $diferencia;
                            break;
                        
                        case ($exentaGs == 0):
                            $venta_gravada = $venta_gravada + $diferencia;
                            break;
                    }
                }
                
                break;
        }

        if (isset($notaCredito->timbrado->id_sucursal_contable))
        {
            if($notaCredito->timbrado->id_sucursal_contable <= 9)
            {
                $resultados = SucursalEmpresa::where('id', '=' ,$notaCredito->timbrado->id_sucursal_contable)->first(['p_siga']);
                if($resultados->p_siga < 10){
                    $sucursal = (string)'0'.$resultados->p_siga;
                }else{
                    $sucursal = (string)$resultados->p_siga;
                }
            }
            else
            {
                $resultados = SucursalEmpresa::where('id', '=' ,$notaCredito->timbrado->id_sucursal_contable)->first(['p_siga']);
                if($resultados->p_siga < 10){
                    $sucursal = (string)'0'.$resultados->p_siga;
               }else{
                   $sucursal = (string)$resultados->p_siga;
               }
           }

        }
        else
        {
            $sucursal = null;
        }

        if(isset($notaCredito->timbrado->numero)){
            $timbrado = $notaCredito->timbrado->numero;
        }else{
            $timbrado = null;
        }

        if(isset($notaCredito->cliente->documento_identidad))
        {
            if($notaCredito->cliente->dv !==null){
            $proveedor = $notaCredito->cliente->documento_identidad . "-" . $notaCredito->cliente->dv;
            }else{
             $proveedor = $notaCredito->cliente->documento_identidad; 
            }
            
        }
        else
        {
            $proveedor = "";
        }

        if(isset($notaCredito->cliente->documento_identidad))
        {
            $proveedor_nombre = $notaCredito->cliente->nombre;  
        }
        else
        {
            $proveedor_nombre = "";
        } 

         if($notaCredito->id_tipo_factura == 1){
            $tipoFactura = 'FC';
         }else{
            $tipoFactura = 'FCR';
         }
            
         if(isset($notaCredito->factura[0]->id_venta_rapida)){    
            if($notaCredito->factura[0]->id_venta_rapida == ''){
                $servicio = '';
            }else{
                if(isset($notaCredito->factura[0]->id_comercio_empresa)){
                    $comercioPersona= ComercioPersona::where('id','=',$notaCredito->factura[0]->id_comercio_empresa)->get();
                    if(isset($comercioPersona[0]->descripcion)){
                        $servicio = $comercioPersona[0]->descripcion;
                    }else{
                        $servicio = '';
                    }
                }else{
                    $servicio = '';
                }
            } 
        }else{
            $servicio = '';
        }


          if($notaCredito->id_proforma != ''){
            $proforma = '- PROF '.$notaCredito->id_proforma;
          }else{
            $proforma = '';
          }  

        $cliente = ' - '.$notaCredito->cliente->nombre;

        $facturaUltimo = substr($notaCredito->nro_nota_credito, -4);

        //FCR 5800 - FATIMA GAONA- SERVICIO TURISMO- PROF 58900

        $concepto = $tipoFactura." ".$facturaUltimo."".$cliente."".$servicio."".$proforma;

        $lista->id = $notaCredito->id;  
        $lista->migrado = $notaCredito->migrado;  
        $lista->ven_tipimp = null;  
        $lista->ven_gra05 = null; 
        $lista->ven_iva05 = null; 
        $lista->ven_disg05 = null;  
        $lista->cta_iva05 = null; 
        $lista->ven_rubgra = null;  
        $lista->ven_rubg05 = null;  
        $lista->ven_disexe = null;  
        $lista->ven_numero = $notaCredito->nro_nota_credito;  
        $lista->ven_imputa = '0';  
        $lista->ven_sucursal = $sucursal;    
        $lista->generar = '0'; 
        $lista->form_pag = $denominacion;     
        $lista->ven_centro = null;  
        $lista->ven_provee = $proveedor;
        $ven_cuenta = DB::select("SELECT * FROM get_cuenta_empresa('NOTA_CREDITO', ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")"); 
        $planCuenta = PlanCuenta::where('id', $ven_cuenta[0]->get_cuenta_empresa)->get(['cod_txt']);
        if(isset($planCuenta[0]->cod_txt)){
            $ven_cuenta = str_replace('.', '',$planCuenta[0]->cod_txt);  
        }else{
             $ven_cuenta = 0;
        }
        $lista->ven_cuenta = $ven_cuenta;   
        $lista->ven_prvnom =  $proveedor_nombre; 
        $lista->cta_tipofa = 'Nota de Crédito';  
        $lista->ven_fecha = date('d/m/Y', strtotime($notaCredito->fecha_hora_nota_credito));   
        $lista->ven_totfac = $total_nota;  
        $lista->ven_exenta = $exentaGs; 
        $lista->ven_gravad = $venta_gravada; 
        $lista->ven_iva = $iva; 
        // $lista->diferencia = round($diferencia,0);
        $lista->ven_retenc = '0';  
        $lista->ven_aux = null; 
        $lista->ven_ctrl = null;    
       // $lista->ven_con = $notaCredito->id_proforma ." - Paquetes de Turismo";
        $lista->ven_con = $this->limitar_cadena($concepto, 79, "");
        $lista->ven_cuota = (int)1;   
        $lista->ven_fecven = date('d/m/Y', strtotime($notaCredito->vencimiento));  
        $lista->cant_dias = '0';   
        $lista->origen = 'LI';  
        $lista->cambio = (int) $cambio;  
        $lista->valor =  floatval($valor);  
        $lista->moneda = $currency;  
        /*if ($exenta === null || $exenta === '') {
            $lista->exen_dolar = 0;
        } else {
            $lista->exen_dolar = round($exenta, 2);
        }*/
        $lista->exen_dolar = $exenta;
        //$lista->exen_dolar = round($exenta, 2);  
       // $lista->concepto = $notaCredito->id_proforma ." - Paquetes de Turismo";
        $lista->concepto = $this->limitar_cadena($concepto, 79, "");
        $cta_iva = DB::select("SELECT * FROM get_cuenta_empresa('IVA_CREDITO', ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
        $planCuenta = PlanCuenta::where('id', $cta_iva[0]->get_cuenta_empresa)->get(['cod_txt']);

        $lista->cta_iva = str_replace('.', '',$planCuenta[0]->cod_txt);  
        $lista->cta_caja = null;    
        $lista->tkdesde = null;
        $lista->tkhasta = null;
        $lista->caja = null;
        $lista->ven_disgra = 'A';  
        $lista->forma_devo = '0';  
        $lista->ven_cuense = null;  
        $lista->anular = '0';  
        $lista->reproceso = null;  
        $cuenta_exe = DB::select("SELECT * FROM get_cuenta_empresa('VENTA_EXENTA', ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");  
        $planCuenta = PlanCuenta::where('id', $cuenta_exe[0]->get_cuenta_empresa)->get(['cod_txt']);
        if(isset($planCuenta[0]->cod_txt)){
            $lista->cuenta_exe =str_replace('.', '',$planCuenta[0]->cod_txt);   
        }else{
            $lista->cuenta_exe ='';   
        }
        $lista->usu_ide = null;
        $lista->ven_timbra = $timbrado;

        $lista->clieasi = 0;
        $lista->ventirptip = null;
        $lista->ventirpgra = null;
        $lista->ventimporteirp = null;
        $lista->ventirpexe = null;
        $lista->irpc = 0;
        $lista->ivasimplificado = 0;
        $lista->ven_gdc = null;
        $lista->venrubrogdc = null;
        $lista->ven_gdccosto = null;
        $lista->ven_gdcventa = null;
        $lista->ven_gdcori = null;
        $lista->ven_nronc = null;
        $lista->ven_nctim = null;
        $lista->ventipcli = null;
        $lista->ven_banctacte = null;
        $lista->ven_bannom = null;
        $lista->venbcobtabte = null;	
        $lista->nofacnotcre = $notaCredito->nro_nota_credito; 
        $lista->notimbfacnotcre =$timbrado; 
        $lista->ventipodoc = null;	
        $lista->ventanoiva = null;
        $listado[] = $lista;
    // }
    }

  foreach($facturas as $key=>$factura) 
    {
        $lista = new \StdClass;

        if (isset($factura->tipoFactura->denominacion)) {
            $denominacion = $factura->tipoFactura->denominacion;
        }else{
            $denominacion = null;
        }

        if ($factura->id_tipo_facturacion == 1) //bruta
        {
            $tot_fac = $factura->total_bruto_factura;
        }
        else
        {
            $tot_fac = $factura->total_neto_factura;
        }

        switch ($factura->id_moneda_venta) 
        {
            case '143':
                $currency = '01';
                $total_factura = (int) round($tot_fac * $factura->cotizacion_contable_compra,0);
                $valor = $tot_fac;
                $cambio = $factura->cotizacion_contable_compra;
                $exenta =  ($factura->total_exentas);
                $exentaGs = (int) round(($factura->total_exentas * $factura->cotizacion_contable_compra),0);
                $gravada = $factura->total_gravadas;
                $gravadaGs = $factura->total_gravadas * $factura->cotizacion_contable_compra;     
                $venGrav = ($factura->total_gravadas - $factura->total_iva) * $factura->cotizacion_contable_compra;
                $auxiliar_gravada = (int) round( ($total_factura - $exentaGs),0);
                $iva = (int) round(($auxiliar_gravada / 11),0);
                $venta_gravada = (int) round(($auxiliar_gravada / 1.1),0);
                $diferencia = (int) round(($total_factura - ($exentaGs + $venta_gravada + $iva)),0);

                if ($diferencia != 0) 
                {
                    switch ($exentaGs) 
                    {
                        case ($exentaGs > 0):
                            $exentaGs = $exentaGs + $diferencia;
                            break;
                        
                        case ($exentaGs == 0):
                            $venta_gravada = $venta_gravada + $diferencia;
                            break;
                    }
                }

                break;

            case '111':
                $currency = '';
                $total_factura =  (int) round($tot_fac,0);
                $valor = 0;
                $cambio = 0;
                if($factura->cotizacion_contable_compra != 0 ){
                    $gravada = (int) round(($factura->total_gravadas / $factura->cotizacion_contable_compra),0);
                }else{
                    $gravada = 0;
                }
                $gravadaGs = (int) round($factura->total_gravadas,0);
                $exenta = 0;
                $exentaGs = (int) round($factura->total_exentas,0);
                $venGrav = (int) round(($factura->total_gravadas - $factura->total_iva),0);
                $auxiliar_gravada = (int) round(($total_factura - $exentaGs),0);
                $iva = (int) round(($auxiliar_gravada / 11),0);
                $venta_gravada = (int) round(($auxiliar_gravada / 1.1),0);
                $diferencia = (int) round(($total_factura - ($exentaGs + $venta_gravada + $iva)),0);

                if ($diferencia != 0) 
                {
                    switch ($exentaGs) 
                    {
                        case ($exentaGs > 0):
                            $exentaGs = $exentaGs + $diferencia;
                            break;
                        
                        case ($exentaGs == 0):
                            $venta_gravada = $venta_gravada + $diferencia;
                            break;
                    }
                }
                
                break;

            case '43':
                $currency = '';
                $total_factura =  (int) round($tot_fac,0);
                $valor = 0;
                $cambio = 0;
                if($factura->cotizacion_contable_compra != 0 ){
                    $gravada = (int) round(($factura->total_gravadas / $factura->cotizacion_contable_compra),0);
                }else{
                    $gravada = 0;
                }
                $gravadaGs = (int) round($factura->total_gravadas,0);
                $exenta = 0;
                $exentaGs = (int) round($factura->total_exentas,0);
                $venGrav = (int) round(($factura->total_gravadas - $factura->total_iva),0);
                $auxiliar_gravada = (int) round(($total_factura - $exentaGs),0);
                $iva = (int) round(($auxiliar_gravada / 11),0);
                $venta_gravada = (int) round(($auxiliar_gravada / 1.1),0);
                $diferencia = (int) round(($total_factura - ($exentaGs + $venta_gravada + $iva)),0);

                if ($diferencia != 0) 
                {
                    switch ($exentaGs) 
                    {
                        case ($exentaGs > 0):
                            $exentaGs = $exentaGs + $diferencia;
                            break;
                        
                        case ($exentaGs == 0):
                            $venta_gravada = $venta_gravada + $diferencia;
                            break;
                    }
                }
                
                break;
        }

        if (isset($factura->timbrado->id_sucursal_contable))
        {

            if($factura->timbrado->id_sucursal_contable <= 9)
            {
                $resultados = SucursalEmpresa::where('id', '=' ,$factura->timbrado->id_sucursal_contable)->first(['p_siga']);
                if($resultados->p_siga < 10){
                    $sucursal = (string)'0'.$resultados->p_siga;
                }else{
                    $sucursal = (string)$resultados->p_siga;
                }
            }
            else
            {
                $resultados = SucursalEmpresa::where('id', '=' ,$factura->timbrado->id_sucursal_contable)->first(['p_siga']);
                if($resultados->p_siga < 10){
                    $sucursal = (string)'0'.$resultados->p_siga;
                }else{
                    $sucursal = (string)$resultados->p_siga;
                }
            }

        }
        else
        {
            $sucursal = null;
        }

        if(isset($factura->timbrado->numero))
        {
            $timbrado = $factura->timbrado->numero;
        }
        else
        {
            $timbrado = "";
        }

        if(isset($factura->cliente->documento_identidad))
        {
            $proveedor = $factura->cliente->documento_identidad; 
            //$proveedor = $factura->cliente->documento_identidad; 
        }
        else
        {
            $proveedor = "";
        }

        if(isset($factura->cliente->documento_identidad))
        {
            $proveedor_nombre = $factura->cliente->nombre;  
        }
        else
        {
            $proveedor_nombre = "";
        } 
            
         if($factura->id_tipo_factura == 1){
            $tipoFactura = 'FC';
         }else{
            $tipoFactura = 'FCR';
         }
            
         if($factura->id_venta_rapida == ''){
            //$servicio = 'SERVICIO TURISMO';
            $servicio = '';
            $base = "";
         }else{
            $comercioPersona= ComercioPersona::where('id','=',$factura->id_comercio_empresa)->get();
            if(isset($comercioPersona[0]->descripcion)){
                 $servicio = $comercioPersona[0]->descripcion;
                 $base = "-".$comercioPersona[0]->descripcion;
            }else{
                 $servicio = '';
                 $base = "";
            }
           
         }

          if($factura->id_proforma != ''){
            $proforma = '- PROF '.$factura->id_proforma;
          }else{
            $proforma = '';
          }  

        $cliente = ' - '.$factura->cliente->nombre;

        $facturaUltimo = substr($factura->nro_factura, -4);

        //FCR 5800 - FATIMA GAONA- SERVICIO TURISMO- PROF 58900

        $concepto = $tipoFactura." ".$facturaUltimo."".$cliente."".$servicio."".$proforma;

        $lista->id = $factura->id;  
        $lista->migrado = $factura->migrado;  
        $lista->ven_tipimp = null;  
        $lista->ven_gra05 = null; 
        $lista->ven_iva05 = null; 
        $lista->ven_disg05 = null;  
        $lista->cta_iva05 = null; 
        $lista->ven_rubgra = null;  
        $lista->ven_rubg05 = null;  
        $lista->ven_disexe = null;  
        $lista->ven_numero = $factura->nro_factura;  
        $lista->ven_imputa = '0';  
        $lista->ven_sucursal = $sucursal;    
        $lista->generar = '0'; 
        $lista->form_pag = $denominacion;    
        $lista->ven_centro = null;  
        $lista->ven_provee = $proveedor;
        $ven_cuenta = DB::select("SELECT * FROM get_cuenta_empresa('VENTA".$base."', ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
        $planCuenta = PlanCuenta::where('id', $ven_cuenta[0]->get_cuenta_empresa)->get(['cod_txt']);
        if(isset($planCuenta[0]->cod_txt)){
            $ven_cuenta = str_replace('.', '',$planCuenta[0]->cod_txt);  
        }else{
             $ven_cuenta = 0;
        }
        $lista->ven_cuenta = $ven_cuenta;    
        $lista->ven_prvnom = $proveedor_nombre; 
        $lista->cta_tipofa = 'Factura';  
        $lista->ven_fecha = date('d/m/Y', strtotime($factura->fecha_hora_facturacion));   
        $lista->ven_totfac = $total_factura;  
        $lista->ven_exenta = $exentaGs; 
        $lista->ven_gravad = $venta_gravada; 
        $lista->ven_iva = $iva; 
        // $lista->diferencia = round($diferencia,0);
        $lista->ven_retenc = '0';  
        $lista->ven_aux = null; 
        $lista->ven_ctrl = null;    
       // $lista->ven_con = $factura->id_proforma ." - Paquetes de Turismo";
        $lista->ven_con = $this->limitar_cadena($concepto, 79, "");
        $lista->ven_cuota = (int)1;   
        $lista->ven_fecven = date('d/m/Y', strtotime($factura->vencimiento));  
        $lista->cant_dias = '0';   
        $lista->origen = 'LI';  
        $lista->cambio = (int) $cambio;  
        $lista->valor =  floatval($valor);  
        $lista->moneda = $currency;  
        //$lista->exen_dolar = round($exenta, 2);
        $lista->exen_dolar = $exenta;
       /* if ($exenta === null || $exenta === '') {
            $lista->exen_dolar = 0;
        } else {
            $lista->exen_dolar = round($exenta, 2);
        }
        */
        //$lista->concepto = $factura->id_proforma ." - Paquetes de Turismo";  
        $lista->concepto = $this->limitar_cadena($concepto, 79, ""); 
        
        $cta_iva = DB::select("SELECT * FROM get_cuenta_empresa('IVA_DEBITO', ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
        $planCuenta = PlanCuenta::where('id', $cta_iva[0]->get_cuenta_empresa)->get(['cod_txt']);

        $lista->cta_iva =str_replace('.', '',$planCuenta[0]->cod_txt); $planCuenta[0]->cod_txt;   
        $lista->cta_caja = null;    
        $lista->tkdesde = null;
        $lista->tkhasta = null;
        $lista->caja = null;
        $lista->ven_disgra = 'A';  
        $lista->forma_devo = '0';  
        $lista->ven_cuense = null;  
        $lista->anular = '0';  
        $lista->reproceso = null;   
        $cuenta_exe = DB::select("SELECT * FROM get_cuenta_empresa('VENTA_EXENTA', ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.")");
        $planCuenta = PlanCuenta::where('id', $cuenta_exe[0]->get_cuenta_empresa)->get(['cod_txt']);
        if(isset($planCuenta[0]->cod_txt)){
            $lista->cuenta_exe =str_replace('.', '',$planCuenta[0]->cod_txt);   
        }else{
            $lista->cuenta_exe ='';   
        }
        $lista->usu_ide = null;
        $lista->ven_timbra = $timbrado;

        $lista->clieasi = 0;
        $lista->ventirptip = null;
        $lista->ventirpgra = null;
        $lista->ventimporteirp = null;
        $lista->ventirpexe = null;
        $lista->irpc = 0;
        $lista->ivasimplificado = 0;
        $lista->ven_gdc = null;
        $lista->venrubrogdc = null;
        $lista->ven_gdccosto = null;
        $lista->ven_gdcventa = null;
        $lista->ven_gdcori = null;
        $lista->ven_nronc = null;
        $lista->ven_nctim = null;
        $lista->ventipcli = null;
        $lista->ven_banctacte = null;
        $lista->ven_bannom = null;
        $lista->venbcobtabte = null;	
        $lista->nofacnotcre = 0; 
        $lista->notimbfacnotcre =0; 
        $lista->ventipodoc = null;	
        $lista->ventanoiva = null;

        $listado[] = $lista;
    // }
    }       
    
    /*echo '<pre>';
    print_r($lista->ven_provee);
    die;//arturo*/
    $cuerpo = array();
       //dd($listado); 
    foreach($listado as $key=>$lista)
    {
        //inicio validaciones no tocar
      /*  if ($lista->exen_dolar === null || trim($lista->exen_dolar) === '') {
            $exen_dolar = '0'; 
        } else {
            $exen_dolar = (string) $lista->exen_dolar; 
        }*/
        if ($lista->exen_dolar === null || trim($lista->exen_dolar) === '') {
            $exen_dolar = '0';
        } else {
            $exen_dolar = (string) $lista->exen_dolar;
            $exen_dolar = str_replace('.', ',', $exen_dolar);
        }
        if ($lista->valor === null || trim($lista->valor) === '') {
            $valor = '0'; 
        } else {
            $valor = (string) $lista->valor; 
            $valor = str_replace('.', ',', $valor);
        }
        if ($lista->ven_exenta === null || trim($lista->ven_exenta) === '') {
            $ven_exenta = '0'; 
        } else {
            $ven_exenta = (string) $lista->ven_exenta; 
            $ven_exenta = str_replace('.', ',', $ven_exenta);
        }
        if ($lista->ven_gravad === null || trim($lista->ven_gravad) === '') {
            $ven_gravad = '0'; 
        } else {
            $ven_gravad = (string) $lista->ven_gravad;
            $ven_gravad = str_replace('.', ',', $ven_gravad); 
        }
        if ($lista->ven_iva === null || trim($lista->ven_iva) === '') {
            $ven_iva = '0'; 
        } else {
            $ven_iva = (string) $lista->ven_iva;
            $ven_iva = str_replace('.', ',', $ven_iva);  
        }
        if ($lista->cambio === null || trim($lista->cambio) === '') {
            $cambio = '0'; 
        } else {
            $cambio = (string) $lista->cambio; 
        }
        if ($lista->form_pag === "Contado") {
            $lista->cant_dias = '0';
        } else {
            $lista->cant_dias = '30';
        }
//fin validaciones para poner 0 no tocar ya que en numerico no arroja los nulos o  vacios en 0
        array_push ($cuerpo, array($lista->ven_tipimp,$lista->ven_gra05,$lista->ven_iva05,
            $lista->ven_disg05,$lista->cta_iva05,$lista->ven_rubgra,$lista->ven_rubg05,$lista->ven_disexe,  
            $lista->ven_numero,$lista->ven_imputa,$lista->ven_sucursal,$lista->generar,$lista->form_pag,    
            $lista->ven_centro,$lista->ven_provee,$lista->ven_cuenta,$lista->ven_prvnom,$lista->cta_tipofa,  
            $lista->ven_fecha,$lista->ven_totfac,$ven_exenta,$ven_gravad,$ven_iva, 
            $lista->ven_retenc,$lista->ven_aux,$lista->ven_ctrl,$lista->ven_con,$lista->ven_cuota,   
            $lista->ven_fecven,$lista->cant_dias,$lista->origen,$cambio,$valor,  
            $lista->moneda,$exen_dolar,$lista->concepto,$lista->cta_iva,$lista->cta_caja,    
            $lista->tkdesde,$lista->tkhasta,$lista->caja,$lista->ven_disgra,$lista->forma_devo,  
            $lista->ven_cuense,$lista->anular,$lista->reproceso,$lista->cuenta_exe,$lista->usu_ide,
            $lista->ven_timbra, $lista->clieasi,$lista->ventirptip,$lista->ventirpgra,$lista->ventimporteirp,
            $lista->ventirpexe,$lista->irpc,$lista->ivasimplificado,$lista->ven_gdc,$lista->venrubrogdc,
            $lista->ven_gdccosto,$lista->ven_gdcventa,$lista->ven_gdcori,$lista->ven_nronc,$lista->ven_nctim,
            $lista->ventipcli,$lista->ven_banctacte,$lista->ven_bannom
        ));

        if ($lista->migrado != true)
        {
            DB::table('facturas')->where('id',$lista->id)->update(['migrado'=>true]);
        }

    }   

        $nombreArchivo = 'Migracion Ventas-' . date('d-m-Y') . '.xlsx';

        return Excel::download(new MigracionVentasExport($cuerpo), $nombreArchivo);
    
    }

    
    private  function limitar_cadena($cadena, $limite, $sufijo){
        // Si la longitud es mayor que el límite...
        if(strlen($cadena) > $limite){
            // Entonces corta la cadena y ponle el sufijo
            return substr($cadena, 0, $limite) . $sufijo;
        }
        
        // Si no, entonces devuelve la cadena normal
        return $cadena;
    }


    private function  responseFormatDatatable($req)
    {
        $datos = array();
        $data =  new \StdClass;
        
        foreach ($req->formSearch as $key => $value) 
        {
            $n = $value['name'];
            $datos[$value['name']] = $value['value'];
            $data-> $n = $value['value'];
        }

        return $data;
     } 

    public function generarExcelFactura(Request $req)
    {

        $fecha_actual = date("Y-m-d H:i:s");
        $fecha_atras =  date("Y-m-d H:i:s",strtotime($fecha_actual."- 30 days")); 
        $sucursalFiltro = array();

   
              //Si request esta cargado realizar consultas
              $clienteId = $req->input('cliente_id');
              $monedaId  = $req->input('moneda_id');
              $tipoFactura = $req->input('tipoFactura');
              $tipoEstado = $req->input('tipoEstado');
              $vendedorId = $req->input('vendedor_id');
              $numFactura = $req->input('numFactura');
              $numProforma = $req->input('numProforma');
              $factura = Factura::query();
              $factura = $factura->with('cliente',
                                        'tipoFactura',
                                        'estado',
                                        'currency',
                                        'estadoCobro',
                                        'vendedorEmpresa',
                                        'tipoFacturacion');

              if($tipoFactura != ''){
                   $factura = $factura->where('id_tipo_facturacion',$tipoFactura);  
               }
               if($clienteId != ''){
                   $factura = $factura->where('cliente_id',$clienteId);  
               }
                if($monedaId != ''){
                   $factura = $factura->where('id_moneda_venta',$monedaId);  
               }
                if($vendedorId != ''){
                   $factura = $factura->where('vendedor_id',$vendedorId);  
               }
                if($numFactura != ''){
                   $factura = $factura->where('nro_factura',$numFactura);  
               }
                if($numProforma != ''){
                   $factura = $factura->where('id_proforma',$numProforma);  
               }
               if($tipoEstado != ''){
                   $factura = $factura->where('id_estado_factura',$tipoEstado);  
               }

              if($req->input('facturacion_desde_hasta') != ''){
                  $fecha = explode('-', $req->input('facturacion_desde_hasta'));
                    $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                    $hasta = $this->formatoFechaEntrada(trim($fecha[1]));

                   $factura = $factura->whereBetween('fecha_hora_facturacion', array(
                  date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
                  date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                      ));
               }
                if($req->input('vencimiento_desde_hasta') != ''){
                  $fecha = explode('-', $req->input('vencimiento_desde_hasta'));
                    $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                    $hasta = $this->formatoFechaEntrada(trim($fecha[1]));

                   $factura = $factura->whereBetween('vencimiento', array(
                  date('Y-m-d H:i:s',strtotime($desde)),
                  date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                      ));
               }

               if($req->input('ultimo_pago_desde_hasta') != ''){
                $fecha = explode('-', $req->input('ultimo_pago_desde_hasta'));
                  $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                  $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
    
                 $factura = $factura->whereBetween('fecha_ultimo_pago', array(
                date('Y-m-d H:i:s',strtotime($desde)),
                date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                    ));
             }
              $factura = $factura->where('id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);
              $factura = $factura->get();

       
       
        $listado = [];

        foreach ($factura as $key=>$value) {
            $lista = new \StdClass;

            if($value->fecha_ultimo_pago != null){
                $date = explode(' ',$value->fecha_ultimo_pago);   
                $hora =  date('H:i:s',strtotime($date[1]));
                $fecha = $this->formatoFechaSalida($date[0]);
                $value->fecha_ultimo_pago = $fecha;
                $date = null; $hora= null; $fecha = null;
                }

            if($value->fecha_hora_facturacion != null){
                $date = explode(' ',$value->fecha_hora_facturacion);   
                $hora =  date('H:i:s',strtotime($date[1]));
                $fecha = $this->formatoFechaSalida($date[0]);
                $lista->fecha_hora_facturacion = $fecha;
            }
            if($value->vendedor_id != null && $value->vendedor_id != ''){
                $persona = Persona::with('sucursalAgencia')->where('id',$value->vendedor_id )->get();
                if(isset($persona[0]->sucursalAgencia['nombre'])){
                   $lista->sucursal = $persona[0]->sucursalAgencia['nombre'];
                }
            }
            if($value->id_tipo_facturacion == '1'){
                $lista->monto = $value->total_bruto_factura;
            } else {
                $lista->monto = $value->total_neto_factura;
            }

            if(isset($value->cliente->nombre)){
                $lista->cliente = $value->cliente->nombre." ". $value->cliente->apellido;
            }else{
                $lista->cliente = "";
            }

            $lista->nro_factura = $value->nro_factura;

            $lista->proforma = $value->id_proforma;

            $lista->estado = $value->estado->denominacion;

            $lista->cobro = $value->estadoCobro->denominacion;

            $lista->moneda = $value->currency->currency_code;

            $lista->saldo = $value->saldo_factura;

           
        $listado[] = $lista;    
    }
    $cuerpo = array();
        
    foreach($listado as $key=>$lista)
    {
        array_push (
                    $cuerpo, array(
                                    $lista->nro_factura,
                                    $lista->proforma,
                                    $lista->estado,
                                    $lista->cobro,
                                    number_format($lista->monto, 2, ',',''),
                                    $lista->moneda, 
                                     number_format($lista->saldo, 2, ',',''),
                                    $lista->fecha_hora_facturacion, 
                                    $value->fecha_ultimo_pago,
                                    $lista->cliente
                                  )

                    );
    }  
   // dd($cuerpo);

        $nombreArchivo = 'Reporte Facturas-' . date('d-m-Y') . '.xlsx';

        return Excel::download(new ReporteFacturasExport($cuerpo), $nombreArchivo);
    
    }    

    public function generarExcelTickets(Request $req)
    {

            $json = 0;

            if(!is_null($req->all()) && !empty($req->all())){
                
                  $json++;
                  $query = 'SELECT * FROM vw_tickets_pendientes as vw  
                  LEFT JOIN personas p 
                  ON p.id = vw.cliente_id 
                  LEFT JOIN   currency cu 
                  ON cu.currency_id = vw.id_moneda_venta 
                  WHERE vw.id_empresa ='.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa.' ';

                    if($req->input('numFactura') != ''){
                      $query .= "AND nro_factura = '".$req->input('numFactura')."'";
                   }
                    if($req->input('numProforma') != ''){
                       $query .= 'AND id_proforma = '.$req->input('numProforma').' ';
                   } 
                   if($req->input('idCliente') != ''){
                       $query .= 'AND cliente_id = '.$req->input('idCliente').' '; 
                   }
                    if($req->input('idMoneda') != ''){
                       $query .= 'AND id_moneda_venta = '.$req->input('idMoneda').' ';
                   }
                    if($req->input('facturacion_desde_hasta') != ''){
                      $fecha =  explode('-', $req->input('facturacion_desde_hasta'));

                        $desde     = $this->formatoFechaEntrada(trim($fecha[0]));
                        $hasta     = $this->formatoFechaEntrada(trim($fecha[1])); 
                        $desde     = date('Y-m-d H:i:s',strtotime($desde.' 00:00:00'));
                        $hasta     = date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'));

                        $query .= " AND fecha_hora_facturacion BETWEEN '".$desde."' AND '".$hasta."'";
                   }
                    $facturas = DB::select($query);
                  //dd($facturas);

            } else {
               $getFacturaClientes = Factura::with('cliente')->distinct()->get(['cliente_id']);
               $getDivisa = Divisas::where('activo','S')->get();

               $facturas = DB::select('SELECT * FROM vw_tickets_pendientes as vw  LEFT JOIN personas p 
                ON p.id = vw.cliente_id LEFT JOIN   currency cu ON cu.currency_id = vw.id_moneda_venta WHERE vw.id_empresa ='.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

               $getFacturaClientes = DB::select('SELECT distinct cliente_id,nombre FROM vw_tickets_pendientes as vw  LEFT JOIN personas p 
                ON p.id = vw.cliente_id LEFT JOIN   currency cu ON cu.currency_id = vw.id_moneda_venta WHERE vw.id_empresa ='.Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa);

            }//else

            $listado = [];
             foreach ($facturas as  $factura) {

                if($factura->fecha_hora_facturacion != null){
                    $date = explode(' ',$factura->fecha_hora_facturacion);   
                    $hora =  date('H:i:s',strtotime($date[1]));
                    $fecha = $this->formatoFechaSalida($date[0]);
                    $factura->fecha_hora_facturacion = $fecha;
                }   

                $lista = new \StdClass;
                $lista->proforma = $factura->id_proforma;
                $lista->factura = $factura->nro_factura;
                $lista->fecha = $factura->fecha_hora_facturacion;
                $lista->cliente = $factura->nombre." ".$factura->apellido;   
                $lista->moneda = $factura->currency_code;   
                $lista->monto = $factura->total_factura;   
                $lista->saldoF = $factura->saldo_factura;   
                $lista->saldoT = $factura->total_ticket;   
                $lista->senha = $factura->senha;   
                $listado[] = $lista;    
    }
    $cuerpo = array();
        
    foreach($listado as $key=>$lista)
    {
        array_push (
                    $cuerpo, array(
                                    $lista->proforma,
                                    $lista->factura,
                                    $lista->fecha,
                                    $lista->cliente,
                                    $lista->moneda,
                                    number_format($lista->monto, 2, ',',''),
                                    number_format($lista->saldoF, 2, ',',''),
                                    number_format($lista->saldoT, 2, ',',''),
                                    number_format($lista->senha , 2, ',','')
                                  )

                    );
    }  

        $nombreArchivo = 'Reporte de Tickets Pendientes-' . date('d-m-Y') . '.xlsx';

        return Excel::download(new ReporteTicketsPendientesExport($cuerpo), $nombreArchivo);
    }   


    public function generarExcelPagoProveedor(Request $req)
    {
     
       $idProveedor = $req->input('idProveedor');
       $idProforma = $req->input('idProforma');
       $nroFactura = $req->input('nroFactura');
       $codigoConfirmacion =  $req->input('codigoConfirmacion');
       $checkin_desde_hasta =  $req->input('checkin_desde_hasta');
       $proveedor_desde_hasta =  $req->input('proveedor_desde_hasta');
       $idMoneda =  $req->input('idMoneda');
       $tieneFechaProveedor =  $req->input('tieneFechaProveedor');
       $tipo_negociacion = $req->input('tipo_negociacion');
       $numOperacion =  $req->input('numOperacion');
       $idProformaNroFact = '';
        $facturaDetalle = FacturaDetalle::query();
        $facturaDetalle =  $facturaDetalle->with('proveedor',
                                                 'prestador',
                                                 'producto',
                                                 'factura',
                                                 'vendedor',
                                                 'currencyVenta',
                                                 'voucher'
                                                 );

        //EMPRESA DTP
        $facturaDetalle = $facturaDetalle->where('id_empresa',$this->getIdEmpresa());
        $facturaDetalle = $facturaDetalle->where('id_producto','!=','1');
        $facturaDetalle = $facturaDetalle->where('id_producto','!=','9');
        // $facturaDetalle = $facturaDetalle->where('activo','=', true);
        $facturaDetalle = $facturaDetalle->orderBy('id', 'desc');


      if($nroFactura != ''){
        $getFactura = Factura::where('nro_factura',$nroFactura)->first(); 
         if(!empty($getFactura) && !is_null($getFactura)){
            $idProformaNroFact = $getFactura->id_proforma;
         }                     
      } //input nro_factura

         if($idProforma != '' || $idProformaNroFact != ''){
          $idProformaNroFact = ($idProforma != '') ? $idProforma : $idProformaNroFact ;
          $facturaDetalle = $facturaDetalle->where('id_proforma', $idProformaNroFact);
        }

        if($codigoConfirmacion!= ''){
        $facturaDetalle = $facturaDetalle->where('cod_confirmacion',$codigoConfirmacion);  
        }

        if($proveedor_desde_hasta != ''){
           $fecha = explode('-', $proveedor_desde_hasta);
                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));

        $facturaDetalle = $facturaDetalle->whereBetween('fecha_pago_proveedor',
                  array($desde.' 00:00:00',$hasta.' 23:59:59'));  
        }

        if($checkin_desde_hasta != ''){
          $fecha = explode('-', $checkin_desde_hasta);
          $desde = $this->formatoFechaEntrada(trim($fecha[0]));
          $hasta = $this->formatoFechaEntrada(trim($fecha[1]));

        $facturaDetalle = $facturaDetalle->whereBetween('fecha_in',array($desde.' 00:00:00',$hasta.' 23:59:59'));  

        } 
 
        if($idProveedor != 0){
          $facturaDetalle = $facturaDetalle->where('id_proveedor',$idProveedor);
        }
        if($idMoneda != ''){
          $facturaDetalle = $facturaDetalle->where('currency_venta_id',$idMoneda);
        }


         if($tieneFechaProveedor != ''){
            $facturaDetalle = $facturaDetalle->where('pagado_proveedor',$tieneFechaProveedor);
        } else {
           $facturaDetalle = $facturaDetalle->where('pagado_proveedor',true);
        } 

        // //FILTRO APLICADO CUANDO LA BUSQUEDA NO ES AJAX
        // if( $ajax == 0) {
        //   $facturaDetalle = $facturaDetalle->where('pagado_proveedor',false);
        // }

        if($numOperacion != '') {
          $facturaDetalle = $facturaDetalle->where('nro_op',$numOperacion);
        }


         $facturaDetalle =  $facturaDetalle->get();
        $datosFact  = array(); 

       foreach ($facturaDetalle as $key => $detalle) {
      //dd($value);     
 
                         if($tipo_negociacion != ''){

                              $proveedor = Persona::with('plazoPago')->where('id',$detalle->id_proveedor)->get();
                              if(isset($proveedor[0]->plazoPago['cantidad_dias'])){ 
                                $mystring = $proveedor[0]->plazoPago['cantidad_dias'];
                                $findme   = '-';
                                if($tipo_negociacion === 'CONTADO'){
                                    if(intval($mystring) != 0){
                                      unset($facturaDetalle[$key]);
                                      continue;
                                    }  
                                }else if($tipo_negociacion === 'PREPAGO'){
                                    $pos = strpos($mystring, $findme);
                                    if($pos === false){
                                      unset($facturaDetalle[$key]);
                                      continue;
                                    }  
                                 }else if($tipo_negociacion === 'CREDITO'){
                                    $pos = strpos($mystring, $findme);
                                    if($pos !== false || $mystring == 0){
                                        unset($facturaDetalle[$key]);
                                        continue;
                                    }  
                                 }

                               }else{
                                    unset($facturaDetalle[$key]);
                                    continue;
                               
                               }

                           }//if*/

                          $facturaBase = Factura::where('id',$detalle->id_factura)
                                                  ->with('vendedorEmpresa')
                                                  ->first(); 
                                                  
                          if($detalle->vendedor->id != 4987){
                              $detalle->vendedor = $detalle->vendedor->nombre ." ".$detalle->vendedor->apellido;
                          }else{
                              $detalle->vendedor =$facturaBase->usuario->nombre ." ".$facturaBase->usuario->apellido;
                          }                      
                      
                 
                         $detalle->fecha_hora_facturacion = date("d/m/Y",strtotime($detalle->factura['fecha_hora_facturacion']));

                         if($facturaBase->id_estado_factura == 29){

                              $proforma = ProformasDetalle::where('id_proforma', $detalle->id_proforma)
                                                            ->with('voucher')
                                                            ->where('item', $detalle->item)
                                                            ->first();

                              if(isset($proforma->costo_proveedor)){                                
                                   $detalle->monto = $proforma->costo_proveedor;
                              }else{
                                   $detalle->monto = 0;
                              }     


                              $idProforma = $detalle->id_proforma;

                              if($detalle->pagado_proveedor == true){

                              $detalle->pago_proveedor_b = 'SI';
                              } else {
                                 $detalle->pago_proveedor_b = 'NO';
                              }


                              if(!is_null($idProforma) && !empty($idProforma)){

                               $factura = Factura::with('pasajero')->where('id_proforma',$idProforma)->first();
                             

                               if(!is_null($factura) && !empty($factura)){

                                  $estadoFactura = $factura->id_estado_factura;
                                  
                                  if($factura->id_pasajero_principal != 4994){
                                      if(isset($factura->pasajero->nombre)){
                                        $detalle->pasajeroPrincipal = $factura->pasajero->nombre;  
                                      }else{
                                        $detalle->pasajeroPrincipal = "";
                                      }
                                     } else {
                                      if(isset($proforma->voucher[0]->pasajero_principal)){
                                        $detalle->pasajeroPrincipal = $proforma->voucher[0]->pasajero_principal;  
                                      }else{
                                        $detalle->pasajeroPrincipal = "";
                                      }
                                  }    


                                }else{
                                    $detalle->pasajeroPrincipal = "";  
                                }

                                if(!is_null($detalle->fecha_in) && !empty($detalle->fecha_in)){
                                  $fecha = explode(' ',$detalle->fecha_in);
                                  $detalle->fecha_in = $this->formatoFechaSalida($fecha[0]);
                                }//if
                                 if(!is_null($detalle->fecha_out) && !empty($detalle->fecha_out)){
                                  $detalle->fecha_out = $this->formatoFechaSalida($detalle->fecha_out);
                                }//if
                                 if(!is_null($detalle->fecha_pago_proveedor) && !empty($detalle->fecha_pago_proveedor)){
                                  $detalle->fecha_pago_proveedor = $this->formatoFechaSalida($detalle->fecha_pago_proveedor);
                                }//if


                              }//if

                              if($detalle->fecha_pagado_al_proveedor !=""){
                                $fechaPago = date('d/m/Y', strtotime($detalle->fecha_pagado_al_proveedor));
                              }else{
                                $fechaPago = "";
                              }
                              
                              $datosFact[] =    [
                                          $detalle->proveedor->nombre,
                                          $detalle->cod_confirmacion,
                                          $detalle->currencyVenta['currency_code'],
                                          //$detalle->monto,
                                          number_format($detalle->monto, 2, ',',''),
                                          $detalle->producto->denominacion,
                                          $detalle->id_proforma,
                                          $detalle->factura->nro_factura,
                                          $detalle->fecha_hora_facturacion,
                                          $detalle->fecha_in,
                                          $detalle->vendedor,
                                          $detalle->pasajeroPrincipal,
                                          $detalle->fecha_pago_proveedor,
                                          $detalle->pago_proveedor_b,
                                          $fechaPago, 
                                          $detalle->nro_op,
                                   ];
                        

    }
      
    }//foreach
    $listado = [];
    foreach ($datosFact as  $factura) {
                $lista = new \StdClass;
                $lista->proforma = $factura[5];  
                $lista->factura = $factura[6]; 
                $lista->fecha_facturacion = $factura[7]; 
                $lista->checkin = $factura[8];
                $lista->vendedor = $factura[9];
                $lista->proveedor = $factura[0];
                $lista->pasajero = $factura[10];
                $lista->monto = $factura[3];
                $lista->codigo = $factura[1];
                $lista->producto = $factura[4];
                $lista->moneda = $factura[2];   
                $lista->fecha = $factura[11];
                $lista->pagado = $factura[12];  
                $lista->fecha_pagado_al_proveedor = $factura[13];   
                $lista->op = $factura[14]; 
                $listado[] = $lista;   
    }

    $cuerpo = array();

    foreach($listado as $key=>$lista)
    {
        array_push (
                    $cuerpo, array(
                                    $lista->proveedor,
                                    $lista->codigo,
                                    $lista->moneda, 
                                    $lista->monto,
                                    $lista->producto,
                                    $lista->proforma, 
                                    $lista->factura,  
                                    $lista->fecha_facturacion,
                                    $lista->checkin,
                                    $lista->vendedor, 
                                    $lista->pasajero,  
                                    $lista->fecha, 
                                    $lista->pagado, 
                                    $lista->fecha_pagado_al_proveedor, 
                                    $lista->op
                                  )
                    );
    }



            $nombreArchivo = 'Reporte de Detalles Pago Proveedor-' . date('d-m-Y') . '.xlsx';

            return Excel::download(new ReporteDetallesPagoProveedorExport($cuerpo), $nombreArchivo);
    }    
    
    public function generarExcelReporteTickets(Request $req){
        // dd($req->all());
        Session::put('req',$req);
        $ajax = 0;
      $formSearch = $req->all();
       // dd($formSearch);
      $draw = intval($req->draw);
      $start = intval($req->start);
      $length = intval($req->length);
      $cont = 0;
      $filtrarMax = 0;
      $query = "SELECT to_char(fecha_pago_bsp,'DD/MM/YYYY') as fecha_pago_formateo,
                        to_char(fecha_emision,'DD/MM/YYYY') as fecha_emision_formateo ,
                        to_char(check_in,'DD/MM/YYYY') as fecha_checkin_formateo, 
                        to_char(check_out,'DD/MM/YYYY') as fecha_checkout_formateo ,
                        * FROM vw_ticket WHERE id_empresa = ".$this->getIdEmpresa();
        $estados = EstadoFactour::where('id_tipo_estado', '8')->get();  
        $pasajeros = DB::select('SELECT DISTINCT pasajero FROM tickets  ORDER BY pasajero ASC');
        $calendarioBsp = DB::select("SELECT  DISTINCT fecha_pago, to_char(fecha_pago,'DD/MM/YYYY') FROM calendario_bsp order by fecha_pago ASC" );
        $monedas = Divisas::where('activo','S')->orderBy('currency_code')->get();
        $aerolineas = DB::select('select * from get_producto_persona(?)', [1]);
        $tipos_tickets = TipoTicket::orderBy('descripcion')->get();


        foreach ($calendarioBsp as  $value) 
        {
          if($value->fecha_pago !='' && !is_null($value->fecha_pago))
          {
            $value->fecha_pago = $this->formatoFechaSalida($value->fecha_pago);
          } 
          else 
          {
            $value->fecha_pago = '';
          }
        }

 
      if($formSearch['fecha_emision_desdeHasta'] != '')
      {
          $fecha = explode('-',$formSearch['fecha_emision_desdeHasta']);
          $desde = $this->formatoFechaEntrada(trim($fecha[0]));
          $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
          $query .= " AND fecha_emision BETWEEN '".$desde."' AND '".$hasta."' ";
      }

    if(isset($datos->fecha_checkIn_desdeHasta )){
        if($datos->fecha_checkIn_desdeHasta != ''){

          $fechaCheck = explode('-',$datos->fecha_checkIn_desdeHasta);
          $desdeC = $this->formatoFechaEntrada(trim($fechaCheck[0]));
          $hastaC = $this->formatoFechaEntrada(trim($fechaCheck[1]));
          $query .= " AND check_in BETWEEN '".$desdeC."' AND '".$hastaC."' ";
        }
     }   
      if($formSearch['estado_id'] != ''){
        $query .= ' AND id_estado = '.$formSearch['estado_id'];
      }
      if($formSearch['pnr'] != ''){
        $query .= " AND pnr = '".$formSearch['pnr']."' ";
      }
      if($formSearch['nro_ticket'] != ''){
        $query .= " AND numero_amadeus = '".$formSearch['nro_ticket']."' ";
      }
      if($formSearch['pasajero'] != ''){
        $query .= " AND pasajero = '".$formSearch['pasajero']."' ";
      }
      if($formSearch['fecha_pago'] != ''){
          //$fecha     = $this->formatoFechaEntrada($formSearch['fecha_pago']);
        $query .= " AND fecha_pago_bsp BETWEEN '".$formSearch['fecha_pago']."' AND '".$formSearch['fecha_pago']."' ";
      }
      if($formSearch['moneda_id'] != ''){
        $query .= " AND id_currency_costo = ".$formSearch['moneda_id'];
      }
      if($formSearch['aerolinea_id'] != ''){
        $query .= " AND id_proveedor = ".$formSearch['aerolinea_id'];
      }
      if($formSearch['aerolinea_id'] != ''){
        $query .= " AND id_proveedor = ".$formSearch['aerolinea_id'];
      }
      if($formSearch['id_proforma'] != ''){
        $query .= " AND id_proforma = ".$formSearch['id_proforma'];
      }
      if($formSearch['nro_factura'] != ''){
        $query .= " AND nro_factura = ".$formSearch['nro_factura'];
      }
      if($formSearch['tipo_ticket'] != ''){
        $query .= " AND id_tipo_ticket = ".$formSearch['tipo_ticket'];
      }
       if($formSearch['id_vendedor_empresa'] != ''){
        $query .= " AND id_usuario_proforma = ".$formSearch['id_vendedor_empresa'];
      }
        if($formSearch['grupo_id'] != ''){
        $query .= " AND id_grupo = ".$formSearch['grupo_id'];
      }
      if($formSearch['mostrar_precio'] == 'N'){
        $query .= " AND total_venta <> 0";
      }

      // dd($query);

      $tickets = DB::select($query);
      // dd($tickets);

       $listado = array();

       foreach ($tickets as $key => $ticket) 
       {

            //dd($ticket);
            if ($ticket->currency_code == 'PYG') {
                $costo = (int) ($ticket->total_costo);
                $venta = (int) ($ticket->total_venta);
            }
            else
            {
                $costo =  $ticket->total_costo;
                $venta = $ticket->total_venta;
            }

            if(isset($ticket->pro_per_n)){ 
                $proveedor = $ticket->pro_per_n.' '.$ticket->pro_ape_a;
            }else{
                $proveedor ="";
            }

            if(isset($ticket->cliente_n)){ 
                $cliente = $ticket->cliente_n;
            }else{
                $cliente ="";
            }

            if(isset($ticket->codigo_iata_3d)){ 
                $aerolinea = $ticket->nombre_proveedor." - ".$ticket->codigo_iata_3d;
            }else{
                $aerolinea = $ticket->nombre_proveedor;
            }

            if(isset($ticket->usuario_amadeus_n)){ 
                $usuario_amadeus = " | ".$ticket->usuario_amadeus_n.' '.$ticket->usuario_amadeus_a.'-'.$ticket->usuario_amadeus;
            }else{
                $usuario_amadeus = "";
            }

            $check_in_out = "";
            if($ticket->fecha_checkin_formateo != ""){ 
                $check_in_out .= $ticket->fecha_checkin_formateo ;
            }else{
                $check_in_out .= "";
            }
            if($ticket->fecha_checkout_formateo != ""){ 
                $check_in_out .= " - ".$ticket->fecha_checkout_formateo;
            }else{
                $check_in_out .= "";
            }

            $usaurio_total = $ticket->usuario."".$usuario_amadeus;

            $vendedor = $ticket->vendedor_nombre." ".$ticket->vendedor_apellido;

            $lista = new \StdClass;
            $lista->nro_ticket = $ticket->numero_amadeus;
            $lista->cliente = $cliente;
            $lista->pasajero = $ticket->pasajero; 
            $lista->proveedor = $aerolinea;
            $lista->fecha_emision = $ticket->fecha_emision;
            $lista->usuarioProforma = $proveedor;
            $lista->proforma = $ticket->id_proforma;
            $lista->usuario = $usaurio_total;
            $lista->usuario_pedido = $vendedor;
            $lista->pago = $ticket->fecha_pago_bsp;
            $lista->tipo = $ticket->codigo_amadeus;
            $lista->origen = $ticket->descripcion;
            $lista->estado = $ticket->estado;
            $lista->pnr = $ticket->pnr;
            $lista->checkin_out = $check_in_out;
            $lista->nro_factura = $ticket->nro_factura;
            $lista->moneda = $ticket->currency_code;
            $lista->costo = $costo;
            $lista->venta = $venta;
            $lista->nro_tarjeta = $ticket->nro_tarjeta;
            $lista->origens = $ticket->origen;
            $lista->total_comision = $ticket->total_comision;//GES-962
            $lista->porcentaje_ganancia = $ticket->porcentaje_ganancia;//GES-962
            $lista->ganancia_venta = $ticket->ganancia_venta;//GES-962
            $lista->renta_extra = $ticket->renta_extra;//GES-962
            $lista->incentivo_vendedor_agencia = $ticket->incentivo_vendedor_agencia;//GES-962
            $lista->renta = $ticket->renta;//GES-962
            $listado[] =  $lista;
       }

        $cuerpo = array();
        
        foreach($listado as $key=>$lista)
        {

            array_push($cuerpo, array($lista->nro_ticket,$lista->cliente,$lista->pasajero,$lista->proveedor,$lista->fecha_emision,$lista->usuario,$lista->proforma,$lista->checkin_out,$lista->usuarioProforma,$lista->nro_tarjeta,$lista->pago,$lista->tipo,$lista->origen,$lista->estado,$lista->nro_factura,$lista->pnr,$lista->moneda,$lista->costo,$lista->venta,$lista->usuario_pedido,$lista->origens,
        $lista->total_comision,$lista->porcentaje_ganancia,$lista->ganancia_venta,$lista->renta_extra,$lista->incentivo_vendedor_agencia,$lista->renta));        
        }

            $nombreArchivo = 'Reporte Tickets-' . date('d-m-Y') . '.xlsx';

            return Excel::download(new ReporteTicketsExport($cuerpo), $nombreArchivo);
    }


    private function getIdEmpresa(){
        return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
    }

        public function generarExcelRecibos(Request $req)
    {
        $recibos = new Recibo; 
        $recibos = $recibos->with(['SucursalEmpresa']);
        $recibos = $recibos->with(['usuario_generado' => function($query){
            $query->selectRaw("substring(concat(nombre,apellido) for 10)||'...' as full_name,*");
        },'cliente' => function($query){
            $query->selectRaw("substring(concat(nombre,apellido,denominacion_comercial) for 10)||'...' as full_name,*");
        }, 'formaCobro.formaCobroReciboDetalles.forma_cobro','moneda', 'sucursal', 'estado','tipo_recibo','sucursalEmpresa']);

        $recibos = $recibos->where('id_empresa',$this->getIdEmpresa());

        //CONTADOR DE RETENCIONES
        $recibos = $recibos->withCount(['detalle'=> function($query){
            $query->whereNotNull("importe_retencion");
        }]);

        $recibos = $recibos->selectRaw("TO_CHAR(fecha_hora_creacion,'DD/MM/YYYY') as fecha_creacion_format,
                                                        substring(concepto for 10)||'...' as concepto_cortado,
                                                        TO_CHAR(fecha_hora_cobro,'DD/MM/YYYY') as fecha_hora_cobro_format,
                                                        CASE
                                                            WHEN id_estado = 40 THEN 2
                                                            WHEN id_estado = 56 THEN 3
                                                            WHEN id_estado = 55 THEN 4
                                                            WHEN id_estado = 31 THEN 5
                                                            ELSE 0
                                                        END AS orden,* ");
        if($req->input('id_cliente')){
            $recibos = $recibos->where('id_cliente',$req->input('id_cliente'));
        }
        if(null !== $req->input('id_estado')){
            if(count($req->input('id_estado')) > 0){
                $recibos = $recibos->whereIn('id_estado',$req->input('id_estado'));
            }
        }   
        if($req->input('id_moneda')){
            $recibos = $recibos->where('id_moneda',$req->input('id_moneda'));
        }

        if(!$req->input('id_cierre')){
            if($req->input('periodo')){
                $fechaPeriodo = explode(' - ', $req->input('periodo'));
                $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
                $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
                $recibos = $recibos->whereBetween('fecha_hora_creacion',array($desde,$hasta));
            }
        }else{
            $recibos = $recibos->where('id_cierre',$req->input('id_cierre'));
        }	


        if($req->input('num_recibo')){
            $recibos = $recibos->where('nro_recibo','like', '%'.trim($req->input('num_recibo')).'%');
        }

        $recibos = $recibos->orderBy('nro_recibo','DESC');
        $recibos = $recibos->get();
        if($req->input('tipo') == 1){
            $cuerpo = array();
            foreach($recibos as $key=>$lista)
            {
                $lista->load('SucursalEmpresa');
              $denominacionSucursal = $lista->sucursalEmpresa->denominacion;
          
               
                array_push (
                            $cuerpo, array(
                                            $lista->cliente->full_name,
                                            $lista->nro_recibo,
                                            $lista->id_estado == 55 ? number_format(0, 2, ',','') : number_format($lista->importe, 2, ',',''),
                                            $lista->tipo_recibo->denominacion,
                                            $lista->estado->denominacion,
                                            $lista->moneda->currency_code,
                                            $lista->fecha_creacion_format,
                                            $lista->concepto,
                                            $lista->fecha_hora_cobro_format,
                                            $lista->usuario_generado->full_name,
                                            $denominacionSucursal
                                            //$lista->sucursal->denominacion
                                          )
    
                            );
            }  
    

                    $nombreArchivo = 'Reporte de Recibos-' . date('d-m-Y') . '.xlsx';

                    return Excel::download(new ReporteRecibosExport($cuerpo), $nombreArchivo);
        }else{
            $resultado = [];
            $contador = 0;
            foreach($recibos as $key=>$recibo){
             
                $denominacionSucursal = $recibo->sucursalEmpresa->denominacion;
               
                /*echo '<pre>';
                print_r($recibo);*/
                if(empty($recibo['formaCobro']['formaCobroReciboDetalles'])){
                    $tipo_id ="";
                    $tipo_n ="";
                    $fecha = '';
                    $nombre = '';
                    $cliente = '';
                    $resultado[$contador]['id'] = $recibo->id;
                    if($recibo->cliente->full_name !== null){
                        $cliente = $recibo->cliente->full_name;
                    }    
                    $resultado[$contador]['cliente'] = $cliente;
                    $resultado[$contador]['nro_recibo'] = $recibo->nro_recibo;
                    if($recibo->tipo_recibo !== null){
                        $tipo_id = $recibo->tipo_recibo->id ;
                        $tipo_n =$recibo->tipo_recibo->denominacion;
                    }	
                    $resultado[$contador]['tipo_recibo_id'] = $tipo_id;
                    $resultado[$contador]['tipo_recibo_denominacion'] = $tipo_n;
                    $resultado[$contador]['detalle_count'] = $recibo->detalle_count;
                    $resultado[$contador]['orden'] = $recibo->orden;
                    $resultado[$contador]['estado'] = $recibo->estado->denominacion;
                    $resultado[$contador]['moneda'] = $recibo->moneda->currency_code;
                    $resultado[$contador]['fecha_creacion_format'] = $recibo->fecha_creacion_format;
                    $resultado[$contador]['concepto_cortado'] = $recibo->concepto;
                    if($recibo->fecha_hora_cobro_format !== null){
                        $fecha= $recibo->fecha_hora_cobro_format;
                    }
                    $resultado[$contador]['fecha_hora_cobro_format'] = $fecha;
                    $resultado[$contador]['forma_cobro'] = 'NO POSEE';
                    if($recibo->usuario_generado !== null){
                        $nombre = $recibo->usuario_generado->full_name;
                    }
                    $resultado[$contador]['usuario_generado'] = $nombre;
                    if($recibo->id_estado == 55){
                        $resultado[$contador]['importe'] = 0;
                    } else {
                        $resultado[$contador]['importe'] = $recibo->importe;
                    }
                    
                   
                    $resultado[$contador]['banco'] = "";
                    $resultado[$contador]['nro_comprobante'] = "";
                    $resultado[$contador]['sucursal'] = $denominacionSucursal;
                    $contador++;
                }else{
                    foreach($recibo['formaCobro']['formaCobroReciboDetalles'] as $keys=>$formaPago){
                        if($formaPago->activo == 1){
                            $tipo_id ="";
                            $tipo_n ="";
                            $fecha = '';
                            $nombre = '';
                            $cliente = '';
                            $banco_cabecera = "";
                            $numero_cuenta = "";
                            $resultado[$contador]['id'] = $recibo->id;
                            if($recibo->cliente->full_name !== null){
                                $cliente = $recibo->cliente->full_name;
                            }    
                            $resultado[$contador]['cliente'] = $cliente;
                            $resultado[$contador]['nro_recibo'] = $recibo->nro_recibo;
                            if($recibo->tipo_recibo !== null){
                                $tipo_id = $recibo->tipo_recibo->id ;
                                $tipo_n =$recibo->tipo_recibo->denominacion;
                            }	
                            $resultado[$contador]['tipo_recibo_id'] = $tipo_id;
                            $resultado[$contador]['tipo_recibo_denominacion'] = $tipo_n;
                            $resultado[$contador]['detalle_count'] = $recibo->detalle_count;
                            $resultado[$contador]['orden'] = $recibo->orden;
                            $resultado[$contador]['estado'] = $recibo->estado->denominacion;
                            $resultado[$contador]['moneda'] = $formaPago->moneda->currency_code;
                            $resultado[$contador]['fecha_creacion_format'] = $recibo->fecha_creacion_format;
                            $resultado[$contador]['concepto_cortado'] = $recibo->concepto;
                            if($recibo->fecha_hora_cobro_format !== null){
                                $fecha= $recibo->fecha_hora_cobro_format;
                            }
                            $resultado[$contador]['fecha_hora_cobro_format'] = $fecha;
                            $resultado[$contador]['forma_cobro'] = $formaPago['forma_cobro']['denominacion'];
                            if($recibo->usuario_generado !== null){
                                $nombre = $recibo->usuario_generado->full_name;
                            }
                            $resultado[$contador]['usuario_generado'] = $nombre;
                            $resultado[$contador]['importe'] = $formaPago['importe_pago'];
                            if(isset($formaPago->banco['banco_cabecera']['nombre'])){
                                $banco_cabecera = $formaPago->banco['banco_cabecera']['nombre'];
                            }	
                            if(isset($formaPago->banco->numero_cuenta)){
                                $numero_cuenta = $formaPago->banco->numero_cuenta; 
                            }	
                            $resultado[$contador]['banco'] = $banco_cabecera." ".$numero_cuenta;
                            if($formaPago['nro_comprobante'] != ""){
                                $nroComprobante = $formaPago['nro_comprobante'];
                            }else{
                                $nroComprobante = $formaPago['tarjeta_num_operacion'];
                            }
                            $resultado[$contador]['nro_comprobante'] = $nroComprobante;
                            $resultado[$contador]['sucursal'] = $denominacionSucursal;
        
                            $contador++;
                        }
                    }	
                }

            }	
            $cuerpo = array();
            foreach($resultado as $key=>$lista)
            {
                array_push (
                            $cuerpo, array(
                                            $lista['cliente'],
                                            $lista['nro_recibo'],
                                            number_format($lista['importe'], 2, ',',''),
                                            $lista['tipo_recibo_denominacion'],
                                            $lista['estado'],
                                            $lista['moneda'],
                                            $lista['fecha_creacion_format'],
                                            $lista['concepto_cortado'],
                                            $lista['forma_cobro'],
                                            $lista['nro_comprobante'],
                                            $lista['banco'],
                                            $lista['fecha_hora_cobro_format'],
                                            $lista['usuario_generado'],
                                            $lista['sucursal']
                                          )
    
                            );
            }  
    
                    $nombreArchivo = 'Reporte de Recibos-' . date('d-m-Y') . '.xlsx';

                    return Excel::download(new ReporteRecibosComprobanteExport($cuerpo), $nombreArchivo);
                ////////////////////////////////////////////////////////////////////////////////////////////
        }
        
    }   




    public function generarExcelReportePendiente(Request $req){
         $query = "SELECT to_char(f.vencimiento,'DD/MM/YYYY') vencimiento, 
                  to_char(f.fecha_hora_facturacion, 'DD/MM/YYYY hh:mm') fecha_hora_facturacion,
                   to_char(f.check_in,'DD/MM/YYYY') check_in,
                  f.nro_factura,
                  lv.saldo AS saldo_factura,
                  f.vencimiento - CURRENT_DATE  dias,
                  f.check_in - CURRENT_DATE as diferencia_check_in,
                  CASE 
                        WHEN f.vencimiento > CURRENT_DATE  THEN 'A VENCER'
                        ELSE 'VENCIDO'
                  END as venc_estado,
                   get_monto_factura(f.id) as total_factura,
                  p.nombre pasajero_n,
                  p.apellido pasajero_a,
                  c.nombre cliente_n,
                  c.id_sucursal_cartera,
                  c.id_tipo_persona,
                  c.apellido cliente_a,
                  c.id cliente_id,
                  car.nombre cartera_n,
                  car.denominacion_comercial cartera_dc,
                  cu.currency_code moneda,
                  f.pasajero_factour,
                  cu.currency_id,
                  u.nombre usuario_nombre,
                  u.apellido usuario_apellido,
                  get_monto_cotizado(get_monto_factura(f.id), f.id_moneda_venta, 143) as totalDolar,
                  get_monto_cotizado(f.saldo_factura, f.id_moneda_venta, 143) as saldoDolar,
                  u.id usuario_id,
                  vrc.id_comercio_empresa,
                  ce.descripcion as nombrecomercio,
                (SELECT COUNT(*) 
                        FROM facturas_detalle 
                        WHERE activo = true and id_producto IN(1,9) and id_proforma = f.id ) as ticketCount                  
                 FROM facturas f 
                  LEFT JOIN libros_ventas lv ON lv.id_documento = f.id
                  LEFT JOIN personas u ON u.id = f.vendedor_id
                  LEFT JOIN personas c ON c.id = f.cliente_id
                  LEFT JOIN personas p ON p.id = f.id_pasajero_principal
                  LEFT JOIN currency cu ON cu.currency_id = f.id_moneda_venta
                  LEFT JOIN personas car ON car.id = c.id_sucursal_cartera
                  LEFT JOIN ventas_rapidas_cabecera vrc ON f.id_venta_rapida = vrc.id
                  LEFT JOIN comercio_empresa ce ON vrc.id_comercio_empresa = ce.id
                  WHERE f.id_estado_factura = 29 
                  AND f.id_empresa = ".$this->getIdEmpresa()." 
                  AND f.id_estado_cobro = 31 
                  AND f.saldo_factura > 0";

                if($req->input("cliente_id") != ''){
                  $query.= ' AND c.id = '.$req->input("cliente_id");
                }
                if($req->input("moneda_id") != ''){
                  $query.= ' AND cu.currency_id = '.$req->input("moneda_id");
                }
                if($req->input("estado") != ''){
                   $query.= " AND CASE 
                        WHEN f.vencimiento > CURRENT_DATE  THEN 'A_VENCER'
                        ELSE 'VENCIDO'
                          END  = '".$req->input("estado")."' ";
                }

                if($req->input("sucursal_id") != ''){
                  $query.= ' AND c.id_sucursal_cartera = '.$req->input("sucursal_id");
                }

                if($req->input('numFactura')){
                  $query.= " AND f.nro_factura  LIKE '%".$req->input("numFactura")."%'";  
                }

                 if($req->input("comercio_id") != ''){
                    if($req->input("comercio_id") == 0){
                        $query.= ' AND vrc.id_comercio_empresa IS NULL';
                    }else{
                        $query.= ' AND vrc.id_comercio_empresa = '.$req->input("comercio_id");
                    }
                  }

                if($req->input("tipo_persona_id") != ''){
                    /*echo '<pre>';
                    print_r($req->input("tipo_persona_id"));*/
                    $condiciones = '';
                    foreach($req->input("tipo_persona_id") as $key=>$tipo){
                      if($key == 0){
                            $condiciones .= $tipo;
                        }else{
                            $condiciones .= ','.$tipo;
                        }

                    }

                   $query.= ' AND c.id_tipo_persona IN ('.$condiciones.')';

                }

                if($req->input("usuario_id") != ''){
                  $query.= ' AND u.id = '.$req->input("usuario_id");
                }

         $facturas = DB::select($query);

         $listado = array();   
         foreach ($facturas as $key => $factura) 
         {
            $lista = new \StdClass;
            $lista->cliente = $factura->cliente_n." ".$factura->cliente_a;
            $lista->fecha_emision = $factura->fecha_hora_facturacion;
            $lista->vencimiento = $factura->vencimiento;
            $lista->checkin = $factura->check_in;
           // $lista->cartera = $factura->cartera_n." ".$factura->cartera_dc ;
            $lista->nro_factura = $factura->nro_factura;
            if($factura->pasajero_factour == ""){
                $lista->pasajero = $factura->pasajero_n." ".$factura->pasajero_a;
            }
            else{
                $lista->pasajero = $factura->pasajero_factour;
            }
            $lista->moneda = $factura->moneda;
            $lista->total = str_replace('.', ',',$factura->total_factura);
            $lista->totalCotizado = str_replace('.', ',',$factura->totaldolar);
            $lista->saldo = str_replace('.', ',',$factura->saldo_factura);
            $lista->saldoCotizado = str_replace('.', ',',$factura->saldodolar);
            $lista->dias = $factura->dias;
            $lista->estado = $factura->venc_estado;
            if($factura->nombrecomercio == null){
                $lista->nombrecomercio = 'DTP';
            }else{
               $lista->nombrecomercio = $factura->nombrecomercio;     
            }
            
            $lista->usuario = $factura->usuario_nombre.' '.$factura->usuario_apellido;

            $diferencia =  str_replace("days", "", $factura->diferencia_check_in);

            if($factura->ticketcount > 0){
                $descripcion = 'TICKET';
            }else{
                if ($factura->dias >0){
                    $descripcion = 'OK';                   
                }
                if($factura->dias <= 0 && intval($diferencia) >= 7){
                    $descripcion = 'VENCIDO'; 
                }
                if($factura->dias <= 0 && intval($diferencia)< 7 && intval($diferencia) >= 0){
                    $descripcion = 'VENCIDO';
                }

                if($factura->dias <= 0 && intval($diferencia)< 0){
                    $descripcion = 'URGENTE';
                }
            }
            $lista->descripcion = $descripcion;
                
            $listado[] =  $lista;
       }

        $cuerpo = array();
        
        foreach($listado as $key=>$lista)
        {
            array_push($cuerpo, array(
            $lista->descripcion, $lista->cliente,$lista->fecha_emision,$lista->vencimiento,$lista->checkin,/*$lista->cartera,*/$lista->nro_factura,$lista->pasajero,$lista->usuario,$lista->moneda,$lista->saldo,$lista->dias,$lista->estado,$lista->nombrecomercio));
        }
      
            $nombreArchivo = 'Reporte Facturas Pendientes-' . date('d-m-Y') . '.xlsx';

            return Excel::download(new ReporteFacturasPendientesExport($cuerpo), $nombreArchivo); 

    } 

   public function generarExcelReporteBSPCobranza(Request $req){
          $query = "SELECT * FROM vw_ticket WHERE id_empresa = ".$this->getIdEmpresa();

           
       
         

          if($req->input('fecha_emision_desdeHasta'))
          {
              $fecha = explode('-',$req->input('fecha_emision_desdeHasta'));

              $desde = $this->formatoFechaEntrada(trim($fecha[0]));
              $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
              $query .= " AND fecha_emision BETWEEN '".$desde."' AND '".$hasta."' ";
          }
          if($req->input('estado_id')){
            $query .= ' AND id_estado = '.$req->input('estado_id');
          }
          if($req->input('fecha_pago')){
            $query .= " AND fecha_pago_bsp BETWEEN '".$req->input('fecha_pago')."' AND '".$req->input('fecha_pago')."' ";
          }
          if($req->input('moneda_id')){
            $query .= " AND id_currency_costo = ".$req->input('moneda_id');
          }
          if($req->input('aerolinea_id')){
            $query .= " AND id_proveedor = ".$req->input('aerolinea_id');
          }
          if($req->input('estado_cobro')){
            $query .= " AND estado_cobro_ticket = '".$req->input('estado_cobro')."' ";
          }

          $tickets = DB::select($query);

          foreach ($tickets as $key => $ticket) 
             {

                $lista = new \StdClass;
                $lista->id = $ticket->id_aerolinea;
                if($ticket->estado_cobro_ticket == 'NO'){
                    $estado  = 'SI';
                }else{
                    $estado  = 'NO';
                }
                        //se agrega columna void para ver si es anulado 
                $void= '';
                if($ticket->id_estado == 33 ){
                    $void='VOID';
                }else{
                    $void='';
                }
                $entrega =0;
                if($ticket->entrega>=$ticket->senha123){
                 $entrega='0';
                }else{
                    $entrega = $ticket->senha123 - $ticket->entrega;
                }

                $lista->estado_cobro = $estado;
                $lista->nro_factura = $ticket->nro_factura;
                $lista->proforma = $ticket->id_proforma;
                $lista->cliente_ag = $ticket->cliente_n.' '.$ticket->cliente_a;
                $lista->nro_ticket = $ticket->numero_amadeus;
                $lista->nombre_pax = $ticket->pasajero;
                $lista->neto_pagar = str_replace('.', ',',$ticket->total_costo);
                $lista->usuario_pedido = $ticket->vendedor_nombre." ".$ticket->vendedor_apellido;
                $lista->usuario_proforma = $ticket->usuario;
                $lista->usuario_emision = $ticket->usuario_amadeus_n." ".$ticket->usuario_amadeus_a;
                $lista->id_estado = $void;
                $lista->senha123 = str_replace('.', ',',$ticket->senha123);
                //$lista->entrega = number_format( 2, ',','' $entrega); 
                if($lista->moneda_id=111){
                    $lista->entrega = number_format($entrega, 0, ',','');
                }else{ 
                    $lista->entrega = number_format($entrega, 2, ',','');
                } 
                $lista->fecha_emision = $ticket->fecha_emision;
                $lista->fecha_pago_bsp = $ticket->fecha_pago_bsp;
                $lista->currency_code = $ticket->currency_code;
                $lista->cotizacion_jurcaip = $ticket->cotizacion_jurcaip;
                $listado[] =  $lista;
             }

         /* echo "<pre>";
          print_r($listado);*/

            $cuerpo = array();
            
            foreach($listado as $key=>$lista)
            {
                array_push($cuerpo, array($lista->estado_cobro,$lista->nro_factura,$lista->proforma,
               $lista->cliente_ag,$lista->nro_ticket,$lista->nombre_pax,$lista->neto_pagar,$lista->usuario_pedido,$lista->usuario_proforma,$lista->id_estado,$lista->senha123,$lista->entrega,$lista->usuario_emision,$lista->fecha_emision,$lista->fecha_pago_bsp,$lista->currency_code,$lista->cotizacion_jurcaip));
            }


            $nombreArchivo = 'Reporte BSP Cobranza-' . date('d-m-Y') . '.xls'; 

            return Excel::download(new ReporteBSPCobranzaExport($cuerpo), $nombreArchivo);
   }

    public function generarExcelReporteCierre(Request $req){
        $cierreRecibos = new CierreRecibo;
        $cierreRecibos = $cierreRecibos->with([
                                                'recibo',
                                                'recibo.formaCobroCabecera.formaCobroReciboDetalles'=> function($query) use($req){
                                                    if($req->input('idCobro')){
                                                        $query->where('id_tipo_pago','=', $req->input('idCobro'));  
                                                    }
                                                    if($req->input('moneda')){
                                                        $query->where('id_moneda','=', $req->input('moneda'));  
                                                    }                                                
                                                },
                                                'recibo.formaCobroCabecera.formaCobroReciboDetalles.moneda'
                                              ]);

    if($req->input('fecha_cierre')) {
         $fecha_cierre =$this->formatoFechaEntrada($req->input('fecha_cierre'));
         $ids = Cierre::where('fecha_cierre', $fecha_cierre)->where('id_empresa', $this->getIdEmpresa())->pluck('id');
         $cierreRecibos = $cierreRecibos->whereIn('id_cierre', $ids);
         } else {
       $cierreRecibos = $cierreRecibos->where('id_cierre',  $req->input('id_cierre'));
       }
           $cierreRecibos = $cierreRecibos->get();

        $listado = [];
        $totalUs = 0;
        $totalGs = 0;
        $data = [];
        if(isset($cierreRecibos)){ 
            foreach($cierreRecibos as $key=>$cierreRecibo){
                if(isset($cierreRecibo->recibo->id)){
                    foreach($cierreRecibo->recibo->formaCobroDetalle as $key2=>$formaCobroDetalle){
                        if(isset($formaCobroDetalle->id_tipo_pago)){
                            if($formaCobroDetalle->id_tipo_pago == $req->input('idCobro')){
                                if($formaCobroDetalle->id_moneda == $req->input('moneda')){
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['id'] = $cierreRecibo->recibo->id;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['cliente'] = $cierreRecibo->recibo->cliente->nombre;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['nro_recibo'] = $cierreRecibo->recibo->nro_recibo; 
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['total_pago'] = $cierreRecibo->recibo->total_pago;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['moneda'] = $cierreRecibo->recibo->moneda->currency_code;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['concepto'] = $cierreRecibo->recibo->concepto;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['cotizacion'] = $cierreRecibo->recibo->cotizacion;

                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['forma_cobro_id'] = $formaCobroDetalle->forma_cobro->id;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['forma_cobro'] = $formaCobroDetalle->forma_cobro->denominacion;
                                        $cheque ='';
                                        if($formaCobroDetalle->id_cheque != ''){
                                            $cheques = Cheque::where('id', $formaCobroDetalle->id_cheque)->get();
                                            if(isset($cheques[0]->cuenta)){
                                                $cheque = $cheques[0]->cuenta;
                                            }
                                        }

                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['cheque'] = $cheque;

                                        $bancoPlaza ='';
                                        if($formaCobroDetalle->id_banco_plaza != 0){
                                            $bancosPlaza = BancoPlaza::where('id', $formaCobroDetalle->id_banco_plaza)->get();
                                            if(isset($bancosPlaza[0]->nombre)){
                                                $bancoPlaza = $bancosPlaza[0]->nombre;
                                            }
                                        }
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['banco_plaza'] = $bancoPlaza;
                                        
                                        $banco ='';
                                        $nomBanck = '';
                                        if($formaCobroDetalle->id_banco_detalle != 0){
                                            $bancos = BancoDetalle::with('banco_cab')
                                                                    ->where('id', $formaCobroDetalle->id_banco_detalle)->get();
                                            if(isset($bancos[0]->numero_cuenta)){
                                                $banco =  $bancos[0]->numero_cuenta;
                                            }

                                            if(isset($bancos[0]->numero_cuenta)){
                                                $nomBanck = $bancos[0]->banco_cab->nombre;
                                            }   
                                        }
                                        $bancos = $nomBanck." ".$banco;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['banco'] = $bancos;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['nro_comprobante'] = $formaCobroDetalle->nro_comprobante;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['importe_pago'] = $formaCobroDetalle->importe_pago;
                                        $data[$cierreRecibo->recibo->id]['detalles'][$key2]['id_moneda'] = $formaCobroDetalle->id_moneda;
                                }       
                            }
                        }   
                    }
                }   
            }
        }   
         $listado = [];
        foreach($data as $key=>$datos){
            foreach($datos['detalles'] as $key2=>$detalle){
                $lista = new \StdClass;
                $lista->nro_recibo = $detalle['nro_recibo'];
                $lista->cliente = $detalle['cliente'];
                $lista->detalles = $detalle['concepto'];
                $lista->moneda = $detalle['moneda'];
                $lista->cotizacion =$detalle['cotizacion'];
                $lista->total_recibo = str_replace('.', ',',$detalle['total_pago']);
                $lista->forma_pago = $detalle['forma_cobro'];
                $lista->comprobante = $detalle['nro_comprobante'];
                if($detalle['id_moneda'] == 111){
                     $lista->monedaPago = 'PYG';
                }else{
                     $lista->monedaPago = 'US';
                }
                $lista->importe = str_replace('.', ',',$detalle['importe_pago']);
                $listado[] =  $lista;
            }
        }

        $cuerpo = array();
        
        foreach($listado as $key=>$lista)
        {
            array_push($cuerpo, array($lista->nro_recibo,$lista->cliente,$lista->detalles,
           $lista->moneda,$lista->cotizacion,$lista->total_recibo,$lista->forma_pago,$lista->comprobante,$lista->monedaPago,$lista->importe));
        }


        $nombreArchivo = 'Gestion Cobros-' . date('d-m-Y') . '.xls';
        return Excel::download(new GestionCobrosExport($cuerpo), $nombreArchivo);



    }    

    public function generarExcelReporteBSP(Request $req){
    
       $query = "SELECT to_char(fecha_emision, 'DDMONYY') fecha_formateada,
                      CASE
                      WHEN comision > 0 THEN ROUND(((comision *100) / ( 
                        CASE 
                          WHEN facial > 0 THEN facial
                          ELSE 1
                          end
                          ) 
                          )::decimal, 2)
                          ELSE 0
                          END as porcentaje_comision, *
                      FROM vw_ticket
                      WHERE id_empresa = ".$this->getIdEmpresa()." 
                        AND id_proveedor != 8391
                        AND (id_grupo is null or id_grupo = 0)";

        if($req->id_aerolinea != ''){
            $query .= " AND id_proveedor = ".$req->id_aerolinea;
        }
        if($req->fecha_pago != ''){
            $query .= " AND fecha_pago_bsp = '".$req->fecha_pago."' ";
        }
        if($req->id_currency !=''){
            $query .= " AND id_currency_costo = ".$req->id_currency."";
        }

        if($req->periodo_desde_hasta !=''){
             $fecha =  explode('-', $req->periodo_desde_hasta);

                $desde     = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta     = $this->formatoFechaEntrada(trim($fecha[1])); 
                $desde     = date('Y-m-d H:i:s',strtotime($desde.' 00:00:00'));
                $hasta     = date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'));
            
            $query .= " AND fecha_emision BETWEEN '".$desde."' AND '".$hasta."'";
        }

        $ticket = DB::select($query." ORDER BY numero_amadeus ASC");

         $listado = array();   
         $valor_transac = 0;
         $valor_tarifa = 0; 
         $impuestos =  0;
         $yq = 0;
         $py = 0; 
         $pen = 0; 
         $porcentaje = 0; 
         $comision = 0; 
         $com = 0;
         $neto = 0; 
         foreach ($ticket as $key => $ticket) 
         {

            $lista = new \StdClass;
            $lista->cia = $ticket->id_aerolinea;
            $lista->trnc = $ticket->codigo_amadeus;
            $lista->documento = $ticket->numero_amadeus;
            $lista->emision = date('d/m/Y', strtotime($ticket->fecha_emision));
            $lista->fop = $ticket->tipo_pago;
            $lista->valor_transac = str_replace('.', ',',$ticket->total_venta);
            $lista->valor_tarifa = str_replace('.', ',',$ticket->facial);
            $lista->imp = str_replace('.', ',',$ticket->impuestos);
            $lista->yq = str_replace('.', ',',$ticket->tasas_yq);
            $lista->py = str_replace('.', ',',$ticket->iva);
            $lista->pen = str_replace('.', ',',$ticket->penalidad);
            $lista->porcentaje = str_replace('.', ',',$ticket->porcentaje_comision);
            if($ticket->pago_tc == null){
                $lista->comision = str_replace('.', ',',$ticket->comision);
            }else{
                $lista->comision = '-'.str_replace('.', ',',$ticket->comision);
            }
            $lista->com = str_replace('.', ',',$ticket->iva_comision);
            $totalneta = 0;
            if($ticket->id_estado != 33){
              if($ticket->total_venta != 0){
                if($ticket->tipo_pago == 'CC'){
                  $totalneta = 0 - floatval($ticket->comision) - floatval($ticket->iva_comision);
                }else{
                  $totalneta = $ticket->total_neto_sin_comision;
                }
             }  
            }

            $lista->neto = str_replace('.', ',',$totalneta);

            $valor_transac = $valor_transac + floatval($ticket->total_venta);
            $valor_tarifa = $valor_tarifa + floatval($ticket->facial); 
            $impuestos = $impuestos + floatval($ticket->impuestos);
            $yq = $yq + floatval($ticket->tasas_yq);
            $py = $py + floatval($ticket->iva); 
            $pen = $pen + floatval($ticket->penalidad); 
            $porcentaje = $porcentaje + floatval($ticket->porcentaje_comision); 
            $comision = $comision + floatval($ticket->comision); 
            $com = $com + floatval($ticket->iva_comision);
            $neto = $neto + floatval($totalneta); 

            $listado[] =  $lista;
       }

        $cuerpo = array();
        
        foreach($listado as $key=>$lista)
        {
            array_push($cuerpo, array($lista->cia,$lista->trnc,$lista->documento,
           $lista->emision,$lista->fop,$lista->valor_transac,$lista->valor_tarifa,$lista->imp,$lista->yq,$lista->py,$lista->pen,$lista->porcentaje,$lista->comision,$lista->com,$lista->neto));
        }

           array_push($cuerpo, array("","","","","",$valor_transac,$valor_tarifa,$impuestos,$yq,$py,$pen,$porcentaje,$comision,$com,$neto));


            $nombreArchivo = 'Reporte BSP-' . date('d-m-Y') . '.xls'; 

            return Excel::download(new ReporteBSPExport($cuerpo), $nombreArchivo); 


    } 
    private function getIdUsuario(){
			  
        return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
    }//function

    public function generarExcelResumenCuentaVendedor(Request $req){
		$exacto =  DB::table('vw_cuenta_corriente_vendedor_resumen');
		$exacto =  $exacto->where('id_usuario', $this->getIdUsuario());       
        $exactos =  $exactos->where('id_empresa', $this->getIdEmpresa());
        $exactos =  $exactos->get();
        $cuerpo = array();
        foreach($exactos as $exacto){
            array_push($cuerpo, array( 
                                    $exacto->cliente,
                                    $exacto->moneda,
                                    number_format($exacto->importe,2,",",""),
                                    number_format($exacto->saldo,2,",","")
                                  )
                       );
         }
            $nombreArchivo = 'Reporte Resumen Cuenta-' . date('d-m-Y') . '.xlsx'; 

            return Excel::download(new ReporteResumenCuentaExport($cuerpo), $nombreArchivo); 
    }   


  public function generarExcelListadoVentas(Request $request){
          $ventas = VentasRapidasCabecera::query();
          $ventas = $ventas->with('ventasDetalle', 'factura', 'currency', 'vendedor', 'cliente', 'estado','vendedor_agencia','pedido', 'comercio','tipoVenta');

            if(!empty($request->input('nro_venta')))
            {
                $ventas = $ventas->where('nro_venta', $request->input('nro_venta'));
            }
            
            if(!empty($request->input('id_estado')))
            {
                $ventas = $ventas->where('id_estado', $request->input('id_estado'));
            }

            if(!empty($request->input('id_moneda')))
            {
                $ventas = $ventas->where('id_moneda', $request->input('id_moneda'));
            }

            if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 3)
            {
                $ventas = $ventas->where('id_vendedor', $this->getIdUsuario());
            }else{
                if(!empty($request->input('id_vendedor_empresa'))){
                    $ventas = $ventas->where('id_vendedor', $request->input('id_vendedor_empresa'));
                }   
            }

            if(!empty($request->input('origen_negocio')))
            {
                $ventas = $ventas->where('id_comercio_empresa', $request->input('origen_negocio'));
            }

            if(!empty($request->input('id_woo_order')))
            {
                $ventas = $ventas->where('id_pedido', $request->input('id_woo_order'));
            }

            if(!empty($request->input('tipo_venta')))
            {
                if($request->input('tipo_venta') == "ONLINE"){
                    $ventas = $ventas->where('id_pedido', '!=' ,null);
                }else{
                    $ventas = $ventas->where('id_pedido', '=' ,null);
                }
            }

            if(!empty($request->input('cliente_id')))
            {
                $ventas = $ventas->where('id_cliente', $request->input('cliente_id'));
            }

            if(!empty($request->input('periodo')))
            {
                $fechaTrim = trim($request->input('periodo'));
                $periodo = explode('-', trim($fechaTrim));
                $desde = explode("/", trim($periodo[0]));
                $hasta = explode("/", trim($periodo[1]));
                $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0];
                $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0];
                $ventas = $ventas->whereBetween('fecha_venta', [$fecha_desde, $fecha_hasta]);
            }
            $ventas = $ventas->where('id_empresa', $this->getIdEmpresa());
            $ventas = $ventas->orderBy('id', 'desc');
            $ventas = $ventas->get();


            if(!empty($request->input('nro_factura')))
            {
                $arrayVenta = [];
                foreach($ventas as $key=>$venta){
                    if(isset($venta->factura->nro_factura)){
                        if($venta->factura->nro_factura == $request->input('nro_factura')){
                            $arrayVenta[] = $ventas[$key];
                        }
                    }
                }
                $ventas = [];
                $ventas = $arrayVenta;  
            }
             $cuerpo = array();
            
            foreach($ventas as $key=>$value)
            {
                
                if(isset($value->cliente->nombre)){
                    $cliente = $value->cliente->nombre.' '.$value->cliente->apellido;
                }else{
                    $cliente = '';
                }
                

                if(isset($value->vendedor->nombre)){
                    $vendedor = $value->vendedor->nombre.' '.$value->vendedor->apellido;
                }else{
                    $vendedor = '';
                }

                if(isset($value->vendedor_agencia->nombre)){
                    $vendedor_agencia = $value->vendedor_agencia->nombre.' '.$value->vendedor_agencia->apellido;
                }else{
                    $vendedor_agencia= '';
                }

                if(isset($value->tipo_venta->descripcion)){
                    $tipoVenta = $value->tipo_venta->descripcion;
                }else{
                    $tipoVenta = '';
                }   

                if(isset($value->pedido->id_woo_order)){
                    $pedido = $value->pedido->id_woo_order;
                }else{
                    $pedido = '';
                } 
                 
                if(isset($value->factura->nro_factura)){
                    $factura = $value->factura->nro_factura;
                }else{
                    $factura = '';
                }  

                if(isset($value->currency->currency_code)){
                    $currency_code = $value->currency->currency_code;
                }else{
                    $currency_code = "";
                }

                if(isset($value->comercio->descripcion)){
		         $comercio = $value->comercio->descripcion;
                }else{
                    $comercio = "";
                }

                 array_push($cuerpo, 
                    array(
                          $value->id,
                          str_replace('.', ',',$value->total_venta),
                          $currency_code,
                          $value->estado->denominacion,
                          $factura,
                          date('d/m/Y', strtotime($value->fecha_venta)),
                          $cliente,
                          $vendedor,
                          $vendedor_agencia,
                          $tipoVenta,
                          $pedido,
                          $comercio
                      )
                );
            }//foreach


            $nombreArchivo = 'Listado Ventas Rapidas -' . date('d-m-Y') . '.xls'; 

            return Excel::download(new ListadoVentasRapidasExport($cuerpo), $nombreArchivo); 


    }

  public function generarExcelListadoNotaCredito(Request $request){

      $personaLogin = $this->getDatosUsuario();
    
            //DATOS DE USUARIO
        $tipoPersona = $personaLogin->id_tipo_persona;
        $idUsuario = $personaLogin->id;
        $sucursalEmpresa = $personaLogin->id_sucursal_empresa;

        $nota = DB::table('nota_credito as nc');
          $nota = $nota->select('nc.*',
                                 DB::raw("to_char(nc.fecha_hora_nota_credito ,'DD/MM/YYYY') as fecha_nota_credito"),
                                'nc.fecha_hora_nota_credito',
                                'nc.saldo_nota_credito as nota_credito_saldo',
                                'f.nro_factura',
                                'f.id_proforma',
                                'cl.nombre as cliente_n',
                                'cl.apellido as cliente_a',
                                'vd.nombre as vendedor_n',
                                'vd.apellido as vendedor_a',
                                'cu.currency_code',
                                'e.denominacion',
                                'us.id_sucursal_empresa',
                                'lv.saldo as saldo_nota_credito',
                                'pn.opcion'
                              );
          $nota = $nota->leftJoin('facturas as f', 'f.id', '=','nc.id_factura');
          $nota = $nota->leftJoin('personas as cl', 'cl.id', '=','nc.cliente_id');
          $nota = $nota->leftJoin('personas as vd', 'vd.id', '=','nc.vendedor_id');
          $nota = $nota->leftJoin('currency as cu', 'cu.currency_id', '=','nc.id_moneda_venta');
          $nota = $nota->leftJoin('estados as e', 'e.id', '=','nc.id_estado_nc');
          $nota = $nota->leftJoin('personas as us', 'us.id', '=','nc.id_usuario');
          $nota = $nota->leftJoin('libros_ventas as lv', 'lv.id_documento', '=','nc.id');
          $nota = $nota->leftJoin('pedidos_nc as pn', 'pn.id_factura', '=','nc.id_factura');


          //REGLAS ESPECIALES
           //SUPERVISOR EMPRESA
          if($tipoPersona == '4'){
               $nota = $nota->where('nc.id_sucursal_empresa',$sucursalEmpresa);
          }
          //VENDEDOR EMPRESA
          if($tipoPersona == '3'){
              $nota = $nota->where('nc.id_usuario',$idUsuario);  
          }//if

          //$nota = $nota->where('nc.id_empresa',$this->getIdEmpresa());  
          //FILTRO DE FORMULARIO
           if($request->input('cliente_id') != ''){
               $nota = $nota->where('nc.cliente_id',trim($request->input('cliente_id')));  
           }
            if($request->input('idMoneda') != ''){
               $nota = $nota->where('nc.id_moneda_venta',trim($request->input('idMoneda')));  
           }
            if($request->input('vendedor_id') != ''){
               $nota = $nota->where('nc.vendedor_id',trim($request->input('vendedor_id')));  
           }
            if($request->input('numFactura') != ''){
               $nota = $nota->where('nc.nc.nro_factura', $request->input('numFactura'));  
           }
            if($request->input('numNotaCredito') != ''){
               $nota = $nota->where('nro_nota_credito',trim($request->input('numNotaCredito')));  
           }
            if($request->input('numProforma') != ''){
               $nota = $nota->where('nc.id_proforma',trim($request->input('numProforma')));  
           }
           if($request->input('periodo_fecha_facturacion') != ''){
                $fecha = explode('-', $request->input('periodo_fecha_facturacion'));
                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
                $nota = $nota->whereBetween('nc.fecha_hora_nota_credito',array($desde.' 00:00:00',$hasta.' 23:59:59'));  
           }
        
           if($request->input('tipoEstado') && count($request->input('tipoEstado'))){

                    if( in_array(1, $request->input('tipoEstado')) && !in_array(2, $request->input('tipoEstado'))){
                    $nota = $nota->where('nc.saldo_nota_credito','>',0);
                    } else if( in_array(2, $request->input('tipoEstado')) && !in_array(1, $request->input('tipoEstado')) ){
                    $nota = $nota->where('nc.saldo_nota_credito','<=',0);
                    } 
                    // else Si es combinado no importa el saldo

                    if(in_array(3, $request->input('tipoEstado')) && !in_array(2, $request->input('tipoEstado')) && !in_array(1, $request->input('tipoEstado'))){
                    $nota =  $nota->where('nc.id_estado_nc',35);
                    } else if( in_array(3, $request->input('tipoEstado')) && ( in_array(2, $request->input('tipoEstado')) || in_array(1, $request->input('tipoEstado')) ) ){
                    $nota =  $nota->whereIn('nc.id_estado_nc',[36,35]);
                    } else if ( in_array(2, $request->input('tipoEstado')) || in_array(1, $request->input('tipoEstado')) ){
                    $nota = $nota->where('nc.id_estado_nc',36);
                    }
                
            }  
            $nota = $nota->where('nc.id_empresa',$this->getIdEmpresa());  
            
            $nota = $nota->get();
    

        $cuerpo = array();
        
        foreach($nota as $key=>$value)
        {
            $tipo_documento = 'Error';
            switch ($value->opcion) {
                case '1':
                    $tipo_documento = 'Devolución';
                    break;
                case '2':
                    $tipo_documento = 'Documento';
                    break;
                case '3':
                    $tipo_documento = 'Refacturación';
                     break;
    
            }

            array_push($cuerpo, 
                array(
                      $value->nro_nota_credito,
                      $value->fecha_nota_credito,
                      $value->nro_factura,
                      $value->total_neto_nc,
                      $value->saldo_nota_credito,
                      $value->currency_code,
                      $value->cliente_a.' '.$value->cliente_n,
                      $value->vendedor_a.' '.$value->vendedor_n,
                      $value->denominacion,
                      $tipo_documento
                  )
            );
        }//foreach


        $nombreArchivo = 'Listado Nota de Credito -' . date('d-m-Y') . '.xls'; 

        return Excel::download(new ListadoNotaDeCreditoExport($cuerpo), $nombreArchivo); 



  }//function


  public function generarExcelReporteFactura(Request $req)
  {
        // dd($req->all());
             $personaLogin = $this->getDatosUsuario();
                //DATOS DE USUARIO
              $tipoPersona = $personaLogin->id_tipo_persona;
              $idUsuarioSistema = $personaLogin->id;
              $sucursalEmpresa = $personaLogin->id_sucursal_empresa;


              $desde     = "";
              $hasta     = "";
              $desdecheck = "";
              $hastacheck = "";
              $formSearch = $req->formSearch;
              $draw = intval($req->draw);
              $start = intval($req->start);
              $length = intval($req->length);

               $periodo = $req->input("periodo");
               if($periodo != ""){
                  $fechaPeriodo = explode(' - ', $periodo);
                  $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                  $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                } else {
                  $desde     = "";
                  $hasta     = "";
                }  
                if($req->input('checkin') != ""){
                    $fechaPeriodo = explode(' - ', $req->input('checkin'));
                    $desdecheck     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                    $hastacheck     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
                  } 

              $cont = 0;
              $filtrarMax = 0;

            $clienteId = $req->input('cliente_id');
            $monedaId  = $req->input('idMoneda');
            $tipoFactura = $req->input('tipoFactura');
            $tipoEstado = $req->input('tipoEstado');
            $vendedorId = $req->input('vendedor_id');
            $numFactura = $req->input('numFactura');
            $numProforma = $req->input('numProforma');
            $impreso = $req->input('impreso');
            $idUsuarioBusqueda = $req->input('id_usuario');
            $sucursalFacturacion = $req->input('id_sucursal');
            $grupo = $req->input('grupo_id');
            $forma_pago = $req->input('forma_pago');
            $idTipoPersona = $req->input('tipo_persona_id');
            $tipo_venta = $req->input('tipo_venta');
            $negocio = $req->input('negocio');
            $id_equipo = $req->input('id_equipo');

              $factura = DB::table('vw_listado_factura');
              $factura = $factura->where('id_empresa',$this->getIdEmpresa());
              $factura = $factura->selectRaw("*,get_monto_cotizado_factura(get_monto_factura(id),id) as totalDolar");

                //OPERATIVO EMPRESA OR ADMINISTRATIVO_EMPRESA OR ADMIN_EMPRESA
                if($tipoPersona == '6' || $tipoPersona == '5' || $tipoPersona == '2'){
                    if($req->id_estado_proforma == ''){ 
                    //PUEDE VER TODAS LAS FACTURAS
                    }
                    //SUPERVISOR EMPRESA
                } else if($tipoPersona == '4'){
                    $factura = $factura->where('id_sucursal_empresa_usuario', $sucursalEmpresa);
                } else {
                    //LOS DEMAS TIPO SOLO PUEDE VER SU PROFORMA
                    $flag = 1;
                    $factura = $factura->where('id_usuario',  $idUsuarioSistema);
                }



                //VENDEDOR EMPRESA
                if($tipoPersona == '3'){
                  $factura = $factura->where('id_usuario',$idUsuarioSistema);  
                }//if

               if($tipoFactura != ''){
                   $factura = $factura->where('id_tipo_facturacion',$tipoFactura);  
               }
               if($clienteId != ''){
                   $factura = $factura->where('cliente_id',$clienteId);  
               }
                if($monedaId != ''){
                   $factura = $factura->where('id_moneda_venta',$monedaId);  
               }
                if($vendedorId != ''){
                   $factura = $factura->where('vendedor_id',$vendedorId);  
               }
                if($numFactura != ''){
                   $factura = $factura->where('nro_factura', 'like', '%' .$numFactura.'%');  
               }
                if($numProforma != ''){
                   $factura = $factura->where('id_proforma',$numProforma);  
               }
               if($tipoEstado != ''){
                   $factura = $factura->where('id_estado_factura',$tipoEstado);  
               }
               if($impreso != ''){
                   $factura = $factura->where('factura_impresa',$impreso);  
               }
               if( $idUsuarioBusqueda != '' ){
                 $factura = $factura->where('id_usuario',$idUsuarioBusqueda); 
               } 
               if($tipo_venta  != ''){ 
                    if($tipo_venta == '1'){ $factura = $factura->whereNull('id_venta_rapida'); }//PROFORMA
                    if($tipo_venta == '2'){$factura = $factura->whereNotNull('id_venta_rapida');}//VENTA RAPIDA
                }  

                if($negocio != ''){
                    $factura = $factura->where('id_unidad_negocio',$negocio);  
                }
 

                if($desde != '' && $hasta != ''){

                  $factura = $factura->whereBetween('fecha_hora_facturacion', array(
                  date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
                  date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                      ));
               }

               if($grupo != ''){
                $factura = $factura->where('id_grupo',$grupo);
              }
   
              if($id_equipo != ''){
                $factura = $factura->where('id_equipo',$id_equipo);  
              }
       
              if($sucursalFacturacion != '' ){
               $factura = $factura->where('id_sucursal_timbrado',$sucursalFacturacion); 
              }
              //para checkin
          if ($desdecheck && $hastacheck) {
            $factura = $factura->whereBetween('check_in', [$desdecheck. ' 00:00:00', $hastacheck.' 23:59:59']);
          }
        //    dd($desde, $hasta);
                $factura = $factura->get();

              /*  echo '<pre>';
                print_r($factura);
                die;*/
                $cantidadFacturas = count($factura);
                $lengtArray = $cantidadFacturas; 
                $resultFacturas = array();

        $listado = array();
        foreach ($factura as $key => $value) {

            if($value->id_proforma != null){
                $autorizado = DB::select("SELECT * FROM get_proforma_autorizada(".$value->id_proforma.")");	
                $proforma_autorizada = $autorizado[0]->get_proforma_autorizada;
              }else{
                $proforma_autorizada = '';
              }
            $boton = '';
            $pasajero =  $value->pasajero_n;

            if($value->id_pasajero_principal == 4994){
              $pasajero = $value->pasajero_online;
            }
            $fechaCheckIn = explode(' ', $value->check_in);
            $fechaChec_kIn = explode('-', $fechaCheckIn[0]);
            if(isset($fechaChec_kIn[2])){
                $resultado = $fechaChec_kIn[2]."/".$fechaChec_kIn[1]."/".$fechaChec_kIn[0];
            }else{
                $resultado = "";
            }    

            $lista = new \StdClass;
            $renta_neta = /*(float)$value->ganancia_venta - ((float)$value->renta_extra+(float)$value->incentivo_vendedor_agencia);*/(float)$value->renta_comisionable;
            $lista->renta =  $this->formatMoney($renta_neta, $value->id_moneda_venta);
            $lista->id = $value->id;
            $lista->factura = $value->nro_factura;
            $lista->fecha_factura =$value->fecha_format;
            $lista->check_in =$resultado;
            $lista->proforma = $value->id_proforma;
            $lista->estado = $value->denominacion;
            $lista->monto =  str_replace('.', ',',$value->monto_factura);
            $lista->moneda = $value->currency_code;
            $lista->cliente = $value->cliente_n;
            $lista->pasajero = $pasajero;
            $lista->sucursal = $value->suc_n;
            $lista->vendedor = $value->vendedor_n;
            $lista->usuario = $value->usuario_n;
            $lista->vencimiento = $value->vencimiento_format;
            $lista->destino = $value->desc_destino;
            $lista->extra = $this->formatMoney($value->renta_extra, $value->id_moneda_venta);
            $lista->comision = $this->formatMoney($value->total_comision, $value->id_moneda_venta);
            $lista->saldo_factura =  $this->formatMoney($value->saldo_factura,$value->id_moneda_venta);
            $lista->porcentaje_gananacia = str_replace('.', ',',$value->porcentaje_ganancia);
            $lista->ganancia =  $this->formatMoney($value->ganancia_venta, $value->id_moneda_venta);
            $lista->incentivo = $this->formatMoney($value->incentivo_vendedor_agencia,$value->id_moneda_venta);
            $lista->totalDolar =  str_replace('.', ',',$value->totaldolar);
            $lista->negocio =  $value->negocio;
            $lista->saldo_factura =  $this->formatMoney($value->saldo_factura, $value->id_moneda_venta);
            $lista->proforma_autorizada = $proforma_autorizada;
            $lista->asistente = $value->asistente_n;
            $lista->nombre_equipo = $value->nombre_equipo;//arturo
            $lista->promocion = $value->promocion;
            $listado[] =  $lista;
        }
       
        $cuerpo = array();

        
        foreach($listado as $key=>$lista)
        {
            array_push($cuerpo, array(  $lista->id,
                                        $lista->factura,
                                        $lista->fecha_factura,
                                        $lista->check_in,
                                        $lista->vencimiento,
                                        $lista->proforma,
                                        $lista->estado,
                                        $lista->monto,
                                        $lista->saldo_factura,
                                        $lista->moneda,
                                        $lista->cliente, 
                                        $lista->pasajero,
                                        $lista->usuario,
                                        $lista->destino,
                                        $lista->comision,
                                        $lista->porcentaje_gananacia,
                                        $lista->ganancia,
                                        $lista->extra,
                                        $lista->incentivo,
                                        $lista->renta,
                                        $lista->vendedor,
                                        $lista->sucursal,
                                        $lista->totalDolar,
                                        $lista->negocio,
                                        $lista->saldo_factura,
                                        $lista->proforma_autorizada,
                                        $lista->asistente,
                                        $lista->nombre_equipo,
                                        $lista->promocion  
                                    ));
        }


        $nombreArchivo = 'Listado Facturas -' . date('d-m-Y') . '.xlsx'; 

        return Excel::download(new ListadoFacturasExport($cuerpo), $nombreArchivo); 
     
    }

    public function generarExcelProforma(Request $req)
    {

		$personaLogin = $this->getDatosUsuario();
		// dd($personaLogin);
		$tipoPersona = $personaLogin->id_tipo_persona;
		$idUsuario = $personaLogin->id;
		$sucursalEmpresa = $personaLogin->id_sucursal_empresa; 
		$flag = 0;

		$proformas = DB::table( 
					DB::raw("( 
						SELECT 
						p.id,
                        P.reconfirmacion,
						p.estado_id, 
						p.pasajero_online,  
						p.id_prioridad, 
						p.id_moneda_venta,
						estados.denominacion,
						f.nro_factura, 
						p.calificacion,
						p.comentario_calificacion,
						p.factura_id,
						p.destino_id,
						destino.desc_destino,
                        equipo.nombre_equipo,
                        p.id_equipo,
						p.fecha_alta, 
						(SELECT id FROM proformas where proforma_padre = p.id LIMIT 1) as expediente,
						(SELECT currency_code FROM currency where currency_id = p.id_moneda_venta LIMIT 1) as moneda_venta,
						p.proforma_padre,
						p.emergencia,
						p.check_out,
						to_char(p.fecha_alta , 'DD/MM/YYYY') as fecha_alta_format,
						p.check_in,
						to_char(p.check_in , 'DD/MM/YYYY') as check_in_format,
						p.cliente_id,
						concat(cliente.nombre,' ',cliente.apellido) cliente_n,
						p.vendedor_id,
					    concat(vendedor.nombre,' ',vendedor.apellido) vendedor_n, 
						p.pasajero_id,	
					    concat(pasajero.nombre,' ',pasajero.apellido) pasajero_n, 
						p.responsable_id,
						concat(operativo.nombre,' ',operativo.apellido) operativo_n, 
						p.id_usuario, 
						concat(usuario.nombre,' ',usuario.apellido) usuario_n,
					    usuario.id_sucursal_empresa id_sucursal_empresa ,
					    p.file_codigo,
					    p.id_empresa,
					    p.markup,
					    (SELECT get_monto_proforma as m FROM get_monto_proforma(p.id)) as total_proforma,
					    (SELECT COUNT(*) 
					    FROM proformas_detalle 
					    WHERE activo = true and id_producto IN( SELECT id  FROM productos WHERE id_grupos_producto = 1 ) and id_proforma = p.id ) as ticketCount,
						unidad_negocios.descripcion as negocio,
						unidad_negocios.id as negocio_id,
                        ".$tipoPersona." as tipo_persona_usuario
		FROM proformas p 
		LEFT JOIN personas as cliente
		ON cliente.id = p.cliente_id 
		LEFT JOIN personas as usuario 
		ON usuario.id = p.id_usuario
		LEFT JOIN personas as vendedor
		ON vendedor.id = p.vendedor_id
		LEFT JOIN personas as pasajero
		ON pasajero.id = p.pasajero_id
		LEFT JOIN personas as operativo 
		ON operativo.id = p.responsable_id
		LEFT JOIN proformas as expediente 
		ON expediente.id = p.proforma_padre
        LEFT JOIN unidad_negocios
		ON unidad_negocios.id = p.id_unidad_negocio
		LEFT JOIN estados
		ON estados.id = p.estado_id
		LEFT JOIN destinos_dtpmundo as destino
		ON destino.id_destino_dtpmundo = p.destino_id
        LEFT JOIN equipo_cabecera as equipo
		ON equipo.id = p.id_equipo
		LEFT JOIN facturas as f
		ON f.id = p.factura_id ) as result")		
		);

		$proformas = $proformas->where('id_empresa', $this->getIdEmpresa());
		// $query = '
		// // WHERE proformas.id_empresa = '.$this->getIdEmpresa();
			/*
            2 "ADMIN_EMPRESA"
            3 "VENDEDOR_EMPRESA"
            4 "SUPERVISOR_EMPRESA"
            5 "ADMINISTRATIVO_EMPRESA"
            6 "OPERATIVO_EMPRESA"
           */
       
        	//======ESTADO PROFORMA=========
        	//OPERATIVO EMPRESA OR ADMINISTRATIVO_EMPRESA OR ADMIN_EMPRESA
			if($tipoPersona == '6' || $tipoPersona == '5' || $tipoPersona == '2'){
				if($req->id_estado_proforma == ''){ 
					//Traer Verificado y  aVerificar
					// $proformas = $proformas->whereIn('estado_id', [3,2]);

				}
					//SUPERVISOR EMPRESA tipo_persona_usuario
			} else if($tipoPersona == '4'){
					$proformas = $proformas->where('id_sucursal_empresa', $sucursalEmpresa);
			} else {
					//LOS DEMAS TIPO SOLO PUEDE VER SU PROFORMA
					$flag = 1;
				    $proformas = $proformas->where('id_usuario', $idUsuario);
			}

			try {
			 if (count($req->input('id_estado_proforma')) > 0) { 
			 	$arrayEstado = $req->input('id_estado_proforma');
			 	foreach ($arrayEstado as $key => $value) {
			 		if(trim($value) === ''){
			 			unset($arrayEstado[$key]);
			 		}
			 	}
			 		if(count($arrayEstado) > 0)
        			$proformas = $proformas->whereIn('estado_id', $arrayEstado);
			}

			 } catch(\Exception $e){}
			
		 	 if($req->id_vendedor_empresa != '' && $flag == 0){			
        		$proformas = $proformas->where('id_usuario', $req->id_vendedor_empresa);
        	}	

			if($req->star_option != ''){
				// dd($req->star_option);
				$option = explode(',',$req->star_option);
				$proformas = $proformas->whereIn('calificacion', $option);

			}
			if($req->post_venta != ''){ 

				if($req->input('post_venta') === 'true')
				$proformas = $proformas->where('calificacion','<>', '0');

				if($req->input('post_venta') === 'false')
				$proformas = $proformas->where('calificacion', '0');
			}

			 if($req->input('nro_factura') != ''){
	        	$proformas = $proformas->where('nro_factura', trim($req->input('nro_factura')));
	        }

      	    if($req->input('responsable_id') != ''){
      	    	$proformas = $proformas->where('responsable_id', trim($req->input('responsable_id')));
      	    }
            
              if($req->input('negocio') != ''){
				$proformas = $proformas->where('negocio_id', trim($req->input('negocio')));
			}


      	    if($req->input('vendedor_id') != ''){
      	    	$proformas = $proformas->where('vendedor_id', trim($req->input('vendedor_id')));	
      	    }	
         	
         	if($req->input('file_codigo') != ''){
      	    	//$proformas = $proformas->where('file_codigo', trim($req->input('file_codigo')));
      	    	$nombre = "%".trim($req->input('file_codigo'))."%";
				// dd($nombre);
				$proformas = $proformas->where('file_codigo','LIKE',$nombre);	
      	    }	

			 if($req->input('id_proforma') != ''){
	        	$proformas = $proformas->where('id', trim($req->input('id_proforma')));	
	        }
	        if($req->input('cliente_id') != ''){ 
	        	$proformas = $proformas->where('cliente_id', trim($req->input('cliente_id')));	
	        }
	         if($req->input('id_proforma_padre') != ''){
	        	$proformas = $proformas->where('proforma_padre', trim($req->input('id_proforma_padre')));	
			}
			
			if($req->input('destino') != ''){
	        	$proformas = $proformas->where('destino_id',$req->input('destino'));	
	        }

	        /*if($req->input('periodo_out') != ""){
	            $fechaPeriodo_out = explode(' - ', $req->input('periodo_out'));
 				$desdeC_out     = $this->formatoFechaEntrada($fechaPeriodo_out[0]).' 00:00:00';
                $hastaC_out     = $this->formatoFechaEntrada($fechaPeriodo_out[1]).' 23:59:59';

                $proformas = $proformas->whereBetween('check_in',array($desdeC_out,$hastaC_out));          
            }*/
            if($req->input('periodo') != ""){
	            $fechaPeriodo = explode(' - ', $req->input('periodo'));
 				$desdeC     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
                $hastaC     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';

                $proformas = $proformas->whereBetween('check_in',array($desdeC,$hastaC));          
            }
            if($req->input('periodo_out') != ""){
	            $fechaPeriodo_out = explode(' - ', $req->input('periodo_out'));
	           	/*echo "</pre>";
    			print_r($fechaPeriodo_out);*/
    			$desdeC_out     = $this->formatoFechaEntrada($fechaPeriodo_out[0]).' 00:00:00';
                $hastaC_out     = $this->formatoFechaEntrada($fechaPeriodo_out[1]).' 23:59:59';

                $proformas = $proformas->whereBetween('check_in',array($desdeC_out,$hastaC_out));          
            }

	        if($req->input('periodos') != ""){ 
	            $fechaPeriodos  = explode(' - ', $req->input('periodos'));
                $desde     = $this->formatoFechaEntrada($fechaPeriodos[0]).' 00:00:00';
                $hasta     = $this->formatoFechaEntrada($fechaPeriodos[1]).' 23:59:59';
                // $query .= " AND proformas.fecha_alta BETWEEN '".$desde."' AND '".$hasta."'";
                $proformas = $proformas->whereBetween('fecha_alta',array($desde,$hasta));

	        } 

	        /*if($req->input('periodo_out') != ""){
	            $fechaPeriodo_out = explode(' - ', $req->input('periodo_out'));
	           	/*echo "</pre>";
    			print_r($fechaPeriodo_out);*/
    		/*	$desdeC_out     = $this->formatoFechaEntrada($fechaPeriodo_out[0]).' 00:00:00';
                $hastaC_out     = $this->formatoFechaEntrada($fechaPeriodo_out[1]).' 23:59:59';

                $proformas = $proformas->whereBetween('check_out',array($desdeC_out,$hastaC_out));          
            }*/

	        if($req->input('id_moneda') != ''){
	          $proformas = $proformas->where('id_moneda_venta', trim($req->input('id_moneda')));	 
	        }
            if ($req->input('reconfirmacion')) {
				if ($req->input('reconfirmacion') == 'true') {
					$proformas = $proformas->where('reconfirmacion', true);
				} elseif ($req->input('reconfirmacion') == 'false') {
					$proformas = $proformas->where('reconfirmacion', false);
				}
			}
	        $proformas = $proformas->orderBy('id', 'ASC');
	        $proformas = $proformas->get();
////////////////////////////////////////////////////////////////////////////////////////////////////////
            $cuerpo = array();

            foreach($proformas as $key=>$lista)
            {
                $pasajero = $lista->pasajero_n;
                if($lista->pasajero_id == 4994){
                    $pasajero = $lista->pasajero_online;
                }
                array_push($cuerpo, array(
                                            $lista->expediente,
                                            $lista->id,
                                            $lista->denominacion,
                                            $lista->nro_factura,
                                            $lista->fecha_alta_format,
                                            $lista->check_in_format,
                                            $lista->cliente_n,
                                            $lista->vendedor_n, 
                                            $pasajero,
                                            $lista->moneda_venta,
                                            $lista->total_proforma, 
                                            $lista->markup,
                                            $lista->operativo_n, 
                                            $lista->usuario_n, 
                                            $lista->desc_destino,
                                            $lista->negocio,
                                            $lista->nombre_equipo,
                                            $lista->reconfirmacion ? 'Reconfirmado' : 'Falta Reconfirmación'
                                        ));
            }


            $nombreArchivo = 'Listado Proformas -' . date('d-m-Y') . '.xls'; // Reemplaza "NombreDelReporte" por el nombre adecuado
            return Excel::download(new ListadoProformasExport($cuerpo), $nombreArchivo);


    }

    private function getDatosUsuario(){
  
   $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
   return $persona = Persona::where('id',$idUsuario)->first();

  }


public function generarExcelClienteReportUno(){


      $personas = Persona::with('tipo_facturacion')
      // ->select('nombre')
      ->where('id_tipo_persona',10)
      ->orWhere('id_tipo_persona',2)
      ->whereNotNull('id_tipo_facturacion')
      ->get();  

  

    
        $cuerpo = array();
        foreach($personas as $key=>$value)
        {
            array_push($cuerpo, array($value->nombre.' '.$value->apellido,
                                      $value->denominacion_comercial,
                                      $value->tipo_facturacion->denominacion));
        }


        $nombreArchivo = 'Reporte Cliente Tipo Facturacion-' . date('d-m-Y') . '.xlsx'; 

        return Excel::download(new ReporteClienteTipoFacturacionExport ($cuerpo), $nombreArchivo); 


        
}
public function generarExcelClienteReportDos(){

     $personas = Factura::with('cliente','tipoFacturacion','cliente.tipo_facturacion')
     ->select('*')
     ->selectRaw("to_char(fecha_hora_facturacion,'DD/MM/YYYY') as fecha_facturacion")
     ->where('id_estado_factura',29)
     ->where('id','>',9000)
     ->get();  

    

    
            $cuerpo = array();
        foreach($personas as $key=>$value)
        {
             $nombre = '';
             $denominacion_comercial  = '';
             $facturacionCliente = '';

            if(!empty($value->cliente)){
                 $nombre = $value->cliente->nombre.' '.$value->cliente->apellido;
                 $denominacion_comercial = $value->denominacion_comercial;

                 if(!empty($value->cliente['tipo_facturacion'])){
                    $facturacionCliente = $value->cliente['tipo_facturacion']->denominacion;
                 } else {
                    $facturacionCliente = '';
                 }


             } else {
                 $nombre = '';
                 $denominacion_comercial  = '';
             }

           
            array_push($cuerpo, array( $nombre,
                                      $denominacion_comercial,
                                      $facturacionCliente,
                                      $value->nro_factura,
                                      $value->fecha_facturacion,
                                      $value->tipoFacturacion->denominacion));
        }



            $nombreArchivo = 'Reporte Cliente Factura-' . date('d-m-Y') . '.xls'; 
            return Excel::download(new ReporteClienteFacturaExport($cuerpo), $nombreArchivo); 
}



public function generarExcelReporteRiesgo(Request $req){


    $vistaRiesgo = DB::table('vw_riesgo_cliente');
 
    if($req["id_persona"] != ''){
     $vistaRiesgo =  $vistaRiesgo->where('id_cliente',$req["id_persona"]);
    }
    if($req["id_tipo_persona"] != ''){
      $vistaRiesgo =  $vistaRiesgo->where('id_tipo_persona',$req["id_tipo_persona"]);
    }
    if($req["ruc"] != ''){
       $vistaRiesgo =  $vistaRiesgo->where('documento_identidad','LIKE','%'.trim($datos["ruc"]).'%');
    }
     if($req["id_estado_saldo"] != ''){
       $vistaRiesgo =  $vistaRiesgo->where('id_estado_saldo',$req["id_estado_saldo"]);
    }
        $vistaRiesgo =  $vistaRiesgo->where('id_empresa',$this->getIdEmpresa());

      $vistaRiesgo =  $vistaRiesgo->get();

            $cuerpo = array();

        foreach($vistaRiesgo as $key=>$value)
        {
            array_push($cuerpo, array(    $value->nombre,
                                          $value->documento_identidad, 
                                          number_format($value->credito,2,",",""),
                                          number_format($value->vencidas,2,",",""),
                                          number_format($value->a_vencer,2,",",""),
                                          number_format($value->en_gastos,2,",",""),
                                          number_format($value->sin_gastos,2,",",""),
                                          number_format($value->saldo,2,",","")
                                      )
                       );
        }

            $nombreArchivo = 'Reporte Cliente Riesgo-' . date('d-m-Y') . '.xls'; 
            return Excel::download(new ReporteClienteRiesgoExport($cuerpo), $nombreArchivo); 

        }

        public function generarExcelTicketsPendientes(Request $req){


            $ticket_pendientes = DB::table("vw_tickets_pendientes as vw");
            $ticket_pendientes = $ticket_pendientes->select("vw.*", "p.nombre","p.apellido",'cu.currency_code');
            $ticket_pendientes = $ticket_pendientes->leftJoin('personas as p', 'p.id', '=','vw.cliente_id' );
            $ticket_pendientes = $ticket_pendientes->leftJoin('currency as cu', 'cu.currency_id', '=','vw.id_moneda_venta' );
            $ticket_pendientes = $ticket_pendientes->where('vw.id_empresa',$this->getIdEmpresa());

        if($req->input("numFactura") != ''){

            $ticket_pendientes = $ticket_pendientes->where('vw.nro_factura',$req->input("numFactura"));
        }
            if($req->input("numProforma") != ''){
            $ticket_pendientes = $ticket_pendientes->where('vw.id_proforma',$req->input("numProforma"));
        } 
        if($req->input("idCliente") != ''){
            $ticket_pendientes = $ticket_pendientes->where('vw.cliente_id',$req->input("idCliente"));
        }
            if($req->input("idMoneda") != ''){
            $ticket_pendientes = $ticket_pendientes->where('vw.id_moneda_venta',$req->input("idMoneda"));
        }
            if($req->input("facturacion_desde_hasta") != ''){
            $fecha =  explode('-', $req->input("facturacion_desde_hasta"));

                $desde     = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta     = $this->formatoFechaEntrada(trim($fecha[1])); 

                $ticket_pendientes = $ticket_pendientes->whereBetween('vw.fecha_hora_facturacion',array($desde,$hasta));

        }

        $ticket_pendientes = $ticket_pendientes->get();

        $cuerpo = array();

        foreach($ticket_pendientes as $key=>$value)
        {
            $nombre = $value->proforma_nombre;
            if($value->proforma_apellido != ""){
                $nombre .= $value->proforma_apellido;
            }
            array_push($cuerpo, array(    $value->id_proforma,
                                        $value->nro_factura, 
                                        $value->fecha_hora_facturacion,
                                        $value->fecha_bsp,
                                        $value->nombre.' '.$value->apellido,
                                        $value->currency_code,
                                        number_format($value->total_cobrado,2,",","."),
                                        number_format($value->saldo_ticket,2,",","."),
                                        $value->usuario_emision,
                                        $nombre
                                    )
                    );
        }

            $nombreArchivo = 'Reporte Tickets Pendientes-' . date('d-m-Y') . '.xlsx';

            return Excel::download(new ReporteTicketsPendientes_2Export($cuerpo), $nombreArchivo);

        }
     public function actualizarProductos(Request $request){
           $proveedores = Persona::where('id_tipo_persona', '=', '14')
                            ->where('id_empresa', '=', Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
                            ->get();
            $arrayProveedor =[];                                
            foreach ($proveedores as $key=>$proveedor){
                $arrayProveedor[$key]['value']= $proveedor->id;
                $arrayProveedor[$key]['label']= $proveedor->nombre;
             }     
             return view('pages.mc.ventas.actualizarProductos')->with(['proveedores'=>$arrayProveedor]);
           

         
    }//class


  public function importUsers(Request $request){
            Session::put('contadorInsercion', 0);
            Session::put('contadorNoInsercion', 0);
            Session::put('totalRegistros', 0);
            Session::put('datosInsercion', array());
            Session::put('datosNoInsertados', array());
            ini_set('memory_limit', '256M');
            $contadorInsercion = 0;
            if($request->input('proveedor') != 0){
                 if($request->excel !=""){
                    DB::table('productos')
                            ->where('id_proveedor',$request->input('proveedor'))
                           // ->whereIn('id', ['3766', '5731', '3688'])
                            ->update([
                                    'stock'=>0
                                    ]); 
                    $totalProductos =Producto::where('id_proveedor', $request->input('proveedor')) 
                                                        ->count();
                    Session::put('totalProductos', $totalProductos);
                    Session::put('proveedor', $request->input('proveedor'));
                    Excel::load($request->excel, function($reader){
                        $excel = $reader->get();
                        $reader->each(function($row){
                                if(!empty($row['codigo'])){
                                    $contadorTotal = Session::get('contadorInsercion');
                                    $contadorTotal++;
                                    Session::put('totalRegistros', $contadorTotal);
                                   
                                    $proveedor = Session::get('proveedor');
                                   // if($row['precio_venta'] == "" ){
                                     // $precioExcel = 0;
                                  //  }else{
                                      $precioExcel = str_replace(',','.', str_replace('.','',$row['precio_venta']));
                                    //}
                                    /*$device = Producto::where( 'woo_sku', 'ilike', trim($row['codigo']))->get();
                                    echo '<pre>';
                                    print_r($device);*/
                                    $device = Producto::where('woo_sku', 'ilike', trim($row['codigo']));
                                    $device->update([
                                                    'stock'=>$row['stock'],
                                                    'id_proveedor'=>$proveedor,
                                                    'precio_venta'=>$precioExcel
                                    ]);                     
                                    $productos = Producto::where('woo_sku', $row['codigo'])
                                                        ->where('id_proveedor', $proveedor) 
                                                        ->first();  
                                    $contadorInsercion = Session::get('contadorInsercion');
                                    $contadorNoInsercion = Session::get('contadorNoInsercion');
                                    if(!empty($productos)){
                                        $contadorInsercion++;
                                        Session::put('contadorInsercion', $contadorInsercion);
                                        $datos = Session::pull('datosInsercion');
                                        $datos[] = $row['stock']."-".$row['codigo']."-".$row['denominacion'];
                                        Session::put('datosInsercion', $datos);
                                    }else{
                                        $contadorNoInsercion++;  
                                        Session::put('contadorNoInsercion', $contadorNoInsercion);
                                        $data = Session::pull('datosNoInsertados');
                                        $data[] = $row['stock']."-".$row['codigo']."-".$row['denominacion'];
                                        Session::put('datosNoInsertados', $data);
                                    }
                                }          
                            });    
                       
                        }); 
                         
                       $totalProductos =Producto::where('id_proveedor', $request->input('proveedor'))
                                                // ->whereIn('id', ['3766', '5731', '3688'])
                                                  ->get();
                       foreach($totalProductos as $baseProducto){
                            if(!empty($baseProducto->id_producto_woo)){
                                $client = new Client([
                                                    'headers' => [ 
                                                               'Authorization' => 'Basic Y2tfMDI3MGMxY2Y5ZTQ2MDA4NTAwOGI2MDUyZmFlYjBhOWYwNmY0MTI3Mjpjc19kMTNkNzRlYTdlYTlhYmZmZTc1ZTllNmNiNzMxYTViOGNlNzEzMzJi',/*,
                                                                'Authorization' => 'Basic Y2tfMGMyYzdhNWVhODI5ZWVjMGQyOGFjMTkzNWNjMjI1Mzg2OTZkOTQ3Yjpjc182NjAzZTRjMGUwMGZkMjAxNzZhYmJlN2EzMDMzMzcxMzZhNWU4MDNj'*/
                                                                'Content-Type' => 'application/json' 
                                                                ]
                                                    ]);
                                try{
                                    $iibRsp = $client->get('https://atukasa.com.py/wp-json/wc/v3/products/'.$baseProducto->id_producto_woo);
                                    $iibObjRsp = json_decode($iibRsp->getBody());  
                                      /*echo '<pre>';
                                       print_r($iibObjRsp->manage_stock);*/
                                     $prodArray = array();
                                    if($iibObjRsp->manage_stock ==1){
                                        $prodArray['update'][0]['id']= $baseProducto->id_producto_woo;
                                        $prodArray['update'][0]['_total_stock_quantity']= $baseProducto->stock;
                                        $prodArray['update'][0]['stock_quantity']= $baseProducto->stock;
                                    }else{
                                        $prodArray['update'][0]['id']= $baseProducto->id_producto_woo;
                                        $prodArray['update'][0]['stock_quantity']= $baseProducto->stock;
                                        if($baseProducto->stock == 0){
                                            $prodArray['update'][0]['stock_status']= "outofstock";
                                        }else{
                                             $prodArray['update'][0]['stock_status']= "instock";    
                                        }

                                    } 
                                      if($baseProducto->precio_venta != 0){
                                        $prodArray['update'][0]['regular_price'] = $baseProducto->precio_venta;
                                        $prodArray['update'][0]['price'] = $baseProducto->precio_venta;
                                        $prodArray['update'][0]['price_html'] = "PYG ".$baseProducto->precio_venta;
                                      }

                                 try{
                                        $iibRsp = $client->post('https://atukasa.com.py/wp-json/wc/v3/products/batch',['body' => json_encode($prodArray)]);

                                        $iibObjRsps = json_decode($iibRsp->getBody()); 
                                             Producto::where([
                                                            'id'=>$baseProducto->id
                                                            ])->update([
                                                                     'rsp_woo'=>json_encode($iibObjRsps)
                                                                    ]);
                                        /*echo '<pre>';
                                         print_r($iibObjRsps);  */               
                                    } catch(\Exception $e){
                                            Producto::where([
                                                            'id'=>$baseProducto->id
                                                            ])->update([
                                                                     'rsp_woo'=>json_encode($e)
                                                                    ]); 
                                        }    
                                } catch(\Exception $e){
                                    Producto::where([
                                                    'id'=>$baseProducto->id
                                                    ])->update([
                                                             'rsp_woo'=>json_encode($e)
                                                            ]); 

                                } 

                            }
                        }
                       // die;
                         flash('Los productos fueron actualizados correctamente')->success();
                       return view('pages.mc.ventas.resumenExcel')->with([
                                                                        'totalRegistros' => Session::get('totalProductos'),
                                                                        'totalExcel' => Session::get('contadorInsercion'),
                                                                        'insertados' => Session::get('contadorInsercion'),
                                                                        'noInsertados' => Session::get('contadorNoInsercion'),
                                                                        'arrayInsertados' => Session::get('datosInsercion'),
                                                                        'arrayNoInsertados' => Session::get('datosNoInsertados')
                                                                        ]);
                        
                }else{
                    flash('No existe archivo a procesar, Intentelo nuevamente')->error();
                   return redirect()->back();
                } 
            }else{
                flash('Seleccione un proveedor, Intentelo nuevamente')->error();
                return redirect()->back();
            }       
     
    }


        public function generarExcelLVenta(Request $request)
        {
            $detalle = new \StdClass;
            $librosVentas = DB::table('v_libros_ventas');
            $librosVentas = $librosVentas->where('id_empresa',$this->getIdEmpresa());
            if($request->input('idProveedor')) $librosVentas = $librosVentas->where('cliente_id',$request->input('idProveedor'));
            if($request->input('nroFactura')) $librosVentas = $librosVentas->where('nro_documento',$request->input('nroFactura'));
            if($request->input('desde_hasta')!= '')
            {
              $fechaTrim = trim($request->input('desde_hasta'));
              $periodo = explode('-', trim($fechaTrim));
              $desde = explode("/", trim($periodo[0]));
              $hasta = explode("/", trim($periodo[1]));
              $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
              $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
              $librosVentas = $librosVentas->whereBetween('fecha_hora', [$fecha_desde, $fecha_hasta]);
            }

           $librosVentas = $librosVentas->orderBy('fecha_documento', 'asc');
           $librosVentas = $librosVentas->orderBy('nro_documento', 'asc');
           $librosVentas = $librosVentas->get();
           // dd($librosVentas);
           $cuerpo = array();
           foreach($librosVentas as $libro){
              array_push($cuerpo, array( 
                                     // $detalle->id = $libro->id
                                      date('d', strtotime($libro->fecha_hora)),
                                      $libro->nro_documento,
                                      0,
                                      0,
                                      $libro->cliente,
                                      $libro->documento_identidad,
                                      number_format($libro->total_gravadas,2,",",""),
                                      $detalle->grav_5 = 0,
                                      number_format($libro->total_iva,2,",",""),
                                      0,
                                      number_format($libro->total_exentas,2,",",""),
                                      number_format($libro->importe,2,",",""),
                                      number_format($libro->monto_retencion,2,",",""),
                                      $libro->saldo
                                    )
                         );

           }

           
            $nombreArchivo = 'Reporte Libro de Ventas-' . date('d-m-Y') . '.xlsx'; 
            return Excel::download(new ReporteLibroVentasExport($cuerpo), $nombreArchivo); 

    


      }

        public function generarExcelMovimientoCuenta(Request $req){

          $id_empresa = $this->getIdEmpresa();

          $bancos = DB::table('vw_cuenta_corriente_bancos');
	
          $bancos = $bancos->where('id_empresa',$id_empresa);
 
         if($req->input('id_cuenta_detalle')){
             $bancos = $bancos->where('id_banco_detalle',$req->input('id_cuenta_detalle'));
         }
 
         if ($req->input('anulado') == 0) 
         {
             $bancos = $bancos->where('activo', true);
         }
  
         if($req->input('periodo_fecha_emision:p_date')){
             $fecha = explode(' - ',$req->input('periodo_fecha_emision:p_date'));

             $desde = explode("/", trim($fecha[0]));
             $hasta = explode("/", trim($fecha[1]));
             $desdes = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
             $hastas = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
             $bancos = $bancos->whereBetween('fecha_emision', array($desdes,$hastas));
         }
 
         if($req->input('periodo_fecha_anulacion:p_date')){
            $fecha = explode(' - ',$req->input('periodo_fecha_anulacion:p_date'));

            $desde = explode("/", trim($fecha[0]));
            $hasta = explode("/", trim($fecha[1]));
            $desdes = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
            $hastas = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
            $bancos = $bancos->whereBetween('fecha_hora_anulacion', array($desdes,$hastas));
        }

        if($req->input('fecha_hora_creacion:p_date')){
            $fecha = explode(' - ',$req->input('fecha_hora_creacion:p_date'));

            $desde = explode("/", trim($fecha[0]));
            $hasta = explode("/", trim($fecha[1]));
            $desdes = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
            $hastas = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
            $bancos = $bancos->whereBetween('fecha_hora_creacion', array($desdes,$hastas));
        }

          $bancos = $bancos->orderBy('fecha_emision','ASC');
          $bancos = $bancos->get();
          $cuerpo = array();
           foreach($bancos as $libro){
              array_push($cuerpo, array( 
                                      date('d/m/Y', strtotime($libro->fecha_emision)),
                                      $libro->fecha_hora_creacion_format,
                                      $libro->banco_n,
                                      $libro->numero_cuenta,
                                      $libro->currency_code,
                                      $libro->cuenta_n,
                                      $libro->id_documento,
                                      number_format($libro->debe,2,",",""),
                                      number_format($libro->haber,2,",",""),
                                      $libro->nro_comprobante,
                                      $libro->suc_nombre,
                                      $libro->concepto,
                                    )
                         );

           }

            $nombreArchivo = 'Reporte Movimiento Cuenta-' . date('d-m-Y') . '.xlsx'; 
            return Excel::download(new ReporteMovimientoCuentaExport($cuerpo), $nombreArchivo); 

         } 

         public function generarExcelDetalleCuenta(Request $req){
               $exacto =  DB::table('vw_cuenta_corriente_cliente_resumen');
               if($req->input("idCliente") != ''){
                   $exacto =  $exacto->where('id_cliente',$req->input("idCliente"));
               }
               if($req->input("idMoneda") != ''){
                   $exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
               }
               $exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
               $exacto =  $exacto->get();
               $resultado = [];
               $contador = 0;
       
               foreach($exacto as $key=>$valor){
                   $extractoDetalles =  DB::table('vw_cuenta_corriente_cliente');
                   if($req->input("fecha_emision") != ''){
                        $fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
                        $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                        $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                        $extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
                    }
                    if($req->input("fecha_vencimiento") != ''){
                        $fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
                        $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                        $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                        $extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
                    }
                    if($req->input("negocio") != ''){
                        $extractoDetalles =  $extractoDetalles->where('id_unidad_negocio',$req->input("negocio"));
                    }	
                    if($req->input("grupo_id") != ''){
                        $extractoDetalles =  $extractoDetalles->where('id_grupo',$req->input("grupo_id"));
                    }	
                    if($req->input("idVendedor") != ''){
                        $extractoDetalles =  $extractoDetalles->where('vendedor_id',$req->input("idVendedor"));
                    }	
        
                   $extractoDetalles =  $extractoDetalles->where('id_cliente',$valor->id_cliente);
                   $extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
                   $extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
                   $extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
                   $extractoDetalles =  $extractoDetalles->get();
                   $detalles = [];
                   if(isset($extractoDetalles[0]->nro_documento)){
                        $resultado[$key] = $valor;
                        $detalles = []; 
                        foreach($extractoDetalles as $keys=>$detalle){
                            $detalles[$keys]['id'] = $detalle->id;
                            $detalles[$keys]['nro_documento'] = $detalle->nro_documento;
                            $detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
                            $detalles[$keys]['moneda'] = $detalle->moneda;
                            $detalles[$keys]['fecha_emision'] = $detalle->fecha_emision;
                            $detalles[$keys]['fecha_vencimiento'] =$detalle->fecha_vencimiento;
                            $detalles[$keys]['check_in'] =$detalle->check_in;
                            $detalles[$keys]['atraso'] = $detalle->atraso;
                            $detalles[$keys]['corte'] = $detalle->corte;
                            $detalles[$keys]['total_comision'] = $detalle->total_comision;
                            $detalles[$keys]['ticketcount'] = $detalle->ticketcount;
                            $detalles[$keys]['importe'] = $detalle->importe_bruto;
                            //$detalles[$keys]['importe_neto'] = $detalle->importe_neto;
                            $detalles[$keys]['importe_neto'] = $detalle->total_neto_documento;
                            $detalles[$keys]['saldo_neto'] = $detalle->saldo_neto;
                            $detalles[$keys]['proforma'] = $detalle->proforma;
                            $detalles[$keys]['senha'] = $detalle->senha;
                            if($detalle->proforma != ""){
                                $detalles[$keys]['proforma'] = $detalle->proforma;
                            }else{
                                $detalles[$keys]['proforma'] = $detalle->id_venta_rapida;
                            }
                            if($detalle->vendedor != " "){
                                $detalles[$keys]['vendedor'] = $detalle->vendedor;
                            }else{
                                $detalles[$keys]['vendedor'] = $detalle->vendedor_rapida;
                            }
                           $detalles[$keys]['pasajero_online'] = $detalle->pasajero_online;
                           $detalles[$keys]['usuario_proforma'] = $detalle->vendedor_rapida;
                           $detalles[$keys]['sucursal_fact'] = $detalle->sucursal_fact;
                        }
                        $resultado[$key]->detalles = $detalles;
                        
                    }
                   }

                $cuerpo = array();
                array_push($cuerpo, array( 
                            'Cliente',
                            '',//moneda
                            '',//importe
                            '', //saldo
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            ''
                    )
                );
                foreach($resultado as $key=>$cabecera){
                    array_push($cuerpo, array( 
                                        $cabecera->cliente,
                                        '',
                                        '',
                                        '',
                                        //$cabecera->moneda,
                                       // number_format($cabecera->importe,2,",",""),
                                        //number_format($cabecera->saldo,2,",",""),
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        ''
                                )
                             );
                    $perfil = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
                    if($perfil == 1||$perfil == 4||$perfil == 8){
                        $venta_prof = 'Proforma';
                        $pasaj = 'Pasajero';
                    }else{
                        $venta_prof = 'Venta';
                        $pasaj = 'Cliente';
                    }
                    array_push($cuerpo, array(
                                                'Id',
                                                'Nro. Documento',
                                                'Tipo Documento',
                                                'Emisión',
                                                'Vencimiento',
                                                'Check-In',
                                                'Atraso',
                                                'Corte',
                                                'Moneda',
                                                'Importe Bruto',
                                                'Seña',
                                                'Comisión',
                                                'Importe Neto',
                                                'Saldo',
                                                '',
                                                $venta_prof,
                                                $pasaj,
                                                'Vendedor',
                                                'Usuario Proforma',
                                                'Sucursal Facturacion'
                                            ));
                    $importe = 0;
                    $total_comision = 0;
                    $importe_neto = 0;
                    $saldo_neto = 0;
                    $total_senha =0;
                    foreach($cabecera->detalles as $keys=>$detalles){
                        if($detalles['ticketcount']  > 0 ){
                            $tk = 'Ticket';
                        }else{
                            $tk = '';
                        }
                        array_push($cuerpo, array( 
                                        $detalles['id'],
                                        $detalles['nro_documento'],
                                        $detalles['tipo_documento'],
                                        date('d/m/Y', strtotime($detalles['fecha_emision'])),
                                        date('d/m/Y', strtotime($detalles['fecha_vencimiento'])),
                                        date('d/m/Y', strtotime($detalles['check_in'])),
                                        $detalles['atraso'],
                                        $detalles['corte'],
                                        $detalles['moneda'],
                                        number_format($detalles['importe'],2,",",""),
                                        number_format($detalles['senha'],2,",",""),
                                        number_format($detalles['total_comision'],2,",",""),
                                        number_format($detalles['importe_neto'],2,",",""),
                                        number_format($detalles['saldo_neto'],2,",",""),
                                        $tk,
                                        $detalles['proforma'],
                                        $detalles['pasajero_online'],
                                        $detalles['vendedor'],
                                        $detalles['usuario_proforma'],
                                        $detalles['sucursal_fact']
                                    )
                        );
                        $importe = $importe + (float)$detalles['importe'];
                        $total_comision =  $total_comision + (float)$detalles['total_comision'];
                        $total_senha =  $total_comision + (float)$detalles['senha'];
                        $importe_neto =  $importe_neto + (float)$detalles['importe_neto'];
                        $saldo_neto =  $saldo_neto + (float)$detalles['saldo_neto'];
    
                    }   
                    array_push($cuerpo, array( 
                                'TOTALES',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                number_format($importe,2,",",""),
                                number_format($total_senha,2,",",""),
                                number_format($total_comision,2,",",""),
                                number_format($importe_neto,2,",",""),
                                number_format($saldo_neto,2,",",""),
                                ''
                        )
                    );
                    array_push($cuerpo, array( 
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    ''
                            )
                        );
                }
                if($req->input("idCliente") != ''){     
                    $cliente = Persona::where('id',$req->input("idCliente"))->first();
                    $clienteNombre = $cliente->nombre." ".$cliente->apellido;
                }else{
                    $clienteNombre = 'Todos los Clientes';
                }    
                if($req->input("idMoneda") != ''){
                    $currency = Currency::where('currency_id', $req->input("idMoneda"))->first();
                    $moneda = $currency->hb_desc."(".$currency->currency_code.")";
                }else{
                    $moneda = 'Todas las Monedas';
                }
            $detalle = new \StdClass;
            $detalle->cliente = $clienteNombre;
            $detalle->moneda = $moneda;

            $export = new ReporteDetalleGenericoExport($cuerpo, $detalle);
            return Excel::download($export, 'Reporte Detalle-' . date('d-m-Y') . '.xlsx');
        } 


        public function generarExcelDetalleSinSubtotal(Request $req){
            $exacto =  DB::table('vw_cuenta_corriente_cliente_resumen');
            if($req->input("idCliente") != ''){
                $exacto =  $exacto->where('id_cliente',$req->input("idCliente"));
            }
            if($req->input("idMoneda") != ''){
                $exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
            }
            $exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
            $exacto =  $exacto->get();
            $resultado = [];
            $contador = 0;
    
            foreach($exacto as $key=>$valor){
                $extractoDetalles =  DB::table('vw_cuenta_corriente_cliente');
                if($req->input("fecha_emision") != ''){
                     $fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
                     $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                     $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                     $extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
                 }
                 if($req->input("fecha_vencimiento") != ''){
                     $fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
                     $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                     $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                     $extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
                 }
                 if($req->input("negocio") != ''){
                    $extractoDetalles =  $extractoDetalles->where('id_unidad_negocio',$req->input("negocio"));
                }	
                if($req->input("grupo_id") != ''){
                    $extractoDetalles =  $extractoDetalles->where('id_grupo',$req->input("grupo_id"));
                }	
                if($req->input("idVendedor") != ''){
                    $extractoDetalles =  $extractoDetalles->where('vendedor_id',$req->input("idVendedor"));
                }	
    
                $extractoDetalles =  $extractoDetalles->where('id_cliente',$valor->id_cliente);
                $extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
                $extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
                $extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
                $extractoDetalles =  $extractoDetalles->get();
                $detalles = [];
                if(isset($extractoDetalles[0]->nro_documento)){
                     $resultado[$key] = $valor;
                     $detalles = [];
                     foreach($extractoDetalles as $keys=>$detalle){
                         $detalles[$keys]['id'] = $detalle->id;
                         $detalles[$keys]['cliente'] = $valor->cliente;
                         $detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
                         $detalles[$keys]['nro_documento'] = $detalle->nro_documento; 
                         $detalles[$keys]['moneda'] = $detalle->moneda;
                         $detalles[$keys]['fecha_emision'] = $detalle->fecha_emision;
                         $detalles[$keys]['fecha_vencimiento'] =$detalle->fecha_vencimiento;
                         $detalles[$keys]['check_in'] = $detalle->check_in;
                         $detalles[$keys]['atraso'] = $detalle->atraso;
                         $detalles[$keys]['corte'] = $detalle->corte;
                         $detalles[$keys]['senha'] = $detalle->senha;
                         $detalles[$keys]['ticketcount'] = $detalle->ticketcount;
                         $detalles[$keys]['importe'] = $detalle->importe_bruto;
                         //$detalles[$keys]['importe_neto'] = $detalle->importe_neto;
                        $detalles[$keys]['importe_neto'] = $detalle->total_neto_documento;
                         $detalles[$keys]['total_comision'] = $detalle->total_comision;
                         $detalles[$keys]['saldo_neto'] = $detalle->saldo_neto;
                         //$detalles[$keys]['proforma'] = $detalle->proforma;
                         if($detalle->proforma != ""){
                            $detalles[$keys]['proforma'] = $detalle->proforma;
                         }else{
                            $detalles[$keys]['proforma'] = $detalle->id_venta_rapida;
                            //$detalles[$keys]['proforma'] = $detalle->proforma;
                         }
                         if($detalle->vendedor != " "){
                            $detalles[$keys]['vendedor'] = $detalle->vendedor;
                         }else{
                            $detalles[$keys]['vendedor'] = $detalle->vendedor_rapida;
                         }
                         $detalles[$keys]['pasajero_online'] = $detalle->pasajero_online;
                         $detalles[$keys]['usuario_proforma'] = $detalle->vendedor_rapida;
                         $detalles[$keys]['sucursal_fact'] = $detalle->sucursal_fact;
                     }
                     $resultado[$key]->detalles = $detalles;
                   
                 }
                }

             $cuerpo = array();
             $perfil = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
             if($perfil == 1||$perfil == 4||$perfil == 8){
                 $venta_prof = 'Proforma';
                 $pasaj = 'Pasajero';
             }else{
                 $venta_prof = 'Venta';
                 $pasaj = 'Cliente';
             }

             array_push($cuerpo, array(
                                    'Id',
                                    'Cliente',
                                    'Nro. Documento',
                                    'Tipo Documento',
                                    'Emisión',
                                    'Vencimiento',
                                    'Check-In',
                                    'Atraso',
                                    'Corte',
                                    'Moneda',
                                    'Importe Bruto',
                                    'Seña',
                                    'Comisión',
                                    'Importe Neto',
                                    'Saldo',
                                    '',
                                    $venta_prof,
                                    $pasaj,
                                    'Vendedor',
                                    'Usuario Proforma',
                                    'Sucursal Facturacion'
                                ));
             foreach($resultado as $key=>$cabecera){
              
                 $importe = 0;
                 $total_comision = 0;
                 $importe_neto = 0;
                 $saldo_neto = 0;
                 foreach($cabecera->detalles as $keys=>$detalles){
                    if($detalles['ticketcount']  > 0 ){
                        $tk = 'Ticket';
                    }else{
                        $tk = '';
                    }
                     array_push($cuerpo, array( 
                                     $detalles['id'],
                                     $detalles['cliente'],
                                     $detalles['nro_documento'],
                                     $detalles['tipo_documento'],
                                     date('d/m/Y', strtotime($detalles['fecha_emision'])),
                                     date('d/m/Y', strtotime($detalles['fecha_vencimiento'])),
                                     date('d/m/Y', strtotime($detalles['check_in'])),
                                     $detalles['atraso'],
                                     $detalles['corte'],
                                     $detalles['moneda'],
                                     number_format($detalles['importe'],2,",",""),
                                     number_format($detalles['senha'],2,",",""),
                                     number_format($detalles['total_comision'],2,",",""),
                                     number_format($detalles['importe_neto'],2,",",""),
                                     number_format($detalles['saldo_neto'],2,",",""),
                                     $tk,
                                     $detalles['proforma'],
                                     $detalles['pasajero_online'],
                                     $detalles['vendedor'],
                                     $detalles['usuario_proforma'],
                                     $detalles['sucursal_fact']
                                 )
                     );
                     $importe = $importe + (float)$detalles['importe'];
                     $total_comision =  $total_comision + (float)$detalles['total_comision'];
                     $importe_senha =  $importe_neto + (float)$detalles['senha'];
                     $importe_neto =  $importe_neto + (float)$detalles['importe_neto'];
                     $saldo_neto =  $saldo_neto + (float)$detalles['saldo_neto'];
 
                 }   
             }
             if($req->input("idCliente") != ''){     
                 $cliente = Persona::where('id',$req->input("idCliente"))->first();
                 $clienteNombre = $cliente->nombre." ".$cliente->apellido;
             }else{
                 $clienteNombre = 'Todos los Clientes';
             }    
             if($req->input("idMoneda") != ''){
                 $currency = Currency::where('currency_id', $req->input("idMoneda"))->first();
                 $moneda = $currency->hb_desc."(".$currency->currency_code.")";
             }else{
                 $moneda = 'Todas las Monedas';
             }
         $detalle = new \StdClass;
         $detalle->cliente = $clienteNombre;
         $detalle->moneda = $moneda;

         $export = new ReporteDetalleGenericoExport($cuerpo, $detalle);
         return Excel::download($export, 'Reporte Detalle-' . date('d-m-Y') . '.xlsx');
     } 



public function generarExcelDetalleCuentaProveedor(Request $req){
        $exacto =  DB::table('vw_cuenta_corriente_proveedor_resumen');
        if($req->input("idProveedor") != ''){
            $exacto =  $exacto->where('id_proveedor',$req->input("idProveedor"));
        }
        if($req->input("idMoneda") != ''){
            $exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
        }
        if($req->input("proveedor_existencia") != ''){
			if($req->input("proveedor_existencia") == 'SI'){
				$exacto =  $exacto->where('id_pais', '<>', 1455);
			}else{
				$exacto =  $exacto->where('id_pais', 1455);
			}
		}

        $exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
        $exacto =  $exacto->get();
        $resultado = [];
        $contador = 0;
        $listado = [];
        foreach($exacto as $key=>$valor){
            $extractoDetalles =  DB::table('vw_cuenta_corriente_proveedor');
            if($req->input("fecha_emision") != ''){
                $fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
                $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                $extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
            }
            if($req->input("fecha_vencimiento") != ''){
                $fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
                $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                $extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
            }
            $extractoDetalles =  $extractoDetalles->where('id_proveedor',$valor->id_proveedor);
            $extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
            $extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
            $extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
            $extractoDetalles =  $extractoDetalles->get();
            if(isset($extractoDetalles[0]->nro_documento)){
                $resultado[$key] = $valor;
                $detalles = [];
                foreach($extractoDetalles as $keys=>$detalle){
                    $detalles[$keys]['id'] = $detalle->id;
                    $detalles[$keys]['nro_documento'] = $detalle->nro_documento;
                    $detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
                    $detalles[$keys]['moneda'] = $detalle->moneda;
                    $detalles[$keys]['fecha_emision'] = date('d/m/Y',strtotime($detalle->fecha_emision));
                    $detalles[$keys]['fecha_vencimiento'] =date('d/m/Y',strtotime($detalle->fecha_vencimiento));
                    $detalles[$keys]['fecha_de_gasto'] =date('d/m/Y',strtotime($detalle->fecha_de_gasto));
                    $detalles[$keys]['atraso'] = $detalle->atraso;
                    $detalles[$keys]['corte'] = $detalle->corte;
                    $detalles[$keys]['importe'] = $detalle->importe;
                    $detalles[$keys]['saldo_neto'] = $detalle->saldo;
                    $detalles[$keys]['proforma'] = $detalle->proforma;
                }
                $resultado[$key]->detalles = $detalles;
            }
        }

            $cuerpo = array();
            array_push($cuerpo, array( 
                        '',
                        'Proveedor',
                        'Moneda',
                        'Saldo',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        ''
                )
            );
            foreach($resultado as $key=>$cabecera){
                array_push($cuerpo, array( 
                                    $cabecera->proveedor,
                                    $cabecera->moneda,
                                    number_format($cabecera->saldo,2,",",""),
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    ''
                            )
                        );
                array_push($cuerpo, array(
                                            'Id',
                                            'Nro. Documento',
                                            'Tipo Documento',
                                            'Emisión',
                                            'Vencimiento',
                                            'Fecha Gasto',
                                            'Atraso',
                                            'Corte',
                                            'Moneda',
                                            'Importe Documento',
                                            'Saldo',
                                            'Proforma'
                                        ));
                $importe = 0;
                $total_comision = 0;
                $importe_neto = 0;
                $saldo_neto = 0;
                foreach($cabecera->detalles as $keys=>$detalles){

                    if($detalles['fecha_vencimiento'] == '31/12/1969'){
                        $vencimiento = "";
                    }else{
                        $vencimiento = $detalles['fecha_vencimiento'];
                    }
                    if($detalles['corte'] == ""){
                        $corte = '0-30';
                    }else{
                        $corte = $detalles['corte'];
                    }
                    if($detalles['fecha_emision'] == '31/12/1969'){
                        $fecha_emision = "";
                    }else{
                        $fecha_emision = $detalles['fecha_emision'];
                    }
                    if($detalles['fecha_de_gasto'] == '31/12/1969'){
                        $fecha_de_gasto = "";
                    }else{
                        $fecha_de_gasto = $detalles['fecha_de_gasto'];
                    }

                    array_push($cuerpo, array( 
                                    $detalles['id'],
                                    $detalles['nro_documento'],
                                    $detalles['tipo_documento'],
                                    $fecha_emision,
                                    $vencimiento,
                                    $fecha_de_gasto,
                                    $detalles['atraso'],
                                    $corte,
                                    $detalles['moneda'],
                                    number_format($detalles['importe'],2,",",""),
                                    number_format($detalles['saldo_neto'],2,",",""),
                                    $detalles['proforma']
                                )
                    );
                    $importe = $importe + (float)$detalles['importe'];
                    $saldo_neto =  $saldo_neto + (float)$detalles['saldo_neto'];

                }   
                array_push($cuerpo, array( 
                            'TOTALES',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            number_format($importe,2,",",""),
                            number_format($saldo_neto,2,",",""),
                            ''
                    )
                );
                array_push($cuerpo, array( 
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                ''
                        )
                    );
            }


            if($req->input("idProveedor") != ''){     
                $cliente = Persona::where('id',$req->input("idProveedor"))->first();
                $clienteNombre = $cliente->nombre." ".$cliente->apellido;
            }else{
                $clienteNombre = 'Todos los Proveedores';
            }    
            if($req->input("idMoneda") != ''){
                $currency = Currency::where('currency_id', $req->input("idMoneda"))->first();
                $moneda = $currency->hb_desc."(".$currency->currency_code.")";
            }else{
                $moneda = 'Todas las Monedas';
            }
        $detalle = new \StdClass;
        $detalle->cliente = $clienteNombre;
        $detalle->moneda = $moneda;

        $export = new ReporteDetalleGenericoExport($cuerpo, $detalle);
        return Excel::download($export, 'Reporte Detalle-' . date('d-m-Y') . '.xlsx');

        } 


    public function generarExcelDetalleSinSubtotalProveedor(Request $req){
        $exacto =  DB::table('vw_cuenta_corriente_proveedor_resumen');
        if($req->input("idProveedor") != ''){
            $exacto =  $exacto->where('id_proveedor',$req->input("idProveedor"));
        }
        if($req->input("idMoneda") != ''){
            $exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
        }
        if($req->input("proveedor_existencia") != ''){
			if($req->input("proveedor_existencia") == 'SI'){
				$exacto =  $exacto->where('id_pais', '<>', 1455);
			}else{
				$exacto =  $exacto->where('id_pais', 1455);
			}
		}

        $exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
        $exacto =  $exacto->get();

        $resultado = [];
        $contador = 0;

        foreach($exacto as $key=>$valor){
            $extractoDetalles =  DB::table('vw_cuenta_corriente_proveedor');
            if($req->input("fecha_emision") != ''){
                $fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
                $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                $extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
            }
            if($req->input("fecha_vencimiento") != ''){
                $fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
                $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                $extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
            }
            $extractoDetalles =  $extractoDetalles->where('id_proveedor',$valor->id_proveedor);
            $extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
            $extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
            $extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
            $extractoDetalles =  $extractoDetalles->get();
            $detalles = [];
            if(isset($extractoDetalles[0]->nro_documento)){
                $resultado[$key] = $valor;
                $detalles = [];
                foreach($extractoDetalles as $keys=>$detalle){
                    $detalles[$keys]['id'] = $detalle->id;
                    $detalles[$keys]['proveedor'] = $valor->proveedor;
                    $detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
                    $detalles[$keys]['nro_documento'] = $detalle->nro_documento; 
                    $detalles[$keys]['moneda'] = $detalle->moneda;
                    $detalles[$keys]['fecha_emision'] = $detalle->fecha_emision;
                    $detalles[$keys]['fecha_vencimiento'] =$detalle->fecha_vencimiento;
                    $detalles[$keys]['fecha_de_gasto'] =date('d/m/Y',strtotime($detalle->fecha_de_gasto));
                    $detalles[$keys]['atraso'] = $detalle->atraso;
                    $detalles[$keys]['corte'] = $detalle->corte;
                    $detalles[$keys]['importe'] = $detalle->importe;
                    $detalles[$keys]['saldo_neto'] = $detalle->saldo;
                    $detalles[$keys]['proforma'] = $detalle->proforma;
                }
                $resultado[$key]->detalles = $detalles;
            }
        }

        $cuerpo = array();
        array_push($cuerpo, array(
                                'Id',
                                'Proveedor',
                                'Nro. Documento',
                                'Tipo Documento',
                                'Emisión',
                                'Vencimiento',
                                'Fecha de Gasto',
                                'Atraso',
                                'Corte',
                                'Moneda',
                                'Importe Documento',
                                'Saldo',
                                'Proforma'
                            ));
        foreach($resultado as $key=>$cabecera){
            $importe = 0;
            $total_comision = 0;
            $importe_neto = 0;
            $saldo_neto = 0;
            foreach($cabecera->detalles as $keys=>$detalles){
                $vencimiento = $detalles['fecha_vencimiento'];
                if($detalles['corte'] == ""){
                    $corte = '0-30';
                }else{
                    $corte = $detalles['corte'];
                }

                array_push($cuerpo, array( 
                                $detalles['id'],
                                $detalles['proveedor'],
                                $detalles['nro_documento'],
                                $detalles['tipo_documento'],
                                date('d/m/Y', strtotime($detalles['fecha_emision'])),
                                date('d/m/Y', strtotime($detalles['fecha_de_gasto'])),
                                $vencimiento,
                                $detalles['atraso'],
                                $corte,
                                $detalles['moneda'],
                                number_format($detalles['importe'],2,",",""),
                                number_format($detalles['saldo_neto'],2,",",""),
                                $detalles['proforma']
                            )
                );
                $importe = $importe + (float)$detalles['importe'];
                $saldo_neto =  $saldo_neto + (float)$detalles['saldo_neto'];

            }   
        }
        if($req->input("idProveedor") != ''){     
            $cliente = Persona::where('id',$req->input("idProveedor"))->first();
            $clienteNombre = $cliente->nombre." ".$cliente->apellido;
        }else{
            $clienteNombre = 'Todos los Proveedores';
        }    
        if($req->input("idMoneda") != ''){
            $currency = Currency::where('currency_id', $req->input("idMoneda"))->first();
            $moneda = $currency->hb_desc."(".$currency->currency_code.")";
        }else{
            $moneda = 'Todas las Monedas';
        }
        $detalle = new \StdClass;
        $detalle->cliente = $clienteNombre;
        $detalle->moneda = $moneda;
        $export = new ReporteDetalleGenericoExport($cuerpo, $detalle);
        return Excel::download($export, 'Reporte Detalle-' . date('d-m-Y') . '.xlsx');

    } 


        public function generarExcelDetalleCuentaVendedor(Request $req){
            $exacto =  DB::table('vw_cuenta_corriente_vendedor_resumen');
            if($req->input("idCliente") != ''){
                $exacto =  $exacto->where('id_cliente',$req->input("idCliente"));
            }
            if($req->input("idMoneda") != ''){
                $exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
            }
            $exacto =  $exacto->where('id_usuario', $this->getIdUsuario());
            $exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
            $exacto =  $exacto->get();
            $resultado = [];
            $contador = 0;

            foreach($exacto as $key=>$valor){
                $extractoDetalles =  DB::table('vw_cuenta_corriente_vendedor');
                if($req->input("fecha_emision") != ''){
                    $fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
                    $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                    $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                    $extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
                }
                if($req->input("fecha_vencimiento") != ''){
                    $fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
                    $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                    $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                    $extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
                }
                $extractoDetalles =  $extractoDetalles->where('id_usuario', $this->getIdUsuario());
                $extractoDetalles =  $extractoDetalles->where('id_cliente',$valor->id_cliente);
                $extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
                $extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
                $extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
                $extractoDetalles =  $extractoDetalles->get();
                $detalles = [];
                if(isset($extractoDetalles[0]->nro_documento)){
                    $resultado[$key] = $valor;
                    $detalles = [];
                    foreach($extractoDetalles as $keys=>$detalle){
                        $detalles[$keys]['nro_documento'] = $detalle->nro_documento;
                        $detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
                        $detalles[$keys]['moneda'] = $detalle->moneda;
                        $detalles[$keys]['fecha_emision'] = $detalle->fecha_emision;
                        $detalles[$keys]['fecha_vencimiento'] =$detalle->fecha_vencimiento;
                        $detalles[$keys]['check_in'] =$detalle->check_in;
                        $detalles[$keys]['atraso'] = $detalle->atraso;
                        $detalles[$keys]['corte'] = $detalle->corte;
                        $detalles[$keys]['importe'] = $detalle->importe;
                        $detalles[$keys]['total_comision'] = $detalle->total_comision;
                        $detalles[$keys]['importe_neto'] = $detalle->importe_neto;
                        $detalles[$keys]['saldo_neto'] = $detalle->saldo_neto;
                        $detalles[$keys]['proforma'] = $detalle->proforma;
                        $detalles[$keys]['vendedor'] = $detalle->vendedor;
                        $detalles[$keys]['pasajero_online'] = $detalle->pasajero_online;
                    }
                    $resultado[$key]->detalles = $detalles;
                }
                }

            $cuerpo = array();
            array_push($cuerpo, array( 
                        'Cliente',
                        'Moneda',
                        'Importe',
                        'Saldo',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        ''
                )
            );
            foreach($resultado as $key=>$cabecera){
                array_push($cuerpo, array( 
                                    $cabecera->cliente,
                                    $cabecera->moneda,
                                    number_format($cabecera->importe,2,",",""),
                                    number_format($cabecera->saldo,2,",",""),
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    ''
                            )
                        );
                array_push($cuerpo, array(
                                            'Nro. Documento',
                                            'Tipo Documento',
                                            'Emisión',
                                            'Vencimiento',
                                            'Check-In',
                                            'Atraso',
                                            'Corte',
                                            'Moneda',
                                            'Importe Bruto',
                                            'Comisión',
                                            'Importe Neto',
                                            'Saldo',
                                            'Proforma',
                                            'Pasajero',
                                            'Vendedor'
                                        ));
                $importe = 0;
                $total_comision = 0;
                $importe_neto = 0;
                $saldo_neto = 0;
                foreach($cabecera->detalles as $keys=>$detalles){
                    array_push($cuerpo, array( 
                                    $detalles['nro_documento'],
                                    $detalles['tipo_documento'],
                                    date('d/m/Y', strtotime($detalles['fecha_emision'])),
                                    date('d/m/Y', strtotime($detalles['fecha_vencimiento'])),
                                    date('d/m/Y', strtotime($detalles['check_in'])),
                                    $detalles['atraso'],
                                    $detalles['corte'],
                                    $detalles['moneda'],
                                    number_format($detalles['importe'],2,",",""),
                                    number_format($detalles['total_comision'],2,",",""),
                                    number_format($detalles['importe_neto'],2,",",""),
                                    number_format($detalles['saldo_neto'],2,",",""),
                                    $detalles['proforma'],
                                    $detalles['pasajero_online'],
                                    $detalles['vendedor']
                                )
                    );
                    $importe = $importe + (float)$detalles['importe'];
                    $total_comision =  $total_comision + (float)$detalles['total_comision'];
                    $importe_neto =  $importe_neto + (float)$detalles['importe_neto'];
                    $saldo_neto =  $saldo_neto + (float)$detalles['saldo_neto'];

                }   
                array_push($cuerpo, array( 
                            'TOTALES',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            number_format($importe,2,",",""),
                            number_format($total_comision,2,",",""),
                            number_format($importe_neto,2,",",""),
                            number_format($saldo_neto,2,",",""),
                            ''
                    )
                );
                array_push($cuerpo, array( 
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                ''
                        )
                    );
            }
            if($req->input("idCliente") != ''){     
                $cliente = Persona::where('id',$req->input("idCliente"))->first();
                $clienteNombre = $cliente->nombre." ".$cliente->apellido;
            }else{
                $clienteNombre = 'Todos los Clientes';
            }    
            if($req->input("idMoneda") != ''){
                $currency = Currency::where('currency_id', $req->input("idMoneda"))->first();
                $moneda = $currency->hb_desc."(".$currency->currency_code.")";
            }else{
                $moneda = 'Todas las Monedas';
            }
        $detalle = new \StdClass;
        $detalle->cliente = $clienteNombre;
        $detalle->moneda = $moneda;

            $export = new ReporteDetalleV2Export($cuerpo, $detalle);
            return Excel::download($export, 'Reporte Detalle-' . date('d-m-Y') . '.xlsx');

        } 


    public function generarExcelDetalleSinSubtotalVendedor(Request $req){
        $exacto =  DB::table('vw_cuenta_corriente_vendedor_resumen');
        if($req->input("idCliente") != ''){
            $exacto =  $exacto->where('id_cliente',$req->input("idCliente"));
        }
        if($req->input("idMoneda") != ''){
            $exacto =  $exacto->where('id_moneda',$req->input("idMoneda"));
        }
        $exacto =  $exacto->where('id_usuario', $this->getIdUsuario());
        $exacto =  $exacto->where('id_empresa', $this->getIdEmpresa());
        $exacto =  $exacto->get();
        $resultado = [];
        $contador = 0;

        foreach($exacto as $key=>$valor){
            $extractoDetalles =  DB::table('vw_cuenta_corriente_vendedor');
            if($req->input("fecha_emision") != ''){
                $fechaPeriodo = explode(' - ', $req->input("fecha_emision"));
                $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                $extractoDetalles = $extractoDetalles->whereBetween('fecha_emision', array($desde.' 00:00:00',$hasta .' 23:59:59'));
            }
            if($req->input("fecha_vencimiento") != ''){
                $fechaPeriodo = explode(' - ', $req->input("fecha_vencimiento"));
                $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
                $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
                $extractoDetalles = $extractoDetalles->whereBetween('fecha_vencimiento', array($desde.' 00:00:00',$hasta .' 23:59:59'));
            }
            $extractoDetalles =  $extractoDetalles->where('id_cliente',$valor->id_cliente);
            $extractoDetalles =  $extractoDetalles->where('id_moneda',$valor->id_moneda);
            $extractoDetalles =  $extractoDetalles->where('id_usuario', $this->getIdUsuario());
            $extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
            $extractoDetalles =  $extractoDetalles->orderBy('id_moneda', 'DESC');
            $extractoDetalles =  $extractoDetalles->get();
            $detalles = [];
            if(isset($extractoDetalles[0]->nro_documento)){
                $resultado[$key] = $valor;
                $detalles = [];
                foreach($extractoDetalles as $keys=>$detalle){
                    $detalles[$keys]['cliente'] = $valor->cliente;
                    $detalles[$keys]['tipo_documento'] = $detalle->tipo_documento;
                    $detalles[$keys]['nro_documento'] = $detalle->nro_documento; 
                    $detalles[$keys]['moneda'] = $detalle->moneda;
                    $detalles[$keys]['fecha_emision'] = $detalle->fecha_emision;
                    $detalles[$keys]['fecha_vencimiento'] =$detalle->fecha_vencimiento;
                    $detalles[$keys]['check_in'] = $detalle->check_in;
                    $detalles[$keys]['atraso'] = $detalle->atraso;
                    $detalles[$keys]['corte'] = $detalle->corte;
                    $detalles[$keys]['importe'] = $detalle->importe;
                    $detalles[$keys]['total_comision'] = $detalle->total_comision;
                    $detalles[$keys]['importe_neto'] = $detalle->importe_neto;
                    $detalles[$keys]['saldo_neto'] = $detalle->saldo_neto;
                    $detalles[$keys]['proforma'] = $detalle->proforma;
                    $detalles[$keys]['vendedor'] = $detalle->vendedor;
                    $detalles[$keys]['pasajero_online'] = $detalle->pasajero_online;
                }
                $resultado[$key]->detalles = $detalles;
            }
            }

        $cuerpo = array();
        array_push($cuerpo, array(
                                'Cliente',
                                'Nro. Documento',
                                'Tipo Documento',
                                'Emisión',
                                'Vencimiento',
                                'Check-In',
                                'Atraso',
                                'Corte',
                                'Moneda',
                                'Importe Bruto',
                                'Comisión',
                                'Importe Neto',
                                'Saldo',
                                'Proforma',
                                'Pasajero',
                                'Vendedor'
                            ));
        foreach($resultado as $key=>$cabecera){
        
            $importe = 0;
            $total_comision = 0;
            $importe_neto = 0;
            $saldo_neto = 0;
            foreach($cabecera->detalles as $keys=>$detalles){
                array_push($cuerpo, array( 
                                $detalles['cliente'],
                                $detalles['nro_documento'],
                                $detalles['tipo_documento'],
                                date('d/m/Y', strtotime($detalles['fecha_emision'])),
                                date('d/m/Y', strtotime($detalles['fecha_vencimiento'])),
                                date('d/m/Y', strtotime($detalles['check_in'])),
                                $detalles['atraso'],
                                $detalles['corte'],
                                $detalles['moneda'],
                                number_format($detalles['importe'],2,",",""),
                                number_format($detalles['total_comision'],2,",",""),
                                number_format($detalles['importe_neto'],2,",",""),
                                number_format($detalles['saldo_neto'],2,",",""),
                                $detalles['proforma'],
                                $detalles['pasajero_online'],
                                $detalles['vendedor']
                            )
                );
                $importe = $importe + (float)$detalles['importe'];
                $total_comision =  $total_comision + (float)$detalles['total_comision'];
                $importe_neto =  $importe_neto + (float)$detalles['importe_neto'];
                $saldo_neto =  $saldo_neto + (float)$detalles['saldo_neto'];

            }   
        }
        if($req->input("idCliente") != ''){     
            $cliente = Persona::where('id',$req->input("idCliente"))->first();
            $clienteNombre = $cliente->nombre." ".$cliente->apellido;
        }else{
            $clienteNombre = 'Todos los Clientes';
        }    
        if($req->input("idMoneda") != ''){
            $currency = Currency::where('currency_id', $req->input("idMoneda"))->first();
            $moneda = $currency->hb_desc."(".$currency->currency_code.")";
        }else{
            $moneda = 'Todas las Monedas';
        }
        $detalle = new \StdClass;
        $detalle->cliente = $clienteNombre;
        $detalle->moneda = $moneda;

        $export = new ReporteDetalleV1Export($cuerpo, $detalle);
        return Excel::download($export, 'Reporte Detalle-' . date('d-m-Y') . '.xlsx');

        } 
 ////////////////////////////////////////////////////////////////////////////////////////////       
        public function generarExcelPlan(Request $req){
            $data_plan = PlanCuenta::where('activo',true)->where('id_empresa', $this->getIdEmpresa())->get();
            $cuerpo = array();
            foreach($data_plan as $cuenta){
                $tipo ='';
                if($cuenta->tipo === 'D'){
                    $tipo = 'DEUDOR';
                }
                if($cuenta->tipo === 'A'){
                    $tipo = 'ACREEDOR';
                }
                if($cuenta->tipo === 'T'){
                    $tipo = 'AMBOS';
                }
                $data = '';
                if($cuenta->asentable === true){
                    $data = 'SI';
                }
                if($cuenta->asentable === false){
                    $data = 'NO';
                }	
                array_push($cuerpo, array( 
                                        $cuenta->cod_txt,
                                        $cuenta->descripcion,
                                        $tipo,
                                        $data
                                      )
                           );
             }

             $export = new ReportePlanCuentasExport($cuerpo);
             return Excel::download($export, 'Reporte Plan Cuentas-' . date('d-m-Y') . '.xlsx');
             

             
        } 

        public function generarExcelFormaCobro(Request $req){
            $formaCobroCliente = new FormaCobroCliente;
            $formaCobroCliente = $formaCobroCliente->where('id_empresa',$this->getIdEmpresa());
            $formaCobroCliente = $formaCobroCliente->get();
            $cuerpo = array();

            foreach($formaCobroCliente as $formaCobro){
                if($formaCobro->depositable == true){
                    $depositable = 'SI';
                  }else{
                    $depositable = 'NO';
                  }
                  if($formaCobro->visible == true){
                    $visible = 'SI';
                  }else{
                    $visible = 'NO';
                  }

                array_push($cuerpo, array( 
                                        $formaCobro->denominacion,
                                        $depositable,
                                        $formaCobro->abreviatura,
                                        $visible,
                                        $formaCobro->tipo
                                      )
                           );
             }

             $export = new ReporteFormaCobroExport($cuerpo);
             return Excel::download($export, 'Reporte Forma Cobro-' . date('d-m-Y') . '.xlsx');
           }    

        public function generarExcelListAnticipo(Request $req){
            $data = new Anticipo;
            $data = $data->with('beneficiario_format','currency','op','op_aplicacion.opCabecera')
            ->selectRaw("to_char(fecha_pago,'DD/MM/YYYY') as fecha_format_pago,
                        to_char(fecha_vencimiento,'DD/MM/YYYY') as fecha_format_vencimiento,
                        *");
                  
            $data = $data->where('id_empresa',$this->getIdEmpresa());       
            $data = $data->where('tipo','P');//OBTIENE LOS ANTICIPOS DE TIPO PROVEEDOR 
    
            if($req->input('fecha_anticipo')){
              //$data = $data->where('fecha_transferencia',$req->input('fecha_emision')); 
              $fecha = explode('-', $req->input('fecha_anticipo'));
              $desde = $this->formatoFechaEntrada(trim($fecha[0]));
              $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
              $data = $data->whereBetween('fecha_pago', array(
                                    date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
                                    date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                                  ));
            }
        
            if($req->input('id_beneficiario')){
              $data = $data->where('id_beneficiario',$req->input('id_beneficiario'));
            }
            if($req->input('estado')){
                if($req->input('estado') == 3){ 
                    $data = $data->where('activo', false);  
                }else{
                    if($req->input('estado') == 1){ 
                        $data = $data->where('saldo','>',0);
                        $data = $data->where('activo',true);  
                    }else if($req->input('estado') == 2){ 
                      $data = $data->where('saldo','<=',0);
                      $data = $data->where('activo',true);  
      
                    }
                }
                //data = $data->where('pendiente',$req->input('pendiente'));
              }
                  if($req->input('nro_anticipo')){
              $data = $data->where('id',$req->input('nro_anticipo'));
            }
    
            /*if($req->input('nro_op')){
              $data = $data->where('op.nro_op',$req->input('nro_op'));
            }*/
    
            if($req->input('id_moneda')){
              $data = $data->where('id_moneda',$req->input('id_moneda'));
            }
            if($req->input('activo')){
              $data = $data->where('activo',$req->input('activo'));  
            }
            $data = $data->get();
    
    

            $resultadoBase = [];
            foreach($data as $key=>$resultado){
                $ops = '';
                foreach($resultado->op_aplicacion as $keys=>$opAplicacion){
                    if($keys == 0){
                      $ops .= $opAplicacion->opCabecera->nro_op;
                    }else{
                      $ops .= ','.$opAplicacion->opCabecera->nro_op;
                    }
                }
                if($resultado->activo == false){
                  $resultado->estado = 'ANULADO';
                }else{
                    if($resultado->saldo > 0){
                        $resultado->estado = 'PENDIENTE';
                    }else{
                        $resultado->estado = 'APLICADO';
                    }
                }

                $resultado->op_aplicado = $ops;
                $resultadoBase[] = $resultado;
            }    
            if($req->input('nro_op')){
              $resultados = [];
              foreach($resultadoBase as $key=>$resultado){
                if($resultado->op->nro_op == $req->input('nro_op')){
                  $resultados[] = $resultado;
                }
              }
              $resultadoBase = [];
              $resultadoBase = $resultados;
            }
            $data = $resultadoBase;
            $cuerpo = array();
            foreach($data as $anticipo){
                if($anticipo->activo == 'S'){
                    $activo = 'SI';
                } else {
                    $activo = 'NO';
                }
               $resultado = "";
               if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'V'){
                    if ($anticipo->id_venta !== 'null'){
                        if($resultado != ""){
                            $resultado .= '/';
                        }
                        $resultado .= $anticipo->id_venta;
                    }
                }else{    
                    if ($anticipo->id_grupo!== 'null'){
                        $resultado .= $anticipo->id_grupo;
                    }
                    if ($anticipo->id_proforma!== 'null'){
                        if($resultado != ""){
                            $resultado .= '/';
                        }
                        $resultado .= $anticipo->id_proforma;
                    }
                }    

                array_push($cuerpo, array( 
                                        $anticipo->id,
                                        $anticipo->op->nro_op,
                                        $anticipo->fecha_format_pago,
                                        $anticipo->op_aplicado,
                                        $anticipo->beneficiario_format->beneficiario_n,
                                        $resultado,
                                        $anticipo->importe,
                                        $anticipo->currency->currency_code,
                                        (float)$anticipo->saldo,
                                        $anticipo->estado,
                                        $anticipo->fecha_format_vencimiento,
                                      )
                           );
             }

             $export = new ReporteAnticipoProveedorExport($cuerpo);
             return Excel::download($export, 'Reporte Anticipo Proveedor -' . date('d-m-Y') . '.xlsx');

        } 
        
        public function generarExcelResumenCuenta(Request $req){
            $exactos =  DB::table('vw_cuenta_corriente_cliente_resumen');
            $exactos =  $exactos->where('id_empresa', $this->getIdEmpresa());
            $exactos =  $exactos->get();
            $cuerpo = array();
            foreach($exactos as $exacto){
                array_push($cuerpo, array( 
                                        $exacto->cliente,
                                        $exacto->moneda,
                                        number_format($exacto->importe,2,",",""),
                                        number_format($exacto->saldo,2,",","")
                                      )
                           );
             }
  

            $export = new ReporteResumenCuentaClienteExport($cuerpo);
            return Excel::download($export, 'Reporte Resumen Cuenta Cliente-' . date('d-m-Y') . '.xlsx');

        }   
        
        public function generarExcelResumenCuentaProveedors(Request $req){
            $exactos =  DB::table('vw_cuenta_corriente_proveedor_resumen');
            $exactos =  $exactos->where('id_empresa', $this->getIdEmpresa());
            $exactos =  $exactos->get();
            $cuerpo = array();
            foreach($exactos as $exacto){
                array_push($cuerpo, array( 
                                        $exacto->proveedor,
                                        $exacto->moneda,
                                        number_format($exacto->importe,2,",",""),
                                        number_format($exacto->saldo,2,",","")
                                      )
                           );
             }
             
             $export = new ReporteResumenCuentaProveedorExport($cuerpo);
             return Excel::download($export, 'Reporte Resumen Cuenta Proveedor-' . date('d-m-Y') . '.xlsx');
             
        }   

        public function generarExcelMayorCuenta(Request $req){
            $data = DB::table('vw_mayor_cuenta');
            $data = $data->where('id_empresa',$this->getIdEmpresa());
            $data = $data->where('asiento_activo', true);
            $data = $data->where('detalle_activo', true); 
    
            if ($req->input("id_cuenta_exenta") != '') 
            {
                $data = $data->where('id_cuenta_contable',$req->input("id_cuenta_exenta"));
    
            }
            if ($req->input("idMoneda") != '') 
            {
                $data = $data->where('id_moneda', $req->input("idMoneda"));
            }
            if($req->input("desde_hasta") != '')
            {
                $fechaTrim = trim($req->input("desde_hasta"));
                $periodo = explode('-', trim($fechaTrim));
                $desde = explode("/", trim($periodo[0]));
                $hasta = explode("/", trim($periodo[1]));
                $fecha_desde = $desde[2]."-".$desde[1]."-".$desde[0].' 00:00:00';
                $fecha_hasta = $hasta[2]."-".$hasta[1]."-".$hasta[0].' 23:59:59';
    
                $data = $data->whereBetween('fecha_documento', [$fecha_desde, $fecha_hasta]);
    
            }

            $data = $data->get();
    
            $cuerpo = array();
            foreach($data as $asiento){

                $debe = $asiento->columna_debe;
                $haber = $asiento->columna_haber;

                if($req->input("idMoneda") && $debe && $req->input("idMoneda") != 111){
                    $debe = $asiento->monto_original;
                } else {
                    $debe = str_replace(',','.', str_replace('.','', $debe));
                }

                if($req->input("idMoneda") && $haber && $req->input("idMoneda") != 111){
                    $haber = $asiento->monto_original;
                } else {
                    $haber = str_replace(',','.', str_replace('.','', $haber));
                }


                array_push($cuerpo, array( 
                                        date('d/m/Y ', strtotime($asiento->fecha_documento)),
                                        $asiento->nro_documento,
                                        $asiento->concepto_detalle,
                                        $asiento->cotizacion,
                                        $asiento->currency_code,
                                        $debe,
                                        $haber
                                      )
                           );
             }
t;

             $export = new ReporteMayorCuentasExport($cuerpo);
             return Excel::download($export, 'Reporte Mayor Cuentas-' . date('d-m-Y') . '.xlsx');
             
        } 
        
        
        public function generarExcelAnticipo(Request $req){

            $data = new Anticipo;
            $data = $data->with('beneficiario_format','estado','currency','recibo','proforma')
            ->selectRaw("to_char(fecha_pago,'DD/MM/YYYY') as fecha_format_pago,
                        to_char(fecha_vencimiento,'DD/MM/YYYY') as fecha_format_vencimiento,
                        to_char(fecha,'DD/MM/YYYY') as fecha_format_anticipo,
                        *");
                  
            $data = $data->where('id_empresa',$this->getIdEmpresa());       
            $data = $data->where('tipo','C');//OBTIENE LOS ANTICIPOS DE TIPO CLIENTE 
            if($req->input('fecha_anticipo')){
                //$data = $data->where('fecha_transferencia',$req->input('fecha_emision')); 
                $fecha = explode('-', $req->input('fecha_anticipo'));
                $desde = $this->formatoFechaEntrada(trim($fecha[0]));
                $hasta = $this->formatoFechaEntrada(trim($fecha[1]));
                $data = $data->whereBetween('fecha', array(
                                      date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
                                      date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
                                    ));
              }
      
            if($req->input('nro_anticipo')){
              $data = $data->where('id',$req->input('nro_anticipo'));
            }
            if($req->input('id_beneficiario')){
                $data = $data->where('id_beneficiario',$req->input('id_beneficiario'));
            }
            if($req->input('id_moneda')){
              $data = $data->where('id_moneda',$req->input('id_moneda'));
            }
           /* if($req->input('estado')){
              if($req->input('estado') == 3){ 
                  $data = $data->where('activo', false);  
              }else{
                  if($req->input('estado') == 1){ 
                      $data = $data->where('saldo','>',0);
                      $data = $data->where('activo',true);  
                  }else if($req->input('estado') == 2){ 
                    $data = $data->where('saldo','<=',0);
                    $data = $data->where('activo',true);  
     
                  }
              }
              //data = $data->where('pendiente',$req->input('pendiente'));
            }*/
            if($req->input('numProforma')){
                $data = $data->where('id_proforma',$req->input('numProforma'));  
            }
            $data = $data->get();
            $arrayBase = [];
            foreach($data as $key=>$base){
                 $arrayBase[$key] = $base;
                 if(isset($base->id_proforma)){                 
                    $usuario = DB::select("SELECT CONCAT(personas.nombre,' ',personas.apellido) as usuario_proforma FROM personas, proformas WHERE personas.id = proformas.id_usuario AND proformas.id =".$base->id_proforma);
                    $arrayBase[$key]['usuario_proforma'] = $usuario[0]->usuario_proforma;
                 }else{
                    $arrayBase[$key]['usuario_proforma'] = '';
                 }
            }
    
            $resultadoBase = $data;
     
            if($req->input('nro_recibo')){
                $resultados = [];
                foreach($resultadoBase as $key=>$resultado){
                  if($resultado->recibo->nro_recibo == $req->input('nro_recibo')){
                    $resultados[] = $resultado;
                  }
                }
                $resultadoBase = [];
                $resultadoBase = $resultados;
              }
     
     
              if($req->input('estado') && count($req->input('estado'))){
                $resultados = [];
                $estados_seleccionados = $req->input('estado');
                foreach($resultadoBase as $key=>$resultado){
                    $coincidencia = strpos($resultado->saldo, 'e-');
                    if ($coincidencia === false) {
                    }else{
                        $resultado->saldo = 0;
                    }
                    if($resultado->activo == false){
                        $resultado->estado = 'ANULADO';
                    }else{
                        if($resultado->saldo > 0){
                            $resultado->estado_detalle = 'PENDIENTE';
                        }else{
                            $resultado->estado_detalle = 'APLICADO';
                        }
                    }
            
                    if(in_array(3, $estados_seleccionados)){ 
                        if($resultado->activo == false){
                            $resultados[] = $resultado;
                        }
                    }
                    if(in_array(1, $estados_seleccionados)){ 
                        if($resultado->activo == true && $resultado->saldo > 0){
                            $resultados[] = $resultado;
                        }
                    }
                    if(in_array(2, $estados_seleccionados)){ 
                        if($resultado->activo == true && $resultado->saldo <= 0){
                            $resultados[] = $resultado;
                        }
                    }
                    
                }
    
                $resultadoBase = [];
                $resultadoBase = $resultados;
            } 
     
            $data  = $resultadoBase;

            $cuerpo = array();
            foreach($data as $anticipo){
                if(isset($anticipo->beneficiario_format->beneficiario_n)){
                    $beneficiario = $anticipo->beneficiario_format->beneficiario_n;
                }else{
                    $beneficiario ="";
                }    
                if(isset($anticipo->proforma->id)){
                    $proforma = $anticipo->proforma->id;
                }else{
                    $proforma ="";
                }    
                if(isset($anticipo->grupo->denominacion)){
                    $grupo = $anticipo->grupo->denominacion;
                }else{
                    $grupo ="";
                }    
               

                    
                if($anticipo->currency->id == 111){
                    $saldo =  str_replace(',','.', str_replace('.','', $anticipo->saldo));
                    $importe = str_replace(',','.', str_replace('.','', $anticipo->importe));
                }else{
                    $saldo = number_format($anticipo->saldo,2,",","");
                    $importe = number_format($anticipo->importe,2,",","");
                }    
              //  dd($anticipo->estado->denominacion);
                array_push($cuerpo, array( 
                                        $beneficiario,
                                        $anticipo->id,
                                        $anticipo->recibo->nro_recibo,
                                        $anticipo->concepto,
                                        $importe,
                                        $anticipo->currency->currency_code,
                                        //$anticipo->estado->denominacion,
                                        $estadoDenominacion = isset($anticipo->estado->denominacion) ? $anticipo->estado->denominacion : 'Anulado',
                                        $proforma,
                                        $anticipo->usuario_proforma,
                                        $grupo,
                                        $anticipo->fecha_format_pago,
                                        $saldo
                                         /*$resultado*/
                                      )
                           );
             }
           
                          
             $export = new ReporteAnticiposV2Export($cuerpo);
             return Excel::download($export, 'Reporte Anticipos v2-' . date('d-m-Y') . '.xlsx');
             

    }

    public function generarExcelIncentivoVendedor(Request $request){
            $query = "SELECT vendedor_nombre, vendedor_apellido,id_moneda_venta, currency_code,sum(total) as total, sum(total_costos) as costo, sum(renta) as renta
            FROM incentivo_vendedor_detalle
            WHERE id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

            if($request->input("idVendedor") != ''){
            $query.= ' AND id_usuario = '.$request->input("idVendedor");
            }

            if($request->input("negocio") != ''){
                $query.= " AND id_unidad_negocio =".$request->input("negocio");
            }	

            if($request->input("idMoneda") != ''){
                $query.= " AND id_moneda_venta = ".$request->input("idMoneda");
            }	

            if($request->input('fecha_facturacion') != ""){
                $fechaPeriodo = explode(' - ', $request->input('fecha_facturacion'));
                $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
                $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
                $query .= " AND fecha_hora_facturacion BETWEEN '".$desde."' AND '".$hasta."'";
            } 
                        
            $query.= " GROUP BY id_usuario, id_moneda_venta, vendedor_nombre,vendedor_apellido,currency_code";

            $incentivo = DB::select($query);
            $cuerpo = array();
            foreach($incentivo as $incentivos){
                if($incentivos->id_moneda_venta == 111){
                    $total =  str_replace(',','.', str_replace('.','', $incentivos->total));
                    $costos = str_replace(',','.', str_replace('.','', $incentivos->costo));
                    $renta =  str_replace(',','.', str_replace('.','', $incentivos->renta));

                }else{
                    $total = number_format($incentivos->total,2,",","");
                    $costos = number_format($incentivos->costo,2,",","");
                    $renta = number_format($incentivos->renta,2,",","");

                }    
                array_push($cuerpo, array( 
                                    $incentivos->vendedor_nombre."".$incentivos->vendedor_apellido,
                                    $incentivos->currency_code,
                                    $total,
                                    $costos,
                                    $renta
                                  )
                       );
         }
         
         $export = new ReporteAnticiposExport($cuerpo);
         return Excel::download($export, 'Reporte Anticipos-' . date('d-m-Y') . '.xlsx');
         
    }

    public function generarExcelIncentivoVendedorDetalle(Request $req){
        $query = "SELECT id_usuario, vendedor_nombre, vendedor_apellido,id_moneda_venta, currency_code,sum(total) as total, sum(total_costos) as costo, sum(renta) as renta
                FROM incentivo_vendedor_detalle
                WHERE id_empresa = ".Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

        if($req->input("idVendedor") != ''){
        $query.= ' AND id_usuario = '.$req->input("idVendedor");
        }

        if($req->input("negocio") != ''){
            $query.= " AND id_unidad_negocio =".$req->input("negocio");
        }	

        if($req->input("idMoneda") != ''){
            $query.= " AND id_moneda_venta = ".$req->input("idMoneda");
        }	

        if($req->input('fecha_facturacion') != ""){
            $fechaPeriodo = explode(' - ', $req->input('fecha_facturacion'));
            $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
            $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
            $query .= " AND fecha_hora_facturacion BETWEEN '".$desde."' AND '".$hasta."'";
        } 
                    
        $query.= " GROUP BY id_usuario, id_moneda_venta, vendedor_nombre,vendedor_apellido,currency_code";

        $incentivo = DB::select($query);
        $resultado = [];
        $contador = 0;

        foreach($incentivo as $key=>$valor){
			$extractoDetalles =  DB::table('incentivo_vendedor_detalle');
			$extractoDetalles =  $extractoDetalles->where('id_usuario',$valor->id_usuario);
			$extractoDetalles =  $extractoDetalles->where('id_moneda_venta',$valor->id_moneda_venta);
			if($req->input("negocio") != ''){
				$extractoDetalles =  $extractoDetalles->where('id_unidad_negocio',$req->input("negocio"));
			}
	
			$extractoDetalles =  $extractoDetalles->where('id_empresa', $this->getIdEmpresa());
			if($req->input("fecha_facturacion") != ''){
				$fechaPeriodo = explode(' - ', $req->input("fecha_facturacion"));
				$desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
				$hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
				$extractoDetalles = $extractoDetalles->whereBetween('fecha_hora_facturacion', array($desde.' 00:00:00',$hasta .' 23:59:59'));
			}
			$extractoDetalles =  $extractoDetalles->orderBy('id_moneda_venta', 'DESC');
			$extractoDetalles =  $extractoDetalles->get();
			 if(isset($extractoDetalles[0]->nro_factura)){
				$resultado[$key] = $valor;
				$detalles = [];
				foreach($extractoDetalles as $keys=>$detalle){
					$detalles[$keys]['cliente'] = $detalle->nombre." ".$detalle->apellido;
					$detalles[$keys]['nro_factura'] = $detalle->nro_factura;
					$detalles[$keys]['id_proforma'] = $detalle->id_proforma;
					$detalles[$keys]['total'] = $detalle->total;
					$detalles[$keys]['total_costos'] = $detalle->total_costos;
					if($detalle->id_estado_factura == 29){
						$detalles[$keys]['renta'] = $detalle->renta;
					}else{
						$detalles[$keys]['renta'] = 0;
					}
					$detalles[$keys]['moneda'] = $detalle->currency_code;
				}
				$resultado[$key]->detalles = $detalles;
			}
		}

      ////////////////////////////////////////////////////////////////  
      $cuerpo = array();
      array_push($cuerpo, array( 
                  'Vendedor',
                  'Moneda',
                  'Total Factura',
                  'Total Costos',
                  'Total Renta',
                  '',
                  ''
          )
      );
      foreach($resultado as $key=>$cabecera){
        if($cabecera->id_moneda_venta == 111){
            $total =  str_replace(',','.', str_replace('.','', $cabecera->total));
            $costos = str_replace(',','.', str_replace('.','', $cabecera->costo));
            $renta =  str_replace(',','.', str_replace('.','', $cabecera->renta));

        }else{
            $total = number_format($cabecera->total,2,",","");
            $costos = number_format($cabecera->costo,2,",","");
            $renta = number_format($cabecera->renta,2,",","");

        }    
          array_push($cuerpo, array( 
                            $cabecera->vendedor_nombre."".$cabecera->vendedor_apellido,
                            $cabecera->currency_code,
                            $total,
                            $costos,
                            $renta,
                            '',
                            '',

                        )
                   );
          array_push($cuerpo, array(
                                      'Cliente',
                                      'Nro. Factura',
                                      'Proforma',
                                      'Moneda',
                                      'Total Factura',
                                      'Total Costos',
                                      'Total Renta'
                                  ));
          $importe = 0;
          $costo = 0;
          $renta = 0;
          foreach($cabecera->detalles as $keys=>$detalles){
              array_push($cuerpo, array( 
                              $detalles['cliente'],
                              $detalles['nro_factura'],
                              $detalles['id_proforma'],
                              $detalles['moneda'],
                              number_format($detalles['total'],2,",",""),
                              number_format($detalles['total_costos'],2,",",""),
                              number_format($detalles['renta'],2,",",""),
                          )
              );
              $importe = $importe + (float)$detalles['total'];
              $costo =  $costo + (float)$detalles['total_costos'];
              $renta =  $renta + (float)$detalles['renta'];

          }   
          array_push($cuerpo, array( 
                      'TOTALES',
                      '',
                      '',
                      '',
                      number_format($importe,2,",",""),
                      number_format($costo,2,",",""),
                      number_format($renta,2,",",""),
                      ''
              )
          );
          array_push($cuerpo, array( 
                          '',
                          '',
                          '',
                          '',
                          '',
                          '',
                          '',
                  )
              );
      }
        $detalle = new \StdClass;

        $nombreArchivo = 'Reporte Detalle-' . date('d-m-Y') . '.xls';
        return Excel::download(new ReporteVendedorIncentivoDetalleExport($cuerpo, $detalle), $nombreArchivo);


    }

    
   public function generarExcelOp(Request $req){

        $data = DB::table('vw_listado_op');
        $data = $data->where('id_empresa',$this->getIdEmpresa());

        if($req->input('id_moneda')){
          $data = $data->where('id_moneda',$req->input('id_moneda'));
        }
        if($req->input('id_proveedor')){
          $data = $data->where('id_proveedor',$req->input('id_proveedor'));
        } 
         if($req->input('id_estado')){
          $data = $data->whereIn('id_estado',$req->input('id_estado'));
        } 
         if($req->input('nro_op')){
          $data = $data->where('nro_op',$req->input('nro_op'));
        }
        if($req->input('periodo_creacion:p_date')){
          $fechaPeriodo = explode(' - ', $req->input('periodo_creacion:p_date'));
          $desdeC  = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
          $hastaC  = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
          $data = $data->whereBetween('fecha_hora_creacion',array($desdeC,$hastaC));         
        }
        if($req->input('periodo_proceso:p_date')){
          $fechaPeriodo = explode(' - ', $req->input('periodo_proceso:p_date'));
          $desdeC  = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
          $hastaC  = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
          $data = $data->whereBetween('fecha_hora_procesado',array($desdeC,$hastaC));         
        }
        if($req->input('periodo_verificacion:p_date')){
          $fechaPeriodo = explode(' - ', $req->input('periodo_verificacion:p_date'));
          $desdeC  = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
          $hastaC  = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
          $data = $data->whereBetween('fecha_hora_verificado',array($desdeC,$hastaC));         
        }
        if($req->input('periodo_autorizacion:p_date')){
          $fechaPeriodo = explode(' - ', $req->input('periodo_autorizacion:p_date'));
          $desdeC  = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
          $hastaC  = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
          $data = $data->whereBetween('fecha_hora_autorizado',array($desdeC,$hastaC));         
        }
        if($req->input('periodo_anulacion:p_date')){
          $fechaPeriodo = explode(' - ', $req->input('periodo_anulacion:p_date'));
          $desdeC  = $this->formatoFechaEntrada($fechaPeriodo[0]).' 00:00:00';
          $hastaC  = $this->formatoFechaEntrada($fechaPeriodo[1]).' 23:59:59';
          $data = $data->whereBetween('fecha_hora_anulacion',array($desdeC,$hastaC));         
        }
        if($req->input('tipo_op'))
        {
          $data = $data->where('fondo_fijo', $req->input('tipo_op') );   
        } 

        $data = $data->get();

        $cuerpo = array();
        foreach($data as $op){
                $ff = '';
                $cantidad = $op->cantidad_facturas;
                if ($op->fondo_fijo == true){
					$ff = 'FF';
				}else{
					if (($op->fondo_fijo == false)){
                         $ff = 'OP';
					}
                    if($op->cantidad_anticipos > 0){
                        $ff = 'AN';
                        $cantidad = $op->cantidad_anticipos;
                    }
				}
                $resultado = $ff." (".$cantidad.")";

                if($op->id_moneda == 111){
                    $total_neto_pago = str_replace(',','.', str_replace('.','', $op->total_neto_pago));
                }else{
                    $total_neto_pago = str_replace(',','', str_replace(',','.', $op->total_neto_pago));
                }
                $resultado = '';
                if($op->cuenta_fondo != ""){
                    $resultado .=  $op->cuenta_fondo." ";
                }
                if($op->numero_cuenta != ""){
                    $resultado .=  $op->numero_cuenta;
                }

                array_push($cuerpo, array( 
                                        $op->nro_op,
                                        $resultado,
                                        $op->fecha_creacion,
                                        $op->proveedor_n,
                                        $op->currency_code,
                                        $op->forma_pago,
                                        $op->numeros_comprobantes,
                                        $resultado,
                                        $total_neto_pago,
                                        $op->estado_n
                                      )
                           );
             }


    return Excel::download(new ReporteOpExport($cuerpo), 'Reporte Op-' . date('d-m-Y') . '.xls');
    }  

    
    
    public function generarExcelPersona(Request $req){
		ini_set('memory_limit', '-1');
		set_time_limit(300);
        $idTipoPersona = $req->input('idTipoPersona');
        $idPersona = $req->input('idPersona');
        $estado = $req->input('estado');
        $cedulaRuc = $req->input('inputCi');
        $email = $req->input('inputEmail');
        $idEmpresaAgencia = $req->input('idEmpresaAgencia');
        $query = " SELECT 
                    ag.nombre as nombreAgencia , 
                    p.nombre as nombrePersona,
                    P.apellido as apellidoPersona, 
                    p.email,
                    tp.denominacion, 
                    p.activo,
                    p.activo_dtpmundo,
                    p.denominacion_comercial, 
                    p.documento_identidad, 
                    to_char(p.fecha_alta,'DD/MM/YYYY') as fecha_alta,p.id 
                    FROM personas AS p
                    LEFT JOIN personas AS ag ON p.id_persona = ag.id
                    LEFT JOIN tipo_persona AS tp ON p.id_tipo_persona = tp.id
                    WHERE p.id_empresa = ".$this->getIdEmpresa();


            if($idTipoPersona != ''){	

            $query .= 'AND p.id_tipo_persona = '.intval($idTipoPersona);
            }
            if($idPersona != ''){
            $query .= ' AND p.id = '.intval($idPersona);

            } 
            if($estado != ''){
            $query .= ' AND p.activo = '.$estado.' ';
            }

            if($cedulaRuc != ''){
            $query .= " AND p.documento_identidad LIKE  '%".$cedulaRuc."%' ";
            }
            if($email != ''){
            $email = trim(strtoupper($email));	
            $query .= " AND UPPER(p.email)  LIKE '%".$email."%' ";

            }
            if($idEmpresaAgencia != ''){
                if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != 1){
                    $query .= ' AND (p.id_persona = '.intval($idEmpresaAgencia).' OR   p.id = '.intval($idEmpresaAgencia).') ';
                }
            }
            $query .= 'order by nombrepersona ASC';
            $personas = DB::select($query);

            $cuerpo = array();
            foreach($personas as $persona){
                if($persona->activo == true){
					$iconoG = 'SI';
				}else{
					$iconoG = 'NO';
				}
				if($persona->activo_dtpmundo == true){
					$iconoO = 'SI';
				}else{
					$iconoO = 'NO';
				}

                if($this->getIdEmpresa() == 1){
                    array_push($cuerpo, array( 
                                            $persona->nombreagencia,
                                            $persona->nombrepersona.' '.$persona->apellidopersona,
                                            $persona->denominacion,
                                            $persona->email,
                                            $persona->id,
                                            $persona->documento_identidad,
                                            $iconoG,
                                            $iconoO
                                        )
                            );
                }else{
                    array_push($cuerpo, array( 
                                            $persona->nombreagencia,
                                            $persona->nombrepersona.' '.$persona->apellidopersona,
                                            $persona->denominacion,
                                            $persona->email,
                                            $persona->id,
                                            $persona->documento_identidad,
                                            $iconoG
                                        )
                             );
                }
        
             }

             $export = new ReportePersonasExport($cuerpo);
             return Excel::download($export, 'Reporte Personas-' . date('d-m-Y') . '.xlsx');

    }

    public function generarExcelProducto(Request $req){
		    ini_set('memory_limit', '-1');
		    set_time_limit(300);
            $productos = DB::table('productos');
            $productos = $productos->select('*',
                                                    DB::raw('get_text_boolean(genera_voucher) as genera_voucher_txt'),
                                                    DB::raw('get_text_boolean(tiene_dtplus) as tiene_dtplus_txt'),
                                                    DB::raw('get_text_boolean(comisiona_estando_solo) as comisiona_estando_solo_txt'),
                                                    DB::raw('get_text_boolean(es_comisionable) as es_comisionable_txt'),
                                                    DB::raw('get_text_boolean(origen_extranjero) as origen_extranjero_txt'),
                                                    DB::raw('get_text_boolean(imprimir_en_factura) as imprimir_en_factura_txt'),
                                                    DB::raw('get_text_boolean(visible) as visible_txt'),
                                                    DB::raw('get_text_boolean(multiple_voucher) as multiple_voucher_txt'));
                                                    
                                                    $productos = $productos->where('id_empresa',$this->getIdEmpresa());
            if($req->input('tiene_dtplus') != ''){
                $productos = $productos->where('tiene_dtplus',$req->input('tiene_dtplus'));
            }
            if($req->input('genera_voucher') != ''){
                $productos = $productos->where('genera_voucher',$req->input('genera_voucher'));
            }

            if($req->input('comisionable') != ''){
                $productos = $productos->where('es_comisionable',$req->input('comisionable'));
            }
            if($req->input('imprimir_en_factura') != ''){
                $productos = $productos->where('imprimir_en_factura',$req->input('imprimir_en_factura'));
            }
            if($req->input('activo') != ''){
                $productos = $productos->where('activo',$req->input('activo'));
            }
            /*if($datos->num_cuenta != ''){
                $productos = $productos->where('cta_contable_compra',$datos->num_cuenta);
            }*/
            if($req->input('denominacion') != ''){
                /*echo '<pre>';
                print_r($datos->denominacion);*/
                $productos = $productos->where('denominacion', 'ilike','%'. $req->input('denominacion').'%');
            }
            if($req->input('tipo_producto') != '')
            {
                $productos = $productos->where('tipo_producto',$req->input('tipo_producto'));
            }
            if($req->input('cod_producto') != '')
            {
                $productos = $productos->where('woo_sku', 'ilike','%'.$req->input('cod_producto') .'%');
            }

            if($req->input('id_proveedor') != '')
            {
                $productos = $productos->where('id_proveedor',$req->input('id_proveedor'));
            }

            if($req->input('id_plan_cuenta_venta') != '')
            {
                $productos = $productos->where('id_cuenta_contable_venta',$req->input('id_plan_cuenta_venta'));
            }

            if($req->input('id_plan_cuenta') != '') 
            {
                $productos = $productos->where('id_plan_cuenta',$req->input('id_plan_cuenta'));
            }


            $productos = $productos->get();
            foreach ($productos as $key => $value) {
                $value->btn = "<a href='".route('editProducto',['id' =>$value->id])."' class='btn btn-info' style='padding-left: 6px;padding-right: 6px;' role='button'><i class='fa fa-fw fa-edit'></i></a>";
        
                        if($value->activo == true){
                            $value->estado_val = '<i class="fa fa-fw fa-circle  verde "  style="color: green;"></i>';
                        }else{
                            $value->estado_val = '<i class="fa fa-fw fa-circle  rojo "  style="color: red;"></i>';
                        }
            }
        
            $cuerpo = array();
            foreach($productos as $anticipo){
                array_push($cuerpo, array( 
                                        $anticipo->denominacion,
                                        $anticipo->id_grupos_producto,
                                        $anticipo->precio_costo,
                                        $anticipo->woo_sku,
                                        $anticipo->genera_voucher_txt,
                                        $anticipo->multiple_voucher_txt,
                                        $anticipo->tiene_dtplus_txt,
                                        $anticipo->es_comisionable_txt,
                                        $anticipo->comisiona_estando_solo_txt,
                                        $anticipo->comision_base,
                                        $anticipo->imprimir_en_factura_txt,
                                        $anticipo->visible_txt,
                                        $anticipo->estado_val
                                      )
                           );
             }

             $export = new ReporteProductosExport($cuerpo);
             return Excel::download($export, 'Reporte Productos-' . date('d-m-Y') . '.xlsx');

        }



        public function generarExcelAplicacionAnticipos(Request $req){
            $resultado =[];
		
			$query = "SELECT id,id_beneficiario,fecha, beneficiario_nombre, beneficiario_apellido, nro_recibo,importe,saldo, moneda";
				
			$query .=" FROM vw_anticipo_clientes ";

			$query .="WHERE id_empresa = ".$this->getIdEmpresa();

			if($req->input('id_cliente') != 0){
			$query .="AND id_beneficiario =".$req->input('id_cliente');
			}

			if($req->input('fecha_emision') != ""){
			$fechaPeriodo= explode('-', $req->input('fecha_emision'));
			$check_in = explode('/', $fechaPeriodo[0]);
			$check_out = explode('/', $fechaPeriodo[1]);
			$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]).' 00:00:00';
			$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]).' 23:59:59';
			$query .="AND fecha BETWEEN '".$checkIn."' AND '".$checkOut."'";
			}

			$query .=" GROUP BY id, beneficiario_nombre, beneficiario_apellido, nro_recibo, importe,saldo, moneda,id_beneficiario,fecha";

			$anticipos = DB::select($query);

			if(!empty($anticipos)){
				$contador = 0;
				foreach($anticipos as $key=>$resumen){
					$detalleResumenes = DB::select("SELECT *
													FROM vw_anticipo_clientes
													WHERE monto is not null
													AND id = ".$resumen->id );	
					if(!empty($detalleResumenes)){
						$resultado[$contador] = $anticipos[$key];
						$resultado[$contador]->detalles = $detalleResumenes;
						$contador = $contador + 1;
					}
				}
			}	
             $cuerpo = array();
             array_push($cuerpo, array( 
                         'Fecha',
                         'Cliente',
                         'Moneda',
                         'Importe',
                         'Saldo',
                         'Moneda',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         ''
                 )
             );
             foreach($resultado as $key=>$cabecera){
                 array_push($cuerpo, array( 
                                     $cabecera->fecha,
                                     $cabecera->beneficiario_nombre,
                                     $cabecera->nro_recibo,
                                     number_format($cabecera->importe,2,",",""),
                                     number_format($cabecera->saldo,2,",",""),
                                     $cabecera->moneda,
                                     '',
                                     '',
                                     '',
                                     '',
                                     '',
                                     '',
                                     ''
                             )
                          );
                 array_push($cuerpo, array(
                                             'Fecha',
                                             'Concepto',
                                             'Fecha Pago',
                                             'Factura',
                                             'Moneda',
                                             'Importe',
                                             'Saldo',
                                             'Proforma',
                                             'Estado',
                                             'Vencimiento'
                                         ));
                 $importe = 0;
                 $total_comision = 0;
                 $importe_neto = 0;
                 $saldo_neto = 0;
                 $total_senha =0;
                 foreach($cabecera->detalles as $keys=>$detalles){
                     array_push($cuerpo, array( 
                                     date('d/m/Y H:m:s', strtotime($detalles->fecha_aplicacion)),
                                     $detalles->concepto,
                                     date('d/m/Y H:m:s', strtotime($detalles->fecha_pago)),
                                     $detalles->nro_factura,
                                     $detalles->moneda,
                                     number_format($detalles->monto,2,",",""),
                                     number_format($detalles->saldo,2,",",""),
                                     $detalles->id_proforma,
                                     $detalles->estado,
                                     $detalles->fecha_vencimiento
                                 )
                     );
                    /* $importe = $importe + (float)$detalles['importe'];
                     $total_comision =  $total_comision + (float)$detalles['total_comision'];
                     $total_senha =  $total_comision + (float)$detalles['senha'];
                     $importe_neto =  $importe_neto + (float)$detalles['importe_neto'];
                     $saldo_neto =  $saldo_neto + (float)$detalles['saldo_neto'];*/
 
                 }   
               /*  array_push($cuerpo, array( 
                             'TOTALES',
                             '',
                             '',
                             '',
                             '',
                             '',
                             '',
                             '',
                             number_format($importe,2,",",""),
                             number_format($total_senha,2,",",""),
                             number_format($total_comision,2,",",""),
                             number_format($importe_neto,2,",",""),
                             number_format($saldo_neto,2,",",""),
                             ''
                     )
                 );*/
                 array_push($cuerpo, array( 
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 ''
                         )
                     );
             }

             if($req->input("id_cliente") != ''){     
                 $cliente = Persona::where('id',$req->input("id_cliente"))->first();
                 $clienteNombre = $cliente->nombre." ".$cliente->apellido;
             }else{
                 $clienteNombre = 'Todos los Clientes';
             }    

             if($req->input('fecha_emision') != ""){
				$fechaPeriodo= explode('-', $req->input('fecha_emision'));
				$check_in = explode('/', $fechaPeriodo[0]);
				$check_out = explode('/', $fechaPeriodo[1]);
				$checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]).' 00:00:00';
				$checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]).' 23:59:59';
                $fecha = $checkIn." - ".$checkOut;
			}else{
                $fecha = 'Todas las Fechas';
            }

         $detalle = new \StdClass;
         $detalle->cliente = $clienteNombre;
         $detalle->fechas = $fecha;

            
         $export = new AnticiposClienteExport($cuerpo, $detalle);
        return Excel::download($export, 'Anticipos Cliente-' . date('d-m-Y') . '.xlsx');

     } 


     public function generarExcelAplicacionAnticiposProveedor(Request $req){
        $resultado =[];
		
        $query = "SELECT id,id_beneficiario,fecha, beneficiario_nombre, beneficiario_apellido, op_anticipo,importe,saldo, moneda";
				
        $query .=" FROM vw_anticipo_proveedores ";

		$query .="WHERE id_empresa = ".$this->getIdEmpresa();

        if($req->input('id_proveedor') != 0){
          $query .="AND id_beneficiario =".$req->input('id_proveedor');
        }

        if($req->input('fecha_emision') != ""){
          $fechaPeriodo= explode('-', $req->input('fecha_emision'));
          $check_in = explode('/', $fechaPeriodo[0]);
          $check_out = explode('/', $fechaPeriodo[1]);
          $checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]).' 00:00:00';
          $checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]).' 23:59:59';
          $query .="AND fecha BETWEEN '".$checkIn."' AND '".$checkOut."'";
        }
		
        $query .=" GROUP BY id, beneficiario_nombre, beneficiario_apellido, op_anticipo, importe,saldo, moneda,id_beneficiario,fecha";

        $anticipos = DB::select($query);

        if(!empty($anticipos)){
          $contador = 0;
          foreach($anticipos as $key=>$resumen){
            $detalleResumenes = DB::select("SELECT *
                            FROM vw_anticipo_proveedores
                            WHERE monto is not null
                            AND id = ".$resumen->id );	
            if(!empty($detalleResumenes)){
              $resultado[$contador] = $anticipos[$key];
              $resultado[$contador]->detalles = $detalleResumenes;
              $contador = $contador + 1;
            }
          }
        }	

        $cuerpo = array();
         array_push($cuerpo, array( 
                     'Fecha',
                     'Proveedor',
                     'Moneda',
                     'Importe',
                     'Saldo',
                     'Moneda',
                     '',
                     '',
                     '',
                     '',
                     '',
                     '',
                     ''
             )
         );
         foreach($resultado as $key=>$cabecera){
             array_push($cuerpo, array( 
                                 $cabecera->fecha,
                                 $cabecera->beneficiario_nombre,
                                 $cabecera->op_anticipo,
                                 number_format($cabecera->importe,2,",",""),
                                 number_format($cabecera->saldo,2,",",""),
                                 $cabecera->moneda,
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 ''
                         )
                      );
             array_push($cuerpo, array(
                                         'Fecha',
                                         'Concepto',
                                         'Fecha Pago',
                                         'Nro OP',
                                         'Moneda',
                                         'Importe',
                                         'Saldo',
                                         'Proforma',
                                         'Estado',
                                         'Vencimiento'
                                     ));
             $importe = 0;
             $total_comision = 0;
             $importe_neto = 0;
             $saldo_neto = 0;
             $total_senha =0;
             foreach($cabecera->detalles as $keys=>$detalles){
                 array_push($cuerpo, array( 
                                 date('d/m/Y H:m:s', strtotime($detalles->fecha_aplicacion)),
                                 $detalles->concepto,
                                 date('d/m/Y H:m:s', strtotime($detalles->fecha_pago)),
                                 $detalles->nro_op,
                                 $detalles->moneda,
                                 number_format($detalles->monto,2,",",""),
                                 number_format($detalles->saldo,2,",",""),
                                 $detalles->id_proforma,
                                 $detalles->estado,
                                 $detalles->fecha_vencimiento
                             )
                 );
                /* $importe = $importe + (float)$detalles['importe'];
                 $total_comision =  $total_comision + (float)$detalles['total_comision'];
                 $total_senha =  $total_comision + (float)$detalles['senha'];
                 $importe_neto =  $importe_neto + (float)$detalles['importe_neto'];
                 $saldo_neto =  $saldo_neto + (float)$detalles['saldo_neto'];*/

             }   
           /*  array_push($cuerpo, array( 
                         'TOTALES',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         '',
                         number_format($importe,2,",",""),
                         number_format($total_senha,2,",",""),
                         number_format($total_comision,2,",",""),
                         number_format($importe_neto,2,",",""),
                         number_format($saldo_neto,2,",",""),
                         ''
                 )
             );*/
             array_push($cuerpo, array( 
                             '',
                             '',
                             '',
                             '',
                             '',
                             '',
                             '',
                             '',
                             '',
                             '',
                             '',
                             ''
                     )
                 );
         }

         if($req->input("id_cliente") != ''){     
             $cliente = Persona::where('id',$req->input("id_cliente"))->first();
             $clienteNombre = $cliente->nombre." ".$cliente->apellido;
         }else{
             $clienteNombre = 'Todos los Clientes';
         }    

         if($req->input('fecha_emision') != ""){
            $fechaPeriodo= explode('-', $req->input('fecha_emision'));
            $check_in = explode('/', $fechaPeriodo[0]);
            $check_out = explode('/', $fechaPeriodo[1]);
            $checkIn = trim($check_in[2]).'-'.trim($check_in[1]).'-'.trim($check_in[0]).' 00:00:00';
            $checkOut = trim($check_out[2]).'-'.trim($check_out[1]).'-'.trim($check_out[0]).' 23:59:59';
            $fecha = $checkIn." - ".$checkOut;
        }else{
            $fecha = 'Todas las Fechas';
        }

     $detalle = new \StdClass;
     $detalle->cliente = $clienteNombre;
     $detalle->fechas = $fecha;

     $export = new AnticiposProveedorExport($cuerpo, $detalle);
    return Excel::download($export, 'Anticipos Proveedor-' . date('d-m-Y') . '.xlsx');

 } 


 public function generarExcelAsiento(Request $req){
    ini_set('memory_limit', '-1');
    set_time_limit(300);
    $asientos = DB::table('v_asientos_contables');
    $asientos = $asientos->where('id_empresa', $this->getIdEmpresa());

    if ($req->input('asiento_anulado') == 1)
    {
        $asientos = $asientos->where('activo', false);
    }
    else
    {
        $asientos = $asientos->where('activo', true);
    }

    if($req->input('id_usuario')){
        $asientos = $asientos->where('id_usuario',$req->input('id_usuario'));
   }
       //LAS FECHAS YA VIENEN FORMATEADAS DESDE LA VISTA
    if($req->input('periodo:p_date')){
        $fecha =  explode(' - ' , $req->input('periodo:p_date'));
        $desde  = $this->formatoFechaEntrada($fecha[0]).' 00:00:00';
        $hasta  = $this->formatoFechaEntrada($fecha[1]).' 23:59:59';
        $asientos = $asientos->whereBetween('fecha_hora', array($desde,$hasta));
   }

   if($req->input('id_asiento')){
        $asientos = $asientos->where('id_asiento', $req->input('id_asiento'));
   }

   if($req->input('tipo_operacion')){
        $asientos = $asientos->where('id_origen_asiento', $req->input('tipo_operacion'));
   }

   if($req->input('documento')){
    $asientos = $asientos->where('nro_documento', $req->input('documento'));
   }

   if($req->input('balanceado')){
    $asientos = $asientos->where('balanceado', $req->input('balanceado'));
   }

   $asientos = $asientos->get();

   $cuerpo = array();
    foreach($asientos as $asiento){
        if ($asiento->activo == true){
            $activo =  'NO';
        }else{
            $activo = 'SÍ';

        }
        array_push($cuerpo, array( 
                                $asiento->id_asiento,
                                $asiento->fecha_hora_format,
                                $asiento->concepto,
                                $asiento->nro_documento,
                                $asiento->abreviatura,
                                $asiento->debe,
                                $asiento->haber,
                                $asiento->usuario,
                                $asiento->balanceado,
                                $activo
                            )
                );
    }
    $export = new ReporteAsientoContableExport($cuerpo);
    return Excel::download($export, 'Reporte Asiento Contable-' . date('d-m-Y') . '.xlsx');


}

        public function generarExcelLiquidaciones(Request $request){
            if($request->input("vendedorDetalle") !== null){
                $idVendedor = $request->input("vendedorDetalle");
            }else{
                $idVendedor = $request->input("vendedorId");
            }
            $explode = explode("/", $request->input("periodoDetalle"));
            $mesLetra = $explode[0];
            $year = $explode[1];
            $mes = $this->getMes($mesLetra);
            
            $data = DB::table('vw_liquidacion_vendedores_detalle');
            $data = $data->where('id_vendedor',$idVendedor);
            if($request->input("id_liquidacion") != ""){
                $data = $data->where('id_liquidacion',$request->input("id_liquidacion"));
            }
            $data = $data->where('id_empresa', $this->getIdEmpresa());
            $data = $data->where('periodo', $mes.''.$year);
            $data = $data->orderBy('periodo','DESC');
            $data = $data->get();
            
            $pyg = 0;
            $usd = 0;
            $eur = 0;
            $cuerpo = array();
            foreach($data as $liquidacionesDetalle){
                /*echo '<pre>';
                print_r($liquidacionesDetalle);*/
                if($liquidacionesDetalle->id_moneda == 111){
                    $pyg = $pyg + round($liquidacionesDetalle->monto_comision)+round($liquidacionesDetalle->comision_renta_extra);
                }
                if($liquidacionesDetalle->id_moneda == 143){
                    $usd = $usd + $liquidacionesDetalle->monto_comision + $liquidacionesDetalle->comision_renta_extra;
                }
                if($liquidacionesDetalle->id_moneda == 43){
                    $eur = $eur + $liquidacionesDetalle->monto_comision + $liquidacionesDetalle->comision_renta_extra;
                }
                $nombre = $liquidacionesDetalle->nombre;
                if($liquidacionesDetalle->apellido != ""){
                    $nombre =$liquidacionesDetalle->nombre .' '.$liquidacionesDetalle->apellido;
                }

                $renta_comisionable = 0;

                if($liquidacionesDetalle->f_renta_comisionable != "")
                {
                    $renta_comisionable = $liquidacionesDetalle->f_renta_comisionable;
                }
                else
                {
                    if($liquidacionesDetalle->nc_renta_comisionable != "")
                    {
                        $renta_comisionable = $liquidacionesDetalle->nc_renta_comisionable;
                    }
                }
                $porcentaje_comision = 0;

                $porcentaje_comision = $liquidacionesDetalle->fact_por_liq;

                array_push($cuerpo, array( 
                                                $liquidacionesDetalle->documento,
                                                $liquidacionesDetalle->tipo_documento,
                                                date('d/m/Y', strtotime($liquidacionesDetalle->fecha_hora_facturacion)),
                                                $liquidacionesDetalle->moneda,
                                                $liquidacionesDetalle->id_proforma,
                                                $nombre,
                                                $renta_comisionable,
                                                $porcentaje_comision,
                                                $liquidacionesDetalle->monto_comision,
                                                $liquidacionesDetalle->renta_extra,
                                                $liquidacionesDetalle->comision_renta_extra
                                            )
                                );

            }			

            $totales = DB::select("SELECT sum(f_renta_comisionable) as rentafactura
                                    FROM vw_liquidacion_vendedores_detalle
                                    WHERE id_vendedor = ".$idVendedor."
                                    AND periodo = '".$mes."".$year."'");
            $totalRenta = (float)$totales[0]->rentafactura;
            
            $comisiones = DB::select("SELECT porcentaje_comision, comision_total_gs,comision_total_usd,comision_total_eur,renta_total
                                    FROM liquidacion_comisiones_vendedores
                                    WHERE id_vendedor = ".$idVendedor."
                                    AND periodo = '".$mes."".$year."'
                                    ORDER BY id DESC
                                    LIMIT 1 ");
            if(isset($comisiones[0])){
                $totalRenta = $comisiones[0]->renta_total;
                $porcentajeComision = $comisiones[0]->porcentaje_comision;
                $comisionGs = "".$pyg."";
                $comisionUsd = "".$usd."";
                $comisionEur = "".$eur."";
            }

            $export = new ReporteLiquidacionesExport($cuerpo);
            return Excel::download($export, 'Reporte de Liquidaciones-' . date('d-m-Y') . '.xls');
            
        } 

        public function getFecha($mes, $anho)      
        {
        $month = $mes; //Reemplazable por número del 1 a 12
        $year = $anho; //Reemplazable por un año valido

        switch(date('n',mktime(0, 0, 0, $month, 1, $year)))
        {
            case 1: $Mes = "Enero"; break;
            case 2: $Mes = "Febrero"; break;
            case 3: $Mes = "Marzo"; break;
            case 4: $Mes = "Abril"; break;
            case 5: $Mes = "Mayo"; break;
            case 6: $Mes = "Junio"; break;
            case 7: $Mes = "Julio"; break;
            case 8: $Mes = "Agosto"; break;
            case 9: $Mes = "Septiembre"; break;
            case 10: $Mes = "Octubre"; break;
            case 11: $Mes = "Noviembre"; break;
            case 12: $Mes = "Diciembre"; break;
        }

        return $Mes.'/'.date('Y',mktime(0, 0, 0, $month, 1, $year));
        }

        public function getMes($mes)      
        {		
            switch($mes)
            {
            case "Enero": $Mes = '01'; break;
            case "Febrero": $Mes = '02'; break;
            case "Marzo": $Mes = '03'; break;
            case "Abril": $Mes = '04'; break;
            case "Mayo": $Mes = '05'; break;
            case "Junio": $Mes = '06'; break;
            case "Julio": $Mes = '07'; break;
            case "Agosto": $Mes = '08'; break;
            case "Septiembre": $Mes = '09'; break;
            case "Octubre": $Mes = '10'; break;
            case "Noviembre": $Mes = '11'; break;
            case "Diciembre": $Mes = '12'; break;
            }

            return $Mes;
        }

// Excel de proforma facturas estados
public function generarExcelReporteProformasEstado(Request $req){

    //Capturamos las fechas enviadas desde el filtro de criterios
    $periodo = $req->periodo;
    $proformaId= $req->numProforma;
    $vendedor= $req->vendedores;
    $estado= $req->id_estado;
    $cambioEstado= $req->vendedores_modificacion;

    if($periodo != ""){
            $fechaPeriodo = explode(' - ', $periodo);
            $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
            $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);
        } else {
            $desde     = "";
            $hasta     = "";
        } 

        $proformasTotales=Proforma::where('proformas.id_empresa',Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa)
        ->leftJoin('personas', 'personas.id', '=', 'proformas.id_usuario')
        ->leftJoin('vw_time_line', 'vw_time_line.id_proforma', '=', 'proformas.id');
       //Calculamos el total de proformas para el paginado sin el offset si envio un numero de proforma
        if($proformaId != ''){

           $proformasTotales = $proformasTotales->where('proformas.id', '=', $proformaId);
           
        }
       //Calculamos el total de proformas para el paginado sin el offset si envio un rango de fechas
        if($desde != '' && $hasta != ''){
           $proformasTotales = $proformasTotales->whereBetween('proformas.fecha_alta', array(
           date('Y-m-d H:i:s',strtotime($desde.' 00:00:00')),
           date('Y-m-d H:i:s',strtotime($hasta.' 23:59:59'))
               ));
        }
       //Verificamos si envio algun estado para filtrar
       if($estado != ''){
           $proformasTotales = $proformasTotales->where('vw_time_line.denominacion', '=', $estado);
           
        }
      //Verificamos si envio algun vendedor para filtrar
       if($vendedor != ''){
           $proformasTotales = $proformasTotales->where('proformas.id_usuario', '=', $vendedor);
       }
       //Verificamos si envio datos en el filtro de cambio de estado de proforma.
       if($cambioEstado != ''){

       $proformasTotales = $proformasTotales->where('vw_time_line.id_editor', '=', $cambioEstado);
       
       }

       $proformasTotales= $proformasTotales->get();

       $cuerpo = array();
       foreach($proformasTotales as $key=>$value)
       {
            $fecha = $value->fecha;
            $fechaObjeto = DateTime::createFromFormat('d/m/Y', $fecha);
            $fechaFormateada = $fechaObjeto->format('Y-m-d');
       
            $fecha1 = new DateTime($value->creacion_proforma);
            $fecha2 = new DateTime($fechaFormateada);
            $diferencia = $fecha1->diff($fecha2);
           array_push($cuerpo, array( $value->id_proforma,
                                      $value->nombre.' '. $value->apellido,
                                      $value->creacion_proforma,
                                      $fechaFormateada,
                                      $value->usuario_n,
                                      $value->denominacion,
                                      $diferencia->format('%a días')
                                     ));
       }


       $export = new ReporteHistoricoProformasExport($cuerpo);
       return Excel::download($export, 'Reporte historico proformas-' . date('d-m-Y') . '.xls');
       


}

public function generarExcelVentaPrestador(Request $req)
{ 
    $id_empresa = $this->getIdEmpresa();
    if($req->input('periodo_out') != ""){
        $fechaPeriodo = explode(' - ', $req->input('periodo_out'));
        $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
        $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
    } 
    ini_set('memory_limit', '1G');
    set_time_limit(240);
   
    //$desde = $req->input('desde');
    //$hasta = $req->input('hasta');
   // $buscar = $req->input('buscar');
    $vendedorId = $req->input('vendedor_id');
    $nroFactura = $req->input('nro_factura'); // Nuevo: Obtener el valor del parámetro 'nro_factura'
	$idProforma = $req->input('id_proforma'); // Nuevo: Obtener el valor del parámetro 'id_proforma'
	$moneda = $req->input('id_moneda');
	$producto = $req->input('producto_0');
	$inputPrestador = $req->input('id_prestador');
	$inputvendedorEmpresa = $req->input('vendedor_empresa');

    $id_proveedor = $req->input('id_proveedor');
    $query = DB::table('facturas AS f')
        ->select(
            'f.id',
            'mv.currency_code AS moneda_venta',
            'mc.currency_code AS moneda_costo',
			'provee.nombre AS proveedor',
			'ft.id_prestador',
			'f.total_bruto_factura',
			'ft.renta',
			'ft.fecha_in',
			'f.fecha_hora_facturacion',
			'f.nro_factura',
			'f.id_proforma',
			'v.nombre AS VendedorAgencia',
			'f.cliente_id',
			'pc.nombre',
			'pc.telefono',
			'pc.email',
			DB::raw("CONCAT(vde.nombre, ' ', vde.apellido) AS VendedorEmpresa"),
			DB::raw("COALESCE(ft.costo_proveedor, 0) + COALESCE(ft.costo_gravada, 0) as precio_costo"),
			'ft.precio_venta',
			'ft.cod_confirmacion',
			'ft.fecha_de_gasto',
			'd.desc_destino',
			'ft.descripcion',
			'pt.denominacion_comercial',
            DB::raw("CONCAT(pt.nombre, ' ', pt.apellido) AS prestador"),
            'producto.denominacion as producto'
        )
        ->leftJoin('personas AS c', 'c.id', '=', 'f.cliente_id')
		->leftJoin('empresas AS em', 'em.id', '=', 'f.id_empresa')
		->leftJoin('personas AS v', 'v.id', '=', 'f.vendedor_id')
		->leftJoin('personas AS vde', 'f.id_usuario', '=', 'vde.id')
		//->leftJoin('personas AS vde', 'f.id_usuario', '=', 'vde.id_vendedor_empresa')
		->leftJoin('personas AS pc', 'pc.id', '=', 'f.cliente_id')
		->leftJoin('facturas_detalle AS ft', 'ft.id_factura', '=', 'f.id')
		->leftJoin('personas AS pt', 'pt.id', '=', 'ft.id_prestador')
		->leftJoin('currency AS mc', 'mc.currency_id', '=', 'ft.currency_costo_id')
		->leftJoin('currency AS mv', 'mv.currency_id', '=', 'ft.currency_venta_id')
		->leftJoin('personas AS u', 'u.id', '=', 'f.id_usuario')
		->leftJoin('destinos_dtpmundo AS d', 'd.id_destino_dtpmundo', '=', 'f.destino_id')
		->leftJoin( 'personas AS provee', 'provee.id','=', 'ft.id_proveedor')
        ->leftJoin( 'productos AS producto', 'producto.id','=', 'ft.id_producto')
		//->leftJoin('proformas AS proforma', 'proforma.id', '=', 'f.id_proforma')
		//->leftJoin('nemo_reservas AS pre', 'pre.id_proforma', '=', 'proforma.id' )
		//->distinct('')
		->where('f.id_empresa', $id_empresa)
        ->where('f.id_estado_factura', '<>', 30)
        ->groupBy(
            'f.id',
            'mv.currency_code',
            'mc.currency_code',
            'provee.nombre',
            'ft.id_prestador',
            'f.total_bruto_factura',
            'ft.renta',
            'ft.fecha_in',
            'f.fecha_hora_facturacion',
            'f.nro_factura',
            'f.id_proforma',
            'v.nombre',
            'f.cliente_id',
            'pc.nombre',
            'pc.email',
            'pc.telefono',
            'vde.nombre',
            'vde.apellido',
            'ft.costo_proveedor',
            'ft.costo_gravada',
            'ft.precio_venta',
            'ft.cod_confirmacion',
            'ft.fecha_de_gasto',
            'd.desc_destino',
            'ft.descripcion',
            'pt.denominacion_comercial',
            'pt.nombre',
            'pt.apellido',
            'producto.denominacion'
        );
	

/*

    if ($desde && $hasta) {
        $query=$query->whereBetween('f.fecha_hora_facturacion', [$desde. ' 00:00:00', $hasta.' 23:59:59']);
    }*/
    if (isset($desde)  && isset($hasta)) {
        $query->whereBetween('f.fecha_hora_facturacion', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);
    }
   /* if ($request->input('search.value')) {
        $searchValue = $request->input('search.value');
        $query->where(function ($query) use ($searchValue) {
            $query->where('f.nro_factura', 'LIKE', "%$searchValue%")
            ->orWhere(DB::raw("CONCAT(pt.nombre, ' ', pt.apellido)"), 'LIKE', '%' . $searchValue . '%')
            ->orWhere('f.nro_factura', 'LIKE', '%' . $searchValue . '%')
            ->orWhere('pc.nombre', 'LIKE', '%' . $searchValue . '%')
            ->orWhere('pc.telefono', 'LIKE', '%' . $searchValue . '%')
            ->orWhere('pc.email', 'LIKE', '%' . $searchValue . '%')
            ->orWhere('ft.descripcion', 'LIKE', '%' . $searchValue . '%')
            ->orWhere('producto.denominacion', 'LIKE', '%' . $searchValue . '%')
            ->orWhere('ft.cod_confirmacion', 'LIKE', '%' . $searchValue . '%')
            ->orWhere(DB::raw("CONCAT(vde.nombre, ' ', vde.apellido)"), 'LIKE', '%' . $searchValue . '%')
            ->orWhere('d.desc_destino', 'LIKE', '%' . $searchValue . '%')
            ->orWhere(DB::raw("CAST(ft.precio_costo AS TEXT)"), 'LIKE', '%' . $searchValue . '%')
            ->orWhere(DB::raw("CAST(ft.precio_venta AS TEXT)"), 'LIKE', '%' . $searchValue . '%')
            ->orWhere(DB::raw("CAST(ft.renta AS TEXT)"), 'LIKE', '%' . $searchValue . '%')
            ->orWhere(DB::raw("CAST(f.id_proforma AS TEXT)"), 'LIKE', '%' . $searchValue . '%');;
        });
    }*/
    /*if ($buscar) {
        $query->where(function ($query) use ($buscar) {
            $query->where('provee.nombre', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.id_prestador', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.total_bruto_factura', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.renta', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.fecha_in', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.fecha_hora_facturacion', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.nro_factura', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.id_proforma', 'LIKE', '%' . $buscar . '%')
            ->orWhere('v.nombre', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.cliente_id', 'LIKE', '%' . $buscar . '%')
            ->orWhere('pc.nombre', 'LIKE', '%' . $buscar . '%')
            ->orWhere('pc.telefono', 'LIKE', '%' . $buscar . '%')
            ->orWhere('pc.email', 'LIKE', '%' . $buscar . '%')
            ->orWhere(DB::raw("CONCAT(vde.nombre, ' ', vde.apellido)"), 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.precio_costo', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.precio_venta', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.cod_confirmacion', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.fecha_de_gasto', 'LIKE', '%' . $buscar . '%')
            ->orWhere('d.desc_destino', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.descripcion', 'LIKE', '%' . $buscar . '%')
            ->orWhere('pt.denominacion_comercial', 'LIKE', '%' . $buscar . '%')
            ->orWhere(DB::raw("CONCAT(pt.nombre, ' ', pt.apellido)"), 'LIKE', '%' . $buscar . '%')
            ->orWhere('producto.denominacion', 'LIKE', '%' . $buscar . '%');
        });
    }*/
    if ($vendedorId) {
        $query=$query->where('v.id', $vendedorId); // Filtrar por el ID del vendedor
    }

    if ($nroFactura) {
        $query=$query->where('f.nro_factura', $nroFactura); // Filtrar por el número de factura
    }

    if ($idProforma) {
        $query=$query->where('f.id_proforma', $idProforma); // Filtrar por el ID de proforma
    }

    if ($moneda) {
        $query=$query->where('f.id_moneda_venta', $moneda);
    }
    if ($producto) {
        $query=$query->where('ft.id_producto', $producto);
    }
    if ($inputPrestador) {
        $query=$query->where('ft.id_prestador', $inputPrestador);
    }
    
    if ($inputvendedorEmpresa) {
        $query=$query->where('vde.id', $inputvendedorEmpresa);
    }
    if ($id_proveedor) {
        $query->where('ft.id_proveedor', $id_proveedor);
    }
//dd($req->all());
    $facturas = $query->orderBy('f.id', 'desc')->get();
   
    $cuerpo = array();

    foreach ($facturas as $factura) {
        array_push($cuerpo, array(
           /* $factura->proveedor,
            $factura->prestador,
            $factura->precio_costo,
            $factura->precio_venta,
            $factura->renta,
            $factura->cod_confirmacion,
            $factura->fecha_de_gasto,
            $factura->fecha_in,
            $factura->fecha_hora_facturacion,
            $factura->nro_factura,
            $factura->id_proforma,
            $factura->vendedorempresa,
            $factura->nombre,
            $factura->VendedorAgencia,
            $factura->desc_destino,
            $factura->moneda_venta,
            $factura->descripcion,*/

            //
            $factura->proveedor,
            $factura->prestador,
            $factura->fecha_hora_facturacion,
            $factura->nro_factura,
            $factura->nombre,
            $factura->telefono,
            $factura->email,
            $factura->id_proforma,
            $factura->descripcion,
            $factura->producto,
            $factura->cod_confirmacion,
            $factura->vendedorempresa,
            $factura->VendedorAgencia,
            $factura->fecha_in,
            $factura->desc_destino,
            $factura->moneda_venta,
            $factura->precio_venta,
            $factura->moneda_costo,
            $factura->precio_costo,
            $factura->renta,
            $factura->fecha_de_gasto,



            
        ));
       
    }

    $export = new ReporteVentaPrestadorExport($cuerpo);
    return Excel::download($export, 'Reporte Venta Prestador-' . date('d-m-Y') . '.xls');
    
    
}


//ATK VENTAS RAPIDAS 
public function generarExcelventaRapida(Request $req)
{
    $id_empresa = $this->getIdEmpresa();
    $desde = $req->input('desde');
    $hasta = $req->input('hasta');
    $buscar = $req->input('buscar');
    $nroFactura = $req->input('nro_factura'); 
    $vendedorId = $req->input('vendedor_id');
    $query = DB::table('facturas AS f')
        ->select(
            'f.id',
            'm.currency_code AS moneda_venta',
            'provee.nombre AS proveedor',
            'ft.renta',
            'ft.fecha_in',
            'f.fecha_hora_facturacion',
            'f.nro_factura',
            'f.id_proforma',
            'v.nombre AS VendedorAgencia',
            'f.cliente_id',
            'pc.nombre',
            DB::raw("CONCAT(vde.nombre, ' ', vde.apellido) AS VendedorEmpresa"),
            'f.id_venta_rapida',
            'ft.descripcion',
            'ft.precio_costo',
            'ft.precio_venta',
            'ft.cantidad',
            'estado.denominacion as estado',
			'producto.denominacion as producto'
        )
        ->leftJoin('personas AS c', 'c.id', '=', 'f.cliente_id')
        ->leftJoin('empresas AS em', 'em.id', '=', 'f.id_empresa')
        ->leftJoin('personas AS v', 'v.id', '=', 'f.vendedor_id')
        ->leftJoin('personas AS vde', 'f.id_usuario', '=', 'vde.id')
        //->leftJoin('personas AS vde', 'f.id_usuario', '=', 'vde.id_vendedor_empresa')
        ->leftJoin('personas AS pc', 'pc.id', '=', 'f.cliente_id')
        ->leftJoin('facturas_detalle AS ft', 'ft.id_factura', '=', 'f.id')
        ->leftJoin('currency AS m', 'm.currency_id', '=', 'f.id_moneda_venta')
        ->leftJoin('personas AS u', 'u.id', '=', 'f.id_usuario')
        ->leftJoin('destinos_dtpmundo AS d', 'd.id_destino_dtpmundo', '=', 'f.destino_id')
        ->leftJoin( 'personas AS provee', 'provee.id','=', 'ft.id_proveedor')
        ->leftJoin( 'estados AS estado', 'estado.id','=', 'f.id_estado_factura')
        ->leftJoin( 'productos AS producto', 'producto.id','=', 'ft.id_producto')
        ->whereNotNull('f.id_venta_rapida') // Agregar esta cláusula;
        ->where('f.id_empresa',$id_empresa)
        ->where('f.id_estado_factura', '<>', 30);
        


    if ($desde && $hasta) {
        $query=  $query->whereBetween('f.fecha_hora_facturacion', [$desde. ' 00:00:00', $hasta.' 23:59:59']);
    }

    if ($buscar) {
        $query->where(function ($query) use ($buscar) {
            $query->where('provee.nombre', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.id_prestador', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.total_bruto_factura', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.renta', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.fecha_in', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.fecha_hora_facturacion', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.nro_factura', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.id_proforma', 'LIKE', '%' . $buscar . '%')
            ->orWhere('v.nombre', 'LIKE', '%' . $buscar . '%')
            ->orWhere('f.cliente_id', 'LIKE', '%' . $buscar . '%')
            ->orWhere('pc.nombre', 'LIKE', '%' . $buscar . '%')
            ->orWhere(DB::raw("CONCAT(vde.nombre, ' ', vde.apellido)"), 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.precio_costo', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.precio_venta', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.cod_confirmacion', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.fecha_de_gasto', 'LIKE', '%' . $buscar . '%')
            ->orWhere('d.desc_destino', 'LIKE', '%' . $buscar . '%')
            ->orWhere('ft.descripcion', 'LIKE', '%' . $buscar . '%')
            ->orWhere(DB::raw("CONCAT(pt.nombre, ' ', pt.apellido)"), 'LIKE', '%' . $buscar . '%')
            ->orWhere('estado.denominacion', 'like', '%' . $buscar . '%')
            ->orWhere('producto.denominacion', 'LIKE', '%' . $buscar . '%');
        });
    }
    if ($vendedorId) {
        $query=  $query->where('v.id', $vendedorId); // Filtrar por el ID del vendedor
    }

    $facturas = $query->orderBy('f.id', 'desc')->get();

    $cuerpo = array();

    foreach ($facturas as $factura) {
        array_push($cuerpo, array(
            $factura->nro_factura,
            $factura->vendedorempresa,
            $factura->id_venta_rapida,
            $factura->proveedor,
            $factura->descripcion,
            $factura->cantidad,
            $factura->precio_costo,
            $factura->precio_venta,
            $factura->renta,
            $factura->nombre,
            $factura->fecha_hora_facturacion,
            $factura->estado,
        
      
        ));
    }

    $export = new ReporteVentaRapidaExport($cuerpo);
    return Excel::download($export, 'Reporte Venta Rapida-' . date('d-m-Y') . '.xls');
    
}

public function generarExcelMetaAgencia(Request $req)
{ 
    $id_empresa = $this->getIdEmpresa();
   
    ini_set('memory_limit', '1G');
    set_time_limit(240);
   
   $persona = strtoupper($req->input('nombre'));
   $denominacion = strtoupper($req->input('denominacion_comercial'));
   $ruc = $req->input('ruc');
   $email = $req->input('correo');
   $buscar = $req->input('buscar');
   $vendedorId = $req->input('id_vendedor_agencia');
    $id_proveedor = $req->input('id_proveedor');
    $meses = [
        1 => 'Enero',
        2 => 'Febrero',
        3 => 'Marzo',
        4 => 'Abril',
        5 => 'Mayo',
        6 => 'Junio',
        7 => 'Julio',
        8 => 'Agosto',
        9 => 'Septiembre',
        10 => 'Octubre',
        11 => 'Noviembre',
        12 => 'Diciembre',
    ];
    $query = DB::table('personas as p')
    ->leftJoin('facturas as f', 'p.id', '=', 'f.cliente_id')
    ->leftJoin('metas_agencias as ma', function ($join) {
        $join->on('p.id', '=', 'ma.id_vendedor_agencia')
            ->where(DB::raw('EXTRACT(MONTH FROM f.fecha_hora_facturacion)'), '=', DB::raw('ma.mes'))
            ->where(DB::raw('EXTRACT(YEAR FROM f.fecha_hora_facturacion)'), '=', DB::raw('CAST(ma.anho AS INT)'));
    })        
    ->leftJoin('estados as estado', 'f.id_estado_factura', '=', 'estado.id')
    ->select([
        DB::raw('COALESCE(ma.id_meta, 0) AS id_meta_ma'),
        DB::raw('COALESCE(ma.id_vendedor_agencia, 0) AS id_vendedor_agencia'),
        DB::raw('COALESCE(ma.mes, ma.mes) AS mes'),
        DB::raw("CASE 
            WHEN EXTRACT(MONTH FROM f.fecha_hora_facturacion) IS NULL OR EXTRACT(MONTH FROM f.fecha_hora_facturacion) = 0 THEN '' 
            ELSE CAST(EXTRACT(MONTH FROM f.fecha_hora_facturacion) AS TEXT)
        END AS mes_fecha_facturacion"),
        DB::raw('COALESCE(ma.meta_mensual, 0) AS meta_mensual'),
        //DB::raw('COALESCE(SUM(f.total_bruto_factura), 0) AS renta_bruta_factura'),
        DB::raw("CONCAT(p.nombre, ' ', p.apellido) AS nombre_completo"),
        'p.nombre',
        DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
            f.cotizacion_factura,
            COALESCE(f.total_bruto_factura, 0),
            f.id_moneda_venta,
            143
        )), 0) AS total_factura_cotizado'),
        DB::raw('COALESCE(SUM(get_monto_cotizacion_custom(
            f.cotizacion_factura,
            COALESCE(f.renta, 0),
            f.id_moneda_venta,
            143
        )), 0) AS renta_cotizada'),
        DB::raw('CAST(ma.anho AS INT) as periodo_meta'),
        DB::raw('(COALESCE(SUM(get_monto_cotizacion_custom(
            f.cotizacion_factura,
            COALESCE(f.total_bruto_factura, 0),
            f.id_moneda_venta,
            143
        )), 0) - COALESCE(ma.meta_mensual, 0)) AS diferencia')
    ])
    ->where('p.id_empresa', $id_empresa)
    ->where('estado.id', '!=', 30);
    //->where('ma.mes', '=', 1);
            
	
          /* if ($req->input('mes') != "") {
                $fechaPeriodo = explode(' ', $req->input('mes'));
                $mes = $fechaPeriodo[0];
                $anio = $fechaPeriodo[1];
                $numeroMes=$this->getMes($mes);
            
                // Convierte el nombre del mes a su número correspondiente
               // $numeroMes = date('m', strtotime("$mes"));
            
                // Crea objetos Carbon con el mes, el año y el primer día del mes
                //$desdeFecha = Carbon::create($anio, $numeroMes, 1, 0, 0, 0);
                //$hastaFecha = $desdeFecha->copy()->endOfMonth(); // Para obtener el último día del mes
            
                //$query->whereBetween('f.fecha_hora_facturacion', [$desdeFecha, $hastaFecha])
                //$query->whereRaw("EXTRACT(YEAR FROM f.fecha_hora_facturacion) = ?", [$anio])
                $query->whereRaw("EXTRACT(YEAR FROM f.fecha_hora_facturacion) = '$anio'")
                ->where('ma.anho', $anio);
            }*/
            if ($req->input('anio') != "") {
                $anio = $req->input('anio');
                
                $query->whereRaw("EXTRACT(YEAR FROM f.fecha_hora_facturacion) = '$anio'")
                    ->where('ma.anho', $anio);
            }
                if ($req->input('search.value')) {
                    $searchValue = $req->input('search.value');
                    $query->where(function ($query) use ($searchValue) {
                        $query->where(DB::raw("CONCAT(p.nombre, ' ', p.apellido)"), 'LIKE', '%' . $searchValue . '%');
                    });
                }
            
                if ($vendedorId) {
                    $query->where('f.cliente_id', $vendedorId); // Filtrar por el ID del vendedor
                }

    $metas = $query
    
    ->groupBy('ma.id_meta', 'ma.id_vendedor_agencia', 'ma.meta_mensual', 'p.nombre', 'p.apellido', 'ma.anho', DB::raw('EXTRACT(MONTH FROM f.fecha_hora_facturacion)'),'ma.mes')
    ->orderBy('ma.anho', 'asc')
    //->orderBy('mes', 'asc')
    ->get();

   
    $cuerpo = array();

    foreach ($metas as $meta) {
        array_push($cuerpo, array(

            $meta->id_vendedor_agencia,
            $meta->nombre,
            $meses[$meta->mes],
            $meta->meta_mensual,
            $meta->total_factura_cotizado,
           // $meta->renta_cotizada,
            $meta->periodo_meta
            //$meta->diferencia 
        ));
       
    }

    $export = new ReporteMetaAgenciasExport($cuerpo);
    return Excel::download($export, 'Reporte Meta Agencias-' . date('d-m-Y') . '.xls');
    
    
}

public function generarExcelMetasAgencias(Request $req)
{ 
    $id_empresa = $this->getIdEmpresa();
   
    ini_set('memory_limit', '1G');
    set_time_limit(240);
   
   $persona = strtoupper($req->input('nombre'));
   $denominacion = strtoupper($req->input('denominacion_comercial'));
   $ruc = $req->input('ruc');
   $email = $req->input('correo');
   $buscar = $req->input('buscar');

    $id_proveedor = $req->input('id_proveedor');

    //$query = DB::select("SELECT id, nombre from personas where id_tipo_persona = 8 and activo = true and id_empresa = ".$this->getIdEmpresa()." order by nombre asc");
    $query = DB::table('personas')
    ->select('id', 'nombre')
    ->where('id_tipo_persona', 8)
    ->where('activo', true)
    ->where('id_empresa', $this->getIdEmpresa())
    ->orderBy('nombre', 'asc')
    ->get();
//dd($req->all());
    //$metas = $query->orderBy('nombre', 'asc')->get();
   
    $cuerpo = array();

    foreach ($query as $meta) {
        array_push($cuerpo, array(

            $meta->id,
            $meta->nombre,
            0,
            2023,            
        ));
       
    }

    $export = new ReporteListadoAgenciasExport($cuerpo);
    return Excel::download($export, 'Reporte Listado Agencias-' . date('d-m-Y') . '.xls');
    
    
}

public function generarExcelresumencobro(Request $request)
{ 
    $id_empresa = $this->getIdEmpresa();
    if($request->input('periodo_out') != ""){
        $fechaPeriodo = explode(' - ', $request->input('periodo_out'));
        $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
        $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
    } 
    $unidad_negocio = $request->input('unidad_negocio');
    ini_set('memory_limit', '1G');
    set_time_limit(240);
   
  
    $query = DB::table('recibos as rcc')
    ->join('recibos_detalle as rcd', 'rcd.id_cabecera', '=', 'rcc.id')
    ->join('libros_ventas as lv', 'lv.id', '=', 'rcd.id_libro_venta')
    ->join('proformas as pf', 'pf.id', '=', 'lv.id_proforma')
    ->join('unidad_negocios as uni', 'uni.id', '=', 'pf.id_unidad_negocio')
    ->join('forma_cobro_recibo_cabecera as fcc', 'fcc.id_recibo', '=', 'rcc.id')
    ->join('forma_cobro_recibo_detalle as fcd', 'fcd.id_cabecera', '=', 'fcc.id')
    ->join('currency as cu', 'cu.currency_id', '=', 'fcd.id_moneda')
    ->join('banco_detalle as banco_d', 'banco_d.id', '=', 'fcd.id_banco_detalle')
    ->join('banco_cabecera as banco_c', 'banco_c.id', '=', 'banco_d.id_banco')
    ->join('tipo_cuenta_banco as tc_banco', 'tc_banco.id', '=', 'banco_d.id_tipo_cuenta')
    ->join('forma_cobro_cliente as f1', 'f1.id', '=', 'fcd.id_tipo_pago')
    ->groupBy('pf.id', 'uni.descripcion', 'uni.id','banco_c.nombre', 'tc_banco.denominacion', 'banco_d.numero_cuenta', 'rcc.id','f1.denominacion')
    
    ->selectRaw('
    pf.id,
    uni.descripcion as unidad_negocio,
    uni.id,
    f1.denominacion,
    CONCAT(banco_c.nombre, \' \' , tc_banco.denominacion, \' \' , banco_d.numero_cuenta) AS banco_cuenta,
    CONCAT(banco_c.nombre, \' \', tc_banco.denominacion) AS banco_cuenta_n,
    SUM(CASE WHEN cu.currency_id = 143 THEN fcd.importe_pago ELSE 0 END) AS total_importe_pago_usd,
    SUM(CASE WHEN cu.currency_id = 43 THEN fcd.importe_pago ELSE 0 END) AS total_importe_pago_eur,
    SUM(CASE WHEN cu.currency_id = 111 THEN fcd.importe_pago ELSE 0 END) AS total_importe_pago_pyg'
    )
    //->distinct('')
    ->where('pf.id_empresa', $id_empresa)
    ->where('rcc.id_estado', '<>', 55)
    ->where('rcc.id_estado', '<>', 31)
    ->where('fcc.activo', '<>', false);

        if (isset($desde)  && isset($hasta)) {
            $query->whereBetween('fcc.fecha_hora', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);
        }
    

        
    if ($unidad_negocio) {
        $query->where('uni.id', $unidad_negocio); // Filtrar por Unidad de negocio
    }

    $resumenes = $query->orderBy('pf.id', 'desc')->get();   
    $cuerpo = array();

    foreach ($resumenes as $resumen) {
        array_push($cuerpo, array(

            $resumen->denominacion,
            $resumen->banco_cuenta,
            $resumen->total_importe_pago_usd,
            $resumen->total_importe_pago_pyg,
            $resumen->unidad_negocio,
        ));
       
    }

    $export = new ReporteListadoResumenCobrosExport($cuerpo);
    return Excel::download($export, 'Reporte Listado Resumen Cobros-' . date('d-m-Y') . '.xls');
    
    
}
//REPORTE PLAN DE CUENTAS SUMAS DE SALDOS 
public function generarExcelSaldoPlan(Request $request)
{ 
    $id_empresa = $this->getIdEmpresa();
    if($request->input('periodo_out') != ""){
        $fechaPeriodo = explode(' - ', $request->input('periodo_out'));
        $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
        $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
    } 

    $query = DB::table('asientos_contables_detalle as acd')
    ->Join('asientos_contables as ac', 'acd.id_asiento_contable', '=', 'ac.id')
    ->Join('personas as p', 'p.id', '=', 'ac.id_usuario')
    ->Join('origen_asiento_contable as o', 'o.id', '=', 'ac.id_origen_asiento')
    ->leftJoin('plan_cuentas as pc', 'pc.id', '=', 'acd.id_cuenta_contable')
    ->leftJoin('tipo_cuenta_corriente as tcc', 'pc.id_tipo_cuenta_corriente', '=', 'tcc.id')
    ->groupBy('acd.id_cuenta_contable', 'pc.descripcion', 'pc.cod_txt', 'tcc.denominacion')
    

    ->selectRaw('
    pc.cod_txt, 
    pc.descripcion, 
    tcc.denominacion, 
    count(*) as count, 
    sum(acd.debe) as debe, 
    sum(acd.haber) as haber,
    CASE
        WHEN (sum(acd.debe) - sum(acd.haber)) > 0.0 THEN sum(acd.debe) - sum(acd.haber)
        ELSE 0.0
    END AS saldo_debe,
    CASE
        WHEN (sum(acd.debe) - sum(acd.haber)) <= 0.0 THEN (sum(acd.debe) - sum(acd.haber)) * -1.0
        ELSE 0.0
    END AS saldo_haber
')
    ->where('ac.activo', true)
    ->where('acd.activo', true)
    ->where('ac.id_empresa', $id_empresa);
    //->orderBy('pc.cod_txt', 'asc');
        
 
        ini_set('memory_limit', '1G');
        set_time_limit(240);
   
        if (isset($desde)  && isset($hasta)) {
            $query->whereBetween('ac.fecha_hora', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);
        }	

    $resumenes = $query->orderBy('pc.cod_txt')->get();   
    $cuerpo = array();
    $suma_debe = 0;
    $suma_haber = 0;
    $suma_saldo_debe = 0;
    $suma_saldo_haber = 0;
    $ultimo_primer_digito = null;
    
    $cabeceras = array(
        'NUMERO DE CUENTA',
        'CUENTA',
        'DENOMINACION',
        'TOTAL DEBE',
        'TOTAL HABER',
        'SALDO DEBE',
        'SALDO HABER'
    );
    foreach ($resumenes as $resumen) {
        $primer_digito = substr($resumen->cod_txt, 0, 1);
        if ($ultimo_primer_digito !== null && $ultimo_primer_digito != $primer_digito) {
            if ($suma_debe != 0 || $suma_haber != 0 || $suma_saldo_debe != 0 || $suma_saldo_haber != 0) {
                array_push($cuerpo, array(
                    'TOTAL: ' . $ultimo_primer_digito,
                    $resumen->denominacion,
                    '',
                    $suma_debe,
                    $suma_haber,
                    $suma_saldo_debe,
                    $suma_saldo_haber,
                ));
                $suma_debe = 0;
                $suma_haber = 0;
                $suma_saldo_debe = 0;
                $suma_saldo_haber = 0;
            }
            array_push($cuerpo, array('','','','','','','')); // Agregar una fila vacía
            array_push($cuerpo, array('','','','','','','')); // Agregar una fila vacía
            array_push($cuerpo, $cabeceras); // Agregar la cabecera
        }
    
        array_push($cuerpo, array(
            $resumen->cod_txt,
            $resumen->descripcion,
            $resumen->denominacion,
            $resumen->debe,
            $resumen->haber,
            $resumen->saldo_debe,
            $resumen->saldo_haber,
        ));
    
        $suma_debe += $resumen->debe;
        $suma_haber += $resumen->haber;
        $suma_saldo_debe += $resumen->saldo_debe;
        $suma_saldo_haber += $resumen->saldo_haber;
        $ultimo_primer_digito = $primer_digito;
    }
    
 /*   if ($suma_debe != 0 || $suma_haber != 0 || $suma_saldo_debe != 0 || $suma_saldo_haber != 0) {
        array_push($cuerpo, array(
            '',
            'Suma para ' . $ultimo_primer_digito,
            '',
            $suma_debe,
            $suma_haber,
            $suma_saldo_debe,
            $suma_saldo_haber,
        ));
    }
*/
$export = new ReporteListadoPlanCuentasExport($cuerpo);
return Excel::download($export, 'Reporte Listado Plan de Cuentas-' . date('d-m-Y') . '.xlsx');

    
}

public function generarExcelventaNc(Request $request)
{ 
    //$id_estado_factura = $request->input('id_estado_factura');
	//$anulado = $request->input('anulado'); 
    $id_empresa = $this->getIdEmpresa();
    if($request->input('periodo_out') != ""){
        $fechaPeriodo = explode(' - ', $request->input('periodo_out'));
        $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
        $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
    } 

    $query = DB::table('vw_ventas_nc_diferencia')

		->where('id_empresa', $id_empresa);
	
		/*if ($id_estado_factura) {
			$query->where('id_estado_factura', $id_estado_factura); // Filtrar por Unidad de negocio
		}
		if ($anulado) {
			$query->where('anulado', $anulado); // Filtrar por Unidad de negocio
		}*/
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('fecha_hora_facturacion', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);}
    

    $resumenes = $query->orderBy('id')->get();   
    $cuerpo = array();

    foreach ($resumenes as $resumen) {
        array_push($cuerpo, array(

            $resumen->nro_factura,
            $resumen->nro_nota_credito,
            $resumen->fecha_hora_facturacion,
            number_format($resumen->total_factura, 2, ',', '.'),
            number_format($resumen->total_cab_nc, 2, ',', '.'),
            number_format($resumen->diferencia, 2, ',', '.'),
            number_format($resumen->saldo_nc, 2, ',', '.'),
            number_format($resumen->monto, 2, ',', '.'),
            $resumen->moneda,
            $resumen->nombre,
        ));
       
    }

    $export = new ReporteListadoFacturasNCExport($cuerpo);
    return Excel::download($export, 'Reporte Listado Facturas -NC' . date('d-m-Y') . '.xlsx');
    
    
}

//REPORTE DETALLES CIERRE DE CAJAS
public function generarExceldetallesCierreCaja(Request $request)
{ 
    ini_set('memory_limit', '1G');
        set_time_limit(240);
    //$id_estado_factura = $request->input('id_estado_factura');
	//$anulado = $request->input('anulado'); 
    $id_empresa = $this->getIdEmpresa();
    if($request->input('periodo_out') != ""){
        $fechaPeriodo = explode(' - ', $request->input('periodo_out'));
        $desde     = $this->formatoFechaEntrada($fechaPeriodo[0]);
        $hasta     = $this->formatoFechaEntrada($fechaPeriodo[1]);			
    } 

    $query = DB::table('vw_reporte_cierre_caja_detalle')

		->where('id_empresa', $id_empresa);
	
		/*if ($id_estado_factura) {
			$query->where('id_estado_factura', $id_estado_factura); // Filtrar por Unidad de negocio
		}
		if ($anulado) {
			$query->where('anulado', $anulado); // Filtrar por Unidad de negocio
		}*/
		if (isset($desde)  && isset($hasta)) {
			$query->whereBetween('fecha_cierre', [$desde . ' 00:00:00', $hasta . ' 23:59:59']);}
    

    $resumenes = $query->orderBy('id_cierre')->get();   
    $cuerpo = array();

    foreach ($resumenes as $resumen) {
        $nombreCompleto = $resumen->nombre . ' ' . $resumen->apellido;
        array_push($cuerpo, array(

            $resumen->nro_recibo,
            $resumen->nombre_recibo,
            $nombreCompleto,
            $resumen->denominacion,
            $resumen->estado_denominacion,
            $resumen->fecha_cierre,
            $resumen->currency_code,
            number_format($resumen->total, 2, ',', '.')
        ));
       
    }

    $export = new ReporteListadoDetalleCierreCajasExport($cuerpo);
    return Excel::download($export, 'Reporte Listado Detalle Cierre de Cajas' . date('d-m-Y') . '.xlsx');
    
    
}


public function generarExcelMasivoPersonas(Request $req)
{ 
    $id_empresa = $this->getIdEmpresa();
   
    ini_set('memory_limit', '1G');
    set_time_limit(240);

    //$query = DB::select("SELECT id, nombre from personas where id_tipo_persona = 8 and activo = true and id_empresa = ".$this->getIdEmpresa()." order by nombre asc");
    $query = DB::table('personas')
        ->where('activo', false) // Set the condition to false to retrieve an empty result
        ->where('id_empresa', $this->getIdEmpresa())
        ->limit(0)
        ->get(); 

    $paises = DB::table('paises')->pluck('name_es', 'cod_pais');

   
    $cuerpo = array();

    foreach ($query as $persona) {
        
        array_push($cuerpo, array(

            $persona->nombre,
            $persona->email,
            $persona->celular,
            $persona->fecha_nacimiento,
            $persona->telefono,
            $persona->id_tipo_persona,
            $persona->id_tipo_identidad,
            $persona->id_pais,
            $persona->direccion,
            $persona->denominacion_comercial,
            $persona->apellido,
            $persona->id_ciudad,
            $persona->documento_identidad,
            $persona->id_plazo_pago,
            $persona->correo_comerciales,
            $persona->correo_administrativo,
            $persona->agente_retentor,
            $persona->cheque_emisor_txt,
            $persona->retentor,
            $persona->porcentaje_retencion,
            $persona->retener_iva,
            $persona->dv,
            $persona->aplica_iva,
            $persona->id_empresa,        
        ));
       
    }

    $export = new ReporteMasivoPersonasExport($cuerpo);
    return Excel::download($export, 'Reporte Masivo Personas-' . date('d-m-Y') . '.xls');
    
    
}

}


