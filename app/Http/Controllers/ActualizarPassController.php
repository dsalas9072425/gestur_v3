<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ActualizarPassController extends Controller
{
	public function index(Request $req){
		return view('pages.actualizarPass')->with('tokenEmail',$req->input('tokenEmail'));
	}
    
}
