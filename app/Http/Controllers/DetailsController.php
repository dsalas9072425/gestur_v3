<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\facades\View;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Config;

class DetailsController extends Controller
{
    /*
	Función para generar los detalles de los hoteles
	-Se envian los datos del hotel
	-Se reciben los datos de detalles del hotel del proveedor
    */
	public function index(Request $req){
		$client = new Client();
		$iibReq = new \StdClass;
		$datosLoggeo = \Session::get('datos-loggeo');
		$iibReq->token = $datosLoggeo->token;
		$datos = new \StdClass;
		$datosDetalle = new \StdClass;
		$datosDetalle->codHotel = $req->input('codHotel');
		$datosDetalle->codProveedor = $req->input('codProveedor');																				  
		$datos->datosDetalle = $datosDetalle;
		//dd($iibReq);
		$iibReq->datos = $datos;
		$logger = new Logger('Log');
		$logger->pushHandler(new \Monolog\Handler\StreamHandler(storage_path() . '/log/detallesIndex.log'), Logger::DEBUG);
		$stack = HandlerStack::create();
		$stack->push(Middleware::log(
									$logger,
								new MessageFormatter(
													'{token} - "code":"{code}" - "metodo": "{method}" - "url": "{uri}"" - "header": "{res_headers}" - "error": "{error}"- "tiempo":"{connect_time}" \n'
													 )
									)
					);
		//print_r("<br/><br/>REQUEST SEARCH:<br>".json_encode($iibReq)."<br>");
		// Se realiza la busqueda de los detalles
		try{
			$iibRsp = $client->post(Config::get('config.iibDetails'), [
				'json' => $iibReq,
				'handler' => $stack
			]);
		}
		catch(RequestException $e){
			return view('pages.timeErrorConexion');
		}
		catch(ClientException $e){
			return view('pages.errorConexion');	
		}	
		$iibObjRsp = json_decode($iibRsp->getBody());
		//dd($iibObjRsp);
		//print_r("<br/><br/>RESPONSE DETAILS:<br>".json_encode($iibObjRsp));
		if($iibObjRsp->codRetorno !=0){
			//Template de error de conexion.
			//return view('pages.errorConexion')->with('confRsp', $iibObjRsp->codRetorno);
		}
		$listadoComodidades =[];
		if(isset($iibObjRsp->datos->datosDetalle->hotel[0]->comodidadHotel)){
			foreach($iibObjRsp->datos->datosDetalle->hotel[0]->comodidadHotel as $key=>$comodidades) {
				$listadoComodidades[$key]= $comodidades;
			}
		}

	$codMon= $req->input('monCod');		
	return view('pages.hoteles.details')->with(['hotel'=> $iibObjRsp->datos->datosDetalle->hotel[0], 'listadoComodidades'=>$listadoComodidades, 'moneda'=>$codMon]);
	}

	
    /*
	Función para generar los detalles de los hoteles al precionar el link Mostrar Detalles 
	-Se envian los datos del hotel
	-Se reciben los datos de detalles del hotel del proveedor
    */
	public function getDetails(Request $req){
		/*
		$client = new Client([
			'headers' => [ 'Content-Type' => 'application/json' ]
		]);
		$iibReq = new \StdClass;
		$iibReq->token = $req->session()->get('token');
		$iibReq->idioma = "CAS";
		$iibReq->idBusqueda= "PY";
		$datos = new \StdClass;
		$datosDetalle = new \StdClass;
		$datosDetalle->codHotel = $req->input('codHotel');
		$datosDetalle->codProveedor = $req->input('codProveedor');
		$datos->datosDetalle = $datosDetalle;
		$iibReq->datos = $datos;
		$loggers = new Logger('Log');
		$loggers->pushHandler(new \Monolog\Handler\StreamHandler(storage_path() . '/log/getDetails.log'), Logger::DEBUG);
		$stacks = HandlerStack::create();
		$stacks->push(Middleware::log(
									$loggers,
								new MessageFormatter(
													'{token} - "code":"{code}" - "metodo": "{method}" - "url": "{uri}"" - "header": "{res_headers}" - "error": "{error}"- "tiempo":"{connect_time}" \n'
													 )
									)
					);
		//print_r("<br/><br/>RESPONSE DETAILS:<br>".json_encode($iibReq));
		$iibRsp = $client->post(Config::get('config.iibDetails'),
				[
					'body' => json_encode($iibReq),
					'handler' => $stacks
					] 
			);
		$iibObjRsp = json_decode($iibRsp->getBody());
	
		if($iibObjRsp->codRetorno !=0){
			//Template de Error de Conexión
			return view('pages.errorConexion')->with('confRsp', $iibObjRsp->codRetorno);
		}
		return $iibRsp->getBody();
		*/
	}
	
}
