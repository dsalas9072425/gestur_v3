<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use	Illuminate\Support\Facades\View;
use App\PlanCuenta;
use App\CuentaGasto;
use App\Persona;
use Session;
use Redirect;
use DB;
use App\Empresa;
use Maatwebsite\Excel\Facades\Excel;

class CuentaContableController extends Controller
{


	public function index(){

		$cuentasCostos = CuentaGasto::with('cuentasPadre')->where('id_empresa',$this->getIdEmpresa())->get();
		return view('pages.mc.cuentaContable.index', compact('cuentasCostos'));

	}

		public function setDataPlanCuenta(Request $req){
				// dd($req->all());

			$err = true;
			$id_grupo  = $req->input('id_cuenta');
			$data_plan = array();
			$codigo = $req->input('codigo');
			$id_empresa =  $this->getIdEmpresa();


			if($id_grupo === 'G'){
				$id_grupo = 0;
			} else {
				$cod = DB::select("SELECT cod_txt from plan_cuentas WHERE id = ".$id_grupo);
				// print_r($cod);
				$codigo_txt = $cod[0]->cod_txt;
				$codigo_txt .='.'.$codigo;
				$codigo = $codigo_txt;
			}

			if($req->input('cuenta_gasto') != ""){
				$cuenta_gasto = $req->input('cuenta_gasto');
			}else{
				$cuenta_gasto = null;
			}

			//Validar codigo base sin puntos
			$codigo_search = trim(str_replace('.','',$codigo));

			$exist = DB::select("SELECT * FROM plan_cuentas WHERE REPLACE(cod_txt, '.', '') = ? and id_empresa = ?" ,[$codigo_search, $id_empresa]);
			
			if(count($exist)){
				return response()->json(['resp'=> true, 'data'=> 0, 'msg' => 'Ya existe un numero de cuenta similar.']);
			}

         try {   
			DB::table('plan_cuentas')
						->insert([
							'cod_txt'=>$codigo,
							'descripcion'=>$req->input('denominacion'),
							'codigo'=>$req->input('codigo'),
							'grupo'=>$id_grupo,
							'tipo'=>$req->input('tipo'),
							'asentable'=>$req->input('asentable'),
							'id_cuenta_gasto'=>$cuenta_gasto,
							'id_empresa'=>$id_empresa
						]);
	
         } catch(\Exception $e){
			Log::error($e);
			return response()->json(['resp'=> true, 'data'=> 0, 'msg' => 'Ocurrio un error al intentar guardar los datos.']);
            $err = false;
         }  

		return response()->json(['resp'=>$err, 'data'=> 1]);

	}


	public function getDataPlanCuenta(Request $req){

		$data_plan = PlanCuenta::where('activo',true)->where('id_empresa', $this->getIdEmpresa())->get();
		return response()->json(['data'=>$data_plan]);
	}


	public function controlCodigo(Request $req){
		
		$cod = $num_max = array();

		if($req->id_cuenta != 'G'){

	 $num_max = DB::select("SELECT COALESCE(max(codigo),0) as max
	 						FROM plan_cuentas 
	 						WHERE id_empresa = ".$this->getIdEmpresa()." 
							 AND grupo = ".$req->id_cuenta); 

	 $cod = DB::select("SELECT cod_txt FROM plan_cuentas WHERE  id = ".$req->id_cuenta);
	  }

	 return response()->json(['data'=>$num_max, 'cod'=>$cod]);
	}




	public function getCuentaInfo(Request $req){

		$cod = DB::table('plan_cuentas')->where('id',$req->id_cuenta)->where('activo', true) ->first();	
		/*echo '<pre>';
		print_r($cod);*/
		return response()->json(['data'=>$cod]);
	}

	

	public function saveCuentaInfo(Request $req){
		$err = false;
		if($req->input('cuenta_gasto_edit') !=""){
			$cuenta_gasto = $req->input('cuenta_gasto_edit');
		}else{
			$cuenta_gasto = null;
		}
       try {   
       
				DB::table('plan_cuentas')
				->where('id',$req->input('id'))
				->update([

					'descripcion'=>$req->input('denominacion'),
					'tipo'=>$req->input('tipo'),
					'asentable'=>$req->input('asentable'),
					'activo'=>$req->input('activo'),
					'cod_txt' =>$req->input('base').".".$req->input('codigo'),
					'id_cuenta_gasto'=>$cuenta_gasto,
					'codigo' =>$req->input('codigo') 
				]);
					$err = true;

       } catch(\Exception $e){
                 $err = false; 
        } 

             return response()->json(['err'=>$err]);  
	}//function



	private function getIdUsuario()
	{
 		return $idUsuario = Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario;
	}

	private function getIdEmpresa()
	{
 		return $idEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;
	}


	public function altaPlan(){

		$id_usuario = $this->getIdUsuario();
		$usuario = Persona::where('id_empresa',1)->find($id_usuario);
		$arr = [28566,89299,469,52163];
		$permiso = false;
		if(isset($usuario->id) && in_array($usuario->id, $arr)){
			$permiso = true;
		}

		if(isset($usuario->id) && $usuario->id_tipo_persona == 1){
			$permiso = true;
		}


		if(!$permiso){
			flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}

		//Empresas que  no estan en el plan de cuenta
		$plan_cuenta = PlanCuenta::select('id_empresa')->groupBy('id_empresa')->get()->toArray();
		$empresas = Empresa::whereNotin('id', $plan_cuenta)->get();
		return view('pages.mc.cuentaContable.altaPlan',compact('empresas'));

	}

	public function guardarPlanCuenta(Request $request){

		$id_usuario = $this->getIdUsuario();
		$usuario = Persona::find($id_usuario);
		$arr = [28566,89299,469,52163];
		$permiso = false;

		if(in_array($usuario->id, $arr)){
			$permiso = true;
		}

		if($usuario->id_tipo_persona == 1){
			$permiso = true;
		}


		if(!$permiso){
			flash('No tiene los permisos para ver esta vista')->error();
            return redirect()->route('home');
		}


		$path = $request->file('file')->getRealPath();
		$data = Excel::load($path)->get();
		$id_empresa = $request->id_empresa;
	
		foreach ($data->toArray() as $item) {
			
			$codigo = $item['cod_txt'];
			$id_grupo = 0;
			$lowercase_tipo = strtolower($item['tipo']);
			$tipo = '';
			$cod_txt = '';
			$asentable = $item['asentable'] == 'SI';

			if($lowercase_tipo == 'deudor'){
				$tipo = 'D';
			} else if($lowercase_tipo == 'acreedor'){
				$tipo = 'A';
			} else {
				Log::error('Error no se pudo identificar el tipo for continua');
				continue;
			}

			
			//Si tiene punto buscamos a su padre
			if (strpos($codigo, '.') !== false) {
				$modified_codigo = $this->decenderUnPunto($codigo);

				// buscar a su padre
				$plan = PlanCuenta::where('cod_txt', $modified_codigo)->where('id_empresa',$id_empresa)->first();
				if($plan){
					$id_grupo = $plan->id;
				} else {

					$buscar = true;
					//Buscamnos a que nivel pertenece hasta que no haya mas puntos 
					// por ejemplo una cuenta que llegue 5.0.0.1, no existe un padre 5 , pero el sistema para comprobar debe correr desde el 1 hasta el 5
					while ($buscar) {
						if(strpos($modified_codigo, '.') !== false){
							$modified_codigo = $this->decenderUnPunto($modified_codigo);
                            $plan = PlanCuenta::where('cod_txt', $modified_codigo)->where('id_empresa',$id_empresa)->first();
							if($plan){
								$id_grupo = $plan->id;
							}
						} else {
							//Es una cuenta sin padre e hijos, es una principal
							$buscar = false;
						}
					}
				}
				
					
				
				
			}



			DB::table('plan_cuentas')
			->insert([
				'cod_txt'=>$codigo,
				'descripcion'=>$item['descripcion'],
				'grupo'=>$id_grupo,
				'tipo'=>$tipo,
				'asentable'=> $asentable,
				'id_empresa'=> $id_empresa
			]);
		}

		flash('Cuenta Cargada');

        return back();
	}


	public function decenderUnPunto($cod){
		$cod_array = explode('.', $cod);
		array_pop($cod_array); //Quita el ultimo elemento
		return implode('.', $cod_array);
	}


}//class