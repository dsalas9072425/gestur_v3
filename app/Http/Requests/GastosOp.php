<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GastosOp extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * RELACIONES PARA LOS GASTOS DE OP
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'id_op' => 'required|numeric',
            'id_cuenta_contable'=>'required:numeric',
            'importe' => 'required|numeric|min:0.01',
            'concepto' => 'required'
        ];
    }

    public function messages()
{
    return [
        'err'=> false,
        'id_op.required' => 'Los datos enviados son corruptos.',
        'id_cuenta_contable.required' => 'Los datos enviados son corruptos.',
        'importe.min' => 'El :attribute debe ser mayor a 0.',
        'concepto.required' => 'dsfdsfdf'
    ];
}


}
