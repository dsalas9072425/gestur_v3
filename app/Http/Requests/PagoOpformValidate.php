<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PagoOpformValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * VALIDACION APLICADA A FOMULARIOS DE PAGO PROVEEDORES
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'sucursal' => 'required|numeric',
            'centro_costo'=>'required:numeric',
            'cotizacion' => 'required|numeric',
    
            'total_pago'=>'required|numeric',
            'total_pago_neto'=>'required|numeric',
            'total_nc'=>'required|numeric',
            'total_anticipo'=>'required|numeric',
    
    
            'pagosDetalle'=>'required',
            'pagosCabecera'=>'required',
            // 'entregado'=>'required',
            // 'concepto'=>'required'
        ];


    }
}
