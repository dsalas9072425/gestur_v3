<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            //\App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            //\App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
		'redirectToLogin'=> \App\Http\Middleware\RedirectToLogin::class,
        'redirectToLogins'=> \App\Http\Middleware\RedirectToLogins::class,
        'autorizate'=> \App\Http\Middleware\Autorizate::class,
        'autorization'=> \App\Http\Middleware\Autorization::class,
        'redirectToUser'=> \App\Http\Middleware\RedirectToUser::class,
        'generateToken'=> \App\Http\Middleware\GenerateToken::class,
        'adminOperativo' => \App\Http\Middleware\AdminOperativo::class,
        'searchToken' => \App\Http\Middleware\SearchToken::class,
        'fogetSession' => \App\Http\Middleware\ForgetSession::class,
        'prevent-back-history' => \App\Http\Middleware\PreventBackHistory::class,
        'autorizacionVistas' => \App\Http\Middleware\AutorizacionVistas::class,
        'AutorizacionVistasAfter' => \App\Http\Middleware\AutorizacionVistasAfter::class,
        

    ];
}
