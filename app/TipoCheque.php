<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCheque extends Model
{
    protected $table = 'tipo_cheques';
    protected $primaryKey = 'id'; 

}