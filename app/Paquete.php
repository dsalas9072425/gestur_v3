<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    protected $table = 'paquetes';

    public function destinos()
    {
        return $this->hasMany('App\PaqueteDestinos', 'paquete_id', 'id');
    }
    public function categorias()
    {
        return $this->hasMany('App\PaqueteCategoriaPaquete', 'id_paquete', 'id');
    }
	
	public function imagen()
    {
        return $this->hasMany('App\ImagenPaquete', 'id_paquete', 'id');
    }
    public function usuario()
    {
       return $this->belongsTo('App\Usuario','usuario_id','id_usuario');
    }
    public function moneda()
    {
       return $this->belongsTo('App\Currency','currency_id','currency_id');
    }

}