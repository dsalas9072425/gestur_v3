<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'logs';
	protected $primarykey = 'id';
	protected $fillable = ['id', 'id_request'];

	public function usuario()
    {
        return $this->belongsTo('App\Usuario','usuario_id','usuario_id');
    }

}
