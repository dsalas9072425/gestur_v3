<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigNemo extends Model
{
    protected $table = 'nemo_configuraciones';
	protected $primarykey = 'id';

}
