<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservasNemo extends Model
{
    protected $table = 'nemo_reservas';
    public $timestamps = false;
}