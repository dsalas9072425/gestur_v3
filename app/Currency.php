<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency';
    protected $primaryKey = 'currency_id'; 
	public $timestamps = false;
	protected $dateFormat = 'Y-m-d H:i:s';


	public function cotizacion()
    {
        return $this->belongsTo('App\Cotizacion','id_currency','currency_id');
    }







}