<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaCredito extends Model
{
    protected $table = 'nota_credito';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';


    public function detalleNotaCredito()
    {
        return $this->hasMany('App\DetalleNotaCredito','id_nota_credito','id');
    }
    public function factura()
    {
        return $this->hasMany('App\Factura','id','id_factura');
    }

    public function facturas()
    {
        return $this->belongsTo('App\Factura','id_factura','id');
    }
 	
	public function cliente()
    {
        return $this->belongsTo('App\Persona', 'cliente_id','id');
    }
  
    public function vendedorEmpresa()
    {
        return $this->belongsTo('App\Persona', 'vendedor_id','id');
    }
     public function usuario()
    {
        return $this->belongsTo('App\Persona', 'id_usuario','id');
    }

    public function pasajero()
    {
        return $this->belongsTo('App\Persona', 'id_pasajero_principal','id');
    }

     public function notaCreditoDetalle()
    {
        return $this->hasMany('App\NotaCreditoDetalle', 'id_nota_credito');
    }

     public function proforma()
    {
        return $this->belongsTo('App\Proforma', 'id','factura_id');
    }

     public function currency()
    {
        return $this->belongsTo('App\Divisas', 'id_moneda_venta','currency_id');
    }

     public function estado()
    {
        return $this->belongsTo('App\EstadoFactour', 'id_estado_nc','id');
    }
    public function estadoCobro()
    {
        return $this->belongsTo('App\EstadoFactour', 'id_estado_cobro','id');
    }

    public function tipoFacturacion()
    {
        return $this->belongsTo('App\TipoFacturacion', 'id_tipo_facturacion','id');
    }

    public function timbrado()
   {
       return $this->belongsTo('App\Timbrado', 'id_timbrado','id');
   }

      public function tipoFactura()
    {
        return $this->belongsTo('App\TipoFactura', 'id_tipo_factura','id');
    }

    public function libroVenta()
    {
        return $this->hasMany('App\LibroVenta', 'id_documento','id');
    }

    public function libroVentaSaldo0()
    {
        return $this->belongsTo('App\LibroVenta', 'id_lv','id');
    }

}