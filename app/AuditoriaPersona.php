<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditoriaPersona extends Model
{
    protected $table = 'personas_auditoria';

    public function usuarioAuditoria()
    {
       return $this->belongsTo('App\Persona','id_usuario_auditoria','id');
    }

    public function tipo_facturacion()
    {
       return $this->belongsTo('App\TipoFacturacion','id_tipo_facturacion','id');
    }

    public function empresa()
    {
       return $this->belongsTo('App\Empresa','id_empresa','id');
    }

    public function tipoPersona()
    {
       return $this->belongsTo('App\TipoPersona','id_tipo_persona','id');
    }

    public function tipo_persona()
    {
       return $this->belongsTo('App\TipoPersona','id_tipo_persona','id');
    }

    public function productos(){
        return $this->belongsToMany('App\TipoPersona','id_tipo_persona','id');
    }

    public function vendedorEmpresa()
    {
       return $this->belongsTo('App\Persona','id_vendedor_empresa','id');
    }

    public function tipoPerfil()
    {
       return $this->belongsTo('App\Perfil','id_perfil','id');
    }

    public function tipoEmpresa()
    {
       return $this->belongsTo('App\Empresa','id_vendedor_empresa','id');
    }

    public function tipoPersoneria()
    {
       return $this->belongsTo('App\TipoPersoneria','id_personeria','id');
    }

     public function tipoIdentidad()
    {
       return $this->belongsTo('App\TipoIdentidad','id_tipo_identidad','id');
    }

    public function lineaCredito()
    {
       return $this->belongsTo('App\LineaCredito','id_linea_credito','id');
    }

     public function agencia()
    {
       return $this->belongsTo('App\Persona','id_persona','id');
    }

    public function sucursalAgencia()
    {
       return $this->belongsTo('App\Persona','id_sucursal_empresa','id');
    }

    public function plazoPago()
    {
       return $this->belongsTo('App\PlazoPago','id_plazo_pago','id');
    }
    

}