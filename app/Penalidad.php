<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penalidad extends Model
{
    protected $table = 'penalidades_grupos';
	protected $primarykey = 'id';
    public $timestamps = false;

}
