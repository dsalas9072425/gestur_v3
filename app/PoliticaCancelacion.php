<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoliticaCancelacion extends Model
{
    protected $table = 'politica_cancelacion';
    protected $primaryKey = 'id'; 

    public function politica()
    {
        return $this->belongsTo('App\Reserva','id','id_reserva');
    }


}