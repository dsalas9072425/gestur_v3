<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCuenta extends Model
{
    protected $table = 'estado_cuenta';
    public $timestamps = false;
	protected $primaryKey = 'id';
	// protected $fillable = ['pnr', 'estado_id', 'codigo_bloqueo'];

	public function currency(){
		return $this->belongsTo('App\Currency', 'currency_id');
	}

	public function persona(){
		return $this->belongsTo('App\Persona', 'persona_id', 'id');
	}

	public function tipoDocumento(){
		return $this->belongsTo('App\TipoDocumento', 'tipo_documento_id');
	}

	public function lineaCredito(){
		return $this->belongsTo('App\LineaCredito', 'tipo_documento_id');
	}

}