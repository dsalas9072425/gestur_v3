<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EscalaComisionDetalle extends Model
{
    protected $table = 'escala_comision_detalle';
    public $timestamps = false;
	protected $primaryKey = 'id';

	public function escalaComision()
    {
      return $this->belongsTo('App\EscalaComision','id_cabecera','id');
    }

}