<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
    protected $fillable = ['tasas_yq'];
	protected $dateFormat = 'Y-m-d H:i:s';

	const FORMA_PAGO_CASH = "CASH";
	
	public function persona(){
		return $this->belongsTo('App\Persona', 'id_proveedor', 'id');
	}

	public function vendedor(){
		return $this->belongsTo('App\Persona', 'id_vendedor', 'id');
	}

	public function grupo(){
		return $this->belongsTo('App\Grupo', 'id_grupo');
	}

	public function tipoTicket(){
		return $this->belongsTo('App\TipoTicket', 'id_tipo_ticket');
	}

	public function estado(){
		return $this->belongsTo('App\EstadoFactour', 'id_estado');
	}

	public function currency(){
        return $this->belongsTo('App\Currency','id_currency_costo','currency_id');
    }

    public function tipoTransaccion(){
		return $this->belongsTo('App\TipoTransaccionTicket', 'id_tipo_transaccion');
	}

}