<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoCierreCajaDetalle extends Model
{
    
    protected $table = 'historico_cierre_caja_detalle';
    public $timestamps = false;


    
	public function moneda()
    {
       return $this->belongsTo('App\Currency','moneda_id','currency_id');
    }

	public function formaCobro()
    {
       return $this->belongsTo('App\FormaCobroCliente','forma_cobro_id','id');
    }

    public function historico_cabecera()
    {
       return $this->belongsTo('App\HistoricoCierreCaja','id_historico_cierre_caja_cabecera','id');
    }


}
