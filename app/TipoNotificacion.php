<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoNotificacion extends Model
{
    protected $table = 'tipos_notificaciones';
    protected $primaryKey = 'id'; 

}