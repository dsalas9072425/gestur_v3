<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuEmpresa extends Model
{

	public function menus()
    {
		return $this->belongsTo('App\Menu', 'id_menu', 'id');
	}

	public function empresas()
    {
		return $this->belongsTo('App\Empresa', 'id_empresa');
	}

}