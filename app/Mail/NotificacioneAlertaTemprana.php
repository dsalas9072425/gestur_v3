<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacioneAlertaTemprana extends Mailable
{
    use Queueable, SerializesModels;

    private $tickets_oasis;
    private $tickets_copa_sin_iva;
    private $tickets_creados_hoy;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tickets_oasis,$tickets_copa_sin_iva, $tickets_creados_hoy )
    {
        $this->tickets_oasis = $tickets_oasis;
        $this->tickets_copa_sin_iva = $tickets_copa_sin_iva;
        $this->tickets_creados_hoy = $tickets_creados_hoy;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[Gestur] Alerta de Sistema')
            ->view('mail.alerta_temprana')
            ->with([
                'tickets_oasis' => $this->tickets_oasis,
                'tickets_copa_sin_iva' => $this->tickets_copa_sin_iva,
                'tickets_creados_hoy' => $this->tickets_creados_hoy,
            ]);
  
    }
}
