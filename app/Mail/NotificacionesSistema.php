<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacionesSistema extends Mailable
{
    use Queueable, SerializesModels;

    private $proceso;
    private $mensaje;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($proceso, $mensaje)
    {
        $this->proceso = $proceso;
        $this->mensaje = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[Gestur] Alerta de Sistema')
            ->view('mail.alerta')
            ->with([
                'proceso' => $this->proceso,
                'mensaje' => $this->mensaje,
            ]);
  
    }
}
