<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VoucherDtpPuntos extends Mailable
{
    use Queueable, SerializesModels;

    public $pdf;
    public $mes;
    public $anho;
    public $empresa_logo;
    public $nombre;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdf,$mes,$anho,$empresa_logo, $nombre)
    {
        $this->pdf = $pdf;
        $this->mes = $mes;
        $this->anho = $anho;
        $this->empresa_logo = $empresa_logo;
        $this->nombre = $nombre;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[Gestur] Voucher Dtp Puntos')
            ->view('mail.voucher_dtp_puntos')
            ->attachData($this->pdf->output(), "voucher_dtp_puntos.pdf");
    }
}
