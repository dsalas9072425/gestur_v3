<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Recibo;
use App\ReciboDetalle;
use App\BancoCabecera;
use App\FormaPagoCliente;
use App\Persona;
use App\Currency;
use App\LibroVenta;
use App\EstadoFactour;
use App\ChequeFp;
use App\FormaCobroRecibo;
use App\FormaCobroReciboDetalle;
use DB;

class ReciboCobranza extends Mailable
{
  use Queueable, SerializesModels;
     
    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $demo;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($demo)
    {
        $this->demo = $demo;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('sergio@dtp.com.py')
                    ->view('mail.reciboCobranza')
                    ->text('mail.reciboCobranza_plain')
                    ->with(
                      [
                            'testVarOne' => '1',
                            'testVarTwo' => '2',
                      ]);
                    /*  ->attach(public_path('/images').'/demo.jpg', [
                              'as' => 'demo.jpg',
                              'mime' => 'image/jpeg',
                      ]);*/
    }
  
       // return $this->from('paulo_aquino@hotmail.es')->view('Mail.reciboCobranza',compact('recibo','detalle','NumeroALetras','formaPago'));

}
