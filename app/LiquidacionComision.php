<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiquidacionComision extends Model
{
  protected $table = 'liquidacion_comisiones_vendedores';

    public function usuario()
    {
      return $this->belongsTo('App\Persona','id_usuario','id');
    }

    public function vendedor()
    {
      return $this->belongsTo('App\Persona','id_vendedor','id');
    }

    public function liquidacionesDetalle()
    {
      return $this->hasMany('App\LiquidacionComisionDetalle','id_liquidacion','id');
    }

}