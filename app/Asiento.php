<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asiento extends Model
{
    protected $table = 'asientos_contables';
    protected $primaryKey = 'id';
    public $timestamps = false;

	public function usuario()
    {
        return $this->belongsTo('App\Persona','id_usuario','id');
    }

    public function asientoDetalle()
    {
        return $this->hasMany('App\AsientoDetalle','id_asiento_contable','id');
    }

    public function origenAsiento()
    {
        return $this->belongsTo('App\OrigenAsiento', 'id_origen_asiento');
    }

}


