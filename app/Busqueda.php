<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Busqueda extends Model
{
    protected $table = 'busquedas';

    public function destino()
    {
        return $this->belongsTo('App\DestinoDtpmundo','id_destino','id_destino_dtpmundo');
    }
    public function usuario()
    {
        return $this->belongsTo('App\Usuario','id_usuario','id_usuario');
    }

}