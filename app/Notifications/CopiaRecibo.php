<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CopiaRecibo extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /** 
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
       public function toMail($notifiable)
        {
            
            return (new MailMessage)
                ->subject($notifiable->subject)
                // ->from('suport@git.com.py', 'GIT')
                ->from($notifiable->from)
                ->greeting($notifiable->greeting)
                ->line($notifiable->linea1)
                ->line($notifiable->cuerpoHtml)
                ->action($notifiable->accionRecibo, $notifiable->direccionRecibo)
                ->line($notifiable->linea2)
                ->attachData($notifiable->adjunto, 'Recibo Nro: '.$notifiable->numRecibo, [
                'mime' => 'application/pdf',
            ]);

        }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
