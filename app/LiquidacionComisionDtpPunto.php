<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class LiquidacionComisionDtpPunto extends Model
{
    protected $table = 'liquidacion_dtp_puntos';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
    // protected $fillable = [
    //     'vendedor_id',
    //     'total_puntos',
    //     'total_comision',
    //     'saldo_comision',
    //     'usuario_id',
    //     'periodo',
    //     'created_at',
    //     'updated_at',
    // ];




    public function meta()
    {
      return $this->belongsTo('App\MetaVendedor','id_meta','id');
    }

    public function liquidacionesDetalle()
    {
      return $this->hasMany('App\LiquidacionComisionDtpPuntoDetalle','id_liquidacion','id');
    }

    public function factura_lc()
    {
      return $this->hasOne('App\LibroCompra','id','id_libros_compras');
    }

    
    
}
