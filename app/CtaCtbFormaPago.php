<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtaCtbFormaPago extends Model
{
    protected $table = 'cta_ctb_forma_pago';
	public $primarykey = 'id';
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function moneda()
    {
        return $this->belongsTo('App\Currency', 'id_moneda','currency_id');
    }
    public function planCuenta()
    {
        return $this->belongsTo('App\PlanCuenta', 'id_cuenta_contable','id');
    }
    public function banco()
    {
        return $this->belongsTo('App\BancoDetalle', 'id_cuenta_banco','id');
    }
}
