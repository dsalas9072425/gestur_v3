<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentasRapidasDetalle extends Model
{
    protected $table = 'ventas_rapidas_detalle';
    protected $primaryKey = 'id'; 
    public $timestamps = false;


    public function producto()
    {
        return $this->hasOne('App\Producto','id','id_producto');
    }

    public function proveedor()
    {
        return $this->hasOne('App\Persona','id','id_proveedor');
    }


}