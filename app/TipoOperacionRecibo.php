<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoOperacionRecibo extends Model
{
    protected $table = 'tipo_operacion_recibo';
    protected $primaryKey = 'id';  
    public $timestamp = "false";

}