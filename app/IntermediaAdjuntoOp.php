<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\OpCabecera;
class IntermediaAdjuntoOp extends Model
{
    protected $table = 'intermedia_adjunto_op'; 

    public function opCabecera()
    {
        return $this->belongsTo(OpCabecera::class, 'op_cabecera_id');
    }
}
