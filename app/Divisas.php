<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisas extends Model
{
    protected $table = 'currency';
    protected $primaryKey = 'currency_id'; 
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';

	public function divisa()
    {
        return $this->belongsTo('App\Cotizacion','id_currency','currency_id');
    }

    public function cotizacion()
    {
        return $this->hasOne('App\Cotizacion','id_currency','currency_id');
    }



}