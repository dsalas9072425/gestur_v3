<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferenciasCuentas extends Model
{
    protected $table = 'transferencias_cuentas';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
}