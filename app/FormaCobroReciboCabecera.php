<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaCobroReciboCabecera extends Model
{
    protected $table = 'forma_cobro_recibo_cabecera';
    protected $primaryKey = 'id'; 


    public function formaCobroReciboDetalles()
    {
        return $this->hasMany('App\FormaCobroReciboDetalle','id_cabecera','id');
    }	

    public function recibo()
    {
        return $this->belogsTo('App\Recibo','id_recibo');
    }


    public function fp_detalle()
    {
      return  $this->hasMany('App\FormaCobroReciboDetalle','id_cabecera');
    }


}