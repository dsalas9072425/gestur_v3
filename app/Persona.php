<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Persona extends Model
{
    protected $table = 'personas';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
    public $append = ['nuevo'];
    use  Notifiable;

    //NOMBRE EN MAYUSCULA
    public function getNombreAttribute($value)
    {
        return ucfirst($value);
    }

     //NOMBRE EN MINUSCULA
    public function getApellidoAttribute($value)
    {
        return ucfirst($value);
    }

    //Nombre completo
    public function getFullNameAttribute()
    {
      $full_name = $this->nombre;
      if(!is_null($this->apellido)){
        $full_name .= ' '.$this->apellido;
      }
      return $full_name;
    }


    public function tipo_facturacion()
    {
      return $this->belongsTo('App\TipoFacturacion','id_tipo_facturacion','id');
    }

    public function empresa()
    {
      return $this->belongsTo('App\Empresa','id_empresa','id');
    }

    public function tipoPersona()
    {
      return $this->belongsTo('App\TipoPersona','id_tipo_persona','id');
    }

    public function tipo_persona()
    {
      return $this->belongsTo('App\TipoPersona','id_tipo_persona','id');
    }

    public function productos(){
      return $this->belongsToMany('App\TipoPersona','id_tipo_persona','id');
    }

    public function vendedorEmpresa()
    {
      return $this->belongsTo('App\Persona','id_vendedor_empresa','id');
    }

    public function tipoPerfil()
    {
      return $this->belongsTo('App\Perfil','id_perfil','id');
    }

    public function tipoEmpresa()
    {
      return $this->belongsTo('App\Empresa','id_vendedor_empresa','id');
    }

    public function tipoPersoneria()
    {
      return $this->belongsTo('App\TipoPersoneria','id_personeria','id');
    }

     public function tipoIdentidad()
    {
      return $this->belongsTo('App\TipoIdentidad','id_tipo_identidad','id');
    }

    public function lineaCredito()
    {
      return $this->belongsTo('App\LineaCredito','id_linea_credito','id');
    }

    public function pais()
    {
      return $this->belongsTo('App\Pais','id_pais','cod_pais');
    }

    public function ciudad()
    {
      return $this->belongsTo('App\Ciudad','id_ciudad','id');
    }

     public function agencia()
    {
      return $this->belongsTo('App\Persona','id_persona','id');
    }

    public function sucursalAgencia()
    {
      return $this->belongsTo('App\Persona','id_sucursal_empresa','id');
    }

    public function plazoPago()
    {
      return $this->belongsTo('App\PlazoPago','id_plazo_pago','id');
    }

    public function usuarioAuditoria()
    {
      return $this->belongsTo('App\Persona','id_usuario','id');
    }

    public function escalaComision()
    {
      return $this->hasOne('App\EscalaComision', 'id_vendedor', 'id');
    }

    public function sucursalEmpresa()
    {
      return $this->hasOne('App\SucursalEmpresa', 'id','id_sucursal_contable');
    }

    public function tarjetas()
    {
      return $this->hasMany('App\TarjetaPersona','id_persona','id');
    }
    
}
