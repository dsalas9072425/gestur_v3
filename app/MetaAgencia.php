<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaAgencia extends Model
{
    protected $table = 'metas_agencias';
    protected $primaryKey = 'id_meta';  
    protected $fillable = ['id_vendedor_agencia', 'meta_anual', 'anho', 'meta_mensual', 'meta_trimestral', 'meta_semestral','mes'];


    public function vendedorAgencia()
    {
        return $this->belongsTo('App\Persona', 'id_vendedor_agencia','id');
    }
}
