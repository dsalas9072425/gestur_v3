<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaDetalleHistorico extends Model
{
    protected $table = 'facturas_detalle_reprice';
	protected $dateFormat = 'Y-m-d H:i:s';


	public function producto()
    {
        return $this->belongsTo('App\Producto', 'id_producto','id');
    }
    public function proveedor()
    {
        return $this->belongsTo('App\Persona', 'id_proveedor','id');
    }
    public function prestador()
    {
        return $this->belongsTo('App\Persona', 'id_prestador','id');
    }
    public function factura()
    {
        return $this->belongsTo('App\Factura', 'id_factura');
    }
    public function vendedor()
    {
        return $this->belongsTo('App\Persona', 'id_usuario','id');
    }
    
    public function currencyVenta()
    {
        return $this->belongsTo('App\Divisas', 'currency_costo_id','currency_id');
    }

    public function voucher()
    {
        return $this->hasMany('App\Voucher', 'id_proforma_detalle','id');
    }
    public function libroCompra()
    {
        return $this->hasMany('App\LibroCompra', 'id_documento_detalle','id');
    }
    public function ProformasDetalle()
    {
        return $this->belongsTo('App\ProformasDetalle', 'id_proforma_detalle','id');
    }
}
