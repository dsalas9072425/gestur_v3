<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoTransaccionTicket extends Model
{
    protected $table = 'tipo_transaccion_ticket';
    protected $primaryKey = 'id'; 
    public $timestamps = false;


}