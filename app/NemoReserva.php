<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NemoReserva extends Model
{
    protected $table = 'reservas_nemo';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
    public function reserva_habitacion()
    {
        return $this->belongsTo('App\NemoHabitacion', 'id_reserva','id');
    }
	public function proveedor(){
		return $this->belongsTo('App\Persona', 'supplier_id', 'id');
	}

	public function prestador(){
		return $this->belongsTo('App\Persona', 'lender_id', 'id');
	}

}