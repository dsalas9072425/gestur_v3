<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileReserva extends Model
{
	protected $table = 'file_reserva';
	public $primarykey = 'file_reserva_id';

}
