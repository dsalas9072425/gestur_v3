<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoIdentidad extends Model
{
    protected $table = 'tipo_identidad';
    protected $primaryKey = 'id';  

}