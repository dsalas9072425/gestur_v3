<?php

namespace App\Jobs;

use App\HistoricoSaldoBanco;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Mail\NotificacionesSistema;
use Illuminate\Support\Facades\Log;

class HistoricoCuentasJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('HistoricoCuentasJob: Inicio de proceso');

        try {
       

                    $cuenta_corriente_banco = DB::table('vw_reporte_cuenta')->get();
                    $fecha = date('Y-m-d H:i:s');
                    // $fecha = '2023-01-04 23:00:00';
             
                        foreach ($cuenta_corriente_banco as $cuenta) {
                            $historico = new HistoricoSaldoBanco();
                            $historico->nombre                  = $cuenta->nombre;
                            $historico->id_banco_cabecera       = $cuenta->id_banco_cabecera;
                            $historico->id_empresa              = $cuenta->id_empresa;
                            $historico->tipo_cuenta             = $cuenta->tipo_cuenta_banco_n;
                            $historico->cuenta_bancaria         = $cuenta->cuenta_bancaria;
                            $historico->saldo                   = $cuenta->saldodetalle;
                            $historico->currency_code           = $cuenta->currency_code;
                            $historico->numero_cuenta           = $cuenta->numero_cuenta;
                            $historico->activo                  = $cuenta->activo;
                            $historico->created_at              = $fecha;
                            $historico->updated_at              = $fecha;
                            $historico->save();
                        }

           
            } catch (\Exception $e) {
                Log::error('HistoricoCuentasJob: Error en proceso: '.$e->getMessage());
                //Enviar correo de alerta
                $proceso = 'HistoricoCuentasJob';
                $mensaje = 'Error en proceso: '.$e->getMessage();
                Mail::to('ti@git.com.py')->send(new NotificacionesSistema($proceso, $mensaje));
            }


        Log::info('HistoricoCuentasJob: Fin de proceso');

    }
}
