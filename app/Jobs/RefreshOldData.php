<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Facades\Log;
use App\Empresa;
use App\Ticket;
use App\Mail\NotificacionesSistema;
use App\Mail\NotificacioneAlertaTemprana;


class RefreshOldData implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
 


            try {
                try {
                       
                    // $today = Carbon::today()->format('Y-m-d');
                    // $firstDayOfYear = Carbon::now()->startOfYear()->toDateString();
                    // $firstDayOfMonth = Carbon::now()->startOfMonth()->toDateString();
    
                    // DB::statement("DROP MATERIALIZED VIEW IF EXISTS public.mv_ticket2");
                    DB::statement("REFRESH MATERIALIZED VIEW public.mv_ticket");
                //     DB::statement("
                //     CREATE MATERIALIZED VIEW public.mv_ticket AS 
                //     SELECT * 
                //     FROM public.vw_ticket where 
                //     EXTRACT(YEAR FROM created_at) < EXTRACT(YEAR FROM CURRENT_DATE)
                // ");
                                                
                    } catch (\Exception $e) {
                        Log::error('GenerarAlerta: Error en proceso de crear notificacion: '.$e->getMessage());
                        //Enviar correo de alerta
                        $proceso = 'Alerta Temprana Utilidades';
                        $mensaje = 'Error en proceso: '.$e->getMessage();
                        Mail::to('soporte@git.com.py')->send(new NotificacionesSistema($proceso, $mensaje));
                    }
                    
                                            
                } catch (\Exception $e) {
                    Log::error('GenerarAlerta: Error en proceso de crear notificacion: '.$e->getMessage());
                    //Enviar correo de alerta
                    $proceso = 'Alerta Temprana Utilidades';
                    $mensaje = 'Error en proceso: '.$e->getMessage();
                    Mail::to('soporte@git.com.py')->send(new NotificacionesSistema($proceso, $mensaje));
                }
     



    }

       
}
