<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Facades\Log;
use App\Empresa;
use App\Ticket;
use App\Mail\NotificacionesSistema;
use App\Mail\NotificacioneAlertaTemprana;


class AlertaTempranaUtilidadesJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
 


            try {
                        //Alerta 1 - Tickets de Oasis con IVA
                        $tickets_oasis = Ticket::where(function($q){
                            $q->where('iva_comision','>',0)
                              ->orWhere('iva','>',0);
                        })->where('id_empresa',21)->get();

                        //Alerta 1 - Tickets de Oasis con IVA
                        $tickets_oasis=  DB::select("SELECT
                            e.denominacion as estado, 
                            c.currency_code,
                            t.numero_amadeus,
                            t.pnr,
                            t.facial,
                            t.total_ticket,
                            t.iva,
                            t.tasas,
                            t.fecha_emision
                            FROM tickets t
                            left join estados e ON e.id = t.id_estado
                            left join currency c ON c.currency_id = t.id_currency_costo
                            WHERE t.id_empresa = 21
                            and (t.iva > 0 or t.iva_comision > 0)
                            and t.id_estado not in (27,33)");
                      
                        //Alerta 2 - Ticket de COPA DTP sin IVA
                        $tickets_copa_sin_iva=  DB::select("SELECT
                            e.denominacion as estado, 
                            c.currency_code,
                            t.numero_amadeus,
                            t.pnr,
                            t.facial,
                            t.total_ticket,
                            t.iva,
                            t.tasas,
                            t.fecha_emision
                            FROM tickets t
                            left join estados e ON e.id = t.id_estado
                            left join currency c ON c.currency_id = t.id_currency_costo
                            WHERE  t.id_empresa = 1
                            and t.tipo = 'copa'
                            and (t.iva <= 0 or t.iva is null)
                            and t.id_estado not in (27,33)");

                        $date = date('Y-m-d');

                        //Tickets del dia
                        $tickets_creados_hoy = DB::select("SELECT
                        t.numero_amadeus,
                        t.pnr,
                        c.currency_code,
                        t.facial,
                        t.total_ticket,
                        t.iva,
                        t.tasas,
                        t.fecha_emision,
                        e.denominacion as empresa
                        FROM tickets t
                        left join empresas e ON e.id = t.id_empresa
                        left join currency c ON c.currency_id = t.id_currency_costo
                        WHERE  t.created_at >= '$date 00:00:00'");

         

                    if(count($tickets_oasis) || count($tickets_copa_sin_iva) || count($tickets_creados_hoy)){
                          //Enviar correo de alerta
                          Mail::to('soporte@git.com.py')->send(new NotificacioneAlertaTemprana($tickets_oasis,$tickets_copa_sin_iva, $tickets_creados_hoy ));
                    }
                      

                    
                                            
                } catch (\Exception $e) {
                    Log::error('GenerarAlerta: Error en proceso de crear notificacion: '.$e->getMessage());
                    //Enviar correo de alerta
                    $proceso = 'Alerta Temprana Utilidades';
                    $mensaje = 'Error en proceso: '.$e->getMessage();
                    Mail::to('soporte@git.com.py')->send(new NotificacionesSistema($proceso, $mensaje));
                }
     



    }

       
}
