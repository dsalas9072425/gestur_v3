<?php

namespace App\Jobs;

use App\HistoricoCierreCaja;
use App\HistoricoCierreCajaDetalle;
use App\Cierre;
use App\CierreResumen;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Mail\NotificacionesSistema;
use App\Mail\NotificacionesHistoricoCierreCaja;
use Illuminate\Support\Facades\Log;

class HistoricoCierreCajaJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $fecha_actual = date('Y-m-d');
        $fecha_ext = date('Y-m-d H:i:s');
        // $fecha_actual = '2023-01-04';
        // $fecha_ext = '2023-01-04 23:59:59';


        try {

                DB::beginTransaction();
                Log::info('HistoricoCierreCajaJob: Inicio de proceso');
               

                           /**
                            * 1. Obtener todos los cierres de caja de la fecha actual
                            * 2. Recorrer cada cierre y guardar en historico_cierre_caja_cabecera
                            * 3. Recorrer cada cierre y guardar en historico_cierre_caja_detalle
                            */   
                        $cierres_resumen = Cierre::where('fecha_cierre',$fecha_actual)->get();
             
                        foreach ($cierres_resumen as $cierre) {
                            $historico = new HistoricoCierreCaja();
                            $historico->id_usuario_cierre       = $cierre->usuario_cierre;
                            $historico->total_gs                = $cierre->total_gs;
                            $historico->total_us                = $cierre->total_us;
                            $historico->id_usuario_anulacion    = $cierre->usuario_anulacion;
                            $historico->fecha_anulacion         = $cierre->fecha_anulacion;
                            $historico->fecha_anulacion         = $cierre->fecha_anulacion;
                            $historico->id_cierre               = $cierre->id;
                            $historico->id_empresa              = $cierre->id_empresa;
        
                            $historico->created_at              = $fecha_ext;
                            $historico->updated_at              = $fecha_ext;
                            $historico->save();



                            $cierre_resumen = CierreResumen::where('id_cierre',$cierre->id)->get();
                            foreach ($cierre_resumen as $resumen) {
                                $historico_detalle = new HistoricoCierreCajaDetalle();
                                $historico_detalle->id_historico_cierre_caja_cabecera = $historico->id;
                                $historico_detalle->forma_cobro_id = $resumen->forma_cobro_id;
                                $historico_detalle->moneda_id = $resumen->moneda_id;
                                $historico_detalle->monto = $resumen->monto;
                                $historico_detalle->save();
                            }
                        }

                        DB::commit();

                             
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('HistoricoCierreCajaJob: Error en proceso: '.$e->getMessage());
                //Enviar correo de alerta
                $proceso = 'HistoricoCierreCajaJob';
                $mensaje = 'Error en proceso: '.$e->getMessage();
                Mail::to('ti@git.com.py')->send(new NotificacionesSistema($proceso, $mensaje));
            }



        Log::info('HistoricoCuentasJob: Fin de proceso');

    }
}
