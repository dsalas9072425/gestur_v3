<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Facades\Log;
use App\Empresa;
use App\Mail\NotificacionesSistema;


class GenerarPdfEjecutivo implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('GenerarPdfEjecutivo: Inicia de proceso');

        /**
         * Al momento de generar las notificaciones de resumen ejecutivo hace una busqueda 
         * en la tabla de destinatarios empresa y por el id empresa y el tipo de notifiacion
         * si no encuentra un correo entonces busca el id del usuario que se utilizo para el insert de la notificacion
         */

            //GEnerar notificacion       
            try {
                        $empresas = Empresa::where('activo',true)->get();
                        foreach ($empresas as $empresa) {
            
                            $id_empresa = $empresa->id;
                            $id_usuario = 1; //GEnerado por el sistema
                            $email_para = null;
                            $email_cc = null;
                            $token = null;
                            $adjunto = null; //Generado en el momento


                            $tiene_destinatarios = DB::table('destinatario_noticaciones')
                            ->where('id_empresa',$id_empresa)
                            ->whereIn('id_tipo_notificaciones',[25,26])
                            ->get();

                            foreach ($tiene_destinatarios as  $value) {
                                $p_arr  =
                                [
                                    $id_usuario,
                                    $email_para,
                                    $email_cc,
                                    $token,
                                    $value->id_tipo_notificaciones,
                                    $adjunto,
                                    $id_empresa
                                ];
                
                                $inserto= DB::select('SELECT public."insert_notificacion"(?,?,?,?,?,?,?)', $p_arr);
                            }           
            
                           
                        }  
                                            
                } catch (\Exception $e) {
                    Log::error('GenerarPdfEjecutivo: Error en proceso de crear notificacion: '.$e->getMessage());
                    //Enviar correo de alerta
                    $proceso = 'GenerarPdfEjecutivo';
                    $mensaje = 'Error en proceso: '.$e->getMessage();
                    Mail::to('ti@git.com.py')->send(new NotificacionesSistema($proceso, $mensaje));
                }
     

       Log::info('GenerarPdfEjecutivo: Finaliza de proceso');

    }

       
}
