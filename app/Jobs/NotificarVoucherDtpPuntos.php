<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Facades\Log;
use App\Empresa;
use App\Mail\NotificacionesSistema;


class NotificarVoucherDtpPuntos implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       Log::info('NotificarVoucherDtpPuntos: Inicia de proceso');

       /**
        * Verificar si hay notificaciones pendientes de envio desde la tabla de liquidacion_dtp_puntos
        */
       $this->correoGanadores();
       $this->correoSaldos();
        


       
       Log::info('NotificarVoucherDtpPuntos: Finaliza de proceso');

    }


    /**
     * Correo informativo a los ganadores de DTP puntos
     */
    public function correoGanadores(){
            try {
                $empresa = Empresa::find(1);
                $empresa_logo = 'https://gestur.git.com.py/logoEmpresa/'.$empresa->logo;	
                $liquidaciones = DB::select("SELECT 
                                        ldp.id,
                                        p.id as id_vendedor,
                                        concat(p.nombre, ' ', p.apellido) as nombre,
                                        mv.meta,
                                        ldp.total_puntos_alcanzados,
                                        ldp.total_cobrado as total_cobrar,
                                        ldp.meta_cumplida,
                                        p.email,
                                        ldp.mes_anho as fecha_corte
                                        FROM liquidacion_dtp_puntos ldp
                                        JOIN meta_vendedores mv ON ldp.id_meta = mv.id
                                        JOIN personas p ON p.id = mv.id_vendedor
                                        WHERE ldp.notificado = false
                                        AND ldp.meta_cumplida = true
                                        ORDER BY ldp.total_cobrado DESC");
            
            
                foreach ($liquidaciones as $liquidacion) {

                    $email = $liquidacion->email;
                    $nombre = $liquidacion->nombre;
                    $fecha_corte = $liquidacion->fecha_corte;
                    DB::table('liquidacion_dtp_puntos')->where('id', $liquidacion->id)->update(['notificado' => true]);

                    try {
                            //Obtener los detalles de la liquidacion, facturas, puntos, etc
                            $detalles = DB::select("SELECT * 
                            from vw_comision_dtp_puntos_detalle
                            WHERE id_liquidacion_dtp_puntos = ? 
                            AND id_vendedor = ?", [$liquidacion->id, $liquidacion->id_vendedor]);
                           
                            $anho = substr($fecha_corte, 2, 4 );
                            $mes = substr($fecha_corte, 0, 2);	
                            $saldo = false; //El correo no es de saldos

                            //Enviar el correo 
                            $pdf = \PDF::loadView('pages.mc.pdfEmails.voucher_dtp_puntos',compact('empresa','empresa_logo','liquidacion','detalles','saldo','anho','mes'));
                            $pdf->setPaper('A4');
                            $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true,'enable_css_float'=>true]);

                            //   Imprimir en la pantalla
                            // $pdf->setPaper('a4', 'letter')->setWarnings(false);
                            // return response($pdf->output(), 200)->header('Content-Type', 'application/pdf');
                            
                            Log::info('Envio de correo => '.$email.' Vendedor : '.$nombre);
                            // $email = 'paquino@git.com.py';
                            Mail::to($email)->send(new \App\Mail\VoucherDtpPuntos($pdf, $mes, $anho, $empresa_logo, $nombre));
                        

                    } catch (\Exception $e) {
                        Log::error('generarVoucherDtpPuntos: Error en envio a email : '.$email.' Vendedor : '.$nombre.' ERROR: '.$e->getMessage());
                    }


                    // dd('listo');
            }


        } catch (\Exception $e) {
            Log::critical('generarVoucherDtpPuntos: Error en envio a email : '.$email.' Vendedor : '.$nombre.' ERROR: '.$e->getMessage());
        }
    }

    /**
     * Correo informativo a los que no ganaron pero tienen un saldo a cobrar
     */
    public function correoSaldos(){
        try {
            $empresa = Empresa::find(1);
            $empresa_logo = 'https://gestur.git.com.py/logoEmpresa/'.$empresa->logo;	
            $liquidaciones = DB::select("SELECT 
                                    ldp.id,
                                    p.id as id_vendedor,
                                    concat(p.nombre, ' ', p.apellido) as nombre,
                                    mv.meta,
                                    ldp.total_puntos_alcanzados,
                                    ldp.total_cobrado as total_cobrar,
                                    ldp.meta_cumplida,
                                    p.email,
                                    ldp.mes_anho as fecha_corte
                                    FROM liquidacion_dtp_puntos ldp
                                    JOIN meta_vendedores mv ON ldp.id_meta = mv.id
                                    JOIN personas p ON p.id = mv.id_vendedor
                                    WHERE ldp.notificado = false
                                    AND ldp.saldo_pendiente > 0.00
                                    ORDER BY ldp.total_cobrado DESC");
        
        
            foreach ($liquidaciones as $liquidacion) {

                $email = $liquidacion->email;
                $nombre = $liquidacion->nombre;
                $fecha_corte = $liquidacion->fecha_corte;
                DB::table('liquidacion_dtp_puntos')->where('id', $liquidacion->id)->update(['notificado' => true]);

                try {
                        //Obtener los detalles de la liquidacion, facturas, puntos, etc
                        $detalles = DB::select("SELECT * 
                        from vw_comision_dtp_puntos_detalle
                        WHERE id_liquidacion_dtp_puntos = ? 
                        AND id_vendedor = ?
                        AND saldo = true", [$liquidacion->id, $liquidacion->id_vendedor]);
                       
                        $anho = substr($fecha_corte, 2, 4 );
                        $mes = substr($fecha_corte, 0, 2);	
                        $saldo = true; //El correo si es de saldos

                        //Enviar el correo 
                        $pdf = \PDF::loadView('pages.mc.pdfEmails.voucher_dtp_puntos',compact('empresa','empresa_logo','liquidacion','detalles','saldo','anho','mes'));
                        $pdf->setPaper('A4');
                        $pdf->setOptions(['isPhpEnabled' => true,'isRemoteEnabled' => true,'enable_css_float'=>true]);

                        // //   Imprimir en la pantalla
                        // $pdf->setPaper('a4', 'letter')->setWarnings(false);
                        // return response($pdf->output(), 200)->header('Content-Type', 'application/pdf');
                        
                        Log::info('Envio de correo => '.$email.' Vendedor : '.$nombre);
                        // $email = 'paquino@git.com.py';
                        Mail::to($email)->send(new \App\Mail\VoucherDtpPuntos($pdf, $mes, $anho, $empresa_logo, $nombre));
                    

                } catch (\Exception $e) {
                    Log::error('generarVoucherDtpPuntos: Error en envio a email : '.$email.' Vendedor : '.$nombre.' ERROR: '.$e->getMessage());
                }


                // dd('listo');
        }


    } catch (\Exception $e) {
        Log::critical('generarVoucherDtpPuntos: Error en envio a email : '.$email.' Vendedor : '.$nombre.' ERROR: '.$e->getMessage());
    }
}

       
}
