<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProformaDetalleReprice extends Model
{
    protected $table = 'proformas_detalle_reprice';
    protected $primarykey = 'id';
    protected $dateFormat = 'Y-m-d H:i:s';

    protected $fillable = [
        'costo_proveedor',
        'precio_costo',
        'cod_confirmacion',
    ];

     public function producto()
    {
        return $this->belongsTo('App\Producto', 'id_producto', 'id');
    }

    public function proveedor()
    {
    	 return $this->belongsTo('App\Persona', 'id_proveedor', 'id');
    }

    public function prestador()
    {
    	 return $this->belongsTo('App\Persona', 'id_prestador', 'id');
    }
    public function proforma()
    {
    	return $this->belongsTo('App\Proforma', 'id_proforma', 'id');
    }
    public function currencyVenta()
    {
        return $this->belongsTo('App\Divisas', 'currency_venta_id', 'currency_id');
    }
     public function currencyCosto()
    {
        return $this->belongsTo('App\Divisas', 'currency_costo_id', 'currency_id');
    }

    public function ticket()
    {
        return $this->hasMany('App\Ticket','id_detalle_proforma','id');
    }
    public function voucher()
    {
        return $this->hasMany('App\Voucher','id_proforma_detalle','id');
    }
 
}
