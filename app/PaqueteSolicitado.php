<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaqueteSolicitado extends Model
{
    protected $table = 'paquetes_solicitados';
    public $timestamps = false;
	protected $primaryKey = 'id';

	public function paquete()
    {
        return $this->belongsTo('App\Paquete','paquete_id','id');
    }

	public function user()
    {
        return $this->belongsTo('App\Usuario','user_id','id_usuario');
    }

}