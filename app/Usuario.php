<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';
    protected $primaryKey = 'id_usuario';
	public $incrementing = false;

	public function agencia()
    {
        return $this->belongsTo('App\Agencias','id_agencia','id_agencia');
    }

	public function sucursal()
    {
        return $this->belongsTo('App\Sucursal','id_sucursal_agencia','id_sucursal_agencia');
    }

    public function perfil()
    {
        return $this->belongsTo('App\Perfil','id_perfil','id_perfil');
    }

	public function setPasswordAttribute($value){
		//dd('se le llamo');
		//$this->attributes['password'] = 'HALO';
		$result = \DB::select('Select f_encrypt_user_password_by_md5(?) as result',array($value));
		//dd($result);
		$this->attributes['password'] = $result[0]->result;
		//dd($result[0]->result);		
		//DB::connection('database')->select('Select dbo.functionName (?,?,?) as result',array(848,95,37));

	}
    
}