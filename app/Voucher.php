<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table = 'vouchers';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';




    public function proforma()
    {
    	return $this->belongsTo('App\Proforma', 'id_proforma', 'id');
    }
    public function proformaDetalle()
    {
    	return $this->belongsTo('App\ProformasDetalle', 'id_proforma_detalle', 'id');
    }
     public function prestador()
    {
        return $this->belongsTo('App\Persona', 'id_prestador', 'id');
    }
    public function producto()
    {
        return $this->hasOne('App\Producto', 'id', 'producto_id');
    }

}