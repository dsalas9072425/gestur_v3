<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposTicket extends Model
{
    protected $table = 'tipos_ticket';
    protected $primaryKey = 'id';  

}