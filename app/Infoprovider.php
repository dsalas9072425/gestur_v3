<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infoprovider extends Model
{
	protected $table = 'infoprovider';
	public $primarykey = 'infoprovider_id';

}
