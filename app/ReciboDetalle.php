<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReciboDetalle extends Model
{
    protected $table = 'recibos_detalle';
    public $timestamps = false;

    public function libroVenta()
    {
        return $this->belongsTo('App\LibroVenta','id_libro_venta','id');
    }
    public function libroCompra()
    {
        return $this->belongsTo('App\LibroCompra','id_libro_compra','id');
    }
	public function anticipo()
    {
        return $this->belongsTo('App\Anticipo','id_anticipo','id');
    }

    public function recibo()
    {
        return $this->hasOne('App\Recibo','id','id_cabecera');
    }


}