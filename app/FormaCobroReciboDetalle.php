<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaCobroReciboDetalle extends Model
{
    protected $table = 'forma_cobro_recibo_detalle';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


    public function fp_cabecera()
    {
      return $this->belongsTo('App\FormaCobroReciboCabecera','id_cabecera');
    }

    public function banco_detalle()
    {
      return $this->belongsTo('App\BancoDetalle','id_banco');
    }

    public function banco()
    {
      return $this->belongsTo('App\BancoDetalle','id_banco_detalle');
    }

    public function forma_cobro()
    {
      return $this->belongsTo('App\FormaCobroCliente','id_tipo_pago');
    }

    public function moneda()
    {
      return $this->hasOne('App\Currency','currency_id','id_moneda');
    }

    
}
