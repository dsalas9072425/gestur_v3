<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaCangoroo extends Model
{
    protected $table = 'reservas_cangoroo';
    protected $primarykey = 'id';
    public $timestamps = false;
}