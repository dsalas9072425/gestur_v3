<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChequeFp extends Model
{
    protected $table = 'fp_cheques';
    protected $primaryKey = 'id'; 
    public $timestamps = false;


}