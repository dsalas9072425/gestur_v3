<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdjuntoDocumento extends Model
{
    protected $table = 'adjuntos_documentos';
    protected $primarykey = 'id';
    public $timestamps = false;
}