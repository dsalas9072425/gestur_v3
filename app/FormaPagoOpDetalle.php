<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaPagoOpDetalle extends Model
{
    protected $table = 'forma_pago_op_detalle';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


    public function fp_cabecera()
    {
      return $this->hasOne('App\FormaPagoOpCabecera','id_cabecera');
    }

    public function banco_detalle()
    {
      return $this->belongsTo('App\BancoDetalle','id_banco_detalle');
    }

    public function forma_pago()
    {
      return $this->belongsTo('App\FormaPagoCliente','id_tipo_pago');
    }
    

    
}
