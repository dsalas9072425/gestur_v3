<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComercioPersona extends Model
{
    protected $table = 'comercio_empresa';
	protected $primarykey = 'id';
}
