<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComisionDetalle extends Model
{
    protected $table = 'escala_comision_detalle';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
   

}