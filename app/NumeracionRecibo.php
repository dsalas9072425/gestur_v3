<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NumeracionRecibo extends Model
{
    protected $table = 'numeracion_recibo';
    protected $primaryKey = 'id'; 
    public $timestamps = false;

    public function sucursal()
    {
        return $this->belongsTo('App\Persona', 'id_sucursal_empresa');
    }

    public function centro_costo()
    {
        return $this->belongsTo('App\CentroCosto', 'id_sucursal_contable');
    }

    


}
