<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $table = 'reservas';
	protected $primarykey = 'id_reserva';
	protected $fillable = ['id_reserva', 'codigo_reserva_proveedor','id_file_reserva'];

	/*
	public function destino()
    {
        return $this->belongsTo('App\DestinationdRecord','id_destino_dtp','id');
    }
	*/
	public function destino()
    {
        return $this->belongsTo('App\DestinoDtpmundo','id_destino_dtp','id_destino_dtpmundo');
    }

	public function agencia()
    {
        return $this->belongsTo('App\Agencias','id_agencia','id_agencia');
    }

	public function usuario()
    {
        return $this->belongsTo('App\Usuario','id_usuario','id_usuario');
    }

    public function agente()
    {
        return $this->belongsTo('App\Usuario','id_usuario','id_usuario');
    }

    public function usuariocancelacion()
    {
        return $this->belongsTo('App\Usuario','id_usuario_cancelacion','id_usuario');
    }

    public function monedaorigen()
    {
        return $this->belongsTo('App\Currency','id_moneda_origen','currency_id');
    }

	/*
    public function ocupantesHabitacion()
    {
        return $this->hasMany('App\OcupantesHabitacion','habitacion','id');
    }
	*/

    public function habitacionesReserva()
    {
		//la tabla PoliticaCancelacion tiene un foreign key (id_reserva) que hace referencia a la key 'id' de la tabla reserva
        return $this->hasMany('App\HabitacionesReserva', 'reserva', 'id');
    }
}
