<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AplicarAnticipo extends Model
{
    protected $table = 'anticipo_aplicacion';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * Get the factura associated with the AplicarAnticipo
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function factura()
    {
        return $this->hasOne(Factura::class, 'id', 'id_factura');
    }

}