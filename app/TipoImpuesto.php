<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoImpuesto extends Model
{
    protected $table = 'tipo_impuesto';
    protected $primaryKey = 'id';
    public $timestamps = false; 
    protected $dateFormat = 'Y-m-d H:i:s';

}