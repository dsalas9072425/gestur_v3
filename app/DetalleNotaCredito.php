<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleNotaCredito extends Model
{
    protected $table = 'nota_credito_detalle';
    protected $primaryKey = 'id'; 
	public $timestamps = false;


}