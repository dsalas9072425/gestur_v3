<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaCabeceraHistorico extends Model
{
    protected $table = 'facturas_reprice';
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';

	public function cliente()
    {
        return $this->belongsTo('App\Persona', 'cliente_id','id');
    }

    public function empresa()
    {
        return $this->belongsTo('App\Empresa', 'id_empresa','id');
    }

    public function destino(){
        return  $this->hasOne('App\DestinoDtpmundo','id_destino_dtpmundo','destino_id');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Persona', 'id_usuario','id');
    }
    public function usuarioFacturacion()
    {
        return $this->hasOne('App\Persona', 'id','usuario_facturacion_id');
    }

    public function asistente()
    {
        return $this->belongsTo('App\Persona', 'id_asistente','id');
    }

    public function vendedorEmpresa()
    {
        return $this->belongsTo('App\Persona', 'vendedor_id','id');
    }

     public function vendedorAgencia()
    {
        return $this->belongsTo('App\Persona', 'id_vendedor_agencia','id');
    }

    public function pasajero()
    {
        return $this->belongsTo('App\Persona', 'id_pasajero_principal','id');
    }

     public function facturaDetalle()
    {
        return $this->hasMany('App\FacturaDetalle', 'id_factura');
    }

     public function tipoFactura()
    {
        return $this->belongsTo('App\TipoFactura', 'id_tipo_factura','id');
    }

     public function proforma()
    {
        return $this->belongsTo('App\Proforma', 'id_proforma','id');
    }

     public function currency()
    {
        return $this->belongsTo('App\Divisas', 'id_moneda_venta','currency_id');
    }

     public function estado()
    {
        return $this->belongsTo('App\EstadoFactour', 'id_estado_factura','id');
    }
    public function estadoCobro()
    {
        return $this->belongsTo('App\EstadoFactour', 'id_estado_cobro','id');
    }

    public function tipoFacturacion()
    {
        return $this->belongsTo('App\TipoFacturacion', 'id_tipo_facturacion','id');
    }

     public function voucher()
    {
        return $this->belongsTo('App\Voucher', 'id_proforma','id_proforma');
    }
    public function timbrado()
   {
       return $this->belongsTo('App\Timbrado', 'id_timbrado','id');
   }
   public function libroventa()
   {
       return $this->hasMany('App\LibroVenta', 'id_documento','id');
   }

    public function ventaRapida()
   {
       return $this->belongsTo('App\VentasRapidasCabecera', 'id_venta_rapida','id');
   }

   public function negocio()
   {
       return $this->belongsTo('App\Negocio', 'id_unidad_negocio','id');
   }

}
