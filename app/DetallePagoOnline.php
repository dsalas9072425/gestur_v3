<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePagoOnline extends Model
{
    protected $table = 'detalle_pagos_online';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';

}
