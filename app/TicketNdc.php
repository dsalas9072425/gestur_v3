<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketNdc extends Model
{
    protected $table = 'tickets_ndc';
    public $timestamps = false;
	protected $primaryKey = 'id';
}