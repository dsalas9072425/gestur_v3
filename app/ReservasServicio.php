<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservasServicio extends Model
{
    protected $table = 'reservas_servicios';
    protected $primaryKey = 'id'; 

	public function agencias()
    {
        return $this->belongsTo('App\Persona','id_agencia','id_agencia');
    }

	public function usuarios()
    {
        return $this->belongsTo('App\Usuario','id_persona','id_usuario');
    }

    public function personas()
    {
        return $this->belongsTo('App\Persona','id_persona','id');
    }

    public function proveedor()
    {
        return $this->belongsTo('App\Persona','id_proveedor','id');
    }


    public function id_agente_dtp()
    {
        return $this->belongsTo('App\Persona','id_agente_dtp','id');
    }

}