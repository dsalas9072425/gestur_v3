<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepositoBancario extends Model
{
    protected $table = 'deposito_bancario';
    protected $primaryKey = 'id';
    public $timestamps = false;




    public function deposito_detalle() 
    {
        return $this->hasMany('App\DepositoBancarioDetalle', 'id_cabecera','id');
    }

    public function banco_detalle()
    {
        return $this->belongsTo('App\BancoDetalle','id_banco_detalle');
    }


    public function moneda()
    {
        return $this->belongsTo('App\Currency', 'id_moneda');
    }


    public function centro_costo()
    {
        return $this->belongsTo('App\CentroCosto', 'id_centro_costo');
    }

    public function sucursal()
    {
        return $this->belongsTo('App\Personas', 'id_sucurusal');
    }



}


