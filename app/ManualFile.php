<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManualFile extends Model
{
    //
    protected $table = 'actualizacion_manual';
    protected $primaryKey = 'id_manual'; 
    protected $fillable = ['fecha_subida', 'usuario_subida', 'archivo_nombre'];
    public $timestamps = false;

}
