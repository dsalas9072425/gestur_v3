<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    protected $table = 'cotizaciones';
    protected $primaryKey = 'id';
    //Para evitar campos de auditorias
    public $timestamps = false;

	//Relacion de Cotizacion con Divisas
	public function moneda()
    {
        return $this->belongsTo('App\Divisas','id_currency','currency_id');
    }

    	public function currency()
    {
        return $this->belongsTo('App\Divisas','id_currency','currency_id');
    }


}


