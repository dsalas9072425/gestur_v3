<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaPaquete extends Model
{
    protected $table = 'categoria_paquete';
}