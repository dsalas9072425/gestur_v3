<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagosOnline extends Model
{
    protected $table = 'pagos_online';
    protected $primaryKey = 'id';  
    public $timestamps = false;

   public function detallePago()
    {
       return $this->hasMany('App\DetallePagoOnline','id_pago_online','id'); 
    }

    public function persona()
    {
       return $this->belongsTo('App\Persona','persona_id','id');
    }
    public function usuario()
    {
       return $this->belongsTo('App\Persona','usuario_id','id');
    }

    public function estado()
    {
       return $this->belongsTo('App\EstadoFactour','estado_id','id');
    }

}