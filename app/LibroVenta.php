<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibroVenta extends Model
{
	protected $table = 'libros_ventas';
	public $primarykey = 'id';
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';

	public function cliente()
    {
        return $this->belongsTo('App\Persona', 'cliente_id','id');
    }
    public function usuario()
    {
        return $this->belongsTo('App\Persona', 'id_usuario','id');
    }

    public function vendedorEmpresa()
    {
        return $this->belongsTo('App\Persona', 'vendedor_id','id');
    }

     public function vendedorAgencia()
    {
        return $this->belongsTo('App\Persona', 'id_vendedor_agencia','id');
    }

    public function pasajero()
    {
        return $this->belongsTo('App\Persona', 'id_pasajero_principal','id');
    }

     public function facturaDetalle()
    {
        return $this->hasMany('App\FacturaDetalle', 'id_factura');
    }

     public function tipoFactura()
    {
        return $this->belongsTo('App\TipoFactura', 'id_tipo_factura','id');
    }

     public function proforma()
    {
        return $this->belongsTo('App\Proforma', 'id_proforma','id');
    }

     public function currency()
    {
        return $this->belongsTo('App\Divisas', 'id_moneda_venta','currency_id');
    }

     public function estado()
    {
        return $this->belongsTo('App\EstadoFactour', 'id_estado_documento','id');
    }
    public function estadoCobro()
    {
        return $this->belongsTo('App\EstadoFactour', 'id_estado_cobro','id');
    }

    public function tipoFacturacion()
    {
        return $this->belongsTo('App\TipoFacturacion', 'id_tipo_facturacion','id');
    }

     public function voucher()
    {
        return $this->belongsTo('App\Voucher', 'id_proforma','id_proforma');
    }
    public function timbrado()
   {
       return $this->belongsTo('App\Timbrado', 'id_timbrado','id');
   }

    public function asiento()
    {
        return $this->belongsTo('App\Asiento', 'id_asiento','id');
    }

    public function tipoDocumento()
    {
        return $this->belongsTo('App\TipoDocumento', 'id_tipo_documento','id');
    }

    public function comercionEmpresa()
    {
        return $this->belongsTo('App\ComercioPersona', 'id_comercio_empresa','id');
    }

    public function timbrado_manual()
    {
        return $this->belongsTo('App\TimbradoLvManual', 'id_timbrado','id');
    }

    //grupos
    public function grupo()
    {
        return $this->belongsTo('App\Grupo', 'id_grupo','id');
    }

    //unidad de negocio
    public function unidadNegocio()
    {
        return $this->belongsTo('App\Negocio', 'id_unidad_negocio','id');
    }




}
