<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentaCorrienteBanco extends Model
{
	protected $table = 'cuenta_corriente_bancos';
	public $primarykey = 'id';
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';


    public function moneda()
    {
        return $this->belongsTo('App\Currency', 'id_moneda','currency_id');
    }
    
    public function banco_detalle()
    {
        return $this->hasOne('App\BancoDetalle','id','id_banco_detalle');
    }

    public function tipo_cuenta()
    {
        return $this->hasOne('App\TipoDocumento','id','id_tipo_cuenta');
    }

 }   