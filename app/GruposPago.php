<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GruposPago extends Model
{
    protected $table = 'grupos_pagos';
	protected $primarykey = 'id';
    public $timestamps = false;
}
