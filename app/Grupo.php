<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $table = 'grupos';
    public $timestamps = false;
	protected $primaryKey = 'id';
	protected $fillable = ['pnr', 'estado_id', 'codigo_bloqueo'];

	public function destino(){
		return $this->belongsTo('App\DestinoDtpmundo');
	}

	public function estado(){
		return $this->belongsTo('App\EstadoFactour');
	}

	public function currency(){
		return $this->belongsTo('App\Currency', 'currency_compra_id');
	}

	public function tipoTicket(){
		return $this->belongsTo('App\TipoTicket', 'tipo_ticket_id');
	}

	public function persona(){
		return $this->belongsTo('App\Persona', 'proveedor_id');
	}

	public function cliente(){
		return $this->belongsTo('App\Persona', 'cliente_id');
	}


	public function tickets(){
		return $this->hasMany('App\Ticket', 'id_grupo');
	}

	public function penalidad(){
		return $this->hasMany('App\Penalidad', 'id_grupo');
	}

	public function pagos(){
		return $this->hasMany('App\GruposPago', 'id_grupo');
	}

	public function senha(){
		return $this->hasMany('App\GruposSenha', 'grupo_id');
	}

	public function usuario(){
		return $this->belongsTo('App\Persona', 'id_usuario');
	}

}