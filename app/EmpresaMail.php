<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpresaMail extends Model
{
    protected $table = 'empresa_mail';
    protected $primaryKey = 'id'; 

   
	public function empresa()
    {
      return $this->belongsTo('App\Empresa','id_empresa','id');
    }

}