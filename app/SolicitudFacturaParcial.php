<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudFacturaParcial extends Model
{
    protected $table = 'proformas_facturacion_parcial';
    protected $primaryKey = 'id'; 
	public $timestamps = false;
	protected $dateFormat = 'Y-m-d H:i:s';

	public function usuario()
    {
        return $this->belongsTo('App\Persona','id_usuario_pedido','id');
    }
	public function cliente()
    {
        return $this->belongsTo('App\Persona','id_persona','id');
    }
    public function currency()
    {
        return  $this->belongsTo('App\Divisas', 'id_moneda', 'currency_id');
    }

    public function estado()
    {
        return  $this->belongsTo('App\EstadoFactour', 'id_estado', 'id');
    }

	public function pasajero()
    {
        return $this->belongsTo('App\Persona','id_pasajero','id');
    }

}