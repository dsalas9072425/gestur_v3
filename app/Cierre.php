<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cierre extends Model
{
    protected $table = 'cierre_caja';
    public $timestamps = false;
	protected $primaryKey = 'cierre_caja';

	public function usuario()
    {
       return $this->belongsTo('App\Persona','usuario_cierre','id');
    }

	public function usuarioAnulacion()
    {
       return $this->belongsTo('App\Persona','usuario_anulacion','id');
    }


}