<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DtpPlusDetalle extends Model
{
    protected $table = 'dtp_plus_detalle';
    public $timestamps = false;


    public function dtplus() {
        return $this->hasOne(DtpPlus::class, 'id', 'dtp_plus_id');
    }


    public function factura()
    {
        return $this->hasOne(Factura::class, 'id', 'id_factura');
    }

    public function nota_credito()
    {
        return $this->hasOne(NotaCredito::class, 'id', 'id_nota_credito');
    }
}