<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timbrado extends Model
{
    protected $table = 'timbrados';
    protected $primaryKey = 'id';

    public function sucursal()
    {
        return $this->belongsTo('App\Persona', 'id_sucursal_empresa');
    }

    public function sucursal_contable()
    {
        return $this->belongsTo('App\Persona', 'id_sucursal_contable');
    }
    
    public function tipo_timbrado()
    {
        return $this->hasOne(TipoTimbrado::class, 'id','id_tipo_timbrado');
    }


}
