<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
  protected $fillable = [
    'denominacion',
    'ruc',
    'telefono',
    'direccion',
    'activo',
    'markup_minimo_venta',
    'venta_minima_incentivo',
    'pagina_web',
    'email',
    'imprimir_detalle_factura',
    'pie_factura_txt',
    'ruc_representante',
    'es_exportador',
    'agente_retentor',
    'dias_vencimiento',
    'tipo_calculo_vencimiento',
    'tipo_empresa',
    'control_de_rentabilidad',
    'controlar_linea_credito',
    'fecha_limite_lc',
    'saldo_pm',
    'head_factura_txt',
    'id_persona',
    'proveedor_ticket',
    'tipo_impresion',
    'representante',
    'pasajero',
    'plantilla',
    'correo_set',
    'ruc_set',
    'autorizacion_set',
    'pasajero_factura',
];    
    protected $table = 'empresas';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';




    public function planSistema()
    {
		return $this->belongsTo('App\PlanSistema', 'id_plan_sistema');
	}

  public function persona()
{
    return $this->hasOne('App\Persona', 'id','id_persona');
}


}