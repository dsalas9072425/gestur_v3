<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanSistema extends Model
{
	protected $table = 'planes_sistema';
    protected $primaryKey = 'id'; 

	public function menus()
    {
		return $this->belongsToMany('App\Menu', 'planes_menu', 'id_plan', 'id_menu')->withPivot('id_tipo_persona');
	}

}