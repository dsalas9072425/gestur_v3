<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlojamientoCangoroo extends Model
{
    protected $table = 'alojamientos_cangoroo';
    protected $primaryKey = 'id';  
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;


}