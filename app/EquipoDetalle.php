<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipoDetalle extends Model
{
    protected $table = 'equipo_detalle';
    protected $primaryKey = 'id'; 
    public $timestamps = false;

    public function vendedor()
    {
        return $this->belongsTo('App\Persona','id_persona','id');
    }


}