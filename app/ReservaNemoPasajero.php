<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaNemoPasajero extends Model
{
    protected $table = 'reservas_nemo_pasajeros';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
}