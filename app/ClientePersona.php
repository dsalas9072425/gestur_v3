<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientePersona extends Model
{
    protected $table = 'personas_clientes';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function vendedor()
    {
        return $this->belongsTo('App\Persona', 'id_persona','id');
    }

}