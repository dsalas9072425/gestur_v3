<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DtpPlus extends Model
{
    protected $table = 'dtp_plus';

	public function usuario()
    {
        return $this->belongsTo('App\Usuario','user_id','id_usuario');
    }

    public function detalles()
    {
        return $this->hasMany(DtpPlusDetalle::class, 'dtp_plus_id', 'id');
    }

    public function vendedor(){
        return $this->belongsTo('App\Persona','user_id','id');
    }

}