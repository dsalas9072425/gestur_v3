<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoAnticipo extends Model
{
    protected $table = 'grupos_anticipo';
    protected $primaryKey = 'id'; 
    public $timestamps = false;
   
	public function formaPago(){
		return $this->belongsTo('App\FormaPagoCliente', 'forma_pago_id');
	}

	public function moneda(){
		return $this->belongsTo('App\Currency', 'moneda_id');
	}

}