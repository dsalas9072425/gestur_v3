<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recibo extends Model
{
    protected $table = 'recibos';
    protected $primaryKey = 'id'; 
    public $timestamps = false;


	public function usuario()
    {
        return $this->belongsTo('App\Persona','id_usuario_creacion','id');    
    }
	public function cliente()
    {
        return $this->belongsTo('App\Persona','id_cliente','id');
    }

	public function moneda()
    {
        return $this->belongsTo('App\Currency','id_moneda','currency_id');
    }

    public function tipo_recibo()
    {
        return $this->belongsTo('App\TipoOperacionRecibo','id_tipo_operacion_recibo');
    }

    public function sucursal()
    {
        return $this->belongsTo('App\Persona','id_sucursal','id');
    }

    public function sucursalEmpresa()
    {
        return $this->belongsTo('App\SucursalEmpresa','id_sucursal','id');
    }


    public function estado()
    {
        return $this->belongsTo('App\EstadoFactour','id_estado','id');
    }

    public function gestor()
    {
        return $this->belongsTo('App\Persona','id_gestor','id');
    }	

    public function detalle()
    {
        return $this->hasMany('App\ReciboDetalle','id_cabecera','id');
    }	

    //ELIMINAR PERO ANTES CAMBIAR EN LOS LUGARES DONDE SE ENCUENTRA
    public function formaCobro()
    {
        return $this->belongsTo('App\FormaCobroReciboCabecera','id','id_recibo');
    }

    public function formaCobroCabecera()
    {
        return $this->hasOne('App\FormaCobroReciboCabecera','id','id_forma_cobro');
    }



    
    public function formaCobroDetalle()
    {
        return $this->hasMany('App\FormaCobroReciboDetalle','id_cabecera','id_forma_cobro');
    }	
    


    public function usuario_generado()
    {
        return $this->belongsTo('App\Persona','id_usuario_creacion','id');
    }    

    public function usuario_cobrado()
    {
        return $this->belongsTo('App\Persona','id_usuario_cobro','id');
    } 

    public function usuario_aplicado()
    {
        return $this->belongsTo('App\Persona','id_usuario_aplicado','id');
    } 

     public function usuario_anulacion()
    {
        return $this->belongsTo('App\Persona','id_usuario_anulacion','id');
    }  

     public function formaCobroReciboCabecera()
    {
        return $this->hasMany('App\FormaCobroRecibo','id_recibo','id');
    }    
    public function reciboAdjunto()
    {
        return $this->hasMany('App\ReciboAdjunto','id_recibo','id');
    }    

}