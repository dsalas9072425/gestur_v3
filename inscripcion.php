<?php
include "config_bancard.php";
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
     
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NRWK7DB');</script>
<!-- End Google Tag Manager -->
	
 <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132789382-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-132789382-1');
      </script>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="keywords" content="Personal, Portfolio, Creative,Design,mordan">
        <meta name="description" content="Taso">
        <meta name="author" content="cosmos-themes">

        <title>TravelHub by DTP</title>

        <!-- favicon -->
        <link href="assets/images/logo/favicon.png" rel="icon" type="image/png">

        <!--Font Awesome css-->
        <link rel="stylesheet"  href="assets/css/fontawesome-all.css">

        <!--Bootstrap css-->
        <link rel="stylesheet"  href="assets/bootstrap/css/bootstrap.css">

        <!--Owl Carousel css-->
        <link rel="stylesheet"  href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet"  href="assets/css/owl.theme.default.min.css">
        <link rel="stylesheet"  href="assets/css/jquery.bxslider.min.css">

        <!--Magnific Popup css-->
        <link rel="stylesheet"  href="assets/css/magnific-popup.css">
        <link rel="stylesheet"  href="assets/css/animate.min.css">
        <link rel="stylesheet"  href="assets/css/progresscircle.css">
        <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">     
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet"> 
        <link rel="stylesheet"  href="assets/css/swiper.css">
        <!--Site Main Style css-->
        <link rel="stylesheet"  href="assets/css/style.css">
        <?php echo $js_bancard_checkout; ?>

        <link rel="stylesheet"  href="assets/css/page.css">
        <link id="colors" rel="stylesheet" href="assets/css/defaults-color.css">
        <style>
            .btn-block {
                display: block;
                width: 80%;
                margin-left: 10%;
                border-color: #686264 !important;
                background-color: #c02c46 !important;
                color: #ffffff !important;
            }          
        </style>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NRWK7DB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="preload">
    <div id="load"></div>
</div>
        <!--Navbar Start-->
        <nav  id="home" class="navbar navbar-expand-lg navbar-dark">
            <div class="container">
                <!-- LOGO -->
                <a class="navbar-brand logo" href="index.html">
                    <img src="assets/images/logo/dummy-image.png" alt="">
                </a>

                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="navbar-collapse collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
                        <!--Nav Links-->
                        <li class="nav-item">
                            <a href="#" class="nav-link active" data-scroll-nav="0" >Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" data-scroll-nav="1" >Actividades</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" data-scroll-nav="2" >El Hotel</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" data-scroll-nav="3">Disertantes</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" data-scroll-nav="4">Precios</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" data-scroll-nav="5">Newsletter</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" data-scroll-nav="6">Contacto</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--Navbar End-->
        <!--Home Section Start-->
        <section id="Particle-ground" class="banner  banner-layer-wave" style="background-image: url('assets/fondos/bg.jpg')" data-stellar-background-ratio=".7" data-scroll-index="0">
            <div class="overlay">
                <div class="container h-100">
                    <div class="row h-100 align-items-center justify-content-center text-center">
              <div class="col-lg-12">
                      <div class="header-app text-center">
                          <h2>TravelHub by DTP</h2>
                          <p>La primera convención para Agentes de Viaje del Paraguay.</p>
                               <div class="header-button">
                                  
                                 
                               </div>
                      </div>
                   </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Home Section End-->
        <section class="features-section pt-100 pb-100" data-scroll-index="1">
            <?php            
                                    
                    include "coneccionDatabase.php";
					
                    $shop_process_id = strval(rand(0,10).date("H").date("i").date("s"));
                    $importe = '200000';
                    $v_amount = $importe.'.00';

                    //$single_buy_url = 'https://vpos.infonet.com.py:8888/vpos/api/0.3/single_buy';

                    $v_token = md5($private_key."".$shop_process_id."".$v_amount."PYG");
                    
                    $data = array( "public_key" => $public_key,
                                "operation"  => array(  "token" => $v_token,
                                                "shop_process_id" => $shop_process_id,
                                                "amount" => $v_amount,
                                                "currency" => "PYG",
                                                "additional_data" => "",
                                                "description" => "Inscripcion a Travel HUB",
                                                "return_url" => 'http://travelhub.com.py/confirmacion.php',
                                                "cancel_url" => 'http://travelhub.com.py/confirmacion.php')
                    );

                    $bancard_request =  json_encode($data);
					
					/*
					$log->write($idXLog, 0, $locate, 'single_buy_url->'.$single_buy_url);
					$log->write($idXLog, 0, $locate, 'public_key->'.$public_key);
					$log->write($idXLog, 0, $locate, 'private_key->'.$private_key);
					$log->write($idXLog, 0, $locate, 'js_bancard_checkout->'.$js_bancard_checkout);
					*/
					
					$ch =  curl_init($single_buy_url); 
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $bancard_request);
					curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					$bancard_response = curl_exec($ch);
					if (curl_error($ch)) {
						$error_msg = curl_error($ch);
						$log->write($idXLog, 0, $locate, 'bancard_response_error->'.$error_msg);
					}
					if($ch!=null) curl_close($ch);


				   $log->write($idXLog, 0, $locate, 'bancard_response->'.$bancard_response);

					$respuesta_bancard = json_decode($bancard_response);
             ?>
         <form role="form" id="register-form"  name="register-form">
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-10"><h1 class="form-title"></i>Formulario de Inscripción</h1></div>
              </div> 
              <br/>
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8"> 
                  <div class="form-group">
                       <div class="input-group">
                       <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                       <input name="numero_documento" id="numero_documento" type="text" class="form-control" placeholder="Nro. de Documento">
                       </div> 
                       <span class="help-block" style="color: #f90808;" id="error_documento"></span>                     
                  </div>
                </div> 
                <div class="col-md-2"></div>  
              </div>   

              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8"> 
                  <div class="form-group">
                       <div class="input-group">
                       <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                       <input name="nombre_apellido" id="nombre_apellido" type="text" class="form-control" placeholder="Nombres y Apellidos">
                       </div> 
                       <span class="help-block" style="color: #f90808;" id="error_nombre"></span>                     
                  </div>
                </div> 
                <div class="col-md-2"></div>  
              </div> 
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8"> 
                  <div class="form-group">
                       <div class="input-group">
                       <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                       <input name="numero_telefono" id="numero_telefono" type="text" class="form-control" placeholder="Teléfono">
                       </div>
                       <span class="help-block" style="color: #f90808;" id="error_numero_telefono"></span>
                  </div>
                </div> 
                <div class="col-md-2"></div>  
              </div>   
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8"> 
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                         <select name="sexo" id="sexo" class="form-control">
                            <option value="0">Sexo</option>
                            <option value="M">Masculino</option>
                            <option value="F">Femenino</option>
                          </select>
                    </div>
                    <span class="help-block" style="color: #f90808;" id="error_tipo_documento"></span>
                  </div>
                </div> 
                <div class="col-md-2"></div>  
              </div>   

              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8"> 
                <div class="form-group">
                     <div class="input-group">
                     <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                     <input name="e_mail" id="e_mail" type="email" class="form-control" placeholder="E-Mail">
                     </div> 
                     <span class="help-block" style="color: #f90808;" id="error_e_mail"></span>                     
                </div>
                </div> 
                <div class="col-md-2"></div>  
              </div> 

              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8"> 
                  <div class="form-group">
                       <div class="input-group">
                       <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                       <input name="agencia" id="agencia" type="text" class="form-control" placeholder="Agencia">
                       </div>
                       <span class="help-block" style="color: #f90808;" id="error_agencia"></span>
                  </div>
                </div> 
                <div class="col-md-2"></div>  
              </div>   
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8"> 
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                         <select name="remera" id="remera" class="form-control">
                            <option value="0">Tamaño de Remera</option>
                            <option value="S">Tamaño S</option>
                            <option value="M">Tamaño M</option>
                            <option value="L">Tamaño L</option>
                            <option value="XL">Tamaño XL</option>
                            <option value="XXL">Tamaño XXL</option>
                          </select>
                    </div>
                    <span class="help-block" style="color: #f90808;" id="error_remera"></span>
                  </div>
                </div> 
                <div class="col-md-2"></div>  
              </div>  

             
              <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8"> 
                 <b>Si desea Factura, por favor completar los siguientes campos:</b>
                  
                  </div> 
                  <div class="col-md-2"></div>  
                </div> 
                <br>   
                <div id="factura">
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8"> 
                    <div class="form-group">
                         <div class="input-group">
                         <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                         <input name="razon" id="razon" type="razon" class="form-control" placeholder="Razon Social">
                         </div> 
                         <span class="help-block" style="color: #f90808;" id="error_razon"></span>                     
                    </div>
                    </div> 
                    <div class="col-md-2"></div>  
                  </div>

                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8"> 
                    <div class="form-group">
                         <div class="input-group">
                         <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                         <input name="ruc" id="ruc" type="ruc" class="form-control" placeholder="RUC">
                         </div> 
                         <span class="help-block" style="color: #f90808;" id="error_ruc"></span>                     
                    </div>
                    </div> 
                    <div class="col-md-2"></div>  
                  </div> 
                </div> 

                 <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8"> 
                         <div class="row">
                              <div class="col-md-4" style="padding-left: 5%;">
                                  <div class="form-group">
                                     <div class="input-group">
                                     <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                        <button type="button" id="btn-info" href="#pagosDiv" data-ancla="pagosDiv" class="taso-btn mt-15 ancla">
                                            <span class="glyphicon glyphicon-log-in"></span> Pago Online
                                        </button>
                                    </div>
                                  </div>                               
                              </div>
                              <div class="col-md-4" style="padding-left: 2%;">
                                  <div class="form-group">
                                     <div class="input-group">
                                     <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                        <button type="button" href="#pagosDiv" data-ancla="pagosDiv" id="btn-bancario" class="taso-btn mt-15 ancla" style="padding-left: 10px;padding-right: 10px;">
                                            <span class="glyphicon glyphicon-log-in"></span>Transferencia Bancaria
                                        </button>
                                    </div>
                                  </div>                               
                              </div>
                              <div class="col-md-4" style="padding-left: 5%;">
                                  <div class="form-group">
                                     <div class="input-group">
                                     <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                        <button type="button" id="btn-efectivo" href="#pagosDiv" data-ancla="pagosDiv" class="taso-btn mt-15 ancla">
                                            <span class="glyphicon glyphicon-log-in"></span>Pago en Efectivo
                                        </button>
                                    </div>
                                  </div>                               
                              </div>  
                          </div> 
                  </div>  
                    <div class="col-md-2"></div>  
                  </div>   
        </form>  
        <div name ="pagosDiv" > 
              <a name="mi-ancla"></a>
              <div class="row" id ="iframe" style="display: none">
                <div class="col-md-2"></div>
                <div class="col-md-8" style="margin-left: 16%;"> 
                    <div class="single-pricing-table single-pricing-center">
                          <div style="height: 100%; width: 80%; margin: auto" id="iframe-container"></div>
                    </div>
                </div> 
                <div class="col-md-2"></div>  
              </div> 

              <div class="row" id ="deposito" style="display: none">
                <div class="col-md-2"></div>
                <div class="col-md-8" style="margin-left: 16%;"> 
                    <div class="single-pricing-table single-pricing-center" style="line-height: 0.5;">
                          <h4>BANCO ITAU</h4><br/>
                          <h5>Desarrollo Turistico Paraguayo S.A</h5><br/>
                          <h5>Ruc: 80001813-3</h5><br/>
                          <h5>Dir:</b> General Bruguez 353 c/ 25 de Mayo.</h5><br/>
                          <h5>Cuenta Corriente Guaraníes de Itaú N°: 996921 </h5><br/>
                          <p><b>Favor, enviar a travelhub@dtp.com.py el comprobante.</p>
                        <br/><br/>
                        <button class="subscribe btn btn-success btn-lg btn-block js-checkout-button js-iframe-button button-customization" id="btnBanco" data-loading_text="Cargando" data-text="Pagar (Gs. 200.000)" type="button">
                          Confirmar Inscripción
                        </button>
                    </div>
                </div> 
                <div class="col-md-2"></div>  
              </div>  

              <div class="row" id ="efectivo" style="display: none">
                <div class="col-md-2"></div>
                <div class="col-md-8" style="margin-left: 16%;"> 
                    <div class="single-pricing-table single-pricing-center" style="line-height: 0.5;">
                      <br/><br/><br/><br/>
                      <h5 style="margin-top: 8%;">Pueden realizar los pagos en nuestras oficinas como también con nuestros cobradores. <br/>Se les
estará entregando un recibo contra dinero y luego les enviaremos la factura digital por correo.<br/></h5>
                      <br/><br/>
                      <button class="subscribe btn btn-success btn-lg btn-block js-checkout-button js-iframe-button button-customization" id="btnEfectivo" data-loading_text="Cargando" data-text="Pagar (Gs. 200.000)" type="button">
                        Confirmar Inscripción
                      </button>
                    </div>
                </div> 
                <div class="col-md-2"></div>  
              </div>  
        </div>       

            </div>   

        </section>
    
      <footer>
        <section class="" style="background-image: url('assets/fondos/contacto.jpg');    background-attachment: fixed;
            background-size: cover;
            background-repeat: no-repeat;">
           <div class="overlay-7 pt-100 pb-100"  data-scroll-index="6">
               <div class="container">
                        <div class="row">
                            <div class="col">
                               <div class="section-title">
                                    <div class="main-title meain-title-bg-white">
                                        <div class="title-main-page">
                                            <h4>Contacto</h4>
                                           <p>Desarrollo Turístico Paraguayo S.A.</p>
                                        </div>
                                    </div>
                                    
                               </div>
                            </div>
                        </div>
                  <div class="row">
                      <div class="col-lg-1">
                      </div>
                      <div class="col-lg-10">
                          <div class="row">
                              <div class="col-md-4">
                                 <div class="contact-info d-flex">
                                     <div class="w-25">
                                         <div class="contact-icon">
                                        <i class="fas fa-phone"></i>
                                         </div>
                                     </div>
                                     <div class="contact-text w-75">
                                         <h2>Teléfono</h2>
                                         <p>(+595) 981 688 316</p>
                                         <p>(+595) 21 221 816</p>
                                     </div>
                                 </div>
                              </div>  
                              <div class="col-md-4"> 
                                 <div class="contact-info d-flex">
                                     <div class="w-25">
                                         <div class="contact-icon">
                                       <i class="far fa-envelope-open"></i>
                                         </div>
                                     </div>
                                     <div class="contact-text w-75">
                                         <h2>Email</h2>
                                         <p>travelhub@dtp.com.py</p>
                                          <!--<p>mkt@dtp.com.py</p -->
                                     </div>
                                 </div>
                              </div>  
                              <div class="col-md-4"> 
                                 <div class="contact-info d-flex mb-0">
                                     <div class="w-25">
                                         <div class="contact-icon">
                                        <i class="fas fa-map-marker-alt"></i>
                                         </div>
                                     </div>
                                     <div class="contact-text w-75">
                                         <h2>Dirección</h2>
                                         <p>General Bruguéz 353, <br> Asunción, Paraguay</p>
                                     </div>
                                 </div>
                              </div>   
                          </div>       
                      </div>
                  </div>
               </div>
           </div>
        </section>
                 <div class="row">
                    <div class="col-lg-12 text-center copy-right-section">
                        <p>© Copyright 2019. DTP TRAVEL GROUP. All Rights Reserved.
                    </div>
                </div>
            </div>
        </footer>
        <!--Jquery js-->
        <script src="assets/js/jquery.js"></script>
        <!--Bootstrap js-->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!--Stellar js-->
        <script src="assets/js/jquery.stellar.js"></script>
        <!--Animated Headline js-->
        <script src="assets/js/animated.headline.js"></script>
        <!--Owl Carousel js-->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!--ScrollIt js-->
        <script src="assets/js/scrollIt.min.js"></script>
        <!--Isotope js-->
        <script src="assets/js/isotope.pkgd.min.js"></script>
        <!--Magnific Popup js-->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!--Particle js-->
        <script src="assets/js/particles.js"></script>
        <script src="assets/js/particle-main.js"></script>
        <script src="assets/js/progresscircle.js"></script>
        <script src="assets/js/swiper.min.js"></script>
        <script src="assets/js/jquery.bxslider.min.js"></script>
        <!--Site Main js-->
        <script src="assets/js/contact.js"></script>
        <script src="assets/js/main.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {

             $("input[name=optradio]").click(function () {    
                if($(this).val() == 'si'){
                    $("#factura").css('display', 'block');
                }else{
                    $("#factura").css('display', 'none');
                }    

              });

              $(".ancla").click(function(evento){
                    //Anulamos la funcionalidad por defecto del evento
                    evento.preventDefault();
                    //Creamos el string del enlace ancla
                    var codigo = "#" + $(this).data("data-ancla");
                    console.log(codigo)
                    //Funcionalidad de scroll lento para el enlace ancla en 3 segundos
                   // $("html,body").animate({scrollTop: $(codigo).offset().top}, 3000);
                  //$("html, body").delay(2000).animate({
                  //    scrollTop: $(codigo).offset().top 
                  //}, 2000);
                  });



                 styles = {
                            "form-background-color": "#001b60",
                            "button-background-color": "#4faed1",
                            "button-text-color": "#fcfcfc",
                            "button-border-color": "#dddddd",
                            "input-background-color": "#fcfcfc",
                            "input-text-color": "#111111",
                            "input-placeholder-color": "#111111"
                        };
                        Bancard.Checkout.createForm('iframe-container', '<?php echo $respuesta_bancard->process_id;?>', styles);
               }) 


            $("#btn-info").click(function(){
                if($("#numero_documento").val() != ""){
                    $("#error_documento").html('');
                    if($("#nombre_apellido").val() != ""){
                        $("#error_nombre").html(''); 
                        if($("#numero_telefono").val() != ""){
                            $("#error_numero_telefono").html('');
                            if($("#sexo").val()  != 0){
                               $("#error_tipo_documento").html('');
                               if($("#e_mail").val()  != ""){
                                    $("#error_e_mail").html(''); 
                                    if($("#agencia").val()  != ""){
                                        $("#error_agencia").html('');
                                        if($("#remera").val()  != 0){
                                                        $("#error_remera").html('');
                                                        console.log($("#register-form").serialize());
                                                        $("#deposito").css('display', 'none');
                                                        $("#efectivo").css('display', 'none');
                                                        $("#iframe").css('display', 'block');
                                                        $.ajax({
                                                                url: "doInscripcion.php",
                                                                type: "GET",
                                                                data: {
                                                                    request_single_buy: <?php echo $bancard_request;?>,
                                                                    fecha_hora_single_buy:'<?php echo date('Y-m-d H:m:i');?>',
                                                                    response_single_buy: <?php echo json_encode($respuesta_bancard);?>,
                                                                    shop_process_id: <?php echo $shop_process_id;?>,
                                                                    numero_documento: $("#numero_documento").val(),
                                                                    nombre_apellido: $("#nombre_apellido").val(),
                                                                    numero_telefono: $("#numero_telefono").val(),
                                                                    e_mail: $("#e_mail").val(),
                                                                    agencia: $("#agencia").val(),
                                                                    remera: $("#remera").val(),
																	                                  sexo: $("#sexo").val(),
                                                                    token: '<?php echo $v_token;?>',
                                                                    ruc: $("#ruc").val(),
                                                                    tipo_pago: 1,
                                                                    razon: $("#razon").val(),
                                                                },
                                                                dataType: "JSON",
                                                                success: function (jsonStr) {
                                                                    //"#result1").html(jsonStr['back_message']);
                                                                }
                                                            });

                                        }else{
                                           $("#error_remera").html('<b style="color:red;">Ingrese el campo<b/>');
                                        }                
                                    }else{
                                        $("#error_agencia").html('<b style="color:red;">Ingrese el campo<b/>');    
                                        }                    
                                }else{
                                    $("#error_e_mail").html('<b style="color:red;">Ingrese el campo<b/>');        
                                    }                        
                            }else{
                                 $("#error_tipo_documento").html('<b style="color:red;">Ingrese el campo<b/>');           
                                }                            
                        }else{
                             $("#error_numero_telefono").html('<b style="color:red;">Ingrese el campo<b/>');               
                            }                                
                    }else{
                        $("#error_nombre").html('<b style="color:red;">Ingrese el campo<b/>');                    
                        }                                    
                }else{
                     $("#error_documento").html('<b style="color:red;">Ingrese el campo<b/>');                       
                    }                                       
            })

            $("#btnBanco").click(function(){
                $.ajax({
                       url: "doInscripcion.php",
                       type: "GET",
                       data: {
                              request_single_buy: <?php echo $bancard_request;?>,
                              fecha_hora_single_buy:'<?php echo date('Y-m-d H:m:i');?>',
                              response_single_buy: <?php echo json_encode($respuesta_bancard);?>,
                              shop_process_id: <?php echo $shop_process_id;?>,
                              numero_documento: $("#numero_documento").val(),
                              nombre_apellido: $("#nombre_apellido").val(),
                              numero_telefono: $("#numero_telefono").val(),
                              e_mail: $("#e_mail").val(),
                              agencia: $("#agencia").val(),
                              remera: $("#remera").val(),
                              sexo: $("#sexo").val(),
                              token: '<?php echo $v_token;?>',
                              ruc: $("#ruc").val(),
                              tipo_pago: 3,
                              razon: $("#razon").val(),
                            },
                      dataType: "JSON",
                        success: function (jsonStr) {
                                                    
                                                    }
                        });
                window.location.href = './confirmacion.php?status=ok_transfer';
             })
             

            $("#btn-bancario").click(function(){
               if($("#numero_documento").val() != ""){
                    $("#error_documento").html('');
                    if($("#nombre_apellido").val() != ""){
                        $("#error_nombre").html(''); 
                        if($("#numero_telefono").val() != ""){
                            $("#error_numero_telefono").html('');
                            if($("#sexo").val()  != 0){
                               $("#error_tipo_documento").html('');
                               if($("#e_mail").val()  != ""){
                                    $("#error_e_mail").html(''); 
                                    if($("#agencia").val()  != ""){
                                        $("#error_agencia").html('');
                                        if($("#remera").val()  != 0){
                                                        $("#error_remera").html('');
                                                        console.log($("#register-form").serialize());
                                                        $("#iframe").css('display', 'none');
                                                        $("#efectivo").css('display', 'none');
                                                        $("#deposito").css('display', 'block');

                                       }else{
                                           $("#error_remera").html('<b style="color:red;">Ingrese el campo<b/>');
                                        }                
                                    }else{
                                        $("#error_agencia").html('<b style="color:red;">Ingrese el campo<b/>');    
                                        }                    
                                }else{
                                    $("#error_e_mail").html('<b style="color:red;">Ingrese el campo<b/>');        
                                    }                        
                            }else{
                                 $("#error_tipo_documento").html('<b style="color:red;">Ingrese el campo<b/>');           
                                }                            
                        }else{
                             $("#error_numero_telefono").html('<b style="color:red;">Ingrese el campo<b/>');               
                            }                                
                    }else{
                        $("#error_nombre").html('<b style="color:red;">Ingrese el campo<b/>');                    
                        }                                    
                }else{
                     $("#error_documento").html('<b style="color:red;">Ingrese el campo<b/>');                       
                    }                                       

            })

            $("#btnEfectivo").click(function(){
                $.ajax({
                       url: "doInscripcion.php",
                       type: "GET",
                       data: {
                              request_single_buy: <?php echo $bancard_request;?>,
                              fecha_hora_single_buy:'<?php echo date('Y-m-d H:m:i');?>',
                              response_single_buy: <?php echo json_encode($respuesta_bancard);?>,
                              shop_process_id: <?php echo $shop_process_id;?>,
                              numero_documento: $("#numero_documento").val(),
                              nombre_apellido: $("#nombre_apellido").val(),
                              numero_telefono: $("#numero_telefono").val(),
                              e_mail: $("#e_mail").val(),
                              agencia: $("#agencia").val(),
                              remera: $("#remera").val(),
                              sexo: $("#sexo").val(),
                              token: '<?php echo $v_token;?>',
                              ruc: $("#ruc").val(),
                              tipo_pago: 2,
                              razon: $("#razon").val(),
                            },
                      dataType: "JSON",
                        success: function (jsonStr) {
				//console.log(jsonStr);
                                                    }
                        });
                    window.location.href = './confirmacion.php?status=ok_efectivo';

             })
             

            $("#btn-efectivo").click(function(){
                if($("#numero_documento").val() != ""){
                    $("#error_documento").html('');
                    if($("#nombre_apellido").val() != ""){
                        $("#error_nombre").html(''); 
                        if($("#numero_telefono").val() != ""){
                            $("#error_numero_telefono").html('');
                            if($("#sexo").val()  != 0){
                               $("#error_tipo_documento").html('');
                               if($("#e_mail").val()  != ""){
                                    $("#error_e_mail").html(''); 
                                    if($("#agencia").val()  != ""){
                                        $("#error_agencia").html('');
                                        if($("#remera").val()  != 0){
                                                        $("#error_remera").html('');
                                                        $("#iframe").css('display', 'none');
                                                        $("#deposito").css('display', 'none');
                                                        $("#efectivo").css('display', 'block');

                                        }else{
                                           $("#error_remera").html('<b style="color:red;">Ingrese el campo<b/>');
                                        }                
                                    }else{
                                        $("#error_agencia").html('<b style="color:red;">Ingrese el campo<b/>');    
                                        }                    
                                }else{
                                    $("#error_e_mail").html('<b style="color:red;">Ingrese el campo<b/>');        
                                    }                        
                            }else{
                                 $("#error_tipo_documento").html('<b style="color:red;">Ingrese el campo<b/>');           
                                }                            
                        }else{
                             $("#error_numero_telefono").html('<b style="color:red;">Ingrese el campo<b/>');               
                            }                                
                    }else{
                        $("#error_nombre").html('<b style="color:red;">Ingrese el campo<b/>');                    
                        }                                    
                }else{
                     $("#error_documento").html('<b style="color:red;">Ingrese el campo<b/>');                       
                    }                                     

            })


        </script>  
</body>
</html>
