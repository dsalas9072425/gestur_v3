
<?php
        $agencia = $_GET['base'];
        $resultadoBase=explode("|", $agencia);

        $agencias = $resultadoBase[1];
        $paquete = $resultadoBase[0];
        //$img1 = "https://www.dtpmundo.com/uploads/".$paquete;
        $img1 = "http://localhost/public/uploads/".$paquete;
        //$img2 = "http://localhost/public/agencias/".$agencias.".png";
        $img2 = "https://www.dtpmundo.com/agencias/".$agencias.".png";

        // Creamos las dos imágenes a utilizar
        $imagen1 = imagecreatefrompng($img1);
        $imagen2 = imagecreatefrompng($img2);

        // Creamos imagen destino
        $imagen3 = imagecreatetruecolor(568, 851);

        // Dibujamos un rectangulo lleno de color verde,
        // que sera nuestro color transparente
        imagecolortransparent($imagen3, 0x00FF00);
        //imagefilledrectangle($imagen3, 0, 0, 200, 150, 0x00FF00);

        // Copiamos una de las imágenes sobre la otra
        imagecopy($imagen1,$imagen2,430,775,0,0,70,70);

        // Copiamos las imagenes pegadas sobre la imagen destino
        imagecopy($imagen3, $imagen1, 0, 0, 0, 0, 950,1280);

        header("Content-type: image/png");
        // Damos salida a la imagen final
        imagepng($imagen3);

        // Destruimos todas las imágenes
        imagedestroy($imagen1);
        imagedestroy($imagen2);
        imagedestroy($imagen3);
?>

