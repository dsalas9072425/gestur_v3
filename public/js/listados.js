 $('#destinations_name_conocer1').autocomplete({
                source: function (request, response) {
                  $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value.desDestino,
                                value: value.desDestino,
                                valor: value.idDestino
                            };
                        }));
                    });
                },
                minLength: 3,
                delay: 100,
                maxShowItems: 7,
                select: function( event, ui ) {
                    $('#destino_conocer1').val(ui.item.valor);
                  }
});

$('#destinations_name_conocer2').autocomplete({
                source: function (request, response) {
                  $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value.desDestino,
                                value: value.desDestino,
                                valor: value.idDestino
                            };
                        }));
                    });
                },
                minLength: 3,
                delay: 100,
                maxShowItems: 7,
                select: function( event, ui ) {
                    $('#destino_conocer2').val(ui.item.valor);
                  }
});

$('#destinations_name_conocer3').autocomplete({
                source: function (request, response) {
                  $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value.desDestino,
                                value: value.desDestino,
                                valor: value.idDestino
                            };
                        }));
                    });
                },
                minLength: 3,
                delay: 100,
                maxShowItems: 7,
                select: function( event, ui ) {
                    $('#destino_conocer3').val(ui.item.valor);
                  }
});


$('#destinations_name_venta1').autocomplete({
                source: function (request, response) {
                  $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value.desDestino,
                                value: value.desDestino,
                                valor: value.idDestino
                            };
                        }));
                    });
                },
                minLength: 3,
                delay: 100,
                maxShowItems: 7,
                select: function( event, ui ) {
                    $('#destino_venta1').val(ui.item.valor);
                  }
});

$('#destinations_name_venta2').autocomplete({
                source: function (request, response) {
                  $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value.desDestino,
                                value: value.desDestino,
                                valor: value.idDestino
                            };
                        }));
                    });
                },
                minLength: 3,
                delay: 100,
                maxShowItems: 7,
                select: function( event, ui ) {
                    $('#destino_venta2').val(ui.item.valor);
                  }
});

$('#destinations_name_venta3').autocomplete({
                source: function (request, response) {
                  $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value.desDestino,
                                value: value.desDestino,
                                valor: value.idDestino
                            };
                        }));
                    });
                },
                minLength: 3,
                delay: 100,
                maxShowItems: 7,
                select: function( event, ui ) {
                    $('#destino_venta3').val(ui.item.valor);
                  }
});
