var lat = -34.397
var lon = 150.644;
var map;
var labelIndex = 0;
var markers = [];
var markk = [];
var counter = 1;
var counters = 1;

$(document).ready(function() {
	objBusqueda.crear();
	// Búsqueda Avanzada
	$("#busqueda-avanzada").on("hide.bs.collapse", function(){
		$(".ba").html('Búsqueda Avanzada <i class="fa fa-angle-down" aria-hidden="true"></i>');
	});
	$("#busqueda-avanzada").on("show.bs.collapse", function(){
		$(".ba").html('Cerrar Búsqueda Avanzada <i class="fa fa-angle-up" aria-hidden="true"></i>');
	});
	$( "#frmBusqueda" ).submit(function(event) {
		if($("#destino").val() != ""){
			var ocupancia = $("<input>").attr("type", "hidden").attr("name", "ocupancia").val(objBusqueda.getHabitaciones());
			$('#frmBusqueda').append($(ocupancia));
			$( "select[data-selected]" ).each(function() {
				});
			objBusqueda.habitaciones.storeRoomsContainer();
			//console.log(window.sessionStorage.getItem("roomsContainer"));
		}else{
			msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i> Seleccione al menos un destino</h2><p><br><button id="aceptar" style="width: 40%;" class="btn btn-primary">Aceptar</button></p></div>';
			mostrarMensaje(msg);
			event.preventDefault();
		}	
	});
	$(".group-result").attr('background-color', 'darkgrey');

	$(".tipo_tabs").click(function(event){
		$( "#tipo").val($(this).attr('id'));
	})	

	$("#resultado").click(function(event){
		pasajeros = "{adultos :"+$("#cantidad_adultos").val()+",ninhos :"+$("#cantidad_ninhos").val()+",infantes :"+$("#cantidad_infantes").val()+"}";
		var arrayPasajeros = $("<input>").attr("type", "hidden").attr("name", "referenciaPasajeros").val(pasajeros);
		$('#frmBusquedaFligth').append($(arrayPasajeros));	
	})	

});
$('input[name="radio-tab"]').click(function () {
	//jQuery handles UI toggling correctly when we apply "data-target" attributes and call .tab('show') 
	//on the <li> elements' immediate children, e.g the <label> elements:
	$(this).closest('label').tab('show');
});

$('input[name="radio-tab-traslado"]').click(function () {
	 //jQuery handles UI toggling correctly when we apply "data-target" attributes and call .tab('show') 
	//on the <li> elements' immediate children, e.g the <label> elements:
	$(this).closest('label').tab('show');
});
var map, map2;

//Se inicialización de los mapas
/*function initialize(condition){	
			var myOptions = {
		        zoom: 14,
		        center: new google.maps.LatLng(0.0, 0.0),
		        mapTypeId: google.maps.MapTypeId.ROADMAP,
		    }
		    var infowindow = new google.maps.InfoWindow({
              content: "placeholder"
            });

		    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		    map2 = new google.maps.Map(document.getElementById("map_canvas2"), myOptions);

		    var infoWindow = new google.maps.InfoWindow({map: map});

			bangalore = {lat: lat, lng: lon};
			// Try HTML5 geolocation.
	        if (navigator.geolocation){
	          	navigator.geolocation.getCurrentPosition(function(position){
		            var pos = {
		              lat: position.coords.latitude,
		              lng: position.coords.longitude
		            };
		            map.setCenter(pos);
		            map2.setCenter(pos);
	          	}, function() {
		            handleLocationError(true, map.getCenter());
		            handleLocationError(true, map2.getCenter());
	          });
	        } else {
	          // Browser doesn't support Geolocation
	          handleLocationError(false, infoWindow, map.getCenter());
	          handleLocationError(false, infoWindow, map2.getCenter());
	        }

	        // Llamada para agregar un marcador al mapa desde
	        google.maps.event.addListener(map, 'click', function(event) {
	          	if(counter ==1){
		        	addMarker(event.latLng, map, counter);
	          	}	
		        counter++
	        });

	        // Llamada para agregar un marcador al mapa hasta
	        google.maps.event.addListener(map2, 'click', function(event) {
	          	if(counters ==1){
		        	addMarkers(event.latLng, map2, counters);
	          	}	
		        counters++
	        });

			google.maps.event.trigger(map, 'resize');

	        // Add a marker at the center of the map.
	        addMarker(bangalore, map);
	        addMarkers(bangalore, map2);
	      }
	      //Genera el mapa en el modal Desde
	      function recrearMapaDesde(){
	      		$('#mapaCoordenadasDesde').modal({
					    }).on('shown.bs.modal', function () {
					        google.maps.event.trigger(map, 'resize');
					    });
	      }
	      //Genear el mapa en el modal Hasta
	      function recrearMapaHasta(){
	      		$('#mapaCoordenadasHasta').modal({
					    }).on('shown.bs.modal', function () {
					        google.maps.event.trigger(map2, 'resize');
					    });
	      }

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	//infoWindow.setPosition(pos);
	/*infoWindow.setContent(browserHasGeolocation ?
	                        'Error: The Geolocation service failed.' :
	                        'Error: Your browser doesn\'t support geolocation.');
}	

//Agrega las marcas al mapa y toma las coordenadas
function addMarker(location, map, counter) {
	var marker = new google.maps.Marker({
								        position: location,
								        label: "A",
								        map: map
		   								});
	markers.push(marker);
	//se discriminan la longitud y latitud
	var markerLatLng = marker.getPosition();
	var latlng = markerLatLng.lat()+","+markerLatLng.lng();
	if(counter == 1){
		$(".coordenadaDesde").attr("value", latlng);
	}
	codeLatLng(latlng,1);
}

//Agrega las marcas al mapa y toma las coordenadas
function addMarkers(location, map2,counters) {
	var mark = new google.maps.Marker({
								        position: location,
								        label: "B",
								        map: map2
		   								});
	markk.push(mark);
	//se discriminan la longitud y latitud
	var markersLatLng = mark.getPosition();
	var latlngs = markersLatLng.lat()+","+markersLatLng.lng();
	if(counters == 1){
		$(".coordenadaHasta").attr("value", latlngs);
	}
	codeLatLng(latlngs,2);
}

// Se agrega la dirección del marcador  
function codeLatLng(latlng, counter) {
	var geocoder = new google.maps.Geocoder(); ;
	var input = latlng;
	var latlngStr = input.split(',', 2);
	var lat = parseFloat(latlngStr[0]);
	var lng = parseFloat(latlngStr[1]);
	var latlng = new google.maps.LatLng(lat, lng);
	geocoder.geocode({'latLng': latlng}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) {
				if(counter <= 1){
					$(".direccionDesde").attr("value", results[0].formatted_address);
				}else{
					$(".direccionHasta").attr("value", results[0].formatted_address);
				}
			} else {
			    alert('No results found');
			}
		} else {
			alert('Geocoder failed due to: ' + status);
		}
	});
}

// Eliminar las marcas del mapa desde
function deleteMarkers(map) {
	for (var i = 0; i < markers.length; i++) {
	    markers[i].setMap(map);
	}
	$(".coordenadaDesde").attr("value", "");
	$(".direccionDesde").attr("value", "");
	counter= 1;
}

// Eliminar las marcas del mapa hasta
function deleteMarker(map2) {
	for (var i = 0; i < markk.length; i++) {
	    markk[i].setMap(map2);
	}
	$(".coordenadaHasta").attr("value", "");
	$(".direccionHasta").attr("value", "");
	counters= 1;
}

function armarTramo(indice){
				inidiceBase = parseInt(indice) + 1;
				indiceNombre = parseInt(inidiceBase) + 1;
				//$("#divMultidestinos_0").clone(true).appendTo("#Resultado");
				var contenio= 	'<div id="divMultidestinos_'+inidiceBase+'" class="multidestinos">'+
								'<div class ="row">'+
		                        '<div class="col-md-1">'+
		                        '<h3 class="textblue">Tramo '+indiceNombre+'</h3>'+
		                        '</div>'+
		                        '<div class="col-md-4">'+
				                '<div class="form-group">'+
				                '<label class="title">Origen</label>'+
				                '<select data-placeholder="Origen" class="chosen-select-deselect" id="origen" name="origen">'+
				                '<option value=""></option>'+
								'<option value="asuncion">Asunción</option>'+
								'</select>'+
				                '</div>'+
				                '</div>'+
				                '<div class="col-md-4">'+
				                '<div class="form-group">'+
				                '<label class="title">Destino</label>'+
				                '<select data-placeholder="Destino" class="chosen-select-deselect" id="destino" name="destino">'+
				                '<option value=""></option>'+
								'<option value="asuncion">Asunción</option>'+
								'</select>'+
				                '</div>'+
				                '</div>'+
				                '<div class="col-xs-2 col-sm-2 col-md-3">'+
				                '<div class="form-group">'+
				                '<label class="title">SALIDA</label>'+
				                '<div class="datepicker-wrap">'+
				                '<input type="text" id="fecha_ida_multidestino" name="fecha_ida_multidestino" class="input-text full-width required" value = ""/>'+
				                '</div>'+
				                '</div>'+
				                '</div>'+
				                '</div>'+
				                '<div class ="row">'+
				                '<div class="col-xs-6 col-sm-2 col-md-2">'+
				                '<label class="title cantPasajeros">Adultos</label>'+            
					            '<div class="input-group form-group">'+
					            '<div class="input-group-btn">'+
								'<button type="button" id="disminuir" class="btn btn-default"><i class="glyphicon glyphicon-minus-sign"></i></button>'+
								'</div>'+
								'<input name="cantidad_adultos" id="cantidad_adultos" class="form-control" value="1" readonly="readonly" />'+
								'<div class="input-group-btn">'+
								'<button type="button" id="aumentar" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i></button>'+
								'</div>'+
								'</div>'+
								'</div>'+
								'<div class="col-xs-6 col-sm-2 col-md-2">'+
								'<label class="title cantPasajeros">Niños (2/11 AÑOS)</label>'+
								'<div class="input-group form-group">'+
								'<div class="input-group-btn">'+
								'<button type="button" id="disminuir-n" class="btn btn-default"><i class="glyphicon glyphicon-minus-sign"></i></button>'+
								'</div>'+
								'<input name="cantidad_ninhos" id="cantidad_ninhos" class="form-control" value="0" readonly="readonly"  />'+
								'<div class="input-group-btn">'+
								'<button type="button" id="aumentar-n" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i></button>'+
								'</div>'+
								'</div>'+
								'</div>'+
								'<div class="col-xs-6 col-sm-2 col-md-2">'+
								'<label class="title cantPasajeros">Infantes (0/2 AÑOS)</label>'+
								'<div class="input-group form-group">'+
								'<div class="input-group-btn">'+
								'<button type="button" id="disminuir-i" class="btn btn-default"><i class="glyphicon glyphicon-minus-sign"></i></button>'+
								'</div>'+
								'<input name="cantidad_infantes" id="cantidad_infantes" class="form-control" value="0" readonly="readonly"  />'+
								'<div class="input-group-btn">'+
								'<button type="button" id="aumentar-i" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i></button>'+
								'</div>'+
								'</div>'+
				                '</div>'+
				                '<div class="col-xs-8 col-sm-3 col-md-2">'+
				                '<label class="title">&nbsp;</label>'+
				                '<button type="button" id ="agregar_0" class="btn btn-success agregarTramo"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Tramo</button>'+
				                '</div>'+
				                '<div class="col-xs-8 col-sm-3 col-md-2">'+
				                '<label class="title">&nbsp;</label>'+
				                '<button type="button" id ="delete_0" class="btn btn-info eliminarTramo"><i class="fa fa-minus-circle" aria-hidden="true"></i> Eliminar Tramo</button>'+
				                '</div>'+
				                '<div class="col-xs-4 col-sm-1 col-md-2">'+
				                '<label class="title">&nbsp;</label>'+
				                '</div>'+
				                '</div>'+
		                        '</div>';
				console.log(contenio);
				$("#Resultado").append(contenio);
}*/