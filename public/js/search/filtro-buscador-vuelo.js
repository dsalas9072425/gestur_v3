/* #################################################################
								VUELOS
   ################################################################# */


$(document).ready(function() {

    // ******* Slider de Precios **********

 /*   $( "#price-rang" ).slider({
        range: true,
        min: 500,
        max: 5000,
        values: [ 850, 3500],
        slide: function( event, ui ) {
            $(".min-price-label").html( ui.values[ 0 ]);
            $(".max-price-label").html( ui.values[ 1 ]);

            $( "#precio_minimo" ).val( ui.values[ 0 ] );
            $( "#precio_maximo" ).val( ui.values[ 1 ] );
        }
    });
    $( "#precio_minimo" ).val($( "#price-rang" ).slider( "values", 0 ) );

    $( "#precio_maximo" ).val($( "#price-rang" ).slider( "values", 1 ) );*/

    // Fin Slider de Precios

    // ******* Slider de Salida **********

    function convertTimeToHHMM(t) {

        var minutes = t % 60;

        var hour = (t - minutes) / 60;

        var timeStr = (hour + "").lpad("0", 2) + ":" + (minutes + "").lpad("0", 2);

        var date = new Date("2019/01/01 " + timeStr + ":00");

        var hhmm = date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'});

        return hhmm;

    }

    $("#salida-slider").slider({

        range: true,
        min: 0,
        max: 1440,
        step: 5,

        values: [ 0, 1440 ],

        slide: function( event, ui ) {

            
            $(".start-salida-label").html( convertTimeToHHMM(ui.values[0]) );

            $(".end-salida-label").html( convertTimeToHHMM(ui.values[1]) );

        }

    });

    $(".start-salida-label").html( convertTimeToHHMM($("#salida-slider").slider( "values", 0 )) );

    $(".end-salida-label").html( convertTimeToHHMM($("#salida-slider").slider( "values", 1 )) );

    // Fin Slider de Salida

    // ******* Slider de Llegada **********

    function convertTimeToHHMM(t) {

        var minutes = t % 60;

        var hour = (t - minutes) / 60;

        var timeStr = (hour + "").lpad("0", 2) + ":" + (minutes + "").lpad("0", 2);

        var date = new Date("2019/01/01 " + timeStr + ":00");

        var hhmm = date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'});

        return hhmm;

    }

    $("#llegada-slider").slider({

        range: true,
        min: 0,
        max: 1440,
        step: 5,

        values: [ 0, 1440 ],

        slide: function( event, ui ) {

            
            $(".start-llegada-label").html( convertTimeToHHMM(ui.values[0]) );

            $(".end-llegada-label").html( convertTimeToHHMM(ui.values[1]) );

        }

    });

    $(".start-llegada-label").html( convertTimeToHHMM($("#llegada-slider").slider( "values", 0 )) );

    $(".end-llegada-label").html( convertTimeToHHMM($("#llegada-slider").slider( "values", 1 )) );

    // Fin Slider de Llegada

});

// ******* Chosen **************

$(".chosen").chosen();

var config = {
    '.chosen-select'           : {},
    '.chosen-select-deselect'  : {allow_single_deselect:true},
    '.chosen-select-no-single' : {disable_search_threshold:10},
    '.chosen-select-no-results': {no_results_text:'Sin resultados!'},
    '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
    $(selector).chosen(config[selector]);
}


 /* Aumentar / Disminuir cantidad de pasajeros */

    /* Adultos */
    $('#disminuir').click(function(){
        //Solo si el valor del campo es diferente de 0

        if ($('#cantidad_adultos').val() != 0)
        {
            console.log($('#cantidad_adultos').val());
            //Decrementamos su valor
            $('#cantidad_adultos').val(parseInt($('#cantidad_adultos').val()) - 1);
            console.log($('#cantidad_adultos').val());
        }

    });

    $('#aumentar').click(function(){
        //Aumentamos el valor del campo
        if ($('#cantidad_adultos').val() != 9)
        {

            $('#cantidad_adultos').val(parseInt($('#cantidad_adultos').val()) + 1);
        }
    });


   /* Niños */

    $('#disminuir-n').click(function(){
        //Solo si el valor del campo es diferente de 0

        if ($('#cantidadN').val() != 0)
            console.log('Menos');
            //Decrementamos su valor
            $('#cantidadN').val(parseInt($('#cantidadN').val()) - 1);

    });

    $('#aumentarN').click(function(){
        //Aumentamos el valor del campo
        console.log('Mas');
        if ($('#cantidadN').val() <= 4)
        {
            $('#cantidadN').val(parseInt($('#cantidadN').val()) + 1);
        }
    });

    /* Infantes */

    $('#disminuir-i').click(function(){
        //Solo si el valor del campo es diferente de 0

        if ($('#cantidad_infantes').val() != 0)
            //Decrementamos su valor
            $('#cantidad_infantes').val(parseInt($('#cantidad_infantes').val()) - 1);

    });

    $('#aumentar-i').click(function(){
        //Aumentamos el valor del campo
        if ($('#cantidad_infantes').val() != 2 && $('#cantidad_adultos').val() > $('#cantidad_infantes').val())
        {
            $('#cantidad_infantes').val(parseInt($('#cantidad_infantes').val()) + 1); 
        }
    });


    /* Fin Aumentar y Disminuir Cantidad de Adultos y Niños */


    $('#fecha_ida_vuelo').datepicker({
        minDate: 0,
        dateFormat: 'dd/mm/yy',
        onSelect: function(dateText) {
            $('#fecha_vuelta_vuelo').datepicker('option','minDate',addDays($('#fecha_ida_vuelo').datepicker('getDate'),1));
            updateNochesFligth();
        }
    });
    $('#fecha_ida_vuelo').datepicker("setDate", new Date());
    
    $('#fecha_vuelta_vuelo').datepicker({
        minDate: 1,
        dateFormat: 'dd/mm/yy',
        onSelect: function(){
            updateNochesFligth();
        }
    });
    $('#fecha_vuelta_vuelo').datepicker("setDate", new Date());
      updateNochesFligth();

    $('#cant_nochesFligthF').keyup(function(){
        $('#fecha_vuelta_vuelo').datepicker('setDate',addDays($('#fecha_ida_vuelo').datepicker('getDate'),parseInt($('#cant_nochesFligthF').val())));
    });


    function updateNochesFligth(){
        $('#cant_nochesFligthF').val(dateDiffInDays($('#fecha_ida_vuelo').datepicker('getDate'),$('#fecha_vuelta_vuelo').datepicker('getDate')));
        if($('#cant_nochesFligthF').val() >= 361){
            $('#cant_nochesFligthF').val(1)
            $('#cant_nochesFligthF').trigger('keyup');
            msg = '<div class="loading"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i>La cantidad de dias no puede superar 361 dias</h2><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
            mostrarMensaje(msg);
            event.preventDefault();
        }
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }


    var _MS_PER_DAY = 1000 * 60 * 60 * 24;

    // a and b are javascript Date objects
    function dateDiffInDays(a, b) {
        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

      return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }