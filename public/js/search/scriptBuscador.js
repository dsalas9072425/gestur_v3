$(function(){
	objBusqueda = (function(){
		var habitaciones = [];
		var roomsContainer = document.getElementById("formHabitaciones");
		var numRooms = 0;
		var addRoomBtn = document.getElementById("agregarHabitacion");
		var remRoomBtn = document.getElementById("removerHabitacion");
		return{
			getFullOccupancy : function(){
				return this.habitaciones.occupancy;;
			},
			crear : function(){
				habitaciones = [];
				for( i= 0 ; i<numRooms ; i++){
					habitaciones[i] = {					
						adultos : parseInt( document.getElementById("h"+(i+1).toString()+"_adultos").value ),
						edadesNinhos : (function(){
							var edades = [];
							for( k=0 ; k<parseInt( document.getElementById("h"+(i+1).toString()+"_ninhos").value ) ; k++ ){
								edades.push( parseInt( document.getElementById("h"+(i+1).toString()+"_n"+(k+1).toString()+"_edad").value ) );
							}
							edades = edades.sort(function(a, b){
								return a - b;
							});
							return edades;
						})()
						
					};
				}
				this.habitaciones.actualizarLabel();
			},
			
			getHabitaciones : function(){
				return JSON.stringify(habitaciones);
			},
			habitaciones : {
				agregar : function(){
					numRooms++;
					var div = document.createElement("div");
					div.setAttribute("class", "row");
					div.setAttribute( "id", "habitacion"+numRooms.toString() );
					div.innerHTML = `
						<div class="content-hab row">
							<!-- <button type="button" class="close closeModal" onclick= "objBusqueda.habitaciones.remover(this.parentNode.parentNode)">&times;</button> -->
							<h5>Habitación `+numRooms.toString()+`</h5>
							<div class="form-group col-xs-6 col-sm-2 col-md-2">
								<label class="title">Adultos</label>
								<select id="h`+numRooms.toString()+`_adultos" class="full-width form-control" onblur= "objBusqueda.crear();" data-selected= "1" onchange= "this.dataset.selected = this.selectedIndex.toString()">
									<option value="1">1</option>
									<option value="2" selected = "selected">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>
							</div>
							<div class="form-group col-xs-6 col-sm-2 col-md-2">
								<label class="title">Niños</label>
								<select id="h`+numRooms.toString()+`_ninhos" class="full-width form-control" onblur= "objBusqueda.crear();" onchange= "objBusqueda.habitaciones.showEdades(this.value, `+numRooms.toString()+`); this.dataset.selected = this.selectedIndex.toString();" data-selected= "0">
									<option value="0" selected = "selected">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
								</select>
							</div>

							<div class="form-group col-xs-4 col-sm-2 col-md-2 left-border" style="display: none;">
								<label class="title">Edad 1</label>
								<select class="full-width form-control" onblur= "objBusqueda.crear();" id= "h`+numRooms.toString()+`_n1_edad" data-selected= "4" onchange= "this.dataset.selected = this.selectedIndex.toString()">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5" selected = "selected">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
								</select>
							</div>

							<div class="form-group col-xs-4 col-sm-2 col-md-2" style="display: none;">
								<label class="title">Edad 2</label>
								<select class="full-width form-control" onblur= "objBusqueda.crear();" id= "h`+numRooms.toString()+`_n2_edad" data-selected= "4" onchange= "this.dataset.selected = this.selectedIndex.toString()">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5" selected = "selected">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
								</select>
							</div>

							<div class="form-group col-xs-4 col-sm-2 col-md-2" style="display: none;">
								<label class="title">Edad 3</label>
								<select class="full-width form-control" onblur= "objBusqueda.crear();" id= "h`+numRooms.toString()+`_n3_edad" data-selected= "4" onchange= "this.dataset.selected = this.selectedIndex.toString()">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5" selected = "selected">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
								</select>
							</div>
						</div>
					`;
					if(numRooms === 3) addRoomBtn.setAttribute("disabled", "disabled");
					else if( numRooms <= 1 ) remRoomBtn.setAttribute("disabled", "disabled");
					else if( remRoomBtn.hasAttribute("disabled") ) remRoomBtn.removeAttribute("disabled");
					roomsContainer.insertBefore( div, addRoomBtn );
					objBusqueda.crear();
				},
				remover : function(){
					roomsContainer.removeChild( document.getElementById("habitacion"+numRooms.toString()) );
					numRooms--;
					if( numRooms === 1 ) remRoomBtn.setAttribute("disabled", "disabled");
					if( addRoomBtn.hasAttribute("disabled") ) addRoomBtn.removeAttribute("disabled");
					objBusqueda.crear();
				},
				showEdades : function(ninhos, habitacion){
					for(var i=1 ; i<= 3 ; i++){
						document.getElementById('h'+habitacion+'_n'+i+'_edad').parentNode.style.display = 'none';
					}
					for(var i=1 ; i<= ninhos ; i++){
						document.getElementById('h'+habitacion+'_n'+i+'_edad').parentNode.style.display = 'block';
					}
				},
				occupancy : {
					habitaciones : 0,
					adultos : 0,
					ninhos : 0
				},
				actualizarLabel : function(){
					this.occupancy.habitaciones = 0;
					this.occupancy.adultos = 0;
					this.occupancy.ninhos = 0;
					habitaciones.forEach(function(habitacion){
						this.occupancy.habitaciones++;
						this.occupancy.adultos += habitacion.adultos;
						this.occupancy.ninhos += habitacion.edadesNinhos.length;
					}, this);
					//document.getElementById("labelBusqueda").innerText = habitaciones.length.toString()+" habitaciones, "+numAdultos.toString()+" adultos y "+numNinhos.toString()+" niños";
					document.getElementById("labelBusqueda").innerHTML = "<b>"+this.occupancy.habitaciones.toString()+"</b> habitaciones,<br><b>"+this.occupancy.adultos.toString()+"</b> adultos y <b>"+this.occupancy.ninhos.toString()+"</b> niños";
				},
				storeRoomsContainer : function(){
					window.sessionStorage.setItem("roomsContainer", roomsContainer.innerHTML);
					window.sessionStorage.setItem("numRooms", numRooms.toString());
				},
				setRoomsContainer : function(){
					roomsContainer.innerHTML = window.sessionStorage.getItem("roomsContainer");
					numRooms = parseInt( window.sessionStorage.getItem("numRooms") );
				$( "select[data-selected]" ).each(function() {
					$(this).val(     $(this).children('option').eq($(this).attr('data-selected')).val()               );
				});
					
					addRoomBtn = document.getElementById("agregarHabitacion");
					remRoomBtn = document.getElementById("removerHabitacion");
					objBusqueda.crear();
				}
				
			} 
		}
	})();

	function validarBusqueda(form){
		return;
		var msg = "";
		if( fechaDiferenciaEnDias(checkIn, checkOut) < 0 )
		{
			msg ="<div style='cursor: default;padding:10px 0;'><h4 class='error'><i class='glyphicon glyphicon-remove-sign'></i> La fecha de check out debe ser posterior a la fecha de check in!</h4><input type='button' id='aceptar' class='btn btn-primary' value='Aceptar' /></div>";
		}
		else if( document.getElementById("destino").value == "*" )
		{
			msg = "<div style='cursor: default;padding:10px 0;'><h4 class='error'><i class='glyphicon glyphicon-remove-sign'></i> No ha seleccionado Destino</h4><p>Debe seleccionar un Destino para continuar!</p><input type='button' id='aceptar' class='btn btn-primary' value='Aceptar' /></div>";
			mostrarMensaje(msg);
		}
		else
		{
			var destino = document.getElementById('destino').options[document.getElementById('destino').selectedIndex].text;
			var fecha_desde = document.getElementById('fecha_desde').value;
			var fecha_hasta = document.getElementById('fecha_hasta').value;
			var cant_noche = document.getElementById('cant_noche').value;
			
			msg = "<br/><img src='images/loading.gif'/><br/><h2>Buscando Hoteles en</h2>";
			msg += "<h3 class='textgreen'><b>"+destino+"</b></h3>";
			msg += "<p class='blockBuscando'>";
			msg += "Desde el <b class='textgreen'>"+fecha_desde+"</b> Hasta el <b class='textgreen'>"+fecha_hasta+"</b><br/>";
			msg += "Para <b class='textgreen'>"+parseInt(objBusqueda.habitaciones.occupancy.adultos)+"</b> Adultos y <b class='textgreen'>"+parseInt(objBusqueda.habitaciones.occupancy.ninhos)+"</b> Ni&ntilde;o/s<br/>";
			msg += "</p>";
			
			objBusqueda.crear();
			document.getElementById("objBusqueda").value = objBusqueda.getObjBusqueda();
			$("select[data-selected]").each(function(){
				var options = this.children;
				for(i=0 ; i<options.length ; i++){
					var option = options.item(i);
					if(option.hasAttribute('selected')) option.removeAttribute('selected');
				}
				this[parseInt(this.dataset.selected)].setAttribute('selected', 'selected');
			});
			objBusqueda.habitaciones.storeRoomsContainer();
			
			form.submit();
			
			$.blockUI({
				centerY: 0,
				message: msg,
				css: {
					color: '#1169B0'
				}
			});
		}
	}
	objBusqueda.habitaciones.agregar();
});

function errorBusqueda() {
					
	$.blockUI({
		message: '<div class="loadingC"><br><br><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No hay resultados para mostrar</h2><p>Favor vuelva a intentar</p><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p><br><br></div>'
	});
						
	$('#aceptar').click(function() { 
		$.unblockUI(); 
		//window.location.href ="home";
		return false; 
	});
}
/* #################################################################
								VUELOS
   ################################################################# */

$(document).ready(function(){

	/* Aumentar / Disminuir cantidad de pasajeros */

	/* Adultos */
    $('#disminuir').click(function(){
        //Solo si el valor del campo es diferente de 0

        if ($('#cantidad_adultos').val() != 0)
        {

            //Decrementamos su valor
            $('#cantidad_adultos').val(parseInt($('#cantidad_adultos').val()) - 1);
        }

    });

    $('#aumentar').click(function(){
        //Aumentamos el valor del campo
        if ($('#cantidad_adultos').val() != 9)
        {

        	$('#cantidad_adultos').val(parseInt($('#cantidad_adultos').val()) + 1);
        }
    });


   /* Niños */

    $('#disminuir-n').click(function(){
        //Solo si el valor del campo es diferente de 0

        if ($('#cantidadN').val() != 0)
        	console.log('Menos');
            //Decrementamos su valor
            $('#cantidadN').val(parseInt($('#cantidadN').val()) - 1);

    });

    $('#aumentarN').click(function(){
        //Aumentamos el valor del campo
        console.log('Mas');
        if ($('#cantidadN').val() <= 4)
        {
        	$('#cantidadN').val(parseInt($('#cantidadN').val()) + 1);
        }
    });

    /* Infantes */

    $('#disminuir-i').click(function(){
        //Solo si el valor del campo es diferente de 0

        if ($('#cantidad_infantes').val() != 0)
            //Decrementamos su valor
            $('#cantidad_infantes').val(parseInt($('#cantidad_infantes').val()) - 1);

    });

    $('#aumentar-i').click(function(){
        //Aumentamos el valor del campo
        if ($('#cantidad_infantes').val() != 2 && $('#cantidad_adultos').val() > $('#cantidad_infantes').val())
        {
        	$('#cantidad_infantes').val(parseInt($('#cantidad_infantes').val()) + 1);
        }
    });


    /* Fin Aumentar y Disminuir Cantidad de Adultos y Niños */

	/* Mostrar / Ocultar "Fecha de Vuelta" en Buscador de Vuelos */

		$('.vuelta').show(); 

		$("#idayvuelta").on( "click", function() {
			$('#idaDiv').show();
		 });
		$("#ida").on( "click", function() {
			$('#idaDiv').hide();  
		});

	/* Fin Mostrar / Ocultar "Fecha de Vuelta" */


/* #################################################################
								TRASLADOS
   ################################################################# */

	/* Adultos */
	    $('.disminuirAdulto').click(function(){
	        //Solo si el valor del campo es diferente de 0
	        if ($('.cantidad-adultos').val() != 0)
	        {
	            //Decrementamos su valor
	            $('.cantidad-adultos').val(parseInt($('.cantidad-adultos').val()) - 1);
	        }

	    });

	    $('.aumentarAdulto').click(function(){
	        //Aumentamos el valor del campo
	        if ($('.cantidad-adultos').val() != 9)
	        {

	        	$('.cantidad-adultos').val(parseInt($('.cantidad-adultos').val()) + 1);
	        }
	    });
	});
	  /* Niños */

	    $('.disminuir_n').click(function(){
	        //Solo si el valor del campo es diferente de 0

	        if ($('.cantidad-ninhos').val() != 0)
	            //Decrementamos su valor
	            $('.cantidad-ninhos').val(parseInt($('.cantidad-ninhos').val()) - 1);

	    });

	    $('.aumentar_n').click(function(){
	        //Aumentamos el valor del campo
	        if ($('.cantidad-ninhos').val() != 4)
	        {
	        	$('.cantidad-ninhos').val(parseInt($('.cantidad-ninhos').val()) + 1);
	        }
	    });


// ******* Chosen **************

$(".chosen").chosen();

var config = {
    '.chosen-select'           : {},
    '.chosen-select-deselect'  : {allow_single_deselect:true},
    '.chosen-select-no-single' : {disable_search_threshold:10},
    '.chosen-select-no-results': {no_results_text:'Sin resultados!'},
    '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
    $(selector).chosen(config[selector]);
}


/* Tabs de Buscador de vuelos Home y Resultados */

$('input[name="radio-tab"]').click(function () {

	$(this).closest('label').tab('show');
		
});