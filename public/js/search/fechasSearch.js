$(function(){
	//alert('yo');
	$('#fecha_desde').datepicker({
		minDate: 0,
		dateFormat: 'dd/mm/yy',
		onSelect: function(dateText) {
			$('#fecha_hasta').datepicker('option','minDate',addDays($('#fecha_desde').datepicker('getDate'),1));
			updateNoches();
		}
	});
	$('#fecha_desde').datepicker("setDate", new Date());
	
	$('#fecha_hasta').datepicker({
		minDate: 1,
		dateFormat: 'dd/mm/yy',
		onSelect: function(){
			updateNoches();
		}
	});
	$('#fecha_hasta').datepicker("setDate", new Date());
	updateNoches();

	if($('#fecha_ida_vuelo').length > 0){
		$('#fecha_ida_vuelo').datepicker({
			minDate: 0,
			dateFormat: 'dd/mm/yy',
			onSelect: function(dateText) {
				$('#fecha_vuelta_vuelo').datepicker('option','minDate',addDays($('#fecha_ida_vuelo').datepicker('getDate'),1));
				updateNochesFligth();
			}
		});
		$('#fecha_ida_vuelo').datepicker("setDate", new Date());
		
		$('#fecha_vuelta_vuelo').datepicker({
			minDate: 1,
			dateFormat: 'dd/mm/yy',
			onSelect: function(){
				updateNochesFligth();
			}
		});
		$('#fecha_vuelta_vuelo').datepicker("setDate", new Date());
		  updateNochesFligth();
	}	  

});

$('#cant_noches').keyup(function(){
	$('#fecha_hasta').datepicker('setDate',addDays($('#fecha_desde').datepicker('getDate'),parseInt($('#cant_noches').val())));
});

$('#cant_nochesFligthF').keyup(function(){
	$('#fecha_vuelta_vuelo').datepicker('setDate',addDaysFligth($('#fecha_ida_vuelo').datepicker('getDate'),parseInt($('#cant_nochesFligthF').val())));
});

function updateNoches(){
	$('#cant_noches').val(dateDiffInDays($('#fecha_desde').datepicker('getDate'),$('#fecha_hasta').datepicker('getDate')));
}

function updateNochesFligth(){
	$('#cant_nochesFligthF').val(dateDiffInDaysFligth($('#fecha_ida_vuelo').datepicker('getDate'),$('#fecha_vuelta_vuelo').datepicker('getDate')));
	if($('#cant_nochesFligthF').val() >= 361){
		$('#cant_nochesFligthF').val(1)
		$('#cant_nochesFligthF').trigger('keyup');
		msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i>La cantidad de dias no puede superar 361 dias</h2><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
		mostrarMensaje(msg);
		event.preventDefault();
	}
}

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

function addDaysFligth(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

var _MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
	// Discard the time and time-zone information.
	var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

function dateDiffInDaysFligth(a, b) {
	// Discard the time and time-zone information.
	var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}
