$(document).ready(function() {

            // ******* Slide de Precios **********

            $( "#price-range" ).slider({
                range: true,
                min: 0,
                max: 3500,
                values: [ 0, 0], 
                slide: function( event, ui ) {
                    $(".min-price-label").html( ui.values[ 0 ]);
                    $(".max-price-label").html( ui.values[ 1 ]);

                    $( "#precio_minimo" ).val( ui.values[ 0 ] );
                    $( "#precio_maximo" ).val( ui.values[ 1 ] );
                }
            });
            $( "#precio_minimo" ).val($( "#price-range" ).slider( "values", 0 ) );

            $( "#precio_maximo" ).val($( "#price-range" ).slider( "values", 1 ) );

            // Fin Slide de Precios

            // ********** Mostrar todas las Habitaciones *********

            /*$("#mostrar-habitaciones").on("hide.bs.collapse", function(){
                $(".precio-habitacion").html('Mostrar Habitaciones Disponibles <i class="fa fa-angle-down" aria-hidden="true"></i>');
            });
            $("#mostrar-habitaciones").on("show.bs.collapse", function(){
                $(".precio-habitacion").html('Ocultar Habitaciones Disponibles <i class="fa fa-angle-up" aria-hidden="true"></i>');
            });*/

            // Fin Mostrar todas las Habitaciones
			
			// ********** Mostrar Detalles *********

           /* $("#mostrar-detalles").on("hide.bs.collapse", function(){
                $(".detalle").html('Mostrar Detalles <i class="fa fa-angle-down" aria-hidden="true"></i>');
            });
            $("#mostrar-detalles").on("show.bs.collapse", function(){
                $(".detalle").html('Ocultar Detalles <i class="fa fa-angle-up" aria-hidden="true"></i>');
            });*/
			
			

            // Fin Mostrar Detalles
			
			// ********** Mostrar Filtro en Móvil *********

            $("#filtro-buscador").on("hide.bs.collapse", function(){
                $(".sh-filter").html('Modificar Búsqueda <i class="fa fa-angle-down" aria-hidden="true"></i>');
            });
            $("#filtro-buscador").on("show.bs.collapse", function(){
                $(".sh-filter").html('Ocultar Búsqueda <i class="fa fa-angle-up" aria-hidden="true"></i>');
            });

            // Fin Mostrar Filtro en Móvil
			
			// ********** Flotar filtro de Bùsqueda cuando sea para mòvil *********
			
			/*var ancho = $(window).width();
			
			alert (ancho);
			
			if (ancho <= 360){
                $('.sh-filtro').addClass("fixed").fadeIn();
            }
            else 
			{
                $('.sh-filtro').removeClass("fixed");
            }*/
			
			
			// ***** Mostrar Ocultar Filtro en Versión Móvil ********
	
				/*$('#sh-filter').click(function(env) {
				
				$('#filtro-buscador').animate({
				  width: "300px"
				}, 1000, "swing");
			  });*/

			  	/* Aumentar / Disminuir cantidad de pasajeros */

			/* Adultos */
		    $('#disminuir').click(function(){
		        //Solo si el valor del campo es diferente de 0

		        if ($('#cantidad_adultos').val() != 0)
		        {

		            //Decrementamos su valor
		            $('#cantidad_adultos').val(parseInt($('#cantidad_adultos').val()));
		        }

		    });
		    $('#cantidad_adultos').change(function(){
		    	console.log('Ingreso');
		    })	
		    $('#aumentar').click(function(){
		        //Aumentamos el valor del campo
		        if ($('#cantidad_adultos').val() != 9)
		        {
		            $cantidad= parseInt($('#cantidad_adultos').val());
		            $('#cantidad_adultos').val(0);
		            //Aumentamos su valor
		            $('#cantidad_adultos').val($cantidad);
	        	}
		    });


			   /* Niños */

		    $('#disminuir-n').click(function(){
		        //Solo si el valor del campo es diferente de 0

		        if ($('#cantidadN').val() > 0){
		        	console.log('Menos');
		            //Decrementamos su valor
		            $('#cantidadN').val(parseInt($('#cantidadN').val()));
		        }else{
		        	return;
		        }    
		    });

		    $('#aumentarN').click(function(){
		        //Aumentamos el valor del campo
		        if ($('#cantidadN').val() < 4)
		        {
		        	$('#cantidadN').val(parseInt($('#cantidadN').val()));
		        }
		    });

		    /* Infantes */

		    $('#disminuir-i').click(function(){
		        //Solo si el valor del campo es diferente de 0

		        if (parseInt($('#cantidad_infantes').val()) != 0) 
		            //Decrementamos su valor
		            $('#cantidad_infantes').val(parseInt($('#cantidad_infantes').val()));

		    });

		    $('#aumentar-i').click(function(){
		        //Aumentamos el valor del campo
		        if ($('#cantidad_infantes').val() != 2)
		        {
		        	$('#cantidad_infantes').val(parseInt($('#cantidad_infantes').val()));
		        }
		    });


		    /* Fin Aumentar y Disminuir Cantidad de Adultos y Niños */

			/* Mostrar / Ocultar "Fecha de Vuelta" en Buscador de Vuelos */

				$('.vuelta').show(); 

				$("#idayvuelta").on( "click", function() {
					$('.vuelta').show();
				 });
				$("#ida").on( "click", function() {
					$('.vuelta').hide(); 
				});

			/* Fin Mostrar / Ocultar "Fecha de Vuelta" */
			
});
	
	// ******* Chosen **************

        $(".chosen").chosen();

        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Sin resultados!'},
            '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
		
	// ******* Cantidad de Estrellas *********

        var expanded = false;
        function showCheckboxes() {
            var checkboxes = document.getElementById("checkboxes");
            if (!expanded) {
                checkboxes.style.display = "block";
                expanded = true;
            } else {
                checkboxes.style.display = "none";
                expanded = false;
            }
        }
		
	/*$(function(){
			display = (function(){
						var expanded = false;
						var checkboxes = document.getElementById("checkboxes");
						function display(mode){
							switch(mode){
								case "show":
									checkboxes.style.display = "block";
									expanded = true;
									break;
								case "hide":
									checkboxes.style.display = "none";
									expanded = false;
									break;
								case "toggle":
									if (!expanded) {
										checkboxes.style.display = "block";
										expanded = true;
									}
									else{
										checkboxes.style.display = "none";
										expanded = false;
									}
									break;
							}
						}
						return display;
				}
			)();
			display('hide');
			$(document).click(function(event){ 
				if(!$(event.target).closest('#estrellasContainer').length){
					/*if($('#estrellasContainer').is(":visible")) {
						$('#estrellasContainer').hide();
					}*/
					/*display('hide');
				}        
			});
			
	});*/
	