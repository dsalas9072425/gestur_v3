var lat = -34.397
var lon = 150.644;
var map;
var labelIndex = 0;
var markers = [];
var markk = [];
var counter = 1;
var counters = 1;
$(document).ready(function() {
	objBusqueda.crear();
	// Búsqueda Avanzada
	$("#busqueda-avanzada").on("hide.bs.collapse", function(){
		$(".ba").html('Búsqueda Avanzada <i class="fa fa-angle-down" aria-hidden="true"></i>');
	});
	$("#busqueda-avanzada").on("show.bs.collapse", function(){
		$(".ba").html('Cerrar Búsqueda Avanzada <i class="fa fa-angle-up" aria-hidden="true"></i>');
	});
});
var map, map2;

$('input[name="radio-tab-traslado"]').click(function () {
	//jQuery handles UI toggling correctly when we apply "data-target" attributes and call .tab('show') 
	//on the <li> elements' immediate children, e.g the <label> elements:
	$(this).closest('label').tab('show');
});

//Se inicialización de los mapas
function initialize(condition){	
			var myOptions = {
		        zoom: 14,
		        center: new google.maps.LatLng(0.0, 0.0),
		        mapTypeId: google.maps.MapTypeId.ROADMAP,
		    }
		    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		    map2 = new google.maps.Map(document.getElementById("map_canvas2"), myOptions);

			bangalore = {lat: lat, lng: lon};

			 // Try HTML5 geolocation.
	        if (navigator.geolocation){
	          	navigator.geolocation.getCurrentPosition(function(position){
		            var pos = {
		              lat: position.coords.latitude,
		              lng: position.coords.longitude
		            };
		            map.setCenter(pos);
		            map2.setCenter(pos);
	          	}, function() {
		            handleLocationError(true, infoWindow, map.getCenter());
		            handleLocationError(true, infoWindow, map2.getCenter());
	          });
	        } else {
	          // Browser doesn't support Geolocation
	          handleLocationError(false, infoWindow, map.getCenter());
	          handleLocationError(false, infoWindow, map2.getCenter());
	        }

	        // Llamada para agregar un marcador al mapa desde
	        google.maps.event.addListener(map, 'click', function(event) {
	          	if(counter ==1){
		        	addMarker(event.latLng, map, counter);
	          	}	
		        counter++
	        });

	        // Llamada para agregar un marcador al mapa hasta
	        google.maps.event.addListener(map2, 'click', function(event) {
	          	if(counters ==1){
		        	addMarkers(event.latLng, map2, counters);
	          	}	
		        counters++
	        });

			google.maps.event.trigger(map, 'resize');

	        // Add a marker at the center of the map.
	        addMarker(bangalore, map);
	        addMarkers(bangalore, map2);
	      }
	      //Genera el mapa en el modal Desde
	      function recrearMapaDesde(){
	      		$('#mapaCoordenadasDesde').modal({
					    }).on('shown.bs.modal', function () {
					        google.maps.event.trigger(map, 'resize');
					    });
	      }
	      //Genear el mapa en el modal Hasta
	      function recrearMapaHasta(){
	      		$('#mapaCoordenadasHasta').modal({
					    }).on('shown.bs.modal', function () {
					        google.maps.event.trigger(map2, 'resize');
					    });
	      }

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow.setContent(browserHasGeolocation ?
	                        'Error: The Geolocation service failed.' :
	                        'Error: Your browser doesn\'t support geolocation.');
}	

//Agrega las marcas al mapa y toma las coordenadas
function addMarker(location, map, counter) {
	var marker = new google.maps.Marker({
								        position: location,
								        label: "A",
								        map: map
		   								});
	markers.push(marker);
	//se discriminan la longitud y latitud
	var markerLatLng = marker.getPosition();
	var latlng = markerLatLng.lat()+","+markerLatLng.lng();
	if(counter == 1){
		$(".coordenadaDesde").attr("value", latlng);
	}
	codeLatLng(latlng,1);
}

//Agrega las marcas al mapa y toma las coordenadas
function addMarkers(location, map2,counters) {
	var mark = new google.maps.Marker({
								        position: location,
								        label: "B",
								        map: map2
		   								});
	markk.push(mark);
	//se discriminan la longitud y latitud
	var markersLatLng = mark.getPosition();
	var latlngs = markersLatLng.lat()+","+markersLatLng.lng();
	if(counters == 1){
		$(".coordenadaHasta").attr("value", latlngs);
	}
	codeLatLng(latlngs,2);
}

// Se agrega la dirección del marcador  
function codeLatLng(latlng, counter) {
	var geocoder = new google.maps.Geocoder(); ;
	var input = latlng;
	var latlngStr = input.split(',', 2);
	var lat = parseFloat(latlngStr[0]);
	var lng = parseFloat(latlngStr[1]);
	var latlng = new google.maps.LatLng(lat, lng);
	geocoder.geocode({'latLng': latlng}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) {
				if(counter <= 1){
					$(".direccionDesde").attr("value", results[0].formatted_address);
				}else{
					$(".direccionHasta").attr("value", results[0].formatted_address);
				}
			} else {
			    alert('No results found');
			}
		} else {
			alert('Geocoder failed due to: ' + status);
		}
	});
}

// Eliminar las marcas del mapa desde
function deleteMarkers(map) {
	for (var i = 0; i < markers.length; i++) {
	    markers[i].setMap(map);
	}
	$(".coordenadaDesde").attr("value", "");
	$(".direccionDesde").attr("value", "");
	counter= 1;
}

// Eliminar las marcas del mapa hasta
function deleteMarker(map2) {
	for (var i = 0; i < markk.length; i++) {
	    markk[i].setMap(map2);
	}
	$(".coordenadaHasta").attr("value", "");
	$(".direccionHasta").attr("value", "");
	counters= 1;
} 