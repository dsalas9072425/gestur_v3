// script de fechas rodriC
var date = (new Date).constructor;
date.prototype.formatear = function(format){
	var dia = this.getDate();
	var mes = this.getMonth();
	mes++;
	var anho = this.getFullYear();
	
	switch(format){
		case "dd/mm/aaaa":
		return dia.toString()+'/'+mes.toString()+'/'+anho.toString();
	}
}
//++ UPD emoran
var checkInObj = document.getElementById("fecha_desde");
var checkOutObj = document.getElementById("fecha_hasta");
var nochesObj = document.getElementById("cant_noche");
var noches;
var checkIn = new Date();
var checkOut = new Date();

//alert(checkInObj);
//alert(checkOutObj);

//alert("scriptFechas..");

function establecerFechas() {
	if( checkInObj.value != "" && nochesObj.value != "" ){
		noches = parseInt( nochesObj.value );
		
		var fechaArray = checkInObj.value.split("/");
		var anho = parseInt( fechaArray[2] );
		var mes = parseInt( fechaArray[1] - 1 );
		var dia = parseInt( fechaArray[0] );
		
		checkIn.setFullYear(anho);
		checkIn.setMonth(mes);
		checkIn.setDate(dia);
		
		updateCheckOut();
	}
	else{
		nochesObj.value = 1;
		noches = parseInt( nochesObj.value );
		
		checkIn.setDate( checkIn.getDate() + noches );
		checkInObj.value = formatearFecha( checkIn );

		updateCheckOut();
	}
}

//++UPD:emoran:13102016(se agregó el control de limite de cantidad de noches)
function updateCheckOut(){
	//alert("updateCheckOut");
	//++noches = checkOut.getDate() - checkIn.getDate(); //parseInt( nochesObj.value );
	noches = parseInt(nochesObj.value);
	//alert(noches);
	if(noches>0 && noches<31)
	{
		//alert("hola:"+noches);
		checkOut.setFullYear( checkIn.getFullYear() );
		checkOut.setMonth( checkIn.getMonth() );
		checkOut.setDate( checkIn.getDate() + noches );
		checkOutObj.value = formatearFecha( checkOut );
		return true;
	}
	else
	{
		checkOut.setFullYear(checkIn.getFullYear());
		checkOut.setMonth(checkIn.getMonth());
		checkOut.setDate(checkIn.getDate()+1);
		checkOutObj.value = formatearFecha(checkOut);
		nochesObj.value = '1';
		nochesObj.focus();
		
		jq.blockUI({
            message: "<div style='cursor: default;padding:10px 0;'><h2 class='textblue'><i class='glyphicon glyphicon-info-sign'></i> Cantidad de Noches inválidas</h2><p>La cantidad de noches debe estar entre 1 y 30 (incluidos)!</p><input type='button' id='aceptar' class='btn btn-primary' value='Aceptar' /></div>"
        });
		
		jq('#aceptar').click(function() { 
            jq.unblockUI(); 
            return false; 
        });
		
		//alert("¡¡La cantidad de noches debe estar entre 1 y 30(incluidos)!!"+noches);
		
		//++
		
		return false;
	}
}

var _MS_PER_DAY = 1000 * 60 * 60 * 24;

// a and b are javascript Date objects
function fechaDiferenciaEnDias(a, b) {
	// Discard the time and time-zone information.
	var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
	//alert("dif");
	return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

function formatearFecha(fechaObj){
	var dia = fechaObj.getDate().toString();
	var mes = fechaObj.getMonth() + 1;
	mes = mes.toString();
	var anho = fechaObj.getFullYear().toString();
	
	var fecha = dia + "/" + mes + "/" + anho;
	return fecha;
}

// script de fechas rodriC
//++ UPD emoran