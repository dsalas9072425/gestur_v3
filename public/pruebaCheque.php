<?php

error_reporting(E_ALL);
/* Get the port for the service. */
$port = "9100";
/* Get the IP address for the target host. */
$host = "192.168.0.151";
/* construct the label */
$mrn = "123456";
$registration_date = "03/13/2013";
$dob = "06/06/1976";
$gender = "M";
$nursing_station = "ED";
$room = "ED01";
$bed = "07";
$lastname = "Lastname";
$firstname = "Firstname";
$visit_id = "12345678";
$label = "q424\nN\n prueba JOHANAAAAAAAAAAAAAAAAAAAAAAAAAA";
$label .= "A10,16,0,3,1,1,N,\"MR# " . $mrn . " ";
$label .= $registration_date . "\"\n";
$label .= "B10,43,0,3,2,4,50,N,\"" . $mrn . "\"\n";
$label .= "A235,63,0,3,1,1,N,\" ";
$label .= $dob . " ";
$label .= $gender . "\"\n";
$label .= "A265,85,0,3,1,1,N,\" ";
$label .= $nursing_station . " ";
$label .= $room . "-";
$label .= $bed . "\"\n";
$label .= "A10,108,0,3,1,1,N,\"";
$label .= $lastname . ",";
$label .= $firstname;
$label .= "\"\n";
$label .= "A10,135,0,3,1,1,N,\" #" . $visit_id . "\"\n";
$label .= "B10,162,0,3,2,4,50,N,\"" . $visit_id . "\"\n";
$label .= "P1\n";
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
if ($socket === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error    ()) . "\n";
} else {
    echo "OK.\n";
}
echo "Attempting to connect to '$host' on port '$port'...";
$result = socket_connect($socket, $host, $port);
if ($result === false) {
    echo "socket_connect() failed.\nReason: ($result) " . socket_strerror    (socket_last_error($socket)) . "\n";
} else {
    echo "OK.\n";
}
socket_write($socket, $label, strlen($label));
socket_close($socket);
?>