/**
 * CREADO PARA USO DE DTP
 * CREADOR: PAULO AQUINO
 * HOTMAIL: paulo_aquino@hotmail.es
 */

var customTypesSerializeJSON = {};

if(!window.moment){
    console.error('Para el correcto funcionamiento del customTypesSerializeJSON.js se necesita la libreria de Moments.js');
}

 customTypesSerializeJSON = {

    /**
     * Depende de la libreria moments.js
     * Recibe fecha en formato DD/MM/YYYY - DD/MM/YYYY @string 
     * @return fecha en formato YYYY-MM-DD @string
     */
    p_date:   function(str) { 
        let result = '';
        if(str.trim() != ''){
          let formato = 'DD/MM/YYYY';
          let fechas = str.split('-');
          let f_1 = moment(fechas[0].trim(),formato).format('YYYY-MM-DD');
          let f_2 = moment(fechas[1].trim(),formato).format('YYYY-MM-DD');
          
           result = f_1+' '+f_2;        
          }
            return result;  
  },    

   /**
     * Depende de la libreria moments.js
     * Recibe fecha en formato DD/MM/YYYY @string 
     * @return fecha en formato YYYY-MM-DD @string
     */
  s_date: function(str) {
    let result = '';
        if(str.trim() != ''){
          let formato = 'DD/MM/YYYY';
          result = moment(str.trim(),formato).format('YYYY-MM-DD');     
          }
            return result;  

},
/**
     * Recibe numeros en formato 000.000,00 @string 
     * @return numeros en formato 000000.00 @number
     */
 convertNumber : function(str){
     
    if(str.trim() != ''){ 
        str = str.replace(/[,.]/g,function (m) {  
                                if(m === '.'){
                                    return '';
                                } 
                                 if(m === ','){
                                    return '.';
                                } 
                           });
        return Number(str);
        }
      
 }



}


