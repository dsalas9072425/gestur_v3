<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("config.php");

 
echo '<br/>--Configuracion--';
echo '<br/>url: '.$url;
echo '<br/>empresa: '.$empresa;
echo '<br/>indice: '.$indice;
echo '<br/>file_path_amadeus: '.$file_path_amadeus;
echo '<br/>file_path_sabre: '.$file_path_sabre;
echo '<br/>file_path_copa: '.$file_path_copa;

//validar directorio
if(is_dir($file_path_amadeus)){
    $directorio = opendir($file_path_amadeus); //ruta actual
    while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
    {
        if (is_dir($file_path_amadeus."/".$archivo))//verificamos si es o no un directorio
        {
            //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
        }
        else
        { 

            //Vaildar extension .txt
            $base_file = pathinfo($archivo);
            if(!(isset($base_file['extension']) && $base_file['extension'] == 'txt')){
                continue;
            }


            $bandera = 0;

            $file = fopen($indice, 'r');
            while ($filas = fgets($file)){
                if(trim($filas) == trim($archivo)){  
                    $bandera = 1;
                }    
            }
            if($bandera != 1){  
                $contenido = "";
                $nombreGuardar = $archivo;
                $archivo = fopen($file_path_amadeus."/".$archivo, 'r');
            
                while ($linea = fgets($archivo)){
                    $contenido .= $linea."||";
                }
                $contenidobase64 =  base64_encode($contenido);
                // var_dump($contenidobase64);
            
                $cuerpo  = array(
                                'air'=>$contenidobase64,
                                'id_empresa'=>$empresa,
                                'id_sucursal'=>$sucursal,
                                'tipo' => 'amadeus',
                                'nombre_archivo' =>  $nombreGuardar,
                                );
                $curl = curl_init();
                preparaUrl($curl, $url, $cuerpo);
                $response = curl_exec($curl);
                $err = curl_error($curl);
                echo "<pre>"; 
                print_r($response);   
                 echo "</pre>";   
                 //registro de log detallado
                 register_log('['.date('d/m/Y H:i:s').' ENVIO] AMADEUS TICKET => '.$nombreGuardar, $log_file);
            
                if ($err) {
                    // echo "cURL Error #:" . $err;
                    register_log('['.date('d/m/Y H:i:s').' ERROR] '.$err, $log_file);
                } else {
                    // $respuesta = json_decode($response);
                    // echo "<pre>"; 
                    // print_r($response);   
                    // echo "</pre>";   
                    $http_code  = curl_getinfo($curl,CURLINFO_HTTP_CODE);
                    if($http_code != 200){
                        register_log('['.date('d/m/Y H:i:s').' ERROR] CODE: '.$http_code.' Recibido => '.$response, $log_file);
                    } 
                }
                curl_close($curl);
                $txt = fopen($indice,"a+");
                fwrite($txt, $nombreGuardar."\n");
            }    
        }
    }
}




//validar directorio
if(is_dir($file_path_sabre)){
    $directorio = opendir($file_path_sabre); //ruta actual
    while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
    {
        if (is_dir($file_path_sabre."/".$archivo))//verificamos si es o no un directorio
        {
            //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
        }
        else
        { 

            //Vaildar extension .PNR
            $base_file = pathinfo($archivo);
            if(!(isset($base_file['extension']) && $base_file['extension'] == 'PNR')){
                continue;
            }


            $bandera = 0;

            $file = fopen($indice, 'r');
            while ($filas = fgets($file)){
                if(trim($filas) == trim($archivo)){  
                    $bandera = 1;
                }    
            }
            if($bandera != 1){  
                $contenido = "";
                $nombreGuardar = $archivo;
                $archivo = fopen($file_path_sabre."/".$archivo, 'r');
            
                while ($linea = fgets($archivo)){
                    $contenido .= $linea."||";
                }
                $contenidobase64 =  base64_encode($contenido);
                // var_dump($contenidobase64);
            
                $cuerpo  = array(
                                'air'=>$contenidobase64,
                                'id_empresa'=>$empresa,
                                'tipo' => 'sabre',
                                'nombre_archivo' =>  $nombreGuardar,
                                );
                               
                $curl = curl_init();
                preparaUrl($curl, $url, $cuerpo);
                $response = curl_exec($curl);
                $err = curl_error($curl);

                echo "<pre>"; 
                print_r($response);   
                echo "</pre>";   

                //registro de log detallado
                register_log('['.date('d/m/Y H:i:s').' ENVIO] SABRE TICKET => '.$nombreGuardar, $log_file);

                // echo $response;
                if ($err) {
                    // echo "cURL Error #:" . $err;
                    register_log('['.date('d/m/Y H:i:s').' ERROR] '.$err, $log_file);
                } else {

                    // $respuesta = json_decode($response);
                    // echo "<pre>"; 
                    // print_r($response);   
                    // echo "</pre>";    
                    

                    $http_code  = curl_getinfo($curl,CURLINFO_HTTP_CODE);
                    if($http_code != 200){
                        register_log('['.date('d/m/Y H:i:s').' ERROR] CODE: '.$http_code.' Recibido => '.$response, $log_file);
                    }
                }

                curl_close($curl);

                //Poner en el indice
                $txt = fopen($indice,"a+");
                fwrite($txt, $nombreGuardar."\n");

                
            }    
        }
    }
}   

//validar directorio
if(is_dir($file_path_copa)){

    $directorio = opendir($file_path_copa); //ruta actual
    while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
    {
    
        if (is_dir($file_path_copa."/".$archivo))//verificamos si es o no un directorio
        {
            //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
        }
        else
        { 

            //Vaildar extension .PNR
            $base_file = pathinfo($archivo);
          /*  if(!(isset($base_file['extension']) && $base_file['extension'] == 'PNR')){
                continue;
            }*/


            $bandera = 0;

            $file = fopen($indice, 'r');



            while ($filas = fgets($file)){
    
                if(trim($filas) == trim($archivo)){  
                    $bandera = 1;
                }    
            }

            if($bandera != 1){  
                $contenido = "";
                $nombreGuardar = $archivo;
                $archivo = fopen($file_path_copa."/".$archivo, 'r');

            
                while ($linea = fgets($archivo)){
    
                    $contenido .= $linea."||";
                }

                $contenidobase64 =  base64_encode($contenido);
                if($contenidobase64 != ""){
            
                    $cuerpo  = array(
                                    'air'=>$contenidobase64,
                                    'id_empresa'=>$empresa,
                                    'tipo' => 'copa',
                                    'nombre_archivo' =>  $nombreGuardar,
                                    );
                    $curl = curl_init();
                    preparaUrl($curl, $url, $cuerpo);
                    $response = curl_exec($curl);

                    echo '<pre>';
                    print_r($response);
      
                    $err = curl_error($curl);

                    //registro de log detallado
                    register_log('['.date('d/m/Y H:i:s').' ENVIO] COPA TICKET => '.$nombreGuardar, $log_file);


                    // echo $response;
                    if ($err) {
                        // echo "cURL Error #:" . $err;
                        register_log('['.date('d/m/Y H:i:s').' ERROR] '.$err, $log_file);
                    } else {

                        // $respuesta = json_decode($response);
                        // echo "<pre>"; 
                        // print_r($response);   
                        // echo "</pre>";    
                        

                        $http_code  = curl_getinfo($curl,CURLINFO_HTTP_CODE);
                        if($http_code != 200){
                            register_log('['.date('d/m/Y H:i:s').' ERROR] CODE: '.$http_code.' Recibido => '.$response, $log_file);
                        }
                    }

                    curl_close($curl);

                    //Poner en el indice
                    $txt = fopen($indice,"a+");
                    fwrite($txt, $nombreGuardar."\n");

                }  
            }    
        }
    }
}


function  preparaUrl($curl, $url, $cuerpo_post) {
    curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0, //no verificar ssl
            CURLOPT_SSL_VERIFYPEER => 0, //no verificar ssl
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>  $cuerpo_post,
          ));
}

function register_log($msg,$file){
    $txt = fopen($file,"a+");
    fwrite($txt, $msg."\n");
}

?>