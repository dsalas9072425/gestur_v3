<?php

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost/dtp_web/public';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
   public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }
  /*  private function prepareForTests()
    {
        Artisan::call('migrate');
        Mail::pretend(true);
    }

    public function setUp()
    {
        parent::setUp(); // Don't forget this!
     
        $this->prepareForTests();
    }*/
    /**
     * Default preparation for each test
     */
    public function setUp()
    {
        parent::setUp();
 
        $this->prepareForTests();
    }
 
    /**
     * Creates the application.
     *
     * @return Symfony\Component\HttpKernel\HttpKernelInterface
     */
   /* public function createApplication()
    {
        $unitTesting = true;
 
        $testEnvironment = 'testing';
 
        return require __DIR__.'/../../start.php';
    }*/
 
    /**
     * Migrates the database and set the mailer to 'pretend'.
     * This will cause the tests to run quickly.
     */
    private function prepareForTests()
    {
        Artisan::call('migrate');
        //Mail::pretend(true);
    }

    
}
