<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "ftp", "s3", "rackspace"
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => 'your-key',
            'secret' => 'your-secret',
            'region' => 'your-region',
            'bucket' => 'your-bucket',
        ],

        'grupos' => [
            'driver' => 'local',
            'root'   => public_path() . '/grupos',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
       ],

        'adjuntoDetalle' => [
           'driver' => 'local',
           'root'   => public_path() . '/adjuntoDetalle',
           'url' => env('APP_URL').'/public',
           'visibility' => 'public',
        ],

        'adjuntoOp' => [
            'driver' => 'local',
            'root'   => public_path() . '/adjuntoDetalle/documentos_op/',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
         ],

        'logoEmpresa' => [
           'driver' => 'local',
           'root'   => public_path() . '/logoEmpresa',
           'url' => env('APP_URL').'/public',
           'visibility' => 'public',
        ],
        
        'uploadDocumento' => [
           'driver' => 'local',
           'root'   => public_path() . '/uploadDocumento',
           'url' => env('APP_URL').'/public',
           'visibility' => 'public',
        ], 
        
        'excelPagoProveedor' => [
            'driver' => 'local',
            'root'   => public_path() . '/excelPagoProveedor',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
         ], 
         'adjuntoLc' => [
            'driver' => 'local',
            'root'   => public_path() . '/adjuntoLc',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
         ], 
         'uploadComprobanteRecibo' => [
            'driver' => 'local',
            'root'   => public_path() . '/uploadComprobanteRecibo',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
         ], 
         'manual' => [
            'driver' => 'local',
            'root'   => public_path(),
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],

        'adjuntoDeposito' => [
            'driver' => 'local',
            'root'   => public_path() .'/comprobante_depositos',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],
        'facturaElectronica' => [
            'driver' => 'local',
            'root'   => public_path() .'/factura_eca',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],

        
    ]

];
