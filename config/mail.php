<?php

return [

    'driver' => env('MAIL_DRIVER', 'smtp'),

    'host' => env('MAIL_HOST', 'smtp.gmail.com'),
    'port' => env('MAIL_PORT', 587),

    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'notificaciones@git.com.py'),
        'name' => env('MAIL_FROM_NAME', 'Notificaciones'),
    ],
    'encryption' => env('MAIL_ENCRYPTION', 'tls'),

    'username' => env('MAIL_USERNAME'),
    'password' => env('MAIL_PASSWORD')

    ];
