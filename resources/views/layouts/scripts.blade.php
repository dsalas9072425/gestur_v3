<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>-->
<script type="text/javascript" src="{{asset('js/html5shiv.js')}}"></script>
<script type="text/javascript" src="{{asset('js/respond.js')}}"></script>

<!--[endif]-->

<!-- Javascript -->
<script type="text/javascript" src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery-ui.1.10.4.min.js')}}"></script>

<!-- Twitter Bootstrap -->
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.blockUI.js')}}"></script>

<!-- Theme Scripts -->
<script type="text/javascript" src="{{asset('js/theme-scripts.js')}}"></script>

<!-- Chosen -->
<script type="text/javascript" src="{{asset('js/chosen.jquery.js')}}"></script>

<!-- Ir Arriba -->
<script type="text/javascript" src="{{asset('js/ir-arriba.js')}}"></script>

<script type="text/javascript" src="{{asset('js/ajax-setup.js')}}"></script>

<!-- Mensajes de error -->
<script type="text/javascript" src="{{asset('js/mensajes.js')}}"></script>
<!--Fom Jquery-->
<script type="text/javascript" src="{{asset('js/jquery.form.js')}}"></script>

<!--Fom DataTable-->
<script type="text/javascript" src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-dialog.min.js')}}"></script>


<!-- load BXSlider scripts // Mobile Tabs -->
<script type="text/javascript" src="{{ URL::asset('components/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
@if( \App::environment() == 'production')
<script>console.log = function() {}</script>
@endif

<script>
	$("#enlace").click(function(event){
		location.reload();
	})

	function autoLogIn(e, n) {
	    var t = document.createElement("form"), o = document.createElement("input"), m = document.createElement("input");
	    t.method = "POST", t.action = "https://servicios.dtpmundo.com/es/login/dynamic-login",
	        o.value = e, o.name = "user", t.appendChild(o), m.value = n, m.name = "pass", t.appendChild(m),
	       document.body.appendChild(t), t.submit(), t.remove();
	}

</script>
