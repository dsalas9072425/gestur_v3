      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENÚ</li>
          @if(Session::has('datos-loggeo'))
            @php
              $menus = Session::get('arrayMenu');
            @endphp

            @foreach($menus['menus'] as $key => $menu)
              @if (isset($menu['visible']))
                @if ($menu['visible'] == true && $menu['activo'] == true)
                <li class="treeview">
                  <a href="#" style="background-color: #072235; color:#fff;">
                    <i class="{{$menu['icono']}}"></i> <span>{{$menu['descripcion']}}</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a> 

                  <ul class="treeview-menu">
                    @foreach($menu['submenu'] as $key => $submenu)
                       @if ($submenu['activo'] == true)
                      <li class="active">
                        <a href="{{route($submenu['url'])}}" style='color:#fff;'><i class="{{$submenu['icono']}}"></i>{{$submenu['descripcion']}}</a></li>
                       @endif
                    @endforeach
                  </ul>
                </li>
                @endif
              @endif  
            @endforeach
          @endif   

      </ul>

               