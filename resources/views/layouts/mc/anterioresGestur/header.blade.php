    
     <!-- Logo -->
    <a href="{{route('factour.home')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="{{asset('images/logo_factour_mini.png')}}" class="img-circle" style="border-radius: 15%;width: 50px;height: 40px;" alt="User Image"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{asset('images/logo_factour.png')}}" class="img-circle" style="border-radius: 15%;width: 100px;height: 40px;" alt="User Image"></span>
    </a>
        
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        </table>
        <ul class="nav navbar-nav">
           <li class="dropdown notifications-menu">
              <div class="row" style="color: white;width: 200px;">
                <div class="col-xs-12 col-sm-12 col-md-12" style="font-size: 15px;">
                  </br>
                   <div id="cotizacion"></div>             
                </div>
              </div>
            </li>
         {{--  <li class="dropdown notifications-menu">
            <a href="" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span id="numero" class="label" style="background-color: #FF0080"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Tienes <span id="numeroheader"></span> notificaciones</li>
              <li>
                <!-- inner menu: contains the actual data  text-aqua-->
                <ul id = "menuNotificaciones" class="menu">
                </ul>
              </li>
              <li id="inical" class="footer"><a href="#">Ver Detalle</a></li>
            </ul>
          </li> --}}
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            @php
              $logo = Session::get('datos-loggeo')->datos->datosUsuarios->logoAgencia;
              $errorLogo = asset('mC/images/img/user2-160x160.jpg');
            @endphp
              <img src="{{asset('personasLogo/'.$logo)}}" onerror="this.src='{{$errorLogo}}'" class="user-image" alt="User Image">
              @if(isset(Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido))
                <span class="hidden-xs">{{Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido}}</span>
              @else
                <span class="hidden-xs">Factour</span>
              @endif   
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                @php
                  $logo = Session::get('datos-loggeo')->datos->datosUsuarios->logoAgencia;
                  $errorLogo = asset('mC/images/img/user2-160x160.jpg');
                @endphp
                <img src="{{asset('personasLogo/'.$logo)}}" onerror="this.src='{{$errorLogo}}'" class="img-circle" alt="User Image">
                <p>
                   @if(isset(Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido))
                      {{Session::get('datos-loggeo')->datos->datosUsuarios->usuario}}<br>{{Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido}}
                      <!--<small>Member since Nov. 2012</small>-->
                  @endif  
                </p>
              </li>
              <li class="user-body">
                <!--<div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>-->
                
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{route('factour.cambiarClave')}}" class="btn btn-default">Cambiar Clave</a>
                </div>
                <div class="pull-right">
                  <a href="{{route('logout')}}" class="btn btn-default">Salir</a>
                </div>
                
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button 
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
      </div>

    </nav>
 <script src="{{asset('mC/js/jquery.min.js')}}"></script>
 <script type="text/javascript">
        $.ajax({
          type: "GET",
          url: "{{route('factour.doCotizacion')}}",
          dataType: 'json',
          success: function(rsp){
                            if(jQuery.isEmptyObject(rsp) == false){
                                var cotizaciones = ""
                                $.each(rsp, function (key, item){
                                  if(key == 0){
                                    cotizaciones +='<span><b>'+item.currency_code+':</b></span>  '+item.venta;
                                  }else{
                                    cotizaciones +=',  <span><b>'+item.currency_code+':</b></span>  '+item.venta;
                                  }
                                })
                                $('#cotizacion').html(cotizaciones);
                            }   
                          }
                 })



                                


 </script>