      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENÚ</li>
            @if(Session::has('datos-loggeo'))
                <li class="treeview">
                  <a href="#" style="background-color: #072235; color:#fff;">
                    <i class="fa fa-shopping-cart"></i> <span>Ventas</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="active"><a href="{{route('factour.listadosProformas')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Proformas</a></li>
                    <li class="active"><a href="{{route('factour.facturaIndex')}}" style='color:#fff;'><i class="fa fa-clipboard"></i>Facturas</a></li>

                    <li class="active"><a href="{{route('factour.listadoNotaCredito')}}" style='color:#fff;'><i class="fa fa-book"></i>Notas de Crédito</a></li>
                  </ul>
                </li>
                 @if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2 || Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 5 || Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 6 )

                <li class="treeview">
                  <a href="#" style="background-color: #072235; color:#fff;">
                    <i class="fa fa-usd"></i> <span>Cotización</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>   
                  <ul class="treeview-menu">
                    <li class="active">
                      <a href="{{route('factour.cotizacionIndex')}}" style='color:#fff;'><i class="fa fa-search"></i>
                      Mostrar Cotización</a></li>
                  </ul>
                </li>
                @endif
                <li class="treeview">
                  <a href="#" style="background-color: #072235; color:#fff;">
                    <i class="fa fa-users"></i> <span>Grupos</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="active"><a href="{{route('factour.indexGrupo')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Listado de grupos</a></li>
                    <li><a href="{{route('factour.grupoAdd')}}" style='color:#fff;'><i class="fa fa-plus"></i>Agregar Grupos</a></li>
                    <!-- <li><a href="#" style='color:#fff;'><i class="fa fa-clipboard"></i>Historico de Proformas</a></li> -->
                  </ul>
                </li>
                <li class="treeview">
                  <a href="#" style="background-color: #072235; color:#fff;">
                    <i class="fa fa-ticket"></i> <span>Tickets</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="active"><a href="{{route('factour.indexTicket')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Listado de tickets</a></li>
                    <li class="active"><a href="{{route('factour.reporteBspIndex')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Reporte BSP</a></li>
                    <!--<li><a href="{{route('factour.create')}}" style='color:#fff;'><i class="fa fa-plus"></i>Agregar Tickets</a></li> -->
                    <!-- <li><a href="#" style='color:#fff;'><i class="fa fa-clipboard"></i>Historico de Proformas</a></li> -->
                  </ul>
                </li>
                @if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2 || Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 5 || Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 6 )

                  <li class="treeview">
                    <a href="#" style="background-color: #072235; color:#fff;">
                      <i class="fa fa-user"></i> <span>Personas</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                      <li class="active"><a href="{{route('factour.personaIndex')}}" style='color:#fff;'><i class="fa  fa-search"></i>Mostrar Persona</a></li>
                      <li class="active"><a href="{{route('factour.personaAdd')}}" style='color:#fff;'><i class="fa fa-user-plus"></i>Nueva Persona</a></li>
                      <li class="active"><a href="{{route('factour.negociacionIndex')}}" style='color:#fff;'><i class="fa  fa-pie-chart"></i>Mostrar Negociación</a></li>
                      <li class="active"><a href="{{route('factour.lineaCreditoIndex')}}" style='color:#fff;'><i class="fa  fa-money"></i>Mostrar Línea Crédito</a></li>
                    </ul>
                  
                  </li>
                 @endif 
               <li class="treeview">
                  <a href="#" style="background-color: #072235; color:#fff;">
                    <i class="fa fa-id-card-o"></i> <span>Cuentas</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="active"><a href="{{route('factour.estadoCuentaindex')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Cuenta Corriente Clientes</a></li>
                    <!-- <li><a href="{{route('factour.create')}}" style='color:#fff;'><i class="fa fa-plus"></i>Agregar Tickets</a></li> -->
                    <!-- <li><a href="#" style='color:#fff;'><i class="fa fa-clipboard"></i>Historico de Proformas</a></li> -->
                  </ul>
                </li>

                <li class="treeview">
                  <a href="#" style="background-color: #072235; color:#fff;">
                    <i class="fa fa-file-archive-o"></i> <span>Cobranzas</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a> 
                  <ul class="treeview-menu"> 
                   <!-- <ul class="treeview-menu">  -->
                    <li class="active"><a href="{{route('factour.reporteFactura')}}" style='color:#fff;'><i class="fa  fa-search"></i>Estados de Factura</a></li>
                    <li class="active"><a href="{{route('factour.reporteFacturasPendientes')}}" style='color:#fff;'><i class="fa  fa-search"></i>Facturas Pendientes</a></li>

                     <li class="active"><a href="{{route('factour.reporteTicketsPendientes')}}" style='color:#fff;'><i class="fa  fa-search"></i>Tickets Pendientes</a></li>
                    <li class="active"><a href="{{route('factour.reporteBspCobranzas')}}" style='color:#fff;'><i class="fa fa-tasks"></i>BCP /Cobranzas</a></li>                  
                  </ul> 
                          
                </li>
             
               <li class="treeview">
                  <a href="#" style="background-color: #072235; color:#fff;">
                    <i class="fa fa-id-card-o"></i> <span>Administración</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>
                  <ul class="treeview-menu">
                    <li class="active"><a href="{{route('factour.sincronizarSaldo')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Sinc. SIGA-FACTOUR</a></li>
                    <li class="active"><a href="{{route('factour.sincronizarSaldoG')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Sinc. SIGA-GESTUR</a></li>

                    <li class="active"><a href="{{route('factour.createMigracionCompras')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Migración Compras</a></li>
                  <!-- </ul>   -->
                   <!-- <ul class="treeview-menu">  -->
                    <li class="active"><a href="{{route('factour.createMigracionVentas')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Migración Ventas</a></li>
                      <li class="active"><a href="{{route('factour.indexProducto')}}" style='color:#fff;'><i class="fa fa-tasks"></i>Productos</a></li>
                  <!-- </ul>     -->
                  </ul>
                </li>

                  <li class="treeview">
                  <a href="#" style="background-color: #072235; color:#fff;">
                    <i class="fa fa-id-card-o"></i> <span>Proveedores</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                  </a>
                  <ul class="treeview-menu">
                      <li class="active"><a href="{{route('factour.reporteDetalleProveedor')}}" style='color:#fff;'><i class="fa fa-search"></i>Pago a Proveedores</a></li>
                  <!-- </ul>     -->
                  </ul>
                </li>
            @endif   
      </ul>

               