
<!-- jQuery 3 -->
<script src="{{asset('mC/js/jquery.min.js')}}"></script>

<script src="{{asset('mC/js/autocomplete.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('mC/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('mC/js/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('mC/js/adminlte.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('mC/js/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap  -->

{{-- <script src="{{asset('mC/js/jquery-jvectormap-1.2.2.min.js')}}"></script> --}}
{{-- <script src="{{asset('mC/js/jquery-jvectormap-world-mill-en.js')}}"></script> --}}

<!-- SlimScroll -->
<script src="{{asset('mC/js/jquery.slimscroll.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('mC/js/Chart.min.js')}}"></script>

<script src="{{asset('mC/js/demo.min.js')}}"></script>

<script src="{{asset('mC/js/moment.min.js')}}"></script>

<script src="{{asset('mC/js/daterangepicker.min.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('mC/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('mC/js/bootstrap-datepicker.es.min.js')}}"></script>

<script src="{{asset('mC/js/bootstrap-timepicker.min.js')}}"></script>

<script src="{{asset('mC/js/jquery.dataTables.min.js')}}"></script>

<script src="{{asset('mC/js/dataTables.bootstrap.min.js')}}"></script>

<script src="{{asset('mC/js/select2.full.min.js')}}"></script>

<script src="{{asset('mC/js/mensajes.min.js')}}"></script>

{{-- <script src="{{asset('mC/js/ajax-setup.js')}}"></script> --}}

<script src="{{asset('mC/js/jquery.blockUI.min.js')}}"></script>

<script src="{{asset('mC/js/bootstrap-dialog.min.js')}}"></script>

{{--wysihtml5--}}
<script src="{{asset('mC/js/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('mC/js/toast.min.js')}}"></script>

<!-- push.js -->
<script src="{{asset('mC/js/push.min.js')}}"></script>
<script src="{{asset('mC/js/serviceWorker.min.js')}}"></script>


{{-- <script src="{{asset('mC/js/icheck.min.js')}}"></script> --}}

<!--<script type="text/javascript">
	$('.sidebar-toggle').trigger('click');
</script>-->

