


    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('gestion/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('gestion/app-assets/vendors/js/charts/raphael-min.js')}}"></script>
    <script src="{{asset('gestion/app-assets/vendors/js/charts/morris.min.js')}}"></script>
    <script src="{{asset('gestion/app-assets/vendors/js/extensions/unslider-min.js')}}"></script>
    <script src="{{asset('gestion/app-assets/vendors/js/timeline/horizontal-timeline.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('gestion/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('gestion/app-assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('gestion/app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script>
    <!-- END: Page JS-->