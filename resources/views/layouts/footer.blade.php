
    <div class="footer-wrapper">
        <div class="container">
           <!-- <div class="auto-container">
            Sec Title
             <br>
              <br>
            <div>
                <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Paquetes Destacados</h1>
            </div>
            <br>
            <div class="row clearfix">
                <div class="col-sm-12 col-md-1">
                </div>
                <div class="col-sm-12 col-md-10">
                    <div id="promocionesFooter">
                    </div> 
                </div>   
                <div class="col-sm-12 col-md-1"> 
                </div>     
            </div>
        </div>-->
        <br>
        <div class="row" style="height: 100px">
        </div>
         </div>
    </div>
    <div class="bottom gray-area">
        <div class="container">
        <section class="info-section" style="display: block;">
                <div class="auto-container">
                    <div class="inner-section">
                        <div class="row clearfix">
                            <!--Info Block-->
                            <div class="info-block col-md-4 col-sm-4 col-xs-12">
                                <div class="inners">
                                    <div class="content">
                                        <div class="icon-box" style="width: 60px; margin-left: 20%; color: #ffffff"><i class="fa fa-map-marker fa-4x" aria-hidden="true"></i></div><br/>
                                         <div class="title" style="color: #ffffff;font-size: 16px;font-weight: 300;" >Gral. Bruguéz 353 c/ 25 de Mayo</div>
                                        <h5 style="font-size: 16px; color: #ffffff" class="filter-name has-expander expanded">ASUNCIÓN, PARAGUAY</h5>
                                    </div>    
                                </div>
                            </div>
                            
                            <!--Info Block-->
                            <div class="info-block col-md-4 col-sm-4 col-xs-12">
                                <div class="inners">
                                    <div class="content">
                                        <div class="icon-box" style="width: 60px; margin-left: 20%; color: #ffffff"><i class="fa fa-volume-control-phone fa-4x" aria-hidden="true"></i></div><br/>
                                        <div class="title" style="color: #ffffff;font-size: 16px;font-weight: 300;" >Números:</div>
                                       <h5 style="font-size: 16px; color: #ffffff" class="filter-name has-expander expanded">595 21 221 816 / 595 21 449 724</h5>
                                    </div>     
                                </div>    
                            </div>
                            
                            <!--Info Block-->
                            <div class="info-block col-md-4 col-sm-4 col-xs-12">
                                <div class="inners">
                                    <div class="content">
                                        <div class="icon-box" style="width: 60px; margin-left: 20%; color: #ffffff"><i class="fa fa-clock-o fa-4x" aria-hidden="true"></i></div><br/>
                                         <div class="title" style="color: #ffffff;font-size: 16px;font-weight: 300;" >Horario de atención:</div>
                                        <h5 style="font-size: 16px; color: #ffffff" class="filter-name has-expander expanded">Lun - Vie: 8am - 6pm</h5>
                                    </div>                                
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="copyright pull-right">
                <div class="widgets-section">
                    <div class="auto-container">
                        <div class="row clearfix">
                            <!--Footer Column-->
                            <div class="footer-column col-md-4 col-sm-4 col-xs-12">
                                <!--Logo Widget-->
                                <h2 class="subtitle hide-medium" style="text-align: center; font-size: xx-large; color: #ffffff; font-weight: 400;">DTP TRAVEL GROUP</h2>
                                DTP TOUR OPERATOR S.R.L Desde el 29 de MARZO de 1985
                            </div>
                                  
                            <!--Footer Column-->
                            <div class="footer-column col-md-4 col-sm-4 col-xs-12">
                                <!--Links Widget-->
                                <h2 class="subtitle hide-medium" style="text-align: center; font-size: xx-large; color: #ffffff; font-weight: 400;">Menú</h2>
                                Hoteles
                                <br/>
                                Vuelos
                                <br/>
                                Circuitos
                                <br/>
                                Actividades
                                <br/>
                                Traslados
                            </div>
                                    
                            <!--Footer Column-->
                            <div class="footer-column col-md-4 col-sm-4 col-xs-12">
                                <!--Form Widget-->
                                <h2 class="subtitle hide-medium" style="text-align: center; font-size: xx-large; color: #ffffff; font-weight: 400;">Boletin De Noticias</h2>
                                Ingrese su correo electrónico y reciba noticias de paquetes
                                <br/>
                                <br/>
                                <input type="text" name="name" value="" placeholder="Su correo electrónico" required="">
                                 <br/>
                                 <br/>
                                <button type="submit" class="btn btn-small btn-info text-center reservarBtn btn transaction normal hide-small normal-button" style="height: 40px;">SUSCRIBIRME</button>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="title" style="color: #ffffff;font-size: 14px;font-weight: 300;" >&copy; <?php echo date("Y"); ?> DTP Mundo - Desarrollo Turistico Paraguayo</div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset ('js/jquery-1.11.1.min.js')}}"></script>
    <script>
    </script>

