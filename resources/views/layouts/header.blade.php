<div class="topnav hidden-xs">

    <div class="container">
        <ul class="quick-menu pull-right">
            @if(Session::has('datos-loggeo'))
                @if(Session::get('datos-loggeo')->datos->datosUsuarios->id_sistema_facturacion)
                    @if(
                        Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == config('constants.adminDtp')||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == config('constants.mktProductos')||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == config('constants.adminAgencia')||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 10/*config('constants.vendedorDtp')*/)   
                        <li class="ribbon"><a class="btn btn-info text-center btn transaction_normal hide-small normal-button" href="{{route('mc.homemc')}}" title="Soporte" style="width: 120px;height: 35px;padding-top: 3px;background-color: #e2076a;padding-left: 10px;">backoffice</a></li>
                     @endif  
                     <li class="ribbon"><a href="{{route('reporteNotificacion')}}" title="Soporte - {{Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil}}"> Notificaciones</a></li>
                     <li class="ribbon currency"><a href="#" title="">Reportes</a>
                        <ul class="menu mini">
                            <li class="active"><a href="{{route('reservas')}}" title="Reservas"><i class="fa fa-user" aria-hidden="true"></i>Reservas</a></li>
                            <li class="active"><a href="{{route('reporteCancelacion')}}" title="Reservas"><i class="fa fa-user" aria-hidden="true"></i>PRÓXIMAS A GASTOS</a></li>
                            <li class="active"><a href="{{route('consultaFacturas')}}" title="Facturas"><i class="fa fa-user" aria-hidden="true"></i>Facturas y Voucher</a></li>
                            <li class="active"><a href="{{route('dtPlus')}}" title="dtPlus"><i class="fa fa-user" aria-hidden="true"></i>DTPlus</a></li>

                            @if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 10)  
                                <li class="active"><a href="{{route('reportePaqueteSolicitado')}}" title="Paquetes Solicitados"><i class="fa fa-user" aria-hidden="true"></i>Paquetes Solicitados</a></li>
                            @endif   
                             
                            @if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == config('constants.adminDtp'))  
                                <li class="active"><a href="{{route('reporteBusqueda')}}" title="Reporte de Busqueda"><i class="fa fa-user" aria-hidden="true"></i>Busqueda</a></li>
                                <li class="active"><a href="{{route('reporteAccesos')}}" title="Reporte de Busqueda"><i class="fa fa-user" aria-hidden="true"></i>Accesos</a></li>
                                 <li class="active"><a href="{{route('reportePromocion')}}" title="Reporte de Promo"><i class="fa fa-user" aria-hidden="true"></i>Promo</a></li>
                            @endif   
                        </ul>       
                     </li>
                   @if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == config('constants.adminDtp'))
                            <li class="ribbon currency"><a href="#" title="">Administración</a>
                                <ul class="menu mini">
                                    <!--<li class="active"><a href="{{route('logActivity')}}" title="Usuario"><i class="fa fa-users" aria-hidden="true"></i> Log de Reservas</a> </li>-->
                                    <li class="active"><a href="{{route('agencias')}}" title="Agencias"><i class="fa fa-university" aria-hidden="true"></i> Agencias</a></li>
                                    <li class="active"><a href="{{route('sucursales')}}" title="Sucursales"><i class="fa fa-home" aria-hidden="true"></i>Sucursales</a> </li>
                                    <li class="active"><a href="{{route('usuarios')}}" title="Usuario"><i class="fa fa-users" aria-hidden="true"></i> Usuarios</a> </li>
                                </ul>
                            </li>
                          @endif
                      
                       {{-- @if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia == config('constants.adminAgencia'))
                            @if(Session::get('datos-loggeo')->datos->datosUsuarios->id_sistema_facturacion)
                                <!--<li class="ribbon currency"><a href="#" title="">Administración</a>
                                    <ul class="menu mini">
                                        <li class="active"><a href="{{route('sucursales')}}" title="Sucursales"><i class="fa fa-home" aria-hidden="true"></i>Sucursales</a> </li>
                                        <li class="active"><a href="{{route('usuarios')}}" title="Usuario"><i class="fa fa-users" aria-hidden="true"></i> Usuarios</a> </li>
                                    </ul>
                                </li>-->
                            @endif
                        @endif
                        --}}
                    @endif
                <li class="ribbon currency"><a href="{{route('perfilUsuario')}}" title="">Mi Perfil</a></li>
                <li class="ribbon currency"><a href="{{route('logout')}}" title="Salir"><!--<i class="fa fa-sign-out" aria-hidden="true"></i>--> Salir</a> </li>
            @else
                <li><a href="{{route('login')}}">Ingresar</a></li>
            @endif
        </ul>
    </div>
</div>
<div class="main-header" <!--style="background-color:#2B4391"-->
    <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
        Mobile Menu Toggle
    </a>
    <div class="container">
        <h1 class="logo navbar-brand">
            <a href="{{route('home')}}" title="DTP Mundo - Inicio">
                <img src="{{asset('images/logo-2.png')}}" alt="DTP Mundo - Desarrollo Turístico Paraguayo" />
            </a>
        </h1>
        <nav id="main-menu" role="navigation">
            <ul class="menu">
            @if(Session::has('datos-loggeo'))
                <?php 
                    $usuario = Session::get("datos-loggeo")->datos->datosUsuarios->usuario;
                    $tokenBk = Session::get("datos-loggeo")->datos->datosUsuarios->tokenBk;
                ?>
                <li class="menu-item-has-children" style="padding-right: 0px;"><a style="margin-top: 5px;" href="{{route('vistapromocion')}}"><img src="{{asset('images/botons.gif')}}" alt="PROMO" style="width: 150px;height: 80px;">
                </a></li>
                <li class="menu-item-has-children"><a href="{{route('home')}}">Motor de Reservas</a></li>
                <!--<li class="menu-item-has-children"><a href="{{route('buscarPaquete')}}">Paquetes</a></li>-->
                <li class="menu-item-has-children"><a href="{{route('afiliados')}}">Contratos de Hoteles</a></li>
			@endif	  
            </ul>
        </nav>
    </div>
    <nav id="mobile-menu-01" class="mobile-menu collapse">
        <ul id="mobile-primary-menu" class="menu">
            <li><a href="{{route('home')}}">Hoteles</a></li>
            <li class="menu-item-has-children"><a id="enlace" href="{{route('home')}}#flights-tab">Vuelos</a></li>
            <li><a href="#">Circuitos</a></li>
        </ul>
        <ul class="mobile-topnav container">
            @if(Session::has('datos-loggeo'))
                <li><a href="#" title="Mis Reservas">Mis Reservas</a></li>
                <li class="ribbon currency"><a href="#" title="">usuasrio@email.com</a>
                    <ul class="menu mini">
                        <li class="active"><a href="#" title="Mi Perfil"><i class="fa fa-user" aria-hidden="true"></i> Mi Perfil</a></li>
                        <li class="active"><a href="#" title="Salir"><i class="fa fa-sign-out" aria-hidden="true"></i> Salir</a> </li>
                    </ul>
                </li>
            @else
                <li><a href="{{route('login')}}">Ingresar</a></li>
            @endif
        </ul>
    </nav>
</div>