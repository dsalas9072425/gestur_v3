    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top navbar-dark navbar-shadow" style="background-color: #00b5b8;" >
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-lg-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item mr-auto">
                    </li>
                    <li class="nav-item d-none d-lg-block nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="toggle-icon ft-toggle-right font-medium-3 white" data-ticon="ft-toggle-right"></i></a></li>
                    <li class="nav-item d-lg-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                       <li class="nav-item nav-search">
                            <br>
                       </li>
                    </ul>

                    <ul class="nav navbar-nav float-right">
                        {{-- <br> --}}
                         <li class="dropdown notifications-menu">
                          <div class="row" style="color: white;width: 200px;">
                            <div class="col-xs-12 col-sm-12 col-md-12" style="font-size: 15px;">
                              </br>
                               <div id="cotizacion_header" style="padding-top: 5px;"></div>             
                            </div>
                          </div>
                        </li>
                        
                        <li class="dropdown dropdown-user nav-item">
                            <div class="dropdown-menu dropdown-menu-right">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

 <script type="text/javascript">
    
 </script>