    
    <script src="{{asset('gestion/app-assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('gestion/app-assets/js/vendors.min.js')}}"></script>



 

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('gestion/app-assets/vendors/js/charts/raphael-min.js')}}"></script>
    <script src="{{asset('gestion/app-assets/vendors/js/charts/morris.min.js')}}"></script>
    <script src="{{asset('gestion/app-assets/vendors/js/extensions/unslider-min.js')}}"></script>
    <script src="{{asset('gestion/app-assets/vendors/js/timeline/horizontal-timeline.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- sweetalert-->
    <script src="{{asset('gestion/app-assets/vendors/js/extensions/sweetalert.min.js')}}"></script>

    <!-- BEGIN: Theme JS--> 
    <script src="{{asset('gestion/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('gestion/app-assets/js/core/app.js')}}"></script>
    {{-- <script src="{{asset('gestion/app-assets/js/scripts/customizer.min.js')}}"></script> --}}
    {{-- <script src="{{asset('gestion/app-assets/js/tables/jszip.min.js')}}"></script> --}}
    <!-- END: Theme JS-->

   <!-- SELECT2 -->    
   <script src="{{asset('gestion/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>

    <!-- DATATABLE -->
    <script src="{{asset('gestion/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    {{-- <script src="{{asset('gestion/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script> --}}
    {{-- <script src="{{asset('gestion/app-assets/js/tables/buttons.html5.min.js')}}"></script> --}}
    <script>
        //CONFIGURACION GLOBAL PARA DATATABLE
        $.extend( true, $.fn.dataTable.defaults, {
        "language": {"url": "{{asset('gestion/app-assets/vendors/json/Datatable-Spanish.json')}}"}} );
        </script>

   <!-- BEGIN: Page JS-->
    {{-- <script src="{{asset('gestion/app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script> --}}
   <!-- sEND: Page JS-->

   <!-- MOMENT -->
   <script src="{{asset('gestion/app-assets/js/moment.min.js')}}"></script>
    <!-- DATERANGE PICKER -->
    <script src="{{asset('gestion/app-assets/js/daterangepicker.min.js')}}"></script>

    <!-- BOOSTRAP DATE PICKER-->
    <script src="{{asset('gestion/app-assets/js/bootstrap-datepicker.min.js')}}"></script> 
    <script src="{{asset('gestion/app-assets/js/bootstrap-datepicker.es.min.js')}}"></script>
    <!-- TIME PICKER-->
    <script src="{{asset('gestion/app-assets/js/bootstrap-timepicker.min.js')}}"></script>

    <!-- NUEVO TOAST-->
    <script src="{{asset('gestion/app-assets/vendors/js/extensions/toastr.min.js')}}"></script>
    <!-- TOAST-->
    <script src="{{asset('gestion/app-assets/js/toast.min.js')}}"></script>

    <!-- SUMERNOTE -->
    <script src="{{asset('gestion/app-assets/js/summernote/summernote-bs4.js')}}"></script>

        <!-- WYSIHTML5 NO FUNCIONA-->
        {{-- <script src="{{asset('gestion//app-assets/js/bootstrap3-wysihtml5.all.min.js')}}"></script> 

         <script src="{{asset('gestion/app-assets/js/scripts/tables/datatables/datatable-advanced.js')}}"></script> --}}

     <script>
        $('#selectEmpresa').select2();
        $('#selectEmpresa').change(function(){
            idEmpresa = $('#selectEmpresa').val();
            $.ajax({
                    type: "POST",
                    url: "{{route('modificarDatos')}}",
                    dataType: 'json',
                    data: 'id='+idEmpresa,
                    tryCount : 0,
                    retryLimit : 3,
                    error : function(xhr, textStatus, errorThrown ) {
                            //REINTENTOS DE CONEXION
                            this.tryCount++;
                            if (this.tryCount <= this.retryLimit) {
                                //try again
                                $.ajax(this);
                                return;
                            }   
                            $.toast({
                                        heading: 'Error',
                                        text: 'Ocurrio un error.',
                                        position: 'top-right',
                                        icon: 'error'
                                    });

                            return;
                        },
                        success: function(rsp){
                            if(rsp.status == 'OK'){
                                $.toast({
                                    heading: 'Success',
                                    text: 'Se ha modificado la empresa.',
                                    position: 'top-right',
                                    showHideTransition: 'slide',
                                    icon: 'success'
                                });
                                location.reload();
                            }

                        }
                  })    
        })    
</script>