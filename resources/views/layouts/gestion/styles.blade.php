 <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" href="{{asset('gestion/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/vendors/css/extensions/unslider.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/vendors/css/weather-icons/climacons.min.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/fonts/meteocons/style.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/vendors/css/charts/morris.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" href="{{asset('gestion/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/css/colors.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/css/components.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" href="{{asset('gestion/app-assets/css/core/menu/menu-types/vertical-menu-modern.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/fonts/simple-line-icons/style.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" href="{{asset('gestion/app-assets/css/pages/timeline.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" href="{{asset('gestion/css/style.css')}}">
    <!-- END: Custom CSS-->

    <!-- DATATABLE -->
    <link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">

    <!-- SELECT2 -->
    <link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/vendors/css/forms/selects/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/vendors/css/forms/selects/select2-bootstrap4.min.css')}}">
    
     <!-- BOOSTRAP DATEPICKER -->
     <link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/css/bootstrap-datepicker.min.css')}}">

      <!-- NUEVO TOAST-->
      <link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/css/plugins/extensions/toastr.css')}}">

     <!-- TOAST-->
     <link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/css/toast.min.css')}}">

      <!-- SUMERNOTE -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
      {{-- <script src="{{asset('gestion/app-assets/css/summernote/summernote-bs4.css')}}"></script> --}}

      <!-- DATERANGE PICKER -->
      <link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/css/daterangepicker.min.css')}}">
     
    <link rel="stylesheet" type="text/css"  href="{{asset('gestion/app-assets/css/core/menu/menu-types/horizontal-menu.css')}}">
    
    <link rel="stylesheet" type="text/css"  href="{{asset('gestion/app-assets/css/core/colors/palette-gradient.css')}}">


