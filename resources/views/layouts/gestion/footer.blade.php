   <?php 
		$ssl   = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on';
		$proto = strtolower($_SERVER['SERVER_PROTOCOL']);
		$proto = substr($proto, 0, strpos($proto, '/')) . ($ssl ? 's' : '' );
		if($_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI'] == 'localhost/factour-v2/public/login'){
			$indicador = 0;
		}else{
			$indicador = 1;
		}
   ?>
    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-border">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright &copy; <?php echo date('Y'); echo ' GIT';?></span><span class="float-md-right d-none d-lg-block"><?php echo 'GIT';?>  with <i class="ft-heart pink"></i></span></p>
    </footer>
    <!-- END: Footer-->
