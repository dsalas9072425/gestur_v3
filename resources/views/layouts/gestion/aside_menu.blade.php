<div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" navigation-header"><span>MENÚ</span>
                  <i class="ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="General"></i>
                </li>
          @if(Session::has('datos-loggeo'))
            @php
              $menus = Session::get('arrayMenu');
              // echo "<pre>";
              // print_r($menus);
              // die;
            @endphp

            @foreach($menus as $key => $orden)

              @foreach($orden as $key => $menu)
                @if (isset($menu['visible']))
                  @if ($menu['visible'] == true && $menu['activo'] == true)
                      <li class="nav-item">
                        <a href="#">
                          <i class="{{$menu['icono']}}"></i>
                          <span class="menu-title" data-i18n=""><b style="color: #18184d;font-weight: 500;">{{$menu['descripcion']}}</b></span>
                      </a>

                    <ul class="menu-content">
                      @if (isset($menu['submenu']))
                        @foreach($menu['submenu'] as $key => $submenu)
                           @if ($submenu['activo'] == true)
                            <li class="menu-item"><a class="menu-item" href="{{route($submenu['url'])}}">{{$submenu['descripcion']}}</a></li>
                           @endif
                        @endforeach
                  @endif

                    </ul>
                  </li>
                  @endif
                @endif  
              @endforeach
@endforeach
            
          @endif   

      </ul>
    </div>