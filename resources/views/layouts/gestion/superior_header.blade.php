<?php $tipo_persona =   DB::select("select denominacion from tipo_persona where id=".Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil);?>
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="padding-left: 5px;">
        <div class="pull-left image">
         @php
            $logo = Session::get('datos-loggeo')->datos->datosUsuarios->logoAgencia;
             $errorLogo = asset('mC/images/img/user2-160x160.jpg');
          @endphp
            <img src="{{asset('personasLogo/'.$logo)}}" onerror="this.src='{{$errorLogo}}'" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info" style="left: 45px;">
          @if(isset(Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido))
            <p style="text-transform:uppercase">{{Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido}}</p>
            <a><i class="fa fa-circle text-success"></i> 
              <?php echo str_replace('_',' ',$tipo_persona[0]->denominacion);?>
            </a>
          @else
            <p style="text-transform:uppercase"></p>
          @endif  

        </div>
      </div>
      <!-- search form -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
    </section>

