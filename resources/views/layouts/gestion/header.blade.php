    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top navbar-dark navbar-shadow" style="background-color: #00b5b8;" >
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-lg-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="{{asset('home')}}" style="padding-top: 5px;">
                            @php
                                  $logoEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->logoEmpresa;
                                // $empresaLogo = "logoEmpresa/".$logoEmpresa;
                                $logoSucursal = DB::select('SELECT s.logo FROM personas p, personas s WHERE p.id = '.Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario.' AND p.id_sucursal_empresa = s.id');
                                if($logoSucursal[0]->logo != "" && $logoSucursal[0]->logo != 'factour.png'){
                                    $baseLogo = $logoSucursal[0]->logo;
                                    $empresaLogo = asset("personasLogo/$baseLogo");
                                }else{
                                    if(isset($comercioPersona[0]->logo)){
                                        $empresaLogo = $comercioPersona[0]->logo;
                                    }else{
                                        $empresaLogo = asset("logoEmpresa/$logoEmpresa");
                                    }
                                }
                            @endphp
                            <img class="brand-logo" src="{{asset($empresaLogo)}}" style="width: 120px;height: 45px;">
                            <h2 class="brand-text" style="color:#404e67"><!--b>GIT</b--></h2>
                        </a></li>
                    <li class="nav-item d-none d-lg-block nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="toggle-icon ft-toggle-right font-medium-3 white" data-ticon="ft-toggle-right"></i></a></li>
                    <li class="nav-item d-lg-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                       <li class="nav-item nav-search" style="margin-left: 25px;">
                            <br>
                            @if(!empty(Session::get('datos-loggeo')->datos->empresas))
                                <select class="select2 form-control" name="selectEmpresa" id="selectEmpresa" style="width: 400px;">
                                    @foreach(Session::get('datos-loggeo')->datos->empresas as $key=>$empresas)
                                        @if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == $empresas['id'])
                                        <option value="{{$empresas['id']}}" selected="selected">{{$empresas['denominacion']}}</option>
                                        @else
                                        <option value="{{$empresas['id']}}">{{$empresas['denominacion']}}</option>
                                        @endif
                                    @endforeach
                                </select> 
                            @endif    
                       </li>
                    </ul>

                    <ul class="nav navbar-nav float-right">
                        {{-- <br> --}}
                         <li class="dropdown notifications-menu">
                          <div class="row" style="color: white;max-width: 550px;">
                            <div class="col-xs-12 col-sm-12 col-md-12" style="font-size: 15px;">
                              </br>
                               <div id="cotizacion_header" style="padding-top: 5px;"></div>             
                            </div>
                          </div>
                        </li>
                        
                        @php
                        $logo = Session::get('datos-loggeo')->datos->datosUsuarios->logoAgencia;
                        $errorLogo = asset('mC/images/img/user2-160x160.jpg');
                        @endphp
                        
                        <li class="dropdown dropdown-user nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <span class="avatar avatar-online">
                            <img src="{{asset('personasLogo/'.$logo)}}" onerror="this.src='{{$errorLogo}}'" alt="avatar"><i></i></span>
                            @if(isset(Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido))
                                    <span class="user-name">{{Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido}}</span>
                                @else
                                    <span class="user-name">Factour</span>
                                @endif   
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{ asset('manual/Gestur - Manual del Usuario.pdf') }}" target='_blank'><i class="ft-user"></i>Manual</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('cambiarClave')}}"><i class="ft-user"></i> Cambiar Clave</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('logout')}}"><i class="ft-power"></i> Salir</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

<script src="{{asset('mC/js/jquery.min.js')}}"></script>
 <script type="text/javascript">
        $.ajax({
          type: "GET",
          url: "{{route('doCotizacion')}}",
          dataType: 'json',
          success: function(rsp){
                            if(jQuery.isEmptyObject(rsp) == false){
                              
                                let cotizaciones = ""
                                $.each(rsp.cotizacion, function (key, item){
                                  cotizaciones +='<span><b>'+item.currency_code+':</b></span> '+item.venta+' ';
                                })
             

                                $.each(rsp.indice_cotizacion, function (key, item){
                                  cotizaciones +='<span><b>'+item.currency_origen+'/'+item.currency_destino+':</b></span> '+item.indice+' ';
                                })
             
                                $('#cotizacion_header').html(cotizaciones);
                            }   
                          }
                 })



                                


 </script>