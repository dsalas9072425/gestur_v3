<!-- Favicon -->
<link rel="shortcut icon" href="{{asset('images/logo_factour_mini.png')}}"">

<!-- Theme Styles -->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

<link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel='stylesheet' type='text/css'>

<!-- Main Style -->
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}" >

<!-- Custom Styles -->
<link rel="stylesheet" href="{{asset('css/custom.css')}}">

<!-- Responsive Styles -->
<link rel="stylesheet" href="{{asset('css/responsive.css')}}">

<!-- Chosen Select  -->
<link rel="stylesheet" href="{{asset('css/chosen.css')}}" >

<!-- Datatable Bootstrap -->
<link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}}">

<!-- Datatable Bootstrap Dialog -->
<link rel="stylesheet" href="{{asset('css/bootstrap-dialog.min.css')}}">

<link rel="stylesheet" href="{{asset('css/main.css') }}">

<link href='https://fonts.googleapis.com/css?family=Ubuntu:300,700|Noto+Sans:400,700|Source+Code+Pro:400,700&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>

<style>
	.info-section .inner-section {
		    position: relative;
		    margin-top: -75px;
		    margin-bottom: -75px;
		    background-color: #FF0080;
	}
	.info-section .inner-section .info-block .inner .content .title .inners {
		    position: relative;
		    color: #ffffff;
		    font-size: 16px;
		    font-weight: 300;
		    margin-bottom: 3px;
	}
	.inners {
			position: relative;
			text-align: left;
			padding: 45px 20px 45px 65px;
	}
	.main-footer .widgets-section {
		    position: relative;
		    padding: 150px 0px 40px;
	}
	.auto-container {
		    position: static;
		    max-width: 1200px;
		    padding: 0px 15px;
		    margin: 0 auto;
	}
	.soap-login-page.style1 .login-form .input-text{
		display: inline-block;
	    width: 100%;
	    padding: 7px 9px;
	    margin: 0;
	    background-color: #fff;
	    font-size: 14px;
	    line-height: 24px;
	    color: #1c2b39;
	    border: 1px solid #bcd5f5;
	    -moz-border-radius: 3px;
	    border-radius: 3px;
	    outline: none;
	    -webkit-appearance: none;
	} 
	.divContainer {
		//background-color: white;
	    padding-top: 15px;
	    padding-bottom: 15px;
	    padding-left: 20px;
	    padding-right: 20px;
	    margin-left: 4%;
	}    

</style>