<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
  <title>GESTUR</title>
  @include('layouts/gestion/meta') 
  
  <!-- ================================================================== 
                   STYLE
     ================================================================== -->

  @section('styles') 
  @include('layouts/gestion/styles')
  @show

   <link rel="preconnect" href="https://fonts.gstatic.com">
   <link rel="preconnect" href="https://www.googletagmanager.com">

   @if(env('APP_ENV') == 'production')
      <!-- Google tag (gtag.js) --> 
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-CS6566ML44"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-CS6566ML44'); </script>
   @endif
</head>



 <!-- ================================================================== 
                   BODY
     ================================================================== -->
<body class="vertical-layout vertical-menu-modern 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    @include('layouts/gestion/headers')
    <div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
       @include('layouts/gestion/aside_menus')

    </div>

     <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
                @yield('content')
        </div> 
     </div>  

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
     @section('scripts')
     @include('layouts/gestion/scripts')

     @include('layouts/gestion/footer')

    @show

</body>

</html>

