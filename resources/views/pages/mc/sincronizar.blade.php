@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	@parent
	<style type="text/css">
	
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}
</style>
  <link rel="stylesheet" href="{{asset('mC/css/toast.min.css')}}">
@endsection
@section('content')

<section id="base-style">
	@include('flash::message') 

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Sincronizar SIGA-FACTOUR</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

		 
          		<div class="row">
          			<div class="col-12 col-sm-12 col-md-4" style="padding-left: 25px;">
          			</div>
					<div class="col-12 col-sm-12 col-md-2" style="padding-left: 65px;">
						<button type="button" id="btnSincronizar" class="btn btn-success btn-lg"  tabindex="17"><b>SINCRONIZAR</b></button>
					</div>  
					<div class="col-12 col-sm-12 col-md-2" style="padding-left: 65px;">
						<a href="{{route('home')}}" class="btn btn-danger btn-lg" style="padding-left: 6px;padding-right: 6px;width: : 60%;" role="button"><b>VOLVER    </b></a>
					</div> 
				  </div>	
				  
				  

				</div>
			</div>
		</section>
	</section>
	


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script src="{{asset('gestion/app-assets/js/toast.min.js')}}"></script>
	<script>
		$(document).ready(function() {		
			$("#btnSincronizar").click(function(){
					$("#btnSincronizar").prop("disabled", true);
					$.toast({
							heading: 'Success',
							text: 'En proceso, esta actualización puede demorarse varios minutos.',
							position: 'top-right',
							showHideTransition: 'slide',
				    		icon: 'success'
						});
					$.ajax({
							type: "GET",
							url: "{{route('doSincronizarSaldo')}}",
							dataType: 'json',
							data: {
									data: 1,
					       			},
							success: function(rsp){
								
							}	

					});
			});	
		});	
	</script>
@endsection