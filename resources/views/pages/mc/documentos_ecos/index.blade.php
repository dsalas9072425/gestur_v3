
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
	</style>
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Documentos Electronicos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmProforma" method="post">
					<div class="row">
						<!--<div class="col-12">
							<a href="{{ route('cotizacionAdd') }}" class="btn btn-success pull-right" role="button">
								<div class="fonticon-wrap style-icon">
									<i class="ft-plus-circle"></i>
								</div>
							</a>
						</div>-->
						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Documento</label>
								<select class="form-control select2" name="documento" id="documento"
									style="width: 100%;" required>
									<option value="">Seleccione Documento Electronico</option>
									<option value="1">Factura Electronica</option>
									<option value="5">Nota de credito</option>

								</select>
							</div>
						</div>

						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Periodo</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i
												class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="periodo" id="periodo"
										required>
								</div>
							</div>
						</div>
						<div class="col-11 mb-1">
							<button type="button" onclick="buscarCotizacion()" id="btnGuardar" class="btn btn-info btn-lg pull-right mb-1">Buscar</button>
						</div>
					</div>
				</form>


				<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr style="text-align: center;">
								<th>Fecha Emision</th>
								<th>Numero Documento</th>
								<th>Cliente</th>
								<th>RUC</th>
								<th>Tipo Documento</th>
								<th>CDC</th>
								<th>Estado </th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody id="lista_cotizacion" style="text-align: center">
						</tbody>
					</table>
				</div>

				<hr>

			</div>
		</div>
	</section>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
		//$(document).ready(function() {

			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$("#listado").dataTable({
				"aaSorting": [[0, "desc"]],
				"searching": true,
				"processing": true,
				"dom": 'Bfrtip', // Define donde se colocarán los botones
				"buttons": [
					'excel' // Habilita el botón de Excel
				]
			});




			var inicioPeriodo, finPeriodo = 'vacio';

			var Fecha = new Date();
			var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

			//Activar calendario
			$( "#periodo_indice" ).daterangepicker({ 
								   timePicker24Hour: true,
							        timePickerIncrement: 30,
							       locale: {
											format: 'DD/MM/YYYY',
											cancelLabel: 'Limpiar'
													    },
									startDate: "01/"+fechaInicial,
        							endDate: new Date()
									});

			$('input[name="periodo_indice"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

			//Activar calendario
			$( "#periodo" ).daterangepicker({ 
								   timePicker24Hour: true,
							        timePickerIncrement: 30,
							       locale: {
											format: 'DD/MM/YYYY',
											cancelLabel: 'Limpiar'
													    },
									startDate: "01/"+fechaInicial,
        							endDate: new Date()
									});

			$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

			var a = moment().subtract(1, 'day');
			$("#periodo").data('daterangepicker').setStartDate(a);

		function buscarCotizacion() {
			$.blockUI({
				centerY: 0,
				message: "<h2>Procesando...</h2>",
				css: {
					color: '#000'
				}
			});

			$("#frmProforma").validate({
			  submitHandler: function(form) {
			    alert('Enviado');
			  }
			});

			var dataString = $('#frmProforma').serialize();

			$.ajax({
						type: "GET",
						url: "{{route('consultaDocumentosEca')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                            });
								$.unblockUI();

                            },
						success: function(rsp){


							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

							$.each(rsp, function (key, item){
							 console.log(item);
							//formatear fecha	
							var fecha = item.fecha_envio;
							var fechaIntermedia = fecha.split(' ');
							var fechaFinal = fechaIntermedia[0].split('-');
							fechaMostrar = fechaFinal[2]+'/'+fechaFinal[1]+'/'+fechaFinal[0]+' '+fechaIntermedia[1];

							if(item.tipo_documento == 1){
								documento = 'Factura Electronica';
								btn_documento = `<div class="col-10" style="padding-left: 0px;"><a href="{{route('verFactura',['id'=>''])}}/${item.id_documento}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${item.documento_nro}</a><div style="display:none;">${item.id_documento}</div></div>`;
							}else{
								documento = 'Nota de Credito Electronica';
								btn_documento = `<div class="col-10" style="padding-left: 0px;"><a href="{{route('verNota',['id'=>''])}}/${item.id_documento}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${item.documento_nro}</a><div style="display:none;">${item.id_documento}</div></div>`;
							}

							nombre = item.cliente_nombre;
							if(jQuery.isEmptyObject(item.cliente_apellido) == false){
								nombre = nombre+' '+item.cliente_apellido;
							}

							btn = `<a href="${item.url_set}" title="Consulta SET" target="_blank" class="btn btn-info" style="padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-edit"></i></a>`;

							if (item.id_impresion === null) {
								btn_set = '';
						 	}else{
								btn_set = `<a onclick="getkude('${item.cdc}')" title="Descargar DE" class="btn btn-success" style="padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-print"></i></a>`;
							}

							if (item.id_impresion === 'Aprobado / Aprobado') {
								btn_reenviar = ``;
							}else{
								if(item.tipo_documento == 1){
									btn_reenviar = `<a onclick="generarJsonFactura(${item.id_documento})" title="Reenviar DE" target="_blank" class="btn btn-info" style="padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-window-restore"></i></a>`;
								}else{
									btn_reenviar = `<a onclick="generarJsonNC(${item.id_documento})" title="Reenviar DE" target="_blank" class="btn btn-info" style="padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-window-restore"></i></a>`;
								}
							}

							var dataTableRow = [
														'<b>'+fechaMostrar+'</b>',
														btn_documento,
														'<b>'+nombre+'</b>',
														'<b>'+item.cliente_documento+' '+item.cliente_dv+'</b>',
														'<b>'+documento+'</b>',
														'<b>'+item.cdc+'</b>',
														'<b>'+item.estado_documento+'</b>',
														btn,
														btn_set,
														btn_reenviar
													];
								var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

							});
							$.unblockUI();
						}//cierreFunc	
				});	
			
		}

		function buscarIndiceCotizacion() {


				$("#frmIndiceCotizacion").validate({
				submitHandler: function(form) {
					alert('Enviado');
				}
				});

				var dataString = $('#frmIndiceCotizacion').serialize();

				$.ajax({
							type: "GET",
							url: "{{route('consultaIndiceCot')}}",
							dataType: 'json',
							data: dataString,

							error: function(jqXHR,textStatus,errorThrown){

							$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la comunicación con el servidor.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
									});

								},
							success: function(rsp){


								var oSettings = $('#listado_indice').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();
								for (i=0;i<=iTotalRecords;i++) {
									$('#listado_indice').dataTable().fnDeleteRow(0,null,true);
								}

								$.each(rsp.divisa, function (key, item){
									// console.log('ingreso');
								//formatear fecha	
								var fecha = item.created_at;
								var fechaIntermedia = fecha.split(' ');
								var fechaFinal = fechaIntermedia[0].split('-');
								fechaMostrar = fechaFinal[2]+'/'+fechaFinal[1]+'/'+fechaFinal[0]+' '+fechaIntermedia[1];


									var dataTableRow = [
															item.moneda_origen.hb_desc,
															item.moneda_destino.hb_desc,
															fechaMostrar,
															item.indice

														];
									var newrow = $('#listado_indice').dataTable().fnAddData(dataTableRow);
									var nTr = $('#listado_indice').dataTable().fnSettings().aoData[newrow[0]].nTr;

								});

						


							}//cierreFunc	
					});	

			}
				

	function getkude(cdc) {
			$.ajax({
					type: "GET",
					url: "{{ route('generarKudePhp') }}",
					dataType: 'json',
					data: {
							cdc: cdc,
							},
				success: function (rsp) {
					console.log(rsp);
					// Descargar el PDF
					if (rsp.success) {
						var downloadLink = document.createElement('a');
						downloadLink.href = rsp.url; // URL del archivo PDF devuelto por el controlador
						downloadLink.download = rsp.filename; // Nombre de archivo sugerido para la descarga
						downloadLink.click();
					} else {
						console.error('Error al generar el Kude');
					}
				},
				error: function (xhr, status, error) {
				console.error('Error en la solicitud AJAX:', error);
				}
			});
	}
			

	function generarJsonFactura(id) {
			return swal({
						title: "GESTUR",
						text: "¿Está seguro que desea enviar de nuevo el DE de Factura Electronica ?",
						showCancelButton: true,
						buttons: {
								cancel: {
															text: "No",
															value: null,
															visible: true,
															className: "btn-warning",
															closeModal: false,
								},
								confirm: {
															text: "Sí, Enviar",
															value: true,
															visible: true,
															className: "",
															closeModal: false
										}
								}
						}).then(isConfirm => {
									if (isConfirm) {
												$.ajax({
														type: "GET",
														url: "{{route('envioFactura')}}",
														dataType: 'json',
														data: {
																idFactura:id,
															},
														error: function(jqXHR,textStatus,errorThrown){},
														success: function(rsp){
																				console.log(rsp);
																				swal("Éxito", 'El envio de del DE fue exitoso', "success");
																				location.reload();
																			}
													})
										}	
									})	
					}


	function generarJsonNC(id) {
			return swal({
						title: "GESTUR",
						text: "¿Está seguro que desea enviar de nuevo el DE de Nota de Credito Electronica?",
						showCancelButton: true,
						buttons: {
								cancel: {
															text: "No",
															value: null,
															visible: true,
															className: "btn-warning",
															closeModal: false,
								},
								confirm: {
															text: "Sí, Enviar",
															value: true,
															visible: true,
															className: "",
															closeModal: false
										}
								}
						}).then(isConfirm => {
									if (isConfirm) {
												$.ajax({
														type: "GET",
														url: "{{route('envioNC')}}",
														dataType: 'json',
														data: {
																idNC:id,
															},
														error: function(jqXHR,textStatus,errorThrown){},
														success: function(rsp){
																				console.log(rsp);
																				swal("Éxito", 'El envio de del DE fue exitoso', "success");
																				location.reload();
																			}
													})
										}	
									})	
					}




	</script>
@endsection