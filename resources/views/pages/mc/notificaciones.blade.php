@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.table>thead:first-child>tr:first-child>td{
			background-color: #FFF;
		}
	</style>	
@endsection
@section('content')
	<div>
        <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
  <!-- Content Header (Page header) -->
			    <section class="content-header">
			      <h1>
			        Notificaciones
			        <small>({{$total}})</small>
			      </h1>
			      <ol class="breadcrumb">
			      </ol>
			    </section>
			    <!-- Main content -->
			    <section class="content">
			      <div class="row">
			        <div class="col-md-3">
			          <div class="box box-solid">
			            <div class="box-header with-border">
			              <h3 class="box-title">Tipos</h3>

			              <div class="box-tools">
			                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
			                </button>
			              </div>
			            </div>
			            @foreach($cabeceraNoficacion as $key1=>$headerNotify)
			            	<div class="box-body no-padding">
				              	<ul class="nav nav-pills nav-stacked">
					                <li dataLabel="{{$key1}}" class="active"><a href="#"><i class="{{$headerNotify['clase']}}"></i> {{$headerNotify['nombre']}}
					                @if($headerNotify['cantidadNoLeidos'] != 0) 		
					                  	<span class="label label-warning pull-right">{{$headerNotify['cantidadNoLeidos']}}</span></a></li>
					                @endif  	
					                <!-- <li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>
					                <li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li>
					                <li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a>
					                </li>
					                <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>-->
			              		</ul>
			            	</div>
					    @endforeach    
			            <!-- /.box-body -->
			          </div>
			          <!-- /. box -->
			         <!-- <div class="box box-solid">
			            <div class="box-header with-border">
			              <h3 class="box-title">Labels</h3>

			              <div class="box-tools">
			                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
			                </button>
			              </div>
			            </div>
			            <div class="box-body no-padding">
			              <ul class="nav nav-pills nav-stacked">
			                <li><a href="#"><i class="fa fa-circle-o text-red"></i> Important</a></li>
			                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Promotions</a></li>
			                <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Social</a></li>
			              </ul>
			            </div>
			            <!-- /.box-body 
			          </div>-->
			          <!-- /.box -->
			        </div>
			        <!-- /.col -->
			        <div class="col-md-9">
			        <?php $cantidad = 0; ?>
			        @foreach($notificacion as $key1=>$notify)
			        	@if($cantidad == 0)
			          	<div id="divMensaje{{$key1}}" class="box box-primary">
			          	@else
			          	<div id="divMensaje{{$key1}}" style="display: none;" class="box box-primary">
			          	@endif
				            <div class="box-header with-border">
				              <h3 class="box-title">{{$notify['nombre']}}</h3>
				              <div class="box-tools pull-right">
				              </div>
				              <!-- /.box-tools -->
				            </div>
				            <!-- /.box-header -->
				            <div class="box-body no-padding">
				              <div class="table-responsive mailbox-messages">
				                <table class="table table-bordered table-hover" style="width: 100%;">
				                	<thead>
										<tr>
						                    <th></th>
						                    <th sytle="background-color: #ffffff">detallesdetalles</th>
						                    <th sytle="background-color: #ffffff">asuntoasuntoasuntoasuntoasuntoasuntoasuntoasunto</th>
						                    <th sytle="background-color: #ffffff">fechahora</th>
						                    <th sytle="background-color: #ffffff">fechahora</th>
						                </tr>
						            </thead> 
						            <tbody>   
				                  	@foreach($notify['detalles'] as $key2=>$notifyDetails)
				                  		@if($notifyDetails['leido'] == 'N')
						               	 	<tr style="font-weight: 600;">
						               	@else
						               		<tr>
						               	@endif
						                    <td><a href="#"><i class="{{$notifyDetails['tipos']['clase']}}"></i></a></td>
						                    <td>&nbsp<a href="#">{{$notifyDetails['de']}}</a></td>
						                    <td>&nbsp&nbsp{{$notifyDetails['asunto']}}</td>
						                    <td>{{date('d/m/Y h:i', strtotime($notifyDetails['fecha_hora_creacion']))}}</td>
						                    <td><a href="{{route('mc.detalleNotificacion', ['id' =>$notifyDetails->id])}}"><i class="fa fa-file-text-o fa-lg"></i></a>
						                </tr>
						            @endforeach
						            </tbody>
				                </table>
				                <!-- /.table -->
				              </div>
				              <!-- /.mail-box-messages -->
				            </div>
			          	</div>
			          	<?php $cantidad++; ?>
			          	@endforeach    
			          <!-- /. box -->
			        </div>
			        <!-- /.col -->
			      </div>
			      <!-- /.row -->
			    </section>
			    <!-- /.content -->
        </div>
    </div>
   	<!-- Modal Request-->
	<div id="requestModal" class="modal fade" role="dialog">
	<!--<form id="form-asignar-expediente" method="post" action="">-->
	  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h2 class="modal-title titlepage" style="font-size: x-large;">Contenido</h2>
			</div>
		  <div class="modal-body">
		  		<div id="contenido"></div>
		  </div>
		  <div class="modal-footer">
			<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 80px; background-color: #e2076a;" data-dismiss="modal">Cerrar</button>
		  </div>
		</div>
	  </div>
	 <!--</form>-->
	</div>
@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(".active").click(function(){
			$(".box-primary").hide()
			$("#divMensaje"+$(this).attr('dataLabel')).show();
		})	
		$('.table').dataTable({
								"aoColumns": [
								    { "width": "10%", "title": ""},
								    { "width": "20%", "title": "Remitente"},
								    { "width": "40%", "title": "Asunto"},
								    { "width": "20%", "title": "Fecha"},
								    { "width": "10%", "title": ""},
								  ],
								  "order": [[3, 'asc']]
						});
		
	</script>
@endsection