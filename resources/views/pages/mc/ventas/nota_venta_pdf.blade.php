<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Nota Pedido</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			margin: 0;
			padding: 0;
		}

		.container {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 75%;
			margin: 0 auto;
			z-index: 1;
		}


		table {
			width: 100%;

		}

		.b-buttom {
			border-bottom: 1px solid;
		}

		.b-top {
			border-top: 1px solid;
		}

		.b-col {
			border-collapse: collapse;
		}

		.n-1 {
			font-weight: 700;
		}

		.text {
			overflow: hidden;
			/*text-overflow: ellipsis;*/
			white-space: nowrap;
			display: block;
			width: 100%;
			min-width: 1px;
		}

		.f-10 {
			font-size: 11px !important;
		}

		.f-9 {
			font-size: 10px !important;
		}

		.f-8 {
			font-size: 9px !important;
		}

		.f-11 {
			font-size: 12px !important;
		}

		.f-12 {
			font-size: 13px !important;
		}

		.c-text {
			text-align: center;
		}

		.r-text {
			text-align: right;
		}

		.r-text-detalle {
			margin-right: 20px;
		}

		.cabecera {}

		.espacio-10 {
			margin-top: 10px;
		}


		#background {
			/*margin-top:100px;*/
			position: absolute;
			z-index: 0;
			background: white;
			display: block;
			min-height: 50%;
			min-width: 50%;
			color: yellow;
		}

		#bg-text {
			color: lightgrey;
			font-size: 120px;
			transform: rotate(300deg);
			-webkit-transform: rotate(300deg);
		}

		.hidden {
			display: none;
		}

		.border-radius {
			border-radius: 5px 5px 5px 5px;
		}

		.border {
			border: 1px solid;
		}
	</style>
</head>

<body>



	@php


	function formatoFecha($date){
	if( $date != ''){

	$date = explode('-', $date);
	$fecha = $date[2]."/".$date[1]."/".$date[0];
	return $fecha;
	} else {
	return 'N/A';
	}
	}


	function formatoFechaSinHora($date){
	if( $date != ''){

	$date = explode(' ', $date);
	$date = trim($date[0]);
	$date = explode('-', $date);

	$fecha = $date[2]."/".$date[1]."/".$date[0];
	return $fecha;
	} else {
	return 'N/A';
	}
	}


	function formatMoney($num,$currency){
	// dd($f);
	if($currency != 111){
	return number_format($num, 2, ",", ".");
	}
	return number_format($num,0,",",".");

	}


	$facturaOriginal = array( ""=>"Original - Cliente",
	"DUPLICADO"=>"Duplicado - Tesoreria");
	$emailEmpresa = $empresa->email;
	$paginaEmpresa = $empresa->pagina_web;
	$telefonoEmpresa = $empresa->telefono;
	$direccionEmpresa = $empresa->direccion;
	$idEmpresa = $empresa->id;
	$facturaCopia = array("COPIA"=>"Copia - Factura no vàlida");
	$facturaAnulada = array("ANULADO"=>"Copia - Factura no vàlida");
	$factClase = '';
	$recorrido = '';
	$contadorDetalles = 0;
	$maxDetalles = 46;
	$contadorRecorrido = 0;
	$m ="";
	$firma="";
	$fecha_firma="";


	//SE DEFINE EL RECORRIDO DE LAS IMPRESIONES
	//Orignial
	if($imprimir == '1'){

	$recorrido = $facturaOriginal;
	$m = "";
	$corte = 2;

	}
	//copia
	else if ($imprimir == '2'){

	$recorrido = $facturaCopia;
	$m = "COPIA ELECTRONICA DE LA FACTURA, SIN NINGUN VALOR COMERCIAL, CONTABLE O ADMINISTRATIVO";
	$corte = 1;
	}
	//anulado copia
	else if($imprimir == '3'){

	$recorrido = $facturaAnulada;
	$m = "COPIA ELECTRONICA DE LA FACTURA, SIN NINGUN VALOR COMERCIAL, CONTABLE O ADMINISTRATIVO";
	$corte = 1;

	}

	@endphp

	@foreach ($recorrido as $key => $value)

	@php
	$contadorRecorrido++;


	/*-==============================================================
	VALORES DE CABECERA INFO NOTA PEDIDO
	================================================================== */
	$imagenNombre = 'http://'.$_SERVER['HTTP_HOST'].'logoEmpresa/'.$empresa->logo;
	$denominacionEmpresa = strtoupper($empresa->denominacion);
	$rucFactura = $empresa->ruc;
	$num_documento = str_pad($venta_rapida_cab[0]->id, 5, "0", STR_PAD_LEFT);
	$factClase = $value;
	// //CONFIG FACTURA
	$mostrarGravada = $empresa->imprimir_detalle_factura;
	$idMonedaVenta = $venta_rapida_cab[0]->id_moneda;
	if(isset($venta_rapida_cab[0]->formaPago['denominacion'])){
		$formaPago = $venta_rapida_cab[0]->formaPago['denominacion'];
	}else{
		$formaPago = "";
	}
	$concepto_generico = $venta_rapida_cab[0]->concepto_generico;

	/*
	* =================================================================
	* VALORES DE CABECERA DATOS DE NOTA PEDIDO
	* =================================================================
	*/
	$vendedorMostrador = $venta_rapida_cab[0]->vendedor['nombre']." ". $venta_rapida_cab[0]->vendedor['apellido'];
	$fechaEmisionFactura = date('d/m/Y', strtotime($venta_rapida_cab[0]->fecha_venta));
	$nombreRazonSocial = $venta_rapida_cab[0]->cliente['nombre']." ". $venta_rapida_cab[0]->cliente['apellido'];
	$rucCliente = $venta_rapida_cab[0]->cliente['documento_identidad'];
	$direccionFactura = $venta_rapida_cab[0]->cliente['direccion'];
	$direccionDelivery = $venta_rapida_cab[0]->delivery_direccion;
	$deliveryTipo = $venta_rapida_cab[0]->delivery_tipo;
	$telefonoFactura = $venta_rapida_cab[0]->cliente['telefono'];
	$idEmpresa = $venta_rapida_cab[0]->id_empresa;

	if($venta_rapida_cab[0]->id_pedido == ""){
		$idPedido = "";
	}else{
		$idPedido = $venta_rapida_cab[0]->pedido->id_woo_order;
	}



	// /**
	// * =================================================================
	// * PIE DE SERVICIOS Y PRODUCTOS
	// * =================================================================
	// */


	$subTotales = "";
	$subTotalTexto = "";
	$exentaTotal = ($venta_rapida_cab[0]->total_exentas != null) ?
	formatMoney($venta_rapida_cab[0]->total_exentas,$idMonedaVenta) : "0,00";
	$iva5Total = "0,00";
	$iva10Total = ($venta_rapida_cab[0]->total_iva != null) ?
	formatMoney($venta_rapida_cab[0]->total_iva,$idMonedaVenta) : "0,00";
	$total_nota = formatMoney($venta_rapida_cab[0]->total_venta,$idMonedaVenta);
	$moneda = $venta_rapida_cab[0]->currency['hb_desc'];

	// /*====
	// 1 Factura Bruta
	// 2 Factura Neta
	// */



	$letraTotal = $NumeroALetras;
	$total_grabada_5 = 0;
	$total_exenta = formatMoney($venta_rapida_cab[0]->total_exentas,$idMonedaVenta);
	$total_grabada_10 = formatMoney($venta_rapida_cab[0]->total_gravadas,$idMonedaVenta);
	// $totalIva = "12,14";
	// $espacios = 0;







	/*==============================================================
	INICIO DE FACTURA
	================================================================== */


	@endphp








	{{-- <div id="background">
  		<p id="bg-text" style="margin-top: 300px;">1</p>
	</div>    --}}
	<br>
	<br>
	<div class="container espacio-10">
		<table>
			<tr>
				{{-- <td style="width: 15%;" class="c-text">
					
					</td> --}}


				<td style="padding: 4px;" class="c-text f-8 border-radius border">
					<div style="with:50%; float:left; margin-top:8px;">
						<img src='{{$logo}}' alt="{{$denominacionEmpresa}}" style="max-width:120px;max-height:70px">
					</div>

					<div style="with:50%;">
						<span class=""><?php echo $denominacionEmpresa;?></span><br>

						@if($idEmpresa == 1)
						<span class="">OPERADORA MAYORISTA DE TURISMO</span><br>
						@endif
						Telefonos: <?php echo $telefonoEmpresa;?><br>
						E-mail: <?php echo $emailEmpresa;?><br>
						<?php echo $paginaEmpresa;?>
					</div>
				</td>



				<td style="width: 40%; padding: 4px;" class="border-radius border">
						<div style="text-align: center;">
							<span class="n-1">Nota de Pedido</span><br>
									{{$idPedido}}<br>
							<span class="f-10"><b><?php echo $factClase; ?> </b></span>
						</div>
				</td>
			</tr>

		</table>

		<table class="b-buttom b-top">
			<tr>
				<td class="f-8" style="padding: 0 0 0 10px;">
					{{-- @foreach($sucursalEmpresa as $sucursales) 
					<b>:</b> {{$sucursales->direccion}}. Telefonos: {{$sucursales->telefono}}<br>
					@endforeach --}}

					<b>Casa Central:</b> Gral Bruguez 353 casi 25 de Mayo, Asunción - Paraguay. Telefonos: 021-221-816
					Fax: 449-724<br>
				</td>
			</tr>
		</table>


		<table class="f-9">
			<tr>
				<td>
					Vendedor Empresa: <?php echo $vendedorMostrador;?>
				</td>
			</tr>
		</table>


		<table class="f-9 border border-radius" style="padding:4px;">

			<colgroup>
				<col style="width: 10%" />
				<col style="width: 10%" />
				<col style="width: 10%" />
				<col style="width: 10%" />
				<col style="width: 10%" />
				<col style="width: 10%" />
				<col style="width: 10%" />
				<col style="width: 10%" />
				<col style="width: 10%" />
				<col style="width: 10%" />

			</colgroup>

			<tr>
				<td colspan="7">
					<b>Fecha de Emision:</b> <?php echo $fechaEmisionFactura; ?>
				</td>
				<td colspan="3">
					<b>Nro.:</b> <?php echo  $num_documento?>
				</td>
			</tr>

			<tr>
				<td colspan="7">
					<b>Nombre / Razon Social:</b> <?php echo $nombreRazonSocial; ?>
				</td>

				<td colspan="3">
					<b>RUC:</b> <?php echo $rucCliente; ?>
				</td>

			</tr>


			<tr>
				<td colspan="7">
					<b>Dirección:</b> <?php echo $direccionFactura; ?>
				</td>

				<td colspan="3">
					<b>Telefono:</b> <?php echo $telefonoFactura; ?>
				</td>

			</tr>
			<tr>
				<td colspan="7">
					<b>Direc. Envio:</b> <?php echo $direccionDelivery; ?>
				</td>

				<td colspan="3">
					<b>Medio Envio:</b> <?php echo $deliveryTipo; ?>
				</td>
			</tr> 
			<tr>
				<td colspan="7">
					
				</td>

				<td colspan="3">
					<b>Forma Pago:</b> <?php echo $formaPago; ?>
				</td>
			</tr> 
			<tr>

			</tr>

		</table>

		<table class="c-text b-col f-10" border="1">

			<colgroup>
				<col />
				<col />
				<col />
				<col />
			
			</colgroup>

			<tr>
				<td width="20%">
					Cod
				</td>

				<td width="10%">
					Cant
				</td>
				<td width="25%">
					Proveedor
				</td>
				<td width="45%">
					Descripción Producto
				</td>

			</tr>

		</table>

		<table class="" FRAME="vsides" RULES="cols" style="border-bottom: 1px solid; ">

			<colgroup>
				<col />
				<col />
				<col />
				<col />
			</colgroup>



			{{-- ===================================
			AGREGAR SERVICIOS Y ESPACIOS
		=================================== --}}

			@foreach($venta_rapida_det as $detalles)

			@php
			//Si es de tipo Ticket agregar codigo confirmacion
			$descripcion = $detalles->item; 
			$desc = $descripcion;


			$longitud = strlen($descripcion);
			$desc = wordwrap($descripcion,50,'<br />',true);
			if(isset($detalles->proveedor->nombre)){
				$proveedor = $detalles->proveedor->nombre." ".$detalles->proveedor->apellido;

		    }else{
				$proveedor ="";
			}
			@endphp

			{{-- VALIDAR SI EL PRODUCTO SE PUEDE IMPRIMIR EN FACTURA --}}
			@if($contadorDetalles <= 40 && $detalles->producto['imprimir_en_factura'] == 1)

				<tr valign="top" class="f-9" nowrap>


					<td class="c-text" width="20%">
						@php
						if($detalles->nro_factura_proveedor){
							echo $detalles->nro_factura_proveedor;
						}else{
							echo '-';
						}
						@endphp
 					</td>

					<td class="c-text" width="10%">
						{{$detalles->cantidad}}
					</td>
					<td class="c-text" width="25%">
						{{$proveedor}}
					</td>

					<td width="45%">
						<?php echo $desc; ?>
					</td>

				</tr>

				<?php
			 $contadorDetalles++;
			 ?>


				@endif





				@endforeach


				{{-- AGREGAR ESPACIOS EN BLANCO PARA COMPLETAR 15 LINEAS EN TOTAL --}}
		<?php 
		$espacios = 0;
		if($contadorDetalles < $maxDetalles){
			$espacios = $maxDetalles - $contadorDetalles;
		}

		for ($i=0; $i < $espacios ; $i++):?>

				<tr valign="top" class="f-11">


					<td class="c-text" width="20%">
						&nbsp;
					</td>

					<td class="c-text" width="10%">

					</td>

					<td class="c-text" width="25%">

					</td>

					<td width="45%">

					</td>

				</tr>

				<?php endfor;?>


				<tr valign="top" class="f-10">
					<td class="c-text" colspan="4"> SIN NINGUN VALOR COMERCIAL, CONTABLE O ADMINISTRATIVO</td>
				</tr>

				
				{{-- <tr valign="top" class="f-10">
					<td class="c-text align-left" colspan="4"> FIRMA:_________________ Aclaracion:____________________</td>
				</tr>
				<tr valign="top" class="f-10">
					<td class="c-text align-left" colspan="4">Fecha/Hora:____________________</td>
				</tr> --}}

				<tr valign="top" class="f-10">


					<td class="c-text" width="20%">
						&nbsp;
					</td>

					<td class="c-text" width="10%">

					</td>
					<td class="c-text" width="25%">

					</td>
					<td width="45%">

					</td>

				</tr>
				
		
		</table>
		<table class="f-9 border border-radius" style="padding:20px;">
		
			<tr>
				<td colspan="3" style="font-weight: bold;">
					<span style="font-size: 12px;">FIRMA:</span>_________________ <span style="font-size: 12px;">Aclaracion:</span>____________________
				</td>
				<td colspan="3" style="font-weight: bold;">
					<span style="font-size: 12px;">Fecha/Hora:</span>____________________
				</td>
			</tr>
		</table>
		
		<!--<table style="border-bottom: 1px solid; margin-top: 10px;">
firma
			<colgroup>
				<col />
				<col />
				<col />
				<col />
			</colgroup>

		</table>-->
	</div>

	{{-- CONTAINER --}}
	@if($contadorRecorrido < $corte) <div>
		<div style="page-break-after:always;"></div>
		</div>
		@endif


		<?php 
//RESETEO CONTADOR
$contadorDetalles = 0;
?>




		@endforeach


</body>

</html>
