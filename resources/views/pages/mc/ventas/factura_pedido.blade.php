<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Factura</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 90%;
			margin:0 auto;
    		z-index:1;
	}
	
	table{
		width: 100%;

	}
	
	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 550;
	}
	.f-6 {
		font-size: 6px !important;
	}
	.f-7 {
		font-size: 7px !important;
	}


	.n-2{
		font-weight: 400;
	}
	.text{
		  overflow: hidden;
		  /*text-overflow: ellipsis;*/
		  white-space: nowrap;
		  display:block;
		  width:100%;
		  min-width:1px;
		}

	.f-10 {
		font-size: 10px !important;
	}
	.f-5 {
		font-size: 5px !important;
	}

	.f-8 {
		font-size: 8px !important;
	}
	.f-9 {
		font-size: 9px !important;
	}
	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}
	.c-text {
		text-align: center;
	}
	.r-text {
		text-align: right;
	}
	.r-text-detalle{
		margin-right: 20px;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 10px;
	}


	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:80px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}



	</style>
</head>
<body>



@php	


 	function formatoFecha($date){
		if( $date != ''){

		$date = explode('-', $date);
		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
		 	return $fecha;
		} else {
			return 'N/A';
		}
	}


	function formatoFechaSinHora($date){
		if( $date != ''){

		$date = explode(' ', $date);
		$date = trim($date[0]);
		$date = explode('-', $date);

		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
		 	return $fecha;
		} else {
			return 'N/A';
		}
	}

		
	function formatMoney($num,$currency){
		// dd($f);
		if($currency != 111){
		return number_format($num, 2, ",", ".");	
		} 
		return number_format($num,0,",",".");

	}
	

	if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1){
	    if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_impresion == 1){
			$facturaOriginal = array( ""=>"Original - Cliente",
								  	  "DUPLICADO"=>"Duplicado - Tesoreria");	
		}else{
			$facturaOriginal = array(""=>"Original - Cliente",
								  	  "DUPLICADO"=>"Duplicado - Tesoreria",
								  	  "TRIPLICADO"=>"TRIPLICADO");
		}
	}else{
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 3){
			$facturaOriginal = array( ""=>"Original - Cliente");
		}else{
			if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_impresion == 1){
			$facturaOriginal = array( ""=>"Original - Cliente",
								  	  "DUPLICADO"=>"Duplicado - Tesoreria");	
			}else{
				$facturaOriginal = array(""=>"Original - Cliente",
										"DUPLICADO"=>"Duplicado - Tesoreria",
										"TRIPLICADO"=>"TRIPLICADO");
			}		
		}

	}

	$emailEmpresa = $empresa->email; 
	$paginaEmpresa = $empresa->pagina_web;
	$telefonoEmpresa = $empresa->telefono;
	$direccionEmpresa = $empresa->direccion;
	$idEmpresa = $empresa->id; 
	$facturaCopia =   array("COPIA"=>"Copia - Factura no vàlida");
	$facturaAnulada = array("ANULADO"=>"Copia - Factura no vàlida");
	$factClase = '';
	$recorrido = '';
	$contadorDetalles = 0;
	$contadorRecorrido = 0;
	$maxDetalles = 7;
	$m ="";


//SE DEFINE EL RECORRIDO DE LAS IMPRESIONES
	//Orignial
if($imprimir == '1'){

$recorrido  = $facturaOriginal;
$m = "";
$corte = 1;

}
	//copia
else if ($imprimir == '2'){

	$recorrido = $facturaCopia;
	$m = "COPIA ELECTRONICA DE LA FACTURA, SIN NINGUN VALOR COMERCIAL, CONTABLE O ADMINISTRATIVO";
	$corte = 1;
}
	//anulado copia
else if($imprimir == '3'){

	$recorrido = $facturaAnulada;
	$m = "COPIA ELECTRONICA DE LA FACTURA, SIN NINGUN VALOR COMERCIAL, CONTABLE O ADMINISTRATIVO";
	$corte = 1;

}else if($imprimir == '4'){
	$facturaOriginal = array( ""=>"Original - Cliente");
	$recorrido  = $facturaOriginal;
	$m = "";
	$corte = 1;

}


@endphp

@foreach ($recorrido as $key => $value)

@php
$contadorRecorrido++;
	

	/*-==============================================================
					VALORES DE CABECERA INFO FACTURA
	==================================================================  */
	$imagenNombre = $empresa->logo;
	$denominacionEmpresa = strtoupper($empresa->denominacion); 
	
	$timbradoNum = $factura[0]->timbrado->numero;
	$timbradoAut = $factura[0]->timbrado->autorizacion;
	$timbradoFA = date('d/m/Y', strtotime($factura[0]->timbrado->fecha_autorizacion)); 

	$fechaInicioVigencia =  formatoFecha($factura[0]->timbrado->fecha_inicio);
	$fechaFinVigencia =  formatoFecha($factura[0]->timbrado->vencimiento);
	$rucFactura = $empresa->ruc;
	$numFactura = $factura[0]->nro_factura;

	$factClase = $value;
	//CONFIG FACTURA
	$mostrarGravada = $empresa->imprimir_detalle_factura;
	$idMonedaVenta = $factura[0]->id_moneda_venta;
	$pieFactura_txt = $empresa->pie_factura_txt;

	/*
	 * =================================================================
	 * 				VALORES DE CABECERA DATOS DE FACTURA
	 * ================================================================= 
	 */

	$vendedorDtp = $vendedorEmpresa[0]->nombre;
	// $vendedorAgencia  = $factura[0]->vendedorEmpresa['nombre']." ". $factura[0]->vendedorEmpresa['apellido'];
	
	$fechaEmisionFactura = date('d/m/Y', strtotime($factura[0]->fecha_hora_facturacion));
	$fechaVencimientoFactura = formatoFecha($factura[0]->vencimiento);
	$tipoFactura = $factura[0]->tipoFactura['denominacion'];
	$nombreRazonSocial = $factura[0]->cliente['nombre']." ". $factura[0]->cliente['apellido'];
	$documento = $factura[0]->cliente['documento_identidad'];
	$dvbase = '';
	$controlar = strpos($documento, '-');
	if ($controlar === false) {
		if($factura[0]->cliente['dv'] !=""){
			$dvbase = "-".$factura[0]->cliente['dv'];
		}
	}
	$rucCliente = $documento.''.$dvbase;
	$direccionFactura = $factura[0]->cliente['direccion'];
	$telefonoFactura = $factura[0]->cliente['telefono'];
	$totalFactura = $factura[0]->total_neto_factura;
	$idEmpresa = $factura[0]->id_empresa;
	$concepto_generico = $factura[0]->concepto_generico;

	/**
	 * =================================================================
	 * 				PIE DE SERVICIOS Y PRODUCTOS
	 * =================================================================
	 */ 

	
	$subTotales = "";
	$subTotalTexto = "";
	$exentaTotal =  ($factura[0]->total_exentas != null) ? formatMoney($factura[0]->total_exentas,$idMonedaVenta) : "0,00";
	$iva5Total = "0,00";
	$iva10Total = ($factura[0]->total_gravadas != null) ? formatMoney($factura[0]->total_gravadas,$idMonedaVenta) : "0,00";
	$total = formatMoney($totalFactura,$idMonedaVenta);
	$moneda = $factura[0]->currency['hb_desc'];
	$ventaId = $factura[0]->id_venta_rapida;
	/*====
	1 Factura Bruta
	2 Factura Neta
	*/


	
	$letraTotal = $NumeroALetras;
	$liquidacionIva10 =  formatMoney($factura[0]->total_iva,$idMonedaVenta);
	$totalIva = "12,14";
	$espacios = 0;



	/*==============================================================
						INICIO DE FACTURA
	 ==================================================================  */

	
@endphp





	

	
	 <div id="background">
  		 <p id="bg-text" style="margin-top: 100px; margin-left: 10px"><?php echo $key  ?></p> 
    </div> 
    

<div class="container espacio-10" style="margin-top: 20px;">
	<table>
			<tr>
					<td style="width: 26%;" class="c-text">
						<img src='{{$logo}}' alt="{{$denominacionEmpresa}}" width="180">
					</td>

					<td style="width: 40%; padding: 5px;" class="c-text f-9">
						<span class="f-12"><b><?php echo $denominacionEmpresa;?></b></span><br>
						@if($idEmpresa == 6)
							<span class="f-9">Comercio al menor de otros productos en comercios no especializados</span><br>
							<span class="f-8">Avda. General Maximo Santos casi Concordia</span><br>
							<span class="f-6">Asuncion Paraguay - Tel:<?php echo $telefonoEmpresa;?></span><br>
						@elseif($idEmpresa == 3)	 
							<span class="f-9"><b>Otras actividades de limpieza de edificios e industrial</b></span><br>
							<span class="f-6">Todo lo necesario para cuidar piscinas.</span><br>
							<span class="f-6">Gama completa de productos con calidad garantizada. Servicios para mantenimiento y cuidado de piscinas.</span><br>
						@elseif($idEmpresa == 15)
							<span class="f-7"><b>De: Franqui S.A.</span><br>
							<span class="f-9">Actividades de consultoría y gestión de servicios informáticos</span><br>
							<span class="f-9">Comercio al por mayor de equipos informáticos y software</span><br>
							<span class="f-9">Comercio al por menor de otros artículos N.C.P.</span><br>
							<span class="f-6"><b>Nuestra Señora del Carmen 1330 - Edificio del Carmen</b></span><br>
							<span class="f-6"><b>021 729-7070</b></span><br>
						@elseif($idEmpresa == 19)
							<span class="f-9">Otros tipos de enseñanza N.C.P</span><br>
							<span class="f-9">Edicion de Libros incluso integrada a al impresiòn</span><br>
							<span class="f-9">Comercio al por menor de otros artículos N.C.P.</span><br>
							<span class="f-6"><b>Avd. Peru c/España</b></span><br>
						@endif

						@if($idEmpresa != 6 && $idEmpresa == 3) E-mail: <?php echo $emailEmpresa;?><br> @endif
						<?php echo $paginaEmpresa;?>
						<br>
						@if($idEmpresa != 6 && $idEmpresa == 3) 
							@foreach($sucursalEmpresa as $sucursales) 
								<span class="f-5">
								<b><?php echo ucwords(strtolower($sucursales->nombre)); ?>:</b> {{$sucursales->direccion}}. Telefonos: {{$sucursales->telefono}}</span><br>
							@endforeach 
						@endif	
					</td>

					<td style="width: 42%;">
						<div style="padding: 3px;">
							<div style="border:1px solid; text-align: center; padding: 5px;">
								<span class="n-2">Timbrado Nro:<span id="numTimbrado"><?php echo $timbradoNum; ?></span></span><br>
								<span class="f-6">Autorizado como autoimpresor por la SET.N° Solicitud:<span><?php echo $timbradoAut; ?></span> Fecha: <span><?php echo $timbradoFA; ?></span></span><br>
								<span class="f-8">Fecha Inicio Vigencia: <?php echo $fechaInicioVigencia; ?></span><br>
								<span class="f-8">Fecha Fin Vigencia: <?php echo $fechaFinVigencia; ?></span><br>
								<span class="n-2">R.U.C <?php echo $rucFactura; ?></span><br>
								<span class="n-2">FACTURA Nro.: <?php echo $numFactura;?></span><br>
								<span class="f-8"><b> <?php echo $factClase; ?> </b></span>
							</div>

						</div>
					</td>
		</tr>

	</table>

	<!-- {{--<table style="margin-top: 10px;"  class="b-buttom b-top">
		<tr>
			<td class="f-9" style="padding: 0 0 0 20px;">
				<br>
				@foreach($sucursalEmpresa as $sucursales) 
					<b><?php echo ucwords(strtolower($sucursales->nombre)); ?>:</b> {{$sucursales->direccion}}. Telefonos: {{$sucursales->telefono}}<br>
				@endforeach
				<br>
				{{-- <b>Casa Central:</b> Gral Bruguez 353 casi 25 de Mayo, Asunción - Paraguay. Telefonos: 021-221-816  Fax: 449-724<br>
				<b>Sucursal Ciudad del Este:</b> Avda Pioneros del Este entre Adrian Jara y Pa'i Perez, Galeria JyD. Ciudad del Este - Paraguay. Telefono: 061-511-779 al 80<br>
				<b>Sucursal Encarnación:</b> Mcal Estigarriba 1997. Galeria San Jorge, Local 13 y 16. Encarnación - Paraguay. Telefono: 071-200-104 --}}
			</td>
		</tr>
	</table>--}} -->


	<!-- {{--<table class="f-10" style="margin-top: 20px;">
		<tr>
			<td>
				Vendedor Empresa: <?php echo $vendedorDtp; ?>
			</td>
			<td>
			</td>
			<td>
				Fecha Vencimiento: <?php echo $fechaVencimientoFactura; ?>
			</td>
		</tr>
	</table> --}} -->


	<table class="f-10"  style="border:1px solid">

		<colgroup>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
	
		</colgroup>

		<tr>
			<td colspan="3">
				<b>Vendedor Empresa:</b> <?php echo $vendedorDtp; ?>
			</td>

			<td colspan="3">
				<b>Vencimiento:</b> <?php echo $fechaVencimientoFactura; ?>	
			</td>

			<td colspan="3">
				<b>Tipo Factura:</b> <?php echo $tipoFactura; ?>
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<b>Fecha de Emision:</b> <?php echo $fechaEmisionFactura; ?>
			</td>

			<td colspan="3">
			
			</td>

			<td colspan="3">
				<b>Venta N°:</b> <?php echo $ventaId; ?>
			</td>
		</tr>


		<tr>
			<td colspan="6">
				<b>Nombre / Razon Social:</b> <?php echo $nombreRazonSocial; ?>
			</td>

			<td colspan="3">
				<b>RUC:</b> <?php echo $rucCliente; ?>
			</td>

		</tr>


		<tr>
			<td colspan="6">
				<b>Dirección:</b> <?php echo $direccionFactura; ?>
			</td>

			<td colspan="3">
				<b>Telefono:</b> <?php echo $telefonoFactura; ?>
			</td>

		</tr>

		<tr>
			<td colspan="4">
				
			</td>

			<td colspan="2">
				
			</td>

			<td colspan="2">
				
			</td>

			<td colspan="2">
				
			</td>
		</tr>

	</table>


	<!--<table style="margin-top: 10px;">

		<colgroup>
			<col style="width: 50%"/>
			<col style="width: 50%"/>
		</colgroup>
		<tr>
			<td>
				Detalles
			</td>
			<td>
				Valor de Ventas
			</td>
		</tr>

	</table>-->

	<table class="c-text b-col f-11" border="1">

	<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>


	
		</colgroup>


		<tr>
			<td width="5%">
				<b>Cod</b>
			</td>

			<td width="5%">
				<b>Cant</b>
			</td>
			<td width="32%">
				<b>Descripción Producto</b>
			</td>
			<td width="9%">
				<b>Pre. Unitario</b>
			</td>

			<td width="9%">
				<b>Exentos</b>
			</td>

			<td width="9%">
				<b>Iva 5%</b>
			</td>

			<td width="9%">
				<b>Iva 10%</b>
			</td>
		</tr>

		</table>
			
		<table class=""  FRAME="vsides" RULES="cols" style="border-bottom: 1px solid; ">

		<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
		</colgroup>

		

		{{-- ===================================
			AGREGAR SERVICIOS Y ESPACIOS
		=================================== --}}
		@if(empty($concepto_generico))
		
			@foreach($facturaDetalle as $detalles)

			@php
			//Si es de tipo Ticket agregar codigo confirmacion
			if($detalles->descripcion != ""){
				$descripcion = $detalles->descripcion;
			}else{
				$descripcion = $detalles->producto->denominacion;
			}
			$desc = wordwrap($descripcion,50,'<br />',true);
			$precio_unitario = (float)$detalles->precio_venta / $detalles->cantidad;

			@endphp
			
			{{-- VALIDAR SI EL PRODUCTO SE PUEDE IMPRIMIR EN FACTURA --}}
			@if($contadorDetalles <= 15 && $detalles->producto->imprimir_en_factura == 1)
			
			<tr valign="top" class="f-10" nowrap>
			

				<td class="c-text" width="5%">
					{{$detalles->id_producto}}
				</td>

				<td class="c-text" width="5%">
					{{$detalles->cantidad}}
				</td>

				<td width="32%" >
					<?php echo $desc; ?>
				</td>
				<td class="r-text" width="9%">
					<div class="r-text-detalle"><?php if($mostrarGravada){ echo ($detalles->exento != null) ? formatMoney($precio_unitario,$idMonedaVenta): ''; } ?><div>
				</td>
				<td class="r-text" width="9%">
					<div class="r-text-detalle"><?php if($mostrarGravada){ echo ($detalles->exento != null) ? formatMoney($detalles->exento,$idMonedaVenta): ''; } ?><div>
				</td>
				<td class="c-text" width="9%">
					<div class="r-text-detalle"><?php if($mostrarGravada){ echo 0; } ?><div>
				</td>
				<td class="r-text" width="9%">
					<div class="r-text-detalle"><?php if($mostrarGravada){ echo ($detalles->gravadas_10 != null) ? formatMoney($detalles->gravadas_10,$idMonedaVenta): ''; } ?><div>
				</td>

			</tr>

				<?php
				$contadorDetalles++;
				?>


				@endif
			@endforeach
		@else
			<tr valign="top" class="f-10" nowrap>
			
				<td class="c-text" width="5%">
					-
				</td>

				<td class="c-text" width="5%">
					1
				</td>

				<td width="32%" >
					<?php echo $concepto_generico; ?>
				</td>
				<td class="r-text" width="9%">
					<div class="r-text-detalle"><div>
				</td>
				<td class="r-text" width="9%">
					<div class="r-text-detalle"><div>
				</td>
				<td class="c-text" width="9%">
					<div class="r-text-detalle"><div>
				</td>
				<td class="r-text" width="9%">
					<div class="r-text-detalle"><div>
				</td>

			</tr>

			<?php
				$contadorDetalles++;
            ?>
		@endif

		{{-- AGREGAR ESPACIOS EN BLANCO PARA COMPLETAR 15 LINEAS EN TOTAL --}}
		<?php 
		if($contadorDetalles < $maxDetalles){
			$espacios = $maxDetalles - $contadorDetalles;
		}

		for ($i=0; $i < $espacios ; $i++):?>

			<tr valign="top" class="f-10">
		

			<td class="c-text" width="5%">
				&nbsp;
			</td>

			<td class="c-text" width="5%">
				
			</td>

			<td width="32%">
				
			</td>

			<td class="c-text" width="9%">
				
			</td>

			<td class="c-text" width="9%">
				
			</td>

			<td class="c-text" width="9%">
				
			</td>

			<td class="c-text" width="9%">
				
			</td>


		</tr>
			
		
	<?php endfor;?>
		
		
		<tr valign="top" class="f-12">
			<td class="c-text" colspan="6"> <?php echo $m; ?></td>
		</tr>
	
		<tr valign="top" class="f-10">
		

			<td class="c-text" width="5%">
				&nbsp;
			</td>

			<td class="c-text" width="5%">
				
			</td>

			<td width="32%">
				
			</td>
			<td class="r-text" width="9%">
				<span class="r-text-detalle">
					@if(!empty($concepto_generico))
						<?php echo $iva10Total; ?>
					@endif
				</span>
			</td>
			<td class="r-text" width="9%">
				<span class="r-text-detalle"><?php echo $exentaTotal; ?></span>
			</td>

			<td class="r-text" width="9%">
				<span class="r-text-detalle"><?php echo $iva5Total; ?></span>
			</td>

			<td class="r-text" width="9%">
				<span class="r-text-detalle"><?php echo $iva10Total; ?></span>
			</td>


		</tr>
		


		

	</table>


	<table style="border-bottom: 1px solid; margin-top: 10px;">

		<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
		</colgroup>


		<tr>

			<td style="width: 10%;" class="f-9">
				<b>SUB-TOTALES:</b>
			</td>

			<td class="c-text f-9" width="40%">
					********** Esta Factura será cancelada en <?php echo $moneda;?> **********
			</td>
				
			<td  style="width: 10%;" class="r-text f-9">
				<span class="c-text-detalle"><?php echo $exentaTotal; ?></span>
			</td>

			<td  style="width: 10%;" class="r-text f-9">
				<span class="r-text-detalle"><?php echo $iva5Total; ?></span>
			</td>
			</td>

			<td  style="width: 10%;" class="r-text f-9">
				<span class="r-text-detalle"><?php echo $iva10Total; ?></span>
			</td>
			</td>

		</tr>

	
	</table>

	<table  style="border-bottom: 1px solid;">
			<tr>

			<td class="f-10">
				<b>TOTAL DE FACTURA:</b> &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $total."       ".$moneda;?>
			</td>

			
		</tr>
	</table>

	<table  style="border-bottom: 1px solid;">
		<tr>
			<td class="f-10">
				son <?php echo $moneda."    ".$letraTotal;?> 
			</td>
		</tr>
	</table>

	<table   style="border-bottom: 1px solid;">
		<tr>
			<td class="f-10" >
				<b>LIQUIDACÍÓN IVA 10%:</b> <span> <?php echo $liquidacionIva10; ?></span>
			</td>	
			
			<td class="f-10">
				<b>LIQUIDACÍÓN IVA 5%:</b> <span> 0,00</span>
			</td>
			
			
			<td class="f-10">
				<b>TOTAL IVA:</b> <span> <?php echo $liquidacionIva10; ?></span>
			</td>
			
		</tr>
		
	</table>

	<table>

		<tr>
			<!--SI ES FCTURA BRUTA -->
			@if($factura[0]->id_tipo_facturacion == 1)
			<td class="f-10">
			
			</td>
			@endif
		</tr>
		
	</table>


<table style="border:1px solid; margin-top: 5px;">
	<tr>
		<td class="f-5" style="padding: 5px;">
			{!! $pieFactura_txt !!}

		</td>
	</tr>
</table>

 @php 
if($contadorRecorrido == 1){
	if($contadorDetalle <= 8){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_impresion == 2){
			echo '<div style="page-break-after:always;"></div>';
		}else{
			echo '<br>----------------------------------------------------------------------------------------------------------------------------------';		
		}
	}else{
			echo '<div style="page-break-after:always;"></div>';
	}
}elseif($contadorRecorrido == 2&& Session::get('datos-loggeo')->datos->datosUsuarios->tipo_impresion != 1){
	echo '<div style="page-break-after:always;"></div>';
}
 @endphp

 
</div>
{{-- CONTAINER --}}

<!--<div style="page-break-after:always;"></div>-->




<?php 
//RESETEO CONTADOR
$contadorDetalles = 0;
?>

	{{--==============================================================
						FIN DE FACTURA
		==================================================================  --}}
	<?php
	//RESETEO CONTADOR
	$contadorDetalles = 0;
	?>


@endforeach


</body>
</html>
