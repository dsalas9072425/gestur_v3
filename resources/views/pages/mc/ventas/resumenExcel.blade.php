@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	
	<style>
		.asistencia{
			display: none;
		}
	 	.error{
	 		color:red;
	 	},
	   	.cabeceraClass {
	   		padding: 10px;
	   		min-width: 250px;
	   		background-color:#E8EAEA;
	   		word-break: break-all;
	   		margin: 10px;

	   	},
		.modal-footer {
		    text-align: left;
		} 
		.nav-tabs>li>a {
		    color: #dacdcd;
		} 
		.base{
			cursor: pointer;
		}

		.modal-bodyProforma{
			  height:250px;
             overflow:auto;
			}

		.commentNone {
			display: none;
		}
		.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

		.text-bold{
			font-weight: 800;
			font-size: 15px;
		}

		.select2-selection {
	    	max-height: 30px !important;
		}

		.select2-selection--single{
			padding-top: 0px !important;	
		}

		.select2-selection__rendered{
			padding-right: 5px;
		}

		.note-editing-area{
    		height: 300px;
		}

	    .chat-content {
	        text-align: left;
	        float: left;       
	        position: relative;
	        display: block;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #404e67;
	        background-color: #edeef0;
	        border-radius: 4px;
	    }

		.select2-selection {
			 padding-right: 0px;
		}

	    .chat-content-left {
	        text-align: right;
	        position: relative;
	        display: block;
	        float: right;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #fff;
	        background-color: #00b5b8;
	        border-radius: 4px;
	    }  
	/*=============================================================================*/

	.valoracion {
	    position: relative;
	    overflow: hidden;
	    display: inline-block;
	}

	.valoracion input {
	    position: absolute;
	    top: -100px;
	}


	.valoracion label {
	    float: right;
	    color: #c1b8b8;
	    font-size: 30px; 
	}

	.valoracion label:hover,
	.valoracion label:hover ~ label,
	.valoracion input:checked ~ label {
	    color: #E2D532;
	}

	.select2-moneda_compra_0-container{
		padding-right: 10px;
    	padding-left: 4px;
	}

	modal-dialog modal-lg{
		margin : 0 auto;
	}

	.modal-lg{
		margin-left: 10%;
	}

/*=============================================================================================*/
	.btnAdjuntar{
			/* background-color: #00a65a !important;
			border-color: #008d4c !important; 
			color:#FFFF !important;*/s
			font-weight:800 !important;  
			padding:10px !important;
	}


	.verificarStyle {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}
	
	.btnSolicitarVerificacion {
		padding:10px !important;
		/* background: #2d3e52 !important;
		border-color: #2d3e51 !important;
		color:#FFFF !important; */
	}

	.btnModificar {
		/* background: #e2076a !important; 
		border-color: #e25a7d !important;  */
		padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnAnular{
		/* background-color: #dd4b39 !important;
    	border-color: #d73925 !important; */
    	padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnHand {
		cursor:pointer; 
		cursor: hand;
	}

	.btnHand:hover {
  opacity: 0.8;
}


.vuelosClass{
   padding: 5px 0px;
}
/*#vuelos th {
	padding-left: 0px;padding-right: 0px;
}*/
.select2-container--default .select2-selection--single{
    padding-left: 0px;
}

.select2-moneda_compra_0-container{
	    padding-left: 0px;
	    padding-right: 0px;
}
		  
	</style>
		
@endsection
@section('content')

    <!-- Main content -->
<section id="base-style">
   	<div class="card-content">
   		@include('flash::message')
            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">Actualizar Producto</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        </ul>
                    </div>
                </div> 
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-bottom: 10px;padding-top: 10px;">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<table id="lista_productos" class="table">
									<tr>
										<td><b>Cantidad de Productos del Proveedor:</b></td><td>{{$totalRegistros}}</td><td></td>
									</tr>	
									<tr>
										@php 
										$totalExcel = intval($insertados)+intval($noInsertados);
										@endphp
										<td><b>Cantidad de Productos del Archivo:</b></td><td>{{$totalExcel}}</td><td></td>
									</tr>	
									<tr>
										<td><b>Cantidad de Productos Actualizados:</b></td><td>{{$insertados}}</td><td><button id="botonInsertados" class="btn btn-primary" data-toggle="modal" data-target="#requestModalInsertados" type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-id-card-o"></i>Detalles</button></td>
									</tr>	
									<tr>
										<td><b>Cantidad de Productos No Actualizados:</b></td><td>{{$noInsertados}}</td><td><button id="botonNoInsertados" class="btn btn-primary" data-toggle="modal" data-target="#requestModalNoInsertados" type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-id-card-o"></i>Detalles</button></td>
									</tr>	
						        </table> 
							</div> 
						</div>   
                    </div>
                </div>
            </div>
	</div>
</section>	


<!-- ========================================
   					MODAL DE PRODUCTOS INSERTADOS
   		========================================  -->		
	<div id="requestModalInsertados" class="modal fade" role="dialog">
	  	<div class="modal-dialog" style="width: 30%;margin-top: 1%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Productos Actualizados</h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
				   	<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-12">
  							<div class="table-responsive">
                                <table id="productosInsertados" class="table">
                                    <thead>
                                       <tr style="background-color: #eceff1;">
                                            <th style="width: 60px;">Cant.</th>
                                            <th style="width: 18%;">Codigo</th>
                                            <th style="width: 18%;">Denominacion</th>
                                        </tr>
                                    </thead>
                                    <tbody  style='font-size: 0.85rem;'>
                                    	@foreach($arrayInsertados as $insertados)
                                    		@php 
                                    			$datos= explode("-",$insertados)
                                    		@endphp
											<tr style="background-color: #eceff1;">
	                                            <th>{{$datos[0]}}</th>
	                                            <th>{{$datos[1]}}</th>
	                                            <th>{{$datos[2]}}</th>
	                                        </tr>                                    	
                                        @endforeach
                                    </tbody>

                                </table>    	
                            </div>							
                        </div>
					</div>
					<br>
					<button type="button" class="btn btn-info" style="width: 90px; background-color: #969dac;margin-left: 75%;" data-dismiss="modal">Cerrar</button>

				</div>
		  	</div>

		 </div>	
	</div>

	<!-- ========================================
   					MODAL DE PRODUCTOS NO INSERTADOS
   		========================================  -->		
	<div id="requestModalNoInsertados" class="modal fade" role="dialog">
	  	<div class="modal-dialog" style="width: 30%;margin-top: 1%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Productos No Actualizados</h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
				 	<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-12">
  							<div class="table-responsive">
                                <table id="productosInsertados" class="table">
                                    <thead>
                                       <tr style="background-color: #eceff1;">
                                            <th style="width: 60px;">Cant.</th>
                                            <th style="width: 18%;">Codigo</th>
                                            <th style="width: 18%;">Denominacion</th>
                                        </tr>
                                    </thead>
                                    <tbody  style='font-size: 0.85rem;'>
                                    	@foreach($arrayNoInsertados as $noInsertados)
                                    		@php 
                                    			$datos= explode("-",$noInsertados)
                                    		@endphp
											<tr style="background-color: #eceff1;">
	                                            <th>{{$datos[0]}}</th>
	                                            <th>{{$datos[1]}}</th>
	                                            <th>{{$datos[2]}}</th>
	                                        </tr>                                    	
                                        @endforeach
                                    </tbody>

                                </table>    	
                            </div>							
                        </div>
					</div>
					<br>
					<button type="button" class="btn btn-info" style="width: 90px; background-color: #969dac;margin-left: 75%;" data-dismiss="modal">Cerrar</button>

				</div>
		  	</div>

		 </div>	
	</div>




@endsection
@section('scripts')

	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<!-- wysihtml core javascript with default toolbar functions --> 
	
	<script>
	
		$('.select2').select2();
		 $.unblockUI();
	</script>
@endsection
