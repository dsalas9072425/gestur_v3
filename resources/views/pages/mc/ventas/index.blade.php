
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">
	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	/*.bgRed:hover{
		color: white;
	}*/

	/*background: #e2076a !important;padding-left: 6px;padding-right: 6px;*/



	/*==================SELECT STAR============================*/

	.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}


.click_estrella {
	cursor:pointer; 
	cursor: hand;
}

/*====================================================
	ESTILO PARA FOCUS
  ====================================================	
*/

input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }



</style>
    <!-- Main content -->
   <section id="base-style">
   		@include('flash::message')
   		<div class="card-content">
		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Ventas Rápidas</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
				<div class="card-body pt-0">
					<a href="{{route('venta')}}" title="Nueva Venta" class="btn btn-success pull-right"
						role="button">
						<div class="fonticon-wrap">
							<i class="ft-plus-circle"></i>
						</div>
					</a>
				</div>		        
				<div class="card-body rounded-bottom rounded-lg">
		     		<form id="filtrosForm">

			     		<div class = "row">
							<div class="col-md-3">
				                <label class="control-label">Número Venta</label>
				                <input type="text" class = "Requerido form-control" name="nro_venta" id="nro_venta" value="" tabindex="1">
							</div>	


			                <div class="col-12 col-sm-3 col-md-3">
			 					<div class="form-group">
						            <label>Fecha Desde/Hasta</label>			
						            	<div class="input-group">
									    <div class="input-group-prepend" style="">
									        <span id="fecha" class="input-group-text"><i class="fa fa-calendar"></i></span>
									    </div>
									    <input type="text" id="periodo" name="periodo" class="form-control">
									</div>
								</div>	
							</div> 

						{{-- 	<div class="col-md-3">
								<label class="control-label">Número Factura</label>
			                	<input type="text" class = "Requerido form-control" name="nro_factura" id="nro_factura" value="" tabindex="2">		
			                </div>	 --}}

							<div class="col-md-3">
		 						<div class="form-group">
						            <label>Estado</label>
									<select class="form-control input-sm select2" name="id_estado" id="id_estado" style=" width: 100%;"  tabindex="4" >
										<option value="">Todos</option>
										@foreach($estados as $key=>$estado)
												<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
										@endforeach	
									</select>
					        	</div>
							</div>	

							<div class="col-md-3">
								<div class="form-group">
						            <label>Moneda</label>
									<select class="form-control input-sm select2" name="id_moneda" id="id_moneda" style=" width: 100%;"  tabindex="4" >
										<option value="">Todos</option>
											@foreach($currencys as $key=>$currency)
												<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
											@endforeach	
										</select>
						        </div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
						            <label>Vendedor Empresa</label>
						            <select class="form-control select2" name="id_vendedor_empresa" id="id_vendedor_empresa" style="width: 100%;" tabindex="5">
										<option value="">Todos</option>
										@foreach($vendedor_empresas as $usuario)
												<option value="{{$usuario->id}}">{{$usuario->nombre}}  {{$usuario->apellido}}</option>
											@endforeach
						            </select>
					        	</div>					
					    	</div>

					    	<div class="col-md-3">
								<div class="form-group">
						            <label>Cliente</label>
									<select class="form-control select2" name="cliente_id"  id="cliente_id" tabindex="6" style="width: 100%;" >
										<option value="">Todos</option>
											@foreach($clientes as $cliente)
												@php
													$ruc = $cliente->documento_identidad;
													if($cliente->dv){
														$ruc = $ruc."-".$cliente->dv;
													}
												@endphp
												<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->full_data}} - {{$cliente->id}}</option>
											@endforeach
									</select>
						        </div>
							</div>	
							<div class="col-md-3">
								<div class="form-group">
						            <label>Tipo de Venta</label>
						            <select class="form-control select2" name="tipo_venta" id="tipo_venta" style="width: 100%;" tabindex="5">
										<option value="">Todos</option>
										<option value="ONLINE">ONLINE</option>
										<option value="OFFLINE">OFFLINE</option>
						            </select>
					        	</div>					
					    	</div>
							<div class="col-md-3">
								<div class="form-group">
						            <label>Origen Negocio</label>
						            <select class="form-control select2" name="origen_negocio" id="origen_negocio" style="width: 100%;" tabindex="5">
										<option value="">Todos</option>
										@foreach($comercioPersonas as $comercioPersona)
												<option value="{{$comercioPersona->id}}">{{$comercioPersona->descripcion}}</option>
											@endforeach
						            </select>
					        	</div>					
					    	</div>
						</div>	
						
					<div class = "row">
						<div class="col-md-3">
							<div class="form-group">
						        <label>Pedido</label>
						        <select class="form-control select2" name="id_woo_order" id="id_woo_order" style="width: 100%;" tabindex="5">
									<option value="">Todos</option>
									@foreach($pedidos as $pedido)
										<option value="{{$pedido->id}}">{{$pedido->id_woo_order}}</option>
									@endforeach
						        </select>
					        </div>					
					    </div>
					    <div class="col-md-3">
				                <label class="control-label">Número Factura</label>
				                <input type="text" class = "Requerido form-control" name="nro_factura" id="nro_factura" value="" tabindex="1">
							</div>	
					    </form>
						<div class="col-md-9">	
						<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg" style=" margin: 0 0 10px 10px;"><b>Excel</b></button>
							<button type="button" onclick="limpiar()"class="pull-right btn btn-light btn-lg text-white mr-1 mb-1" tabindex="7" ><b>Limpiar</b></button>
						 	<button type="button" class="pull-right  btn btn-info btn-lg mr-1 mb-1"  role="button" onclick="buscar()" tabindex=8><b>Buscar</b></button>
						</div>	 	
					</div>		 	
					<div class="table-responsive">
			              <table id="listado" class="table" style="width: 100%;">
			                <thead>
								<tr style="text-align: center;">
							        <th>Venta</th>
							        <th>Monto Venta</th>
							        <th>Moneda</th>
							        <th>Estado</th>
							        <th>Factura</th>
									<th>Fecha</th>
							        <th>Cliente</th>
									<th>Vendedor <br>
										Empresa</th>
									<th>Vendedor <br>
										Agencia</th>
									<th>Tipo Venta</th>
									<th></th>
									<th>Origen</th>
							        <th></th>
					            </tr>
			                </thead>
			                <tbody style="text-align: center;">

			                </tbody>
			            </table>
			        </div>  
		        </div>
		    </div>
		</div>
    </div>	
</section>
    <!-- /.content -->



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>

	<script>
		$(document).ready(function() 
		{
			$('#fecha').click(function(){
				$('input[name="periodo"]').trigger('click');
			}) 

			var Fecha = new Date();
	        var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

			let select_1 = $('.select2').select2();
			let select_2 = $('.estado_proforma').select2({
									multiple:true,
									maximumSelectionLength: 2,
									placeholder: 'Todos'
								});
			let config = {
							timePicker24Hour: true,
							timePickerIncrement: 30,
							autoUpdateInput: false,
							locale: {
										format: 'DD/MM/YYYY',
										cancelLabel: 'Limpiar',
										applyLabel: 'Aplicar',					
										fromLabel: 'Desde',
										toLabel: 'Hasta',
										customRangeLabel: 'Seleccionar rango',
										daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',  'Diciembre']
									}
						};

			$('input[name="periodo"]').daterangepicker(config);
			$('input[name="periodo"]').val('');
			$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');});
			$('input[name="periodo"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});
			
			// buscar();
		});

		const formatter = new Intl.NumberFormat('de-DE', {currency: 'USD',minimumFractionDigits: 0});


		function buscar()
		{				
			table =   $('#listado').DataTable({
					        "destroy": true,
					        "select": true,
							"order": [ 0, 'desc' ],
					        "ajax": 
					        {
					        	data: $('#filtrosForm').serializeJSON(),
						        url: "{{route('getVentasRapidas')}}",
						        type: "GET",
						    	error: function(jqXHR,textStatus,errorThrown)
						    	{
		                           $.toast({
			                            heading: 'Error',
			                            text: 'Ocurrió un error en la comunicación con el servidor.',
			                            position: 'top-right',
			                            showHideTransition: 'fade',
			                            icon: 'error'
			                        });
	                            	
	                            	$.unblockUI();
                        		}
						  	},

						 "columns":
						 [
						 			{data: "id"},

						 			{
										data: function (x) 
										{
											return (!isNaN(parseFloat(x.total_venta))) ? formatter.format(parseFloat(x.total_venta)) : 0;
										}
									}, 

						 			//{data: "currency.currency_code"},
						 			{
										data: function (x) 
										{
											var moneda = '';
											if(jQuery.isEmptyObject(x.currency) == false){
												moneda = x.currency.currency_code;
											}else{
												moneda = '';
											}	
											return moneda;
										}
									},

						 			{data: "estado.denominacion"},

						 			{
										data: function (x) 
										{
											var factura = '';

											if (x.factura != null) 
											{
												factura = x.factura.nro_factura;
											}

											return factura;
										}
									},

						 			{ "data": function(x)
										{
											var fecha = x.fecha_venta.split(' ');
											var f = formatearFecha(fecha[0]);
											return f;
										}
									},

						 			{
										data: function (x) 
										{	
											cliente = '';
											if(Boolean(x.cliente))
											{
												var cliente = x.cliente.nombre;

													if (x.cliente.apellido != null) 
													{
														cliente = cliente+' '+x.cliente.apellido;
													}
											}
										

											return cliente;
										}
									},

									{
										data: function (x) 
										{
											vendedor = '';
												if(Boolean(x.vendedor))
												{
												var vendedor = x.vendedor.nombre;

												if (x.vendedor.apellido != null) 
												{
													vendedor = vendedor+' '+x.vendedor.apellido;
												}
											}

											return vendedor;
										}
									},
									{
										data: function (x) 
										{
											vendedor_agencia = '';
											
												if(Boolean(x.vendedor_agencia))
												{
												var vendedor_agencia = x.vendedor_agencia.nombre;

												if (x.vendedor_agencia.apellido != null) 
												{
													vendedor_agencia = vendedor_agencia+' '+x.vendedor_agencia.apellido;
												}
											}

											return vendedor_agencia;
										}
									},

									{
										data: function (x) 
										{
											var tipo = '';
											if(jQuery.isEmptyObject(x.pedido) == false){
												order = '<b>('+x.pedido.id_woo_order+')</b>';
											}else{
												order = '';
											}	
											tipo = x.tipo_venta.descripcion+'  '+order;
											return tipo;
										}
									},
									{
										data: function (x) 
										{

											cliente = '<b>Cliente:</b> ';
											if(Boolean(x.cliente))
											{
												var cliente = x.cliente.nombre;

													if (x.cliente.apellido != null) 
													{
														cliente += cliente+' '+x.cliente.apellido;
													}
											}

											telefono = 'Telefono: ';
											if(jQuery.isEmptyObject(x.delivery_teléfono) == false){
												telefono += x.delivery_teléfono;
											}
												
											btn = '';
											if(jQuery.isEmptyObject(x.delivery_lat) == false){
												btn += `<a href="https://web.whatsapp.com/send?text= `+cliente+`%20%20`+telefono+`%20%20https://maps.google.com/?q=`+x.delivery_lat+`,`+x.delivery_lng+`" target="_blank" data-action="share/whatsapp/share"><img  title="Whatsapp" src="{{URL::asset('images/icon/wap.png')}}" style="width: 35px;"/></a>`;
											}
											console.log(x);
											
											
											return btn;
										}
									},

									{
										data: function (x) 
										{
											var img = '';
											if(jQuery.isEmptyObject(x.comercio) == false){
		                            			img = '<img style="max-width:90px;" src="'+x.comercio.logo+'" alt="'+x.comercio.descripcion+'">';
		                            		}else{
		                            			img = '';
		                            		}

											return img;
										}
									},

								{
									data: function (x) 
									{
										btn = '';
										if(x.estado.id == 71 || x.estado.id ==75){
											btn +=`<a href="{{route('editVenta',['id'=>''])}}/${x.id}"  class="fa fa-fw fa-edit">`; 
										}
											btn += `<a href="{{route('verVenta',['id'=>''])}}/${x.id}"  class="fa fa-fw fa-search">`;

										return btn;
									}
								}

						 	]		
						                    
					    })

		}//function

		$("#botonExcel").on("click", function(e){ 
			
                e.preventDefault();
                 $('#filtrosForm').attr('method','post');
               $('#filtrosForm').attr('action', "{{route('generarExcelListadoVentas')}}").submit();
            });

		function limpiar()
		{
			$('#cliente_id').val('').trigger('change.select2');
			$('#periodo').val('').trigger('change.select2');
			$('#id_estado').val('').trigger('change.select2');
			$('#id_moneda').val('').trigger('change.select2');
			$('#id_vendedor_empresa').val('').trigger('change.select2');
			$('#nro_factura').val('');
			$('#nro_venta').val('');
		}

		function formatearFecha(texto)
 		{
	        if(texto != '' && texto != null)
	        {
	          	return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	        }
	        else
	        {
	         	return '';
         	}
        }

	</script>
@endsection