@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	
	<style>
		.asistencia{
			display: none;
		}
	 	.error{
	 		color:red;
	 	},
	   	.cabeceraClass {
	   		padding: 10px;
	   		min-width: 250px;
	   		background-color:#E8EAEA;
	   		word-break: break-all;
	   		margin: 10px;

	   	},
		.modal-footer {
		    text-align: left;
		} 
		.nav-tabs>li>a {
		    color: #dacdcd;
		} 
		.base{
			cursor: pointer;
		}

		.modal-bodyProforma{
			  height:250px;
             overflow:auto;
			}

		.commentNone {
			display: none;
		}
		.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

		.text-bold{
			font-weight: 800;
			font-size: 15px;
		}

		.select2-selection {
	    	max-height: 30px !important;
		}

		.select2-selection--single{
			padding-top: 0px !important;	
		}

		.select2-selection__rendered{
			padding-right: 5px;
		}

		.note-editing-area{
    		height: 300px;
		}

	    .chat-content {
	        text-align: left;
	        float: left;       
	        position: relative;
	        display: block;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #404e67;
	        background-color: #edeef0;
	        border-radius: 4px;
	    }

		.select2-selection {
			 padding-right: 0px;
		}

	    .chat-content-left {
	        text-align: right;
	        position: relative;
	        display: block;
	        float: right;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #fff;
	        background-color: #00b5b8;
	        border-radius: 4px;
	    }  
	/*=============================================================================*/

	.valoracion {
	    position: relative;
	    overflow: hidden;
	    display: inline-block;
	}

	.valoracion input {
	    position: absolute;
	    top: -100px;
	}


	.valoracion label {
	    float: right;
	    color: #c1b8b8;
	    font-size: 30px; 
	}

	.valoracion label:hover,
	.valoracion label:hover ~ label,
	.valoracion input:checked ~ label {
	    color: #E2D532;
	}

	.select2-moneda_compra_0-container{
		padding-right: 10px;
    	padding-left: 4px;
	}

	modal-dialog modal-lg{
		margin : 0 auto;
	}

	.modal-lg{
		margin-left: 10%;
	}

/*=============================================================================================*/
	.btnAdjuntar{
			/* background-color: #00a65a !important;
			border-color: #008d4c !important; 
			color:#FFFF !important;*/s
			font-weight:800 !important;  
			padding:10px !important;
	}


	.verificarStyle {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}
	
	.btnSolicitarVerificacion {
		padding:10px !important;
		/* background: #2d3e52 !important;
		border-color: #2d3e51 !important;
		color:#FFFF !important; */
	}

	.btnModificar {
		/* background: #e2076a !important; 
		border-color: #e25a7d !important;  */
		padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnAnular{
		/* background-color: #dd4b39 !important;
    	border-color: #d73925 !important; */
    	padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnHand {
		cursor:pointer; 
		cursor: hand;
	}

	.btnHand:hover {
  opacity: 0.8;
}


.vuelosClass{
   padding: 5px 0px;
}
/*#vuelos th {
	padding-left: 0px;padding-right: 0px;
}*/
.select2-container--default .select2-selection--single{
    padding-left: 0px;
}

.select2-moneda_compra_0-container{
	    padding-left: 0px;
	    padding-right: 0px;
}
		  
	</style>
		
@endsection
@section('content')

    <!-- Main content -->
<section id="base-style">
   	<div class="card-content">
   		@include('flash::message')
        <form id="frmVenta" method="post" action="{{route('doEditVenta')}}" >
            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">Venta Nro: {{$ventas[0]->id}}</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-bottom: 10px;padding-top: 10px;">
                    	<?php
                    			//echo '<pre>';
                    			//print_r($ventas[0]->ventasDetalle);

                    	?>
                        <div class="row">
                            <div class="col-12 col-sm-4 col-md-2">
								<div class="form-group">
									<label>Fecha Venta </label>
									<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text" style="padding-right: 10px;padding-left: 10px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="hidden" value="{{$ventas[0]->id}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="id" id="idVenta">
											<input type="text" value="{{date('d/m/Y', strtotime($ventas[0]->fecha_venta))}}" disabled="disabled" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="fecha_ventass" id="fechaVentas" tabindex="1">
										</div>
								</div>
							</div>
                            <div class="col-12 col-sm-4 col-md-4" style="padding-left: 0px;left: 9px;">
									<div id="botonesModal" class="row" style="margin-left: 0px; margin-right: 0px;">
										<label>Cliente</label><br>
										<div class="input-group">
                                            <div class="input-group-prepend" id="button-addon1" style="width: 100%;">
                                                <button id="botonPasajero" class="btn btn-primary" data-toggle="modal" data-target="#requestModalCliente" type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-id-card-o"></i></button>
												<select class="form-control select2" name="cliente_id" id="cliente_id" style="width: 100%;">
												</select>                                        
                                            </div>
										</div>	
									</div>
							</div>
                            <div class="col-12 col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label>Tipo de Factura</label> 
                                    <select class="form-control select2" name="tipo_factura" id="tipo_factura" tabindex="4"
                                        style="width: 100%;">
                                        <option value="">Seleccione Tipo de Factura</option>
										@foreach($tipoFacturas as $tipoFactura)
											<option value="{{$tipoFactura->id}}">{{$tipoFactura->denominacion}} </option>
										@endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label>Moneda</label> 
                                    <select class="form-control select2" name="moneda" id="moneda" tabindex="5"
                                        style="width: 100%;">
                                        <option value="">Seleccione Moneda</option>
										@foreach($currencys as $key=>$currency)
											<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
										@endforeach	
                                    </select>
                                </div>   
                            </div>
							<div class="col-12 col-sm-1 col-md-2">
								<div class="form-group">
                                    <label>Zona</label> 
									<select class="form-control select2" name="zona" id="zonasSelect" tabindex="6" style="width: 100%;">
										<option value="">Seleccione Zona</option>
										@foreach($zonas as $zona)
											<option value="{{$zona->id}}">{{$zona->descripcion}} </option>
										@endforeach
									</select>
								</div>	
							</div>
						</div> 
						<div class="row">
							<div class="col-12 col-sm-4 col-md-2" style="padding-left: 8px;">
                                <div class="form-group">
                                    <label>Forma de Pago</label> 
                                    <select class="form-control select2" name="id_forma_pago" id="id_forma_pago" tabindex="5"
                                        style="width: 100%;">
                                        <option value="">Seleccione Moneda</option>
										@foreach($formasPagos as $key=>$formasPago)
											<option value="{{$formasPago->id}}">{{$formasPago->denominacion}}</option>
										@endforeach	
                                    </select>
                                </div>
                            </div>
							<div class="col-12 col-sm-1 col-md-2">
								<label>Estado Venta</label> 
                            	<input type="text" value="{{$ventas[0]->estado->denominacion}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
							</div>
							<div class="col-12 col-sm-1 col-md-2">
								<label>Comprobante</label> 
                            	<input type="text" value="{{$ventas[0]->comprobante}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="comprobante" >
							</div>
                            <div class="col-12 col-sm-4 col-md-2">
                                <div class="form-group">
                                    <label>Origen Negocio</label>
                                    <select class="form-control select2" name="origen_comercio" required id="origen_comercio" tabindex="4"
                                        style="width: 100%;">
                                        <option value="">Seleccione Origen</option>
										@foreach($comercioPersonas as $comercioPersona)
											<option value="{{$comercioPersona->id}}">{{$comercioPersona->descripcion}} </option>
										@endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-1 col-md-4">
								<label>Direccion Delivery</label> 
                            	<input type="text" value="{{$ventas[0]->delivery_direccion}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="delivery_direccion" >
							</div>
							<div class="col-12 col-sm-1 col-md-4">
								<label>Referencia</label> 
                            	<input type="text" value="{{$ventas[0]->delivery_referencia}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="delivery_referencia" >
							</div>	
							 <div class="col-12 col-sm-4 col-md-2">
								<div class="form-group">
									<label>Fecha Vencimiento </label>
									<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text" style="padding-right: 10px;padding-left: 10px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-calendar"></i></span>
											</div>
											@if($ventas[0]->fecha_vencimiento == "")
												<input type="text" value="" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="fecha_venta" id="fechaVenta" tabindex="1">
											@else
												<input type="text" value="{{date('d/m/Y', strtotime($ventas[0]->fecha_vencimiento))}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="fecha_venta" id="fechaVenta" tabindex="1">	
											@endif
										</div>
								</div>
							</div>					
                        </div>	
						<div class="row">
							<div class="col-12 col-sm-1 col-md-11" style="padding-right: 0px;">
								<label>Concepto Genérico</label> 
								<textarea name="concepto" id="concepto" rows="2" cols="200" style="width: 99%;">{{$ventas[0]->concepto_generico}}</textarea>
							</div>	
						</div>

						<div class="row">
							
                            	@if(empty($ventas[0]->id_vendedor))
	                            	@if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 5 ||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 6 ||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2 )
	                            	<div class="col-12 col-sm-1 col-md-3">
									<br><br>
										<a id="asignarme" data="{{$ventas[0]->id}}" title="Asignarme Venta" class="btn btn-info" style="margin-left: 5px;width: 230px; color:#2a2e30;"><i class="ft-save" style="font-size: 18px;"></i>Asignarme Venta</a>
										 </div>  	
									@endif	
	                            @endif
                           
                            <div class="col-12 col-sm-1 col-md-3">
                            	<br><br>
                            	<button id="botonMapaZona" class="btn btn-info" data-toggle="modal" data-target="#requestModalMapaZona" style="margin-left: 5px;width: 230px;color:#2a2e30;padding-top: 14px;" type="button"><i class="fa fa-id-card-o"></i>Mapa de Zona</button>
                            </div>	
                            {{--@if(!empty($ventas[0]->delivery_lat))--}}
	                             <div class="col-12 col-sm-1 col-md-3">
	                            	<br><br>
	                            	<button id="botonActualizarLatLong" class="btn btn-info" data-toggle="modal" data-target="#requestModalLatLong" style="margin-left: 5px;width: 230px;color:#2a2e30;padding-top: 14px;" type="button"><i class="fa fa-id-card-o"></i>Actualizar Latitud/Longitud</button>
	                              
	                            </div>
                           {{-- @endif --}}
                            @if(!empty($ventas[0]->delivery_lat))
 								<div class="col-12 col-sm-1 col-md-3">
	                            	<br><br>
	                            	<button id="botonMapaDelivery" class="btn btn-info" data-toggle="modal" data-target="#requestModalDelivery" style="margin-left: 5px;width: 230px;color:#2a2e30;padding-top: 14px;" type="button"><i class="fa fa-id-card-o"></i>Mapa de Delivery</button>
	                            </div>	
                              @endif
                            <?php 
								if(isset($btn[0])){ 
									if($ventas[0]->id_estado == 75){
									?>
	                            	<div class="col-12 col-sm-1 col-md-3">	
										<br><br>	
										<a id="reactivar" data="{{$ventas[0]->id}}" title="Reactivar Venta" class="btn btn-info" style="margin-left: 5px;width: 230px; color:#2a2e30;"><i class="ft-save" style="font-size: 18px;"></i>Reactivar Venta</a>
									</div>		
								<?php 
										}
									}
								?>	                        
							
						</div>   
                    </div>
                </div>
            </div>
            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">Detalles de Venta</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">
                        <div id="mensajes"></div>
                            <div class="table-responsive">
                                <table id="lista_ventas" class="table">
                                    <thead>
                                       <tr style="background-color: #eceff1;">
                                            <th style="width: 60px;">Cant.</th>
                                            <th style="width: 18%;">Producto</th>
                                            <th style="width: 18%;">Proveedor</th>
                                            <th style="width: 10%;">Código_Producto</th>
                                            <th style="width: 15%;">Item</th>
                                            <th style="width: 10%;">Costo</th>
                                            <th style="width: 10%;">Iva</th>
                                            <th style="width: 10%;">Venta</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody  style='font-size: 0.85rem;'>

                                    </tbody>
                                    <tfoot>	
										<tr>
                                            <td>
                                                <input type="text" class = "form-control form-control-sm numerico cant" required name="cantidad_0" id="cantidad_0" value="1"/>
                                            </td>
                                            <td>
                                                <select class="form-control select2 producto" name="producto_0" required id="producto_0" data="0" value="0" style="width: 100%;">
                                                    <option value="">Seleccione Producto</option>
                                                </select>
                                            </td>
                                             <td>
                                                <select class="form-control select2" name="proveedor_0" required id="proveedor_0" data="0" value="0" style="width: 100%;">
                                                    <option value="">Seleccione Proveedor</option>
													@foreach($proveedores as $proveedor)
														<option value="{{$proveedor->id}}">{{$proveedor->nombre}} {{$proveedor->apellido}}</option>
													@endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class = "form-control form-control-sm" required name="nro_factura_0" id="nro_factura_0" value=""/>
                                            </td>
                                            <td>
                                                <input type="text" class = "form-control form-control-sm " name="item_0" value="" id="item_0"/>
                                            </td>                                            
                                            <td>
                                                <input type="hidden" class = "form-control form-control-sm numerico costo" name="costo_0" value="0" id="costo_0"/>
												<input type="text" class = "form-control form-control-sm numerico input_costo" name="input_costo_0" value="0" disabled="disabled" id="input_costo_0"/>
                                            </td>
                                            <td>
                                                <input type="text" class = "form-control form-control-sm numerico" name="iva_0" disabled="disabled" value="10" id="iva_0"/>
												<input type="hidden" class = "form-control form-control-sm" name="tipo_impuesto_0" id="tipo_impuesto_0"/>
												<input type="hidden" class = "form-control form-control-sm" name="valor_impuesto_0" id="valor_impuesto_0"/>
                                            </td>
                                            <td>
                                                <input type="text" class = "form-control form-control-sm numerico" required name="venta_0" value="0" id="venta_0"/>
                                            </td>
                                            <td>
                                                <a id="mostrar_0" data="0" class="glyphicon glyphicon-save-file" title="Desplegar Detalle"  style="margin-left: 5px;"><i class="ft-save" style="font-size: 18px;"></i></a>
                                            </td>
                                        </tr>                                    
                                    </tfoot>
                                </table>    	
                            </div>	
                        </div>
                    </div>
                </div>
            </div>

            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">Resumen</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-bottom: 10px;padding-top: 10px;">
					<div class= "row">
							<div class="col-md-2">
							<input type="hidden" class = "Requerido form-control numerico" name="total_5" id="total_5" style="font-weight: bold;font-size: 15px;" value='0' disabled="disabled"/>
							<input type="hidden" class = "Requerido form-control numerico" name="total_10" id="total_10" style="font-weight: bold;font-size: 15px;" value='0' disabled="disabled"/>
							</div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label style="font-size: small;font-weight: 700; display: none;">TOTAL_EXENTAS</label>
									@if(!empty($ventas[0]->total_exentas))	
                                    	<input type="text" class = "Requerido form-control numerico" name="total_exenta" id="total_exenta" style="font-weight: bold;font-size: 15px; display: none;" value='{{$ventas[0]->total_exentas}}' disabled="disabled"/>
									@else
										<input type="text" class = "Requerido form-control numerico" name="total_exenta" id="total_exenta" style="font-weight: bold;font-size: 15px; display: none;" value='0' disabled="disabled"/>
									@endif

								
								</div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label style="font-size: small;font-weight: 700; display: none;">TOTAL_GRAVADAS</label>		
									@if(!empty($ventas[0]->total_gravadas))	
                                    	<input type="text" class = "Requerido form-control numerico" name="total_gravadas" id="total_gravadas" style="font-weight: bold;font-size: 15px; display: none;" value='{{$ventas[0]->total_gravadas}}' disabled="disabled" />
									@else
                                    <input type="text" class = "Requerido form-control numerico" name="total_gravadas" id="total_gravadas" style="font-weight: bold;font-size: 15px; display: none;" value='0' disabled="disabled" />
									@endif
								</div>
                            </div> 
							<div class="col-md-2">
                                <div class="form-group">
                                    <label style="font-size: small;font-weight: 700; display: none;">IVA</label>	
									@if(!empty($ventas[0]->total_iva))	
                                    	<input type="text" class = "Requerido form-control numerico" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px; display: none;" value='{{$ventas[0]->total_iva}}' disabled="disabled" />
									@else
										<input type="text" class = "Requerido form-control numerico" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px; display: none;" value='0' disabled="disabled" />
									@endif
								</div>
                            </div> 
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label style="font-size: small; color: red;font-weight: 700;">TOTAL_FACTURA</label>
									@if(!empty($ventas[0]->total_iva))	
                                    	<input type="text" class = "Requerido form-control numerico" name="total_factura" id="total_factura" style="font-weight: bold;font-size: 15px;" value='{{$ventas[0]->total_venta}}' disabled="disabled"/>
									@else
										<input type="text" class = "Requerido form-control numerico" name="total_factura" id="total_factura" style="font-weight: bold;font-size: 15px;" value='0' disabled="disabled"/>
									@endif
								</div>						
                            </div>
                              <div class="col-md-2">
                              	<br>
								<button type="button" id="guardarBtn" class="btn btn-info btn-lg" style="margin-bottom: 10px;"> GUARDAR</button>
							</div>	
                        </div>	
                        <!--<div class= "row">
                            <div class="col-md-10">
							</div>	
                            <div class="col-md-2">
								<button type="button" id="guardarBtn" class="btn btn-info btn-lg" style="margin-bottom: 10px;"> GUARDAR</button>
							</div>	
						</div>-->	
                    </div>
                </div>
            </div>
        </form>    
	</div>
</section>		
<!-- ========================================
   					MODAL PASAJEROS
   		========================================  -->		
		   <div id="requestModalCliente" class="modal fade" role="dialog">

	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Ingresar Cliente</h2>
					</div>
					<form id="frmCliente" style="margin-top: 1%;">
						<div class="modal-body">
							<div id="contenido">
								<div class="form-group">
									<label>Nombre</label>						 
									<input type="text" required class = "form-control form-control-sm" name="nombre" id="nombre"/>
								</div>    
								<div class="form-group">
									<label>Apellido</label>						 
									<input type="text" required class = "form-control form-control-sm" name="apellido" id="apellido"/>
								</div>  
								<div class="row"  style="margin-bottom: 1px;">
									<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
										<div class="form-group">
											<label>Tipo Documento</label>						 
											<select class="form-control select2" name="tipo_identidad" id="tipo_identidad" style="width: 100%;">
												@foreach($tipoDocumentos as $tipoDocumento)
													<option value="{{$tipoDocumento->id}}">{{$tipoDocumento->denominacion}} </option>
												@endforeach

											</select> 
										</div>  
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;margin-bottom: 15px;">
										<div class="form-group">
											<label>Nro Documento</label>						 
											<input type="text" class = "form-control form-control-sm" id="documento" name="documento"/>
										</div>    
									</div> 
								</div>
								<div class="form-group">
									<label>Email</label>						 
									<input type="text" required class = "form-control form-control-sm" name="email" id="email"/> 
								</div>    
								<div class="form-group" style="display: none">
									<label>Nro. de Teléfono</label>						 
									<input type="text" required class = "form-control form-control-sm" name="telefono" id="telefono"/> 
								</div>
								<div class="form-group">
									<label>Dirección</label>						 
									<input type="text" required class = "form-control form-control-sm" name="direccion" id="direccion"/>
								</div>  
							</div>
						</div> 
				  	</form>
				  <div class="modal-footer">
					<button type="button" id="btnAceptarPasajero" class="btn btn-info" onClick="guardarCliente()" style="width: 90px; background-color: #e2076a;">Aceptar</button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>

	<!-- ========================================
   					MODAL LATITUD/LONGITUD
   		========================================  -->		
		   <div id="requestModalLatLong" class="modal fade" role="dialog">

	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Actualizar Latitud / Longitud</h2>
					</div>
					<form id="frmCliente" style="margin-top: 1%;">
						<div class="modal-body">
							<div id="contenido">
								<div class="form-group">
									<label>Latitud</label>						 
									<input type="text" required class = "form-control form-control-sm" name="latitud" id="latitud"/>
								</div>    
								<div class="form-group">
									<label>Longitud</label>						 
									<input type="text" required class = "form-control form-control-sm" name="longitud" id="longitud"/>
								</div>  
							</div>
						</div> 
				  	</form>
				  <div class="modal-footer">
					<button type="button" id="btnAce" class="btn btn-info" onClick="guardarLatLong()" style="width: 90px; background-color: #e2076a;">Aceptar</button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>
	<!-- ========================================
   					MODAL PROVEEDORES
   		========================================  -->		
		<div id="requestModalProveedor" class="modal fade" role="dialog">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Actualizar Proveedor</h2>
					</div>
					<form id="frmProveedor" style="margin-top: 1%;">
						<div class="modal-body">
							<div id="contenido">
								<div class="form-group">
									<input type="hidden" required class = "form-control form-control-sm" name="idLineaProveedor" id="idLineaProveedor"/>
									<label>Proveedor</label>						 
									 <select class="form-control select2" name="proveedorActualizar" required id="proveedorActualizar" data="0" value="0" style="width: 100%;">
                                        <option value="">Seleccione Proveedor</option>
										@foreach($proveedores as $proveedor)
												@php
													$ruc = $proveedor->documento_identidad;
													if($proveedor->dv){
														$ruc = $ruc."-".$proveedor->dv;
													}
												@endphp
											<option value="{{$proveedor->id}}">{{$ruc}} - {{$proveedor->nombre}} {{$proveedor->apellido}}</option>
										@endforeach
                                    </select>
								</div>    
							</div>
						</div> 
				  	</form>
				  <div class="modal-footer">
					<button type="button" id="btnAce" class="btn btn-info" onClick="guardarProveedor()" style="width: 90px; background-color: #e2076a;">Aceptar</button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>
	<!-- ========================================
   					MODAL DE MAPA DELIVERY
   		========================================  -->		
	<div id="requestModalDelivery" class="modal fade" role="dialog">
	  	<div class="modal-dialog modal-xl" style="height: 100%;width:80%;margin-left: 9%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">MAPA DE DELIVERY<span id="clienteName"></span></h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
					<div id="map-tab" style="width:100%;height:450px"></div>

					<button type="button" class="btn btn-info" style="width: 90px; background-color: #969dac;margin-left: 75%;" data-dismiss="modal">Cerrar</button>
				</div>
		  	</div>

		 </div>	
	</div>

	<!-- ========================================
   					MODAL DE MAPA ZONA
   		========================================  -->		
	<div id="requestModalMapaZona" class="modal fade" role="dialog">
	  	<div class="modal-dialog modal-xl" style="height: 100%;width:80%;margin-left: 9%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">MAPA DE ZONAS<span id="clienteName"></span></h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
					<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1_nipSqwHbIw0DvYhiqfGhA7Z-0rcqLvU&ll=-25.298510223966137%2C-57.54707680000001&z=12" width="100%" height="800"></iframe>
					<button type="button" class="btn btn-info" style="width: 90px; background-color: #969dac;margin-left: 75%;" data-dismiss="modal">Cerrar</button>
				</div>
		  	</div>

		 </div>	
	</div>
@endsection
@section('scripts')

	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<!-- wysihtml core javascript with default toolbar functions 
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCu2ZW8F0SdXXa2bLgnIu-vIInMdg_IMQw&libraries=places&callback=initialize" type="text/javascript"></script>--> 
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/parsley.js')}}"></script>

	<script>
		var map;
		var panorama;
		var fenway;
		var mapOptions;
		var panoramaOptions;
	/*	@if(!empty($ventas[0]->delivery_lat))
			var lat = {{$ventas[0]->delivery_lat}};
			var lon = {{$ventas[0]->delivery_lng}};
		@else 
			var lat = 0;
			var lon = 0;		
		@endif	

		function initialize(){		
			map = new google.maps.Map(document.getElementById('map-tab'), {
				center: {lat: lat, lng: lon},
				zoom: 18
			});
			var marker = new google.maps.Marker({
			  position: map.getCenter(),
			  map: map,
			  title: "<?php echo 'Direccion Delivery'; ?>"
			});
			var descripcion = '';
			descripcion = '<b>Numero1</b><br>Ciudad<br>Precio desde: <b>{1.000'

			var infowindow = new google.maps.InfoWindow({
				content: descripcion
			});

			marker.addListener('click', function() {
				infowindow.open(map, marker);
			});				
		}*/

		$(function () {
			$("#fechaVenta").datepicker({
			dateFormat: 'dd/mm/yy',
			minDate: new Date()
		});
		$('#fechaVenta').datepicker('setStartDate', new Date());
		$("#fechaVenta").datepicker('setDate', new Date());
		});


		$(document).ready(function()
		{
				$('.select2').select2();
				$("#cliente_id").select2({
						ajax: {
								url: "{{route('getClienteVenta')}}",
								dataType: 'json',
								placeholder: "Seleccione un Cliente",
								delay: 0,
								data: function (params) {
											return {
												q: params.term, // search term
												page: params.page
													};
								},
								processResults: function (data, params){
											var results = $.map(data, function (value, key) {
											console.log(value);
										/* return {
													children: $.map(value, function (v) {*/ 
														return {
																	id: value.id,
																	text: value.full_data
																};
													/*  })
													};*/
											});
													return {
														results: results,
													};
								},
								cache: true
								},
								escapeMarkup: function (markup) {
												return markup;
								}, // let our custom formatter work
								minimumInputLength: 3,
					});

				@if(isset($ventas[0]->cliente->nombre))	
					@php
						$ruc = $ventas[0]->cliente->documento_identidad;
						if($ventas[0]->cliente->dv){
							$ruc .= $ruc."-".$ventas[0]->cliente->dv;
						}
					@endphp
					$('#cliente_id').append('<option value="{{$ventas[0]->id_cliente}}">{{$ruc}} - {{$ventas[0]->cliente->nombre}} {{$ventas[0]->cliente->apellido}}</option>')
					$('#cliente_id').trigger("chosen:updated");
				@endif

				$("#producto_0").select2({
						ajax: {
								url: "{{route('getProductoVenta')}}",
								dataType: 'json',
								placeholder: "Seleccione un Producto",
								delay: 0,
								data: function (params) {
											return {
												q: params.term, // search term
												page: params.page
													};
								},
								processResults: function (data, params){
											var results = $.map(data, function (value, key) {
											console.log(value);
										/* return {
													children: $.map(value, function (v) {*/ 
														return {
																	id: value.id,
																	text: value.denominacion
																};
													/*  })
													};*/
											});
													return {
														results: results,
													};
								},
								cache: true
								},
								escapeMarkup: function (markup) {
												return markup;
								}, // let our custom formatter work
								minimumInputLength: 3,
					});

					$("#proveedor_0").select2({
						ajax: {
								url: "{{route('getProveedorVenta')}}",
								dataType: 'json',
								placeholder: "Seleccione un Producto",
								delay: 0,
								data: function (params) {
											return {
												q: params.term, // search term
												page: params.page
													};
								},
								processResults: function (data, params){
											var results = $.map(data, function (value, key) {
											console.log(value);
										/* return {
													children: $.map(value, function (v) {*/ 
														return {
																	id: value.id,
																	text: value.nombre+" "+value.apellido
																};
													/*  })
													};*/
											});
													return {
														results: results,
													};
								},
								cache: true
								},
								escapeMarkup: function (markup) {
												return markup;
								}, // let our custom formatter work
								minimumInputLength: 3,
					});

				$('#moneda').val('{{$ventas[0]->id_moneda}}').select2();
				$('#tipo_factura').val('{{$ventas[0]->id_tipo_factura}}').select2();
				$('#id_forma_pago').val('{{$ventas[0]->id_forma_pago}}').select2(); 
				$('#zonasSelect').val('{{$ventas[0]->zona_id}}').select2();
				$('#origen_comercio').val('{{$ventas[0]->id_comercio_empresa}}').select2();
				$('#tipo_factura').trigger('change');
				@foreach($ventas[0]->ventasDetalle as $key=>$ventasDetalle)	
					@if($ventasDetalle->activo == 't')
						cantItem = parseInt($('#lista_ventas >tbody >tr').length)+1;
						cantidad = "{{$ventasDetalle->cantidad}}";
						producto_id = "{{$ventasDetalle->id_producto}}";
						costo = "{{$ventasDetalle->precio_costo}}";
	                    inputCosto = "{{$ventasDetalle->precio_costo}}";
						iva = 0;
						venta = "{{$ventasDetalle->precio_venta}}";
						origen = "{{$ventasDetalle->origen}}";
						valor_iva = 0;
						id_iva = 0;  
						item = "";
						item =  "{{$ventasDetalle->item}}";  
						nrofactura = "{{$ventasDetalle->nro_factura_proveedor}}";
						proveedor = "{{$ventasDetalle->id_proveedor}}";	
						console.log(origen);		
						tabla = `
						<tr id="lineaVenta_${cantItem}" class="tabla_filas">
							<td><input type="text" id ="cantidad_${cantItem}" disabled="disabled" value = "${cantidad}" class = "form-control form-control-sm numerico cant" name="datos[${cantItem}][cantidad]"/></td>
							<td>
								<select id ="producto_${cantItem}" disabled="disabled" class="form-control select2 producto" name="datos[${cantItem}][producto_id]" data="${cantItem}" style="width: 100%;">`;
									tabla += `<option value="{{$ventasDetalle->id_producto}}">{{$ventasDetalle->producto_detalle}} </option>`;
								tabla += `</select>                          
							</td>
							<td>
								<div class="input-group-prepend" id="button-addon1" style="width: 100%;">`;
										if(proveedor ==""){
			                       		 tabla += `<button data="${cantItem}" class="btn btn-primary botonProveedor" id="botonProveedor${cantItem}" data-toggle="modal" data-target="#requestModalProveedor" type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-id-card-o"></i></button>`;
										}

										tabla += `<select id ="proveedor_${cantItem}" disabled="disabled" class="form-control select2 proveedor" name="datos[${cantItem}][proveedor_id]" data="${cantItem}" style="width: 100%;">
										<option value="">Seleccione Proveedor</option>`;
										@foreach($proveedores as $proveedor)
										tabla += '<option value="{{$proveedor->id}}">{{$proveedor->nombre}} {{$proveedor->apellido}}</option>';
										@endforeach
									tabla += `</select>                        
			                    </div>
							</td>
							<td>
	                            <input type="text" class = "form-control form-control-sm" disabled="disabled" required  name="datos[${cantItem}][nro_factura]" id="nro_factura_${cantItem}" value="${nrofactura}"/>
	                        </td>
	                        <td>`;
	                        tabla +=`<input type="text" class = "form-control form-control-sm " disabled="disabled" name="datos[${cantItem}][item]" name="item_${cantItem}" value="${item}" id="item_${cantItem}"/>`;
	                        
	                        tabla +=`</td>       
							<td>
	                            <input type="hidden" class = "form-control form-control-sm numerico costo" name="datos[${cantItem}][costo]" disabled="disabled" value="${costo}" id="costo_${cantItem}"/>
								<input type="text" class = "form-control form-control-sm numerico input_costo" disabled="disabled" name="datos[${cantItem}][input_costo]" value="${inputCosto}" id="input_costo_${cantItem}"/>

	                        </td>
							<td><input type="text" id ="iva_${cantItem}" disabled="disabled"  value = "${iva}" class = "form-control form-control-sm numerico" name="datos[${cantItem}][iva]"/></td>
							<td>
							<input type="text" id ="venta_${cantItem}" disabled="disabled" value = "${venta}" class = "form-control form-control-sm numerico valor" name="datos[${cantItem}][venta]"/></td>
							<input type="hidden" class = "form-control form-control-sm" value ="${id_iva}" name="datos[${cantItem}][tipo_impuesto]" id="tipo_impuesto_${cantItem}"/>
							<input type="hidden" class = "form-control form-control-sm" value ="${valor_iva}" name="datos[${cantItem}][valor_impuesto]" id="valor_impuesto_${cantItem}" />
							<input type="hidden" class = "form-control form-control-sm" value ="${origen}" name="datos[${cantItem}][origen]" id="origen_${cantItem}" />
							<td>`
							//if(origen === "M"){
							tabla += `<a class="glyphicon glyphicon-save-file" title="Guardar Detalle" onclick="guardarDetalle(${cantItem})" id="save_${cantItem}" style="margin-left: 5px;"><i class="ft-save oculto" style="font-size: 18px;"></i></a>
								<a class="glyphicon glyphicon-save-file" id="cancelar_${cantItem}" onclick="cancelarDetalle(${cantItem})" title="Cancelar"  style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>

								<a class="glyphicon glyphicon-save-file" onclick="modificarDetalle(${cantItem})" id="modificar_${cantItem}" title=Modificar Detalle" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>	 
								<a class="glyphicon glyphicon-save-file" id="eliminar_${cantItem}" onclick="eliminarDetalle(${cantItem})" title="Eliminar Detalle" style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>`
							//}	
							tabla += `
								<input type="hidden" id ="productoId_${cantItem}" value = "${producto_id}"  class = "form-control form-control-sm numerico" name="datos[${cantItem}][producto]"/>
								<input type="hidden" id ="ivaMonto_${cantItem}" value = "0"  class = "form-control form-control-sm numerico ivaMonto"/>

							</td>
						</tr>
						`;
						$('#lista_ventas tbody').append(tabla);
						$('#producto_'+cantItem).select2();
						$('#producto_'+cantItem).val(producto_id).select2();
						$('#producto_'+cantItem).prop("disabled", true);
						$('#proveedor_'+cantItem).select2();
						$('#proveedor_'+cantItem).val(proveedor).trigger('change.select2');
						$('#proveedor_'+cantItem).prop("disabled", true);
						$('#save_'+cantItem).css('display','none');
						$('#cancelar_'+cantItem).css('display','none');
						/*$.ajax({
							type: "GET",
							url: "{{route('getIvaProcudcto')}}",
							dataType: 'json',
							data: {
									idPorducto : producto_id,
									idLinea: cantItem,
									itemBase: item
								},
								error: function(){
									$('.load_err').hide();
									$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
									},
								success: function(rsp){
									console.log('Ingresoooo1');
									cantidad  = $('#cantidad_'+rsp.linea).val();
									costo  = parseInt(cantidad) * parseFloat(rsp.valorReal);
									venta  = parseInt(cantidad) * parseFloat(rsp.valorVenta);

									$('#iva_'+rsp.linea).val(rsp.denominacion);
									$('#valor_impuesto_'+rsp.linea).val(rsp.valor);
									$('#tipo_impuesto_'+rsp.linea).val(rsp.id);
									$('#item_'+rsp.linea).val(rsp.item);
									$('#costo_'+rsp.linea).val(costo);
									$('#venta_'+rsp.linea).val(venta);
	                               if(rsp.valorReal > 0){
	                               	console.log('{ENtro}');
	                               		$('#input_costo_'+rsp.linea).val(0);
	                                    $('#input_costo_'+rsp.linea).prop("disabled", true);
	                                }else{
	                                    $('#input_costo_'+rsp.linea).prop("disabled", true);
	                                }
									if(venta > 0){
										$('#venta_'+rsp.linea).prop("disabled", true);
									}else{
										$('#venta_'+rsp.linea).prop("disabled", false);
									}

									if(rsp.proveedor_id != 0){	
										$('#proveedor_'+rsp.linea).append('<option value="'+rsp.proveedor_id+'">'+rsp.proveedor_nombre+'</option>')
										$('#proveedor_'+rsp.linea).trigger("chosen:updated");
										$('#proveedor_'+rsp.linea).val(rsp.proveedor_id).select2();
									}	

								}
						})		
	                    */
						monto5 = "{{$ventasDetalle->iva_5}}";	
						total5 = parseFloat($('#total_5').val()) + parseFloat(monto5);
						monto10 = "{{$ventasDetalle->iva}}";
						total10 = parseFloat($('#total_10').val()) + parseFloat(monto10);
						ivaMonto = parseFloat(monto5)+ parseFloat(monto10);
						$('#total_5').val(total5); 
						$('#total_10').val(total10); 
						$('#ivaMonto_'+cantItem).val(ivaMonto);

					@endif	
				@endforeach                    	
 			baseProveedor();
			$('#mostrar_0').on('click',function(){
				if($('#proveedor_'+$(this).attr('data')).val() != null && $('#proveedor_'+$(this).attr('data')).val() != 0){
					$('#proveedor_'+$(this).attr('data')).find('.select2-container--default .select2-selection--single').css('border-color','d2d6de');
					if($('#producto_'+$(this).attr('data')).val() != null && $('#producto_'+$(this).attr('data')).val() != 0){ 
							$('#producto_'+$(this).attr('data')).find('.select2-container--default .select2-selection--single').css('border-color','d2d6de');
							if($('#item_'+$(this).attr('data')).val() != null && $('#item_'+$(this).attr('data')).val() != ""){
								$('#item_'+$(this).attr('data')).css('border-color', '#d2d6de'); 
								if($('#venta_'+$(this).attr('data')).val() != null && $('#venta_'+$(this).attr('data')).val() != 0){
									$('#venta_'+$(this).attr('data')).css('border-color', '#d2d6de'); 
									cantItem = parseInt($('#lista_ventas >tbody >tr').length)+1;
									cantidad = $('#cantidad_'+$(this).attr('data')).val();
									producto_id = $('#producto_'+$(this).attr('data')).val();
									producto_text = $('#producto_'+$(this).attr('data')+' :selected').text();
									costo = $('#costo_'+$(this).attr('data')).val();
	                                inputCosto = $('#input_costo_'+$(this).attr('data')).val();
									iva = $('#iva_'+$(this).attr('data')).val();
									venta = $('#venta_'+$(this).attr('data')).val();
									valor_iva = $('valor_impuesto_'+$(this).attr('data')).val();
									id_iva = $('#tipo_impuesto_'+$(this).attr('data')).val();  
									item =  $('#item_'+$(this).attr('data')).val();  
									nrofactura =  $('#nro_factura_'+$(this).attr('data')).val();
									proveedor =  $('#proveedor_'+$(this).attr('data')).val();
									tabla = `
									<tr id="lineaVenta_${cantItem}" class="tabla_filas">
										<td><input type="text" id ="cantidad_${cantItem}" disabled="disabled" value = "${cantidad}" class = "form-control form-control-sm numerico" name="datos[${cantItem}][cantidad]"/></td>
										<td><select id ="producto_${cantItem}" disabled="disabled" class="form-control select2 producto" name="datos[${cantItem}][producto_id]" data="${cantItem}" style="width: 100%;">
												<option value="`+producto_id+`">`+producto_text+`</option>`;
											tabla += `</select>
										</td>
										<td><select id ="proveedor_${cantItem}" disabled="disabled" class="form-control select2 proveedor" name="datos[${cantItem}][proveedor_id]" data="${cantItem}" style="width: 100%;">
												<option value="">Seleccione Proveedor</option>`;
												@foreach($proveedores as $proveedor)
												tabla += '<option value="{{$proveedor->id}}">{{$proveedor->nombre}} {{$proveedor->apellido}}</option>';
												@endforeach
											tabla += `</select>
										</td>
										<td>
				                            <input type="text" class = "form-control form-control-sm numerico" disabled="disabled" required  name="datos[${cantItem}][nro_factura]" id="nro_factura_${cantItem}" value="${nrofactura}"/>
				                        </td>
				                        <td>
				                            <input type="text" class = "form-control form-control-sm " disabled="disabled" name="datos[${cantItem}][item]" name="item_${cantItem}" value="${item}" id="item_${cantItem}"/>
				                        </td>       
										<td>
	                                        <input type="hidden" class = "form-control form-control-sm numerico costo" name="datos[${cantItem}][costo]" disabled="disabled" value="${costo}" id="costo_${cantItem}"/>
											<input type="text" class = "form-control form-control-sm numerico input_costo" disabled="disabled" name="datos[${cantItem}][input_costo]" value="${inputCosto}" id="input_costo_${cantItem}"/>
	                                    </td>
										<td><input type="text" id ="iva_${cantItem}" disabled="disabled"  value = "${iva}" class = "form-control form-control-sm numerico" name="datos[${cantItem}][iva]"/></td>
										<td>
										<input type="text" id ="venta_${cantItem}" disabled="disabled" value = "${venta}" class = "form-control form-control-sm numerico valor" name="datos[${cantItem}][venta]"/></td>
										<input type="hidden" class = "form-control form-control-sm" value ="${id_iva}" name="datos[${cantItem}][tipo_impuesto]" id="tipo_impuesto_${cantItem}"/>
										<input type="hidden" class = "form-control form-control-sm" value ="${valor_iva}" name="datos[${cantItem}][valor_impuesto]" id="valor_impuesto_${cantItem}"/>

										<td>
										    <a class="glyphicon glyphicon-save-file" title="Guardar Detalle" onclick="setTimeout(guardarDetalle(${cantItem}), 1000);" id="save_${cantItem}" style="margin-left: 5px; display:none"><i class="ft-save oculto" style="font-size: 18px;"></i></a>
											<a class="glyphicon glyphicon-save-file" id="cancelar_${cantItem}" onclick="cancelarDetalle(${cantItem})" title="Cancelar"  style="margin-left: 5px; display:none"><i class="ft-x-circle" style="font-size: 18px;"></i></a>
											<a class="glyphicon glyphicon-save-file" id="modificar_${cantItem}" onclick="modificarDetalle(${cantItem})" title=Modificar Detalle" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>	
											<a class="glyphicon glyphicon-save-file" id="eliminar_${cantItem}" onclick="eliminarDetalle(${cantItem})" title="Eliminar Detalle"  style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>
											<input type="hidden" id ="productoId_${cantItem}" value = "${producto_id}"  class = "form-control form-control-sm numerico" name="datos[${cantItem}][producto]"/>
											<input type="hidden" id ="ivaMonto_${cantItem}" value = "0"  class = "form-control form-control-sm numerico ivaMonto"/>
										</td>
									</tr>
									`;
									$('#lista_ventas tbody').append(tabla);
									$('#producto_'+cantItem).select2();
									$('#producto_'+cantItem).val(producto_id).select2();
									$('#producto_'+cantItem).prop("disabled", true);
									$('#proveedor_'+cantItem).select2();
									$('#proveedor_'+cantItem).val(proveedor).select2();
									$('#proveedor_'+cantItem).prop("disabled", true);
									$('#input_costo'+cantItem).prop("disabled", true);
									clear($(this).attr('data'));
									comboProductos(0);
									comboProveedor(0);
									comboProductos(cantItem);
									comboProveedor(cantItem);
									sumar(iva, venta,cantItem);
									}else{
										$('#venta_'+$(this).attr('data')).css('border-color', 'red');
										$.toast({
												heading: 'Error',
												text: 'Ingrese el Monto de Venta.',
												showHideTransition: 'fade',
												position: 'top-right',
												icon: 'error'
											});
							}			
								}else{
										$('#item_'+$(this).attr('data')).css('border-color', 'red');
										$.toast({
												heading: 'Error',
												text: 'Ingrese el Item.',
												showHideTransition: 'fade',
												position: 'top-right',
												icon: 'error'
											});
							}			
						}else{
							$('#producto_'+$(this).attr('data')).find('.select2-container--default .select2-selection--single').css('border','1px solid red');
								$.toast({
										heading: 'Error',
										text: 'Seleccione un Producto.',
										showHideTransition: 'fade',
										position: 'top-right',
										icon: 'error'
									});
						}
					}else{
							$('#proveedor_'+$(this).attr('data')).find('.select2-container--default .select2-selection--single').css('border','1px solid red');
								$.toast({
										heading: 'Error',
										text: 'Seleccione un Proveedor.',
										showHideTransition: 'fade',
										position: 'top-right',
										icon: 'error'
									});
						}				
				});
				productoCh();
			});

		$('.cant').change(function(){ 
			cantidad  = $(this).val(); 
			indice  =$(this).attr('id');
			linea = indice.split('_');
			valorReal = clean_num($('#costo_0').val());
			valorVenta = (clean_num($('#venta_0').val())/cantidad);
			costo  = parseInt(cantidad) * parseFloat(valorReal);
			venta  = parseInt(cantidad) * parseFloat(valorVenta);
			$('#costo_'+linea[1]).val(costo);
			$('#venta_'+linea[1]).val(venta);
		})	

		function productoCh(){
			$('.cant').change(function(){ 
				cantidad  = $(this).val(); 
				indice  =$(this).attr('id');
				linea = indice.split('_');
				valorReal = clean_num($('#costo_0').val());
				valorVenta = clean_num($('#venta_0').val());
				costo  = parseInt(cantidad) * parseFloat(valorReal);
				venta  = parseInt(cantidad) * parseFloat(valorVenta);
				$('#costo_'+linea[1]).val(costo);
				$('#venta_'+linea[1]).val(venta);
			})	

			$('.producto').change(function(){ 
				console.log($(this).attr('data'));
				linea = $(this).attr('data');
				$.ajax({
					type: "GET",
					url: "{{route('getIvaProcudcto')}}",
					dataType: 'json',
					data: {
							idPorducto : $(this).val(),
							idLinea: $(this).attr('data')
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
							success: function(rsp){
									console.log('Ingresoooo1');
									cantidad  = $('#cantidad_'+rsp.linea).val();
									costo  = parseInt(cantidad) * parseFloat(rsp.valorReal);
									venta  = parseInt(cantidad) * parseFloat(rsp.valorVenta);

									$('#iva_'+rsp.linea).val(rsp.denominacion);
									$('#valor_impuesto_'+rsp.linea).val(rsp.valor);
									$('#tipo_impuesto_'+rsp.linea).val(rsp.id);
									$('#item_'+rsp.linea).val(rsp.item);
									$('#costo_'+rsp.linea).val(costo);
									$('#venta_'+rsp.linea).val(venta);
	                               if(rsp.valorReal > 0){
	                               	console.log('{ENtro}');
	                               		$('#input_costo_'+rsp.linea).val(0);
	                                    $('#input_costo_'+rsp.linea).prop("disabled", true);
	                                }else{
	                                    $('#input_costo_'+rsp.linea).prop("disabled", true);
	                                }
									if(venta > 0){
										$('#venta_'+rsp.linea).prop("disabled", true);
									}else{
										$('#venta_'+rsp.linea).prop("disabled", false);
									}
									if(rsp.proveedor_id != 0){	
										$('#proveedor_'+rsp.linea).append('<option value="'+rsp.proveedor_id+'">'+rsp.proveedor_nombre+'</option>')
										$('#proveedor_'+rsp.linea).trigger("chosen:updated");
										$('#proveedor_'+rsp.linea).val(rsp.proveedor_id).select2();
									}	



								}
				})		

			})

			$('.numerico').inputmask("numeric", {
			    radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				 // prefix: '$', //No Space, this will truncate the first character
				rightAlign: false,
				oncleared: function () { self.Value(''); }
			});

            $('.input_costo').change(function(){
				console.log($(this).val());
				idCosto = $(this).attr('id').split('_');
				$('#costo_'+idCosto[2]).val($(this).val());
			});	

		}	

		$('#tipo_factura').change(function(){ 
			if($(this).val()== 2){
				$("#fechaVenta").prop('disabled', true);
			}else{
				$("#fechaVenta").prop('disabled', false);
			}

		});
        
        $('.input_costo').change(function(){
				console.log($(this).val());
				idCosto = $(this).attr('id').split('_');
				$('#costo_'+idCosto[2]).val($(this).val());
			});	

		function clear(linea){
		$('#cantidad_'+linea).val(1);
			$('#producto_'+linea).val('').select2();
			$('#costo_'+linea).val(0);
			$('#iva_'+linea).val(0);
			$('#venta_'+linea).val(0);
			$('#proveedor_'+linea).val('').select2();
			$('#nro_factura_'+linea).val('');
			$('#item_'+linea).val('');
		}
		function comboProductos(linea){ 
			$("#producto_"+linea).select2({
						ajax: {
								url: "{{route('getProductoVenta')}}",
								dataType: 'json',
								placeholder: "Seleccione un Producto",
								delay: 0,
								data: function (params) {
											return {
												q: params.term, // search term
												page: params.page
													};
								},
								processResults: function (data, params){
											var results = $.map(data, function (value, key) {
											console.log(value);
										/* return {
													children: $.map(value, function (v) {*/ 
														return {
																	id: value.id,
																	text: value.denominacion
																};
													/*  })
													};*/
											});
													return {
														results: results,
													};
								},
								cache: true
								},
								escapeMarkup: function (markup) {
												return markup;
								}, // let our custom formatter work
								minimumInputLength: 3,
					});


		}

		$('#botonPasajero').click(function(){
				$.ajax({
						type: "GET",
						url: "{{route('getVerCliente')}}",
						dataType: 'json',
						data: {
							id : $('#cliente_id').val()
							},
						error: function(){
										$('.load_err').hide();
										$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
										},
						success: function(rsp){
											console.log(rsp);
											$('#nombre').val(rsp[0].nombre); 
											$('#apellido').val(rsp[0].apellido);  
											$('#tipo_identidad').val(rsp[0].id_tipo_identidad);  
											$('#documento').val(rsp[0].documento_identidad);  
											$('#email').val(rsp[0].email);  
											$('#telefono').val(rsp[0].telefono);  
											$('#direccion').val(rsp[0].direccion); 
									}		
							
						})			
		})	

		function guardarProveedor(){
				$.ajax({
						type: "GET",
						url: "{{route('getGuardarProveedor')}}",
						dataType: 'json',
						data: {
							idProducto : $('#producto_'+ $('#idLineaProveedor').val()).val(),
							idProveedor : $('#proveedorActualizar').val()
							},
						error: function(){
										$('.load_err').hide();
										$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
										},
						success: function(rsp){
											console.log(rsp);
											if(rsp.status = "OK"){
												$('#proveedor_'+ $('#idLineaProveedor').val()).select2().val(rsp.id).trigger("change");
												$('#botonProveedor'+ $('#idLineaProveedor').val()).css('display','none');
													$("#requestModalProveedor").modal('hide');
												$.toast({
														heading: 'Exito',
														text: 'Se han actualizado los datos.',
														position: 'top-right',
														showHideTransition: 'slide',
												    	icon: 'success'
													}); 	 
											}else{
												$.toast({
														heading: 'Error',
														text: 'Se han actualizado los datos.',
														showHideTransition: 'fade',
														position: 'top-right',
														icon: 'error'
													});
												}		
										}
							
						})			
		}		

		function modificarDetalle(linea){
			console.log(linea);
			$('#cantidad_'+linea).prop('disabled', false);
			$('#producto_'+linea).prop('disabled', false);

			$('#proveedor_'+linea).prop('disabled', false);
			$('#nro_factura_'+linea).prop('disabled', false);
			$('#item_'+linea).prop('disabled', false);

			$('#costo_'+linea).prop('disabled', false);
			$('#iva_'+linea).prop('disabled', false);
			$('#venta_'+linea).prop('disabled', false);
			$('#save_'+linea).css('display', 'block');
			$('#cancelar_'+linea).css('display', 'block');
			$('#eliminar_'+linea).css('display', 'none'); 
			$('#modificar_'+linea).css('display', 'none');
			producto(linea)
			productoCh();
			$('table#lista_ventas tr#lineaVenta_'+linea).find('input,select,select2,textarea').prop("disabled", false);

		}	

    	function producto(linea){
			$.ajax({
					type: "GET",
					url: "{{route('getProductos')}}",
					dataType: 'json',
					error: function(){
										$('.load_err').hide();
										$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
										},
					success: function(rsp){
											console.log(rsp);
											$.each(rsp, function (key, item){
												$('#producto_'+linea).append($("<option></option>").attr("value", item.id).text(item.denominacion)); 
											});	
										}	
					});
		}			


		function cancelarDetalle(linea){ 
			$('#save_'+linea).css('display', 'none');
			$('#cancelar_'+linea).css('display', 'none');
			$('#eliminar_'+linea).css('display', 'block'); 
			$('#modificar_'+linea).css('display', 'block');
			$('table#lista_ventas tr#lineaVenta_'+linea).find('input,select,select2,textarea').prop("disabled", true);
		}
			
		function guardarDetalle(linea){
			if($('#producto_'+linea).val() != null && $('#producto_'+linea).val() != 0){ 
				$('#producto_'+$(this).attr('data')).find('.select2-container--default .select2-selection--single').css('border-color','d2d6de');
					if($('#item_'+linea).val() != null && $('#item_'+linea).val() != ""){
						$('#item_'+linea).css('border-color', '#d2d6de'); 
						if($('#venta_'+linea).val() != null && $('#venta_'+linea).val() != 0){
								$('#venta_'+$(this).attr('data')).css('border-color', '#d2d6de'); 
								$('#save_'+linea).css('display', 'none');
								$('#cancelar_'+linea).css('display', 'none');
								$('#eliminar_'+linea).css('display', 'block'); 
								$('#modificar_'+linea).css('display', 'block');
								$('table#lista_ventas tr#lineaVenta_'+linea).find('input,select,select2,textarea').prop("disabled", true);
								sumar($('#iva_'+linea).val(), $('#venta_'+linea).val(), linea);
						}else{
							$('#venta_'+linea).css('border-color', 'red');
							$.toast({
										heading: 'Error',
										text: 'Ingrese el Monto de Venta.',
										showHideTransition: 'fade',
										position: 'top-right',
										icon: 'error'
										});
							}			
					}else{
						$('#item_'+linea).css('border-color', 'red');
						$.toast({
								heading: 'Error',
								text: 'Ingrese el Item.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});
						}			
			}else{
				$('#producto_'+linea).find('.select2-container--default .select2-selection--single').css('border','1px solid red');
				$.toast({
						heading: 'Error',
						text: 'Seleccione un Producto.',
						showHideTransition: 'fade',
						position: 'top-right',
						icon: 'error'
					});
			}			
		}	

		function eliminarDetalle(linea){
			iva = $('#iva_'+linea).val();
			venta = $('#venta_'+linea).val();
			$("#lineaVenta_"+linea).remove();
			sumar(iva,venta,linea);
		}	

		function sumar(iva, venta, linea){
			totalDeuda = 0;
			totalIva = 0;

			$(".valor").each(function(){
				console.log('valor '+clean_num($(this).val()))
				total = clean_num($(this).val());
				totalDeuda+=parseFloat(total) || 0;
			});
			$('#total_factura').val(totalDeuda);

			if(iva == 0){
				valor = clean_num($("#total_exenta").val());	
				suma_exenta = valor + venta;
				$("#total_exenta").val(suma_exenta);
			}
			if(iva == 5){
				venta = parseFloat(clean_num(venta));
				var iva5 = parseFloat(venta)*0.21;
				$("#ivaMonto_"+linea).val(iva5);
				var gravadas = venta - iva5;
				var gravadasTotal = gravadas + parseFloat(clean_num($("#total_gravadas").val()));
				$("#total_gravadas").val(gravadasTotal);
			}
			if(iva == 10){
				venta = parseFloat(clean_num(venta));
				var iva10 = parseFloat(venta)/11;
				$("#ivaMonto_"+linea).val(iva10);
				var gravadas = venta - iva10;
				var gravadasTotal = gravadas + parseFloat(clean_num($("#total_gravadas").val()));
				$("#total_gravadas").val(gravadasTotal);
			}
		}

			function guardarCliente(){
			if($('#direccion').val() != ""){
				//if($('#telefono').val() != ""){
					if($('#documento').val() != ""){
						if($('#nombre').val() != ""){
							$.ajax({
								type: "GET",
								url: "{{route('getGuardarCliente')}}",
								dataType: 'json',
								data: {
										nombre : $('#nombre').val(),
										apellido : $('#apellido').val(),
										tipo_identidad : $('#tipo_identidad').val(),
										documento : $('#documento').val(),
										telefono : $('#telefono').val(),
										direccion : $('#direccion').val()
									},
									error: function(){
										$('.load_err').hide();
										$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
										},
									success: function(rsp){
										console.log(rsp);
										if(rsp.status == 'Ok'){
											var newOption = new Option(rsp.nombre+" "+rsp.apellido, rsp.id, false, false);
												$('#cliente_id').append(newOption);
												$("#cliente_id").select2().val(rsp.id).trigger("change");
												$("#requestModalCliente").modal('hide');
											$.toast({
													heading: 'Exito',
													text: 'Se han actualizado los datos.',
													position: 'top-right',
													showHideTransition: 'slide',
											    	icon: 'success'
												}); 	
										} else {
											 $.toast({
									                heading: 'Error',
									                position: 'top-right',
									                text: 'Ocurrio un error en la comunicación con el servidor.',
									                showHideTransition: 'fade',
									                icon: 'error'
									            });
										}
									}	
							})
						}else{
			 				$.toast({
									heading: 'Error',
									position: 'top-right',
									text: 'Ingrese Nombre.',
									showHideTransition: 'fade',
									icon: 'error'
								});
						}	
					}else{
		 				$.toast({
								heading: 'Error',
								position: 'top-right',
								text: 'Ingrese Nro Documento.',
								showHideTransition: 'fade',
								icon: 'error'
							});
					}		
				/*}else{
	 				$.toast({
							heading: 'Error',
							position: 'top-right',
							text: 'Ingrese Telefono.',
							showHideTransition: 'fade',
							icon: 'error'
						});
				}*/		
			}else{
 				$.toast({
						heading: 'Error',
						position: 'top-right',
						text: 'Ingrese Direccion.',
						showHideTransition: 'fade',
						icon: 'error'
					});
			}				

		}

		$('.numerico').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

		$( "#guardarBtn" ).click(function() {
			if($('#lista_ventas >tbody >tr').length > 0){
				if($('#zonasSelect').val() != null && $('#zonasSelect').val() != 0){
					$('#zonasSelect').css('border-color', '#d2d6de');
					$('#zonasSelect').focus();
					if($('#origen_comercio').val() != null && $('#origen_comercio').val() != 0){
						$('#origen_comercio').css('border-color', '#d2d6de');
						if($('#cliente_id').val() != null){
							$('#cliente_id').css('border-color', '#d2d6de');
							$('#origen_comercio').focus();
							$('table#lista_ventas tr').find('input,select,select2,textarea').prop("disabled", false);
							$( "#frmVenta" ).submit();
						}else{
							$('#cliente_id').find('.select2-container--default .select2-selection--single').css('border','1px solid red');
							$.toast({
									heading: 'Error',
									text: 'Seleccione un Cliente.',
									showHideTransition: 'fade',
									position: 'top-right',
									icon: 'error'
								});
						}		
					}else{
						$('#origen_comercio').find('.select2-container--default .select2-selection--single').css('border','1px solid red');
						$.toast({
								heading: 'Error',
								text: 'Seleccione una Comercio.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});
					}		
				}else{
						$('#zonasSelect').find('.select2-container--default .select2-selection--single').css('border','1px solid red');
						$.toast({
								heading: 'Error',
								text: 'Seleccione una Zona.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});
					}		
			}else{
				$.toast({
						heading: 'Error',
						text: 'Ingrese un detalle en la venta',
						showHideTransition: 'fade',
						position: 'top-right',
						icon: 'error'
						});
				}	

		
		});
		$("#reactivar").click(function() {
			 	idVenta = $("#reactivar").attr('data');
				$.ajax({
					type: "GET",
					url: "{{route('getReactivarVenta')}}",
					dataType: 'json',
					data: {
							documento : idVenta
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							console.log(rsp);
							if(rsp.status == 'OK'){
								$.toast({
										heading: 'Exito',
										text: 'Se reactivado la venta.',
										position: 'top-right',
										showHideTransition: 'slide',
								    	icon: 'success'
									}); 
								$("#reactivar").css('display', 'none');
								location. reload();
							} else {
								 $.toast({
						                heading: 'Error',
						                position: 'top-right',
						                text: 'Ocurrio un error en la comunicación con el servidor.',
						                showHideTransition: 'fade',
						                icon: 'error'
						            });
							}
						}	
				})	

			})

		$("#asignarme").click(function() {
			 	idVenta = $("#asignarme").attr('data');
				$.ajax({
					type: "GET",
					url: "{{route('getAsignarVenta')}}",
					dataType: 'json',
					data: {
							documento : idVenta
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							console.log(rsp);
							if(rsp.status == 'OK'){
								$.toast({
										heading: 'Exito',
										text: 'Se te a asignado esta venta.',
										position: 'top-right',
										showHideTransition: 'slide',
								    	icon: 'success'
									}); 
								$("#asignarme").css('display', 'none');
							} else {
								 $.toast({
						                heading: 'Error',
						                position: 'top-right',
						                text: 'Ocurrio un error en la comunicación con el servidor.',
						                showHideTransition: 'fade',
						                icon: 'error'
						            });
							}
						}	
				})	

			})
		
			$('#email').change(function(){ 
				console.log($(this).val());
				linea = $(this).attr('data');
				$.ajax({
					type: "GET",
					url: "{{route('getEmailCliente')}}",
					dataType: 'json',
					data: {
							email : $(this).val(),
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
								console.log(rsp);
								if(rsp.status  == 'ERROR'){
									$.toast({
											heading: 'Error',
											text: 'Ya existe el registrado el correo. Intentelo nuevamente!',
											showHideTransition: 'fade',
											position: 'top-right',
											icon: 'error'
											});
									$('#email').val('');
									$('#email').css('border-color', '#d2d6de');
								}
								
						}
				})		

			})

			$('#documento').change(function(){ 
				console.log($(this).val());
				linea = $(this).attr('data');
				$.ajax({
					type: "GET",
					url: "{{route('getDocumentoCliente')}}",
					dataType: 'json',
					data: {
							documento : $(this).val(),
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
								console.log(rsp);
								if(rsp.status  == 'ERROR'){
									$.toast({
											heading: 'Error',
											text: 'Ya existe el registrado el documento. Intentelo nuevamente!',
											showHideTransition: 'fade',
											position: 'top-right',
											icon: 'error'
											});
									$('#documento').val('');
									$('#documento').css('border-color', '#d2d6de');
								}
								
						}
				})		

			})

			$("#botonActualizarLatLong").click(function() {
				idVenta = $("#idVenta").val();
				$.ajax({
					type: "GET",
					url: "{{route('getLatLong')}}",
					dataType: 'json',
					data: {
							documento : idVenta,
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							console.log(rsp);
							if(rsp.status == 'OK'){
								 $("#latitud").val(rsp.latitud); 
								 $("#longitud").val(rsp.longitud);
							} else {
								 $.toast({
						                heading: 'Error',
						                position: 'top-right',
						                text: 'Ocurrio un error en la comunicación con el servidor.',
						                showHideTransition: 'fade',
						                icon: 'error'
						            });
							}
						}	
				})	

			})	

			function guardarLatLong(){
				idVenta = $("#idVenta").val();
				$.ajax({
					type: "GET",
					url: "{{route('guardarLatLong')}}",
					dataType: 'json',
					data: {
							idVenta : idVenta,
							longitud : $("#longitud").val(),
							latitud: $("#latitud").val()
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							console.log(rsp);
							if(rsp.status == 'OK'){
								$("#requestModalLatLong").modal('hide');
								location. reload();
								$.toast({
										heading: 'Exito',
										text: rsp.mensaje,
										position: 'top-right',
										showHideTransition: 'slide',
								    	icon: 'success'
									}); 
							} else {
								 $.toast({
						                heading: 'Error',
						                position: 'top-right',
						                text: rsp.mensaje,
						                showHideTransition: 'fade',
						                icon: 'error'
						            });
							}
						}	
				})	
			}	
			function baseProveedor(){
				$('.botonProveedor').click(function(){ 
					console.log($(this).attr('data'));
					idProveedor = $(this).attr('data');
					$("#idLineaProveedor").val(idProveedor);
					$("#proveedorActualizar").val("").trigger('change.select2');

				})

				$('.cant').change(function(){ 
					indice  =$(this).attr('id');
					linea = indice.split('_');
					cant_ant = $('#cantidad_'+linea[1]).val();
					cantidad  = $(this).val(); 

					$.ajax({
							type: "GET",
							url: "{{route('getIvaProcudcto')}}",
							dataType: 'json',
							data: {
									idPorducto : $('#producto_'+linea[1]).val(),
									idLinea: linea[1],
									itemBase: linea[1]
								},
								error: function(){
									$('.load_err').hide();
									$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
									},
								success: function(rsp){
									console.log('Ingresoooo1');
									cantidad  = $('#cantidad_'+rsp.linea).val();
									costo  = parseInt(cantidad) * parseFloat(rsp.valorReal);
									venta  = parseInt(cantidad) * parseFloat(rsp.valorVenta);
									$('#iva_'+rsp.linea).val(rsp.denominacion);
									$('#valor_impuesto_'+rsp.linea).val(rsp.valor);
									$('#tipo_impuesto_'+rsp.linea).val(rsp.id);
									$('#item_'+rsp.linea).val(rsp.item);
									$('#costo_'+rsp.linea).val(costo);
									$('#venta_'+rsp.linea).val(venta);
	                               if(rsp.valorReal > 0){
	                               	console.log('{ENtro}');
	                               		$('#input_costo_'+rsp.linea).val(0);
	                                    $('#input_costo_'+rsp.linea).prop("disabled", true);
	                                }else{
	                                    $('#input_costo_'+rsp.linea).prop("disabled", true);
	                                }
									if(venta > 0){
										$('#venta_'+rsp.linea).prop("disabled", true);
									}else{
										$('#venta_'+rsp.linea).prop("disabled", false);
									}
									if(rsp.proveedor_id != 0){	
										$('#proveedor_'+rsp.linea).append('<option value="'+rsp.proveedor_id+'">'+rsp.proveedor_nombre+'</option>')
										$('#proveedor_'+rsp.linea).trigger("chosen:updated");
										$('#proveedor_'+rsp.linea).val(rsp.proveedor_id).select2();
									}	

								}
						})
				})	


			}
				
			$("#btnGuardarZona").click(function() {
			 	idVenta = $("#btnGuardarZona").attr('data');
				$.ajax({
					type: "GET",
					url: "{{route('getAsignarZona')}}",
					dataType: 'json',
					data: {
							documento : idVenta,
							idZona: $("#zonasSelect").val()
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							console.log(rsp);
							if(rsp.status == 'OK'){
								$.toast({
										heading: 'Exito',
										text: 'Se ha asignado la Zona a la Venta.',
										position: 'top-right',
										showHideTransition: 'slide',
								    	icon: 'success'
									}); 
								$("#asignarme").css('display', 'none');
							} else {
								 $.toast({
						                heading: 'Error',
						                position: 'top-right',
						                text: 'Ocurrio un error en la comunicación con el servidor.',
						                showHideTransition: 'fade',
						                icon: 'error'
						            });
							}
						}	
				})	

			})
		
		producto(0);

		const formatter = new Intl.NumberFormat('de-DE', {
							  currency: 'USD'
							});
		{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
			NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}
		function clean_num(n,bd=false){
			if(n && bd == false){ 
			n = n.replace(/[,.]/g,function (m) {  
					 				 if(m === '.'){
					 				 	return '';
					 				 } 
					 				  if(m === ','){
					 				 	return '.';
					 				 } 
					 			});
			return Number(n);
		}
		if(bd){
			return Number(n);
		}
		return 0;
		}		
		function comboProveedor(linea){ 
			$('.cant').change(function(){ 
					indice  =$(this).attr('id');
					linea = indice.split('_');
					cant_ant = $('#cantidad_'+linea[1]).val();
					cantidad  = $(this).val(); 

					$.ajax({
							type: "GET",
							url: "{{route('getIvaProcudcto')}}",
							dataType: 'json',
							data: {
									idPorducto : $('#producto_'+linea[1]).val(),
									idLinea: linea[1],
									itemBase: linea[1]
								},
								error: function(){
									$('.load_err').hide();
									$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
									},
								success: function(rsp){
									console.log('Ingresoooo1');
									cantidad  = $('#cantidad_'+rsp.linea).val();
									costo  = parseInt(cantidad) * parseFloat(rsp.valorReal);
									venta  = parseInt(cantidad) * parseFloat(rsp.valorVenta);
									$('#iva_'+rsp.linea).val(rsp.denominacion);
									$('#valor_impuesto_'+rsp.linea).val(rsp.valor);
									$('#tipo_impuesto_'+rsp.linea).val(rsp.id);
									$('#item_'+rsp.linea).val(rsp.item);
									$('#costo_'+rsp.linea).val(costo);
									$('#venta_'+rsp.linea).val(venta);
	                               if(rsp.valorReal > 0){
	                               	console.log('{ENtro}');
	                               		$('#input_costo_'+rsp.linea).val(0);
	                                    $('#input_costo_'+rsp.linea).prop("disabled", true);
	                                }else{
	                                    $('#input_costo_'+rsp.linea).prop("disabled", true);
	                                }
									if(venta > 0){
										$('#venta_'+rsp.linea).prop("disabled", true);
									}else{
										$('#venta_'+rsp.linea).prop("disabled", false);
									}

								}
						})
				})	
			$("#proveedor_"+linea).select2({
						ajax: {
								url: "{{route('getProveedorVenta')}}",
								dataType: 'json',
								placeholder: "Seleccione un Producto",
								delay: 0,
								data: function (params) {
											return {
												q: params.term, // search term
												page: params.page
													};
								},
								processResults: function (data, params){
											var results = $.map(data, function (value, key) {
											console.log(value);
														return {
																	id: value.id,
																	text: value.nombre+" "+value.apellido
																};
											});
													return {
														results: results,
													};
								},
								cache: true
								},
								escapeMarkup: function (markup) {
												return markup;
								}, 
								minimumInputLength: 3,
					});

		}	  
	</script>
@endsection
