@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	
	<style>
		.asistencia{
			display: none;
		}
	 	.error{
	 		color:red;
	 	},
	   	.cabeceraClass {
	   		padding: 10px;
	   		min-width: 250px;
	   		background-color:#E8EAEA;
	   		word-break: break-all;
	   		margin: 10px;

	   	},
		.modal-footer {
		    text-align: left;
		} 
		.nav-tabs>li>a {
		    color: #dacdcd;
		} 
		.base{
			cursor: pointer;
		}

		.modal-bodyProforma{
			  height:250px;
             overflow:auto;
			}

		.anularStyle {
			padding:5px 40px; 
			margin-left: 5px;
		}

		.notaCreditoStyle{
			padding:5px 40px; 
			margin-left: 5px; 
			background-color: #F4A460 !important; 
			border-color: #F4A460 !important
		}
		.imprimirFactStyle{
			padding:5px 40px; 
			margin-left: 5px; 
			background:#E5097F !important;
		}

		.editarVoucherStyle{
			margin-left: 5px; 
			padding:5px 40px;
		}

		.commentNone {
			display: none;
		}
		.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

		.text-bold{
			font-weight: 800;
			font-size: 15px;
		}

		.select2-selection {
	    	max-height: 30px !important;
		}

		.select2-selection--single{
			padding-top: 0px !important;	
		}

		.select2-selection__rendered{
			padding-right: 5px;
		}

		.note-editing-area{
    		height: 300px;
		}

	    .chat-content {
	        text-align: left;
	        float: left;       
	        position: relative;
	        display: block;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #404e67;
	        background-color: #edeef0;
	        border-radius: 4px;
	    }

		.select2-selection {
			 padding-right: 0px;
		}

	    .chat-content-left {
	        text-align: right;
	        position: relative;
	        display: block;
	        float: right;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #fff;
	        background-color: #00b5b8;
	        border-radius: 4px;
	    }  
	/*=============================================================================*/

	.valoracion {
	    position: relative;
	    overflow: hidden;
	    display: inline-block;
	}

	.valoracion input {
	    position: absolute;
	    top: -100px;
	}


	.valoracion label {
	    float: right;
	    color: #c1b8b8;
	    font-size: 30px; 
	}

	.valoracion label:hover,
	.valoracion label:hover ~ label,
	.valoracion input:checked ~ label {
	    color: #E2D532;
	}

	.select2-moneda_compra_0-container{
		padding-right: 10px;
    	padding-left: 4px;
	}

	modal-dialog modal-lg{
		margin : 0 auto;
	}

	.modal-lg{
		margin-left: 10%;
	}

/*=============================================================================================*/
	.btnAdjuntar{
			/* background-color: #00a65a !important;
			border-color: #008d4c !important; 
			color:#FFFF !important;*/s
			font-weight:800 !important;  
			padding:10px !important;
	}


	.verificarStyle {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}
	
	.btnSolicitarVerificacion {
		padding:10px !important;
		/* background: #2d3e52 !important;
		border-color: #2d3e51 !important;
		color:#FFFF !important; */
	}

	.btnModificar {
		/* background: #e2076a !important; 
		border-color: #e25a7d !important;  */
		padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnAnular{
		/* background-color: #dd4b39 !important;
    	border-color: #d73925 !important; */
    	padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnHand {
		cursor:pointer; 
		cursor: hand;
	}

	.btnHand:hover {
  opacity: 0.8;
}


.vuelosClass{
   padding: 5px 0px;
}
/*#vuelos th {
	padding-left: 0px;padding-right: 0px;
}*/
.select2-container--default .select2-selection--single{
    padding-left: 0px;
}

.select2-moneda_compra_0-container{
	    padding-left: 0px;
	    padding-right: 0px;
}
		  
	</style>
		
@endsection
@section('content')

    <!-- Main content -->
<section id="base-style">
   	<div class="card-content">
   		@include('flash::message')
        <form id="frmVenta" method="post" action="{{route('doEditVenta')}}" >
            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">Factura</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-bottom: 10px;padding-top: 10px;">
                    	
                         <div class="row">
  							<div class="col-12 col-sm-1 col-md-3">
                            	<?php 
                            		if(isset($facturas[0]->nro_factura)){
                            			$factura = $facturas[0]->nro_factura;
                            		}else{
                            			$factura = '';
                            		}
                            	?>
                            	<label>Numero de Factura</label> 
                            	<input type="text" value="{{$factura}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;color: red;" name="nrofactura" disabled="disabled" >
                            </div> 
                            <div class="col-md-3">
                            	<label>Cliente</label>
								@php
									$ruc = $facturas[0]->cliente->documento_identidad;
									if($facturas[0]->cliente->dv){
										$ruc = $ruc."-".$facturas[0]->cliente->dv;
									}
								@endphp
								<input type="text" value="{{$ruc}} - {{$facturas[0]->cliente->nombre}} {{$facturas[0]->cliente->apellido}} - {{$facturas[0]->cliente->id}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
							</div>
                          
                            <div class="col-12 col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Tipo de Factura</label> 
                                   	<input type="text" value="{{$facturas[0]->tipoFactura->denominacion}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
									<label>Fecha Venta </label>
									<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text" style="padding-right: 10px;padding-left: 10px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="hidden" value="{{--$ventas[0]->id--}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="id" id="idVenta">
											<input type="text" value="{{--date('d/m/Y', strtotime($ventas[0]->fecha_venta))--}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="fecha_venta" id="fechaVenta" disabled="disabled" tabindex="1">
										</div>
								</div>
							</div>

						</div>	
						<div class="row">
                            <div class="col-12 col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Moneda</label> 
                                    <input type="text" value="{{$facturas[0]->currency->currency_code}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" id="vendedor">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Estado </label> 
									<input type="text" value="{{$facturas[0]->estado->denominacion}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" id="vendedor">
                                </div>
                            </div>
							<div class="col-12 col-sm-4 col-md-3">
								
								<div class="form-group">
	                            	<label>Cotización</label> 
	                            	<input type="text" value='{{number_format($facturas[0]->cotizacion_factura,2,",",".")}}' class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" id="vendedorO">
                                </div>
                            </div>
                            <div class="col-12 col-sm-1 col-md-3">
                            	<label>Vencimiento</label> 
                            	<input type="text" value="{{date('d/m/Y', strtotime($facturas[0]->vencimiento))}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled">
                            </div>
                        </div> 
						<div class="row" style="margin-top: 20px;">
							<div class="col-12 col-sm-1 col-md-3">
								<label>Venta N°</label> 
								<input type="text" value="{{$facturas[0]->id_venta_rapida}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled">
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<label>Usuario de Facturación</label> 
							   	<input type="text" value="{{$usuarioImpresion->nombre}} {{$usuarioImpresion->apellido}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<label>Fecha de Facturación</label> 
                            	<input type="text" value="{{date('d/m/Y h:m:s', strtotime($facturas[0]->fecha_hora_facturacion))}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<label>Tipo de Facturación</label> 
                            	<input type="text" value="{{$facturas[0]->tipoFacturacion->denominacion}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<div class="form-group">
									<label>Fecha de Impresión</label> 
									<input type="text" value="{{date('d/m/Y h:m:s', strtotime($facturas[0]->fecha_hora_impresion_factura))}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
								</div>	
							</div>
							<div class="col-12 col-md-3"> 
								<br>
								<label>Impreso</label>						 
								<input type="text" class = "form-control form-control-sm input-sm" value="<?php echo ($facturas[0]->factura_impresa == 1) ? 'SI' : 'NO' ;?>"   readonly  style="font-weight: bold;"/>
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<label>Comprobante</label> 
                            	<input type="text" value="{{--$ventas[0]->comprobante--}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="comprobante" disabled="disabled" >
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<label>Origen Negocio</label> 
								<?php 
                            		if(!empty($comercio)){
                            			echo '<br>';
                            			echo '<img style="max-width:100px;" src="'.$comercio->logo.'" alt="'.$comercio->descripcion.'">';
                            		}else{
                            			echo '';
                            		}
                            		?>	
                            </div>
                            @if($facturas[0]->id_estado_factura == "30")
							<div class="col-12 col-md-3" style="margin-top: 20px;">
								<div class="form-group">
									<label>Usuario de Anulación</label>
									@foreach($personas as $person)
										@if($person->id == $facturas[0]->usuario_anulacion_id)
											<input type="text" class = "form-control form-control-sm input-sm" value="{{$person->apellido}}, {{$person->nombre}}" disabled="disabled"/>
										@endif
									@endforeach
								</div>
							</div>
							<div class="col-12 col-md-3" style="margin-top: 20px;">
								<div class="form-group">
									<label>Fecha de Anulación</label>
									@if(date('d/m/Y', strtotime($facturas[0]->fecha_hora_anulacion)) !== '31/12/1969')
										<input type="text" value="{{date('d/m/Y H:m:i', strtotime($facturas[0]->fecha_hora_anulacion))}}" class="form-control form-control-sm pull-right fecha input-sm" name="vencimiento" id="vencimiento" disabled="disabled">
									@endif    
								</div>
							</div>
						@endif	
						</div>	
					<br>
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-11">
							<a href="{{route('imp_fact_pedido_original',['id'=>$facturas[0]->id])}}" class="btn btn-success pull-right text-white" style="padding:5px 40px; margin-left: 5px; " role="button" title="Imprimir Original"><i class="fa fa-print fa-lg"></i></a>
							{{-- CARGA LOS BOTONES POR PERMISO DE PERSONAS --}}
							<a href="{{route('imp_fact_pedido_original',['id'=>$facturas[0]->id])}}" class="btn btn-success pull-right text-white" style="padding:5px 40px; margin-left: 5px; " role="button" title="Imprimir Original"><i class="fa fa-file fa-lg"></i></a>
							@foreach ($btn as $boton)
								<?php  echo $boton; ?>
							@endforeach
							<a href="{{route('verVenta',['id'=>$facturas[0]->id_venta_rapida])}}" class="btn btn-info pull-right text-white" style="padding:5px 40px; margin-left: 5px; " role="button" title="Ver Venta"><i class="fa fa-file fa-lg"></i></a>
						</div>
					</div>                    
					</div>
                </div>
            </div>
            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">DETALLES DE FACTURA</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">
                        <div id="mensajes"></div>
                            <div class="table-responsive">
                                <table id="lista_ventas" class="table">
                                    <thead>
                                       <tr style="background-color: #eceff1;">
                                            <th style="width: 60px;">Cant.</th>
                                            <th style="width: 18%;">Producto</th>
                                            <th style="width: 18%;">Proveedor</th>
                                            <th style="width: 10%;">Código_Producto</th>
                                            <th style="width: 15%;">Item</th>
                                            <th style="width: 10%;">Precio Unitario</th>
                                            <th style="width: 10%;">Exentos</th>
                                            <th style="width: 10%;">Iva 5%</th>
											<th style="width: 10%;">Iva 10%</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody  style='font-size: 1rem;'>
                                    	@foreach($facturas[0]->facturaDetalle as $key=>$facturaDetalle)
											@php	
												$descripcion = $facturaDetalle->producto->denominacion;
												$desc = wordwrap($descripcion,50,'<br />',true);
												$precio_unitario = 0;
												if($facturaDetalle->precio_venta != 0 || $facturaDetalle->precio_venta != ''){
													if($facturaDetalle->cantidad != 0 || $facturaDetalle->cantidad != ''){
														$precio_unitario = $facturaDetalle->precio_venta / $facturaDetalle->cantidad;
													}
												}
											@endphp	
											 <tr>
	                                            <th style="width: 60px;">{{$facturaDetalle->cantidad}}</th>
	                                            <th style="width: 18%;">{{$facturaDetalle->producto->denominacion}}</th>
	                                            <th style="width: 18%;">{{$facturaDetalle->proveedor->nombre}}</th>
	                                            <th style="width: 10%;">{{$facturaDetalle->producto->id}}</th>
	                                            <th style="width: 15%;">{{$desc}}</th>
	                                            <th style="width: 10%;">{{number_format($precio_unitario,2,",",".")}}</th>
	                                            <th style="width: 10%;">{{number_format($facturaDetalle->exento,2,",",".")}}</th>
												<th style="width: 10%;">0</th>
	                                            <th style="width: 10%;">{{number_format($facturaDetalle->gravadas_10,2,",",".")}}</th>
	                                            <th></th>
	                                        </tr>
	                                    @endforeach                                      
                                    </tbody>

                                </table>    	
                            </div>	
                        </div>
                    </div>
                </div>
            </div>

            <div class="card" style="border-radius: 14px;">
               
                 <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body">
		        		<div class="row" >
							<div class="col-md-2" >
								<div class="form-group">
									<label style="font-size: small; color: red;font-weight: 700;">TOTAL_FACTURA</label>
									<input type="text" class = "form-control form-control-sm" name="total_factura" id="total_factura" style="font-weight: bold;font-size: 15px;" value='{{number_format($totalFactura[0]->total,2,",",".")}}' disabled="disabled"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">TOTAL_BRUTO</label>
									<input type="text" class = "form-control form-control-sm" name="total_bruto" id="total_bruto" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->total_bruto_factura,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">TOTAL_NETO</label>		
									<input type="text" class = "form-control form-control-sm" name="total_neto" id="total_neto" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->total_neto_factura,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">EXENTAS</label>
									<input type="text" class = "form-control form-control-sm" name="total_bruto" id="total_exentas" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->total_exentas,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">GRAVADAS</label>
									<input type="text" class = "form-control form-control-sm" name="total_iva" id="total_gravadas" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->total_gravadas,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">IVA</label>
									<input type="text" class = "form-control form-control-sm" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->total_iva,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">COSTOS</label>
									<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->total_costos,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">COMISIÓN</label>
									<input type="text" class = "form-control form-control-sm" name="total_comision" id="total_comision" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->ventaRapida->total_comision_vendedor_empresa,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-2" >
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">NO_COMISIONA</label>
									<input type="text" class = "form-control form-control-sm" name="total_comision" id="total_no_comisiona" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->total_no_comisiona,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">MARKUP</label>
									<input type="text" class = "form-control form-control-sm" name="markupF" id="markupF" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->markup,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">% GCIA.</label>
									<input type="text" class = "form-control form-control-sm" name="total_costo" id="porcentaje_ganancia" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->porcentaje_ganancia,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">GANANCIA</label>
									<input type="text" class = "form-control form-control-sm" name="senh" id="ganancia_venta" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->ganancia_venta,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">INCENTIVO</label>
									<input type="text" class = "form-control form-control-sm" name="incentivo" id="incentivo" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->incentivo_vendedor_agencia,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">RENTA</label>
									<input type="text" class = "form-control form-control-sm" name="incentivo" id="renta" style="font-weight: bold;font-size: 15px;" value='{{number_format($facturas[0]->renta,2,",",".")}}' disabled="disabled" tabindex="15"/>
								</div>
							</div>
						</div>	
	         	</div>
	       	</div>
            </div>
            
        </from>    
	</div>
</section>		

@endsection
@section('scripts')

	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>

	<!-- wysihtml core javascript with default toolbar functions --> 
	
	<script>

	
	</script>
@endsection
