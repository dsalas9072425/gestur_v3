@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	
	<style>
		.asistencia{
			display: none;
		}
	 	.error{
	 		color:red;
	 	},
	   	.cabeceraClass {
	   		padding: 10px;
	   		min-width: 250px;
	   		background-color:#E8EAEA;
	   		word-break: break-all;
	   		margin: 10px;

	   	},
		.modal-footer {
		    text-align: left;
		} 
		.nav-tabs>li>a {
		    color: #dacdcd;
		} 
		.base{
			cursor: pointer;
		}

		.modal-bodyProforma{
			  height:250px;
             overflow:auto;
			}

		.commentNone {
			display: none;
		}
		.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

		.text-bold{
			font-weight: 800;
			font-size: 15px;
		}

		.select2-selection {
	    	max-height: 30px !important;
		}

		.select2-selection--single{
			padding-top: 0px !important;	
		}

		.select2-selection__rendered{
			padding-right: 5px;
		}

		.note-editing-area{
    		height: 300px;
		}

	    .chat-content {
	        text-align: left;
	        float: left;       
	        position: relative;
	        display: block;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #404e67;
	        background-color: #edeef0;
	        border-radius: 4px;
	    }

		.select2-selection {
			 padding-right: 0px;
		}

	    .chat-content-left {
	        text-align: right;
	        position: relative;
	        display: block;
	        float: right;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #fff;
	        background-color: #00b5b8;
	        border-radius: 4px;
	    }  
	/*=============================================================================*/

	.valoracion {
	    position: relative;
	    overflow: hidden;
	    display: inline-block;
	}

	.valoracion input {
	    position: absolute;
	    top: -100px;
	}


	.valoracion label {
	    float: right;
	    color: #c1b8b8;
	    font-size: 30px; 
	}

	.valoracion label:hover,
	.valoracion label:hover ~ label,
	.valoracion input:checked ~ label {
	    color: #E2D532;
	}

	.select2-moneda_compra_0-container{
		padding-right: 10px;
    	padding-left: 4px;
	}

	modal-dialog modal-lg{
		margin : 0 auto;
	}

	.modal-lg{
		margin-left: 10%;
	}

/*=============================================================================================*/
	.btnAdjuntar{
			/* background-color: #00a65a !important;
			border-color: #008d4c !important; 
			color:#FFFF !important;*/s
			font-weight:800 !important;  
			padding:10px !important;
	}


	.verificarStyle {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}
	
	.btnSolicitarVerificacion {
		padding:10px !important;
		/* background: #2d3e52 !important;
		border-color: #2d3e51 !important;
		color:#FFFF !important; */
	}

	.btnModificar {
		/* background: #e2076a !important; 
		border-color: #e25a7d !important;  */
		padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnAnular{
		/* background-color: #dd4b39 !important;
    	border-color: #d73925 !important; */
    	padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnHand {
		cursor:pointer; 
		cursor: hand;
	}

	.btnHand:hover {
  opacity: 0.8;
}


.vuelosClass{
   padding: 5px 0px;
}
/*#vuelos th {
	padding-left: 0px;padding-right: 0px;
}*/
.select2-container--default .select2-selection--single{
    padding-left: 0px;
}

.select2-moneda_compra_0-container{
	    padding-left: 0px;
	    padding-right: 0px;
}
		  
	</style>
		
@endsection
@section('content')

    <!-- Main content -->
<section id="base-style">
   	<div class="card-content">
   		@include('flash::message')
            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">Actualizar Producto</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        </ul>
                    </div>
                </div> 
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-bottom: 10px;padding-top: 10px;">
						    <form method="post" action="{{url('importExcel')}}" id="frmulario" enctype="multipart/form-data">
						    {{csrf_field()}}
						    <div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<br>
									<label class="control-label">Provedor:</label>
								</div>	
								<div class="col-xs-12 col-sm-6 col-md-6">
									<br>
									<select name="proveedor" class="select2 form-control" required id="proveedor">
										<option value="0">Seleccione un Proveedor</option>
										@foreach($proveedores as $key=>$proveedor)
											<option value="{{$proveedor['value']}}">{{$proveedor['label']}}</option>
										@endforeach
									</select>
								</div>	
							</div>
							<br>
						    <div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<br>
									<label class="control-label">Archivo:</label>
								</div>
							    <div class="col-xs-12 col-sm-6 col-md-6">  	
							    	<br>  
							        <input type="file" name="excel">
							    </div> 
						    </div>   
						    <br><br>
						        <input class="btn-primary btn-lg" type="submit" value="Procesar" style="margin-left: 15%;">
						    </form>
                    </div>
                </div>
            </div>
	</div>
</section>		

@endsection
@section('scripts')

	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<!-- wysihtml core javascript with default toolbar functions --> 
	
	<script>
	
		$('.select2').select2();
 		$("#frmulario").bind("submit", function() { 
            $.blockUI({
                                                 centerY: 0,
                                                 message: "<h2>Procesando...</h2>",
                                                 css: {
                                                     color: '#000'
                                                 }
                                            });
       });	
	</script>
@endsection
