@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	
	<style>
		.asistencia{
			display: none;
		}
	 	.error{
	 		color:red;
	 	},
	   	.cabeceraClass {
	   		padding: 10px;
	   		min-width: 250px;
	   		background-color:#E8EAEA;
	   		word-break: break-all;
	   		margin: 10px;

	   	},
		.modal-footer {
		    text-align: left;
		} 
		.nav-tabs>li>a {
		    color: #dacdcd;
		} 
		.base{
			cursor: pointer;
		}

		.modal-bodyProforma{
			  height:250px;
             overflow:auto;
			}

		.commentNone {
			display: none;
		}
		.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

		.text-bold{
			font-weight: 800;
			font-size: 15px;
		}

		.select2-selection {
	    	max-height: 30px !important;
		}

		.select2-selection--single{
			padding-top: 0px !important;	
		}

		.select2-selection__rendered{
			padding-right: 5px;
		}

		.note-editing-area{
    		height: 300px;
		}

	    .chat-content {
	        text-align: left;
	        float: left;       
	        position: relative;
	        display: block;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #404e67;
	        background-color: #edeef0;
	        border-radius: 4px;
	    }

		.select2-selection {
			 padding-right: 0px;
		}

	    .chat-content-left {
	        text-align: right;
	        position: relative;
	        display: block;
	        float: right;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #fff;
	        background-color: #00b5b8;
	        border-radius: 4px;
	    }  
	/*=============================================================================*/

	.valoracion {
	    position: relative;
	    overflow: hidden;
	    display: inline-block;
	}

	.valoracion input {
	    position: absolute;
	    top: -100px;
	}


	.valoracion label {
	    float: right;
	    color: #c1b8b8;
	    font-size: 30px; 
	}

	.valoracion label:hover,
	.valoracion label:hover ~ label,
	.valoracion input:checked ~ label {
	    color: #E2D532;
	}

	.select2-moneda_compra_0-container{
		padding-right: 10px;
    	padding-left: 4px;
	}

	modal-dialog modal-lg{
		margin : 0 auto;
	}

	.modal-lg{
		margin-left: 10%;
	}

/*=============================================================================================*/
	.btnAdjuntar{
			/* background-color: #00a65a !important;
			border-color: #008d4c !important; 
			color:#FFFF !important;*/s
			font-weight:800 !important;  
			padding:10px !important;
	}


	.verificarStyle {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}
	
	.btnSolicitarVerificacion {
		padding:10px !important;
		/* background: #2d3e52 !important;
		border-color: #2d3e51 !important;
		color:#FFFF !important; */
	}

	.btnModificar {
		/* background: #e2076a !important; 
		border-color: #e25a7d !important;  */
		padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnAnular{
		/* background-color: #dd4b39 !important;
    	border-color: #d73925 !important; */
    	padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnHand {
		cursor:pointer; 
		cursor: hand;
	}

	.btnHand:hover {
  opacity: 0.8;
}


.vuelosClass{
   padding: 5px 0px;
}
/*#vuelos th {
	padding-left: 0px;padding-right: 0px;
}*/
.select2-container--default .select2-selection--single{
    padding-left: 0px;
}

.select2-moneda_compra_0-container{
	    padding-left: 0px;
	    padding-right: 0px;
}
		  
	</style>
		
@endsection
@section('content')

    <!-- Main content -->
<section id="base-style">
   	<div class="card-content">
   		@include('flash::message')
        <form id="frmVenta" method="post" action="{{route('doEditVenta')}}" >
            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">Venta Nro: {{$ventas[0]->id}}</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-bottom: 10px;padding-top: 10px;">
                    	
                         <div class="row">
                            <div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
									<label>Fecha Venta </label>
									<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text" style="padding-right: 10px;padding-left: 10px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="hidden" value="{{$ventas[0]->id}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="id" id="idVenta">
											<input type="text" value="{{date('d/m/Y', strtotime($ventas[0]->fecha_venta))}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="fecha_venta" id="fechaVenta" disabled="disabled" tabindex="1">
										</div>
								</div>
							</div>

 							<div class="col-md-3">
									<div id="botonesModal" class="row" style="margin-left: 0px; margin-right: 0px;">
										<label>Cliente</label><br>
										<div class="input-group">
                                            <div class="input-group-prepend" id="button-addon1" style="width: 100%;">
                                                <button id="botonCliente" class="btn btn-primary" data-toggle="modal" data-target="#requestModalCliente" type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-id-card-o"></i></button>
                                            	<select class="form-control select2" name="cliente_id" id="cliente_id" style="width: 100%" aria-describedby="button-addon1" tabindex="3" >
													<option value="">Seleccione Cliente</option>
													@foreach($clientes as $cliente)
													<option value="{{$cliente->id}}">{{$cliente->full_data}} - {{$cliente->id}} </option>
													@endforeach
												</select>                                        
                                            </div>
										</div>	
									</div>
								</div>
                            <div class="col-12 col-sm-1 col-md-3">
                            	<?php 
                            		if(isset($ventas[0]->factura->nro_factura)){
                            			$factura = $ventas[0]->factura->nro_factura;
                            		}else{
                            			$factura = '';
                            		}
                            	?>
                            	<label>Numero de Factura</label> 
                            	<input type="text" value="{{$factura}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
                            </div>
                            <div class="col-12 col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Tipo de Factura</label> 
                                    <select class="form-control select2" name="tipo_factura" id="tipo_factura" tabindex="4"
                                        style="width: 100%;">
                                        <option value="">Seleccione Tipo de Factura</option>
										@foreach($tipoFacturas as $tipoFactura)
											<option value="{{$tipoFactura->id}}">{{$tipoFactura->denominacion}} </option>
										@endforeach
                                    </select>
                                </div>
                            </div>
						</div>	
						<div class="row">
                            <div class="col-12 col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Moneda</label> 
                                    <select class="form-control select2" name="moneda" id="moneda" tabindex="5"
                                        style="width: 100%;">
                                        <option value="">Seleccione Moneda</option>
										@foreach($currencys as $key=>$currency)
											<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
										@endforeach	
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Vendedor Empresa </label> 
                                    <?php 
										if(!empty($ventas[0]->vendedor->nombre)){
											$vendedorEmpresa = $ventas[0]->vendedor->nombre." ".$ventas[0]->vendedor->apellido."  (".$ventas[0]->vendedor->usuario.")";
										}else{
											$vendedorEmpresa = '';
										}
									?>
									<input type="text" value="{{$vendedorEmpresa}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" id="vendedor">
                                </div>
                            </div>
							<div class="col-12 col-sm-4 col-md-3">
									<?php 
										if(!empty($ventas[0]->vendedor_agencia)){
											$vendedorOnline = $ventas[0]->vendedor_agencia->nombre." ".$ventas[0]->vendedor_agencia->apellido;
										}else{
											$vendedorOnline = '';
										}
									?>
							<div class="form-group">
                            	<label>Vendedor Online</label> 
                            	<input type="text" value="{{$vendedorOnline}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" id="vendedorO">
                                </div>
                            </div>
                            <div class="col-12 col-sm-1 col-md-3">
                            	<label>Tipo de Venta</label> 
                            	<input type="text" value="{{$ventas[0]->tipoVenta->descripcion}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled">
                            </div>
                        </div> 
						<div class="row">
							<div class="col-12 col-sm-1 col-md-3">
								<label>Pedido</label> 
								<?php 
                            		if(!empty($ventas[0]->pedido)){
                            			$pedido = $ventas[0]->pedido->id_woo_order;
                            		}else{
                            			$pedido = '';
                            		}
                            	?>
                            	<input type="text" value="{{$pedido}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled">
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<label>Fecha Pedido</label> 
								<?php 
                            		if(!empty($ventas[0]->pedido)){
                            			$fecha = date('d/m/Y', strtotime($ventas[0]->pedido->created_at));
                            		}else{
                            			$fecha = '';
                            		}
                            	?>
                            	<input type="text" value="{{$fecha}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<label>Forma de Pago</label> 
								<?php 
                            		if(!empty($ventas[0]->formaPago)){
                            			$formaPago = $ventas[0]->formaPago->denominacion;
                            		}else{
                            			$formaPago = '';
                            		}
                            	?>
                            	<input type="text" value="{{$formaPago}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<label>Estado Pedido</label> 
                            	<input type="text" value="{{$status}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<div class="form-group">
									<label>Zona</label> 
									<?php 
                            		if(!empty($ventas[0]->zona)){
                            			$zona = $ventas[0]->zona->descripcion;
                            		}else{
                            			$zona = '';
                            		}
                            		?>
									<input type="text" value="{{$zona}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
								</div>	
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<label>Estado Venta</label> 
                            	<input type="text" value="{{$ventas[0]->estado->denominacion}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" name="nrofactura" disabled="disabled" >
							</div>

							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<label>Comprobante</label> 
                            	<input type="text" value="{{$ventas[0]->comprobante}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="comprobante" disabled="disabled" >
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<label>Origen Negocio</label> 
								<?php 
                            		if(!empty($ventas[0]->comercio)){
                            			echo '<br>';
                            			echo '<img style="max-width:100px;" src="'.$ventas[0]->comercio->logo.'" alt="'.$ventas[0]->comercio->descripcion.'">';
                            		}else{
                            			echo '';
                            		}
                            		?>	
                            </div>
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<label>Cliente Delivery</label> 
                            	<input type="text" value="{{$ventas[0]->delivery_cliente}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="delivery_cliente"  disabled="disabled" >
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<label>Teléfono Delivery</label> 
                            	<input type="text" value="{{$ventas[0]->delivery_telefono}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="delivery_telefono" disabled="disabled"  disabled="disabled"  >
							</div>	 							
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<label>Direccion Delivery</label> 
                            	<input type="text" value="{{$ventas[0]->delivery_direccion}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="delivery_direccion" disabled="disabled"  >
							</div>
							<div class="col-12 col-sm-1 col-md-3">
								<br>
								<label>Referencia</label> 
                            	<input type="text" value="{{$ventas[0]->delivery_referencia}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="delivery_referencia"  disabled="disabled" >
							</div>	
						</div>	
						<div class="row">
							<div class="col-12 col-sm-4 col-md-3" style="padding-left: 0px;padding-right: 25px;">
								<br>
									<label>Fecha Vencimiento </label>
									<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text" style="padding-right: 10px;padding-left: 10px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-calendar"></i></span>
											</div>
											@if($ventas[0]->fecha_vencimiento == "")
												<input type="text" value="" class="form-control pull-right input-sm text-bold" disabled="disabled" style="height: 30px;" name="fecha_venta" id="fechaVenta" tabindex="1">
											@else
												<input type="text"  disabled="disabled" value="{{date('d/m/Y', strtotime($ventas[0]->fecha_vencimiento))}}" class="form-control pull-right input-sm text-bold" style="height: 30px;" name="fecha_venta" id="fechaVenta" tabindex="1">	
											@endif
										</div>
								</div>	
							<div class="col-12 col-sm-1 col-md-9" style="padding-right: 0px;">
								<br>
								<label>Concepto Genérico</label> 
								<textarea name="concepto" id="concepto" rows="2" cols="200" style="width: 99%;" disabled="disabled">{{$ventas[0]->concepto_generico}}</textarea>
							</div>	
						</div>	
						<div class="row">	
							<div class="col-12 col-sm-1 col-md-3">
							<br><br>
                            	@if(empty($ventas[0]->id_vendedor))	
	                            	@if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 5 ||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 6 ||Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2 )
										<a id="asignarme" data="{{$ventas[0]->id}}" title="Asignarme Venta" class="btn btn-info" style="margin-left: 5px;width: 230px;color:#2a2e30;"><i class="ft-save" style="font-size: 18px;"></i>Asignarme Venta</a>
		                            @endif	
		                        @endif	    
                            </div>   
                            <div class="col-12 col-sm-1 col-md-3">
                            	<br><br>
                            	<button id="botonMapaZona" class="btn btn-info" data-toggle="modal" data-target="#requestModalMapaZona" style="margin-left: 5px;width: 230px; color:#2a2e30;" type="button"><i class="fa fa-id-card-o"></i>Mapa de Zona</button>
                            </div>	
                             <div class="col-12 col-sm-1 col-md-3">
                              @if(!empty($ventas[0]->delivery_lat))
                            	<br><br>
                            	<button id="botonMapaDelivery" class="btn btn-info" data-toggle="modal" data-target="#requestModalDelivery" style="margin-left: 5px;width: 230px; color:#2a2e30;" type="button"><i class="fa fa-id-card-o"></i>Mapa de Delivery</button>
                            @endif
                            </div>	
 										
						</div>      
                    </div>
                </div>
            </div>
            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">Detalles de Venta</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">
                        <div id="mensajes"></div>
                            <div class="table-responsive">
                                <table id="lista_ventas" class="table">
                                    <thead>
                                       <tr style="background-color: #eceff1;">
                                            <th style="width: 60px;">Cant.</th>
                                            <th style="width: 18%;">Producto</th>
                                            <th style="width: 18%;">Proveedor</th>
                                            <th style="width: 10%;">Código_Producto</th>
                                            <th style="width: 15%;">Item</th>
                                            <th style="width: 10%;">Costo</th>
                                            <th style="width: 10%;">Iva</th>
                                            <th style="width: 10%;">Venta</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody  style='font-size: 0.85rem;'>

                                    </tbody>

                                </table>    	
                            </div>	
                        </div>
                    </div>
                </div>
            </div>

            <div class="card" style="border-radius: 14px;">
               
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-bottom: 10px;padding-top: 10px;">
					<div class= "row">
							<input type="hidden" class = "Requerido form-control numerico" name="total_5" id="total_5" style="font-weight: bold;font-size: 15px;" value='0' disabled="disabled"/>
							<input type="hidden" class = "Requerido form-control numerico" name="total_10" id="total_10" style="font-weight: bold;font-size: 15px;" value='0' disabled="disabled"/>

							 <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: small;font-weight: 700;">COMISION_VENDEDOR</label>
                                    <input type="text" class = "Requerido form-control numerico" name="total_exenta" id="total_exenta" style="font-weight: bold;font-size: 15px;" value='{{$ventas[0]->total_comision_vendedor_empresa}}' disabled="disabled"/>
                                </div>
                            </div>	

							<div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: small;font-weight: 700;">RENTA_VENTA</label>
                                    <input type="text" class = "Requerido form-control numerico" name="total_exenta" id="total_exenta" style="font-weight: bold;font-size: 15px;" value='{{$ventas[0]->renta_venta}}' disabled="disabled"/>
                                </div>
                            </div>

							<div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: small;font-weight: 700;">COMISION_ONLINE</label>
                                    <input type="text" class = "Requerido form-control numerico" name="total_exenta" id="total_exenta" style="font-weight: bold;font-size: 15px;" value='{{$ventas[0]->total_comision_vendedor_agencia}}' disabled="disabled"/>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: small;font-weight: 700;">TOTAL_EXENTAS</label>
                                    <input type="text" class = "Requerido form-control numerico" name="total_exenta" id="total_exenta" style="font-weight: bold;font-size: 15px;" value='{{$ventas[0]->total_exentas}}' disabled="disabled"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: small;font-weight: 700;">TOTAL_GRAVADAS</label>		
                                    <input type="text" class = "Requerido form-control numerico" name="total_gravadas" id="total_gravadas" style="font-weight: bold;font-size: 15px;" value='{{$ventas[0]->total_gravadas}}' disabled="disabled" />
                                </div>
                            </div> 
							<div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: small;font-weight: 700;">IVA</label>		
                                    <input type="text" class = "Requerido form-control numerico" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px;" value='{{$ventas[0]->total_iva}}' disabled="disabled" />
                                </div>
                            </div> 
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: small; color: red;font-weight: 700;">TOTAL_FACTURA</label>
                                    <input type="text" class = "Requerido form-control numerico" name="total_factura" id="total_factura" style="font-weight: bold;font-size: 15px;" value='{{$ventas[0]->total_venta}}' disabled="disabled"/>
                                </div>						
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="footerModal">
            	@if($ventas[0]->id_estado == 71 || $ventas[0]->id_estado == 75 )
          			<button type="button" id="notaVenta" onclick="notaVentas('{{$ventas[0]->id}}')" class="btn btn-info" style="width: 30%;background-color: #e2076a;padding-right: 5px;padding-left: 5px;">GENERAR NOTA DE PEDIDO</button> 
					{{--<!--<button type="button" id="notaFactura" class="btn btn-danger" onclick="notaFacturas('{{$ventas[0]->id}}')" style="width: 120px; background-color: #e2076a;" >FACTURAR</button>-->--}}
					<a href="../editVenta/{{$ventas[0]->id}}"  class="btn btn-danger" role="button">EDITAR</a>
					<button type="button" id="id_anular_venta" class="btn btn-danger" onclick="modalAnularVentaRapida('{{$ventas[0]->id}}')" style="width: 180px; background-color: #e2076a;">ANULAR-VENTA</button>
				@endif

				@if($ventas[0]->id_estado == 72)
				  	<button type="button" id="notaVenta" onclick="descargarNota('{{$ventas[0]->id}}')" class="btn btn-info" style="width: 30%;background-color: #e2076a;padding-right: 5px;padding-left: 5px;">DESCARGAR NOTA DE PEDIDO </button>
					<button type="button" id="notaFactura" class="btn btn-danger" onclick="notaFacturas('{{$ventas[0]->id}}')" style="width: 120px; background-color: #e2076a;" >FACTURAR</button>
					<button type="button" id="id_anular_venta" class="btn btn-danger" onclick="modalAnularVentaRapida('{{$ventas[0]->id}}')" style="width: 180px; background-color: #e2076a;">ANULAR-VENTA</button>
					@if($permiso == 1)
						<a href="../editVenta/{{$ventas[0]->id}}"  class="btn btn-danger" role="button">EDITAR</a>
					@endif
				@endif

           		@if($ventas[0]->id_estado == 73)
				   <button type="button" id="notaVenta" onclick="descargarNota('{{$ventas[0]->id}}')" class="btn btn-info" style="width: 30%;background-color: #e2076a;padding-right: 5px;padding-left: 5px;">DESCARGAR NOTA DE PEDIDO</button>
				   <?php echo $btn ?> 
          			<button type="button" id="id_anular_factura" class="btn btn-danger" onclick="modalAnular('{{$ventas[0]->id_factura}}')" style="width: 180px; background-color: #e2076a;">ANULAR-FACTURA</button>
				@endif

			</div>


        </from>    
	</div>
</section>		

<!-- ========================================
   					MODAL DE CLIENTE
   		========================================  -->		
	<div id="requestModalCliente" class="modal fade" role="dialog">
	  	<div class="modal-dialog" style="width: 30%;margin-top: 14%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Cliente: <span id="clienteName"></span></h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
				   	<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class= "col-xs-12 col-sm-10 col-md-10">
                            <label>Numero de Telefono</label> 
                            <input type="text" value="{{$factura}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" id="telefono" disabled="disabled" >
						</div>
					</div>
					<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class= "col-xs-12 col-sm-10 col-md-10">
                            <label>Numero de Celular</label> 
                            <input type="text" value="{{$factura}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" id="celular" disabled="disabled" >
						</div>
					</div>
					<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class= "col-xs-12 col-sm-10 col-md-10">
                            <label>Email</label> 
                            <input type="text" value="" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" id="email" disabled="disabled" >
						</div>
					</div>			
					<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class= "col-xs-12 col-sm-10 col-md-10">
                            <label>Dirección</label> 
                            <input type="text" value="" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" id="direccion" disabled="disabled" >
						</div>
					</div>			
					<br>
					<button type="button" class="btn btn-info" style="width: 90px; background-color: #969dac;margin-left: 75%;" data-dismiss="modal">Cerrar</button>

				</div>
		  	</div>

		 </div>	
	</div>

	<!-- ========================================
   					MODAL DE MAPA DELIVERY
   		========================================  -->		
	<div id="requestModalDelivery" class="modal fade" role="dialog">
	  	<div class="modal-dialog modal-xl" style="height: 100%;width:80%;margin-left: 9%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">MAPA DE DELIVERY<span id="clienteName"></span></h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
					<div id="map-tab" style="width:100%;height:450px"></div>
					<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
					   		<button type="button" class="btn btn-info" style="width: 90px; background-color: #969dac;margin-left: 75%;" data-dismiss="modal">Cerrar</button>
						</div>
						<div class= "col-xs-12 col-sm-1 col-md-1">
							<a type="button" class="btn btn-info" style="border-color: #1ab0c3 !important; width: 169.992188px;background-color: #969dac;margin-left: 75%;border-left-width: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;padding-bottom: 12px;" href="https://maps.google.com/?q={{$ventas[0]->delivery_lat}},{{$ventas[0]->delivery_lng}}" target="_blank">Abrir en otra pestaña
							</a>
						</div>
					</div>	
				</div>
		  	</div>

		 </div>	
	</div>

	<!-- ========================================
   					MODAL DE MAPA ZONA
   		========================================  -->		
	<div id="requestModalMapaZona" class="modal fade" role="dialog">
	  	<div class="modal-dialog modal-xl" style="height: 100%;width:80%;margin-left: 9%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">MAPA DE ZONAS<span id="clienteName"></span></h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
					<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1_nipSqwHbIw0DvYhiqfGhA7Z-0rcqLvU&ll=-25.298510223966137%2C-57.54707680000001&z=12" width="100%" height="800"></iframe>
					<button type="button" class="btn btn-info" style="width: 90px; background-color: #969dac;margin-left: 75%;" data-dismiss="modal">Cerrar</button>
				</div>
		  	</div>

		 </div>	
	</div>

<!-- ========================================
   					MODAL DE PROVEEDOR
   		========================================  -->		
	<div id="requestModalProveedor" class="modal fade" role="dialog">
	  	<div class="modal-dialog" style="width: 30%;margin-top: 14%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Proveedor: <span id="proveedoName"></span></h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
				   	<div class="row" style="display: none">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class= "col-xs-12 col-sm-10 col-md-10">
                            <label>Numero de Telefono</label> 
                            <input type="text" value="{{$factura}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" id="proTelefono" disabled="disabled" >
						</div>
					</div>
					<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class= "col-xs-12 col-sm-10 col-md-10">
                            <label>Numero de Celular</label> 
                            <input type="text" value="{{$factura}}" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" id="proCelular" disabled="disabled" >
						</div>
					</div>
					<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class= "col-xs-12 col-sm-10 col-md-10">
                            <label>Email</label> 
                            <input type="text" value="" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" id="proEmail" disabled="disabled" >
						</div>
					</div>			
					<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class= "col-xs-12 col-sm-10 col-md-10">
                            <label>Dirección</label> 
                            <input type="text" value="" class="form-control pull-right fecha input-sm text-bold" style="height: 30px;" id="proDireccion" disabled="disabled" >
						</div>
					</div>			
					<br>
					<button type="button" class="btn btn-info" style="width: 90px; background-color: #969dac;margin-left: 75%;" data-dismiss="modal">Cerrar</button>
				</div>
		  	</div>

		 </div>	
	</div>
 <!-- ========================================
   					MODAL DE ZONA
   		========================================  -->		
	<div id="requestModalZona" class="modal fade" role="dialog">
	  	<div class="modal-dialog" style="width: 30%;margin-top: 14%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Zona</h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
				   	<div class="row">
					   	<div class= "col-xs-12 col-sm-1 col-md-1">
						</div>
						<div class= "col-xs-12 col-sm-10 col-md-10">
							<br>
							<select class="form-control select2" name="zonas" id="zonasSelect" tabindex="4" style="width: 100%;">
                                <option value="">Seleccione Zona</option>
								@foreach($zonas as $zona)
									<option value="{{$zona->id}}">{{$zona->descripcion}} </option>
								@endforeach
                            </select>
						</div>
					</div>
					<br>
					<button type="button" id="btnGuardarZona" data="{{$ventas[0]->id}}"  class="btn btn-info" style="width: 90px; background-color: #969dac;margin-left: 75%;" data-dismiss="modal">Aceptar</button>
				</div>
		  	</div>

		 </div>	
	</div>

	

@endsection
@section('scripts')

	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>

	<!--<script async="false" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCu2ZW8F0SdXXa2bLgnIu-vIInMdg_IMQw&libraries=places&callback=initialize" type="text/javascript"></script>

	 wysihtml core javascript with default toolbar functions --> 
	
	<script>

		var map;
		var panorama;
		var fenway;
		var mapOptions;
		var panoramaOptions;





		$(function () {
			$("#fechaVenta").datepicker({
				dateFormat: 'dd/mm/yy'
			});
		//	$("#fechaVenta").datepicker('setDate', new Date());
		$("#zonasSelect").prop('disabled', false);


		




		});

		{{-- /*
			@if(!empty($ventas[0]->delivery_lat))
			var lat = {{$ventas[0]->delivery_lat}};
				var lon = {{$ventas[0]->delivery_lng}};
			@else 
				var lat = 0;
				var lon = 0;		
			@endif	
			console.log(lat);
			console.log(lon);*/
			
			--}}
			
		/*	function initialize(){	
			console.log('Ingreso');	
				map = new google.maps.Map(document.getElementById('map-tab'), {
					center: {lat: lat, lng: lon},
					zoom: 18
				});
				var marker = new google.maps.Marker({
				  position: map.getCenter(),
				  map: map,
				  title: "<?php echo 'Direccion Delivery'; ?>"
				});
				var descripcion = '';
				descripcion = '<b>Numero1</b><br>Ciudad<br>Precio desde: <b>{1.000'

				var infowindow = new google.maps.InfoWindow({
					content: descripcion
				});

				marker.addListener('click', function() {
					infowindow.open(map, marker);
				});				
			}*/

		$(document).ready(function()
		{
				//initialize();
				$('.select2').select2();
				$('#cliente_id').val('{{$ventas[0]->id_cliente}}').select2();
				$('#moneda').val('{{$ventas[0]->id_moneda}}').select2();
				$('#tipo_factura').val('{{$ventas[0]->id_tipo_factura}}').select2();
				$('.select2').prop("disabled", true);
				@foreach($ventas[0]->ventasDetalle as $key=>$ventasDetalle)	
					@if($ventasDetalle->activo == 't')
						cantItem = parseInt($('#lista_ventas >tbody >tr').length)+1;
						cantidad = "{{$ventasDetalle->cantidad}}";
						producto_id = "{{$ventasDetalle->id_producto}}";
						costo = "{{$ventasDetalle->precio_costo}}";
						console.log(costo);
	                    inputCosto = "{{$ventasDetalle->precio_costo}}";
						iva = 0;
						venta = "{{$ventasDetalle->precio_venta}}";
						origen = "{{$ventasDetalle->origen}}";
						valor_iva = 0;
						id_iva = 0;  
						item =  "{{$ventasDetalle->item}}";  
						nrofactura = "{{$ventasDetalle->nro_factura_proveedor}}";
						proveedor = "{{$ventasDetalle->id_proveedor}}";	
						montoIva = "{{$ventasDetalle->id_proveedor}}";		
						tabla = `
						<tr id="lineaVenta_${cantItem}" class="tabla_filas">
							<td><input type="text" id ="cantidad_${cantItem}" disabled="disabled" value = "${cantidad}" class = "form-control form-control-sm numerico" name="datos[${cantItem}][cantidad]"/></td>
							<td>
								<input type="text" class = "form-control form-control-sm" disabled="disabled" required  name="datos[${cantItem}][producto_id]" id="producto_${cantItem}"/>
							</td>
							<td>
								<div class="input-group">
	                                <div class="input-group-prepend" id="button-addon1" style="width: 	100%;">`;
		                                if(proveedor != ""){
											tabla += `<button id="botonCliente" class="btn btn-primary" data-toggle="modal" onclick="datProveedor(`+proveedor+`)" type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-id-card-o"></i></button>`;
		                                }

	                                   tabla += ` <select id ="proveedor_${cantItem}" disabled="disabled" class="form-control select2 proveedor" name="datos[${cantItem}][proveedor_id]" data="${cantItem}" style="width: 100%;">
												<option value="">Seleccione Proveedor</option>`;
												@foreach($proveedores as $proveedor)
												tabla += '<option value="{{$proveedor->id}}">{{$proveedor->nombre}} {{$proveedor->apellido}}</option>';
												@endforeach
											tabla += `</select>                                     
	                                            </div>
											</div>	
							</td>
							<td>
	                            <input type="text" class = "form-control form-control-sm" disabled="disabled" required  name="datos[${cantItem}][nro_factura]" id="nro_factura_${cantItem}" value="${nrofactura}"/>
	                        </td>
	                        <td>
	                            <input type="text" class = "form-control form-control-sm " disabled="disabled" name="datos[${cantItem}][item]" name="item_${cantItem}" value="${item}" id="item_${cantItem}"/>
	                        </td>       
							<td>
	                            <input type="hidden" class = "form-control form-control-sm numerico costo" name="datos[${cantItem}][costo]" disabled="disabled" value="${costo}" id="costo_${cantItem}"/>
								<input type="text" class = "form-control form-control-sm numerico input_costo" disabled="disabled" name="datos[${cantItem}][input_costo]" value="${inputCosto}" id="input_costo_${cantItem}"/>

	                        </td>
							<td><input type="text" id ="iva_${cantItem}" disabled="disabled"  value = "${iva}" class = "form-control form-control-sm numerico" name="datos[${cantItem}][iva]"/></td>
							<td>
							<input type="text" id ="venta_${cantItem}" disabled="disabled" value = "${venta}" class = "form-control form-control-sm numerico valor" name="datos[${cantItem}][venta]"/></td>
							<input type="hidden" class = "form-control form-control-sm" value ="${id_iva}" name="datos[${cantItem}][tipo_impuesto]" id="tipo_impuesto_${cantItem}"/>
							<input type="hidden" class = "form-control form-control-sm" value ="${valor_iva}" name="datos[${cantItem}][valor_impuesto]" id="valor_impuesto_${cantItem}" />

							<td>`;
						
							tabla += `
								<input type="hidden" id ="productoId_${cantItem}" value = "${producto_id}"  class = "form-control form-control-sm numerico" name="datos[${cantItem}][producto]"/>
								<input type="hidden" id ="ivaMonto_${cantItem}" value = "0"  class = "form-control form-control-sm numerico ivaMonto"/>

							</td>
						</tr>
						`;
						$('#lista_ventas tbody').append(tabla);
						$('#proveedor_'+cantItem).select2();
						$('#proveedor_'+cantItem).val(proveedor).trigger('change.select2');
						$('#proveedor_'+cantItem).prop("disabled", true);

						$('#save_'+cantItem).css('display','none');
						$('#cancelar_'+cantItem).css('display','none');
						$.ajax({
							type: "GET",
							url: "{{route('getIvaProcudcto')}}",
							dataType: 'json',
							data: {
									idPorducto : producto_id,
									idLinea: cantItem
								},
								error: function(){
									$('.load_err').hide();
									$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
									},
								success: function(rsp){
									console.log(rsp);
									$('#iva_'+rsp.linea).val(rsp.denominacion);
									$('#valor_impuesto_'+rsp.linea).val(rsp.valor);
									$('#tipo_impuesto_'+rsp.linea).val(rsp.id);
									//$('#item_'+rsp.linea).val(rsp.item);
	                               if(rsp.valorReal != 0 && rsp.valorReal != null){
	                                	$('#input_costo_'+rsp.linea).val(rsp.input);
	                                    $('#input_costo_'+rsp.linea).prop("disabled", true);
	                                }
									$('#item_'+rsp.linea).val(rsp.item);
									$('#producto_'+rsp.linea).val(rsp.producto);

								}
						})		
	                    
						monto5 = "{{$ventasDetalle->iva_5}}";	
						total5 = parseFloat($('#total_5').val()) + parseFloat(monto5);
						monto10 = "{{$ventasDetalle->iva}}";
						total10 = parseFloat($('#total_10').val()) + parseFloat(monto10);
						ivaMonto = parseFloat(monto5)+ parseFloat(monto10);
						$('#total_5').val(total5); 
						$('#total_10').val(total10); 
						$('#ivaMonto_'+cantItem).val(ivaMonto);
					@endif 
				@endforeach   
 
			/*	$('#mostrar_0').on('click',function(){
					cantItem = parseInt($('#lista_ventas >tbody >tr').length)+1;
					cantidad = $('#cantidad_'+$(this).attr('data')).val();
					producto_id = $('#producto_'+$(this).attr('data')).val();
					producto_text = $('#producto_'+$(this).attr('data')+' :selected').text();
					costo = $('#costo_'+$(this).attr('data')).val();
					iva = $('#iva_'+$(this).attr('data')).val();
					venta = $('#venta_'+$(this).attr('data')).val();
					valor_iva = $('valor_impuesto_'+$(this).attr('data')).val();
					id_iva = $('#tipo_impuesto_'+$(this).attr('data')).val();  
					tabla = `
					<tr id="lineaVenta_${cantItem}" class="tabla_filas">
						<td><input type="text" id ="cantidad_${cantItem}" disabled="disabled" value = "${cantidad}" class = "form-control form-control-sm numerico" name="datos[${cantItem}][cantidad]"/></td>
						<td><select id ="producto_${cantItem}" disabled="disabled" class="form-control select2 producto" name="datos[${cantItem}][producto_id]" data="${cantItem}" style="width: 100%;">
								<option value="">Seleccione Producto</option>`;
								@foreach($productos as $producto)
								tabla += `<option value="{{$producto->id}}">{{$producto->denominacion}} </option>`;
								@endforeach
							tabla += `</select>
						</td>
						<td><input type="text" id ="costo_${cantItem}" disabled="disabled" value = "${costo}" class = "form-control form-control-sm numerico" name="datos[${cantItem}][costo]"/></td>
						<td><input type="text" id ="iva_${cantItem}" disabled="disabled"  value = "${iva}" class = "form-control form-control-sm numerico" name="datos[${cantItem}][iva]"/></td>
						<td>
						<input type="text" id ="venta_${cantItem}" disabled="disabled" value = "${venta}" class = "form-control form-control-sm numerico valor" name="datos[${cantItem}][venta]"/></td>
						<input type="hidden" class = "form-control form-control-sm" value ="${id_iva}" name="datos[${cantItem}][tipo_impuesto]" id="tipo_impuesto_${cantItem}"/>
						<input type="hidden" class = "form-control form-control-sm" value ="${valor_iva}" name="datos[${cantItem}][valor_impuesto]" id="valor_impuesto_${cantItem}"/>

						<td>
							<a class="glyphicon glyphicon-save-file" onclick="modificarDetalle(${cantItem})" title=Modificar Detalle" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>	
							<a class="glyphicon glyphicon-save-file" onclick="eliminarDetalle(${cantItem})" title="Eliminar Detalle"  style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>
							<input type="hidden" id ="productoId_${cantItem}" value = "${producto_id}"  class = "form-control form-control-sm numerico" name="datos[${cantItem}][producto]"/>
						</td>
					</tr>
					`;
					$('#lista_ventas tbody').append(tabla);
					$('#producto_'+cantItem).select2();
					$('#producto_'+cantItem).val(producto_id).select2();
					$('#producto_'+cantItem).prop("disabled", true);
					clear($(this).attr('data'));
					sumar(iva, venta);
				});*/
			});


		function modificarDetalle(linea){
			console.log(linea);
			$('#cantidad_'+linea).prop('disabled', false);
			$('#producto_'+linea).prop('disabled', false);
			$('#costo_'+linea).prop('disabled', false);
			$('#iva_'+linea).prop('disabled', false);
			$('#venta_'+linea).prop('disabled', false);
			$('#save_'+linea).css('display', 'block');
			$('#cancelar_'+linea).css('display', 'block');
			$('#eliminar_'+linea).css('display', 'none'); 
			$('#modificar_'+linea).css('display', 'none');
			$('table#lista_ventas tr#lineaVenta_'+linea).find('input,select,select2,textarea').prop("disabled", false);
		}	

		function cancelarDetalle(linea){ 
			$('#save_'+linea).css('display', 'none');
			$('#cancelar_'+linea).css('display', 'none');
			$('#eliminar_'+linea).css('display', 'block'); 
			$('#modificar_'+linea).css('display', 'block');
			$('table#lista_ventas tr#lineaVenta_'+linea).find('input,select,select2,textarea').prop("disabled", true);
		}
			
		function guardarDetalle(linea){
			$('#save_'+linea).css('display', 'none');
			$('#cancelar_'+linea).css('display', 'none');
			$('#eliminar_'+linea).css('display', 'block'); 
			$('#modificar_'+linea).css('display', 'block');
			$('table#lista_ventas tr#lineaVenta_'+linea).find('input,select,select2,textarea').prop("disabled", true);
		}	

		function eliminarDetalle(linea){
			$("#lineaVenta_"+linea).remove();
		}	

		function modalAnularVentaRapida(idVenta)
		{

			return swal({
				title: "Anular Venta",
				text: "¿Está seguro que desea anular la venta?",
				showCancelButton: true,
				buttons: {
					cancel: {
						text: "No, cancelar",
						value: null,
						visible: true,
						className: "btn-warning",
						closeModal: false,
					},
					confirm: {
						text: "Sí, anular",
						value: true,
						visible: true,
						className: "",
						closeModal: false
					}
				}
			}).then(isConfirm => {


				if (isConfirm) {
					$.when(anularVentaAjax(idVenta)).then((a)=>{ 
						swal("Éxito",a , "success");
						location. reload();
					},(b)=>{
						swal("Cancelado", b, "error");
					});
					

				} else {
					swal("Cancelado", "", "error");

				}
			});
		}

		function modalAnular(idFactura) {

			return swal({
				title: "Anular Factura",
				text: "¿Está seguro que desea anular esta factura?",
				showCancelButton: true,
				buttons: {
					cancel: {
						text: "No, cancelar",
						value: null,
						visible: true,
						className: "btn-warning",
						closeModal: false,
					},
					confirm: {
						text: "Sí, anular",
						value: true,
						visible: true,
						className: "",
						closeModal: false
					}
				}
			}).then(isConfirm => {


				if (isConfirm) {
					$.when(anularFacturaAjax(idFactura)).then((a)=>{ 
						swal("Éxito",a , "success");
						location. reload();
					},(b)=>{
						swal("Cancelado", b, "error");
					});
					

				} else {
					swal("Cancelado", "", "error");

				}
			});
			}

		// $('.modalAnular').on('click',function(){
		// 	modalAnular();
		// });

		$('.modalAnularVentaRapida').on('click',function(){
			modalAnularVentaRapida();
		});

		
		function  anularFacturaAjax(idFactura){
			var id = $('.modalAnular').attr('data-btn');
			$.ajax({
					type: "GET",
					url: "{{route('anularFacturaVentaRapida')}}",
					dataType: 'json',
					data: {
							id:idFactura
					},
					error: function(jqXHR,textStatus,errorThrown){

										reject('Ocurrió un error en la comunicación con el servidor.');

										},

									success: function(rsp){
										console.log(rsp);
									if(rsp.err != 'OK'){
						            		swal("Cancelado", rsp.response[0].anular_factura_ventas_rapidas, "error");							
										} else {
											swal("Exito", "La factura fue anulada.", "success");
											$('.btn-danger').attr('disabled',true);
											location. reload();

									}

						}//cierreFunc	
					});//Cierre ajax
		}//func

		function  anularVentaAjax(idVenta){
			return new Promise((resolve, reject) => { 

					var id = $('.modalAnularVentaRapida').attr('data-btn');
								
								$.ajax({
									type: "GET",
									url: "{{route('anularVentaRapida')}}",
									dataType: 'json',
									data: {
										id:idVenta
									},

									error: function(jqXHR,textStatus,errorThrown){

										reject('Ocurrió un error en la comunicación con el servidor.');

										},

									success: function(rsp){
										console.log(rsp);
									if(rsp.err != 'OK'){
											reject(['Ocurrió un error la venta no fue anulada.',rsp.response[0].anular_factura]);
									} else {
										resolve('La venta fue anulada.')
										$('.btn-danger').attr('disabled',true);
										location. reload();
									}

						}//cierreFunc	
					});//Cierre ajax
			});
		}//func
			

		function sumar(iva, venta){
			totalDeuda = 0;
			$(".valor").each(function(){
				total = $(this).val().replace('.', '');
				totalDeuda+=parseInt(total) || 0;
			});
			$('#total_factura').val(totalDeuda);
			   
			if(iva == 0){
				valor = $("#total_exenta").val();	
				suma_exenta = valor + venta;
				$("#total_exenta").val(suma_exenta);
			}
			if(iva == 5){
				valor_gravada = $("#total_gravadas").val();
				suma_gravada = valor_gravada + venta;
				$("#total_gravadas").val(suma_gravada);
				suma5 = venta * 0.21;
				sumeTotal5 = parseFloat(suma5) + parseFloat($("#total_5").val());
				$("#total_5").val(sumeTotal5);
			}
			if(iva == 10){
				suma_10 = venta / 11;
				valor = $("#total_10").val();	
				sumeTotal10 = parseFloat(valor) + suma_10;
				$("#total_10").val(sumeTotal10);
				valor_gravada = $("#total_gravadas").val();
				suma_gravada = valor_gravada + venta;
				$("#total_gravadas").val(suma_gravada);
			}
		}

		function datProveedor(idCliente){
				$.ajax({
					type: "GET",
					url: "{{route('getCliente')}}",
					dataType: 'json',
					data: {
							documento : idCliente
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							console.log(rsp);
							if(rsp.status == 'OK'){
								$("#proCelular").val(rsp.celular);
								$("#proDireccion").val(rsp.direccion);
								$("#proEmail").val(rsp.email);
								$("#proTelefono").val(rsp.telefono);
								$("#proveedoName").html(rsp.nombre);
								$("#requestModalProveedor").modal('show');
							} else {
								 $.toast({
						                heading: 'Error',
						                position: 'top-right',
						                text: 'Ocurrio un error en la comunicación con el servidor.',
						                showHideTransition: 'fade',
						                icon: 'error'
						            });
							}
						}	
				})	
		}


		$('.numerico').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

		$( "#guardarBtn" ).click(function() {
			$('table#lista_ventas tr').find('input,select,select2,textarea').prop("disabled", false);
			$("#frmVenta").validate({
				rules: {
				                nro_venta : {
				                required : true
				                }				},
				messages: {
				                nro_venta: {
				                    required: "Ingrese Nro de venta"
				                },
				              
					}
				});

			$( "#frmVenta" ).submit();
		});

		function descargarNota(){
			$ruta2 = "{{route('nota_pedido_pdf', ['id' =>$ventas[0]->id])}}";
			window.location.replace($ruta2);
		}


		function notaVentas(idVenta){
			return swal({
                        title: "Generar Nota de Venta",
                        text: " ¿Esta seguro que desea generar la nota de venta? Una vez generada ya no se podrá editar la venta.",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, Generar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
									 $.ajax({
											type: "GET",
											url: "{{route('getImprimirNota')}}",
											dataType: 'json',
											data: {
													idVenta : idVenta,
												},
												error: function(){
													$('.load_err').hide();
													$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
													},
												success: function(rsp){
													console.log(rsp);
													if(rsp.status == 'OK'){
														$ruta2 = "{{route('nota_pedido_pdf', ['id' =>$ventas[0]->id])}}";
														$("#notaVenta").css('display', 'none');
														$("#footerModal").html(`<button type="button" id="notaVenta" class="btn btn-danger" onclick="notaFacturas('{{$ventas[0]->id}}')" style="width: 120px; background-color: #e2076a;" >FACTURAR</button>`);
														window.location.replace($ruta2);
														swal("Exito", "", "success");
														location. reload();
													} else {
														$.toast({
																heading: 'Error',
																position: 'top-right',
																text: rsp.mensaje,
																showHideTransition: 'fade',
																icon: 'error'
															});
															swal("Cancelado", "", "error");
													}
												}	
										})	                              
									} else {
                                     swal("Cancelado", "", "error");
                                }
            			});
		}

	function notaFacturas(idVenta){
		return swal({
                        title: "Generar Factura",
                        text: "¿Esta seguro que desea generar la factura ?",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, Generar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
									 $.ajax({
											type: "GET",
											url: "{{route('getImprimirFactura')}}",
											dataType: 'json',
											data: {
													idVenta : idVenta,
												},
												error: function(){
													$('.load_err').hide();
													$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
													},
												success: function(rsp){
													console.log(rsp);
													if(rsp.status == 'OK'){ 
														swal("Exito", "", "success");
														$("#nrofactura").val(rsp.numeroFactura);
														$ruta2 = `../imp_fact_pedido/`+rsp.factura;
														window.location.replace($ruta2);
														$("#notaFactura").css('display', 'none');
														$("#footerModal").html(`<a href="../imp_fact_pedido/`+rsp.factura+`"  class="btn btn-danger" role="button">FACTURA</a>`);
														location. reload();
													} else {
														$.toast({
																heading: 'Error',
																position: 'top-right',
																text: rsp.mensaje,
																showHideTransition: 'fade',
																icon: 'error'
															});
														swal("Cancelado", "", "error");
													}
												}	
										})	                              
									} else {
                                     swal("Cancelado", "", "error");
                                }
            			});

					}
			$("#botonCliente").click(function() {
				idCliente = '{{$ventas[0]->id_cliente}}';
				$.ajax({
					type: "GET",
					url: "{{route('getCliente')}}",
					dataType: 'json',
					data: {
							documento : idCliente
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							console.log(rsp);
							if(rsp.status == 'OK'){
								$("#celular").val(rsp.celular);
								$("#direccion").val(rsp.direccion);
								$("#email").val(rsp.email);
								$("#telefono").val(rsp.telefono);
								$("#clienteName").html(rsp.nombre);
								$("#requestModalCliente").modal('show');

							} else {
								 $.toast({
						                heading: 'Error',
						                position: 'top-right',
						                text: 'Error no existe el cliente solicitado.',
						                showHideTransition: 'fade',
						                icon: 'error'
						            });
							}
						}	
				})	
			});

			$("#btnGuardarZona").click(function() {
			 	idVenta = $("#btnGuardarZona").attr('data');
				$.ajax({
					type: "GET",
					url: "{{route('getAsignarZona')}}",
					dataType: 'json',
					data: {
							documento : idVenta,
							idZona: $("#zonasSelect").val()
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrió un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							console.log(rsp);
							if(rsp.status == 'OK'){
								$.toast({
										heading: 'Exito',
										text: 'Se ha asignado la Zona a la Venta.',
										position: 'top-right',
										showHideTransition: 'slide',
								    	icon: 'success'
									}); 
								$("#asignarme").css('display', 'none');
							} else {
								 $.toast({
						                heading: 'Error',
						                position: 'top-right',
						                text: 'Ocurrio un error en la comunicación con el servidor.',
						                showHideTransition: 'fade',
						                icon: 'error'
						            });
							}
						}	
				})	

			})

			$("#asignarme").click(function() {
			 	idVenta = $("#asignarme").attr('data');
				$.ajax({
					type: "GET",
					url: "{{route('getAsignarVenta')}}",
					dataType: 'json',
					data: {
							documento : idVenta
						},
						error: function(){
							$('.load_err').hide();
							$('.msj_err').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							console.log(rsp);
							if(rsp.status == 'OK'){
								$.toast({
										heading: 'Exito',
										text: 'Se te a asignado esta venta.',
										position: 'top-right',
										showHideTransition: 'slide',
								    	icon: 'success'
									}); 
								$("#asignarme").css('display', 'none');
							} else {
								 $.toast({
						                heading: 'Error',
						                position: 'top-right',
						                text: 'Ocurrio un error en la comunicación con el servidor.',
						                showHideTransition: 'fade',
						                icon: 'error'
						            });
							}
						}	
				})	

			})

	</script>
@endsection
