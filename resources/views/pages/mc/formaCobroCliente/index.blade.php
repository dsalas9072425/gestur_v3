@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
  @parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
  @include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Forma de Cobro Cliente</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-body pt-0">
          <div class="alert  alert-light" role="alert">
           <b>Abreviaturas:</b> 
            <ol>
              <li>EFECT, CHEQ, CHEQ_DIF utilizan las cuentas parametrizadas en <a href="{{route('cuentaEmpresa')}}">Administración > Cuentas Contables</a> </li>
              <li>FACT, RETEN, TD, TC VO utilizan cuentas contables y cuentas de fondos configuradas aquí.</li>
              <li>DEP Y TRANSF utilizan la cuenta contable de la cuenta de fondo seleccionada al momento del cobro</li>
            </ol>
          </div>
          <form id="formPlanCuenta">
            <div class="row">
              <div class="col-12">
                <a href="{{route('addFormaCobro')}}" title="Agregar Persona" class="btn btn-success pull-right  btn-lg"
                role="button">
                <div class="fonticon-wrap">
                  <i class="ft-plus-circle"></i>
                </div>
              </a>
              <button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style=" margin: 0px;"><b>Excel</b></button>
              </div>
    
            </div>
          </form>	 
				</div>	
       


        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
                <table id="listado" class="table">
                  <thead>
                    <tr>
                      <th style="width: 20%">Denominacion</th>
                      <th>Depositable</th>
                      <th>Abreviatura</th>
                      <th>Visible</th>
                      <th>Tipo</th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
            </div> 
        </div> 
    </div>        
</section>


@endsection
@section('scripts')
  @include('layouts/gestion/scripts')
  <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

  <script>
    $('#listado').dataTable();

    var datepickers = $('#inici'); 
          if (datepickers.length > 0) { 
          datepickers.datepicker({ 
           format: "dd/mm/yyyy", 
          // startDate: new Date() 
          }); 
          } 

    var datepickers = $('#vencimient'); 
          if (datepickers.length > 0) { 
          datepickers.datepicker({ 
           format: "dd/mm/yyyy", 
          // startDate: new Date() 
          }); 
          } 
    $('#inicio').click(function(){
      $('input[name="inicio"]').trigger('click'); 
    })  
    $('#vencimiento').click(function(){
      $('input[name="vencimiento"]').trigger('click');  
    })  

    $('.select2').select2();


     /*   var dataString = $("#frmBusqueda"+$(this).attr('data')).serialize();
        dataString += '&type='+encabezado;*/
        $.ajax({
          type: "GET",
          url: "{{route('getFormaCobro')}}",
          dataType: 'json',
         // data: dataString,
          success: function(rsp){
            var oSettings = $("#listado").dataTable().fnSettings();
            var iTotalRecords = oSettings.fnRecordsTotal();
            let iconoAsignr = '';
            let iconoEditar = '';

            for (i=0;i<=iTotalRecords;i++) {
              $("#listado").dataTable().fnDeleteRow(0,null,true);
            }
            $.each(rsp, function (key, item){
                  iconoAsignr = '';
                  iconoEditar ='<a href="editarFormaCobro/'+item.id+'" class="btn btn-success" title="Editar" style="padding-left: 6px;padding-right: 6px;margin-right: 2px;" role="button"><i class="fa fa-fw fa-edit"></i></a>';
                  if(item.select_cuenta){
                    iconoAsignr ='<a href="asignarCuentaContable/'+item.id+'" class="btn btn-info" title="Asignar Cuentas Contables" style="padding-left: 6px;padding-right: 6px;margin-right: 2px;" role="button"><i class="fa fa-fw fa-tasks"></i></a>';
                  }
                   

                  if(item.depositable == true){
                    depositable = 'SI';
                  }else{
                    depositable = 'NO';
                  }
                  if(item.visible == true){
                    visible = 'SI';
                  }else{
                    visible = 'NO';
                  }
                  if(item.abreviatura !== null){
                    abreviatura = item.abreviatura;
                  }else{
                    abreviatura = '';
                  }
                  if(item.cod_txt !== null){
                    cod_txt = item.cod_txt;
                  }else{
                    cod_txt = '';
                  }
                  if(item.descripcion !== null){
                    descripcion = item.descripcion;
                  }else{
                    descripcion = '';
                  }

                  if(item.tipo !== null){
                    tipo = item.tipo;
                  }else{
                    tipo = '';
                  }

                var dataTableRow = [
                          '<b>'+item.denominacion+'</b>',
                          depositable,
                          item.abreviatura,
                          visible,
                          tipo,
                          iconoEditar,
                          iconoAsignr
                        ];
                var newrow = $("#listado").dataTable().fnAddData(dataTableRow);

                // set class attribute for the newly added row 
                var nTr = $("#listado").dataTable().fnSettings().aoData[newrow[0]].nTr;

            })
          }
        })    

		$("#botonExcel").on("click", function(e)
		{ 
			e.preventDefault();
			$('#formPlanCuenta').attr('method','post');
			$('#formPlanCuenta').attr('action', "{{route('generarExcelFormaCobro')}}").submit();
		});

      
  </script>
@endsection