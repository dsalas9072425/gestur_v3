@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
	@include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Editar Forma</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
            	<form id="frmProforma" method="post" action="{{route('doEditFormaCobro')}}">
	            	<div class="row"  style="margin-bottom: 1px;">
						
						<input type="hidden" name="id" value="{{$formaCobro->id}}">
						<div class="col-xs-12 col-sm-4 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label class="control-label">Denominaciòn</label>
								<input type="text" required class = "form-control" name="denominacion" id="denominacion" value="{{$formaCobro->denominacion}}">
							</div>
						</div>	
	
						<div class="col-xs-12 col-sm-4 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Visible</label>					            	
								<select class="form-control input-sm select2" name="visible" id="visible" style=" padding-left: 0px;width: 100%;" required>
									<option value="1">SI</option>
									<option value="">NO</option>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Activo</label>					            	
								<select class="form-control input-sm select2" name="activo" id="activo" style=" padding-left: 0px;width: 100%;" required>
									<option value="1">Activo</option>
									<option value="">Inactivo</option>
								</select>
							</div>
						</div>
	
					</div>
				</form>	
				<div class="col-12">
					<button type="submit" form="frmProforma" id="btnGuardar" class="btn btn-success btn-lg pull-right mb-1" tabindex="17"><b>GUARDAR</b></button>
				</div>
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

	<script>
		$("#depositable").val("{{$formaCobro->depositable}}").select2();
		$("#activo").val("{{$formaCobro->activo}}").select2();
		$("#abreviatura").val("{{$formaCobro->abreviatura}}").select2();
		$("#tipo").val("{{$formaCobro->tipo}}").select2();
		
		var datepickers = $('#inici'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		var datepickers = $('#vencimient'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 
		$('#inicio').click(function(){
			$('input[name="inicio"]').trigger('click');	
		}) 	
		$('#vencimiento').click(function(){
			$('input[name="vencimiento"]').trigger('click');	
		}) 	

		$('.select2').select2();
			
	</script>
@endsection