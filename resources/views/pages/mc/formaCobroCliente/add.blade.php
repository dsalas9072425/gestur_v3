@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
	@include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Agregar PAGO CLIENTE</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
				<div class="alert  alert-light" role="alert">
					<b>Abreviaturas:</b> 
					 <ol>
						<li>EFECT: Efectivo - Depositable SI - Tipo VALORES</li>
						<li>CHEQ: Cheque    			- Depositable SI - Tipo VALORES</li>
						<li>CHEQ_DIF: Cheque Diferido   - Depositable SI - Tipo VALORES</li>
						<li>FACT: Factura  				- Depositable NO - Tipo DOCUMENTO</li>
						<li>RETEN: Retención  			- Depositable NO - Tipo DOCUMENTO</li>
						<li>TD: Tarjeta Debito  		- Depositable NO - Tipo VALORES</li>
						<li>TC: Tarjeta Credito  		- Depositable NO - Tipo VALORES</li>
						<li>VO: Voucher  				- Depositable NO - Tipo DOCUMENTO</li>
						<li>DEP: Deposito  				- Depositable NO - Tipo DOCUMENTO</li>
					 </ol>
				   </div>

            	<form id="frmProforma" method="post" action="{{route('doAddFormaCobro')}}">
	            	<div class="row"  style="margin-bottom: 1px;">
						<div class="col-xs-12 col-sm-4 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label class="control-label">Denominaciòn</label>
								<input type="text" required class = "form-control" name="denominacion" id="denominacion" placeholder="Establecimiento"  style="text-transform: uppercase" required>
							</div>
						</div>	
						<div class="col-xs-12 col-sm-4 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label class="control-label">Abreviaciòn</label>
								<select class="form-control input-sm select2" name="abreviatura" id="abreviatura" style=" padding-left: 0px;width: 100%;" required>
									@foreach ($abreviaturas as $key => $abreviatura)
										<option value="{{$key}}">{{$key}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Visible</label>					            	
								<select class="form-control input-sm select2" name="visible" id="visible" style=" padding-left: 0px;width: 100%;" required>
									<option value="1">SI</option>
									<option value="">NO</option>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" style="padding-left: 15px;padding-right: 25px; display:none">
							<div class="form-group">
								<label>Activo</label>					            	
								<select class="form-control input-sm select2" name="activo" id="activo" style=" padding-left: 0px;width: 100%;" required>
									<option value="1">Activo</option>
									<option value="">Inactivo</option>
								</select>
							</div>
						</div>
					</div>
				</form>	
				<div class="col-12">
					<button type="submit" form="frmProforma" id="btnGuardar" class="btn btn-success btn-lg pull-right mb-1" tabindex="17"><b>GUARDAR</b></button>
				</div>
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

	<script>
		var datepickers = $('#inici'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		var datepickers = $('#vencimient'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 
		$('#inicio').click(function(){
			$('input[name="inicio"]').trigger('click');	
		}) 	
		$('#vencimiento').click(function(){
			$('input[name="vencimiento"]').trigger('click');	
		}) 	

		$('.select2').select2();
			
	</script>
@endsection