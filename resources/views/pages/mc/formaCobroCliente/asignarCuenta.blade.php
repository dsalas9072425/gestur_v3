@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
  @parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
  @include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Forma de Cobro: {{$formaCobro->denominacion}}</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <form id="frmAsignar">
            <div class="row" style="margin-bottom: 1px;margin-left: 0px;">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label class="control-label">Cuenta Contable</label>
                  <select class="form-control input-sm select2" name="plan_contable" id="plan_contable" style=" padding-left: 0px;width: 100%;" required>
                      <option value=" ">Seleccione Cuenta Contable</option>
                      @foreach($data_plans as $data_plan)
                        <option value="{{$data_plan->id}}"> {{$data_plan->cod_txt}} -  {{$data_plan->descripcion}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
                <input type="hidden" class = "form-control" name="id" id="id" value="{{$formaCobro->id}}" style="text-transform: uppercase"/>	
              <div class="col-xs-12 col-md-5">
                  <div class="form-group">
                    <label class="control-label">Banco</label>
                    <select class="form-control input-sm select2" name="banco" id="banco" style=" padding-left: 0px;width: 100%;" required>
                        <option value=" ">Seleccione Cuenta Bancaria</option>
                        @foreach($bancos as $banco)
                          <option data-moneda="{{$banco->currency_code}}" value="{{$banco->id}}">{{$banco->numero_cuenta}} - {{$banco->banco_n}} - {{$banco->currency_code}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
            <div class="col-xs-12 col-sm-2 col-md-1">
            <br>
            <a onclick="agregarCtta()" class="btn btn-success" style="padding-left: 10px;padding-right: 10px;color:#fff;margin-top: 5px;" role="button"><i class="fa fa-fw fa-check"></i></a>
            </div>
				  </div>
         </form>

        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
                <table id="listado" class="table">
                  <thead>
                    <tr>
                      <th>Cuenta Contable</th>
                      <th>Moneda</th>
                      <th>Cuenta Banco</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($cttaCtte as $ctta)
                        <tr id="fila{{$ctta->id}}">
                          <th>{{ $ctta->planCuenta->cod_txt}} - {{$ctta->planCuenta->descripcion}}</th>
                          <th>{{ $ctta->moneda ? $ctta->moneda->hb_desc : 'ERROR' }}</th>
                          <th>{{ $ctta->banco ? $ctta->banco->banco_cabecera->nombre .' - '.$ctta->banco->numero_cuenta  : 'ERROR'}} </th>
                          <th><a onclick="eliminarCtta({{$ctta->id}})" class="btn btn-danger" style="padding-left: 6px;padding-right: 6px;color:#fff;" role="button"><i class="fa fa-times"></i></a></th>
                        </tr>
                      @endforeach  
                  </tbody>
                </table>
            </div> 
        </div> 
        <div class="col-12" style="margin-bottom: 20px;">
            <button type="button"  id="btnVolver"  class="btn-mas-request btn btn-info pull-right"><b>Volver</b></button>	
            <br>
        </div>
    </div>        
</section>


@endsection
@section('scripts')
  @include('layouts/gestion/scripts')
  <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

  <script>
   // $('#listado').dataTable();

    var datepickers = $('#inici'); 
          if (datepickers.length > 0) { 
          datepickers.datepicker({ 
           format: "dd/mm/yyyy", 
          // startDate: new Date() 
          }); 
          } 

    var datepickers = $('#vencimient'); 
          if (datepickers.length > 0) { 
          datepickers.datepicker({ 
           format: "dd/mm/yyyy", 
          // startDate: new Date() 
          }); 
          } 
    $('#inicio').click(function(){
      $('input[name="inicio"]').trigger('click'); 
    })  
    $('#vencimiento').click(function(){
      $('input[name="vencimiento"]').trigger('click');  
    })  

    $('.select2').select2();

    $('#btnVolver').click(()=>{
      location.href ="{{ route('indexFormaCobro')}}";
    });

    function eliminarCtta(id){ 
          swal({
                  title: "Eliminar",
                  text: "¿Está seguro que desea eliminar la cuenta contable?",
                  icon: "warning",
                  showCancelButton: true,
                  buttons: {
                      cancel: {
                          text: "No, Cancelar",
                          value: null,
                          visible: true,
                          className: "btn-warning",
                          closeModal: false,
                      },
                      confirm: {
                          text: "Sí, Eliminar",
                          value: true,
                          visible: true,
                          className: "",
                          closeModal: false
                      }
                  }
              }).then(isConfirm => {
	            if (isConfirm) {
                          dataString = 'idCtta='+id;
                          $.ajax({
                              type: "GET",
                              url: "{{route('getEliminarCtaCtte')}}",
                              dataType: 'json',
                              data: dataString,
                              success: function(rsp){
                                if(rsp.status == 'OK'){
                                  $("#fila"+id).remove();
                                  swal("Éxito", rsp.mensaje, "success");
                                }else{
                                  swal("Cancelado", rsp.mensaje, "error");
                                }
                              }    
                            })  
                          } else {
                                swal("Cancelado", "La operación fue cancelada", "error");
                            }
                    })
    }

    function agregarCtta(){ 
        tipoForma = '{{$formaCobro->depositable}}';
        if($('#plan_contable').val().trim() ==""){
          $('#plan_contable').find('.select2-container--default .select2-selection--single').css('border-color','d2d6de');
          $.toast({
                  heading: 'Error',
                  text: 'Seleccione una Cuenta Contable', 
                  position: 'top-right',
                  showHideTransition: 'fade',
                  icon: 'error'
                  });

          return;
        }

        if($('#banco').val().trim() =="" ){
          $.toast({
                    heading: 'Error',
                    text: 'Seleccione una Cuenta Bancaria',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                  });
          return;
        }


        guardar();
           
       
    }


    function guardar(){
    var dataString = $("#frmAsignar").serialize();
              $.ajax({
                  type: "GET",
                  url: "{{route('getAsignarCtaCtte')}}",
                  dataType: 'json',
                  data: dataString,
                  success: function(rsp){
                    if(rsp.status == 'OK'){


                        tabla = "";
                        tabla += `<tr id="fila${rsp.id}">
                                    <th>${ $('#plan_contable :selected').text() }</th>
                                    <th>${ $('#banco :selected').data('moneda') }</th>
                                    <th>${ $('#banco :selected').text() }</th>
                                    <th><a onclick="eliminarCtta(${rsp.id})" class="btn btn-danger" style="padding-left: 6px;padding-right: 6px;color:#fff;" role="button"><i class="fa fa-times"></i></a></th>
                                  </tr>`;
                        $('#listado tbody').append(tabla);


                      $.toast({
                                  heading: 'Exito',
                                  text: rsp.mensaje,
                                  position: 'top-right',
                                  showHideTransition: 'slide',
                                    icon: 'success'
                                });  

                    }else{

                      $.toast({
                              heading: 'Error',
                              text: rsp.mensaje,
                              position: 'top-right',
                              showHideTransition: 'fade',
                              icon: 'error'
                            });

                    } 
                  }    
                })
              }
  </script>
@endsection