<!DOCTYPE html>
<html>
<head>
	<title>Voucher Proforma <?php echo $voucherArray[0]->id_proforma;?></title>

	<style type="text/css">

	* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			width: 750px;
			/*width: 90%;*/
			margin:0 auto;
	}
	
	table{
		width: 100%;
		border-collapse: collapse;
	}

	.center-text{
		text-align: center;
	}

	.m-0{
		margin:0;
	}

	.cabecera{
		background: #f3f5f5;
	}

	.cabeceraDetalle{

	}
	.localizador{
		background: #183B83;
	}
	.text-bold{
		font-weight: bold;
	}
	.text-italic{
		font-style: italic;
	}
	
	.flexbox-container{
		display: -ms-flex;
		display: -webkit-flex;
		display: flex;
	}

	.flexbox-container > div{
		width: 50%;
		padding: 10px;
	}

	.flexbox-container > div:first-child{
		margin-right: 20px;
	}
	.border-bottom-red-td td{
		border-bottom: 1px solid #183B83;
	}
	.border-section{
		border-left: 1px solid #c4cecb;
		border-right: 1px solid #c4cecb;

	}

	.border-comentarios{
	border-left: 1px solid #c4cecb;
	border-right: 1px solid #c4cecb;
	border-bottom: 1px solid #c4cecb;
	}

	.footer{
		border:1px solid #c4cecb;
	}
	.text-cabecera-info{
		font-size: 13px;
	}

	.padding-td td{
		padding: 10px;
	}

	.text-footer{
		font-size: 12px;
	}
		
	</style>
	
	
</head>
<body>



	@php
		$voucherContador = 0;
		$voucherTotal = count($voucherArray);
	@endphp

@foreach($voucherArray as $vou)

	@php
		$voucherContador++;
	    $imagenEmpresa = 'https://gestur.git.com.py/logoEmpresa/'.trim($vou->logoEmpresa);
		$imagenAgencia = 'https://gestur.git.com.py/personasLogo/'.trim($vou->logoAgencia);
		$imagenIcono = 'https://gestur.git.com.py/iconosVoucher/'.trim($vou->icono);

	@endphp

	{{-- ============================================
				INICIO DE VOUCHER 
		=============================================--}}


		
	<div class="container">
			<table>
			<tr>

				<td style="width: 30%;">
					<img src="{{asset($imagenAgencia)}}" alt="{{asset($imagenAgencia)}}" >
				</td>
				<td style="width: 70%; height: 140px;"></td>
			</tr>
		</table>


		<table style="margin-bottom: 10px;">
			<tr>
				<td class="center-text cabecera" style="height: 50px;">
					<p class="m-0" style="font-size:18px; ">
						<?php echo $vou->titulo; ?>
					</p>
			<p class="m-0" style="font-size: 11px;">
			<b>Reserva Confirmada y Garantizada <?php echo $vou->subTituloUno; ?></b> /<i> Booking confirmed and guaranteed <?php echo $vou->subTituloDos; ?></i></p>
				</td>
			</tr>
		</table>

		<table style="background-color:#e5f7f4; ">

			<tr>

				<td class="localizador" style="width: 30%; height: 150px; text-align: center; color:white;">
					
					@if($vou->icono != null && $vou->icono != '')
					<img style="width:30px; height:30px;" src="{{asset($imagenIcono)}}" alt="{{asset($imagenIcono)}}">
				   @endif

					<p style="font-size: 12px;margin-top: 5px;"><b>Localizador</b> / Reference number :</p>
					<p style="font-size: 25px; margin-top: 10px;"><b>{{$vou->codigoConfirmacion}}</b></p>


				</td>


				<td  style="width: 70%; padding: 5px;" valign="top">
				
				<div style="font-weight: bold; font-size: 20px; margin:10px 0 0 0; display:inline-block;"></div>

						<br>
						<br>
				
					<table>
					<tr>
						<td>	
						<span style="" class="text-bold text-cabecera-info">Nombre de pasajero</span> / <span class="text-cabecera-info" style="font-style: italic; font-weight: 400;">Passenger name : </span>
						</td>

						<td style="text-align: left;">
						<span class="text-cabecera-info" style="font-size: 11px;"> <?php echo $vou->nombre; ?></span>
						</td>
							
					</tr>
					
				</table>
				<div style="margin-top: 8px;"></div>
				
					<span class="text-bold text-cabecera-info">Fecha confirmación reserva</span> 
					<span class="text-cabecera-info text-italic">/ Booking date: </span>
					<span class="text-cabecera-info"><?php echo $vou->fechaConfirmacionReserva; ?></span><br>
					<br>
					<span class="text-bold text-cabecera-info">Proforma TO</span>  
					<span class="text-cabecera-info text-italic">/ Proforma TO :</span> 
					<span class="text-cabecera-info"> <?php echo $vou->id_proforma;?></span>

					
				</td>
			</tr>
		</table>

		<table>
				<tr class="border-bottom-red-td">
				<td style="width: 100%; padding-top: 10px;">
					<span class="text-bold">Desde</span>
					<span class="text-italic"> / From :</span>&nbsp; <?php echo $vou->desde; ?> &nbsp;
					<span class="text-bold">Hasta</span>
					<span class="text-italic"> / To :</span>&nbsp; <?php echo $vou->hasta; ?>
				</td>
			</tr> 


		</table>
	

		<table class="border-section">

		

			<tr class="padding-td border-bottom-red-td" style="font-size: 12px; width: 100%">
				<td>
					<span class="text-bold">Unidades</span><br>
					<span class="text-italic">Units</span>
				</td>
				<td>
					<span class="text-bold">Tipo de Servicio</span><br>
					<span class="text-italic">Service Type</span>
				</td>
				<td>
					<span class="text-bold">Adultos</span><br>
					<span class="text-italic">Adults</span>
				</td>
				<td>
					<span class="text-bold">Niños</span><br>
					<span class="text-italic">Children</span>
				</td>
			<!--MOSTRAR NOCHES SEGUN PRODUCTO -->
			@if($vou->grupo_producto == 2)	
				<td>
					<span class="text-bold">Noches</span><br>
					<span class="text-italic">Nights</span>
				</td>
			@endif	

				</tr> 



			
			
			
			 <?php 
				if(isset($vou->servicio)){
				 	echo $vou->servicio; 
				}else{
					echo "";
				}
			 ?>
		
			
				

		</table>

		<table class="border-comentarios padding-td" >
			<tr>
				<td>
					<b>Obervaciones</b>
					<i>/ Remarks</i>
				</td>
			</tr>
			<tr>
				<td style="font-size: 13px;height: 200px;" valign="top">
					{{-- COMENTARIOS --}}
					<?php echo $vou->comentario; ?>
				
				</td>
			</tr>
		</table>
		@if($vou->producto_id == 16 ||$vou->producto_id == 5)
			<table class="footer padding-td" style="margin-top: 10px;">
				<tr>
					<td>
						<span class="text-footer"> <em>Le informamos que en algunos países existe un impuesto local conocido como "tasa de estancia", "tasa municipal", "tasa turística" "resort fee" (o denominación similar) que no está incluido en el precio de la reserva y deberá ser abonados directamente por cada huésped en el establecimiento y/o aeropuerto correspondiente. Tenga presente que estos impuestos pueden cambiar en el tiempo, por lo tanto es necesario que compruebe si el país de destino cobra este tipo de impuestos y la cantidad actual a pagar.</em></span>
						<br>
						<div style="margin-top: 5px;"></div>
					</td>
				</tr>
			</table>
		@endif	
		<!--DATOS PRESTADOR -->
		<table class="footer padding-td" style="margin-top: 10px;">
			<tr>
				<td>
					<div style="font-weight: bold; font-size: 20px; display:inline-block;">{{$vou->nombrePrestador}}</div>
			
					<br>

					<span class="text-footer"><b>{{$vou->direccionPrestador}} - {{$vou->ciudadPrestador}} ,  {{$vou->paisPrestador}} </b></span><br>
					<span class="text-footer"><b>Teléfono</b> / <span class="text-italic">Telephone :</span> {{$vou->telefonoPrestador}}   </span><br>
					<br>
					<div style="margin-top: 5px;"></div>
					

				</td>
			</tr>
		</table>
			

			<!--DATOS PROVEEDOR -->
		<table class="padding-td" style="margin-top: 15px; font-size: 12px;">
			<tr>
				<td style="text-align: center;">
					<!--<p style="font-size: 14px;color: red;">La validez de este bono/voucher de servicio esta sujeta al pago total del servicio de la Agencia de viajes.</p>
					<p style="font-size: 14px; color: red;">En caso de que el mismo esté pendiente antes de la fecha de viaje, este documento queda sin efecto.</p>
					<p style="font-size: 14px;color: red;">La validez de este bono/voucher está sujeta al pago total, por parte de la Agencia de viajes,</p>
					<p style="font-size: 14px;color: red;">antes del inicio del servicio</p>-->
					<br>
					<br>
					<br>
					<span><b>Reservado y pagadero por</b></span> <i>/  Booked and payable by</i> <?php echo $vou->nombreProveedor; ?>
				</td>
			</tr>
		</table>

		<table >
			<tr>
				<td style="width:75%; ">
				
				</td>
				<td>
					<table style="text-align: center;">
						<tr>
							<td>
								<img src="{{ asset($imagenEmpresa) }}" alt="{{ asset($imagenEmpresa) }}" width="50">

							</td>
						</tr>
						<tr>
							<td>
								<div style="font-size: 12px; background-color:#e5f7f4; border-radius: 2px;padding: 5px; ">
									<p>Bono generado el <b> <?php echo $vou->generado; ?></b></p>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
 



	</div>
	<!--container-->

@if($voucherContador < $voucherTotal)
<div style="page-break-after:always;"></div>
@endif
@endforeach


			
	





</body>
</html>