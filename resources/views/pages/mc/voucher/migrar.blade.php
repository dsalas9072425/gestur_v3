
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
	</style>
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Migrar Datos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">
                <form action="{{route('doASubirArchivo')}}" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Datos a Migrar</label>
								<select class="form-control select2" name="id_dato" id="id_dato"
									style="width: 100%;" required>
									<option value="">Seleccione Datos a Migrar</option>
                                    <option value="CLI">Clientes</option>
                                    <option value="PRV">Proveedores</option>
                                    <option value="LC">Libros Compras</option>
                                    <option value="LV">Libros Ventas</option>
								</select>
							</div>
						</div>
                        <div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Archivo</label>
                                <input type="file" name="file" class="form-control" id="exampleInputFile">
                            </div>
                        </div>

						<div class="col-11 mb-1">
                            <button type="submit" name="submit" class="btn btn-info btn-lg pull-right mb-1">Migrar</button>
						</div>
					</div>
				</form>

    		</div>
		</div>
	</section>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
		$("#id_dato").select2();


	</script>
@endsection