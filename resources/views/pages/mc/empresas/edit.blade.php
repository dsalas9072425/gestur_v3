@extends('masters')
@section('title', 'Panel de Control')
@section('styles')


	@parent
@endsection
@section('content')
<style type="text/css">
    <!-- Main content -->
    input.form-control:focus ,.select2-container--focus,button:focus {
    border-color: rgba(82,168,236,.8);
    outline: 0;
    outline: thin dotted \9;
    -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
    box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
}


input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }
</style>

<section id="base-style">
	@include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Editar Empresa</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 


               

					<Form action="{{route('empresa.update', $empresa->id)}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
					{{ method_field('PUT') }}
	            	<div class="row"  style="margin-bottom: 1px;">
						<div class="col-xs-12 col-sm-3 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Denominacion (*)</label>					            	
                                <input type="text" id="denominacion" name="denominacion"  value="{{ $empresa->denominacion }}" required class="form-control text-uppercase">

                            </div>
						</div>	
                        <div class="col-xs-12 col-sm-3 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Ruc (*)</label>					            	
                                <input type="text" id="ruc" name="ruc" value="{{$empresa->ruc}}"  required class="form-control text-uppercase">

                            </div>
						</div>
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Telefono (*)</label>
                                <input type="text" required class = "form-control" name="telefono" id="telefono" value="{{$empresa->telefono}}"  placeholder="" required>
                            </div>
                        </div>

					</div>
                    <div class="row"  style="margin-bottom: 1px;">
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Direccion (*)</label>
                                <input type="text" required class = "form-control" name="direccion" id="direccion" value="{{$empresa->direccion}}" required>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                            
                                <label for="activo">Estado</label>

                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="activo" id="true"  value="true" {{ $empresa->activo ? 'checked' : '' }}>
                                    <label class="form-check-label" for="true">
                                        ACTIVO
                                    </label>
                                </div>
            
                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="activo" id="false" value="false" {{ !$empresa->activo ? 'checked' : '' }}>
                                    <label class="form-check-label" for="false">
                                        INACTIVO
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row"  style="margin-bottom: 1px;">
							
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Markup Minimo Venta</label>
                                <input type="number" required class = "form-control" name="markup_minimo_venta" id="markup_minimo_venta" value="{{$empresa->markup_minimo_venta}}" placeholder="" required>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Venta Minima (incentivo)</label>
                                <input type="number" required class = "form-control" name="venta_minima_incentivo" id="venta_minima_incentivo" value="{{$empresa->venta_minima_incentivo}}" placeholder="" required>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Pagina Web</label>
                                <input type="text" required class = "form-control" name="pagina_web" id="pagina_web" value="{{$empresa->pagina_web}}" placeholder="" required>
                            </div>
                        </div>
					</div>

                    <div class="row"  style="margin-bottom: 1px;">
							
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Email (*)</label>
                                <input type="email" required class = "form-control" name="email" id="email" value="{{$empresa->email}}" placeholder="" required>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                            
                                <label for="activo">Imprimir Detalle Factura?</label>

                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="imprimir_detalle_factura" id="true"  value="true" {{ $empresa->imprimir_detalle_factura ? 'checked' : '' }}>
                                    <label class="form-check-label" for="true">
                                        SI
                                    </label>
                                </div>
            
                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="imprimir_detalle_factura" id="false" value="false" {{ !$empresa->imprimir_detalle_factura ? 'checked' : '' }}>
                                    <label class="form-check-label" for="false">
                                        NO
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label class="control-label">Pie Factura</label>
                                <textarea required class="form-control" name="pie_factura_txt" id="pie_factura_txt" placeholder="Obs. tiene que ser en formato texto" required>{{$empresa->pie_factura_txt}}</textarea>
                            </div>
                        </div>
                        
					</div>

                    <div class="row"  style="margin-bottom: 1px;">
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Ruc Representante</label>
                                <input type="text" required class = "form-control" name="ruc_representante" id="ruc_representante" value="{{$empresa->ruc_representante}}" placeholder="" required>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                            
                                <label for="activo">Es Exportador?</label>

                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="es_exportador" id="true"  value="true" {{ $empresa->es_exportador ? 'checked' : '' }}>
                                    <label class="form-check-label" for="true">
                                        SI
                                    </label>
                                </div>
            
                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="es_exportador" id="false" value="false" {{ !$empresa->es_exportador ? 'checked' : '' }}>
                                    <label class="form-check-label" for="false">
                                        NO
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                            
                                <label for="activo">Es Agente Retentor?</label>

                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="agente_retentor" id="true"  value="true" {{ $empresa->agente_retentor ? 'checked' : '' }}>
                                    <label class="form-check-label" for="true">
                                        SI
                                    </label>
                                </div>
            
                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="agente_retentor" id="false" value="false" {{ !$empresa->agente_retentor ? 'checked' : '' }}>
                                    <label class="form-check-label" for="false">
                                        NO
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row"  style="margin-bottom: 1px;">
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Dias de Vencimiento</label>
                                <input type="number" required class = "form-control" name="dias_vencimiento" id="dias_vencimiento" value="{{$empresa->dias_vencimiento}}" placeholder="Solo numero Enteros"  required pattern="[0-9]*">
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Tipo de Calculo de Vencimiento</label>
                                <select class="form-control input-sm select2" name="tipo_calculo_vencimiento" id="tipo_calculo_vencimiento">
                                    <option value="" selected disabled>Seleccione un Calculo</option>
									<option value="E" @if($empresa->tipo_calculo_vencimiento == 'E') selected @endif>Fecha Emision</option>
									<option value="G" @if($empresa->tipo_calculo_vencimiento == 'G') selected @endif>Gastos</option>
									<option value="C" @if($empresa->tipo_calculo_vencimiento == 'C') selected @endif>CheckIn</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Tipo Empresa</label>
                                <select class="form-control input-sm select2" name="tipo_empresa" id="tipo_empresa">
                                    <option value="" selected disabled>Seleccione el Tipo de Empresa</option>
									<option value="D" @if($empresa->tipo_empresa == 'D') selected @endif>DTP</option>
									<option value="A" @if($empresa->tipo_empresa == 'A') selected @endif>AGENCIAS</option>
									<option value="V" @if($empresa->tipo_empresa == 'V') selected @endif>VENTAS RAPIDAS</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row"  style="margin-bottom: 1px;">
                
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Control Rentabilidad</label>
                                <select class="form-control input-sm select2" name="control_de_rentabilidad" id="control_de_rentabilidad">
                                    <option value="" selected disabled>Seleccionar Rentabilidad</option>
									<option value="V" @if($empresa->control_de_rentabilidad == 'V') selected @endif>VENTAS</option>
									<option value="I" @if($empresa->control_de_rentabilidad == 'I') selected @endif>ITEM</option>
                                </select>
                            </div>
                        </div>
<!--hacer un select2 la funcion esta en listar proveedorticket-->
                   <!--     <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Proveedor Ticket</label>
                                <select class="form-control" name="proveedor_ticket" id="proveedor_ticket">
                                    <option value="" selected disabled>Seleccionar Proveedor</option>
                                    <option value="V">VENTAS</option>
                                    <option value="I">ITEM</option>
                                </select>
                            </div>
                        </div>-->

                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                            
                                <label for="activo">Controlar Linea de Credito?</label>

                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="controlar_linea_credito" id="true"  value="true" {{ $empresa->controlar_linea_credito ? 'checked' : '' }}>
                                    <label class="form-check-label" for="true">
                                        SI
                                    </label>
                                </div>
            
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" required name="controlar_linea_credito" id="false" value="false" {{ !$empresa->controlar_linea_credito ? 'checked' : '' }}>
									<label class="form-check-label" for="false">
                                        NO
                                    </label>
                                </div>
                            </div>
                        </div>
                       
                    </div>

                    <div class="row"  style="margin-bottom: 1px;">
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Fecha limite (linea de credito)</label>
                                <input type="number" pattern="[0-9]+(\.[0-9]+)?" title="Solo se permiten números" required class="form-control" name="fecha_limite_lc" id="fecha_limite_lc" value="{{trim($empresa->fecha_limite_lc)}}" placeholder="" required>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                            
                                <label for="activo">Saldo</label>

                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="saldo_pm" id="true"  value="true" {{ $empresa->saldo_pm ? 'checked' : '' }}>
                                    <label class="form-check-label" for="true">
                                        SI
                                    </label>
                                </div>
            
                                <div class="form-check">
									<input class="form-check-input" type="radio" required name="saldo_pm" id="false" value="false" {{ !$empresa->saldo_pm ? 'checked' : '' }}>
                                    <label class="form-check-label" for="false">
                                        NO
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label class="control-label">Head Factura</label>
                                <textarea required class="form-control" name="head_factura_txt" id="head_factura_txt" placeholder="Obs. tiene que ser en formato texto">{{$empresa->head_factura_txt}}</textarea>
                            </div>
                        </div>
                        
                    </div>

                    <div class="row"  style="margin-bottom: 1px;">
                  
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Planes Sistema</label>
                                <select class="form-control input-sm select2" name="id_plan_sistema" id="id_plan_sistema">
                                    <option value="" selected disabled>Seleccionar Proveedor</option>
									<option value="1" @if($empresa->id_plan_sistema == '1') selected @endif>ESTANDAR</option>
									<option value="2" @if($empresa->id_plan_sistema == '2') selected @endif>---</option>
									<option value="3" @if($empresa->id_plan_sistema == '3') selected @endif>FULL</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Pasjero Factura</label>
                                <input type="text" required class="form-control" name="pasajero_factura" id="pasajero_factura" placeholder="" value="{{ $empresa->pasajero_factura }}">
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                            
                                <label for="activo">Generar LC ticket</label>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" required name="genera_lc_ticket" id="true" value="true" {{ $empresa->genera_lc_ticket ? 'checked' : '' }}>
                                    <label class="form-check-label" for="true">
                                        SI
                                    </label>
                                </div>
            
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" required name="genera_lc_ticket" id="false" value="false" {{ $empresa->genera_lc_ticket ? 'checked' : '' }}>
                                    <label class="form-check-label" for="false">
                                        NO
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label for="activo">Pagar TC detalle Proforma</label>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" required name="pagar_tc_detalle_proforma" id="true" value="true" {{ $empresa->pagar_tc_detalle_proforma == 'true' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="true">
                                        SI
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" required name="pagar_tc_detalle_proforma" id="false" value="false" {{ $empresa->pagar_tc_detalle_proforma == 'false' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="false">
                                        NO
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label class="control-label">Correo SET</label>
                                <input type="email" required class="form-control" name="correo_set" id="correo_set" placeholder="" value="{{ $empresa->correo_set ?? '' }}">
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label class="control-label">Ruc SET</label>
                                <input type="text" required class="form-control" name="ruc_set" id="ruc_set" placeholder="" value="{{ $empresa->ruc_set ?? '' }}">
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label class="control-label">Autorizacion SET</label>
                                <input type="date" required class="form-control" name="autorizacion_set" id="autorizacion_set" placeholder="" value="{{ $empresa->autorizacion_set ?? '' }}">
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label class="control-label">Tipo Plantilla</label>
                                <select class="form-control input-sm select2" name="plantilla" id="plantilla">
                                    <option value="" selected disabled>Seleccionar Tipo Impresion</option>
                                    <option value="facturaEsqueleto" {{ $empresa->plantilla == 'facturaEsqueleto' ? 'selected' : '' }}>Factura pre-impresa</option>
                                    <option value="facturaDE" {{ $empresa->plantilla == 'facturaDE' ? 'selected' : '' }}>Factura Electronica</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
                            <label>Proveedor Ticket</label>					            	
                            <select class="form-control input-sm select2" name="proveedor_ticket" id="proveedor_ticket" style="padding-left: 0px;width: 100%;">
                                <option value="" selected disabled>Seleccione un proveedor</option>
                                @foreach($proveedor as $key=>$prov)
                                    <option {{ old('proveedor_ticket') == $prov->id ? 'selected' : '' }} value="{{$prov->id}}">{{$prov->nombre}}</option>
                                @endforeach	
                            </select>
                        </div>
                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label class="control-label">Tipo Impresion</label>
                                <select class="form-control input-sm select2" name="tipo_impresion" id="tipo_impresion">
                                    <option value="">Seleccionar Tipo Impresion</option>
                                    <option value="1" {{ $empresa->tipo_impresion == 1 ? 'selected' : '' }}>Pagina Completa</option>
                                    <option value="4" {{ $empresa->tipo_impresion == 4 ? 'selected' : '' }}>Pagina Divididas</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label class="control-label">Representante</label>
                                <input type="text" required class="form-control" name="representante" id="representante" placeholder="" value="{{ $empresa->representante }}">
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4">
                            <div class="form-group">
                                <label class="control-label">Logo</label>
                                <input type="file"  class="form-control" name="logo" id="logo" placeholder="">
                            </div>
                        </div>
                    </div>  
                   
			</div>	
	            	
            
                <a href="{{ route('empresa.index') }}" class="btn btn-secondary btn-lg pull-right mb-2" >Cancelar</a>
                <button type="submit"  class="btn btn-success btn-lg pull-right mb-1" tabindex="17"><b>GUARDAR</b></button>
            
           
				</form>	
				
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')
@include('layouts/gestion/scripts')
	<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
  <script>



  {{--==========================================
					VARIABLES GLOBALES
			==========================================--}}
			var ordenamiento = [];
			var table;
			// DEFINE EL IDIOMA ESPAÑOL A TODOS LOS SELECT2
			var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};


		$(document).ready(function() {


				let select_1 = $('.select2').select2({
					language: lang_es_select2
				});
			



    $("#id_persona").select2({
        language: lang_es_select2,
        ajax: {
                url: "{{route('get.personas')}}",
                dataType: 'json',
                placeholder: "TODOS",
                delay: 0,
                data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                                    };
                },
                cache: true
                },
                escapeMarkup: function (markup) {
                                return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
    });
});//READY DOCUMENT
</script>
@endsection