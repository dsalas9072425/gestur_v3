@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
        .pagination .page-item {
            border-radius: 50%;
        }
    </style>
@endsection
@section('content')

<section id="base-style">
<br>

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Listado Empresas</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-md-9">
                    
                        <a href="{{ route('empresa.create') }}" class="btn btn-info">Crear Empresa</a>

                    </div>

                    
                    <div class="col-md-3">
                        <form action="{{ route('empresa.index') }}">
                            <LAbel>Buscar:</LAbel>
                            <input type="search" name="buscar" class="form-control">                            
                        </form>
                    </div>
                </div>


                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>DENOMINACION</th>
                            <th>ESTADO</th>
                            <th>TELEFONO</th>
							<th>LOGO</th>
							<th>DIRECCION</th>
							<th>RUC</th>
							<th>EMAIL</th>
							<th>ACCION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($empresas  as $empresa)

                        <tr>
                            <td>{{ $empresa->id }}</td>
                            <td>{{ $empresa->denominacion }}</td>
                            <td>{{ $empresa->activo ? 'ACTIVO' : 'INACTIVO' }}</td>
                            <td>{{ $empresa->telefono }}</td>
							<td>
								<img src="{{ asset('personas/' . $empresa->logo) }}" alt="" width="50px">
							</td>
							
							<td>{{ $empresa->direccion }}</td>
							<td>{{ $empresa->ruc }}</td>
							<td>{{ $empresa->email }}</td>
                          
                            <td>
                                <a href="{{route('empresa.edit', $empresa->id)}}" class="btn btn-warning">
                                    <span class="fa fa-edit"></span>
                                </a>
                        
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $empresas->links('pagination::bootstrap-4') }}
                TOTAL DATOS: {{ $empresas->total() }}
  

            </div>
        </div>

    </div>
</div>

@endsection