@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style type="text/css">
	
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}
	.card,.card-header {
					border-radius: 14px !important;
					}
</style>

@endsection
@section('content')
			@include('flash::message') 


			<section id="base-style">

				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Cuenta Corriente Clientes</h4>
						<div class="heading-elements">
							<ul class="list-inline mb-0">
								<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="card-content collapse show" aria-expanded="true">
						<div class="card-body">

					<form action="{{route('getCuenta')}}" id="frmBusqueda" method="get">	
							<div class="row">

									<div class="col-xs-12 col-sm-5">
										<div class="form-group">
											<label>Agencia</label>
											<select class="form-control input-sm select2" required name="persona" id="persona" style="width: 100%;">
												<option value="">Seleccione Agencia</option>
												@foreach($personas as $key=>$persona)
													@php
														$ruc = $persona->documento_identidad;
														if($persona->dv){
															$ruc .= $ruc."-".$persona->dv;
														}
													@endphp
													<option value="{{$persona->id}}">{{$ruc}} -
														{{$persona->nombre}} {{$persona->apellido}} @if($persona->denominacion !="")-
														{{$persona->denominacion}}@endif</option>
												@endforeach
											</select>
										</div>
									</div>


									<div class="col-xs-12 col-sm-12">
										<button type="button" id="btnBuscar" class="btn btn-info btn-lg pull-right mr-5">Buscar</button>
									</div>
							</div>
						</form>



						<div class="table-responsive">
							<table id="listado" class="table">
								<thead>
									<tr>
										<th>Estado</th>
										<th>Agencia</th>
										<th>Línea de Crédito</th>
										<th>Saldo</th>
										<th>Deuda</th>
										<th></th>
									</tr>
								</thead>

								<tbody style="text-align: center">
									@if(!isset($estado_cuenta))
									@foreach($estado_cuenta as $key=>$cuenta)
									<tr>
										<td><i class="{{$cuenta['icoSaldo']}}"></i>
											<div style="display: none;">{{$cuenta['pesoIcoSaldo']}}</div>
										</td>
										<td>{{$cuenta['nombre']}}</td>
										<td>{{$cuenta['monto']}}</td>
										<td>{{$cuenta['saldo']}}</td>
										<td>{{$cuenta['saldo_real']}}</td>
										<td>
											<a href="{{ route('indexDetallado', $cuenta['id'])}}" class="btn btn-danger" role="button" id="ver"><i class="fa fa-fw fa-search"></i>
											</a>
										</td>
									</tr>
									@endforeach.
									@endif
								</tbody>
							</table>
						</div>


					</div>
				</div>
				</div>
			</section>
     

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$('.select2').select2();

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
			$( ".fecha" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});
		$("#itemList").select2({
					    ajax: {
					            url: "{{route('destinoGrupo')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});
		});
		$("#btnBuscar").click(function(){
			var dataString = $("#frmBusqueda").serialize();
			$.ajax({
					type: "GET",
					url: "{{route('getCuenta')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){ 
						console.log(rsp);

						var oSettings = $('#listado').dataTable().fnSettings();
						var iTotalRecords = oSettings.fnRecordsTotal();
						for (i=0;i<=iTotalRecords;i++) {
							$('#listado').dataTable().fnDeleteRow(0,null,true);
						}
						$.each(rsp, function (key, item){
							console.log(item);
							var boton = '<a href="indexDetallado/'+item.persona_id+'" class="btn btn-info" role="button" id="ver"><i class="fa fa-fw fa-search"></i>';

							var icoSaldo = '<i class="'+item.icoSaldo+'"></i><div style="display: none;">'+item.pesoIcoSaldo+'</div>';

						

							// var link = '<a href="indexDetallado/'+item.id+'">Ver</a>';

							var dataTableRow = [
							icoSaldo,
							item.nombre,
							item.monto,
							item.saldo,
							item.saldo_real,
							boton
							];
							var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
// set class attribute for the newly added row
var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
})
					}
				})
		});

		
		
	</script>
@endsection