@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style type="text/css">
		th { white-space: nowrap; }
	</style>
@endsection

@section('content')
	<style type="text/css">
		.icoRojo{
			color:red;
		}

		.icoVerde{
			color:green;
		}

		.icoAmarillo{
			color:#C7A10A;
		}

		.card,.card-header {
					border-radius: 14px !important;
					}
	</style>


@include('flash::message') 
<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Movimientos Cta. Cte.</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

				<form action="{{route('getDetallesCuenta')}}" id="frmBusqueda" method="get">

					<div class="row">
						@if(count($lineaCredito) > 0)
						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label class="control-label">Línea de crédito</label>
								<input type="text" class="form-control" name="lc" id="lc"
									value="{{number_format($lineaCredito[0]->monto,2,",",".")}}" readonly="" />
							</div>
						</div>
						@endif



						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Fecha Desde/Hasta</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right" name="periodo" id="periodo" required>
								</div>
							</div>
						</div>







						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Tipo de Documentos</label>
								<select class="form-control input-sm select2" required name="visible" id="visible"
									style="width: 100%;">
									<option value=true>Solo Documentos Contables</option>
									<option value=false>Todos los Movimientos</option>
								</select>
							</div>
						</div>

						<input type="hidden" class="form-control" name="id" id="id" value="{{$id}}" hidden="hidden" />



						<div class="col-xs-12 col-sm-12">
							<button type="button" id="btnBuscar" class="btn btn-info btn-lg pull-right mr-5 mb-1">Buscar</button>
						</div>
					</div>


				</form>
				<div class="table-responsive">
					<table id="listado" class="table">
						<thead>
							<tr>
								<th>Fecha</th>
								<th>Tipo Movimiento</th>
								<th>Documento</th>
								<th>Debe</th>
								<th>Haber</th>
								<th>GS</th>
								<th>Saldo</th>
							</tr>
						</thead>

						<tbody style="text-align: center">
						</tbody>

					</table>
				</div>



			</div>
		</div>
	</div>
</section>
        
      
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$('.select2').select2();

			// $("#listado").dataTable({
			// 	 "aaSorting":[[0,"desc"]]
			// 	});

			$( ".fecha" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
									dateFormat: 'yy-mm-dd',
									orientation: 'bottom'
									});





// var total = 0;
// $('#listado').DataTable().rows().data().each(function(el, index){
//   //Asumiendo que es la columna 5 de cada fila la que quieres agregar a la sumatoria
//   total += el[6];
// });
// console.log(total);

	$(document).ready(function() {
    $('#listado').DataTable( {
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            // total = api
            //     .column( 5 )
            //     .data()
            //     .reduce( function (a, b) {
            //         return parseInt(a) + parseInt(b);
            //     }, 0 );
 
            // // Total over this page
            // pageTotal = api
            //     .column( 5, { page: 'current'} )
            //     .data()
            //     .reduce( function (a, b) {
            //         return parseInt(a) + parseInt(b);
            //     }, 0 );
 
            // // Update footer
            // $( api.column( 5 ).footer() ).html(
            //     '$'+pageTotal +' ( $'+ total +' total)'
            // );
        }
    } );
    $("#btnBuscar").trigger('click');
} );

			$("#itemList").select2({
					    ajax: {
					            url: "{{route('destinoGrupo')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});
		});

		var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$( "#periodo" ).daterangepicker({
		                                  timePicker24Hour: true,
		                                  timePickerIncrement: 30,
		                                   locale: {
		                                       format: 'DD/MM/YYYY '//H:mm'
		                                   },
		                                    startDate: "01/"+fechaInicial,
        									endDate: new Date(),
                                		});

		$("#btnBuscar").click(function(){
			var dataString = $("#frmBusqueda").serialize();
			$.ajax({
					type: "GET",
					url: "{{route('getDetallesCuenta')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){ 
						console.log(rsp);

						var oSettings = $('#listado').dataTable().fnSettings();
						var iTotalRecords = oSettings.fnRecordsTotal();
						for (i=0;i<=iTotalRecords;i++) {
							$('#listado').dataTable().fnDeleteRow(0,null,true);
						}
						$.each(rsp, function (key, item){
							console.log(item);

							var dataTableRow = [
								item.fecha,
								item.denominacion,
								item.documento,
								item.debe,
								item.haber,
								item.importe_gs,
								item.saldo,
							];
							var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
						// set class attribute for the newly added row
						var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
						})
					}
				})
		});


	</script>
@endsection