@extends('masters')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
@endsection
@section('content')
	<div style="margin-left: 1%; margin-right: 2%;" class="col-xs-12 col-sm-12 col-md-11">
        @include('flash::message')
        <br>
        <div id="warning"></div>
        <h1>Reporte de Facturas Pendientes</h1>
        <div class="block sectiontop">
                <br>
                <form action="" id="frmBusqueda" method="post"> 
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-10 col-md-10">
                            <div class="row">
                                <!--<div class="col-xs-12 col-sm-3 col-md-3">
                                    <label>Desde</label>
                                    <div class="input-group date">
								        <div class="input-group-addon">
								            <i class="fa fa-calendar"></i>
								        </div>
								        <input type="text" name="desde" id="desde" class="form-control pull-right">
								    </div> 	
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <label>Hasta</label>
                                    <div class="input-group date">
								        <div class="input-group-addon">
								            <i class="fa fa-calendar"></i>
								        </div>
								        <input type="text" name="hasta" id="hasta" class="form-control pull-right">
								    </div> 	
                                </div>-->
								<div class="col-md-8">
									<label>Vendedor</label>
									<select class="form-control select2" id="usuario" style="width: 100%;" name="vendedor_id">
										<option value="0">Seleccione Todos</option>
										@foreach($listadoUsuarios as $key=>$usuarios)
											<option value="{{$usuarios['value']}}"><b>{{$usuarios['labelUsuario']}}</b> - {{$usuarios['labelApellidoNombre']}}<br/></option> 
										@endforeach
									</select>
                                </div>
								<div class="col-md-4">
									<label>Estado</label>
									<select class="form-control select2" id="estado" style="width: 100%;" name="estado">
										<option value=1>Vencido</option>
										<option value=2>A Vencer</option>
										<option value=3>Todos</option>
									</select>
                                </div>
                            </div>
                    	</div>
                    	<div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                            <div class="col-md-12">
                                <button type="button" style="width: 130px; margin-bottom: 10px; background-color: #e2076a; margin-top: 15px; height: 40px;" id= "doConsultaFactura" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Buscarx Facturas</button><br>
                              <button type="button" style="width: 130px;  background-color: #3c8dbc; height: 40px;" id= "pull-right text-center btn btn-success btn-lg" class="btn btn-primary"><i class="glyphicon glyphicon glyphicon-book"></i>Excel</button>
                            </div>
                            <br>
                        </div>
					</div>
					<hr style="margin-top: 0px;margin-bottom: 0px;">
					<div class="row">
	                     <div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">
	                    	<div class="row">
	                    	    <div class="col-xs-12 col-sm-8 col-md-8" style="padding-right: 0px;">
	                    	   	 	<div class="row">
			                    	    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">
			                    	   		<h3>Vencido Gs:</h3>  	
			                    	   	</div> 	 
			                    	   	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">	 
			                    	      	<div id ="vencidogs"></div>  
			                    	    </div>  	
			                    	    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">
			                    	   		 <h3>Vencido Us:</h3>	
			                    	   	</div> 	 
			                    	   	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">	 
			                    	      	 <div id ="vencidous"></div>
			                    	    </div>  	
	                    	      	</div>
	                    	    </div>	
	                    	    <div class="col-xs-12 col-sm-4 col-md-4" style="padding-right: 0px;">
					              	<span class="info-box-number" id="numero1" style="font-size: 65px;width: 55%; color: red"></span>
	                    	    </div>	
	                    	 </div>	
	                    </div>
	                    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">
	                    	<div class="row">
	                    	    <div class="col-xs-12 col-sm-8 col-md-8" style="padding-right: 0px;">
	                    	   	 	<div class="row">
			                    	    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">
			                    	   		<h4>A Vencer Gs:</h4>  	
			                    	   	</div> 	 
			                    	   	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">	 
			                    	      	<div id ="avencergs"></div>  
			                    	    </div>  	
			                    	    <div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">
			                    	   		 <h4>A Vencer Us:</h4>	
			                    	   	</div> 	 
			                    	   	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-right: 0px;">	 
			                    	      	 <div id ="avencerus"></div>
			                    	    </div>  	
	                    	      	</div>
	                    	    </div>	
	                    	    <div class="col-xs-12 col-sm-4 col-md-4" style="padding-right: 0px;">
					              	<span class="info-box-number" id="numero2" style="font-size: 65px;width: 55%; color: green""></span>
	                    	    </div>	
	                    	 </div>	
	                    </div>
               		</div> 
					<hr style="margin-top: 0px;margin-bottom: 0px;height: 1px;">                    
					<div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12" id="divFacturas" >
							<div class="table-responsive">
								<br>
								<table id="facturas" class="table table-bordered table-hover">
									<thead>
										<tr class="bgblue">
											<th>Emisión</th>
											<th>Factura</th>
											<th>Pasajero</th>
											<th>Vendedor</th>
											<th>Total</th>
											<th>Saldo</th>
											<th>Moneda</th>
											<th>Vencimiento</th>
											<th>Estado</th>
											<th>Descargar</th>
										</tr>
									</thead>
										<tbody>
										</tbody>
								</table>
							</div>
                        </div>
                    </div>
					<div class="row" id="totalReservas">
					</div>
                </form>

        </div>	
	</div>
@endsection

@section('scripts')
 @parent
	<script>
		$(document).ready(function() {
			$("#desde").datepicker();
			$("#hasta").datepicker();
			$('#agencia option[value="{{$agency[0]->idAgencia}}"]').attr("selected", "selected");
			$('#agencia').trigger("chosen:updated");
			
			$('#usuario').select2();
            getButton();

            $("#facturas").dataTable({
				"aaSorting":[[0,"desc"]]
			});
			$("#doConsultaFactura").trigger('click');
			$("#limpiarFiltros").click(function(){
				//$("#desde").val("");
				//$("#desde").datepicker("setDate", new Date());
				
				$("#hasta").val("");
				$("#agencia").val(0);
				$('#agencia').trigger("chosen:updated");
				$("#usuario").val(0);
				$('#usuario').trigger("chosen:updated");
			});
			$("#botonExcel").on("click", function(e){
				e.preventDefault();
    			$('#frmBusqueda').attr('action', "{{route('generarExcelFactura')}}").submit();
			});

		})	

		function getButton(){
			$("#doConsultaFactura").click(function(){
				console.log('Ingreso');
				var dataString = $("#frmBusqueda").serialize();
				$.ajax({
					type: "GET",
					url: "{{route('mc.doConsultaFactura')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
									//$.unblockUI();
									console.log(rsp);
									var oSettings = $('#facturas').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();

									for (i=0;i<=iTotalRecords;i++) {
										$('#facturas').dataTable().fnDeleteRow(0,null,true);
									}

									var totalVencerUs = rsp.cabecera.FacturasSaldoT + rsp.cabecera.totalUsVencer;
									var totalVencerGs = rsp.cabecera.totalGsVencer + rsp.cabecera.totalGsaVencer;
									if(rsp.cabecera.totalGsVencer != 0){
										porcentajeVencidogs =  (rsp.cabecera.totalGsVencer * 100)/totalVencerGs;
									}else{
										porcentajeVencidogs = 0;
									}	
									if(rsp.cabecera.totalUsVencer != 0){
										porcentajeVencidous =  (rsp.cabecera.totalUsVencer * 100)/totalVencerUs;
									}else{
										porcentajeVencidous = 0;
									}	
									if(rsp.cabecera.totalGsaVencer != 0){
										porcentajeAvencergs =  (rsp.cabecera.totalGsaVencer * 100)/totalVencerGs;
									}else{
										porcentajeAvencergs = 0;
									}	
									if(rsp.cabecera.totalUsaVencer != 0){
										porcentajeAvencerus =  (rsp.cabecera.totalUsaVencer * 100)/totalVencerUs;
									}else{
										porcentajeAvencerus = 0;
									}	

									totalVencer = (Math.round(porcentajeVencidogs)+Math.round(porcentajeVencidous))/2;
									totalAvencer = (Math.round(porcentajeAvencergs) +Math.round(porcentajeAvencerus))/2
									$('#numero1').html(Math.round(rsp.cabecera.porcentajeVencido).toFixed(1)+"%");
									$('#numero2').html(Math.round(rsp.cabecera.porcentajeAVencer).toFixed(1)+"%");	

									$('#vencidogs').html('<h3><b>'+formatNumber(rsp.cabecera.totalGsVencer, 2,',','.')+'Gs</b></h3>');
									$('#vencidous').html('<h3><b>'+formatNumber(rsp.cabecera.totalUsVencer, 2,',','.')+'Us</b></h3>');
									$('#avencergs').html('<h3><b>'+formatNumber(rsp.cabecera.totalGsaVencer, 2,',','.')+'Gs</b></h3>');
									$('#avencerus').html('<h3><b>'+formatNumber(rsp.cabecera.totalUsaVencer, 2,',','.')+'Us</b></h3>');

									$('#vencidogsp').html('<h1 style="color: #e2076a;">'+Math.round(porcentajeVencidogs)+'%</h1>');
									$('#vencidousp').html('<h1 style="color: #e2076a;">'+Math.round(porcentajeVencidous)+'%</h1>');
									$('#avencergsp').html('<h1 style="color: #e2076a;">'+Math.round(porcentajeAvencergs)+'%</h1>');
									$('#avencerusp').html('<h1 style="color: #e2076a;">'+Math.round(porcentajeAvencerus)+'%</h1>');

									$.each(rsp.resultado, function (key, item){
										var botonfactura = "<a href='usuariospdf?id="+item.url_factura+"'><i class='fa fa-file-text-o fa-lg' style='   color: blue;'></i>  Factura</a>";
										var botonvoucher = "<a href='usuariospdf?id="+item.url_voucher+"'><i class='fa fa-file-text-o fa-lg' style='   color: red;'></i>  Voucher</a>";										
										var accion = botonfactura+""+botonvoucher;
										var dataTableRow = [
															item.factura_fecha,
															item.factura_numero,
															item.nombre_cliente,
															item.factura_vendedor,
															item.factura_total,
															item.saldo,
															item.factura_moneda,
															item.fecha_vencimiento,
															item.estado,
															accion,
															];
										var newrow = $('#facturas').dataTable().fnAddData(dataTableRow);
										// set class attribute for the newly added row 
										var nTr = $('#facturas').dataTable().fnSettings().aoData[newrow[0]].nTr;
										// and parse the row:
										var nTds = $('td', nTr);
									 });
								},
							});	
					});
		}

			function formatNumber(num) {
			    if (!num || num == 'NaN') return '0';
			    if (num == 'Infinity') return '&#x221e;';
			    num = num.toString().replace(/\$|\,/g, '');
			    if (isNaN(num))
			        num = "0";
			    sign = (num == (num = Math.abs(num)));
			    num = Math.floor(num * 100 + 0.50000000001);
			    cents = num % 100;
			    num = Math.floor(num / 100).toString();
			    if (cents < 10)
			        cents = "0" + cents;
			    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
			        num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
			    return (((sign) ? '' : '-') + num + ',' + cents);
			}
	</script>		
@endsection