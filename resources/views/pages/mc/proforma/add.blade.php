@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	 <style type="text/css">
	 	.error{
	 		color:red;
	 	}
	 </style>
@endsection
@section('content')

	@include('flash::message')  

    <section class="base-style"> 

		<section class="card">
			<div class="card-header">
				<h4 class="card-title">Proformas de Facturas</h4>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-content collapse show" aria-expanded="true">
				<div class="card-body pt-0">

					<form id="frmProforma" method="post" action="{{route('doAddProforme')}}">

						<input type="hidden" class="Requerido form-control" id="control" value="0" style="font-weight: bold;" />

						<input type="hidden" class="Requerido form-control" id="indicador" value="0" style="font-weight: bold;" />
						
						<div class="row">
							<div class="col-12 col-sm-3 col-md-3">
								<div class="row">
									<div class="col-md-11" style="padding-right: 7px;">
										<div class="form-group">
											<label>Cliente (*)</label>
											<!--{{--<select class="form-control select2" name="cliente_id" id="cliente_id" tabindex="1"	style="width: 100%;">
												<option value="">Seleccione Cliente</option>
												@foreach($clientes as $cliente)
													<option value="{{$cliente->id}}">{{$cliente->full_data}} - {{$cliente->id}} </option>
												@endforeach
											</select>--}}-->
											<select lass="form-control select2" name="cliente_id" id="cliente_id" tabindex="1"	style="width: 100%;">												
												<option value="">Seleccione Cliente</option>
											</select>
										</div>										
									</div>
									<div class="col-md-1" style="padding-top: 15px;padding-left: 0px;padding-right: 0px;">
										<div class="form-group">
											<br>
											@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa > 1)
												<a data-toggle="modal" href="#requestModalCliente" class="btn-mas-request"	style="margin-left: 5px;"><i class="fa fa-plus fa-lg"></i></a>	
											@endif	
										</div>	
									</div>
								</div>	
							</div>

							<div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
									<label>Vendedor <label id="id_categoriaLabel" class="error"></label></label>
									<select class="form-control select2" name="id_categoria" id="id_categoria" tabindex="2"
										style="width: 100%;" required>
										<option value="0">Seleccione Vendedor</option>
									</select>
								</div>
							</div>
							<div class="col-12 col-sm-2 col-md-3">
								<div class="form-group">
									<label>Tipo Cliente</label>
									<input type="text" class="Requerido form-control" name="tipo_cliente" id="tipo_cliente"
										readonly="readonly" tabindex="3" />
								</div>
							</div>
							<div class="col-12 col-sm-2 col-md-3">
								<div class="form-group">
									<label>Tipo de Factura</label>
									<input type="text" class="Requerido form-control" name="tipo_factura" id="tipo_factura"
										readonly="readonly" tabindex="4" style="font-weight: bold;" />
								</div>
							</div>
						</div>

						<div class="row">

							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Linea de Crédito</label>
									<input type="text" class="Requerido form-control" name="linea_credito" id="linea_credito"
										readonly="readonly" tabindex="5" style="font-weight: bold;" />
								</div>
							</div>
							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Saldo Disponible</label>
									<input type="text" class="Requerido form-control" name="saldo_disponible"
										id="saldo_disponible" readonly="readonly" style="font-weight: bold;" tabindex="6" />

								</div>
							</div>
							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Proforma Principal</label>
									<select class="form-control select2" name="expediente" id="id_proforma"
										style="width: 100%;" tabindex="7">
										<option value="0">Seleccione Proforma Principal</option>
									
									</select>
								</div>
							</div>
							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Moneda de Facturación (*)</label>
									<select class="form-control input-sm select2" name="moneda" id="moneda"
										style=" padding-left: 0px;width: 100%;" required tabindex="8">
										<option value="">Seleccione Moneda</option>
										@foreach($currencys as $key=>$currency)
										<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>	
							<hr>
							<h3>Datos Proformas</h3>

							<div class="row">
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Destino (*)</label>
										<select data-placeholder="Destinos" id="destino" name="destino" style="width: 100%;" tabindex="9">

										</select>
									</div>
								</div>
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Check In/Out (*)</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span id="checkIn" class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" class="Requerido form-control input-sm" id="periodo" name="periodo" tabindex="10" />
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
									<div class="form-group">
										<label>Vencimiento (*)</label>
										<div class="input-group">
												<div class="input-group-prepend">
													<span id ="vencimiento" class="input-group-text"><i class="fa fa-calendar"></i></span>
												</div>
											<input type="text" class="form-control pull-right fecha" name="vencimiento"
												id="vencimiento" tabindex="11">
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Cotización (*)</label>
										<input type="text" class="Requerido form-control numeric" name="cotizacion"
											id="cotizacion" readonly="readonly" tabindex="14" value="{{$cotizacion}}" />
									</div>
								</div>
							<div class="row">
								<div class="col-12 col-sm-12 col-md-6">
									<div class="row">
										<div class="col-md-11" style="padding-left: 30px;">
												<div class="form-group">
													<label>Pasajeros (*)</label>
													<label id="pasajero_principal-error" class="error" for="pasajero_principal"></label>
													 <select class="form-control select2" name="pasajero_principal[]" id="pasajero_principal" style="width: 100%;">
	               									 </select>
												</div>										
											</div>
											<div class="col-md-1" style="padding-top: 15px;padding-left: 10px;">												<div class="form-group">
													<br>
													<a data-toggle="modal" id="botonPasajero" href="#requestModalPasajero" class="btn-mas-request"	style="margin-left: 5px;"><i class="fa fa-plus fa-lg"></i></a>	
												</div>	
											</div>
									</div>	
								</div>
								<div class="col-12 col-sm-12 col-md-6">
									<label style="margin-right: 20px;">Comentario Proforma</label>
									<!--<a data-toggle="modal" href="#requestModal" class="btn-mas-request"><i class="fa fa-pencil-square-o fa-lg"></i></a>	-->
									<textarea name="comentario" rows="2" cols="200" style="width: 99%;"
										tabindex="13"></textarea>
								</div>

							</div>
							<div class="row">
								<div class="col-12 col-sm-3 col-md-3" style="padding-left: 30px;">
									<div class="form-group">
										<label>Prioridad (*)</label>
										<select class="form-control select2" name="idPrioridad" id="idPrioridad"
											style="width: 100%;">
											<option value="1">NORMAL</option>
											<option value="2">URGENTE</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Incentivo Agencia</label>
										<select class="form-control select2" name="incentivo_agencia" id="incentivo_agencia"
											style="width: 100%;">
											<option value="true">Si</option>
											<option value="false">No</option>
										</select>
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Monto Seña</label>
										<input type="text" onkeypress="return justNumbers(event);"
											class="Requerido form-control numeric" name="senha" id="senha" value="0"
											tabindex="15" />
									</div>
								</div>
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Tipo de Documento</label>
										<select class="form-control select2" name="tipo_venta" id="tipo_venta"
											style="width: 100%;" tabindex="16">
											@foreach($tipoFacturas as $key=>$tipoFactura)
											<option value="{{$tipoFactura->id}}">{{$tipoFactura->denominacion}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<?php 
									$contador = count($negocios);
								?>
								<div class="col-12 col-sm-3 col-md-3" style="padding-left: 30px;">
									<div class="form-group">
										<label>Negocio </label>
										<select class="form-control select2" name="negocio" id="negocio" style="width: 100%;">
		 									@if($contador > 1)
											 	<option value="">Seleccione Negocio</option>
												@foreach($negocios as $key=>$negocio)
													<option value="{{$negocio->id}}">{{$negocio->descripcion}}</option>
												@endforeach
											@else 
												@foreach($negocios as $key=>$negocio)
													<option value="{{$negocio->id}}" selected="selected">{{$negocio->descripcion}}</option>
												@endforeach
											@endif
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-4 col-md-3">
									<div class="form-group">
										<label>Grupos </label>
										<select class="form-control select2" name="grupo_id" id="grupo_id" tabindex="2"
											style="width: 100%;" required>
											<option value="0">Seleccione Grupo</option>
											@foreach($grupos as $key=>$grupo)
											<option value="{{$grupo->id}}"><b>{{$grupo->denominacion}}</b></option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-4 col-md-3">
									<div class="form-group">
										<label>Promoción <label id="id_categoriaLabel" class="error"></label></label>
										<select class="form-control select2" name="id_promocion" id="id_promocion"
											tabindex="2" style="width: 100%;" required>
											<option value="0">Seleccione Promoción</option>
											@foreach($promociones as $key=>$promocion)
											<option value="{{$promocion->id}}"><b>{{$promocion->descripcion}}</b></option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-12 col-sm-4 col-md-3">
									<div class="form-group">
										<label>Tarifa<label class="error"></label></label>
										<select class="form-control select2" name="id_tarifa" id="id_tarifa" tabindex="2"
											style="width: 100%;" required>
											@foreach($tarifas as $key=>$tarifa)
											<option value="{{$tarifa->id}}"><b>{{$tarifa->descripcion}}</b></option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-3 col-md-3" style="padding-left: 30px;">
									<div class="form-group">
										<label>File/Código</label>
										<input type="text" class="Requerido form-control" name="file_codigo" id="file_codigo" tabindex="14"/>
									</div>
								</div>
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Imprimir Precio Unitario</label>
										<select class="form-control select2" name="imprimir_precio_unitario" id="imprimir_precio_unitario" style="width: 100%;">
											<option value="true">Si</option>
											<option selected='selected' value="false">No</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-4 col-md-3">
									<div class="form-group">
										<label>Usuario<label id="id_usuarioLabel" class="error"></label></label>
										<select class="form-control select2" name="id_usuario" id="id_usuario" tabindex="2"
											style="width: 100%;" required>
											<option value="{{$usuario->id}}">{{$usuario->nombre}} {{$usuario->apellido}} ({{$usuario->usuario}})</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-12">
								<button type="button" id="btnGuardar" class="btn btn-success btn-lg pull-right mb-1" tabindex="17"><b>GUARDAR</b></button>
							</div>
					</form>
				</div>
			</div>
		</section>
		</section>
    <!-- /.content -->


    <div id="requestModalPasajero" class="modal fade" role="dialog">
    	<div class="modal-dialog">
    		<!-- Modal content-->
    		<div class="modal-content">
    			<div class="modal-header">
    				<h2 class="modal-title titlepage" style="font-size: x-large;">Ingresar Pasajero</h2>
    			</div>

    			<div class="modal-body">
    				<form id="frmPasajero" method="post" action="">
					<div class="form-group">
    						<label>Tipo Documento</label>
    						<select class="form-control select2" name="tipo_identidad" id="tipo_identidads" style="width: 100%;">
    							@foreach($tipoIdentidads as $key=>$tipoIdentidad)
    								<option value="{{$tipoIdentidad->id}}">{{$tipoIdentidad->denominacion}}</option>
    							@endforeach
    						</select>
    					</div>
						<div class="row"  style="margin-bottom: 1px;">
	    					<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;">
		    					<div class="form-group">
		    						<label>Nro Documento</label>
		    						<input type="text" class="Requerido form-control" name="documento" id="documento" />
		    					</div>
		    				</div>
	    					<div class="col-xs-12 col-sm-6 col-md-2">
		    					<div class="form-group">
		    						<label>DV</label>
		    						<input type="text" class="Requerido form-control" name="dv" id="dv" disabled="disabled" />
		    					</div>
		    				</div>
		    			</div>	
						<div class="form-group">
    						<label>Apellido</label>
    						<input type="text" required class="Requerido form-control controlNombre" name="apellido" id="apellido" />
						</div>
    					<div class="form-group">
    						<label>Nombre</label>
    						<input type="text" required class="Requerido form-control controlNombre" name="nombre" id="nombre" />
						</div>

    					<div class="form-group">
    						<label>Correo Electrónico</label>
    						<input type="text" required class="Requerido form-control" name="email" id="email" />
						</div>
    					
    				</form>
    			</div>
    		
				<div class="modal-footer">
					<button type="button" id="btnAceptarPasajero" class="btn btn-success"><b>Aceptar</b></button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" data-dismiss="modal"><b>Salir</b></button>
				</div>
			</div>
    	</div>
	</div>
	

    <div id="requestModalCliente" class="modal fade" role="dialog">
    	<div class="modal-dialog">
    		<!-- Modal content-->
    		<div class="modal-content">
    			<div class="modal-header">
    				<h2 class="modal-title titlepage" style="font-size: x-large;">Crear Cliente</h2>
    			</div>
    			<div class="modal-body" style="padding-top: 5px;">
    				<small style="color:red">"Solo para nuevos clientes, si el cliente ya existe solo debe seleccionarlo de la lista"</small>
    				<form id="frmCliente" method="post" action="" style="margin-top: 10px;">
					<div class="form-group">
		    				<label>Tipo Documento</label>
		    				<select class="form-control select2" name="tipo_identidad" id="tipo_identidades" style="width: 100%;">
		    					@foreach($tipoIdentidads as $key=>$tipoIdentidad)
		    						<option value="{{$tipoIdentidad->id}}">{{$tipoIdentidad->denominacion}}</option>
		    					@endforeach
		    				</select>
		    			</div>
						<div class="row"  style="margin-bottom: 1px;">
	    					<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;">
		    					<div class="form-group">
		    						<label>Nro Documento</label>
		    						<input type="text" class="Requerido form-control" name="documento_cliente" id="documento_cliente" />
		    					</div>
		    				</div>
	    					<div class="col-xs-12 col-sm-6 col-md-2">
		    					<div class="form-group">
		    						<label>DV</label>
		    						<input type="text" class="Requerido form-control" name="documento_dv" id="documento_dv" disabled="disabled" />
		    					</div>
		    				</div>
		    			</div>	
    					<div class="form-group">
    						<label>Apellido</label>
    						<input type="text" required class="Requerido form-control" name="apellido" id="apellido_cliente" />
						</div>

    					<div class="form-group">
    						<label>Nombre o Razon Social(*)</label>
    						<input type="text" required class="Requerido form-control" name="nombre" id="nombre_cliente" />
						</div>
						
						<div class="form-group">
							<label>Teléfono (*)</label>						 
							<input type="text" class = "Requerido form-control input-sm" name="telefono" id="telefono"/>
						</div>
			 			<div class="form-group">
							<label>Dirección (*)</label>						 
							<input type="text" class = "Requerido form-control input-sm" name="direccion_cliente" id="direccion_cliente"/>
						</div>
						<div class="form-group">
							<label>Pais (*)</label>						 
							<select class="form-control select2" name="pais_cliente" id="pais_cliente" style="width: 100%;">
								@foreach($paises as $key=>$pais)
									<option value="{{$pais->cod_pais}}">{{$pais->name_es}}</option>
								@endforeach
							</select>
						</div>

    					<div class="form-group">
    						<label>Correo Electrónico</label>
    						<input type="text" required class="Requerido form-control" name="email" id="email_cliente" />
						</div>
    				</form>
    			</div>
    		
				<div class="modal-footer">
					<button type="button" id="btnAceptarCliente" class="btn btn-success"><b>Aceptar</b></button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" data-dismiss="modal"><b>Salir</b></button>
				</div>
			</div>
    	</div>
	</div>


    <div id="requestModal" class="modal fade" role="dialog">
    	<div class="modal-dialog">
    		<!-- Modal content-->
    		<div class="modal-content">
    			<div class="modal-header">
    				<h2 class="modal-title titlepage" style="font-size: x-large;">Seleccionar Proforma</h2>
    			</div>
    			<div class="modal-body">
    				<div id="contenido">
    					<h4>Inicio Totla </h4>
    				</div>
    			</div>
    			<div class="modal-footer">
    				<button type="button" id="btnAceptarProforma" class="btn btn-danger"
    					style="width: 90px; background-color: #e2076a;">Aceptar</button>
    				<button type="button" id="btnCancelarVSTour" class="btn btn-danger"
    					style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
    			</div>
    		</div>
    	</div>
    </div>


    <div id="requestModalSeleccion" class="modal fade" role="dialog">
    	<div class="modal-dialog">
    		<!-- Modal content-->
    		<div class="modal-content">
    			<div class="modal-header">
    				<h2 class="subtitle hide-medium"><i class="fa fa-id-card-o"></i> ¿Desea cargar items en la Venta?</h2>
    			</div>
    			<br />
    			<div class="modal-body">
    				<div id="detalle" class="modal-footer">

    				</div>
    			</div>
    		</div>
    	</div>
    </div>




@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	

	<script type="text/javascript" src="{{asset('gestion/app-assets/js/parsley.js')}}"></script>
	<script>

		var idEmpresa = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";

		$(document).ready(function() {
			

			$('#pasajero_principal').select2({
									multiple:true,
									placeholder: 'Pasajero'
								});
			if(idEmpresa == 17){
				$("#id_tarifa").val(2).select2({disabled: true});
				$("#imprimir_precio_unitario").val('true').select2({disabled: true});
				$("#incentivo_agencia").val('false').select2({disabled: true});
				$("#tipo_venta").val(1).select2({disabled: true});
				
			}else{
				$("#id_tarifa").val(1).select2();
			}
			

			$('.numeric').inputmask("numeric", {
				    radixPoint: ",",
				    groupSeparator: ".",
				    digits: 2,
				    autoGroup: true,
				    rightAlign: false,
				    oncleared: function () { self.Value(''); }
			});



			ordenarSelect('pasajero_principal');

			$('.select2').select2();


		    $("#pasajero_principal").select2({
		        ajax: {
		                url: "{{route('getPasajeroPrincipal')}}",
		                dataType: 'json',
		                placeholder: "Seleccione un Beneficiario",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                processResults: function (data, params){
		                            var results = $.map(data, function (value, key) {
		                                        return {
		                                                    id: value.id,
		                                                    text: value.pasajero_data
		                                                };
		                            });
		                                    return {
		                                        results: results,
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });


			
			$("#moneda").val(143).select2();

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

			var datepickers = $('#vencimiento'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

			$('input[name="periodo"]').daterangepicker({
													        timePicker24Hour: true,
													        timePickerIncrement: 30,
													        locale: {
													            format: 'DD/MM/YYYY'//H:mm'
													        },
													       // minDate: new Date()
								    					});




			$("#periodo").val("");

			$("#vencimiento").val("");

			$("#periodo").change(function()
			{	
				$.ajax({
						type: "GET",
						url: "{{route('getVencimiento')}}",
						dataType: 'json',
						data: {
							fechaVencimiento: $(this).val()
				       			},
						success: function(rsp){
							console.log(rsp);
							$('input[name="vencimiento"]').datepicker();
							$('input[name="vencimiento"]').val(rsp.data);
						}	
				});				
			});


			$("#moneda").change(function(){	
				$.ajax({
						type: "GET",
						url: "{{route('getCotizacion')}}",
						dataType: 'json',
						data: {
							dataCurrency: $(this).val()
				       			},
						success: function(rsp){
							$("#cotizacion").val(rsp);
						}	
				});				
			});

	
			$("#destino").select2({
					    ajax: {
					            url: "{{route('destinoProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});
			$('#botonPasajero').click(function(){
				$("#nombre").val('');
				$("#apellido").val('');
				$("#documento").val('');
				$("#email").val('');
			});

			$("#id_proforma").select2({
					    ajax: {
					            url: "{{route('get.getProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione una proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            // processResults: function (data, params){
					            //     var results = $.map(data, function (value, key) {
					            //         return {
					            //             children: $.map(value, function (v) {
					            //                 return {
					            //                     id: key,
					            //                     text: v
					            //                 };
					            //             })
					            //         };
					            //     });
					            //     return {
					            //         results: results,
					            //     };
					            // },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});
			main();


		});

		 $("#frmProforma").validate({  // initialize plugin on the form
			debug: false,
			rules: {
				"cliente_id": {
					required: true
				},
				"moneda": {
					required: true
				},
				"destino": {
					required: true
				},
				"periodo": {
					required: true
				},
				"pasajero_principal": {
					required: true
				},
				"vencimiento": {
					required: true
				},
				"negocio": {
					required: true
				}
			},
			messages: {
				"cliente_id": {
					required: "<br>Seleccione un Cliente"
				},
				"moneda": {
					required: "<br>Seleccione una Moneda"
				},
				"destino": {
					required: "<br>Seleccione un Destino"
				},
				"periodo": {
					required: "<br>Ingrese un Rango de Fechas"
				},
				"pasajero_principal": {
					required: "<br>Seleccione un Pasajero"
				},
				"vencimiento": {
					required: "<br>Ingrese un vencimiento"
				},
				"negocio": {
					required: "<br>Ingrese un Negocio"
				}

			}
		});


		var options = { 
	                beforeSubmit:  showRequest,
					success: showResponse,
					dataType: 'json' 
	        }; 

	 	$('body').delegate('#image','change', function(){
	 		$('#upload').ajaxForm(options).submit();  		
	 	});

		function showRequest(formData, jqForm, options) { 
			$("#validation-errors").hide().empty();
			$("#output").css('display','none');
		    return true; 
		} 
		function showResponse(response, statusText, xhr, $form){ 

			if(response.success == false)
			{
				var arr = response.errors;
				console
				$.each(arr, function(index, value)
				{
					if (value.length != 0)
					{
						$("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
					}
				});
				$("#validation-errors").show();
			} else {
				 $("#imagen").val(response.archivo);	
				 $("#output").html("<img class='img-responsive' src='"+response.file+"'/>");
				 $("#output").css('display','block');
				 $("#btn-delete").html("<button type='button' id='btn-imagen' data-id='"+response.archivo+"'><i class='glyphicon glyphicon-remove'></i></button>");
				 eliminarImagen();
			}
		}

		function  controlGrupo(){
			//SI CLIENTE O GRUPO ES VACIO ENTONCES DESACTIVAR ALERTA
			if($('#cliente_id').val() != '' & $("#grupo_id").val() != 0){

				$.ajax({
				type: "GET",
				url: "{{route('doGrupoCom')}}",
				dataType: 'json',
				data: {
				idGrupo: $("#grupo_id").val(),
				idCliente: $("#cliente_id").val()
				},
				error: function (jqXHR, textStatus, errorThrown) {
				$.toast({
					heading: 'Error',
					position: 'top-right',
					text: 'Ocurrio un error en la comunicación con el servidor.',
					showHideTransition: 'fade',
					icon: 'error'
				});
				$("#control").val(1);
				},
				success: function (rsp) {

					if (rsp.err == false) {

						$.toast({
							heading: 'Error',
							position: 'top-right',
							text: 'El Cliente no esta ligado al grupo',
							showHideTransition: 'fade',
							icon: 'error'
						});

						$("#control").val(1);
					} else {
						$("#control").val(0);
					}

				}
				});

				} else {
				$("#control").val(0);
				}

		}//

		$("#cliente_id").change(function(){	
			$('#id_categoria').select2({
				disabled: false
			});
			$.ajax({
					type: "GET",
					url: "{{route('getDatosCliente')}}",
					dataType: 'json',
					data: {
						dataCliente: $(this).val()
			       			},
					success: function(rsp){
						$("#tipo_cliente").val(rsp.tipo_persona);

						console.log(jQuery.isEmptyObject(rsp.tipo_facturacion));

						if(jQuery.isEmptyObject(rsp.tipo_facturacion) == false){
							$("#tipo_factura").val(rsp.tipo_facturacion);
						}else{
							$("#tipo_factura").val('Factura Neta');
						}
						$("#linea_credito").val(rsp.limite_credito);
						$("#saldo_disponible").val(rsp.saldo_disponible);
						var vendedores = rsp.vendedores;
						if(rsp.id_tipo_persona == 8){
							$('#id_categoria').empty();
								var newOption = new Option('Seleccione Vendedor', 0, false, false);
								$('#id_categoria').append(newOption); 
							$.each(rsp.vendedores, function (key, item){
								var newOption = new Option(item.denominacion, item.id, false, false);
								$('#id_categoria').append(newOption);
							})	
						}else{ 
							$('#id_categoria').select2({
									  disabled: true
							});
						}
					}	
				});	

			controlGrupo();

			});

           function agregarDv(){
                $.ajax({
                        type: "GET",
                        url: "{{ route('agregarDv') }}",
                        dataType: 'json',
                        data: { documento: $('#documento_cliente').val().trim()},

                         error: function(jqXHR,textStatus,errorThrown){
                            $('.mensaje_err_documento').html(' No se pudo validar. ');
                            $('.mensaje_err_documento').css('color','red');
                            inputReq.VerificarDoc(1,false);
                      
                        },
                        success: function(rsp){
                           // console.log(rsp[0]);
						    if(jQuery.isEmptyObject(rsp[0]) == false){
								if(jQuery.isEmptyObject(rsp[0].dv) == true){
									$('#documento_cliente').val(rsp[0].ruc);
									$('#documento_cliente').prop('disabled',true);
									$('#documento_dv').val(rsp[0].dv);
									$('#documento_dv').prop('disabled',true);
									$('#tipo_identidades').val(2).trigger('change.select2');
									$('#tipo_identidades').prop('disabled',true);
								}else{
									$('#documento_dv').prop('disabled',false);
									$('#tipo_identidades').val(1).trigger('change.select2');
								}

								if(jQuery.isEmptyObject(rsp[0].nombre) == false){
									denominacion = rsp[0].nombre;              
									if(denominacion.includes(",") == true){
										base = denominacion.split(',');
										nombre = base[1];
										apellido = base[0];
									}else{
										nombre = denominacion ;
										apellido = '';
									}
									$('#nombre_cliente').val(nombre);
									$('#nombre_cliente').prop('disabled',true);
									$('#apellido_cliente').val(apellido);
									$('#apellido_cliente').prop('disabled',true);
								}
							}else{
                                $('#tipo_identidades').val(1).trigger('change.select2');
                                $('#nombre_cliente').val('');
                                $('#apellido_cliente').val('');
                                $('#documento_dv').val('');
                           }

                        }
                })   
           }    



		$('#checkIn').click(function(){
			$('input[name="periodo"]').trigger('click');	
		}) 
		$('#vencimiento').click(function(){
			$('input[name="vencimiento"]').trigger('click');	
		}) 	

		$("#tipo_tarjeta").change(function(){
			valorInicial = $("#tipo_factura").val();
			if($(this).val() == 'true'){
				$("#tipo_factura").val();
				$("#tipo_factura").val('Factura Bruta');
			}else{
				$("#tipo_factura").val(valorInicial);
			}
		})

		$("#btnGuardar").click(function(){
				idEmpresa = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
				console.log(idEmpresa);
				if(idEmpresa == 1){
					if($("#id_promocion").val() == 0){
						$.toast({
									heading: 'Error',
									position: 'top-right',
									text: 'Seleccione una Promocion',
									showHideTransition: 'fade',
									icon: 'error'
								});
					}else{
						guardar();
					}
				}else{
					guardar();
				}
		})	


		function guardar(){
			if($("#pasajero_principal").val() != ""){
						$("#pasajero_principal-error").html('');
							if($("#control").val() == 0){
							/*	$('#currency').css('border-color', '#d2d6de');*/
								if($("#frmProforma").valid()){ 
									var vendedor = $("#id_categoria").val();
									console.log(jQuery.isEmptyObject(vendedor));
									console.log(vendedor);
									if($("#tipo_cliente").val() == "AGENCIA" && vendedor == 0){
										$('#id_categoriaLabel').css('display', 'block');
										$('#id_categoriaLabel').html("Seleccione un Vendedor");	
									}else{				
										$('#id_categoriaLabel').css('display', 'none');
										$('#id_categoriaLabel').html("")

										//Hanilitar campos y deshabilitar si es BP
										$("#id_tarifa").prop('disabled',false);
										$("#imprimir_precio_unitario").prop('disabled',false);
										$("#incentivo_agencia").prop('disabled',false);
										$("#tipo_venta").prop('disabled',false);

										var dataString = $("#frmProforma").serialize();

										if(idEmpresa == 17){
											$("#id_tarifa").prop('disabled',true);
											$("#imprimir_precio_unitario").prop('disabled',true);
											$("#incentivo_agencia").prop('disabled',true);
											$("#tipo_venta").prop('disabled',true);
										}
										
										$.ajax({
												type: "GET",
												url: "{{route('doGuardarProforma')}}",
												dataType: 'json',
												data: dataString,
												success: function(rsp){
													if(rsp.status == 'OK'){
															return swal({
											                        title: "GESTUR",
											                        text: "¿Desea cargar Items en la Venta?",
											                        showCancelButton: true,
											                        buttons: {
											                                cancel: {
											                                        text: "No",
											                                        value: null,
											                                        visible: true,
											                                        className: "btn-warning",
											                                        closeModal: false											                                },
											                                confirm: {
											                                        text: "Sí, cargar",
											                                        value: true,
											                                        visible: true,
											                                        className: "",
											                                        closeModal: false
											                                    }
											                                }
											                }).then(isConfirm => {
											                        if (isConfirm) {
											                            swal("Éxito", "", "success");
											                       		window.location.replace("{{route('detallesProforma',['id'=>''])}}/"+rsp.id);
											                        } else {
											                            swal("Cancelado", "", "error");
											                            $ruta2 = "{{route('listadosProformas')}}";
											                            window.location.replace($ruta2);
											                        }
											            	});
											            }    
													}
												});
									}
								}	
						
						}else{
							$.toast({
				                   heading: 'Error',
				                   position: 'top-right',
				                   text: 'El cliente no está ligado al grupo, inténtelo nuevamente',
				                   showHideTransition: 'fade',
				                   icon: 'error'
				                  });

						}
				}else{
					$("#pasajero_principal-error").html('Seleccione el pasajero');
				}
		}


			
		$("#grupo_id").change(function () {
			controlGrupo();
		})

		$("#btnAceptarPasajero").click(function(){
			$('#dv').prop('disabled',false);
			var dataString = $("#frmPasajero").serialize();
			if($("#apellido").val() != '' || $("#nombre").val() != ''){
				$.ajax({
						type: "GET",
						url: "{{route('doPasajeroProforma')}}",
						dataType: 'json',
						data: dataString,
						  error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
						success: function(rsp){
										$("#requestModalPasajero").modal('hide');
										console.log(rsp);
										if(rsp.status == 'OK'){
										var newOption = new Option(rsp.nombre, rsp.id, false, false);
										$('#pasajero_principal').append(newOption);
										$("#pasajero_principal").select2().val(rsp.id).trigger("change");
										$('#dv').prop('disabled',true);
										actualizar();
									} else {
										 $.toast({
					                            heading: 'Error',
					                             position: 'top-right',
					                            text: 'Ocurrio un error en la comunicación con el servidor.',
					                            showHideTransition: 'fade',
					                            icon: 'error'
					                        });
											$('#dv').prop('disabled',true);
										}//else



									},
						});	
			}else{
				console.log('No existe')
			}
		});

		$("#documento").change(function(){
			agregarDvs()
		});
		$("#tipo_identidads").change(function(){
			idEmpresa = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
			if(idEmpresa == 21){
				if(this.value == 4){
					$('#dv').val(0);
					$('#documento').val(99999901);
				}
			}
		
			
		});
		$("#tipo_identidades").change(function(){
			if(idEmpresa == 21){
				if(this.value == 4){
					$('#documento_dv').val(0);
					$('#documento_cliente').val(99999901);
				}
			}
		});

		function agregarDvs(){
			documento =$('#documento').val().trim();
				if(documento == 99999901){
					$('#tipo_identidads').val(4).trigger('change.select2');
					$('#dv').val(0);
				}else{
                $.ajax({
                        type: "GET",
                        url: "{{ route('agregarDv') }}",
                        dataType: 'json',
                        data: { documento: documento},

                         error: function(jqXHR,textStatus,errorThrown){
                            $('.mensaje_err_documento').html(' No se pudo validar. ');
                            $('.mensaje_err_documento').css('color','red');
                            inputReq.VerificarDoc(1,false);
                      
                        },
                        success: function(rsp){
                           // console.log(rsp[0]);
						   if(jQuery.isEmptyObject(rsp[0]) == false){
								if(jQuery.isEmptyObject(rsp[0].dv) == true){
									$('#documento').val(rsp[0].ruc);
									$('#dv').val(rsp[0].dv);
									$('#dv').prop('disabled',true);
									$('#tipo_identidads').val(2).trigger('change.select2');
								}else{
									$('#dv').prop('disabled',false);
									$('#tipo_identidads').val(1).trigger('change.select2');
								}

								if(jQuery.isEmptyObject(rsp[0].nombre) == false){
									denominacion = rsp[0].nombre;              
									base = denominacion.split(',');
									$('#nombre').val(base[1]);
									$('#apellido').val(base[0]);
								}
						   }else{
								$('#dv').val('');
								$('#nombre').val('');
								$('#apellido').val('');
								$('#tipo_identidads').val(1).trigger('change.select2');
						   }
                        }
                })  
			} 
           }    

		$("#documento_cliente").change(function(){
			documento=$(this).val();
			if(documento == 99999901){
					$('#tipo_identidads').val(4).trigger('change.select2');
					$('#documento_dv').val(0);
			}else{
			$.ajax({
					type: "GET",
					url: "{{route('doPasajeroCliente')}}",
					dataType: 'json',
					data: {
						dataDocumento: $(this).val()
			       			},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
						if(rsp.status == 'OK'){
							$("#documento_cliente").css('border-color', '#ccd6e6');
							$("#indicador").val(1);
						}else{
	                        $.toast({
	                            heading: 'Error',
	                             position: 'top-right',
	                            text: 'Ya existe este numero de documento verifiquelo',
	                            showHideTransition: 'fade',
	                            icon: 'error'
	                        });
	                        $("#indicador").val(0);
	                        $("#documento_cliente").val($("#documento_cliente").val());
	                        $("#documento_cliente").css('border-color', 'red');
	                        $("#documento_cliente").focus();
						}

					}
			})
			agregarDv();	
			}		
		});		
////////////////////////////////////////////////////////////////
		$("#btnAceptarCliente").click(function(){
			$('#documento_dv').prop('disabled',false);
			$('#documento_cliente').prop('disabled',false);
			$('#tipo_identidades').prop('disabled',false);
			$('#nombre_cliente').prop('disabled',false);
			$('#apellido_cliente').prop('disabled',false);
			var dataString = $("#frmCliente").serialize();
			if(validar() == true){
						$.ajax({
								type: "GET",
								url: "{{route('doClienteProforma')}}",
								dataType: 'json',
								data: dataString,
								  error: function(jqXHR,textStatus,errorThrown){

		                           $.toast({
		                            heading: 'Error',
		                             position: 'top-right',
		                            text: 'Ocurrió un error en la comunicación con el servidor.',
		                            showHideTransition: 'fade',
		                            icon: 'error'
		                        });

		                        },
								success: function(rsp){
											$("#requestModalCliente").modal('hide');
											console.log(rsp);
											if(rsp.status == 'OK'){
											var newOption = new Option(rsp.nombre, rsp.id, false, false);
											$('#cliente_id, #pasajero_principal').append(newOption);
											// Preestablecer la búsqueda por defecto en cliente
											// Establecer el valor del campo de búsqueda de Select2
											$("#cliente_id, #pasajero_principal").select2().val(rsp.id).trigger("change");
											setTimeout(function() {
												recallSelectPasajeroPrincipal();
												recallSelectCliente();
											}, 1000);
											$("#nombre_cliente").val('')
											$("#direccion_cliente").val('')
											$("#email_cliente").val('') 
											$("#apellido_cliente").val('') 
											$("#documento_cliente").val('');
											$('#documento_dv').val('');
											$("#tipo_identidad").val(1).trigger("change");
											$("#pais_cliente").val('').trigger("change");
											$('#documento_dv').prop('disabled',true);
										} else {
											 $.toast({
						                            heading: 'Error',
						                             position: 'top-right',
						                            text: 'Ocurrio un error en la comunicación con el servidor.',
						                            showHideTransition: 'fade',
						                            icon: 'error'
						                        });
												$('#documento_dv').prop('disabled',true);
												$('#documento_cliente').prop('disabled',true);
												$('#tipo_identidades').prop('disabled',true);
												$('#nombre_cliente').prop('disabled',true);
												$('#apellido_cliente').prop('disabled',true);

											}//else
										},
							});	
			}		
		});

		$(".controlNombre").change(function(){
			$.ajax({
					type: "GET",
					url: "{{route('doControlNombre')}}",
					dataType: 'json',
					data: {
							dataNombre:$("#nombre").val(),
							dataApellido: $("#apellido").val()
			       			},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                    },
					success: function(rsp){
									if(rsp == 'ERROR'){
										$.toast({
											heading: 'Error',
											position: 'top-right',
											text: 'Ya existe registrado este pasajero.',
											showHideTransition: 'fade',
											icon: 'error'
										});
										$("#nombre").val('');
										$("#apellido").val('');

									}
					}
			})		

		});


		function validar(){
			validado = true
			error = 0;
			
			//Vamos a poner obligatorio direccion y telefono en todos los casos
			if ($("#direccion_cliente").val().trim() == "") {
				$.toast({
					heading: 'Error',
					position: 'top-right',
					text: 'Ingrese la direccion.',
					showHideTransition: 'fade',
					icon: 'error'
				});
				error = 1;
			}

			if ($("#telefono").val().trim() == "") {
				$.toast({
					heading: 'Error',
					position: 'top-right',
					text: 'Ingrese un número de teléfono.',
					showHideTransition: 'fade',
					icon: 'error'
				});
				error = 1;
			}

			if($("#nombre_cliente").val().trim() ==""){
					$.toast({
					        heading: 'Error',
					        position: 'top-right',
					        text: 'Ingrese el Nombre o la Razon Social.',
					        showHideTransition: 'fade',
					        icon: 'error'
					    });
					error = 1;
				}

			if($("#tipo_identidades").val() == 2){
		
				if($("#documento_cliente").val()==""){
					$.toast({
					        heading: 'Error',
					        position: 'top-right',
					        text: 'Ingrese el Numero de documento.',
					        showHideTransition: 'fade',
					        icon: 'error'
					    });
					error = 1;
				}					
				
				// if($("#email_cliente").val()==""){
				// 	$.toast({
				// 	        heading: 'Error',
				// 	        position: 'top-right',
				// 	        text: 'Ingrese el correo.',
				// 	        showHideTransition: 'fade',
				// 	        icon: 'error'
				// 	    });
				// 	error = 1;
				// }else{
				// 	$.ajax({
                //             type: "GET",
                //             url: "{{ route('validarCorreo') }}",
                //             dataType: 'json',
                //             data: dataString,
                //             error: function(jqXHR,textStatus,errorThrown){
                //                 $('.mensaje_err').html('El correo ya existe');
                //                 $('.mensaje_err').css('color','red');
                //                 inputReq.VerificarCorreo(1,false);
                //                 $('#validacionCorreo').val(1);
                //             },
                //             success: function(rsp){
                //                 console.log(rsp.resp);
                //                 if(rsp.resp != '0'){
				// 					$.toast({
				// 							heading: 'Error',
				// 							position: 'top-right',
				// 							text: 'Ya existe el correo.',
				// 							showHideTransition: 'fade',
				// 							icon: 'error'
				// 						});
				// 					error = 1;
                //                 }
                //             }
                        
                //         });
				// }

				if(error == 1){
					validado = false
				}
				return validado;			
			}else{

				if($("#apellido_cliente").val()=="" && $("#tipo_identidades").val()== 2){  
					$.toast({
					        heading: 'Error',
					        position: 'top-right',
					        text: 'Ingrese el Apellido.',
					        showHideTransition: 'fade',
					        icon: 'error'
					    });
					error = 1;
				}	
				if($("#documento_cliente").val()==""){
					$.toast({
					        heading: 'Error',
					        position: 'top-right',
					        text: 'Ingrese el Numero de documento.',
					        showHideTransition: 'fade',
					        icon: 'error'
					    });
					error = 1;
				}					
	
				// if($("#email_cliente").val()==""){
				// 	$.toast({
				// 	        heading: 'Error',
				// 	        position: 'top-right',
				// 	        text: 'Ingrese el correo.',
				// 	        showHideTransition: 'fade',
				// 	        icon: 'error'
				// 	    });
				// 	error = 1;
				// }else{
				// 	var dataString = {'e' : $('#documento_cliente').val().trim(),
                //               		 'idTipoPersona' : $('#tipo_identidad').val()}
				// 	$.ajax({
                //             type: "GET",
                //             url: "{{ route('validarCorreo') }}",
                //             dataType: 'json',
                //             data: dataString,
                //             error: function(jqXHR,textStatus,errorThrown){
                //                 $('.mensaje_err').html('El correo ya existe');
                //                 $('.mensaje_err').css('color','red');
                //                 inputReq.VerificarCorreo(1,false);
                //                 $('#validacionCorreo').val(1);
                //             },
                //             success: function(rsp){
                //                 console.log(rsp.resp);
                //                 if(rsp.resp != '0'){
				// 					$.toast({
				// 							heading: 'Error',
				// 							position: 'top-right',
				// 							text: 'Ya existe el correo.',
				// 							showHideTransition: 'fade',
				// 							icon: 'error'
				// 						});
				// 					error = 1;
                //                 }
                //             }
                        
                //         });
				// }

				if(error == 1){
					validado = false
				}
				return validado;			
			}


		}
//////////////////////////////////////////////////////////
		function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

		function ordenarSelect(id_componente)
	    {
	      var selectToSort = jQuery('#' + id_componente);
	      var optionActual = selectToSort.val();
	      selectToSort.html(selectToSort.children('option').sort(function (a, b) {
	        return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
	      })).val(optionActual);
	    }

		sumaFecha = function(d, fecha)
		{
		 var Fecha = new Date();
		 var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
		 var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
		 var aFecha = sFecha.split(sep);
		 var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
		 fecha= new Date(fecha);
		 fecha.setDate(fecha.getDate()-parseInt(d));
		 var anno=fecha.getFullYear();
		 var mes= fecha.getMonth()+1;
		 var dia= fecha.getDate();
		 mes = (mes < 10) ? ("0" + mes) : mes;
		 dia = (dia < 10) ? ("0" + dia) : dia;
		 var fechaFinal = dia+sep+mes+sep+anno;
		 return (fechaFinal);
		 }

		 function actualizar(){
			$("#pasajero_principal > option").prop("selected", "selected");
            $("#pasajero_principal").trigger("change");
 		}

		 function main() {

			$('#cliente_id').select2({
										//maximumSelectionLength: 2,
										placeholder: 'Pasajero'
									});

			ordenarSelect('cliente_id');						


			$("#cliente_id").select2({
					ajax: {
							url: "{{route('getClienteListado')}}",
							dataType: 'json',
							placeholder: "TODOS",
							delay: 0,
							data: function (params) {
										return {
											q: params.term, // search term
											page: params.page
												};
							},
							processResults: function (data, params){
										var results = $.map(data, function (value, key) {
										console.log(value);
										documento = value.documento_identidad;
										if(value.dv != ""){
											documento = documento +'-'+value.dv
										}
									/* return {
												children: $.map(value, function (v) {*/ 
													return {

																id: value.id,
																text: documento +' - '+ value.pasajero_data
															};
												/*  })
												};*/
										});
												return {
													results: results,
												};
							},
							cache: true
							},
							escapeMarkup: function (markup) {
											return markup;
							}, // let our custom formatter work
							minimumInputLength: 3,
				});

			$('#id_usuario').select2({
										//maximumSelectionLength: 2,
										placeholder: 'Usuario'
									});

			ordenarSelect('id_usuario');						


			$("#id_usuario").select2({
					ajax: {
							url: "{{route('getUsuarioListado')}}",
							dataType: 'json',
							placeholder: "TODOS",
							delay: 0,
							data: function (params) {
										return {
											q: params.term, // search term
											page: params.page
												};
							},
							processResults: function (data, params){
										var results = $.map(data, function (value, key) {
										console.log(value);
									/* return {
												children: $.map(value, function (v) {*/ 
													return {

																id: value.id,
																text: value.pasajero_data
															};
												/*  })
												};*/
										});
												return {
													results: results,
												};
							},
							cache: true
							},
							escapeMarkup: function (markup) {
											return markup;
							}, // let our custom formatter work
							minimumInputLength: 3,
				});


		}

	function recallSelectCliente(){
		$("#cliente_id").select2({
					ajax: {
							url: "{{route('getClienteListado')}}",
							dataType: 'json',
							placeholder: "TODOS",
							delay: 0,
							data: function (params) {
										return {
											q: params.term, // search term
											page: params.page
												};
							},
							processResults: function (data, params){
										var results = $.map(data, function (value, key) {
										console.log(value);
										documento = value.documento_identidad;
										if(value.dv != ""){
											documento = documento +'-'+value.dv
										}
									/* return {
												children: $.map(value, function (v) {*/ 
													return {

																id: value.id,
																text: documento +' - '+ value.pasajero_data
															};
												/*  })
												};*/
										});
												return {
													results: results,
												};
							},
							cache: true
							},
							escapeMarkup: function (markup) {
											return markup;
							}, // let our custom formatter work
							minimumInputLength: 3,
				});
	}

	function recallSelectPasajeroPrincipal(){
		$("#pasajero_principal").select2({
		        ajax: {
		                url: "{{route('getPasajeroPrincipal')}}",
		                dataType: 'json',
		                placeholder: "Seleccione un Beneficiario",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                processResults: function (data, params){
		                            var results = $.map(data, function (value, key) {
		                                        return {
		                                                    id: value.id,
		                                                    text: value.pasajero_data
		                                                };
		                            });
		                                    return {
		                                        results: results,
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });
	}
	</script>
@endsection
