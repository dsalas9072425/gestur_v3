
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">
	.negrita{
		font-weight: bold;
	}

	.negrita:hover{
		  background-color: #F0F0F0;
	}

</style>

<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Buscar Reserva</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	  <form id="frmSucConsulta" autocomplete="off">
				        <input type="hidden" class ="form-control" name="id_tipo" value="{{$id_tipo}}" id="id_tipo"/>
            	  		<div class = "row">
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Cliente</label>
									<select class="form-control selectCliente" name="cliente_id"  id="cliente_id" tabindex="1" style="width: 100%;" >
										<option value="">Todos</option>
											@foreach($clientes as $cliente)
												@php
													$ruc = $cliente->documento_identidad;
													if($cliente->dv){
														$ruc = $ruc."-".$cliente->dv;
													}
												@endphp
												<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->nombre}}  {{$cliente->apellido}} - {{$cliente->id}}</option>
											@endforeach
									</select>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Usuario</label>
									<select class="form-control select2" name="id_vendedor_empresa" id="id_vendedor_empresa" tabindex="2" style="width: 100%;" >
										<option value="">Todos</option>
										@foreach($vendedor_empresas as $usuario)
											<option value="{{$usuario->id}}">{{$usuario->nombre}}  {{$usuario->apellido}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Vendedor</label>
									<select class="form-control select2" name="vendedor_id"  id="vendedor_id" tabindex="3" style="width: 100%;">
										<option value="">Todos</option>
										@foreach($vendedor_agencia as $vendedor)
											<option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
										@endforeach
									</select>
								</div>
							</div>
						    <div class="col-12 col-md-3">
								<div class="form-group">
									<label>Nombre Apellido Pasajero</label>
									<input type="text" tabindex="4" class ="form-control" name="nombrePasajero" value="" id="nombrePasajero" maxlength="45"/>
								</div>
							</div>
						</div>
						<div class = "row">			    
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Código Proveedor</label>
									<input type="text" tabindex="5" class = "form-control" name="cod_confirmacion" value="" id="cod_confirmacion"/>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Código Nemo</label>
									<input type="text" tabindex="5" class = "form-control" name="cod_nemo" value="" id="cod_nemo"/>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Proforma</label>
									<input type="text" tabindex="5" class = "form-control" name="proforma" value="" id="proforma"/>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-3">  
									<div class="form-group adaptForm">
									   <label>Desde / Hasta Checkin</label>						 
									   <div class="input-group">
										   <div class="input-group-prepend" style="">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										   </div>
											<input type="text"  class = "Requerido form-control input-sm" id="periodo_in" name="periodo_in" tabindex="6"/>
									   </div>
								   </div>	
							</div> 
							<div class="col-12 col-sm-6 col-md-3">  
									<div class="form-group adaptForm">
									   <label>Desde / Hasta Checkout</label>						 
									   <div class="input-group">
										   <div class="input-group-prepend" style="">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										   </div>
											<input type="text"  class = "Requerido form-control input-sm" id="periodo_out" name="periodo_out" tabindex="7"/>
									   </div>
								   </div>	
							</div> 
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Disponible/En Proforma</label>
									<select class="form-control select2" name="proforma_estado" id="proforma_estado" tabindex="3" style="width: 100%;">
										<option value="">Todos</option>	
										<option value="no">Disponible</option>
										<option value="si">En proforma</option>
									</select>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Estado</label>
									<select class="form-control select2" name="estado" id="estado" tabindex="3" style="width: 100%;">
										<option value="">Todos</option>
										@foreach($estados as $estado)
											<option value="{{$estado->codigo_estado}}">{{$estado->codigo_estado}}</option>
										@endforeach
										<option value="Activo en Proforma">Activo en Proforma</option>
										<option value="Inactivo en Proforma">Inactivo en Proforma</option>
									</select>
								</div>
							</div>
							@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1)
								<div class="col-12 col-sm-6 col-md-3">  
									<div class="form-group">
										<label>Fecha Desde/Hasta Pago Nemo</label>						 
										<div class="input-group">
											<div class="input-group-prepend" style="">
												<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" class="form-control pull-right fecha" name="pago_desde_hasta" tabindex="7" id="pago_desde_hasta" value="">
										</div>
									</div>	
								</div> 	
							@endif

						<div class = "row">			   
							<div class="col-12" style="margin-top: 20px;padding-left: 30px;">
								<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white" style="margin: 0 0 10px 10px;"><b>Limpiar</b></button>
								<a  id="btnBuscar" class="pull-right btn btn-info btn-lg" style="margin: 0 5px; color: #fff;" role="button" tabindex="8"><b>Buscar</b></a>
							</div>
						</div>			
            	  </form>
            	<div class="table-responsive table-bordered">
	              <table id="listado" class="table" style="width: 100%;">
	                <thead>
					  	<tr>
							<th>Proforma</th>
							<th>Estado</th>
							<th>Factura</th>
							<th>Producto</th>
							<th>Proveedor</th>
							<th>Moneda</th>
							<th>Costo</th>
							<th>Codigo</th>
							<th>Agencia</th>
							<th>Vendedor</th>
							<th>Usuario</th>
							<th>Check-in</th>
							<th>Pasajero</th>
							<th>
								@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1)
										Fecha Pago Nemo
								@endif
							</th>
							<th></th>
		              	</tr>
	                </thead>
	               

			        <tbody style="text-align: center;">
	 					            
			         
			        </tbody>
	              </table>
	            </div>  
            </div>
        </div>    	
    </div>    
</section>
<div id="requestModal" class="modal fade" role="dialog">
	  		<div class="modal-dialog modal-xl" style="width: 100%;margin-top: 7%;">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="closeModal"  data-dismiss="modal" aria-label="Close">
	        				<span aria-hidden="true">&times;</span>
	        			</button>

						<h2 class="modal-title titlepage" style="font-size: x-large;">ASIGNAR PROFORMAS <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2></br>
						<a onclick="modalDestinos()" role="button" class="btn btn-info text-white" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;"><i class="fa fa-edit"></i>Nueva Proforma</a><span id="mensaje_c" style="font-weight: bold;"></span>
						<input type="hidden" class="form-control pull-right" name="id_reserva" id="id_reserva">
						<input type="hidden" class="form-control pull-right" name="id_producto" id="producto">
						<input type="hidden" class="form-control pull-right" name="cliente" id="cliente">
					</div>
				  	<div class="modal-body">

				  		<input type="hidden" id="idPersonas" value="">
						<div class="table-responsive">
				              <table id="listadoTickets" class="table" style="width:100%">
				                <thead>
									<tr style="text-align: center;">
								        <th></th>
								        <th>Proforma</th>
								        <th>Estado</th>
								        <th>Factura</th>
										<th>Fecha Proforma</th>
								        <th>Check-In</th>
								        <th>Cliente</th>
										<th>Vendedor</th>
										<th>Pasajero</th>
										<th>Operativo</th>
								        <th>Usuario</th>
						            </tr>
				                </thead>
				                <tbody style="text-align: center">

						        </tbody>
				              </table>
			            </div>
				    </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-info" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>
	<div id="modalDestino" class="modal fade" role="dialog">
	  		<div class="modal-dialog" style="width: 40%;margin-top: 7%;">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">ASIGNAR DESTINOS <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2></br>
					</div>
				  	<div class="modal-body">
						<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
							<div class="form-group form-button-cabecera">
 								<select class="form-control select2 input-sm text-bold" name="destino_id" id="destino_id" style="width: 100%;" tabindex="7">
									<option value="0">Seleccione Destino</option>
								</select>
							</div>
						 </div>
  					</div> 
				  <div class="modal-footer">
					<a onclick="asignarProformaReservas(0)" role="button" class="btn btn-info" style="margin-left:5px;color:#fff;padding-left: 6px;padding-right: 6px;"><i class="fa fa-edit"></i>Guardar</a>
				  </div>
			</div>
	  	</div>
	</div>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/vendors/js/tables/jszip.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/vendors/js/tables/buttons.html5.min.js')}}"></script> --}}
	<script>

	$(function() {

	$('.select2').select2();

	$('.selectCliente').select2({
		minimumInputLength: 2
		 // language: "es"
	});

	var tipo  = '{{$id_tipo}}';

	if(tipo == ""){
		$('#id_tipo').val('');
	}else{ 
		buscarProforma();
	}
	var Fecha = new Date();
	var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

	$('input[name="periodo_in"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    },
												startDate: "01/"+fechaInicial,
        										endDate: new Date()
								    			});
	$('input[name="periodo_in"]').val('');
	$('input[name="periodo_in"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

	$('input[name="pago_desde_hasta"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			});

	$('input[name="pago_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

	$('#pago_desde_hasta').val('');

	$('input[name="periodo_out"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			});

	$('input[name="periodo_out"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

	$('#periodo_out').val('');
			  
	$('#proforma_estado').val("no").trigger('change.select2');

	// buscarProforma();

	});


	function limpiar(){

		$('#cliente_id').val('').trigger('change.select2');
		$('#id_vendedor_empresa').val('').trigger('change.select2');
		$('#vendedor_id').val('').trigger('change.select2');
		$('#nombrePasajero');	
		$('#cod_confirmacion');	
		$('#periodo_in').val('');
		$('#periodo_out').val('');

		// buscarProforma();

	}



	/*
	Para formatear numeros a moneda USD PY
	 */
	const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD',
					  minimumFractionDigits: 2
					});
	

	$('#btnBuscar').on('click',function(){
		buscarProforma();
	});

	function buscarProforma(){

		$.blockUI({
				centerY: 0,
				message: "<h2>Procesando...</h2>",
				css: {
				        color: '#000'
				    }
				 });

		//RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){
			 table =   $('#listado').DataTable({
					        "destroy": true,
					        "select": true,
					        "ajax": {
					        		data: { "formSearch": $('#frmSucConsulta').serializeArray() },
						            url: "{{route('ajaxBuscarReserva')}}",
						            type: "GET",
						    error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        }
                        		// formatter.format(parseFloat(x.total_venta_proforma));
						        },"columns":[
						        			  {"data": function(x){ 
												  console.log(x);
												if(x.id_proforma === null){
													var btn = `<a style="color: #009c9f;">0</a>`;
												}else{ 
													var btn = `<a target="_blank" href="{{ route('detallesProforma',['id'=>'']) }}/${x.id_proforma}"  >${x.id_proforma}</a>`;
												}
													 return btn;
											 }},
						        			 {"data": "estado"},			
						        			 {"data": function(x){ 
						        			 	var btn = '';
						        			 	if(x.factura_id === null){
													var btn = ``;
												}else{
						        			 	 	btn = `<a target="_blank" href="{{ route('verFactura',['id'=>'']) }}/${x.factura_id}"  >${x.nro_factura}</a>`;
						        			 	}
						        			 		return btn;	}},
						        			 {"data": "producto_n"},
						        			 {"data": "proveedor_n"},
						        			 {"data": "currency_code"},
						        			 {"data": function(x){ 
						        			 			return formatter.format(parseFloat(x.costo_proveedor));}},
						        			 {"data": function(x){
						        			 	var cod = x.cod_confirmacion;
						        			 		//cod = cod.substr(0,20);
						        			 		return cod;
						        			 }},
						        			 {"data": "cliente_n"},
						        			 {"data": "vendedor_n"},
						        			 {"data": "usuario_n"},
						        			 {"data": "fecha_in_format"},
						        			 {"data": function(x){ 
						        			 	var pasajero_n = x.pasajero_n;ajero_n = x.p
						        			 	//SI ES PASAJERO DTP
						        			 	if(x.pasajero_id == 4994){
						        			 		pasajero_n = x.pasajero_online;
						        			 	}
						        			 	return pasajero_n;   }},
											{"data": "fecha_notificado_nemo_format"},
											{"data": function(x){
													if(x.id_proforma === null && x.id_proveedor !=  0 && x.estado_id != 1 && x.agencia_id !=  null && x.vendedor_id !=  null){
														btnAsignar = "<a onclick='asignarProforma("+x.id+","+x.id_producto+","+x.cliente_id+")' id="+x.id+" class='btn btn-info' style='margin-left:5px;padding-left: 6px;padding-right: 6px; color:#fff;' role='button'><i class='fa fa-edit'></i></a>";
													}else{
														btnAsignar = "";
													}	
													return btnAsignar; 

											}},

						        		],
						        		"aaSorting":[[0,"desc"]],
						        		"createdRow": function ( row, data, index ) {
						        					$(row).addClass('negrita');
						        				}		
						                    
					    }).on('xhr.dt', function ( e, settings, json, xhr ){
								//Ajax event - fired when an Ajax request is completed.;
							 $.unblockUI();
						});
			}, 300);

	}//function

	function asignarProforma(idReserva,idProducto,idCliente){
		$("#id_reserva").val(idReserva);
		$("#producto").val(idProducto);
		$("#cliente").val(idCliente);
		$.ajax({
				type: "GET",
				url: "{{route('ajaxProforma')}}",
				dataType: 'json',
				success: function(rsp){
						console.log(rsp);
									var oSettings = $('#listadoTickets').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();
									for (i=0;i<=iTotalRecords;i++) {
										$('#listadoTickets').dataTable().fnDeleteRow(0,null,true);
									}
									$.each(rsp, function (key, item){
										if(jQuery.isEmptyObject(item.fecha_emision_formateo) == false){
											fechaEmision = item.fecha_emision_formateo;
										}else{
											fechaEmision ="";
										}

										if(jQuery.isEmptyObject(item.nro_factura) == false){
											nroFactura = item.nro_factura;
										}else{
											nroFactura ="";
										}

										if(jQuery.isEmptyObject(item.fecha_alta_format) == false){
											fechaAlta = item.fecha_alta_format;
										}else{
											fechaAlta ="";
										}

										if(jQuery.isEmptyObject(item.check_in_format) == false){
											checkIn = item.check_in_format;
										}else{
											checkIn ="";
										}


										var accion = '<a onclick="asignarProformaReservas('+item.id+')" role="button" class="btn btn-info" style="margin-left:5px;color:#fff;padding-left: 6px;padding-right: 6px;"><i class="fa fa-edit"></i></a>';
										var dataTableRow = [
														accion,
														item.id,
														'<b>'+item.denominacion+'</b>',
														nroFactura,
														fechaAlta,
														checkIn,
														'<b>'+item.cliente_n+ '</b>',
														item.vendedor_n,
														item.pasajero_n,
														item.operativo_n,
														item.usuario_n
															];

										var newrow = $('#listadoTickets').dataTable().fnAddData(dataTableRow);
										// set class attribute for the newly added row 
										var nTr = $('#listadoTickets').dataTable().fnSettings().aoData[newrow[0]].nTr;
										// and parse the row:
										var nTds = $('td', nTr);

						})
						$("#requestModal").modal("show");

				}

			})	 
		}	


		function asignarProformaReservas(idProforma){
			return swal({
				title: "GESTUR",
				text: '¿Está seguro de que desea asignar este servicio a la proforma?',
				showCancelButton: true,
				buttons: {
						cancel: {
								text: "No",
								value: null,
								visible: true,
								className: "btn-warning",
								closeModal: false,
						},
						confirm: {
								text: "Sí, solicitar",
								value: true,
								visible: true,
								className: "",
								closeModal: false
							}
						}
					}).then(isConfirm => {
						if (isConfirm) {
								dataString = {
									idProforma : idProforma,
									idProducto : $("#producto").val(),
									idReserva : $("#id_reserva").val(),
									idDestinos : $("#destino_id").val(),
								}
								$.ajax({
										type: "GET",
										url: "{{route('asignarProformaReserva')}}",
										dataType: 'json',
										data: dataString,
										success: function(rsp){	
										console.log(rsp);	
											if(rsp[0].p_cod_retorno_out == 'OK'){
												var descrpcion = 'Proforma N°: '+rsp[0].proforma;
												if(rsp[0].p_item_detalle_numero_out != 0){	
													descrpcion += '  Item N°: '+rsp[0].item;
												}	
												swal("Exito", descrpcion, "success");
												if(idProforma == 0){
														$.toast({
															heading: 'Exito',
															text: 'Se ha generado, la proforma ',
															position: 'top-right',
															showHideTransition: 'slide',
															icon: 'success'
														});  
												}else{
													$.toast({
															heading: 'Exito',
															text: descrpcion,
															position: 'top-right',
															showHideTransition: 'slide',
															icon: 'success'
														});  
												}

												$("#requestModal").modal('hide');
												$("#modalDestino").modal('hide');
												buscarProforma();
											}else{
												swal("Cancelado", rsp[0].p_cod_retorno_out, "error");											}	
										}
									})	
						} else {
							swal("Cancelado", "", "error");
						}
				});
			}	
			function modalDestinos(){
			 	if($("#cliente").val() !== ""){
					$("#destino_id").select2({
								ajax: {
										url: "{{route('destinoProforma')}}",
										dataType: 'json',
										placeholder: "Seleccione un Proforma",
										delay: 0,
										data: function (params) {
											return {
												q: params.term, // search term
												page: params.page
											};
										},
										processResults: function (data, params){
											var results = $.map(data, function (value, key) {
												return {
													children: $.map(value, function (v) {
														return {
															id: key,
															text: v
														};
													})
												};
											});
											return {
												results: results,
											};
										},
										cache: true
									},
									escapeMarkup: function (markup) {
										return markup;
									}, // let our custom formatter work
									minimumInputLength: 3,
								});

					$("#modalDestino").modal("show");
				}else{
					$.toast({
							heading: 'Error',
							text: 'La reserva no posee Agencia.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
						});

				}	 
			}

	</script>
@endsection