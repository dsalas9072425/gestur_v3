
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">
	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	/*.bgRed:hover{
		color: white;
	}*/

	/*background: #e2076a !important;padding-left: 6px;padding-right: 6px;*/



	/*==================SELECT STAR============================*/

	.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}


.click_estrella {
	cursor:pointer; 
	cursor: hand;
}

/*====================================================
	ESTILO PARA FOCUS
  ====================================================	
*/

input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }



</style>
    <!-- Main content -->
   <section id="base-style">
   		<div class="card-content">

		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Listado de Proformas</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
				<div class="card-body pt-0">
					<a href="{{route('proformaAdd')}}" title="Agregar Persona" class="btn btn-success pull-right"
						role="button">
						<div class="fonticon-wrap">
							<i class="ft-plus-circle"></i>
						</div>
					</a>
				</div>		        
				<div class="card-body rounded-bottom rounded-lg">
		     		<form id="filtroProforma">

			     		<div class = "row">
							<div class="col-md-3">
								<div class="form-group">
						            <label>Cliente</label>
									<select class="form-control" name="cliente_id"  id="cliente" tabindex="1" style="width: 100%;" >
												<option value="">Todos</option>
									</select>
						        </div>
							</div>	
	 						 <div class="col-12 col-sm-3 col-md-3">
			 					<div class="form-group">
						            <label>Desde/Hasta Checkin</label>			
						            	<div class="input-group">
									    <div class="input-group-prepend" style="">
									        <span id="fechcheck"class="input-group-text"><i class="fa fa-calendar"></i></span>
									    </div>
									   <input type="text" id="periodo_out" name="periodo_out" class="form-control">
									</div>
								</div>	 
							</div> 	
	 						 <div class="col-12 col-sm-3 col-md-3">
			 					<div class="form-group">
						            <label>Desde/Hasta Fecha Proforma</label>			
						            	<div class="input-group">
									    <div class="input-group-prepend" style="">
									        <span id="fechprofor" class="input-group-text"><i class="fa fa-calendar"></i></span>
									    </div>
									    <input type="text" id="periodos" name="periodos" class="form-control">
									</div>
								</div>	
							</div> 	
					
							<div class="col-md-3">
		 						<div class="form-group">
						            <label>Estado Proforma</label>
									<select class="form-control input-sm estado_proforma" name="id_estado_proforma[]" id="id_estado_proforma" style=" width: 100%;"  tabindex="5" >
										<option></option>
										@foreach($estados as $key=>$estado)
												<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
										@endforeach	
									</select>
					        	</div>
							</div>	
							<div class="col-md-3">
								<div class="form-group">
						            <label>Moneda</label>
									<select class="form-control input-sm select2" name="id_moneda" id="moneda" style=" width: 100%;"  tabindex="6" >
										<option value="">Todos</option>
											@foreach($currencys as $key=>$currency)
												<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
											@endforeach	
										</select>
						        </div>
							</div>	
							<div class="col-md-3">
				                <label class="control-label">Expediente</label>
				                <input type="text" class = "Requerido form-control" name="id_proforma_padre" id="id_proforma_padre" value="" tabindex="7">
							</div>	
							<div class="col-md-3">
								<div class="form-group">
						            <label>Usuario</label>
						            <select class="form-control select2" name="id_vendedor_empresa" id="id_vendedor_empresa" style="width: 100%;" tabindex="8">
										<option value="">Todos</option>
										@foreach($vendedor_empresas as $usuario)
												<option value="{{$usuario->id}}">{{$usuario->text}}</option>
											@endforeach
						            </select>
					        	</div>					
					    	</div>	
					
							<div class="col-md-3">
						        <div class="form-group" >
						            <label>Operativo</label>
						            <select class="form-control select2" name ="responsable_id" id="responsable_id" style="width: 100%;" tabindex="9">
										<option value="">Todos</option>
										@foreach($getOperativo as $operativo)
												<option value="{{$operativo->id}}">{{$operativo->nombre}}  {{$operativo->apellido}}</option>
											@endforeach
						            </select>
						        </div>
							</div>	
							<div class="col-md-3">
								<label class="control-label">Nro Factura</label>
			                	<input type="text" class = "Requerido form-control" name="nro_factura" id="nro_factura" value="" tabindex="10">					
			                </div>	
							<div class="col-md-3">
				                <label class="control-label">Nro Proforma</label>
				                <input type="text" class = "Requerido form-control" name="id_proforma" id="id_proforma" value="" tabindex="11">
							</div>	
							<div class="col-md-3">
						        <div class="form-group">
						            <label>Vendedor</label>
									<select class="form-control" name="vendedor_id"  id="vendedor" style="width: 100%;" tabindex="12">
										<option value="">Todos</option>
									</select>
						        </div>
							</div>	
					
							<div class="col-md-3">	
								<div class="form-group">
						            <label>Sucursal</label>
									<select class="form-control select2" name="id_sucursal"  id="id_sucursal" style="width: 100%;" tabindex="13">
										<option value="">Todos</option>
										@foreach ($sucursals as $sucursal)
											<option value="{{$sucursal->id}}">{{$sucursal->nombre}}</option>
										@endforeach
									</select>
						        </div>
							</div>		
							<div class="col-md-3">	
								<div class="form-group">
						            <label>Reconfirmacion</label>
									<select class="form-control select2" name="reconfirmacion"  id="reconfirmacion"  style="width: 100%;" tabindex="15">
										<option value="">Todos</option>
										<option value="true">SI</option>
										<option value="false">NO</option>
									</select>
						        </div>
							</div>		
							<div class="col-md-3">	
								<div class="form-group">
						            <label>Post-Venta</label>
									<select class="form-control select2" name="post_venta"  id="post_venta"  style="width: 100%;" tabindex="15">
										<option value="">Todos</option>
										<option value="true">SI</option>
										<option value="false">NO</option>
									</select>
						        </div>
							</div>	
							<div class="col-md-3">
				                <label class="control-label">File/Código</label>
				                <input type="text" class = "Requerido form-control" name="file_codigo" id="file_codigo" value="" tabindex="11">
							</div>	
							<div class="col-md-3">	
								<div class="form-group">
						            <label>Destino (*)</label>
									<select class="form-control select2" name="destino"  id="destino" style="width: 100%;" tabindex="13">
										<option value="">Todos</option>
									</select>
						        </div>
							</div>		
							<div class="col-md-3">	
								<div class="form-group">
						            <label>Negocio</label>
									<select class="form-control select2" name="negocio"  id="negocio" style="width: 100%;" tabindex="14">
										<option value="">Todos</option>
										@foreach ($negocios as $negocio)
											<option value="{{$negocio->id}}">{{$negocio->descripcion}}</option>
										@endforeach
									</select>
						        </div>
							</div>		
							<div class="col-md-3">
								<div class="form-group">
									<label>Grupos  </label></label>
									<select class="form-control select2" name="grupo_id" id="grupo_id" tabindex="2" style="width: 100%;" required>
										<option value="">Todos</option>
										@foreach($grupos as $key=>$grupo)
											<option value="{{$grupo->id}}"><b>{{$grupo->denominacion}}</b></option>
										@endforeach	
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
						            <label>Asistente</label>
						            <select class="form-control select2" name="id_asistente" id="id_asistente" style="width: 100%;" tabindex="8">
										<option value="">Todos</option>
										@foreach($asistentes as $usuario)
												<option value="{{$usuario->id}}">{{$usuario->nombre}}  {{$usuario->apellido}}</option>
											@endforeach
						            </select>
					        	</div>					
					    	</div>	
							<div class="col-md-3">
								<div class="form-group">
						            <label>Equipo</label>
						            <select class="form-control select2" name="id_equipo" id="id_equipo" style="width: 100%;" tabindex="8">
										<option value="">Todos</option>
										@foreach($equipos as $equipo)
												<option value="{{$equipo->id}}">{{$equipo->nombre_equipo}}</option>
											@endforeach
						            </select>
					        	</div>					
					    	</div>	
							<div class="col-md-3">
								<div class="form-group">
						            <label>Prioridad</label>
						            <select class="form-control select2" name="id_prioridad" id="id_prioridad" style="width: 100%;" tabindex="8">
										<option value="">Todos</option>
										@foreach($prioridades as $prioridad)
												<option value="{{$prioridad->id_prioridad}}">{{$prioridad->des_prioridad}}</option>
											@endforeach
						            </select>
					        	</div>					
					    	</div>	


							<div class="col-md-3">
		                		<input type="hidden" name="star_option" value="" id="star_input">
							</div>		
						</div>	
					</form>	
					<div class = "row">
						<div class="col-md-12">	
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="pull-right btn btn-light btn-lg text-white mr-1 mb-1" tabindex="17" ><b>Limpiar</b></button>
						 	<button type="button"  class="pull-right  btn btn-info btn-lg mr-1 mb-1"  role="button" onclick="consultaAjax()" tabindex="16"><b>Buscar</b></button>
						</div>	 	
					</div>		 	
					<div class="table-responsive">
			              <table id="listado" class="table" style="width: 100%;">
			                <thead>
								<tr style="text-align: center;">
<!-- {{--							
								@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1)
										<th>Postergación</th>
									@else
										<th></th>
									@endif
--}}-->
							        <th>Proforma</th>
									<th>Prioridad</th>
							        <th>Estado</th>
							        <th>Factura</th>
									<th>Fecha Proforma</th>
							        <th>Check-In</th>
							        <th>Cliente</th>
									<th>Vendedor</th>
									<th>Pasajero</th>
									<th>Moneda Venta</th>
									<th>Monto Total </th>
									<th>% Renta</th>
									<th>Operativo</th>
							        <th>Usuario</th>
									<th>Destino</th>
									<th>Negocio</th>
							        <th></th>
					            </tr>
			                </thead>
			                <tbody style="text-align: center;">

			                </tbody>
			            </table>
			        </div>  
		        </div>
		    </div>
		</div>
    </div>	
</section>
    <!-- /.content -->
		

    <input type="hidden" name="perfil" id="perfilUsuario" value="0" />

	

     <!-- ========================================
   					MODAL POST VENTA
   	========================================  -->		

	<div class="modal fade" id="modalPostVenta"  role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="" style="font-weight: 800;">Calificación Post-Venta  <i style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
			<span class="msj_err" style="color:#DB6042;"></span>
			<span class="msj_success" style="color:#489946;"></span>
	      </div>
	      <div class="modal-body">
				<form id="valoracion_form" method="post" > 

									<div class="row">
					            		<div class="col-xs-12">
					            			<h4 style="margin:0; font-weight: 600;">Estrellas</h4>
						            			<div class="valoracion">
												  
											  	</div>
					            		</div>
					            	
					            	<div class="col-xs-12">
						            	<div class="form-group">
		    								<label style="font-weight: 600;font-size: 18px;">Comentario </label>
							            	<textarea class="form-control" rows="5" name="comentario" id="comentarioPostVenta" readonly></textarea>
						            	</div>
					            	</div>	
					            </div>	
					     <input type="hidden" name="id_proforma" value="">       
	  		   </form>	
	      </div>
	      <div class="modal-footer">
	      	  <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><b>Cerrar</b></button>
	      </div>
	    </div>
	  </div>
	</div>





@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>

	<script>
	
	$('[name="estrellas[]"]').click(function() {
      
			var arr = $('[name="estrellas[]"]:checked').map(function(){
			return this.value;
			}).get();
			
			var str = arr.join(',');
			
			$('#arr').text(JSON.stringify(arr));
			
			$('#star_input').val(str);
		
		});


		{{--==========================================
					VARIABLES GLOBALES
			==========================================--}}
			var ordenamiento = [];
			var table;
			// DEFINE EL IDIOMA ESPAÑOL A TODOS LOS SELECT2
			var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};


		$(document).ready(function() {


				let select_1 = $('.select2').select2({
					language: lang_es_select2
				});
				let select_2 = $('.estado_proforma').select2({
								    language: lang_es_select2,
									multiple:true,
									maximumSelectionLength: 2,
									placeholder: 'Todos'
								});

								
			$("#cliente").select2({
				language: lang_es_select2,
		        ajax: {
		                url: "{{route('get.clientes')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });


			$("#destino").select2({
				language: lang_es_select2,
		        ajax: {
		                url: "{{route('get.destinos')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });

			$("#vendedor").select2({
				language: lang_es_select2,
		        ajax: {
		                url: "{{route('get.vendedores.agencia')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });


			//ESTADO SEGUN EL TIPO PERSONA
			var tipo_persona = '{{$tipoPersona}}';
			 ordenamiento = [1,'desc'];

	/*		switch(tipo_persona){

			case '6':
				 $('#id_estado_proforma').val([3,47]).trigger('change.select2');
				 ordenamiento = [2,'desc'];
			break;

			case '2':
				$('#id_estado_proforma').val('24').trigger('change.select2');
			break;

			case '4':
				$('#id_estado_proforma').val('24').trigger('change.select2');
			break;

			default:
				$('#id_estado_proforma').val('1').trigger('change.select2');
			

		}*/
			$('#id_estado_proforma').val('1').trigger('change.select2');
			$('#fechcheck').click(function(){
				$('input[name="periodo_out"]').trigger('click');	
			}) 

			$('#fechprofor').click(function(){
				$('input[name="periodos"]').trigger('click');
			}) 

			var Fecha = new Date();
	        var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();


			let config = {
												timePicker24Hour: true,
												timePickerIncrement: 30,
												autoUpdateInput: false,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			};
			


			$( ".fecha" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$('input[name="periodo"]').daterangepicker(config);
			$('input[name="periodo"]').val('');
			$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
			$('input[name="periodo"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});

			$('input[name="periodos"]').daterangepicker(config);
			$('input[name="periodos"]').val('');
			$('input[name="periodos"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
			$('input[name="periodos"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});


			$('input[name="periodo_out"]').daterangepicker(config);
			$('input[name="periodo_out"]').val('');
			$('input[name="periodo_out"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
			$('input[name="periodo_out"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			}); 
		/*	$("#destino").select2({
					    ajax: {
					            url: "{{route('destinoProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});*/
		//	consultaAjax();

		});//READY DOCUMENT

		function fechaActual(){
			var d = new Date();
		    var month = d.getMonth()+1;
			var day = d.getDate();

					var output = d.getFullYear() + '/' +
					    ((''+month).length<2 ? '0' : '') + month + '/' +
					    ((''+day).length<2 ? '0' : '') + day;

					// alert(output);
					return output;
		}

		function calcularDias(in_date,out_date){
				var fechaini = new Date(in_date);
				var fechafin = new Date(out_date);
				var diasdif= fechafin.getTime()-fechaini.getTime();
				var contdias = Math.round(diasdif/(1000*60*60*24));
				return contdias;
			}




		function consultaAjax(){
		$.blockUI({
				centerY: 0,
				message: "<h2>Procesando...</h2>",
				css: {
					color: '#000'
				}
			});
		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){
			const formatter = new Intl.NumberFormat('de-DE', {currency: 'USD',minimumFractionDigits: 0});
			 table =   $('#listado').DataTable({
					        "destroy": true,
					        "select": true,
							"processing": true,
							"serverSide":true,
					        "ajax": {
					        		data: $('#filtroProforma').serializeJSON(),
						            url: "{{route('getDatosProforma')}}",
						            type: "GET",
									error: function(jqXHR,textStatus,errorThrown){
								$.toast({
									heading: 'Error',
									text: 'Ocurrio un error en la comunicación con el servidor.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
								});
									$.unblockUI();
								}
                        		// formatter.format(parseFloat(x.total_venta_proforma));
						  },
						 "columns":[
						        {data: function(x){
						        	return `<a href="{{route('detallesProforma',['id'=>''])}}/${x.id}"  class="bgRed">
						        	<i class="fa fa-fw fa-search"></i>${x.id}</a>
						        	<div style="display:none;">${x.id}</div>
						        	`;
				
						        }},
								{data: 'des_prioridad', render: function(data, type, full, meta){
										if(data == "URGENTE"){
											return ('<span style="color:red">'+data+'</span>');
										}else{
											return ('<span style="color:green">'+data+'</span>');
										}
										
									}
								},
						        {data: 'denominacion'},
						        {data: 'nro_factura'},
						        {data: 'fecha_alta_format'},
						        {data: 'check_in_format'},
						        {data: 'cliente_n'},
						        {data: 'vendedor_n'},
						        {data: function(x){ 
						        	var pasajero = x.pasajero_n;
						        	if(x.pasajero_id == 4994){
						        		pasajero = x.pasajero_online;
						        	}
						        	return pasajero;
						        }},
						        {data: 'moneda_venta'},
						        {data: function(x){ 
						        	var total_proforma = 0;
						        	if(jQuery.isEmptyObject(x.total_proforma) == false){
						        		var total_proforma = formatter.format(parseFloat(x.total_proforma));
						        	}
						        	return total_proforma;
						        }},
						        {data: 'markup'},
						        {data: 'operativo_n'},
						        {data: 'usuario_n'},
								{data: 'desc_destino'},
								{data: 'negocio'},
						        {data: function(x){ 
						        	var plane = '';
						        	if(x.ticketcount > 0){ 
						        		plane =  `<i class="fa fa-plane evento fa-lg" style ="color: #00acd6;" aria-hidden="true"></i>`;
						        }
						        return plane;
						    }}			 			

						        			],"columnDefs": [
												    {
												        targets: 0,
												        className: 'hover'
												    }
												  ],
												  "order":ordenamiento,
						     "createdRow": function ( row, data, index ) {
						     	
						        	// $(row).attr('onclick',`detalleProforma(${data['id']})`);
						        	$(row).addClass('negrita');
						        	
						        				}			
						                    
					    }).on('xhr.dt', function ( e, settings, json, xhr ){
								//Ajax event - fired when an Ajax request is completed.;
							 $.unblockUI();
								});
					    
			 
					    	}, 300);
			}//function


	function detalleProforma(n){

		location.href = '{{route('detallesProforma',['id'=>''])}}/'+n;
		 // window.open(,'_blank');
		// window.location = $(this).find("a").attr("href"); 
  // 		return false;
	}

	function limpiar(){
			$('#cliente').val('').trigger('change.select2');
			$('#periodo').val('').trigger('change.select2');
			$('#periodos').val('').trigger('change.select2');
			$('#id_estado_proforma').val('').trigger('change.select2');
			$('#moneda').val('').trigger('change.select2');
			$('#id_vendedor_empresa').val('').trigger('change.select2');
			$('#responsable_id').val('').trigger('change.select2');
			$('#post_venta').val('').trigger('change.select2');
			$('#vendedor').val('').trigger('change.select2');
			$('#id_sucursal').val('').trigger('change.select2');

			$('#nro_factura').val('');
			$('#id_proforma').val('');
			$('#id_proforma_padre').val('');
			$('#periodo_out').val('');
			$('.star_check').prop('checked', false);
			$('#star_input').val('');


			// consultaAjax();
		}




		function mostrar_postVenta(id_proforma){
			$('.valoracion').html('');
			$('#comentarioPostVenta').html('');

			dataString = { id_proforma: id_proforma};


				$.ajax({
					type: "GET",
					url: "{{route('getpostventa')}}",
					dataType: 'json',
					data: dataString,
							success: function(rsp){
						
								console.log(rsp);
							let calif = Number(rsp.resp.calificacion);
						 		let star = '';
						 		if(calif > 0){
						 			star += `<div style="display:none;">${calif}</div>`;
						 		}

						 		for (var i = 0; i < 5; i++) {

						 			if(calif > i){ 
						 			star += `<span class ="star_yelllow star-font">★</span>`;
						 			} else {
						 			star += `<span class="star">★</span><div style="display:none;"></div>`;
						 			}
						 			
						 		}

						 		$('.valoracion').html(star);
						 		$('#comentarioPostVenta').html(rsp.resp.comentario_calificacion);
						 		$('#modalPostVenta').modal('show');

							}
					})	
			}

			$("#itemList").select2({
					    ajax: {
					            url: "{{route('destinoGrupo')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});

			 userAuth();

			function userAuth(){

				
			 var id = $('#perfilUsuario').val();

				switch (id) {

			        
			        case '1':
			     $('.btnVerificacion').css("display", "none");

			            break;


			        default:


			        break;


			    }
			}
					       		

		// $('#listado tbody').on( 'click', 'tr', function () {
		// 	console.log('AAA'+$(this).attr('id'));     

		// 	if (!isNaN($(this).attr('id'))) { 
		// 		console.log('1');     
		// 		window.location.replace("../factour/detallesProforma/"+$(this).attr('id'));
		// 	}
		// });


	/**
	 * Convierte un texto de la forma 2017-01-10 a la forma
	 * 10/01/2017
	 *
	 * @param {string} texto Texto de la forma 2017-01-10
	 * @return {string} texto de la forma 10/01/2017
	 *
	 */
	function formatoFecha(texto){
			if(texto != '' && texto != null){
		  return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
			} else {
		  return '';	
				
			}

		}




					function validar(data){

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}


			$("#botonExcel").on("click", function(e){ 
			console.log('Inicil');
                e.preventDefault();
                $('#filtroProforma').attr('method','post');
                $('#filtroProforma').attr('action', "{{route('generarExcelProforma')}}").submit();
            });





	</script>
@endsection