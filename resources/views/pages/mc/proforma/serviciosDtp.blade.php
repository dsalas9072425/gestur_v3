
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<!-- Main content -->
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Servicios Online</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	<form id="frmReservaServicios" method="get"  autocomplete="off">
            		<div class="row">   

						<div class="col-12 col-sm-6 col-md-3">
							<div class="form-group">
								<label>Tipo Servicio</label>
								<select class="form-control" name="tipo_servicio" id="tipo_servicio" style="width: 100%;">
									<option value="">Todos</option>
									<option value="Transfer">Traslado</option>
									<option value="Tour">Circuitos</option>
									<option value="Activity">Actividades</option>
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-6 col-md-3">  
							<div class="form-group adaptForm">
								<label>Fecha de Reserva</label>						 
								<div class="input-group">
									<div class="input-group-prepend" style="">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="periodo" id="periodo" >
								</div>
							</div>	
						</div> 

						<div class="col-12 col-sm-6 col-md-3">
							<div class="form-group">
								<label>Localizador</label>
	                			<input type="text" class = "Requerido form-control" name="localizador" id="localizador" value="" tabindex="8">
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<div class="form-group">
								<label>Proforma</label>
	                			<input type="number" class = "Requerido form-control" name="proforma" id="proforma" value="" tabindex="8">
							</div>
						</div>
			
  						<div class="col-12 col-sm-6 col-md-3">
							<div class="form-group">
								<label>Pasajero</label>
	                			<input type="text" class = "Requerido form-control" name="pasajero" id="pasajero" value="" tabindex="8" maxlength="50">
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-3">
							<div class="form-group">
								<label>Moneda</label>
								<select class="form-control select2" name="divisa_id"  id="divisa_id" style="width: 100%;" required>
									<option value="">Seleccione Moneda</option>
										@foreach($monedas as $key => $moneda)
											<option value="{{$moneda->currency_code}}">{{$moneda->currency_code}}</option>
										@endforeach
								</select>
							</div>
						</div>
				
						<div class="col-12  col-md-6">
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white mr-1 mt-1"><b>Limpiar</b></button>

								<button type="button" onclick="buscarReservaServicio()" id="btnGuardar" class="btn btn-info btn-lg pull-right  mt-1 mr-1"><b>Buscar</b></button>
						</div>

					</div>					
            	</form>	
            	<div class="table-responsive table-bordered mt-1">
	              <table id="listado" class="table">
	                <thead>
					  	<tr>
							<th>Fecha</th>
							<th>Localizador</th>
							<th>Proveedor</th>
							<th>Proforma</th>
							<th>Pasajero</th>
							<th>Agencia</th>
							<th>Usuario</th>
							<th>Tipo</th>
							<th>Vendedor</th>
							<th>Fecha de Gasto</th>
							<th>Monto<br>Cancelación</th>
							<th>Precio<br>Venta</th>
							<th>Moneda</th>
							<th>Asignar<br>Proforma</th>		              
						</tr>
	                </thead>
	                <tbody id="lista_cotizacion" style="text-align: center">
						@foreach($reservaServicios as $key=>$reservaActividad)
							</tr>	
								<td>{{date("d/m/Y",strtotime($reservaActividad->fecha_reserva))}}</th>
								<td>{{$reservaActividad->localizador}}</td>
								<td>{{$reservaActividad->proveedor}}</td>
								<td>{{$reservaActividad->numero_proforma}}</td>
								<td>{{$reservaActividad->pasajero_principal}}</td>
								<td>{{$reservaActividad->personas->nombre}}</td>
								<td>
									{{$reservaActividad->usuario}}
								</td>
								<td>{{$reservaActividad->tipo_servicio}}</td>
								<td>
									@if(isset($reservaActividad->id_agente_dtp->nombre))
										{{$reservaActividad->id_agente_dtp->nombre}}
									@endif	
								</td>
								<td>{{$reservaActividad->fecha_cancelacion}}</td>
								<td>{{$reservaActividad->monto_cancelacion}}</td>
								<td>{{$reservaActividad->precio_venta}}</td>
								<td>{{$reservaActividad->moneda_proveedor}}</td>
								<td>
									@if($reservaActividad->id_estado_reserva == 2 && $reservaActividad->numero_proforma == "")
										<a onclick='asignarProforma({{$reservaActividad->id}},"{{$reservaActividad->tipo_servicio}}")' id="{{$reservaActividad->id_reserva}}"class="btn btn-info" style="margin-left:5px;padding-left: 6px;padding-right: 6px; color:#fff;" role="button"><i class="fa fa-edit"></i></a>
									@endif	
								</td>
							</tr>
						@endforeach
			        </tbody>
	              </table>
	            </div>  
            </div>
        </div>    
    </div>    
</section>
<!-- /.content -->
	<div id="requestModal" class="modal fade" role="dialog">
	  		<div class="modal-dialog modal-xl" style="width: 100%;margin-top: 7%;">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="closeModal"  data-dismiss="modal" aria-label="Close">
	        				<span aria-hidden="true">&times;</span>
	        			</button>

						<h2 class="modal-title titlepage" style="font-size: x-large;">ASIGNAR PROFORMAS <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2></br>
						<a onclick="modalDestinos()" role="button" class="btn btn-info text-white" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;"><i class="fa fa-edit"></i>Nueva Proforma</a><span id="mensaje_c" style="font-weight: bold;"></span>
						<!-- <span id="mensaje_c" style="font-weight: bold;"></span>-->
						<input type="hidden" class="form-control pull-right fecha" name="producto" id="producto">
						<input type="hidden" class="form-control pull-right fecha" name="servicios" id="servicios">
					</div>
				  	<div class="modal-body">

				  		<input type="hidden" id="idPersonas" value="">
						<div class="table-responsive">
				              <table id="listadoTickets" class="table" style="width:100%">
				                <thead>
									<tr style="text-align: center;">
								        <th></th>
								        <th>Proforma</th>
								        <th>Estado</th>
								        <th>Factura</th>
										<th>Fecha Proforma</th>
								        <th>Check-In</th>
								        <th>Cliente</th>
										<th>Vendedor</th>
										<th>Pasajero</th>
										<th>Operativo</th>
								        <th>Usuario</th>
						            </tr>
				                </thead>
				                <tbody style="text-align: center">

						        </tbody>
				              </table>
			            </div>
				    </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-info" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>
	<div id="modalDestino" class="modal fade" role="dialog">
	  		<div class="modal-dialog" style="width: 40%;margin-top: 7%;">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">ASIGNAR DESTINOS <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2></br>
					</div>
				  	<div class="modal-body">
						<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
							<div class="form-group form-button-cabecera">
 								<select class="form-control select2 input-sm text-bold" name="destino_id" id="destino_id" style="width: 100%;" tabindex="7">
									<option value="0">Seleccione Destino</option>
								</select>
							</div>
						 </div>
  					</div> 
				  <div class="modal-footer">
					<a onclick="asignarProformaServicios(0)" role="button" class="btn btn-info" style="margin-left:5px;color:#fff;padding-left: 6px;padding-right: 6px;"><i class="fa fa-edit"></i>Guardar</a>
				  </div>
			</div>
	  	</div>
	</div>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
		//$(document).ready(function() {

			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});
			
			$('input[id="periodo"]').daterangepicker({
													        timePicker24Hour: true,
													        timePickerIncrement: 30,
													        locale: {
													            format: 'DD/MM/YYYY',
													             cancelLabel: 'Cancelar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']

													        },
													       // minDate: new Date()
								    					});

			$('input[id="periodo"]').val('');

			$("#listado").dataTable({
				 					"aaSorting":[[0,"desc"]]
									});



		function limpiar()
		{	
			$('#tipo_servicio').val('').trigger('change.select2');
			$('#divisa_id').val('').trigger('change.select2');
			$('#periodo').val('');
			$('#localizador').val('');
			$('#proforma').val('');
			$('#pasajero').val('');
		}							


    function asignarProforma(idServicios, idProducto){

		$("#producto").val(idProducto);
		$("#servicios").val(idServicios);
		$.ajax({
				type: "GET",
				url: "{{route('ajaxProforma')}}",
				dataType: 'json',
				success: function(rsp){
						console.log(rsp);
									var oSettings = $('#listadoTickets').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();
									for (i=0;i<=iTotalRecords;i++) {
										$('#listadoTickets').dataTable().fnDeleteRow(0,null,true);
									}
									$.each(rsp, function (key, item){
										if(jQuery.isEmptyObject(item.fecha_emision_formateo) == false){
								            fechaEmision = item.fecha_emision_formateo;
								        }else{
								        	fechaEmision ="";
								        }

										if(jQuery.isEmptyObject(item.nro_factura) == false){
								            nroFactura = item.nro_factura;
								        }else{
								        	nroFactura ="";
								        }

										if(jQuery.isEmptyObject(item.fecha_alta_format) == false){
								            fechaAlta = item.fecha_alta_format;
								        }else{
								        	fechaAlta ="";
								        }

										if(jQuery.isEmptyObject(item.check_in_format) == false){
								            checkIn = item.check_in_format;
								        }else{
								        	checkIn ="";
								        }


										var accion = '<a onclick="asignarProformaServicios('+item.id+')" role="button" class="btn btn-info" style="margin-left:5px;color:#fff;padding-left: 6px;padding-right: 6px;"><i class="fa fa-edit"></i></a>';
								        var dataTableRow = [
								        				accion,
								                   		item.id,
					                        			'<b>'+item.denominacion+'</b>',
								                        nroFactura,
								                        fechaAlta,
								                        checkIn,
								                        '<b>'+item.cliente_n+ '</b>',
			 											item.vendedor_n,
								                        item.pasajero_n,
								                        item.operativo_n,
								                        item.usuario_n
															];

										var newrow = $('#listadoTickets').dataTable().fnAddData(dataTableRow);
										// set class attribute for the newly added row 
										var nTr = $('#listadoTickets').dataTable().fnSettings().aoData[newrow[0]].nTr;
										// and parse the row:
										var nTds = $('td', nTr);

						})
						$("#requestModal").modal("show");

				}

			})	 
		}	



   function asignarProformaServicios(idProforma){

  				return swal({
                        title: "GESTUR",
                        text: '¿Está seguro de que desea asignar este servicio a la proforma?',
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, solicitar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
                                	    swal("Exito", "", "success");
								 		dataString = {
											idProforma : idProforma,
											idProducto : $("#producto").val(),
											idServicio : $("#servicios").val(),
											idDestinos : $("#destino_id").val(),
										}

								   		$.ajax({
												type: "GET",
												url: "{{route('asignarProformaServicios')}}",
												dataType: 'json',
												data: dataString,
												success: function(rsp){	
												console.log(rsp);	
													if(rsp[0].p_cod_retorno_out == 0){
														var descrpcion = 'Proforma Nª: '+rsp[0].p_id_proforma_out;
														if(rsp[0].p_item_detalle_numero_out != 0){	
															descrpcion += 'Item Nª: '+rsp[0].p_item_detalle_numero_out;
														}	

														$.toast({
															heading: 'Exito',
															text: rsp[0].p_descripcion_out+"<br>"+descrpcion,
															position: 'top-right',
															showHideTransition: 'slide',
															icon: 'success'
														});  
														$("#requestModal").modal('hide');
														$("#modalDestino").modal('hide');
														buscarReservaServicio();
													}else{
														$.toast({
								                            heading: 'Error',
								                            text: 'Ocurrio un error al momento de generar la Proforma.',
								                            position: 'top-right',
								                            showHideTransition: 'fade',
								                            icon: 'error'
								                        });
													}	
												}
											})	
				            	} else {
                                     swal("Cancelado", "", "error");
                                }
						});





  
   	}	

		function buscarReservaServicio(){

			var dataString = $('#frmReservaServicios').serialize();

			$.ajax({
						type: "GET",
						url: "{{route('buscarServicio')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },
						success: function(rsp){
							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
									$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

							$.each(rsp, function (key, item){
									console.log(item.id_estado_reserva );
									console.log(item.numero_proforma);
									btn = '';
									if(item.id_estado_reserva == 2 && item.numero_proforma == null||item.numero_proforma == 0){
										direccion = item.id+",`"+item.tipo_servicio+'`';
										btn += '<a onclick="asignarProforma('+direccion+')" id="'+item.id_reserva+'" class="btn btn-info" style="margin-left:5px;color: #fff; padding-left: 6px;padding-right: 6px;"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;&nbsp';
									}

									if(jQuery.isEmptyObject(item.proveedor.denominacion_comercial) == false){
								      proveedor = item.proveedor.denominacion_comercial;
								    }else{
								      proveedor ="";
								   }

							//formatear fecha	
							var fecha = item.fecha_reserva;
							var fechaIntermedia = fecha.split(' ');
							var fechaFinal = fechaIntermedia[0].split('-');
							fechaMostrar = fechaFinal[2]+'/'+fechaFinal[1]+'/'+fechaFinal[0];
								var dataTableRow = [
														fechaMostrar,
														item.localizador,
														proveedor,
														item.numero_proforma,
														item.nombre_pasajero,
														item.agencia,
														item.usuario,
														item.tipo_servicio,
														'',
														item.fecha_cancelacion,
														item.monto_cancelacion,
														item.precio_venta,
														item.moneda_proveedor,
														btn
													];
								var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

							});

					


						}//cierreFunc	
				});	
			
		}

		function modalDestinos(){
			$("#destino_id").select2({
					    ajax: {
					            url: "{{route('destinoProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
						});

			$("#modalDestino").modal("show");
		}
	</script>
@endsection