@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	 
	<style>
		.asistencia{
			display: none;
		}
	 	.error{
	 		color:red;
	 	},
	   	.cabeceraClass {
	   		padding: 10px;
	   		min-width: 250px;
	   		background-color:#E8EAEA;
	   		word-break: break-all;
	   		margin: 10px;

	   	},
		.modal-footer {
		    text-align: left;
		} 
		.nav-tabs>li>a {
		    color: #dacdcd;
		} 
		.base{
			cursor: pointer;
		}

		.modal-bodyProforma{
			  height:250px;
             overflow:auto;
			}

		.commentNone {
			display: none;
		}
		.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

		.text-bold{
			font-weight: 800;
			font-size: 15px;
		}

		.select2-selection {
	    	max-height: 32px !important;
		}

		.select2-selection--single{
			padding-top: 0px !important;	
		}

		.select2-selection__rendered{
			padding-right: 6px;
		}

		.note-editing-area{
    		height: 300px;
		}

	    .chat-content {
	        text-align: left;
	        float: left;       
	        position: relative;
	        display: block;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #404e67;
	        background-color: #edeef0;
	        border-radius: 4px;
	    }

		.confirmacion{
			width: 478px;
			border-radius: 5px;
		}
		.select2-selection {
			 padding-right: 0px;
		}

	    .chat-content-left {
	        text-align: right;
	        position: relative;
	        display: block;
	        float: right;
	        padding: 8px 15px;
	        margin: 0 20px 10px 0;
	        clear: both;
	        color: #fff;
	        background-color: #00b5b8;
	        border-radius: 4px;
	    }  
	/*=============================================================================*/

	.valoracion {
	    position: relative;
	    overflow: hidden;
	    display: inline-block;
	}

	.valoracion input {
	    position: absolute;
	    top: -100px;
	}


	.valoracion label {
	    float: right;
	    color: #c1b8b8;
	    font-size: 30px; 
	}

	.valoracion label:hover,
	.valoracion label:hover ~ label,
	.valoracion input:checked ~ label {
	    color: #E2D532;
	}

	.select2-moneda_compra_0-container{
		padding-right: 10px;
    	padding-left: 4px;
	}

	modal-dialog modal-lg{
		margin : 0 auto;
	}

	.modal-lg{
		margin-left: 10%;
	}

/*=============================================================================================*/
	.btnAdjuntar{
			/* background-color: #00a65a !important;
			border-color: #008d4c !important; 
			color:#FFFF !important;*/s
			font-weight:800 !important;  
			padding:10px !important;
	}


	.verificarStyle {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}
	.solicitarFacturaParcial {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}

	.btnSolicitarVerificacion {
		padding:10px !important;
		/* background: #2d3e52 !important;
		border-color: #2d3e51 !important;
		color:#FFFF !important; */
	}

	.btnModificar {
		/* background: #e2076a !important; 
		border-color: #e25a7d !important;  */
		padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnAnular{
		/* background-color: #dd4b39 !important;
    	border-color: #d73925 !important; */
    	padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnHand {
		cursor:pointer; 
		cursor: hand;
	}

	.btnHand:hover {
  opacity: 0.8;
}


.vuelosClass{
   padding: 5px 0px;
}
/*#vuelos th {
	padding-left: 0px;padding-right: 0px;
}*/
.select2-container--default .select2-selection--single{
    padding-left: 0px;
}

.select2-moneda_compra_0-container{
	    padding-left: 0px;
	    padding-right: 0px;
}
		  
	</style>
		
@endsection
@section('content')
@php
        function formatMoney($num,$currency){
            // dd($f);
            if($currency != 111){
            return number_format($num, 2, ",", ".");	 
            } 
            return number_format($num,0,",",".");

        }
@endphp

    <!-- Main content -->
<section id="base-style">
   	<div class="card-content">
		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Proforma</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
                		<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body rounded-bottom rounded-lg">
		        	<form id="frmProforma" method="get" >
						@if(!empty($proformas))
							@foreach($proformas as $key=>$proforma)
							<input type="hidden" class = "Requerido form-control" id="control" value="0"  style="font-weight: bold;"/>
							<div class= "row">
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Nro Proforma</label>
										<input type="text" style="color:red" class = "Requerido form-control input-sm text-bold" name="proforma_id" value="{{$proforma->id}}" readonly="readonly"/>
										<input type="hidden" style="color:red" class = "Requerido form-control input-sm text-bold" id="comisionAgencia" value=""/>
									</div>
								</div>
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Estado</label>
										<input type="text" style="color:red" disabled="disabled" class = "Requerido form-control input-sm text-bold" name="" value="{{$proforma->estado}}" readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
									</div>
								</div>
							    <div class="col-md-3">
									<div id="botonesModalCliente" class="row" style="margin-left: 0px; margin-right: 0px;">
										<label>Cliente</label><br>
										<div class="input-group">
                                            <div class="input-group-prepend" id="button-addon2" style="width: 100%;">
	                                            <button id="botonCliente" disabled="disabled" class="btn btn-primary" data-toggle="modal" data-target="#requestModalCliente" type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-plus fa-lg"></i></button>
												<select class="form-control select2" name="cliente_id" id="cliente_id" style="width: 100%" disabled="disabled" aria-describedby="button-addon2" >
													@foreach($clientes as $cliente)
														@php
															$ruc = $cliente->documento_identidad;
															if(isset($cliente->dv)){
																$ruc = $ruc."-".$cliente->dv;
															}
														@endphp
														<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->nombre}} {{$cliente->apellido}} - {{$cliente->denominacion}} - {{$cliente->id}}</option>													
													@endforeach
												</select>                                        
                                            </div>
										</div>	
									</div>
								</div>
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Vendedor</label>
										<select class="form-control select2 input-sm text-bold" name="id_categoria" id="id_categoria" disabled="disabled" style="width: 100%;" required>
											<option value="0">Seleccione Vendedor</option>
										</select>
									</div>
								</div>							
							</div>	
							<div class= "row">
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Destino</label>
 										<select class="form-control select2 input-sm text-bold" name="destino_id" id="destino_id" style="width: 100%;" tabindex="7">
											<option value="0">Seleccione Destino</option>
									    </select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Check In - Check Out</label>
										<input type="text"  class = "Requerido form-control input-sm text-bold" id="periodo" value="{{$proforma->check_in_out}}" name="periodo" disabled="disabled" />
									</div>
								</div>
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Moneda de Facturación</label>
									    <select class="form-control select2 input-sm text-bold" name="moneda" id="moneda" style="width: 100%;" required disabled="disabled" >
										    <option value="">Seleccione Moneda</option>
											@if(isset($currencys))
												@foreach($currencys as $key=>$currency)
													@if($currency->tipo == 'V')
														<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
													@endif
												@endforeach	
											@endif
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Vencimiento </label>
									    <input type="text" value="" class="form-control pull-right fecha input-sm text-bold" name="vencimiento" id="vencimiento" style="color:red" data-toggle="popover" data-placement="left"  data-content="La fecha de vencimiento depende de las políticas de la empresa">
									</div>
								</div>
							</div>	
							<div class= "row">
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Monto Seña</label>
										<input type="text" onkeypress="return justNumbers(event);" class ="Requerido form-control numeric input-sm text-bold" name="senha" id="senha" value="{{$proforma->senha}}" disabled="disabled"/>
									</div>
								</div>
								<div class="col-md-3">
									<div class="input-group-sm">
									    <label>Proforma Principal</label>
									    <select class="form-control select2 input-sm text-bold" name="expediente" id="id_expediente" style="width: 100%;" disabled="disabled">
									    	@if($proforma->proforma_padre != 0)
												<option value="{{$proforma->proforma_padre}}">Proforma Nro: {{$proforma->proforma_padre}}</option>
											@endif	
									    </select>
									</div>
							    </div>
								<div class="col-md-3">
						 			<div class="input-group-sm">
									    <label>Incentivo Agencia</label>						 
									    <select class="form-control select2 input-sm text-bold" name="incentivo_agencia" id="incentivo_agencia" style="width: 100%;" disabled="disabled">
									        <option value="true">Sí</option>
											<option value="false">No</option>
									    </select>
									</div>  
								</div> 	
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Prioridad</label>
										<select class="form-control select2 input-sm text-bold" name="idPrioridad" id="idPrioridad" style="width: 100%;" disabled="disabled" >
											<option value="0">Seleccione una opción</option>
											<option value="1">NORMAL</option>
									        <option value="2">URGENTE</option>
									    </select>
									</div>
								</div>
							</div>				
							<div class="row">
								<div class="col-md-3"> 
						 			<div class="input-group-sm">
									    <label>Tipo de Facturación</label>						 
										<input type="text" class = "Requerido form-control input-sm text-bold" name="tipo_factura" id="tipo_factura" readonly="readonly"/>
									</div>  
								</div> 
								<div class="col-md-3">
							 		<div class="input-group-sm">
										<label>Tipo Documento</label>						 
										<select class="form-control select2 input-sm text-bold" name="tipo_venta" id="tipo_venta" style="width: 100%;" disabled="disabled" >
											@foreach($tipoFacturas as $key=>$tipoFactura)
												<option value="{{$tipoFactura->id}}">{{$tipoFactura->denominacion}}</option>
											@endforeach	
										</select>
									
									</div>  
								</div> 
							    <div class="col-md-3">
						 			<div class="input-group-sm">
									 <label>Negocio </label>
										<select class="form-control select2" name="negocio" id="negocio" style="width: 100%;">
												@foreach($negocios as $key=>$negocio)
													<option value="{{$negocio->id}}" selected="selected">{{$negocio->descripcion}}</option>
												@endforeach
										</select>
									</div> 
								</div>

								@if(isset($proforma->factura_id))	
									<div class="col-md-3">
										<div class="input-group-sm">
											<a href="{{route('verFactura',['id'=>''])}}/{{$proforma->factura_id}}" style="color:black"  class="bgRed"><label class = "base">Nro Factura  <i class="fa fa-fw fa-search" style="color:red"></i></label></a>
											<input style="color:red" type="text" class = "Requerido form-control input-sm text-bold" name="proforma_id" style="font-weight: bold;font-size: 15px;" value="{{$proforma->nro_factura}}" readonly="readonly"/>
										</div>
									</div>
								@else
									<div class="col-md-3">
										<div class="input-group-sm">
											<label>Nro Factura</label>
											<input style="color:red" type="text" class = "Requerido form-control input-sm text-bold" name="proforma_id" style="font-weight: bold;font-size: 15px;" value="" readonly="readonly"/>
										</div>
									</div>
								@endif	
							</div>			
							<div class="row">	
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Grupos  </label>
										<select class="form-control select2 input-sm text-bold" name="grupo_id" id="grupo_id" tabindex="2" style="width: 100%;" required>
											<option value="0">Seleccione Grupo</option>
											@foreach($grupos as $key=>$grupo)
												<option value="{{$grupo->id}}"><b>{{$grupo->denominacion}}</b></option>
											@endforeach	
										</select>
									</div>
							    </div>
								<div class="col-md-3">
									<div class="input-group-sm">
										<label>Promoción   <label id="id_categoriaLabel" class="error"></label></label>
										<select class="form-control select2 input-sm text-bold" name="id_promocion" id="id_promocion" tabindex="2" style="width: 100%;" required>
											<option value="">Seleccione Promoción</option>
											@foreach($promociones as $key=>$promocion)
												<option value="{{$promocion->id}}"><b>{{$promocion->descripcion}}</b></option>
											@endforeach	
										</select>
									</div>
							    </div>	
							    <div class="col-md-6">
									<div id="botonesModal" class="row" style="margin-left: 0px; margin-right: 0px;">
										<label>Pasajeros</label><br>
										<div class="input-group">
                                            <div class="input-group-prepend" id="button-addon1" style="width: 100%;">
												@php 
													$cantidad = count($pasajeros);
												@endphp
                                                <button id="botonPasajero" disabled="disabled" class="btn btn-primary" data-toggle="modal" data-target="#requestModalPasajero" type="button" style="padding-bottom: 0px;padding-top: 2px;padding-right: 10px;padding-left: 10px;"><i class="fa fa-plus fa-lg"></i></button>
												<select class="form-control select2" name="pasajero_principal[]" multiple="multiple" id="pasajero_principal" style="width: 100%" aria-describedby="button-addon1" >
													@foreach($pasajeros as $key=>$pasajero)
														@if($key < 2)
															<option value="{{$pasajero->id}}" selected="selected">{{$pasajero->pasajero_n}}</option>
														@endif
													@endforeach	
												</select>      
												@if($cantidad > 2)
													<button id="botonPasajeroDetalles" onclick="pasajerosDetalle()" class="btn btn-primary" data-toggle="modal" data-target="#requestModalPasajeroDetalle" type="button" style="padding-bottom: 0px;padding-top: 2px;padding-right: 10px;padding-left: 10px;"><i class="fa fa-flickr fa-lg"></i></button>
												@endif
                                            </div>
										</div>	
									</div>
								</div>
							</div>			
							<div class="row">	
								<div class="col-md-3"> 
									<div class="input-group-sm">
										<label>Cantidad Pasajeros </label>
										<input type="text" onkeypress="return justNumbers(event);" style="font-weight: bold;font-size: 15px;" value="{{$proforma->cantidad_pasajero}}"  class="form-control input-sm" name="cant_pasajero" id="cant_pasajero" disabled="disabled">
									</div>
								</div>
								@if($proforma->pasajero_id == 4994)
									<div class="col-md-3">
										<div class="input-group-sm">
											<label>Pasajero </label>
											<input type="text" value="{{--$pasasjeroVoucher--}}" class="form-control input-sm" name="pasajeroDTPmundo" id="pasajeroDTPmundo" disabled="disabled">
										</div>
									</div> 
								@endif   

								@if($permisoEditarUsuario == 1)
									<div class="col-md-3" style="height: 60px;"> 
										<div class="input-group-sm">
											<label>Usuario</label>
											<select class="form-control select2 input-sm text-bold" name="id_usuario" id="usuarioNA" tabindex="2" style="width: 100%;" disabled="disabled">
													@foreach($usuarios as $key=>$usuario)
														<option value="{{$usuario->id}}"><b>{{$usuario->nombre}} {{$usuario->apellido}}({{$usuario->email}})</b></option>
													@endforeach	
											 </select>
										</div>
									</div> 
								@else
									<div class="col-md-3" style="height: 60px;"> 
										<div class="input-group-sm">
											<label>Usuario</label>
											<input type="text" style="font-weight: bold;font-size: 15px;" value="{{$proforma->usuario_nombre}} {{$proforma->usuario_apellido}} ({{$proforma->usuario_email}})" class="form-control pull-right input-sm " disabled="disabled">
										</div>
									</div> 
								@endif 
								
								<div class="col-md-3"> 
									<div class="input-group-sm">
										<label>Incentivo Asignado</label>
											<!--<input type="number" onKeyUp="return controlNumerico($(this).val());" style="font-weight: bold;font-size: 15px;" min="1" max="10" class="form-control pull-right input-sm " value="{{$proforma->incentivo_venta}}" name="incentivo" id="incentivo" disabled="disabled">-->
											<select class="form-control select2 input-sm text-bold" name="incentivo" id="incentivo" tabindex="2" style="width: 100%;" disabled="disabled">
												<?php 
												for($x=0;$x<=10;$x=$x+1){ 
													echo "<option value='".$x."'><b>".$x."</b></option>" ; 
												};
												for($y=10.5;$y<=30;$y=$y+0.5){ 
													echo "<option value='".$y."'><b>".str_replace('.',',',$y)."</b></option>" ; 
												};
												?>
											 </select>
										</div>
								</div>	

								<div class="col-md-3"> 
									<div class="input-group-sm">
										<label>Tarifa</label>
										<select class="form-control select2 input-sm text-bold" name="id_tarifa" id="id_tarifa" tabindex="2" style="width: 100%;" disabled="disabled">
											@foreach($tarifas as $key=>$tarifa)
												<option value="{{$tarifa->id}}"><b>{{$tarifa->descripcion}}</b></option>
											@endforeach	
										 </select>
									</div>
								</div>	
								<div class="col-md-3" style="height: 60px;">
									<div class="input-group-sm">
										<label>File/Código Cliente</label>
										@if($marcadorfile == 1)
											<input type="text" style="font-weight: bold;font-size: 15px;" value='{{$proforma->file_codigo}}' class="form-control input-sm" name="file_codigo" disabled="disabled">
										@else
											<input type="text" style="font-weight: bold;font-size: 15px;" value='{{$proforma->file_codigo}}' class="form-control input-sm" name="file_codigo" id="file_codigo" disabled="disabled">
										@endif	
									</div>
								</div> 

								<!-- {{--<div class="col-md-3" id="saldoGrupo" >
									<div class="input-group-sm">
										<label>Saldo Grupo </label>
										<input type="text" style="font-weight: bold;font-size: 15px;" value='{{number_format($saldo,2,",",".")}}' class="form-control input-sm" name="saldoGrupoMonto" id="saldoGrupoMonto" disabled="disabled">
									</div>
								</div> --}} -->
									<div  class="col-md-3" style="height: 60px; {{ $proforma->facturacion_parcial ? '' : 'display:none'}}">
										<div class="input-group-sm">
											<label>Saldo de Proforma</label>
											<input type="text" style="font-weight: bold;font-size: 15px;" value='{{ number_format($proforma->saldo_por_facturar, 2, ",", ".") }}' class="form-control input-sm" name="saldoFacturaMonto" id="saldoFacturaMonto" disabled="disabled">
										</div>
									</div>


								<div class="col-12 col-sm-3 col-md-3" style="height: 60px;">
									<div class="form-group">
										<label>Imprimir Precio Unitario</label>
										<select class="form-control select2" name="imprimir_precio_unitario" id="imprimir_precio_unitario" style="width: 100%;">
											<option value="1">Si</option>
											<option value="">No</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="height: 60px;">
						 			<div class="form-group form-button-cabecera">
									    <label>Tipo</label>						 
									    <select class="form-control select2 input-sm text-bold" name="tipo_tarjeta" id="tipo_tarjeta" style="width: 100%;"  disabled="disabled">
											<option value="false">Neta</option>
											<option value="true">Bruta</option>
									    </select>
									</div> 
								</div>
								@if($permisoEditarUsuario == 1)
									<div class="col-md-3" style="height: 60px;"> 
										<div class="input-group-sm">
											<label>Asistente</label>
											<select class="form-control select2 input-sm text-bold" name="id_asistente" id="id_asistente" tabindex="2" style="width: 100%;" disabled="disabled">
													@foreach($usuarios as $key=>$usuario)
														<option value="{{$usuario->id}}"><b>{{$usuario->nombre}} {{$usuario->apellido}}({{$usuario->email}})</b></option>
													@endforeach	
											 </select>
										</div>
									</div> 
								@else
									<div class="col-md-3" style="height: 60px;"> 
										<div class="input-group-sm">
											<label>Asistente</label>
											<input type="text" style="font-weight: bold;font-size: 15px;" value="{{$proforma->asistente_nombre}} {{$proforma->asistente_apellido}} ({{$proforma->asistente_email}})" class="form-control pull-right input-sm " disabled="disabled">
										</div>
									</div> 
								@endif 
								@if (!empty($proforma->motivo_autorizacion))
									<div class="col-md-4" style="height: 100px;">
										<div class="input-group-sm">
											<label>Motivo Autorizacion</label>
											<input type="text" style="font-weight:; font-size: 15px;" value="{{ $proforma->motivo_autorizacion }}" class="form-control pull-right input-sm" disabled="disabled">
										</div>
									</div>
								@endif
								@if (!empty($usuario_autorizacion))
								<div class="col-md-4" style="height: 100px;">
									<div class="input-group-sm">
										<label>Autorizado Por</label>
										<input type="text" style="font-weight:; font-size: 15px;" value="{{ $usuario_autorizacion[0]->nombre }}" class="form-control pull-right input-sm" disabled="disabled">
									</div>
								</div>
								@endif
							<!--REPRICE-->
							@if (collect($reprice)->contains('reprice', true))
							<div class="col-md-4" style="height: 100px;">
									<div class="input-group-sm">
										<label>Reprice</label>
										<input type="text" style="font-weight:; font-size: 15px;" value="SI" class="form-control pull-right input-sm" disabled="disabled">
									</div>
							</div>
							@endif

								@if (!empty($proforma->reconfirmacion))
										<div class="col-md-3">
										<div class="input-group-sm">
											<label>Reconfirmacion</label>
											<input type="text" class="Requerido form-control input-sm text-bold" name="reconfirmacion" 
											value="{{ $proforma->reconfirmacion ? '★ RECONFIRMADO' : '☆ SIN RECONFIRMACION' }}" 
											readonly="readonly" style="color: red; font-size: 16px;">
									 
										</div>
									</div>
								@endif
								{{-- <pre>
									{{ json_encode($usuario_autorizacion, JSON_PRETTY_PRINT) }}
								</pre> --}}

								@if (!empty($proforma->comentario_autorizacion))
								<div class="col-12 col-sm-12 col-md-12">
									<label style="margin-right: 20px;">Comentario Autorizacion</label>
									<textarea readonly disabled name="comentario_autorizacion" id="comentario_autorizacion" rows="2" cols="200" style="width: 99%; background-color: #eee; cursor: not-allowed;">{{ $proforma->comentario_autorizacion }}</textarea>
								</div>
								@endif
						
								<div class="col-md-12">
									<label>Clientes Adicionales </label>
									<select class="form-control select2 input-sm text-bold" multiple style="width: 100%;" disabled>
										@foreach ($clientes_auxiliar as $cliente)
											<option value="{{$cliente->cliente->id}}" selected>{{$cliente->cliente->nombre}} {{$cliente->cliente->apellido}}</option>
										@endforeach
									</select>
								</div>
								
								<div class="col-12 col-sm-12 col-md-12">
									<label style="margin-right: 20px;">Concepto genérico</label>
									<textarea name="concepto" id="concepto" rows="2" cols="200" style="width: 99%;"></textarea>
								</div>

							</div>
							<div class="row"> 
								<div class="col-md-4" style="padding-top: 20px;">
									{{-- NO MOSTRAR SI ES ANULADO O FACTURADO --}}
									@if($proforma->estado_id != 5 && $proformas[0]->estado_id != 4)
								       <div class="btn-group">
											<button type="button" class="btn btn-info btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    Acciones <span class="caret" width="100px"></span>
											</button>
											<ul class="dropdown-menu" style=" width: 250px !important;">
												{{-- MOSTRAR SI ES EL USUARIO PROPIETARIO --}}
									            @if($activador == 1)	
													<li class="btnHand">
														<a   data-toggle="modal" class="btnAdjuntar" data-target="#modalAdjunto" title="Adjuntar">
															<i class="fa fa-files-o fa-lg"></i>
															<b>Adjuntar</b>
														</a>
													</li>
												@endif	
												@foreach ($btn as $boton)
													{{-- CARGA LOS BOTONES POR PERMISO DE PERSONAS --}}
													<?php echo '<li class="btnHand">'.$boton.'</li>'; ?>
													@endforeach
													<li class="baseProforma btnHand">
														<a href="#" class="solicitarFacturaParcial" title="Solicitar Factura Parcial">
														<i class="fa fa-fw fa-credit-card"></i><b>Solicitar Factura Parcial</b> </a>
													</li>
										 		{{-- PROFORMA EN ESTADO ABIERTO --}}
													@if($proforma->estado_id == 1)	
														{{-- MOSTRAR SI ES EL USUARIO PROPIETARIO --}}
													@if($activador == 1)	
														<li class="baseProforma btnHand">
															<a  class="btnSolicitarVerificacion"  id="btnVerificacion" title="Solicitar Verificación" style="">
																<i class="fa fa-external-link fa-lg"></i><b>Solicitar Verificación</b></a>
														</li>
														@if($permisoEmpresaSolicitud)
															<li class="baseProforma btnHand">
																<a  class="btnModificar" id="btnActualizar" title="Modificar">
																	<i class="fa fa-fw fa-edit fa-lg" ></i><b>Modificar</b></a>
															</li>
														@endif
														@if($anticipos != 1)
															<li class="baseProforma btnHand">
																<a  class="btnAnular" id="btnAnular" title="Anular" style="">
																	<i class="fa fa-times fa-lg"></i><b>Anular</b></a>
															</li>
														@endif
														<li class="btnHand">
															<a  class="btnSolicitarVerificacion"  id="btnFormaPago" title="Asignar Forma de Pago">
																<i class="fa fa-fw fa-credit-card"></i><b>Forma Pago</b></a>
														</li>
													@endif
												@endif	

											{{-- NO MOSTRAR SI ES ANULADO O FACTURADO --}}
												@if($proforma->estado_id != 5 && $proformas[0]->estado_id != 4 && $proformas[0]->estado_id !=83)
													@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
														<li class="btnHand">
															<a  title="" class="btnSolicitarVerificacion" id="btnEmisionTicket"><i class="fa fa-fw fa-ticket"></i><b>Solicitud Emisión Ticket</b></a>
														</li>
													@endif	
												@endif
											<li class="btnHand">
												<a href="{{ route('previewFactura',['id'=>$proforma->id]) }}" class="verificarStyle"  title ="Preview" id=""><i class="fa fa-eye fa-lg"></i><b>Preview</b></a>
											</li>

								    	</ul>
									</div>
							 		@if($activador == 1)
										<a class="btn btn-primary btn-lg actualizarProforma" id="btnGuardarProforma" style="display: none;" title="Guardar"><i class="fa fa-fw fa-save"></i></a>
										<a class="btn btn-error btn-lg actualizarProforma" id="btnCancelar" style="background-color: red;color: white;" title="Guardar"><i class="fa fa-fw fa-close"></i></a>
								@endif	
					            @endif	
					            @if($proformas[0]->estado_id == 4)
					            	@if(Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario == $proforma->id_usuario || Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2 || Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 6)
					            		@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'A')	
												<!--{{--<button type="button" class="btn btn-danger btn-lg " data-toggle="modal" data-target="#myModales" >
													<i class="fa fa-exclamation-triangle"></i> <b>Emergencia</b>
												</button>--}}-->
										@endif		
									@endif
								@endif
								</div> 
								<div class="col-md-1" style="padding-top: 25px;margin-top: 5px;">
									@if($proforma->estado_id != 5 && $proformas[0]->estado_id != 4 && $proformas[0]->estado_id != 83)
										@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'A')					
										<!--<button type="button" id="btnNemo" style="width: 80px;background-color: #fff;padding-left: 0px;border-top-width: 0px;border-left-width: 0px;border-right-width: 0px;border-bottom-width: 0px;">
												<img class="direct-chat-img" style="border-radius: 0%;width: 80px;height: 25px;" src="{{asset('images/nemo.png')}}" alt="message user image">
											</button>-->
										@endif
									@endif	
								</div>
								<div class="col-md-7" style="padding-top: 20px;">
										<div class="pull-right" style="display: inline-block; margin-bottom:0; vertical-align: bottom; border:1px solid; padding:5px; width: 60%; background: white; color: #d63939;    border-color: #d60000; font-size: 20px;    font-weight: bold;" id="campoComentarios">	 Sin comentarios ....
												</div>							
												<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#modalAdjunto" style="background-color: #ed00ef;margin:0 5px 0 0;padding-top: 10px;padding-bottom: 10px;">
											    <i class="fa fa-files-o fa-lg"></i>
												</button>
												<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal" style="background-color: #ed00ef;margin:0 5px 0 0;padding-top: 10px;padding-bottom: 10px;">
													<i class="fa fa-fw  fa fa-weixin"></i>
												</button>
												@if($proformas[0]->estado_id == 4 && $proformas[0]->calificacion == 0)
											<button type="button" class="btn btn-info pull-right" title="Post Venta" data-toggle="modal" data-target="#modalPostVenta" style="margin:0 5px 0 0;padding-top: 10px;padding-bottom: 10px;">
													<i class="fa fa-fw fa-star"></i>
												</button>
											@endif
							     				<button type="button" class="btn btn-info pull-right" data-toggle="modal" title="Time Line" data-target="#modalTimeLineProforma" style="margin:0 5px 0 0;padding-top: 10px;padding-bottom: 10px;">
													<i class="fa fa-fw fa-clock-o"></i>
												</button>
											@if($proformas[0]->estado_id == 1)	
												<button type="button" class="btn btn-info pull-right" id="btn_pf_dt" data-toggle="modal" title="Detalles de Factura" data-target="#modalDetallesFactura" style="margin:0 5px 0 0;padding-top: 10px;padding-bottom: 10px;">
													<i class="fa fa-fw 	fa fa-print"></i>
												</button>
											@endif
											@if(count($solicitudes) != 0)											
												<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#modalSolicitudes" title="Ver Solicitudes Facturas Pendientes" style="margin:0 5px 0 0;padding-top: 10px;padding-bottom: 10px;">
													<i class="fa fa-fw 	fa fa-window-restore"></i>
												</button>	
											@endif
											<button type="button" class="btn btn-info pull-right" title="Clientes Adicionales" data-toggle="modal" data-target="#requestModalCliente" style="margin:0 5px 0 0;padding-top: 10px;padding-bottom: 10px;">
													<i class="fa fa-fw fa-user"></i>
												</button>

								        </div>
				    			</div>
		        			@endforeach	
						@endif
						<input type="hidden" class = "Requerido form-control input-sm" id="proforma_id" name="proforma_id" value="{{$id}}" />
		         	</form> 	
  				</div>
		    </div>
		</div>
		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Detalles de Proforma</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">
					<div id="mensajes"></div>
					    <div class="table-responsive">
						    <table id="lista_productos" class="table">
						        <thead>
									<tr style="background-color: #eceff1;">
										<th style="padding-left: 0px;padding-right: 0px;">Cant.</th>
										<th >Producto</th>
										<th style="width: 80px;">Código</th>
										<th style="width: 12%;">Proveedor</th>
										<th style="padding-left: 10px;padding-right: 5px;">Moneda<br>Costo</th>
										<th>Costo</th>
										<th>Tasas<br>N/C</th>
										<th>Comisión</th>
										<th>Venta</th>
										<th style="padding-left: 10px;padding-right: 10px;text-align: center;">%</th>
										<th></th>
										<th></th>
									</tr>
						        </thead>
						        <tbody style='font-size: 0.85rem;'>
						        	@foreach($detalles as $key=>$detalle)
										<tr id="{{$detalle->item}}" data-toggle="tooltip" data-placement="right" data-html="true" title="<b>Check In :</b> {{$detalle->fecha_in}}<br><b>Check Out :</b> {{$detalle->fecha_out}}<br><b>Prestador :</b> {{$detalle->prestador}}"
											@if($detalle->reprice)
												style="background-color: orange;" {{-- Cambia el color a naranja si reprice es true --}}
											@endif
											>
											<th style="padding: 0.25rem 0.5rem;">{{$detalle->cantidad}}</th>
											<td style="padding: 0.25rem 1rem;">{{$detalle->producto}}</td>
											<th style="padding: 0.25rem 0.5rem;text-align: center;">{{$detalle->cod_confirmacion}}</th>
											<th style="padding: 0.25rem 0.5rem;">{{$detalle->proveedor}}</th>
											<td style="padding: 0.25rem 0.5rem;text-align: center;">{{$detalle->moneda_costo}}</td>
											<td style="padding: 0.25rem 0.5rem; text-align: center;">{{formatMoney($detalle->total_costo,$detalle->id_moneda_costo)}}</td>
											<td style="padding: 0.25rem 0.5rem; text-align: center;">{{formatMoney($detalle->monto_no_comisionable,$detalle->id_moneda_costo)}}</td>
											<td style="padding: 0.25rem 0.5rem; text-align: center;">{{$detalle->comision}}</td>
											<td style="padding: 0.25rem 0.5rem; text-align: center;">{{formatMoney($detalle->precio_venta,$proformas[0]->id_moneda_venta)}}</td>
											<th style="padding: 0.25rem 0.5rem;text-align: center;">{{number_format($detalle->porcentaje_renta_item,2,",",".")}} / {{number_format($detalle->renta_minima_producto,2,",",".")}}</th>
											<th style="padding-left: 0px;padding-right: 0px;text-align: center;width: 15px;">
												<i class="fa fa-fw fa-circle" title="Inactivo Gestur" style="color: {{$detalle->cumple_rentabilidad}};"></i>
											</th>  
											<td style="padding: 0.25rem 0.5rem;">
												@if($permisoEmpresaSolicitud == 1)	
													@if($detalle->origen == 'M')
														@if($proforma->estado_id != 5 && $proformas[0]->estado_id != 4 && $proformas[0]->estado_id != 83)
															@if($activador == 1)

																{{-- SI NO SE PAGO CON TC TIENE COMPORTAMIENTO NORMAL GES-751 --}}
																@if(!$detalle->pagado_con_tc)
																
																	<a id="{{$detalle->id}}" class="btn-update-request" onclick="updateDetalle({{$detalle->id}},{{$detalle->item}})" title="Editar Detalle" data="{{$detalle->item}}" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>
																	<a data="{{$detalle->id}}"  title="Eliminar Detalle" indice="{{$detalle->id}}" class="btn-cancelar-request" style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>

																	@if($detalle->pago_tc_detalle_proforma && $detalle->proveedor_genera_lc && $tipo_empresa->pagar_tc_detalle_proforma)
																		<a data-proforma_detalle_id="{{$detalle->id}}" class="btn-update-request" onclick="pagarTcDetalle({{$detalle->id}})" title="Pagar con TC" style="margin-left: 5px;"><i class="fa fa-credit-card" style="font-size: 18px;color:red;"></i></a>
																	@endif
																	
																@else
																	{{-- CUANDO YA CUENTA CON UN PAGO DE TC --}}
																	<a  class="btn-update-request" disabled title="Bloqueado por pago de TC" style="margin-left: 5px;color:green;"><i class="fa fa-credit-card" style="font-size: 18px;"></i></a>
																@endif
															
																
															@endif	
														@endif
													@else
														@if($detalle->id_producto == 16)	
															@if($proforma->estado_id != 5 && $proformas[0]->estado_id != 4 && $proformas[0]->estado_id != 83)
																@if($activador == 1)
																	<a id="{{$detalle->id}}" class="btn-update-request" onclick="updateDetalle({{$detalle->id}},{{$detalle->item}})" title="Editar Detalle" data="{{$detalle->item}}" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>
																@endif
															@endif	
														@endif		
														@if($detalle->id_producto == 38961 && $permisoAnularFee)	
															@if($proforma->estado_id != 5 && $proformas[0]->estado_id != 4 && $proformas[0]->estado_id != 83)
																<a data="{{$detalle->id}}"  title="Eliminar Detalle" indice="{{$detalle->id}}" class="btn-cancelar-request" style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>
															@endif	
														@endif	
													@endif		
												@endif
											<a id="mostrar_{{$detalle->item}}" class="glyphicon glyphicon-save-file" title="Desplegar Detalle" numero ="{{$detalle->id}}" data="{{$detalle->item}}" style="margin-left: 5px;"><i class="ft-download" style="font-size: 18px;"></i></a>
											<a id="ocultar_{{$detalle->item}}" numero ="{{$detalle->id}}" class="glyphicon glyphicon-open-file" data="{{$detalle->item}}"  title="Ocultar Detalle" style="margin-left: 5px;"><i class="ft-upload" style="font-size: 18px;"></i></a>
											</td>		
										</tr>
										<tr class='ocultado' id="oculto_{{$detalle->item}}" style="display: none">
										</tr>
									@endforeach			                	
								</tbody>
								<tfoot>	
									<?php $cantidad = $item + 1; ?>
									@if($proformas[0]->estado_id != 5 && $proformas[0]->estado_id != 4&& $proformas[0]->estado_id != 83)	
										@if($activador == 1 || $activar_detalle == true) 	
											@if($permisoEmpresaSolicitud == 1)
												<tr id="0" >
													<form id="itemProforma_0" method="get" style="margin-top: 2%;">
														<td style="padding-left: 0px;padding-right: 0px;padding-top: 5px;padding-bottom: 5px;">
															<input type="hidden" class = "Requerido form-control input-sm" id="proforma_id_0" name="proforma_id" value="{{$id}}" />
															<input type="hidden" class = "form-control form-control-sm" id="indice_0" name="item_0" value="{{$cantidad}}" style="width: 20px;padding-left: 5px;padding-right: 5px;" tabindex="1"/>
															<input type="text" class = "form-control form-control-sm numerico" name="cant_0" id="cant_0" onkeypress="return justNumbers(event);" maxlength="2"  value="1" style="width: 40px;">
														</td>
														<td colspan="2" style="padding-left: 0px;padding-right: 0px;padding-top: 5px;padding-bottom: 5px;">
															<div class="row">
																<div class="col-md-11" style=" padding-right: 0px;">
																	<select class="form-control form-control-sm select2 producto" name="producto_0" id="producto_0" style="width: 100%;" tabindex="2" >
																		<option value="0">Seleccione Producto</option>
																			@foreach($productos as $key=>$producto)
																				<option value="{{$producto['id']}}">{{$producto['denominacion']}}</option>
																			@endforeach	
																	</select>
																</div>	
																<div class="col-md-1" style="padding-left: 0px;">
																	<a data-toggle="modal" href="#requestModalTicket"  title="Tickets" data="0" linea="0" id="ticket_0" class="btn-ticket-base" style="margin-left: 5px; display:none"><i class="ft-navigation"></i></a>
																</div>	
															</div>	
														</td>
														<td ></td>
														<td style="padding-right: 0px;padding-left: 0px;"></td>
														<!--<td style="padding-right: 0px;padding-left: 10px;"></td>
														<td style="padding-right: 0px;display: none;"></td>-->
														<td></td>
														<td style="padding-left: 10px;padding-right: 0px;"></td>
														<td style="padding-right: 0px;"></td>
														<td style="padding-right: 0px;"></td>
														<td></td>
														<td></td>
														<td style="padding-left: 10px;padding-right: 20px;"></td>
														<td style="padding-left: 5px;padding-right: 5px;"></td> 
														<!--<td style="padding-left: 0px;padding-right: 0px;">-->
														</td>
													</form>    
												</tr>
											@endif 
										@endif  
									@endif     
						        </tfoot>
						    </table>    	
						</div>	
					</div>
				</div>
		    </div>
		</div>

		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Resumen</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body rounded-bottom rounded-lg">
		        	<div class= "row">
						<div class="col-md-2">
							<div class="form-group">
								<label style="font-size: small; color: red;font-weight: 700;">TOTAL_FACTURA</label>
								<input type="text" class = "Requerido form-control" name="total_factura" id="total_factura" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proformas[0]->total_proforma,$proformas[0]->id_moneda_venta)}}' disabled="disabled"/>
							</div>						
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label style="font-size: small;font-weight: 700;">TOTAL_BRUTO</label>
								<input type="text" class = "Requerido form-control" name="total_bruto" id="total_bruto" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->total_bruto_facturar,$proformas[0]->id_moneda_venta)}}' disabled="disabled"/>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label style="font-size: small;font-weight: 700;">TOTAL_NETO</label>		
								<input type="text" class = "Requerido form-control" name="total_neto" id="total_neto" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->total_neto_facturar,$proformas[0]->id_moneda_venta)}}' disabled="disabled" />
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label style="font-size: small;font-weight: 700;">EXENTAS</label>
								<input type="text" class = "Requerido form-control" name="total_bruto" id="total_exentas" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->total_exentas,$proformas[0]->id_moneda_venta)}}' disabled="disabled" />
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label style="font-size: small;font-weight: 700;">GRAVADAS (IVA incluido)</label>
								<input type="text" class = "Requerido form-control" name="total_iva" id="total_gravadas" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->total_gravadas,$proformas[0]->id_moneda_venta)}}' disabled="disabled" />
							</div>							
						</div>
						<div class="col-md-2">
								<div class="form-group">
									<label style="font-size: small;font-weight: 700;">IVA</label>
									<input type="text" class = "Requerido form-control" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->total_iva,$proformas[0]->id_moneda_venta)}}' disabled="disabled" />
								</div>
							</div>	
						</div>	
						<div class= "row">
							@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
								<div class="col-md-6">
							@else
								<div class="col-md-4">
							@endif
								<div class= "row">
									<div class="col-md-4">
										<label style="font-size: small;font-weight: 700;">COSTOS</label>
										<input type="text" class = "Requerido form-control" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->total_costos,$proformas[0]->id_moneda_venta)}}' disabled="disabled"/>									
									</div>	
									@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
										<div class="col-md-3">
									@else
										<div class="col-md-4">
									@endif
										<?php 
											if($proforma->comision_operador > 0){
												$comision = $proforma->comision_operador;
											}else{
												$comision = $proforma->total_comision;
											}
										?>
										<label style="font-size: small;font-weight: 700;">COMISIÓN</label>
										<input type="text" class = "Requerido form-control md" name="total_comision" id="total_comision" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($comision,$proformas[0]->id_moneda_venta)}}' disabled="disabled" />									
									</div>			
									@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
										<div class="col-md-3">
									@else
										<div class="col-md-4">
									@endif
										<label style="font-size: small;font-weight: 700;">N/COMISIONA</label>
										<input type="text" class = "Requerido form-control" name="total_comision" id="total_no_comisiona" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->total_no_comisiona,$proformas[0]->id_moneda_venta)}}' disabled="disabled"/>									
									</div>	
									@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')		
										<div class="col-md-2">
											<label style="font-size: small;font-weight: 700;">MARKUP</label>
											<input type="text" class = "Requerido form-control" name="markupF" id="markupF" style="font-weight: bold;font-size: 15px;padding-left: 8px;" value='{{formatMoney($proforma->markup,$proformas[0]->id_moneda_venta)}}' disabled="disabled" />									
										</div>	
									@endif		
								</div>	
							</div>	
							@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
								<div class="col-md-6">
							@else
								<div class="col-md-8">
							@endif
								<div class= "row">
									@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
										<div class="col-md-3">
									@else
										<div class="col-md-2">
									@endif
										<label style="font-size: small;font-weight: 700;">R. BRUTA</label>
										<input type="text" class = "Requerido form-control" name="senh" id="ganancia_venta" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->ganancia_venta,$proformas[0]->id_moneda_venta)}}' disabled="disabled" />									
									</div>	
									@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
										<div class="col-md-3">
									@else
										<div class="col-md-2">
									@endif
										<label style="font-size: small;font-weight: 700;">INCENTIVO</label>
										<input type="text" class = "Requerido form-control" name="incentivo_proforma" id="incentivo_proforma" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->incentivo_vendedor_agencia,$proformas[0]->id_moneda_venta)}}' disabled="disabled" />									
									</div>	
									@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa != 'D')
										<div class="col-md-2">
											<label style="font-size: small;font-weight: 700;">R. EXTRA</label>
											<input type="text" class = "Requerido form-control" name="renta_extra" id="renta_extra" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->renta_extra,$proformas[0]->id_moneda_venta)}}' disabled="disabled"/>
										</div>
										<div class="col-md-2">
											<label style="font-size: small;font-weight: 700;">R.NO COMISIONA</label>
											<input type="text" class = "Requerido form-control" name="renta_extra" id="renta_extra" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->renta_no_comisionable,$proformas[0]->id_moneda_venta)}}' disabled="disabled"/>
										</div>
									@endif
									@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
										<div class="col-md-3">
									@else
										<div class="col-md-2">
									@endif
										<label style="font-size: small;font-weight: 700;">R. NETA</label>
										<input type="text" class = "Requerido form-control" name="renta_neta" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->renta_neta,$proformas[0]->id_moneda_venta)}}' disabled="disabled" />	
									</div>	
									@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
										<div class="col-md-3">
									@else
										<div class="col-md-2">
									@endif
										<label style="font-size: small;font-weight: 700;">% RENTA</label>
										<input type="text" class = "Requerido form-control" name="total_costo" id="porcentaje_ganancia" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($proforma->porcentaje_ganancia,$proformas[0]->id_moneda_venta)}}' disabled="disabled"/>									
									</div>
								</div>	
							</div>	
						</div>	
					</div>		
				</div>
		    </div>
		</div>	</div>
</section>		


 <!-- ========================================
   					MODAL AGREGAR DETALLE
   		========================================  -->		

	  	<div class="modal fade text-left show" id="requestModalDetalle" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel17" aria-modal="true">
            <div class="modal-dialog modal-lg" role="document" style="margin-left: 10%;">
                <div class="modal-content" style="width: 130% !important;">
                    <div class="modal-header">
                        <!--<h4 class="modal-title" id="myModalLabel17">Agregar Detalle</h4>-->
                        <button type="button" class="close" data-dismiss="modal" tabindex = "-1" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <div class="form-body">
								<div class= "row" id="reservaNemo" style="display: none;">
			                    	<div class="card" style="border-radius: 14px;width: 98%;margin-left: 10px;margin-right: 10px;">
									    <div class="card-header" style="border-radius: 15px;">
									        <h4 class="card-title">Reservas Nemo</h4>
									        <div class="heading-elements">
									            <ul class="list-inline mb-0">
							                		<li><a data-action="collapse" id="btnReservaNemo"><i class="ft-plus"></i></a></li>
									            </ul>
									        </div>
									    </div>
									    <div class="card-content collapse" aria-expanded="true">
									    	<form id ="datosReservaNemo" class="form form-horizontal form-bordered">
												<!--<input type="hidden" id="currency" name="currency" class = "form-control form-control-sm" value = '{{$proformas[0]->id_moneda_venta}}'/>
												<input type="hidden" id="linea" name="linea" class = "form-control form-control-sm" />
												<input type="hidden" id ="productoAsisten" class = "Requerido form-control input-sm" name="producto" value="3"/>-->

										        <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">	
										        	 <div id="idReservaNemo"></div>
			                    				</div>
			                    			</form>	
		                    			</div>	
		                    		</div>	
		                    	</div>

								<div class= "row" id="reservaCangoroo" style="display: none;">
			                    	<div class="card" style="border-radius: 14px;width: 98%;margin-left: 10px;margin-right: 10px;">
									    <div class="card-header" style="border-radius: 15px;">
									        <h4 class="card-title">Reservas Cangoroo</h4>
									        <div class="heading-elements">
									            <ul class="list-inline mb-0">
							                		<li><a data-action="collapse" id="btnReservaCangoroo"><i class="ft-plus"></i></a></li>
									            </ul>
									        </div>
									    </div>
									    <div class="card-content collapse" aria-expanded="true">
									    	<form id ="datosReservaCangoroo" class="form form-horizontal form-bordered">
										        <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">	
										        	 <div id="idReservaCangoroo"></div>
			                    				</div>
			                    			</form>	
		                    			</div>	
		                    		</div>	
		                    	</div>

		                    	<div class= "row" id="especificosDatos">
			                    	<div class="card" style="border-radius: 14px;width: 98%;margin-left: 10px;margin-right: 10px;">
									    <div class="card-header" style="border-radius: 15px;">
									        <h4 class="card-title">Carga Manual</h4>
									        <div class="heading-elements">
									            <ul class="list-inline mb-0">
							                		<li><a data-action="collapse" id="btnEspecificos"><i class="ft-minus"></i></a></li>
									            </ul>
									        </div>
									    </div>
									    <div class="card-content collapse show" aria-expanded="true">
									    	<form id ="datosEspecificos" class="form form-horizontal form-bordered">
									    		<input type="hidden" id="productoTicket" name="productoTicket" class = "form-control form-control-sm"/>
												<input type="hidden" id="cliente" name="cliente" class = "form-control form-control-sm" value="{{$cliente_id}}" />
												<input type="hidden" id="currency" name="currency" class = "form-control form-control-sm" value = '{{$proformas[0]->id_moneda_venta}}'/>
												<input type="hidden" id="linea" name="linea" class = "form-control form-control-sm" />
												<input type="hidden" id ="productoAsisten" class = "Requerido form-control input-sm" name="producto" value="3"/>

										        <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">	
										        	 <div id="idEspecifico"></div>
			                    				</div>
			                    			</form>	
		                    			</div>	
		                    		</div>	
		                    	</div>
		                    	<form id ="datosDetalles" class="form form-horizontal form-bordered">
			                    	<div class= "row">
										<div class="card" style="border-radius: 14px;width: 100%;margin-left: 10px;margin-right: 10px;">
										    <div class="card-header" style="border-radius: 15px;">
										        <h4 class="card-title">Datos de: <spam id ="productoDescripcion"></spam></h4>
										        <div class="heading-elements">
										            <ul class="list-inline mb-0">
								                		<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
										            </ul>
										        </div>
										    </div>
										    <div class="card-content collapse show" aria-expanded="true">
										        <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;padding-right: 0px;">
													<div class="row">
														<div class="col-md-3" style=" padding-right: 0px;">
															<input type="hidden" id="currency_venta" name="currency_venta" class ="form-control form-control-sm" value ='{{$proformas[0]->id_moneda_venta}}' />
															<label>Código Confirmación</label>
															<input type="text" class = "form-control form-control-sm numerico" name="confirmacion_0" id="confirmacion_0" maxlength="30" tabindex="0"/>
														</div>	
														<div class="col-md-3" style=" padding-right: 0px;">
															<label>In/Out</label>
															<div id="periodos">
																<input type="text" class = "form-control form-control-sm periodos" id="periodo_0" tabindex = "0" name="periodo_0" value= "0"/>
															</div>													
														</div>	
														
														<div class="col-md-3" style=" padding-right: 0px;">
															<label>Proveedor</label>
															<select class="form-control select2 input-sm text-bold" name="proveedor_0" id="proveedor_0" style="width: 100%;">
																		<option value="">Seleccione Proveedor</option>
															</select>
														</div>	
														<div class="col-md-3">
															<div class="row">
																<div class="col-md-10" style="padding-right: 0px;">
																	<label>Prestador</label>
																	<!--<select class="form-control form-control-sm select2" name="prestador_0" id="prestador_0" data="0" tabindex = "0">
																   </select>-->
																   <select class="form-control select2 input-sm text-bold" name="prestador_0" id="prestador_0" style="width: 100%;">
																		<option value="">Seleccione Prestador</option>
																	</select>
																</div>   
														   		<div class="col-md-1" id="divPrestador" style="padding-left: 5px;padding-top: 10px;">
														   			<br>
																	@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1)
																		<a data-toggle="modal" title="Seleccionar Prestador" href="#requestModalPrestador" id="nuevo_prestador" data="0" class="btn-prestador-request"><i class="fa fa-plus"></i></a>
																	@else 
																		<a data-toggle="modal" title="Seleccionar Prestador" href="#requestModalNuevoPrestador" id="nuevo_prestador" data="0" class="btn-prestador-request"><i class="fa fa-plus"></i></a>
																	@endif
																</div>
															</div>	
														</div>	
				                    				</div>
													<div class="row" style="margin-top: 10px;">
														<div class="col-md-3" style=" padding-right: 0px;">
															<div class="row">
																<div class="col-md-3" style=" padding-right: 0px;">
																   <label >Moneda</label>
																	<select class="form-control form-control-sm select2" name="moneda_compra_0" id="moneda_compra_0" tabindex = "0">
																		<option value="0" selected="selected"></option>
																		@foreach($currencys as $key=>$currency)
																			<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
																		@endforeach	
																	</select>												
																</div>	
																<div class="col-md-9" style="padding-left: 7px;">
																	@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
																		<label >Costo</label>
																	@else
																		<label >Costo Exenta</label>
																	@endif
																	<input type="text" class = "form-control form-control-sm cost numeric" id="costo_0" onkeypress="return justNumbers(event);" name="costo_0" tabindex ="0"/>
																</div>		
															</div>	
														</div>	 
														<div class="col-md-3">	
															<div class="row">
																		@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1 || Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 4|| Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 8)
																	<div class="col-md-8" style="padding-right: 0px;">
																		@else
																	<div class="col-md-6" style="padding-right: 0px;">
																		@endif
																		<label>Costo Gravada</label>
																		<input type="text"  class = "form-control form-control-sm cost numeric" id="gravado_costo_0" onkeypress="return justNumbers(event);" name="gravado_costo_0"  tabindex = "0" disabled="disabled" value="0" />
																	</div>	
																		@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1 || Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 4|| Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 8|| Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 17 && Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia==58902)
																			<div class="col-md-4" style="padding-right: 0px;">
																				<label>Comisión</label>
																				<input type="text" id="comision_agencia_0" name="comision_agencia_0" onkeypress="return justNumbers(event);"  class = "form-control form-control-sm comAgencia" maxlength="2" tabindex = "0"/>
																				<input type="hidden" id="com_agencia_0" value="0" name="com_agencia_0" onkeypress="return justNumbers(event);" maxlength="2" class = "Requerido form-control input-sm"/>
																				<input type="hidden" id="tipo_0s" value="0" name="tipo_0" onkeypress="return justNumbers(event);" maxlength="2" class = "Requerido form-control input-sm"/>
																			</div>
																		@else
																			<div class="col-md-6" style="padding-right: 0px;">
																				<label>Comisión</label>
																				<input type="text" id="comision_operador_0" name="comision_operador_0" onkeypress="return justNumbers(event);"  class = "form-control form-control-sm comAgencia numeric" value="0" tabindex = "0"/>
																				<input type="hidden" id="com_operador_0" value="0" name="com_operador_0" onkeypress="return justNumbers(event);" maxlength="2" class = "Requerido form-control input-sm"/>
																				<input type="hidden" id="tipo_0s" value="0" name="tipo_0" onkeypress="return justNumbers(event);" maxlength="2" class = "Requerido form-control input-sm"/>
																			</div>	
																		@endif
															</div>
														</div>	
														@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
															<div class="col-md-3" >
																<div class="row">
																	<div class="col-md-12" style=" padding-right: 0px;">
																		<label>Tasas N/C</label>
																		<input type="text" class = "form-control form-control-sm numeric" id="tasas_0" onkeypress="return justNumbers(event);" name="tasas_0" tabindex = "0"/>
																	</div>
																	<div class="col-md-6" style="display:none">
																		<label>Base Comisionable</label>
																		<input type="text" class = "form-control form-control-sm numeric" id="base_comisionable_0" onkeypress="return justNumbers(event);" name="base_comisionable_0" tabindex = "0"/>
																	</div>
																</div>	
															</div>

															<div class="col-md-3">
																<label>Venta</label>
																<input type="text" style="width: 85%;" class = "form-control form-control-sm sale numeric" id="venta_0" value="0" onkeypress="return justNumbers(event);" name="venta_0" tabindex = "0"/>
															</div>	
													</div>
					                    			<div class="row" style="margin-top:10px;">
														@else
														<div class="col-md-3" style="padding-right: 0px;">
															<label>Venta</label>
															<input type="text" style="width: 100%;" class = "form-control form-control-sm sale numeric" id="venta_0" value="0" onkeypress="return justNumbers(event);" name="venta_0" tabindex = "0"/>

															<input type="hidden" class = "form-control form-control-sm numeric" id="tasas_0" name="tasas_0" value="0"/>
														</div>	
														@endif
														
					                    				@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
															<div class="col-md-3" style=" padding-right: 0px;">
																<label>Markup</label>
																<input type="text" id="markup_0" name="markup_0" onkeypress="return justNumbers(event);"  onchange="return justNumbers(event);" class = "form-control form-control-sm markup" tabindex = "0"/>
															</div>	
														@else
																<input type="hidden" id="markup_0" name="markup_0" onkeypress="return justNumbers(event);"  onchange="return justNumbers(event);" class = "form-control form-control-sm markup" value="0" tabindex = "0"/>

														@endif
														@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
															<div class="col-md-3" style=" padding-right: 0px;">
														@else
															<div class="col-md-3" >
														@endif		
															<label>Descripción de Factura</label>
							                    			<input type="text" name="descrip_factura_0" id="descrip_factura_0" class = "form-control form-control-sm" tabindex = "0"/>
							                    		</div>	
							                    		<div class="col-md-3" style=" padding-right: 0px;">
															@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
																<div class="row">
																	<div class="col-md-6" style=" padding-right: 0px;">
															@else
																<div class="col-md-12" style=" padding-left: 0px;padding-right: 0px;">
															@endif			
																	<label>Pago Proveedor</label>
									                    			<input type="text" name="vencimientos" id="vencimientos" class = "form-control form-control-sm" tabindex = "0"/>
									                    		</div>	
									                    	@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')
									                    		<div class="col-md-6">
									                    	@else
									                    	</div>
									                    	<div class="col-md-3" style=" padding-right: 0px;">
									                    	@endif
									                    		
																	<label>Fecha Gasto</label>
																	<input type="text" class = "form-control form-control-sm" id="fecha_gasto_0" name="fecha_gasto_0" tabindex = "0"/>
																</div>	
						                    				@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'D')		
								                    			</div>	
								                    		</div>	
								                    	@endif	
														<div class="col-md-3" style="padding-right: 50px;">
															<label>Monto Gasto</label>
															<input type="text" class = "form-control form-control-sm numeric" id="monto_gasto_0" onkeypress="return justNumbers(event);" name="monto_gasto_0" value="0" tabindex = "0"/>
														</div>		
					                    			</div>	
													<input type="hidden" class = "form-control form-control-sm" name="cantidad_0" id="cantidad_0" style="width: 40px;">
													<input type="hidden" id="item" name="item" class ="form-control form-control-sm" value ='1' />
													<input type="hidden" id="grupo" name="grupo" class ="form-control form-control-sm" value ='0' />	
													<input type="hidden" id="productoId_0" name="productoId_0" class ="form-control form-control-sm"/>
													<input type="hidden" id="tarjeta_id_0" name="tarjeta_id_0" class ="form-control form-control-sm"/>
													<input type="hidden" id="cod_autorizacion_0" name="cod_autorizacion_0" class = "form-control form-control-sm"/>
													<input type="hidden" id="horaIn_0" name="horaIn_0" class = "form-control form-control-sm" />
													<input type="hidden" id="horaOut_0" name="horaOut_0" class = "form-control form-control-sm"/>
													<input type="hidden" id="vueloIn_0" name="vueloIn_0" class = "form-control form-control-sm"/>
													<input type="hidden" id="vueloOut_0" name="vueloOut_0" class = "form-control form-control-sm"/>
													<input type="hidden" id="idTipoTraslado_0" name="id_tipo_traslado_0" class = "form-control form-control-sm"/>
													<input type="hidden" id="fee_0" name="fee_0" class = "form-control form-control-sm"/>
													<input type="hidden" id="exentos_0" name="exentos_0" class = "form-control form-control-sm" />
													<input type="hidden" id="gravada_0" name="gravada_0" class = "Requerido form-control input-sm"/>
													<input type="hidden" id="proforma_id" name="proforma_id"  value="{{$proforma->id}}" class = "Requerido form-control input-sm"/>
													<input type="hidden" id="detalle_proforma_id" name="detalle_proforma_id"  class = "Requerido form-control input-sm"/>
													<input type="hidden" class = "Requerido form-control input-sm" id="tarifa_asistencia_0" name="tarifa_asistencia_0" />
													<input type="hidden" class = "Requerido form-control input-sm" id="cantidad_dias_0" name="cantidad_dias_0" />
													<input type="hidden" class = "Requerido form-control input-sm" id="cantidad_personas_0" name="cantidad_personas_0" />
													<input type="hidden" class = "Requerido form-control input-sm" id="tickets_0" name="tickets" />
													<input type="hidden" id ="reserva_nemo_id" class = "Requerido form-control input-sm" name="reserva_nemo_id" value=""/>
													<input type="hidden" id ="grupo_reserva_cangoroo_id" class = "Requerido form-control input-sm" name="grupo_reserva_cangoroo_id" value=""/>
													<input type="hidden" id ="reserva_cangoroo_id" class = "Requerido form-control input-sm" name="reserva_cangoroo_id" value=""/>
													<input type="hidden" id ="tipo_reserva_cangoroo" class = "Requerido form-control input-sm" name="tipo_reserva_cangoroo" value=""/>
													<input type="hidden" id="proveedor_id_0" name="proveedor_id_0" class ="form-control form-control-sm" />
													<input type="hidden" id="prestador_id_0" name="prestador_id_0" class ="form-control form-control-sm"/>

												</div>
			                    			</div>	
			                    		</div>	
			                    	</div>	
			                    </form>    
		                    </div> 	
		                </form>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" id="guardar" onclick="guardarProformaDetalle()"  class="btn btn-outline-primary">Guardar</button>
                        <button type="button" style="display: none" id="editar" class="btn btn-outline-primary">Guardar</button>
                    </div>
                </div>
            </div>
        </div>

	<!-- ========================================
   					MODAL PAGO TC DETALLE PROFORMA
   	========================================  -->	
	   <div id="requestPagoTC" class="modal fade" role="dialog">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Pagar Reserva <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_c" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">
					  	<form id="frmPagoTC" method="get">
							<div class="row" >
								<div class="col-md-12">
									<div class="form-group">
										<label>Tarjeta Utilizada</label>						 
										<select class="form-control select2" id="tarjeta_utilizada" style="width: 100%;">
											@foreach($tarjetas as $tarjeta )
												<option value="{{$tarjeta->id_banco_detalle}}">{{$tarjeta->banco_detalle->numero_cuenta.' - '.$tarjeta->banco_detalle->currency->currency_code}} ({{$tarjeta->banco_detalle->banco_cabecera->nombre}})</option>
											@endforeach
											
										</select>
									</div>   
								</div>
							</div>
							<div class="row" >
								<div class="col-md-12">
									<div class="form-group">
										<label>Nro.de Voucher/Comprobante.</label>						 
										<input type="text" class = "form-control" name="comprobante" id="pago_tc_comprobante" placeholder="" value="" />								
									</div>   
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="adjunto_modal_tc">Adjunto OP</label>
										<input type="file" class="form-control-file" name="adjunto_tc" id="adjunto_modal_tc" accept=".pdf, image/*">
										<small>Solo se acepta imagenes y pdf</small>
									  </div>
								</div> 
							</div>
							<input type="hidden" id="pago_tc_id_proforma_detalle" name="id_proforma_detalle">
						</form>
				  </div> 
				  <div class="modal-footer">
				  	<button type="button" id="btn_pago_tc" class="btn btn-info" > <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true" id="spinner_btn_modal_tc" style="display: none;"></span>&nbsp; Pagar</button>
					<button type="button" id="btn_cancelar_tc" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>

  <!-- ========================================
   					MODAL EMERGENCIA
   		========================================  -->		
    <!-- /.content -->
	<div id="myModales" class="modal fade" role="dialog">
	  	<div class="modal-dialog" style="width: 50%;margin-top: 13%;">
		<!-- Modal content-->
			<div class="modal-content">
				<input type="hidden" class = "Requerido form-control input-sm"  name="proforma_ids" value="{{$id}}" />
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">EMERGENCIA</h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
					<div class="row">	
						<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 25px;">
							<div class="form-group">
								<label>Tipo </label>
								<select class="form-control select2" name="emergencia_tipo" id="emergencia_tipo" style="width: 100%;" >
									<option value="">Seleccione Tipo</option>
									<option value="1">Postergación</option>
									<option value="2">Cancelación</option>
								</select>
							</div>
						</div> 
					</div>	
					<br>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 25px;">
							<div class="form-group">
								<label>Nro de Proforma</label>
								<input type="text" disabled="disabled" class = "Requerido form-control" maxlength="100" name="proforma_emergencia" id="proforma_emergencia" style="font-weight: bold;font-size: 15px;"/>
							</div>
						</div>
					</div> 	

					<br>
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5" style="padding-left: 25px;">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 25px;">
							<button type="button" id="btnGuardarEmergencia" class="btn btn-success btn-lg" style="width: 100px; background-color:#ed00ef;margin-left: 7%;">Guardar</button>
							<button type="button" id="btnCancelarVSTour" class="btn btn-danger btn-lg" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
						</div>		
					</div>	
				</div>
		  	</div>
		 </div>	
	</div>


 <!-- ========================================
   					MODAL PRESTADOR
   		========================================  -->		
    <!-- /.content -->
	<div id="requestModalNuevoPrestador" class="modal fade" role="dialog">
				<div class="modal-body" style="padding-top: 0px;">
					<form id="frmNuevoPrestador" method="post" action="">
						<div class="modal-dialog">
							<input type="hidden" class = "Requerido form-control input-sm" id="lineaTraslado" name="lineaTraslado"/>

						<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<h2 class="modal-title titlepage" style="font-size: x-large;">Ingresar Prestador</h2>
										<small style="color:red; font-weight: bold;">Campos obligatorios (*)</small>
									</div>
									<div class="modal-body">
										<div id="contenido">
										<div class="row"  style="margin-bottom: 1px;">
											<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;">
												<div class="form-group">
													<label>Nro Documento (*)</label>
													<input type="text" required class="Requerido form-control input-sm" name="documento_prestador" id="documento_prestador" />
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-2">
												<div class="form-group">
													<label>DV</label>
													<input type="text" class="Requerido form-control input-sm" name="dv_prestador" id="dv_prestador" disabled="disabled" />
												</div>
											</div>
											</div>	
											<div class="form-group">
												<label>Tipo Factura</label>
												<select class="form-control select2" required name="tipo_identidad_prestador" id="tipo_identidad_prestador" style="width: 100%;">
													@foreach($tipoIdentidads as $key=>$tipoIdentidad)
														<option value="{{$tipoIdentidad->id}}">{{$tipoIdentidad->denominacion}}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label>Nombre (*)</label>
												<input type="text" required class="Requerido form-control input-sm" name="nombre_prestador" id="nombre_prestador" />
											</div>
											
											<div class="form-group">
												<label>Apellido (*)</label>
												<input type="text" required class="Requerido form-control input-sm" name="apellido_prestador" id="apellido_prestador" />
											</div>
											
											<div class="form-group">
												<label>Correo Electrónico</label>
												<input type="email" class="Requerido form-control input-sm" name="email_prestador" id="email_prestador" />
											</div>
											 <div class="form-group">
												<label>Dirección (*)</label>						 
												<input type="text" required class = "Requerido form-control input-sm" name="direccion_prestador" id="direccion_prestador"/>
											</div>
											<div class="form-group">
												<label>Pais <span></span>(*)</label>						 
												<select class="form-control select2" required name="pais_prestador" id="pais_prestador" style="width: 100%;">
													@foreach($paises as $key=>$pais)
														<option value="{{$pais->cod_pais}}">{{$pais->name_es}}</option>
													@endforeach
												</select>
											</div>
											</div>
									</div> 
								<input type="hidden" class = "Requerido form-control" name="marcador1" valor="1"/>

								<div class="modal-footer">
									<button type="button" id="btnAceptarPrestador" class="btn btn-success" style="width: 90px; background-color: #e2076a;"><span id="spinner_modal_prestador" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="display: none;"></span> Aceptar</button>
									<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
								</div>
							</div>
						</div>					
					</form>	
		 </div>	
	</div>


 <!-- ========================================
   					MODAL PRESTADOR
   		========================================  -->		
    <!-- /.content -->
	<div id="requestModalPrestador" class="modal fade" role="dialog">
	  	 <div class="modal-dialog modal-lg" role="document" style="margin-left: 10%;">
            <div class="modal-content" style="width: 130% !important;">

				<input type="hidden" class = "Requerido form-control input-sm"  name="proforma_id" value="{{$id}}" />
				<input type="hidden" class = "Requerido form-control input-sm" id="lineaTraslado" name="lineaTraslado"/>

				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Prestador</h2>
					  <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button> -->

				</div>
				<div class="modal-body" style="padding-top: 0px;">
					<form id="frmPrestador" method="post" action="">
						<div class="row">	
							<div class="col-md-5">
								<div class="form-group">
									<label>Destino</label>
										<select data-placeholder="Destinos" class="form-control select2" id="DestinoId" name="destino" style="width: 100%;" >
					 								@foreach($arrayDestino as $key=>$destinoProforma)
														<option value="{{$destinoProforma['id']}}">{{$destinoProforma['value']}}</option>
													@endforeach												
										</select> 										
								</div>
							</div> 
							<div class="col-md-5">
								<div class="form-group">
									<label>Nombre Prestador</label>
									<input type="text" class = "Requerido form-control" name="nombre" id="nombrePrestador" style="font-weight: bold;font-size: 15px;" />
								</div>	
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<br>
									<button type="button" id="btnBuscarPrestador" class="btn btn-danger">Buscar</button>
								</div>	
							</div>
						</div>
					</form>	
					<br>
					<br>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">		
							<table id="listadoPrestador" class="table">
							    <thead>
									<tr>
										<th>Nombre</th>
										<th>Dirección</th>
										<th>Teléfono</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>	
					</div>				
					<br>
				</div>
		  	</div>
		 </div>	
	</div>


<!-- ========================================
   					MODAL NEMO
   		========================================  -->		

	  <div class="modal fade text-left" id="requestModalNemo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel16">Reservas Nemo</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
					<form id="frmNemo" method="GET" style="margin-top: 2%;">
						<table id="rowNemoDetalles" class="table">
							<thead>
								<tr>
									<th style="width: 5px;padding-left: 5px;padding-right: 5px;"></th>
									<th style="width: 1%;">Codigo</th>
									<th style="width: 3%;">Producto</th>
									<th style="width: 1%;">Fecha</th>
									<th style="width: 3%;">Prestador</th>
									<th style="width: 1%;">In/Out</th>
									<th style="width: 3%;">Pasajero</th>
									<th style="width: 3%;">Proveedor</th>
									<!--<th>Destino</th>-->
									<th style="width: 1%;">Monto</th>
									<th style="width: 1%;">Usuario</th>
								</tr>
							</thead>
							<tbody style='font-size: 0.85rem;'>
									
							</tbody>		
						</table>	
					</form>	
					<div class="row">
						<div class= "col-md-10">
						</div>
						<div class= "col-md-1">
							<button type="button" id="btnAceptarNemo" class="btn btn-danger" style="width: 90px; background-color: #e2076a;" >Aceptar</button>
						</div>
						<div class= "col-md-1" style="padding-left: 0px;">
							<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;margin-left: 5px;" data-dismiss="modal">Cerrar</button>						
						</div>
					</div>

				</div>
		  	</div>
		 </div>	
	</div>



<!-- ========================================
   					MODAL VERIFICAR PROFORMA
   	========================================  -->		

	<div class="modal fade" id="modalVerificarProforma"  role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" >
	    <div class="modal-content" style="margin-top: 25%;">
	      <div class="modal-body">
	       <div class="row">
			<div class="col-md-12" style="padding-left: 25px;">
				<ul id="msjListaAutorizacion" class="swal-text" style="margin-top: 15px;margin-bottom: 15px;">
				</ul>
				<h4 class="swal-text" style="font-size: x-large;margin-bottom: 30px;"><b>¿Desea solicitar autorización al supervisor?</b></h4>
			</div>
	       	<div class="col-md-6 text-center">
			 <button type="button" class="swal-button swal-button--confirm"  id="btnPedirAutorizacion">SI</button>
	       	</div>

	       	<div class="col-md-6 text-center">
			 <button type="button" class="swal-button swal-button--cancel btn-warning"  data-dismiss="modal" aria-label="Close" id="">NO</button>
	       	</div>

	       </div>
	      </div>
	    </div>
	  </div>
	</div>
	<input type="hidden" id="markupProformaHidden" value="{{$proforma->markup}}">

 <!-- ========================================
   			MODAL TIME LINE
   	========================================  -->	
<div class="modal modalProforma fade" id="modalTimeLineProforma" role="dialog" aria-labelledby="modalTimeLineProforma">
      <div class="modal-dialog  modal-dialogProforma" role="document">
        <div class="modal-content">
          <div class="modal-header">
	            <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
	            	<span aria-hidden="true">&times;</span>
	            </button>
	            <h4 class="modal-title" id=""><i class="fa fa-fw fa-calendar-check-o"></i>   TIMELINE PROFORMA N: {{$id}}</h4>
          </div>

          <div class="modal-body modal-bodyProforma" style="height: 500px;">
				<div class="content-fluid">
					<section class="timeline-center timeline-wrapper">
						<ul class="timeline">

							<?php $cont =0; ?>
						    @foreach($timeLineProforma as $estados)
						    <?php $cont++; ?>

						    <li class="time-label">
						        <span class="" style="background-color:#dd4b39;color:white">
						            {{-- {{$estados->fecha}} --}}
						            {{$estados->denominacion}}
						        </span>
						    </li>

						    <li>
						        <i class="{{$estados->ico_estado}}"></i>
						        <div class="timeline-item">
						            <span class="time"><i class="fa fa-clock-o"></i> {{$estados->fecha}} {{$estados->hora}}</span>
						            <h3 class="timeline-header">
						                <a href="#">
						                
						                    <img src="{{asset('personasLogo/'.$estados->logo)}}"  onerror="this.src='{{asset('images/agencias/foto-perfi.jpg')}}';" style="border-radius: 50%; width: 40px;height: 40px;" alt="..." class="" />
						                 {{$estados->usuario_n}} 
						                </a>
						                 <span>{{$estados->denominacion}}</span>
						            </h3>
						            @if($estados->comentario != null)
						            <div class="timeline-footer">
							                <a class="btn btn-primary btn-md" onclick="getCommetTimeLine('<?php echo $cont;?>')" class="comentarioTimeLine<?php echo $cont;?>">COMENTARIOS</a>
							                <div class="comentarioBoxTimeLine<?php echo $cont;?> commentNone">
							                	<p>
							                		{{$estados->comentario}}
							                	</p>
							                </div>
							            </div>
							          @endif  
						        </div>
						    </li>
						 @endforeach
					</ul>
					</section>
				</div>
          </div>
        </div>
      </div>
    </div>

	<!-- ========================================
   					MODAL VERIFICAR RENTA
   	========================================  -->		

	   <div class="modal fade confirmacion" id="modalVerificarRenta"  role="dialog" aria-labelledby="myModalLabel" style="padding-right: 17px;margin-left: 35%;margin-top: 12%;">
	  <div class="modal-dialog" >
	    <div class="modal-content">
	      <div class="modal-body">
	       <div class="row">
			<div class="col-md-12 text-center">
					<h4 class="swal-text" style="font-size: x-large;"><b>GESTUR</b></h4><br>
					<h4 class="swal-text" style="margin-bottom: 30px;margin-top: 15px;font-size: 15px;">¿Está seguro de que desea solicitar la verificación de la proforma?</h4>
			</div>
				<div class="col-md-5 text-center"></div>
				<div class="col-md-2 text-center">
					<button type="button" class="swal-button swal-button--cancel btn-warning" data-dismiss="modal" aria-label="Close" id="">No</button>
				</div>
				<div class="col-md-5 text-center">
					<button type="button" class="swal-button swal-button--confirm" id="btnVerificacionRenta">Si, Solicitar</button>
				</div>

	       </div>
	      </div>
	    </div>
	  </div>
	</div>

     <!-- ========================================
   					MODAL POST VENTA
   	========================================  -->		

	<div class="modal fade" id="modalPostVenta"  role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="" style="font-weight: 800;">Reconfirmacion<i style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
			<small style="font-weight: 200;">(*) Obligatorios</small>
			<br>
			<span class="msj_err" style="color:#DB6042;"></span>
			<span class="msj_success" style="color:#489946;"></span>
	      </div>
	      <div class="modal-body">
				<form id="valoracion_form" method="post" > 

									<div class="row">
										@if (!empty($usuario_post_venta))
											<div class="col-md-6" style="height: 100px;">
												<div class="input-group-sm">
													<label>Usuario Reconfirmacion</label>
													<input type="text" style="font-weight:; font-size: 15px;" value="{{ $usuario_post_venta[0]->nombre }}" class="form-control pull-right input-sm" disabled="disabled">
												</div>
											</div>
										@endif
										<div class="col-md-12">
											<div class="form-group">
												<label style="font-weight: 600; font-size: 18px;">Comentario (*)</label>
												<textarea class="form-control" rows="5" name="comentario_calificacion" id="comentarioPostVenta" {{ $proforma->comentario_calificacion ? 'readonly' : '' }}>{{ $proforma->comentario_calificacion }}</textarea>
											</div>
										</div>
											
					            </div>	
					     <input type="hidden" id="id_proforma" name="id_proforma" value="{{$id}}">       
	  		   </form>	
	      </div>
	      <div class="modal-footer">
	      	  {{-- <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><b>Cerrar</b></button> --}}
			<button type="button" class="btn btn-success btn-lg"  id="btnCalificarPostVenta"><b>Guardar</b></button>
	      </div>
	    </div>
	  </div>
	</div>

<!-- ========================================
   					MODAL LISTA COMENTARIOS
   				========================================  	
		<div id="myModal" class="modal fade" role="dialog">-->    
        <div class="modal fade text-left" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">		  
            <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content" style="width: 570px;">
		      <div class="modal-header">
                <h4 class="modal-title">Comentarios</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
        	<!-- Construct the box with style you want. Here we are using box-danger -->
            <!-- Then add the class direct-chat and choose the direct-chat-* contexual class -->
            <!-- The contextual class should match the box, so we are using direct-chat-danger -->
              <div class="box box-danger direct-chat direct-chat-danger">
                <div class="box-header with-border">
                  <div class="box-tools pull-right">
                 
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages" id="chatDirect" style="height:355px; overflow: scroll;overflow-x: hidden;">
        		@php
        		$comentId = 0; 
        		$lado = 'l';	

        		 function mensajeLeft($mensaje,$nombre,$fechaHora,$id,$logo=''){
        			$img = ($logo != '') ?  $logo : "factour.png"; 

                    $imagen = asset('personasLogo')."/".$img;

                  	$rsp = "<!-- Mensaje por defecto a la izquierda -->
                    <div class='direct-chat-msg' data='{$id}'>
                        <div class='row'>
                            <div class='col-md-2'><span class='direct-chat-name pull-left'><b>{$nombre}</b></span></div>
                            <div class='col-md-7'></div>
                            <div class='col-md-3' style='padding-left: 2px;padding-right: 10px;'><span class='direct-chat-timestamp pull-right' style='font-size: smaller;'>{$fechaHora}</span></div>    
                        </div>
                        <div class='row'>
                            <div class='col-md-2'><img class='direct-chat-img' src='".$imagen."' style='width:50px' alt='message user image'></div>
                            <div class='col-md-10' style='word-break: break-all; padding-left: 0px;'><div class='chat-content'>
                            {$mensaje}</div></div>
                        </div>
                     </div> <br>";

                    return $rsp;

        		  }	
       		  
                  function mensajeRight($mensaje,$nombre,$fechaHora,$id,$logo=''){
        			$img = ($logo != '') ?  $logo : "factour.png"; 
                    $imagen = asset('personasLogo')."/".$img;


                    $rsp = "<!-- Mensaje por defecto a la izquierda -->
                    <div class='direct-chat-msg right' data='{$id}'>
                        <div class='row'>
                            <div class='col-md-3' style='padding-left: 2px;padding-right: 10px;'><span class='direct-chat-name pull-right' style='font-size: smaller;'>{$fechaHora}</span></div>
                            <div class='col-md-7'></div>
                            <div class='col-md-2'><span class='direct-chat-timestamp pull-left'><b>{$nombre}</b></span></div>    
                        </div>
                        <div class='row'>
                            <div class='col-md-10'><div class='chat-content-left' style='word-break: break-all; padding-left: 0px;'>
                            {$mensaje}</div></div>
                            <div class='col-md-2'><img class='direct-chat-img' src='".$imagen."' style='width:50px' alt='message user image'></div>
                        </div>
                     </div><br> ";

                  return $rsp;

                  }
        		@endphp	

        		@if(count($comentario) > 0)


                  @foreach($comentario as $coment)
                  @php

                  $mensaje = $coment->comentario;
                  $nombre = $coment->persona['nombre'];
                  $fecha = $coment->fecha_hora;
                  $logo  =$coment->persona['logo'];

                   if($fecha != '' && $fecha != NULL){
        		 	
        		 	$f = explode(' ', $fecha);

        		 	$date = explode('-', $f[0]);

        		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
        		 	$fecha .= ' '.$f[1];
        		 } else {
        		 	$fecha = 'N/A';
        		 }
         		  $idPersona = $coment->id_usuario;

                  if($comentId != 0 && $comentId != $coment->id_usuario){

                  	if($lado == 'l'){

                  		$lado = 'r';
                  		echo mensajeRight($mensaje,$nombre,$fecha, $idPersona,$logo);

                  	} else {

                  		$lado = 'l';
                  		echo mensajeLeft($mensaje,$nombre,$fecha, $idPersona,$logo);
                  	}


                  }	else if($comentId == $coment->id_usuario){
                  	
                  	if($lado == 'l'){

                  		$lado = 'l';
                  		echo mensajeLeft($mensaje,$nombre,$fecha, $idPersona,$logo);

                  	} else {

                  		$lado = 'r';
                  		echo mensajeRight($mensaje,$nombre,$fecha, $idPersona,$logo);
                  	}

                  } else {

                  	$lado = 'l';
                  	echo mensajeLeft($mensaje,$nombre,$fecha, $idPersona,$logo);
                  }
                    $comentId = $coment->id_usuario;

                  @endphp
                  @endforeach
        		@endif
           
                </div><!-- /.box-body -->
                <div class="box-footer">
                  <div class="input-group">
                    <input type="text" name="message" placeholder="Escribe tu comentario ..." class="form-control" id="mensajeChat" maxlength="200">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-danger btn-flat" id="btnChat">Enviar</button>
                    </span>

                  </div>
                  <span id="errorChat" style="color:red;"><h6></h6></span>
                </div><!-- /.box-footer-->
              </div><!--/.direct-chat -->

		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
		      </div>
		    </div>

		  </div>
			</div>
		</div>


 <!-- ========================================
   					MODAL ADJUNTOS
   		========================================  -->	
	<div class="modal fade text-left" id="modalAdjunto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content" style="width: 80%">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel16">Adjuntos</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
	                <!-- Post -->
	                <div class="post">
	                  <!-- /.user-block -->
	                  		<div class ='row' id='output'>
	                  			@foreach($adjunto as $key=>$adjunt)
	                  				<?php
	                  					$tamanho = $adjunt->nombre_adjunto;
	                  					$tamanhoInicial = explode('.', $tamanho);
										$ruta = 'adjuntoDetalle/'.$adjunt->nombre_adjunto;
										$eliminar = $adjunt->id.", '".$tamanho."'";
									?>
									<div id = "{{$adjunt->id}}">
										<div class="col-md-2" style="margin-bottom: 20px;">
											@if(isset($tamanhoInicial[1]))
												@if($tamanhoInicial[1] == 'pdf')
													<a href="{{asset($ruta)}}" target="_blank"><img class="img-responsive" style="width:120px;" src="{{asset('images/file.png')}}" alt="Photo"></a>
												@else	
													<a href="{{asset($ruta)}}" target="_blank"><img class="img-responsive" style="width:120px; height: 120px;" src="{{asset($ruta)}}" alt="Photo"></a>
												@endif	
											@else
												<a href="{{asset($ruta)}}" target="_blank"><img class="img-responsive" style="width:120px; height: 120px;" src="{{asset($ruta)}}" alt="Photo"></a>
											@endif
										</div>
										{{-- NO MOSTRAR SI ES ANULADO O FACTURADO --}}
											@if($proforma->estado_id != 5 && $proformas[0]->estado_id != 4)
		                                <div class="col-md-1">
		                                    <button type='button' id='btn-imagen' onclick="eliminarImagen({{$eliminar}})"><i class="ft-x-circle" style="font-size: 18px;"></i></button>
		                                </div>
											@endif
	                                </div>  
	                  			@endforeach
			                </div>
			                <br>
							{{-- NO MOSTRAR SI ES ANULADO O FACTURADO --}}
											@if($proforma->estado_id != 5 && $proformas[0]->estado_id != 4)
			                <div class="row"> 
				                <form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{ url('uploadAdjunto') }}" autocomplete="off">
				                	<input type="hidden" class = "Requerido form-control input-sm" name="proforma_id" value="{{$id}}" readonly="readonly"/>
		                            <div class="form-group">
		                              <div class="row">
		                                <div>
		                                  
		                                </div>
		                                <div class="col-md-12">
			                                  <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
			                                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
			                                  <input type="file" class="form-control" name="image[]" id="image" style="margin-left: 3%; width: 96%" multiple/>  <h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: PNG, JPG, PDF</b></h4>
		                                  	<div id="validation-errors"></div>

		                                  </div> 
		                                </div>  
		                              </div>  
		                          </form>
		                    </div>  
							@endif  
	                	</div>
	                <!-- /.post -->
					</div>
					<div class="modal-footer">
	                    <div class="row">
							<div class="col-md-6">
								<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px;background-color: #969dac;margin-left: 25px;" data-dismiss="modal">Aceptar</button>
							</div>	
		                    <div class="col-md-6">
								<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>	
				</div>	
			</div>	
		</div>	
	</div>	
<!-- ========================================
   					MODAL PASAJEROS
   		========================================  -->		
    <div id="requestModalPasajero" class="modal fade" role="dialog">
		<form id="frmPasajero" method="post" action="" style="margin-top: 12%;">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Ingresar Pasajero</h2>
					</div>
				  	<div class="modal-body">
				  		<div id="contenido">
						  <div class="row"  style="margin-bottom: 1px;">
	    					<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;">
		    					<div class="form-group">
		    						<label>Nro Documento</label>
		    						<input type="text" class="Requerido form-control input-sm" name="documento" id="documento" />
		    					</div>
		    				</div>
	    					<div class="col-xs-12 col-sm-6 col-md-2">
		    					<div class="form-group">
		    						<label>DV</label>
		    						<input type="text" class="Requerido form-control input-sm" name="dv" id="dv" disabled="disabled" />
		    					</div>
		    				</div>
							</div>	
							<div class="form-group">
								<label>Tipo Factura</label>
								<select class="form-control select2" name="tipo_identidad" id="tipo_identidad" style="width: 100%;">
									@foreach($tipoIdentidads as $key=>$tipoIdentidad)
										<option value="{{$tipoIdentidad->id}}">{{$tipoIdentidad->denominacion}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Nombre</label>
								<input type="text" required class="Requerido form-control input-sm" name="nombre" id="nombre" />
							</div>
							
							<div class="form-group">
								<label>Apellido</label>
								<input type="text" required class="Requerido form-control input-sm" name="apellido" id="apellido" />
							</div>
							
							<div class="form-group">
								<label>Correo Electrónico</label>
								<input type="text" required class="Requerido form-control input-sm" name="email" id="email" />
							</div>
							</div>
					</div> 
				  <input type="hidden" class = "Requerido form-control" name="marcador1" valor="1"/>

				  <div class="modal-footer">
					<button type="button" id="btnAceptarPasajero" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Aceptar</button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	 </form>
	</div>
	

     <!-- ========================================
   					MODAL CLIENTES ADICIONALES
   	========================================  -->		

	<!--<div class="modal fade" id="modalClienteAdjunto" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-lg" role="document" style="margin-left: 28%;width: 40%;margin-top: 7%;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Clientes adicionales</h4>
			<br>
	      </div>
	      <div class="modal-body">
		  		<div id = "clientesAdicionales" style="margin-bottom: 10px;">
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-11" style="padding-left: 15px;padding-right: 0px;">
						<select class="form-control select2" name="cliente_id_aux" id="cliente_id_aux" style="width: 100%" aria-describedby="button-addon2" >
						</select>                                        
					</div>
					<div class="col-xs-12 col-sm-6 col-md-1" style="padding-left: 15px;">
						<button type="button" id="agregarClienteAux" class="btn btn-info"><i class="fa fa-check" aria-hidden="true"></i></button>
					</div>
				</div>
	      </div>
	      <div class="modal-footer">
	      	  	<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><b>Cerrar</b></button> 
	      </div>
	    </div>
	  </div>
	</div>-->

	<div id="requestModalCliente" class="modal fade" role="dialog">
		
		<form id="frmCliente" method="post" action="" style="margin-top: 5%;">
			<input type="hidden" class = "Requerido form-control input-sm"  name="proforma_id" value="{{$id}}" />
			<div class="modal-dialog modal-lg" role="document" style="margin-left: 28%;width: 37%;margin-top: 7%;">
		<!-- Modal content-->
				<div class="modal-content"> 
					<div class="modal-header">
						<h4 class="modal-title">Clientes adicionales</h4>
					</div>
				  	 <div class="modal-body" style="padding-top: 5px;padding-left: 50px;padding-right: 0px;">
				  		<br>
					    <div id = "clientesAdicionales" style="margin-bottom: 10px;">
							
					    </div>
 					   <div class="row">
					   		<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;padding-right: 0px;">
								<select class="form-control select2" name="cliente_id_aux" id="cliente_id_aux" style="width: 100%" aria-describedby="button-addon2" >
				  					<option value=""> Seleccione cliente</option>
								</select>                                        
							</div>
							<div class="col-xs-12 col-sm-6 col-md-2" style="padding-left: 15px;">
								<button type="button" id="agregarClienteAux" class="btn btn-info"><i class="fa fa-check" aria-hidden="true"></i></button>
								<button type="button" id="clienteNuevo" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button>
							</div>
						</div>
					    <div id ="nuevoCliente" style="display:none;padding-right: 60px;"> 
							<br>
							<small style="color:red">"Solo para nuevos clientes, si el cliente ya existe solo debe seleccionarlo de la lista"</small> <br>
							<small style="color:red"> (*) Campos Obligatorios</small>
							<div id="contenido">
							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;">
									<div class="form-group">
										<label>Nro Documento</label>
										<input type="text" class="Requerido form-control input-sm" name="documento_cliente" style="height: 30.979166px;" id="documento_cliente" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="form-group">
										<label>DV</label>
										<input type="text" class="Requerido form-control input-sm" name="documento_dv" id="documento_dv" style="height: 30.979166px;" disabled="disabled" />
									</div>
								</div>
							</div>	
							<div class="form-group">
								<label>Tipo Documento</label>
								<select class="form-control select2" name="tipo_identidad" id="tipo_identidad" style="width: 100%;">
									@foreach($tipoIdentidads as $key=>$tipoIdentidad)
										<option value="{{$tipoIdentidad->id}}">{{$tipoIdentidad->denominacion}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group">
								<label>Nombre</label>						 
								<input type="text" required class = "Requerido form-control input-sm" name="nombre" style="height: 30.979166px;" id="nombre_cliente"/>
							</div>    
							<div class="form-group">
									<label>Apellido</label>						 
									<input type="text" required class = "Requerido form-control input-sm" name="apellido" style="height: 30.979166px;" id="apellido_cliente"/>
							</div>  
							<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 0px;padding-right: 0px;">
								<div class="form-group">
									<label>Teléfono (*)</label>						 
									<input type="text" class = "Requerido form-control input-sm" name="telefono_cliente" id="telefono_cliente"/>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="form-group">
											<label>Dirección (*)</label>						 
											<input type="text" class = "Requerido form-control input-sm" name="direccion_cliente" style="height: 30.979166px;" id="direccion_cliente"/>
										</div>    
							</div>
							<div class="form-group">
								<label>Pais (*)</label>						 
								<select class="form-control select2" name="pais_cliente" id="pais_cliente" style="width: 100%;">
									@foreach($paises as $key=>$pais)
										<option value="{{$pais->cod_pais}}">{{$pais->name_es}}</option>
									@endforeach
								</select>
							</div>  

							<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="form-group">
											<label>Correo Electrónico</label>						 
											<input type="text" class = "Requerido form-control input-sm" name="email_cliente" style="height: 30.979166px;" id="email_cliente"/>
										</div>    
									</div>
							</div>	
							<div class="row">
								<div class="col-xs-8 col-sm-8 col-md-6" style="padding-left: 0px;padding-right: 0px;">
								</div>
								<div class="col-xs-8 col-sm-4 col-md-3" style="padding-left: 30px;padding-right: 0px;">
									<button type="button" id="btnAceptarCliente" class="btn btn-success" style="width: 90px; background-color: #e2076a;">Aceptar</button>
								</div>
								<div class="col-xs-8 col-sm-4 col-md-3" style="padding-left: 0px;padding-right: 0px;">
									<button type="button" id="btnCerrarCliente" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Cancelar</button>
								</div>
							</div>
						</div> 
				  	</div>
				  <div class="modal-footer">
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	 </form>
	</div>



<!-- ========================================
   					MODAL CLIENTE
   		========================================  -->		
		   <!-- {{--	   <div id="requestModalCliente" class="modal fade" role="dialog">
		<form id="frmCliente" method="post" action="" style="margin-top: 5%;">
			<input type="hidden" class = "Requerido form-control input-sm"  name="proforma_id" value="{{$id}}" />

	  		<div class="modal-dialog">
				<div class="modal-content"> 
					<div class="modal-header">
						<h4 class="modal-title">Clientes </h4>
					</div>
				  	 <div class="modal-body" style="padding-top: 5px;">
							<br>
							<small style="color:red">"Solo para nuevos clientes, si el cliente ya existe solo debe seleccionarlo de la lista"</small>
							<div id="contenido">
							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;">
									<div class="form-group">
										<label>Nro Documento</label>
										<input type="text" class="Requerido form-control input-sm" name="documento_cliente" style="height: 30.979166px;" id="documento_cliente" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="form-group">
										<label>DV</label>
										<input type="text" class="Requerido form-control input-sm" name="documento_dv" id="documento_dv" style="height: 30.979166px;" disabled="disabled" />
									</div>
								</div>
							</div>	
							<div class="form-group">
								<label>Tipo Documento</label>
								<select class="form-control select2" name="tipo_identidad" id="tipo_identidad" style="width: 100%;">
									@foreach($tipoIdentidads as $key=>$tipoIdentidad)
										<option value="{{$tipoIdentidad->id}}">{{$tipoIdentidad->denominacion}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group">
								<label>Nombre</label>						 
								<input type="text" required class = "Requerido form-control input-sm" name="nombre" style="height: 30.979166px;" id="nombre_cliente"/>
							</div>    
							<div class="form-group">
									<label>Apellido</label>						 
									<input type="text" required class = "Requerido form-control input-sm" name="apellido" style="height: 30.979166px;" id="apellido_cliente"/>
							</div>  
							<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="form-group">
											<label>Dirección</label>						 
											<input type="text" class = "Requerido form-control input-sm" name="direccion_cliente" style="height: 30.979166px;" id="direccion_cliente"/>
										</div>    
							</div>
							<div class="form-group">
								<label>Pais</label>						 
								<select class="form-control select2" name="pais_identidad" id="pais_cliente" style="width: 100%;">
									@foreach($paises as $key=>$pais)
										<option value="{{$pais->cod_pais}}">{{$pais->name_es}}</option>
									@endforeach
								</select>
							</div>  

							<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="form-group">
											<label>Correo Electrónico</label>						 
											<input type="text" class = "Requerido form-control input-sm" name="email_cliente" style="height: 30.979166px;" id="email_cliente"/>
										</div>    
									</div>
							</div>	
				  	</div>
				  <div class="modal-footer">
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
					<button type="button" id="btnAceptarClientes" class="btn btn-success" style="width: 90px; background-color: #e2076a;">Aceptar</button>
				  </div>
			</div>
	  	</div>
	 </form>
	</div>--}}-->
<!-- ========================================
   					MODAL CLIENTE
   		========================================  -->		
		   <div id="requestModalClienteSolicitud" class="modal fade" style="z-index: 2000" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
		<form id="frmClientes" method="post" action="" style="margin-top: 5%;">
		<input type="hidden" class = "Requerido form-control input-sm"  name="proforma_id" value="{{$id}}" />
	  		<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Ingresar Cliente</h2>
					</div> 
				  	 <div class="modal-body" style="padding-top: 5px;">
    				<small style="color:red">"Solo para nuevos clientes, si el cliente ya existe solo debe seleccionarlo de la lista"</small>

				  		<div id="contenido">
						  <div class="row"  style="margin-bottom: 1px;">
	    					<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;">
		    					<div class="form-group">
		    						<label>Nro Documento</label>
		    						<input type="text" class="Requerido form-control input-sm" name="documento_cliente" style="height: 30.979166px;" id="documento_clientes" />
		    					</div>
		    				</div>
	    					<div class="col-xs-12 col-sm-6 col-md-2">
		    					<div class="form-group">
		    						<label>DV</label>
		    						<input type="text" class="Requerido form-control input-sm" name="documento_dv" id="documento_dvs" style="height: 30.979166px;" disabled="disabled" />
		    					</div>
		    				</div>
		    			</div>	
						<div class="form-group">
		    				<label>Tipo Documento</label>
		    				<select class="form-control select2" name="tipo_identidad" id="tipo_identidads" style="width: 100%;">
		    					@foreach($tipoIdentidads as $key=>$tipoIdentidad)
		    						<option value="{{$tipoIdentidad->id}}">{{$tipoIdentidad->denominacion}}</option>
		    					@endforeach
		    				</select>
		    			</div>

				 			<div class="form-group">
							    <label>Nombre</label>						 
								<input type="text" required class = "Requerido form-control input-sm" name="nombre" style="height: 30.979166px;" id="nombre_clientes"/>
							</div>    
				 			<div class="form-group">
							    <label>Apellido</label>						 
								<input type="text" required class = "Requerido form-control input-sm" name="apellido" style="height: 30.979166px;" id="apellido_clientes"/>
							</div>  
							<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 0px;padding-right: 0px;">
						 			<div class="form-group">
									    <label>Dirección</label>						 
										<input type="text" class = "Requerido form-control input-sm" name="direccion_cliente" style="height: 30.979166px;" id="direccion_clientes"/>
									</div>    
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12" style="padding-left: 0px;padding-right: 0px;">
						 			<div class="form-group">
									    <label>Correo Electrónico</label>						 
										<input type="text" class = "Requerido form-control input-sm" name="email_cliente" style="height: 30.979166px;" id="email_clientes"/>
									</div>    
								</div>
							</div>	
				  		</div>
				  <div class="modal-footer">
					<button type="button" id="btnAceptarClientex" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Aceptar</button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	 </form>
	</div>

<!--{{-- ========================================
   			MODAL EMISION TICKET
   	========================================  --}} -->			

	<div class="modal fade" id="modalEmisionTicket"  role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" style="margin-left: 20%;">
	    <div class="modal-content" style="width: 160%;height: 800px;">
	      <div class="modal-header">
	        <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
	        	<span aria-hidden="true">&times;</span>
	        </button>
	        <h4 class="modal-title" id="" style="font-weight: 800;">
	        	<i class="fa fa-fw fa-send-o"></i>
	        	Solicitar Emisión de Ticket 
	        	<i style="display:none;" class="fa fa-refresh fa-spin load_solicitud_ticket"></i>
	        </h4>
			<small class="badge bg-blue">Campos Obligatorios *</small>
			<br>
			<span class="msj_err_modal_solicitud" style="color:#DB6042; font-weight: 600;"></span>
			<span class="msj_success_modal_solicitud" style="color:#489946; font-weight: 600;"></span>
	      </div>
	      <form id="solicitudTicketForm" method="POST">
	      <div class="modal-body">
	      <br>
	      <br>
	      <div class="row">
		      	<div class="col-md-12">
					<div class="form-group">
			    		<label style="font-weight: 600px;font-size: 18px;">Comentario *</label>
						<textarea class="form-control" rows="20" style="height:400px;" id="text_area_emision"></textarea>
					</div>
				</div>
           </div>
		   <div class="row">	
				<div class="col-md-6">
					<div class="form-group">
						<label>Tipo Ticket *</label> 						 
						<select class="form-control select2 input-lg" name="id_tipo_ticket" id="id_tipo_ticket" style="width: 100%;">				
							<option value="">Seleccione Tipo Ticket</option>	
							@foreach ($productosAereos as $producto)
								<option value="{{$producto->id}}">{{$producto->denominacion}}</option>
							@endforeach
							</select>
					</div> 
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Precio Venta <span class="ob">*</span></label>
						<div class="input-group">
							  <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
		                	  <input type="text" class="form-control control_numeric" name="precio" value="" id="precio_venta_solicitud" placeholder="Precio">
					    </div>
					</div>

     
	               
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Procentaje Comisión   <span class="ob">*</span></label>
						<div class="input-group">
							  <span class="input-group-addon"><b>%</b></span>
		                	  <input type="text" class="form-control control_numeric" id="porcentaje_comision_ticket" name="porcentaje" value="" placeholder="Porcentaje">
					    </div>
					</div>
				</div>
						 
			</div> 	
			</div>			            	
					       
			    <input type="hidden" name="id_proforma" value="{{$id}}">       
	  		   	<input type="hidden" name="comentario" id="hidden_comentario_solicitud">
	  		    <input type="hidden"  id="tipo_operacion_solcitud">
	    
	      </form>	
	      <div class="modal-footer">
	      	  {{-- <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><b>Cerrar</b></button> --}}
			  <button type="button" class="btn btn-success btn-lg"  id="enviarSolicitudEmisionTicket"><b>Enviar Operaciones</b></button>
	      </div>
	    </div>
	  </div>
	</div>

	 <!-- {{--==============================================
			FORMA DE PAGO
	============================================== --}} -->	
	<div class="modal fade" id="modalFormaPago"  role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document" style="margin-left: 30%;margin-top: 5%;">
	    <div class="modal-content" style="width: 900px; height: : 900px;">
	      <div class="modal-header">
	        <button type="button" class="closeModal"  data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
             </button>
	        <h4 class="modal-title" id="" style="font-weight: 800;">
	        	<i class="fa fa-fw fa-credit-card"></i>
	        	Forma de Pago
	        	<i class="fa fa-refresh fa-spin" id="carga_forma_paga" style="display: none;"></i>
	        </h4>
			 <small id="msj_pago" class="badge bg-blue">Campos Obligatorios *</small>
			<br>
			<span class="" style="color:#DB6042; font-weight: 600;"></span>
			<span class="" style="color:#489946; font-weight: 600;"></span>
	      </div>
	   
	    <div class="modal-body">
	      	<div class="row">
		      	<div class="col-md-6" style="margin-bottom: 5px;">
					<div class="row">
						<div class="col-md-3" style="margin-bottom: 5px;">
	                  		<label for="monto_proforma" class="control-label" style="text-align: right;">Monto Proforma :</label>
	                  	</div>	
		                <div class="col-md-9">
		                    <input type="text" class="form-control" style="font-weight: 800; font-size: 20px;" id="monto_proforma" disabled>
		                </div>
	                </div>
				</div>
				<div class="col-md-6" style="margin-bottom: 5px;">
					<div class="row">
						<div class="col-md-3" style="margin-bottom: 5px;">
			                  <label for="diferencia_pago" class="control-label" style="text-align: right;">Diferencia :</label>
			            </div>
	                   <div class="col-md-9">
	                   	 	<input type="text" class="form-control" style="font-weight: 800; font-size: 20px;" id="diferencia_pago" disabled>
	                  </div>
	                </div>
				</div>
			</div>	
			<div class="row">	
			   	<form id="formOptionFormaPago"> 
					<div class="col-md-3">
						<div class="form-group">
							<label>Forma Pago *</label> 						 
							<select class="form-control select2 input-lg" name="id_forma_pago" id="id_forma_pago" style="width: 100%;">				
								@foreach ($forma_pago as $forma)
									<option value="{{$forma->id}}">{{$forma->denominacion}}</option>	
								@endforeach
								
								</select>
						</div> 
					</div>

					<div class="col-md-2">
						<div class="form-group">
							<label>Documento </label>
				            <input type="text" class="form-control" name="documento" id="documento_forma_pago" placeholder="Documento">
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label>Importe Pago *</label>
				            <input type="text" class="form-control control_usd" name="importe_pago"  id="importe_pago" placeholder="Importe Pago">
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label>Moneda *</label> 						 
							<select class="form-control select2 input-lg"  id="id_moneda_pago" name="id_moneda_pago" style="width: 100%;">				
								@foreach ($currencys as $currency)
								<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
								@endforeach
								</select>
						</div> 
					</div>
						
					<div class="col-md-1">
						<button type="button" style="margin-top: 25px;" onclick="agregarFilaPago()"  title="Agregar Forma Pago" class="btn-mas-request btn btn-info btn-sm" ><i class="fa fa-plus fa-lg"></i></button>
					</div>
					<input type="hidden" name="id_proforma" value="{{$id}}">   
				</form>
			<input type="hidden" id="primera_carga_bandera_id" value="N">   
			<form id="formFormaPago"> 
			<div class="col-md-12">
				<div class="table-responsive" >
					<table id="listadoPagos" class="table" style="width:100%">
					<thead>
						<tr>
							<th>Pago</th>
							<th>Documento</th>
							<th>Importe Pago</th>
							<th>Importe</th>
							<th>Moneda</th>
							<th></th>
						</tr>
					</thead>

					<tbody  style="text-align: center">
					</tbody>          
				  </table>
			   </div>
		  </div>
			<input type="hidden" name="id_proforma" value="{{$id}}">   
			</form>	
	  	</div> 	
	</div>			            	
				    
	    <div class="modal-footer">
 			<button type="button" class="btn btn-danger btn-lg"  id="btnEliminarTodosPago"><b>Eliminar Forma Pago</b></button>
			<button type="button" class="btn btn-success btn-lg" disabled id="btnGuardarFormaPago"><b>Guardar</b></button>
	      </div>
	    </div>
	 </div>
	</div>
	 <!-- ========================================
   					MODAL SOLICITUDES
   		========================================  -->	
		   <div id="modalSolicitudes" class="modal fade" role="dialog">
			<div class="modal-dialog modal-xl" role="document" style="width: 80%;margin-left: 15%;margin-top: 10%;">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
                <h4 class="modal-title">Solicitudes de Facturación</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
				  	<table id="soluciones" class="table" style="width: 100%;">
				  		<thead>
							<tr style="background-color: #eceff1;">
								<th>Fecha</th>
								<th>Cliente</th>
								<th>Usuario Pedido</th>
								<th>Pasajero Pedido</th>
								<th>Moneda</th>
								<th>Monto</th>
								<th>Estado</th>
								<th></th>
							</tr>	
						</thead>
						<tbody>
						</tbody>
					</table>
		      </div>
		      <div class="modal-footer" style="padding-bottom: 5px;padding-top: 5px;">
		        <button type="button" class="btn btn-success" data-dismiss="modal" id="cerrarModal">Cerrar</button>
		      </div>
		    </div>

		  </div>
		</div>
	<!--{{--==============================================
				MODAL AVISO
	============================================== --}}-->
	<div class="modal fade" id="modalAviso" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" style="margin-left: 18%;">
			<div class="modal-content" style="width: 900px;">
				<div class="modal-header">
					<button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="" style="font-weight: 800;">Cliente con Acuerdo Especial</h4>
					
					
				</div>

				<div class="modal-body">
					<div class="col-md-12">
						<div class="form-group">
							<label style="font-weight: 600;font-size: 18px;">Acuerdo Especial</label>
							<textarea class="form-control" rows="5" readonly id="acuerdo_id"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="requestModalPasajeroDetalle" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" style="margin-left: 25%;margin-top: 7%;">
			<div class="modal-content" style="width: 900px;">
				<div class="modal-header">
					<button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="" style="font-weight: 800;">Pasajeros de Proforma</h4>
				</div>
				<div class="modal-body">
					<div id="detallesPasajero">

					</div>
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>


	<!-- ========================================
   					MODAL PROVEEDOR NETO
   	========================================  -->		

	<div class="modal fade confirmacion" id="modalProveedorNeto"  role="dialog" aria-labelledby="myModalLabel" style="padding-right: 17px;margin-left: 35%;margin-top: 12%;">
	  <div class="modal-dialog" >
	    <div class="modal-content">
	      <div class="modal-body">
	       <div class="row">
			<div class="col-md-12 text-center">
					<h4 class="swal-text" style="font-size: x-large;"><b>GESTUR</b></h4><br>
					<h4 class="swal-text" style="margin-bottom: 30px;margin-top: 15px;font-size: 16px;">Este ítem corresponde a un proveedor NETO, debe
cargar la comisión correspondiente en un ítem adicional.</h4>
			</div>
				<div class="col-md-5 text-center"></div>
				<div class="col-md-2 text-center">
					<button type="button" class="swal-button swal-button--cancel btn-warning" data-dismiss="modal" aria-label="Close" id="">Salir</button>
				</div>
	       </div>
	      </div>
	    </div>
	  </div>
	</div>


	<!-- ========================================
   			MODAL DETALLES DE FACTURA
   	========================================  -->		

	<div class="modal fade" id="modalDetallesFactura" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" style="margin-left: 6%;margin-top: 5%;">
			<div class="modal-content" style="width: 300%;margin-left: 30%;">
				<div class="modal-header">
					<h4 class="modal-title" id="" style="font-weight: 800;">Detalles de Factura</h4>
				</div>
				<div class="modal-body">
					<div class="table-responsive" >
						<form id="formFacturaDetalle"> 
							<input type="hidden" name="proforma" value="{{$proformas[0]->id}}">
							<table id="listadoFactura" class="table" style="width:100%">
								<thead>
									<tr>
										<th style="width: 5%;">Cant.</th>
										<th style="width: 40%;">Descripcion</th>
										<th style="width: 10%;">Precio Unitario</th>
										<th style="width: 10%;">Exento</th>
										<th style="width: 10%;">Gravada 5</th>
										<th style="width: 10%;">Gravada 10</th>
										<th style="width: 3%;">
										</th>
									</tr>
								</thead>
								<tbody style="text-align: center">
									<tr id="0">
										<th>
											<input type="text" class = "form-control input-sm numeric sumando" id="cantD_0" value="0">
										</th>
										<th>
											<input type="text" class = "form-control input-sm" id="desc_0" value="">
										</th>
										<th>
											<input type="text" class = "form-control input-sm numeric sumando" id="pru_0" value="0">
										</th>
										<th>
											<input type="text" class = "form-control input-sm numeric" id="exe_0" value="0">
										</th>
										<th>
											<input type="text" class = "form-control input-sm numeric" id="iv5_0" value="0">
										</th>
										<th>
											<input type="text" class = "form-control input-sm numeric" id="iv10_0" value="0">
										</th>
										<th>
											<input type="hidden" id="totalDetalle_0" value="0">
											<button type="button" onclick="agregarFilaDetalle()" class="btn btn-danger" style="width: 50px;background-color: #157521 !important;border-color: #1e6f15 !important;"><i class="fa fa-plus" style="font-size: 18px;"></i></button>
										</th>
									</tr>
								</tbody>          
							</table>
						</from>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger btn-lg"><b>Salir</b></button>
					<button type="button" class="btn btn-success btn-lg" disabled id="btnGuardarDetalleFactura"><b>Guardar</b></button>
				</div>
			</div>
		</div>
	</div>


	<input type="hidden" id="markupProformaHidden" value="{{$proforma->markup}}">


		<!-- ========================================
   					MODAL SOLICITAR FACTURA PARCIAL
   			========================================  -->		

	 <div class="modal fade" id="modalSolicitarFacturaParcial"  role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" >
	    <div class="modal-content" style="margin-top: 25%;">
	      <div class="modal-header">
	        <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-fw fa-warning"></i> Solicitar Factura Parcial</h4>
	      </div>
	      <div class="modal-body">
		  	<form id="frmSolicitarFactura" method="GET" style="margin-top: 2%;">
			  	<input type="hidden" class = "Requerido form-control input-sm text-bold" id="id_proforma_pedido" name="id_proforma_pedido" value="{{$proforma->id}}"/>
				<div class="row">
					<div class="col-md-12" style="margin-bottom: 15px;">
						<div class="row" style="margin-left: 0px; margin-right: 0px;">
							<label>Cliente</label><br>
							<div class="input-group">
                                <div class="input-group-prepend" id="button-addon2" style="width: 100%;">
	                                <button id="botonClienteSolicitud" class="btn btn-primary" data-toggle="modal" data-target="#requestModalClienteSolicitud" type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-plus fa-lg"></i></button>
									<select class="form-control select2" name="cliente_pedido" id="cliente_pedido" style="width: 100%" aria-describedby="button-addon2" >
										<option value="">Seleccione un Cliente</option>									
									</select>                                        
                                </div>
							</div>	
						</div>
					</div>
					<div class="col-md-12 cliente_solicitud">
						<label>RUC</label>
						<input type="text" class = "form-control" name="ruc" id="ruc" maxlength="30" tabindex="0" style="padding-top: 5px;padding-bottom: 5px;height: 35.22222px;"/>
					</div>
					<div class="col-md-12" style="margin-bottom: 15px;">
						<div class="row" style="margin-left: 0px; margin-right: 0px;">
							<label>Pasajero</label><br>
							<div class="input-group">
                                <div class="input-group-prepend" id="button-addon2" style="width: 100%;">
	                                <button id="botonPasajeroSolicitud" class="btn btn-primary" data-toggle="modal" data-target="#requestModalPasajeroSolicitud" type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-plus fa-lg"></i></button>
									<select class="form-control select2" name="pasajero_pedido" id="pasajero_pedido" style="width: 100%" aria-describedby="button-addon2" >
										<option value="">Seleccione un Pasajero</option>									
									</select>                                        
                                </div>
							</div>	
						</div>
					</div>
					<div class="col-md-12 cliente_solicitud" style="margin-top: 15px;margin-bottom: 15px;">
						<label>Razón Social</label>
						<input type="text" class = "form-control" name="razon_social" id="razon_social" maxlength="30" tabindex="0" style="padding-top: 5px;padding-bottom: 5px;height: 35.22222px;"/>
					</div>
					<div class="col-md-12" style="margin-top: 15px;">
						<label>Moneda de Facturación</label>
						<select class="form-control select2 text-bold" name="moneda_factura_parcial" id="moneda_factura_parcial" style="width: 100%;" >
							<option value="">Seleccione Moneda</option>
							@if(isset($currencys))
								@foreach($currencys as $key=>$currency)
									@if($currency->tipo == 'V')
										<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
									@endif
								@endforeach	
							@endif
						</select>
					</div>
					<div class="col-md-12" style="margin-top: 15px;">
						<label>Monto</label>
						<input type="text" class = "form-control numeric" name="monto_factura_parcial" style="padding-top: 5px;padding-bottom: 5px;height: 35.22222px;" id="monto_factura_parcial" onkeypress="return justNumbers(event);" tabindex="0"/>
						<input type="hidden" id="monto_cotizado_factura_parcial" name="monto_cotizado_factura_parcial" class = "form-control numeric"/>
						
					</div>

				</div>
			</form>
			<br>
			<br>
	      <div class="modal-footer">
				<button type="button" id="btnGuardarSolicitarFacturaParcial" class="btn btn-success btn-lg" style="width: 100px; background-color:#ed00ef;margin-left: 7%;">Guardar</button>
				<button type="button" class="btn btn-danger btn-lg" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
	      </div>
	    </div>
	  </div>
	</div>

<!-- ========================================
   					MODAL PASAJEROS
   	========================================  -->		
	<div id="requestModalPasajeroSolicitud" class="modal fade" role="dialog">
		<form id="frmPasajeroSolicitud" method="post" action="" style="margin-top: 12%;">
	  		<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Ingresar Pasajero</h2>
					</div>
				  	<div class="modal-body">
				  		<div id="contenido">
				 			<div class="form-group">
							    <label>Nombre</label>						 
								<input type="text" required class = "Requerido form-control" name="nombre" id="nombre_pedido"/>
							</div>    
				 			<div class="form-group"> 
							    <label>Apellido</label>						 
								<input type="text" required class = "Requerido form-control" name="apellido" id="apellido_pedido"/>
							</div>  
				          	<div class="row"  style="margin-bottom: 1px;">
				          		<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
				          			<div class="form-group">
							            <label>Tipo Documento</label>						 
							            <select class="form-control select2" name="tipo_identidad" id="tipo_venta_pedido" style="width: 100%;">
											@foreach($tipoIdentidads as $key=>$tipoIdentidad)
												<option value="{{$tipoIdentidad->id}}">{{$tipoIdentidad->denominacion}}</option>
											@endforeach	
							            </select>
									</div>  
				          		</div>
								<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;margin-bottom: 15px;">
						 			<div class="form-group">
									    <label>Nro Documento</label>						 
										<input type="text" class = "Requerido form-control" name="documento_pedido"/>
									</div>    
								</div>
								<input type="hidden" class = "Requerido form-control" name="marcador1" valor="2"/>
							</div>	
				  		</div>
				  </div> 
				  <div class="modal-footer">
					<button type="button" id="btnAceptarSolicitudPasajero" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Aceptar</button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	 </form>
	</div>
	
@endsection
@section('scripts')

	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	
	<!-- wysihtml core javascript with default toolbar functions --> 
	<script>
		$.fn.modal.Constructor.prototype.enforceFocus = function() {};
	</script>

	<script>

		var permisoAnularFee = Boolean({{$permisoAnularFee}});

		function pasajerosDetalle(){
			$.ajax({
					type: "GET",
					url: "{{route('getInfoProforma')}}",
					dataType: 'json',
					data: {
							id_proforma : $('#proforma_id').val() 
						},
					success: function(rsp){
								//<input type="text" class="form-control" name="documento" id="documento_forma_pago" placeholder="Documento">
								resultado = "";
								$.each(rsp, function(index,value){
									resultado += '<input type="text" class="form-control" disabled="disabled" value="'+value.pasajero_n+'" placeholder="Documento">';
								})	
								$('#detallesPasajero').html(resultado) 
					
					}
			})	

		}

		function getCommetTimeLine(cont){

		  if ( $('.comentarioBoxTimeLine'+cont).hasClass("commentNone") ) {
		    $('.comentarioBoxTimeLine'+cont).removeClass('commentNone');
		  } else {
		    $('.comentarioBoxTimeLine'+cont).addClass('commentNone');
		  }

		}

		$('#pasajero_principal').select2({
									multiple:true,
									//maximumSelectionLength: 2,
									placeholder: 'Pasajero'
								});
		$('#pasajero_principal_peticion').select2({
									placeholder: 'Pasajero'
								});


		$('#pasajero_principal').prop('disabled',true);

	    inicio();

	//	comentarioCorto();

		//cargando();

		setTimeout(delase, 2000);

		$('.close').on('click',function(){
			$('#requestModalDetalle').modal('hide');
		})	

		$('#text_area_emision').summernote();

		$('#emergencia_tipo').change(function(event) {
			valor = $(this).val();
			if(valor == 1){
				$('#proforma_emergencia').prop('disabled',false);
			}else{
				$('#proforma_emergencia').prop('disabled',true);	
			}
			
		});	

		$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { 
				$(this).val(0);
			 }
		});

		$('#indice_0').prop('disabled',true);
		

		{{--================LOGICA SOLICITUD TICKET================--}}

		$('.control_numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false
		});

		function campo_activate(id_producto){
				//TICKET PAQUETE
			if(id_producto == 9){
				$('#precio_venta_solicitud').prop('disabled',true);
				$('#porcentaje_comision_ticket').prop('disabled',true);
				$('#precio_venta_solicitud').val('');
				$('#porcentaje_comision_ticket').val('');
				$('.ob').hide();
			} else {
				$('#precio_venta_solicitud').prop('disabled',false);
				$('#porcentaje_comision_ticket').prop('disabled',false);
				$('.ob').show();
			}

		}

		$('#id_tipo_ticket').change(function(event) {
			let id_producto = $(this).val();
			campo_activate(id_producto);
		
		});

		$('#btnEmisionTicket').on('click',function(){
			//RECARGAR SI EXISTEN DATOS
			$('#modalEmisionTicket').modal('show');
			$('.load_solicitud_ticket').show();
			$('.msj_err_modal_solicitud').html('');
			$('.msj_success_modal_solicitud').html('');
			$('#tipo_operacion_solcitud').val('E');

			$.ajax({
					type: "POST",
					url: "{{route('getInfoEmisionTicket')}}",
					dataType: 'json',
					data: {id_proforma : $('#proforma_id').val() },
						error: function(){
							$('.load_solicitud_ticket').hide();
							$('.msj_err_modal_solicitud').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
						   $('.load_solicitud_ticket').hide();
							if(!$.isEmptyObject(rsp.data)){
								$('.msj_success_modal_solicitud').html('Se recuperaron los datos.');
								$("#id_tipo_ticket").val(rsp.data.id_producto_ticket).select2();
								$("#precio_venta_solicitud").val(rsp.data.precio_venta);
								$("#porcentaje_comision_ticket").val(rsp.data.porcentaje_comision);
								$('#text_area_emision').summernote('code',rsp.data.comentario);
								//$('#text_area_emision ~ iframe').contents().find('.wysihtml5-editor').html(rsp.data.comentario);
								//TICKET PAQUETE
								campo_activate(rsp.data.id_producto_ticket);
								if(rsp.data.propietario == false && rsp.data.puede_editar == false){
									$('#tipo_operacion_solcitud').val('');
									$('#enviarSolicitudEmisionTicket').hide();
									}

								if(rsp.data.puede_editar == true){
									$('#enviarSolicitudEmisionTicket').html('<b>ACTUALIZAR</b>');
									$('#tipo_operacion_solcitud').val('U');
									actualizarCabecera();
									}	


					      	} else {

								$("#id_tipo_ticket").val('').select2();
								$("#precio_venta_solicitud").val('');
								$("#porcentaje_comision_ticket").val('');
								$('#text_area_emision').summernote('code','');

					      	}
						}
					});
		});


		$(document).keyup(function(e){
			if(e.which==27) {
			    $('#modalEmisionTicket').modal('hide');
			}
		});

		$('#enviarSolicitudEmisionTicket').on('click',function(){
			$('.load_solicitud_ticket').show();
			$('#hidden_comentario_solicitud').val($('#text_area_emision').val());
			$('.msj_err_modal_solicitud').html('');
			$('.msj_success_modal_solicitud').html('');
			var operacion = $('#tipo_operacion_solcitud').val();

			var ok = 0;
			//VALIDAR FORM
			ok += ($("#id_tipo_ticket").val() != '') ? 0 : 1;
			ok += ($.trim($('#hidden_comentario_solicitud').val()) != '') ? 0 : 1;
			if($("#id_tipo_ticket").val() != 9 ){ 
			ok += ($.trim($("#precio_venta_solicitud").val()) != '') ?  0 : 1;
			ok += ($.trim($("#porcentaje_comision_ticket").val()) != '') ? 0: 1;
			}

			if(ok === 0){ 

				var dataString = $('#solicitudTicketForm').serialize();

				$.ajax({
					type: "POST",
					url: "{{route('solicitarEmisionTicket')}}",
					dataType: 'json',
					data: dataString,
						error: function(){
							setTimeout($('.load_solicitud_ticket').hide(),1000);
							$('.msj_err_modal_solicitud').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
					      	$('.load_solicitud_ticket').hide();
					      	if(rsp.operacion === true){
					      		$('.msj_success_modal_solicitud').html('Los datos fueron almacenados.');

					      		if(operacion == 'E'){
					      		window.location.replace('{{ route('listadosProformas') }}');
					      		// comentarioAuto('3');
					      			}
					      	} else {
					      		$('.msj_err_modal_solicitud').html('Ocurrio un error, vuelva a intentarlo.');	
					      	}
					      	
							}
					});	
			} else {
				$('.load_solicitud_ticket').hide();
				$('.msj_err_modal_solicitud').html('Complete los campos por favor.');
			}
		});

		$('.solicitarFacturaParcial').click(function(){
			$('#producto_pedido').val(valor).select2();
			$('#concepto_parcial').val('');
			$('#moneda_factura_parcial').val(0).select2();
			$('#monto_factura_parcial').val(0);

			$('.cliente_solicitud').css('display', 'none');
			$.ajax({
			              type: "GET",
			              url: "{{route('getCargaCliente')}}",//fileDelDTPlus
			              dataType: 'json',
						  data: {id_proforma : $('#proforma_id').val() },
			              success: function(rsp){
						 	$('#cliente_pedido').empty()
			              	$.each(rsp, function (key, item){
								var newOption = new Option(item.documento_identidad+' - '+item.nombre+' - '+item.denominacion, item.id, false, false);
								$('#cliente_pedido').append(newOption);
							})	
							valor = '{{$cliente_id}}';		
							$("#cliente_pedido").val(valor).select2();
							monedaP = '{{$proformas[0]->id_moneda_venta}}';
							$("#moneda_factura_parcial").val(monedaP).select2();
							//$('#monto_factura_parcial').val($('#saldoFacturaMonto').val());
			              }
			        })


					$.ajax({
			              type: "GET",
			              url: "{{route('getCargaPasajero')}}",//fileDelDTPlus
			              dataType: 'json',
			              success: function(rsp){
						 	$('#pasajero_pedido').empty()
			              	$.each(rsp, function (key, item){
								var newOption = new Option(item.pasajero_n, item.id, false, false);
								$('#pasajero_pedido').append(newOption);
							})	
							valor = $('#pasajero_principal').val();		
							$("#pasajero_pedido").val(valor).select2();
			              }
			        })


					$.ajax({
			              type: "GET",
			              url: "{{route('getSaldoProforma')}}",
			              dataType: 'json',
						  data: {id_proforma : $('#proforma_id').val() },
			              success: function(rsp){
							$('#monto_factura_parcial').val(rsp);			              }
			        })

					$('#modalSolicitarFacturaParcial').modal('show');
		});

		$('#btnGuardarSolicitarFacturaParcial').click(function(){ 
			if($('#producto_pedido').val() != "" &&parseFloat($('#monto_factura_parcial').val()) != 0 && $('#moneda_factura_parcial').val() != ""&& $('#pasajero_pedido').val() != ""){
				$.ajax({
						type: "GET",
						url: "{{route('getMontoCotizado')}}",
						dataType: 'json',
						data: {
								dataMonto: clean_num($('#monto_factura_parcial').val()),
								dataMonedaCompra: $('#moneda_factura_parcial').val(),
								dataCurrency_venta: $('#moneda').val()
							},
							success: function(rsp){
													$('#monto_cotizado_factura_parcial').val(rsp);
													guardarParcial();
													}
						})


			}else{
					$.toast({
							heading: 'Error',
							text: 'Complete el formulario para la facturacion parcial.',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
						});				
				}

		});

		function guardarParcial(){
			if(parseFloat(clean_num($('#saldoFacturaMonto').val())) >= parseFloat($('#monto_cotizado_factura_parcial').val())){
						return swal({
								title: "GESTUR",
								text: "¿Está seguro que desea generar la solicitud de factura parcial?",
								showCancelButton: true,
								buttons: {
										cancel: {
												text: "No",
												value: null,
												visible: true,
												className: "btn-warning",
												closeModal: false,
											},
										confirm: {
												text: "Sí, Generar",
												value: true,
												visible: true,
												className: "",
												closeModal: false
											}
										}
						}).then(isConfirm => {
								if (isConfirm) {
								//	if($("#frmSolicitarFactura").isValid()){
										var dataString = $("#frmSolicitarFactura").serialize();
										$.ajax({
												type: "GET",
												url: "{{route('getSolicitarFactura')}}",
												dataType: 'json',
												data: {
														cliente_pedido: $('#cliente_pedido').val(),
														producto_pedido: $('#producto_pedido').val(),
														concepto_parcial: $('#concepto_parcial').val(),
														moneda_factura_parcial: $('#moneda_factura_parcial').val(),
														monto_factura_parcial: $('#monto_factura_parcial').val(),
														id_proforma_pedido: $('#id_proforma_pedido').val(),
														pasajero_pedido: $('#pasajero_pedido').val()
													},

												success: function(rsp){
													if(rsp.status == 'OK'){
														$("#modalSolicitarFacturaParcial").modal('hide');
														swal("Éxito", rsp.mensaje, "success");
														actualizarCabecera();
														location.reload();
													}else{
														swal("Cancelado", rsp.mensaje, "error");
													} 
												}
											})
									} else {
										swal("Cancelado", "", "error");
									}
							//	}
						});
				}else{
					$.toast({
							heading: 'Error',
							text: 'El monto ya sobrepasa el saldo de la proforma.',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
						});				
				}
		}


		$("#frmSolicitarFactura").validate({  // initialize plugin on the form
			debug: false,
			rules: {
				"cliente_pedido": {
					required: true
				},
				"producto_pedido": {
					required: true
				},
				"concepto_parcial": {
					required: true
				},
				"moneda_factura_parcial": {
					required: true
				},
				"monto_factura_parcial": {
					required: true
				}
			},
			messages: {
				"cliente_pedido": {
					required: "<br>Seleccione un Cliente"
				},
				"producto_pedido": {
					required: "<br>Seleccione un Producto"
				},
				"concepto_parcial": {
					required: "<br>Seleccione un Concepto"
				},
				"moneda_factura_parcial": {
					required: "<br>Seleccione una moneda"
				},
				"monto_factura_parcial": {
					required: "<br>Ingrese el monto"
				}
			}
		});


		$('#cliente_pedido').change(function(){
			$('.cliente_solicitud').css('display', 'block');
			cliente_solicitud = $('#cliente_pedido :selected').text();
			cliente_partes = cliente_solicitud.split(' - ');
			$('#ruc').val(cliente_partes[0])
			$('#razon_social').val(cliente_partes[1]);
			
		});	


		{{--================LOGICA POST VENTA================--}}



		$('#btnCalificarPostVenta').on('click', function () {
    let ok = 0;
    $('.load_err').show();
    $('.msj_err').html('');
    $('.msj_success').html('');

    if ($('#comentarioPostVenta').val().trim() == '') {
        ok++;
        $('.msj_err').html('Complete el comentario es obligatorio');
        $('.load_err').hide();
    }

    let dataString = $('#valoracion_form').serialize();

    if (ok == 0) {
        $.ajax({
            type: "GET",
            url: "{{route('almacenarCalificacion')}}",
            dataType: 'json',
            data: dataString,
            error: function () {
                $('.load_err').hide();
                $('.msj_err').html('Ocurrió un error, vuelva a intentarlo.');
            },
            success: function (rsp) {
                $('.load_err').hide();

                if (rsp.resp == 'true') {
                    $('.msj_success').html('Los datos fueron almacenados con éxito')
                    $('#modalPostVenta').modal('hide');

                } else {
                    $('.msj_err').html('Ocurrió un error, vuelva a intentarlo.');
                }

                // Verifica si el campo comentario_calificacion tiene contenido
                if ($('#comentarioPostVenta').val().trim() !== '') {
                    // Si el campo tiene contenido, oculta el botón de Calificar
                    $('#btnCalificarPostVenta').hide();
                }
            }
        });
    }
});


{{--==============================================
			FORMA DE PAGO
	============================================== --}}

	$(document).keyup(function(e){

    if(e.which==27) {
        $('#modalFormaPago').modal('hide');
	    }
	});

	{{--CONTROL DE DOLAR POR DEFECTO --}}
	$('#importe_pago').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false
		});


	{{--SEGUN LA MONDEA SELECCIONADA SE HABILITA UNA MASCARA PARA PONER COMAS O NO --}}

	monedaP = '{{$proformas[0]->id_moneda_venta}}';
	$("#id_moneda_pago").val(monedaP).trigger('change.select2');
	$("#id_moneda_pago").prop('disabled', true);


	$('#id_moneda_pago').change(function() {
	$('#importe_pago').inputmask('remove');
		
	if($(this).val() == '143'){
		$('#importe_pago').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false
		});
	} else if($(this).val() == '111'){

		$('#importe_pago').inputmask("numeric", {
			 radixPoint: ",",
			groupSeparator: ".",
			digits: 0,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false
		});

	}//else
	
	});


const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD'
					});
{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
	NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}
function clean_num(n,bd=false){
	if(n && bd == false){ 
	n = n.replace(/[,.]/g,function (m) {  
			 				 if(m === '.'){
			 				 	return '';
			 				 } 
			 				  if(m === ','){
			 				 	return '.';
			 				 } 
			 			});
	return Number(n);
}
if(bd){
	return Number(n);
}
return 0;
}





	
	function agregarFilaPago(){

		let ok = 0;
		let id_forma_pago = $('#id_forma_pago').val();
		let texto_forma_pago = $('#id_forma_pago :selected').text();
		let documento = $('#documento_forma_pago').val().trim();
		let importe_pago = ($('#importe_pago').val().trim() !== '' ? $('#importe_pago').val().trim(): ok++);
		let id_moneda = $('#id_moneda_pago').val();
		let texto_moneda = $('#id_moneda_pago :selected').text();
		let importe_cotizado;
		let monto_proforma;
		let msj = '';
		$('#carga_forma_paga').show();
		$('#msj_pago').removeClass('bg-blue');

		
		if(ok != 0){
			$('#carga_forma_paga').hide(2000);
			$('#msj_pago').css('background-color','#C93F3F' );
			$('#msj_pago').html('Complete los campos obligatorios por favor.');

		} else {




	//OBTENER MONTO COTIZADO Y MONTO ACTUALIZADO DE PROFORMA
	$("#id_moneda_pago").prop('disabled', false);	
	let form = 	$('#formOptionFormaPago').serializeJSON();
	let dataString = { form : form, option:1};
		$.ajax({
					type: "GET",
					url: "{{route('getformaPago')}}",
					dataType: 'json',
					async:false,
					data: dataString,
						error: function(){
						ok++;
						$('#carga_forma_paga').hide(2000);
						$('#msj_pago').css('background-color','#C93F3F' );
						$('#msj_pago').html('Ocurrio un error en la comunicación con el servidor.');
							},
						success: function(rsp){
								if(rsp.linea_fp.length > 0){
									importe_cotizado = formatter.format(parseFloat(rsp.linea_fp[0].importe_cotizado));
									monto_proforma = formatter.format(parseFloat(rsp.linea_fp[0].monto_proforma));

									$('#monto_proforma').val(monto_proforma);	
									}
							}

					});

		$("#documento_forma_pago").val('');
		$("#importe_pago").val('');
		$("#id_moneda_pago").prop('disabled', true);
			
		 }

		if(ok === 0){
			let cantItem = $('#listadoPagos tr').length - 1;
			let tabla = `
			<tr id="lineaPagoEliminar_${cantItem}" class="tabla_filas">
				<td><input type="text" readonly value = "${texto_forma_pago}" class = "form-control input-sm" /></td>
				<td><input type="text" readonly value = "${documento}" class = "form-control input-sm" name="datos[][documento]"/></td>
				<td><input type="text" readonly value = "${importe_pago}" class = "form-control input-sm" name="datos[][importePago]"/></td>
				<td><input type="text" readonly value = "${importe_cotizado}" class = "form-control input-sm" name="datos[][importe]"/></td>
				<td><input type="text" readonly value = "${texto_moneda}" class = "form-control input-sm"/></td>
				<td><button type="button" onclick="eliminarFilaPago(${cantItem})" class="btn btn-danger"><b>Eliminar</b></button>
				<input type="hidden"  value = "${id_forma_pago}"  class = "form-control input-sm" name="datos[][id_forma_pago]"/>
				<input type="hidden"  value = "${id_moneda}" class = "form-control input-sm" name="datos[][id_moneda_pago]"/>
				</td>
			</tr>
			`;
			$('#listadoPagos tbody').append(tabla);

			let cont = 0;
			$.each($('.tabla_filas'), function(index,value){
					let num_cant = value.id.split('lineaPagoEliminar_');
					if(num_cant[1] != cont){
						$('#'+value.id).attr('id','lineaPagoEliminar_'+cont);
						//MODIFICAR ONCLICK
						$(value.children[5]).find('button').attr('onclick','eliminarFilaPago('+cont+')');
					}
					cont ++;

					});

		let form = 	$('#formFormaPago').serializeJSON();
		let sum_total = 0;
		let dato = 0;
		//	CALCULAR LA DIFERENCIA
		$.each(form.datos, function(index,value){
			sum_total += clean_num(value.importe);
					});

		monto_proforma = clean_num(monto_proforma);
		let total = monto_proforma - sum_total.toFixed(2);

		$('#diferencia_pago').val(formatter.format(parseFloat(total.toFixed(2))));


		if(total > monto_proforma){
			ok++;
			$('#carga_forma_paga').hide(2000);
			$('#msj_pago').css('background-color','#C93F3F' );
			$('#msj_pago').html('La suma de los importes excede el valor de facturación de la proforma.');
			$('#btnGuardarFormaPago').prop('disabled',true);
		} else if(total < 0 && total != 0){
			if($('#id_forma_pago').val() != 2){
				ok++;
				$('#carga_forma_paga').hide(2000);
				$('#msj_pago').css('background-color','#C93F3F' );
				$('#msj_pago').html('La diferencia no puede ser un numero negativo.');
				$('#btnGuardarFormaPago').prop('disabled',true);
			}else{	

		}
		} else if(total == 0){
				ok++
			$('#carga_forma_paga').hide(2000);
			$('#msj_pago').css('background-color','#00a65a' );
			$('#msj_pago').html('Los montos coinciden, puede guardar la operación.');
			$('#btnGuardarFormaPago').prop('disabled',false);
		}

		if(ok == 0){ 
			$('#btnEliminarTodosPago').prop('disabled',false);
			$('#carga_forma_paga').hide(2000);
			$('#msj_pago').css('background-color','#00a65a' );
			$('#msj_pago').html('Linea creada con exito.');
			if($('#id_forma_pago').val()  == 2){
				generarGastosTarjetas();
			}

		}
	 }//if

	}//function

	function actualizarDiferencia(){

        let form = 	$('#formFormaPago').serializeJSON();
		let sum_total = 0;
		let dato = 0;
		ok = 0;
		//	CALCULAR LA DIFERENCIA
		$.each(form.datos, function(index,value){
			sum_total += clean_num(value.importe);
					});

		monto_proforma = clean_num($('#total_factura').val());

		let total = monto_proforma - sum_total.toFixed(2);
		$('#diferencia_pago').val(formatter.format(parseFloat(total.toFixed(2))));
    }

	
	function eliminarFilaPago(id){
		$('#carga_forma_paga').show();
		let monto_resta = $('#lineaPagoEliminar_'+id).find('input')[3].value;
		let diferencia = $('#diferencia_pago').val();

		let dato_diferencia = clean_num(diferencia);
		let dato_monto_resta = clean_num(monto_resta);

		let operacion = dato_diferencia +  dato_monto_resta;
		$('#diferencia_pago').val(formatter.format(parseFloat(operacion.toFixed(2))));
		$('#lineaPagoEliminar_'+id).remove();
		
		$('#carga_forma_paga').hide(2000);
		$('#btnGuardarFormaPago').prop('disabled',true);

		$.ajax({
					type: "GET",
					url: "{{route('eliminarItemsPago')}}",
					dataType: 'json',
					data: {idProforma : $('#proforma_id').val() },
						error: function(){
							$('#carga_forma_paga').hide(2000);
							$('#msj_pago').removeClass('bg-blue');
							$('#msj_pago').css('background-color','#C93F3F' );
							$('#msj_pago').html('Ocurrio un error en la comunicación con el servidor.');
						
							},
						success: function(rsp){
											$.each(rsp, function (key, item){
												$.ajax({
														type: "GET",
														url: "{{route('deleteLinea')}}",
														dataType: 'json',
														data: {
																dataLinea: item.id,
																dataProforma: $('#proforma_id').val()
												       			},
														success: function(rsp){
															$.unblockUI();
															$.toast({
																    heading: 'Exito',
																    text: 'Se ha eliminado el Detalle.',
																    position: 'top-right',
																    showHideTransition: 'slide',
											    					icon: 'success'
																});  

															$('.ocultado').hide();
															$('table#lista_productos tr#'+item.item).remove();
															$('#botones_'+item.item).empty();
															actualizarCabecera();
															location.reload();
														}	
												});
											});	
										}

				});	

			var nFilas = $("#listadoPagos tr").length;

			if(nFilas > 0){
				$('#btnEliminarTodosPago').prop('disabled',true);	
				$('#btnGuardarFormaPago').prop('disabled',true);
			}else{
				$('#btnEliminarTodosPago').prop('disabled',false);	
				$('#btnGuardarFormaPago').prop('disabled',false);
			}

			setTimeout(actualizarDiferencia,1400);

	}//function




	$('#btnGuardarFormaPago').on('click',function(){
		operacion_set_pago(1);
	});


	$('#btnEliminarTodosPago').on('click',function(){
				operacion_set_pago(2);
			$('#listadoPagos tbody').html('');

		        $.ajax({
					type: "GET",
					url: "{{route('eliminarItemsPago')}}",
					dataType: 'json',
					data: {idProforma : $('#proforma_id').val() },
						error: function(){
							$('#carga_forma_paga').hide(2000);
							$('#msj_pago').removeClass('bg-blue');
							$('#msj_pago').css('background-color','#C93F3F' );
							$('#msj_pago').html('Ocurrio un error en la comunicación con el servidor.');
						
							},
						success: function(rsp){
						/////////////////////////////////////////////////////
								$.each(rsp, function (key, item){
									$.ajax({
											type: "GET",
											url: "{{route('deleteLinea')}}",
											dataType: 'json',
											data: {
													dataLinea: item.id,
													dataProforma: $('#proforma_id').val()
									       			},
											success: function(rsp){
												$.unblockUI();
												$.toast({
													    heading: 'Exito',
													    text: 'Se ha eliminado el Detalle.',
													    position: 'top-right',
													    showHideTransition: 'slide',
								    					icon: 'success'
													});  

												$('.ocultado').hide();
												$('table#lista_productos tr#'+item.item).remove();
												$('#botones_'+item.item).empty();
												var nFilas = $("#listadoPagos tr").length;

												if(nFilas > 0){
													$('#btnEliminarTodosPago').prop('disabled',true);
													$('#btnGuardarFormaPago').prop('disabled',true);	
												}else{
													$('#btnEliminarTodosPago').prop('disabled',false);	
													$('#btnGuardarFormaPago').prop('disabled',false);
												}									
												actualizarCabecera();
												location.reload();
											}	
									});
								});	
						/////////////////////////////////////////////////////
					 }

				});	
			setTimeout(actualizarDiferencia,1400);
		  
	});

	function operacion_set_pago(option){

		$('#carga_forma_paga').show();
		let form = 	$('#formFormaPago').serializeJSON();
		let dataString = { form:form, option:option };
			
				$.ajax({
					type: "GET",
					url: "{{route('setformaPago')}}",
					dataType: 'json',
					data: dataString,
						error: function(){
							$('#carga_forma_paga').hide(2000);
							$('#msj_pago').removeClass('bg-blue');
							$('#msj_pago').css('background-color','#C93F3F' );
							$('#msj_pago').html('Ocurrio un error en la comunicación con el servidor.');
						
							},
						success: function(rsp){
							$('#carga_forma_paga').hide(2000);



							if(rsp.err == true){
								if(option === 1){
								$('#msj_pago').removeClass('bg-blue');
								$('#msj_pago').css('background-color','#00a65a' );
								$('#msj_pago').html('Los datos fueron almacenados!.');
								$('#btnGuardarFormaPago').prop('disabled',true);
								$('#modalFormaPago').modal('hide');
								}
								if(option === 2){
								$('#msj_pago').removeClass('bg-blue');
								$('#msj_pago').css('background-color','#00a65a' );
								$('#btnGuardarFormaPago').prop('disabled',true);
								$('#msj_pago').html('Los datos fueron eliminados!.');


								$('#listadoPagos tbody').html('');
								let monto = $('#monto_proforma').val();
								$('#diferencia_pago').val(monto);

								var nFilas = $("#listadoPagos tr").length;

								}

							} else {
							}

						}
					});	
		
				setTimeout(actualizarDiferencia,1400);
			}//function

	

	$('#btnFormaPago').on('click',function(){
		getformaPago(2);
			});

	function getformaPago(option){

	$('#carga_forma_paga').show();	
	//LEVANTAR MODAL
	$('#modalFormaPago').modal('show');

	let primera_carga = 0;
	let flag_aux = 0;
	let dataString = { id_proforma :$('#proforma_id').val(), option:option};

		
	
		$.ajax({
					type: "GET",
					url: "{{route('getformaPago')}}",
					dataType: 'json',
					async:false,
					data: dataString,
						error: function(){
							$('#carga_forma_paga').hide(2000);
							$('#msj_pago').removeClass('bg-blue');
							$('#msj_pago').css('background-color','#C93F3F' );
							$('#msj_pago').html('Ocurrio un error en la comunicación con el servidor.');
						
							},
			success: function(rsp){

			$('#monto_proforma').val(formatter.format(parseFloat((rsp.monto_proforma > 0) ? rsp.monto_proforma : 0)));

			{{--SOLO SE PRODUCE 1 CARGA --}}
			if($('#primera_carga_bandera_id').val() === 'N'){ 

			if(rsp.get_fp.length > 0){
				$('#listadoPagos tbody').html('');		
				$('#primera_carga_bandera_id').val('S');
			    $('#diferencia_pago').val('0');
			    primera_carga++;
				
				$.each(rsp.get_fp, function(index, val) {
					let cantItem = $('#listadoPagos tr').length - 1;
					let tabla = `<tr id="lineaPagoEliminar_${cantItem}" class="tabla_filas">
					<td><input type="text" readonly value = "${val.forma_pago_txt}" class = "form-control input-sm" /></td>
					<td><input type="text" readonly value = "${val.documento}" class = "form-control input-sm" name="datos[][documento]"/></td>
					<td><input type="text" readonly value = "${formatter.format(parseFloat(val.importe_pago))}" class = "form-control input-sm" name="datos[][importePago]"/></td>
					<td><input type="text" readonly value = "${formatter.format(parseFloat(val.importe))}" class = "form-control input-sm" name="datos[][importe]"/></td>
					<td><input type="text" readonly value = "${val.currency_code}" class = "form-control input-sm"/></td>
					<td><button type="button" onclick="eliminarFilaPago(${cantItem})" class="btn btn-danger"><b>Eliminar</b></button>
					<input type="hidden"  value = "${val.id_forma_pago}"  class = "form-control input-sm" name="datos[][id_forma_pago]"/>
					<input type="hidden"  value = "${val.id_moneda}" class = "form-control input-sm" name="datos[][id_moneda_pago]"/>
					</td>
					</tr>
					`;
					$('#listadoPagos tbody').append(tabla);
				});//EACH
				

								}//IF
					} 

				{{--CALCULAR LA DIFERENCIA --}}	
				let form = 	$('#formFormaPago').serializeJSON();
				let sum_total = 0;

				$.each(form.datos, function(index,value){
					{{-- PARA SUMAR EN JAVASCRIPT SE DEBE QUITAR LAS COMAS Y USAR PUNTOS--}}
					dato = clean_num(value.importe);
					sum_total += dato;
					flag_aux++;
							})
				let total = clean_num(rsp.monto_proforma,true) - sum_total;
				$('#diferencia_pago').val(formatter.format(parseFloat(total.toFixed(2))));


				{{--VALIDAR MONTO EN OTRAS CARGAS --}}
				if(rsp.get_fp.length > 0 && primera_carga === 0 || flag_aux > 0){
					if(total != 0){
						$('#msj_pago').removeClass('bg-blue');
						$('#msj_pago').css('background-color','#C93F3F' );
						$('#msj_pago').html('La diferencia debe ser 0.');
						$('#btnGuardarFormaPago').prop('disabled',true);
					}
				 }



				//OCULTAR LOADING
				$('#carga_forma_paga').hide(2000);
							}//success
					});	//ajax	

                 var nFilas = $("#listadoPagos tr").length;

				if((nFilas - 1) != 0){
					$('#btnEliminarTodosPago').prop('disabled',false);	
					$('#btnGuardarFormaPago').prop('disabled',false);
				}else{
				   $('#btnEliminarTodosPago').prop('disabled',true);	
				   $('#btnGuardarFormaPago').prop('disabled',true);
			   }

	}//function
			
	 


		

/*-==============================================
			AVISO DE ACUERDO
	============================================== */

	function eventAviso(){

		let flat = "{{$acuerdo->err}}";
		if(flat){
			$('#modalAviso').modal('show');
			$('#acuerdo_id').val(`{{$acuerdo->acuerdo}}`);
		}

	}//
		$('.btn-traslado-base').click(function(){
			valor = $("#tipo_traslado_id").val();	
			switch(valor){
	    		case '1':
	    			$("#inDiv").css('display', 'block');
	    			$("#outDiv").css('display', 'none');
	    		break;
	    		case '2':
	    			$("#outDiv").css('display', 'block');
	    			$("#inDiv").css('display', 'none');
	    		break;
	    		case '3':
	    			$("#outDiv").css('display', 'block');
	    			$("#inDiv").css('display', 'block');
	    		break;
	    	} 

		});

		$('.btn-prestador-request').click(function(){
			$("#listadoPrestador").dataTable().fnDestroy();
			$("#listadoPrestador").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

		});	

		$('#nombrePrestador').change(function(){
				$('#nombrePrestador').css('border-color','#d2d6de');
				//if($('#formTipoPersona').val() == 15){
					$.ajax({
						type: "GET",
						url: "{{route('datosPrestadores')}}",//fileDelDTPlus
						dataType: 'json',
						data: {  
								dataFile:$('#nombrePrestador').val()
							},
						success: function(rsp){
								if(rsp == false){
									$.toast({
											heading: 'Error',
											position: 'top-right',
											text: 'El nombre de este prestador ya existe.',
											showHideTransition: 'fade',
											icon: 'error'
										});
									$('#nombrePrestador').css('border-color','#E7491F');
									$('#nombrePrestador').val('');
									$('#nombrePrestador').focus();
								}               
						}
					})
				//}    
		});



		$('#btnBuscarPrestador').click(function(){
			if($('#DestinoId').val() != 0){
				if($('#nombrePrestador').val().length != 0){
					var dataString = $("#frmPrestador").serialize();
					$.ajax({
					type: "GET",
					url: "{{route('buscarPrestador')}}",
					dataType: 'json',
					data: dataString,
							success: function(rsp){
									var oSettings = $('#listadoPrestador').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();
									for (i=0;i<=iTotalRecords;i++) {
										$('#listadoPrestador').dataTable().fnDeleteRow(0,null,true);
									}
									$.each(rsp, function (key, item){
										var accion = "<a href='#' data='"+item.id+"' class='btn btn-info text-center btn transaction_normal hide-small normal-button btnPrestador' style='padding-top: 5px; backgroundpadding-top: 5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;padding-bottom: 5px;' role='button'><i class='fa fa-check' aria-hidden='true'></i></a>";
								        var dataTableRow = [
															item.name,
															item.address,
															item.phone,
															accion
															];

										var newrow = $('#listadoPrestador').dataTable().fnAddData(dataTableRow);
										// set class attribute for the newly added row 
										var nTr = $('#listadoPrestador').dataTable().fnSettings().aoData[newrow[0]].nTr;
										// and parse the row:
										var nTds = $('td', nTr);
									})	

									guardarPrestador();

							}
					})	
				}else{
					$("#nombrePrestador").css('border-color', 'red');
					$.toast({
							heading: 'Error',
							text: 'Ingrese un Nombre.',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
						});				
				}	
			}else{
				$("#DestinoId").find('.select2-container--default .select2-selection--single').css('border','1px solid red');
					$.toast({
							heading: 'Error',
							text: 'Ingrese un Destino.',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
						});				
			}	
		});

		$('#btnVerificacion').click(function(){
			$('#modalVerificarRenta').modal('show');
 			/*	return swal({
                        title: "GESTUR",
                        text: "¿Está seguro de que desea solicitar la verificación de la proforma?",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, solicitar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
									swal("", "", "");
                                     verificarProforma();
                                } else {
                                     swal("Cancelado", "", "error");
                                }
            	});*/
        });

		$('#btnVerificacionRenta').click(function(){
			verificarProforma();
		});

		function verificarProforma()
		{
			var idProforma = $('#proforma_id').val();
			var dataString  = {'idProforma':idProforma};

			$.ajax({
					type: "GET",
					// url: "route('factour.verificarMarkup')",
					url: "{{route('verificarProforma')}}",
					dataType: 'json',
					data: dataString,
			       	error: function(jqXHR,textStatus,errorThrown){

		                   $.toast({
						    heading: 'Error',
						    text: 'Ocurrió un error en la comunicación con el servidor.',
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error',
						    hideAfter : 8000

						});
		                    $.unblockUI();

						},
					success: function(rsp){
						let msj = [];

						if(rsp.err){

							if(rsp.pendienteAuth.length > 0){
								
								$('#msjListaAutorizacion').html('');
								$.each(rsp.pendienteAuth,(index,value)=>{
									if(value == 1){
										$('#msjListaAutorizacion').append('<li type="disc"><h4>Este cliente requiere autorización de Administración para facturar.</h4></li>');
									}

									if(value == 2){
										$('#msjListaAutorizacion').append('<li type="disc"><h4>La renta esta por debajo del mínimo requerido por la Empresa.</h4></li>');
									}

									if(value == 3){
										$('#msjListaAutorizacion').append('<li type="disc"><h4 style="color:red"> Existen ítems que no cumplen con el mínimo requerido por la empresa, favor verifique.</h4></li>');
									}

								});

								
								$('#modalVerificarProforma').modal('show');
							
							} else {
								estadoProforma('2');
							}
							$("#modalVerificarRenta").modal('hide');
						} else {
							$.toast({
						    heading: 'Error',
						    text: 'Ocurrió un error al intentar verificar la proforma',
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error',
						    hideAfter : 8000

						});
		                    $.unblockUI();
							$("#modalVerificarRenta").modal('hide');
						}
		
					}	

			})
		}

			
	    $('input.timepicker').timepicker({
        									'timeFormat':'H:i'
   		 								});
	    $('.btn-traslado-request').click(function(){
	    	linea = $(this).attr('linea');
	    	$("#lineaTraslado").val(linea);
	    	$("#inDiv").css('display', 'none');
	    	$("#outDiv").css('display', 'none');
	    	$("#tipo_traslado_id").val(0).select2();
	    });	

		$('.btn-asistencia-request').click(function(){
			$("#lineaAsistencia").val($(this).attr('data'));
			$.ajax({
					type: "GET",
					url: "{{route('getAsistencia')}}",
					dataType: 'json',
					data: {
						dataDetalleProforma: $(this).attr('data'),
						dataProforma: $('#proforma_id').val()
			       			},
					success: function(rsp){
							$("#tipoAsistencia").val(0).select2();
							$("#personasAsistencia").val(rsp.cant_personas);
							$("#diasAsistencia").val(rsp.dias)
							$("#tipoAsistencia").val(rsp.tipo_asistencia).select2();
							}	
			});						
		});	

		$('.btn-asistencia-request-ua').click(function(){
			$("#lineaAsistencia").val($(this).attr('data'));
			$.ajax({
					type: "GET",
					url: "{{route('getAsistenciaUA')}}",
					dataType: 'json',
					data: {
						dataDetalleProforma: $(this).attr('data'),
						dataProforma: $('#proforma_id').val()
			       			},
					success: function(rsp){
							$("#tipoAsistenciaAU").val(0).select2();
							$("#personasAsistenciaAU").val(rsp.cant_personas);
							$("#diasAsistenciaAU").val(rsp.dias);
							$("#exentaAsistencia").val(rsp.exenta);
							$("#gravadaAsistencia").val(rsp.grabado);
							$("#tipoAsistenciaAU").val(rsp.tipo_asistencia).select2();
							}	
			});									
		})
			

		$('#btnAsistenciaUA').click(function(){
			if($("#tipoAsistenciaAU").val() != 0){
				if($("#diasAsistenciaAU").val() != "" && $("#diasAsistenciaAU").val() != 0 ){
					if($("#personasAsistenciaAU").val() != "" && $("#personasAsistenciaAU").val() != 0  ){
						if($("#exentaAsistencia").val() != "" && $("#exentaAsistencia").val() != 0  ){
							if($("#gravadaAsistencia").val() != "" && $("#gravadaAsistencia").val() != 0  ){
								$('#mensajaErrorSuperior').hide();
								var dataString = $("#frmAsistenciaAU").serialize();
								$.ajax({
										type: "GET",
										url: "{{route('guardarAsistenciaAU')}}",
										dataType: 'json',
										data: dataString,
										success: function(rsp){
														 	lineaAsistencia = $("#lineaAsistencia").val();
															$("#cantidad_personas_"+lineaAsistencia).val(rsp.cant_personas);
															$("#costo_"+lineaAsistencia).val(rsp.costo);
															$("#venta_"+lineaAsistencia).val(rsp.venta);
															$("#cantidad_dias_"+lineaAsistencia).val(rsp.dias);
															$("#tarifa_asistencia_"+lineaAsistencia).val(rsp.tipo_asistencia)
															$("#cantidad_personas_"+lineaAsistencia).prop("disabled", true);
															$("#confirmacion_"+lineaAsistencia).focus();
															$("#exentos_"+lineaAsistencia).val(rsp.exenta);
															$("#gravada_"+lineaAsistencia).val(rsp.gravada);
															$(".sale").trigger('change');
															$("#costo_"+lineaAsistencia).prop("disabled", true);
															$('#especificosDatos .collapse').collapse('hide');
														}
									});
							}else{
								$('#mensajaErrorSuperiorUA').html('Ingrese monto gravada');					
						   }	
						}else{
							$('#mensajaErrorSuperiorUA').html('Ingrese monto exenta');					
						}	
					}else{
						$('#mensajaErrorSuperiorUA').html('Ingrese personas en Asistencia');					
					}	
				}else{
					$('#mensajaErrorSuperiorUA').html('Ingrese los dias de Asistencia');					
				}	
			}else{
				$('#mensajaErrorSuperiorUA').html('Seleccione el tipo de Asistencia');					
			}		
		});


		$('#btnPedirAutorizacion').click(function(){
			$('#modalVerificarProforma').modal('toggle');
			estadoProforma('1');
		});


		function estadoProforma(option){

			$.blockUI({
                centerY: 0,
                message: "<h2>Procesando pedido...</h2>",
                css: {
                    color: '#000'
                }
            });

			var idProforma = $('#proforma_id').val();
			var ok = 0;
			var dataString  = {'idProforma':idProforma,'option': option };
			var mensaje = '';

		$.ajax({
					type: "GET",
					url: "{{route('solicitarVerificacionProforma')}}",
					dataType: 'json',
					data: dataString,
			       	error: function(jqXHR,textStatus,errorThrown){

		                   $.toast({
						    heading: 'Error',
						    text: 'Ocurrió un error en la comunicación con el servidor.',
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});
		                    $.unblockUI();

						},
					success: function(rsp)
					{
						if(rsp.err != 'true')
						{
							mensaje = rsp.mensaje;
							ok++;
						}

					}	

			}).done(function(){

					if(ok > 0){
						$.toast({
						    heading: 'Error',
						    text: mensaje,
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});
						 $.unblockUI();

					} else {

						if(option == '1'){ comentarioAuto('1');	}
						 $.unblockUI();
						 
						 $.blockUI({
			                centerY: 0,
			                message: "<h2>Pedido enviado...</h2>",
			                css: {
			                    color: '#000'
			                }
			            });

				 		 window.location.href = "{{route('listadosProformas')}}";
					}
			});
		}//fucntion

					function comentarioAuto(option){
						var idUsuario = $('#idUsuario').val();
                 		var idProforma = $('#proforma_id').val();
						var mensaje = '';

						if(option == '1'){mensaje = '====== Pedido de Autorizacion =====';}

						if(option == '2'){mensaje = '============ Autorizado ===========';}

						if(option == '3'){mensaje = '============ Solicitud Ticket =====';}

						var dataString = {"comentario": mensaje,"idUsuario":idUsuario,"idProforma":idProforma};

						guardarComentarioAjax(dataString);

   
			}//function

			$('input[name="periodo"]').daterangepicker({
							        //timePicker: true,
							        timePicker24Hour: true,
							        timePickerIncrement: 30,
							        locale: {
							            format: 'DD/MM/YYYY ',
							             cancelLabel: 'Cancelar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']

							        }
							    });

		
	        var datepicker = $('#vencimiento'); 
			    if (datepicker.length > 0) { 
			    datepicker.datepicker({ 
			     format: "dd/mm/yyyy", 
			       language: "es"
			    // startDate: new Date() 
			    }); 
			    } 
				$('#vencimiento').val('{{$proformas[0]->vencimiento}}');

		function keyPress(){
			$(".markup").change(function(){
				var ventaId = $(this).attr('id');
				var indice = ventaId.split('_');
				$.ajax({
					type: "GET",
					url: "{{route('calcularVenta')}}",
					dataType: 'json',
					data: {
							dataTasas: $('#tasas_'+indice[1]).val(),
							dataMonedaCompra: $('#moneda_compra_'+indice[1]).val(),
							dataCosto: $('#costo_'+indice[1]).val(),
							dataCurrency_venta: $('#moneda').val(),
							dataMarkup: $('#markup_'+indice[1]).val()
						},
							success: function(rsp){
								if(rsp > 0){
									$('#venta_'+indice[1]).val(rsp);
								}else{
									$('#venta_'+indice[1]).val(0);
								}

						}
				})		
			})

			// ESTEEEEEE
			$(".sale").change(function(){ 
				calcularMarkup();	
			})	
		}//keypress()

		pasajero = '{{$pasajeroProforma}}';
		//$("#pasajero_principal").val(pasajero).select2();
		$("#pasajero_principal2").val(pasajero).select2();

		var tarifa = '{{$id_tarifa}}';		
		$("#id_tarifa").val(tarifa).select2();

		valor = '{{$cliente_id}}';		
		$("#cliente_id").val(valor).select2();

		$.ajax({
					type: "GET",
					url: "{{route('getDatosCliente')}}",
					dataType: 'json',
					data: {
						dataCliente: valor
			       			},
					success: function(rsp){
						$("#tipo_cliente").val(rsp.tipo_persona);
						//$("#tipo_factura").val(rsp.tipo_facturacion);
						$("#linea_credito").val(rsp.limite_credito);
						$("#saldo_disponible").val(rsp.saldo_disponible);
						var vendedores = rsp.vendedores;
						if(vendedores.length != 0){
							$('#id_categoria').empty();
								var newOption = new Option('Seleccione Vendedor', 0, false, false);
								$('#id_categoria').append(newOption); 
							$.each(rsp.vendedores, function (key, item){
								var newOption = new Option(item.denominacion, item.id, false, false);
								$('#id_categoria').append(newOption);
								valors = '{{$vendedor_id}}';
								$("#id_categoria").val(valors).select2();

							})	
						}else{ 
							$('#id_categoria').select2({
									  disabled: true
							});
						}
						$("#id_categoria").trigger('change');
					}

			});		

		$("#pasajero_principal").change(function(){
			$("#pasajeroDTPmundo").css('display', 'none');
		});	


		monedaP = '{{$proformas[0]->id_moneda_venta}}';
		$("#moneda").val(monedaP).select2();

		$("#moneda_compra_0").val(143).select2();

		$('.quitar').click(function() { 
				$("#destino option:selected").each(function(){
					$.ajax({
							type: "GET",
							url: "{{route('actualizarPrecioTicket')}}",
							dataType: 'json',
							data: {
									dataIdTicket: $(this).val()
								},	
						success: function(rsp){
							var aRestar = parseFloat($("#total_ticket").val());
							var montoTicket = parseFloat(rsp[0].get_venta_ticket);
							total = aRestar - montoTicket;
							$("#total_ticket").val(total.toFixed(2));
							var aRestarTotal = parseFloat($("#totalVenta").val());
							total = aRestarTotal - montoTicket;
							$("#totalVenta").val(total.toFixed(2));

						}
							
					});
				})
			return !$('#destino option:selected').remove().appendTo('#origen'); 
		});

        $("#descripcion").change(function(){
	    	$("#destinos").select2().enable(false);
	     });
	     
	    $("#destinos").change(function(){
	    	$("#descripcion").select2().enable(false);
	     });

		$(document).ready(function() {
			var comentarioCalificacion = $('#comentarioPostVenta').val().trim();

// Verifica si el campo tiene contenido
if (comentarioCalificacion !== '') {
	// Si tiene contenido, oculta el botón de Calificar
	$('#btnCalificarPostVenta').hide();
}
			$('.select2').select2();
			$("#gravado_costo_0").prop("disabled", false);
			$('#requestModalDetalle').on('hidden.bs.modal', function(e) { 
					$('#producto_0').val(0).select2();
			});
			
			$('#botonPasajero').click(function(){
				$("#nombre").val('');
				$("#apellido").val('');
				$("#documento").val('');
			});

			$('#botonCliente').click(function(){
				$("#nombre_cliente").val('');
				$("#apellido_cliente").val('');
				$("#documento_cliente").val('');
				$("#direccion_cliente").val('');
				$("#telefono_cliente").val('');

			});


			$("#nombrePrestador").val('');

			var tipo_facturacion = '{{$proforma->tipo_facturacion}}';
			if (tipo_facturacion == 1) {
				$("#tipo_factura").val();
				$("#tipo_factura").val('Factura Bruta');
			}
			else{
				$("#tipo_factura").val();
				$("#tipo_factura").val('Factura Neta');
			}

				$("#id_expediente").select2({
							    ajax: {
							            url: "{{route('datosProforma')}}",
							            dataType: 'json',
							            placeholder: "Seleccione un Proforma",
							            delay: 0,
							            data: function (params) {
							                return {
							                    q: params.term, // search term
							                    page: params.page
							                };
							            },
							            processResults: function (data, params){
							                var results = $.map(data, function (value, key) {
							                    return {
							                        children: $.map(value, function (v) {
							                            return {
							                                id: key,
							                                text: v
							                            };
							                        })
							                    };
							                });
							                return {
							                    results: results,
							                };
							            },
							            cache: true
							        },
							        escapeMarkup: function (markup) {
							            return markup;
							        }, // let our custom formatter work
							        minimumInputLength: 5,
					});

					$("#destino_id").select2({
					    ajax: {
					            url: "{{route('destinoProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
						});

						/*prestador();
						select_proveedor();*/
				/*	$("#prestador_0").select2({
					    ajax: {
					            url: "{{route('prestadorProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
						});*/

				/*	$("#proveedor_0").select2({
					    ajax: {
					            url: "{{route('proveedorProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page,
										producto:$("#productoId_0").val()
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
						});*/

				$("#cliente_id_aux").select2({
					        ajax: {
					                url: "{{route('getClienteAuxiliar')}}",
					                dataType: 'json',
					                placeholder: "Seleccione un Cliente",
					                delay: 0,
					                data: function (params) {
					                            return {
					                                q: params.term, // search term
					                                page: params.page
					                                    };
					                },
					                processResults: function (data, params){
					                            var results = $.map(data, function (value, key) {
					                           /* return {
					                                    children: $.map(value, function (v) {*/ 
															var datos = value.pasajero_data;
															var documento = value.documento_identidad; 
															if(jQuery.isEmptyObject(value.dv) == false){
																var documento = value.documento_identidad+"-"+value.dv; 
															}
															resultado = documento +' _ '+datos;
					                                        return {
					                                                    id: value.id,
					                                                    text: resultado
					                                                };
					                                      /*  })
					                                    };*/
					                            });
					                                    return {
					                                        results: results,
					                                    };
					                },
					                cache: true
					                },
					                escapeMarkup: function (markup) {
					                                return markup;
					                }, // let our custom formatter work
					                minimumInputLength: 3,
					    });



				$("#destinos").select2({
					    ajax: {
					            url: "{{route('destinoProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
				});

				$("#destinoId").select2({
					    ajax: {
					            url: "{{route('destinoProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
				});

				$("#DestinoId").select2({
					    ajax: {
					            url: "{{route('destinoProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
				});	


		        var options = { 
		                        beforeSubmit:  showRequest,
		                		success: showResponse,
					            dataType: 'json' 
		                }; 
		        $('body').delegate('#image','change', function(){
		            $('#upload').ajaxForm(options).submit();      
		        });

				fechGastoProveeedor();

				$('.posterior').each(function (index, value) { 
						var producto = $(this).val();
						var idProducto = $(this).attr('id');
						var productoBase = idProducto.split('_');
				});	
				$("#indice_0").prop("disabled", false);

				$("#indice_0").focus();

	        	var datepicker = $('#vencimiento_0'); 
				    if (datepicker.length > 0) { 
											    datepicker.datepicker({ 
											     format: "dd/mm/yyyy",
											       language: "es"
											    // startDate: new Date() 
											    }); 
											} 
	        	
	        	var datepickers = $('.fecha'); 
				
				if (datepickers.length > 0) { 
											    datepickers.datepicker({ 
											     format: "dd/mm/yyyy",
											       language: "es"
											    // startDate: new Date() 
											    }); 
											} 

				//postInsert();
				keyPress();
		});

		function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

	   	$('input[name="tarjeta"]').click(function(){
	   		$('.codigoTarjeta').each(function (){
	   			$(this).css('display', 'none');
	   		})	
	   		var codigoTarjeta = $(this).attr('id').split('_');
	   		$('#columna_'+codigoTarjeta[1]).css('display', 'block');
	    })

		$("#indice_0").prop("disabled", false);
		$("#indice_0").focus();

	 	$("#moneda_compra_0").focusout(function(){
	   		$("#moneda_compra_0").select2('open');
	   	}) 

		$("#periodo_0").change(function(){
		   		//$("#proveedor_0").focus();
		   		if(parseInt($("#grupo").val()) == 3){
					$.ajax({
							type: "GET",
							url: "{{route('getPrecioAsistencia')}}",
							dataType: 'json',
							data: {
								dataTipoAsistencia: $("#tipoAsistencia").val(),
								dataPeriodo: $("#periodo_0").val(),
								dataCantidad: $("#personasAsistencia").val()
								},
							success: function(rsp){		  
								$("#cantidad_personas_0").val(rsp.cant_personas);
								$("#costo_0").val(rsp.costo);
								$("#cantidad_dias_0").val(rsp.dias);
								$("#tarifa_asistencia_0").val(rsp.tipo_asistencia);							
							}
					})	
				}
		   	})

		//===== GUARDAR LINEA DE PROFORMA ====
		//
		function guardarProformaDetalle(){
			// console.log($("#proveedor_id_0").val());
			// console.log($("#prestador_0").val());
				$('.ocultado').empty();
				$('.ocultado').hide();
				$("#periodo_0").prop("disabled", false);
				var venta = $("#venta_0").val();
				var markup = $('#markup_0').val();
				if($("#descrip_factura_0").val() != ""){
					$('#descrip_factura_0').css('border-color', '#d2d6de');	
					if($("#confirmacion_0").val() != null && $("#confirmacion_0").val() != 0){ 
						$('#confirmacion_0').css('border-color', '#d2d6de');	
						if($("#proveedor_id_0").val() != null && $("#proveedor_id_0").val() != 0){ 
							$('#proveedor_0').find('.select2-container--default .select2-selection--single').css('border-color','d2d6de');
							if($("#prestador_id_0").val() != null && $("#prestador_id_0").val() != 0){ 
								$('#prestador_0').find('.select2-container--default .select2-selection--single').css('border-color','d2d6de');
									$('#costo_0').css('border-color', '#d2d6de'); 
										if($("#periodo_0").val() != null && $("#periodo_0").val() != 0){ 	
												$('#comision_operador_0').css('border-color', '#d2d6de');  
												$('#markup_0').css('border-color', '#d2d6de');  
												$('#proveedor_0').prop("disabled", false);
												$('#prestador_0').prop("disabled", false);
												$('#cantidad_personas_0').prop("disabled", false);
												procesarndo();
												//RETRASO PARA GUARDAR LA FILA DE LA PROFORMA</br>
												var nFilas = $("#lista_productos tr:visible").length;
												var lineasGuardadas = nFilas - 1;
												if(lineasGuardadas <= 15){
													var idEmpresaBase = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}"
													if(idEmpresaBase != 'V'){ 
														if($("#tipo_0s").val() == 1){
															if($("#comision_operador_0").val() <= 0){
																$("#comision_operador_0").css('border-color', 'red');
																$.unblockUI();
																$.toast({
																	heading: 'Error',
																	text: 'La comisión no puede ser 0.',
																	showHideTransition: 'fade',
																	position: 'top-right',
																	icon: 'error'
																});
															}else{
																$('#gravado_costo_0').css('border-color', '#d2d6de')
																guardarDetalle();
															}
														}else{	
															$('#gravado_costo_0').css('border-color', '#d2d6de')
															guardarDetalle();
														}	
													}else{
														$('#gravado_costo_0').css('border-color', '#d2d6de')
														guardarDetalle();
													}								
												}else{
														$.unblockUI();
														$.toast({
															heading: 'Error',
															text: 'Se ha ingresado la máxima cantidad de items en el Detalle de Proforma.',
															showHideTransition: 'fade',
															position: 'top-right',
															icon: 'error'
														});									
													}
											} else {
												$('#periodo_0').css('border-color', 'red');
												$.toast({
														heading: 'Error',
														text: 'Ingrese la Fecha In/Out.',
														showHideTransition: 'fade',
														position: 'top-right',
														icon: 'error'
													});
											}			
							}else{
								$("#prestadorO").find('.select2-container--default .select2-selection--single').css('border','1px solid red');
								$.toast({
										heading: 'Error',
										text: 'Ingrese un Prestador.',
										showHideTransition: 'fade',
										position: 'top-right',
										icon: 'error'
									});
							}
						}else{
							$("#proveedorO").find('.select2-container--default .select2-selection--single').css('border','1px solid red');
									$.toast({
										heading: 'Error',
										text: 'Ingrese un Proveedor.',
										showHideTransition: 'fade',
										position: 'top-right',
										icon: 'error'
									});
						}
					}else{
						$("#confirmacion_0").css('border-color', 'red');
						$.toast({
								heading: 'Error',
								text: 'Ingrese el Codigo de Confirmación.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});
					}	
				}else{
					$("#descrip_factura_0").css('border-color', 'red');
					$.toast({
								heading: 'Error',
								text: 'Ingrese el Detalle de la Factura.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});
			 }	
		}

		$("#indice_0").prop("disabled", false);
	    $("#indice_0").focus();

	    $('.cod_autorizacion').change(function(){
	    	tarjeta = $(this).attr('id')
	    	var inicio =  tarjeta.split('_');
	   		$('#tarjeta_id_'+$('#idLinea').val()).val('');
	   		$('#cod_autorizacion_'+$('#idLinea').val()).val('');
	   		$('#tarjeta_id_'+$('#idLinea').val()).val(inicio[2]);
	   		$('#cod_autorizacion_'+$('#idLinea').val()).val($(this).val());
	   	});

	    $('#btnCancelarVSTour').click(function(){
	   		$('#btnAceptarVoucher').hide();
	    });	

		$("#comision_agencia_0").change(function(){
			comision_agencia = $(this).val();
			if(parseFloat(comision_agencia) > parseFloat($('#com_agencia_0').val())){
				$("#comision_agencia_0").val($('#com_agencia_0').val());
				$("#comision_agencia_0").focus();
				$.toast({
						heading: 'Error',
						text: 'La comisión supera la comisión pactada.',
						showHideTransition: 'fade',
						position: 'top-right',
						icon: 'error'
					});
			}
		})
			
		$("#vencimiento_0").change(function(){
	   		$("#vencimientos_"+$('#base').val()).val($(this).val());
	   	})


		//=========SELECT PRODUCTOS =========
	   	$('.producto').change(function(){
			$('#reservaNemo').css('display', 'none');   
			$('#reservaCangoroo').css('display', 'none');   
			
	   		if($(this).val() != 0){
				//$('#requestModalDetalle').modal({backdrop: 'static', keyboard: false});
		   		$("#requestModalDetalle").modal('show');
		   		$("#editar").css('display', 'none');
				$("#guardar").css('display', 'block');
				linea = $(this).attr('id')
		    	var lineaNro =  linea.split('_');	   
		    	$('#linea').val(lineaNro[1]);
				$("#cantidad_0").val(0);
		    	errase();
		    	$('#especificosDatos').css('display', 'none');
				perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
				if(perfil == 1){
						$('#gravado_costo_0').prop("disabled", true);
				}else{
						$('#gravado_costo_0').prop("disabled", false);
				}
				$("#tickets_0").val('');
		    	$('#item').val($('#indice_0').val()); 
				$("#cantidad_0").val($("#cant_0").val());
		   		var producto = $(this).val(); 
		   		$('#productoDescripcion').html($('#producto_0 :selected').text());
		   		$('#productoDescripcion').prop("disabled", true);
		   		$('#productoId_0').val(producto);
		   		$('#idEspecifico').html('');
				$("#producto_0").select2("close");
				console.log('1');
				prestador();
				select_proveedor();
				change_producto();
				datosEspecificos(producto, lineaNro[1]);	
				datosItem();
		   	}	
	   	});	


		function inizializateVuelo(){
			$('#vuelos').dataTable({
				 "aaSorting":[[0,"desc"]],
				// "bLengthChange" : false,
				 "pageLength": 5,
				});

			$("#tipo").change(function(){
				$.ajax({
							type: "GET",
							url: "{{route('filtrarTicketTabla')}}",
							dataType: 'json',
							data: {
									dataTipoId: $(this).val(),
									idMonedaProforma: $("#moneda").val()
								},	
						success: function(rsp){
										var oSettings = $('#vuelos').dataTable().fnSettings();
										var iTotalRecords = oSettings.fnRecordsTotal();
										for (i=0;i<=iTotalRecords;i++) {
											$('#vuelos').dataTable().fnDeleteRow(0,null,true);
										}

										$.each(rsp, function (key, item){
										var checkbox = "<input type='checkbox' id='"+item.id+"' style='width: 15px;height: 25px;' class='form-control listOrigen "+item.id_proveedor+"' indice='"+item.id_grupo+"' data='"+item.id_proveedor+"' comision='"+item.comision+"' currency = '"+item.id_currency_costo+"' onclick='pasar("+item.id+")'  value='"+item.id+"' name='tickets["+item.id+"]'>";
											var dataTableRow = [
																	checkbox,
																	item.nro_ticket,
																	item.pnr,
																	item.pasajero,
																	item.moneda,
																	item.venta,
																	item.tarifa,
																	item.aerolinea,
																	item.grupo
																];
											var newrow = $('#vuelos').dataTable().fnAddData(dataTableRow);
											// set class attribute for the newly added row 
											var nTr = $('#vuelos').dataTable().fnSettings().aoData[newrow[0]].nTr;
											// and parse the row:
											var nTds = $('td', nTr);
											nTds.css('padding-top','5px');
											nTds.css('padding-left', '0px');
											nTds.css('padding-right','0px');
											nTds.css('padding-bottom','5px');
											$('#vuelos').dataTable().fnDraw();
										});	
									}
				 	});		
			 });	


			$("#procesar").click(function(){ 
				var contadorx = 0;
				$(".listOrigen").each(function() {
					if($('#'+$(this).attr('id')).prop('checked') ){
						contadorx++;
					}
				});		
				if(contadorx != 0 ){

					var linea = 0;
					var dataString = $("#datosEspecificos").serialize();
					$.ajax({
							type: "GET",
							url: "{{route('procesarTicket')}}",
							dataType: 'json',
							data: dataString+"&idGrupo="+$("#grupo_id").val(),
							success: function(rsp){
										if(rsp.status == 'OK'){
											limpiarTiket(linea)			
											$("#tickets_0").val('');							
											$("#tickets_"+linea).val(rsp.tickets);	
											$('#cantidad_personas_'+linea).val(rsp.cantidad);
											$('#costo_'+linea).val(rsp.costo);
											$('#markup_'+linea).val(rsp.markup);
											$('#venta_'+linea).prop("disabled", true);	
											$('#venta_'+linea).val(rsp.monto);
											$('#tasas_'+linea).val(rsp.tasa);
											$('#confirmacion_'+linea).val(rsp.codigo);
											$('#descripcion_'+linea).val(rsp.determinada);
											$('#descrip_factura_0').val(rsp.determinada);
											$('#comision_agencia_'+linea).val(rsp.comision_agencia);
											$('#moneda_compra_'+linea).val(rsp.moneda).select2();
											/*$('#proveedor_'+linea).val(rsp.proveedor).select2();
											$('#prestador_'+linea).val(rsp.prestador).select2();*/
											prestador();
											select_proveedor();
											$('#proveedor_id_0').val(rsp.proveedor);
											$('#prestador_0').val(rsp.prestador);
											$('#proveedor_0').val(rsp.proveedor);
											$('#prestador_id_0').val(rsp.prestador);
											$('#select2-proveedor_0-container').html(rsp.proveedor_nombre);
											$('#select2-prestador_0-container').html(rsp.prestador_nombre);
											$('#confirmacion_'+linea).prop("disabled", true);
											$('#proveedor_'+linea).prop("disabled", true);
											$('#costo_'+linea).prop("disabled", true);
											$('#prestador_'+linea).prop("disabled", true);
											$('#comision_agencia_'+linea).prop("disabled", true);
											$('#moneda_compra_'+linea).prop("disabled", true);
											$('#cantidad_personas_'+linea).prop("disabled", true);
											$('#cantidad_personas_'+linea).prop("disabled", true);
											$('#gravado_costo_'+linea).prop("disabled", true);
											$('#venta_'+linea).prop("disabled", false);
											$('#markup_'+linea).prop("disabled", false);
											$('#tasas_'+linea).prop("disabled", true);
											$('#vencimientos').val("{{date('d/m/Y')}}");
											$('#comision_agencia_'+linea).prop("disabled", false);
											perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
											if(perfil == 1){
												$('#vencimientos').prop("disabled", true);
												$('#fecha_gasto_'+linea).val("{{date('d/m/Y')}}");
												$('#fecha_gasto_'+linea).prop("disabled", true);
												$('#monto_gasto_'+linea).val(rsp.monto);
												$('#monto_gasto_'+linea).prop("disabled", true);
											}
											if(jQuery.isEmptyObject(rsp.checkIn) == false && jQuery.isEmptyObject(rsp.checkOut) == false){
												$('#periodo_'+linea).val(rsp.checkIn+' - '+rsp.checkOut);
												$('#periodo_'+linea).prop("disabled", true);
											}else{
												$('#periodo_'+linea).val('');
												$('#periodo_'+linea).focus();
											}
											$('#especificosDatos .collapse').collapse('hide');
										}
							}
						})
				}else{
					alert('Seleccione un Ticket');
				}	
			 });


			$('.fee').keyup(function(){
				total= parseFloat($('#total_ticket').val());
				if($(this).val() == ""){
					monto = 0;
				}else{
					montoBase = $(this).val().replace('.', '');
					montoBase2 = montoBase.replace(',', '.');
					monto = parseFloat(montoBase2);
				}
				totalVenta = total + monto;
				$('#totalVenta').val(totalVenta);
				$("#fee_0").val(monto);
			})
		}

		function limpiarTiket(linea){
			$('#confirmacion_'+linea).prop("disabled", false);
			$('#proveedor_'+linea).prop("disabled", false);
			$('#costo_'+linea).prop("disabled", false);
			$('#prestador_'+linea).prop("disabled", false);
			$('#comision_agencia_'+linea).prop("disabled", false);
			$('#moneda_compra_'+linea).prop("disabled", false);
			$('#cantidad_personas_'+linea).prop("disabled", false);
			$('#cantidad_personas_'+linea).prop("disabled", false);
			$('#gravado_costo_'+linea).prop("disabled", true);
			$('#venta_'+linea).prop("disabled", false);
			$('#markup_'+linea).prop("disabled", false);
			$('#tasas_'+linea).prop("disabled", false);
			$("#tickets_0").prop("disabled", false);
			$("#tickets_0").val('');
			$('#comision_agencia_'+linea).prop("disabled", false);
			$('#periodo_'+linea).prop("disabled", false);
		}	

		function inizializateTraslado(){
			$("#tipo_traslado_id").val(0).select2(); 
			//$('#lineaTraslado').val(lineaNro[1]);
			$("#inDiv").css('display', 'none');
			$("#outDiv").css('display', 'none');
			$("#in").val('');
			$("#out").val('');
			$("#vuelo_in").val('');
			$("#vuelo_out").val('');
			$("#horaIn").val('');
			$("#horaOut").val('');
		    $('.fechaHora').datepicker({ 
			     format: "dd/mm/yyyy", 
			     language: "es"
			   //  startDate: new Date() 
			    }); 
			anexoTraslado()
		}
		
		function anexoTraslado(){
		   $('#tipo_traslado_id').change(function(){
		    	valor = $(this).val();
		    	$("#inDiv").css('display', 'none');
		    	$("#outDiv").css('display', 'none');
		    	$("#in").val('');
		    	$("#out").val('');
		    	linea = $('#linea').val();
		    	$("#vuelo_in").val('');
		    	$("#vuelo_out").val('');
		    	$("#horaIn").val('');
		    	$("#horaOut").val('');
				$("#costo_0").prop("disabled", false);
		    	switch(valor){
		    		case '1':
		    			$("#inDiv").css('display', 'block');
		    			$("#outDiv").css('display', 'none');
		    		break;
		    		case '2':
		    			$("#outDiv").css('display', 'block');
		    			$("#inDiv").css('display', 'none');
		    		break;
		    		case '3':
		    			$("#outDiv").css('display', 'block');
		    			$("#inDiv").css('display', 'block');
		    		break;
		    	}

	    	});	

			$('#btnActualizarTraslado').click(function(){
				if($('#tipo_traslado_id').val() != 0){
					$("#horaIn_"+$('#linea').val()).val('');
					$("#horaOut_"+$('#linea').val()).val('');
					$("#vueloIn_"+$('#linea').val()).val('');
					$("#vueloOut_"+$('#linea').val()).val('');

					$("#idTipoTraslado_"+$('#linea').val()).val($('#tipo_traslado_id').val());
					$("#horaIn_"+$('#linea').val()).val($('#horaIn').val());
					$("#horaOut_"+$('#linea').val()).val($('#horaOut').val());
					$("#vueloIn_"+$('#linea').val()).val($('#vuelo_in').val());
					$("#vueloOut_"+$('#linea').val()).val($('#vuelo_out').val());
					valor = $('#tipo_traslado_id').val();
					switch(valor){
						case '1':
							periodo = $("#in").val()+" - "+$("#in").val();
						break;
						case '2':
							periodo = $("#out").val()+" - "+$("#out").val();
						break;
						case '3':
							periodo = $("#in").val()+" - "+$("#out").val();
						break;
					}
					$('#guardar').prop("disabled", false);
					$("#costo_0").prop("disabled", false);
					$("#periodo_"+$("#linea").val()).val(periodo);	
					$("#periodo_"+$("#linea").val()).prop("disabled", true);
					$("#cantidad_personas_"+$("#linea").val()).focus();	
					$('#especificosDatos .collapse').collapse('hide');
				}else{
					$.toast({
		     				heading: 'Error',
		     				position: 'top-right',
		     				text: 'Seleccione un Tipo de Traslado.',
		     				showHideTransition: 'fade',
		     				icon: 'error'
		     			});
				}
			});		
		}   

		function inizializateAsistencia(producto){
			$("#tipoAsistencia").select2();
			$("#productoAsisten").val(producto);
			$("#tipoAsistencia").val(0).select2();
			$('input[name="in_out"]').daterangepicker({
													        timePicker24Hour: true,
													        timePickerIncrement: 30,
													        locale: {
													    format: 'DD/MM/YYYY',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													        },
													        minDate: new Date()
								    					});

			@if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'A')
				$("#costo_0").prop("disabled", true);
			@endif
			$('#btnAsistencia').click(function(){
				if($("#tipoAsistencia").val() != 0){
					if($("#personasAsistencia").val() != "" && $("#personasAsistencia").val() != 0  ){
						$('#mensajaErrorSuperior').addClass('hidden');
						var dataString = $("#datosEspecificos").serialize();
						$.ajax({
								type: "GET",
								url: "{{route('guardarAsistencia')}}",
								dataType: 'json',
								data: dataString,
								success: function(rsp){
										lineaAsistencia = $("#linea").val();
										$("#cantidad_personas_"+lineaAsistencia).val(rsp.cant_personas);
										$("#costo_"+lineaAsistencia).val(rsp.costo);
										$("#cantidad_dias_"+lineaAsistencia).val(rsp.dias);
										$("#tarifa_asistencia_"+lineaAsistencia).val(rsp.tipo_asistencia);
										$("#confirmacion_"+lineaAsistencia).focus();
										$('#especificosDatos .collapse').collapse('hide');
									}
						});
					}else{
						$('#mensajaErrorSuperior').removeClass('hidden');
						$('#mensajeAlertaSuperior').html('Ingrese personas en Asistencia');					
					}	
					if($("#periodo_0").val() != ""){

						$.ajax({
								type: "GET",
								url: "{{route('getPrecioAsistencia')}}",
								dataType: 'json',
								data: {
									dataTipoAsistencia: $("#tipoAsistencia").val(),
									dataPeriodo: $("#periodo_0").val(),
									dataCantidad: $("#personasAsistencia").val()
									},
								success: function(rsp){		  
									$("#cantidad_personas_0").val(rsp.cant_personas);
									$("#costo_0").val(rsp.costo);
									$("#cantidad_dias_0").val(rsp.dias);
									$("#tarifa_asistencia_0").val(rsp.tipo_asistencia);							
								}
						})
					}	
				}else{
					$('#mensajaErrorSuperior').removeClass('hidden');
					$('#mensajeAlertaSuperior').html('Seleccione el tipo de Asistencia');					
				}		
			});

		 }


		function pasar(id){
			if($('#'+id).prop('checked') ){
				var contador = 0;
				var indice = 0;
				var contadorx = 0;
				$(".listOrigen").each(function() {
					if($('#'+$(this).attr('id')).prop('checked') ){
						if($(this).attr('data') != $('#'+id).attr('data') || $(this).attr('currency') != $('#'+id).attr('currency') || $(this).attr('comision') != $('#'+id).attr('comision')){	
							$('#'+id).prop("checked", false); 
					   		$('#columna_'+id).css('background-color', '#F1D6D6');	
					   		contador = contador + 1;
					   		$('#ticket_'+id).remove();
						}else{
							$('#columna_'+id).css('background-color', '#FFF');
							intputSelect = '<input type="hidden" id ="ticket_'+id+'" class = "Requerido form-control input-sm" value="'+id+'" name="ticket['+id+']"/>'	
							$('#ticketsSelect').append(intputSelect);
						}	
					}
				})	
				if(contador == 0){
					$.ajax({
							type: "GET",
							url: "{{route('actualizarPrecioTicket')}}",
							dataType: 'json',
							data: {
									dataIdTicket: $('#'+id).val()
								},	
							success: function(rsp){
									totalTicket = parseFloat($("#total_ticket").val()) + parseFloat(rsp[0].get_venta_ticket);
									totalUnico = parseFloat($('#fee_unico').val());
									finalTotal = totalTicket + totalUnico;
									$("#total_ticket").val(totalTicket.toFixed(2));	
									$("#totalVenta").val(finalTotal.toFixed(2));	
								}
							});

				}

			}else{
					$.ajax({
							type: "GET",
							url: "{{route('actualizarPrecioTicket')}}",
							dataType: 'json',
							data: {
									dataIdTicket: $('#'+id).val()
								},	
							success: function(rsp){
									totalTicket = parseFloat($("#total_ticket").val()) - parseFloat(rsp[0].get_venta_ticket);
									totalUnico = parseFloat($('#fee_unico').val());
									finalTotal = totalTicket + totalUnico;
									$("#total_ticket").val(totalTicket.toFixed(2));	
									$("#totalVenta").val(finalTotal);	
								}
							});
						$('#ticket_'+id).remove();
			}


		}


		$('.btn-tarjeta-request').click(function(){ 
			$('#idLinea').val($(this).attr('data'));
			$('#cod_aut_1').val('');
		})	

		function posProveedorPrestador(base, proveedor, prestador){
			$('#proveedor_'+base).val(parseInt(proveedor)).select2();
			$('#prestador_'+base).val(parseInt(prestador)).select2();
		}	

		function postInsert(){

			$('.fee').keyup(function(){
				total= parseFloat($('#total_ticket').val());
				if($(this).val() == ""){
					monto = 0;
				}else{
					montoBase = $(this).val().replace('.', '');
					montoBase2 = montoBase.replace(',', '.');
					monto = parseFloat(montoBase2);
				}
				totalVenta = total + monto;
				$('#totalVenta').val(totalVenta);
			})

			$("#indice_0").prop("disabled", false);	
			$("#indice_0").focus();
			$('.btn-traslado-base').css('display', 'none');
			$('.btn-ticket-base').css('display', 'none');			

			$('.btn-traslado-request').click(function(){ 
				$("#inDiv").css('display', 'none');
		    	$("#outDiv").css('display', 'none');
		    	$("#in").val('');
		    	$("#out").val('');
			    $("#horaIn_"+$("#lineaTraslado").val()).val('');
			    $("#horaOut_"+$("#lineaTraslado").val()).val('');
			    $("#vueloIn_"+$("#lineaTraslado").val()).val('');
			    $("#vueloOut_"+$("#lineaTraslado").val()).val('');
				$.ajax({
						type: "GET",
						url: "{{route('getTrasladorEditar')}}",
						dataType: 'json',
						data: {
								dataData: $(this).attr('linea'),
								dataProforma: $('#proforma_id').val()
					       	},
						success: function(rsp){	
								$("#tipo_traslado_id").val(rsp.id_tipo_traslado).select2();
								$('#tipo_traslado_id').trigger('change');
								
								if(jQuery.isEmptyObject(rsp.fecha_in) == false){
									var fechaIn = rsp.fecha_in;
									var $In = fechaIn.split('-');
									fecha_in = $In[2]+"/"+$In[1]+"/"+$In[0];
								}else{
									fecha_in = "";
								}	

								if(jQuery.isEmptyObject(rsp.fecha_out) == false){
									var fechaOut = rsp.fecha_out;
									var Out = fechaOut.split('-');
									fecha_out = Out[2]+"/"+Out[1]+"/"+Out[0];
								}else{
									fecha_out ="";
								}	

								var horaIn = rsp.hora_in;

								if(jQuery.isEmptyObject(horaIn) == false){
									var hora = horaIn.split('-');
									var horaBase = hora[0];
								}else{
									var horaBase = "";
								}	
								$("#in").val(fecha_in);
								$("#out").val(fecha_out);
								$("#horaIn").val(horaBase);
								$("#horaOut").val(rsp.hora_out);
								$("#vuelo_in").val(rsp.vuelo_in);
								$("#vuelo_out").val(rsp.vuelo_out);
						}	
				})			
			})
				
			$('.btn-asistencia-request').click(function(){
				$("#lineaAsistencia").val($(this).attr('data'));
				$.ajax({
						type: "GET",
						url: "{{route('getAsistencia')}}",
						dataType: 'json',
						data: {
							dataDetalleProforma: $(this).attr('data'),
							dataProforma: $('#proforma_id').val()
				       			},
						success: function(rsp){
								$("#tipoAsistenciaAU").val(0).select2();
								$("#personasAsistencia").val(rsp.cant_personas);
								$("#diasAsistencia").val(rsp.dias)
							    $("#exentaAsistencia").val(rsp.exenta);
							    $("#gravadaAsistencia").val(rsp.grabado);
								$("#tipoAsistenciaAU").val(rsp.tipo_asistencia).select2();
								}	
				});						
			});	

			$('.btn-asistencia-request-ua').click(function(){
				$("#lineaAsistencia").val($(this).attr('data'));
				$.ajax({
						type: "GET",
						url: "{{route('getAsistenciaUA')}}",
						dataType: 'json',
						data: {
							dataDetalleProforma: $(this).attr('data'),
							dataProforma: $('#proforma_id').val()
				       			},
						success: function(rsp){
								$("#tipoAsistenciaAU").val(0).select2();
								$("#personasAsistenciaAU").val(rsp.cant_personas);
								$("#diasAsistenciaAU").val(rsp.dias);
								$("#exentaAsistencia").val(rsp.exenta);
								$("#gravadaAsistencia").val(rsp.grabado);
								$("#tipoAsistenciaAU").val(rsp.tipo_asistencia).select2();
								}	
				});									
			})

		    $('.btn-traslado-request').click(function(){
		    	linea = $(this).attr('linea');
		    	$("#lineaTraslado").val(linea);
		    	$("#inDiv").css('display', 'none');
		    	$("#outDiv").css('display', 'none');
		    	$("#tipo_traslado_id").val(0).select2();
		    });	

			$('.btn-ticket-request').click(function(){ 
				$("#aerolinea").select2().val(0).trigger("change");
				$("#pasajero").val('');
				$("#nro_ticket").val('');
				$("#pnr").val('');
				$("#total_ticket").val(0);
				$("#fee_unico").val(0);
				$("#totalVenta").val(0);
				$("#tipo").select2().val(0).trigger("change");
				$("#salida_grupo").css('display', 'none');
				$("#salida_grupos").css('display', 'none');

				var linea = $(this).attr('linea');
				$('#linea').val(linea);

			    $('#origen').empty();

				$.ajax({
						type: "GET",
						url: "{{route('getTicketEditar')}}",
						dataType: 'json',
						data: {
								dataData: $(this).attr('data'),
								dataProforma: $('#proforma_id').val()
					       	},
						success: function(rsp){
								option = "";
								totalTicket = 0;
								$.each(rsp, function (key, item){
									$('#destino').empty();
									if(item.numero_amadeus !== null){
										numeroT = item.numero_amadeus;
									}else{
										numeroT = '';
									}
									if(item.pasajero !== null){
										pasajeroT = item.pasajero;
									}else{
										pasajeroT = '';
									}
									
									if(item.comision != 0){
										comision = 'Comisionable';
									}else{
										comision = 'Neto';
									}	
									
									if(item.persona.nombre !== null){
										proveedor = item.persona.nombre;
									}else{
										proveedor = '';
									}	

									$('#destino').val();
										if(jQuery.isEmptyObject(item.currency.currency_code) != false){
													valor = item.currency.currency_code
												}else{
													valor = "";	
												}
									option += '<option class="form-control listOrigen" indice="'+item.id_grupo+'" onclick="return cambio('+item.id_grupo+');" data="'+item.id_proveedor+'" comision="'+item.comision+'" currency ="'+item.id_currency_costo+'" style="align-items:center;" value="'+item.id+'">&nbsp&nbsp&nbsp&nbsp'+numeroT+'&nbsp&nbsp&nbsp&nbsp'+pasajeroT+'&nbsp&nbsp&nbsp&nbsp'+item.pnr+'&nbsp&nbsp&nbsp&nbsp'+item.precio_venta+'&nbsp&nbsp&nbsp&nbsp'+valor+'&nbsp&nbsp&nbsp&nbsp<br>'+proveedor+'</br>&nbsp&nbsp&nbsp&nbsp'+comision+'</option>';
									$('#origen option[value="'+item.id+'"]').remove();

									totalTicket = totalTicket + parseFloat(item.precio_venta);
									finalTotal = rsp[0].totalDetalle;
								
								})
								var fee = parseFloat(finalTotal) - parseFloat(totalTicket);
								$('#fee_unico').val(fee.toFixed(2));
								$("#total_ticket").val(totalTicket.toFixed(2));	
							    $("#totalVenta").val(finalTotal);

								$('#destino').append(option);
						}		
					});
			})	

		    $('.vencimientos').change(function(){
				var id = $(this).attr('id');
				var baseId = id.split('_');
		   		$("#vencimientos_"+baseId[1]).val($(this).val());
		   	})

			$("#tipoAsistencia").change(function(){
					$.ajax({
							type: "GET",
							url: "{{route('getPrecioAsistencia')}}",
							dataType: 'json',
							data: {
								dataTipoAsistencia: $("#tipoAsistencia").val(),
								dataPeriodo: $("#periodo_0").val(),
								dataCantidad: $("#personasAsistencia").val()
								},
							success: function(rsp){		  
								$("#cantidad_personas_0").val(rsp.cant_personas);
								$("#costo_0").val(rsp.costo);
								$("#cantidad_dias_0").val(rsp.dias);
								$("#tarifa_asistencia_0").val(rsp.tipo_asistencia);							
							}
					})	
			})

			$('.btn-descripcion-request').click(function(){ 
				$('#idLineaDesc').val($(this).attr('data'));
			})	

			$('.btn-descripcion-request').click(function(){ 
				if($(this).attr('data') != 0){
					$.ajax({
							type: "GET",
							url: "{{route('getDescripcion')}}",
							dataType: 'json',
							data: {
									dataData: $(this).attr('data'),
									dataProforma: $('#proforma_id').val()
								    },
							success: function(rsp) {	
												if($('#editable').val() == '1'){
													$('#descrip_factura_0').prop("disabled", false);
													$('#btnGuardarDescripcion').prop("disabled", false);
												}else{
													$('#descrip_factura_0').prop("disabled", true);
													$('#btnGuardarDescripcion').prop("disabled", true);
												}	
												$('#descrip_factura_0').val(rsp.mensaje)
													}	
					})	
				}else{
					$('#descrip_factura_0').prop("disabled", false);
					$('#btnGuardarDescripcion').prop("disabled", false);
				}	
			})	

			$('#btnGuardarDescripcion').click(function(){
				var linea = $('#idLineaDesc').val();
				var descripcion = $('#descrip_factura_0').val();
				$('#descripcion_'+linea).val(descripcion);
			})	

		   $('.cod_autorizacion').change(function(){
		   		tarjeta = $(this).attr('id')
	    		var inicio =  tarjeta.split('_');

		   		$('#tarjeta_id_'+$('#idLinea').val()).val('');
		   		$('#cod_autorizacion_'+$('#idLinea').val()).val('');
		   		$('#tarjeta_id_'+$('#idLinea').val()).val(inicio[2]);
		   		$('#cod_autorizacion_'+$('#idLinea').val()).val($(this).val());
		   	});

			$('.btn-cancelar-request').click(function(){ 
				$('#editable').val(0);
				$('table#lista_productos tr#'+$(this).attr('indice')).find('input,select,select2,textarea').prop("disabled", true);
				$('#producto_'+$(this).attr('indice')).select2().enable(false);
				$('#proveedor_'+$(this).attr('indice')).select2().enable(false);						
				$('#prestador_'+$(this).attr('indice')).select2().enable(false);
				$('#moneda_compra_'+$(this).attr('indice')).select2().enable(false);
				$('#botonesEdit_'+$(this).attr('indice')).css('display', 'none');
				$('#botones_'+$(this).attr('indice')).css('display', 'block');
			});	

			$(".comAgencia").change(function(){
				comision_agencia = $(this).val();
				tarjeta = $(this).attr('id')
	    		var inicio =  tarjeta.split('_');	
				if(parseFloat(comision_agencia) > parseFloat($('#comisionAgencia').val())){
					//alert(parseFloat(comision_agencia)+'>'+parseFloat($('#comisionAgencia').val()));
					$("#comision_agencia_"+inicio[2]).val($('#com_agencia_'+inicio[2]).val());
					$("#comision_agencia_"+inicio[2]).focus();
					$.toast({
							heading: 'Error',
							text: 'La comisión supera la comisión pactada.',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
						});
				}
			})

			$("#requestModalVoucher").on('hidden.bs.modal', function () {
	           $("#btnAceptarVoucher").off();
	           $('#btnEditarVoucher').off();	
			})

//			pasar();
		  $("#indice_0").prop("disabled", false);

			$("#producto_0").focusin(function(){
		   		$("#producto_0").select2('open');
		   	})

			ordenarSelect('producto_0');

		}	

		function inicializarElementos(){
			$('.fechaInOut').daterangepicker({
											timePicker24Hour: true,
											timePickerIncrement: 30,
											locale: {
												format: 'DD/MM/YYYY',
												 cancelLabel: 'Cancelar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']

													},
											minDate: new Date()
								    		});
		}

		function guardarVoucher(){
			
			$('#btnAceptarVoucher').unbind();

			$('#btnAceptarVoucher').click(function(){ 
				
				$('.productoVoucher').select2().enable(true);
		   		var dataString = $("#frmVoucher").serialize();

		   		$.ajax({
							type: "GET",
							url: "{{route('guardarVoucher')}}",
							dataType: 'json',
							data: dataString,
							success: function(rsp){
													if(rsp.status == 'OK'){
														$("#requestModalVoucher").modal('hide');
														limpiar(rsp.item);
													}

												}
						})		
		   		})
			}	

		   	$("#requestModalVoucher").on('hidden.bs.modal', function () {
	           $("#btnAceptarVoucher").off();
	           $('#btnEditarVoucher').off();	
			})

		
		function inicioNeW(){
			$('#indice_0').prop("disabled", false);
			$('#indice_0').focus();
			var date = new Date();
			var currentMonth = date.getMonth();
			var currentDate = date.getDate();
			var currentYear = date.getFullYear();			
			$('.select2').select2();

			$('#periodo_0').prop("disabled", false);
	        var datepickers = $('.fechaGasto'); 
			if (datepickers.length > 0) { 
										datepickers.datepicker({ 
											     format: "dd/mm/yyyy", 
											       language: "es"
											     //startDate: new Date() 
											    }); 
										} 

			$('.glyphicon-open-file').hide();
			$('.glyphicon-open-file').click(function(){	

				$('#oculto_'+$(this).attr('data')).hide();
				$('#mostrar_'+$(this).attr('data')).show();
				$('#ocultar_'+$(this).attr('data')).hide();
				$('#oculto_'+$(this).attr('data')).html('');
			})	

			ajusteMontos();

		   	$("#venta_0").change(function(){ 
				var markupId = $(this).attr('id');
				var indice = markupId.split('_');
				calcularMarkup();
			})	

		
			$('.btn-cancelar-request').click(function(){
				eliminarFila($(this).attr('indice'));

			})	

			$("#moneda_compra_0").focusin(function(){
		   		$("#moneda_compra_0").select2('open');
		   	}) 

			$("#periodo_0").change(function(){
		   		$("#proveedor_0").focus();
		   		if(parseInt($("#grupo").val()) == 3){
					$.ajax({
							type: "GET",
							url: "{{route('getPrecioAsistencia')}}",
							dataType: 'json',
							data: {
								dataTipoAsistencia: $("#tipoAsistencia").val(),
								dataPeriodo: $("#periodo_0").val(),
								dataCantidad: $("#personasAsistencia").val()
								},
							success: function(rsp){		  
								$("#cantidad_personas_0").val(rsp.cant_personas);
								$("#costo_0").val(rsp.costo);
								$("#cantidad_dias_0").val(rsp.dias);
								$("#tarifa_asistencia_0").val(rsp.tipo_asistencia);							
							}
					})	
				}	
		   	})

		}	

		//INICIO DE PROFORMA
		function inicio(){

			//$("#gravado_costo_0").prop("disabled", true);

			//MODAL AVISO
			eventAviso();
			//AVISO DE VENCIMIENTO INPUT

			$('#fecha_gasto_0').datepicker({ 
			     format: "dd/mm/yyyy", 
			     language: "es"
			   //  startDate: new Date() 
			}); 

			/* $("#proveedor_0").focusin(function(){
		   		$("#proveedor_0").select2('open');
		   	})*/

   		   /* $("#prestador_0").focusin(function(){
					$("#prestador_0").select2().on('select2-focus', function(){
						$(this).select2('open');
					});
				})*/
			$("#moneda_compra_0").focusin(function(){
		   		$("#moneda_compra_0").select2('open');
		   	})

			   var tipo = '{{$proformas[0]->pago_tarjeta}}';
				if(tipo == 1){
					indicador = 'true';
				}else{
					indicador = 'false';
				}
				$('#tipo_tarjeta').val(indicador).select2();


			$("#periodo_0").change(function(){
		    	//$("#proveedor_0").focus();
		   		if(parseInt($("#grupo").val()) == 3){
					$.ajax({
							type: "GET",
							url: "{{route('getPrecioAsistencia')}}",
							dataType: 'json',
							data: {
								dataTipoAsistencia: $("#tipoAsistencia").val(),
								dataPeriodo: $("#periodo_0").val(),
								dataCantidad: $("#personasAsistencia").val()
								},
							success: function(rsp){		  
								$("#cantidad_personas_0").val(rsp.cant_personas);
								$("#costo_0").val(rsp.costo);
								$("#cantidad_dias_0").val(rsp.dias);
								$("#tarifa_asistencia_0").val(rsp.tipo_asistencia);							
							}
					})	
				}	
		   	})

			$("#moneda_compra_0").change(function(){
				comisionable = clean_num($('#base_comisionable_0').val());
				totalCosto = parseFloat(clean_num($('#costo_0').val())) + parseFloat(clean_num($('#gravado_costo_0').val()));
				producto = $('#producto_0').val();
				monedaCompra = $('#moneda_compra_0').val();
				monedaVenta = $('#moneda').val();

				calcularPrecioCosto(comisionable, totalCosto, producto,monedaCompra,monedaVenta);
		   		$("#costo_0").focus();
		   	})

			ajusteMontos();

	
			$(".comAgencia").change(function(){
				comision_agencia = $(this).val();
				tarjeta = $(this).attr('id')
	    		var inicio =  tarjeta.split('_');	
				if(parseFloat(comision_agencia) > parseFloat($('#comisionAgencia').val())){
					$("#comision_agencia_"+inicio[2]).val($('#com_agencia_'+inicio[2]).val());
					$("#comision_agencia_"+inicio[2]).focus();
					$.toast({
							heading: 'Error',
							text: 'La comisión supera la comisión pactada.',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
						});
				}
			})

			if({{$saldo}}!= 0){
				$('#saldoGrupo').show();
			}else{
				$('#saldoGrupo').hide();
			}
			$('#usuarioNA').select2().val('{{$proforma->id_usuario}}').trigger("change");
			$('#id_asistente').select2().val('{{$proforma->id_asistente}}').trigger("change");
			$('#tasas_0').val(0);
			$("#tipo_venta").select2().val('{{$proforma->tipo_factura_id}}').trigger("change");
			//$("#id_promocion").select2().val("{{$proforma->id}}").trigger("change");
			$("#id_promocion").select2().val("{{$proforma->id_promocion}}").trigger("change");
			$('#id_promocion').select2().enable(false);

			$("#grupo_id").select2().val("{{$proforma->id_grupo}}").trigger("change");
			$('#grupo_id').select2().enable(false);

			var incentivo_agencia = '{{$proforma->tiene_incentivo_vendedor_agencia}}';
			if (incentivo_agencia == ''){
				var opcion = 'false';
			}
			else{
				var opcion = 'true';
			}
			<?php
				//if(strpos($proforma->concepto_generico, "   ")){
					$texto = rawurlencode($proforma->concepto_generico);
					$concepto =rawurldecode(str_replace("%0D%0A","  -  ",$texto));
				/*}else{
					$concepto =$proforma->concepto_generico;
				}*/
			?>
			$('#concepto').html("<?php echo $concepto; ?>");

			$("#incentivo_agencia").select2().val(opcion).trigger("change");

		 	var id_unidad_negocio = '{{$proforma->id_unidad_negocio}}';
		 	$("#negocio").select2().val(id_unidad_negocio).trigger("change");
			$("#negocio").select2().enable(false);
			$('#indice_0').prop("disabled", false);
			$("#indice_0").focus();
			var date = new Date();
			var currentMonth = date.getMonth();
			var currentDate = date.getDate();
			var currentYear = date.getFullYear();	
			$('.select2').select2();	
			precio_unitario  ="{{$proforma->imprimir_precio_unitario_venta}}";
			$("#imprimir_precio_unitario").select2().val(precio_unitario).trigger("change");
			$("#incentivo").select2().val("{{$proforma->incentivo_venta}}").trigger("change");
			$('#imprimir_precio_unitario').select2().enable(false);

			document.getElementById("concepto").disabled = true;

			 $("#idPrioridad").val('{{$prioridad}}').trigger('change.select2');
			var Fecha = new Date();
			var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

			 $('input[name="periodo_0"]').daterangepicker({
													        timePicker24Hour: true,
													        timePickerIncrement: 30,
													        locale: {
																	format: 'DD/MM/YYYY',
																	applyLabel: 'Aplicar',					
																	fromLabel: 'Desde',
																	toLabel: 'Hasta',
																	customRangeLabel: 'Seleccionar rango',
																	daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
																	monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
																		'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
																		'Diciembre']
													       			 },
													        minDate: moment().subtract(30, 'days') ,
								    					});

	        var datepicker = $('#vencimientos'); 
			    if (datepicker.length > 0) { 
			    datepicker.datepicker({ 
			     format: "dd/mm/yyyy", 
			       language: "es"
			    // startDate: new Date() 
			    }).on('changeDate', function(e){
					$('#vencimientos').popover('hide');
				}); 

			    } 

	        var datepicker = $('#vencimiento_0'); 
			    if (datepicker.length > 0) { 
			    datepicker.datepicker({ 
			     format: "dd/mm/yyyy", 
			     language: "es"
			    // startDate: new Date() 
			    }); 

			    } 

			$('.glyphicon-open-file').hide();
			$('.glyphicon-save-file').click(function(){
				idLinea = $(this).attr('data');
				$('#oculto_'+$(this).attr('data')).show();
				$('#mostrar_'+$(this).attr('data')).hide();
				$('#ocultar_'+$(this).attr('data')).show();
						$.ajax({
								type: "GET",
				             	url: "{{route('mostrarDatosDetalle')}}",//fileDelDTPlus
								dataType: 'json',
								data: {
										dataId: $(this).attr('numero'),
										dataMoneda:$('#moneda').val()
						       			},
								success: function(arrayDestino){
									    tabla = '<td colspan ="12" style="border-color: black;background-color: #dcd9d9;">';
										tabla += '<table cellpadding="8" cellspacing="0" border="0">';
										tabla += '<tr style="font-size: 0.75rem;">';
										tabla += '<td style="width:160px; text-align:center;"><b>Diferencia:</b>&nbsp;&nbsp;'+arrayDestino.diferencia_tributaria+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Costo Exenta:</b>&nbsp;&nbsp;'+arrayDestino.costo_exenta+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Costo Grav.:</b>&nbsp;&nbsp;'+arrayDestino.costo_gravada+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Exenta Item:</b>&nbsp;&nbsp;'+arrayDestino.exenta_item+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Grav.Item:</b>&nbsp;&nbsp;'+arrayDestino.gravada_item+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Iva:</b>&nbsp;&nbsp;'+arrayDestino.iva_porcion_gravada+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Comisión:</b>&nbsp;&nbsp;'+arrayDestino.comision_agencia+'</td>';
										idPerfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}";
										if(idPerfil == 'D'){
											tabla += '<td style="width:160px; text-align:center;"><b>% Comision:</b>&nbsp;&nbsp;'+arrayDestino.porcentaje_comision_agencia+'</td>';
										}
										tabla += '<td style="width:160px; text-align:center;"><b>No Comisiona:</b>&nbsp;&nbsp;'+arrayDestino.monto_no_comisionable+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Incentivo:</b>&nbsp;&nbsp;'+arrayDestino.dtplus+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Neto:</b>&nbsp;&nbsp;'+arrayDestino.neto_facturar+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Renta:</b>&nbsp;&nbsp;'+arrayDestino.renta+'</td></tr></td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Reprice:</b>&nbsp;&nbsp;' + (arrayDestino.hasOwnProperty('reprice') && arrayDestino.reprice ? 'SI' : 'NO');
										@if($reprice_item) //para mostrar solo a administrativo empresa 

										
											if (arrayDestino.renta_reprice.length) {
												for (let i = 0; i < arrayDestino.renta_reprice.length; i++) {
													//let detalle_reprice = arrayDestino.renta_reprice[i];
													tabla += '<td style="width:160px; text-align:center;" class="text-bold small"><b>Renta Reprice:</b>&nbsp;&nbsp;'+arrayDestino.renta_reprice[i].renta+'</td>';
													tabla += '<td style="width:160px;  text-align:center;" class="text-bold small"><b>Proveedor:</b>&nbsp;&nbsp;'+arrayDestino.renta_reprice[i].nombre+'</td>';
													tabla += '<td style="width:160px; text-align:center;" class="text-bold small"><b>Cod Conf Reprice:</b>&nbsp;&nbsp;'+arrayDestino.renta_reprice[i].cod_confirmacion+'</td>';
													tabla += '<td style="width:160px; text-align:center;" class="text-bold small"><b>Fecha:</b>&nbsp;&nbsp;'+arrayDestino.renta_reprice[i].created_at+'</td>';
													tabla += '<td style="width:160px; text-align:center;" class="text-bold small"><b>Precio Costo Reprice:</b>&nbsp;&nbsp;'+arrayDestino.renta_reprice[i].precio_costo+'</td></tr>';
													console.log(arrayDestino.renta_reprice);
												}
											}	
										@endif//fin permiso para mostrar reprice a administrativo empresa
										tabla += '</table></td>';
										//tabla += '<td style="width:160px; text-align:center;"><b>Renta REPRICE:</b>&nbsp;&nbsp;'+arrayDestino.renta+'</td></tr></td>' + '</table></td>';	
										$('#oculto_'+idLinea).html(tabla);									
									}
						})			
			})
			$('.glyphicon-open-file').click(function(){	
				$('#oculto_'+$(this).attr('data')).hide();
				$('#mostrar_'+$(this).attr('data')).show();
				$('#ocultar_'+$(this).attr('data')).hide();
				$('#oculto_'+$(this).attr('data')).html('');
			})
			$.ajax({
							type: "GET",
							url: "{{route('getDestinosProforma')}}",
							dataType: 'json',
							data: {
									dataProducto: "{{$destino}}",
					       			},
							success: function(rsp){
								 $('#destino_id').append($("<option></option>").attr("value", rsp[0].id).text(rsp[0].nombre)); 
								 $("#destino_id").val(rsp[0].id).trigger('change.select2');
								$('#destino_id').attr('disabled','disabled');
							}
			});	
			$('#destino_id').attr('disabled','disabled');

			$('.btn-cancelar-request').click(function(){
				eliminarFila($(this).attr('indice'));
			})	
						
			$('#vencimiento').val("{{date('d/m/Y', strtotime($proforma->vencimiento))}}");
			$('#vencimiento').attr('disabled','disabled');
			$('#vencimiento').popover();

		}
 		$("#indice_0").focus();

 		function errase(){

 			$("#periodo_0").val('');
 			$("#cantidad_personas_0").val(0);
 			$("#confirmacion_0").val('');
 			$("#costo_0").val(0);
 			$("#costo_0").prop("disabled", false);
 			$("#comision_agencia_0").val('');
 			$("#venta_0").val(0);
 			$("#venta_0").prop("disabled", false);
 			$("#markup_0").val('');
 			$("#markup_0").prop("disabled", false);
 			$("#descripcion_0").val('');
 			$("#tasas_0").val(0);
			$("#base_comisionable_0").val(0);
 			$("#descrip_factura_0").val('');
 			$("#fecha_gasto_0").val('');
 			$("#monto_gasto_0").val(0);
 			$("#vencimientos").val('');
 			$("#gravado_costo_0").val(0);
			$("#comision_operador_0").val(0);
			$("#ticket_0").val('');
 			//$("#gravado_costo_0").prop("disabled", true);
 		}

 		function limpiar(id){
			$("#periodo_"+id).prop("disabled", true);
			$("#cantidad_personas_"+id).prop("disabled", true);
			$("#confirmacion_"+id).prop("disabled", true);
			$("#descripcion_"+id).prop("disabled", true);
			$("#costo_"+id).prop("disabled", true);
			$("#comision_agencia_"+id).prop("disabled", true);
			$("#venta_"+id).prop("disabled", true);
			$("#markup_"+id).prop("disabled", true);
			$("#gravado_costo_0").val(0);
			$("#comision_operador_0").val(0);
			$("#ticket_0").val('');

		}
			
		function fechGastoProveeedor(){
			$('#indice_0').prop("disabled", false);
			$("#indice_0").focus();

			$('.btn-vencimiento-request').click(function(){ 
				$("#base").val($(this).attr('data'));
				$("#vencimiento_0").val('');
				$.ajax({
						type: "GET",
						url: "{{route('getDatosFecha')}}",
						dataType: 'json',
						data: {
							dataProforma: $('#proforma_id').val(),
							dataItem: $(this).attr('data')
				       			},
						success: function(rsp){
							if(jQuery.isEmptyObject(rsp) == false){
								if(jQuery.isEmptyObject(rsp[0].fecha_pago_proveedor) == false){
									var fechaProveedor = rsp[0].fecha_pago_proveedor;
									var fecha = fechaProveedor.split('-');
									fechaVencimiento = fecha[2]+'/'+fecha[1]+'/'+fecha[0]
									$("#vencimiento_0").val(fechaVencimiento);
								}	
							}
						}
					})	


			})	
		}	

		var formatNumber = {
		 separador: ".", // separador para los miles
		 sepDecimal: ',', // separador para los decimales
		 formatear:function (num){
		 num +='';
		 var splitStr = num.split('.');
		 var splitLeft = splitStr[0];
		 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
		 var regx = /(\d+)(\d{3})/;
		 while (regx.test(splitLeft)) {
		 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
		 }
		 return this.simbol + splitLeft +splitRight;
		 },
		 new:function(num, simbol){
		 this.simbol = simbol ||'';
		 return this.formatear(num);
		 }
		}

		    // =======================================
			// 				AJAX
			// =======================================
			// 
			/**
			 * GUARDAR COMENTARIO DEL MODAL COMENTARIOS
			 */
			
			$('#btnChat').click(function(){
				function mensajeDerecha(mensaje,nombre,fechaHora,id,logo){
					if(logo == ''){
						logo = 'avatar.png';
					}
					var rsp = `
						<div class='direct-chat-msg right' data='${id}'>
	                        <div class='row'>
	                            <div class='col-md-3' style='padding-left: 2px;padding-right: 10px;'><span class='direct-chat-name pull-right' style='font-size: smaller;'>${fechaHora}</span></div>
	                            <div class='col-md-7'></div>
	                            <div class='col-md-2'><span class='direct-chat-timestamp pull-left'><b>${nombre}</b></span></div>    
	                        </div>
	                        <div class='row'>
	                            <div class='col-md-10'><div class='chat-content-left' style='word-break: break-all; padding-left: 0px;'>
	                            ${mensaje}</div></div>
                            <div class='col-md-2'><img class="direct-chat-img" style="width: 50px;" src="${logo}" onerror="this.src='../personasLogo/factour.png'" alt="message user image"></div>
	                        </div>
	                     </div><br>`;
          			return rsp;
				}

				var inicial = '{{$proformas[0]->emergencia}}';
				if(inicial == 'Postergación'){
					$('#emergencia_tipo').val(1).select2();
				}else{
					$('#emergencia_tipo').val(2).select2(); 
				}

				var proformas = '{{$proformas[0]->proforma_padre}}';

				$('#proforma_emergencia').val(proformas);

				if(inicial.length != 0){
					$('#emergencia_tipo').prop('disabled',true); 
					$('#proforma_emergencia').prop('disabled',true); 
					$('#btnGuardarEmergencia').prop('disabled',true);
				}	



			function mensajeIzquierda(mensaje,nombre,fechaHora,id,logo){
				if(logo == ''){
					logo = 'avatar.png';
				}
					var rsp = `
							<div class='direct-chat-msg' data='${id}'>
		                        <div class='row'>
		                            <div class='col-md-2'><span class='direct-chat-name pull-left'><b>${nombre}</b></span></div>
		                            <div class='col-md-7'></div>
		                            <div class='col-md-3' style='padding-left: 2px;padding-right: 10px;'><span class='direct-chat-timestamp pull-right' style='font-size: smaller;'>${fechaHora}</span></div>    
		                        </div>
		                        <div class='row'>
		                            <div class='col-md-2'><img class="direct-chat-img" style="width: 50px;" src="${logo}" onerror="this.src='../personasLogo/factour.png'" alt="message user image"></div>
		                            <div class='col-md-10' style='word-break: break-all; padding-left: 0px;'><div class='chat-content'>
		                            ${mensaje}</div></div>
		                        </div>
		                     </div> <br>`;

		            return rsp;
				}

				@php
	              $logo = Session::get('datos-loggeo')->datos->datosUsuarios->logoAgencia;
	              $errorLogo = asset('mC/images/img/user2-160x160.jpg');
	            @endphp
				 var cantDiv = $('#chatDirect').children().length;
                 var lado  = $('#chatDirect div:nth-last-child('+cantDiv+')');
                 var idChat = lado.attr('data');
                 var esDerecha = $('#chatDirect div:nth-child(0)').hasClass('right');
                 var nombre = "{{Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido}}"; 
                 var imgLogo = "{{asset('personasLogo/'.$logo)}}";
                 var idUsuario = '{{Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario}}';
                 var idProforma = $('#proforma_id').val();
                 var fechaHora = moment().format('DD/MM/YYYY HH:mm:ss');
                 var mensaje = $('#mensajeChat').val();
                 var ok = '0';

                  if(idUsuario == idChat){
                  	if(esDerecha){
                  		$('#chatDirect').prepend(mensajeDerecha(mensaje,nombre,fechaHora,idUsuario,imgLogo));
                  	} else {
                  		$('#chatDirect').prepend(mensajeIzquierda(mensaje,nombre,fechaHora,idUsuario,imgLogo));
                  	}
                  } else {
                  	if(esDerecha){
                  		$('#chatDirect').prepend(mensajeIzquierda(mensaje,nombre,fechaHora,idUsuario,imgLogo));
                  	} else {
                  		$('#chatDirect').prepend(mensajeDerecha(mensaje,nombre,fechaHora,idUsuario,imgLogo));
                  	}
                  }
                  $('#mensajeChat').val('');

                  $('#errorChat').html('');
				var dataString = {"comentario": mensaje,"idUsuario":idUsuario,"idProforma":idProforma};

				guardarComentarioAjax(dataString);

				}); 

			function guardarComentarioAjax(dataString){

                   $.ajax({
                            type: "GET",
                            url: "{{ route('guardarComentario') }}",
                            dataType: 'json',
                            data: dataString,
                            
                             error: function(jqXHR,textStatus,errorThrown){

		                   $.toast({
						    heading: 'Error',
						    text: 'Ocurrio un error en la comunicación con el servidor.',
						    showHideTransition: 'fade',
						    icon: 'error'
						});

                        	$('#cargando').addClass('hidden');
						},
                            success: function(rsp){

                            	if(rsp.rsp == 'true'){
	                                var comentario = rsp.comentario.comentario;
									var comentarioCorto = comentario.substring(0, 50);
									$('#campoComentario').html(comentarioCorto+"...");
									$('#errorChat').html('Mensaje Enviado');
									$('#errorChat').css('color','green');

                            	} else {

                            		$('#errorChat').html('No se pudo enviar el mensaje');
                            		$('#errorChat').css('color','red');
                            	}

                                    

                                                }//function
                        })       
			}//function

			function showRequest(formData, jqForm, options) { 
			    $("#validation-errors").hide().empty();
			    return true; 
			} 
			
			function showResponse(response, statusText, xhr, $form)  { 
			            if(response.success == false)
			              {
			                var arr = response.errors;
			                $.each(arr, function(index, value)
			                {
			                  if (value.length != 0)
			                  {
			                    $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
			                  }
			                });
			                $("#validation-errors").show();
			            } else {

							$.each(response.data, function(index, value){
								$("#imagen").val(value.archivo); 
								var envio = value.id+", '"+value.archivo+"'";
								if(value.indice == 0){
									$("#output").append('<div id= "'+value.id+'"><div class="col-sm-2" id= "'+value.id+'"><a href="'+value.file+'" target="_blank"><img class="img-responsive" style="width:120px; height:120px;" src="'+value.file+'" alt="Photo"></a></div><div class="col-md-12 col-sm-1 col-md-1"><button type="button" id="btn-imagen" onclick="eliminarImagen('+envio+')"><i class="ft-x-circle" style="font-size: 18px;"></i></button></div></div>');
								}else{
									$("#output").append('<div id= "'+value.id+'"><div class="col-sm-2"><a href="'+value.file+'" target="_blank"><img class="img-responsive" style="width:120px;" src="{{asset('images/file.png')}}" alt="Photo"></a></div><div class="col-md-12 col-sm-1 col-md-1"><button type="button" id="btn-imagen" data-id="'+value.archivo+'" onclick="eliminarImagen('+envio+')"><i class="ft-x-circle" style="font-size: 18px;"></i></button></div></div>');
								}     
								$("#btnAceptarCanje").css('display','block');
							});
			                 
			            }
			       }

			      function eliminarImagen(id, archivo){
			       // $("#btn-imagen").on("click", function(e){
			          $.ajax({
			              type: "GET",
			              url: "{{route('fileAdjuntoEliminar')}}",//fileDelDTPlus
			              dataType: 'json',
			              data: {
									dataId: id,
									dataFile:archivo
					       			},
			              success: function(rsp){
			                $("#imagen").val("");
			                $("#"+rsp).remove();
			              }
			       //   })      

			        });
			      }  
				

			$(".actualizarProforma").hide(); 		
			$("#btnActualizar").click(function(){ 
					$(".baseProforma").hide(); 
					$(".actualizarProforma").show();

					//BOTON AGREGAR PASAJERO
					$("#id_asistente").select2().enable(true);
					$("#cliente_id").select2().enable(true);
					$("#idPrioridad").select2().enable(true);
					$("#id_categoria").select2().enable(true);
					$("#pasajero_principal").select2().enable(true);
					$("#destinoId").select2().enable(true);
					@if($cobrado == 0)
						$("#moneda").select2().enable(true);
					@endif
					$("#id_expediente").select2().enable(true);
					$("#senha").prop("disabled", false);
					$("#destinations_name").prop("disabled", false);
					$("#imprimir_precio_unitario").prop("disabled", false);
					$("#id_promocion").select2().enable(true);//$("#periodo").prop("disabled", false); 
					$("#vencimiento").prop("disabled", false);
					document.getElementById("concepto").disabled = true;
					$("#vencimientos").prop("disabled", false);
					$("#destino_id").select2().enable(true);
					$("#tipo_tarjeta").select2().enable(true);
					$("#incentivo_agencia").prop("disabled", false);
					$("#file_codigo").prop("disabled", false);
					//$("#tipo_venta").prop("disabled", true);
					@if($datosAnticipos->isEmpty())
					$("#tipo_venta").prop("disabled", false);
					@else
					$("#tipo_venta").prop("enable", true); 
					@endif
					//$("#tipo_venta").prop("disabled", false);
					$("#negocio").select2().enable(true);
					$("#grupo_id").select2().enable(true);
					$("#cant_pasajero").prop("disabled", false);
					$("#id_tarifa").prop("disabled", false);
					$("#concepto").prop("disabled", false);
					$("#botonPasajero").prop("disabled", false);
					$("#botonCliente").prop("disabled", false);
					@if($permisoEditarIncentivo == 1)
						$("#incentivo").prop("disabled", false);
					@endif
					$.ajax({
			              type: "GET",
			              url: "{{route('getCargaCliente')}}",//fileDelDTPlus
			              dataType: 'json',
						  data: {id_proforma : $('#proforma_id').val() },
			              success: function(rsp){
						 	$('#cliente_id').empty()
			              	$.each(rsp, function (key, item){
								var newOption = new Option(item.documento_identidad+' - '+item.nombre+' - '+item.denominacion+' - '+item.id, item.id, false, false);
								$('#cliente_id').append(newOption);
							})	
							valor = '{{$cliente_id}}';		
							$("#cliente_id").val(valor).select2();
			              }
			        })

					if('{{$permisoEditarUsuario}}' ==1){
						$("#usuarioNA").prop("disabled", false);
						$("#id_asistente").prop("disabled", false);
						$("#divTUsuario").css('display', 'block');
				    }    

					$("#pasajero_principal").select2({
					        ajax: {
					                url: "{{route('getPasajeroPrincipal')}}",
					                dataType: 'json',
					                placeholder: "Seleccione un Beneficiario",
					                delay: 0,
					                data: function (params) {
					                            return {
					                                q: params.term, // search term
					                                page: params.page
					                                    };
					                },
					                processResults: function (data, params){
					                            var results = $.map(data, function (value, key) {
					                           /* return {
					                                    children: $.map(value, function (v) {*/ 
					                                        return {
					                                                    id: value.id,
					                                                    text: value.pasajero_data
					                                                };
					                                      /*  })
					                                    };*/
					                            });
					                                    return {
					                                        results: results,
					                                    };
					                },
					                cache: true
					                },
					                escapeMarkup: function (markup) {
					                                return markup;
					                }, // let our custom formatter work
					                minimumInputLength: 3,
					    });

						$("#pasajero_principal_peticion").select2({
					        ajax: {
					                url: "{{route('getPasajeroPrincipal')}}",
					                dataType: 'json',
					                placeholder: "Seleccione un Beneficiario",
					                delay: 0,
					                data: function (params) {
					                            return {
					                                q: params.term, // search term
					                                page: params.page
					                                    };
					                },
					                processResults: function (data, params){
					                            var results = $.map(data, function (value, key) {
					                           /* return {
					                                    children: $.map(value, function (v) {*/ 
					                                        return {
					                                                    id: value.id,
					                                                    text: value.pasajero_data
					                                                };
					                                      /*  })
					                                    };*/
					                            });
					                                    return {
					                                        results: results,
					                                    };
					                },
					                cache: true
					                },
					                escapeMarkup: function (markup) {
					                                return markup;
					                }, // let our custom formatter work
					                minimumInputLength: 3,
					    });

					$.ajax({
			              type: "GET",
			              url: "{{route('getCargarGrupos')}}",//fileDelDTPlus
			              dataType: 'json',
			              success: function(rsp){
			              	$.each(rsp, function (key, item){
								var newOption = new Option(item.denominacion, item.id, false, false);
								$('#grupo_id').append(newOption);
							})	

			              }
			        })

					$.ajax({
			              type: "GET",
			              url: "{{route('getCargarExpedientes')}}",//fileDelDTPlus
			              dataType: 'json',
			              success: function(rsp){
			              	var newOption = new Option('Seleccione Proforma',"",false, false);
			              	$('#id_expediente').append(newOption);
			              	$.each(rsp, function (key, item){
			              		var newOption = new Option('Proforma Nro:'+item.id, item.id, false, false);
								$('#id_expediente').append(newOption);
							})	

			              }
			        })

					destinosFuntion();
			});

			$("#btnCancelar").click(function(){ 
					$(".baseProforma").show();
					$(".actualizarProforma").hide(); 
					//BOTON AGREGAR PASAJERO
					/*$("#botonesModal").hide(); 
					$("#btonesModalActivo").show();*/
					$("#usuarioNA").select2().enable(false);
					$("#tipo_tarjeta").select2().enable(false);
					$("#id_asistente").select2().enable(false);
					$("#incentivo").select2().enable(false);
					$("#imprimir_precio_unitario").select2().enable(false);
					$("#cliente_id").select2().enable(false);
					$("#idPrioridad").select2().enable(false);
					$("#id_categoria").select2().enable(false);
					$("#pasajero_principal").select2().enable(false);
					$("#destinoId").select2().enable(false);
					$("#moneda").select2().enable(false);
					$("#id_expediente").select2().enable(false);
					$("#id_promocion").select2().enable(false);
					$("#destino_id").select2().enable(false);
					$("#grupo_id").select2().enable(false);
					$("#senha").prop("disabled", true);
					$("#file_codigo").prop("disabled", true);
					$("#periodo").prop("disabled", true); 
					$("#vencimiento").prop("disabled", true);
					$("#incentivo_agencia").prop("disabled", true);
					$("#tipo_venta").prop("disabled", true);
					$("#negocio").prop("disabled", true);
					$("#negocio").select2().enable(false);
					$("#id_promocion").select2().enable(false);
					$("#id_tarifa").select2().enable(false);
					$("#grupo_id").select2().enable(false);
					$("#cant_pasajero").prop("disabled", true);

			});

		     $("#grupo_id").change(function () {
		     	if ($(this).val() == 0) {
		     		$("#control").val(0)
		     	}
		     	$.ajax({
		     		type: "GET",
		     		url: "{{route('doGrupoCom')}}",
		     		dataType: 'json',
		     		data: {
		     			idGrupo: $(this).val(),
		     			idCliente: $('#cliente_id').val()
		     		},
		     		error: function (jqXHR, textStatus, errorThrown) {
		     			$.toast({
		     				heading: 'Error',
		     				position: 'top-right',
		     				text: 'Ocurrio un error en la comunicación con el servidor.',
		     				showHideTransition: 'fade',
		     				icon: 'error'
		     			});
						 $("#control").val(1);
		     		},
		     		success: function (rsp) {
		     			if ($("#grupo_id").val() != 0) {
		     				if (rsp.err == false) {
		     					$("#control").val(1);
		     				} else {	
		     					$("#control").val(0);
		     				}
		     			}
		     		}
		     	})

		     })


			 $("#btnGuardarProforma").click(function(){
				idEmpresa = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
				if(idEmpresa == 1){
					if($("#id_promocion").val() == 0){
						$.toast({
									heading: 'Error',
									position: 'top-right',
									text: 'Seleccione una Promocion',
									showHideTransition: 'fade',
									icon: 'error'
								});
					}else{
						guardar();
					}
				}else{
					guardar();
				}
			})	


			function guardar(){
					if($("#control").val() == 0){
						$("#moneda").prop("disabled", false);
						$("#periodo").prop("disabled", false);
						$("#tipo_venta").prop("disabled", false);
						var dataString = $("#frmProforma").serialize();
						$.ajax({
								type: "GET",
								url: "{{route('actualizarProforma')}}",
								dataType: 'json',
								data: dataString,

								error: function(e) {
									console.log('Error de comunicación');
								},
								success: function(rsp){
											if(rsp.status == 'OK'){

												$("#tipo_factura").val(rsp.tipo_facturacion);	
												$(".baseProforma").show();
												$(".actualizarProforma").hide(); 
												//BOTON AGREGAR PASAJERO
												//$("#botonesModal").hide(); 
												$("#btonesModalActivo").show();
												$("#cliente_id").select2().enable(false);
												$("#id_categoria").select2().enable(false);
												$("#pasajero_principal").select2().enable(false);
												$("#idPrioridad").select2().enable(false);
												$("#destino_id").select2().enable(false);
												$("#moneda").select2().enable(false);
												$("#id_expediente").select2().enable(false);
												$("#senha").prop("disabled", true);
												$("#periodo").prop("disabled", true); 
												$("#file_codigo").prop("disabled", true); 
												$("#vencimiento").prop("disabled", true);
												$("#incentivo_agencia").prop("disabled", true);
												$("#tipo_venta").prop("disabled", true);
												$("#negocio").select2().enable(false);
												$("#id_promocion").select2().enable(false);
												$("#id_tarifa").select2().enable(false);
												$("#grupo_id").select2().enable(false);
												$("#tipo_tarjeta").select2().enable(false);
												$("#cant_pasajero").prop("disabled", true);
												$("#imprimir_precio_unitario").prop("disabled", true);
												$("#comentario").prop("disabled", true);
												$("#usuarioNA").prop("disabled", true);
												$("#id_asistente").prop("disabled", true);
												$.toast({
														heading: 'Exito',
														text: rsp.mensaje,
														position: 'top-right',
														showHideTransition: 'slide',
														icon: 'success'
													});  
													//location.reload();

											} else {
											$("#periodo").prop("disabled", true); 
											$.toast({
													heading: 'Error',
													text: rsp.mensaje,
													position: 'top-right',
													showHideTransition: 'fade',
													icon: 'error'
												});
											}	

								}	
							});
						setTimeout(actualizarDetalles, 3000);	
						setTimeout(actualizarCabecera, 6000);
					}else{
						$.toast({
							heading: 'Error',
							position: 'top-right',
							text: 'El Cliente no esta ligado al grupo, Intentelo Nuevamente',
							showHideTransition: 'fade',
							icon: 'error'
							});

					}	
				}	

			$("#cliente_id").change(function(){	
				$('#id_categoria').select2({
					disabled: false
				});

				if($(this).val() != ''){
					$.ajax({
							type: "GET",
							url: "{{route('getDatosCliente')}}",
							dataType: 'json',
							data: {
								dataCliente: $(this).val()
					       			},
					       			
	                       error: function(jqXHR,textStatus,errorThrown){

	                           $.toast({
	                            heading: 'Error',
	                            text: 'Ocurrio un error en la comunicación con el servidor.',
	                            position: 'top-right',
	                            showHideTransition: 'fade',
	                            icon: 'error'
	                        });

	                        },
							success: function(rsp){
								$("#tipo_cliente").val(rsp.tipo_persona);
							//	$("#tipo_factura").val(rsp.tipo_facturacion);
								$("#linea_credito").val(rsp.limite_credito);
								$("#saldo_disponible").val(rsp.saldo_disponible);
								var vendedores = rsp.vendedores;
								if(vendedores.length != 0){
									$('#id_categoria').empty();
										var newOption = new Option('Seleccione Vendedor', 0, false, false);
										$('#id_categoria').append(newOption); 
									$.each(rsp.vendedores, function (key, item){
										var newOption = new Option(item.denominacion, item.id, false, false);
										$('#id_categoria').append(newOption);
									})	

								}else{ 
									$('#id_categoria').select2({
											  disabled: true
									});
								}
							}	
					});	//ajax

				}//if	

				$.ajax({
					type: "GET",
					url: "{{route('doGrupoCom')}}",
					dataType: 'json',
					data: {
						idGrupo: $("#grupo_id").val(),
						idCliente: $('#cliente_id').val()
					},
					error: function (jqXHR, textStatus, errorThrown) {
						$.toast({
							heading: 'Error',
							position: 'top-right',
							text: 'Ocurrio un error en la comunicación con el servidor.',
							showHideTransition: 'fade',
							icon: 'error'
						});

						$("#control").val(1);
					},
					success: function (rsp) {
						if ($("#grupo_id").val() != 0) {

							if (rsp.err == false) {
								$("#control").val(1);
							} else {
								$("#control").val(0);
							}
						}

					}
				})



			});	//change()

		$("#btnAceptarPrestador").click(function(){
	
			$('#dv_prestador').prop('disabled',false);
			let form = document.getElementById('frmNuevoPrestador');
			// Verifica si el formulario es válido
			if (!form.checkValidity()) {
				form.reportValidity(); // Muestra los avisos nativos de validación
				$.toast({
					heading: 'Error',
					position: 'top-right',
					text: 'Complete los campos obligatorios.',
					showHideTransition: 'fade',
					icon: 'error'
				});
				return;
			}

			$('#spinner_modal_prestador').show();
			$('#btnAceptarPrestador').prop('disabled', true);
        	$('#btnCancelarVSTour').prop('disabled', true);

				$.ajax({
						type: "GET",
						url: "{{route('doPrestadorProforma')}}",
						dataType: 'json',
						data: $("#frmNuevoPrestador").serialize(),
						success: function(rsp){
										var newOption = new Option(rsp.nombre, rsp.id, false, false);
										$('#prestador_0').append(newOption);
										$("#prestador_0").select2().val(rsp.id).trigger("change");
										$("#requestModalNuevoPrestador").modal('hide');
										$('#dv_prestador').prop('disabled',true);

										$('#frmNuevoPrestador')[0].reset();
								},
						error:function(){
									$.toast({
										heading: 'Error',
										position: 'top-right',
										text: 'Ocurrio un error en la comunicación con el servidor.',
										showHideTransition: 'fade',
										icon: 'error'
									});
								},
						complete: function() {
								// Oculta el spinner
								$('#spinner_modal_prestador').hide();
								$('#btnAceptarPrestador').prop('disabled', false);
								$('#btnCancelarVSTour').prop('disabled', false);
							}
						});	
			
		});



		$("#btnAceptarPasajero").click(function(){
			$('#dv').prop('disabled',false);
			var dataString = $("#frmPasajero").serialize();
			if($("#apellido").val() != '' || $("#nombre").val() != ''){
				$.ajax({
						type: "GET",
						url: "{{route('doPasajeroProforma')}}",
						dataType: 'json',
						data: dataString,
						success: function(rsp){
							$("#requestModalPasajero").modal('hide');
										var $select2 = $("#pasajero_principal").select2();
										$select2.append($('<option>', { //agrego los valores que obtengo de una base de datos
										                        value: rsp.id,
										                        text: rsp.nombre
										                       }));
										/*$select2.val(rsp.id).trigger("change");*/
										var selectedValues = new Array();
										count = 0;
										@foreach($pasajeros as $key=>$pasajero)
											selectedValues['{{$key}}'] = "{{$pasajero->id}}";
											count++;
										@endforeach	
										var total = count + 1;
										selectedValues[total] = rsp.id;

										$("#pasajero_principal").val(selectedValues);

										var $select3 = $("#pasajero_principal2").select2();
										$select3.append($('<option selected="selected">', { //agrego los valores que obtengo de una base de datos
										                        value: rsp.id,
										                        text: rsp.nombre
										                       }));
										$select3.val(rsp.id).trigger("change");		
										$('#dv').prop('disabled',true);
							
								},
						});	
			}else{
				console.log('No existe')
			}
		});
		$("#btnAceptarSolicitudPasajero").click(function(){
			var dataString = $("#frmPasajeroSolicitud").serialize();
			if($("#apellido_pedido").val() != '' || $("#nombre_pedido").val() != ''){
				$.ajax({
						type: "GET",
						url: "{{route('doPasajeroProforma')}}",
						dataType: 'json',
						data: dataString,
						success: function(rsp){
										$("#requestModalPasajero").modal('hide');
										var $select2 = $("#pasajero_principal").select2();
										$select2.append($('<option>', { //agrego los valores que obtengo de una base de datos
										                        value: rsp.id,
										                        text: rsp.nombre
										                       }));
										/*$select2.val(rsp.id).trigger("change");*/
										var selectedValues = new Array();
										count = 0;
										@foreach($pasajeros as $key=>$pasajero)
											selectedValues['{{$key}}'] = "{{$pasajero->id}}";
											count++;
										@endforeach	
										var total = count + 1;
										selectedValues[total] = rsp.id;

										if($("#marcador1").val() == 1){					   
											$("#pasajero_principal").val(selectedValues);

											var $select3 = $("#pasajero_principal2").select2();
											$select3.append($('<option selected="selected">', { //agrego los valores que obtengo de una base de datos
																	value: rsp.id,
																	text: rsp.nombre
																}));
											$select3.val(rsp.id).trigger("change");
										}else{
											$("#pasajero_pedido").val(selectedValues);

											var $select3 = $("#pasajero_pedido").select2();
											$select3.append($('<option value="'+rsp.id+'"selected="selected">'+rsp.nombre+'</option>', { }));
											$("#requestModalPasajeroSolicitud").modal('hide');
											$("#pasajero_pedido").val(rsp.id).trigger('change.select2');
											colocarDato(rsp.id);
										}							
									},
						});	
			}else{
				console.log('No existe')
			}
		});
		
		function colocarDato(id){
			$("#colocarDato").val(id).trigger('change.select2');
		}

    	function destinoFuntion(){

				$("#destinos").select2({
					    ajax: {
					            url: "{{route('destinoProformaGrupo')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
						});
		}

		function redirectProforma(){
			location.replace("{{route('detallesProforma',['id'=>''])}}/"+$("#id_proforma").val());
		}

		function destinosFuntion(){
			$("#destino_id").select2({
					    ajax: {
					            url: "{{route('destinoProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
						});
		}
	
		$('.btnErrorSuperior').click(function(){
			$('#mensajaErrorSuperior').addClass('hidden');
		});

		$('.btnErrorSuperiors').click(function(){
			$('#mensajeConcordancia').addClass('hidden');
		});

		function format(value) {
		    return '<div>' + value + '</div>';
		}


		function actualizarCabecera(){
			 $.ajax({
			        type: "GET",
			        url: "{{route('actualizarCabecera')}}",//fileDelDTPlus
			        dataType: 'json',
			        data: {
							dataId: $('#proforma_id').val(),
					},
			        success: function(rsp){
								$('#senhas').val(rsp.senha);
								var fechaProveedor = rsp.vencimiento;
								if(jQuery.isEmptyObject(fechaProveedor) == false){
									var fecha = fechaProveedor.split('-');
									fechaVencimiento = fecha[2]+'/'+fecha[1]+'/'+fecha[0]
								}else{
									fechaVencimiento = '';
								}	
								$('#monto_proforma').val(rsp.total_factura);
								$('#total_factura').val(rsp.total_factura);
								$('#vencimiento').val(fechaVencimiento);
								$('#total_bruto').val(rsp.total_bruto_facturar);
								$('#total_neto').val(rsp.total_neto_facturar);
								$('#total_iva').val(rsp.total_iva);
								$('#total_comision').val(rsp.total_comision);
								$("#incentivo").select2().val(rsp.incentivo_vendedor_agencia).trigger("change");
								$("#incentivo").prop("disabled", true); 
								$('#total_costo').val(rsp.total_costos);
								$('#markupF').val(rsp.markup);
								$('#total_exentas').val(rsp.total_exentas);
								$('#total_gravadas').val(rsp.total_gravadas);
								$('#total_no_comisiona').val(rsp.total_no_comisiona);
								$('#incentivo_proforma').val(rsp.incentivo_proforma);
								$('#renta').val(rsp.renta);
								$('#porcentaje_ganancia').val(rsp.porcentaje_ganancia);
								$('#ganancia_venta').val(rsp.ganancia_venta);
								$('#saldoFacturaMonto').val(rsp.saldo_factura);
								$('#periodo').val(rsp.periodo);
								$('#senha').val(rsp.senha);
								$('#concepto').val(rsp.concepto);
								$('#renta_extra').val(rsp.rextra);
								renta_neta = parseFloat(rsp.ganancia_venta) - parseFloat(rsp.rextra);
								$('#renta_neta').val(renta_neta);
								document.getElementById("concepto").disabled = true;
								$('#grupo_id').val(rsp.id_grupo).trigger('change.select2');
								$('#pasajero_principal2').val(rsp.pasajero_id).trigger('change.select2');  
								$('#id_tarifa').val(rsp.id_tarifa).trigger('change.select2'); 
								if(rsp.saldoGrupo != 0){
									$('#saldoGrupoMonto').val(rsp.saldoGrupo);
									$('#saldoGrupo').show();
								}else{
									$('#saldoGrupo').hide();
								}
								var $select2 = $("#pasajero_principal").select2();
								var selectedValues = new Array();
								count = 0;
								totals = 0;
								$.each(rsp.pasajeros, function(index,value){
									if(count < 1){
										$select2.append($('<option>', { 
																		value: value.id,
																		text: value.pasajero_n
																		}));
										selectedValues[value.id] = value.id;
										count++;
									}
									totals++;
								});
								var total = count + 1;
								selectedValues[total] = rsp.id;
								$("#pasajero_principal").val(selectedValues);
								$("#control").val(0);
								if((totals + 1) > 2){
									$('#botonPasajeroDetalles').css('display', 'block');
								}else{
									$('#botonPasajeroDetalles').css('display', 'none');
								}
					}
			});
		}

		$('.ocultado').hide();

		function AgregarEliminarVoucher(){
			$('.deleteLineaVoucher').click(function(){ 
				$("#lineaVoucher"+$(this).attr('id')).remove();
			});	
		} 

		function numeroAleatorio(min, max) {
  			return Math.round(Math.random() * (max - min) + min);
		}

		function AgregarEditarVoucher(){
			$('#btnEditarVoucher').unbind();
			$('#btnEditarVoucher').click(function(){ 
				$('.productoVoucher').select2().enable(true);
		   		var dataString = $("#frmVoucher").serialize();
		   		$.ajax({
							type: "GET",
							url: "{{route('editarVoucher')}}",
							dataType: 'json',
							data: dataString,
							success: function(rsp){
													if(rsp.status == 'OK'){
														$("#requestModalVoucher").modal('hide');
														limpiar(rsp.item);
													}

												}
						})		
		   		})	
		} 

		function actualizarDetalle(){
			$.ajax({
					type: "GET",
					url: "{{route('getMontoDetalle')}}",
					dataType: 'json',
					data: {
							dataProforma: $('#proforma_id').val()
						},
					success: function(rsp){
									$.each(rsp, function (key, item){
														$("#costo_"+key).prop("disabled", false);
														$("#markup_"+key).prop("disabled", false);
														$("#venta_"+key).prop("disabled", false);
														$("#comision_agencia_"+key).prop("disabled", false);
				 										$("#costo_"+key).val(item.precio_compra);
				 										$("#venta_"+key).val(item.precio_venta);
				 										$("#markup_"+key).val(item.markup);	
				 										$("#comision_agencia_"+key).val(item.porcentaje);
														$("#costo_"+key).prop("disabled", true);
														$("#venta_"+key).prop("disabled", true);
														$("#markup_"+key).prop("disabled", true);
														$("#markup_"+key).prop("disabled", true);
														$("#comision_agencia_"+key).prop("disabled", true); 
													})	

								}	
			});

		}
		$("#documento").change(function(){
			agregarDvs()
		});

		function agregarDvs(){
                $.ajax({
                        type: "GET",
                        url: "{{ route('agregarDv') }}",
                        dataType: 'json',
                        data: { documento: $('#documento').val().trim()},

                         error: function(jqXHR,textStatus,errorThrown){
                            $('.mensaje_err_documento').html(' No se pudo validar. ');
                            $('.mensaje_err_documento').css('color','red');
                            inputReq.VerificarDoc(1,false);
                      
                        },
                        success: function(rsp){
                           // console.log(rsp[0]);
                           $('#documento').val(rsp[0].ruc);
                            if(jQuery.isEmptyObject(rsp[0].dv) == true){
                                $('#dv').val(rsp[0].dv);
                                $('#dv').prop('disabled',true);
                                $('#tipo_identidad').val(2).trigger('change.select2');
                            }else{
                                $('#dv').prop('disabled',false);
                                $('#tipo_identidad').val(1).trigger('change.select2');
                            }

                            if(jQuery.isEmptyObject(rsp[0].nombre) == false){
                                denominacion = rsp[0].nombre;              
                                base = denominacion.split(',');
                                $('#nombre').val(base[1]);
                                $('#apellido').val(base[0]);
                            }
                        }
                })   
           }    


		function actualizarDetalles(){
			$.ajax({
					type: "GET",
					url: "{{route('getMontoDetalle')}}",
					dataType: 'json',
					data: {
							dataProforma: $('#proforma_id').val()
						},
					success: function(rsp){
									$.each(rsp, function (key, item){
										$("#costo_"+key).prop("disabled", false);
										$("#markup_"+key).prop("disabled", false);
										$("#venta_"+key).prop("disabled", false);
										$("#comision_agencia_"+key).prop("disabled", false);
 										$("#costo_"+key).val(item.precio_compra);
 										$("#venta_"+key).val(item.precio_venta);
 										$("#markup_"+key).val(item.markup);	
 										$("#comision_agencia_"+key).val(item.porcentaje);
										$("#costo_"+key).prop("disabled", true);
										$("#venta_"+key).prop("disabled", true);
										$("#markup_"+key).prop("disabled", true);
										$("#markup_"+key).prop("disabled", true);
										$("#comision_agencia_"+key).prop("disabled", true);
 									})	

								}	
			});

		}

		function cargando(){
			$.blockUI({
                    centerY: 0,
                    message: "<h2>Cargando el Detalle de Proforma...</h2>",
                    css: {
                        color: '#000'
                        }
                    });
		}

		function procesarndo(){
			$.blockUI({
                    centerY: 0,
                    message: "<h2>Procesando...</h2>",
                    css: {
                        color: '#000'
                        }
                    });
		}

		function delase(){
			$.unblockUI();
		}	

		function guardarFila(){
			if($("#venta_0").val() == "" && $("#venta_0").val() == 0){
					$.ajax({
					type: "GET",
					url: "{{route('calcularVenta')}}",
					dataType: 'json',
					data: {
							dataTasas: $('#tasas_0').val(),
							dataMonedaCompra: $('#moneda_compra_0').val(),
							dataCosto: $('#costo_0').val(),
							dataCurrency_venta: $('#moneda').val(),
							dataMarkup: $('#markup_0').val()
						},
							success: function(rsp){
								if(rsp > 0){

									$('#venta_0').val(rsp);

								} else {

									$('#venta_0').val(0);
								}

								$('#venta_0').css('border-color', 'red');
						}
					});		

				}else{
					$('#venta_0').val($("#venta_0").val());
				}
			$('#guardar').prop("disabled", false);
			$('#datosDetalles').find('input,select,select2,textarea').prop("disabled", false);
			var dataString = $("#datosDetalles").serialize();
			$.ajax({
				type: "GET",
				url: "{{route('guardarFila')}}",
				dataType: 'json',
				data: dataString,
				success: function(rsp){
								if(rsp.status == 'OK'){
									$.unblockUI();
									actualizarCabecera();
									valor_anterior = parseInt($("#item").val());
									var tbody = $('#lista_productos tbody');
									periodo = $('#periodo_0').val(); 
									checkInOut = periodo.split('-');
									if($('#comision_operador_0').val() === null){
										comision = rsp.porcentaje_comision;
									}else{
										comision = $('#comision_operador_0').val();
									}	
									fila_contenido = '';
									fila_contenido += '<tr id="'+valor_anterior+'" data-toggle="tooltip" data-placement="right" data-html="true" title="Check In : '+checkInOut[0]+'&#10;Check Out : '+checkInOut[1]+'&#10;Prestador : '+rsp.prestador+'">';
									fila_contenido += '<th style="padding: 0.25rem 0.5rem;">'+$('#cant_0').val()+'</th>';
									fila_contenido += '<td style="padding: 0.25rem 1rem;">'+$('#producto_0 :selected').text()+'</td>';
									//fila_contenido += '<td style="padding: 0.25rem 1rem;">'+$('#periodo_0').val()+'</td>';
									fila_contenido += '<th style="padding: 0.25rem 1rem;text-align: center;">'+$('#confirmacion_0').val()+'</th>';
									fila_contenido += '<th style="padding: 0.25rem 1rem;">'+rsp.proveedor+'</th>';
									//fila_contenido += '<th style="padding: 0.25rem 1rem;">'+$('#prestador_0 :selected').text()+'</th>';
									fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+$('#moneda_compra_0 :selected').text()+'</td>';
									fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+rsp.costo_total_proveedor+'</td>';
									fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+rsp.tasas+'</td>';
									fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+rsp.porcentaje_comision+'</td>';
									fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+$('#venta_0').val()+'</td>';
									fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center;">'+rsp.markup+'</th>';
									fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center;"><i class="fa fa-fw fa-circle" title="Inactivo Gestur" style="color:'+rsp.color+';"></i></th>';		
									fila_contenido += '<td style="padding: 0.25rem 0.5rem;">';
									if(rsp.origen != 'A'){

										
										if(!rsp.pagado_con_tc){
											fila_contenido += '<a id="'+rsp.id+'" class="btn-update-request" onclick="updateDetalle('+rsp.id+','+rsp.item+')" title="Editar Detalle" data="'+rsp.item+'" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>';
											fila_contenido += '<a data="'+rsp.id+'"  title="Eliminar Detalle" indice="'+rsp.id+'" class="btn-cancelar-request" style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>';

											//Verificamos si puede pagar con TC
											if(rsp.prestador.prestador_migrar && rsp.producto_migrar && rsp.pagar_tc_detalle_proforma){
												fila_contenido += '<a data-proforma_detalle_id="'+rsp.id+'" class="btn-update-request" onclick="pagarTcDetalle('+rsp.id+')" title="Pagar con TC" style="margin-left: 5px;"><i class="fa fa-credit-card" style="font-size: 18px;color:red;"></i></a>';
											}
										} else {
											//Si es pagado con TC bloqueamos detalle
											fila_contenido += '<a  class="btn-update-request" disabled title="Bloqueado por pago de TC" style="margin-left: 5px;color:green;"><i class="fa fa-credit-card" style="font-size: 18px"></i></a>';
										}

										
									} else if(rsp.origen == 'A' && permisoAnularFee && rsp.id_producto == 38961){
										fila_contenido += '<a data="'+rsp.id+'"  title="Eliminar Detalle" indice="'+rsp.id+'" class="btn-cancelar-request" style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>';
									}

									fila_contenido += '<a id="mostrar_'+valor_anterior+'" class="glyphicon glyphicon-save-file" title="Desplegar Detalle" numero ="'+rsp.id+'" data="'+valor_anterior+'" style="margin-left: 5px;"><i class="ft-download" style="font-size: 18px;"></i></a>';

									fila_contenido += '<a style ="display:none" id="ocultar_'+valor_anterior+'" numero ="'+rsp.id+'" class="glyphicon glyphicon-open-file" data="'+valor_anterior+'"  title="Ocultar Detalle" style="margin-left: 5px;"><i class="ft-upload" style="font-size: 18px;"></i></a>';

									fila_contenido += '</td>';
									fila_contenido += '</tr>';
									fila_contenido += '<tr class="ocultado" id="oculto_'+valor_anterior+'" style="display: none"></tr>';	
									fila_contenido += '</td>';
									tbody.append(fila_contenido);

									if(rsp.reserva_nemo_id !=""){
										valor_anteriores = valor_anterior+1;
										$.ajax({
												type: "GET",
												url: "{{route('getDetalleProforma')}}",
												dataType: 'json',
												data: {
														dataProforma: $('#proforma_id').val(),
														dataItem: valor_anteriores,
														dataDetallePadre: rsp.id
													},
												success: function(rsps){
													console.log(rsps);
													periodo = $('#periodo_0').val(); 
													checkInOut = periodo.split('-');
													fila_contenido = '';
													fila_contenido += '<tr id="'+valor_anteriores+'" data-toggle="tooltip" data-placement="right" data-html="true" title="Check In : '+checkInOut[0]+'&#10;Check Out : '+checkInOut[1]+'&#10;Prestador : '+rsps.prestador.nombre+'">';
													fila_contenido += '<th style="padding: 0.25rem 0.5rem;">'+$('#cant_0').val()+'</th>';
													fila_contenido += '<td style="padding: 0.25rem 1rem;">'+rsps.producto.denominacion+'</td>';
													//fila_contenido += '<td style="padding: 0.25rem 1rem;">'+$('#periodo_0').val()+'</td>';
													fila_contenido += '<th style="padding: 0.25rem 1rem;text-align: center;">'+rsps.cod_confirmacion+'</th>';
													fila_contenido += '<th style="padding: 0.25rem 1rem;">'+rsps.proveedor.nombre+'</th>';
													//fila_contenido += '<th style="padding: 0.25rem 1rem;">'+$('#prestador_0 :selected').text()+'</th>';
													if(rsps.comision_operador != 0){
														comision = rsps.porcentaje_comision;
													}else{
														comision = rsps.porcentaje_comision_agencia;
													}
													total_costo = rsps.costo_proveedor + rsps.costo_gravado;  
													fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">USD</td>';
													fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+total_costo+'</td>';
													fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+rsps.monto_no_comisionable+'</td>';
													fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+comision+'</td>';
													fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+rsps.precio_venta+'</td>';
													fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center;">'+rsps.porcentaje_renta_item+'/'+rsps.renta_minima_producto+'</th>';
													if(rsps.cumple_rentabilidad_minima == true){
														fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center;"><i class="fa fa-fw fa-circle" title="Inactivo Gestur" style="color:green;"></i></th>';		
													}else{
														fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center;"><i class="fa fa-fw fa-circle" title="Inactivo Gestur" style="color:red;"></i></th>';		
													}
													fila_contenido += '<td style="padding: 0.25rem 0.5rem;">';
													fila_contenido += '<a id="mostrar_'+valor_anteriores+'" onclick="mostrarDetalle('+valor_anteriores+','+rsps.id+')" class="glyphicon glyphicon-save-file" title="Desplegar Detalle" numero ="'+rsps.id+'" data="'+valor_anteriores+'" style="margin-left: 5px;"><i class="ft-download" style="font-size: 18px;"></i></a>';
													fila_contenido += '<a style ="display:none" onclick="ocultarDetalle('+valor_anteriores+')"  id="ocultar_'+valor_anteriores+'" numero ="'+rsps.id+'" class="glyphicon glyphicon-open-file" data="'+valor_anteriores+'"  title="Ocultar Detalle" style="margin-left: 5px;"><i class="ft-upload" style="font-size: 18px;"></i></a>';
													fila_contenido += '</td>';
													fila_contenido += '</tr>';
													fila_contenido += '<tr class="ocultado" id="oculto_'+valor_anteriores+'" style="display: none"></tr>';	
													fila_contenido += '</td>';
													tbody.append(fila_contenido);
												}
											});
											valor_anterior = valor_anteriores;
									}

									var indicadorItem = parseInt(valor_anterior)+ 1;	
									$('#item').val(indicadorItem);
									$('#cant_0').val(1);
									$('#indice_0').val(indicadorItem);
									$("#requestModalDetalle").modal('hide');
									$("#producto_0").select2().val(0).trigger("change");
									$.toast({
											 heading: 'Exito',
											text: 'Se ha creado el Detalle.',
											position: 'top-right',
											showHideTransition: 'slide',
										    icon: 'success'
										});  
									$('#reserva_nemo_id').val("");
									$('#reserva_cangoroo_id').val("");
									$('#grupo_reserva_cangoroo_id').val("");
									$('#tipo_reserva_cangoroo').val('');
									$("#requestModalDetalle").modal('hide');
									inicioNeW();

									//Limpiar select de prestador y proveedor
									//Error sucede cuando se selecciona mismo proveedor al crear otro item
									$('#proveedor_0').val('');
									$('#prestador_0').val('');
									
								}else{
									$.toast({
											heading: 'Error',
											text: 'Ocurrio un error en la comunicación con el servidor.',
											showHideTransition: 'fade',
											position: 'top-right',
											icon: 'error'
										});
								}
							}
			});
		}

		$("#editar").click(function(){
			if($("#proveedor_id_0").val() != "" || $("#proveedor_id_0").val() != 0){
				if($("#prestador_id_0").val() != "" || $("#prestador_id_0").val() != 0){
					var idEmpresaBase = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}"
					if(idEmpresaBase == 'D'){
					if($("#fecha_gasto_0").val() != null && $("#monto_gasto_0").val() != 0){	
						$('#fecha_gasto_0').css('border-color', '#d2d6de');
						$('#monto_gasto_0').css('border-color', '#d2d6de');	
						base = $('#linea').val();	 
						$('#datosDetalles').find('input,select,select2,textarea').prop("disabled", false);
						if($("#tipo_0s").val() == 1){
							if($("#comision_operador_0").val() <= 0){
								$("#comision_operador_0").css('border-color', 'red');
								$.unblockUI();
								$.toast({
										heading: 'Error',
										text: 'La comisión no puede ser 0.',
										showHideTransition: 'fade',
										position: 'top-right',
										icon: 'error'
									});
							}else{
								$('#comision_operador_0').css('border-color', '#d2d6de');
								updateDtp();
							}
						}else{	
							updateDtp();
							$('#comision_operador_0').css('border-color', '#d2d6de');
							//$("#modalProveedorNeto").modal('show');

						}
					}else{	
							$.unblockUI();
							$('#fecha_gasto_0').css('border-color', 'red');
							$('#monto_gasto_0').css('border-color', 'red');
							$.toast({
									heading: 'Error',
									ext: 'Se debe ingresar el monto y la fecha del gasto.',
									showHideTransition: 'fade',
									position: 'top-right',
									icon: 'error'
									});									
						}	
				}else{	
					if($("#costo_0").val()){
						$('#costo_0').css('border-color', '#d2d6de');	
						$('#gravado_costo_0').css('border-color', '#d2d6de');
						if($("#gravado_costo_0").val()){
							$('#costo_0').css('border-color', '#d2d6de');
							$('#gravado_costo_0').css('border-color', '#d2d6de');
							base = $('#linea').val();	 
								$('#datosDetalles').find('input,select,select2,textarea').prop("disabled", false);
								if(idEmpresaBase != 'V'){
									if($("#tipo_0s").val() == 1){
										if($("#comision_operador_0").val() <= 0){
											$("#comision_operador_0").css('border-color', 'red');
											$.unblockUI();
											$.toast({
													heading: 'Error',
													text: 'La comisión no puede ser 0.',
													showHideTransition: 'fade',
													position: 'top-right',
													icon: 'error'
												});
										}else{
											updateAgencia();
										}
									}else{	
										updateAgencia();
										//$("#modalProveedorNeto").modal('show')
									}
								}else{
									updateAgencia();
								}
						}else{	
							$.unblockUI();
							$('#gravado_costo_0').css('border-color', 'red');
							$.toast({
									heading: 'Error',
									text: 'Se deben ingresar los costo gravado.',
									showHideTransition: 'fade',
									position: 'top-right',
									icon: 'error'
								});									
						}
					}else{	
						$.unblockUI();
						$('#costo_0').css('border-color', 'red');
						$.toast({
								heading: 'Error',
								text: 'Se deben ingresar el costo exento.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
						});									
					}
				//////////////////////////////////////////////////			
					}	
				}else{	
						$.unblockUI();
						$.toast({
								heading: 'Error',
								text: 'Ingrese un proveedor.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
						});									
					}
			}else{	
				$.unblockUI();
				$.toast({
								heading: 'Error',
								text: 'Ingrese un prestador.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
						});									
			}					
	 });


		function guardarPrestador(){
			$('.btnPrestador').click(function(){
				var idPrestador = $(this).attr('data');
				var dataString  = {'idPrestador':idPrestador};
				$.ajax({
						type: "GET",
						url: "{{route('guardarPrestador')}}",
						dataType: 'json',
						data: dataString,
				       	error: function(jqXHR,textStatus,errorThrown){
			                   $.toast({
							    heading: 'Error',
							    text: 'Ocurrio un error en la comunicación con el servidor.',
							    position: 'top-right',
							    showHideTransition: 'fade',
							    icon: 'error'
							});
			                    $.unblockUI();

							},
						success: function(rsp){
								var newOption = new Option(rsp.nombre, rsp.id, false, false);
								$('#prestador_0').append(newOption);
								$("#prestador_0").select2().val(rsp.id).trigger("change");
								$("#requestModalPrestador").modal('hide');
						}
						
				})		
			});

		}

	   function datosEspecificos(producto, item){
	   			errase();
				$('#costo_0').css('border-color', '#d2d6de');
				$('#gravado_costo_0').css('border-color', '#d2d6de');
	   			$('#requestModalDetalle').find('input,select,select2,textarea').prop("disabled", false);
	   			perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";


				if(perfil == 1){
						$('#gravado_costo_0').prop("disabled", true);
				}else{
						$('#gravado_costo_0').prop("disabled", false);
				}
				
				$.ajax({
					type: "GET",
					url: "{{route('consultaGrupoProducto')}}",
					dataType: 'json',
					data: {
							dataPrducto: producto,
						},
					success: function(rsp){
							let es_nemo = false;
							let es_cangoroo = false;

							$('#reservaNemo').css('display', 'none');
							$('#reservaCangoroo').css('display', 'none');

							if(rsp.producto_nemo){
								es_nemo = true;
								$('#reservaNemo').css('display', 'block'); 
							} else if(rsp.producto_cangoroo){
								es_cangoroo = true;
                                $('#reservaCangoroo').css('display', 'block');
							}

							 
								

							var valor = parseInt(rsp.grupo_producto);
							 
							if(valor != 0){

								//Aereos
								if(valor == 1){
									perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
									if(perfil == 1){
										$('#gravado_costo_0').prop("disabled", true);
									}else{
										$('#gravado_costo_0').prop("disabled", false);
									}
									$('#especificosDatos').css('display', 'block');
									$('#productoTicket').val(producto);
									var contenido = "";
										contenido += "<div class='row'>";
										contenido += "<div class='col-md-3'>";
										contenido += "<div class='input-group-sm'>";
										contenido += "<label>Tipo</label><br>";
										contenido += "<select class='form-control input-sm select2' name='tipo' id='tipo' style='width: 100%;'>";
										contenido += "<option value='0'>Selecccione un Tipo</option>";
										@foreach($tiposTickets as $tipoticket)
											tipo_ticket_id = "{{$tipoticket->id}}";
											tipo_ticket_descripcion ='{{$tipoticket->descripcion}}';
											if(tipo_ticket_id == 1){
												contenido += `<option value='`+tipo_ticket_id+`' selected='selected'>`+tipo_ticket_descripcion+`</option>`;
											}else{	
												contenido += `<option value='`+tipo_ticket_id+`'>`+tipo_ticket_descripcion+`</option>`;
											}	
										@endforeach
										contenido += "</select>";
										contenido += "</div>";
										contenido += "</div>";
										contenido += "</div>";
									$.ajax({
											type: "GET",
											url: "{{route('filtrarDatosLista')}}",
											data: {
												idMonedaProforma: $("#moneda").val(),
												idGrupo: $("#grupo_id").val(),
												idProforma: $("#proforma_id").val()
											},
											dataType: 'json',
											success: function(rsp){
													$("#total_ticket").val(0);
													$("#totalVenta").val(0);
													$("#fee_unico").val(0);
													$("#ticket_0").val('');

													contenido += "<div class='row'>";
													contenido += "<div class='col-md-12'> ";
													contenido += "<table id='vuelos' class='table' style='width: 100%;font-size: 0.9rem;'>";
													contenido += "<thead>";
													contenido += "<tr>";
													contenido += "<th></th>";
													contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Nro Ticket</th>";
													contenido += "<th style='padding-left: 0px;padding-right: 0px;'>PNR</th>";
													contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Pasajero</th>";
													contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Moneda</th>";
													contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Venta</th>";
													contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Tarifa</th>";
													contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Aerolinea</th>";
													contenido += "<th id='thgrupo' style='padding-left: 0px;padding-right: 0px;'>Grupo</th>";
													contenido += "</tr>";
													contenido += "</thead>";	
													$.each(rsp, function (key, item){
															if(item.numero_amadeus !== null){
																numeroT = item.numero_amadeus;
															}else{
																numeroT = '';
															}	
															if(item.pasajero !== null){
																pasajeroT = item.pasajero;
															}else{
																pasajeroT = '';
															}
															if(item.comision != 0){
																comision = 'Comisionable';
															}else{
																comision = 'Neto';
															}
															if(jQuery.isEmptyObject(item.currency) != true){
																valor = item.currency.currency_code	
															}else{
																valor = "";
															}
															var n = item.persona;
															if(n != null){
																proveedor = (item.persona.nombre != null) ? item.persona.nombre : '';
															}else{
																proveedor = '';
															}	
													contenido += "<tr id='columna_"+item.id+"'>";
													contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 0px;'><input type='checkbox' id='"+item.id+"' style='width: 15px;height: 25px;' class='form-control listOrigen "+item.id_proveedor+"' indice='"+item.id_grupo+"' data='"+item.id_proveedor+"' comision='"+item.comision+"' currency = '"+item.id_currency_costo+"' onclick='pasar("+item.id+")'  value='"+item.id+"' onclick='pasar("+item.id+")' name='tickets["+item.id+"]'></td>";
													contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+numeroT+"</td>";
													contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+item.pnr+"</td>";
													contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+pasajeroT+"</td>";
													contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+valor+"</td>";
													contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+item.precio_venta+"</td>";
													contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+comision+"</td>";
													contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+proveedor+"</td>";
													contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'></td>";
													contenido += "</tr>";
												})
												contenido += "</table>";
												contenido += "</div>";	
												contenido += "</div>";
												contenido += "<div class ='row'>";
												contenido += "<div class='col-md-4'></div>";
												contenido += "<div class='col-md-3'>";
												contenido += '<input type="hidden" disabled="disabled" class = "Requerido form-control form-control-sm" id="total_ticket" value="0" name="total_ticket" disabled="disabled" />';
												contenido += '<label>FEE</label>';
												contenido += '<input type="text" class = "Requerido form-control form-control-sm fee numeric" id="fee_unico" value="0" name="fee_unico"/>';
												contenido += "</div>";
												contenido += "<div class='col-md-3'>";
												contenido += '<label>Total Tickets</label>';
												contenido += '<input type="text" disabled="disabled" class = "Requerido form-control form-control-sm" id="totalVenta" value="0" name="totalVenta" disabled="disabled"/>';
												contenido += "</div>";
												contenido += "<div class='col-md-1'>";
												contenido += "<br>";
												contenido += '<button type="button" class="btn btn-outline-primary" style="width: 100px;" id="procesar"><b>Procesar</b></button>';
												contenido += "</div>";
												contenido += "</div>";
												contenido += "<div id ='ticketsSelect'>";
												contenido += "</div>";
												$('#idEspecifico').html(contenido);
												inizializateVuelo();
												$('#especificosDatos .collapse').collapse('show');

											}//success	
										});//ajax	
								}

								//Alojamientos
								if(valor == 2){
									if(es_nemo){
										datosReservasHotelNemo();
									}

									if(es_cangoroo){
										datosReservasCangoroo(1);
									}
								}

								//Otros/Terrestre
								if(valor == 6){
									if(es_nemo){
										datosReservasCircuitoNemo();
									} 

									if(es_cangoroo){
										datosReservasCangoroo(2);
									}
								}

								//Asistencia al Viajero
								if(valor == 3){
										perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
										if(perfil > 1){
											$('#gravado_costo_0').prop("disabled", false);
										}

										contenido = '';
										$('#especificosDatos').css('display', 'block');
										contenido += '<div class="row">';
										contenido += '<div class="col-md-3" style="padding-left: 25px;">';
										contenido += '<div class="form-group">';
										contenido += '<label>Tipo Asistencia</label>';
										contenido += '<select class="form-control form-control-sm select2" name="tipo_asistencia" id="tipoAsistencia" style="width: 100%;" >';
										contenido += '<option value="0">Seleccione Tipo de Asistencia</option>';
										@foreach($asistencias as $key=>$asistencia)
										contenido += '<option value="{{$asistencia->id}}">{{$asistencia->tarifa_asistencia_nombre}} - {{$asistencia->tarifa_asistencia_precio}}</option>';
										@endforeach	
										contenido += '</select>';
										contenido += '</div>';
										contenido += '</div>';
										/*contenido += '<div class="col-md-3" style="padding-left: 25px;">';
										contenido += '<label>In/Out</label>';
										contenido += '<input type="text" class = "form-control form-control-sm periodos" id="inout"  name="in_out"/>';
										contenido += '</div>'; */
										contenido += '<div class="col-md-3" style="padding-left: 25px;">';
										contenido += '<div class="form-group">';
										contenido += '<label>Cantidad de Personas</label>';
										contenido += '<input type="text" onkeypress="return justNumbers(event);" class = "Requerido form-control form-control-sm" name="personas" id="personasAsistencia" value="0" />';
										contenido += '</div>';
										contenido += '</div>';  
										contenido += '<div class="col-md-3" style="padding-left: 25px;">';
										contenido += '<label></label>';
										contenido += '<br>';
										contenido += '<button type="button" id="btnAsistencia" class="btn btn-outline-primary" style="width: 90px; ">Aceptar</button>';
										contenido += '</div>';	
										contenido += '</div>';
										$('#idEspecifico').html(contenido);
										inizializateAsistencia(producto);
										$('#especificosDatos .collapse').collapse('show');
								}

								//Traslados
								if(valor == 8){
									perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
										//console('Llego');
										if(perfil == 1){
											$('#gravado_costo_0').prop("disabled", true);
										}else{
											$('#gravado_costo_0').prop("disabled", false);
										}

										contenido = '';
										$('#especificosDatos').css('display', 'block');
										contenido += '<div class="row">';
										contenido += '<div class="col-md-3" style="padding-left: 25px;">';
										contenido += '<div class="form-group">';
										contenido += '<label>Tipo Asistencia</label>';
										contenido += '<select class="form-control form-control-sm select2" name="tipo_asistencia" id="tipoAsistencia" style="width: 100%;" >';
										contenido += '<option value="0">Seleccione Tipo de Asistencia</option>';
										@foreach($asistencias as $key=>$asistencia)
										contenido += '<option value="{{$asistencia->id}}">{{$asistencia->tarifa_asistencia_nombre}} - {{$asistencia->tarifa_asistencia_precio}}</option>';
										@endforeach	
										contenido += '</select>';
										contenido += '</div>';
										contenido += '</div>';
										contenido += '<div class="col-md-3" style="padding-left: 25px;">';
										contenido += '<div class="form-group">';
										contenido += '<label>Cantidad de Personas</label>';
										contenido += '<input type="text" onkeypress="return justNumbers(event);" class = "Requerido form-control form-control-sm" name="personas" id="personasAsistencia" value="0" />';
										contenido += '</div>';
										contenido += '</div>';  
										contenido += '<div class="col-md-3" style="padding-left: 25px;">';
										contenido += '<label></label>';
										contenido += '<br>';
										contenido += '<button type="button" id="btnAsistencia" class="btn btn-outline-primary" style="width: 90px; ">Aceptar</button>';
										contenido += '</div>';	
										contenido += '</div>';
										$('#idEspecifico').html(contenido);
										inizializateAsistencia(producto);
										$('#especificosDatos .collapse').collapse('show');
										
										if(es_nemo){
											datosReservasTrasladoNemo();
										} 
										
										if(es_cangoroo) {
											datosReservasCangoroo(3);
										}
										
										
								}

								//Traslados
								if(valor == 8){
									perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
									if(perfil > 1){
											$('#gravado_costo_0').prop("disabled", false);
									}
									$('#guardar').prop("disabled", true);
									contenido = '';
									$('#especificosDatos').css('display', 'block');
									var contenido = "";
									contenido += "<div class='row'>";
									contenido += "<div class='col-md-12'>";
									contenido += "<div class='input-group-sm'>";
									contenido += "<label>Tipo</label><br>";
									contenido += '<select class="form-control select2" name="tipo_traslado_id" id="tipo_traslado_id" style="width: 100%;" >';
									contenido += '<option value="0">Seleccione Tipo</option>';
									contenido += '<option value="1">In</option>';
									contenido += '<option value="2">Out</option>';
									contenido += '<option value="3">In/Out</option>';
									contenido += '</select>';
									contenido += "</div>";
									contenido += "</div>";
									contenido += "</div>";
									contenido += '<div id="inDiv" style="display: none;">';
									contenido += '<div class="row" style="margin-top: 10px;">';
									contenido += '<div class="col-md-3" style="padding-left: 25px;">';
									contenido += '<label>Fecha In</label>';
									contenido += '<div class="input-group">';
									contenido += '<div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">';
									contenido += '<i class="fa fa-calendar"></i>';
									contenido += '</div>';
									contenido += '<input type="text" value="" class="form-control form-control-sm pull-right fechaHora" name="in" id="in">';
									contenido += '</div>';
									contenido += '</div>';
									contenido += '<div class="col-md-3" style="padding-left: 10px;padding-right: 20px;">';
									contenido += '<label>Hora In:</label>';
									contenido += '<div class="input-group">';
									contenido += '<input type="time" name="horaIn" id="horaIn" class="form-control form-control-sm">';
									contenido += '<div class="input-group-addon">';
									contenido += '<i class="fa fa-clock-o"></i>';
									contenido += '</div>';
									contenido += '</div>';
									contenido += '</div> ';
									contenido += '<div class="col-md-6" style="padding-left: 10px;padding-right: 20px;">';
									contenido += '<label>Numero de Vuelo</label>';
									contenido += '<input type="text" class = "Requerido form-control form-control-sm" maxlength="100" name="vuelo_in" id="vuelo_in" style="font-weight: bold;font-size: 15px;"/>';
									contenido += '</div>';
									contenido += '</div>';
									contenido += '</div>';	
									contenido += '<div id="outDiv" style="display: none;">';
									contenido += '<div class="row" style="margin-top: 10px;">';						
									contenido += '<div class="col-md-3" style="padding-left: 25px;">';
									contenido += '<label>Fecha Out</label>';
									contenido += '<div class="input-group">';
									contenido += '<div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">';
									contenido += '<i class="fa fa-calendar"></i>';
									contenido += '</div>';
									contenido += '<input type="text" value="" class="form-control form-control-sm pull-right fechaHora" name="out" id="out">';
									contenido += '</div>';
									contenido += '</div>';
									contenido += '<div class="col-md-3" style="padding-left: 10px;padding-right: 20px;">';
									contenido += '<label>Hora Out:</label>';
									contenido += '<div class="input-group">';
									contenido += '<input type="time" class="form-control form-control-sm" name="horaOut" id="horaOut">';
									contenido += '<div class="input-group-addon">';
									contenido += '<i class="fa fa-clock-o"></i>';
									contenido += '</div>';
									contenido += '</div>';
									contenido += '</div>';   	
									contenido += '<div class="col-md-6" style="padding-left: 10px;padding-right: 20px;">';
									contenido += '<label>Numero de Vuelo</label>';
									contenido += '<input type="text" class = "Requerido form-control form-control-sm" maxlength="100" name="vuelo_out" id="vuelo_out" style="font-weight: bold;font-size: 15px;"/>';
									contenido += '</div>';
									contenido += '</div>';	
									contenido += '</div>';
									contenido += '<div class="row" style="margin-top: 10px;">';	
									contenido += '<div class="col-md-8" style="padding-left: 25px;">';	
									contenido += '</div>';				
									contenido += '<div class="col-md-3" style="padding-left: 25px;">';
									contenido += '<button type="button" id="btnActualizarTraslado" class="btn btn-outline-primary" style="width: 90px;margin-left: 80%;" >Aceptar</button>';
									contenido += '</div>';
									$('#idEspecifico').html(contenido);
									inizializateTraslado();
									$('#especificosDatos .collapse').collapse('show');
								}

								//Actividades
								if(valor == 13){
									datosReservasActividadNemo();
								}

								//Asistencia UA
								if(valor == 10){
												perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
												if(perfil == 4 ||perfil == 8){
													$("#gravado_costo_0").prop("disabled", false);

													$('#especificosDatos').css('display', 'block');
													var contenido = "";
													contenido += "<div class='row'>";						
													contenido += '<div class="col-md-2 col-sm-3 col-md-3" style="padding-left: 0px;">';
													contenido += '<div class="form-group">';
													contenido += '<label>Tipo Asistencia</label>';
													contenido += '<select class="form-control select2" name="tipo_asistenciaAU" id="tipoAsistenciaAU" style="width: 100%;" >';
													contenido += '<option value="0">Seleccione Tipo de Asistencia</option>';
													@foreach($asistencias as $key=>$asistencia)
													contenido += '<option value="{{$asistencia->id}}">{{$asistencia->tarifa_asistencia_nombre}} - {{$asistencia->tarifa_asistencia_precio}}</option>';
													@endforeach	
													contenido += '</select>';
													contenido += '</div>';
													contenido += '</div>';
													contenido += '<div class="col-md-2 col-sm-1 col-md-1" style="padding-left: 0px;padding-right: 0px;">';
													contenido += '<div class="form-group">';
													contenido += '<label>Personas</label>';
													contenido += '<input type="text" onkeypress="return justNumbers(event);" class = "Requerido form-control form-control-sm" name="personasAU" id="personasAsistenciaAU" value="0" />';
													contenido += '</div>';
													contenido += '</div> ';
													contenido += '<div class="col-md-2 col-sm-1 col-md-1" style="padding-left: 0px;padding-right: 0px;">';
													contenido += '<br>';
													contenido += '<button type="button" id="btnAsistenciaUA" style="margin-left:20px;" class="btn btn-outline-primary" >Aceptar</button>';
													contenido += '</div>';
													contenido += '</div>';

													$('#idEspecifico').html(contenido);
													inizializateUA(producto);
													var fechaIn = rsps[0].fecha_in.split('-');
													var periodoIn = fechaIn[2]+'/'+fechaIn[1]+'/'+fechaIn[0];
													var fechaOut = rsps[0].fecha_out.split('-');
													var periodoOut = fechaOut[2]+'/'+fechaOut[1]+'/'+fechaOut[0];
													$("#periodo_0").val(periodoIn+" - "+periodoOut).change();
													$("#personasAsistencia").val(rsps[0].asistencia_cantidad_personas);	
													$("#descrip_factura_0").val(rsps[0].descripcion);
													$("#confirmacion_0").val(rsps[0].cod_confirmacion);
													$("#moneda_compra_0").val(rsps[0].currency_costo_id).select2();
													$("#costo_0").val(rsps[0].costo_proveedor);
													$("#tasas_0").val(rsps[0].monto_no_comisionable);
													$("#base_comisionable_0").val(rsps[0].base_comisionable);
													$("#comision_agencia_0").val(rsps[0].porcentaje_comision_agencia);
													$("#venta_0").val(rsps[0].precio_venta);
													$("#markup_0").val(rsps[0].monto_gasto);	
													var fechaPago = rsps[0].fecha_pago_proveedor
													var fechaPagoFormato = fechaPago[2]+'/'+fechaPago[1]+'/'+fechaPago[0];
													$("#vencimientos").val(fechaPagoFormato);
													if(jQuery.isEmptyObject( rsps[0].fecha_gasto) == false){
														var fechaGasto = rsps[0].fecha_gasto.split('-');
														var fechaGastoFormato = fechaGasto[2]+'/'+fechaGasto[1]+'/'+fechaGasto[0];
													}else{
														var fechaGastoFormato = "";
													}	
													$("#exentos_0").val(rsps[0].porcion_exenta);
													$("#gravada_0").val(rsps[0].porcion_gravada);

													$("#gravado_costo_0").val(rsps[0].costo_gravado);
													$("#fecha_gasto_0").val(fechaGastoFormato);
													$("#monto_gasto_0").val(rsps[0].monto_gasto);
													$('#especificosDatos .collapse').collapse('show');
													//$("#tipoAsistenciaAU").val(rsps[0].id_tarifa_asistencia).select2();
													$("#tipoAsistenciaAU").val(rsps[0].id_tarifa_asistencia).trigger('change.select2');
													$("#personasAsistenciaAU").val(asistencia_cantidad_personas);
													inizializateUA(producto)
												}	
								} else {
												$("#inout").val(rsps[0].fecha_in+""+rsps[0].fecha_out);	
												var fechaIn = rsps[0].fecha_in.split('-');
												var periodoIn = fechaIn[2]+'/'+fechaIn[1]+'/'+fechaIn[0];
												var fechaOut = rsps[0].fecha_out.split('-');
												var periodoOut = fechaOut[2]+'/'+fechaOut[1]+'/'+fechaOut[0];
												$("#periodo_0").val(periodoIn+" - "+periodoOut).change();
												$("#personasAsistencia").val(rsps[0].asistencia_cantidad_personas);	
												$("#descrip_factura_0").val(rsps[0].descripcion);
												$("#confirmacion_0").val(rsps[0].cod_confirmacion);
												$("#moneda_compra_0").val(rsps[0].currency_costo_id).select2();
												$("#costo_0").val(rsps[0].costo_proveedor);
												$("#tasas_0").val(rsps[0].monto_no_comisionable);
												$("#base_comisionable_0").val(rsps[0].base_comisionable);
												$("#comision_agencia_0").val(rsps[0].porcentaje_comision_agencia);
												$("#exentos_0").val(rsps[0].porcion_exenta);
												$("#gravada_0").val(rsps[0].porcion_gravada);

												$("#gravado_costo_0").val(rsps[0].costo_gravado);
												$("#venta_0").val(rsps[0].precio_venta);
												$("#markup_0").val(rsps[0].monto_gasto);
												if(jQuery.isEmptyObject(rsps[0].fecha_gasto) == false){
													var fechaPago = rsps[0].fecha_pago_proveedor.split('-');
													var fechaPagoFormato = fechaPago[2]+'/'+fechaPago[1]+'/'+fechaPago[0];
												}else{
													var fechaPagoFormato = '';	
												}
												$("#vencimientos").val(fechaPagoFormato);
												if(jQuery.isEmptyObject(rsps[0].fecha_gasto) == false){
													var fechaGasto = rsps[0].fecha_gasto.split('-');
													var fechaGastoFormato = fechaGasto[2]+'/'+fechaGasto[1]+'/'+fechaGasto[0];
												}else{
													var fechaGastoFormato = rsps[0].fecha_gasto;	
												}
												$("#fecha_gasto_0").val(fechaGastoFormato);
												$("#monto_gasto_0").val(rsps[0].monto_gasto);

												if(rsps[0].origen == 'A'){
													$('#requestModalDetalle').find('input,select,select2,textarea').prop("disabled", true);
													$("#comision_agencia_0").prop("disabled", false);
													$("#venta_0").prop("disabled", false);
													$("#editar").css('display', 'block')
												}
								}


								$("#comision_operador_0").val(rsps[0].comision_operador);
								perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
								if(perfil == 1){
									$('#gravado_costo_0').prop("disabled", true);
								}else{
									$('#gravado_costo_0').prop("disabled", false);
								}


							}else{
								$.toast({
									heading: 'Error',
									text: 'El producto no esta asignado a un Grupo.',
									showHideTransition: 'fade',
									position: 'top-right',
									icon: 'error'
								});
							}		
					}
				})	
				$.ajax({
						type: "GET",
						url: "{{route('getComisionAgencia')}}",
						dataType: 'json',
						data: {
								dataProducto: producto, 
								dataProforma: $('#proforma_id').val()
				       			},
						success: function(rsp){
								perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
								idSucursalAgencia = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia}}";
								if(perfil == 1||perfil == 4||perfil == 8 || perfil== 17 && idSucursalAgencia == 58902){									
									if(rsp == null){
										$('#comision_agencia_0').val(0);
										$('#com_agencia_0').val(0);
									}else{
										$('#comision_agencia_0').val(rsp);
										$('#com_agencia_0').val(rsp);
									}
								}else{
									$('#comision_agencia_0').val(0);
									$('#comision_agencia_0').prop("disabled", true);
									$('#com_agencia_0').val(0);
								}

						}	
				});
				$.ajax({
					type: "GET",
					url: "{{route('getProducto')}}",
					dataType: 'json',
					data: {
							dataProducto: producto,
						},
						success: function(rsp){
									$('#descrip_factura_0').val(rsp);
									$('#productoDescripcion').html(rsp);
									//$('#productoDescripcion').prop("disabled", true);
								}	
						});
			
		}		

		function inizializateUA(producto){
			$("#productoAsisten").val(producto);
			$("#tipoAsistenciaAU").val(0).select2();
			$("#personasAsistenciaAU").val(0);
			$("#diasAsistenciaAU").val(0);
			$("#personasAsistenciaAU").val(0);
			$("#diasAsistenciaAU").val(0);							    
			$("#costo_0").prop("disabled", false);
			$("#gravado_costo_0").val(0);
			$("#gravado_costo_0").prop("disabled", false);
			$('#btnAsistenciaUA').click(function(){
				if($("#tipoAsistenciaAU").val() != 0){
					if($("#diasAsistenciaAU").val() != "" && $("#diasAsistenciaAU").val() != 0 ){
						if($("#personasAsistenciaAU").val() != "" && $("#personasAsistenciaAU").val() != 0  ){
							if($("#exentaAsistencia").val() != "" && $("#exentaAsistencia").val() != 0  ){
								if($("#gravadaAsistencia").val() != "" && $("#gravadaAsistencia").val() != 0  ){
									$('#mensajaErrorSuperior').hide();
									var dataString = $("#datosEspecificos").serialize();
									$.ajax({
											type: "GET",
											url: "{{route('guardarAsistenciaAU')}}",
											dataType: 'json',
											data: dataString,
											success: function(rsp){
																$("#cantidad_personas_0").val(rsp.cant_personas);
																$("#costo_0").val(rsp.costo);
																$("#venta_0").val(rsp.venta);
																$("#cantidad_dias_0").val(rsp.dias);
																$("#tarifa_asistencia_0").val(rsp.tipo_asistencia)
																$("#cantidad_personas_0").prop("disabled", true);
																$("#confirmacion_0").focus();
																$("#exentos_0").val(rsp.exenta);
																$("#gravada_0").val(rsp.gravada);
																$(".sale").trigger('change');
																$("#costo_0").prop("disabled", false);
																$("#gravado_costo_0").val(rsp.gravada);
																$("#gravado_costo_0").prop("disabled", false);	
																$('#especificosDatos .collapse').collapse('hide');
															}
										});
								}else{
									$('#mensajaErrorSuperiorUA').html('Ingrese monto gravada');					
							   }	
							}else{
								$('#mensajaErrorSuperiorUA').html('Ingrese monto exenta');					
							}	
						}else{
							$('#mensajaErrorSuperiorUA').html('Ingrese personas en Asistencia');					
						}	
					}else{
						$('#mensajaErrorSuperiorUA').html('Ingrese los dias de Asistencia');					
					}	
				}else{
					$('#mensajaErrorSuperiorUA').html('Seleccione el tipo de Asistencia');					
				}		
			});

		}

		function ordenarSelect(id_componente)
	    {
	      var selectToSort = jQuery('#' + id_componente);
	      var optionActual = selectToSort.val();
	      selectToSort.html(selectToSort.children('option').sort(function (a, b) {
	        return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
	      })).val(optionActual);
	    }

			//BOTON ELIMINAR LINEA DE PROFORMA
			//$('.btn-menos-request').click(function(){ 
		function eliminarFila(valor){
					//valor = $(this).attr('data');
			return swal({
                    title: "GESTUR",
                    text: "¿Está seguro de que desea eliminar el ítem?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, eliminar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            eliminarDetalle(valor);
                        } else {
                            swal("Cancelado", "", "error");
                        }
            	});



		}



	function eliminarDetalle(valor){
		$.ajax({
			type: "GET",
			url: "{{route('deleteLinea')}}",
			dataType: 'json',
			data: {
					dataLinea: valor,
					dataProforma: $('#proforma_id').val()
				},
			success: function(rsp){
						if(rsp.status == 'OK'){
							swal("Éxito", rsp.mensaje, "success");
							$('.ocultado').hide();
							$('table#lista_productos tr#'+rsp.item).remove();
							$('#botones_'+rsp.item).empty();
							if(rsp.nemo == 1){
								linea = rsp.item + 1;
								$('table#lista_productos tr#'+linea).remove();
							}
							actualizarCabecera();
							//location.reload();
						}else{
							swal("Cancelado", rsp.mensaje, "error");
						}
					}	
			});		
	}
	
	$('#btnNemo').click(function(){
			$.ajax({
					type: "GET",
					url: "{{route('getDatosNemo')}}",
					dataType: 'json',
					success: function(rsp){
								var oSettings = $('#rowNemoDetalles').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();
								for (i=0;i<=iTotalRecords;i++) {
									$('#rowNemoDetalles').dataTable().fnDeleteRow(0,null,true);
								}	
								$.each(rsp, function (key, item){
									checkbox = '<input type="radio" value="'+item.id+'" class="form-check-input resNem" name="reservaNemo">';
									var fecha = item.fecha_creacion;
									var fechaIntermedia = fecha.split(' ');
									var fechaFinal = fechaIntermedia[0].split('-');
									fechaMostrar = fechaFinal[2]+'/'+fechaFinal[1]+'/'+fechaFinal[0];

									var fecha = item.check_in;
									var fechaIntermedia = fecha.split(' ');
									var checkIn = fechaIntermedia[0].split('-');
									fechacheckIn = checkIn[2]+'/'+checkIn[1]+'/'+checkIn[0];

									var fecha = item.check_out;
									var fechaIntermedias = fecha.split(' ');
									var checkOut = fechaIntermedias[0].split('-');
									fechacheckOut = checkOut[2]+'/'+checkOut[1]+'/'+checkOut[0];

									var dataTableRow = [	
															checkbox,
															'<b>'+item.hotel_booking_service_item_id+'</b>',
															item.denominacion,
															fechaMostrar,
															'<b>'+item.prestador_name+'</b>',
															fechacheckIn+'<br>'+fechacheckOut,
															item.det_client_name,
															item.hotel_supplier_desc,
															//item.dest_city_desc+' '+item.dest_country_desc,
															'<b>'+item.price_amount_global+' '+item.currency_code+'</b>',
															item.email,
														];
									var newrow = $('#rowNemoDetalles').dataTable().fnAddData(dataTableRow);
								})	
					}
				  })		
				  $('#requestModalNemo').modal('show');
		})	


	$("#btnAceptarNemo").click(function(){	
			var dataString = $("#frmNemo").serialize();
				dataString += '&idProforma='+$("#proforma_id").val();
				if( $("#frmNemo input[name='reservaNemo']:radio").is(':checked')) { 
			   		$.ajax({
									type: "GET",
									url: "{{route('guardarReservaNemo')}}",
									dataType: 'json',
									data: dataString,
									success: function(rsp){
										if(rsp.codigo = 'OK'){
											actualizarCabecera();
											setTimeout(actualizarDetalles, 1000);	
														if(jQuery.isEmptyObject(rsp.detalle[0].fecha_in) == false){
															var fecha = rsp.detalle[0].fecha_in;
															var fechaIntermedia = fecha.split(' ');
															var checkIn = fechaIntermedia[0].split('-');
															fechacheckIn = checkIn[2]+'/'+checkIn[1]+'/'+checkIn[0];

															var fecha = rsp.detalle[0].fecha_out;
															var fechaIntermedias = fecha.split(' ');
															var checkOut = fechaIntermedias[0].split('-');
															fechacheckOut = checkOut[2]+'/'+checkOut[1]+'/'+checkOut[0];
														}	

														valor_anterior = parseInt(rsp.detalle[0].item);
														var tbody = $('#lista_productos tbody');
														fila_contenido = '';
														fila_contenido += '<tr id="'+rsp.detalle[0].item+'" data-toggle="tooltip" data-placement="left" data-html="true" title="<b>Check In :</b>'+fechacheckIn+'<br><b>Check Out :</b> '+fechacheckOut+'<br><b>Prestador :</b> '+rsp.detalle[0].prestador_detalle+'">';

														fila_contenido += '<th style="padding: 0.25rem 0.5rem;">'+valor_anterior+'</th>';
														fila_contenido += '<td style="padding: 0.25rem 1rem;">'+rsp.detalle[0].descripcion+'</td>';
														//fila_contenido += '<td style="padding: 0.25rem 1rem;">'+fechacheckIn+' - '+fechacheckOut+'</td>';
														fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center">'+rsp.detalle[0].cod_confirmacion+'</th>';
														if(jQuery.isEmptyObject(rsp.detalle[0].proveedor)){
															fila_contenido += '<th style="padding: 0.25rem 1rem;"></th>';
														}else{
															fila_contenido += '<th style="padding: 0.25rem 1rem;">'+rsp.detalle[0].proveedor.nombre+'</th>';
														}
														//fila_contenido += '<th style="padding: 0.25rem 1rem;">'+rsp.detalle[0].prestador_detalle+'</th>';
														fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center">'+rsp.detalle[0].currency_costo.currency_code+'</td>';
														fila_contenido += '<td style="padding: 0.25rem 1rem;text-align: center">'+rsp.detalle[0].precio_costo+'</td>';
														fila_contenido += '<td style="padding: 0.25rem 1rem;text-align: center">'+rsp.detalle[0].porcentaje_comision_agencia+'</td>';
														fila_contenido += '<td style="padding: 0.25rem 1rem;text-align: center">'+rsp.detalle[0].comision_agencia+'</td>';
														fila_contenido += '<td style="padding: 0.25rem 1rem;text-align: center">'+rsp.detalle[0].precio_venta+'</td>';
														fila_contenido += '<th style="padding: 0.25rem 1rem;text-align: center">'+rsp.detalle[0].markup+'</th>';			
														fila_contenido += '<td style="padding: 0.25rem 0.5rem;">';
														fila_contenido += '<a id="'+rsp.detalle[0].id+'" class="btn-update-request" onclick="updateDetalle('+rsp.id_detalle_proforma+','+rsp.detalle[0].item+')" title="Editar Detalle" data="'+rsp.detalle[0].item+'" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>';

														fila_contenido += '<a id="mostrar_'+valor_anterior+'" class="glyphicon glyphicon-save-file" title="Desplegar Detalle" numero ="'+rsp.id_detalle_proforma+'" data="'+valor_anterior+'" style="margin-left: 5px;"><i class="ft-download" style="font-size: 18px;"></i></a>';

														fila_contenido += '<a id="ocultar_'+valor_anterior+'" numero ="'+rsp.id_detalle_proforma+'" class="glyphicon glyphicon-open-file" data="'+valor_anterior+'"  title="Ocultar Detalle" style="margin-left: 5px;"><i class="ft-upload" style="font-size: 18px; display:none"></i></a>';

														fila_contenido += '</td>';
														fila_contenido += '</tr>';
														fila_contenido += '<tr class="ocultado" id="oculto_'+valor_anterior+'" style="display: none"></tr>';	

														tbody.append(fila_contenido);

														var indicadorItem = valor_anterior + 1;	
														$('#item').val(indicadorItem);
														$('item_0').val(indicadorItem);

														$('#requestModalNemo').modal('hide');
														var indicadorItem = valor_anterior + 1;	
														$('#item').val(indicadorItem);
														$('#item_0').val(indicadorItem);
														inicioNeW();

										}else{	
											$.toast({
												heading: 'Error',
												text: 'La comisión supera la comisión pactada.',
												showHideTransition: 'fade',
												position: 'top-right',
												icon: 'error'
											});
										}

									}	
								
								})	
					}else{
						$.toast({
		     				heading: 'Error',
		     				position: 'top-right',
		     				text: 'Seleccione una reserva',
		     				showHideTransition: 'fade',
		     				icon: 'error'
		     			});
					}
		})	
		$("#documento_clientes").change(function(){
			$.ajax({
					type: "GET",
					url: "{{route('doPasajeroCliente')}}",
					dataType: 'json',
					data: {
						dataDocumento: $(this).val()
			       			},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
						if(rsp.status == 'OK'){
							$("#documento_clientes").css('border-color', '#ccd6e6');
						}else{
	                        $.toast({
	                            heading: 'Error',
	                             position: 'top-right',
	                            text: 'Ya existe este numero de documento verifiquelo',
	                            showHideTransition: 'fade',
	                            icon: 'error'
	                        });
	                        $("#documento_clientes").val('');
	                        $("#documento_clientes").css('border-color', 'red');
	                        $("#documento_clientes").focus();
						}

					}
			})	
			agregarDvx();		
		});		


		$("#documento_cliente").change(function(){
			$.ajax({
					type: "GET",
					url: "{{route('doPasajeroCliente')}}",
					dataType: 'json',
					data: {
						dataDocumento: $(this).val()
			       			},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
						if(rsp.status == 'OK'){
							$("#documento_cliente").css('border-color', '#ccd6e6');
						}else{
	                        $.toast({
	                            heading: 'Error',
	                             position: 'top-right',
	                            text: 'Ya existe este numero de documento verifiquelo',
	                            showHideTransition: 'fade',
	                            icon: 'error'
	                        });
	                        $("#documento_cliente").val('');
	                        $("#documento_cliente").css('border-color', 'red');
	                        $("#documento_cliente").focus();
						}

					}
			})	
			agregarDv();		
		});		

		$("#documento_prestador").change(function(){
			$.ajax({
					type: "GET",
					url: "{{route('doPasajeroCliente')}}",
					dataType: 'json',
					data: {
						dataDocumento: $(this).val()
			       			},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
						if(rsp.status == 'OK'){
							$("#documento_prestador").css('border-color', '#ccd6e6');
						}else{
	                        $.toast({
	                            heading: 'Error',
	                             position: 'top-right',
	                            text: 'Ya existe este numero de documento verifiquelo',
	                            showHideTransition: 'fade',
	                            icon: 'error'
	                        });
	                        $("#documento_prestador").val('');
	                        $("#documento_prestador").css('border-color', 'red');
	                        $("#documento_prestador").focus();
						}

					}
			})	
			agregarDvt();		
		});		

		function agregarDvt(){
                $.ajax({
                        type: "GET",
                        url: "{{ route('agregarDv') }}",
                        dataType: 'json',
                        data: { documento: $('#documento_prestador').val()},

                         error: function(jqXHR,textStatus,errorThrown){
                            $('.mensaje_err_documento').html(' No se pudo validar. ');
                            $('.mensaje_err_documento').css('color','red');
                            inputReq.VerificarDoc(1,false);
                      
                        },
                        success: function(rsp){
                            if(jQuery.isEmptyObject(rsp[0]) == false){
								$('#documento_prestador').val(rsp[0].ruc);
                                $('#dv_prestador').val(rsp[0].dv);
                                $('#dv_prestador').prop('disabled',true);
                                $('#tipo_identidad_prestador').val(2).trigger('change.select2');
								denominacion = rsp[0].nombre;              
                                base = denominacion.split(',');
                                $('#nombre_prestador').val(base[1]);
                                $('#apellido_prestador').val(base[0]);

                            }else{
                                $('#dv_prestador').prop('disabled',false);
                                $('#tipo_identidad_prestador').val(1).trigger('change.select2');
								$('#nombre_prestador').val('');
                                $('#apellido_prestador').val('');
                            }

                        }
                })   
           }    

		function agregarDv(){
                $.ajax({
                        type: "GET",
                        url: "{{ route('agregarDv') }}",
                        dataType: 'json',
                        data: { documento: $('#documento_cliente').val()},

                         error: function(jqXHR,textStatus,errorThrown){
                            $('.mensaje_err_documento').html(' No se pudo validar. ');
                            $('.mensaje_err_documento').css('color','red');
                            inputReq.VerificarDoc(1,false);
                      
                        },
                        success: function(rsp){
                           // console.log(rsp[0]);
                           $('#documento_cliente').val(rsp[0].ruc);
                            if(jQuery.isEmptyObject(rsp[0].dv) == true){
                                $('#documento_dv').val(rsp[0].dv);
                                $('#documento_dv').prop('disabled',true);
                                $('#tipo_identidad').val(2).trigger('change.select2');
                            }else{
                                $('#documento_dv').prop('disabled',false);
                                $('#tipo_identidad').val(1).trigger('change.select2');
                            }

                            if(jQuery.isEmptyObject(rsp[0].nombre) == false){
                                denominacion = rsp[0].nombre;              
                                base = denominacion.split(',');
                                $('#nombre_cliente').val(base[1]);
                                $('#apellido_cliente').val(base[0]);
                            }
                        }
                })   
           }    

		   function agregarDvx(){
                $.ajax({
                        type: "GET",
                        url: "{{ route('agregarDv') }}",
                        dataType: 'json',
                        data: { documento: $('#documento_clientes').val()},

                         error: function(jqXHR,textStatus,errorThrown){
                            $('.mensaje_err_documento').html(' No se pudo validar. ');
                            $('.mensaje_err_documento').css('color','red');
                            inputReq.VerificarDoc(1,false);
                      
                        },
                        success: function(rsp){
                           // console.log(rsp[0]);
                           $('#documento_clientes').val(rsp[0].ruc);
                            if(jQuery.isEmptyObject(rsp[0].dv) == true){
                                $('#documento_dvs').val(rsp[0].dv);
                                $('#documento_dvs').prop('disabled',true);
                                $('#tipo_identidads').val(2).trigger('change.select2');
                            }else{
                                $('#documento_dvs').prop('disabled',false);
                                $('#tipo_identidads').val(1).trigger('change.select2');
                            }

                            if(jQuery.isEmptyObject(rsp[0].nombre) == false){
                                denominacion = rsp[0].nombre;              
                                base = denominacion.split(',');
                                $('#nombre_clientes').val(base[1]);
                                $('#apellido_clientes').val(base[0]);
                            }
                        }
                })   
           }    

		$("#btnAceptarCliente").click(function(){
			$('#documento_dv').prop('disabled',false);
			var dataString = $("#frmCliente").serialize();
			if($("#nombre_cliente").val().trim() != ''   & $("#documento_cliente").val().trim() != '' 
			 & $("#telefono_cliente").val().trim() != '' & $("#direccion_cliente").val().trim() != ''){
				$.ajax({
						type: "GET",
						url: "{{route('doClienteProformas')}}",
						dataType: 'json',
						data: dataString,
						  error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
						success: function(rsp){
										if(rsp.status == 'OK'){
											$("#nuevoCliente").css('display', 'none')
											var datos = rsp.nombre;
											if(jQuery.isEmptyObject(rsp.apellido) == false){
												var datos = rsp.nombre +' '+rsp.apellido;
											}
											nombre = datos+' - '+rsp.tipo_persona
											var documento = rsp.documento; 
											if(jQuery.isEmptyObject(rsp.dv) == false){
												var documento = rsp.documento+"-"+rsp.dv; 
											}
											resultado = documento +' '+nombre
											var htmlTags =  '<div class="row" id="columna'+rsp.id+'">'+
																	'<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;padding-right: 0px;">'+
																	'<input type="text" id="clienteAuxiliar_'+rsp.id+'" name="clienteAuxiliar_'+rsp.id+'" class ="form-control form-control-sm" value ="'+resultado+'"/>'+
																	'</div>'+
																	'<div class="col-xs-12 col-sm-6 col-md-2">'+
																		'<button type="button" onclick="deleteClienteAux('+rsp.id+')" id="delete_'+rsp.id+'" class="btn btn-danger"><i class="fa fa-minus" aria-hidden="true"></i></button>'+
																	'</div>'+
																'</div>';
													
											$("#clientesAdicionales").append(htmlTags);	
									} else {
										 $.toast({
					                            heading: 'Error',
					                             position: 'top-right',
					                            text: 'Ocurrio un error en la comunicación con el servidor.',
					                            showHideTransition: 'fade',
					                            icon: 'error'
					                        });
											$('#documento_dv').prop('disabled',true);
										}//else
									},
						});	
			}else{
				console.log('No existe')
			}
		});

		$("#btnAceptarClientex").click(function(){
			$('#documento_dvs').prop('disabled',false);
			var dataString = $("#frmClientes").serialize();
			if($("#apellido_clientes").val() != '' || $("#nombre_clientes").val() != '' || $("#documento_clientes").val() != ''){
				$.ajax({
						type: "GET",
						url: "{{route('doClienteProformas')}}",
						dataType: 'json',
						data: dataString,
						  error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
						success: function(rsp){
									$("#requestModalClienteSolicitud").modal('hide');
									if(rsp.status == 'OK'){
										var nombre =rsp.documento+' - '+rsp.nombre+' - '+rsp.id;
										var newOption = new Option(nombre, rsp.id, false, false);
										$('#cliente_pedido').append(newOption);
										$("#cliente_pedido").select2().val(rsp.id).trigger("change");
										$('#documento_dvs').prop('disabled',true);
										$('#ruc').val();
									} else {
										 $.toast({
					                            heading: 'Error',
					                             position: 'top-right',
					                            text: 'Ocurrio un error en la comunicación con el servidor.',
					                            showHideTransition: 'fade',
					                            icon: 'error'
					                        });
											$('#documento_dvs').prop('disabled',true);
										}

									}
						});	
			}else{
				console.log('No existe')
			}
		});




function msjToast(msj, opt = '', jqXHR = false, textStatus = '') {
    if (msj != '') {
        aux.msj.push('<b>' + msj + '</b>');
    }

    if (opt === 'info') {
        $.toast().reset('all');

        $.toast({
            heading: '<b>Atención</b>',
            position: 'top-right',
            text: aux.msj,
            width: '400px',
            hideAfter: false,
            icon: 'info'
        });
        aux.msj = [];

    } else if (opt === 'error') {
        $.toast().reset('all');

        if (jqXHR === false && textStatus === '') {
            msj = aux.msj;
        } else {

            errCode = jqXHR.status;
            errMsj = jqXHR.responseText;

            msj = '<b>';
            if (errCode === 0)
                msj += 'Error de conectividad a internet, verifica tu conexión.';
            else if (errCode === 404)
                msj += 'Error al realizar la solicitud.';
            else if (errCode === 500) {
                if (aux.msj.length > 0) {
                    msj += aux.msj;
                } else {
                    msj += 'Ocurrio un error en la comunicación con el servidor.'
                }

            } else if (errCode === 406) {
                msj += errMsj;
            } else if (errCode === 422)
                msj += 'Complete todos los datos requeridos'
            else if (textStatus === 'timeout')
                msj += 'El servidor tarda mucho en responder.';
            else if (textStatus === 'abort')
                msj += 'El servidor tarda mucho en responder.';
            else if (textStatus === 'parsererror') {
                msj += 'Error al realizar la solicitud.';
            } else
                msj += 'Error desconocido, pongase en contacto con el area tecnica.';
            msj += '</b>';
        }

        $.toast({
            heading: '<b>Error</b>',
            text: aux.msj,
            position: 'top-right',
            hideAfter: false,
            width: '400px',
            icon: 'error'
        });

        aux.msj = [];
    } else if (opt === 'success') {
        $.toast().reset('all');

        $.toast({
            heading: '<b>Exito</b>',
            text: aux.msj,
            position: 'top-right',
            hideAfter: false,
            width: '400px',
            icon: 'success'
        });
        aux.msj = [];

    }
} //

	$('#id_categoria').change(function() {
				var dataString = {idProforma : $('#id_proforma').val(), idPersona : $('#id_categoria').val()};
				$.ajax({
							type: "GET",
							url: "{{route('obtenerIncentivoVenta')}}",
							dataType: 'json',
							data: dataString,

						error: function(jqXHR,textStatus,errorThrown){

								$('#mensaje_cambio').css('color','red');
								$('#mensaje_cambio').html('Ocurrio un error en la comuniación con el servidor');
								$('.cargandoImg').hide();

							},
							success: function(rsp){
													if(jQuery.isEmptyObject(rsp) == false){
														$("#incentivo").select2().val(rsp).trigger("change");
													}else{
														$("#incentivo").select2().val(0).trigger("change");
													}	
												}
						});   

			});  




				$('#btnGuardarEmergencia').on('click',function(){
					$('#btnGuardarEmergencia').prop('disabled',true);
						$.ajax({
								type: "GET",
								url: "{{route('getEmergencia')}}",
								dataType: 'json', 
								data: {
									dataProforma: $('#proforma_id').val(),
									dataTipoEmergencia:  $('#emergencia_tipo :selected').text(),
									dataTipoEmergenciaId:  $('#emergencia_tipo').val(),
									dataProformaEmergencia: $('#proforma_emergencia').val(),
									},
								success: function(rsp){
									if(rsp.status == 'OK'){
										$('#id_expediente').val($('#proforma_emergencia').val()).select2();
										$('#emergencia').val( $('#emergencia_tipo :selected').text())
										@php
							              $logo = Session::get('datos-loggeo')->datos->datosUsuarios->logoAgencia;
							              $errorLogo = asset('mC/images/img/user2-160x160.jpg');
							            @endphp
										 var imgLogo = "{{asset('personasLogo/'.$logo)}}";
						                 var fechaHora = moment().format('DD/MM/YYYY HH:mm:ss');
						                 var nombre = "{{Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido}}"; 
										$('#chatDirect').append(mensajeIzquierda(rsp.comentario.comentario,nombre,fechaHora,rsp.comentario.id_usuario,imgLogo));
										var comentarioCorto = rsp.comentario.comentario.substring(0, 50);
										$('#emergencia_tipo').prop('disabled',true); 
										$('#proforma_emergencia').prop('disabled',true); 
										
										$('#campoComentarios').html(comentarioCorto+"...");
										$('#myModales').modal('hide');
										$.toast({
												heading: 'Exito',
												text: rsp.mensaje,
												position: 'top-right',
												showHideTransition: 'slide',
												icon: 'success'
											});  
									}else{
										$.toast({
											heading: 'Error',
											text: rsp.mensaje,
											showHideTransition: 'fade',
											position: 'top-right',
											icon: 'error'
										});				
										$('#btnGuardarEmergencia').prop('disabled',false);
									}


								}
							})	
					})	
function mensajeIzquierda(mensaje,nombre,fechaHora,id,logo){
				if(logo == ''){
					logo = 'avatar.png';
				}
						var rsp = `<!-- Mensaje por defecto a la izquierda -->
            <div class="direct-chat-msg" data="${id}">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">${nombre}</span>
                <span class="direct-chat-timestamp pull-right">${fechaHora}</span>
              </div><!-- /.direct-chat-info -->
               <img class="direct-chat-img" src="../../personasLogo/${logo}" onerror="this.src='/factour.png'" alt="message user image"><!-- /.direct-chat-img -->
              <div class="direct-chat-text" style=" word-break: break-all;">
                ${mensaje}
              </div><!-- /.direct-chat-text -->
            </div><!-- /.direct-chat-msg -->`;

            return rsp;
				}

		function datosReservasHotelNemo(){
			$.ajax({
					type: "GET",
					url: "{{route('reservasNemo')}}",
					dataType: 'json',
					data: {
							dataTipo: 1,
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){

						//$('#reservaNemo').css('display', 'block');
						table = `<table id="tableNemo" style='width: 100%'>`;
						table += `<thead>`;
						table += `<tr>`;
						    table += `<th></th>`;
							table += `<th>Codigo</th>`;
							table += `<th>Codigo Nemo</th>`;
							table += `<th>Check In</th>`;
							table += `<th>Check Out</th>`;
							table += `<th>Cliente</th>`;
							table += `<th>Usuario</th>`;
							table += `<th>Proveedor</th>`;
							table += `<th>Moneda</th>`;
							table += `<th>Precio Total</th>`;
						table += `</tr>`;
						table += `</thead>`;
						table += `<tbody  style='font-size: 0.80rem;'>`;
						$.each(rsp, function(index,value){	
							var usaurio = '';
							if(jQuery.isEmptyObject(value.booking_user_name) == false){
								usaurio += value.booking_user_name;
							}
							if(jQuery.isEmptyObject(value.client_email) == false){
								if(usaurio == ''){
									usaurio += value.client_email;
								}else{
									usaurio += ' - '+value.client_email;
								}
							}
							if(jQuery.isEmptyObject(value.booking_agency_creation_user) == false){
								if(usaurio == ''){
									usaurio += value.booking_agency_creation_user;
								}else{
									usaurio += ' - '+value.booking_agency_creation_user;
								}
							}
							proveedor = '&nbsp;'; 
							if(value.nombre !== null){	
								proveedor = value.nombre;
							}
							var checkbox = "<input type='checkbox' onclick='cargarReserva("+value.id+")' id='"+value.id+"' style='width: 15px;height: 25px;' class='form-control checkbox listNemo_"+value.id+"'>";
							table += `<tr>`;
							table += `<td >`+checkbox+`</td>`;
							table += `<td >`+value.code_supplier+`</td>`;
							table += `<td >`+value.booking_reference+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkin)+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkout)+`</td>`;
							table += `<td>`+value.client_name+`</td>`;
							table += `<td>`+usaurio+`</td>`;
							table += `<td>`+proveedor+`</td>`;
							table += `<td>`+value.moneda+`</td>`;
							table += `<td>`+value.price_neto+`</td>`;
							table += `</tr>`;
						})
						table += `</tbody>`;
						table += `</table>`;
						$('#idReservaNemo').html(table);
						$('#tableNemo').dataTable();

					}
				})
		}

		function datosReservasTrasladoNemo(){
			$.ajax({
					type: "GET",
					url: "{{route('reservasNemo')}}",
					dataType: 'json',
					data: {
							dataTipo: 3,
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
						//$('#reservaNemo').css('display', 'block');
						table = `<table id="tableNemo" style='width: 100%'>`;
						table += `<thead>`;
						table += `<tr>`;
						    table += `<th></th>`;
							table += `<th>Codigo</th>`;
							table += `<th>Codigo Nemo</th>`;
							table += `<th>Check In</th>`;
							table += `<th>Check Out</th>`;
							table += `<th>Cliente</th>`;
							table += `<th>Usuario</th>`;
							table += `<th>Proveedor</th>`;
							table += `<th>Moneda</th>`;
							table += `<th>Precio Total</th>`;
						table += `</tr>`;
						table += `</thead>`;
						table += `<tbody  style='font-size: 0.80rem;'>`;
						$.each(rsp, function(index,value){	
							var usaurio = '';
							if(jQuery.isEmptyObject(value.booking_user_name) == false){
								usaurio += value.booking_user_name;
							}
							if(jQuery.isEmptyObject(value.client_email) == false){
								usaurio += ' - '+value.client_email;
							}
							if(jQuery.isEmptyObject(value.booking_agency_creation_user) == false){
								usaurio += ' - '+value.booking_agency_creation_user;
							}
						 	var checkbox = "<input onclick='cargarReserva("+value.id+")' type='checkbox' id='"+value.id+"' style='width: 15px;height: 25px;' class='form-control checkbox listNemo_"+value.id+"'>";
							table += `<tr>`;
							table += `<td >`+checkbox+`</td>`;
							table += `<td >`+value.code_supplier+`</td>`;
							table += `<td >`+value.booking_reference+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkin)+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkout)+`</td>`;
							table += `<td>`+value.client_name+`</td>`;
							table += `<td>`+usaurio+`</td>`;
							table += `<td>`+value.nombre+`</td>`;
							table += `<td>`+value.moneda+`</td>`;
							table += `<td>`+value.precio_total+`</td>`;
							table += `</tr>`; 
						})
						table += `</tbody>`;
						table += `</table>`;
						$('#idReservaNemo').html(table);
						$('#tableNemo').dataTable();

					}
				})
		}

		function datosReservasActividadNemo(){
			$.ajax({
					type: "GET",
					url: "{{route('reservasNemo')}}",
					dataType: 'json',
					data: {
							dataTipo: 4,
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
						//$('#reservaNemo').css('display', 'block');
						table = `<table id="tableNemo" style='width: 100%'>`;
						table += `<thead>`;
						table += `<tr>`;
						    table += `<th></th>`;
							table += `<th>Codigo</th>`;
							table += `<th>Codigo Nemo</th>`;
							table += `<th>Check In</th>`;
							table += `<th>Check Out</th>`;
							table += `<th>Cliente</th>`;
							table += `<th>Usuario</th>`;
							table += `<th>Proveedor</th>`;
							table += `<th>Moneda</th>`;
							table += `<th>Precio Total</th>`;
						table += `</tr>`;
						table += `</thead>`;
						table += `<tbody  style='font-size: 0.80rem;'>`;
						$.each(rsp, function(index,value){	
							var usaurio = '';
							if(jQuery.isEmptyObject(value.booking_user_name) == false){
								usaurio += value.booking_user_name;
							}
							if(jQuery.isEmptyObject(value.client_email) == false){
								usaurio += ' - '+value.client_email;
							}
							if(jQuery.isEmptyObject(value.booking_agency_creation_user) == false){
								usaurio += ' - '+value.booking_agency_creation_user;
							}

						 	var checkbox = "<input type='checkbox' onclick='cargarReserva("+value.id+")' id='"+value.id+"' style='width: 15px;height: 25px;' class='form-control checkbox listNemo_"+value.id+"'>";
							table += `<tr>`;
							table += `<td >`+checkbox+`</td>`;
							table += `<td >`+value.code_supplier+`</td>`;
							table += `<td >`+value.booking_reference+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkin)+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkout)+`</td>`;
							table += `<td>`+value.client_name+`</td>`;
							table += `<td>`+usaurio+`</td>`;
							table += `<td>`+value.nombre+`</td>`;
							table += `<td>`+value.moneda+`</td>`;
							table += `<td>`+value.precio_total+`</td>`;
							table += `</tr>`; 
						})
						table += `</tbody>`;
						table += `</table>`;
						$('#idReservaNemo').html(table);
						$('#tableNemo').dataTable();

					}
				})
		}

		function datosReservasCircuitoNemo(){
			$.ajax({
					type: "GET",
					url: "{{route('reservasNemo')}}",
					dataType: 'json',
					data: {
							dataTipo: 2,
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
						//$('#reservaNemo').css('display', 'block');
						table = `<table id="tableNemo" style='width: 100%'>`;
						table += `<thead>`;
						table += `<tr>`;
						    table += `<th></th>`;
							table += `<th>Codigo</th>`;
							table += `<th>Codigo Nemo</th>`;
							table += `<th>Check In</th>`;
							table += `<th>Check Out</th>`;
							table += `<th>Cliente</th>`;
							table += `<th>Usuario</th>`;
							table += `<th>Proveedor</th>`;
							table += `<th>Moneda</th>`;
							table += `<th>Precio Total</th>`;
						table += `</tr>`;
						table += `</thead>`;
						table += `<tbody  style='font-size: 0.80rem;'>`;
						$.each(rsp, function(index,value){	
							var usaurio = '';
							if(jQuery.isEmptyObject(value.booking_user_name) == false){
								usaurio += value.booking_user_name;
							}
							if(jQuery.isEmptyObject(value.client_email) == false){
								usaurio += ' - '+value.client_email;
							}
							if(jQuery.isEmptyObject(value.booking_agency_creation_user) == false){
								usaurio += ' - '+value.booking_agency_creation_user;
							}

						 	var checkbox = "<input type='checkbox' onclick='cargarReserva("+value.id+")' id='"+value.id+"' style='width: 15px;height: 25px;' class='form-control checkbox listNemo_"+value.id+"'>";
							table += `<tr>`;
							table += `<td >`+checkbox+`</td>`;
							table += `<td >`+value.code_supplier+`</td>`;
							table += `<td >`+value.booking_reference+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkin)+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkout)+`</td>`;
							table += `<td>`+value.client_name+`</td>`;
							table += `<td>`+usaurio+`</td>`;
							table += `<td>`+value.nombre+`</td>`;
							table += `<td>`+value.moneda+`</td>`;
							table += `<td>`+value.precio_total+`</td>`;
							table += `</tr>`; 
						})
						table += `</tbody>`;
						table += `</table>`;
						$('#idReservaNemo').html(table);
						$('#tableNemo').dataTable();

					}
				})
		}

		function cargarReserva(id){
			$.ajax({
					type: "GET",
					url: "{{route('reservasNemoDatos')}}",
					dataType: 'json',
					data: {
							dataId: id,
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                            position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){

						$(".checkbox").prop("checked", false); 
						$("#"+id).prop("checked", true);
						if(jQuery.isEmptyObject(rsp.code_supplier) == false){
							$("#confirmacion_0").val(rsp.code_supplier);
						}else{
							$("#confirmacion_0").val(rsp.codigo);
						}
						$("#confirmacion_0").prop("disabled", true);
						$("#periodo_0").val(rsp.checkIn+" - "+rsp.checkOut);
						$("#periodo_0").prop("disabled", true);
						prestador();
						select_proveedor();
						$('#proveedor_0').val(rsp.proveedor);
						$('#prestador_0').val(rsp.prestador);
						$('#proveedor_id_0').val(rsp.proveedor);
						$('#prestador_id_0').val(rsp.prestador);
						$('#select2-proveedor_0-container').html(rsp.proveedor_name);
						$('#select2-prestador_0-container').html(rsp.prestador_name);

						/*if(rsp.proveedor == 573){
							$("#prestador_0").prop("disabled", false);
							$("#proveedor_0").prop("disabled", false);
						}else{
							$("#prestador_0").prop("disabled", true);
							$("#proveedor_0").prop("disabled", true);
							$('#proveedor_0').select2("enable",false);
							$('#prestador_0').select2("enable",false);
						}*/	
						$("#costo_0").val(rsp.costo);
						$("#fee_0").val(rsp.fee);
						$("#monto_gasto_0").val(rsp.monto_gasto);
						$("#costo_0").prop("disabled", true);
						$("#tasas_0").val(0);
						$("#tasas_0").prop("disabled", true);
						$("#base_comisionable_0").val(0);
						$("#base_comisionable_0").prop("disabled", true);
						$("#moneda_compra_0").val(rsp.moneda).trigger('change.select2');
						$("#moneda_compra_0").prop("disabled", true);
						$("#venta_0").val(rsp.venta);
						$("#vencimientos").val(rsp.pago_proveedor);
						$("#fecha_gasto_0").val(rsp.vencimiento);
						$("#reserva_nemo_id").val(id);
						$("#fecha_gasto_0").prop("disabled", true);
						$("#monto_gasto_0").prop("disabled", true);
						$("#gravado_costo_0").prop("disabled", true);
						$("#comision_agencia_0").prop("disabled", true);
						$('#reservaNemo .collapse').collapse('hide');
						$("#costo_0").trigger('change');
						$("#vencimientos").prop("disabled", true);
					}
			})

		}	

		function cargarReservaCangoro(id_reserva, id_grupo_reserva){
			/**
			 * 1 Alojamiento
			 * 2 Tour
			 * 3 Transfer
			*/
			$.ajax({
					type: "GET",
					url: "{{route('reservasCangoroDatos')}}",
					dataType: 'json',
					data: {id_reserva, id_grupo_reserva},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                            position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor para recuperar los datos de cangoroo',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
						let id = id_grupo_reserva+"_"+id_reserva;

						$(".checkbox").prop("checked", false); 
						$("#"+id).prop("checked", true);
						if(jQuery.isEmptyObject(rsp.code_supplier) == false){
							$("#confirmacion_0").val(rsp.code_supplier);
						}else{
							$("#confirmacion_0").val(rsp.codigo);
						}

						$("#periodo_0").val(rsp.checkIn+" - "+rsp.checkOut);


						prestador();
						select_proveedor();

						$('#proveedor_0').val(rsp.proveedor);
						$('#proveedor_id_0').val(rsp.proveedor);
						$('#select2-proveedor_0-container').html(rsp.proveedor_name);

						$('#prestador_0').val(rsp.prestador);
						$('#prestador_id_0').val(rsp.prestador);
						$('#select2-prestador_0-container').html(rsp.prestador_name);

						$("#costo_0").val(rsp.costo);
						$("#fee_0").val(rsp.fee);
						$("#monto_gasto_0").val(rsp.monto_gasto);
						$("#tasas_0").val(0);
						$("#base_comisionable_0").val(0);
						$("#moneda_compra_0").val(rsp.id_moneda_costo).trigger('change.select2');
				

						$("#venta_0").val(rsp.venta);
						$("#vencimientos").val(rsp.pago_proveedor);
						$("#fecha_gasto_0").val(rsp.vencimiento);
						$("#reserva_cangoroo_id").val(id_reserva);
						$("#grupo_reserva_cangoroo_id").val(id_grupo_reserva);
						$("#tipo_reserva_cangoroo").val(rsp.tipo_reserva);
						$('#reservaCangoroo .collapse').collapse('hide');

						disabledAllDetalle();//deshabilitar campos
						calcularMarkup();
		
					}
			})

		}

		function formatearFecha(texto)
 		{
	        if(texto != '' && texto != null)
	        {
	          	return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	        } 
	        else 
	        {
	         	return '';    
         	}
        }
/*
		$('#requestModalDetalle').on('focus', '.select2', function (e) {
			if (e.originalEvent) {
				var s2element = $(this).siblings('select');
				s2element.select2('open');

				// Set focus back to select2 element on closing.
				s2element.on('select2:closing', function (e) {
					s2element.select2('focus');
				});
			}
		}); 
		$('#requestModalDetalle').on('select2:open', () => {
			document.querySelector('.select2-search__field').focus();
		});*/

		/*function dismis(){
				$('#proveedor_0').select2("close"); 
				$('#prestador_0').select2("close");
				$('#moneda_compra_0').select2("close");
		}*/

		function controlNumerico(id){
			if(id > 0 && id <= 10){
			}else{	
				$("#incentivo").select2().val(1).trigger("change");
				$.toast({
                            heading: 'Error',
                            position: 'top-right',
                            text: 'El valor del incentivo debe estar entre 1 y 10',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
			}
		}
		cambioProveedor();

		function cambioProveedor(){
			$("#proveedor_0").change(function(){
				tipo = $("#proveedor_0 option:selected").attr('tipo');
				$("#tipo_0s").val(tipo); 
				if(tipo == 2){
					$("#comision_operador_0").val(0);
					$("#comision_operador_0").prop("disabled", true);
				}else{
					$("#comision_operador_0").val(0);
					$("#comision_operador_0").prop("disabled", false);
				}
				$("#proveedor_id_0").val($(this).val());
			})

		}

		function ajusteTipo(){
			tipo = $("#proveedor_0 option:selected").attr('tipo');
			if(tipo == 0 ||tipo == ''){
				tipo = 2;
			}
			$("#tipo_0s").val(tipo); 
			if(tipo == 2){
					$("#comision_operador_0").val(0);
					$("#comision_operador_0").prop("disabled", true);
				}else{
					$("#comision_operador_0").val(0);
					$("#comision_operador_0").prop("disabled", false);
				}

		}	


		function guardarDetalle(){
				//--------------------------------------------------------------------//
			var idEmpresaBase = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}"
			if(idEmpresaBase == 'D'){
				if($("#fecha_gasto_0").val() != null && $("#monto_gasto_0").val() != 0){
					$('#fecha_gasto_0').css('border-color', '#d2d6de');
					$('#monto_gasto_0').css('border-color', '#d2d6de');
					setTimeout("guardarFila()", 500);
				}else{	
					$.unblockUI();
					$('#fecha_gasto_0').css('border-color', 'red');
					$('#monto_gasto_0').css('border-color', 'red');
					$.toast({
								heading: 'Error',
								text: 'Se debe ingresar el monto y la fecha del gasto.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});									
				}	
			}else{
				if($("#costo_0").val()){
					$('#costo_0').css('border-color', '#d2d6de');
					if($("#gravado_costo_0").val()){
						$('#gravado_costo_0').css('border-color', '#d2d6de');
						setTimeout("guardarFila()", 500);
					}else{	
						$.unblockUI();
						$('#gravado_costo_0').css('border-color', 'red');
						$.toast({
								heading: 'Error',
								text: 'Se deben ingresar los costo gravado.',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});									
					}
				}else{	
					$.unblockUI();
					$('#costo_0').css('border-color', 'red');
					$.toast({
							heading: 'Error',
							text: 'Se deben ingresar el costo exento.',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
							});									
				}	
			}
			//--------------------------------------------------------------------//
		}											

	function updateDtp(){
			$.ajax({
						type: "GET",
						url: "{{route('updateDetalle')}}",
						dataType: 'json',
						data: {
								dataItem: $('#item').val(),
								dataProforma_id_: $('#proforma_id').val(),
								dataCurrency_venta: $('#moneda').val(),
								dataCantidad: $('#cantidad_0').val(),
								dataProducto:$('#productoId_0').val(),
								dataPeriodo: $('#periodo_0').val(),
								dataCantidadPersonas: $('#cantidad_personas_0').val(),
								dataConfirmacion: $('#confirmacion_0').val(),
								dataProveedor: $('#proveedor_id_0').val(),
								dataPrestador: $('#prestador_id_0').val(),
								dataDescripcion: $('#descrip_factura_0').val(),
								dataMonedaCompra: $('#moneda_compra_0').val(),
								dataCosto: $('#costo_0').val(),
								dataComisionAgencia: $('#comision_agencia_0').val(),
								dataVenta: $('#venta_0').val(),
								dataMarkup: $('#markup_0').val(),
								dataTarjetaId: $('#tarjeta_id_0').val(),
								dataCodAutorizacion: $('#cod_autorizacion_0').val(),
								dataHoraIn: $('#horaIn_0').val(),
								dataHoraOut: $('#horaOut_0').val(),
								dataVueloIn: $('#vueloIn_0').val(),
								dataVueloOut: $('#vueloOut_0').val(),
								dataTipoTraslado: $('#idTipoTraslado_0').val(),
								dataVencimiento: $('#vencimientos_0').val(),
								dataDetalleProforma: $('#detalle_proforma_id').val(),
								dataTasas: $('#tasas_0').val(),
								dataTarifaAsistencia: $('#tarifa_asistencia_0').val(),
								dataCantidadDias: $('#cantidad_dias_0').val(),
								dataExento: $('#exentos_0').val(),
								dataGravada: $('#gravada_0').val(),
								dataFechaGasto: $('#fecha_gasto_0').val(),
								dataGasto: $('#monto_gasto_0').val(),
								dataComison: $('#comision_operador_0').val(),
								dataCostoGravada: $('#gravado_costo_0').val(),
								dataBaseComisionable: $('#base_comisionable_0').val()
							},
							success: function(rsp){
										if(rsp.status == 'OK'){
											$('#producto_'+rsp.item).select2().enable(false);
											$('#proveedor_'+rsp.item).select2().enable(false);						
											$('#prestador_'+rsp.item).select2().enable(false);
											$('#moneda_compra_'+rsp.item).select2().enable(false);
											$('table#lista_productos tr#'+rsp.item).find('input,select,select2,textarea').prop("disabled", true);
											$('#botonesEdit_'+rsp.item).css('display', 'none');
											$('#botones_'+rsp.item).css('display', 'block');
											productoDesc = rsp.detalle[0].producto.denominacion;
											actualizarCabecera();
											$.toast({
													heading: 'Exito',
													text: 'Se ha modificado el Detalle.',
													position: 'top-right',
																	showHideTransition: 'slide',
																	icon: 'success'
																});  
																periodo = $('#periodo_0').val(); 
																checkInOut = periodo.split('-');
																if(rsp.detalle[0].comision_operador == 0){
																	if(jQuery.isEmptyObject(rsp.detalle[0].porcentaje_comision_agencia) == false){
																		comision  = rsp.detalle[0].porcentaje_comision_agencia;
																	}else{
																		comision  = 0;
																	}
																}else{ 
																	comision = rsp.detalle[0].comision_operador;		
																}
																total_costo = parseFloat(rsp.detalle[0].costo_proveedor)+parseFloat(rsp.detalle[0].costo_gravado);																
																fila_contenido = '';
																fila_contenido += '<tr id="'+rsp.item+'" data-toggle="tooltip" data-placement="left" data-html="true" title="<b>Check In :</b>'+checkInOut[0]+'<br><b>Check Out :</b> '+checkInOut[1]+'<br><b>Prestador :</b> '+rsp.detalle[0].prestador.nombre+'">';
																fila_contenido += '<th style="padding: 0.25rem 0.5rem;">'+$('#cantidad_0').val()+'</th>';
																fila_contenido += '<td style="padding: 0.25rem 1rem;">'+productoDesc+'</td>';
																//fila_contenido += '<td style="padding: 0.25rem 1rem;">'+$('#periodo_0').val()+'</td>';
																fila_contenido += '<th style="padding: 0.25rem 1rem;text-align: center;">'+$('#confirmacion_0').val()+'</th>';
																fila_contenido += '<th style="padding: 0.25rem 1rem;">'+rsp.detalle[0].proveedor.nombre+'</th>';
																//fila_contenido += '<th style="padding: 0.25rem 1rem;">'+$('#prestador_0 :selected').text()+'</th>';
																fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+$('#moneda_compra_0 :selected').text()+'</td>';
																fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+formatter.format(parseFloat(total_costo))+'</td>';
																fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+$('#tasas_0').val()+'</td>';
																fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+comision+'</td>';
																fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+$('#venta_0').val()+'</td>';
																fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center;">'+rsp.detalle[0].porcentaje_renta_item+' / '+rsp.detalle[0].renta_minima_producto+'</th>';
																if(rsp.detalle[0].cumple_rentabilidad_minima == 1){
																	color = 'green';
																}else{
																	color = 'red';
																}
																fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center;"><i class="fa fa-fw fa-circle" title="Inactivo Gestur" style="color:'+color+';"></i></th>';		
																fila_contenido += '<td style="padding: 0.25rem 0.5rem;">';
																if(rsp.detalle[0].origen != 'A'){
																	if(!rsp.detalle[0].pagado_con_tc){
																		fila_contenido += '<a id="'+rsp.id+'" class="btn-update-request" onclick="updateDetalle('+rsp.id+','+rsp.item+')" title="Editar Detalle" data="'+rsp.item+'" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>';
																		fila_contenido += '<a data="'+rsp.id+'"  title="Eliminar Detalle" indice="'+rsp.id+'" class="btn-cancelar-request" style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>';
																		if(rsp.detalle[0].prestador.migrar && rsp.pagar_tc_detalle_proforma && rsp.detalle[0].producto.pago_tc_detalle_proforma){
																			fila_contenido += '<a data-proforma_detalle_id="'+rsp.id+'" class="btn-update-request" onclick="pagarTcDetalle('+rsp.id+')" title="Pagar con TC" style="margin-left: 5px;"><i class="fa fa-credit-card" style="font-size: 18px;color:red;"></i></a>';
																		}
																	} else {
																		fila_contenido += '<a  class="btn-update-request" disabled title="Bloqueado por pago de TC" style="margin-left: 5px;color:green;"><i class="fa fa-credit-card" style="font-size: 18px;"></i></a>';
																	}
																	
																}else{
																	fila_contenido += '<a id="'+rsp.id+'" class="btn-update-request" onclick="updateDetalle('+rsp.id+','+rsp.item+')" title="Editar Detalle" data="'+rsp.item+'" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>';
																}

																fila_contenido += '<a id="mostrar_'+rsp.item+'" class="glyphicon glyphicon-save-file" title="Desplegar Detalle" numero ="'+rsp.id+'" data="'+rsp.item+'" style="margin-left: 5px;"><i class="ft-download" style="font-size: 18px;"></i></a>';

																fila_contenido += '<a style ="display:none" id="ocultar_'+rsp.item+'" numero ="'+rsp.id+'" class="glyphicon glyphicon-open-file" data="'+rsp.item+'"  title="Ocultar Detalle" style="margin-left: 5px;"><i class="ft-upload" style="font-size: 18px;"></i></a>';

																fila_contenido += '</td>';
																fila_contenido += '</tr>';
																$("#lista_productos").find("tbody").find("tr").each(function(){ 
																	if($(this).attr("id") == rsp.item) {
																		$(this).replaceWith(fila_contenido);
																		inicioNeW();
																} 	
																})    	
																$("#requestModalDetalle").modal('hide');
																//location.reload();
															}else{
																$.toast({
																	heading: 'Error',
																	text: 'Ocurrio un error en la comunicación con el servidor.',
																	showHideTransition: 'fade',
																	position: 'top-right',
																	icon: 'error'
																});

															}	
											}
									})
		}

		function updateAgencia(){
					$.ajax({
								type: "GET",
								url: "{{route('updateDetalle')}}",
								dataType: 'json',
								data: {
										dataItem: $('#item').val(),
										dataProforma_id_: $('#proforma_id').val(),
										dataCurrency_venta: $('#moneda').val(),
										dataCantidad: $('#cantidad_0').val(),
										dataProducto:$('#productoId_0').val(),
										dataPeriodo: $('#periodo_0').val(),
										dataCantidadPersonas: $('#cantidad_personas_0').val(),
										dataConfirmacion: $('#confirmacion_0').val(),
										dataProveedor: $('#proveedor_id_0').val(),
										dataPrestador: $('#prestador_id_0').val(),
										dataDescripcion: $('#descrip_factura_0').val(),
										dataMonedaCompra: $('#moneda_compra_0').val(),
										dataCosto: $('#costo_0').val(),
										dataComisionAgencia: $('#comision_agencia_0').val(),
										dataVenta: $('#venta_0').val(),
										dataMarkup: $('#markup_0').val(),
										dataTarjetaId: $('#tarjeta_id_0').val(),
										dataCodAutorizacion: $('#cod_autorizacion_0').val(),
										dataHoraIn: $('#horaIn_0').val(),
										dataHoraOut: $('#horaOut_0').val(),
										dataVueloIn: $('#vueloIn_0').val(),
										dataVueloOut: $('#vueloOut_0').val(),
										dataTipoTraslado: $('#idTipoTraslado_0').val(),
										dataVencimiento: $('#vencimientos_0').val(),
										dataDetalleProforma: $('#detalle_proforma_id').val(),
										dataTasas: $('#tasas_0').val(),
										dataTarifaAsistencia: $('#tarifa_asistencia_0').val(),
										dataCantidadDias: $('#cantidad_dias_0').val(),
										dataExento: $('#exentos_0').val(),
										dataGravada: $('#gravada_0').val(),
										dataFechaGasto: $('#fecha_gasto_0').val(),
										dataGasto: $('#monto_gasto_0').val(),
										dataComison: $('#comision_operador_0').val(),
										dataCostoGravada: $('#gravado_costo_0').val()

									},
									success: function(rsp){
												if(rsp.status == 'OK'){
													$('#producto_'+rsp.item).select2().enable(false);
													$('#proveedor_'+rsp.item).select2().enable(false);						
													$('#prestador_'+rsp.item).select2().enable(false);
													$('#moneda_compra_'+rsp.item).select2().enable(false);
													$('table#lista_productos tr#'+rsp.item).find('input,select,select2,textarea').prop("disabled", true);
													$('#botonesEdit_'+rsp.item).css('display', 'none');
													$('#botones_'+rsp.item).css('display', 'block');
													productoDesc = rsp.detalle[0].producto.denominacion;
													actualizarCabecera();
													$.toast({
															heading: 'Exito',
															text: 'Se ha modificado el Detalle.',
															position: 'top-right',
																			showHideTransition: 'slide',
																			icon: 'success'
																		});  
																		periodo = $('#periodo_0').val(); 
																		checkInOut = periodo.split('-');
																		if(rsp.detalle[0].comision_operador == 0){
																			if(jQuery.isEmptyObject(rsp.detalle[0].porcentaje_comision_agencia) == false){
																				comision  = rsp.detalle[0].porcentaje_comision_agencia;
																			}else{
																				comision  = 0;
																			}
																		}else{ 
																			comision = rsp.detalle[0].comision_operador;		
																		}

																		total_costo = parseFloat(rsp.detalle[0].costo_proveedor)+parseFloat(rsp.detalle[0].costo_gravado);																

																		fila_contenido = '';
																		fila_contenido += '<tr id="'+rsp.item+'" data-toggle="tooltip" data-placement="left" data-html="true" title="<b>Check In :</b>'+checkInOut[0]+'<br><b>Check Out :</b> '+checkInOut[1]+'<br><b>Prestador :</b> '+rsp.detalle[0].prestador.nombre+'">';
																		fila_contenido += '<th style="padding: 0.25rem 0.5rem;">'+$('#cantidad_0').val()+'</th>';
																		fila_contenido += '<td style="padding: 0.25rem 1rem;">'+productoDesc+'</td>';
																		//fila_contenido += '<td style="padding: 0.25rem 1rem;">'+$('#periodo_0').val()+'</td>';
																		fila_contenido += '<th style="padding: 0.25rem 1rem;text-align: center;">'+$('#confirmacion_0').val()+'</th>';
																		fila_contenido += '<th style="padding: 0.25rem 1rem;">'+rsp.detalle[0].proveedor.nombre+'</th>';
																		//fila_contenido += '<th style="padding: 0.25rem 1rem;">'+$('#prestador_0 :selected').text()+'</th>';
																		fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+$('#moneda_compra_0 :selected').text()+'</td>';
																		fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+formatter.format(parseFloat(total_costo))+'</td>';
																		fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+$('#tasas_0').val()+'</td>';
																		fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+comision+'</td>';
																		fila_contenido += '<td style="padding: 0.25rem 1rem; text-align: center;">'+$('#venta_0').val()+'</td>';
																		fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center;">'+rsp.detalle[0].porcentaje_renta_item+' / '+rsp.detalle[0].renta_minima_producto+'</th>';
																		if(rsp.detalle[0].cumple_rentabilidad_minima == 1){
																			color = 'green';
																		}else{
																			color = 'red';
																		}
																		fila_contenido += '<th style="padding: 0.25rem 1rem; text-align: center;"><i class="fa fa-fw fa-circle" title="Inactivo Gestur" style="color:'+color+';"></i></th>';		
																		fila_contenido += '<td style="padding: 0.25rem 0.5rem;">';
																		if(rsp.detalle[0].origen != 'A'){

																			if(!rsp.detalle[0].pagado_con_tc){
																				fila_contenido += '<a id="'+rsp.id+'" class="btn-update-request" onclick="updateDetalle('+rsp.id+','+rsp.item+')" title="Editar Detalle" data="'+rsp.item+'" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>';
																				fila_contenido += '<a data="'+rsp.id+'"  title="Eliminar Detalle" indice="'+rsp.id+'" class="btn-cancelar-request" style="margin-left: 5px;"><i class="ft-x-circle" style="font-size: 18px;"></i></a>';

																				if(rsp.detalle[0].prestador.migrar && rsp.pagar_tc_detalle_proforma && rsp.detalle[0].producto.pago_tc_detalle_proforma){
																					fila_contenido += '<a data-proforma_detalle_id="'+rsp.id+'" class="btn-update-request" onclick="pagarTcDetalle('+rsp.id+')" title="Pagar con TC" style="margin-left: 5px;"><i class="fa fa-credit-card" style="font-size: 18px;color:red;"></i></a>';
																				}
																			}  else {
																				fila_contenido += '<a  class="btn-update-request" disabled title="Bloqueado por pago de TC" style="margin-left: 5px;color:green;"><i class="fa fa-credit-card" style="font-size: 18px;"></i></a>';
																			}

																		}else{
																			fila_contenido += '<a id="'+rsp.id+'" class="btn-update-request" onclick="updateDetalle('+rsp.id+','+rsp.item+')" title="Editar Detalle" data="'+rsp.item+'" style="margin-left: 5px;"><i class="ft-clipboard" style="font-size: 18px;"></i></a>';
																		}

																		fila_contenido += '<a id="mostrar_'+rsp.item+'" class="glyphicon glyphicon-save-file" title="Desplegar Detalle" numero ="'+rsp.id+'" data="'+rsp.item+'" style="margin-left: 5px;"><i class="ft-download" style="font-size: 18px;"></i></a>';

																		fila_contenido += '<a style ="display:none" id="ocultar_'+rsp.item+'" numero ="'+rsp.id+'" class="glyphicon glyphicon-open-file" data="'+rsp.item+'"  title="Ocultar Detalle" style="margin-left: 5px;"><i class="ft-upload" style="font-size: 18px;"></i></a>';

																		fila_contenido += '</td>';
																		fila_contenido += '</tr>';
																		$("#lista_productos").find("tbody").find("tr").each(function(){ 
																			if($(this).attr("id") == rsp.item) {
																				$(this).replaceWith(fila_contenido);
																				inicioNeW();
																		} 	
																		})    	
																		$("#requestModalDetalle").modal('hide');
																	}else{
																		$.toast({
																			heading: 'Error',
																			text: 'Ocurrio un error en la comunicación con el servidor.',
																			showHideTransition: 'fade',
																			position: 'top-right',
																			icon: 'error'
																		});

																	}	
													}
											})
		}
		$("#producto_pedido").change(function(){
			$.ajax({
					type: "GET",
					url: "{{route('getProducto')}}",
					dataType: 'json',
					data: {
							dataProducto: $("#producto_pedido").val(),
						  },
					success: function(rsp){
								$('#concepto_parcial').val(rsp);
					}
			})
		})

		$('#cant_0').change(function() {
			$('#cantidad_0').val($(this).val());
		})

		function agregarFilaDetalle(){
			cantidad = $('#cantD_0').val();
			descripcion = $('#desc_0').val();
			precio_unitario = clean_num($('#pru_0').val());
			exenta = clean_num($('#exe_0').val());
			iva5 = clean_num($('#iv5_0').val());
			iva10 = clean_num($('#iv10_0').val());
			total = $('#totalDetalle_0').val()
			sumando  = exenta + iva5 + iva10;
			if(parseFloat(clean_num($('#total_factura').val()))!= 0){
				if(sumando == total){
					generarFilaDetalle(cantidad,descripcion,precio_unitario,exenta,iva5,iva10,total,0);
				}else{
					$.toast({
							heading: 'Error',
							text: 'El monto total y la suma de exento y gravada.',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
						});

				}	
			}else{
				$.toast({
							heading: 'Error',
							text: 'Ingrese un detalle de proforma.',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
						});

			}
		}

	

		function generarFilaDetalle(cantidad,descripcion,precio_unitario,exenta,iva5,iva10,total){
			var rowCount = $("#listadoFactura tbody tr").length;

			var tbody = $('#listadoFactura tbody');

			var id_proforma = '{{$proformas[0]->id}}';

			var htmlTags =  '<tr id="detalleFila'+rowCount+'">'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric" id="cantD_'+rowCount+'" name="detalle['+rowCount+'][cantidad]" value="'+cantidad+'">'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm" id="desc_'+rowCount+'" name="detalle['+rowCount+'][descripcion]" value="'+descripcion+'">'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric" id="pru_'+rowCount+'" name="detalle['+rowCount+'][precio_unitario]" value="'+precio_unitario+'">'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric detalle_exenta" id="exe_'+rowCount+'" name="detalle['+rowCount+'][exenta]" value="'+exenta+'">'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric detalle_gravada5" id="iv5_'+rowCount+'" name="detalle['+rowCount+'][iva5]" value="'+iva5+'">'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric detalle_gravada10" id="iv10_'+rowCount+'" name="detalle['+rowCount+'][iva10]" value="'+iva10+'">'+
								'</th>'+
								'<th>'+
									'<input type="hidden" id="id_proforma_'+rowCount+'" name="detalle['+rowCount+'][id_proforma]" value="'+id_proforma+'">'+
									'<input type="hidden" class="totalesDetallesFactura" id="totalDetalle_'+rowCount+'" name="detalle['+rowCount+'][total]" value="'+total+'">'+
									'<button type="button" onclick="deleteFilaDetalle('+rowCount+')" class="btn btn-danger" style="width: 50px; background-color: #e2076a;"><i class="fa fa-times" style="font-size: 18px;"></i></button>'+
								'</th>'+
							'</tr>';
			tbody.append(htmlTags);
			$('#cantD_0').val(0);
			$('#desc_0').val('');
			$('#pru_0').val(0);
			$('#exe_0').val(0);
			$('#iv5_0').val(0);
			$('#iv10_0').val(0);
			$('#totalDetalle_0').val(0);
			$('#btnGuardarDetalleFactura').prop("disabled", false);
			$('.numeric').inputmask("numeric", {
				radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				// prefix: '$', //No Space, this will truncate the first character
				rightAlign: false,
				oncleared: function () { 
					$(this).val(0);
				}
			});

		}	

		$(".sumando").change(function(){
			cantidad  = $('#cantD_0').val();
			precio_unitario = clean_num($('#pru_0').val()); 
			total = cantidad * precio_unitario;
			$('#totalDetalle_0').val(total);
		})

		$('#btnGuardarDetalleFactura').on('click',function(){
			var suma = 0;
			msj = '';
			$('.totalesDetallesFactura').each(function(){
				suma += parseFloat($(this).val());
			});
			total_factura = clean_num($('#total_factura').val());
			falg1 = parseFloat(total_factura) - parseFloat(suma.toFixed(2));
			if(falg1 != 0){
				msj += 'El monto total de la proforma y la suma total de los detalles no coinciden.';
			}
			var suma_exenta = 0;
			$('.detalle_exenta').each(function(){
				suma_exenta += parseFloat(clean_num($(this).val()));
			});
			total_exenta = clean_num($('#total_exentas').val());
			falg2 = parseFloat(total_exenta) - parseFloat(suma_exenta);
			if(falg2 != 0){
				msj += 'El monto total de exenta de la proforma no conincide con el de los detalles';
			}
			var suma_gravada5= 0;
			$('.detalle_gravada5').each(function(){
				suma_gravada5 += parseFloat(clean_num($(this).val()));
			});

			var suma_gravada10= 0;
			$('.detalle_gravada10').each(function(){
				suma_gravada10 += parseFloat(clean_num($(this).val()));
			});
			total_gravada10 = clean_num($('#total_gravadas').val());

			falg3 = parseFloat(total_gravada10) - (parseFloat(suma_gravada10) + parseFloat(suma_gravada5));

			if(falg3 != 0){
				msj += 'El monto total de gravadas de la proforma no conincide con el de los detalles';
			}

			if(falg1 == 0 && falg2 == 0 && falg3 == 0){
				guardarFilaDetalles();
			}else{
				if(falg1 != 0 && falg2 != 0 && falg3 != 0){
					var rowCount = $("#listadoFactura tbody tr").length;
					if(rowCount == 1){
						guardarFilaDetalles();
					}else{
						msj = 'Los montos no coinciden con el monto de la total, exenta y gravada de la proforma'
						$('#btnGuardarDetalleFactura').prop('disabled',false);
						$.toast({
								heading: 'Error',
								text: msj,
							//	showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});
					}
				}else{
					$.toast({
								heading: 'Error',
								text: msj,
							//	showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});
				}	

			}		
		})

		function guardarFilaDetalles(){
			$('#btnGuardarDetalleFactura').prop('disabled',true);
			var dataString = $("#formFacturaDetalle").serialize();
			$.ajax({
					type: "GET",
					url: "{{route('getGuardarDetalleFactura')}}",
					dataType: 'json', 
					data: dataString, 
					success: function(rsp){
							if(rsp.status == 'OK'){
								$('#modalDetallesFactura').modal('hide');
								$('#btnGuardarDetalleFactura').prop('disabled',false);
								$.toast({
										heading: 'Exito',
										text: rsp.mensaje,
										position: 'top-right',
										showHideTransition: 'slide',
										icon: 'success'
									});  
							}else{
								$.toast({
									heading: 'Error',
									text: rsp.mensaje,
									showHideTransition: 'fade',
									position: 'top-right',
									icon: 'error'
								});

							}

					}
			})
		}

	 	agregarFilaDetalles();

		function agregarFilaDetalles(){
			$.ajax({
					type: "GET",
					url: "{{route('getDetalleFactura')}}",
					dataType: 'json',
					data: {
							dataProforma: '{{$proformas[0]->id}}',
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                            position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
								$.each(rsp, function(index,value){	
									generarFilaDetalle(value.cantidad,value.descripcion,value.precio_venta,value.exento,value.gravadas_5,value.gravadas_10,value.total,0);
								})	
					}
			})
		}	 

		function ajusteMontos(){
			
		/*	$("#costo_0").change(function(){ 
				comisionable = clean_num($('#base_comisionable_0').val());
				totalCosto = parseFloat(clean_num($('#costo_0').val())) + parseFloat(clean_num($('#gravado_costo_0').val()));
				producto = $('#producto_0').val();
				monedaCompra = $('#moneda_compra_0').val();
				monedaVenta = $('#moneda').val();

				calcularPrecioCosto(comisionable, totalCosto, producto,monedaCompra,monedaVenta);
			})	

			$("#gravado_costo_0").change(function(){
				comisionable = clean_num($('#base_comisionable_0').val());
				totalCosto = parseFloat(clean_num($('#costo_0').val())) + parseFloat(clean_num($('#gravado_costo_0').val()));
				producto = $('#producto_0').val();
				monedaCompra = $('#moneda_compra_0').val();
				monedaVenta = $('#moneda').val();

				calcularPrecioCosto(comisionable, totalCosto, producto,monedaCompra,monedaVenta);

		   	})

			$("#comision_operador_0").change(function(){
				comisionable = clean_num($('#base_comisionable_0').val());
				totalCosto = parseFloat(clean_num($('#costo_0').val())) + parseFloat(clean_num($('#gravado_costo_0').val()));
				producto = $('#producto_0').val();
				monedaCompra = $('#moneda_compra_0').val();
				monedaVenta = $('#moneda').val();

				calcularPrecioCosto(comisionable, totalCosto, producto,monedaCompra,monedaVenta);
		   	})

			$('#base_comisionable_0').change(function() {
				comisionable = clean_num($('#base_comisionable_0').val());
				totalCosto = parseFloat(clean_num($('#costo_0').val())) + parseFloat(clean_num($('#gravado_costo_0').val()));
				producto = $('#producto_0').val();
				monedaCompra = $('#moneda_compra_0').val();
				monedaVenta = $('#moneda').val();

				calcularPrecioCosto(comisionable, totalCosto, producto,monedaCompra,monedaVenta);
			})*/
					/////////////////////////////////////////////////PARA AJUSTAR///////////////////////////////////////////////////////////			
			$("#costo_0").change(function(){ 
				perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
				if(perfil  != 1){
				//	alert(clean_num($("#costo_0").val())+" - "+clean_num($("#gravado_costo_0").val()));
					if($("#tipo_0s").val() == 2){
						$("#exentos_0").val($(this).val());
						$("#venta_0").val(parseFloat(clean_num($("#costo_0").val())) + parseFloat(clean_num($("#gravado_costo_0").val())));					
					}else{
						$("#exentos_0").val($(this).val());
						$("#venta_0").val(parseFloat(clean_num($("#costo_0").val())) + parseFloat(clean_num($("#gravado_costo_0").val())));
					}
				}
				var markupId = $(this).attr('id');
				var indice = markupId.split('_');
				$.ajax({
					type: "GET",
					url: "{{route('calcularMarkup')}}",
					dataType: 'json',
					data: {
							dataTasas: $('#tasas_0').val(),
							dataMonedaCompra: $('#moneda_compra_0').val(),
							dataCosto: $('#costo_0').val(),
							dataCurrency_venta: $('#moneda').val(),
							dataVenta: $('#venta_0').val()
						},
							success: function(rsp){
										if(rsp > 0){
											$('#markup_'+indice[1]).val(rsp);
										}else{
											$('#markup_'+indice[1]).val(rsp);
										}
				
						}
				})	
				$.ajax({
					type: "GET",
					url: "{{route('ventaCotizado')}}",
					dataType: 'json',
					data: {
							dataTasas: $('#tasas_0').val(),
							dataMonedaCompra: $('#moneda_compra_0').val(),
							dataCosto: $('#costo_0').val(),
							dataCurrency_venta: $('#moneda').val(),
							dataVenta: $('#venta_0').val()
						},
							success: function(rsp){
								if(rsp > 0){
											$('#venta_0').val(rsp);
										}else{
											$('#venta_0').val(rsp);
										}
				
						}
				})	

			})	

			$("#gravado_costo_0").change(function(){
				perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
				if(perfil  != 1){
					$('#gravada_0').val($(this).val());
					costo = parseFloat(clean_num($("#costo_0").val()));
					gravado = parseFloat(clean_num($("#gravado_costo_0").val()));
					comision = parseFloat(clean_num($("#comision_operador_0").val()));
					if($("#tipo_0s").val() == 1){
						suma = costo + gravado;
					}else{
						suma = costo + gravado;
					}
					$("#venta_0").val(suma);

					var markupId = $(this).attr('id');
					var indice = markupId.split('_');
					$.ajax({
						type: "GET",
						url: "{{route('calcularMarkup')}}",
						dataType: 'json',
						data: {
								dataTasas: $('#tasas_0').val(),
								dataMonedaCompra: $('#moneda_compra_0').val(),
								dataCosto: $('#costo_0').val(),
								dataCurrency_venta: $('#moneda').val(),
								dataVenta: $('#venta_0').val()
							},
								success: function(rsp){
											idPerfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}";
											if(idPerfil== 'D'){
												$('#markup_0').val(rsp);
											 }else{
											 	$('#markup_0').val(0);
											 }					
							}
					})	

				}
				$.ajax({
					type: "GET",
					url: "{{route('ventaCotizado')}}",
					dataType: 'json',
					data: {
							dataTasas: $('#tasas_0').val(),
							dataMonedaCompra: $('#moneda_compra_0').val(),
							dataCosto: $('#costo_0').val(),
							dataCurrency_venta: $('#moneda').val(),
							dataVenta: $('#venta_0').val()
						},
							success: function(rsp){
								if(rsp > 0){
											$('#venta_0').val(rsp);
										}else{
											$('#venta_0').val(rsp);
										}
				
						}
				})	
				//$("#comision_agencia_0").focus();
		   	})
		   	
		   	$("#comision_operador_0").change(function(){
				perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
				if(perfil  != 1){
					$('#comision_operador_0').val($(this).val());
					costo = parseFloat(clean_num($("#costo_0").val()));
					gravado = parseFloat(clean_num($("#gravado_costo_0").val()));
					comision = parseFloat(clean_num($("#comision_operador_0").val()));
					if($("#tipo_0s").val() == 1){
						suma = costo + gravado;
					}else{
						suma = costo + gravado;
					}
					$("#venta_0").val(suma);

					var markupId = $(this).attr('id');
					var indice = markupId.split('_');
					$.ajax({
						type: "GET",
						url: "{{route('calcularMarkup')}}",
						dataType: 'json',
						data: {
								dataTasas: $('#tasas_0').val(),
								dataMonedaCompra: $('#moneda_compra_0').val(),
								dataCosto: $('#costo_0').val(),
								dataCurrency_venta: $('#moneda').val(),
								dataVenta: $('#venta_0').val()
							},
								success: function(rsp){
											idPerfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}";
											if(idPerfil== 'D'){
												$('#markup_0').val(rsp);
											 }else{
											 	$('#markup_0').val(0);
											 }					
							}
					})	

				}
				$.ajax({
					type: "GET",
					url: "{{route('ventaCotizado')}}",
					dataType: 'json',
					data: {
							dataTasas: $('#tasas_0').val(),
							dataMonedaCompra: $('#moneda_compra_0').val(),
							dataCosto: $('#costo_0').val(),
							dataCurrency_venta: $('#moneda').val(),
							dataVenta: $('#venta_0').val()
						},
							success: function(rsp){
								if(rsp > 0){
											$('#venta_0').val(rsp);
										}else{
											$('#venta_0').val(rsp);
										}
				
						}
				})	
				//$("#comision_agencia_0").focus();
		   	})

			$("#venta_0").change(function(){ 
				var markupId = $(this).attr('id');
				var indice = markupId.split('_');
				$.ajax({
					type: "GET",
					url: "{{route('calcularMarkup')}}",
					dataType: 'json',
					data: {
							dataTasas: $('#tasas_0').val(),
							dataMonedaCompra: $('#moneda_compra_0').val(),
							dataCosto: $('#costo_0').val(),
							dataCurrency_venta: $('#moneda').val(),
							dataVenta: $('#venta_0').val()
						},
							success: function(rsp){
										if(rsp > 0){
											$('#markup_'+indice[1]).val(rsp);
										}else{
											$('#markup_'+indice[1]).val(rsp);
										}
				
						}
				})
			})
			calcularMarkup();
				/////////////////////////////////////////////////PARA AJUSTAR///////////////////////////////////////////////////////////			
		}

		function calcularPrecioCosto(comisionable, totalCosto, producto,monedaCompra,monedaVenta){
			$.ajax({
						type: "GET",
						url: "{{route('calcularPrecioCosto')}}",
						dataType: 'json',
						data: {
								dataTotalCosto: totalCosto,
								dataBaseComisionable: comisionable,
								dataProducto: producto,
								dataMonedaCompra: monedaCompra,
								dataMonedaVenta: monedaVenta
							},
						error: function(jqXHR,textStatus,errorThrown){
							$.toast({
								heading: 'Error',
								position: 'top-right',
								text: 'Ocurrió un error en la comunicación con el servidor.',
								showHideTransition: 'fade',
								icon: 'error'
							});
						},
						success: function(rsp){
									$('#venta_0').val(rsp);
						}
				})
			
			calcularMarkup();
		}	

		function calcularMarkup(){
			 $.ajax({
					type: "GET",
					url: "{{route('calcularMarkup')}}",
					dataType: 'json',
					data: {
							dataTasas: $('#tasas_0').val(),
							dataMonedaCompra: $('#moneda_compra_0').val(),
							dataCosto: $('#costo_0').val(),
							dataCurrency_venta: $('#moneda').val(),
							dataVenta: $('#venta_0').val()
						},
							success: function(rsp){
										if(rsp > 0){
											$('#markup_0').val(rsp);
										}else{
											$('#markup_0').val(rsp);
										}
				
						}
				})	

		}	

		addFilaDetalle();

		function addFilaDetalle(){
			$.ajax({
					type: "GET",
					url: "{{route('getDetalleSolicitud')}}",
					dataType: 'json',
					data: {
							dataProforma: '{{$proformas[0]->id}}',
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                            position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
								$.each(rsp, function(index,value){	
									if(jQuery.isEmptyObject(value.pasajero) == false){
										pasajero = value.pasajero.nombre+ ' '+value.pasajero.apellido;
									}else{
										pasajero ="";
									}
									if(jQuery.isEmptyObject(value.cliente) == false){
										cliente = value.cliente.nombre+ ' '+value.cliente.apellido;
									}else{
										cliente ="";
									}

									if(jQuery.isEmptyObject(value.usuario) == false){
										usuario = value.usuario.nombre;
									}else{
										usuario ="";
									}

									//console.log(value.id+','+value.fecha_hora_pedido+','+value.cliente.nombre+','+value.usuario.nombre+','+value.currency.currency_code+','+value.monto+','+value.estado.denominacion+',-'+value.pasajero.nombre+ ' '+value.pasajero.apellido);
									generarFilaSolicitudes(value.id,value.fecha_hora_pedido,cliente,usuario,value.currency.currency_code,value.monto,value.estado.denominacion,pasajero);
								})	
					}
			})
		}	 


		function generarFilaSolicitudes(id,fecha_hora_pedido,cliente,usuario,currency,monto,estado,pasajero){
			var rowCount = $("#soluciones tbody tr").length;

			var tbody = $('#soluciones tbody');

			fecha = fecha_hora_pedido.split(' ');
			fecha_formateado = formatearFecha(fecha[0])+' '+fecha[1];
            monto_formateado = formatter.format(parseFloat(monto));
			if(estado == 'Pendiente'){
				btn = '<button type="button" class="btn btn-danger" onclick="eliminarFilaSolicitud('+id+')" id="btnGuardarFormaPago"><b>Eliminar</b></button>';
			}else{
				btn = '<br><br>';
			}

			var htmlTags =  '<tr id="'+id+'">'+
									'<td>'+fecha_formateado+'</td>'+
									'<td>'+cliente+'</td>'+
									'<td>'+usuario+'</td>'+
									'<td>'+pasajero+'</td>'+
									'<td>'+currency+'</td>'+
									'<td>'+monto_formateado+'</td>'+
									'<th>'+estado+'</th>'+
									'<th>'+
									btn+
									'</th>'+
							'</tr>';
			tbody.append(htmlTags);
		}	

		function eliminarFilaSolicitud(id){
			$.ajax({
					type: "GET",
					url: "{{route('getEliminrSolicitud')}}",
					dataType: 'json',
					data: {
							idPedido: id,
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                            position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
									if(rsp.status == 'OK'){
										$('#'+id).remove();
										$.toast({
												heading: 'Exito',
												text: rsp.mensaje,
												position: 'top-right',
												showHideTransition: 'slide',
												icon: 'success'
											});
											location.reload();
									}else{	
										$.toast({
												heading: 'Error',
												position: 'top-right',
												text: rsp.mensaje,
												showHideTransition: 'fade',
												icon: 'error'
											});
									}

					}
			})
		}	 
		
		$('#cant_0').change(function() {
			$('#cantidad_0').val($(this).val());
		})


		function deleteFilaDetalle(id){
			$('#detalleFila'+id).remove();
		}

		 
		$("#clienteNuevo").click(function(){
			$("#nuevoCliente").css('display', 'block')
		})
		
		$("#btnCerrarCliente").click(function(){
			$("#nuevoCliente").css('display', 'none')
		})

		$("#agregarClienteAux").click(function(){
			$.ajax({
					type: "GET",
					url: "{{route('getGuardarClienteAuxiliar')}}",
					dataType: 'json',
					data: {
							dataCliente: $("#cliente_id_aux").val(),
							dataProforma: '{{$proformas[0]->id}}'
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                            position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },
					success: function(rsp){
									if(rsp.status == 'OK'){
										$.toast({
												heading: 'Exito',
												text: rsp.mensaje,
												position: 'top-right',
												showHideTransition: 'slide',
												icon: 'success'
											});
											var htmlTags =  '<div class="row" id="columna'+rsp.id+'">'+
																'<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;padding-right: 0px;">'+
																'<input type="text" id="clienteAuxiliar_'+rsp.id+'" name="clienteAuxiliar_'+rsp.id+'" class ="form-control form-control-sm" value ="'+$("#cliente_id_aux").find('option:selected').text()+'"/>'+
																'</div>'+
																'<div class="col-xs-12 col-sm-6 col-md-1">'+
																	'<button type="button" onclick="deleteClienteAux('+rsp.id+')" id="delete_'+rsp.id+'" class="btn btn-danger"><i class="fa fa-minus" aria-hidden="true"></i></button>'+
																'</div>'+
															'</div>';
															$("#clientesAdicionales").append(htmlTags);	
															$("#cliente_id_aux").val('');
									}else{	
										$.toast({
												heading: 'Error',
												position: 'top-right',
												text: rsp.mensaje,
												showHideTransition: 'fade',
												icon: 'error'
											});
									}


					}
			})
		})

							function deleteClienteAux(id){
					$.ajax({
							type: "GET",
							url: "{{route('getEliminrClienteAux')}}",
							dataType: 'json',
							data: {
									idCliente: id,
								},
							error: function(jqXHR,textStatus,errorThrown){
								$.toast({
									heading: 'Error',
									position: 'top-right',
									text: 'Ocurrió un error en la comunicación con el servidor.',
									showHideTransition: 'fade',
									icon: 'error'
								});

							},
							success: function(rsp){
									if(rsp.status == 'OK'){
										$('#columna'+id).remove();
										$.toast({
												heading: 'Exito',
												text: rsp.mensaje,
												position: 'top-right',
												showHideTransition: 'slide',
												icon: 'success'
											});
									}else{	
										$.toast({
												heading: 'Error',
												position: 'top-right',
												text: rsp.mensaje,
												showHideTransition: 'fade',
												icon: 'error'
											});
									}

							}
						});

				}	

				cargarDatosAuxiliares();
				function cargarDatosAuxiliares(){
					$.ajax({
							type: "GET",
							url: "{{route('getDatosClienteAux')}}",
							dataType: 'json',
							data: {
									dataProforma: '{{$proformas[0]->id}}'
								},
							error: function(jqXHR,textStatus,errorThrown){
								$.toast({
									heading: 'Error',
									position: 'top-right',
									text: 'Ocurrió un error en la comunicación con el servidor.',
									showHideTransition: 'fade',
									icon: 'error'
								});

							},
							success: function(rsp){
											$.each(rsp, function(index,value){	
												var datos = value.nombre;
												if(jQuery.isEmptyObject(value.apellido) == false){
													var datos = value.nombre +' '+value.apellido;
												}
												nombre = datos+' - '+value.tipo_persona
												var documento = value.documento_identidad; 
												if(jQuery.isEmptyObject(value.dv) == false){
													var documento = value.documento_identidad+"-"+value.dv; 
												}

												resultado = documento +' '+nombre;
												var htmlTags =  '<div class="row" id="columna'+value.id+'">'+
																'<div class="col-xs-12 col-sm-6 col-md-10" style="padding-left: 15px;padding-right: 0px;">'+
																'<input type="text" id="clienteAuxiliar_'+value.id+'" name="clienteAuxiliar_'+value.id+'" class ="form-control form-control-sm" value ="'+resultado+'"/>'+
																'</div>'+
																'<div class="col-xs-12 col-sm-6 col-md-1">'+
																	'<button type="button" onclick="deleteClienteAux('+value.id+')" id="delete_'+value.id+'" class="btn btn-danger"><i class="fa fa-minus" aria-hidden="true"></i></button>'+
																'</div>'+
															'</div>';
												
												$("#clientesAdicionales").append(htmlTags);	

											})	
							}
						});

				}	

	function updateDetalle(idProforma,item){
				//$('.btn-update-request').click(function(){
						//var idProforma = $(this).attr('id');
						$('#linea').val(item);
						$("#especificosDatos").css('display', 'none');
						$("#guardar").css('display', 'none');
						$("#detalle_proforma_id").val(idProforma);
						$('#especificosDatos').css('display', 'none');
						ajusteMontos();	
						$.ajax({
								type: "GET",
								url: "{{route('datosProformaDetalle')}}",
								dataType: 'json',
								data: {
									dataDetalleProforma: idProforma,
									dataProforma: $('#proforma_id').val()
										}, 
								success: function(rsps){
									console.log(rsps);
									producto = rsps[0].id_producto;
									
									$("#productoId_0").val(producto);
									$('#cantidad_0').val(rsps[0].cantidad);
									$.ajax({
										type: "GET",
										url: "{{route('consultaGrupoProducto')}}",
										dataType: 'json',
										data: {
												dataPrducto: rsps[0].id_producto,
											},
										success: function(rsp){
												ajusteTipo()
												let es_nemo = false;
												let es_cangoroo = false;

												$('#reservaNemo').css('display', 'none');
												$('#reservaCangoroo').css('display', 'none');

												if(rsp.producto_nemo){
													es_nemo = true;
													$('#reservaNemo').css('display', 'block'); 
												} else if(rsp.producto_cangoroo){
													es_cangoroo = true;
                                                    $('#reservaCangoroo').css('display', 'block');
												}

												 
													

												var valor = parseInt(rsp.grupo_producto);
												$('#grupo').val(valor); 
												if(valor != 0){
													if(valor == 1){
															perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
															if(perfil  == 1){
																$('#gravado_costo_0').prop("disabled", true);
															}else{
																$('#gravado_costo_0').prop("disabled", false);
															}

															$('#especificosDatos').css('display', 'block');
															$('#productoTicket').val(producto);
															$.ajax({
																type: "GET",
																url: "{{route('filtrarDatos')}}",
																data: {
																	idMonedaProforma: $("#moneda").val(),
																	idGrupo: $("#grupo_id").val(),
																	idProforma: $("#proforma_id").val(),
																	idDetalle: $("#detalle_proforma_id").val()
																},
																dataType: 'json',
																success: function(rsp){
																				//$('.collapse').collapse()comision_operador_0
																				$("#total_ticket").val(0);
																				$("#totalVenta").val(0);
																				$("#fee_unico").val(0);
																				var contenido = "";
																					contenido += "<div class='row'>";
																					contenido += "<div class='col-md-3'>";
																					contenido += "<div class='input-group-sm'>";
																					contenido += "<label>Tipo</label><br>";
																					contenido += "<select class='form-control input-sm select2' name='tipo' id='tipo' style='width: 100%;'>";
																					contenido += "<option value='0'>Selecccione un Tipo</option>";
																					@foreach($tiposTickets as $tipoticket)
																						tipo_ticket_id = "{{$tipoticket->id}}";
																						tipo_ticket_descripcion ='{{$tipoticket->descripcion}}';
																						if(tipo_ticket_id == 1){
																							contenido += `<option value='`+tipo_ticket_id+`' selected='selected'>`+tipo_ticket_descripcion+`</option>`;
																						}else{	
																							contenido += `<option value='`+tipo_ticket_id+`'>`+tipo_ticket_descripcion+`</option>`;
																						}	
																					@endforeach
																					contenido += "</select>";
																					contenido += "</div>";
																					contenido += "</div>";
																					
																					contenido += "</div>";
																					contenido += "<div class='row'>";
																					contenido += "<div class='col-md-12'> ";
																					contenido += "<table id='vuelos' class='table' style='width: 100%;font-size: 0.9rem;'>";
																					contenido += "<thead>";
																					contenido += "<tr>";
																					contenido += "<th></th>";
																					contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Nro Ticket</th>";
																					contenido += "<th style='padding-left: 0px;padding-right: 0px;'>PNR</th>";
																					contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Pasajero</th>";
																					contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Moneda</th>";
																					contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Venta</th>";
																					contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Tarifa</th>";
																					contenido += "<th style='padding-left: 0px;padding-right: 0px;'>Aerolinea</th>";
																					contenido += "<th id='thgrupo' style='padding-left: 0px;padding-right: 0px;'>Grupo</th>";

																					contenido += "</tr>";
																					contenido += "</thead>";	
																					$.each(rsp.tickets, function (key, item){
																						if(item.numero_amadeus !== null){
																							numeroT = item.numero_amadeus;
																						}else{
																							numeroT = '';
																						}	
																						
																						if(item.pasajero !== null){
																							pasajeroT = item.pasajero;
																						}else{
																							pasajeroT = '';
																						}

																						if(item.comision != 0){
																							comision = 'Comisionable';
																						}else{
																							comision = 'Neto';
																						}
																						if(jQuery.isEmptyObject(item.currency.currency_code) != false){
																							valor = item.currency.currency_code	
																						}else{
																							valor = "";
																						}


																						var n = item.persona;
																						if(n != null){
																							proveedor = (item.persona.nombre != null) ? item.persona.nombre : '';
																						}else{
																							proveedor = '';
																						}	
																						if(jQuery.isEmptyObject(item.pnr) == false){
																							pnr =item.pnr;
																						}else{
																							pnr = '';
																						}	

																						if(jQuery.isEmptyObject(item.precio_venta) == false){
																							precio_venta =item.precio_venta;
																						}else{
																							precio_venta = '';
																						}	
																						if(jQuery.isEmptyObject(item.id_grupo) == false){
																							id_grupo =item.id_grupo;
																						}else{
																							id_grupo = '';
																						}	
																						
																						if(jQuery.isEmptyObject(item.comision) == false){
																							comision =item.comision;
																						}else{
																							comision = '';
																						}	

																						if(jQuery.isEmptyObject(item.id_currency_costo) == false){
																							id_currency_costo =item.id_currency_costo;
																						}else{
																							id_currency_costo = '';
																						}	

																						contenido += "<tr id='columna_"+item.id+"'>";
																						contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 0px;'>";
																						if(item.ckeck == true){
																							contenido += "<input type='checkbox' id='"+item.id+"' checked='checked' style='width: 15px;height: 25px;' class='form-control listOrigen "+item.id_proveedor+"' indice='"+id_grupo+"' data='"+item.id_proveedor+"' comision='"+comision+"' currency = '"+id_currency_costo+"' onclick='pasar("+item.id+")'  value='"+item.id+"' onclick='pasar("+item.id+")' name='tickets["+item.id+"]'>";
																						}else{	
																							contenido += "<input type='checkbox' id='"+item.id+"' style='width: 15px;height: 25px;' class='form-control listOrigen "+item.id_proveedor+"' indice='"+id_grupo+"' data='"+item.id_proveedor+"' comision='"+comision+"' currency = '"+id_currency_costo+"' onclick='pasar("+item.id+")'  value='"+item.id+"' onclick='pasar("+item.id+")' name='tickets["+item.id+"]'>";
																						}	

																						contenido += "</td>";
																						contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+numeroT+"</td>";
																						contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+pnr+"</td>";
																						contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+pasajeroT+"</td>";
																						contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+valor+"</td>";
																						contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+precio_venta+"</td>";
																						contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+comision+"</td>";
																						contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'>"+proveedor+"</td>";
																						contenido += "<td style='padding-left: 0px;padding-right: 0px;padding-bottom: 5px;padding-top: 5px;'></td>";
																						contenido += "</tr>";
																					})
																					contenido += "</table>";
																					contenido += "</div>";	
																					contenido += "</div>";
																					var totalDetalle =  parseFloat(rsps[0].precio_venta);
																					var totalTicket =  parseFloat(rsps[0].precio_venta);
																					var fee = parseFloat(rsps[0].fee_unico);

																					contenido += "<div class ='row'>";
																					contenido += "<div class='col-md-4'></div>";
																					contenido += "<div class='col-md-3'>";
																					contenido += '<input type="hidden" disabled="disabled" class = "Requerido form-control form-control-sm" id="total_ticket" value="'+totalTicket+'" name="total_ticket" disabled="disabled" />';
																					contenido += '<label>FEE</label>';
																					contenido += '<input type="text" class = "Requerido form-control form-control-sm fee numeric" id="fee_unico" value="'+parseFloat(fee)+'" name="fee_unico"/>';
																					contenido += "</div>";
																					contenido += "<div class='col-md-3'>";
																					contenido += '<label>Total Tickets</label>';
																					contenido += '<input type="text" disabled="disabled" class = "Requerido form-control form-control-sm" id="totalVenta" value="'+totalDetalle+'" name="totalVenta" disabled="disabled"/>';
																					contenido += "</div>";

																					contenido += "<div class='col-md-1'>"
																					contenido += '<button type="button" class="btn btn-outline-primary" style="width: 100px;" id="procesar"><b>Procesar</b></button>';
																					contenido += "</div>";	
																					contenido += "<div id ='ticketsSelect'>";
																					contenido += "</div'>";
																																	
																				$('#idEspecifico').html(contenido);
																					inizializateVuelo();
																					$("#tipoAsistencia").val(rsps[0].id_tarifa_asistencia);
																					$("#inout").val(rsps[0].fecha_in+""+rsps[0].fecha_out);

																					var fechaIn = rsps[0].fecha_in.split('-');
																					var periodoIn = fechaIn[2]+'/'+fechaIn[1]+'/'+fechaIn[0];

																					var fechaOut = rsps[0].fecha_out.split('-');
																					var periodoOut = fechaOut[2]+'/'+fechaOut[1]+'/'+fechaOut[0];

																					$("#periodo_0").val(periodoIn+" - "+periodoOut);	
																					$("#personasAsistencia").val(rsps[0].asistencia_cantidad_personas);	
																					$("#descrip_factura_0").val(rsps[0].descripcion);
																					$("#confirmacion_0").val(rsps[0].cod_confirmacion);
																					$("#moneda_compra_0").val(rsps[0].currency_costo_id).select2();
																					$("#costo_0").val(rsps[0].costo_proveedor);
																					$("#tasas_0").val(rsps[0].monto_no_comisionable);
																					$("#base_comisionable_0").val(rsps[0].base_comisionable);
																					$("#comision_agencia_0").val(rsps[0].porcentaje_comision_agencia);
																					$("#venta_0").val(rsps[0].precio_venta);
																					$("#markup_0").val(rsps[0].markup);
																					if(jQuery.isEmptyObject( rsps[0].fecha_pago_proveedor) == false){
																						var fechaPago = rsps[0].fecha_pago_proveedor.split('-');
																						var fechaPagoFormato = fechaPago[2]+'/'+fechaPago[1]+'/'+fechaPago[0];
																					}else{
																						var fechaPagoFormato = ""; 
																					}	
																					$("#vencimientos").val(fechaPagoFormato);
																					if(jQuery.isEmptyObject(rsps[0].fecha_gasto) == false){
																						var fechaGasto = rsps[0].fecha_gasto.split('-');
																						var fechaGastoFormato = fechaGasto[2]+'/'+fechaGasto[1]+'/'+fechaGasto[0];
																					}else{
																						var fechaGastoFormato =	"";
																					}	
																					$('#cantidad_0').val(rsps[0].cantidad);
																					$("#fecha_gasto_0").val(fechaGastoFormato);
																					$("#monto_gasto_0").val(rsps[0].monto_gasto);
																					$("#gravado_costo_0").val(rsps[0].costo_gravado);
																					$("#exentos_0").val(rsps[0].porcion_exenta);
																					$("#gravada_0").val(rsps[0].porcion_gravada);

																					$("#confirmacion_0").prop("disabled", true);
																					$("#proveedor_0").prop("disabled", true);
																					$("#prestador_0").prop("disabled", true);
																					$("#costo_0").prop("disabled", true);
																					$("#tasas_0").prop("disabled", true);
																					$("#base_comisionable_0").prop("disabled", true);
																					$("#venta_0").prop("disabled", true);
																					$("#markup_0").prop("disabled", true);
																					$("#periodo_0").prop("disabled", true);
																					$("#moneda_compra_0").prop("disabled", true);
																					$('#comision_agencia_0').prop("disabled", false);
																					perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
																					if(perfil == 1){
																						$('#vencimientos').prop("disabled", true);
																						$('#monto_gasto_0').prop("disabled", true);
																						$('#fecha_gasto_0').prop("disabled", true);
																					}		

																					$('#especificosDatos .collapse').collapse('show');
																			}//success	
																	});//ajax	
													}	

													//Alojamientos
													if(valor == 2){
														if(es_nemo){
															datosReservasHotelNemo();
														}

														if(es_cangoroo){
															datosReservasCangoroo(1);
														}
														
													}			
													
													
													if(valor == 3){
															perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
															if(perfil  == 1){
																$('#gravado_costo_0').prop("disabled", true);
															}else{
																$('#gravado_costo_0').prop("disabled", false);
															}
															contenido = '';
															$('#especificosDatos').css('display', 'block');
															contenido += '<div class="row">';
															contenido += '<div class="col-md-3" style="padding-left: 25px;">';
															contenido += '<div class="form-group">';
															contenido += '<label>Tipo Asistencia</label>';
															contenido += '<select class="form-control form-control-sm select2" name="tipo_asistencia" id="tipoAsistencia" style="width: 100%;" >';
															contenido += '<option value="0">Seleccione Tipo de Asistencia</option>';
															@foreach($asistencias as $key=>$asistencia)
															contenido += '<option value="{{$asistencia->id}}">{{$asistencia->tarifa_asistencia_nombre}} - {{$asistencia->tarifa_asistencia_precio}}</option>';
															@endforeach	
															contenido += '</select>';
															contenido += '</div>';
															contenido += '</div>';
															contenido += '<div class="col-md-3" style="padding-left: 25px;">';
															contenido += '<div class="form-group">';
															contenido += '<label>Cantidad de Personas</label>';
															contenido += '<input type="text" onkeypress="return justNumbers(event);" class = "Requerido form-control form-control-sm" name="personas" id="personasAsistencia" value="0" />';
															contenido += '</div>';
															contenido += '</div>';  
															contenido += '<div class="col-md-3" style="padding-left: 25px;">';
															contenido += '<label></label>';
															contenido += '<br>';
															contenido += '<button type="button" id="btnAsistencia" class="btn btn-outline-primary" style="width: 90px; ">Aceptar</button>';
															contenido += '</div>';	
															contenido += '</div>';
															$('#idEspecifico').html(contenido);
															inizializateAsistencia(producto)
															
															$("#tipoAsistencia").val(rsps[0].id_tarifa_asistencia).select2();
															$("#inout").val(rsps[0].fecha_in+""+rsps[0].fecha_out);	
															var fechaIn = rsps[0].fecha_in.split('-');
															var periodoIn = fechaIn[2]+'/'+fechaIn[1]+'/'+fechaIn[0];

															var fechaOut = rsps[0].fecha_out.split('-');
															var periodoOut = fechaOut[2]+'/'+fechaOut[1]+'/'+fechaOut[0];
															$("#gravado_costo_0").val(rsps[0].costo_gravado);
															$("#periodo_0").val(periodoIn+" - "+periodoOut);	
															$("#personasAsistencia").val(rsps[0].asistencia_cantidad_personas);	
															$("#descrip_factura_0").val(rsps[0].descripcion);
															$("#confirmacion_0").val(rsps[0].cod_confirmacion);
															$("#moneda_compra_0").val(rsps[0].currency_costo_id).select2();
															$("#exentos_0").val(rsps[0].porcion_exenta);
															$("#gravada_0").val(rsps[0].porcion_gravada);
															$("#costo_0").val(rsps[0].costo_proveedor);
															$("#tasas_0").val(rsps[0].monto_no_comisionable);
															$("#base_comisionable_0").val(rsps[0].base_comisionable);
															$("#comision_agencia_0").val(rsps[0].porcentaje_comision_agencia);
															$("#venta_0").val(rsps[0].precio_venta);
															$("#markup_0").val(rsps[0].monto_gasto);
															var fechaPago = rsps[0].fecha_pago_proveedor
															var fechaPagoFormato = fechaPago[2]+'/'+fechaPago[1]+'/'+fechaPago[0];
															$("#vencimientos").val(fechaPagoFormato);
															if(jQuery.isEmptyObject( rsps[0].fecha_gasto) == false){
																var fechaGasto = rsps[0].fecha_gasto.split('-');
																var fechaGastoFormato = fechaGasto[2]+'/'+fechaGasto[1]+'/'+fechaGasto[0];
															}else{
																	fechaGastoFormato = "";
															}	
															$("#fecha_gasto_0").val(fechaGastoFormato);
															$('#cantidad_0').val(rsps[0].cantidad);
															$('#especificosDatos .collapse').collapse('show');
													}

													//Circuitos - tour
													if(valor == 6){
														if(es_nemo){
															datosReservasCircuitoNemo();
														}

														if(es_cangoroo){
															datosReservasCangoroo(2);
														}
													}


													if(valor == 8){
														perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
														if(perfil  == 1){
															$('#gravado_costo_0').prop("disabled", true);
														}else{
															$('#gravado_costo_0').prop("disabled", false);
														}

														$('#especificosDatos').css('display', 'block');
														var contenido = "";
														contenido += "<div class='row'>";
														contenido += "<div class='col-md-12'>";
														contenido += "<div class='input-group-sm'>";
														contenido += "<label>Tipo</label><br>";
														contenido += '<select class="form-control select2" name="tipo_traslado_id" id="tipo_traslado_id" style="width: 100%;" >';
														contenido += '<option value="0">Seleccione Tipo</option>';
														contenido += '<option value="1">In</option>';
														contenido += '<option value="2">Out</option>';
														contenido += '<option value="3">In/Out</option>';
														contenido += '</select>';
														contenido += "</div>";
														contenido += "</div>";
														contenido += "</div>";
														contenido += '<div id="inDiv" style="display: none;">';
														contenido += '<div class="row" style="margin-top: 10px;">';
														contenido += '<div class="col-md-3" style="padding-left: 25px;">';
														contenido += '<label>Fecha In</label>';
														contenido += '<div class="input-group">';
														contenido += '<div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">';
														contenido += '<i class="fa fa-calendar"></i>';
														contenido += '</div>';
														contenido += '<input type="text" value="" class="form-control form-control-sm pull-right fechaHora" name="in" id="in">';
														contenido += '</div>';
														contenido += '</div>';
														contenido += '<div class="col-md-3" style="padding-left: 10px;padding-right: 20px;">';
														contenido += '<label>Hora In:</label>';
														contenido += '<div class="input-group">';
														contenido += '<input type="time" name="horaIn" id="horaIn" class="form-control form-control-sm">';
														contenido += '<div class="input-group-addon">';
														contenido += '<i class="fa fa-clock-o"></i>';
														contenido += '</div>';
														contenido += '</div>';
														contenido += '</div> ';
														contenido += '<div class="col-md-6" style="padding-left: 10px;padding-right: 20px;">';
														contenido += '<label>Numero de Vuelo</label>';
														contenido += '<input type="text" class = "Requerido form-control form-control-sm" maxlength="100" name="vuelo_in" id="vuelo_in" style="font-weight: bold;font-size: 15px;"/>';
														contenido += '</div>';
														contenido += '</div>';
														contenido += '</div>';	
														contenido += '<div id="outDiv" style="display: none;">';
														contenido += '<div class="row" style="margin-top: 10px;">';						
														contenido += '<div class="col-md-3" style="padding-left: 25px;">';
														contenido += '<label>Fecha Out</label>';
														contenido += '<div class="input-group">';
														contenido += '<div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">';
														contenido += '<i class="fa fa-calendar"></i>';
														contenido += '</div>';
														contenido += '<input type="text" value="" class="form-control form-control-sm pull-right fechaHora" name="out" id="out">';
														contenido += '</div>';
														contenido += '</div>';
														contenido += '<div class="col-md-3" style="padding-left: 10px;padding-right: 20px;">';
														contenido += '<label>Hora Out:</label>';
														contenido += '<div class="input-group">';
														contenido += '<input type="time" class="form-control form-control-sm" name="horaOut" id="horaOut">';
														contenido += '<div class="input-group-addon">';
														contenido += '<i class="fa fa-clock-o"></i>';
														contenido += '</div>';
														contenido += '</div>';
														contenido += '</div>';   	
														contenido += '<div class="col-md-6" style="padding-left: 10px;padding-right: 20px;">';
														contenido += '<label>Numero de Vuelo</label>';
														contenido += '<input type="text" class = "Requerido form-control form-control-sm" maxlength="100" name="vuelo_out" id="vuelo_out" style="font-weight: bold;font-size: 15px;"/>';
														contenido += '</div>';
														contenido += '</div>';	
														contenido += '</div>';
														contenido += '<div class="row" style="margin-top: 10px;">';	
														contenido += '<div class="col-md-8" style="padding-left: 25px;">';	
														contenido += '</div>';				
														contenido += '<div class="col-md-3" style="padding-left: 25px;">';
														contenido += '<button type="button" id="btnActualizarTraslado" class="btn btn-outline-primary" style="width: 90px; margin-left: 80%;" >Aceptar</button>';
														contenido += '</div>';
														$('#idEspecifico').html(contenido);
														inizializateTraslado();
															$("#tipoAsistencia").val(rsps[0].id_tarifa_asistencia).select2();
															$("#inout").val(rsps[0].fecha_in+""+rsps[0].fecha_out);	
															var fechaIn = rsps[0].fecha_in.split('-');
															var periodoIn = fechaIn[2]+'/'+fechaIn[1]+'/'+fechaIn[0];

															var fechaOut = rsps[0].fecha_out.split('-');
															var periodoOut = fechaOut[2]+'/'+fechaOut[1]+'/'+fechaOut[0];
															
															$("#gravado_costo_0").val(rsps[0].costo_gravado);
															$("#periodo_0").val(periodoIn+" - "+periodoOut);
															$("#personasAsistencia").val(rsps[0].asistencia_cantidad_personas);	
															$("#descrip_factura_0").val(rsps[0].descripcion);
															$("#confirmacion_0").val(rsps[0].cod_confirmacion);
															$("#moneda_compra_0").val(rsps[0].currency_costo_id).select2();
															$("#costo_0").val(rsps[0].costo_proveedor);
															$("#tasas_0").val(rsps[0].monto_no_comisionable);
															$("#base_comisionable_0").val(rsps[0].base_comisionable);
															$("#comision_agencia_0").val(rsps[0].porcentaje_comision_agencia);
															$("#venta_0").val(rsps[0].precio_venta);
															$("#markup_0").val(rsps[0].monto_gasto);
															$("#exentos_0").val(rsps[0].porcion_exenta);
															$("#gravada_0").val(rsps[0].porcion_gravada);
															$('#cantidad_0').val(rsps[0].cantidad);
															var fechaPago = rsps[0].fecha_pago_proveedor
															var fechaPagoFormato = fechaPago[2]+'/'+fechaPago[1]+'/'+fechaPago[0];
															$("#vencimientos").val(fechaPagoFormato);
															if(jQuery.isEmptyObject( rsps[0].fecha_gasto) == false){
																var fechaGasto = rsps[0].fecha_gasto.split('-');
																var fechaGastoFormato = fechaGasto[2]+'/'+fechaGasto[1]+'/'+fechaGasto[0];
															}else{
																var fechaGastoFormato = "";
															}	
															$("#fecha_gasto_0").val(fechaGastoFormato);
															$('#especificosDatos .collapse').collapse('show');
													}

													if(valor == 13){
														datosReservasActividadNemo();
													}


													//Transalado
													if(valor == 10){
														perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
														if(perfil  == 1){
															$('#gravado_costo_0').prop("disabled", true);
														}else{
															$('#gravado_costo_0').prop("disabled", false);
														}

														if(perfil == 4 ||perfil == 8){

															$('#especificosDatos').css('display', 'block');
															var contenido = "";
															contenido += "<div class='row'>";						
															contenido += '<div class="col-md-2 col-sm-3 col-md-3" style="padding-left: 0px;">';
															contenido += '<div class="form-group">';
															contenido += '<label>Tipo Asistencia</label>';
															contenido += '<select class="form-control select2" name="tipo_asistenciaAU" id="tipoAsistenciaAU" style="width: 100%;" >';
															contenido += '<option value="0">Seleccione Tipo de Asistencia</option>';
															@foreach($asistencias as $key=>$asistencia)
															contenido += '<option value="{{$asistencia->id}}">{{$asistencia->tarifa_asistencia_nombre}} - {{$asistencia->tarifa_asistencia_precio}}</option>';
															@endforeach	
															contenido += '</select>';
															contenido += '</div>';
															contenido += '</div>';
															contenido += '<div class="col-md-2 col-sm-1 col-md-1" style="padding-left: 0px;padding-right: 0px;">';
															contenido += '<div class="form-group">';
															contenido += '<label>Personas</label>';
															contenido += '<input type="text" onkeypress="return justNumbers(event);" class = "Requerido form-control form-control-sm" name="personasAU" id="personasAsistenciaAU" value="0" />';
															contenido += '</div>';
															contenido += '</div> ';
															contenido += '<div class="col-md-2 col-sm-1 col-md-1" style="padding-left: 0px;padding-right: 0px;">';
															contenido += '<br>';
															contenido += '<button type="button" id="btnAsistenciaUA" class="btn btn-outline-primary" >Aceptar</button>';
															contenido += '</div>';
															contenido += '</div>';

															$('#idEspecifico').html(contenido);
															inizializateUA(producto)
															var fechaIn = rsps[0].fecha_in.split('-');
															var periodoIn = fechaIn[2]+'/'+fechaIn[1]+'/'+fechaIn[0];
															var fechaOut = rsps[0].fecha_out.split('-');
															var periodoOut = fechaOut[2]+'/'+fechaOut[1]+'/'+fechaOut[0];
															$("#periodo_0").val(periodoIn+" - "+periodoOut).change();
															$("#personasAsistencia").val(rsps[0].asistencia_cantidad_personas);	
															$("#personasAsistenciaAU").val(rsps[0].asistencia_cantidad_personas);	
															$("#tipoAsistenciaAU").val(rsps[0].id_tarifa_asistencia).trigger('change.select2');
															$("#descrip_factura_0").val(rsps[0].descripcion);
															$("#confirmacion_0").val(rsps[0].cod_confirmacion);
															$("#moneda_compra_0").val(rsps[0].currency_costo_id).select2();
															$("#gravado_costo_0").val(rsps[0].costo_gravado);
															$("#costo_0").val(rsps[0].costo_proveedor);
															$("#tasas_0").val(rsps[0].monto_no_comisionable);
															$("#base_comisionable_0").val(rsps[0].base_comisionable);
															$("#comision_agencia_0").val(rsps[0].porcentaje_comision_agencia);
															$("#exentos_0").val(rsps[0].porcion_exenta);
															$("#gravada_0").val(rsps[0].porcion_gravada);
															$("#venta_0").val(rsps[0].precio_venta);
															$("#markup_0").val(rsps[0].monto_gasto);	
															var fechaPago = rsps[0].fecha_pago_proveedor
															var fechaPagoFormato = fechaPago[2]+'/'+fechaPago[1]+'/'+fechaPago[0];
															$("#vencimientos").val(fechaPagoFormato);
															if(jQuery.isEmptyObject( rsps[0].fecha_gasto) == false){
																var fechaGasto = rsps[0].fecha_gasto.split('-');
																var fechaGastoFormato = fechaGasto[2]+'/'+fechaGasto[1]+'/'+fechaGasto[0];
															}else{
																var fechaGastoFormato = "";
															}	
															$('#cantidad_0').val(rsps[0].cantidad);
															$("#fecha_gasto_0").val(fechaGastoFormato);
															$('#especificosDatos .collapse').collapse('show');

															if(es_nemo){
																datosReservasTrasladoNemo();
															}

															if(es_cangoroo){
																datosReservasCangoroo(3);
															}
															
														}	
													}else{
														perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
														if(perfil != 1){
															$('#gravado_costo_0').prop("disabled", false);
														}else{
															$('#gravado_costo_0').prop("disabled", true);
														}
														$("#inout").val(rsps[0].fecha_in+""+rsps[0].fecha_out);	
														var fechaIn = rsps[0].fecha_in.split('-');
														var periodoIn = fechaIn[2]+'/'+fechaIn[1]+'/'+fechaIn[0];
														var fechaOut = rsps[0].fecha_out.split('-');
														var periodoOut = fechaOut[2]+'/'+fechaOut[1]+'/'+fechaOut[0];
														$("#periodo_0").val(periodoIn+" - "+periodoOut).change();
														$("#personasAsistencia").val(rsps[0].asistencia_cantidad_personas);	
														$("#descrip_factura_0").val(rsps[0].descripcion);
														$("#confirmacion_0").val(rsps[0].cod_confirmacion);
														$("#moneda_compra_0").val(rsps[0].currency_costo_id).select2();
														$("#costo_0").val(rsps[0].costo_proveedor);
														$("#gravado_costo_0").val(rsps[0].costo_gravado);
														$("#base_comisionable_0").val(rsps[0].base_comisionable);
														$("#tasas_0").val(rsps[0].monto_no_comisionable);
														$("#base_comisionable_0").val(rsps[0].base_comisionable);
														$("#comision_agencia_0").val(rsps[0].porcentaje_comision_agencia);
														$("#venta_0").val(rsps[0].precio_venta);
														$("#markup_0").val(rsps[0].monto_gasto);
														if(jQuery.isEmptyObject(rsps[0].fecha_gasto) == false){
															var fechaPago = rsps[0].fecha_pago_proveedor.split('-');
															var fechaPagoFormato = fechaPago[2]+'/'+fechaPago[1]+'/'+fechaPago[0];
														}else{
															var fechaPagoFormato = '';
														}
														
														$("#vencimientos").val(fechaPagoFormato);
														if(jQuery.isEmptyObject(rsps[0].fecha_gasto) == false){
															var fechaGasto = rsps[0].fecha_gasto.split('-');
															var fechaGastoFormato = fechaGasto[2]+'/'+fechaGasto[1]+'/'+fechaGasto[0];
														}else{
															var fechaGastoFormato = rsps[0].fecha_gasto;	
														}
														$("#fecha_gasto_0").val(fechaGastoFormato);
														$("#monto_gasto_0").val(rsps[0].monto_gasto);
														if(rsps[0].origen == 'A'){
															$('#requestModalDetalle').find('input,select,select2,textarea').prop("disabled", true);
															$("#venta_0").prop("disabled", false);
															$("#editar").css('display', 'block')
														}
														$('#cantidad_0').val(rsps[0].cantidad);
													}
													$("#comision_operador_0").val(rsps[0].comision_moneda_costo);

													calcularMarkup();

													$.ajax({
															type: "GET",
															url: "{{route('getProducto')}}",
															dataType: 'json',
															data: {
																	dataProducto: producto,
																},
															success: function(rsp){
																	$('#productoDescripcion').html(rsp);
																	//$('#productoDescripcion').prop("disabled", true);
															}	
													});
													//$("#comision_agencia_0").prop("disabled", true);
													perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
														if(perfil != 1){
															$('#gravado_costo_0').prop("disabled", false);
														}else{
															$('#gravado_costo_0').prop("disabled", true);
														}
												/*	$.ajax({
															type: "GET",
															url: "{{route('getRevisarCotizacion')}}",
															dataType: 'json',
															data: {
																	dataMonedaCompra: $('#moneda_compra_0').val(),
																	dataCosto: $('#costo_0').val(),
																	dataCurrency_venta: $('#moneda').val(),
																	dataVenta: $('#venta_0').val()
																},
															success: function(rsp){
																if(rsp == 'OK'){
																	$('#editar').prop("disabled", false);
																}else{
																	$('#editar').prop("disabled", true);
																}
															}
														})*/

												}else{ 
													$.toast({
														heading: 'Error',
														text: 'El producto no esta asignado a un Grupo.',
														showHideTransition: 'fade',
														position: 'top-right',
														icon: 'error'
													});
												}		
											}
										})
										prestador();
										select_proveedor();
										$('#proveedor_0').val(rsps[0].proveedor.id);
										$('#prestador_0').val(rsps[0].prestador.id);
										$('#proveedor_id_0').val(rsps[0].proveedor.id);
										$('#prestador_id_0').val(rsps[0].prestador.id);
										$('#select2-proveedor_0-container').html(rsps[0].proveedor.nombre);
										$('#select2-prestador_0-container').html(rsps[0].prestador.nombre);
								}	
								
						})
							
						perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
						idSucursalAgencia = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia}}";
						if(perfil == 1||perfil == 4||perfil == 8 || perfil== 17 && idSucursalAgencia == 58902){
						}else{
							$('#comision_agencia_0').val(0);
							$('#comision_agencia_0').prop("disabled", true);
							$('#com_agencia_0').val(0);
						}
						$("#editar").css('display', 'block');						
						$("#requestModalDetalle").modal('show');
					//})
					console.log('13');
					//updateDetalle();

	}			



	function prestador(){
			console.log('Prestador');
			$("#prestador_0").select2({
					    ajax: {
					            url: "{{route('prestadorProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
						});
		$('#prestador_0').change(function() {
			$('#prestador_id_0').val($(this).val());
		})
	}

	function select_proveedor(){
		$("#proveedor_0").select2({
					    ajax: {
					            url: "{{route('proveedorProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page,
										producto:$("#productoId_0").val()
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
						});

		$('#proveedor_0').change(function() {
			$('#proveedor_id_0').val($(this).val());
		})

	}		
	
	function change_producto(){
		$('#select2-proveedor_0-container').html('');
		$('#select2-prestador_0-container').html('');
	}

	$('#prestador_0').change(function() {
		$('#prestador_id_0').val($(this).val());
	})

	$('#proveedor_0').change(function() {
		$('#proveedor_id_0').val($(this).val());
	})

	function mostrarDetalle(idLinea,numero){
				$('#oculto_'+idLinea).show();
				$('#mostrar_'+idLinea).hide();
				$('#ocultar_'+idLinea).show();
						$.ajax({
								type: "GET",
				             	url: "{{route('mostrarDatosDetalle')}}",//fileDelDTPlus
								dataType: 'json',
								data: {
										dataId: numero,
										dataMoneda:$('#moneda').val()
						       			},
								success: function(arrayDestino){
										tabla = '<td colspan ="12" style="border-color: black;background-color: #dcd9d9;">';
										tabla += '<table cellpadding="8" cellspacing="0" border="0">';
										tabla += '<tr style="font-size: 0.75rem;">';
										tabla += '<td style="width:160px; text-align:center;"><b>Diferencia:</b>&nbsp;&nbsp;'+arrayDestino.diferencia_tributaria+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Costo Exenta:</b>&nbsp;&nbsp;'+arrayDestino.costo_exenta+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Costo Grav.:</b>&nbsp;&nbsp;'+arrayDestino.costo_gravada+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Exenta Item:</b>&nbsp;&nbsp;'+arrayDestino.exenta_item+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Grav.Item:</b>&nbsp;&nbsp;'+arrayDestino.gravada_item+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Iva:</b>&nbsp;&nbsp;'+arrayDestino.iva_porcion_gravada+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Comisión:</b>&nbsp;&nbsp;'+arrayDestino.comision_agencia+'</td>';
										idPerfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}";
										if(idPerfil == 'D'){
											tabla += '<td style="width:160px; text-align:center;"><b>% Comision:</b>&nbsp;&nbsp;'+arrayDestino.porcentaje_comision_agencia+'</td>';
										}
										tabla += '<td style="width:160px; text-align:center;"><b>No Comisiona:</b>&nbsp;&nbsp;'+arrayDestino.monto_no_comisionable+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Incentivo:</b>&nbsp;&nbsp;'+arrayDestino.dtplus+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Neto:</b>&nbsp;&nbsp;'+arrayDestino.neto_facturar+'</td>';
										tabla += '<td style="width:160px; text-align:center;"><b>Renta:</b>&nbsp;&nbsp;'+arrayDestino.renta+'</td></tr></table></td>';
										$('#oculto_'+idLinea).html(tabla);
									}
						})			
	}

	function ocultarDetalle(idLinea){
				$('#oculto_'+idLinea).hide();
				$('#mostrar_'+idLinea).show();
				$('#ocultar_'+idLinea).hide();
				$('#oculto_'+idLinea).html('');
	}


	$('#btnAnular').click(function(){  
				return swal({
                        title: "GESTUR",
                        text: "¿Seguro desea Anular la Proforma?",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, anular",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
										$.ajax({
											type: "GET",
											url: "{{route('anularProforma')}}",
											dataType: 'json',
											data: {
													dataProforma: $('#proforma_id').val()
									       			},
											success: function(rsp){
												$.unblockUI();
												if(rsp[0].anular_proforma == 'OK'){
                                     				swal("Éxito", "", "success");

												//	window.location.replace("{{route('listadosProformas')}}");
												}else{
													swal("Cancelado", rsp[0].anular_proforma, "error");

														
 													// $("#mensajeSuccesSuperior").append('<div class="alert alert-error"><strong>'+ rsp[0].anular_proforma+'</strong><div>');
												}
											}	
										});                                
									} else {
                                     swal("Cancelado", "", "error");
                                }
            			});
			});	

			function datosItem(){
				$.ajax({
					type: "GET",
					url: "{{route('doItem')}}",
					dataType: 'json',
					data: {
							dataProforma: $('#proforma_id').val()
			       			},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                    },
					success: function(rsp){
						$('#item').val(rsp[0].item_nuevo)
					}	
				});
			}

			function pagarTcDetalle(id_proforma_detalle){
				$("#requestPagoTC").modal({
					keyboard:false,
					backdrop:'static'
				});
				$('#pago_tc_id_proforma_detalle').val(id_proforma_detalle);
				$('#pago_tc_comprobante').val('{{$id}}')
			}

				$('#btn_pago_tc').on("click",function(){

						return swal({
							title: "¿Está seguro de realizar esta Operación?",
							text: "Gestur va a generar una Orden de Pago de forma automática, y el detalle ya no podre ser eliminado hasta anular la Orden de Pago",
							buttons: {
									cancel: {
											text: "No",
											value: null,
											visible: true,
											className: "btn-warning",
										},
									confirm: {
											text: "Sí, Realizar",
											value: true,
											visible: true,
										}
									}
						}).then(isConfirm => {
							pagarTc()
						});

				});


				function pagarTc(){
					$('#btn_pago_tc').prop('disabled',true);
					$('#spinner_btn_modal_tc').show();	
					$('#btn_cancelar_tc').prop('disabled',true);

								var formData = new FormData();
								formData.append('tarjeta', $('#tarjeta_utilizada').val());
								formData.append('comprobante', $('#pago_tc_comprobante').val());
								formData.append('id_proforma_detalle', $('#pago_tc_id_proforma_detalle').val());
								formData.append('adjunto_op', $('#adjunto_modal_tc')[0].files[0]);

								$.ajax({
									type: "POST",
									url: "{{route('pagarReservaDetalle')}}",
									data: formData,
									contentType: false,
									processData: false,
									enctype: 'multipart/form-data',
									cache: false,
									error: function () {
										$('#btn_pago_tc').prop('disabled', false);
										$('#btn_cancelar_tc').prop('disabled',false);
										$('#spinner_btn_modal_tc').hide();
										swal("Cancelado", 'Ocurrio un error en la comunicación con el servidor', "error");
									},
									success: function (rsp) {

										if (rsp.status == 'OK') {
											$('#requestPagoTC').modal("hide");
											swal("Éxito!", rsp.mensaje + ' y se creó la OP Nº: ' + rsp.id_op, "success");
											location.reload();
											
										} else {
											$('#requestPagoTC').modal("hide");
											$('#btn_pago_tc').prop('disabled', false);
											$('#btn_cancelar_tc').prop('disabled',false);
											swal("Cancelado", rsp.mensaje, "error");
										}

										$('#spinner_btn_modal_tc').hide();
									}
								});
				}

			
				
				



		function datosReservasCangoroo(id_tipo_reserva){

			/**
			 * 1 Alojamiento
			 * 2 Tour
			 * 3 Transfer
			*/

			$.ajax({
					type: "GET",
					url: "{{route('reservasCangoroo')}}",
					dataType: 'json',
					cache: false,
					data: { 
							id_tipo_reserva : id_tipo_reserva, 
							user_id: "{{$proformas[0]->id_usuario}}" 
						},
					error: function(jqXHR,textStatus,errorThrown){
                        $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor para datos de cangoroo',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                    },

					success: function(rsp){
						let usaurio = '';
						let proveedor;
						let cliente = '';
						let vendedor_empresa = '';
						let checkbox = '';
						let table = `<table id="tableCangoroo" style='width: 100%'>`;
						table += `<thead>`;
						table += `<tr>`;
						    table += `<th></th>`;
							table += `<th>Codigo</th>`;
							table += `<th>Codigo Cangoroo</th>`;
							table += `<th>Check In</th>`;
							table += `<th>Check Out</th>`;
							table += `<th>Cliente</th>`;
							table += `<th>Usuario</th>`;
							table += `<th>Proveedor</th>`;
							table += `<th>Moneda</th>`;
							table += `<th>Precio Total</th>`;
						table += `</tr>`;
						table += `</thead>`;
						table += `<tbody  style='font-size: 0.80rem;'>`;

						$.each(rsp, function(index,value){	
							
							proveedor = 'N/D'; 
							if(value.nombre !== null){	
								proveedor = value.proveedor;
							}

							cliente = 'N/D'; 
							if(value.client_name !== null){	
								cliente = value.client_name;
							}

							vendedor_empresa = 'N/D'; 
							if(value.vendedor_empresa !== null){	
								vendedor_empresa = value.vendedor_empresa;
							}

							// console.log(value);

							checkbox = "<input type='checkbox' onclick='cargarReservaCangoro("+value.id_reserva+","+value.grupo_reserva_id+")' id='"+value.grupo_reserva_id+"_"+value.id_reserva+"' style='width: 15px;height: 25px;' class='form-control checkbox' >";
							table += `<tr>`;
							table += `<td >`+checkbox+`</td>`;
							table += `<td >`+value.identificador+`</td>`;
							table += `<td >`+value.booking_reference+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkin)+`</td>`;
							table += `<td>`+formatearFecha(value.booking_checkout)+`</td>`;
							table += `<td>`+cliente+`</td>`;
							table += `<td>`+vendedor_empresa+`</td>`;
							table += `<td>`+proveedor+`</td>`;
							table += `<td>`+value.moneda_venta+`</td>`;
							table += `<td>`+value.precio_venta+`</td>`;
							table += `</tr>`;
						})


						table += `</tbody>`;
						table += `</table>`;
						$('#idReservaCangoroo').html(table);
						$('#tableCangoroo').dataTable();
						$('#reservaCangoroo .collapse').collapse('show');
						disabledAllDetalle();

					}
				})
		}


		function disabledAllDetalle(){
			// $("#venta_0").prop("disabled", true);
			$("#fecha_gasto_0").prop("disabled", true);
			$("#monto_gasto_0").prop("disabled", true);
			$("#gravado_costo_0").prop("disabled", true);
			$("#comision_agencia_0").prop("disabled", true);
			$("#vencimientos").prop("disabled", true);
			$("#markup_0").prop("disabled", true);
			$("#tasas_0").prop("disabled", true);
			$("#moneda_compra_0").prop("disabled", true);
			$("#base_comisionable_0").prop("disabled", true);
			$("#prestador_0").prop("disabled", true);
			$("#proveedor_0").prop("disabled", true);
			$("#costo_0").prop("disabled", true);
			$("#confirmacion_0").prop("disabled", true);
			$("#periodo_0").prop("disabled", true);
		}



	</script>
@endsection
