
@extends('masters')
@section('title', 'Reservas Cangoroo')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">
	.negrita{
		font-weight: bold;
	}

	.negrita:hover{
		  background-color: #F0F0F0;
	}

</style>

<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Buscar Reserva</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	  <form id="frmSucConsulta" autocomplete="off">
				        <input type="hidden" class ="form-control" name="id_tipo" value="{{$id_tipo}}" id="id_tipo"/>
					
            	  		<div class = "row">
							<div class="col-12">
								<div class="alert alert-light" role="alert">
									<ul>
										<li>Las columnas con contenido N/A(1) significa dato no encontrado y lo que va entre parentesis es el id de cangoroo que no se encuentra configurado en el campo Codigo Cangoroo de Personas</li>
									</ul>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Cliente</label>
									<select class="form-control selectCliente" name="cliente_id"  id="cliente_id" tabindex="1" style="width: 100%;" >
										<option value="">Todos</option>
											@foreach($clientes as $cliente)
												@php
													$ruc = $cliente->documento_identidad;
													if($cliente->dv){
														$ruc = $ruc."-".$cliente->dv;
													}
												@endphp
												<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->nombre}}  {{$cliente->apellido}} - {{$cliente->id}}</option>
											@endforeach
									</select>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Usuario</label>
									<select class="form-control select2" name="id_vendedor_empresa" id="id_vendedor_empresa" tabindex="2" style="width: 100%;" >
										<option value="">Todos</option>
										@foreach($vendedor_empresas as $usuario)
											<option value="{{$usuario->id}}">{{$usuario->nombre}}  {{$usuario->apellido}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Vendedor</label>
									<select class="form-control select2" name="vendedor_id"  id="vendedor_id" tabindex="3" style="width: 100%;">
										<option value="">Todos</option>
										@foreach($vendedor_agencia as $vendedor)
											<option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
										@endforeach
									</select>
								</div>
							</div>
						    <div class="col-12 col-md-3">
								<div class="form-group">
									<label>Nombre Apellido Pasajero</label>
									<input type="text" tabindex="4" class ="form-control" name="nombrePasajero" value="" id="nombrePasajero" maxlength="45"/>
								</div>
							</div>
						</div>
						<div class = "row">			    
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Código Proveedor</label>
									<input type="text" tabindex="5" class = "form-control" name="cod_confirmacion" value="" id="cod_confirmacion"/>
								</div>	
							</div>

							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Proforma</label>
									<input type="text" tabindex="5" class = "form-control" name="proforma" value="" id="proforma"/>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-3">  
									<div class="form-group adaptForm">
									   <label>Desde / Hasta Checkin</label>						 
									   <div class="input-group">
										   <div class="input-group-prepend" style="">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										   </div>
											<input type="text"  class = "Requerido form-control input-sm" id="periodo_in" name="periodo_in" tabindex="6"/>
									   </div>
								   </div>	
							</div> 
							<div class="col-12 col-sm-6 col-md-3">  
									<div class="form-group adaptForm">
									   <label>Desde / Hasta Checkout</label>						 
									   <div class="input-group">
										   <div class="input-group-prepend" style="">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										   </div>
											<input type="text"  class = "Requerido form-control input-sm" id="periodo_out" name="periodo_out" tabindex="7"/>
									   </div>
								   </div>	
							</div> 
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Estado Gestur</label>
									<select class="form-control select2" name="proforma_estado" id="proforma_estado" tabindex="3" style="width: 100%;">
										<option value="">Todos</option>	
										<option value="EN PROFORMA">EN PROFORMA</option>
										<option value="FACTURADO">FACTURADO</option>
										<option value="DISPONIBLE">DISPONIBLE</option>
										<option value="NO DISPONIBLE">NO DISPONIBLE</option>
									</select>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Estado Cangoroo</label>
									<select class="form-control select2" name="estado" id="estado" tabindex="3" style="width: 100%;">
										<option value="">Todos</option>
										<option value="Cancelled">Cancelado</option>
										<option value="PendingCancellation">Pendiente de Cancelación</option>
										<option value="Rejected">Rechazado</option>
										<option value="Confirmed">Confirmado</option>
									</select>
								</div>
							</div>
							{{-- @if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1)
								<div class="col-12 col-sm-6 col-md-3">  
									<div class="form-group">
										<label>Fecha Desde/Hasta Pago Cangoroo</label>						 
										<div class="input-group">
											<div class="input-group-prepend" style="">
												<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" class="form-control pull-right fecha" name="pago_desde_hasta" tabindex="7" id="pago_desde_hasta" value="">
										</div>
									</div>	
								</div> 	
							@endif --}}
								
							   
							<div class="col-12" style="margin-top: 20px;padding-left: 30px;">
								<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white" style="margin: 0 0 10px 10px;"><b>Limpiar</b></button>
								<a  id="btnBuscar" class="pull-right btn btn-info btn-lg" style="margin: 0 5px; color: #fff;" role="button" tabindex="8"><b>Buscar</b></a>
								<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
							</div>
								
							<input type="hidden" name="format" value="ajax" id="format_form">
            	  </form>
            	<div class="table-responsive table-bordered">
	              <table id="listado" class="table" style="width: 100%;">
	                <thead>
					  	<tr>
							<th>Booking ID</th>
							<th>Servicio ID</th>
							<th>Codigo</th>
							<th>Proforma</th>
							<th>Estado <br> Gestur</th>
							<th>Estado <br> Cangoroo</th>
							<th>Producto</th>
							<th>Proveedor</th>
							<th>Moneda C</th>
							<th>Costo</th>
							<th>Moneda V</th>
							<th>Venta</th>
							<th>Cliente</th>
							<th>Vendedor</th>
							<th>Usuario</th>
							<th>Check-in</th>
							<th>Pasajero</th>
							{{-- <th>
								@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1)
										Fecha Pago Cangoroo
								@endif
							</th> --}}
							<th></th>
		              	</tr>
	                </thead>
	               

			        <tbody style="text-align: center;">
	 					            
			         
			        </tbody>
	              </table>
	            </div>  
            </div>
        </div>    	
    </div>    
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>

	<script>

	$(function() {

	$('.select2').select2();

	$('.selectCliente').select2({
		minimumInputLength: 2
		 // language: "es"
	});

	var tipo  = '{{$id_tipo}}';

	if(tipo == ""){
		$('#id_tipo').val('');
	}else{ 
		$('#estado').val('').trigger('change.select2');
		if(tipo == 'enproforma'){
			$('#proforma_estado').val('EN PROFORMA').trigger('change.select2');;
		} else {
			$('#proforma_estado').val("DISPONIBLE").trigger('change.select2');
		}
		buscarProforma();
	}
	var Fecha = new Date();
	var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

	$('input[name="periodo_in"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    },
												startDate: "01/"+fechaInicial,
        										endDate: new Date()
								    			});
	$('input[name="periodo_in"]').val('');
	$('input[name="periodo_in"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

	$('input[name="pago_desde_hasta"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			});

	$('input[name="pago_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

	$('#pago_desde_hasta').val('');

	$('input[name="periodo_out"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			});

	$('input[name="periodo_out"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

	$('#periodo_out').val('');
			  
	// $('#proforma_estado').val("no").trigger('change.select2');

	// buscarProforma();

	});


	function limpiar(){

		$('#cliente_id').val('').trigger('change.select2');
		$('#id_vendedor_empresa').val('').trigger('change.select2');
		$('#vendedor_id').val('').trigger('change.select2');
		$('#nombrePasajero');	
		$('#cod_confirmacion');	
		$('#periodo_in').val('');
		$('#periodo_out').val('');

		// buscarProforma();

	}



	/*
	Para formatear numeros a moneda USD PY
	 */
	const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD',
					  minimumFractionDigits: 2
					});
	

	$('#btnBuscar').on('click',function(){
		buscarProforma();
	});

	function buscarProforma(){

		$('#format_form').val('ajax');
		$.blockUI({
				centerY: 0,
				message: "<h2>Procesando...</h2>",
				css: {
				        color: '#000'
				    }
				 });

		//RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){
			 table =   $('#listado').DataTable({
					        "destroy": true,
					        "select": true,
					        "ajax": {
					        		data: $('#frmSucConsulta').serializeJSON(),
						            url: "{{route('buscarReservaCangoroo.ajax')}}",
						            type: "GET",
						    error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        }
                        		// formatter.format(parseFloat(x.total_venta_proforma));
						        },"columns":[
											  {"data" : 'bookingid'},
											  {"data" : 'serviceid'},
											  {"data": "code_supplier"},
						        			  {"data": function(x){ 
												 
												if(x.id_proforma === null){
													var btn = `<a style="color: #009c9f;">0</a>`;
												}else{ 
													var btn = `<a target="_blank" href="{{ route('detallesProforma',['id'=>'']) }}/${x.id_proforma}"  >${x.id_proforma}</a>`;
												}
													 return btn;
											 }},
											 {"data": "estado_gestur"},		
						        			 {"data": "status"},				
						        			 {"data": "tipo_reserva"},
											 {"data": function(x){
												if(!x.proveedor){
													return 'N/A ('+x.proveedor_name_cangoroo+'-'+x.proveedor_id_cangoroo+')';
												}
												return x.proveedor;
											 }},

						        			 {"data": "moneda_costo"},
						        			 {"data": function(x){ 
						        			 			return formatter.format(parseFloat(x.precio_costo));}},
											 {"data": "moneda_venta"},
						        			 {"data": function(x){ 
						        			 			return formatter.format(parseFloat(x.precio_venta));}},
						        			 {"data": function(x){
												if(!x.client_name){
													return 'N/A ('+x.clientname+'-'+x.cliente_id_cangoroo+')';
												}
												return x.client_name;
											 }},
											 {"data": function(x){
												if(!x.usuario){
													return 'N/A ('+x.creationuserdetail_name+'-'+x.creationuserdetail_id+')';
												}
												return x.usuario;
											 }},
						        			 {"data": function(x){
												if(!x.id_vendedor_empresa){
													return 'N/A ('+x.responsible_operator_name+'-'+x.responsible_operator_id+')';
												}
												return x.vendedor_empresa;
											 }},
						        			 {"data": "booking_checkin_f"},
						        			 {"data": "pasajero_n"},
											// {"data": "fecha_notificado_nemo_format"},
											// {"data": function(x){
											// 		if(x.id_proforma === null && x.id_proveedor !=  0 && x.estado_id != 1 && x.agencia_id !=  null && x.vendedor_id !=  null){
											// 			btnAsignar = "<a onclick='asignarProforma("+x.id+","+x.id_producto+","+x.cliente_id+")' id="+x.id+" class='btn btn-info' style='margin-left:5px;padding-left: 6px;padding-right: 6px; color:#fff;' role='button'><i class='fa fa-edit'></i></a>";
											// 		}else{
											// 			btnAsignar = "";
											// 		}	
											// 		return btnAsignar; 

											// }
										// },

						        		],
										"aaSorting":[[0,"desc"]],
						        		"createdRow": function ( row, data, index ) {
						        					$(row).addClass('negrita');
						        				}		
						                    
					    }).on('xhr.dt', function ( e, settings, json, xhr ){
								//Ajax event - fired when an Ajax request is completed.;
							 $.unblockUI();
						});
			}, 300);

	}//function

	function asignarProforma(idReserva,idProducto,idCliente){
		$("#id_reserva").val(idReserva);
		$("#producto").val(idProducto);
		$("#cliente").val(idCliente);
		$.ajax({
				type: "GET",
				url: "{{route('ajaxProforma')}}",
				dataType: 'json',
				success: function(rsp){
						console.log(rsp);
									var oSettings = $('#listadoTickets').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();
									for (i=0;i<=iTotalRecords;i++) {
										$('#listadoTickets').dataTable().fnDeleteRow(0,null,true);
									}
									$.each(rsp, function (key, item){
										if(jQuery.isEmptyObject(item.fecha_emision_formateo) == false){
											fechaEmision = item.fecha_emision_formateo;
										}else{
											fechaEmision ="";
										}

										if(jQuery.isEmptyObject(item.nro_factura) == false){
											nroFactura = item.nro_factura;
										}else{
											nroFactura ="";
										}

										if(jQuery.isEmptyObject(item.fecha_alta_format) == false){
											fechaAlta = item.fecha_alta_format;
										}else{
											fechaAlta ="";
										}

										if(jQuery.isEmptyObject(item.check_in_format) == false){
											checkIn = item.check_in_format;
										}else{
											checkIn ="";
										}


										var accion = '<a onclick="asignarProformaReservas('+item.id+')" role="button" class="btn btn-info" style="margin-left:5px;color:#fff;padding-left: 6px;padding-right: 6px;"><i class="fa fa-edit"></i></a>';
										var dataTableRow = [
														accion,
														item.id,
														'<b>'+item.denominacion+'</b>',
														nroFactura,
														fechaAlta,
														checkIn,
														'<b>'+item.cliente_n+ '</b>',
														item.vendedor_n,
														item.pasajero_n,
														item.operativo_n,
														item.usuario_n
															];

										var newrow = $('#listadoTickets').dataTable().fnAddData(dataTableRow);
										// set class attribute for the newly added row 
										var nTr = $('#listadoTickets').dataTable().fnSettings().aoData[newrow[0]].nTr;
										// and parse the row:
										var nTds = $('td', nTr);

						})
						$("#requestModal").modal("show");

				}

			})	 
		}	


		function asignarProformaReservas(idProforma){
			return swal({
				title: "GESTUR",
				text: '¿Está seguro de que desea asignar este servicio a la proforma?',
				showCancelButton: true,
				buttons: {
						cancel: {
								text: "No",
								value: null,
								visible: true,
								className: "btn-warning",
								closeModal: false,
						},
						confirm: {
								text: "Sí, solicitar",
								value: true,
								visible: true,
								className: "",
								closeModal: false
							}
						}
					}).then(isConfirm => {
						if (isConfirm) {
								dataString = {
									idProforma : idProforma,
									idProducto : $("#producto").val(),
									idReserva : $("#id_reserva").val(),
									idDestinos : $("#destino_id").val(),
								}
								$.ajax({
										type: "GET",
										url: "{{route('asignarProformaReserva')}}",
										dataType: 'json',
										data: dataString,
										success: function(rsp){	
										console.log(rsp);	
											if(rsp[0].p_cod_retorno_out == 'OK'){
												var descrpcion = 'Proforma N°: '+rsp[0].proforma;
												if(rsp[0].p_item_detalle_numero_out != 0){	
													descrpcion += '  Item N°: '+rsp[0].item;
												}	
												swal("Exito", descrpcion, "success");
												if(idProforma == 0){
														$.toast({
															heading: 'Exito',
															text: 'Se ha generado, la proforma ',
															position: 'top-right',
															showHideTransition: 'slide',
															icon: 'success'
														});  
												}else{
													$.toast({
															heading: 'Exito',
															text: descrpcion,
															position: 'top-right',
															showHideTransition: 'slide',
															icon: 'success'
														});  
												}

												$("#requestModal").modal('hide');
												$("#modalDestino").modal('hide');
												buscarProforma();
											}else{
												swal("Cancelado", rsp[0].p_cod_retorno_out, "error");											}	
										}
									})	
						} else {
							swal("Cancelado", "", "error");
						}
				});
			}	
			function modalDestinos(){
			 	if($("#cliente").val() !== ""){
					$("#destino_id").select2({
								ajax: {
										url: "{{route('destinoProforma')}}",
										dataType: 'json',
										placeholder: "Seleccione un Proforma",
										delay: 0,
										data: function (params) {
											return {
												q: params.term, // search term
												page: params.page
											};
										},
										processResults: function (data, params){
											var results = $.map(data, function (value, key) {
												return {
													children: $.map(value, function (v) {
														return {
															id: key,
															text: v
														};
													})
												};
											});
											return {
												results: results,
											};
										},
										cache: true
									},
									escapeMarkup: function (markup) {
										return markup;
									}, // let our custom formatter work
									minimumInputLength: 3,
								});

					$("#modalDestino").modal("show");
				}else{
					$.toast({
							heading: 'Error',
							text: 'La reserva no posee Agencia.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
						});

				}	 
			}



			$("#botonExcel").on("click", function(e){ 
				
                e.preventDefault();
				$('#format_form').val('excel');
                $('#frmSucConsulta').attr('method','GET');
                $('#frmSucConsulta').attr('action', "{{route('buscarReservaCangoroo.ajax')}}").submit();
            });

	</script>
@endsection