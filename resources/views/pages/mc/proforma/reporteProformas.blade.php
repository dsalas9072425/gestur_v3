@extends('masters')
@section('title', 'Reporte proformas')
@section('styles')
	@parent
	 <style type="text/css">
	 	.error{
	 		color:red;
	 	}
	 </style>
@endsection
@section('content')

	@include('flash::message')  

	<section class="base-style"> 
		<section class="card">
			<div class="card-header">
				<h4 class="card-title">Reporte de proformas</h4>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-content collapse show" aria-expanded="true">
				<div class="card-body pt-0">
					<form id="filtroProformas">
					<div class="row">
					    <div class="col-12 col-sm-3 col-md-3">
		 					<div class="form-group">
					            <label>Creacion proforma</label>			
					            	<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text"  class = "Requerido form-control input-sm" id="periodo" name="periodo" tabindex="10"/>
								</div>
							</div>	
						</div> 
						<div class="col-12 col-sm-3 col-md-3">  
		 					<div class="form-group">
								<label>Nro. Proforma</label>
								<input type="number" class="form-control" id="nro_proforma" name="numProforma" value="" >
							</div>	
						</div> 
						<div class="col-12 col-sm-3 col-md-3">  
		 			       <div class="form-group">
					            <label>Vendedores</label>
								<select class="form-control" name="vendedores"  id="vendedores" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
								</select>
					        </div>
						</div> 
						<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
						            <label>Estado</label>
						            <select class="form-control select2" name="id_estado" id="id_estado" style="width: 100%;" tabindex="8">
										<option value="">Todos</option>
											@foreach($estados as $estado)
												<option value="{{$estado->denominacion}}">{{$estado->denominacion}}</option>
											@endforeach
						            </select>
					        	</div>					
					    	</div>	
				    </div>  
					<div class="row">
					<div class="col-12 col-sm-3 col-md-3">  
		 			       <div class="form-group">
					            <label>Usuario modificacion</label>
								<select class="form-control" name="vendedores_modificacion"  id="vendedores_modificacion" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
								</select>
					        </div>
						</div> 
					</div>
				    <!-- row-->

					<!--row--> 	
					<div class="row">
							
						<div class="col-12">
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white mr-1"><b>Limpiar</b></button>
							<button  onclick ="mostarProformas()" class="pull-right btn btn-info btn-lg mr-1" type="button"><b>Buscar</b></button>

				    	</div>	
					</div>	
            	</form>
				<div class="table-responsive">
						    <table id="lista_proforma" class="table">
						        <thead>
									<tr style="background-color: #eceff1;">
										<th>Proforma</th>
										<th>Creado por</th>
										<th>Fecha de creacion proforma</th>
										<th>Fecha Modificacion</th>
										<th>Usuario Modificacion</th>
										<th>Estado Actual</th>
										<th>Dias desde la creacion</th>
									</tr>
						        </thead>
						    </table>    	
						</div>	
				</div>
			</div>
		</section>
	</section>
    <!-- /.content -->

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	

	<script type="text/javascript" src="{{asset('gestion/app-assets/js/parsley.js')}}"></script>
	<script>

	var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};
		$(document).ready(function() {
		$("#botonExcel").on("click", function(e){ 
			e.preventDefault();
				$('#filtroProformas').attr('method','post');
			$('#filtroProformas').attr('action', "{{route('generarExcelReporteProformasEstado')}}").submit();
		});
		$('.select2').select2({
			language: lang_es_select2
		});
		$('#id_estado').select2();
		
		
		$("#vendedores, #vendedores_modificacion").select2({
				language: lang_es_select2,
		        ajax: {
		                url: "{{route('get.personas')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });

		var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('input[name="periodo"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(7, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });

			mostarProformas();
		});

		function mostarProformas(){	
			$("#lista_proforma").dataTable({
				"searching": false,
				"processing": true,
				"serverSide": true,
				"destroy": true,
				"ajax": {
				"url": "{{route('verDatosProformas')}}",
			    "data": {
			         "formSearch": $('#filtroProformas').serializeArray() }
			     },
				 "columns":[
									{data: "id_proforma"},
									{data: "nombre",
										render: function (data, type, row) {
											return(data+' '+row.apellido);
										}},
									{data: "creacion_proforma",
										render: function (data, type, row) {
											const fechaHora = new Date(data);
											const dia = fechaHora.getDate().toString().padStart(2, '0');
											const mes = (fechaHora.getMonth()+1).toString().padStart(2, '0');
											const anio = fechaHora.getFullYear();
											const fechaString = dia + '/' + mes + '/' + anio; // "12-03-2019"
											return (fechaString);
										}
									},
									{data: "fecha"},
									{data: "usuario_n"},
									{data: "denominacion"},
									{data: "fecha", "orderable": false,
										render: function (data, type, row) {
											const fechaInicio = new Date(row.creacion_proforma);
											
											const partesFecha = data.split('/');
											const fecha = new Date(partesFecha[2], partesFecha[1]-1, partesFecha[0]);
											const fechaISO = fecha.toISOString().substring(0, 10); // "2023-01-03"

											const fechaFin = new Date(fechaISO);
											const unDiaEnMilisegundos = 1000 * 60 * 60 * 24; // 86400000 milisegundos en un día
											const diferenciaEnMilisegundos = fechaFin.getTime() - fechaInicio.getTime();
											const diferenciaEnDias = Math.floor(diferenciaEnMilisegundos / unDiaEnMilisegundos);
											return (diferenciaEnDias);
										}
									}
								]
				});

		}

		function limpiar(){
			$('#vendedores').val('').trigger('change.select2');
			$('#vendedores_modificacion').val('').trigger('change.select2');
			$('#nro_proforma').val('');
			$('#id_estado').val('').trigger('change.select2');;
			mostarProformas();
		}
	</script>
@endsection
