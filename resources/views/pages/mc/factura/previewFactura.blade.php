<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Factura</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			/*width: 76%;*/
			width: 90%;
			margin:0 auto;
		    /*position:absolute;*/
    		z-index:1;
	}
	
	table{
		width: 100%;
		/*border:1px solid;*/

	}
	
	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 700;
	}

	.f-10 {
		font-size: 10px !important;
	}

	.f-9 {
		font-size: 9px !important;
	}

	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}
	.c-text {
		text-align: center;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 20px;
	}

	.f-8 {
		font-size: 5px !important;
	}

	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:120px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}



	</style>
</head>
<body>



@php	

 	function formatoFecha($date){
		if( $date != ''){

		$date = explode('-', $date);
		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
		 	return $fecha;
		} else {
			return 'N/A';
		}
	}


	function formatoFechaSinHora($date){
		if( $date != ''){

		$date = explode(' ', $date);
		$date = trim($date[0]);
		$date = explode('-', $date);

		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
		 	return $fecha;
		} else {
			return 'N/A';
		}
	}


	if (!function_exists('formatMoney')){
		function formatMoney($num,$currency){
			// dd($f);
			if($currency != 111){
			return number_format($num, 2, ",", ".");	
			} 
			return number_format($num,0,",",".");

		}
	}


	$factClase = '';
	$contadorDetalles = 0;
	$maxDetalles = 15;
	$contadorRecorrido = 0;
	$m ="";
	$margin = 300;
	$exentaTotal = '';
	$iva5Total = '';
	$iva10Total = '';
	$total = '';
	$moneda = '';




	$recorrido = "COPIA PREVIEW";
	$m = "PROFORMA - PREVIEW SIN NINGUN VALOR COMERCIAL, CONTABLE O ADMINISTRATIVO";
	$corte = 1;
	$margin = 100;




	
	

	/*
	 * =================================================================
	 * 				VALORES DE CABECERA DATOS DE FACTURA
	 * ================================================================= 
	 */
	


	$denominacionEmpresa = strtoupper($empresa->denominacion); 
	$vendedorDtp = $proformas[0]->usuario['nombre'];
	if(isset($proformas[0]->vendedor['nombre'])){
		$vendedorAgencia  = $proformas[0]->vendedor['nombre'];
	}else{
		$vendedorAgencia  = '';
	}
	$fechaEmisionFactura = date('m/d/Y');
	$fechaVencimientoFactura = formatoFecha($proformas[0]->vencimiento);
	$nombreRazonSocial = $proformas[0]->cliente['nombre'].' '.$proformas[0]->cliente['apellido'];
	$tipoFactura = $proformas[0]->tipoFacturacion['denominacion'];
	$rucCliente = $proformas[0]->cliente['documento_identidad'];
	$direccionFactura = $proformas[0]->cliente['direccion'];
	$telefonoFactura = $proformas[0]->cliente['telefono'];

	$pasajeroFactura = $pasajero;
	$in = formatoFechaSinHora($proformas[0]->check_in);
	$out = formatoFecha($proformas[0]->check_out);
	$proforma =  $proformas[0]->id;
	$comentario_unico =  $proformas[0]->concepto_generico;

	$imprimir_precio_unitario_venta = $proformas[0]->imprimir_precio_unitario_venta;

	//CONFIG FACTURA
	if($imprimir_precio_unitario_venta != ""&& $indicadorProducto == 0){
		$mostrarGravada = true;
	}else{
		$mostrarGravada = $empresa->imprimir_detalle_factura;
	}

	$idMonedaVenta = $proformas[0]->id_moneda_venta;
	$pieFactura_txt = $empresa->pie_factura_txt;



	/**
	 * =================================================================
	 * 				PIE DE SERVICIOS Y PRODUCTOS
	 * =================================================================
	 */ 

	

	
	$letraTotal = $NumeroALetras;
	$liquidacionIva10 =  formatMoney($proformas[0]->total_iva,$proformas[0]->id_moneda_venta);
	$espacios = 0;







	/*==============================================================
						INICIO DE FACTURA
	 ==================================================================  */

	
@endphp





	

	
	- <div id="background">
  <p id="bg-text" style="margin-top: <?php echo $margin  ?>;"><?php echo $recorrido  ?></p>
	</div>  

<div class="container espacio-10">

	

						<!--==============================================================
									DETALLE CABECERA
	 					==================================================================  -->


	<table class="f-11" style="margin-top: 20px;">
		<tr>
			<td>
				<?php 
					echo 'Vendedor Empresa: '.$vendedorDtp; 	
					?>			
			</td>
			<td>
				Vendedor Agencia: <?php echo $vendedorAgencia; ?>
			</td>
		</tr>
	</table>


	<table class="f-11"  style="border:1px solid">

		<colgroup>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
	
		</colgroup>


		<tr>
			<td colspan="3">
				<b>Fecha de Emision:</b> <?php echo $fechaEmisionFactura; ?>
			</td>

			<td colspan="3">
				<b>Vencimiento:</b> <?php echo $fechaVencimientoFactura; ?>
			</td>

			<td colspan="3">
				<b>Tipo Factura:</b> <?php echo $tipoFactura; ?>
			</td>
		</tr>


		<tr>
			<td colspan="6">
				<b>Nombre / Razon Social:</b> <?php echo $nombreRazonSocial; ?>
			</td>

			<td colspan="3">
				<b>RUC:</b> <?php echo $rucCliente; ?>
			</td>

		</tr>


		<tr>
			<td colspan="7">
				<b>Dirección:</b> <?php echo $direccionFactura; ?>
			</td>

			<td colspan="3">
				<b>Telefono:</b> <?php echo $telefonoFactura; ?>
			</td>

		</tr>

		<tr>
			<td colspan="4">
				<b>Pasajero:</b> <?php echo $pasajeroFactura;?>
			</td>

			<td colspan="2">
				<b>IN:</b> <?php echo $in;?>
			</td>

			<td colspan="2">
				<b>OUT:</b> <?php echo $out;?>
			</td>

			<td colspan="2">
				<b>Proforma N.:</b> <?php echo $proforma;?>
			</td>
		</tr>

	</table>


	<table style="margin-top: 10px;">

		<colgroup>
			<col style="width: 50%"/>
			<col style="width: 50%"/>
		</colgroup>
		<tr>
			<td>
				Detalles
			</td>
			<td>
				Valor de Ventas
			</td>
		</tr>

	</table>

	<table class="c-text b-col" border="1">

	<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>


	
		</colgroup>


		<tr>
			<td width="5%">
				Cod
			</td>

			<td width="5%">
				Cant
			</td>
			<td width="40%">
				Descripción Producto
			</td>

			<td width="10%">
				Exentos
			</td>

			<td width="10%">
				Iva 5%
			</td>

			<td width="10%">
				Iva 10%
			</td>
		</tr>

		</table>
			
		<table class=""  FRAME="vsides" RULES="cols" style="border-bottom: 1px solid; ">

		<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
		</colgroup>

		

		{{-- ===================================
			AGREGAR SERVICIOS Y ESPACIOS
		=================================== --}}
		@if($comentario_unico == "")
			@php
				if(!isset($proformasDetalle[0]->id_producto)){
					$exentaTotal = 0;
					$iva5Total =  0;
					$iva10Total = 0;
				}
			@endphp	
			@foreach($proformasDetalle as $detalles)
				@php

				//Si es de tipo Ticket agregar codigo confirmacion
				$descripcion = $detalles->descripcion;
				if(isset($detalles->producto->id)){
					/*if($detalles->producto->id == 9  || 
						$detalles->producto->id == 31 ||
						$detalles->producto->id == 29 ||
						$detalles->producto->id == 32 ||
						$detalles->producto->id == 31 ||
						$detalles->producto->id == 1){*/
						$descripcion .= ' ('. $detalles->cod_confirmacion.' )';
						$longitud = strlen($descripcion);
						$desc = wordwrap($descripcion,50,'<br />',true);
					//}
				}else{
					$desc = wordwrap($descripcion,50,'<br />',true);
				}
				if(isset($detalles->id_producto)){
					$imprimir_en_factura =  $detalles->producto['imprimir_en_factura'];
					
				}else{
					$imprimir_en_factura = 1;
					$mostrarGravada = true;
					$imprimir_precio_unitario_venta = 1;
					$exentaTotal = (float)$exentaTotal + (float)$detalles->exento;
					$iva5Total =  (float)$detalles->gravadas_5;
					$iva10Total =  (float)$detalles->gravadas_10;

				}

				@endphp
				{{--$detalles->id_producto--}}
				{{-- VALIDAR SI EL PRODUCTO SE PUEDE IMPRIMIR EN FACTURA --}}
				@if($contadorDetalles <= 15 &&  $imprimir_en_factura == 1)
				
						<tr valign="top" class="f-10" nowrap>

							<td class="c-text" width="5%">
								<?php
									if(isset($detalles->id_producto)){
										echo $detalles->id_producto;										
									}else{
										echo '-';
									}
								?>
							
							</td>

							<td class="c-text" width="5%">
								{{$detalles->cantidad}}
							</td>

							<td width="30%" >
								<?php echo $desc; ?>
							</td>
							<td class="r-text" width="10%">
								<div class="r-text-detalle"><?php if($mostrarGravada){ 
																	if($imprimir_precio_unitario_venta != ""&& $indicadorProducto == 0){
																		echo ($detalles->precio_venta != null) ? formatMoney($detalles->precio_venta,$idMonedaVenta): ''; 
																	}	
																} ?><div>
							</td>

							<td class="r-text" width="10%">
								<div class="r-text-detalle"><?php if($mostrarGravada){ 
																			echo ($detalles->exento != null) ? formatMoney($detalles->exento,$idMonedaVenta): ''; 
																		} ?><div>
							</td>

							<td class="c-text" width="10%">
								<div class="r-text-detalle"><?php if($mostrarGravada){ 
																				echo ($detalles->gravadas_5 != null) ? formatMoney($detalles->gravadas_5,$idMonedaVenta): ''; 
																	} ?><div>
							</td>

							<td class="r-text" width="10%">
								<div class="r-text-detalle"><?php if($mostrarGravada){ 
																				echo ($detalles->gravadas_10 != null) ? formatMoney($detalles->gravadas_10,$idMonedaVenta): ''; 
														} ?><div>
							</td>


						</tr>

					<?php
					$contadorDetalles++;
					?>

				@endif

				@endforeach
			@else 
			<?php //echo '-----------------------------------------------------------------------'; ?>
				<tr valign="top" class="f-10" nowrap>
					<td class="c-text" width="5%">
						-
					</td>
					<td class="c-text" width="5%">
						1
					</td>
					<td width="30%" >
						<?php echo $comentario_unico; ?>
					</td>
					<td class="r-text" width="10%">
						<div class="r-text-detalle"><?php if($mostrarGravada){ 
															if($indicadorProducto == 0){ 
																echo $precioUTotal; 
															}	
														}  ?><div>
					</td>

					<td class="r-text" width="10%">
						<div class="r-text-detalle"><?php if($mostrarGravada){ echo $exentaTotal; } ?><div>
					</td>

					<td class="c-text" width="10%">
						<div class="r-text-detalle"><?php if($mostrarGravada){ echo $iva5Total; } ?><div>
					</td>

					<td class="r-text" width="10%">
						<div class="r-text-detalle"><?php if($mostrarGravada){ echo $iva10Total; } ?><div>
					</td>


					</tr>

					<?php
					$contadorDetalles++;
					?>

				<?php //echo '-----------------------------------------------------------------------'; ?>

			@endif

		{{-- AGREGAR ESPACIOS EN BLANCO PARA COMPLETAR 15 LINEAS EN TOTAL --}}
		<?php 
		if($contadorDetalles < $maxDetalles){
			$espacios = $maxDetalles - $contadorDetalles;
		}

		for ($i=0; $i < $espacios ; $i++):?>

			<tr valign="top" class="f-11">
		

			<td class="c-text" width="5%">
				&nbsp;
			</td>

			<td class="c-text" width="5%">
				
			</td>

			<td width="30%">
				
			</td>
			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>


		</tr>
			
		
	<?php endfor;
		if($exentaTotal == 0){
			$exentaTotal = $proformas[0]->total_exentas;
		}
		if($iva5Total == 0){
			$iva5Total = 0;
		}
		if($iva10Total == 0){
			$iva10Total = $proformas[0]->total_gravadas;
		}

	?>
		
		
		<tr valign="top" class="f-12">
			<td class="c-text" colspan="6"> <?php echo $m; ?></td>
		</tr>
	
		<tr valign="top" class="f-10">
		

			<td class="c-text" width="5%">
				&nbsp;
			</td>

			<td class="c-text" width="5%">
				
			</td>

			<td width="30%">
				
			</td>
			<td class="r-text" width="10%">
			<span class="r-text-detalle"><?php  
												if($imprimir_precio_unitario_venta != ""&& $indicadorProducto == 0){
													if($comentario_unico != "" || $mostrarGravada == ""){
														echo $precioUTotal; 
													}
												}	
												?></span> 
			</td>
			<td class="r-text" width="10%">
				<span class="r-text-detalle"><?php echo '&nbsp;&nbsp;&nbsp;'.formatMoney($exentaTotal,$idMonedaVenta); ?></span>
			</td>

			<td class="r-text" width="10%">
				<span class="r-text-detalle"><?php echo '&nbsp;&nbsp;&nbsp;'.formatMoney($iva5Total,$idMonedaVenta); ?></span>
			</td>

			<td class="r-text" width="10%">
				<span class="r-text-detalle"><?php echo '&nbsp;&nbsp;&nbsp;'.formatMoney($iva10Total,$idMonedaVenta); ?></span>
			</td>

		</tr>
	
	</table>


	<table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid;border-spacing: 0px;">
		<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
		</colgroup>
		<tr>
			
			<td style="width: 70px; padding-top: 7px;" class="f-9">
				<b>SUB-TOTALES:</b>
			</td>

			<td class="c-text f-9" colspan="2" style="width: 283px; padding-top: 7px;">
					********** Esta Factura será cancelada en <?php echo $moneda;?> **********
			</td>
				
			<td style="width: 70px;border-left: 1px solid;border-right: 1px solid;padding: 0 0 0 0; padding-top: 7px;" class="c-text f-9">
				<?php
				echo formatMoney($exentaTotal,$idMonedaVenta); ?>
			</td>

			<td style="width: 70px;border-right: 1px solid;padding: 0 0 0 0; padding-top: 7px;" class="c-text f-9">
				<?php echo formatMoney($iva5Total,$idMonedaVenta); ?>
			</td>
			</td>

			<td style="width: 70px;padding: 0 0 0 0; border-bottom: 1px solid #ffffff; padding-top: 7px;" class="c-text f-9">
				<?php echo formatMoney($iva10Total,$idMonedaVenta); ?>
			</td>
			</td>

		</tr>

	</table>

	<table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; border-spacing: 0px;">
		<tr>
			<td class="f-10" style="width:623px;border-bottom: 1px solid;padding: 0 0 0 0;">
				<b>TOTAL DE FACTURA:</b> &nbsp;&nbsp;son <?php echo $moneda."    ".$letraTotal;?> 
			</td>
			<td class="f-10" rowspan="3" style="border-top: 1px solid #ffffff;padding-left: 15%;border-spacing: 0px;border-left: 1px solid;">
				<span class="c-text f-12"><b><?php echo $totalFactura;?><b></span>
			</td>
		</tr>
		<tr>
			<td class="f-10" style="padding: 0 0 0 0;">
			</td>
		</tr>
	</table>


	<table   style="border-bottom: 1px solid;">
		<tr>
			<td class="f-12" >
				<b>LIQUIDACÍÓN IVA 10%:</b> <span> <?php echo $liquidacionIva10; ?></span>
			</td>	
			
			<td class="f-12">
				<b>LIQUIDACÍÓN IVA 5%:</b> <span> 0,00</span>
			</td>
			
			
			<td class="f-12">
				<b>TOTAL IVA:</b> <span> <?php echo $liquidacionIva10; ?></span>
			</td>
			
		</tr>
		
	</table>

	<table>

		<tr>
			<!--SI ES FCTURA BRUTA -->
			@if($proformas[0]->id_tipo_facturacion == 1)
			<td class="f-11">
				COM .AG : <?php echo formatMoney($proformas[0]->total_comision,$proformas[0]);?>
			</td>
			@endif
		</tr>
		
	</table>


	<table style="border:1px solid; margin-top: 5px;">
	<tr>
		<td class="f-8" style="padding: 5px;">
			{!! $pieFactura_txt !!}

		</td>
	</tr>
</table>

</div>
{{-- CONTAINER --}}


	{{--==============================================================
						FIN DE FACTURA
		==================================================================  --}}









	

</body>
</html>