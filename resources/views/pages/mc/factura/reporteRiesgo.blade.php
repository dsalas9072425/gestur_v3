
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

@endsection
@section('content')

<style type="text/css">
	

	.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

</style>

<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Reporte Riesgo Cliente</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	<form id="consultaRiesgo" style="margin-top: 2%;" method="get">
					<div class="row" >
			            <div class="col-md-3" style="">
					        <div class="form-group">
					            <label>Clientes</label>
								<select class="form-control select2" name="id_persona"  id="id_persona" style="width: 100%;">
									<option value="">Todos</option>
									@foreach ($persona as $p)
										<option value="{{$p->id}}">{{$p->nombre}} {{$p->nombre}} {{ $p->denominacion_comercial}}</option>
									@endforeach
								</select>
					        </div>
			            </div>
			            <div class="col-md-3" style="">
					        <div class="form-group">
					            <label>Tipo Persona</label>
								<select class="form-control select2" name="id_tipo_persona"  id="id_tipo_persona" style="width: 100%;">
									<option value="">Todos</option>
									@foreach ($tipoPersona as $tp)
										<option value="{{$tp->id}}">{{$tp->denominacion}}</option>
									@endforeach
								</select>
					        </div>
			            </div>
						<div class="col-md-3" style="padding-left: 15px;">
						 	<div class="form-group">
							 	 <label>RUC</label>
							     <input type="text" class="form-control" id="ruc"  name="ruc" value="">
							</div>
						</div>
   					    <div class="col-xs-12 col-sm-3 col-md-3" style="">
					        <div class="form-group">
					            <label>Saldo</label>
								<select class="form-control select2" name="id_estado_saldo"  id="id_estado_saldo"  style="width: 100%;">
									<option value="">Todos</option>
									<option value="1">Positivo</option>
									<option value="2">Negativo</option>
									<option value="0">Cero</option>
								</select>
					        </div>
			            </div>
					</div>
					<div class = "row">
						<div class="col-md-12" style="padding-right: 25px;">
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg" style="margin: 0 5px;"><b>Excel</b></button>
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white" style="margin: 0 5px;"><b>Limpiar</b></button>			
							<a  onclick ="buscar()" class="pull-right  btn btn-info btn-lg" style="margin: 0 5px; color:#fff;" role="button"><b>Buscar</b></a>
				 		</div>
					</div>	
            	</form>	
            	<br>
				<div class="table-responsive table-bordered">
		            <table id="listado" class="table" style="width: 100%;">
		                <thead>
							<tr>
							  	<th></th>
							  	<th>Cliente</th>
							  	<th>RUC</th>
							  	<th>Credito</th>
							  	<th>Vencidas</th>
								<th>A Vencer</th>
								<th>En Gastos</th>
								<th>Sin Gastos</th>
								<th>Saldo</th>
								<th>Estado</th>
				            </tr>
		                </thead>
		                <tbody style="text-align: center">				         
				        </tbody>
		            </table>
	            </div>
            </div>	
        </div>    
    </div>    
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
<script type="text/javascript">

	$(function() {

	$('.select2').select2();
	sincronizar();
	buscar();
	});



	function limpiar(){
		$('#id_persona').val('').trigger('change.select2');
		$('#id_tipo_persona').val('').trigger('change.select2');
		$('#id_estado_saldo').val('').trigger('change.select2');
		$('#ruc').val('');
		buscar();
	}

		function sincronizar(){
				$.blockUI({
				                        centerY: 0,
				                        message: "<h2>Sincronizando con el Sistema de Gestión...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });

		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){
		

		$.ajax({
							type: "GET",
							url: "{{route('doSincronizarSaldoG')}}",
							dataType: 'json',
							data: {
									data: 1,
					       			},
					       			 error: function(jqXHR,textStatus,errorThrown){

										$.toast({
											heading: 'Error',
											text: 'Ocurrio un error al sincronizar los datos con el Sistema de Gestión.',
											position: 'top-right',
											showHideTransition: 'fade',
											icon: 'error'
										});
                             $.unblockUI();

                        },
							success: function(rsp){
								  $.unblockUI();
								
							}	

					});

		}, 300);

	}


	
			
			const formatter = new Intl.NumberFormat('en-US', {
					  // style: 'currency',
					  currency: 'USD',
					  minimumFractionDigits: 2
					});


			$("#botonExcel").on("click", function(e){ 
			console.log('Inicil');
                e.preventDefault();
               $('#consultaRiesgo').attr('action', "{{route('generarExcelReporteRiesgo')}}").submit();
            });


		function buscar(){ 

			$("#listado").dataTable({
						 "destroy": true,
						"ajax": {
						"url": "{{route('ajaxReporteRiesgo')}}",
						 "type": "GET",
						 "data": {
							        "formSearch": $('#consultaRiesgo').serializeArray() },
						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
							    
					     },
					    

					      "columns": [			
					      						{ "data": "id_cliente"},
									            { "data": "nombre" },
									            
									            { "data": "documento_identidad" },
									            { "data": function(x){
									            	return formatter.format(parseFloat(x.credito));
									            } }, 
									            { "data": function(x){
									            	return formatter.format(parseFloat(x.vencidas));
									            } },
									            { "data":  function(x){
									            	return formatter.format(parseFloat(x.a_vencer));
									            } },
									            { "data": function(x){
									            	return formatter.format(parseFloat(x.en_gastos));
									            } },
									            { "data":  function(x){
									            	return formatter.format(parseFloat(x.sin_gastos));
									            } },
									            { "data": function(x){
									            	return formatter.format(parseFloat(x.saldo));
									            } }, 
									            { "data": function(x){
									            	let m , p = '';
									            		if(x.saldo > 0){
									            			m = `<i class='fa fa-thumbs-o-up icoVerde'></i>
									            			<div style="display: none;">0</div>`
									            		}else if( x.saldo == 0){
									            			m = `<i class='fa fa-exclamation-triangle icoAmarillo'></i>
									            			<div style="display: none;">1</div>`;
									            		}else {
									            			m = `<i class='fa fa-exclamation-triangle icoRojo'></i>
									            			<div style="display: none;">2</div>`;
									            		}	
									            		return m;
									            	// return formatter.format(x.saldo);
									            } }
									           
									        ], 
									         "columnDefs":[{"visible": false, "targets":0}],
									          "aaSorting":[[0,"desc"]],
					    "createdRow": function ( row, data, iDataIndex ) {
									
									 // console.log(data);
						         } 
					
				});	

		}
</script>

@endsection