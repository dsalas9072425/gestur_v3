
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

	
    .correcto_col { 
		height: 74px;
     }

</style>


<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Reporte Facturas</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
 
    		<form id="consultaFactura" method="post" >

			<div class="row"  style="margin-bottom: 1px;">

	            <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Cliente</label>
						<select class="form-control select2" name="cliente_id"  id="cliente" tabindex="1" style="width: 100%;">
							<option value="">Seleccione Cliente</option>
							@foreach($clienteFactura as $factura)
								@php
									$ruc = $factura->cliente['documento_identidad'];
									if($factura->cliente['dv']){
										$ruc = $ruc."-".$factura->cliente['dv'];
									}
								@endphp
								<option value="{{$factura->cliente['id']}}">{{$ruc}} - {{$factura->cliente['nombre']}}</option>
							@endforeach
						</select>
			        </div>
	            </div>


	            <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Moneda</label>
						<select class="form-control select2" name="moneda_id"  id="moneda" tabindex="1" style="width: 100%;">
							<option value="">Seleccione Moneda</option>
							@foreach($monedas as $moneda)
							<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
							@endforeach
						</select>
			        </div>
	            </div>




				<div class="col-12 col-sm-3 col-md-3">  
 					<div class="form-group">
			            <label>Facturación Desde - Hasta</label>						 
						<div class="input-group">
						    <div class="input-group-prepend" style="">
						        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
						    </div>
						    <input type="text" class="form-control pull-right fecha" name="facturacion_desde_hasta" id="periodo_fact" value="">
						</div>
					</div>	
				</div>


				<div class="col-12 col-sm-3 col-md-3">  
 					<div class="form-group">
			            <label>Vencimiento Desde - Hasta</label>						 
						<div class="input-group">
						    <div class="input-group-prepend" style="">
						        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
						    </div>
						    <input type="text" class="form-control pull-right fecha" name="vencimiento_desde_hasta" id="periodo_venc" value="">
						</div>
					</div>	
				</div> 

				


			<div class="col-12 col-sm-3 col-md-3">  
				<div class="form-group">
				   <label>Último Pago Desde - Hasta</label>						 
				   <div class="input-group">
					   <div class="input-group-prepend" style="">
						<span class="input-group-text"><i class="fa fa-calendar"></i></span>
					   </div>
					   <input type="text" class="form-control pull-right fecha" name="ultimo_pago_desde_hasta" id="periodo_ultimo" value="">
				   </div>
			   </div>	
		   </div> 


	            <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Tipo Factura</label>
						<select class="form-control select2" name="tipoFactura"  id="" tabindex="1" style="width: 100%;">
							<option value="">Seleccione Tipo</option>
							@foreach($tipoFactura as $tipo)
							<option value="{{$tipo->id}}">{{$tipo->denominacion}}</option>
							@endforeach
						</select>
			        </div>
	            </div>


	             <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Estado</label>
						<select class="form-control select2" name="tipoEstado"  id="" tabindex="1" style="width: 100%;">
							<option value="">Seleccione Estado</option>
							
							@foreach($tipoEstado as $estado)
							<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
							@endforeach
						</select>
			        </div>
	            </div>

	             

	            	<div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Vendedor</label>
						<select class="form-control select2" name="vendedor_id"  id="" tabindex="1" style="width: 100%;">
							<option value="">Seleccione Vendedor</option>
							@foreach($vendedorEmpresa as $vendedor)
								@if(isset($vendedor->vendedorEmpresa['id']))		
									<option value="{{$vendedor->vendedorEmpresa['id']}}">{{$vendedor->vendedorEmpresa['nombre']}} {{$vendedor->vendedorEmpresa['apellido']}}</option>
								@endif	
							@endforeach
						</select>
			        </div>
	            </div>

	          

	             	




					<div class="col-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Cobro</label>
								<select class="form-control select2" name="cobro_id"  id="cobro_id" tabindex="1" style="width: 100%;">
									<option value="">Seleccione Estado de Cobro</option>
									@foreach($tipoCobro as $cobro)
									<option value="{{$cobro->id}}">{{$cobro->denominacion}}</option>
									@endforeach
								</select>
							</div>
						</div>

		    	 <div class="col-6 col-sm-3 col-md-3">
				 <div class="form-group">
				 	 <label>Nro. Factura</label>
				     <input type="text" class="form-control" id=""  name="numFactura" value="">
					</div>
				</div>

				 <div class="col-6 col-sm-3 col-md-3">
				 <div class="form-group">
				 	 <label>Nro. Proforma</label>
				     <input type="text" class="form-control" id="" name="numProforma" value="" >
					</div>
				</div>


		   


				<div class="col-12">
					<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg" ><b>Excel</b></button>
					<button type="button" onclick ="consultaFactura()" class="pull-right text-center btn btn-info btn-lg mr-1"><b>Buscar</b></button>
		  		</div>
		  				
				</div>
				<!--row--> 



   
            	<div class="table-responsive table-bordered mt-1">
	              <table id="listado" class="table">
	                <thead>
					  <tr>
					  	<th>Saldo</th>
					  	<th>Vencimiento</th>
						<th>Factura</th>
						<th>Proforma</th>
						<th>Estado</th>
						<th>Cobro</th>
						<th>Monto</th>
						<th>Moneda</th>
						<th>Saldo</th>
						<th>Fecha Fact.</th>
						<th>Fecha Venc.</th>
						<th>Fecha Ult. Pago</th>
						<th>Cliente</th>

		              </tr>
	                </thead>
	                <tbody id="lista_cotizacion" style="text-align: center">
	 					
				            @foreach($facturas as $factura)
					             <tr>
					                <td><i class="{{$factura->icoSaldo}}"></i><div style="display: none;">{{$factura->pesoIcoSaldo}}</div></td>
					                <td><i class="{{$factura->icoVenc}}"></i><div style="display: none;">{{$factura->pesoIcoVenc}}</div></td>
					                @if(isset($factura->id_venta_rapida))
					                	 <td><a href="{{route('verVenta',['id'=>''])}}/{{$factura->id_venta_rapida}}" style=" font-weight: bold; color:#111;" class="bgRed">
		                    					<i class="fa fa-fw fa-search"></i>{{$factura->nro_factura}}</a><div style="display:none;">{{$factura->id}}</div>
		                    			</td>
					                @else
						                <td><a href="{{route('verFactura',['id'=>''])}}/{{$factura->id}}" style=" font-weight: bold; color:#111;" class="bgRed">
		                    					<i class="fa fa-fw fa-search"></i>{{$factura->nro_factura}}</a><div style="display:none;">{{$factura->id}}</div>
		                    			</td>
		                    		@endif
					                <td>{{$factura->id_proforma}}</td>
					                <td>{{$factura->estado['denominacion']}}</td>
					                <td>{{$factura->estadoCobro['denominacion']}}</td>
					                <td>{{$factura->monto}}</td>
					                <td>{{$factura->currency['currency_code']}}</td>
					                <td>{{$factura->saldo_factura}}</td>
					                <td>{{$factura->fecha_hora_facturacion}}</td>
									<td>{{$factura->vencimiento}}</td>
									<td>{{$factura->fecha_ultimo_pago}}</td>
					                <td>{{$factura->cliente['nombre']}}</td>
					            </tr>
				            @endforeach
			        </tbody>
	              </table>
	            </div>  
           

			</div>
		</div>
	</section>
</section>



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
<script type="text/javascript">


//CALENDARIO

$(()=>{
	calendar();
	$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
	$('.select2').select2();

});

function calendar(){
	{{--CALENDARIO OPCIONES EN ESPAÑOL --}}
	


	let locale = {
				format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};
	let conf = {timePicker24Hour: true,
				timePickerIncrement: 30,
				locale: locale};		


	let calendar_01 = $('#periodo_fact').daterangepicker(conf);
	let calendar_02 = $('#periodo_venc').daterangepicker(conf);
	let calendar_03 = $('#periodo_ultimo').daterangepicker(conf);
												
	$('#periodo_fact').val('');
	calendar_01.on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
	$('#periodo_venc').val('');
	calendar_02.on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

	$('#periodo_ultimo').val('');
	calendar_03.on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });


	 

	 }



	

		
	


			function validar(data){

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}



			function consultaFactura() {

				 $.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });


			var dataString = $('#consultaFactura').serialize();
			//console.log( $('#consultaFactura').serializeArray());
				$.ajax({
						type: "GET",
						url: "{{route('reporteFactura')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });
                           $.unblockUI();

                            },

						success: function(rsp){
							$.unblockUI();

							// console.log(rsp.response);



							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

							$.each(rsp.response, function (key, item){

							
			     			console.log(item);

								if(validar(item)){

						var icoSaldo = '<i class="'+item.icoSaldo+'"></i><div style="display: none;">'+item.pesoIcoSaldo+'</div>';
						var icoVenc = '<i class="'+item.icoVenc+'"></i><div style="display: none;">'+item.pesoIcoVenc+'</div>';		

        	     		btn = ``;
			     		if(item.id > 89999)
			     		{
							if(Boolean(item.id_venta_rapida))
							{
								btn = `<a href="{{route('verVenta',['id'=>''])}}/${item.id_venta_rapida}" style="font-weight: bold; color:#111;" class="bgRed">
                    			<i class="fa fa-fw fa-search"></i>${item.nro_factura}</a><div style="display:none;">${item.id}</div>`;		
							}
							else
							{
			     				btn = `<a href="{{route('verFactura',['id'=>''])}}/${item.id}" style="font-weight: bold; color:#111;" class="bgRed">
                    			<i class="fa fa-fw fa-search"></i>${item.nro_factura}</a><div style="display:none;">${item.id}</div>`;		
							}

			     		} 
			     		else 
			     		{
			     			btn = `<a  class="bgRed">${item.nro_factura}</a><div style="display:none;">${item.id}</div>`;
			     		}

						 //console.log(item);
						var dataTableRow = [

						icoSaldo,
						icoVenc,
						btn,
						item.id_proforma,
						(validar(item.estado)) ? item.estado.denominacion : '',
						(validar(item.estado_cobro)) ? item.estado_cobro.denominacion : '',
						(validar(item.monto)) ? item.monto : '',
						(validar(item.currency)) ? item.currency.currency_code: '',
						item.saldo_factura,
						(validar(item.fecha_hora_facturacion)) ? item.fecha_hora_facturacion : '',
						(validar(item.vencimiento)) ? item.vencimiento : '',
						(validar(item.fecha_ultimo_pago)) ? item.fecha_ultimo_pago : '',
						(validar(item.cliente)) ? item.cliente.nombre : ''


											];

						var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
						var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
									}

							})

					

							
						
					


						}//cierreFunc	
				});//Cierre ajax

			
			
		}

		$("#botonExcel").on("click", function(e){ 
			console.log('Inicil');
                e.preventDefault();
               $('#consultaFactura').attr('action', "{{route('generarExcelFactura')}}").submit();
            });

</script>

@endsection