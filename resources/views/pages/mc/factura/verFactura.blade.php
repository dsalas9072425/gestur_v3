@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	 <style type="text/css">
	 	.error{ 
	 		color:red;
	 	}

	.anularStyle {
		padding:5px 40px; 
		margin-left: 5px;
	}

	.notaCreditoStyle{
		padding:5px 40px; 
		margin-left: 5px; 
		background-color: #F4A460 !important; 
		border-color: #F4A460 !important
	}
	.imprimirFactStyle{
		padding:5px 40px; 
		margin-left: 5px; 
		background:#E5097F !important;
	}

	.editarVoucherStyle{
		margin-left: 5px; 
		padding:5px 40px;
	}

	input{
		font-weight: bold !important;
		/* font-size: 18px; */
	}

	.form-button-cabecera{
			margin-bottom: 5px !important;
		}
	/* .input-sm{
		font-size: 15px !important;
	}		 */

		.select2-selection {
	    	max-height: 30px !important;
		}

		.select2-selection--single{
			padding-top: 0px !important;	
		}

	 </style>
@endsection
@section('content')
@php
        function formatMoney($num,$currency){
            // dd($f);
            if($currency != 111){
            return number_format($num, 2, ",", ".");	 
            } 
            return number_format($num,0,",",".");

        }
@endphp

<section id="base-style">
  @include('flash::message') 
    @if(!empty($facturas)) 
		@foreach($facturas as $key=>$factura)
		  <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Factura</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
					<div class="row">	
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Nro Factura</label>
								<input type="text" style="color:red" class = "form-control form-control-sm form-control form-control-sm-sm"  value="{{$factura->nro_factura}}" readonly/>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Cliente</label>
								<?php
									$documento = $factura->documento_cliente;
									if($factura->dv_cliente !== ''){
										$documento =$documento.'-'.$factura->dv_cliente; 
									}
								?>
								<input type="text" class = "form-control form-control-sm input-sm"  value="{{$documento}} - {{$factura->cliente_nombre}} {{$factura->cliente_apellido}} - {{$factura->cliente_id}}" readonly/>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Vendedor</label>
								@if(isset($factura->vendedor_nombre))	
									<input type="text" class = " form-control form-control-sm input-sm"  value="{{$factura->vendedor_nombre}}" readonly/>
								@else
									<input type="text" class = "form-control form-control-sm input-sm"  value="" readonly/>
								@endif	
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Pasajero Principal</label>
								<input type="text" class = "form-control form-control-sm input-sm"  value="@if(isset($pasajeroNombre)){{$pasajeroNombre}} @endif" readonly="readonly"/>
								<div id="botonesModal" style='display: none'> 
									<a data-toggle="modal" href="#requestModalPasajero" class="btn-mas-request">
										<i class="fa fa-plus fa-lg"></i>
									</a>
								</div>
							</div>
						</div>
					</div>	
					<div class="row">
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Destino</label>
								<input type="text" class="form-control form-control-sm input-sm"  value="{{$factura->destino}}" readonly/>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Check In - Check Out</label>
								<?php 
									$fecha_periodo1 = date('d/m/Y', strtotime($factura->check_in)); 
									$fecha_periodo2 = date('d/m/Y', strtotime($factura->check_out)); 
									$periodo = $fecha_periodo1." - ".$fecha_periodo2;
								?>
								<input type="text"  class="form-control form-control-sm input-sm" id="periodo" value="{{$periodo}}" name="periodo" disabled="disabled"/>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Moneda de Facturación</label>
								<input type="text" class = "form-control form-control-sm input-sm"  value="{{$factura->moneda_venta}}" readonly/>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Cotización</label>
								<input type="text" readonly class = "form-control form-control-sm input-sm" value="{{$factura->cotizacion_factura}}"/>
							</div>
						</div>	
					</div>	
					<div class="row">
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Monto Seña</label>
								<input type="text" onkeypress="return justNumbers(event);" class = "form-control form-control-sm input-sm" value="{{$factura->senha}}" disabled="disabled"/>
							</div>
						</div>
			            <div class="col-12 col-md-3">
							<div class="form-group">
								<label>Vencimiento</label>
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									@if(date('d/m/Y', strtotime($factura->vencimiento)) !== '31/12/1969')
										<input type="text" value="{{date('d/m/Y', strtotime($factura->vencimiento))}}" class="form-control form-control-sm pull-right fecha input-sm" disabled="disabled">
									@else
										<input type="text" value="" class="form-control form-control-sm pull-right input-sm" name="vencimiento" id="vencimiento" disabled="disabled">
									@endif    
								</div>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Estado</label>
								@if($factura->id_estado_factura == "30")
									<input type="text" disabled="disabled" class = "form-control form-control-sm input-sm" name="proforma_id" value="{{$factura->estado_factura}}" readonly="readonly" style="font-weight: bold;font-size: 18px; color:red;"/>
								@else
									<input type="text" disabled="disabled" class = "form-control form-control-sm input-sm" name="proforma_id" value="{{$factura->estado_factura}}" readonly="readonly" style="font-weight: bold;font-size: 18px;"/>
								@endif	
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Proforma N°</label>
								<input type="text" style="color:red" class = "form-control form-control-sm input-sm"  value="{{$factura->id_proforma}}" readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
							</div>
						</div>
					</div>	
					<div class="row">
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Usuario de Facturación</label>
								<input type="text" class = "form-control form-control-sm input-sm" value="{{$factura->usuario_f_apellido}}, {{$factura->usuario_f_nombre}}" disabled="disabled"/>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Fecha de Facturación</label>
								@if(date('d/m/Y', strtotime($factura->fecha_hora_facturacion)) !== '31/12/1969')
									<input type="text" value="{{date('d/m/Y H:m:i', strtotime($factura->fecha_hora_facturacion))}}" class="form-control form-control-sm pull-right fecha input-sm"   disabled="disabled">
								@endif    
							</div>
						</div>
						<div class="col-12 col-md-3"> 
						 	<div class="form-group">
								<label>Tipo de Facturación</label>					
								<input type="text" class = "form-control form-control-sm input-sm" value="{{$facturas[0]->tipo_facturacion}}" readonly style="font-weight: bold;"/>
							</div>  
						</div>
						<div class="col-12 col-md-3"> 
							<div class="form-group">
								<label>Tipo Factura</label>						 
								<input type="text" class = "form-control form-control-sm input-sm" value="{{$facturas[0]->tipo_factura}}"   readonly  style="font-weight: bold;"/>
							</div>
						</div> 
					</div>	
					<div class="row">
						<div class="col-12 col-md-3"> 
							<div class="form-group">
								<label>Usuario Proforma</label>						 
								<input type="text" class = "form-control form-control-sm input-sm" value="{{$factura->usuario_apellido}}, {{$factura->usuario_nombre}} - {{$factura->id_usuario}}"  readonly style="font-weight: bold;"/>
							</div>
						</div> 
					    <div class="col-12 col-md-3"> 
							<div class="form-group">
								<label>Impreso</label>						 
								<input type="text" class = "form-control form-control-sm input-sm" value="<?php echo ($facturas[0]->factura_impresa == 1) ? 'SI' : 'NO' ;?>"   readonly  style="font-weight: bold;"/>
							</div>
						</div>
						<div class="col-12 col-md-3"> 
							<div class="form-group">
								<label>Fecha de Impresión</label>						 
								<input type="text" class = "form-control form-control-sm input-sm" value="{{$fecha}}" readonly style="font-weight: bold;"/>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Grupos  </label>
								<input type="text" class = "form-control form-control-sm input-sm" value="{{$factura->grupo_factura}}" readonly style="font-weight: bold;"/>
							</div>
						</div>

					</div>	
			
					<div class="row">
						{{-- ========ANULADO=========== --}}
						@if($factura->id_estado_factura == "30")
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Usuario de Anulación</label>
									<input type="text" class = "form-control form-control-sm input-sm" value="{{$factura->usuario_a_apellido}}, {{$factura->usuario_a_nombre}}" disabled="disabled"/>
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Fecha de Anulación</label>
									@if(date('d/m/Y', strtotime($factura->fecha_hora_anulacion)) !== '31/12/1969')
										<input type="text" value="{{date('d/m/Y H:m:i', strtotime($factura->fecha_hora_anulacion))}}" class="form-control form-control-sm pull-right fecha input-sm" name="vencimiento" id="vencimiento" disabled="disabled">
									@endif    
								</div>
							</div>
						@endif	
	            		<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Cantidad Pasajero</label>
								<input type="text" class="form-control form-control-sm input-sm" value="{{$factura->cantidad_pasajero}}" readonly />
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Saldo de Factura</label>
								<?php 
									if($factura->id_estado_factura == "30"){
										$saldo = 0;
									}else{
										if(isset($factura->factura_saldo)){
											$saldo = formatMoney($factura->factura_saldo,$factura->id_moneda_venta);
										}else{
											$saldo = 0;
										}
									}
								?>
								<input type="text" class="form-control form-control-sm input-sm" value="{{$saldo}}" readonly />
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Negocio</label>
								<input type="text" class="form-control form-control-sm input-sm" value="{{$factura->negocio}}" readonly />
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>File/Código</label>
								<input type="text" class="form-control form-control-sm input-sm" value="{{$factura->file_codigo}}" readonly />
							</div>
						</div>
						<div class="col-12 col-md-3"> 
							<div class="form-group">
								<label>Asistente</label>						 
								<input type="text" class = "form-control form-control-sm input-sm" value="{{$factura->asistente_apellido}}, {{$factura->asistente_nombre}}"  readonly style="font-weight: bold;"/>
							</div>
						</div> 
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Promocion</label>
								<input type="text" class="form-control form-control-sm input-sm" value="{{$factura->promocion_nombre}}" readonly />
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<?php
									$nro_factura = '';
									if (isset($factura->nro_factura_anterior)) {
										$nro_factura = $factura->nro_factura_anterior;
									}
									?>

								<label>Factura Anterior</label>
								<input type="text" style="color:red" id='fac_anterior' class ="form-control form-control-sm form-control form-control-sm-sm "  value="{{$nro_factura}}" readonly />
								
							</div>
						</div>
						<div class="col-12 col-md-3"> 
							<div class="form-group">
								<label>CDC</label>						 
								<input type="text" class ="form-control form-control-sm input-sm" value="{{$cdc}}" id="inputCdc" readonly style="font-weight: bold;color:green"/>
							</div>
						</div> 
					</div>	
					<br>
					<?php if($cdc != null){ ?>
						<div class="row">
							<div class="col-12 col-md-12"> 
								<div class="form-group">
									<input type="text" class ="form-control form-control-sm input-sm" value="POR PROCEDIMIENTO DE SIFEN: LAS FACTURAS ELECTRONICAS PUEDEN SER ANULADAS HASTA 48HS DESPUES DE LA APROBACION DEL DOCUMENTO ELECTRONICO" readonly style="text-align: center;font-weight: bold;color:red; font-size: 18px"/>
								</div>
							</div> 
						</div>	
						<?php } ?>
					<br>				
					<div class="row">
						<div class="col-12 col-md-3">
							@if(isset($liquidacion->id))	
								<a href="{{route('verLiquidaciones')}}?datos={{$liquidacion->id}}" style="color:black"  class="bgRed"><label class = "base">Liquidación  <i class="fa fa-fw fa-search" style="color:red;cursor: pointer;"></i></label></a>
								<input style="color:red" type="text" class = "Requerido form-control input-sm text-bold" name="proforma_id" style="font-weight: bold;font-size: 15px;" value="{{$liquidacion->periodo}}" readonly="readonly"/>
							@endif	
						</div>
						<div class="col-md-9">
							<br>

							@if($cdc == null)
								<a role='button' href="/generarJsonFacturaURL/{{$factura->id}}" class="btn btn-info pull-right text-white" style="padding:5px 40px; margin-left: 5px;" title='Reenviar Factura para DE'><i class='fa fa-external-link-square'></i></a>
							@endif	

							{{-- CARGA LOS BOTONES POR PERMISO DE PERSONAS --}}
							@foreach ($btn as $boton)
								<?php  echo $boton; ?>
							@endforeach
							
							 <a data-target="#modalDetallesFactura" data-toggle="modal" id="btn_pf_dt" class="btn btn-info pull-right text-white" style="padding:5px 40px; margin-left: 5px;" role="button"><i class="fa fa-object-group"></i></a>

							<a onclick="verVoucher({{$factura->id_proforma}},{{$factura->id}})" data-toggle="modal" data-target="#modalAdjunto" class="btn btn-success pull-right text-white" style="padding:5px 40px; margin-left: 5px;" role="button"><i class="fa fa-files-o fa-lg"></i></a>

							<a onclick="formaPago({{$factura->id_proforma}})" title="Forma Pago"  class="btn btn-primary pull-right text-white" style="padding:5px 40px; margin-left: 5px;" role="button"><i class="fa fa-fw fa-credit-card"></i></a>

							<a href="{{route('detallesProforma',['id'=>$factura->id_proforma])}}" class="btn btn-info pull-right text-white" style="padding:5px 40px; margin-left: 5px; " role="button" title="Ver Proforma"><i class="fa fa-file fa-lg"></i></a>
							
							@if($voucher > 0 && $factura->id_estado_factura == "29")
								<a id='btnVoucher' class="btn btn-success pull-right text-white" style="padding:5px 40px; margin-left: 5px; background:#E5097F !important; " role="button" title="Voucher"><i class="fa fa-print fa-lg"></i><b>&nbsp; V</b></a>
							@endif	
							<a onclick="verExtracto({{$factura->id}})" data-toggle="modal" data-target="#modalExtracto" class="btn btn-success pull-right text-white" style="padding:5px 40px; margin-left: 5px;" role="button"><i class="fa fa-calculator fa-lg"></i></a>
							<a role='button' href="{{route('enviarFactura',['id'=>$factura->id])}}" class="btn btn-success pull-right text-white" style="padding:5px 40px; margin-left: 5px;" title='Enviar Factura por E-mail'><i class='fa fa-envelope'></i></a>
							<a role='button' href="{{route('imprimirFacturaOriginal',['id'=>$factura->id])}}" class='imprimirFactStyle btn btn-info text-white pull-right' style="border-color: #1ab0c3 !important;background-color: #2dcee3 !important;" title='Imprimir Original'><i class='fa fa-print fa-lg'></i></a>  

						</div>
					</div>	
	            </div>
	        </div>
	    </div>        	
	    <div class="card" style="border-radius: 14px;">
		   <div class="card-header" style="border-radius: 14px;">
		        <h4 class="card-title">Detalles de Factura</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body">
					<div class="table-responsive">
						<table id="lista_productos" class="table">
							<thead>
								<tr>
									<th>Cant.</th>
									<th>Producto</th>
									<th>In/Out</th>
									<th>Código</th>
									<th>Proveedor</th>
									<th>Prestador</th>
									<th>Detalle</th>
									<th>Moneda</th>
									<th>Costo</th>
									<th>Tasas</th>
									<th>C.A.</th>
									<th>Venta Neta</th>
									<th>Venta</th>
									<th>%M</th>
									<th>Renta</th>
									<th>%R</th>
									<th>Comisiona</th>
									<th>Comentario</th>
								</tr>
							</thead>
							<tbody >	
								@foreach($facturaDetalles as $key=>$detalle)
									<tr>
										<td>{{$detalle->cantidad}}</td>
										<td>{{$detalle->producto}}</td>
										<td style="text-align: center;">
											@if(date('d/m/Y', strtotime($detalle->fecha_in)) !== '31/12/1969')
													{{date('d/m/Y', strtotime($detalle->fecha_in))}} - 
												@endif
												<br>
												@if(date('d/m/Y', strtotime($detalle->fecha_out)) !== '31/12/1969')
													{{date('d/m/Y', strtotime($detalle->fecha_out))}}
											 @endif
										</td>
										<td>{{$detalle->cod_confirmacion}}</td>
										<td>{{$detalle->proveedor}}</td>
										<td>{{$detalle->prestador}}</td>
										<td>{{$detalle->descripcion}}</td>
									 	<td>{{$detalle->moneda_costo}}</td>
										<td><b>{{formatMoney($detalle->total_costo,$detalle->currency_costo_id)}}</b></td>
										<td><b>{{formatMoney($detalle->monto_no_comisionable_proveedor,$factura->id_moneda_venta)}}</b></td>
										<td>{{formatMoney($detalle->porcentaje_comision_agencia,$factura->id_moneda_venta)}}</td>
										<td><b>{{formatMoney($detalle->total_neto,$factura->id_moneda_venta)}}</b></td>
										<td><b>{{formatMoney($detalle->precio_venta,$factura->id_moneda_venta)}}</b></td>
										<td><b>{{number_format($detalle->markup,2,",",".")}}</b></td>
										<td><b>{{formatMoney($detalle->renta,$factura->id_moneda_venta)}}</b></td>
										<td><b>
											<?php
												$porcentaje_renta =	DB::select('SELECT public."get_porcentaje_ganancia_factura_detalle"('.$detalle->id_factura.','.$detalle->id.')');
												$porcentaj  = $porcentaje_renta [0]->get_porcentaje_ganancia_factura_detalle*100;
												echo $porcentaje_renta [0]->get_porcentaje_ganancia_factura_detalle;
											?>
										</b></td>
										<td style="text-align: center;">
											@if($detalle->comisiona == true)
												<b>SI</b>
											@else
												<b>NO</b>
											@endif
										</td>
										<td><b>{{$detalle->descripcion_comisiona}}</b></td>
									</tr>	 
								@endforeach
							</tbody>	
						</table>	
					</div>    
	         	</div>
	       	</div>
	   	</div>     

	    <div class="card" style="border-radius: 14px;">
		   <div class="card-header" style="border-radius: 14px;">
		        <h4 class="card-title">Resumen Factura</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body">
		        		<div class="row">
							@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa > 1)
								<div class="col-md-7">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label style="font-size: small; color: red;font-weight: 700;">TOTAL_FACTURA</label>
												<input type="text" class = "form-control form-control-sm" name="total_factura" id="total_factura" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($totalFactura[0]->total,$factura->id_moneda_venta)}}' disabled="disabled"/>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label style="font-size: small;font-weight: 700;">TOTAL_BRUTO</label>
												<input type="text" class = "form-control form-control-sm" name="total_bruto" id="total_bruto" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_bruto_factura,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label style="font-size: small;font-weight: 700;">TOTAL_NETO</label>		
												<input type="text" class = "form-control form-control-sm" name="total_neto" id="total_neto" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_neto_factura,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label style="font-size: small;font-weight: 700;">EXENTAS</label>
												<input type="text" class = "form-control form-control-sm" name="total_bruto" id="total_exentas" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_exentas,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-5">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label style="font-size: small;font-weight: 700;">GRAVADAS (IVA incluido)</label>
												<input type="text" class = "form-control form-control-sm" name="total_iva" id="total_gravadas" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_gravadas,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label style="font-size: small;font-weight: 700;">IVA</label>
												<input type="text" class = "form-control form-control-sm" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_iva,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label style="font-size: small;font-weight: 700;">R. COMISIONABLE</label>
												<input type="text" class = "form-control form-control-sm" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->renta_comisionable,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
											</div>
										</div>
									</div>
								</div>
							@else
								<div class="col-md-2" >
									<div class="form-group">
										<label style="font-size: small; color: red;font-weight: 700;">TOTAL_FACTURA</label>
										<input type="text" class = "form-control form-control-sm" name="total_factura" id="total_factura" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($totalFactura[0]->total,$factura->id_moneda_venta)}}' disabled="disabled"/>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label style="font-size: small;font-weight: 700;">TOTAL_BRUTO</label>
										<input type="text" class = "form-control form-control-sm" name="total_bruto" id="total_bruto" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_bruto_factura,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label style="font-size: small;font-weight: 700;">TOTAL_NETO</label>		
										<input type="text" class = "form-control form-control-sm" name="total_neto" id="total_neto" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_neto_factura,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label style="font-size: small;font-weight: 700;">EXENTAS</label>
										<input type="text" class = "form-control form-control-sm" name="total_bruto" id="total_exentas" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_exentas,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label style="font-size: small;font-weight: 700;">GRAVADAS (IVA incluido)</label>
										<input type="text" class = "form-control form-control-sm" name="total_iva" id="total_gravadas" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_gravadas,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label style="font-size: small;font-weight: 700;">IVA</label>
										<input type="text" class = "form-control form-control-sm" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_iva,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
								</div>
							@endif
						</div>
						<div class="row" >	
							<div class="col-md-5">
								<div class="row" >	
									<div class="col-md-3">
										<label style="font-size: small;font-weight: 700;">COSTOS</label>
										<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_costos,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
									<div class="col-md-3">
										<?php 
											if($factura->comision_operador > 0){
												$comision = $factura->comision_operador;
											}else{
												$comision = $factura->total_comision;
											}
										?>
										<label style="font-size: small;font-weight: 700;">COMISIÓN</label>
										<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($comision,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
									<div class="col-md-3">
										<label style="font-size: small;font-weight: 700;">N/COMISIONA</label>
										<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->total_no_comisiona,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
									<div class="col-md-3">
										<label style="font-size: small;font-weight: 700;">MARKUP</label>
										<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->markup,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
								</div>
							</div>
							<div class="col-md-7">
								<div class="row" >	
									<div class="col-md-3">
										<label style="font-size: small;font-weight: 700;">R.BRUTA</label>
										<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->ganancia_venta,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
									<div class="col-md-2">
										<label style="font-size: small;font-weight: 700;">INCENTIVO</label>
										<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->incentivo_vendedor_agencia,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
									<div class="col-md-3">
										<label style="font-size: small;font-weight: 700;">R.NETA</label>
										<?php 
											if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1){
												$renta_neta = (float)$factura->ganancia_venta - (float)$factura->renta_extra - (float)$factura->incentivo_vendedor_agencia;
											}else{
												$renta_neta = (float)$factura->renta_comisionable;
											}
											//	$renta_neta = (float)$factura->renta;
										?>

										<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($renta_neta,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
									<div class="col-md-2">
										<label style="font-size: small;font-weight: 700;">% RENTA</label>
										<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->porcentaje_ganancia,2,",",".")}}' disabled="disabled" tabindex="15"/>
									</div>
									<div class="col-md-2">
										<label style="font-size: small;font-weight: 700;">R.EXTRA</label>
										<input type="text" class = "form-control form-control-sm" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{formatMoney($factura->renta_extra,$factura->id_moneda_venta)}}' disabled="disabled" tabindex="15"/>
									</div>
								</div>
							</div>
						</div>
	         	</div>
	       	</div>
	   	</div>        	
		@endforeach
	@endif
</section>


<!-- ========================================
   					MODAL ADJUNTOS
   		========================================  -->		
	<div id="modalAdjunto" class="modal fade" role="dialog">
	  	<div class="modal-dialog" style="width: 60%;margin-top: 5%;">
		<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto</h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
	                <!-- Post -->
	                <div class="post" style="margin-left: 4%;margin-right: 4%;">
	                  <!-- /.user-block -->
	                  	<div class="row margin-bottom">
	                  		<div id='output'>
	                  			
			                </div>    
						</div>
	                    <div class="row">
		                    <div class="col-xs-12 col-sm-10 col-md-9">
							</div>
		                    <div class="col-xs-12 col-sm-2 col-md-2">
								<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
							</div>
						</div>								

	                	</div>
	                <!-- /.post -->
					</div>
				</div>	
			</div>	
		</div>	
	</div>	




</section>
    <!-- /.content -->



{{--==============================================
			MODAL MULTIPLE VOUCHER
	============================================== --}}

<div class="modal fade" id="modalVoucherImprimir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" style="margin-left: 0px;" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" ><i class="fa fa-fw fa-files-o"></i> Imprimir Multiple Vouchers</h4>
	        <h6 class="msgError"></h6>
	      </div>
	      <div class="modal-body">
	       <div class="row">
	       		<div class="col-xs-1 text-center" style="width: 20px;">
	       		</div>
		       	<div class="col-xs-5 text-center">
				 <button type="button" class="btn btn-success btn-lg" onclick="imprimirVoucher('true')"> <i class="fa fa-fw fa-files-o"></i> <b>Múltiple voucher</b></button>
		       	</div>

		       	<div class="col-xs-5 text-center" style="margin-left: 20px;">
				 <button type="button" class="btn btn-danger btn-lg" onclick="imprimirVoucher('false')"><i class="fa fa-fw fa-file-o"></i> <b>Un solo voucher</b></button>
		       	</div>

	       </div>
	      </div>
	      <div class="modal-footer">

	      </div>
	    </div>
	  </div>
	</div>


	<!-- ========================================
   			MODAL DETALLES DE FACTURA
   	========================================  -->		

	   <div class="modal fade" id="modalDetallesFactura" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" style="margin-left: 6%;margin-top: 7%;">
			<div class="modal-content" style="width: 300%;">
				<div class="modal-header">
					<h4 class="modal-title" id="" style="font-weight: 800;">Detalles de Factura</h4>
				</div>
				<div class="modal-body">
					<div class="table-responsive" >
						<form id="formFacturaDetalle"> 
							<input type="hidden" name="proforma" value="{{$factura->id_proforma}}">
							<table id="listadoFactura" class="table" style="width:100%">
								<thead>
									<tr>
										<th style="width: 5%;">Cant.</th>
										<th style="width: 40%;">Descripcion</th>
										<th style="width: 10%;">Precio Unitario</th>
										<th style="width: 10%;">Exento</th>
										<th style="width: 10%;">Gravada 5</th>
										<th style="width: 10%;">Gravada 10</th>
									</tr>
								</thead>
								<tbody style="text-align: center">
								</tbody>          
							</table>
						</from>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger btn-lg"><b>Salir</b></button>
				</div>
			</div>
		</div>
	</div>

		
 {{--==============================================
			FORMA DE PAGO
	============================================== --}}
	<div class="modal fade" id="modalFormaPago" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 800px;">
				<div class="modal-header">
					<h5 class="modal-title">Forma de Pago</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="row">

						<div class="col-12" style="margin-bottom: 5px;">
							<div class="form-group row">
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Monto Proforma :</label>
								<div class="col-sm-5">
									<input type="text" class="form-control"
										style="font-weight: 800; font-size: 20px;" id="monto_proforma" disabled>
								</div>
							</div>
						</div>

						<br>
							<div class="col-12">
								<div class="table-responsive">
									<table id="listadoPagos" class="table" style="width:100%">
										<thead>
											<tr>
												<th>Pago</th>
												<th>Documento</th>
												<th>Importe Pago</th>
												<th>Importe</th>
												<th>Moneda</th>
											</tr>
										</thead>

										<tbody style="text-align: center">
										</tbody>
									</table>
								</div>
							</div>

					</div>
				</div>




				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><b>Cerrar</b></button>
				</div>
			</div>
		</div>
	</div>

	<!-- ========================================
   			MODAL ANULAR DE 
   	========================================  -->		

	   <div class="modal fade" id="modalAnularFactura" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" style="margin-left: 25%;margin-top: 10%;">
			<div class="modal-content" style="width: 150%;">
				<div class="modal-header">
					<h4 class="modal-title" id="" style="font-weight: 800;">Anular Factura Electrónica</h4>
				</div>
				<div class="modal-body">
					<div class="table-responsive" >
						<form id="formFacturaDetalle"> 
							<input type="hidden" name="factura_id" value="{{$factura->id}}">
							<div class="col-md-12">
								<div class="form-group">
									<label style="font-weight: 600;font-size: 18px;">Motivo de Anulación</label>
									<textarea class="form-control" rows="5" id="acuerdo_id"></textarea>
								</div>
							</div>
						</from>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success btn-lg" onclick="anularDE()"><i class="fa fa-fw fa-file-o"></i> <b>Guardar</b></button>
					<button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger btn-lg"><b>Salir</b></button>
				</div>
			</div>
		</div>
	</div>


 {{--==============================================
			VER EXTRACTO
	============================================== --}}
	<div class="modal fade" id="modalExtranho" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document" style="margin-left: 5%;margin-top: 7%;">
			<div class="modal-content" style="width: 250%;">
				<div class="modal-header">
					<h5 class="modal-title">Ver Extracto</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-12" style="margin-bottom: 5px;">
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Total de Factura :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm" style="font-weight: 800; font-size: 20px;" value='{{number_format($totalFactura[0]->total,2,",",".")}}' id="monto_factura" disabled>
								</div>
							</div>	
							<?php 
							    $total = 0;
							 	if(isset($totalFactura[0]->total)){
									$total = $totalFactura[0]->total;
								}
							   $saldo_factura = 0;
							   	if(isset($factura->saldo_factura)){
									$saldo_factura = $factura->saldo_factura;
								}

							   $cobrado = (float)$total - (float)$saldo_factura;
							?>
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Total Cobrado :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm" style="font-weight: 800; font-size: 20px;" id="monto_cobrado" value='{{number_format($cobrado,2,",",".")}}' disabled>
								</div>
							</div>
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Saldo Factura :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm" style="font-weight: 800; font-size: 20px;" id="monto_saldo" value='{{number_format($saldo_factura,2,",",".")}}' disabled>
								</div>
							</div>
						</div>
						<br>
							<div class="col-12">
								<div class="table-responsive">
									<table id="listadoExtractos" class="table" style="width:100%">
										<thead>
											<tr>
												<th style="text-align: center;">REC/NC/AN/FAC</th>
												<th style="text-align: center;">Fecha</th>
												<th style="text-align: center;">Importe<br>REC/NC/AN/FAC</th>
												<th style="text-align: center;">Moneda<br>REC/NC/AN/FAC</th>
												<th style="text-align: center;">Cotizacion</th>
												<th style="text-align: center;">Importe<br>Cobro</th>
												<th style="text-align: center;">Moneda<br>Cobro</th>
												<th style="text-align: center;">Forma<br>Cobro</th>
	
											</tr>
										</thead>
										<tbody style="text-align: center">
										</tbody>
									</table>
								</div>
							</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><b>Cerrar</b></button>
				</div>
			</div>
		</div>
	</div>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>

	<script>
if($('#fac_anterior').val() !== ''){
	$(".notaCreditoStyle").hide();
}
	$("#grupo_id").select2();
	$("#grupo_id").select2().val("{{$factura->id_grupo}}").trigger("change");
	$('#grupo_id').prop('disabled', 'disabled');

	$('#btnVoucher').on('click',function(){
		$('#modalVoucherImprimir').modal('show');
	})

	function imprimirVoucher(option){
		$('#modalVoucherImprimir').modal('hide');
		location.href ="{{route('voucher')}}?id={{$factura->id_proforma}}&multiple="+option;
	}

		$('.modalAnular').on('click',function(){
			if($('#inputCdc').val() == ""){	
					return swal({
						title: "Anular Factura",
						text: "¿Está seguro que desea anular esta factura?",
						showCancelButton: true,
						buttons: {
							cancel: {
								text: "No, Cancelar",
								value: null,
								visible: true,
								className: "btn-warning",
								closeModal: false,
							},
							confirm: {
								text: "Si , Anular",
								value: true,
								visible: true,
								className: "",
								closeModal: false
							}
						}
					}).then(isConfirm => {
                                if (isConfirm) {
									var id = $('.modalAnular').attr('data-btn');
									$.ajax({
										type: "GET",
										url: "{{route('anularFactura')}}",
										dataType: 'json',
										data: {
											idFactura:id
										},
										error: function(jqXHR,textStatus,errorThrown){

											reject('Ocurrió un error en la comunicación con el servidor.');

											},
										success: function(rsp){
											if(rsp.err != 'OK'){
												swal("Cancelado", rsp.response[0].anular_factura, "error");
											    } else {
													swal("Exito", "La factura fue anulada.", "success");
													location.reload();
												}
											}	
										})	                              
									} else {
                                     swal("Cancelado", "", "error");
                                }
            			});
			}else{
				$("#modalAnularFactura").modal('show');
			}

		});			
		
		function verExtracto(idFactura){	
			$("#listadoExtractos").DataTable();
			$("#modalExtranho").modal('show');
			$.ajax({
					type: "GET",
					url: "{{route('getExtracto')}}",
					dataType: 'json',
					data: {
							idFactura:idFactura
					},
					error: function(jqXHR,textStatus,errorThrown){
						reject('Ocurrió un error en la comunicación con el servidor.');
					},
					success: function(rsp){
										var oSettings = $('#listadoExtractos').dataTable().fnSettings();
										var iTotalRecords = oSettings.fnRecordsTotal();

										for (i=0;i<=iTotalRecords;i++) {
											$('#listadoExtractos').dataTable().fnDeleteRow(0,null,true);
										}
										totalPago = 0;
										$.each(rsp.exacto, function (key, item){
											// console.log(item);
											if(item.tipo =='NC'){
											btn = `<a href="{{route('verNota',['id'=>''])}}/${item.id_recibo_nc}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${item.nro_recibo_nc}</a><div style="display:none;">${item.id_recibo_nc}</div>`;
											}
											if(item.tipo =='RECIBO'){
											btn = `<a href="{{route('controlRecibo',['id'=>''])}}/${item.id_recibo_nc}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${item.nro_recibo_nc}</a><div style="display:none;">${item.id_recibo_nc}</div>`;
											}		
											if(item.tipo =='AN'){
												btn = `<a href="{{route('anticipoDetalleCliente',['id'=>''])}}/${item.id_anticipo}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${item.id_anticipo}</a><div style="display:none;">${item.id_anticipo}</div>`;
											}
											if(item.tipo =='FACTURA_ANTERIOR'){
												btn = `<a href="{{route('verFactura',['id'=>''])}}/${item.id_factura_anterior}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${item.nro_recibo_nc}</a><div style="display:none;">${item.id_factura_anterior}</div>`;
											}
											totalPago = parseFloat(item.importe_cobrado) + parseFloat(totalPago);
											accion = '<button type="button" id="btnAceptarProforma" class="btn btn-danger" onclick="calcular('+item.saldo_factura+','+item.id+')" style="width: 40px;background-color: #e2076a;padding-right: 5px;padding-left: 5px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-plus fa-lg"></i></button>';
											var dataTableRow = [
																btn,
																item.fecha_aplicacion,
																formatter.format(parseFloat(item.importe_item_recibo)),
																item.moneda_recibo,
																item.cotizacion,
																formatter.format(parseFloat(item.importe_cobrado)),
																item.moneda_cobro,
																item.tipo,
															];

										var newrow = $('#listadoExtractos').dataTable().fnAddData(dataTableRow);
										var nTr = $('#listadoExtractos').dataTable().fnSettings().aoData[newrow[0]].nTr;
										})
										//monto_cobrado
										// console.log(totalPago);
										total = parseFloat(clean_num($('#monto_factura').val()));
										saldo = total - totalPago;
										/*$('#monto_cobrado').val(formatter.format(totalPago));
										$('#monto_saldo').val(formatter.format(saldo));*/

										var winSize = {
										wheight : $(window).height(),
										wwidth : $(window).width()
										};
										var modSize = {
										mheight : $('#modalExtranho').height(),
										mwidth : $('#modalExtranho').width()
										};
										$('#modalExtranho').css({
											'padding-left' :  ((winSize.wheight - (modSize.mheight/2))/2),
										});
					}	
				});	

		}

		function verVoucher(proforma, item){
			
			// console.log(proforma);
			// console.log(item);
			$.ajax({
					type: "GET",
					url: "{{route('getAdjuntosVoucher')}}",
					dataType: 'json',
					data: {
							dataProforma: proforma,
							dataItem: item,
			       			},
					success: function(rsp){
							// console.log(rsp.length);
							$("#output").html('');
							if(rsp.length != 0){
								$.each(rsp, function (key, item){
								 	        ruta = 'adjuntoDetalle/'+item.nombre_adjunto;	
											// console.log(item.nombre_adjunto);
											var indice  = item.nombre_adjunto;
											extension = indice.split('.');
											// console.log(extension);
						                if(extension[1] != 'pdf' && extension[1] != 'doc'){
						                 	$("#output").append('<div id= "'+item.id+'"><div class="col-sm-2" id= "'+item.id+'"><a href="/adjuntoDetalle/'+item.nombre_adjunto+'" target="_blank"><img class="img-responsive" style="width:80px; height:120px;" src="/adjuntoDetalle/'+item.nombre_adjunto+'" alt="Photo"></a><label>'+item.nombre_adjunto+'</label></div><div class="col-xs-12 col-sm-1 col-md-1"></div></div>');
						                 }else{
						                      $("#output").append('<div id= "'+item.id+'"><div class="col-sm-2"><a href="/adjuntoDetalle/'+item.nombre_adjunto+'" target="_blank"><img class="img-responsive" style="width:80px;" src="{{asset('images/file.png')}}" alt="Photo"></a><label>'+item.nombre_adjunto+'</label></div><div class="col-xs-12 col-sm-1 col-md-1"></div></div>');
						                } 
								})	
							}else{
								$("#modalAdjunto").modal('hide');
							}		
					}
				})		
		}




		 function formaPago(id_proforma){
	 	$('#input_id_proforma').val(id_proforma);
	 	getformaPago(2,id_proforma);
	 	// console.log(id_proforma);
	 }



const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD'
					});
{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
	NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}
function clean_num(n,bd=false){
	if(n && bd == false){ 
	n = n.replace(/[,.]/g,function (m) {  
			 				 if(m === '.'){
			 				 	return '';
			 				 } 
			 				  if(m === ','){
			 				 	return '.';
			 				 } 
			 			});
	return Number(n);
}
if(bd){
	return Number(n);
}
return 0;
}



function getformaPago(option,id_proforma){


	$('#carga_forma_paga').show();	
	//LEVANTAR MODAL
	$('#modalFormaPago').modal('show');
	let dataString = { id_proforma :id_proforma, option:option};

		$.ajax({
					type: "GET",
					url: "{{route('getformaPago')}}",
					dataType: 'json',
					async:false,
					data: dataString,
						error: function(){
							$('#carga_forma_paga').hide(2000);
							$('#msj_pago').removeClass('bg-blue');
							$('#msj_pago').css('background-color','#C93F3F' );
							$('#msj_pago').html('Ocurrio un error en la comunicación con el servidor.');
						
							},
			success: function(rsp){

			$('#monto_proforma').val(formatter.format(parseFloat(rsp.monto_proforma)));
			$('#listadoPagos tbody').html('<tr class="odd"><td valign="top" colspan="17" class="dataTables_empty">No hay datos disponibles</td></tr>');	

			if(rsp.get_fp.length > 0){
			    $('#listadoPagos tbody').html('');
				$.each(rsp.get_fp, function(index, val) {
				let cantItem = $('#listadoPagos tr').length - 1;
				let tabla = `<tr id="lineaPagoEliminar_${cantItem}" class="tabla_filas">
				<td><input type="text" readonly value = "${val.forma_pago_txt}" class = "form-control form-control-sm input-sm" /></td>
				<td><input type="text" readonly value = "${val.documento}" class = "form-control form-control-sm input-sm" name="datos[][documento]"/></td>
				<td><input type="text" readonly value = "${formatter.format(parseFloat(val.importe_pago))}" class = "form-control form-control-sm input-sm" name="datos[][importePago]"/></td>
				<td><input type="text" readonly value = "${formatter.format(parseFloat(val.importe))}" class = "form-control form-control-sm input-sm" name="datos[][importe]"/></td>
				<td><input type="text" readonly value = "${val.currency_code}" class = "form-control form-control-sm input-sm"/></td>
				</tr>
				`;
				$('#listadoPagos tbody').append(tabla);
				});//EACH
				
								}//IF
						$('#carga_forma_paga').hide(2000);
							}//success


					});	//ajax	


		
	}//function

	$('#btn_pf_dt').on('click',function(){
			addFilaDetalle();
	})

	function addFilaDetalle(){
		$.ajax({
				type: "GET",
				url: "{{route('getDetalleFactura')}}",
				dataType: 'json',
				data: {
						dataProforma: '{{$factura->id_proforma}}',
					},
				error: function(jqXHR,textStatus,errorThrown){
					$.toast({
						heading: 'Error',
						position: 'top-right',
						text: 'Ocurrió un error en la comunicación con el servidor.',
						showHideTransition: 'fade',
						icon: 'error'
					});

				},
				success: function(rsp){
							// console.log(rsp);
							if(jQuery.isEmptyObject(rsp) == false){
								$.each(rsp, function(index,value){	
									generarFilaDetalle(value.cantidad,value.descripcion,value.precio_venta,value.exento,value.gravadas_5,value.gravadas_10,value.total,0);
								})
							}else{
								$.toast({
										heading: 'Error',
										position: 'top-right',
										text: 'No existe detalle personalizado en esta factura.',
										showHideTransition: 'fade',
										icon: 'error'
									});

								$('#modalDetallesFactura').modal('hide');
							}	
				}
		})
	}		

	function generarFilaDetalle(cantidad,descripcion,precio_unitario,exenta,iva5,iva10,total){
			var rowCount = $("#listadoFactura tbody tr").length;

			var tbody = $('#listadoFactura tbody');

			var id_proforma = '{{$factura->id_proforma}}';

			var htmlTags =  '<tr id="'+rowCount+'">'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric" id="cantD_'+rowCount+'" name="detalle['+rowCount+'][cantidad]" value="'+cantidad+'" disabled>'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm" id="desc_'+rowCount+'" name="detalle['+rowCount+'][descripcion]" value="'+descripcion+'" disabled>'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric" id="pru_'+rowCount+'" name="detalle['+rowCount+'][precio_unitario]" value="'+precio_unitario+'" disabled>'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric" id="exe_'+rowCount+'" name="detalle['+rowCount+'][exenta]" value="'+exenta+'" disabled>'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric" id="iv5_'+rowCount+'" name="detalle['+rowCount+'][iva5]" value="'+iva5+'" disabled>'+
								'</th>'+
								'<th>'+
									'<input type="text" class = "form-control input-sm numeric" id="iv10_'+rowCount+'" name="detalle['+rowCount+'][iva10]" value="'+iva10+'" disabled>'+
									'<input type="hidden" id="id_proforma_'+rowCount+'" name="detalle['+rowCount+'][id_proforma]" value="'+id_proforma+'">'+
									'<input type="hidden" class="totalesDetallesFactura" id="totalDetalle_'+rowCount+'" name="detalle['+rowCount+'][total]" value="'+total+'">'+
								'</th>'+
							'</tr>';
			tbody.append(htmlTags);
			$('.numeric').inputmask("numeric", {
				radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				// prefix: '$', //No Space, this will truncate the first character
				rightAlign: false,
				oncleared: function () { 
					$(this).val(0);
				}
			});

		}	
        
		contador = '{{$contador}}';

		function anularDE(){

				return swal({
						title: "Anular Factura",
						text: "¿Está seguro que desea anular esta factura?",
						showCancelButton: true,
						buttons: {
							cancel: {
								text: "No, Cancelar",
								value: null,
								visible: true,
								className: "btn-warning",
								closeModal: false,
							},
							confirm: {
								text: "Si , Anular",
								value: true,
								visible: true,
								className: "",
								closeModal: false
							}
						}
					}).then(isConfirm => {
                                if (isConfirm) {
									var id = $('.modalAnular').attr('data-btn');
									$.ajax({
											type: "GET",
											url: "{{route('anularDE')}}",
											dataType: 'json',
											data: {
													idFactura:'{{$factura->id}}',
													cdc: $('#inputCdc').val(),
													motivo: $('#acuerdo_id').val()
													},
											success: function(rsp){
													console.log(rsp);
													if(rsp.status == 'OK'){
														swal("Exito", rsp.mensaje, "success");
														$("#modalAnularFactura").modal('hide');
														location.reload();

													}else{
														swal("Cancelado", rsp.mensaje, "error");
													}
												}
										});                            
									} else {
                                     swal("Cancelado", "", "error");
                                }
            			});
		}


	</script>
@endsection
