<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Factura</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 90%;
			margin:0 auto;
    		z-index:1;
	}
	
	table{
		width: 100%;
		border-spacing: 0px;
	}
	
	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 700;
	}
	.n-2{
		font-weight: 350;
	}
	.text{
		  overflow: hidden;
		  /*text-overflow: ellipsis;*/
		  white-space: nowrap;
		  display:block;
		  width:100%;
		  min-width:1px;
		}

	.f-10 {
		font-size: 10px !important;
	}
	.f-5 {
		font-size: 5px !important;
	}
	.f-6 {
		font-size: 6px !important;
	}
	.f-7 {
		font-size: 7px !important;
	}

	.f-8 {
		font-size: 8px !important;
	}
	.f-9 {
		font-size: 9px !important;
	}
	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}
	.c-text {
		text-align: center;
	}
	.r-text {
		text-align: right;
	}
	.r-text-detalle{
		margin-right: 20px;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 10px;
	}


	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:80px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}



	</style>
</head>
<body>



@php	


 	function formatoFecha($date){
		if( $date != ''){

		$date = explode('-', $date);
		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
		 	return $fecha;
		} else {
			return 'N/A';
		}
	}


	function formatoFechaSinHora($date){
		if( $date != ''){

		$date = explode(' ', $date);
		$date = trim($date[0]);
		$date = explode('-', $date);

		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
		 	return $fecha;
		} else {
			return 'N/A';
		}
	}

		
	function formatMoney($num,$currency){
		// dd($f);
		if($currency != 111){
		return number_format($num, 2, ",", ".");	
		} 
		return number_format($num,0,",",".");

	}
	if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1){
	    if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_impresion == 1){
			$facturaOriginal = array( ""=>"Original - Cliente",
								  	  "DUPLICADO"=>"Duplicado - Tesoreria");	
			$maxDetalles = 8;
			$tipo = 0;
		}else{
			$facturaOriginal = array(""=>"Original - Cliente",
								  	  "DUPLICADO"=>"Duplicado - Tesoreria",
								  	  "TRIPLICADO"=>"TRIPLICADO");
			$maxDetalles = 39;
			$tipo = 1;
		}
	}else{
		if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 3){
			$facturaOriginal = array( ""=>"Original - Cliente");
			$maxDetalles = 8;
			$tipo = 0;
		}else{
			if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_impresion == 1){
				$facturaOriginal = array( ""=>"Original - Cliente",
								  	  "DUPLICADO"=>"Duplicado - Tesoreria");	
				$maxDetalles = 8;
				$tipo = 0;
			}else{
				$facturaOriginal = array(""=>"Original - Cliente",
										"DUPLICADO"=>"Duplicado - Tesoreria",
										"TRIPLICADO"=>"TRIPLICADO");
				$maxDetalles = 39;	
				$tipo = 1;					
			}		
		}

	}
	$emailEmpresa = $empresa->email; 
	$paginaEmpresa = $empresa->pagina_web;
	$telefonoEmpresa = $empresa->telefono;
	$direccionEmpresa = $empresa->direccion;
	$idEmpresa = $empresa->id; 
	$facturaCopia =   array("COPIA"=>"Copia - Factura no vàlida");
	$facturaAnulada = array("ANULADO"=>"Copia - Factura no vàlida");
	$factClase = '';
	$recorrido = ''; 
	$contadorDetalles = 0;
	$contadorRecorrido = 0;
	$m ="";


//SE DEFINE EL RECORRIDO DE LAS IMPRESIONES
	//Orignial
if($imprimir == '1'){

$recorrido  = $facturaOriginal;
$m = "";
$corte = 3;

}
	//copia
else if ($imprimir == '2'){

	$recorrido = $facturaCopia;
	$m = "COPIA ELECTRONICA DE LA FACTURA, SIN NINGUN VALOR COMERCIAL, CONTABLE O ADMINISTRATIVO";
	$corte = 1;
}
	//anulado copia
else if($imprimir == '3'){

	$recorrido = $facturaAnulada;
	$m = "COPIA ELECTRONICA DE LA FACTURA, SIN NINGUN VALOR COMERCIAL, CONTABLE O ADMINISTRATIVO";
	$corte = 1;

}

@endphp

@foreach ($recorrido as $key => $value)

@php
$contadorRecorrido++;
	

	/*-==============================================================
					VALORES DE CABECERA INFO FACTURA
	==================================================================  */
	$imagenNombre = $empresa->logo;
	$denominacionEmpresa = strtoupper($empresa->denominacion); 
	
	$timbradoNum = $factura[0]->timbrado->numero;
	$timbradoFA = $factura[0]->timbrado->fecha_autorizacion; 
	$timbradoAut = $factura[0]->timbrado->autorizacion;
	$fechaInicioVigencia =  formatoFecha($factura[0]->timbrado->fecha_inicio);
	$fechaFinVigencia =  formatoFecha($factura[0]->timbrado->vencimiento);
	$rucFactura = $empresa->ruc;
	$numFactura = $factura[0]->nro_factura;

	$factClase = $value;
	//CONFIG FACTURA
	$mostrarGravada = $empresa->imprimir_detalle_factura;
	$idMonedaVenta = $factura[0]->id_moneda_venta;
	$pieFactura_txt = $empresa->pie_factura_txt;



	/*
	 * =================================================================
	 * 				VALORES DE CABECERA DATOS DE FACTURA
	 * ================================================================= 
	 */
	



	$vendedorDtp = $vendedorEmpresa[0]->nombre." ".$vendedorEmpresa[0]->apellido;
	if(isset($factura[0]->vendedorEmpresa['nombre'])){
		$vendedorAgencia  = $factura[0]->vendedorEmpresa['nombre']." ". $factura[0]->vendedorEmpresa['apellido'];
	}else{
		$vendedorAgencia  = "";
	}
	
	$fechaEmisionFactura = date('d/m/Y', strtotime($factura[0]->fecha_hora_facturacion));
	$fechaVencimientoFactura = formatoFecha($factura[0]->vencimiento);
	$tipoFactura = $factura[0]->tipoFactura['denominacion'];
	$nombreRazonSocial = $factura[0]->cliente['nombre']." ". $factura[0]->cliente['apellido'];
	$rucCliente = $factura[0]->cliente['documento_identidad'];
	$direccionFactura = $factura[0]->cliente['direccion'];
	$telefonoFactura = $factura[0]->cliente['telefono'];
	$pasajeroFactura = $pasajeroNombre;
	$in = formatoFechaSinHora($factura[0]->check_in);
	$out = formatoFecha($factura[0]->check_out);
	$proforma =  $factura[0]->proforma['id'];
	$idEmpresa = $factura[0]->id_empresa;





	/*
	 * =================================================================
	 * 				DESCRIPCION DE SERVICIOS Y PRODUCTOS EJEMPLO
	 * =================================================================
	  */
		
	$codigoProducto = "8";
	$cantidadProducto = "1";
	$descripcionProducto  = "Nuevo Prodcuto";



	/**
	 * =================================================================
	 * 				PIE DE SERVICIOS Y PRODUCTOS
	 * =================================================================
	 */ 

	
	$subTotales = "";
	$subTotalTexto = "";
	$exentaTotal =  ($factura[0]->total_exentas != null) ? formatMoney($factura[0]->total_exentas,$idMonedaVenta) : "0,00";
	$iva5Total = "0,00";
	$iva10Total = ($factura[0]->total_gravadas != null) ? formatMoney($factura[0]->total_gravadas,$idMonedaVenta) : "0,00";
	$total = formatMoney($totalFactura,$idMonedaVenta);
	$moneda = $factura[0]->currency['hb_desc'];

	/*====
	1 Factura Bruta
	2 Factura Neta
	*/


	
	$letraTotal = $NumeroALetras;
	$liquidacionIva10 =  formatMoney($factura[0]->total_iva,$idMonedaVenta);
	$totalIva = "12,14";
	$espacios = 0;







	/*==============================================================
						INICIO DE FACTURA
	 ==================================================================  */

	
@endphp


<!--{{--
	 <div id="background">
	 @if($tipo == 0)	
	 	<p id="bg-text" style="margin-top: 100px; margin-left: 10px"><?php echo $key  ?></p>
	@else 
		<p id="bg-text" style="margin-top: 100px; margin-left: 10px;margin-top: 5%;"><?php echo $key  ?></p>
	@endif
	</div>  --}}-->

	@if($tipo == 0)	
		<div class="container espacio-10">
	@else 
		<div class="container espacio-10" style="margin-top: 5%;" >
	@endif
	<table>
		<tr>
					<td style="width: 25%;" class="c-text">
						<img src='{{$logo}}' alt="{{$logo}}" width="150">
					</td>


					<td style="width: 40%; padding: 5px;" class="c-text f-9">
						@if($idEmpresa != 2)	
							<span class="f-9"><?php echo $denominacionEmpresa;?></span><br>
						@endif	
						@if($idEmpresa == 1)
							<span class="f-9">OPERADORA MAYORISTA DE TURISMO</span><br>
							<span class="f-8">Actividades de Agencia de Viaje</span><br>
							<span class="f-8">CAPITAL Gs. 94.779.358.-</span><br>
							<span class="f-9">REGISTRO SENATUR Nro. 287-B</span><br>
						@elseif($idEmpresa == 4)
							<span class="f-10">de Claudia Patricia Cano Galeano</span><br>
							Actividades de Agencia de Viaje<br> 
						@elseif($idEmpresa == 2)	 
							<span class="f-9">de DISCOVERY TRAVEL SA</span><br>
							<span class="f-9">AGENCIA DE VIAJES</span><br>
							<span class="f-6">25 de Mayo esq Gaudioso Nuñez</span><br>
							<span class="f-6">Asuncion Paraguay - Tel:(021)205-596/205-227</span><br>
							<span class="f-6">Cel:(0984)316458/contacto@smarttravel.com.py</span><br>
						@endif
						<!--<span class="f-5">
							Telefonos: <?php echo $telefonoEmpresa;?><br>
							E-mail: <?php echo $emailEmpresa;?><br>
							<?php echo $paginaEmpresa;?> 
						</span>	
						<br>-->
						@foreach($sucursalEmpresa as $sucursales) 
								<span class="f-5">
								<b><?php echo ucwords(strtolower($sucursales->nombre)); ?>:</b> {{$sucursales->direccion}}. Telefonos: {{$sucursales->telefono}}</span><br>
						@endforeach 
						@if($idEmpresa == 4)
							<span class="f-5">
								E-mail: <?php echo $emailEmpresa;?><br>
							</span>	
						@endif
						<br>
					</td>
					<td style="width:42%;"> 
						<div style="padding: 5px;">
							<div style="border:1px solid; text-align: center; padding: 5px;">
								<span class="n-2">Timbrado Nro:<span id="numTimbrado"><?php echo $timbradoNum; ?></span></span><br>
								<span class="f-7">AUTORIZACION COMO AUTORIZACION Nº:<span><?php echo $timbradoAut; ?></span>FECHA: <span><?php echo $timbradoFA; ?></span></span><br>
								<span class="f-8">Fecha Inicio Vigencia: <?php echo $fechaInicioVigencia; ?></span><br>
								<span class="f-9">Fecha Fin Vigencia: <?php echo $fechaFinVigencia; ?></span><br>
								<span class="n-2">R.U.C <?php echo $rucFactura; ?></span><br>
								<span class="n-2">FACTURA Nro.: <?php echo $numFactura;?></span><br>
								<span class="f-8"><b> <?php echo $factClase; ?> </b></span>	
							</div>

						</div>
					</td>
		</tr>

	</table>

	<!-- {{--<table style="margin-top: 10px;"  class="b-buttom b-top">
		<tr>
			<td class="f-10" style="padding: 0 0 0 20px;">
				<br>
				@foreach($sucursalEmpresa as $sucursales) 
					<b><?php echo ucwords(strtolower($sucursales->nombre)); ?>:</b> {{$sucursales->direccion}}. Telefonos: {{$sucursales->telefono}}<br>
				@endforeach
				<br>
				{{-- <b>Casa Central:</b> Gral Bruguez 353 casi 25 de Mayo, Asunción - Paraguay. Telefonos: 021-221-816  Fax: 449-724<br>
				<b>Sucursal Ciudad del Este:</b> Avda Pioneros del Este entre Adrian Jara y Pa'i Perez, Galeria JyD. Ciudad del Este - Paraguay. Telefono: 061-511-779 al 80<br>
				<b>Sucursal Encarnación:</b> Mcal Estigarriba 1997. Galeria San Jorge, Local 13 y 16. Encarnación - Paraguay. Telefono: 071-200-104 --}}
			</td>
		</tr>
	</table>


	<table class="f-11" style="margin-top: 20px;">
		<tr>
			<td>
				Vendedor Empresa: <?php echo $vendedorDtp; ?>
			</td>

			<td>
				Vendedor Agencia: <?php echo $vendedorAgencia; ?>
			</td>
		</tr>
	</table>--}} -->


	<table class="f-10" style="border:1px solid">

		<colgroup>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
	
		</colgroup>

		<tr>
			<td colspan="3">
				<b>Fecha de Emision:</b> <?php echo $fechaEmisionFactura; ?>
			</td>

			<td colspan="3">
				<b>Vencimiento:</b> <?php echo $fechaVencimientoFactura; ?>
			</td>

			<td colspan="3">
				<b>Tipo Factura:</b> <?php echo $tipoFactura; ?>
			
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<b>Nombre / Razon Social:</b> <?php echo $nombreRazonSocial; ?>
			</td>

			<td colspan="3">
				
			</td>

			<td colspan="3">
				<b>RUC:</b> <?php echo $rucCliente; ?>
		
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<b>Dirección:</b> <?php echo $direccionFactura; ?>
			</td>

			<td colspan="3">
				<b>Telefono:</b> <?php echo $telefonoFactura; ?>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<b>Pasajero:</b> <?php echo $pasajeroFactura;?>
			</td>

			<td colspan="3">
				<b>Proforma N.:</b> <?php echo $proforma;?>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<b>Vendedor Empresa:</b> <?php echo $vendedorDtp; ?>

			</td>
			<td colspan="3">
				@if($idEmpresa == 1)	
					<b>Vendedor Agencia:<b> <?php echo $vendedorAgencia; ?>
				@endif	
			</td>

			<td colspan="3">
				<b>IN/OUT:</b> <?php echo $in;?> - <?php echo $out;?>
			</td>
		</tr>

	</table>

	<table class="c-text b-col f-11" border="1">

		<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>		
		</colgroup>
		<tr>
			<td width="5%">
				Cod
			</td>

			<td width="5%">
				Cant
			</td>
			<td width="30%">
				Descripción Producto
			</td>
			<td width="10%">
				Precio Unitario
			</td>
			<td width="10%">
				Exentos
			</td>

			<td width="10%">
				Iva 5%
			</td>

			<td width="10%">
				Iva 10%
			</td>
		</tr>

		</table>
			
		<table class=""  FRAME="vsides" RULES="cols" style="border-bottom: 1px solid; ">

		<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
		</colgroup>

		

		{{-- ===================================
			AGREGAR SERVICIOS Y ESPACIOS
		=================================== --}}
		
		@foreach($facturaDetalle as $detalles)

		@php
		//Si es de tipo Ticket agregar codigo confirmacion
		$descripcion = $detalles->descripcion;
		$desc = $descripcion;
			if($detalles->producto->id == 9  || 
			   $detalles->producto->id == 31 ||
			   $detalles->producto->id == 29 ||
			   $detalles->producto->id == 32 ||
			   $detalles->producto->id == 31 ||
			   $detalles->producto->id == 1){

			
			
			   $descripcion .= ' ('. $detalles->cod_confirmacion.' )';
			   $longitud = strlen($descripcion);
			  $desc = wordwrap($descripcion,50,'<br />',true);

		
			}


		@endphp
		
		{{-- VALIDAR SI EL PRODUCTO SE PUEDE IMPRIMIR EN FACTURA --}}
		@if($contadorDetalles <= 15 && $detalles->producto['imprimir_en_factura'] == 1)
		
		<tr valign="top" class="f-10" nowrap>
		

			<td class="c-text" width="5%">
				{{$detalles->id_producto}}
			</td>

			<td class="c-text" width="5%">
				{{$detalles->cantidad}}
			</td>

			<td width="30%" >
				<?php echo $desc; ?>
			</td>
			<td class="r-text" width="10%">
				<div class="r-text-detalle"><?php if($mostrarGravada){ echo ($detalles->precio_venta != null) ? formatMoney($detalles->precio_venta,$idMonedaVenta): ''; } ?><div>
			</td>

			<td class="r-text" width="10%">
				<div class="r-text-detalle"><?php if($mostrarGravada){ echo ($detalles->exento != null) ? formatMoney($detalles->exento,$idMonedaVenta): ''; } ?><div>
			</td>

			<td class="c-text" width="10%">
				<div class="r-text-detalle"><?php if($mostrarGravada){ echo ($detalles->exento != null) ? formatMoney($detalles->exento,$idMonedaVenta): ''; } ?><div>
			</td>

			<td class="r-text" width="10%">
				<div class="r-text-detalle"><?php if($mostrarGravada){ echo ($detalles->gravadas_10 != null) ? formatMoney($detalles->gravadas_10,$idMonedaVenta): ''; } ?><div>
			</td>


		</tr>

			 <?php
			 $contadorDetalles++;
			 ?>


			@endif



			

		@endforeach
		

		{{-- AGREGAR ESPACIOS EN BLANCO PARA COMPLETAR 15 LINEAS EN TOTAL --}}
		<?php 
		if($contadorDetalles < $maxDetalles){
			$espacios = $maxDetalles - $contadorDetalles;
		}

		for ($i=0; $i < $espacios ; $i++):?>

			<tr valign="top" class="f-11">
		

			<td class="c-text" width="5%">
				&nbsp;
			</td>

			<td class="c-text" width="5%">
				
			</td>

			<td width="30%">
				
			</td>
			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>


		</tr>
			
		
	<?php endfor;?>
		
		
		<tr valign="top" class="f-12">
			<td class="c-text" colspan="6"> <?php echo $m; ?></td>
		</tr>
	
		<tr valign="top" class="f-10">
		

			<td class="c-text" width="5%">
				&nbsp;
			</td>

			<td class="c-text" width="5%">
				
			</td>

			<td width="30%">
				
			</td>
			<td class="r-text" width="10%">
			</td>
			<td class="r-text" width="10%">
				<span class="r-text-detalle"><?php echo $exentaTotal; ?></span>
			</td>

			<td class="r-text" width="10%">
				<span class="r-text-detalle"><?php echo $iva5Total; ?></span>
			</td>

			<td class="r-text" width="10%">
				<span class="r-text-detalle"><?php echo $iva10Total; ?></span>
			</td>

		</tr>
	
	</table>


	<table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid;border-spacing: 0px;">
		<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
		</colgroup>
		<tr>
			<td style="width: 10%; padding-top: 7px;" class="f-9">
				<b>SUB-TOTALES:</b>
			</td>

			<td class="c-text f-9" colspan="2" style="width: 40%; padding-top: 7px;">
					********** Esta Factura será cancelada en <?php echo $moneda;?> **********
			</td>
				
			<td style="width: 10%;border-left: 1px solid;border-right: 1px solid;padding: 0 0 0 0; padding-top: 7px;" class="r-text f-9">
				<span class="r-text-detalle"><?php echo $exentaTotal; ?></span>
			</td>

			<td style="width: 10%;border-right: 1px solid;padding: 0 0 0 0; padding-top: 7px;" class="r-text f-9">
				<span class="r-text-detalle"><?php echo $iva5Total; ?></span>
			</td>
			</td>

			<td style="width: 10%;padding: 0 0 0 0; border-bottom: 1px solid #ffffff; padding-top: 7px;" class="r-text f-9">
				<span class="r-text-detalle"><?php echo $iva10Total; ?></span>
			</td>
			</td>

		</tr>

	</table>

	<table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; border-spacing: 0px;">
		<tr>
			<td class="f-10" style="width:625px;border-right: 1px solid;border-bottom: 1px solid;">
				<b>TOTAL DE FACTURA:</b> &nbsp;&nbsp;son <?php echo $moneda."    ".$letraTotal;?> 
			</td>
			<td class="f-10" rowspan="3" style="border-top: 1px solid #ffffff;padding-left: 15%;">
				<span class="c-text"><?php echo $total;?></span>
			</td>
		</tr>
		<tr>
			<td class="f-10" style="border-right: 1px solid;">
				&nbsp;&nbsp;
			</td>
		</tr>
	</table>

	<table style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid;">
		<tr>
			<td class="f-10" >
				<b>LIQUIDACÍÓN IVA 10%:</b> <span> <?php echo $liquidacionIva10; ?></span>
			</td>	
			
			<td class="f-10">
				<b>LIQUIDACÍÓN IVA 5%:</b> <span> 0,00</span>
			</td>
			
			
			<td class="f-10">
				<b>TOTAL IVA:</b> <span> <?php echo $liquidacionIva10; ?></span>
			</td>
			@if($factura[0]->id_tipo_facturacion == 1)
			<td class="f-10">
				<b>COM .AG : </b> <span><?php echo formatMoney($factura[0]->total_comision,$idMonedaVenta);?></span>
			</td>
			@endif
			
		</tr>
		
	</table>

	
<table style="border:1px solid; margin-top: 5px;">
	<tr>
		<td class="f-5" style="padding: 5px;">
			{!! $pieFactura_txt !!}

		</td>
	</tr>
</table>
 @php 
if($contadorRecorrido == 1){
	if($contadorDetalle <= 8){
		if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_impresion == 2){
			echo '<div style="page-break-after:always;"></div>';
		}else{
			echo '<br>     ----------------------------------------------------------------------------------------------------------------------------------';		
		}
	}else{
			echo '<div style="page-break-after:always;"></div>';
	}
}elseif($contadorRecorrido == 2&& Session::get('datos-loggeo')->datos->datosUsuarios->tipo_impresion != 1){
	echo '<div style="page-break-after:always;"></div>';
}
 @endphp
</div>

<?php 
//RESETEO CONTADOR
$contadorDetalles = 0;
?>

	{{--==============================================================
						FIN DE FACTURA
		==================================================================  --}}
	<?php
	//RESETEO CONTADOR
	$contadorDetalles = 0;
	?>


@endforeach


</body>
</html>
