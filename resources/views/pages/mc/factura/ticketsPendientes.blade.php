
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	
	
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#buttonsSuc {
		display: inline;
	}
	#buttonsSuc .dt-buttons {
		display: inline;
	}

	#buttonsVend {
		display: inline;
	}
	#buttonsVend .dt-buttons {
		display: inline;
	}

	.color-danger {
	  color: red;
	  font-weight: bold;
	/*  background-color: #a94442;
	  border-color: #ebccd1;*/
	}






</style>


<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Reporte Tickets Pendientes</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<form id="consultaFactura" autocomplete="off" method="post">
					<div class="row">
						<div class="col-12">
							<div class="alert  alert-light" role="alert">
								<b>No incluye tickets de grupos.</b>
							   </div>
						</div>
	
					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Cliente</label>
							<select class="form-control select2" name="idCliente" id="idCliente" tabindex="1"
								style="width: 100%;">
								<option value="">Todos</option>
								@foreach($clientes as $cliente)
									@php
										$ruc = $cliente->documento_identidad;
										if($cliente->dv){
											$ruc = $ruc."-".$cliente->dv;
										}
									@endphp
									<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->full_data}}</option>
								@endforeach
							</select>
						</div>
					</div>


					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Moneda</label>
							<select class="form-control select2" name="idMoneda" id="idMoneda" tabindex="2"
								style="width: 100%;">

								<option value="">Todos</option>
								@foreach($divisas as $divisa)
								<option value="{{$divisa->currency_id}}">{{$divisa->currency_code}}</option>
								@endforeach

							</select>
						</div>
					</div>


					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Fecha Facturación Desde - Hasta</label>
							<div class="input-group">
								<div class="input-group-prepend" style="">
									<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								</div>
								<input type="text" class="form-control pull-right fecha" name="facturacion_desde_hasta"
									 value="" tabindex="3" maxlength="30">
							</div>
						</div>
					</div>


					<div class="col-6 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Nro. Factura</label>
							<input type="text" class="form-control" name="numFactura" maxlength="30" value="" tabindex="4">
						</div>
					</div>



					<div class="col-6 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Nro. Proforma</label>
							<input type="number" class="form-control" name="numProforma" maxlength="30" tabindex="5" value="">
						</div>
					</div>
					<div class="col-12">
						<button type="button" id="botonExcel" class="pull-right btn btn-success btn-lg mr-1"><b>Excel</b></button>		
						<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
						<button onclick="consultaAjax()" class="pull-right btn btn-info btn-lg mr-1"
							type="button"><b>Buscar</b></button>
					</div>
			</div>
			<!--row-->
				</form>


			<div class="table-responsive table-bordered mt-1">
				<table id="listado" class="table" style="width: 100%;">
				<thead>
						<tr>
							<th>Proforma</th>
							<th>Factura</th>
							<th>Factura</th>
							<th>Fecha Facturacion</th>
							<th>Fecha BSP</th>
							<th>Cliente</th>
							<th>Moneda</th>
							<!--<th>Monto Factura</th>
						<th>Saldo Factura</th>-->
							<th>Seña</th>
							<th>Saldo Ticket</th>
							<th>Usuario Emision</th>
							<th>Usuario Proforma</th>
						</tr>
					</thead>					
					<tbody id="lista_cotizacion" style="text-align: center">



					</tbody>
				</table>
			</div>


		</div>
	</section>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/dataTables.buttons.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jszip.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript">


	/**
	 * Convierte un texto de la forma 2017-01-10 a la forma
	 * 10/01/2017
	 *
	 * @param {string} texto Texto de la forma 2017-01-10
	 * @return {string} texto de la forma 10/01/2017
	 *
	 */
	function formatoFecha(texto){
			if(texto != '' && texto != null){
		  return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
			} else {
		  return '';	
				
			}

		}



			/*
	Para formatear numeros a moneda USD PY
	 */
	const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD',
					  minimumFractionDigits: 2
					});

		function consultaAjax(){

		$.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });

		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){

					
			var table =   $('#listado').DataTable({
					        "destroy": true,
					        "select": true,
					        "ajax": {
					        		data: { "formSearch": $('#consultaFactura').serializeArray() },
						            url: "{{route('ajaxReporteTicketsPendientes')}}",
						            type: "GET",
						    error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                            $.unblockUI();
                        }
                        		// formatter.format(parseFloat(x.total_venta_proforma));
						  },

						  

						 "columns":[ {data: "id_proforma"},
						 			 {data: "nro_factura"},
									 {data: function(x){
									 	 link_fact = `<a href="{{route('verFactura',['id'=>''])}}/${x.id_factura}"  style="color:inherit; font-weight: bold; ">
						        	<i class="fa fa-fw fa-search"></i>${x.nro_factura}</a>`;
						        	return link_fact;
									 	}},

									 {data: function(x){
									 	 let fecha = x.fecha_hora_facturacion;
									 	     fecha = fecha.split(" ");
									 	     return formatoFecha(fecha[0]);
									 	}},
									 {data: function(x){
									 	 let fecha = x.fecha_bsp;
									 	     return formatoFecha(x.fecha_bsp);
									 	}},
									 {data: "nombre"},
									 {data: "currency_code"},
									 {data: function(x){
									 	 return formatter.format(parseFloat(x.total_cobrado))
									 	}},
									 {data: function(x){
									 	 return formatter.format(parseFloat(x.saldo_ticket))
									 	}},
									 {data: "usuario_emision"},
									 {data: function(x){
											nombre = x.proforma_nombre;
											if(jQuery.isEmptyObject(x.proforma_apellido) == false){
												nombre += ' '+x.proforma_apellido;
											}
									 	     return nombre;
									 	}}

								],"columnDefs": [
												    {
												        targets: 0,
												        className: 'hover'
												    },
												    {
										                "targets": [ 1 ],
										                "visible": false,
										                "searchable": false
										            },
										            { "type": "datetime", "targets": [3] }
												  ],
												  "aaSorting":[[2,"desc"]],
						     "createdRow": function ( row, data, index ) {
											     
											     		var date_2 = new Date(data['fecha_bsp']);
														var date_1 = new Date(fechaActual());

														var day_as_milliseconds = 86400000;
														var diff_in_millisenconds = date_2 - date_1;
														var diff_in_days = diff_in_millisenconds / day_as_milliseconds;
											        	$(row).addClass('negrita');
											        	if(jQuery.isEmptyObject(data['fecha_bsp']) == false){
												        	if(diff_in_days < 7 ){
												        		$(row).addClass('color-danger');
												        	}
												        	
											        	}				        	
						        				}			
						                    
					    }).on('xhr.dt', function ( e, settings, json, xhr ){
								//Ajax event - fired when an Ajax request is completed.;
							 $.unblockUI();
								});
					    
			   buttonSuc([0,1,3,4,5,6,7,8],table);


					    	}, 300);


			

}//function

	$("#botonExcel").on("click", function(e){
	    e.preventDefault();
	    $('#consultaFactura').attr('action', "{{route('generarExcelTicketsPendientes')}}").submit();
	});	

		function buttonSuc(column,tableSuc){



					/*$('#buttonsSuc').html('');
						var buttons = new $.fn.dataTable.Buttons(tableSuc, { 
							    buttons: [{	
							    						title: 'Reporte Tickets Pendientes',
										                extend: 'excelHtml5',
										                 text: 'Exportar a Excel',
										                 className: 'btnExcel',
								                exportOptions: {
								                    columns: ':visible'
								                },
								                exportOptions: {
									                    columns:  column,
									                    format: {
									                    		//seleccionamos las columnas para dar formato para el excel
												                body: function ( data, row, column, node ) {
												                	if(column != 0 &&
												                	   column != 1 &&
												                	   column != 2 &&
																	   column != 3 &&
																	   column != 4 &&
												                	   column != 5  ){
												                		if(data != '0')
																			data = replaceAll(data, ".", "" );
																			//cambiamos la coma por un punto
																			if(data != '0')
																			data = data.replace(",",".");
																		//	alert(formatter.format(parseFloat(data)));
																			//listo
																			var numFinal = parseFloat(data);
													                    
												                    return  numFinal;
												                		} 
												                			return data;
												                		
												                }
												            }
									                },
									                //Generamos un nombre con fecha actual
									                filename: function() {
										               var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
										               // var selected_machine_name = $("#output_select_machine select option:selected").text()
										               return 'ReporteTicketPendientes-'+date_edition;
										           }

								            }]
							}).container().appendTo('#buttonsSuc'); */
				}


	// $("#listado").dataTable({
	// 			 "aaSorting":[[0,"desc"]]
	// 			});
	$('.select2').select2();



		$(function() {

  $('input[name="facturacion_desde_hasta"]').daterangepicker({
												      autoUpdateInput: false,
												     	locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']

													    },
  });

  $('input[name="facturacion_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
  });

  $('input[name="facturacion_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});


		function limpiar(){
			document.getElementById("consultaFactura").reset();
			$('#idCliente').val('').trigger('change.select2');
			$('#idMoneda').val('').trigger('change.select2');

		}
		
		function fechaActual(){	
			var d = new Date();
			var month = d.getMonth()+1;
			var day = d.getDate();
			var output = d.getFullYear() + '-' +
			    (month<10 ? '0' : '') + month + '-' +
			    (day<10 ? '0' : '') + day;
			return output;
		}	    

	function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}


</script>

@endsection