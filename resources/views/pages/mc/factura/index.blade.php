
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
	
	.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
	}	
	.bgReda{
		font-size: 15px;
		color: red;
		font-weight: 800;
		white-space: nowrap;
	}

</style>
	@parent
@endsection

@section('content')
<section id="base-style">
  @include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Listado Facturas</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	<form id="consultaFactura">
					<div class="row">
			            <div class="col-12 col-sm-3 col-md-3">
					        <div class="form-group">
					            <label>Cliente</label>
								<select class="form-control" name="cliente_id"  id="cliente" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
								</select>
					        </div>
			            </div>
			            <div class="col-12 col-sm-3 col-md-3">
					        <div class="form-group">
					            <label>Moneda</label>
								<select class="form-control select2" name="idMoneda"  id="moneda" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($monedas as $moneda)
									<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
									@endforeach
								</select>
					        </div>
			            </div>

					    <div class="col-12 col-sm-3 col-md-3">
		 					<div class="form-group">
					            <label>Desde / Hasta Fecha de Facturación.</label>			
					            	<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text"  class = "Requerido form-control input-sm" id="periodo" name="periodo" tabindex="10"/>
								</div>
							</div>	
						</div> 
						

						<div class="col-12 col-sm-3 col-md-3">  
		 					<div class="form-group">
								<label>Nro. Proforma</label>
								<input type="number" class="form-control" id="nro_proforma" name="numProforma" value="" >
							</div>	
						</div> 
				    </div>  
				    <!-- row-->
					<div class="row">
			            <div class="col-12 col-sm-3 col-md-3">
					        <div class="form-group">
					            <label>Tipo Factura</label>
								<select class="form-control select2" name="tipoFactura"  id="tipo_factura" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($tipoFactura as $tipo)
									<option value="{{$tipo->id}}">{{$tipo->denominacion}}</option>
									@endforeach
								</select>
					        </div>
			            </div>


			             <div class="col-12 col-sm-3 col-md-3">
					        <div class="form-group">
					            <label>Estado</label>
								<select class="form-control select2" name="tipoEstado"  id="tipo_estado" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									
									@foreach($tipoEstado as $estado)
									<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
									@endforeach
								</select>
					        </div>
			            </div>

			            <div class="col-12 col-sm-3 col-md-3">
					        <div class="form-group">
					            <label>Vendedor</label>
								<select class="form-control select2" name="vendedor_id"  id="vendedor_id" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($vendedorEmpresa as $vendedor)
										@if(isset($vendedor->vendedorEmpresa['nombre']))
										<option value="{{$vendedor->vendedorEmpresa['id']}}">{{$vendedor->vendedorEmpresa['nombre']}}@if($vendedor->vendedorEmpresa['apellido'] !==""){{$vendedor->vendedorEmpresa['apellido']}}@endif @if(isset($vendedor->vendedorEmpresa['denominacion_comercial'])) - {{$vendedor->vendedorEmpresa['denominacion_comercial']}}@endif</option>
										@endif	
									@endforeach
								</select>
					        </div>
			            </div>

			           <div class="col-6 col-sm-3 col-md-3">
						 <div class="form-group">
						 	 <label>Nro. Factura</label>
						     <input type="text" class="form-control" id="num_factura"  name="numFactura" value="" maxlength="30">
							</div>
						</div>
				    </div> 
				    <!--row--> 
				    <div class="row">
						<div class="col-md-3">
						  <div class="form-group">
					            <label>Impreso</label>
								<select class="form-control select2" name="impreso"  id="impreso" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									<option value="true">SI</option>
									<option value="false">NO</option>
									
								</select>
					        </div>
						</div>
						
						<div class="col-md-3">
						  <div class="form-group">
					            <label>Usuario</label>
								<select class="form-control select2" name="id_usuario"  id="id_usuario" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
							
								</select>
					        </div>
						</div>

						<div class="col-md-3">
						  <div class="form-group">
					            <label>Sucursal Facturación</label>
								<select class="form-control select2" name="id_sucursal"  id="id_sucursal" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach ($sucursals as $sucursal)
										<option value="{{$sucursal->id}}">{{$sucursal->nombre}}</option>
									@endforeach
								</select>
					        </div>
						</div> 

						<div class="col-md-3">
							<div class="form-group">
								<label>Grupos  </label></label>
								<select class="form-control select2" name="grupo_id" id="grupo_id" tabindex="2" style="width: 100%;" required>
									<option value="">Todos</option>
									@foreach($grupos as $key=>$grupo)
										<option value="{{$grupo->id}}"><b>{{$grupo->denominacion}}</b></option>
									@endforeach	
								</select>
							</div>
						</div>
					</div>	
					<!--row--> 
					<div class="row">
						<!--<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Forma Pago  </label></label>
									<select class="form-control select2" name="forma_pago" id="forma_pago"  style="width: 100%;">
										<option value="">Todos</option>
										<option value="true"><b>SI</option>
										<option value="false"><b>NO</option>
									</select>
							</div>
						</div>-->
						<div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
						            <label>Negocio</label>
									<select class="form-control select2" name="negocio"  id="negocio" style="width: 100%;" tabindex="14">
										<option value="">Todos</option>
										@foreach ($negocios as $negocio)
											<option value="{{$negocio->id}}">{{$negocio->descripcion}}</option>
										@endforeach
									</select>
						        </div>
							</div>		

						<div class="col-12 col-sm-4 col-md-3">
					        <div class="form-group">
					            <label>Tipo Persona</label>
								<select class="form-control select2" name="tipo_persona_id[]" multiple="multiple" id="tipo_persona_id" tabindex="5" style="width: 100%;">
									@foreach($tipoPersona as $persona)
										@if($persona->id == 8)
											<option value="{{$persona->id}}" selected="selected">{{$persona->denominacion}}</option>
										@else
											<option value="{{$persona->id}}">{{$persona->denominacion}}</option>
										@endif	
									@endforeach
								</select>
					        </div>
						   </div>
						   
						   <div class="col-12 col-md-3">
							<div class="form-group">
								  <label>Tipo Venta</label>
								  <select class="form-control select2" name="tipo_venta"  id="tipo_venta" style="width: 100%;">
									  <option value="">Todos</option>
									  <option value="1">Proforma</option>
									  <option value="2">Venta Rapida</option>
									  
								  </select>
							  </div>
						  </div>
						  <div class="col-md-3">
				                <label class="control-label">File/Código</label>
				                <input type="text" class = "Requerido form-control" name="file_codigo" id="file_codigo" value="" tabindex="11">
						  </div>
						  <div class="col-md-3">	
								<div class="form-group">
						            <label>Destino (*)</label>
									<select class="form-control select2" name="destino"  id="destino" style="width: 100%;" tabindex="13">
										<option value="">Todos</option>
									</select>
						        </div>
						   </div>	
						   <div class="col-md-3">
								<div class="form-group">
						            <label>Asistente</label>
						            <select class="form-control select2" name="id_asistente" id="id_asistente" style="width: 100%;" tabindex="8">
										<option value="">Todos</option>
										@foreach($asistentes as $usuario)
												<option value="{{$usuario->id}}">{{$usuario->nombre}}  {{$usuario->apellido}}</option>
											@endforeach
						            </select>
					        	</div>					
					    	</div>	
							<div class="col-md-3">
								<div class="form-group">
						            <label>Equipo</label>
						            <select class="form-control select2" name="id_equipo" id="id_equipo" style="width: 100%;" tabindex="8">
										<option value="">Todos</option>
										@foreach($equipos as $equipo)
												<option value="{{$equipo->id}}">{{$equipo->nombre_equipo}}</option>
											@endforeach
						            </select>
					        	</div>					
					    	</div>	
							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
								   <label>Desde / Hasta Fecha Checkin</label>			
									   <div class="input-group">
									   <div class="input-group-prepend" style="">
										   <span class="input-group-text"><i class="fa fa-calendar"></i></span>
									   </div>
									   <input type="text"  class ="form-control input-sm" id="checkin" name="checkin" tabindex="10"/>
								   </div>
							   </div>	
						   </div> 
						
					</div>		
					<!--row--> 	
					<div class="row">
							
						<div class="col-12">
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white mr-1"><b>Limpiar</b></button>
							<button  onclick ="consultaFacturaServerSide()" class="pull-right btn btn-info btn-lg mr-1" type="button"><b>Buscar</b></button>

				    	</div>	
					</div>	
            	</form>
				<div class="row">
					<div class="col-12 col-md-1">
						<b style="color:red;" style="padding-left: 30px;margin-bottom: 10px;">Autorizado</b>
					</div>
					<div class="col-12 col-md-3"></div>
				</div>	
				<br>
            	<div class="table-responsive table-bordered font-weight-bold">
                  <table id="listado" class="table" style="width: 100%;">
                    <thead>
                      <tr>
                        <th>Factura</th>
                        <th>Fecha</th>
                        <th>Checkin</th>
						<th>Vencimiento</th>
                        <th>Proforma</th>
                        <th>Estado</th>
                        <th>Monto</th>
                        <th>Moneda</th>
                        <th>Cliente</th>
                        <th>Pasajero</th>
                        <th>Usuario</th>
						<th>Destino</th>
						<th>Comisión</th>
                        <th>%Renta</th>
                        <th>R.Bruto</th>
						<th>R.Extra</th>
						<th>Incentivo</th>
						<th>R.Neta</th>
                        <th>Sucursal</th>
                        <th>Vendedor</th>
						<th>Negocio</th>
						<th>Promoción</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody style="text-align: center">
                    </tbody>
                  </table>
                </div>

            </div>
        </div>
    </div>
</section>           	

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript">

		var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};
	
	$(document).ready(function(){

		$('.select2').select2({
			language: lang_es_select2
		});

		$("#cliente").select2({
				language: lang_es_select2,
		        ajax: {
		                url: "{{route('get.clientes')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
							console.log(params);
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });

		$("#id_usuario").select2({
				language: lang_es_select2,
		        ajax: {
		                url: "{{route('get.usuariosProformas')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {

		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });

			$("#destino").select2({
				language: lang_es_select2,
		        ajax: {
		                url: "{{route('get.destinos')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });

		let select_2 = $('#tipo_persona_id').select2({
										multiple:true,
										//maximumSelectionLength: 2,
										placeholder: 'Todos',
										language: lang_es_select2,
									});

		var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('input[name="periodo"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });
//para checking
$('input[name="checkin"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="checkin"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });

				  $('#checkin').val('');

	});//DOCUMENT READY





	$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                 $('#consultaFactura').attr('method','post');
               $('#consultaFactura').attr('action', "{{route('generarExcelReporteFactura')}}").submit();
            });


			function validar(data){

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}

		function limpiar(){
			$('#cliente').val('').trigger('change.select2');
			$('#moneda').val('').trigger('change.select2');
			$('#periodo').val('');
			$('#checkin').val('');
			$('#nro_proforma').val('');
			$('#tipo_factura').val('').trigger('change.select2');
			$('#tipo_estado').val('').trigger('change.select2');

			$('#vendedor_id').val('').trigger('change.select2');
			$('#num_factura').val('');
			$('#impreso').val('').trigger('change.select2');
			$('#id_usuario').val('').trigger('change.select2');
			$('#id_sucursal').val('').trigger('change.select2');
			$('#grupo_id').val('').trigger('change.select2');
			$('#forma_pago').val('').trigger('change.select2');
			

			// consultaFactura();
			//  consultaFacturaServerSide();
		}



	   function consultaFacturaServerSide() 
	   {

				var btn;
				var pasajero;
				var avion;

				$.blockUI({
												centerY: 0,
												message: "<h2>Procesando...</h2>",
												css: {
													color: '#000'
												}
											});

					// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
				setTimeout(function ()
				{

					//Destruir dataTable	
					// $("#listado").dataTable().fnDestroy();	

					$("#listado").dataTable({
						"searching": false,
						"processing": true,
						"serverSide": true,
						destroy:true,
						
						"ajax": {
						"url": "{{route('consultaFacturaServerSide')}}",
						"data": $('#consultaFactura').serializeJSON() }
						,
						"columns":[
									{data: function(x){
										btn = `<div class="row" style="width: 220px;">`;
										if(x.total_ticket > 0){
											btn += '<div class="col-2" style="padding-left: 15px;padding-right: 0px;"><i class="fa fa-plane fa-lg" style="color: #00acd6;font-size: 1.8em;" aria-hidden="true"></i></div>';
										}else{
											btn += '<div class="col-2"></div>';
										} 
										if(x.id > 89999){
											if(x.id_venta_rapida != null){
												btn += `<div class="col-10" style="padding-left: 0px;"><a href="{{route('verFacturas',['id'=>''])}}/${x.id}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${x.nro_factura}</a><div style="display:none;">${x.id}</div></div>`;
											} else {
												btn += `<div class="col-10" style="padding-left: 0px;"><a href="{{route('verFactura',['id'=>''])}}/${x.id}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${x.nro_factura}</a><div style="display:none;">${x.id}</div></div>`;
											} 
												
										} else {
												btn += `<div class="col-10" style="padding-left: 0px;"><a class="bgRed">${x.nro_factura}</a><div style="display:none;">${x.id}</div></div>`;
										}
										btn += `</div>`;
									return btn; 
									}},
									{data: "fecha_format"},
									{data: "check_in_format"},
									{data: "vencimiento_format"},
									{data: "id_proforma"},
									{data: "denominacion"},
									{data: "monto_factura"},
									{data: "currency_code"},
									{data: "cliente_n"},
									{data: function(x) {
										pasajero = '';
										if(x.id_pasajero_principal == '4994'){
											pasajero += x.pasajero_online
										} else { 
											if(x.pasajero_n== " "){
												pasajero += x.pasajeros_nom;
											}else{
												pasajero += x.pasajero_n;
											}
										}
										return pasajero;
									}},
									{data: "usuario_n"},
									{data: "desc_destino"},
									{data: "total_comision"},
									{data: "porcentaje_ganancia"},
									{data: "ganancia_venta"},
									{data: "renta_extra"},
									{data: function(x){
										resultado = 0;
										if(x.incentivo_vendedor_agencia != null){
											resultado = x.incentivo_vendedor_agencia
										} 
										return resultado;
									}},
									{data: "renta"},
									{data: "suc_n"},
									{data: "vendedor_n"},
									{data: "negocio"},
									{data: "promocion"},
									{data: function(x){
										empresa = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
										btn = '';
										if(parseInt(empresa) == 1){
												if(jQuery.isEmptyObject(x.btn[0]) == false){
													if(x.id_venta_rapida == null){
														btn += x.btn[0];	
													}else{
														btn += btn = `<a href="{{route('imp_fact_pedido',['id'=>''])}}/${x.id}"  class="btn btn-success text-white pull-right imprimirFactStyle" title="Imprimir Factura"><i class="fa fa-print fa-lg"></i></a><div style="display:none;">${x.id}</div>`;
													}		
												}else{
													btn = "";
												}
										}else{
											if(x.id_venta_rapida == null){
												btn += `<a href="{{route('imprimirFactura',['id'=>''])}}/${x.id}"  class="btn btn-success text-white pull-right imprimirFactStyle" title="Imprimir Factura"><i class="fa fa-print fa-lg"></i></a><div style="display:none;">${x.id}</div>`;
											}else{
												btn += btn = `<a href="{{route('imp_fact_pedido',['id'=>''])}}/${x.id}"  class="btn btn-success text-white pull-right imprimirFactStyle" title="Imprimir Factura"><i class="fa fa-print fa-lg"></i></a><div style="display:none;">${x.id}</div>`;
										    }		
										}		
										return btn;
										
									}}
								],
							"aaSorting":[[0,"desc"]],	   
							'rowCallback': function(row, data, index){
											// console.log(data['proforma_autorizada']);
											if(data['proforma_autorizada'] == "SI"){
												$(row).addClass('bgReda');
											}
										}
						}).on('xhr.dt', function ( e, settings, json, xhr ){
										//Ajax event - fired when an Ajax request is completed.;
									$.unblockUI();
						});


				}, 300);


					// $.unblockUI();

	
		}






	$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                 $('#consultaFactura').attr('method','post');
               $('#consultaFactura').attr('action', "{{route('generarExcelReporteFactura')}}").submit();
            });

</script>

@endsection
