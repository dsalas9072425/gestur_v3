
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')


<!-- Main content -->
    <section class="content"> 
		        <div class="box">
		            <div class="box-header">
		             <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">PAGINA DE ERROR</h1>
		            </div>    	
						    <!-- SELECT2 EXAMPLE -->
						    <div class="box box-default" style="border-top-width: 1px;margin-bottom: 0px;">
						        
						        <!-- /.box-header -->
						        <div class="box-body">
							      	<!-- /.box -->
						         
								<div class="box-header with-border">
						          <h3 class="box-title">La factura no se puede imprimir</h3>
						          <h1>{{$mensajeErr}}</h1>
						        </div>                                                
						    

								    

								</div>
				          	</div>
				            <!-- /.box -->
						    <!-- SELECT2 EXAMPLE -->
						   
				          	
						</div>	
		          <!-- /.box -->
			    </div>
		        <!-- /.col -->	
    </section>
    <!-- /.content -->














@endsection
@section('scripts')
	@include('layouts/mc/scripts')

@endsection