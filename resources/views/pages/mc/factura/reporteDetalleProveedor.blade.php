
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/vendors/css/tables/datatable/select.dataTables.min.css')}}">
@endsection
@section('content')

<style type="text/css">
	

	.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

.card,.card-header {
	border-radius: 14px !important;
}

</style>


<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Reporte Detalles Pago Proveedor</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

				<form id="consultaFactura" method="post">
					<div class="row">

						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Proveedor</label>
								<select class="form-control select2" name="idProveedor" id="proveedor" tabindex="1"
									style="width: 100%;">
									<option value="0">Seleccione Proveedor</option>

									@foreach($getProveedor as $proveedor)
									@if($proveedor->activo == true)
									@php
									$activo = 'ACTIVO';
									@endphp
									@else
									@php
									$activo = 'INACTIVO';
									@endphp
									@endif
									<option value="{{$proveedor->id}}">{{$proveedor->nombre}} <b>({{$activo}})</b>
									</option>
									@endforeach

								</select>
							</div>
						</div>

						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Proforma</label>
								<input type="text" class="form-control" id="idProforma" name="idProforma" value="">
							</div>
						</div>

						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Nro. Factura</label>
								<input type="text" class="form-control" id="nroFactura" name="nroFactura" value="">
							</div>
						</div>

						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Codigo</label>
								<input type="text" class="form-control" id="codigoConfirmacion"
									name="codigoConfirmacion" value="">
							</div>
						</div>



						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Desde / Hasta Fecha de Facturación.</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="checkin_desde_hasta"
										id="checkin_desde_hasta" value="">
								</div>
							</div>
						</div>






						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Fecha Proveedor Desde - Hasta</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha"
										name="proveedor_desde_hasta" id="proveedor_desde_hasta" value="">
								</div>
							</div>
						</div>


						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Moneda</label>
								<select class="form-control select2" name="idMoneda" id="idMoneda" tabindex="1"
									style="width: 100%;">
									<option value="">Seleccione Moneda</option>
									@foreach($monedas as $moneda)
									<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
									@endforeach

								</select>
							</div>
						</div>

						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Pagado </label>
								<select class="form-control select2" name="tieneFechaProveedor" id="tieneFechaProveedor"
									tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									<option value="true">SI</option>
									<option selected="selected" value="false">NO</option>


								</select>
							</div>
						</div>

						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Tipo Negociación</label>
								<select class="form-control select2" name="tipo_negociacion" id="tipo_negociacion"
									tabindex="1" style="width: 100%;">
									<option value="">Seleccione una Opción</option>
									@foreach ($plazoPago as $plazo)
									<option value="{{$plazo->n}}">{{$plazo->n}}</option>
									@endforeach


								</select>
							</div>
						</div>

						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Número de operación</label>
								<input type="text" class="form-control" id="numOperacion" name="numOperacion" value="">
							</div>
						</div>


						<div class="col-xs-12 col-sm-12 mb-2">
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg"><b>Excel</b></button>
							<button type="button" id="pagoOpercion"
								class="btn btn-info btn-lg pull-right mr-1"><b>Aplicar OP</b></button>
							<button type="button" onclick="limpiar()" id="btnLimpiar"
								class="btn btn-light btn-lg pull-right mr-1"><b>Limpiar</b></button>
							<button type="button" onclick="consultaFactura()"
								class="btn btn-info btn-lg pull-right mr-1"><b>Buscar</b></button>
						</div>


					</div>
				</form>



				<div class="table-responsive table-bordered">
					<table id="listado" class="table table-hover" style="width: 100%;">
						<thead>
							<tr>
								<th></th>
								<th>ID</th>
								<th></th>
								<th>Proforma</th>
								<th>Factura</th>
								<th>Fecha Facturación</th>
								<th>Checkin</th>
								<th>Vendedor</th>
								<th>Pasajero</th>
								<th>Monto</th>
								<th>Codigo</th>
								<th>Proveedor</th>
								<th>Moneda</th>
								<th>Producto</th>
								<th>Fecha Proveedor</th>
								<th>Pagado</th>
								<th>Fecha OP</th>
								<th>OP</th>
								<th>Adjuntos</th>
							</tr>
						</thead>
						<tbody style="text-align: center">
						</tbody>
					</table>
				</div>





			</div>
		</div>
	</section>
</section>
	
 <!-- ========================================
   					MODAL ADJUNTOS
   		========================================  -->		
	<div id="modalAdjunto" class="modal fade" role="dialog">
		<div class="modal-dialog" style="width: 60%;margin-top: 5%;">
			<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto</h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
					<div class="container-fluid">

						<div class="row" id='output'>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>



	<div id="modalOperacionPago" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Cargar número de operación</h2>
					<span id="mensaje_c"></span>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
					
					<div class="row" id="vista_1">
						<div class="col-12">
							<div class="form-group">
								<label>Número Operación <span class="cargandoImg" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
								</label>
								<input type="text" class="form-control" value="" id="num_operacion" />
							</div>
						</div>



						<div class="col-12">
							<button type="button" id="btnProcesarOperacion" class="btn btn-success">Aceptar</button>
						</div>
					</div>



					<div class="row" id="vista_2" style="display:none;">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Cantidad de items procesados <span class="cargandoImg" style="display: none;">
										<i class="fa fa-refresh fa-spin"></i></span>
								</label>
								<input type="text" class="form-control" value="" id="procesado_op" readonly="readonly" />
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Cantidad de items Confirmados <span class="cargandoImg" style="display: none;">
										<i class="fa fa-refresh fa-spin"></i></span>
								</label>
								<input type="text" class="form-control" value="" id="confirmado_op" readonly="readonly" />
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Total <span class="cargandoImg" style="display: none;">
										<i class="fa fa-refresh fa-spin"></i></span>
								</label>
								<input type="text" class="form-control" value="" id="total_monto_op" readonly="readonly" />
							</div>
						</div>

					
					</div>
				</div>

				</div>

				<div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				</div>

			</div>
		</div>
	</div>
   
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/js/dataTables.select.min.js')}}"></script> --}}
	<script src="{{asset('gestion/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')}}"></script>
<script type="text/javascript">

	//$("#listado").dataTable();

	//MANEJAR DATOS OP PROCESADAS
		const data = {
			idMoneda : '',
			idProveedor : '',
			items : [],
			setMoneda: function(d){this.idMoneda = d;},
			getMoneda: function(){return this.idMoneda;},
			setProveedor: function(d){this.idProveedor = d;},
			getProveedor: function(){return this.idProveedor;},
			clearData:function(){this.idMoneda = '';
								 this.idProveedor = '';},
			setItems: function(d){this.items = d;},
			getItems: function(){return this.items;}					 

		};
	
	$(function() {

		

  $('input[name="proveedor_desde_hasta"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Limpiar'
      }
  });

  $('input[name="proveedor_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
  });

  $('input[name="proveedor_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });





  $('input[name="checkin_desde_hasta"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Limpiar'
      }
  });

  $('input[name="checkin_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
  });

  $('input[name="checkin_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });






sumaFecha = function(d, fecha)
		{
		 var Fecha = new Date();
		 var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
		 var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
		 var aFecha = sFecha.split(sep);
		 var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
		 fecha= new Date(fecha);
		 fecha.setDate(fecha.getDate()-parseInt(d));
		 var anno=fecha.getFullYear();
		 var mes= fecha.getMonth()+1;
		 var dia= fecha.getDate();
		 mes = (mes < 10) ? ("0" + mes) : mes;
		 dia = (dia < 10) ? ("0" + dia) : dia;
		 var fechaFinal = dia+sep+mes+sep+anno;
		 return (fechaFinal);
		 }

});

$('#pagoOpercion').on('click',function(){

	$('#num_operacion').val('');
	$('#mensaje_c').html('');
	$('#vista_1').show();
	$('#vista_2').hide();
	var n = document.getElementsByClassName('selected');
	console.log(n);

	if(n.length != 0){

	if(data.getMoneda() == '' || data.getProveedor() == ''){
			$.toast({
			    heading: 'Atención',
			    position: 'top-right', 
			    text: 'Debe seleccionar un proveedor, una moneda y hacer la busqueda.',
			    showHideTransition: 'slide',
			    icon: 'info'
			});

	} else {
		
		$("#modalOperacionPago").modal("show");	
	}

	} else {

		$.toast({
    heading: 'Atención',
    position: 'top-right', 
    text: 'Seleccione uno o varios item para ser procesados.',
    showHideTransition: 'slide',
    icon: 'info'
});

                           
	}


});

$('#btnProcesarOperacion').on('click',function(){

	$('.cargandoImg').show();
	$('#mensaje_c').html('');

		var ids = [];
		var n = document.getElementsByClassName('selected');
		var numOp = $('#num_operacion').val();
		console.log(n.length);		
		$.each(n, function(index, val) {
			var indice = $(this).attr('id');
		    s = $('#moneda_'+indice).val();
			console.log(s);
			ids.push(s);
		});

		data.setItems(ids);

		dataString = {
			listaId : ids,
			numOp : numOp
		}
		console.log('??????????????????????');
		console.log(dataString);
		console.log('??????????????????????');
		$.ajax({
						type: "GET",
						url: "{{ route('cancelarOperacion') }}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){
						 	$('#mensaje_c').html('Ocurrio un error en la comunicación con el servidor.');
						 	$('#mensaje_c').css('color','red');
						 	$('.cargandoImg').hide();

                          },

						success: function(rsp){
							$('.cargandoImg').hide();

							if(rsp.resp == 0){
							$('#mensaje_c').html('La operación se realizo con exito');
						 	$('#mensaje_c').css('color','green');

						 		$.each(n, function(index, val) {

						 			// var u = val.getElementsByTagName("tr");   
										// console.log(u);

									var s = val.getElementsByTagName("td");
			    					s = $('#moneda_'+index).val();
								     $('#'+s).prop('disabled',true);
								     $('#'+s).removeClass('select-checkbox');
								     $('#'+s).removeClass('selected');

								      var h = $('#'+s).parents('tr');
								  
								      console.log(h);


									
								});

							} else {

							$('#mensaje_c').html('Ocurrio un error en la operación');
						 	$('#mensaje_c').css('color','red');
							}


						}

					}).done(function(){

						$('.cargandoImg').show();
						$('#procesado_op').val('');
						$('#confirmado_op').val('');
						$('#total_monto_op').val('');
						$('#vista_1').hide();


						$.ajax({
						type: "GET",
						url: "{{ route('comprobarOP') }}",
						dataType: 'json',
						data: {lista:ids},

						 error: function(jqXHR,textStatus,errorThrown){
						 	$('#mensaje_c').html('Ocurrio un error en la comunicación con el servidor.');
						 	$('#mensaje_c').css('color','red');
						 	$('.cargandoImg').hide();
                          },

						success: function(rsp){
							$('.cargandoImg').hide();
							$('#vista_2').show();
							$('#procesado_op').val(rsp.resp.cantidadSeleccionada);
							$('#confirmado_op').val(rsp.resp.cantidadProcesada);
							$('#total_monto_op').val(rsp.resp.monto);

							var dataString = {
							  'idProveedor':$('#proveedor').val(),
							  'idProforma': $('#idProforma').val(),
							  'nroFactura': $('#nroFactura').val(),
							  'codigoConfirmacion' : $('#codigoConfirmacion').val(),
							  'checkin_desde_hasta' :$('#checkin_desde_hasta').val(),
							  'proveedor_desde_hasta': $('#proveedor_desde_hasta').val(),
							  'idMoneda' : $('#idMoneda').val(),
							  'tieneFechaProveedor' : $('#tieneFechaProveedor').val(),
							  'tipo_negociacion' : $('#tipo_negociacion').val(),
							  'numOperacion' : $('#numOperacion').val(),
							   };
							buscarPago(dataString);
						}



					});





});



});



	$('.select2').select2();

		
		function limpiar(){
			//reset campos input
			document.getElementById("consultaFactura").reset();
			$('#proveedor').val('').trigger('change.select2');
			$('#idMoneda').val('').trigger('change.select2');
			$('#tieneFechaProveedor').val('').trigger('change.select2');

		}
	
		
			var dataString = {
							  'idProveedor':$('#proveedor').val(),
							  'idProforma': $('#idProforma').val(),
							  'nroFactura': $('#nroFactura').val(),
							  'codigoConfirmacion' : $('#codigoConfirmacion').val(),
							  'checkin_desde_hasta' :$('#checkin_desde_hasta').val(),
							  'proveedor_desde_hasta': $('#proveedor_desde_hasta').val(),
							  'idMoneda' : $('#idMoneda').val(),
							  'tieneFechaProveedor' : $('#tieneFechaProveedor').val(),
							  'tipo_negociacion' : $('#tipo_negociacion').val(),
							  'numOperacion' : $('#numOperacion').val(),
							   };
	
			buscarPago(dataString);


			function validar(data){

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}

			function consultaFactura() {
				if($('#idProveedor').val() != '' && $('#idMoneda').val() != '') {
                    data.setProveedor($('#idProveedor').val());
                    data.setMoneda($('#idMoneda').val());
                } else {
                    data.clearData();
                }
				var dataString = {
							  'idProveedor':$('#proveedor').val(),
							  'idProforma': $('#idProforma').val(),
							  'nroFactura': $('#nroFactura').val(),
							  'codigoConfirmacion' : $('#codigoConfirmacion').val(),
							  'checkin_desde_hasta' :$('#checkin_desde_hasta').val(),
							  'proveedor_desde_hasta': $('#proveedor_desde_hasta').val(),
							  'idMoneda' : $('#idMoneda').val(),
							  'tieneFechaProveedor' : $('#tieneFechaProveedor').val(),
							  'tipo_negociacion' : $('#tipo_negociacion').val(),
							  'numOperacion' : $('#numOperacion').val(),
							   };
				buscarPago(dataString);
			}		
		
		function buscarPago(data){


			$.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });

		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){


			var table = $("#listado").DataTable({
						"searching": false,
						 "destroy": true,
						"processing": true,
						"serverSide": true,
						"ajax": {
						"url": "{{route('ajaxDetalleProveedor')}}",
					    "data": {
					           "dataString": data 
					    		}
					    },
						"columnDefs": [ 
								{
						            orderable: false,
						            className: 'select-checkbox',
						            targets:   0
						        },
						        {
			                        "targets": 1,
			                        "visible": false,
			                        "searchable": false
			                    },

						        ],
						"select": {
						            style: 'multi',
						            selector: 'td:first-child'
						        },
								"aaSorting":[[2,"desc"]],
								"createdRow": function ( row, data, iDataIndex ) {
									// $(row).attr('id', iDataIndex);
							
									$(row).attr('id', data[1]);
									console.log(data[14]);
									 var n = $(row, 'tr');
									//n[0].attr('id', data[6]);
									//$('tr', row).attr('id', data[6]);
									var n = $(row,'#select-checkbox');
						          if ( data[15] == 'SI') {
									 n = n[0].getElementsByClassName('select-checkbox');
									 n[0].classList.add('readOnly');
									 n[0].classList.remove('select-checkbox');
									//  // console.log(n);
									 
						         } 
					}
				}).on('xhr.dt', function ( e, settings, json, xhr ){
								//Ajax event - fired when an Ajax request is completed.;
							 $.unblockUI();
								});	


			}, 300);
		}			

		$("#botonExcel").on("click", function(e){ 
			console.log('Inicil');
                e.preventDefault();
               $('#consultaFactura').attr('action', "{{route('generarExcelPagoProveedor')}}").submit();
            });

		function verVoucher(proforma, item){
			
			console.log(proforma);
			console.log(item);
			$.ajax({
					type: "GET",
					url: "{{route('getAdjuntosVoucher')}}",
					dataType: 'json',
					data: {
							dataProforma: proforma,
							dataItem: item,
			       			},
					success: function(rsp){
							console.log(rsp.length);
							$("#output").html('');
							if(rsp.length != 0){
								$.each(rsp, function (key, item){
								 	        ruta = 'adjuntoDetalle/'+item.nombre_adjunto;	
											console.log(item.nombre_adjunto);
											var indice  = item.nombre_adjunto;
											extension = indice.split('.');
											console.log(extension);
						                if(extension[1] != 'pdf' && extension[1] != 'doc'){
											//  $("#output").append('<div id= "'+item.id+'"><div class="col-sm-2" id= "'+item.id+'"><a href="../adjuntoDetalle/'+item.nombre_adjunto+'" target="_blank"><img class="img-responsive" style="width:80px; height:120px;" src="../adjuntoDetalle/'+item.nombre_adjunto+'" alt="Photo"></a><label>'+item.nombre_adjunto+'</label></div><div class="col-xs-12 col-sm-1 col-md-1"></div></div>');
											 $("#output").append(`
														<div class="col-6 mr-1" id= "${item.id}">
															<a href="../adjuntoDetalle/'${item.nombre_adjunto}" target="_blank">
																<img class="img-responsive" style="width:80px; height:120px;" src="../adjuntoDetalle/${item.nombre_adjunto}" alt="Photo">
															</a>
															<label>${item.nombre_adjunto}</label>
														</div>`);
						                 }else{
											$("#output").append(`
												<div class="col-6">
													<a href="../adjuntoDetalle/${item.nombre_adjunto}" target="_blank">
														<img class="img-responsive" style="width:80px;" src="{{asset('images/file.png')}}" alt="Photo">
													</a>
													<label>${item.nombre_adjunto}</label>
												</div>`);
						                    // $("#output").append('<div id= "'+item.id+'"><div class="col-sm-2"><a href="../adjuntoDetalle/'+item.nombre_adjunto+'" target="_blank"><img class="img-responsive" style="width:80px;" src="{{asset('images/file.png')}}" alt="Photo"></a><label>'+item.nombre_adjunto+'</label></div><div class="col-xs-12 col-sm-1 col-md-1"></div></div>');
						                } 
								})	
							}else{
								$("#modalAdjunto").modal('hide');
							}		
					}
				})		
		}
		$(document).ready(function () {
			 var dtable = $('#listado').DataTable();
			$('.filter-button').on('click', function () {
		        //clear global search valuess
		        dtable.search('');
		        $('.filter').each(function(){ 
		        if(this.value.length){
		          dtable.column($(this).data('columnIndex')).search(this.value);
		        }
		        });
		        dtable.draw();
		    });
		    
		    $( ".dataTables_filter input" ).on( 'keyup change',function() {
		        dtable.columns().search('');
		       //clear input values
		       $('.filter').val('');
			});	
		})    

</script>

@endsection