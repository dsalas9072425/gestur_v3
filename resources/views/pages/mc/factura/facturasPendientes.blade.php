@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}
	#ocultar {
		display: none;
	}
</style>


<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Reporte Facturas Pendientes</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
    			<form id="consultaFactura">

			<div class="row">
	            <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Cliente</label>
						<select class="form-control select2" name="cliente_id"  id="cliente" tabindex="1" >
							<option value="">Seleccione Cliente</option>
							@foreach($clientes as $cliente)
								@php
									$ruc = $cliente->documento_identidad;
									if($cliente->dv){
										$ruc = $ruc."-".$cliente->dv;
									}
								@endphp
								<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->nombre_c}} - {{$cliente->denominacion_comercial}}</option>
							@endforeach
						</select>
			        </div>
	            </div>
	            <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Moneda</label>
						<select class="form-control select2" name="moneda_id"  id="moneda" tabindex="2" >
							<option value="">Seleccione Moneda</option>
							
							@foreach($monedas as $moned)
							<option value="{{$moned->currency_id}}">{{$moned->currency_code}}</option>
							@endforeach
							
						</select>
			        </div>
	            </div>

	             <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Estado</label>
						<select class="form-control select2" name="estado"  id="estado" tabindex="3" >
							<option value="">Seleccione Estado</option>
							<option value="VENCIDO">VENCIDO</option>
							<option value="A_VENCER">A VENCER</option>
							
						</select>
			        </div>
	            </div>

	             <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Sucursal Cartera</label>
						<select class="form-control select2" name="sucursal_id"  id="sucursal_id" tabindex="4" >
							<option value="">Seleccione Sucursal</option>
							@foreach($sucursalCartera as $sucursal)
							<option value="{{$sucursal->id}}">{{$sucursal->nombre}} {{$sucursal->denominacion_comercial}}</option>
							@endforeach
							
						</select>
			        </div>
	            </div>

	            </div>

	        <div class="row">
	            <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Tipo Persona</label>
						<select class="form-control select2" name="tipo_persona_id[]"  id="tipo_persona_id" tabindex="5" >
							@foreach($tipoPersona as $persona)
							<option value="{{$persona->id}}">{{$persona->denominacion}}</option>
							@endforeach
						</select>
			        </div>
	            </div>
	            <div class="col-12 col-sm-3 col-md-3">
			        <div class="form-group">
			            <label>Usuario</label>
						<select class="form-control select2" name="usuario_id"  id="usuario_id" tabindex="6" >
							<option value="">Seleccione Opción</option>
							@foreach($usuarios as $usuario)
							<option value="{{$usuario->id}}">{{$usuario->nombre}} {{$usuario->apellido}}</option>
							@endforeach
						</select>
			        </div>
	            </div>

				<div class="col-12 col-sm-3 col-md-3">  
 					<div class="form-group">
			            <label>Check-In Desde - Hasta</label>						 
						<div class="input-group">
						    <div class="input-group-prepend" style="">
						        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
						    </div>
						    <input type="text" class="form-control pull-right fecha" name="checkin_desde_hasta" id="periodo_fact" value="">
						</div>
					</div>	
				</div>
				<div class="col-12 col-sm-3 col-md-3">  
 					<div class="form-group">
			            <label>Vencimiento Desde - Hasta</label>						 
						<div class="input-group">
						    <div class="input-group-prepend" style="">
						        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
						    </div>
						    <input type="text" class="form-control pull-right fecha" name="vencimiento_desde_hasta" id="periodo_venc" value="">
						</div>
					</div>	
				</div> 
	            <div class="col-12">
			       	<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button> 
	            	<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
			       	<button type="button" onclick ="consultaFactura()" class="pull-right btn btn-info btn-lg mr-1" ><b>Buscar</b></button>
	            </div>
	 		</div>
	    </form>  	
		

    
            	<div class="table-responsive table-bordered mt-1">
	              <table id="listado" class="table">
	                <thead>
					  <tr>
					  	<th>Emisión</th>
					  	<th>Vencimiento</th>
						<th>Checkin</th>
						<th>Cliente</th>
						<th>Cartera</th>
						<th style ='width: 150px'>N° Factura</th>
						<th>Pasajero</th>
						<th>Usuario</th>
						<th>Moneda</th>
						<th>Total</th>
						<th>Saldo</th>
						<th>Dias</th>
						<th>Estado</th>

		              </tr>
	                </thead>
	                <tbody id="lista_cotizacion" style="text-align: center">
				        @foreach($facturas as $factura)
					        <tr>
				                  <td><span id="ocultar">{{$factura->fecha_hora_facturacion}}</span>{{$factura->fecha_hora_facturacion}}</td>
				                  <td>{{$factura->vencimiento}}</td>
				                  <td>{{$factura->check_in}}</td>
				                  <td>{{$factura->cliente_n}}</td>
				                  <td>{{$factura->cartera_n}} {{$factura->cartera_dc}}</td>
				                  <td>
				                  	@if($factura->id_venta_rapida == "")
				                  		<a href="{{route('verFactura',['id'=>''])}}/{{$factura->id}}" style=" font-weight: bold; color:#111;" class="bgRed">
                    					<i class="fa fa-fw fa-search"></i>{{$factura->nro_factura}}</a><div style="display:none;">{{$factura->id}}</div>
                    				@else
										<a href="{{route('verVenta',['id'=>''])}}/{{$factura->id_venta_rapida}}" style=" font-weight: bold; color:#111;" class="bgRed">
                    					<i class="fa fa-fw fa-search"></i>{{$factura->nro_factura}}</a><div style="display:none;">{{$factura->id_venta_rapida}}</div>
                    				@endif	
                    					</td>
				                  @if($factura->pasajero_factour == "")
				                  		<td>{{$factura->pasajero_n}} {{$factura->pasajero_a}}</td>
				                  @else
				                  		<td>{{$factura->pasajero_factour}}</td>
				                  @endif
				                  <td>{{$factura->usuario_nombre}} {{$factura->usuario_apellido}}</td>
				                  <td>{{$factura->moneda}}</td>
				                  <td>{{$factura->total_factura}}</td>
				                  <td>{{$factura->saldo_factura}}</td>
				                  <td>{{$factura->dias}}</td>
				                  <td>{{$factura->venc_estado}}</td>
				            </tr>
						@endforeach
			        </tbody>
	              </table>
	            </div>  
        
	
		
			</div>
		</div>
	</section>
</section>



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
<script type="text/javascript">


	$("#listado").dataTable({
		"order": [ 0, 'desc' ]
	});
	$('.select2').select2();

	let select_2 = $('#tipo_persona_id').select2({
									multiple:true,
									maximumSelectionLength: 2,
									placeholder: 'Todos'
								});
	$('#tipo_persona_id').val('').trigger('change.select2');
	let locale = {
				format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};
	let conf = {timePicker24Hour: true,
				timePickerIncrement: 30,
				locale: locale};		


	let calendar_01 = $('#periodo_fact').daterangepicker(conf);
	let calendar_02 = $('#periodo_venc').daterangepicker(conf);

	$('#periodo_fact').val('');
	calendar_01.on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
	$('#periodo_venc').val('');
	calendar_02.on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

		function limpiar(){
			$('#cliente').val('').trigger('change.select2');
			$('#moneda').val('').trigger('change.select2');
			$('#estado').val('').trigger('change.select2');
			$('#sucursal_id').val('').trigger('change.select2');
			$('#tipo_persona_id').val('').trigger('change.select2');
		}

		function validart(data){

			if(data == undefined || data == null || data === '')
					return '';
			return data;
		}



			function validar(data){

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}



			function consultaFactura() {

				 $.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });


			var dataString = $('#consultaFactura').serialize();
				$.ajax({
						type: "GET",
						url: "{{route('consultaFacturasPendientes')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });
                              $.unblockUI();

                            },

						success: function(rsp){
							   $.unblockUI();

							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

							$.each(rsp.facturas, function (key, item){

							console.log(item);

							var c = '';

								if(validar(item)){

							  if(validar(item.fecha_hora_facturacion)){
							    c = "<span id='ocultar'>"+item.fecha_hora_facturacion_s+"</span>"+item.fecha_hora_facturacion;		
									} else {
								c = '<span id=ocultar></span>';		
									}

							if(item.pasajero_factour == ""){
								var pasajero = validart(item.pasajero_n)+' '+validart(item.pasajero_a);
							}else{
								var pasajero = item.pasajero_factour;
							}

							btn = ``;

							if(jQuery.isEmptyObject(item.id_venta_rapida) == false){
								btn = `<a href="{{route('verVenta',['id'=>''])}}/${item.id_venta_rapida}" style="font-weight: bold; color:#111;" class="bgRed"><i class="fa fa-fw fa-search"></i>${item.nro_factura}</a><div style="display:none;">${item.id}</div>`;	
				     		}else{
								btn = `<a href="{{route('verFactura',['id'=>''])}}/${item.id}" style="font-weight: bold; color:#111;" class="bgRed"><i class="fa fa-fw fa-search"></i>${item.nro_factura}</a><div style="display:none;">${item.id}</div>`;		

				     		}									
						 
						var dataTableRow = [
							c,
							item.vencimiento,
							item.check_in,
							item.cliente_n,
							validart(item.cartera_n)+' '+validart(item.cartera_dc),


							btn,
							pasajero,
							item.usuario_nombre+' '+item.usuario_apellido,
							item.moneda,
							item.total_factura,
							item.saldo_factura,
							item.dias,
							item.venc_estado
											];

						var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
						var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
									}

							});

						}//cierreFunc	
				});//Cierre ajax

			
			
		}

		$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
				$('#consultaFactura').attr("method", "post");               
				$('#consultaFactura').attr('action', "{{route('generarExcelReportePendiente')}}").submit();
            });


</script>

@endsection
