@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Tipo de Timbrado</h1>
            </div> 
            @include('flash::message')      
            <div class="row">
		        <div class="col-xs-12">
		            <a href="{{route('tipoTimbradoAdd')}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px;margin-left: 10px;" role="button">Nuevo Tipo de Timbrado</a>
		        </div>
		    </div>        
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="table-responsive">
	              <table id="listado" class="table">
	                <thead>
					  <tr>
						<th>Id</th>
						<th>Denominacion</th>
			            <th class="oculto">Ver</th>
		              </tr>
	                </thead>
	                <tbody>
	 					@foreach($tipoTimbrados as $tipoTimbrado)
				            <tr>
				                <td><b>{{$tipoTimbrado['id']}}</b></td>
				                <td>{{$tipoTimbrado['denominacion']}}</td>
				                <td>
				                	<a href="{{route('tipoTimbradoEdit', ['id' =>$tipoTimbrado['id']])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; background: #e2076a !important; width: 106px;" role="button">Editar</a>
									<a href="{{route('tipoTimbradoDelete', ['id' =>$tipoTimbrado['id']])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; width: 106px;" role="button">Eliminar</a>
				                </td>
				            </tr>	
			            @endforeach
			        </tbody>
	              </table>
	            </div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/mc/scripts')
	<script>
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
		});
	</script>
@endsection