@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Agregar Nueva Asistencia</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
                <div class="row">
                    <form id="nuevoServicio" style="width:100%" class="p-3">
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="nombre">Nombre del servicio:</label>
                                <input type="text" name="nombre" class="form-control" placeholder="Nombre del servicio" id="nombreServicio" required>
                            </div>
                            <div class="col-lg-4">
                                <label for="tarifa">Tarifa del servicio:</label>
                                <input type="text" name="tarifa" class="form-control numeric" placeholder="Tarifa del servicio" id="tarifa" onkeypress="return justNumbers(event);" required>
                            </div>
                            <div class="col-lg-4">
                                <label for="exenta">Monto exento:</label>
                                <input type="text" name="exenta"  class="form-control numeric" placeholder="Monto exento de la Tarifa del Servicio" id="exenta" onkeypress="return justNumbers(event);" required>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-6">
                                <label for="exenta">Monto grabado:</label>
                                <input type="text" name="grabada"  class="form-control numeric" placeholder="Monto grabado de la Tarifa del Servicio" id="exenta" onkeypress="return justNumbers(event);" required>
                            </div>
                            <div class="col-lg-6">
                                <label for="exenta">IVA %:</label>
                                <input type="number" name="iva"  min="0" max="100"  class="form-control" placeholder="Porcentaje IVA" id="exenta" onkeypress="return justNumbers(event);" required>
                            </div>
                        </div>
						<div class="row mt-3">
							<div class="col-lg-6">
                                <label>Empresas</label>
								<select class="form-control select2" name="empresa"  id="empresa" tabindex="1" style="width: 100%;">
                                    <option value="0">Ver todas</option>
                                    @foreach($empresas as $empresa)
                                        <option value="{{$empresa->id}}">{{$empresa->denominacion}}</option> 
                                    @endforeach

                                </select>
                            </div>
							
                            <div class="col-lg-6">
                                <label>Moneda</label>
                                <select class="form-control select2" name="moneda"  id="moneda" tabindex="1" style="width: 100%;" required>
                                    @foreach($cambio as $servicio)
                                        <option value="{{$servicio->currency_id}}">{{$servicio->currency_code}}</option> 
                                    @endforeach
                                </select>
                            </div>
						</div>
                        <div class="pull-right">
                            <a href="{{url('indexAsistenciaViajero')}}" class="btn btn-secondary mt-4 ml-2" >Volver</a>
                        </div>
                        <div class="pull-right">
                             <button type="submit" class="btn btn-success mt-4" >Guardar</button>
                         </div>
                        
                    </form>
                </div>
			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/parsley.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
        $(document).ready(function() {
            $("#moneda").val(143).select2();
            $("#empresa").select2();
			$('.numeric').inputmask("numeric", {
				    radixPoint: ",",
				    groupSeparator: ".",
				    digits: 2,
				    autoGroup: true,
				    rightAlign: false,
				    oncleared: function () { self.Value(''); }
			});
            $( "#nuevoServicio" ).submit(function(e) {
				$("#nuevoServicio").validate();
				if(!$("#nuevoServicio").isValid){
					e.preventDefault();
                	var dataString = $('#nuevoServicio').serialize();
					$.ajax({
						type: "GET",
						url: "{{route('agregarNuevoServicio')}}",
						dataType: 'json',
						data: dataString,

							error: function(jqXHR,textStatus,errorThrown){

							$.toast({
							heading: 'Error',
							text: 'Ocurrio un error en la comunicación con el servidor.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});

							},
						success: function(rsp){
							if(rsp.estado == 1){
								$.toast({
								heading: 'Agregado con exito.',
								text: 'Se creo con exito el nuevo item de menu',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'success'
								});
								$('#nuevoServicio')[0].reset();
							}else{		
								$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la insercion.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
									});
							}
						
						}
					});	
				}
            });
        });

        function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

	</script>
@endsection