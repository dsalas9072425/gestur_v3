@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>

	</style>
@endsection
@section('content')

@include('flash::message') 

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Editar Asistencia</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
		<div class="card-body">
				<form id="guardarEdicionForm" autocomplete="off">
					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="descripcion">Nombre</label>
								<input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Administracion" value="{{$datos->tarifa_asistencia_nombre}}" required>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="precio">Precio</label>
								<input type="text" class="form-control numeric" id="precio" name="precio" placeholder="Administracion" value="{{$datos->tarifa_asistencia_precio}}" onkeypress="return justNumbers(event);" required>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="exenta">Exenta</label>
								<input type="text" class="form-control numeric" id="exenta" name="exenta" placeholder="Administracion" value="{{$datos->tarifa_asistencia_exenta}}" onkeypress="return justNumbers(event);" required>
							</div>
						</div>
 					</div>
					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="grabada">Grabada</label>
								<input type="text" class="form-control numeric" id="grabada" name="grabada" placeholder="Administracion" value="{{$datos->tarifa_asistencia_grabada}}" onkeypress="return justNumbers(event);" required>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="iva">IVA</label>
								<input type="text" class="form-control" id="iva" name="iva" placeholder="Administracion" value="{{$datos->tarifa_asistencia_iva}}" onkeypress="return justNumbers(event);" required>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
                                <label for="moneda">Tipo de Cambio</label>
                                <select class="form-control select2" name="moneda"  id="moneda" tabindex="1" style="width: 100%;">
                                    <option value="">Todos</option>
                                    @foreach($cambio as $moneda)
                                        <option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option> 
                                    @endforeach
                                </select>
							</div>
						</div>
 					</div>
					<div class="row">
						<div class="col">
							<div class="form-group">
                                <label for="moneda">Estado</label>
                                <select class="form-control select2" name="estado"  id="estado" tabindex="1" style="width: 100%;">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
                                <label for="moneda">Empresa</label>
                                <select class="form-control select2" name="empresa"  id="empresa" tabindex="1" style="width: 100%;">
                                    <option value="0">Ver todas</option>
                                    @foreach($empresas as $empresa)
                                        <option value="{{$empresa->id}}">{{$empresa->denominacion}}</option> 
                                    @endforeach

                                </select>
							</div>
						</div>
					</div>
                    <input type="hidden" value="{{$datos->id}}" name="id">
                     <button type="submit" id="guardarEdicion" class="btn btn-success pull-right mb-1 ml-1">Guardar</button>
						<a href="{{url('indexAsistenciaViajero')}}" class="btn btn-secondary text-white pull-right">Volver</a>
				</form>		
                @php
                if( $datos->activo == 1){
                    $selected=1;
                }else{
                    $selected=0;
                }
                @endphp

			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/parsley.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
    
	<script>
		$(document).ready(function() {
            $("#moneda").val({{$datos->id_moneda}}).select2();
            $("#estado").val({{$selected}}).select2();
            $("#empresa").val({{$datos->id_empresa}}).select2();
            $('.numeric').inputmask("numeric", {
				    radixPoint: ",",
				    groupSeparator: ".",
				    digits: 2,
				    autoGroup: true,
				    rightAlign: false,
				    oncleared: function () { self.Value(''); }
			});

            $( "#guardarEdicionForm" ).submit(function(e) {
				$("#guardarEdicionForm").validate();
				if(!$("#guardarEdicionForm").isValid){
					e.preventDefault();
                	var dataString = $('#guardarEdicionForm').serialize();
					$.ajax({
						type: "GET",
						url: "{{route('guardarEditarAsistencia')}}",
						dataType: 'json',
						data: dataString,

							error: function(jqXHR,textStatus,errorThrown){

							$.toast({
							heading: 'Error',
							text: 'Ocurrio un error en la comunicación con el servidor.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});

							},
						success: function(rsp){
							if(rsp.estado == 1){
								$.toast({
								heading: 'Actualizado con exito.',
								text: 'Se actualizo con exito los detalles de la asistencia',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'success'
								});
								window.location.href = "{{URL::to('indexAsistenciaViajero')}}"
							}else{		
								$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la actualizacion.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
									});
							}
						
						}
					});	
				}
            });
        });
        function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

	</script>
@endsection