@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>

	</style>
@endsection
@section('content')

@include('flash::message') 

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Asitencia al Viajero</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
                <a href="{{ url('addAsistencia')}}" title="Agregar Asistencia" class="btn btn-success pull-right mt-2"
                            role="button" id="addAsistencia">
                            <div class="fonticon-wrap">
                                <i class="ft-plus-circle"></i>
                            </div>
                         </a>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body"> 			
                    <div class="row">                         
                        <form id="frmAsistenciaConsulta">
                            <div class="col">
                            <label for="servicioNombre">Servicio</label>
                            <input type="text" class="form-control" placeholder="Servicio" id="servicioNombre" name="servicioNombre">
                            </div>
                        </form>
                    </div>
                <div class="row mt-3">
                    <div class="col-12 pb-1">
                    <button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg ml-1"><b>Excel</b></button>

                         <button type="button"  id="limpiar" class="btn btn-light btn-lg pull-right text-center text-white ml-1" id="buscar">Limpiar</button>

                         <button  type="button" id="buscar" class="btn btn-primary btn-lg pull-right text-center text-white ml-1">Buscar</a>

                    </div>
                </div>	
            	<div class="table-responsive">
	              <table id="listadoAsistencias" class="table text-center" style="width: 100%;">
	                <thead style="text-align: center">
							<th>Nombre</th>
							<th>Precio</th>
							<th>Exenta</th>
							<th>Grabada</th>
							<th>IVA</th>
							<th>Moneda</th>
							<th>Estado</th>
							<th>Editar</th>
	                </thead>
	              </table>
				</div>  
			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
    
	<script>
		$(document).ready(function() {
            verListadoAsistencias();

        });
        $( "#buscar" ).on( "click", function() {
                datos=$("#servicioNombre").val();
                verListadoAsistencias(datos);
            });
            $( "#limpiar" ).on( "click", function() {
                $("#servicioNombre").val("");
                verListadoAsistencias();
            });
        $("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                $('#frmAsistenciaConsulta').attr('method','post');
                $('#frmAsistenciaConsulta').attr('action', "{{route('generarExcelAsistencia')}}").submit();
            });

        function verListadoAsistencias(busqueda){
            $("#listadoAsistencias").dataTable({
				"paging":true,
				"searching": true,
				"processing": true,
				"destroy": true,
				"search": {
					return: true,
				},
				"ajax": {
				"url": "{{route('asistenciasConsulta')}}",
			    "data": {
			        "filtro": busqueda }
			    },
                "columns":[
                    {data: 'nombre'},
                    {data: 'precio'},
                    {data: 'exenta'},
                    {data: 'grabada'},
                    {data: 'moneda'},
                    {data: 'iva'},
                    {data: 'estado',
                    render: function(data, type, row, meta) {
                        if(data == true){
                            return '<i class="fa fa-fw fa-circle  verde " title="Activo" style="color: green;"></i>';
                        }else{
                            return '<i class="fa fa-fw fa-circle  rojo " title="Inactivo" style="color: red;"></i> '; 
                        }        
                    }
                    },
                    {data: 'id',
                        render: function(data, type, row, meta) {
                            return (`<a href="{{route('editarAsistencia',['id'=>''])}}/${data}" class="btn btn-primary text-white pull-right" title=Editar Menu"><i class="fa fa-pencil"></i></a>`); 
                        }}

                ]

				});
        }
	</script>
@endsection