@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
    <style type="text/css">
		.error{
			color:red;
		}
        input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }
	</style>
@endsection
@section('content')
<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Agregar Meta</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
<!--funciona para todas las validacio-->


            	<form id="metas" action="{{ route('metaAgencias.store') }}" method="post" enctype="multipart/form-data">
	            	
                  	

                    <div class="row"  style="margin-bottom: 1px;">
                        {{-- <div class="col-md-3">
                            <div class="form-group">
                                <label>Vendedor Agencia</label>
                                <select class="form-control" name="id_vendedor_agencia"  id="vendedor" style="width: 100%;" tabindex="12">
                                    <option value="">Todos</option>
                                    
                                </select>
                            </div>
                        </div> --}}
						<div class="col-md-3">
							<div class="form-group">
								<label>Agencia</label>
								<select class="form-control input-sm select2" name="id_vendedor_agencia"
									id="vendedor" style="width:100%;">
									<option value="1">Todos</option>
									@foreach($agencias as $agencia)
									<option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="col-xs-12 col-sm-3 col-md-4" style="padding-left: 15px;padding-right: 70px;">
							<div class="form-group">
								<label for="descripcion">Meta Anual<label class="error" ></label></label>
                                <input type="number" id="meta_anual" autocomplete="off" required name="meta_anual" class="form-control">
                            </div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Año</label>					            	
								<select class="form-control input-sm select2"  name="anho" id="" style="width: 100%;" tabindex="12">
									<option value="">Seleccione Año</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
									<option value="2024">2024</option>
									<option value="2025">2025</option>
									<option value="2026">2026</option>
									<option value="2027">2027</option>
								</select>
							</div>
						</div> 
					</div>
					
			</div>	
	    	
            <a href="{{ route('metaAgencias') }}" class="btn btn-secondary btn-md pull-right mb-1">Cancelar</a>
            <button type="submit" id="guardarEdicion" name="accion" value="guardar" class="btn btn-success btn-md pull-right mb-1 mr-1" tabindex="17"><b>GUARDAR</b></button>
			<button type="button" id="botonExcel" onclick="botonExcel()" class="pull-right text-center btn btn-success btn-md mb-1 mr-1"><b>Generar Planilla</b></button>
         	
				</form>	
				<form action="{{ route('Meta-agencia-masivo') }}" method="POST" enctype="multipart/form-data">
					<div class="col-xs-12" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
						<div class="form-group">
							<label for="exampleFormControlFile1">Subir Plantilla con Metas</label>
							<input type="file" class="form-control-file" id="exampleFormControlFile1" name="file" required>
						  </div>
					</div>
				<div class="row">
					<div class="col-md-12" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
						<button class="btn btn-success btn-lg" name="accion" value="procesar" type="submit">PROCESAR PLANTILLA</button>
					</div>
				</div>
			</form>
				
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script>
$("#guardarEdicion").validate({  // initialize plugin on the form
    debug: false,
    rules: {
        "deescripcion": {
            required: true
        },

    },
    messages: {
        "descripcion": {
            required: "<br>No puede quedar vacio el campo"
        },

    }
});

$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                $('#metas').attr('method','post');
               	$('#metas').attr('action', "{{route('generarExcelMetasAgencias')}}").submit();
            });

var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};
            // $(document).ready(function() {

            //     $("#vendedor").select2({
            //                     language: lang_es_select2,
            //                     ajax: {
		    //             url: "{{route('get.vendedores.agencia')}}",
		    //             dataType: 'json',
		    //             placeholder: "TODOS",
		    //             delay: 0,
		    //             data: function (params) {
		    //                         return {
		    //                             q: params.term, // search term
		    //                             page: params.page
		    //                                 };
		    //             },
		    //             cache: true
		    //             },
		    //             escapeMarkup: function (markup) {
		    //                             return markup;
		    //             }, // let our custom formatter work
		    //             minimumInputLength: 3,
		    //     });
            // });//READY DOCUMENT
        
        $(".select2").select2();

</script>
@endsection