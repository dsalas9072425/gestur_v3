<!DOCTYPE html>
<html lang="es" translate="no">
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
    @parent
@endsection
    
@section('content')
<style>
.status-container {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    padding: 2px 4px; /* Ajusta el espacio interno del contenedor */
    border-radius: 4px; /* Ajusta el radio de los bordes */
}

.bg-success {
    background-color: #28a745;
}

.bg-danger {
    background-color: #dc3545;
}

.status-text {
    font-size: 12px; /* Ajusta el tamaño del texto */
}
</style>
<style>
    .pagination .page-item {
        border-radius: 50%;
    }
    /* Estilos para la tabla con desplazamiento horizontal */
    .table-responsive {
        overflow-x: auto;
    }
</style>
<style type="text/css">
	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	/*.bgRed:hover{
		color: white;
	}*/

	/*background: #e2076a !important;padding-left: 6px;padding-right: 6px;*/



	/*==================SELECT STAR============================*/

	.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}


.click_estrella {
	cursor:pointer; 
	cursor: hand;
}

/*====================================================
	ESTILO PARA FOCUS
  ====================================================	
*/

input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }



</style>

<section id="base-style">
    <br>
<form id="metitas">
    <div class="card">
        <div class="card-header">
            <div class="card-body">   
            <h4 class="card-title">Reporte Metrica</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
            {{-- <div class="card-body pt-0">

                <a href="{{route('metaAgencias.create')}}" title="Agregar Persona" class="btn btn-success pull-right"
                    role="button">
                    <div class="fonticon-wrap">
                        <i class="ft-plus-circle"></i>
                    </div>
                </a>
            </div> --}}
        <div class="row">
            
           

            {{-- <div class="col-md-3">
                <div class="form-group">
                    <label>Vendedor Agencia</label>
                    <select class="form-control" name="vendedor_id"  id="vendedor" style="width: 100%;" tabindex="12">
                        <option value="">Todos</option>
                        
                    </select>
                </div>
            </div>	 --}}
            <div class="col-md-3">
                <div class="form-group">
                    <label>Agencia</label>
                    <select class="form-control input-sm select2" name="id_vendedor_agencia"
                        id="vendedor" style="width:100%;">
                        <option value="">Todos</option>
                        @foreach($agencias as $agencia)
                        <option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="f">Periodo</label>
                    <select class="form-control input-sm select2" name="anio" id="anio" style="width: 100%;" tabindex="12">
                        <?php
                        $anioMinimo = 2019; // Establece el año mínimo que deseas mostrar
                        $anioActual = date('Y');
            
                        for ($i = $anioActual; $i >= $anioMinimo; $i--) {
                            echo '<option value="' . $i . '">' . $i . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            
            





          
        </div> 
    </form>   
        <div class="row">
             <div class="col-md-12">
             {{--   <a href="{{ route('facturapre_excel') }}" class="pull-right text-center btn btn-success btn-lg mr-1">Excel</a> --}}
              <button type="button" id="botonExcel" onclick="botonExcel()" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button> 
             <button type="button" onclick="limpiar()" id="btnLimpiar" class="pull-right btn btn-light btn-lg text-white mr-1 mb-1" tabindex="17" ><b>Limpiar</b></button>
                <button type="button" class="pull-right btn btn-info btn-lg mr-1 mb-1" role="button" onclick="buscarPersonas()" tabindex="16"><b>Buscar</b></button>
            </div>
        </div>
 
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                 
                        <div class="row">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3">
                           
                            </div>
                        </div>
                        <div class="table-responsive">
                        <table id="meta" class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Cod Agencia</th>
                                    <th>Agencia</th>
                                    <th>Total Bruto Factura USD</th>
                                    <th>Total Bruto Renta USD</th>
                                    <th>Mes</th>
                                    <th>Meta Mensual</th>
                                    <th>Periodo Meta</th>
                                    <th>Diferencia</th>
                            </thead>
                            <tbody id="facturas-table">
                              
    

                    
                        
                            </tbody>
                        </table>
                 
                    </div>
                  
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
 
    @include('layouts/gestion/scripts')
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
    <script>
 function getMonthName(monthNumber) {
    var meses = [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
    ];
    
    return meses[monthNumber - 1]; // Resta 1 porque los meses en el array comienzan desde 0.
}

        function buscarPersonas() {
      $('#meta').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ route('indexMetricaAjax') }}', 
            type: 'GET',
            data: $('#metitas').serializeJSON()
        },
        columns: [
            { data: 'id_vendedor_agencia', name: 'id_vendedor_agencia' },
            { data: 'nombre_completo', name: 'nombre_completo' },
            { data: 'total_factura_cotizado', name: 'total_factura_cotizado',render: function(data, type, row) {
            if (type === 'display' && data !== null) {
            return parseFloat(data).toLocaleString(); // Agrega el separador de miles
            }
            return data;}  },
            { data: 'renta_cotizada', name: 'renta_cotizada',render: function(data, type, row) {
            if (type === 'display' && data !== null) {
            return parseFloat(data).toLocaleString(); // Agrega el separador de miles
            }
            return data;}  },
            { data: 'mes_fecha_facturacion', name: 'mes_fecha_facturacion',render: function(data, type, row) {
        if (type === 'display' && data !== null) {
            return getMonthName(data); // Llama a la función para obtener el nombre del mes.
        }
        return data;
    } },
            { data: 'meta_mensual', name: 'meta_mensual',render: function(data, type, row) {
            if (type === 'display' && data !== null) {
            return parseFloat(data).toLocaleString(); // Agrega el separador de miles
            }
            return data;}  },
            { data: 'periodo_meta', name: 'periodo_meta' },
            { data: 'diferencia', name: 'diferencia',render: function(data, type, row) {
            if (type === 'display' && data !== null) {
            return parseFloat(data).toLocaleString(); // Agrega el separador de miles
            }
            return data;}  },
           // { data: 'cantidad_proformas', name: 'cantidad_proformas' },
            //{ data: 'cantidad_facturas', name: 'cantidad_facturas' },

           
        ]
    });
}




function limpiar(){
          
    $('#vendedor').val('').trigger('change.select2');
    $('#nombre').val('');
    $('#denominacion_comercial').val('');
    $('#ruc').val('');
    $('#correo').val('');
    
}


$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                 $('#metitas').attr('method','post');
               $('#metitas').attr('action', "{{route('generarExcelMetaAgencia')}}").submit();
            });



            var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};
            // $(document).ready(function() {

            //     $("#vendedor").select2({
            //                     language: lang_es_select2,
            //                     ajax: {
		    //             url: "{{route('get.vendedores.agencia')}}",
		    //             dataType: 'json',
		    //             placeholder: "TODOS",
		    //             delay: 0,
		    //             data: function (params) {
		    //                         return {
		    //                             q: params.term, // search term
		    //                             page: params.page
		    //                                 };
		    //             },
		    //             cache: true
		    //             },
		    //             escapeMarkup: function (markup) {
		    //                             return markup;
		    //             }, // let our custom formatter work
		    //             minimumInputLength: 3,
		    //     });
            // });//READY DOCUMENT
        
        $(".select2").select2();


    </script>

    
@endsection


