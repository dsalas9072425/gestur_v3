@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
    <style type="text/css">
		.error{
			color:red;
		}
        input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }
	</style>
@endsection
@section('content')
<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Editar Meta</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
<!--funciona para todas las validacio-->


            	
                    <Form action="{{route('metaAgencias.update', $meta->id_meta)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="row" style="margin-bottom: 1px;">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Vendedor Agencia</label>
                                        <input type="text" id="meta_anual" autocomplete="off" required value="{{$meta->vendedorAgencia->nombre}} {{$meta->vendedorAgencia->apellido}}" name="meta_anual" readonly class="form-control">
                                </div>
                            </div>
                        
                            <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px; padding-right: 15px;">
                                <div class="form-group">
                                    <label for="descripcion">Periodo<label class="error"></label></label>
                                    <input type="text" id="anho" autocomplete="off" required value="{{$meta->anho}}" name="anho" readonly class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px; padding-right: 15px;">
                                <div class="form-group">
                                    <label for="descripcion">Meta Anual<label class="error"></label></label>
                                    <input type="text" id="meta_anual" autocomplete="off" required value="{{$meta->meta_anual}}" name="meta_anual" readonly class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px; padding-right: 15px;">
                                <div class="form-group">
                                    <label for="descripcion">Meta Semestral<label class="error"></label></label>
                                    <input type="text"  pattern="[0-9]+(\.[0-9]+)?" title="Solo se permiten números" id="meta_semestral" autocomplete="off"  value="{{$meta->meta_semestral}}" readonly name="meta_semestral" class="form-control">
                                </div>
                            </div>
                        
                            <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px; padding-right: 15px;">
                                <div class="form-group">
                                    <label for="descripcion">Meta Trimestral<label class="error"></label></label>
                                    <input type="text" id="meta_trimestral" autocomplete="off" required value="{{$meta->meta_trimestral}}" name="meta_trimestral" readonly class="form-control">
                                </div>
                            </div>
                        
                            <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px; padding-right: 15px;">
                                <div class="form-group">
                                    <label for="descripcion">Meta Mensual<label class="error"></label></label>
                                    <input type="text" id="meta_mensual" autocomplete="off" required value="{{$meta->meta_mensual}}" name="meta_mensual" class="form-control">
                                </div>
                            </div>
                        
                            
                            <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px; padding-right: 15px;">
                                <div class="form-group">
                                    <label for="descripcion">Mes<label class="error"></label></label>
                                    <input type="text"  pattern="[0-9]+(\.[0-9]+)?" title="Solo se permiten números" id="mes" autocomplete="off"  value="{{$meta->mes}}"readonly name="mes" class="form-control">
                                </div>
                            </div>
                        </div>
					
			</div>	
	            	
            <a href="{{ route('metaAgencias') }}" class="btn btn-secondary btn-md pull-right mb-2">Cancelar</a>
            <button type="submit" id="guardarEdicion" class="btn btn-success btn-md pull-right mb-1" tabindex="17"><b>GUARDAR</b></button>

         	
				</form>	
				
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script>
$("#guardarEdicion").validate({  // initialize plugin on the form
    debug: false,
    rules: {
        "deescripcion": {
            required: true
        },

    },
    messages: {
        "descripcion": {
            required: "<br>No puede quedar vacio el campo"
        },

    }
});

var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};
            $(document).ready(function() {

                $("#vendedor").select2({
                                language: lang_es_select2,
                                ajax: {
		                url: "{{route('get.vendedores.agencia')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		        });
            });//READY DOCUMENT
        
        $(".select2").select2();

</script>
@endsection