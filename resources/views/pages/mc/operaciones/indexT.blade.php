
		 		@php
		 		
				 function validar($data){
					if(!is_null($data)){
						return $data;
					} else {
						return 'N/A';
					}
				}

				 /**
* SI ES DIFERENTE AL GUARANI REALIZA 
* EL FORMATEO CON COMAS SINO SERA SIN COMAS
*/
function formatMoney($valor,$id_moneda_venta){


   if($id_moneda_venta != 111){
	   return number_format($valor,2,",",".");
   }
   return number_format($valor,0,",",".");

}//function	
			@endphp



@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')


<!-- Main content -->


<style type="text/css">

/*LADO IZQUIERDO*/
  .alturaSuperiorIzq{
	  height: 30% !important;
  }
  .alturaInferiorIzq{
	  height: 70% !important;
  }



/*LADO DERECHO*/
  .alturaSuperiorDer{
	  height: 30% !important;
  }

  .alturaInferiorIzq{
	  height: 70% !important;
  }
  .select2-selection {
	    	max-height: 32px !important;
   }

  .select2-selection--single{
			padding-top: 0px !important;	
		}

  .select2-selection__rendered{
			padding-right: 6px;
		}

   .select2-puntos_venta-container{
		padding-right: 10px;
    	padding-left: 4px;
	}

	.select2-container--default .select2-selection--single{
    padding-left: 0px;
}

.select2-puntos_venta-container{
	    padding-left: 0px;
	    padding-right: 0px;
}
/*	div {
	  border:1px solid;
  }*/

   .btnAprobar {
   margin:0 3% 0 0;
   }

  .cabeceraClass {
	  padding: 10px;
	  /*min-width: 200px;*/
	  background-color:#E8EAEA;
	  word-break: break-all;
	  margin: 10px;

  }

  .negrita {
	  font-weight: 700;
  }

  .styleInput {
	  font-weight: bold;
	  font-size: 12px;
	  height: 30px;
   padding: 5px 10px;
   font-size: 12px;
   line-height: 1.5;
   border-radius: 3px;
  }

  .btnAut {
	  margin:0 3% 0 0;
	  display: none;
  }
  .btn-verde{
	  background-color: #00a65a !important;
  }

  .form-button-cabecera{
   margin-bottom: 5px !important;
}	

.chat-content {
   text-align: left;
   float: left;       
   position: relative;
   display: block;
   padding: 8px 15px;
   margin: 0 20px 10px 0;
   clear: both;
   color: #404e67;
   background-color: #edeef0;
   border-radius: 4px;



}

.select2-selection .select2-selection--single{
   padding-left: 1px;
   padding-right: 1px;
}

.chat-content-left {
   text-align: right;
   position: relative;
   display: block;
   float: right;
   padding: 8px 15px;
   margin: 0 20px 10px 0;
   clear: both;
   color: #fff;
   background-color: #00b5b8;
   border-radius: 4px;
}   
input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }
</style>
<section class="base-style">
   <div class="card  mb-1">
	   <div class="card-header">
		   <h4 class="card-title"><i class="fa fa-fw fa-file-text"></i>Control Operativo </h4>
		   <div class="heading-elements">
			   <ul class="list-inline mb-0">
				   <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
				   <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
			   </ul>
		   </div>
	   </div>
	   <div class="card-content collapse show" aria-expanded="true">
		   <div class="card-body">
		  <div class="row">

		   @if (isset($proformas))

		   @foreach($proformas as $proforma)
		   <div class="col-3">
			   <!-- Agencia -->
			   <div class="form-group form-button-cabecera">
				   <label>Cliente: </label>
				   <input type="text" class="form-control styleInput" value="{{$proforma->agencia['nombre']}}" disabled="disabled" />
			   </div>
		   </div>
		   <?php 
			   if(isset($proforma->vendedor['nombre'])){
				   $vendedor = $proforma->vendedor['nombre'];
			   }else{
					$vendedor = "";
			   }
		   ?>
		   <div class="col-3">
			   <div class="form-group form-button-cabecera">
				   <label>Vendedor: </label>
				   <input type="text" class="form-control styleInput" value="{{$vendedor}}" disabled="disabled" />
			   </div>
		   </div>

		   <div class="col-2">
			   <div class="form-group form-button-cabecera">
				   <label>N. Proforma:</label>
				   <input type="text" class="form-control styleInput" value='{{$proforma->id}}' disabled="disabled" />
			   </div>
		   </div>

		   {{--========================================================
			ELEMENTOS QUE CAMBIAN AL DAR GUARDAR TENER EN CUENTA 
			BORRAR
			========================================================--}}

		   <div class="col-2">
			   <div class="form-group form-button-cabecera">
				   <label>Vencimiento: </label>
				   <input type="text" class="form-control styleInput" id="fechaVenc" value='{{$proforma->vencimiento}}' disabled="disabled" />


			   </div>
		   </div>


		   <div class="col-2">
			   <div class="form-group form-button-cabecera">
				   <label>Checkin / Checkout:</label>
				   <input type="text" class="form-control styleInput" value='{{$proforma->check_in}} - {{$proforma->check_out}}'
					   disabled="disabled" />
			   </div>
		   </div>

		   <div class="col-4">
				<label>Puntos de Ventas</label>
				<select class="styleInput form-control select2" name="punto_venta" id="puntos_venta" style="width: 100%">
					@foreach($puntos_ventas as $key=>$puntos_venta)
						<option value="{{$puntos_venta->id}}"> &nbsp; &nbsp;{{$puntos_venta->establecimiento}}-{{$puntos_venta->expedicion}}   -  &nbsp; &nbsp;{{$puntos_venta->sucursal->denominacion_comercial}} - &nbsp; &nbsp; {{$puntos_venta->tipo_timbrado->denominacion}}</option>
					@endforeach 
				</select>			   
		   </div>

		   <div class="col-2">
			   <div class="form-group form-button-cabecera">
				   <label>Estado:</label>
				   <input type="text" class="form-control styleInput negrita" id="id_estado"
					   value="{{$proforma->estado['denominacion']}}" disabled="disabled" />

			   </div>
		   </div>

		   <div class="col-2">
			   <div class="form-group form-button-cabecera">
				   <label>Monto Proforma:</label>
				   <input type="text" class="form-control styleInput" id="montoProforma"
					   value="{{formatMoney($totalProforma[0]->total,$proforma->id_moneda_venta)}} {{$proforma->currency['currency_code']}}"
					   disabled="disabled" />

   
			   </div>
		   </div>

		   <div class="col-2">
			   <div class="form-group form-button-cabecera">
				   <label>Saldo Disponible:</label>
				   <input type="text" class="form-control styleInput" id=""
					   value='{{formatMoney($saldoDisponible[0]->get_saldo_real,$proforma->id_moneda_venta)}}'
					   disabled="disabled" />


				   {{-- <span id="saldoDisponible">$saldoDisponible[0]->get_saldo_real != null</span></span><span id="currencyProforma">USD</span> --}}

			   </div>
		   </div>


		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Total Bruto</label>
				   <input type="text" class="styleInput form-control" id="total_bruto"
					   value='{{formatMoney($proforma->total_bruto_facturar,$proforma->id_moneda_venta)}}'
					   disabled="disabled" tabindex="15" />
			   </div>
		   </div>

		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Total Neto</label>
				   <input type="text" class="styleInput form-control" id="total_neto"
					   value='{{formatMoney($proforma->total_neto_facturar,$proforma->id_moneda_venta)}}'
					   disabled="disabled" />
			   </div>
		   </div>


		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Exentas</label>
				   <input type="text" class="styleInput form-control" id="total_exentas"
					   value='{{formatMoney($proforma->total_exentas,$proforma->id_moneda_venta)}}'
					   disabled="disabled" />
			   </div>
		   </div>

		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Gravadas</label>
				   <input type="text" class="styleInput form-control" id="total_gravadas"
					   value='{{formatMoney($proforma->total_gravadas,$proforma->id_moneda_venta)}}'
					   disabled="disabled" />
			   </div>
		   </div>

		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Iva</label>
				   <input type="text" class="styleInput form-control" id="total_iva"
					   value='{{formatMoney($proforma->total_iva,$proforma->id_moneda_venta)}}'
					   disabled="disabled" />
			   </div>
		   </div>

		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Markup</label>
				   <input type="text" class="styleInput form-control" id="markupF" value='{{ $proforma->markup}}'
					   disabled="disabled" tabindex="15" />
			   </div>
		   </div>

		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Comisión</label>
				   <input type="text" class="styleInput form-control" id="total_comision"
					   value='{{formatMoney($proforma->total_comision,$proforma->id_moneda_venta)}}'
					   disabled="disabled" />
			   </div>
		   </div>

		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Tipo Facturación</label>
				   @if(isset($proforma->tipoFacturacion->denominacion))
				   <input type="text" class="styleInput form-control"
					   value='{{$proforma->tipoFacturacion->denominacion}}' disabled="disabled" />
				   @else
				   <input type="text" class="styleInput form-control" value=''
					   disabled="disabled" />
				   @endif
			   </div>
		   </div>
		   {{-- ============================================================================= --}}

		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Destino</label>
				   <input type="text" class="styleInput form-control" id="id_destino"
					   value='{{$proforma->destino_id}}' disabled="disabled" />
			   </div>
		   </div>


		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Incentivo Agencia</label>
				   <input type="text" class="styleInput form-control" id="incentivo_agencia"
					   @if($proforma->tiene_incentivo_vendedor_agencia == 1)
				   value='SI'
				   @else
				   value='NO'
				   @endif
				   disabled="disabled"/>
			   </div>
		   </div>


		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>Tarjeta de Crédito</label>
				   <input type="text" class="styleInput form-control"
					   @if($proforma->pago_tarjeta == 1)
				   value='SI'
				   @else
				   value='NO'
				   @endif
				   disabled="disabled"/>
			   </div>
		   </div>

		   <div class="col-12 col-sm-4 col-md-2">
			   <div class="form-group form-button-cabecera">
				   <label>File/Código</label>
				   <input type="text" class="styleInput form-control" id="file_codigo" value='{{ $proforma->file_codigo}}' disabled="disabled" tabindex="15" />
			   </div>
		   </div>
		   <div class="col-12 col-sm-4 col-md-4">
		   	   <label>Pasajeros</label>
			   <select class="form-control select2" name="pasajero_principal[]" multiple="multiple" id="pasajero_principal" style="width: 100%" disabled="disabled" >
				   @foreach($pasajeros as $key=>$pasajero)
					   <option value="{{$pasajero->id}}" selected="selected">{{$pasajero->pasajero_n}}</option>
				   @endforeach 
			   </select>
		   </div>  
		   <div class="col-md-3">
			
		</div>  
		   <div class="col-12 col-sm-12 col-md-12">
			   <div
				   style="display: inline-block; margin-bottom:0; vertical-align: bottom; border:1px solid; padding:5px; width: 150px; background: #D5DBDB; text-align: center;">
				   <b>Ultimo Comentario</b>
			   </div>
			   @if(count($solicitudes) != 0)
				   <div style="display: inline-block; margin-bottom:0; vertical-align: bottom; border:1px solid; padding:5px; width: 55%; background: white;"
				   id="campoComentario"> 
			   @else 
				   <div style="display: inline-block; margin-bottom:0; vertical-align: bottom; border:1px solid; padding:5px; width: 76%; background: white;"
				   id="campoComentario"> 

			   @endif	
				   Sin comentarios ....
			   </div>

			   <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal"
				   style="background-color: #ed00ef;">
				   <i class="fa fa-fw  fa fa-weixin"></i>
			   </button>

			   <a role="button" href="{{ route('previewFactura',['id'=>$proforma->id]) }}" class="btn btn-info" title="Preview" id=""><i class="fa fa-eye fa-lg"></i></a>
			   @if(count($solicitudes) != 0)
				   <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalSolicitudes" style="background-color: #ed00ef;width: 360px;margin-left: 1%;"><b>{{count($solicitudes)}} Solicitud(es) Pendiente(s)</b></button>
			   @endif
		   </div>    
		   <div class="col-12 mt-1">
			   <button type="button" class="btn btn-danger btn-lg pull-right" onclick="rechazo()"
				   id="btnRechazar">DEVOLVER</button>

			   @foreach ($btn as $boton)
			   {{-- CARGA LOS BOTONES POR PERMISO DE PERSONAS --}}
			   <?php echo $boton; ?>
			   @endforeach

		   </div>
		   {{-- col --}}

	   </div><!-- row -->
	   
   </div>
</div>
</div>



@endforeach


				@else

				<div class="row">
					<div class="col-12">
						<h2>Vaya vaya algo ha salido mal en esta proforma</h2>
					</div>
				</div>

			 @endif






  <div class="row">


	  <div class="col-12 col-md-6">
		  <div class="card  mb-1">
			  <div class="card-header">
				  <h4 class="card-title"><i class="fa fa-fw fa-file-text"></i> Detalles de Proforma</h4>
				  <div class="heading-elements">
					  <ul class="list-inline mb-0">
						  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
						  <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					  </ul>
				  </div>
			  </div>
			  <div class="card-content collapse show" aria-expanded="true">
				  <div class="card-body">

					  <div class="table-responsive table-bordered">
						  <table id="listadoDos" class="table display nowrap">
							  <thead>
								  <tr>
								   	  <th style="width: 10%; padding: 0.5rem 1rem;">Item</th>
									  <th style="width: 30%; padding: 0.5rem 1rem;">Producto</th>
									  <th style="width: 10%; padding: 0.5rem 1rem;"></th>
									  <th style="width: 50%; padding: 0.5rem 1rem;">Descripción</th>
								  </tr>
							  </thead>
							  <tbody id="lista_cotizacion" style="text-align: center">
								  @if(count($proformasDetalle) > 0)
								  @foreach($proformasDetalle as $detalle)
								  <tr>
								   	  <td style="padding: 0px;">{{$detalle->item}}</td>
									  <td style="padding: 0px;">{{$detalle->producto['denominacion']}}</td>
									  <td style="padding: 0px">
										  <?php $style = '';?>
										  @if($detalle->verificado_operativo === true)
										  <?php $style = 'btn-verde';?>
										  @endif

										  <button class="btn btn-info text-center hide-small normal-button btnVer {{$style}}" role="button" value="{{$detalle->id}}" id="proforma_detalle_{{$detalle->id}}">VER</button>

									  </td>
									  <td>{{$detalle->descripcion}}</td>
								  </tr>
								  @endforeach

								  @endif
							  </tbody>
						  </table>
					  </div>
				  </div>
			  </div>
		  </div>
	  </div>



	  <div class="col-12 col-md-6">
		  <div class="card  mb-1">
			  <div class="card-header">
				  <h4 class="card-title"><i class="fa fa-fw fa-file-photo-o"></i> Imagenes Adjuntas</h4>
				  <div class="heading-elements">
					  <ul class="list-inline mb-0">
						  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
						  <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					  </ul>
				  </div>
			  </div>
			  <div class="card-content collapse show" aria-expanded="true">
				  <div class="card-body">
					  <div class="table-responsive table-bordered">
						  <table id="listado" class="table display nowrap" >
							  <thead>
								  <tr>
									  <th style="width: 80%;">Nombre</th>
									  <th style="width: 20%;"></th>
								  </tr>
							  </thead>
							  <tbody style="text-align: center">
								  @if(count($imagenes) > 0)
									  @foreach($imagenes as $imagen)
										  <?php $style = ''; ?>
											  @if($imagen->verificado_operativo == 'true')
											  <?php $style = 'btn-verde'; ?>
											  @endif
										  <tr>
											  <td style="padding-top: 10px;padding-bottom: 0px;">
											   {{$imagen->nombre_adjunto}}
										   </td>
											  <td style="padding-top: 0px;padding-bottom: 0px;padding-left: 10px;padding-right: 10px;">
												   <button class="btn btn-info text-center hide-small normal-button {{$style}}" id="adjunto_{{$imagen->id}}" onclick="adjuntoSelect('{{$imagen->id}}')" role="button" value="{{$imagen->nombre_adjunto}}">VER</button>
										   </td>
										  </tr>
									  @endforeach
								  @endif
							  </tbody>
						  </table>
					  </div>
					  <input type="hidden" value="" id="idAdjunto">
				  </div>
			  </div>
		  </div>
	  </div>



	  <div class="col-12 col-md-6">
		  <div class="card  mb-1" style="font-size: 0.9rem;">
			  <div class="card-header">
				  <h4 class="card-title"><i class="fa fa-fw fa-edit"></i> Campos detalle proforma</h4>
				  <h6 class="spanAsteriscos">Campo obligatorio y editable <i class="fa fa-fw fa-asterisk"></i></h6>
				  <div class="heading-elements">
					  <ul class="list-inline mb-0">
						  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
						  <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					  </ul>
				  </div>
			  </div>
			  <div class="card-content collapse show" aria-expanded="true">
				  <div class="card-body">
					  <input type="hidden" name="estadoHidden"
						  value="<?php echo (isset($proformas)) ? $proformas[0]->estado_id : 0 ;?>"
						  id="estadoProforma">
					  <input type="hidden" name="idUsuario" id="idUsuario" value="{{$idUsuario->id}}">
					  <div class="box box-success">
						  <form id="frmDetallesProforma">


							  <div class="overlay hidden" id="cargando">
								  <i class="fa fa-refresh fa-spin"></i>
							  </div>


							  <input type="hidden" name="idDetalleProforma" value="" id="idDetalleProforma">
							  <input type="hidden" name="idProforma"
								  value="<?php echo (isset($proforma)) ? $proformas[0]->id : '' ;?>" id="idProforma">


							  <div class="row">

								  <div class="col-12 col-sm-4 col-md-4" id="">
									  <div class="form-group">
										  <label for="">Proveedor <i class="fa fa-fw fa-asterisk"></i></label>
										  <select class="form-control selectorAvanzado hab_1" name="proveedor"
											  id="selectProveedor" style="width: 100%;">
											  <option value="0">Seleccione Proveedor</option>
										  </select>
									  </div>
								  </div>
								  <div class="col-12 col-sm-4 col-md-4" id="inputTipoDocumento">
									  <div class="form-group">
										  <label for="">Prestador <i class="fa fa-fw fa-asterisk"></i></label>
										  <select class="form-control selectorAvanzado hab_1" name="prestador"
											  id="selectPrestador" style="width: 100%;">
											  <option value="0">Seleccione Prestador.</option>

											  @if(count($prestadores) > 0)

											  @foreach($prestadores as $prestador)
											  <option value="{{$prestador->id}}" data="{{$prestador->id}}">
												  {{$prestador->nombre}}</option>
											  @endforeach
											  @endif

										  </select>
									  </div>
								  </div>

								  <div class="col-12 col-sm-4 col-md-4" id="">
									  <div class="form-group">
										  <label for="">Producto</label>
										  <input type="hidden" class="form-control" name="" id="producto_id"
											  value="" />
										  <input readonly="readonly" type="text" class="form-control" name=""
											  id="producto" value="" />
									  </div>
								  </div>

							  </div>
							  <!-- row -->
						   @if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa != 1)
							   <div class="row">

								   <div class="col-12 col-sm-8 col-md-8" id="">
									   <div class="row">
										   <div class="col-5">
											   <label for="">Costo <i class="fa fa-fw fa-asterisk"></i></label>
										   </div>
										   <div class="col-5">
											   <label for="">Costo Gravada</label>
										   </div>

									   </div>
									   <div class="row">

										   <div class="col-5">
											   <div class="form-group">
												   <input type="number" class="form-control hab_1" id="precioCosto"
													   value="" name="precioCosto" >

											   </div>
										   </div>
										   <div class="col-5">
											   <div class="form-group">
												   <input type="number" class="form-control hab_1" id="costoGravado"
													   value="" name="costoGravado" readonly="readonly">

											   </div>
										   </div>

										   <div class="col-2" style="padding-left: 0;text-align: center">

											   <!-- <input  type="text" class="form-control" name="" id="currencyCosto" value="" /> -->

											   <select class="form-control selectorAvanzado hab_1" name="currencyCosto"
												   id="currencyCosto" style="width: 100%;">
												   <option value="0">N/A</option>

												   @if(count($currency) > 0)

													   @foreach($currency as $divisas)
														   <option value="{{$divisas->currency_id}}"
															   data="{{$divisas->currency_id}}">{{$divisas->currency_code}}
														   </option>
													   @endforeach

												   @endif

											   </select>

										   </div>
									   </div>
								   </div>

								   <div class="col-12 col-sm-4 col-md-4" id="">
									   <div class="row">
										   <div class="col-9">
											   <div class="form-group">
												   <label for="">Precio Venta</label>
												   <input type="number" class="form-control" id="precioVenta"
													   readonly="readonly">

											   </div>
										   </div>
										   <div class="col-3" style="padding-left: 0;text-align: center">
											   <div class="form-group">
												   <label for="">&nbsp;</label>
												   <input readonly="readonly" style="padding-left: 1px;padding-right: 1px;"type="text" class="form-control"
													   id="currencyVenta" value="" />
											   </div>
										   </div>
									   </div>
								   </div>

							   </div>
							   <div class="row">
								   <div class="col-12 col-sm-4 col-md-4" id="">
									   <div class="form-group">
										   <label for="">Comision Agencia <i class="fa fa-fw fa-asterisk"></i></label>
										   <input type="text" class="form-control hab_1" name="comisionAgencia" id="comisionAgencia" value="" />
									   </div>
								   </div>
						   @else 
							   <div class="row">
								   <div class="col-12 col-sm-4 col-md-4" id="">
									   <div class="row">
										   <div class="col-12">
											   <label for="">Costo <i class="fa fa-fw fa-asterisk"></i></label>
										   </div>
									   </div>
									   <div class="row">
										   <div class="col-9">
											   <div class="form-group">
												   <input type="number" class="form-control hab_1" id="precioCosto"
													   value="" name="precioCosto">
													   <input type="hidden" class="form-control hab_1" id="costoGravado"
													   value="0" name="costoGravado">
											   </div>
										   </div>
										   <div class="col-3" style="padding-left: 0;text-align: center">

											   <!-- <input  type="text" class="form-control" name="" id="currencyCosto" value="" /> -->

											   <select class="form-control selectorAvanzado hab_1" name="currencyCosto"
												   id="currencyCosto" style="width: 100%;">
												   <option value="0">N/A</option>

												   @if(count($currency) > 0)

													   @foreach($currency as $divisas)
														   <option value="{{$divisas->currency_id}}"
															   data="{{$divisas->currency_id}}">{{$divisas->currency_code}}
														   </option>
													   @endforeach

												   @endif

											   </select>
											   <input class="hidden form-control hab_1" readonly="readonly" style="padding-left: 1px;padding-right: 1px; width:100%; " type="hidden" value="" id="currencyTextBlock">

										   </div>
									   </div>
								   </div>

								   <div class="col-12 col-sm-4 col-md-4" id="">
									   <div class="row">
										   <div class="col-9">
											   <div class="form-group">
												   <label for="">Precio Venta</label>
												   <input type="number" class="form-control" id="precioVenta"
													   readonly="readonly">

											   </div>
										   </div>
										   <div class="col-3" style="padding-left: 0;text-align: center">
											   <div class="form-group">
												   <label for="">&nbsp;</label>
												   <input readonly="readonly" style="padding-left: 1px;padding-right: 1px;"type="text" class="form-control"
													   id="currencyVenta" value="" />
											   </div>
										   </div>
									   </div>
								   </div>
								   <div class="col-12 col-sm-4 col-md-4" id="">
									   <div class="form-group">
										   <label for="">Comision Agencia <i class="fa fa-fw fa-asterisk"></i></label>
										   <input type="text" class="form-control hab_1" name="comisionAgencia" id="comisionAgencia" value="" />
									   </div>
								   </div>

							   </div>
							   <div class="row">
						   @endif 

							  <!-- row -->

								  <div class="col-12 col-sm-4 col-md-4" id="">
									  <div class="form-group">
										  <label for="">Codigo confirmación <i
												  class="fa fa-fw fa-asterisk"></i></label>
										  <input type="text" class="form-control hab_1" name="codigoConfirmacion" id="codigoConfirmacion" value="" />
									  </div>
								  </div>

								  <div class="col-4 col-sm-4" style="height: 74px;">
									  <div class="form-group">
										  <label>Fecha IN</label>
										  <div class="input-group">
											  <div class="input-group-prepend">
											   <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
											  </div>
											  <input type="text" class="form-control hab_1" id="fechaIn"
												  name="fechaIn">
										  </div>
									  </div>
								  </div>
								  
								  <div class="col-4 col-sm-4" style="height: 74px;">
									  <div class="form-group">
										  <label>Fecha OUT</label>
										  <div class="input-group">
											  <div class="input-group-prepend">
											   <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
											  </div>
											  <input type="text" class="form-control hab_1" id="fechaOut"
												  name="fechaOut">
										  </div>
									  </div>
								  </div>

								  <div class="col-4 col-sm-4" style="height: 74px;">
									  <div class="form-group">
										  <label>Pago Proveedor</label>
										  <div class="input-group">
											  <div class="input-group-prepend">
											   <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
											  </div>
											  <input type="text" class="form-control hab_1" id="pagoProveedorFecha"
												  name="pagoProveedorFecha">
										  </div>
									  </div>
								  </div>
								  <div class="col-4 col-sm-4" style="height: 74px;">
									  <div class="form-group">
										  <label>Fecha Gasto</label>
										  <div class="input-group">
											  <div class="input-group-prepend">
											   <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
											  </div>
											  <input type="text" class="form-control hab_1" id="gastoFecha" name="gastoFecha">
										  </div>
									  </div>
								  </div>

								  <!-- GRUPO AEREO-->
									  <div class="col-12" class="grupo_aereo" style="display: none;">
										  <div class="form-group">
											  <label>Codigo Confirmación </label>
											  <textarea class="form-control" readonly rows="1"
												  id="textAreaCod"></textarea>
										  </div>
									  </div>
								  <!-- GRUPO OTROS/TERRESTRE-->
									  <div class="col-4 col-sm-4 grupo_translado" style="display: none;">
										  <div class="form-group">
											  <label>N° Vuelo IN </label>
											  <input type="text" class="form-control" readonly
												  id="num-vuelo-in"></textarea>
										  </div>
									  </div>

									  <div class="col-4 col-sm-4 grupo_translado" style="display: none;">
										  <div class="form-group">
											  <label>N° Vuelo OUT </label>
											  <input type="text" class="form-control" readonly
												  id="num-vuelo-out"></textarea>
										  </div>
									  </div>


								  <!-- GRUPO ASISTENCIA-->
									  <div class="col-4 col-sm-4 grupo_asistencia" style="display: none">
										  <div class="form-group">
											  <label> Tipo Asistencia</label>
											  <input type="text" readonly class="form-control" id="tipo_asistencia" />
										  </div>
									  </div>

									  <div class="col-4 col-sm-4 grupo_asistencia" style="display: none">
										  <div class="form-group">
											  <label> Cantidad días</label>
											  <input type="text" readonly class="form-control"
												  id="cantidad_dias_asistencia" />
										  </div>
									  </div>

									  <div class="col-4 col-sm-4 grupo_asistencia" style="display: none">
										  <div class="form-group">
											  <label> Cantidad Personas</label>
											  <input type="text" readonly="readonly" class="form-control"
												  id="cantidad_personas_asistencia" />
											  <input type="hidden" class="form-control"  id="indiceVoucher" value="0"/>
										  </div>
									  </div>

								  <div class="col-12 col-sm-12" id="">
									  <div class="form-group">
										  <label>Descripción </label>
										  <textarea class="form-control" rows="1" name="descripcion"
											  id="formDescripcion"></textarea>
									  </div>
								  </div>
							  </div>
							  <!-- row -->
						  </form>

						  <div class="box-footer">
							  <button type="submit" disabled="disabled" class="btn btn-primary pull-right"
								  id="btnGuardar">Guardar</button>

							  <button class="btn btn-primary pull-right btn-verde"
								  style="margin-right: 5px; display:none;" id="btnVerificarDetalle"><i
									  class="fa fa-fw fa-check-circle"></i></button>
							  <button class="btn btn-primary pull-right "
								  style="margin-right: 5px; display:none; background-color:#dd4b39"
								  id="btnAnularVerificacionDetalle"><i class="fa fa-fw fa-close"></i></button>

							  <button type="button" class="btn btn-primary" id="btnCrearVoucher" data-toggle="modal"
								  disabled="disabled" data-target="#requestModalVoucher">Crear Voucher</button>
						  </div>
					  </div>
				  </div>
			  </div>
		  </div>
	  </div>
	  <div class="col-12 col-md-6">
		  <div class="card mb-1">
			  <div class="card-header">
				  <h4 class="card-title"><i class="fa fa-fw fa-eye"></i> Vista de Adjunto</h4>
				  <div class="heading-elements">
					  <ul class="list-inline mb-0">
						  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
						  <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					  </ul>
				  </div>
			  </div>
			  <div class="card-content collapse show" aria-expanded="true">
				  <div class="card-body">
					  <img src="" class="img-responsive hidden" id="imgMostrar" style=" overflow: scroll;">
					  <iframe id="verPdf" src="" class="hidden" style="width: 100%;height: 400px;"></iframe>
				  </div>
			  </div>
		  </div>
	  </div>

  </div>
  <!-- row -->
   </section>
   <!-- /.BASE STYLE -->



		 <!-- ========================================
				  MODAL LISTA COMENTARIOS
			  ========================================  	
   <div id="myModal" class="modal fade" role="dialog">-->    
   <div class="modal fade text-left" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">		  
	   <div class="modal-dialog">
	   <!-- Modal content-->
	   <div class="modal-content" style="width: 570px;">
		 <div class="modal-header">
		   <h4 class="modal-title">Comentarios</h4>
		   <button type="button" class="close" data-dismiss="modal">&times;</button>
		 </div>
		 <div class="modal-body">
	   <!-- Construct the box with style you want. Here we are using box-danger -->
	   <!-- Then add the class direct-chat and choose the direct-chat-* contexual class -->
	   <!-- The contextual class should match the box, so we are using direct-chat-danger -->
		 <div class="box box-danger direct-chat direct-chat-danger">
		   <div class="box-header with-border">
			 <div class="box-tools pull-right">
			
			 </div>
		   </div><!-- /.box-header -->
		   <div class="box-body">

			 <!-- Conversations are loaded here -->
			<div class="direct-chat-messages" id="chatDirect" style="height:355px; overflow: scroll;overflow-x: hidden;">>
		   @php
		   $comentId = 0; 
		   $lado = 'l';	

			function mensajeLeft($mensaje,$nombre,$fechaHora,$id,$logo=''){
			   $img = ($logo != '') ?  $logo : "factour.png"; 

			   $imagen = asset('personasLogo')."/".$img;

				 $rsp = "<!-- Mensaje por defecto a la izquierda -->
			   <div class='direct-chat-msg' data='{$id}'>
				   <div class='row'>
					   <div class='col-md-2'><span class='direct-chat-name pull-left'><b>{$nombre}</b></span></div>
					   <div class='col-md-7'></div>
					   <div class='col-md-3' style='padding-left: 2px;padding-right: 10px;'><span class='direct-chat-timestamp pull-right' style='font-size: smaller;'>{$fechaHora}</span></div>    
				   </div>
				   <div class='row'>
					   <div class='col-md-2'><img class='direct-chat-img' src='".$imagen."' style='width:50px' alt='message user image'></div>
					   <div class='col-md-10' style='word-break: break-all; padding-left: 0px;'><div class='chat-content'>
					   {$mensaje}</div></div>
				   </div>
				</div> <br>";

			   return $rsp;

			 }	
			
			 function mensajeRight($mensaje,$nombre,$fechaHora,$id,$logo=''){
			   $img = ($logo != '') ?  $logo : "factour.png"; 
			   $imagen = asset('personasLogo')."/".$img;


			   $rsp = "<!-- Mensaje por defecto a la izquierda -->
			   <div class='direct-chat-msg right' data='{$id}'>
				   <div class='row'>
					   <div class='col-md-3' style='padding-left: 2px;padding-right: 10px;'><span class='direct-chat-name pull-right' style='font-size: smaller;'>{$fechaHora}</span></div>
					   <div class='col-md-7'></div>
					   <div class='col-md-2'><span class='direct-chat-timestamp pull-left'><b>{$nombre}</b></span></div>    
				   </div>
				   <div class='row'>
					   <div class='col-md-10'><div class='chat-content-left' style='word-break: break-all; padding-left: 0px;'>
					   {$mensaje}</div></div>
					   <div class='col-md-2'><img class='direct-chat-img' src='".$imagen."' style='width:50px' alt='message user image'></div>
				   </div>
				</div><br> ";

			 return $rsp;

			 }
		   @endphp	

		   @if(count($comentario) > 0)


			 @foreach($comentario as $coment)
			 @php

			 $mensaje = $coment->comentario;
			 $nombre = $coment->persona['nombre'];
			 $fecha = $coment->fecha_hora;
			 $logo  =$coment->persona['logo'];

			  if($fecha != '' && $fecha != NULL){
				
				$f = explode(' ', $fecha);

				$date = explode('-', $f[0]);

				$fecha = $date[2]."/".$date[1]."/".$date[0];
				$fecha .= ' '.$f[1];
			} else {
				$fecha = 'N/A';
			}
			  $idPersona = $coment->id_usuario;

			 if($comentId != 0 && $comentId != $coment->id_usuario){

				 if($lado == 'l'){

					 $lado = 'r';
					 echo mensajeRight($mensaje,$nombre,$fecha, $idPersona,$logo);

				 } else {

					 $lado = 'l';
					 echo mensajeLeft($mensaje,$nombre,$fecha, $idPersona,$logo);
				 }


			 }	else if($comentId == $coment->id_usuario){
				 
				 if($lado == 'l'){

					 $lado = 'l';
					 echo mensajeLeft($mensaje,$nombre,$fecha, $idPersona,$logo);

				 } else {

					 $lado = 'r';
					 echo mensajeRight($mensaje,$nombre,$fecha, $idPersona,$logo);
				 }

			 } else {

				 $lado = 'l';
				 echo mensajeLeft($mensaje,$nombre,$fecha, $idPersona,$logo);
			 }
			   $comentId = $coment->id_usuario;

			 @endphp
			 @endforeach
		   @endif
	  
		   </div><!-- /.box-body -->
		   <div class="box-footer">
			 <div class="input-group">
			   <input type="text" name="message" placeholder="Escribe tu comentario ..." class="form-control" id="mensajeChat" maxlength="200">
			   <span class="input-group-btn">
				 <button type="button" class="btn btn-danger btn-flat" id="btnChat">Enviar</button>
			   </span>

			 </div>
			 <span id="errorChat" style="color:red;"><h6></h6></span>
		   </div><!-- /.box-footer-->
		 </div><!--/.direct-chat -->

		 </div>
		 <div class="modal-footer">
		   <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
		 </div>
	   </div>

	 </div>
   </div>
</div>

<!-- MODAL Facturar ? -->
   <div id="Modalfacturar" class="modal fade" role="dialog">
	 <div class="modal-dialog">

	   <!-- Modal content-->
	   <div class="modal-content">
		 <div class="modal-header">
		   <h4 class="modal-title">¿Desea facturar esta proforma?</h4>
		   <button type="button" class="close" data-dismiss="modal">&times;</button>
		 </div>
		 <div class="modal-body">

		   

		   <div class="row">
		   
			<div class="col-6" id="" style="text-align:center;">

				<button type="submit" class="btn btn-lg btn-primary" data-dismiss="modal" onclick="cambiarEstado('2')"id="">NO</button>
						   
			</div>

			 <div class="col-6" id="" style="text-align:center;">
				 <a type="button" class="btn btn-lg btn-primary" style="color:#fff;" id="" data-dismiss="modal" onclick="cambiarEstado('4')">FACTURAR</a>
			</div>

		 </div>

		 </div>


		 <div class="modal-footer">
		   <button type="button" class="btn btn-success" data-dismiss="modal" id="cerrarModal">Cerrar</button>
		 </div>
	   </div>

	 </div>
   </div>

   
<!-- MODAL modalSolicitudes ? -->
<div id="modalSolicitudes" class="modal fade" role="dialog">
	   <div class="modal-dialog modal-xl" role="document" style="width: 80%;margin-left: 15%;margin-top: 10%;">
	   <!-- Modal content-->
	   <div class="modal-content">
		 <div class="modal-header">
		   <h4 class="modal-title">Solicitudes de Facturación Pendiente</h4>
		   <button type="button" class="close" data-dismiss="modal">&times;</button>
		 </div>
		 <div class="modal-body">
				 <table style="width: 100%;margin-left: 3%;">
					 <tr>
						 <th>Fecha</th>
					   <th>Cliente</th>
					   <th>Usuario Pedido</th>
					   <th>Pasajero Pedido</th>
					   <th>Moneda</th>
					   <th>Monto</th>
					   <th></th>
				   </tr>	
				   @foreach($solicitudes as $solicitud)  
					   <tr>
						   <td>{{date('d/m/Y h:m:s',strtotime($solicitud->fecha_hora_pedido))}}</td>
						   <td>
						   		<?php 
						   			if(isset($solicitud->cliente->nombre)){
							   			echo $solicitud->cliente->nombre." ".$solicitud->cliente->apellido;
									}
								?>
						   </td>
						   <td>
						   		<?php 
						   			if(isset($solicitud->usuario->nombre)){
							   			echo $solicitud->usuario->nombre." ".$solicitud->usuario->apellido;
									}
								?>
						   </td>
						   <td><?php 
						   			if(isset($solicitud->pasajero->nombre)){
										$nombre = $solicitud->pasajero->nombre;
										if(isset($solicitud->pasajero->apellido)){
											$nombre = $nombre." ".$solicitud->pasajero->apellido;
										}else{
											$nombre = $nombre;
										}
									}else{
										$nombre = '';
									}

						 	  ?>
							   {{$nombre}}</td>
						   <td>{{$solicitud->currency->currency_code}}</td>
						   <?php 
							   if($solicitud->id_moneda == 111){
								   $monto = number_format($solicitud->monto,0,",",".");
							   }else{
								   $monto = number_format($solicitud->monto,2,",",".");
							   } 
						   ?>
						   <td>{{$monto}}</td>
						   <th>
							   <button type="button" class="btn btn-info" data-toggle="modal" onclick="facturarParcial({{$solicitud->id}})" style="background-color: #ed00ef;width: 100px;margin-left: 1%;"><b>FACTURAR</b></button>
						   </th>
					   </tr>
				   @endforeach	
			   </table>
		 </div>
		 <div class="modal-footer">
		   <button type="button" class="btn btn-success" data-dismiss="modal" id="cerrarModal">Cerrar</button>
		 </div>
	   </div>

	 </div>
   </div>




<!-- ========================================
				  MODAL VOUCHER
	  ========================================  -->	

   <div class="modal fade text-left" id="requestModalVoucher" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
	   <div class="modal-dialog modal-xl" role="document">
		   <div class="modal-content">
			   <div class="modal-header">
				   <h4 class="modal-title" id="myModalLabel16">Datos de Voucher</h4>
				   <button type="button" id="btnAgregarVoucher" onclick="agergarLinea();" class="btn btn-success" style="width: 50px;margin-left: 78%; display: none;">+</button>
				   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					   <span aria-hidden="true">&times;</span>
				   </button>
			   </div>
			   <div class="modal-body">
				   <form id="frmVoucher" method="POST" style="margin-top: 1%;padding-left: 0px;margin-left: 30px;">
					   <div id="rowDetalles" >
						   <div id="cabeceraVoucher">
								<div class="row">
								   <div class= "col-md-7">
								   </div>
								   <div class= "col-md-1">
									   <label>Cantidad: </label>
								   </div>
								   <div class= "col-md-2">
										   <select class="form-control input-sm select2" name="cantidad_habitaciones" id="cantidad_habitaciones" style="width: 100%;">';
											   @for ($i = 0; $i <= 30; $i++)
												   <option value="{{ $i }}">{{ $i }}</option>
											   @endfor                                     
										   </select>
								   </div>
								   <div class= "col-md-2">
									   <button type="button" id="btnVoucher" class="btn btn-danger" style="background-color: #969dac; ">Generar</button>
								   </div>
							   </div>    
						   </div>
						   <input type="hidden" class="form-control input-sm pull-right" name="proforma" id="proforma" value="{{$proforma->id}}">
						   <input type="hidden" class="form-control input-sm pull-right" name="detalle_proforma" id="detalle_proforma">
						   <div class="row">
							   <div id="detallesVoucher" style="margin-left: 0px;margin-right: 5px;max-height: 550px;overflow-y: scroll;overflow-x: hidden;padding-left: 15px;">

							   </div>
						   </div>  
					   </div>
				   </form> 
			   </div>
			   <div class="modal-footer">
				   <div class="row">  
						<div class= "col-md-4" id="editVoucher" style="display: none;">
							<button type="button" id="btnEditarVoucher" onclick="editarVoucher();" class="btn btn-danger" style="background-color: #e2076a;">Guardar</button>
						</div>
						<div class= "col-md-4" id="addVoucher">
							<button type="button" id="btnAceptarAgregarVoucher" onclick="agregarVoucher();" class="btn btn-danger" style=" background-color: #e2076a;">Guardar</button>
						</div>
						<div class= "col-md-4">
							<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;margin-left: 20px;" data-dismiss="modal">Cerrar</button>
						</div>
					</div>

			   </div>
		   </div>
	   </div>
   </div>

   <!-- Modal -->
   <div class="modal fade" id="modalSaldo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	 <div class="modal-dialog" role="document">
	   <div class="modal-content">
		 <div class="modal-header">
		   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		   <h4 class="modal-title" id="">Mensaje de Alerta</h4>
		 </div>
		 <div class="modal-body">
		  <span id="txtModalSaldo"></span>
		 </div>
		 <div class="modal-footer">
		   <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		 </div>
	   </div>
	 </div>
   </div>

<!-- ========================================
				  MODAL COMENTARIOS
	  ========================================  -->		
<!-- /.content -->
<div id="requestModalComentario" class="modal fade" role="dialog">
	 <div class="modal-dialog" style="width: 50%;margin-top: 3%;">
   <!-- Modal content-->
	   <div class="modal-content">

		   <div id="modal-header" class="modal-header">
			   <h2 class="modal-title titlepage" style="font-size: x-large;">Comentarios</h2>
		   </div>
		   <div class="modal-body" style="padding-top: 0px;">
			   <div class="box-body pad">
				   <textarea class="textarea" id = "detalle" name = "detalle" placeholder="Place some text here" style="width: 100%;height: 850px;font-size: 14px;line-height: 18px;border: 1px solid #dddddd;padding: 15px;"></textarea>
			   </div>  
			   <div class="row">
				   <div class="col-md-6">	
				   </div>
				   <div class="col-md-3" style="padding-left: 0px;">
					   <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				   </div>	
				   <div class="col-md-3" style="padding-left: 0px;">
					   <button type="button" onclick="agregarComentarios();" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Guardar</button> 
				   </div>	
			   </div>	           
		   </div>
		   <input type="hidden" class = "Requerido form-control input-sm" id="proforma_id" name="proforma_id" value="{{$proforma->id}}" />
		   <input type="hidden" class = "Requerido form-control input-sm" id="lineaDet" name="lineaDet"/>
		 </div>
	</div>	
</div>



<!-- ========================================
				  MODAL PRESTADOR
	  ========================================  -->		
<!-- /.content -->
<div id="requestModalPrestador" class="modal fade" role="dialog">
	 <div class="modal-dialog" style="width: 50%;margin-top: 5%;">
   <!-- Modal content-->
	   <div class="modal-content">
		   <input type="hidden" class = "Requerido form-control input-sm" id="proforma_id" name="proforma_id" value="{{$proforma->id}}" />
		   <input type="hidden" class = "Requerido form-control input-sm" id="lineaVoucher" name="lineaTraslado"/>

		   <div id="modal-header" class="modal-header">
			   <h2 class="modal-title titlepage" style="font-size: x-large;">Prestador</h2>
		   </div>
		   <div class="modal-body" style="padding-top: 0px;">
			   <form id="frmPrestador" method="post" action="">
				   <div class="row">	
					   <div class="col-md-5">
						   <div class="form-group">
							   <label>Destino</label>
								<select data-placeholder="Destinos" id="destino" name="destino"  style="width: 100%;" tabindex="9">
										@foreach($valorDestinos as $key=>$destino)
										   <option value="{{$destino['id']}}" selected="select">{{$destino['value']}}</option>
									   @endforeach
							   </select> 		
							   
						   </div>
					   </div> 
					   <div class="col-md-5">
						   <div class="form-group">
							   <label>Nombre Prestador</label>
							   <input type="text" class = "Requerido form-control" name="nombre" id="nombrePrestador" style="font-weight: bold;font-size: 15px;" />
						   </div>	
					   </div>
					   <div class="col-md-2">
						   <div class="form-group">
							   <br>
							   <button type="button" id="btnBuscarPrestador" class="btn btn-danger">Buscar</button>
						   </div>	
					   </div>
				   </div>
			   </form>	
			   <br>
			   <br>
			   <div class="row">
				   <div class="col-12 col-sm-12 col-md-12">		
					   <table id="listadoPrestador" class="table">
						   <thead>
							   <tr>
								   <th>Nombre</th>
								   <th>Dirección</th>
								   <th>Teléfono</th>
								   <th></th>
							   </tr>
						   </thead>
						   <tbody>
						   </tbody>
					   </table>
				   </div>	
			   </div>				
			   <br>
		   </div>
		 </div>
	</div>	
</div>



<!-- Modal -->
<div class="modal fade" id="modalAutorizar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h4 class="modal-title" id="">Mensaje de Alerta</h4>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 </div>
 <div class="modal-body">
  <h4><b>¿Esta seguro que desea autorizar la operación?</b></h4>
  <div class="row">
	  <br>
	  <div class="col-6 text-center">
		  <button type="button" class="btn btn-success btn-lg" data-dismiss="modal" onclick="autorizarProforma()">SI AUTORIZAR</button>
	  </div>
	  <div class="col-6 text-center">
		  <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">NO AUTORIZAR</button>
	  </div>
  </div>
<br>
<div class="col-md-8">
<div class="form-group">
	<label>Motivo Autorizacion</label>
	<select class="styleInput form-control select2" name="motivo_autorizacion"
		id="motivo_autorizacion" style="width:100%;">
		 @foreach($motivo_anulacion as $motivo)
		<option value="{{$motivo->id}}">{{$motivo->descripcion}}</option>
		@endforeach 
	</select>
</div>
</div>
  <div class="col-12 text-center">
	<h4><b>Comentario autorizacion</b></h4>
	<textarea class="form-control" name="comentario_autorizacion" id="comentario_autorizacion" cols="30" rows="10"></textarea>
	</div>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
 </div>
</div>
</div>
</div>


<!-- MODAL COMENTARIO OBLIGATORIO POR RECHAZO -->
<div class="modal fade" id="modalComentarioRechazo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
   <div class="modal-content">
	   <div class="modal-header">
		   <h4 class="modal-title block" id="">COMENTARIO POR DEVOLUCIÓN REQUERIDO
			   <small class="block">Maximo 200 caracteres (*)</small>
			   <small class="msgError block"></small>
		   </h4>
		   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
			   aria-hidden="true">&times;</span></button>
		   

	   </div>
	   <div class="modal-body">
		   <div class="container-fluid">
			   <div class="row">
				   <div class="col-12">
					   <textarea onkeydown="pulsar(event)" rows="4"
						   style="width: 100%; min-height: 300px; font-size: 15px;" id="textRechazo"
						   maxlength="200"></textarea>
				   </div>
			   </div>
		   </div>
	   </div>
	   <div class="modal-footer">
		   <button type="button" class="btn btn-success" id="btnRechazoComentario">Enviar y Cerrar</button>
	   </div>
   </div>
</div>
</div>

   
<input type="hidden" id="btnAutorizarCondition" value="{{$btnAutorizar}}">

@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script>
@if ((Session::get('datos-loggeo')->datos->datosUsuarios->idUsuario == 16989  && $proforma->estado['denominacion'] == 'Abierta'))
var editarNemo = true;
@else
var editarNemo = false;
@endif
$('.textarea').summernote();


fechaDateRange();
comentarioCorto();
verificarEstado();
fechaCabacera();
obtenerDestino();
//var prestadores = {!! json_encode($prestadores) !!}; 
$('#pasajero_principal').select2({
							   multiple:true,
							   //maximumSelectionLength: 2,
							   placeholder: 'Pasajero'
						   });

 $('#puntos_venta').select2();

 $("#puntos_venta").val('{{$p_v_d}}').select2();	
 

function adjuntoSelect(id){
   $('#idAdjunto').val(id);

   //CARGAR ARCHIVO ADJUNTO
   // $('.btnImagen').click(function(){
	   var archivoName = $('#adjunto_'+id).val();
	   var extension = archivoName.split('.');
		archivo = extension[extension.length - 1];
		archivo = archivo.toLowerCase();

	   var getUrl = window.location;
	   console.log(getUrl);
	   var baseUrl = getUrl .protocol
	   var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
	   
	   console.log(baseUrl);

	   if(archivo === 'jpg' || archivo === 'png'|| archivo === 'jpeg'){
		   $('#imgMostrar').removeClass('hidden');
		   $('#verPdf').addClass('hidden');
		   direccion = '../adjuntoDetalle/'+archivoName;
		   $('#imgMostrar').attr('src',direccion);
	   }
	   if(archivo === 'pdf' || archivo === 'docx'){
		   $('#imgMostrar').addClass('hidden');
		   $('#verPdf').removeClass('hidden');
		   direccion = '../adjuntoDetalle/'+archivoName;
		   $('#verPdf').attr('src',direccion);
	   }


}



function obtenerDestino(){

   dataString = {id_destino:$('#id_destino').val()};


		$.ajax({
					   type: "GET",
					   url: "{{ route('obtenerDestino') }}",
					   dataType: 'json',
					   data: dataString,
					   
						error: function(jqXHR,textStatus,errorThrown){

					  $.toast({
					   heading: 'Error',
					   text: 'Ocurrio un error en la comunicación con el servidor.',
					   showHideTransition: 'fade',
					   icon: 'error'
				   });


				   },
					   success: function(rsp){

					 if(rsp.data != ''){
					 $('#id_destino').val(rsp.data)
					 } else {
					 $('#id_destino').val('')
					 }	
						   }//success

				   });
}


$('#btnVerificarDetalle').on('click',function(){
   console.log('Verificar');
   verificarDetalle(1);
});

$('#btnAnularVerificacionDetalle').on('click',function(){
   console.log('Anular');
   verificarDetalle(0);
});


function verificarDetalle(option){


   var detalle_proforma = $('#idDetalleProforma').val();
   var id_adjunto = $('#idAdjunto').val();

   var dataString = { id_proforma_detalle: detalle_proforma, 
					  option:option,
					  id_adjunto:id_adjunto};

   var btn = $('#proforma_detalle_'+detalle_proforma).hasClass('btn-verde');
   var msj = 'Ocurrio un error';
   var err = 0;

   // if(btn && option == 1){msj = 'El detalle ya esta verificado.'; err++;}
   // if(btn == false && option == 0){msj = 'El detalle ya esta anulado.'; err++;}
   // if(detalle_proforma == '' ){msj = 'Seleccione un detalle de proforma para verificar.'; err++;}

	   if(err == 0){


	$.ajax({
					   type: "GET",
					   url: "{{ route('verificarDetalle') }}",
					   dataType: 'json',
					   async:false,
					   data: dataString,
					   
						error: function(jqXHR,textStatus,errorThrown){

					  $.toast({
					   heading: 'Error',
					   text: 'Ocurrio un error en la comunicación con el servidor.',
					   showHideTransition: 'fade',
					   icon: 'error'
				   });


				   },
					   success: function(data){

						 if(data.resp == 'true'){
							 

							 $.each(data.data_adjunto, function(index, val) {
								 console.log(val.verificado_operativo);
								 console.log(id_adjunto);
									 
									 if(val.verificado_operativo == true){
									 $('#adjunto_'+val.id).addClass('btn-verde');
								 } else {
									 $('#adjunto_'+val.id).removeClass('btn-verde');
								 }
							 });

 
						 //Verificar	
						 if(option == 1){
							 $('#proforma_detalle_'+detalle_proforma).addClass('btn-verde');	
							 $('#btnVerificarDetalle').hide();
							 $('#btnAnularVerificacionDetalle').show();
						 //Anular Verificacion	
						 } else {
						   $('#proforma_detalle_'+detalle_proforma).removeClass('btn-verde');	
							 $('#btnVerificarDetalle').show();
							 $('#btnAnularVerificacionDetalle').hide();
							 
						 } 		


						 } else {

								 $.toast({
					   heading: 'Atención !',
					   text: msj,
					   showHideTransition: 'fade',
					   icon: 'info'
				   });

						 }

						   }//success

				   }).done(function(){
					   
						   
				   });
	   } else {


			 $.toast({
					   heading: 'Atención !',
					   text: msj,
					   showHideTransition: 'fade',
					   icon: 'info'
				   });

	   }
				  

}

$('.modalAut').on('click',function() {
	 $("#modalAutorizar").modal("show");
});

function resetForm(){
	   var datos = ['#selectPrestador',
					'#selectProveedor',
					'#formDescripcion',
					'#codigoConfirmacion',
					'#fechaOut',
					'#fechaIn',
					'#comisionAgencia',
					'#precioCosto',
					'#currencyCosto',
					'#currencyVenta',
					'#precioCosto'
					];


   for(var j = 0; j < datos.length;j++){
   var id = datos[j];

		var name = $(id).get(0).tagName;
	  

	   if(name == 'INPUT')
		   $(id).val('');
			   
	   if(name == 'SELECT')
		$(id).val('').trigger('change.select2');

		if(name == 'TEXTAREA')
		  $(id).html('');
				

		}//for

}//function



function autorizarProforma(){

   // var btnAut = $('#btnAutorizarCondition').val();
   var motivo_autorizacion = $('#motivo_autorizacion').val();
   var comentario_autorizacion = $('#comentario_autorizacion').val();
   var idUsuario = $('#idUsuario').val();
   var idProforma = $('#idProforma').val();
   var mensaje = '==========AUTORIZADO=========';
   var dataString = {"comentario": mensaje,"idUsuario":idUsuario,"idProforma":idProforma,"motivo_autorizacion":motivo_autorizacion,"comentario_autorizacion":comentario_autorizacion};
   var ok = 0;

   
			$.ajax({
					   type: "GET",
					   url: "{{ route('autorizarProforma') }}",
					   dataType: 'json',
					   data: dataString,
					   
						error: function(jqXHR,textStatus,errorThrown){

					  $.toast({
					   heading: 'Error',
					   text: 'Ocurrio un error en la comunicación con el servidor.',
					   showHideTransition: 'fade',
					   icon: 'error'
				   });


				   },
					   success: function(rsp){

						   if(rsp.err == 'true'){
						   guardarComentarioAjax(dataString);


							 // location.href ="{{ route('listadosProformas') }}";

						   } else {
								$.toast({
								   heading: 'Error',
								   text: 'Ocurrio un error en la comunicación con el servidor.',
								   showHideTransition: 'fade',
								   icon: 'error'
									   });			
								ok++;
						   } //else

						   }//function

				   }).done(function(){
						   //cambiar estado a verificar
						   if(ok == 0){

						   cambiarEstado('3');
						   }
				   });

			
			

	   

}

function rechazo (){
   console.log('Rechazo');

   $('#modalComentarioRechazo').modal('show');
}

function guardarComentarioRechazo(){
   var mensaje = $('#textRechazo').val().trim();

	   if(mensaje != ''){
			$('#modalComentarioRechazo').modal('hide');
			var idUsuario = $('#idUsuario').val();
			var idProforma = $('#idProforma').val();

	   var dataString = {"comentario": mensaje,"idUsuario":idUsuario,"idProforma":idProforma};
	   guardarComentarioAjax(dataString);
	   $('.msgError').html('');
	   cambiarEstado('1');
   
	   } else {
		   $('.msgError').html('El mensaje esta vacio !');
	   }
}

$('#btnRechazoComentario').click(function(){
   
   guardarComentarioRechazo();
});


function verificarSaldoMonto(){

   var resp = '{{$mensajeSaldoProforma[0]->control_saldo}}';

   if(resp != '' && resp != 'null'){
	   $('#txtModalSaldo').html(resp);
	   $('#modalSaldo').modal('show');
   }
   


   }

	   $('.selectorAvanzado').select2();
	   $('.select2').on('change', function() {
		 $(this).trigger('blur');
	   }); 

	  



	   function justNumbers(e)
   {
	   var keynum = window.event ? window.event.keyCode : e.which;
	   if ((keynum == 8) || (keynum == 46))
	   return true;
		
	   return /\d/.test(String.fromCharCode(keynum));
   }



	 
   
   function verificarEstado(){
	   console.log('FUNCION VERIFICAR ESTADO');

	   var estado = $('#estadoProforma').val();
	   $('.btnAprobar').hide();
	   var btnAut = $('#btnAutorizarCondition').val();
	   console.log(btnAut);
	   if(btnAut === 'false'){ 
	   console.log("estado",estado);

	   {{--ABIERTA --}}
	   if(estado == '1'){
	   $('#id_estado').css('color','red');
	   $('.btnAprobar').hide();
	   }

	   {{--VERIFICADO --}}
	   if(estado == '2'){
		   console.log('FACTURAR');
		   $('#id_estado').css('color','green');
		   $('.btnClassAprobar').html('FACTURAR');
		   $('.btnAprobar').show();
		   $('.btnAprobar').attr('data-toggle','modal');
		   $('.btnAprobar').attr('data-target','#Modalfacturar');
		   $('.btnAprobar').prop('disabled',false);
		   $('#btnRechazar').prop('disabled',false);

		   {{--FACTURADO--}}
	   } if(estado == '4'){	
		   $('#id_estado').css('color','blue');
		   $('.btnAprobar').hide();
		   deshabilitarCampos();
		   $('.btnAprobar').prop('disabled',true);
		   $('#btnRechazar').prop('disabled',true);
		   $('#btnGuardar').prop('disabled',true);
		   $('#btnCrearVoucher').prop('disabled',true);
		   $('#btnCrearVoucher').html('Control Operativo');
	   }

	   {{--A VERIFICAR--}}
	   if(estado == '3'){
		   $('#id_estado').css('color','#9A7D0A');
	   }

	   {{--EN VERIFICACION--}}
	   if(estado == '41' || estado == '47'){

	   $('#id_estado').css('color','#9A7D0A');
	   $('.btnClassAprobar').html('APROBAR');
	   $('.btnAprobar').show();
	   $('.btnAprobar').attr('data-toggle','modal');
	   $('.btnAprobar').attr('data-target','#Modalfacturar');
	   }

	   {{--SI NO ESTA FACTURADO--}}
	   if(estado != '4'){
		   verificarSaldoMonto();
	   }

	   {{--SOLICITAR AUTORIZACION--}}
	   if(estado == '24'){

		   $('.btnAprobar').css('display','none');
		   $('#id_estado').css('color','#F65C09');
		   $('.btnAut').css('display','inline-block');
	   }

	   } else{
		   $('.btnAprobar').show();
		   $('.btnAprobar').attr('data-toggle','modal');
		   $('.btnAprobar').attr('data-target','#Modalfacturar');
		   $('.btnAut').css('display','inline-block');
	   }

   }//function


   $('#btnCrearVoucher').click(function(){ 
	   console.log($('#idDetalleProforma').val());
	   $('#detalle_proforma').val($('#idDetalleProforma').val());
	   $("#btnAceptarVoucher").addClass('hidden');
   })


/**
* Calendario configuraciones
* @return {[type]} [description]
*/
function fechaDateRange(){

var options = {};
		   
		   options.showDropdowns   = false;
		   options.autoApply       = true;
		   options.autoUpdateInput = false;
		   options.linkedCalendars = true;
		   options.locale          = { 
			   format: 'DD/MM/YYYY',
			   fromLabel: 'Desde',
			   toLabel: 'Hasta',
			   customRangeLabel: 'Seleccionar rango',
			   daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			   monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
				   'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
				   'Diciembre'],
			   firstDay: 1
									 };



   
   //Iniciar el calendario con la fecha elegida indepenediente del campo clickeado
$('#fechaIn').daterangepicker(options,function(start, end, label){  

	  actualizarfechaCalendario(end,start,'#fechaOut');});



$('#fechaOut').daterangepicker(options,function(start, end, label){
		actualizarfechaCalendario(end,start,'#fechaIn');});



function actualizarfechaCalendario(end,start){ 

	   if(end != "" || start != ""){

   $('#fechaIn').data('daterangepicker').setStartDate(start);
	var checkin = start.format('DD/MM/YYYY');
	$('#fechaIn').val(checkin);

	$('#fechaIn').data('daterangepicker').setEndDate(end); 
	var checkout = end.format('DD/MM/YYYY');
	   $('#fechaOut').val(checkout);
	   }

   

}

$('#pagoProveedorFecha').datepicker({
		   timePicker: true,
		   minDate: 0,
		   format: 'dd/mm/yyyy'
	   });


$('#gastoFecha').datepicker({
		   timePicker: true,
		   minDate: 0,
		   format: 'dd/mm/yyyy'
	   });


}









$('#frmDetallesProforma')[0].reset();

$('.btnError').click(function(){

   $('#mensajaError').addClass('hidden');

});

$('.btnSucces').click(function(){

   $('#mensajeSucces').addClass('hidden');

});

$('.btnSuccesSuperior').click(function(){

   $('#mensajeSuccesSuperior').addClass('hidden');

});

$('.btnErrorSuperior').click(function(){

   $('#mensajaErrorSuperior').addClass('hidden');

});






/**
* Convierte un texto de la forma 2017-01-10 a la forma
* 10/01/2017
*
* @param {string} texto Texto de la forma 2017-01-10
* @return {string} texto de la forma 10/01/2017
*
*/
function formatoFecha(texto){
	   if(texto != '' && texto != null){
	 return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	   } else {
	 return '';	
		   
	   }

   }



// ======================================
// 	DATA TABLE
// ======================================			
	   $("#listadoComentario").dataTable({
		   paging: false,
		   info:false
		   });
	   $("#listado").dataTable({
		   paging: false,
		   info:false
		   });
	   $("#listadoDos").dataTable({
		   paging: false,
		   info:false
		   });
	   $("#listadoPrestador").dataTable({
		   paging: false,
		   info:false
		   });
				

   //Cargar los campos al presionar ver en la tabla Ticket			 
   $('.btnVer').click(function(){
	   var id = $(this).val();
	   cargarCampoDetalle(id);

   });			

   

   function habilitarCampos(){
	   var estado = $('#estadoProforma').val();

	   if(estado != 4){
	   $('.hab_1').attr('readonly',false);
	   $('#btnGuardar').prop('disabled',false);
	   $('#selectProveedor').attr('disabled',false);
	   $('#selectPrestador').attr('disabled',false);
	   $('#currencyCosto').attr('disabled',false);
	   $('#pagoProveedorFecha').prop('disabled',false);
	   $('#formDescripcion').attr('disabled',false);
	   fechaDateRange();
		   
	   }
   }


   function deshabilitarCampos(){
	   
	   $('.hab_1').attr('readonly',true);
	   $('#fechaIn').unbind();
	   $('#fechaOut').unbind();
	   $('#pagoProveedorFecha').prop('disabled',true);
	   $('#btnGuardar').prop('disabled',true);
	   $('#selectProveedor').attr('disabled',true);
	   $('#selectPrestador').attr('disabled',true);
	   $('#currencyCosto').attr('disabled',true);
	   $('#formDescripcion').attr('disabled',true);
	   $('#grupo_asistencia').hide();
   }	


   function comentarioCorto(){
	@php
		$ultimo_comentario = count($comentario) ? $comentario[0]->comentario : '';
		$ultimo_comentario = str_replace(array("\n", "\r"), '', $ultimo_comentario); //Quitar saltos de linea
		$ultimo_comentario = htmlspecialchars($ultimo_comentario, ENT_QUOTES, 'UTF-8'); //Quitar acentos, comillas
	@endphp
	   var ultimoComentario = "{{$ultimo_comentario}}"; 
   
	   var comentarioCorto = ultimoComentario.substring(0, 50);
	   $('#campoComentario').html(comentarioCorto+"...");
   }

   
   function cargarImagen(nombre){
	   direccion = '../../images/ProformaDoc/'+nombre;

	   $('#imgMostrar').attr('src',direccion);
   }

	   /**
		* Comprobar que los datos no sean ceros ni espacios vacios
		*/
	   function comprobar(data) {
		   var ok = 0;
		   var datos = '';

	   for (var i = 0; i < data.length;i++) {
				datos = $(data[i]).val().trim();

				if(data[i] == '#comisionAgencia'){
					if (datos == ''){
						return false;
						break;
					}
				} else if(datos == '' || datos <= 0){
				   return false;
				   break;
				}
		   }
			   return true;
		   

	   }

	   // =======================================
	   // 				AJAX
	   // =======================================
	   // 
	   
	   /**
		* GUARDAR COMENTARIO DEL MODAL COMENTARIOS
		*/
	   
	   $('#btnChat').click(function(){

		   function mensajeDerecha(mensaje,nombre,fechaHora,id,logo){

			   if(logo == ''){
			   logo = 'factour.png';
		   }
			   
			   var rsp = `<!-- Mensaje a la derecha -->
	   <div class="direct-chat-msg right" data="${id}">
		 <div class="direct-chat-info clearfix">
		   <span class="direct-chat-name pull-right">${nombre}</span> 
		   <span class="direct-chat-timestamp pull-left">${fechaHora}</span>
		 </div><!-- /.direct-chat-info -->
		 <img class="direct-chat-img" src="../../personasLogo/${logo}" onerror="this.src='../../../personasLogo/factour.png'" alt="message user image"><!-- /.direct-chat-img -->
		 <div class="direct-chat-text" style=" word-break: break-all;">
		 ${mensaje}
	   </div><!-- /.direct-chat-msg -->
	 </div><!--/.direct-chat-messages-->`;

	 return rsp;

		   }


	   function mensajeIzquierda(mensaje,nombre,fechaHora,id,logo){
		   if(logo == ''){
			   logo = 'avatar.png';
		   }
				   var rsp = `<!-- Mensaje por defecto a la izquierda -->
	   <div class="direct-chat-msg" data="${id}">
		 <div class="direct-chat-info clearfix">
		   <span class="direct-chat-name pull-left">${nombre}</span>
		   <span class="direct-chat-timestamp pull-right">${fechaHora}</span>
		 </div><!-- /.direct-chat-info -->
		  <img class="direct-chat-img" src="../../personasLogo/${logo}" onerror="this.src='../../../personasLogo/factour.png'"  alt="message user image"><!-- /.direct-chat-img -->
		 <div class="direct-chat-text" style=" word-break: break-all;">
		   ${mensaje}
		 </div><!-- /.direct-chat-text -->
	   </div><!-- /.direct-chat-msg -->`;

	   return rsp;
		   }




			var cantDiv = $('#chatDirect').children().length;
			var lado  = $('#chatDirect div:nth-last-child('+cantDiv+')');
			var idChat = lado.attr('data');
	console.log('idChat=>'+idChat);
			var esDerecha = $('#chatDirect div:nth-child(0)').hasClass('right');
			var nombre = '{{$idUsuario->nombre}}';
			var imgLogo = '{{$idUsuario->logo}}';
			console.log(imgLogo);
			var idUsuario = $('#idUsuario').val();
			var idProforma = $('#idProforma').val();
			var fechaHora = moment().format('DD/MM/YYYY HH:mm:ss');
			var mensaje = $('#mensajeChat').val();
			var ok = '0';

			console.log('IdUsuario=>'+idUsuario);

			 if(idUsuario == idChat){
				 if(esDerecha){
					 $('#chatDirect').prepend(mensajeDerecha(mensaje,nombre,fechaHora,idUsuario,imgLogo));
				 } else {
					 $('#chatDirect').prepend(mensajeIzquierda(mensaje,nombre,fechaHora,idUsuario,imgLogo));
				 }
			 } else {
				 if(esDerecha){
					 $('#chatDirect').prepend(mensajeIzquierda(mensaje,nombre,fechaHora,idUsuario,imgLogo));
				 } else {
					 $('#chatDirect').prepend(mensajeDerecha(mensaje,nombre,fechaHora,idUsuario,imgLogo));
				 }
			 }
			 $('#mensajeChat').val('');

			 $('#errorChat').html('');
		   var dataString = {"comentario": mensaje,"idUsuario":idUsuario,"idProforma":idProforma};

		   guardarComentarioAjax(dataString);

		   }); 

	   function guardarComentarioAjax(dataString){

			  $.ajax({
					   type: "GET",
					   url: "{{ route('guardarComentario') }}",
					   dataType: 'json',
					   async : false,
					   data: dataString,
					   
						error: function(jqXHR,textStatus,errorThrown){

					  $.toast({
					   heading: 'Error',
					   text: 'Ocurrio un error en la comunicación con el servidor.',
					   showHideTransition: 'fade',
					   icon: 'error'
				   });

					   $('#cargando').addClass('hidden');
				   },
					   success: function(rsp){

						   if(rsp.rsp == 'true'){
						   var comentario = rsp.comentario.comentario;
						   var comentarioCorto = comentario.substring(0, 50);
						   $('#campoComentario').html(comentarioCorto+"...");
						   $('#errorChat').html('Mensaje Enviado');
						   $('#errorChat').css('color','green');
						   console.log('Mensaje');

						   } else {

							   $('#errorChat').html('No se pudo enviar el mensaje');
							   $('#errorChat').css('color','red');
						   }

							   

										   }//function
				   })       
	   }//function
			  

	   //Cambia el estado de la proforma y guarda el comentario si es rechazado
	   function cambiarEstado(estado){
		   console.trace('Cambiar Estado');
		   
		   var ok= 0;	
		   console.log('Estado Anterior = '+$('#estadoProforma').val());
		   var estadoAnterior = $('#estadoProforma').val();
		   console.log('Ingreso estado '+estado);
		   var idProforma = $('#idProforma').val();
		   var dataString = {'estado':estado,'idProforma':idProforma};

		   if(estado != 4){
	 			 $.ajax({
				   type: "GET",
				   url: "{{route('guardarEstado')}}",
				   dataType: 'json',
				   async:false,
				   data: dataString,
				   error: function(jqXHR,textStatus,errorThrown){
					  $.toast({
					   heading: 'Error',
					   text: 'Ocurrio un error en la comunicación con el servidor. - Cambio Estado',
					   position: 'top-right',
					   showHideTransition: 'fade',
					   icon: 'error'
				   });

					   ok++;	
					   $('#cargando').addClass('hidden');
					   $('#estadoProforma').val(estadoAnterior);
					   actualizarCabecera();
				   },
				   success: function(rsp){

					   if(rsp.rsp == true){
						   
   
						   $('#cargando').addClass('hidden');
						   actualizarCabecera();

					   } else {
								$.toast({
					   heading: 'Error',
					   text: 'Ocurrio un error al intentar cambiar el estado.',
					   position: 'top-right',
					   showHideTransition: 'fade',
					   icon: 'error'
				   });
				   

						   ok++;	
						   $('#cargando').addClass('hidden');
						   $('#estadoProforma').val(estadoAnterior);
						   actualizarCabecera();
					 

					   } //else




					}//funcion  
		   }).done(function(){
			   if(ok == 0){
				   if (estado == '1' || estado == '3'){
			   $.blockUI({
				   centerY: 0,
				   message: "<h2>Redirigiendo al listado de Proformas...</h2>",
				   css: {
					   color: '#000'
				   }
			   });
			   
	   //Redirigir si fue facturado con exito
	   location.href ="{{ route('listadosProformas')}}";

   }//estado 1 y 3
}//ok
		   });
		
		} else {
				console.log($('#puntos_venta').val());
				if($('#puntos_venta').val() === null){
					$.toast({
								heading: 'Error',
								text: 'Seleccione un Punto de Venta.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
							});

				} else {
						$.blockUI({
							centerY: 0,
							message: "<h2>Procesando pedido de facturación...</h2>",
							css: {
								color: '#000'
							}
						});


						var idProforma = '{{$proformas[0]->id}}';
						var idPunto = $('#puntos_venta').val();
						$.ajax({
								type: "GET",
								url: "{{route('facturarOperativo')}}",
								dataType: 'json',
								data: {
											idProforma:idProforma,
											idPunto:idPunto
											},
								error: function(jqXHR,textStatus,errorThrown){
									ok++;	
									$.toast({
											heading: 'Error',
											text: 'Ocurrio un error en la comunicación con el servidor.',
											position: 'top-right',
											showHideTransition: 'fade',
											icon: 'error'
										});
									$.unblockUI();
									$('#cargando').addClass('hidden');

									//QUEDA EN ESTADO VERIFICADO
							
								},
								success: function(rsp){
								$.unblockUI();

								if(rsp.err != true){

								$.toast({
									heading: 'Error',
									text: rsp.response,
									position: 'top-right',
									hideAfter: false,
									icon: 'error'
								});
								$('#cargando').addClass('hidden');
								$.unblockUI();
								// //QUEDA EN ESTADO VERIFICADO EL CAMBIO DE ESTADO
								// SE PRODUCE EN EL CONTROLADOR
									actualizarCabecera();


								} else {
									$.unblockUI();
										$.blockUI({
												centerY: 0,
												message: "<h2>Redirigiendo a vista de factura...</h2>",
												css: {
													color: '#000'
												}
											});
									//Redirigir si fue facturado con exito
									location.href ="{{ route('verFactura',['id'=>'']) }}/"+rsp.idFactura;
								}


											}//funcion  
								})
						   
						}
				   }//ELSE ESTADO != 4		

				   actualizarCabecera();

	   }//function


		   /**
		* Generar filas para voucher
		*/
	   $('#btnVoucher').click(function(){

		   $('#btnVoucher').prop("disabled", true);
		   $("#detallesVoucher").empty();
		   $("#btnAceptarVoucher").removeClass('hidden');
		   let cantidad = $('#cantidad_habitaciones').val() ? $('#cantidad_habitaciones').val() : 1;
		   let detalle = '';

			agergarLinea(cantidad);					 

	   })//function click btnVoucher	

	   /**
		* Actualizar los voucher
		*/
	   $('#btnCrearVoucher').click(function(){

		   $("#detallesVoucher").empty();
		   $('#detalle_proforma').val($('#idDetalleProforma').val());
		   let producto_id_detalle_proforma = $('#producto_id').val(); //Produto del detalle de proforma
		   $("#editVoucher").hide();

		   //PRIMERO RECUPERAMOS LOS VOUCHER YA CREADOS
		   $.ajax({
					   type: "GET",
					   url: "{{route('getVoucherUpdate')}}",
					   dataType: 'json',
					   data: { dataVoucher: $('#idDetalleProforma').val() },
					   success: function(rsp){
						
							   if(rsp.vouchers.length != 0){
							
								   let total = rsp.vouchers.length;
								   let detalle = '';
								   let d = '';
								   let month = '';
								   let day = '';
								   let output = '';

								   $('#indiceVoucher').val(0);
								   $('#indiceVoucher').val(total)
								   $("#btnAgregarVoucher").show();
								   $("#editVoucher").show();
								   $("#cabeceraVoucher").css('display', 'none');
						

								   $.each(rsp.vouchers, function (key, item){
									    d = new Date();
									    month = d.getMonth()+1;
									    day = d.getDate();
									    output =  (day<10 ? '0' : '') + day+ '/' + (month<10 ? '0' : '') + month + '/' + d.getFullYear();

									   if(item.fecha_in != null){
										   fechaIn = item.fecha_in;
										   fechaIn1 = fechaIn.split(' ');
										   fechaInT = fechaIn1[0].split('-');
										   fechaTIn = fechaInT[2]+"/"+fechaInT[1]+"/"+fechaInT[0];
									   }else{
										   fechaTIn =output;
									   }
									   if(item.fecha_out != null){
										   fechaOut = item.fecha_out;
										   fechaOut1 = fechaOut.split(' ');
										   fechaOutT = fechaOut1[0].split('-');
										   fechaOutTIn = fechaOutT[2]+"/"+fechaOutT[1]+"/"+fechaOutT[0];
									   }else{
										   fechaOutTIn =output;
									   }	

									   periodo = fechaTIn+" - "+fechaOutTIn; 

									   detalle += '<div class="row" id="lineaVoucher'+item.id+'" style=" margin-top: 20px;">';
									   detalle += '<div class= "col-md-2" style="padding-left: 0px;">';	
									   detalle += '<label>Producto</label>';			
									   detalle += '<select class="form-control input-sm select2 productoVoucher" name="base['+key+'][producto]" id="product_'+key+'"  style="width: 100%;">';
									   detalle += '<option value="0">Seleccione Producto</option>';
									   @foreach($productosVoucher as $key=>$producto)
									   productoId = '{{$producto->id}}';
									   if(productoId == item.producto_id){
										   detalle += '<option value="{{$producto->id}}" selected="selected">{{$producto->denominacion}}</option>';
									   }else{
										   detalle += '<option value="{{$producto->id}}">{{$producto->denominacion}}</option>';
									   }
									   @endforeach	
									   detalle += '</select>';
									   detalle += '</div>';

										   detalle += '<div class="col-12 col-sm-2 col-md-2" id="inputTipoDocumento" style="padding-left: 0px;padding-right: 0px;">';
										   detalle += '<div class="row">';
										   detalle += '<div class="col-md-10" style="padding-right: 0px;">';
										   detalle += '<div class="form-group">';	
										   detalle += '<label for="">Prestador<i class="fa fa-fw fa-asterisk"></i></label>';
										   detalle += '<select class="form-control arturopresi"  id="selectPrestador_'+key+'"name="base['+key+'][id_prestador]" style="width: 100%;">';
										   detalle += '<option value="">Seleccione Prestador.</option>';
										   
										
											detalle += '<option selected  value="' +item.prestador.id + '" data="' + item.prestador.id + '">' + item.prestador.nombre + '</option>';
											
											detalle += '</select>';
											detalle += '</div>';
											detalle += '</div>';
			
										   detalle += '</div>';

										   detalle += '</div>';
									   //   } 								            		
									   detalle += '<div class= "col-md-2" style="padding-left: 0px;">';	
									   detalle += '<label>Tipo/Servicio</label>';	
									   detalle += '<input type="text" required class = "Requerido form-control input-sm" name="base['+key+'][descripcion]" id="descripcion" value="'+item.descripcion_servicio+'"/>';	
									   detalle += '<input type="text" required class = "Requerido form-control input-sm" name="base['+key+'][noches]" id="noches" style="display:none" value="'+item.noches+'"/>';											
									   detalle += '</div>';

									   detalle += '<div class= "col-md-1" style="padding-left: 10px;">';	
									   detalle += '<label>Comentario</label><br>';	
									   detalle += '<textarea name="base['+key+'][comentario]" style=" display:none;" id="comentarioDetalle_'+key+'">'+item.comentario+'</textarea>';
									   detalle += '<a data-toggle="modal" title="Seleccionar Prestador" href="#requestModalComentario" onclick="identificarComentario('+key+')" id="descrip_0" data="0" class="btn-prestador-request"  style="margin-left: 5px;" ><i class="ft-edit-3" style="font-size: xx-large;"></i></a>';
									   detalle += '</div>';
									   detalle += '<div class= "col-md-1" style="padding-left: 0px;">';
									   detalle += '<label>Fecha In/Out</label>';	
									   detalle += '<input type="text"  class = "Requerido form-control input-sm fechaInOut" id="fechaInOut_'+key+'" name="base['+key+'][fecha_in_out]" value="'+periodo+'" />';
									   detalle += '</div>';
									   detalle += '<div class= "col-md-1" style="padding-left: 0px;">';
									   detalle += '<div class="row">';
									   detalle += '<div class= "col-md-6" style="padding-right: 7px;">';
									   detalle += '<label>Adultos</label>';	
									   detalle += '<input type="text" onkeypress="return justNumbers(event);" maxlength="2" required class = "Requerido form-control input-sm" name="base['+key+'][adultos]" id="comentario" value="'+item.cant_adultos+'"/>';	
									   detalle += '</div>';
									   detalle += '<div class= "col-md-6" style="padding-left: 7px;">';	
									   detalle += '<label>Niños</label>';	
									   detalle += '<input type="text" required onkeypress="return justNumbers(event);" maxlength="2" class = "Requerido form-control input-sm" name="base['+key+'][ninhos]" id="comentario" value="'+item.cant_child+'"/>';
									   detalle += '</div>';
									   detalle += '</div>';
									   detalle += '</div>';	
									   detalle += '<div class= "col-md-2" style="padding-left: 0px;">';	
									   detalle += '<label>Pasajero Principal</label>';	
									   detalle += '<input type="text" required class = "Requerido form-control input-sm" name="base['+key+'][pasajero_principal]" id="comentario"  value="'+item.pasajero_principal+'"/>';	
									   detalle += '</div>';
									   detalle += '<div class= "col-md-1" style="padding-left: 0px;">';	
									   detalle += '<label></label><br>';
									   detalle += '<a id="'+item.id+'"class="btn btn-danger" deleteLineaVoucher" onclick ="eliminarLinea('+item.id+')"  style="margin-left: 5px;height: 37.979166px;width: 39.979166px;margin-top: 10px;"><i class="ft-x-circle" style="font-size: 15px;color:white"></i></a>';
								  
									   detalle += '</div>';
									   detalle += '</div>';
									   detalle += '</div>';
									   detalle += '</div>';
					
								   })	


								   $("#detallesVoucher").html(detalle); //Cargar html

								   prestador('arturopresi'); //Iniciar selector clase arturopresi
								   selectorFecha('fechaInOut'); //Iniciar selector de fecha
								   $('.productoVoucher').select2(); //Iniciar selector de productos

									//Deshabilitar elementos por defecto
								   $('.productoVoucher').prop('disabled',true) //deshabilitar selectores de select2 productos
								   $(".arturopresi").prop("disabled", true); //deshabilitar selectores de prestador
								   $(".fechaInOut").prop("disabled", true);

								   //Estos productos son paquetes de DTP
								   if(producto_id_detalle_proforma == 15 || producto_id_detalle_proforma == 38014){
									  $('.productoVoucher').prop('disabled',false) 
									  $(".arturopresi").prop("disabled", false);
									  $(".fechaInOut").prop("disabled", false);
								   }


								   $('#addVoucher').hide();
								   $('#editVoucher').show();	
								   $('#btnEditarVoucher').css('display', 'block');


							   } else {
								
								//SI NO TENEMOS VOUCHER CREAMOS UNA NUEVA LINEA
								   $('#addVoucher').show();
								   $('#btnAceptarVoucher').css('display', 'block');
								   $('#btnEditarVoucher').hide();
								   $('#btnAgregarVoucher').hide();
								   $('#btnVoucher').show();
								   $("#cabeceraVoucher").css('display', 'block');
								   $("#detallesVoucher").empty();

								//    agergarLinea();								
						   }	
					   },
					   error:function(){
							$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la comunicación con el servidor, vuelva a intentarlo',
								showHideTransition: 'fade',
								position: 'top-right',
								icon: 'error'
							});

					   }
			   });

	   });


	   
   $("#destino").select2({
				   ajax: {
						   url: "{{route('destinoProforma')}}",
						   dataType: 'json',
						   placeholder: "Seleccione un destino",
						   delay: 0,
						   data: function (params) {
							   return {
								   q: params.term, // search term
								   page: params.page
							   };
						   },
						   processResults: function (data, params){
							   var results = $.map(data, function (value, key) {
								   return {
									   children: $.map(value, function (v) {
										   return {
											   id: key,
											   text: v
										   };
									   })
								   };
							   });
							   return {
								   results: results,
							   };
						   },
						   cache: true
					   },
					   escapeMarkup: function (markup) {
						   return markup;
					   }, // let our custom formatter work
					   minimumInputLength: 3,
	   });


   $('#btnBuscarPrestador').click(function(){
	   if($('#DestinoId').val() != 0){
		   if($('#nombrePrestador').val().length != 0){
			   var dataString = $("#frmPrestador").serialize();
			   $.ajax({
			   type: "GET",
			   url: "{{route('buscarPrestador')}}",
			   dataType: 'json',
			   data: dataString,
					   success: function(rsp){
							   var oSettings = $('#listadoPrestador').dataTable().fnSettings();
							   var iTotalRecords = oSettings.fnRecordsTotal();
							   for (i=0;i<=iTotalRecords;i++) {
								   $('#listadoPrestador').dataTable().fnDeleteRow(0,null,true);
							   }
							   $.each(rsp, function (key, item){
								   var accion = "<a href='#' data='"+item.id+"' class='btn btn-info text-center btn transaction_normal hide-small normal-button btnPrestador' style='padding-top: 5px; backgroundpadding-top: 5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;padding-bottom: 5px;' role='button'><i class='fa fa-check' aria-hidden='true'></i></a>";
								   var dataTableRow = [
													   item.name,
													   item.address,
													   item.phone,
													   accion
													   ];

								   var newrow = $('#listadoPrestador').dataTable().fnAddData(dataTableRow);
								   // set class attribute for the newly added row 
								   var nTr = $('#listadoPrestador').dataTable().fnSettings().aoData[newrow[0]].nTr;
								   // and parse the row:
								   var nTds = $('td', nTr);
							   })	

							   guardarPrestador();

					   }
			   })	
		   }else{
			   $("#nombrePrestador").css('border-color', 'red');
			   $.toast({
					   heading: 'Error',
					   text: 'Ingrese un Nombre.',
					   showHideTransition: 'fade',
					   position: 'top-right',
					   icon: 'error'
				   });				
		   }	
	   }else{
		   $("#DestinoId").find('.select2-container--default .select2-selection--single').css('border','1px solid red');
			   $.toast({
					   heading: 'Error',
					   text: 'Ingrese un Destino.',
					   showHideTransition: 'fade',
					   position: 'top-right',
					   icon: 'error'
				   });				
	   }	
   });

	   /**
		* Guarda la informacion que se edito en el campo de los detalles de proforma
		*/
   $('#btnGuardar').click(function(event){
	   event.preventDefault();
	   var datos = ['#selectPrestador',
					'#selectProveedor',
					'#codigoConfirmacion',
					'#fechaOut',
					'#fechaIn',
					'#comisionAgencia',
					'#precioCosto',
					'#currencyCosto',
					'#precioCosto'
					];

				   

	   if(comprobar(datos)){
		$('#precioCosto').prop( "disabled", false );
	   dataString = $('#frmDetallesProforma').serialize();
	   var v = $('#frmDetallesProforma').serializeArray();
	   console.log(v);

			  $.ajax({
				   type: "GET",
				   url: "{{route('guardarEditDetalleProforma')}}",
				   dataType: 'json',
				   data: dataString,

					error: function(jqXHR,textStatus,errorThrown){

					  $.toast({
					   heading: 'Error',
					   text: 'Ocurrio un error en la comunicación con el servidor.',
					   position: 'top-right',
					   showHideTransition: 'fade',
					   icon: 'error'
				   });

					   $('#cargando').addClass('hidden');
				   },
				   success: function(rsp){

				   if(rsp.rsp === true){
					  verificarDetalle(1);
							   $.toast({
										heading: 'Exito',
									   text: 'Se ha creado el Detalle.',
									   position: 'top-right',
									   showHideTransition: 'slide',
									   icon: 'success'
								   });  

						$('#frmDetallesProforma')[0].reset();
						deshabilitarCampos();


				   } else {
					   $.toast({
							   heading: 'Error',
							   text: 'Vaya vaya algo salio mal al intentar guardar los datos.',
							   showHideTransition: 'fade',
							   icon: 'error'
						   })
				   }

			   }//funcion  
		   }).done(function(){
				actualizarCabecera();
				$('#btnVerificarDetalle').hide();	
				$('#btnAnularVerificacionDetalle').hide();

		   });

	   } else {
		   $.toast({
			   heading: 'Error',
			   text: 'Complete los campos requeridos porfavor.',
			   showHideTransition: 'fade',
			   icon: 'error'
		   });

	   }//else


			   });



   function guardarPrestador(){
	   $('.btnPrestador').click(function(){
		   var idPrestador = $(this).attr('data');
		   var dataString  = {'idPrestador':idPrestador};
		   $.ajax({
				   type: "GET",
				   url: "{{route('guardarPrestador')}}",
				   dataType: 'json',
				   data: dataString,
					  error: function(jqXHR,textStatus,errorThrown){
						  $.toast({
						   heading: 'Error',
						   text: 'Ocurrio un error en la comunicación con el servidor.',
						   position: 'top-right',
						   showHideTransition: 'fade',
						   icon: 'error'
					   });
						   $.unblockUI();

					   },
				   success: function(rsp){
						   linea = $("#lineaVoucher").val();
						   var newOption = new Option(rsp.nombre, rsp.id, false, false);
						   $('.selectPrestador').append(newOption);
						   $("#selectPrestador_"+linea).select2().val(rsp.id).trigger("change");
						   $("#nombrePrestador").val("");
						   $("#requestModalPrestador").modal('hide');
				   }
				   
		   })		
	   });

   }






	   /**
		*Llamado por el evento de btn ver de la tabla detalle proforma
		* Con el id detalle proforma cargar los datos en el formulario
		* @param  {[type]} id [description]
		* @return {[type]}    [description]
		*/
	function cargarCampoDetalle(id){
	   


	   $('#idDetalleProforma').val(id);
	   $('#cargando').removeClass('hidden');
		$('#detalle_proforma').val(id);	

		 $.ajax({
				   type: "get",
				   url: "{{route('getDetallesAjax')}}",
				   dataType: 'json',
				   data: {
					   id:id
				   },
				   error: function(jqXHR,textStatus,errorThrown){

					   $.toast({
					   heading: 'Error',
					   text: 'Ocurrio un error en la comunicación con el servidor.',
					   showHideTransition: 'fade',
					   icon: 'error'
				   });

					   $('#cargando').addClass('hidden');
				   },		
				   success: function(rsp){

					   console.log(rsp.datos[0]);

					   $('#btnAnularVerificacionDetalle').hide();
					   $('#btnVerificarDetalle').hide();	
					   if(rsp.datos[0].verificado_operativo == true){
						   $('#btnAnularVerificacionDetalle').show();
						   $('#btnVerificarDetalle').hide();
					   } else {
						   $('#btnVerificarDetalle').show();
						   $('#btnAnularVerificacionDetalle').hide();
					   }
					   
						 


					   //proforma_detalle_{$detalle->id}

					   $('#cargando').addClass('hidden');

					   resetForm();
					   if(rsp.datos != false && rsp.datos != 'null'){


						   // idProveedor =  idProveedor.toString();
						   // idPrestador = idPrestador.toString();
						   var btnVoucher = rsp.datos[0].producto.genera_voucher;
						   console.log(rsp);

						   if(btnVoucher == true){
							   console.log('Si genera Vooucher');
							   $('#btnCrearVoucher').attr('disabled',false);
						   } else {
							   console.log('NO genera Vooucher');
							   $('#btnCrearVoucher').attr('disabled',true);
						   }



				   $('#formDescripcion').val(rsp.datos[0].descripcion);
				   $('#costoGravado').val(rsp.datos[0].costo_gravado);
					$('#precioCosto').val(rsp.datos[0].costo_proveedor);
				
				  
				   $('#precioVenta').val(rsp.datos[0].precio_venta);
				   $('#gastoFecha').val(formatoFecha(rsp.datos[0].fecha_gasto));
				   $('#currencyVenta').val((rsp.datos[0].proforma.currency != null) ? rsp.datos[0].proforma.currency.currency_code : '');

				   // $('#currencyCosto').val(rsp.datos[0].currency_costo_id);
				   $('#pagoProveedorFecha').val(formatoFecha(rsp.datos[0].fecha_pago_proveedor));


				   
				   // $("#selectPrestador").val(idPrestador).trigger('change.select2');
				   $("#currencyCosto").val(rsp.datos[0].currency_costo_id).trigger('change.select2')
				   //$('#currencyCosto').next(".select2-container").hide();
				   $("#currencyTextBlock").removeClass( "hidden" ).val(rsp.datos[0].currency_costo['currency_code']).prop( "disabled", true );
				   //console.log('currencyCosto '+rsp.datos[0].currency_costo_id);

					  // console.log(rsp.datos[0]);



				   $('#producto').val(rsp.datos[0].producto.denominacion);
				   $('#producto_id').val(rsp.datos[0].producto.id);
				   $('#comisionAgencia').val(rsp.datos[0].porcentaje_comision_agencia);
				   $('#codigoConfirmacion').val(rsp.datos[0].cod_confirmacion);


				   $('#fechaIn').val(formatoFecha(rsp.datos[0].fecha_in));
				   $('#fechaOut').val(formatoFecha(rsp.datos[0].fecha_out));




					   if( rsp.proveedor != 'null' && rsp.proveedor != ''){
						   
						   var idProveedor = (rsp.datos[0].id_proveedor != null) ? rsp.datos[0].id_proveedor : 0;
						   

						   //Se carga el Proveedor de acuerdo al producto del detalle
						 $('#selectProveedor').empty();
						 $('#selectProveedor').select2({disabled: false});

						  var newOption = new Option('Seleccione Proveedor','', false, false);

						   $('#selectProveedor').append(newOption);

						   $.each(rsp.proveedor, function (key, item){
							newOption = new Option(item.nombre, item.id, false, false);
						   $('#selectProveedor').append(newOption);
					   });

						   $("#selectProveedor").val(idProveedor).trigger('change.select2');


					   } else {
						   //mensaje de errror
						$('#selectProveedor').empty();   
					   $('#selectProveedor').select2({placeholder: 
						   "Seleccione Proveedor",disabled: true });
					   } //else



						   if(rsp.prestador != 'null' && rsp.prestador != ''){
							   var idPrestador = (rsp.datos[0].id_prestador != null) ? rsp.datos[0].id_prestador : 0;

							 //Se carga el Prestador de acuerdo al producto del proveedor y los prestadores disponibles
						 $('#selectPrestador').empty();
						 $('#selectPrestador').select2({disabled: false});

						  var newOption = new Option('Seleccione Prestador','', false, false);

						   $('#selectPrestador').append(newOption);

						  $.each(rsp.prestador, function (key, item){
							newOption = new Option(item.nombre, item.id, false, false);
						   $('#selectPrestador').append(newOption);
					   });

						  $("#selectPrestador").val(idPrestador).trigger('change.select2');

						   } else {

						   $('#selectPrestador').empty();   
						   $('#selectPrestador').select2({placeholder: 
						   "Seleccione Prestador",disabled: true });
						   } //else


						   console.log('Vuelta');
						   $('#grupo_aereo').hide();
						   $('#grupo_asistencia').hide();
						   $('#cantidad_personas_asistencia').val('');
						   $('#cantidad_dias_asistencia').val('');
						   $('#textAreaCod').html('');

						   $('#grupo_translado').hide();
						   $('#num-vuelo-out').val('');
						   $('#num-vuelo-in').val('');

						   //GRUPO AEREO	
						   if(rsp.datos[0].producto.id_grupos_producto == 1 ){
							   console.log('GRUPO AEREO');
							   $('#grupo_aereo').show();
							   $('#textAreaCod').html(rsp.datos[0].cod_confirmacion);
						   }


						 // GRUPO OTROS/TERRESTRE
						  if(rsp.datos[0].producto.id_grupos_producto == 6 ){
							   console.log('GRUPO OTROS/TERRESTRE');
							   $('#grupo_translado').show();
							   $('#num-vuelo-out').val(rsp.datos[0].vuelo_out);
							   $('#num-vuelo-in').val(rsp.datos[0].vuelo_in);
						   }
				  

						   //GRUPO ASISTENCIA
						   if(rsp.datos[0].producto.id_grupos_producto == 3 ){
							   console.log('GRUPO ASISTENCIA');
								  $('#grupo_asistencia').show();
								  $('#cantidad_personas_asistencia').val(rsp.datos[0].asistencia_cantidad_personas);
								  $('#cantidad_dias_asistencia').val(rsp.datos[0].asistencia_dias);
								  // console.log(rsp);
								  $('#tipo_asistencia').val(rsp.tipo_asistencia);
						   }


					  if(rsp.datos[0].id_producto != '1'  && 
							rsp.datos[0].id_producto != '33'  && 
							rsp.datos[0].id_producto != '9'  && 
							rsp.datos[0].id_producto != '16' && 
								 rsp.datos[0].origen == 'M'  && 
							$('#estadoProforma').val() != 4 || editarNemo ){

						   habilitarCampos();
							   
					   } else {

						   deshabilitarCampos();
						   }
					   } else {

				   $.toast({
					   heading: 'Error',
					   text: 'Ocurrio un error en la comunicación con el servidor.',
					   showHideTransition: 'fade',
					   icon: 'error'
				   });

					   $('#cargando').addClass('hidden');
					   
					   }//else de rsp.datos

				 }//funcion  
		   });//ajax
   }//function



   function editarVoucher(){
	  $('.productoVoucher').prop('disabled',false)
	  $(".fechaInOut").prop("disabled", false); 
	  $(".arturopresi").prop("disabled", false); 

	  $('#btnEditarVoucher').prop('disabled',true); //Boton de guardar
	  $('#btnEditarVoucher').html('En Proceso...');

	   var dataString = $("#frmVoucher").serialize();
	   let producto_id_detalle_proforma = $('#producto_id').val(); //Produto del detalle de proforma

	  //Productos paquetes, puede editar todo
	  if(producto_id_detalle_proforma != 15 && producto_id_detalle_proforma != 38014){
	  	$('.productoVoucher').prop('disabled',true)
	  	$(".fechaInOut").prop("disabled", true); 
	  	$(".arturopresi").prop("disabled", true); 
	  }


		  $.ajax({
			   type: "POST",
			   url: "{{route('editarVoucher')}}",
			   dataType: 'json',
			   data: dataString,
			   success: function(rsp){

				$('#btnEditarVoucher').prop('disabled',false); //Boton de guardar
	  			$('#btnEditarVoucher').html('Guardar');

							   if(rsp.status == 'OK'){
								   $("#requestModalVoucher").modal('hide');
								   $.toast({
										   heading: 'Exito',
										   text: rsp.mensaje,
										   position: 'top-right',
										   showHideTransition: 'slide',
										   icon: 'success'
									   });  
								   limpiar(rsp.item);

							   }else{
									$.toast({
									   heading: 'Error',
									   text: 'Ocurrio un error en la comunicación con el servidor.',
									   showHideTransition: 'fade',
									   icon: 'error'
								   });
							   }
				},
				error: function(){

					$('#btnEditarVoucher').prop('disabled',false); //Boton de guardar
	  			    $('#btnEditarVoucher').html('Guardar');

					$.toast({
						heading: 'Error',
						text: 'Ocurrio error en la comunicación con el servidor.',
						position: 'top-right',
						showHideTransition: 'fade',
						icon: 'error'
					});

				}
	   })


   } 

   function agregarVoucher(){
	   
		$('.productoVoucher').prop('disabled',false)
		$(".fechaInOut").prop("disabled", false); 
	    $(".arturopresi").prop("disabled", false); 

		$('#btnAceptarAgregarVoucher').prop('disabled',true); //Boton de guardar
		$('#btnAceptarAgregarVoucher').html('En Proceso...');

	   var dataString = $("#frmVoucher").serialize();
	   let producto_id_detalle_proforma = $('#producto_id').val(); //Produto del detalle de proforma

	   //Productos paquetes, puede editar todo
	   if(producto_id_detalle_proforma != 15 && producto_id_detalle_proforma != 38014){
			$('.productoVoucher').prop('disabled',true)
			$(".fechaInOut").prop("disabled", true); 
			$(".arturopresi").prop("disabled", true); 
	   }



	   $.ajax({
			   type: "POST",
			   url: "{{route('guardarVoucher')}}",
			   dataType: 'json',
			   data: dataString,
			   success: function(rsp){

				$('#btnAceptarAgregarVoucher').prop('disabled',false); //Boton de guardar
				$('#btnAceptarAgregarVoucher').html('Guardar');

						   if(rsp.status == 'OK'){
								   $("#requestModalVoucher").modal('hide');
								   $.toast({
										   heading: 'Exito',
										   text: rsp.mensaje,
										   position: 'top-right',
										   showHideTransition: 'slide',
										   icon: 'success'
									   });  
								   limpiar(rsp.item);
							   }else{
									$.toast({
									   heading: 'Error',
									   text: 'Ocurrio un error en la comunicación con el servidor, vuelva a intentarlo',
									   showHideTransition: 'fade',
									   icon: 'error'
								   });
							   }

			},
			error: function(){

				$('#btnAceptarAgregarVoucher').prop('disabled',false); //Boton de guardar
				$('#btnAceptarAgregarVoucher').html('Guardar');

				$.toast({
					heading: 'Error',
					text: 'Ocurrio error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});
			}
	   })	
   }

   function eliminarLinea(id){
	   $("#lineaVoucher"+id).remove();	
   }


   function agergarLinea( cantidad = 1){
	
	   let detalle = '';
	   let producto_id_detalle_proforma =  $('#producto_id').val(); //Produto del detalle de proforma
	   $.ajax({
			   type: "GET",
			   url: "{{route('getDetalleProducto')}}",
			   dataType: 'json',
			   data: {
				   dataDetalleProforma: $('#detalle_proforma').val(),
			   },
			   success: function(rsp){

								if(rsp.status != 'ok'){
									return '';
								}


							   let productoId;
							   
							   let detalle = "";
							   for (i = 0; i < cantidad; ++i ){
													   idNumero = parseInt($('#indiceVoucher').val())+1;
									
													   detalle += '<div class="row" id="lineaVoucher'+idNumero+'" style=" margin-top: 20px;">';
													//--------------------------------------------------------------------
													   detalle += '<div class= "col-md-2" style="padding-left: 0px;">';	
													   detalle += '<label>Producto</label>';			
													   detalle += '<select class="form-control input-sm select2 productoVoucher" name="base['+idNumero+'][producto]" id="productoVoucher'+idNumero+'" style="width: 100%;">';
													   detalle += '<option value="0">Seleccione Producto</option>';

													   @foreach($productosVoucher as $key=>$producto)
														   productoId = '{{$producto->id}}';
														   if(productoId == rsp.producto){
															   detalle += '<option value="{{$producto->id}}" selected="selected">{{$producto->denominacion}}</option>';
														   }else{
															   detalle += '<option value="{{$producto->id}}">{{$producto->denominacion}}</option>';
														   }

													   @endforeach	
													   detalle += '</select>';
													   detalle += '</div>';
													//--------------------------------------------------------------------
													   detalle += '<div class="col-12 col-sm-2 col-md-2" id="inputTipoDocumento">';
													   detalle += '<div class="form-group">';
													   detalle += '<label for="">Prestador</label>';
														 detalle += '<select class="form-control arturopresi"  id="selectPrestador_'+idNumero+'" name="base['+idNumero+'][id_prestador]" style="width: 100%;">';
													   detalle += '<option value="">Seleccione Prestador.</option>';
													
														detalle += '<option selected  value="' +rsp.prestador.id + '" data="' + rsp.prestador.id + '">' + rsp.prestador.nombre + '</option>';
													   detalle += '</select>';
													   detalle += '</div>';
													   detalle += '</div>';
													//--------------------------------------------------------------------
													   detalle += '<div class= "col-md-2" style="padding-left: 0px;">';	
													   detalle += '<label>Tipo/Servicio</label>';	
													   detalle += '<input type="text" required class = "Requerido form-control input-sm" name="base['+idNumero+'][descripcion]" id="descripcion"/>';
													   detalle += '<input type="text" required class = "Requerido form-control input-sm" name="base['+idNumero+'][noches]" id="noches" style="display:none"/>';															
													   detalle += '</div>';
													//--------------------------------------------------------------------

													   detalle += '<div class= "col-md-1" style="padding-left: 10px;">';	
													   detalle += '<label>Comentario</label><br>';	
													   detalle += '<textarea name="base['+idNumero+'][comentario]" style=" display:none;" id="comentarioDetalle_'+idNumero+'"></textarea>';
													   detalle += '<a data-toggle="modal" title="Seleccionar Prestador" href="#requestModalComentario" onclick="identificarComentario('+idNumero+')" id="descrip_0" data="0" class="btn-prestador-request"  style="margin-left: 5px;" ><i class="ft-edit-3" style="font-size: xx-large;"></i></a>';
													   detalle += '</div>';

													//--------------------------------------------------------------------
													   detalle += '<div class= "col-md-1" style="padding-left: 0px;">';
													   detalle += '<label>Fecha In/Out</label>';	
													   detalle += '<input type="text"  class = "Requerido form-control input-sm fecha_in_out_modal" id="fechaInOut_'+idNumero+'" name="base['+idNumero+'][fecha_in_out]" value="'+$('#fechaIn').val()+' - '+$('#fechaOut').val()+'" />';
													   detalle += '</div>'; 
													//--------------------------------------------------------------------
													   detalle += '<div class= "col-md-1" style="padding-left: 0px;">';
													   detalle += '<div class="row">';
													   detalle += '<div class= "col-md-6" style="padding-right: 7px;">';
													   detalle += '<label>Adultos</label>';	
													   detalle += '<input type="text" onkeypress="return justNumbers(event);" maxlength="2" required value = "0" class = "Requerido form-control input-sm" name="base['+idNumero+'][adultos]" id="comentario"/>';	
													   detalle += '</div>';
													   detalle += '<div class= "col-md-6" style="padding-left: 7px;">';	
													   detalle += '<label>Niños</label>';	
													   detalle += '<input type="text" required onkeypress="return justNumbers(event);" maxlength="2" value = "0" class = "Requerido form-control input-sm" name="base['+idNumero+'][ninhos]" id="comentario"/>';
													   detalle += '</div>';
													   detalle += '</div>';
													   detalle += '</div>';
													//--------------------------------------------------------------------
													   detalle += '<div class= "col-md-2" style="padding-left: 0px;">';	
													   detalle += '<label>Pasajero Principal</label>';	
													   detalle += `<input type="text" required class = "Requerido form-control input-sm" name="base[`+idNumero+`][pasajero_principal]" id="comentario" value= ""/>`;	
													   detalle += '</div>';
													   detalle += '<div class= "col-md-1" style="padding-left: 0px;">';	
													   detalle += '<label></label><br>';
													   detalle += '<a id="'+idNumero+'" class="btn btn-danger" deleteLineaVoucher" onclick ="eliminarLinea('+idNumero+')"  style="margin-left: 5px;height: 37.979166px;width: 39.979166px;margin-top: 10px;"><i class="ft-x-circle" style="font-size: 15px;color:white"></i></a>';
													   detalle += '</div>';
													   detalle += '</div>';	
													   $('#indiceVoucher').val(idNumero);


							   }// foreach

							   $("#detallesVoucher").append(detalle); //Cargar HTML
							   prestador('arturopresi');
							   selectorFecha('fechaInOut');
							   $('.productoVoucher').select2(); //Iniciar selector de productos

							   $('.productoVoucher').prop('disabled',true) //deshabilitar selectores de select2 productos
							   $(".fechaInOut").prop("disabled", true); //deshabilitar selectores de fecha
							   $(".arturopresi").prop("disabled", true); //deshabilitar selectores de prestador
							   
							   //Productos paquetes, puede editar todo
							   if(producto_id_detalle_proforma == 15 || producto_id_detalle_proforma == 38014){
								$('.productoVoucher').prop('disabled',false)
								$(".fechaInOut").prop("disabled", false); 
								$(".arturopresi").prop("disabled", false); 
							   }

							   //Habilitar el boton
							   $('#btnVoucher').prop("disabled", false);
						   },

					error:function(){

						$.toast({
							heading: 'Error',
							text: 'Ocurrio un error en la comunicación con el servidor, vuelva a intentarlo',
							showHideTransition: 'fade',
							position: 'top-right',
							icon: 'error'
						});

					}
		   })						 
		   $('#btnEditarVoucher').show();

		   //AgregarEditarVoucher();

  //});	



   }



	function limpiar(id){

	   $("#periodo_"+id).prop("disabled", true);
	   $("#cantidad_personas_"+id).prop("disabled", true);
	   $("#confirmacion_"+id).prop("disabled", true);
	   $("#descripcion_"+id).prop("disabled", true);
	   $("#costo_"+id).prop("disabled", true);
	   $("#comision_agencia_"+id).prop("disabled", true);
	   $("#venta_"+id).prop("disabled", true);
	   $("#markup_"+id).prop("disabled", true);
   }

	   
   //Eliminar el evento de boton asignado
   $("#requestModalVoucher").on('hidden.bs.modal', function () {
	  $("#btnAceptarVoucher").off();
	  $('.deleteLineaVoucher').off();
});


function numeroAleatorio(min, max) {
	 return Math.round(Math.random() * (max - min) + min);
}

function pulsar(e) {
if (e.which === 13) {
e.preventDefault();
console.log('prevented');
return false;
}
}

function identificarLinea(linea){ 
   $("#lineaDet").val(linea);
}

function identificarComentario(linea){
   //alert(comentario);
   $("#lineaDet").val(linea);
   /*comentario = $("#comentarioDetalle_"+linea).val();
   $('#detalle').summernote('reset');
   $('#detalle').summernote('editor.pasteHTML', comentario);*/

   var comentario = $("#comentarioDetalle_"+linea).val();
   $('#detalle').summernote('reset');
   $("#detalle").summernote("code", comentario);

}

function fechaCabacera(){

   var vencimiento = $('#fechaVenc').val().trim();
 /*  var fecha_in = $('#fecha_in').val().trim();
   var fecha_out = $('#fecha_out').val().trim();


   $('#fecha_out').val(formatoFecha(fecha_out));
   $('#fecha_in').val(formatoFecha(fecha_in));*/
   $('#fechaVenc').val(formatoFecha(vencimiento));
}

/**
* OBTENER DATOS DE LA PROFORMA
*/
function actualizarCabecera(){

   id = {id:$('#idProforma').val()};
		   $.ajax({
			   type: "GET",
			   url: "{{route('infoProforma')}}",
			   dataType: 'json',
			   data: id,
			   success: function(rsp){
						   console.log(rsp);

						   if(rsp.err){
							   // console.log(rsp.err);

							   $('#fecha_out').val(rsp.resp.fecha_out);
							   $('#fecha_in').val(rsp.resp.fecha_in);
							   $('#fechaVenc').val(rsp.resp.fecha_venc);
							   $('#total_bruto').val(rsp.resp.total_bruto);
							   $('#total_neto').val(rsp.resp.total_neto);
							   $('#total_exentas').val(rsp.resp.total_exentas);
							   $('#total_gravadas').val(rsp.resp.total_gravadas);
							   $('#total_iva').val(rsp.resp.total_iva);
							   $('#total_comision').val(rsp.resp.total_comision);
							   $('#estadoProforma').val(rsp.resp.estado_id);
							   $('#id_estado').val(rsp.resp.estado_denominacion);


						   }



							   }//fucntion
	   }).done(function(){
		   verificarEstado();
	   });	


}


	function agregarComentarios(){
		$('#comentarioDetalle_'+$('#lineaDet').val()).val($('#detalle').val());
		$('#comentario_'+$('#lineaDet').val()).val($('#detalle').val());
		$('#requestModalComentario').modal('hide');
	}

    function identificarLinea(linea){ 
    	$("#lineaDet").val(linea);
    }
	
    function identificarComentario(linea){
        //alert(comentario);
        $("#lineaDet").val(linea);
    	/*comentario = $("#comentarioDetalle_"+linea).val();
        $('#detalle').summernote('reset');
        $('#detalle').summernote('editor.pasteHTML', comentario);*/

        var comentario = $("#comentarioDetalle_"+linea).val();
        $('#detalle').summernote('reset');
        $("#detalle").summernote("code", comentario);

    }

	function fechaCabacera(){

		var vencimiento = $('#fechaVenc').val().trim();
		/*var fecha_in = $('#fecha_in').val().trim();
		var fecha_out = $('#fecha_out').val().trim();


		$('#fecha_out').val(formatoFecha(fecha_out));
		$('#fecha_in').val(formatoFecha(fecha_in));*/
		$('#fechaVenc').val(formatoFecha(vencimiento));
	}

	/**
	 * OBTENER DATOS DE LA PROFORMA
	 */
	function actualizarCabecera(){

		id = {id:$('#idProforma').val()};
				$.ajax({
					type: "GET",
					url: "{{route('infoProforma')}}",
					dataType: 'json',
					data: id,
					success: function(rsp){
								console.log(rsp);

								if(rsp.err){
									// console.log(rsp.err);

									$('#fecha_out').val(rsp.resp.fecha_out);
									$('#fecha_in').val(rsp.resp.fecha_in);
									$('#fechaVenc').val(rsp.resp.fecha_venc);
									$('#total_bruto').val(rsp.resp.total_bruto);
									$('#total_neto').val(rsp.resp.total_neto);
									$('#total_exentas').val(rsp.resp.total_exentas);
									$('#total_gravadas').val(rsp.resp.total_gravadas);
									$('#total_iva').val(rsp.resp.total_iva);
									$('#total_comision').val(rsp.resp.total_comision);
									$('#estadoProforma').val(rsp.resp.estado_id);
									$('#id_estado').val(rsp.resp.estado_denominacion);
	

								}



									}//fucntion
			}).done(function(){
				verificarEstado();
			});	


	}
	

	function facturarParcial(id){
		   if($('#puntos_venta').val() === null){
					$.toast({
								heading: 'Error',
								text: 'Seleccione un Punto de Venta.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
							});
			}else{
				return swal({
							title: "GESTUR",
							text: "¿Está seguro que desea generar esta Factura Parcial?",
							showCancelButton: true,
							buttons: {
									cancel: {
											text: "No",
											value: null,
											visible: true,
											className: "btn-warning",
											closeModal: false,
									},
									confirm: {
											text: "Sí, Facturar",
											value: true,
											visible: true,
											className: "",
											closeModal: false
										}
									}
								}).then(isConfirm => {
									if (isConfirm) {
										$.ajax({
											type: "GET",
											url: "{{route('generarFacturaParcial')}}",
											dataType: 'json',
											data: {
												id: id,
												dataPuntoVenta:$('#puntos_venta').val() 
											},
											success: function(rsp){
												if(rsp.status == 'OK'){
														swal("Éxito", rsp.mensaje, "success");
														location.replace("{{route('verFactura',['id'=>''])}}/"+rsp.id);
													}else{
														swal("Cancelado", rsp.mensaje, "error");
													}
												}
											});
									} else {
										swal("Cancelado", "", "error");
									}
					});
			}
		}

		//prestadorloca

		function prestador(class_select){
			$('.'+class_select).select2({
					    ajax: {
					            url: "{{route('prestadorProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
						});

		}

		/**
		 * Activar selector de fechaInOut
		 * */
		function selectorFecha(class_input){
			$('.'+class_input).daterangepicker({
					timePicker24Hour: true,
					timePickerIncrement: 30,
					locale: {
						format: 'DD/MM/YYYY'//H:mm'
							},
					minDate: new Date()
			});	
		}
		

		$(document).ready(function() {
			@if(count($solicitudes) != 0)
				$(".btnAprobar").css('display', 'none');
			@endif
		});


		var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};
  
        
        $("#motivo_autorizacion").select2();

	</script>
@endsection