@extends('masters')
@section('title', 'Booking de Vuelos')
@section('styles')
	@parent
@endsection
<?php
include("../comAerea.php");
include("../destinationFligth.php");
include("../aeronaves.php");
include("../tipo_asiento.php");
?>
@section('content')
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-plane"></i>&nbsp;&nbsp;Localizador / <span class="text-italic">Reference number:</span>&nbsp;&nbsp;
            <span style="font-size: 35px;color: darkblue;">{{$resultado[0]->controlnumber}}</span>
            <!--<small class="pull-right">Date: 2/10/2014</small>-->
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
	      <div class="row">
	      	@foreach($resultado[0]->pasajeros as $key=>$pasajeros)
		        <div class="col-xs-12">
		          <h2 style="margin-left: 5%;">
		            <i class="fa fa-user"></i>&nbsp;&nbsp;{{$pasajeros->apellido}}, {{$pasajeros->nombre}}
		          </h2>
		        </div>
	        @endforeach	
	      </div>
      </div>
      <!-- /.row -->
		<section class="content-header">
		    <h1>
		       Itinerario
		    </h1>
		</section>
		</br>

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
            	<th>Tramo</th>
	            <th>Origen</th>
	            <th>Destino</th>
	            <th>Fecha_Salida</th>
	            <th>Fecha_Llegada</th>
	            <th>Compañia</th>
	            <th>Aeronave</th>
	            <th>Clase</th>
            </tr>
            </thead>
            <tbody>
            	@foreach($resultado[0]->itinerarios as $key=>$itinerario)
            		<?php
                        $timezone = new DateTimeZone('UTC'); 
                        $dateArrival = DateTime::createFromFormat('dmY', $itinerario->fecha_destino, $timezone);
                        $dateDepature = DateTime::createFromFormat('dmY', $itinerario->fecha_origen, $timezone);
                    ?>
		            <tr>
		            	<td>{{$itinerario->tramo}}</td>
			            <td><b>{{$destination[$itinerario->origen]}}</b></td>
			            <td><b>{{$destination[$itinerario->destino]}}</b></td>
			            <td>{{$dateArrival->format('d F')}}&nbsp;&nbsp;{{date("G:i",strtotime($itinerario->hora_destino))}}</td>
			            <td>{{$dateDepature->format('d F')}}&nbsp;&nbsp;{{date("G:i",strtotime($itinerario->hora_origen))}}</td>
			            <td><img src="https://images.kiwi.com/airlines/64/{{$itinerario->carrier_company}}.png" class="imageHead" style="width: 20px;">&nbsp;{{$comAerea[$itinerario->carrier_company]}}</td>
			            <td>
			            @if(isset($aeronave[$itinerario->tipo_aeronave]))
			            	{{$aeronave[$itinerario->tipo_aeronave]}}
			            @else
			            	{{'No definido'}}
			            @endif
			           	</td>
			            <td>{{$tarifa[$itinerario->clase]}}</td>
		            </tr>
		        @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- <div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>-->
 @endsection

@section('scripts')
  @parent
    include('layouts/mc/scripts')    
@endsection
