@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
	@include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Editar Timbrado</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
            	<form id="frmProforma" method="post" action="{{route('doEditTimbrado')}}">
	            	<div class="row"  style="margin-bottom: 1px;">
	            		<input type="hidden" required class = "form-control" name="id" id="id" placeholder="Expedicion" value="{{$timbrado->id}}" style="text-transform: uppercase"/>
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Tipo de Timbrado</label>					            	
								<select class="form-control input-sm select2" name="tipoTimbrado" id="tipoTimbrado" style=" padding-left: 0px;width: 100%;" required >
									<option value=" ">Seleccione Tipo de Timbrado</option>
									@foreach($tipoTimbrados as $tipoTimbrado)
										<option value="{{$tipoTimbrado->id}}">{{$tipoTimbrado->denominacion}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Establecimiento</label>
								<input type="text" required class = "form-control" name="Establecimiento" id="establecimiento" placeholder="Establecimiento" value="{{$timbrado->establecimiento}}" style="text-transform: uppercase" required>
							</div>
						</div>	
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Expedicion</label>
								<input type="text" required class = "form-control" name="expedicion" id="expedicion" placeholder="Expedicion" value="{{$timbrado->expedicion}}" style="text-transform: uppercase" required>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Número</label>
								<input type="text" required class = "form-control" name="numero" id="numero" placeholder="Número de timbrado" value="{{$timbrado->numero}}" style="text-transform: uppercase" required>
							</div>
						</div>
					</div>	
	            	<div class="row"  style="margin-bottom: 1px;">
						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
							<div class="form-group">
								<label>Fecha Inicio (*)</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span id ="inicio" class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" value="{{date('d/m/Y', strtotime($timbrado->fecha_inicio))}}" name="inicio" id="inici" tabindex="11" required>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
							<div class="form-group">
								<label>Vencimiento (*)</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span id ="vencimiento" class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" value="{{date('d/m/Y', strtotime($timbrado->vencimiento))}}" class="form-control pull-right fecha" name="vencimiento" id="vencimient" tabindex="11" required>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Número Inicial</label>
								<input type="text" required class = "form-control" name="numeroInicio" id="numeroI" placeholder="Número Inicial" value="{{$timbrado->numero_inicial}}" style="text-transform: uppercase" required>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Número Final </label>
								<input type="text" required class = "form-control" value="{{$timbrado->numero_final}}" name="numeroFinal" id="numeroF" placeholder="Número Inicial" style="text-transform: uppercase" required>
							</div>
						</div>
					</div>
					<div class="row"  style="margin-bottom: 1px;">
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Número Actual </label>
								<input type="text" disabled="disabled" class = "form-control" name="numeroActual" id="numeroA" placeholder="Número Inicial" value="{{$timbrado->numero_actual}}" style="text-transform: uppercase" required>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Sucursal</label>					            	
								<select class="form-control input-sm select2" name="sucursal" id="sucursal" style=" padding-left: 0px;width: 100%;" required>
									<option value=" ">Seleccione Sucursal</option>
									@foreach($sucursalEmpresas as $sucursalEmpresa)
										<option value="{{$sucursalEmpresa->id}}">{{$sucursalEmpresa->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Centro de Costo</label>					            	
								<select class="form-control input-sm select2" name="centroCosto" id="centroCosto" style=" padding-left: 0px;width: 100%;" required>
									<option value=" ">Seleccione estado</option>
									@foreach($centroCostos as $centroCosto)
										<option value="{{$centroCosto->id}}">{{$centroCosto->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Autorización</label>
								<input type="text" required class = "form-control" name="autorizacion" id="autorizacion" value="{{$timbrado->autorizacion}}" placeholder="Autorización" style="text-transform: uppercase" required>
							</div>
						</div>
					</div>
					<div class="row" style="margin-bottom: 1px;">
						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
							<div class="form-group">
								<label>Fecha de Autorización (*)</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" id="fecha_autorizacion" class="form-control pull-right fecha" name="fecha_autorizacion" value="{{date('d/m/Y', strtotime($timbrado->vencimiento))}}" required>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Activo</label>					            	
								<select class="form-control input-sm select2" name="activo" id="activo" style=" padding-left: 0px;width: 100%;" required>
									<option value="1">Activo</option>
									<option value="">Inactivo</option>
								</select>
							</div>
						</div>
					</div>
				</form>	
				<div class="col-12">
					<button type="submit" form="frmProforma" id="btnGuardar" class="btn btn-success btn-lg pull-right mb-1" tabindex="17"><b>GUARDAR</b></button>
				</div>
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

	<script>
		$("#tipoTimbrado").val("{{$timbrado->id_tipo_timbrado}}").select2();
		$("#sucursal").val("{{$timbrado->id_sucursal_empresa}}").select2();
		$("#centroCosto").val("{{$timbrado->id_sucursal_contable}}").select2();
		$("#activo").val("{{$timbrado->activo}}").select2();
		
		var datepickers = $('#inici'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		var datepickers = $('#vencimient'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 
		var datepicker = $('#fecha_autorizacion'); 
			    if (datepicker.length > 0) { 
			    datepicker.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		$('#inicio').click(function(){
			$('input[name="inicio"]').trigger('click');	
		}) 	
		$('#vencimiento').click(function(){
			$('input[name="vencimiento"]').trigger('click');	
		}) 	
		$('#fecha_autorizacion').click(function(){
			$('input[name="fecha_autorizacion"]').trigger('click');	
		}) 	

		$('.select2').select2();
			
	</script>
@endsection