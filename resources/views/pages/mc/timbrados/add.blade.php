@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
	@include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Agregar Timbrado</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
            	<form id="frmProforma" method="post" action="{{route('doAddTimbrado')}}">
	            	<div class="row"  style="margin-bottom: 1px;">
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Tipo de Timbrado</label>					            	
								<select class="form-control input-sm select2" name="tipoTimbrado" id="tipoTimbrado" style=" padding-left: 0px;width: 100%;" required>
									<option value=" ">Seleccione Tipo de Timbrado</option>
									@foreach($tipoTimbrados as $tipoTimbrado)
										<option value="{{$tipoTimbrado->id}}">{{$tipoTimbrado->denominacion}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Establecimiento</label>
								<input type="text" required class = "form-control" name="Establecimiento" id="establecimiento" placeholder="Establecimiento" style="text-transform: uppercase" required>
							</div>
						</div>	
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Expedicion</label>
								<input type="text" required class = "form-control" name="expedicion" id="expedicion" placeholder="Expedicion" style="text-transform: uppercase" required>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Número</label>
								<input type="text" required class = "form-control" name="numero" id="numero" placeholder="Número de timbrado" style="text-transform: uppercase" required>
							</div>
						</div>
					</div>	
	            	<div class="row"  style="margin-bottom: 1px;">
						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
							<div class="form-group">
								<label>Fecha Inicio (*)</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span id ="inicio" class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="inicio"	id="inici" tabindex="11" required>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
							<div class="form-group">
								<label>Vencimiento (*)</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span id ="vencimiento" class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="vencimiento"	id="vencimient" tabindex="11" required>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Número Inicial</label>
								<input type="text" required class = "form-control" name="numeroInicio" id="numeroI" value = "0" placeholder="Número Inicial" style="text-transform: uppercase" maxlength="6" required>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Número Final </label>
								<input type="text" required class = "form-control" name="numeroFinal" id="numeroF" placeholder="Número Inicial" style="text-transform: uppercase" maxlength="6" required>
							</div>
						</div>
					</div>
					<div class="row"  style="margin-bottom: 1px;">
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Número Actual </label>
								<input type="text" required class = "form-control" name="numeroActual" id="numeroA" value = "0" placeholder="Número Inicial" value= "0" style="text-transform: uppercase" required>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Sucursal</label>					            	
								<select class="form-control input-sm select2" name="sucursal" id="sucursal" style=" padding-left: 0px;width: 100%;" required>
									<option value=" ">Seleccione Sucursal</option>
									@foreach($sucursalEmpresas as $sucursalEmpresa)
										<option value="{{$sucursalEmpresa->id}}">{{$sucursalEmpresa->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Centro de Costo</label>					            	
								<select class="form-control input-sm select2" name="centroCosto" id="centroCosto" style=" padding-left: 0px;width: 100%;" required>
									<option value=" ">Seleccione estado</option>
									@foreach($centroCostos as $centroCosto)
										<option value="{{$centroCosto->id}}">{{$centroCosto->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Autorización</label>
								<input type="text" required class = "form-control" name="autorizacion" id="autorizacion" placeholder="Autorización" style="text-transform: uppercase" required>
							</div>
						</div>
					</div>
					<div class="row" style="margin-bottom: 1px;">
						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
							<div class="form-group">
								<label>Fecha de Autorización (*)</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="fecha_autorizacion"	id="fecha_autorizacion" tabindex="11" required>
								</div>
							</div>
						</div>
					</div>
				</form>	
				<div class="col-12">
					<button type="submit" form="frmProforma" id="btnGuardar" class="btn btn-success btn-lg pull-right mb-1" tabindex="17"><b>GUARDAR</b></button>
				</div>
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

	<script>
		var datepickers = $('#inici'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		var datepickers = $('#vencimient'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		var datepicker = $('#fecha_autorizacion'); 
			    if (datepicker.length > 0) { 
			    datepicker.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		$('#inicio').click(function(){
			$('input[name="inicio"]').trigger('click');	
		}) 	
		$('#vencimiento').click(function(){
			$('input[name="vencimiento"]').trigger('click');	
		}) 	
		$('#fecha_autorizacion').click(function(){
			$('input[name="fecha_autorizacion"]').trigger('click');	
		}) 	


		$('.select2').select2();
			
	</script>
@endsection