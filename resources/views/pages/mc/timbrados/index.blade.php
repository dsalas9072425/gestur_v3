@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
  @parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
  @include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Timbrados</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
          <br>
          <div class="ard-body pt-0" style="margin-right: 45px;">
            <a href="{{route('addTimbrado')}}" title="Agregar Timbrado" class="btn btn-success pull-right"	role="button">
              <div class="fonticon-wrap">
                <i class="ft-plus-circle"></i>
              </div>
            </a>
          </div>		        
          <br>
          <br>
            <div class="card-body"> 
                <table id="listado" class="table">
                  <thead>
                    <tr>
                      <th style="width: 20%">Tipo</th>
                      <th>Establecimiento</th>
                      <th>Expedicion</th>
                      <th>Numero</th>
                      <th>Sucursal</th>
                      <th>Centro<br>Costo</th>
                      <th>Fecha Inicio</th>
                      <th>Fecha Vencimiento</th>
                      <th>Numero Inico</th>
                      <th>Numero Final</th>
                      <th>Numero Actual</th>
                      <th>Estado</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
            </div> 
        </div> 
    </div>        
</section>


@endsection
@section('scripts')
  @include('layouts/gestion/scripts')
  <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

  <script>
    $('#listado').dataTable();

    var datepickers = $('#inici'); 
          if (datepickers.length > 0) { 
          datepickers.datepicker({ 
           format: "dd/mm/yyyy", 
          // startDate: new Date() 
          }); 
          } 

    var datepickers = $('#vencimient'); 
          if (datepickers.length > 0) { 
          datepickers.datepicker({ 
           format: "dd/mm/yyyy", 
          // startDate: new Date() 
          }); 
          } 
    $('#inicio').click(function(){
      $('input[name="inicio"]').trigger('click'); 
    })  
    $('#vencimiento').click(function(){
      $('input[name="vencimiento"]').trigger('click');  
    })  

    $('.select2').select2();


     /*   var dataString = $("#frmBusqueda"+$(this).attr('data')).serialize();
        dataString += '&type='+encabezado;*/
        $.ajax({
          type: "GET",
          url: "{{route('getTimbrados')}}",
          dataType: 'json',
         // data: dataString,
          success: function(rsp){
            var oSettings = $("#listado").dataTable().fnSettings();
            var iTotalRecords = oSettings.fnRecordsTotal();
            for (i=0;i<=iTotalRecords;i++) {
              $("#listado").dataTable().fnDeleteRow(0,null,true);
            }
            $.each(rsp, function (key, item){
                var iconoEditar ='<a href="editTimbrado/'+item.id+'" class="btn btn-success" style="padding-left: 6px;padding-right: 6px;margin-right: 2px;" role="button"><i class="fa fa-fw fa-edit"></i></a>';
                var iconoEditar ='';
                  var checkIn = item.fecha_inicio.split('-');
                  fechaInicio = checkIn[2]+'/'+checkIn[1]+'/'+checkIn[0];
                  var vencimiento = item.vencimiento.split('-');
                  fechaVencimiento = vencimiento[2]+'/'+vencimiento[1]+'/'+vencimiento[0];
                  if(item.activo == true){
                    activo = 'Activo';
                  }else{
                    activo = 'Inactivo';
                  }

                var dataTableRow = [
                          '<b>'+item.tipoTimbrado+'</b>',
                          item.establecimiento,
                          item.expedicion,
                          item.numero,
                          item.sucursal,
                          item.sucursalContable,
                          fechaInicio,
                          fechaVencimiento,
                          item.numero_inicial,
                          item.numero_final,
                          item.numero_actual,
                          '<b>'+activo+'</b>',
                          iconoEditar
                        ];
                var newrow = $("#listado").dataTable().fnAddData(dataTableRow);

                // set class attribute for the newly added row 
                var nTr = $("#listado").dataTable().fnSettings().aoData[newrow[0]].nTr;

            })
          }
        })    


      
  </script>
@endsection
