@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
  @parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
  @include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Timbrados</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <form id="reporteTimbrado">
           <div class="row">
              <div class="col-12 col-sm-3 col-md-4">
                  <div class="form-group" style="padding-left: 15px;">
                    <label>Numero</label>
                    <input type="text" class="form-control" name="numero" value="">
                  </div>
              </div>

              <div class="col-12 col-sm-3 col-md-4">
                  <div class="form-group">
                    <label>Proveedor</label>
                    <select class="form-control select2" name="proveedor" id="proveedor">
                        <option value="">Seleccione Proveedor</option>
                        @foreach($getProveedor as $proveedor)
                          @php
                              $ruc = $proveedor->documento_identidad;
                              if($proveedor->dv){
                                $ruc = $ruc."-".$proveedor->dv;
                              }
                          @endphp
                          <option value="{{$proveedor->id}}">{{$ruc}} - {{$proveedor->nombre}}</b></option>
                        @endforeach
                        </select>
                  </div>
              </div>
              <div class="col-12 col-sm-2 col-md-2">
                  <div class="form-group">
                      <br>
                      <button type="button" class="btn btn-info pull-right btn-lg mr-1" onclick="consultaTimbrado()"><b>Buscar</b></button>
                   </div>
              </div> 
           </div>
        </form>  

        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
                <table id="listado" class="table">
                  <thead>
                    <tr>
                      <th style="width: 10%">Fecha de Creación</th>
                      <th style="width: 10%">Nro Timbrado</th>
                      <th style="width: 10%">Fecha Inicio</th>
                      <th style="width: 10%">Fecha Vencimiento</th>
                      <th style="width: 25%">Proveedor</th>
                      <th style="width: 25%">Denominacion</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
            </div> 
        </div> 
    </div>        
</section>

{{-- ========================================
            EDITAR TIMBRADO
    ========================================  --}}      

    <div class="modal fade" id="modalEditMovimiento" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" style="margin-left: 25%;margin-top: 5%;">
        <div class="modal-content" style="width: 150%;">
          <div class="modal-header">
            <h4 class="modal-title"  style="font-weight: 800;">Modificar Timbrado <i style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
            <button type="button" class="closeModal btn-danger"  data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
                <form id="formTimbradosOp" class="form-horizontal form-group-sm"> 
                        <input type="hidden" class="form-control" id="idMovimiento" name="idMovimiento"  placeholder="">
                        <div class="form-group row">
                                <label for="" class="col-sm-3 control-label">Fecha Inicio</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control fecha" id="fecha_creacion" name="fecha_creacion"  placeholder="">
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="" class="col-sm-3 control-label">Fecha Vencimiento</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control fecha" id="fecha_vencimiento" name="fecha_vencimiento"  placeholder="">
                                </div>
                        </div>

                        <div class="form-group row">
                                <label for="" class="col-sm-3 control-label">Numero Timbrado</label>
                                <div class="col-sm-9">
                                  <input type="number" class="form-control" id="numero_timbrado" name="numero_timbrado"  placeholder="">
                                </div>
                        </div>
               </form>  
          </div> 
          <div class="modal-footer">
             <button type="button" onclick="guardarMovimiento()" id="btnEditarMovimiento" class="btn btn-success btn-lg"><b>Confirmar</b></button>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('scripts')
  @include('layouts/gestion/scripts')
  <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>

  <script>
    $('#listado').dataTable();

    var datepickers = $('#fecha_creacion'); 
          datepickers.datepicker({ 
           format: "dd/mm/yyyy" ,
          // startDate: new Date() 
          }) 

    var datepickers = $('#fecha_vencimiento'); 
          datepickers.datepicker({ 
           format: "dd/mm/yyyy",
          // startDate: new Date() 
          }) 
    $('#inicio').click(function(){
      $('input[name="inicio"]').trigger('click'); 
    })  
    $('#vencimiento').click(function(){
      $('input[name="vencimiento"]').trigger('click');  
    })  

    $('.select2').select2();

       function consultaTimbrado(){
              $.blockUI({
                                centerY: 0,
                                message: "<h2>Procesando...</h2>",
                                css: {
                                    color: '#000'
                                }
                            });
           var dataString = $("#reporteTimbrado").serialize();
            $.ajax({
              type: "GET",
              url: "{{route('getTimbradosCargados')}}",
              dataType: 'json',
              data: dataString,
              success: function(rsp){
                  var oSettings = $("#listado").dataTable().fnSettings();
                  var iTotalRecords = oSettings.fnRecordsTotal();
                  for (i=0;i<=iTotalRecords;i++) {
                    $("#listado").dataTable().fnDeleteRow(0,null,true);
                  }
                  $.each(rsp, function (key, item){
                    //console.log(item);
                        permiso = '{{$permiso}}';
                         var iconoEditar ='';
                        if(permiso == true){
                          iconoEditar ='<a onclick="modificarElemento('+item.id+')" class="btn btn-success" style="padding-left: 6px;padding-right: 6px;margin-right: 2px;" role="button"><i class="fa fa-fw fa-edit"></i></a>';
                        }

                        var creacion = item.fecha_hora_creacion.split(' ');
                        var inicio = creacion[0].split('-');
                        fechaCreacion = inicio[2]+'/'+inicio[1]+'/'+inicio[0]+" "+creacion[1];
                       
                        var inicio = item.fecha_inicio.split('-');
                        fechaInicio = inicio[2]+'/'+inicio[1]+'/'+inicio[0];

                        var vencimiento = item.fecha_vencimiento.split('-');
                        fechaVencimiento = vencimiento[2]+'/'+vencimiento[1]+'/'+vencimiento[0];

                    var dataTableRow = [
                                fechaCreacion,
                                item.nro_timbrado,
                                fechaInicio,
                                fechaVencimiento,
                                item.proveedor.nombre,
                                item.denominacion,
                                iconoEditar
                              ];
                      var newrow = $("#listado").dataTable().fnAddData(dataTableRow);

                      // set class attribute for the newly added row 
                      var nTr = $("#listado").dataTable().fnSettings().aoData[newrow[0]].nTr;

                  })
                   $.unblockUI();
                }  
             })                 
        }  


       function modificarElemento(id){ 
            $('#modalEditMovimiento').modal('show');
            dataString = { id_timbrado: id};
            $.ajax({
                    type: "GET",
                    url: "{{route('getModificarTimbrados')}}",
                    dataType: 'json',
                    data: dataString,
                            success: function(rsp){
                            console.log(rsp);
                             $('#idMovimiento').val(rsp[0].id); 
                             $('#numero_timbrado').val(rsp[0].nro_timbrado); 
                             var creacion = rsp[0].fecha_inicio.split('-');
                             var fechaCreacion = creacion[2]+"/"+creacion[1]+"/"+creacion[0];
                             var vencimiento = rsp[0].fecha_vencimiento.split('-');
                             var fechaVencimiento = vencimiento[2]+"/"+vencimiento[1]+"/"+vencimiento[0];
                             $('#fecha_creacion').val(fechaCreacion); 
                             $('#fecha_vencimiento').val(fechaVencimiento);
                        }
                })  

        } 


       function guardarMovimiento(id){ 
          if($('#fecha_creacion').val()!= ""){
             if($('#fecha_vencimiento').val()!= ""){
               if($('#numero_timbrado').val()!= ""){
              
                    let dataString = $('#formTimbradosOp').serializeJSON();
                    
                    $.ajax({
                        type: "GET",
                        url: "{{route('doUpdateTimbrado')}}",
                        dataType: 'json',
                        data: dataString,
                         error: function(jqXHR,textStatus,errorThrown){

                                        },
                        success: function(rsp){
                            console.log(rsp);
                            if(rsp.status = 'OK'){
                                $.toast({
                                    heading: 'Success',
                                    text: rsp.mensaje,
                                    position: 'top-right',
                                    showHideTransition: 'slide',
                                    icon: 'success'
                                });
                                $('#modalEditMovimiento').modal("hide");
                                consultaTimbrado();
                             }else{
                                $.toast({
                                  heading: 'Error',
                                  position: 'top-right',
                                  text: rsp.mensaje,
                                  showHideTransition: 'slide',
                                  icon: 'error'
                                });
                              }
                             } 
                        })
                }else{
                    $.toast({
                                  heading: 'Error',
                                  position: 'top-right',
                                  text: 'Ingrese el numero de timbrado',
                                  showHideTransition: 'slide',
                                  icon: 'error'
                                });
                }    
              }else{
                  $.toast({
                                  heading: 'Error',
                                  position: 'top-right',
                                  text: 'Ingrese la fecha de vencimiento',
                                  showHideTransition: 'slide',
                                  icon: 'error'
                                });
                  
                }   
           }else{
              $.toast({
                                  heading: 'Error',
                                  position: 'top-right',
                                  text: 'Ingrese la fecha de inicio',
                                  showHideTransition: 'slide',
                                  icon: 'error'
                                });
                  
                }      
       }

  </script>
@endsection