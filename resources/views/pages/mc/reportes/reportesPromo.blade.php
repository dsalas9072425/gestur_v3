@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	<div>

            <div id="main" class="col-sm-12 col-md-11 info-reserva sectiontop">
            	<form id="frmAgencias" class="contact-form" action="" method="post">
					<div class="booking-information travelo-box">
						<div>
							<br>
							<br>
							<h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Reporte de Canje de Promo</h1>
							<br>
							<br>
							<div style= "background-color: #ffffff;padding-top: 20px;padding-left: 10px;padding-right: 10px;">
	            				<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
		                            <thead>
		                            	<tr>
		                                	<th>#</th>
		                                	<th>Fecha Pedido</th>
		                                  	<th>Usuario</th>
		                                  	<th>Agencia</th>
		                                  	<th>Opcion</th>
					                      	<th>Monto</th>
					                      	<th>Lista Facturas</th>
					                      	<th>Tipo</th>
		                                   	<th>Voucher</th>
		                                   	<th>Factura</th>
		                                   	<th>Pagado</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                            	@foreach($promos as $promo)
			                            	<tr>
			                            		<td><b>{{$promo->id}}</b></td>
			                            	    <td><b>{{$promo->fecha_pedido}}</b></td>
			                                  	<td>{{$promo->nombre_apellido}}</td>
						                      	<td>{{$promo->razon_social}}</td>
			                                   	<td><img class="img-responsive" src="./images/{{$promo->opcion_cambio}}.png" style="width: 50px;"></td>
			                                   	<td><b>{{$promo->monto}}</b></td>
			                                   	<td><a data-toggle="modal" id="" href="#requestModal" onclick="testFunction({{$promo->id}});" class="btn-mas-request"><i class="fa fa-file-text-o fa-lg"></i>Facturas</a></td>
			                                   	<td>
			                                   		@if($promo->tipo == 1)
			                                   			<b>{{'DTPlus'}}</b>
			                                   		@else
			                                   			<b>{{'Promoción'}}</b>
			                                   		@endif
			                                   	</td>
												<td>
													<a style="height: 45px; width: 35px;padding-top: 10px; background-color: #f80a84; color: white;" class="btn" href="{{route('voucherpdfUser', ['id' =>$promo->id])}}"><i class="fa fa-file-pdf-o"></i></a>
												</td>
			                                   	<td><a data-toggle="modal" id="" href="#requestAttachment" onclick="attachFunction({{$promo->id}});" class="btn-mas-attach"><i class="fa fa-file-text-o fa-lg"></i></a></td>
			                                   	<td>
													@if($promo->estado_pedido == 'C')
														<input id="check_{{$promo->id}}" onclick='cancelar({{$promo->id}})' checked="checked" type="checkbox">
													@else
														<input id="check_{{$promo->id}}" onclick='cancelar({{$promo->id}})' type="checkbox">
													@endif	
												</td>
			                                </tr>   	
		                            	@endforeach       	
		                            </tbody>
	                        	</table>
	                       	</div>	 
                        </div>
                    </div>    			
            </div>
    </div>
   	<!-- Modal Request-->
	<div id="requestModal" class="modal fade" role="dialog">
	<!--<form id="form-asignar-expediente" method="post" action="">-->
	  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h2 class="modal-title titlepage" style="font-size: x-large;">Listado de Facturas</h2>
			</div>
		  <div class="modal-body">
		  		<div id="contenido"></div>
		  </div>
		  <div class="modal-footer">
			<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 80px; background-color: #e2076a;" data-dismiss="modal">Cerrar</button>
		  </div>
		</div>
	  </div>
	 <!--</form>-->
	</div>

   	<!-- Modal Request-->
	<div id="requestAttachment" class="modal fade" role="dialog">
	<!--<form id="form-asignar-expediente" method="post" action="">-->
	  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content" style="width: 800px;height: 700px;">
		  <div class="modal-body" id="attach">
		  </div>
		  <div class="modal-footer">
			<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 80px; background-color: #e2076a;" data-dismiss="modal">Cerrar</button>
		  </div>
		</div>
	  </div>
	 <!--</form>-->
	</div>



@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
		});

				function testFunction(idNotificacion){
					console.log("{{route('mc.getDetalleLista')}}?id="+idNotificacion);
					$.ajax({
							type: "GET",
							url: "{{route('mc.getDetalleLista')}}",
							data: "id="+idNotificacion,
							dataType: 'json',
							success: function(rsp){
											var datos  = rsp.split(',');
											var contenido = "";
											$.each(datos, function(key,item){
												console.log(item);
												contenido += "<b>"+item+"</b><br>";
											})
											$("#contenido").html(contenido);
										}	
							})

				}		

				function attachFunction(idAttach){
					console.log("{{route('mc.getDetalleAttach')}}?id="+idAttach); 
					$.ajax({
							type: "GET",
							url: "{{route('mc.getDetalleAttach')}}",
							data: "id="+idAttach,
							dataType: 'json',
							success: function(rsp){
										console.log(rsp);
										$("#attach").html('<img class="img-responsive" src="'+rsp.file+'" style="width: 50%;height: 90%;">');
										}	
							})

				}		


				function cancelar(idDtplus){
					    console.log($( "#check_"+idDtplus ).val());
				        BootstrapDialog.confirm({
				            title: 'DTP',
				            message: 'Seguro desea modificar el estado de este Voucher?',
				            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
				            closable: true, // <-- Default value is false
				            draggable: true, // <-- Default value is false
				            btnCancelLabel: 'No', // <-- Default value is 'Cancel',
				            btnOKLabel: 'Si', // <-- Default value is 'OK',
				            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
				            callback: function(result) {
				                // result will be true if button was click, while it will be false if users close the dialog directly.
				                if(result) 
								{
				                    $.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });
									
									//alert("{{route('getCancelarReservas')}}?idProveedor="+idProveedor);
								$.ajax({
								type: "GET",
								url: "{{route('mc.getPagarPromo')}}?idDtplus="+idDtplus,
										data: idDtplus,
										success: function(rsp){
												console.log(rsp);
												$.unblockUI();
												if(rsp == 'false'){
													var mensaje = 'Se ha desactivado el pago de Voucher';
													$( "#check_"+idDtplus ).prop( "checked", false );
												}else{
													var mensaje = 'Se ha pagado el Proveedor';
													$( "#check_"+idDtplus ).prop( "checked", true );
												}
				                                $("#buscarReservas").trigger('click');
					                                BootstrapDialog.show({
					                                    title: 'DTPMUNDO',
					                                    message: mensaje
					                                });
					                            }

										})
				                }else {
				                	if($("#check_"+idDtplus ).val() == 'on'){
				                		$("#check_"+idDtplus ).prop( "checked", false );
				                	}else{
										$("#check_"+idDtplus ).prop( "checked", true );
				                	}
				                    return false;
				                }
				            },
				        });
				}


	</script>
@endsection