@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
    @parent
    <style>
        .pagination .page-item {
            border-radius: 50%;
        }
        /* Estilos para la tabla con desplazamiento horizontal */
        .table-responsive {
            overflow-x: auto;
        }
    </style>
@endsection
@section('content')
<style type="text/css">
	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	/*.bgRed:hover{
		color: white;
	}*/

	/*background: #e2076a !important;padding-left: 6px;padding-right: 6px;*/



	/*==================SELECT STAR============================*/

	.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}


.click_estrella {
	cursor:pointer; 
	cursor: hand;
}

/*====================================================
	ESTILO PARA FOCUS
  ====================================================	
*/

input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }



</style>
<section id="base-style">
    <br>

    <div class="card">
        <div class="card-header">
            <div class="card-body">
            <h4 class="card-title">Listado Ventas Rapidas</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class = "row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Desde (Facturacion)</label>			
                    <div class="input-group">
                        <div class="input-group-prepend" style="">
                            <span id="fechcheck" class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input type="date" id="desde" name="desde" class="form-control">
                    </div>
                </div>	 
            </div> 
    
            <div class="col-md-3">
                <div class="form-group">
                    <label>Hasta (Facturacion)</label>			
                    <div class="input-group">
                        <div class="input-group-prepend" style="">
                            <span id="fechcheck" class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input type="date" id="hasta" name="hasta" class="form-control">
                    </div>
                </div>	 
            </div> 

            <div class="col-md-3">
                <div class="form-group">
                    <label>Vendedor</label>
                    <select class="form-control" name="vendedor_id"  id="vendedor" style="width: 100%;" tabindex="12">
                        <option value="">Todos</option>
                    </select>
                </div>
            </div>	
        </div> 
        <div class="row">
            <div class="col-12 col-sm-4 col-md-3">
                <div class="form-group">
                    <label for="formNombres">Nro.Factura</label>
                    <input type="text" class="form-control" tabindex="5" name="nro_factura"
                        id="nro_factura" value="" maxlength="100" />
                </div>
            </div>
        </div> 

        <div class="row">
             <div class="col-md-12">
             {{--   <a href="{{ route('facturapre_excel') }}" class="pull-right text-center btn btn-success btn-lg mr-1">Excel</a> --}}
             <button type="submit" id="botonExcel"  onclick="botonExcel()" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
             <button type="button" onclick="limpiar()" id="btnLimpiar" class="pull-right btn btn-light btn-lg text-white mr-1 mb-1" tabindex="17" ><b>Limpiar</b></button>
                <button type="button" class="pull-right btn btn-info btn-lg mr-1 mb-1" role="button" onclick="buscarFacturas()" tabindex="16"><b>Buscar</b></button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    
                        <div class="row">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3">
                                <form action="{{ route('ventaRapida') }}">
                                    <label>Buscar:</label>
                                    <input type="search" name="buscar" autocomplete="off" class="form-control">                            
                                </form>
                            </div>
                        </div>
                       <div class="table-responsive">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>NRO. FACTURA</th>
                                    <th>VENDEDOR</th>
                                    <th>NRO. VTA. RAPIDA</th>
                                    <th>PROVEEDOR</th>
                                    <th>DESCRIPCION ITEM</th>
                                    <th>CANTIDAD</th>
                                    <th>COSTO</th>
                                    <th>VENTA</th>
                                    <th>RENTA</th>
                                    <th>CLIENTE</th>
                                    <th>FECHA FACTURA</th>
                                    <th>ESTADO FACT.</th>
                                </tr>
                            </thead>
                            <tbody id="facturas-table">
                                @foreach($facturas as $factura)
                                <tr>
                                    <td>{{ $factura->nro_factura }}</td>
                                    <td>{{ $factura->vendedorempresa }}</td>
                                    <td>{{ $factura->id_venta_rapida }}</td>
                                    <td>{{ $factura->proveedor }}</td>
                                    <td>{{ $factura->descripcion }}</td>
                                    <td>{{ $factura->cantidad }}</td>
                                    <td>{{ $factura->precio_costo }}</td>
                                    <td>{{ $factura->precio_venta }}</td>
                                    <td>{{ $factura->renta }}</td>
                                    <td>{{ $factura->nombre }}</td>
                                    <td>{{ $factura->fecha_hora_facturacion }}</td>
                                    <td>{{ $factura->estado }}</td>
        
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $facturas->links('pagination::bootstrap-4') }}
                        TOTAL DATOS: {{ $facturas->total() }}
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('scripts')
	@include('layouts/gestion/scripts')
	<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
    <script>

function buscarConEnter(event) {
    if (event.key === 'Enter') {
        event.preventDefault(); // Evitar el envío del formulario
        buscarFacturas(); // Llamar a la función buscarFacturas()
    }
}


    
        function buscarFacturas() {
            var desde = document.getElementById('desde').value;
            var hasta = document.getElementById('hasta').value;
            var vendedor = document.getElementById('vendedor').value;
            var nroFactura  = document.getElementById('nro_factura').value;

            var url = "{{ route('ventaRapida') }}";
            url += "?desde=" + desde + "&hasta=" + hasta;
            if (vendedor) {
                url += "&vendedor_id=" + vendedor; // Agregar el ID del vendedor al URL si está seleccionado
            }
            
            if (nroFactura) {
                url += "&nro_factura=" + nroFactura; // Agregar el número de factura al URL si está especificado
            }

    window.location.href = url;
}
        window.onload = function() {
            var params = new URLSearchParams(window.location.search);
            var desde = params.get('desde');
            var hasta = params.get('hasta');
            var vendedor = params.get('vendedor_id');
            var nroFactura = params.get('nro_factura');
            var buscar = params.get('buscar');
            document.getElementById('desde').value = desde || '';
            document.getElementById('hasta').value = hasta || '';
            document.getElementById('vendedor').value = vendedor || '';
            document.getElementById('nro_factura').value = nroFactura || ''; 
            document.querySelector('input[name="buscar"]').value = buscar || '';
        };
    

       /* $("#botonExcel").on("click", function(e) {
    e.preventDefault();

    window.location.href = "{{ route('generarExcelventaRapida') }}";
});*/
//EXCEL 
function botonExcel() {
            var desde = document.getElementById('desde').value;
            var hasta = document.getElementById('hasta').value;
            var vendedor = document.getElementById('vendedor').value;
            var nroFactura  = document.getElementById('nro_factura').value;
            var url = "{{ route('generarExcelventaRapida') }}";
            url += "?desde=" + desde + "&hasta=" + hasta;
            if (vendedor) {
                url += "&vendedor_id=" + vendedor; // Agregar el ID del vendedor al URL si está seleccionado
            }
            if (nroFactura) {
                url += "&nro_factura=" + nroFactura; // Agregar el número de factura al URL si está especificado
            }

    window.location.href = url;
}
// DEFINE EL IDIOMA ESPAÑOL A TODOS LOS SELECT2
var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};
            $(document).ready(function() {

                $("#vendedor").select2({
                                language: lang_es_select2,
                                ajax: {
		                url: "{{route('get.vendedores.agencia')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });
        });//READY DOCUMENT

        function limpiar(){
	
			$('#vendedor').val('').trigger('change.select2');
		
			$('#desde').val('');
			$('#hasta').val('');
		
            $('#nro_factura').val('');

            $('input[name="buscar"]').val('');

			// consultaAjax();
		}

    </script>
@endsection
@endsection

