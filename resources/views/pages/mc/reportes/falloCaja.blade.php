
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	.ui-datepicker-calendar {
    display: none;
    }
	.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
	}	
</style>
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Reporte Fallo Caja</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	<form id="formFallaCaja"  method="post" autocomplete="off">

					<div class="row mt-1">
						
			            <div class="col-md-4">
					        <div class="form-group">
					            <label>Vendedor </label> <span class="combo_1 combo"> </span> <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg combo_1"></i>
								<select class="form-control select2" name="vendedor_id"  id="vendedor_id" style="width: 100%;">
									<option value="">Seleccione Vendedor</option>
									 @foreach($vendedores as $vendedor)
									<option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
									@endforeach 
									
								</select>
					        </div>
						</div>
						
					    <div class="col-12 col-sm-4 col-md-4">
		 					<div class="form-group">
					            <label>Periodo Desde/Hasta.</label>			
					            	<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text"  class = "Requerido form-control input-sm" id="periodo" name="periodo" tabindex="10"/>
								</div>
							</div>	
						</div> 

					</div>
					<div class="row">
						<div class="col-md-12">
							<div  class="readOnly fuenteDtplus" id="dtpPlusInfo" style="height:30px; width: 100%; border:1px solid #d2d6de; padding: 6px;"><span class="textDtplus">TOTAL $ </span><span id="monto"></span></div>
						</div>

				 		<div class="col-12 mb-1 mt-1">
							<a  onclick ="consultaAjax()" class="btn btn-info btn-lg text-white font-weight-bold pull-right mr-2" role="button"><b>Buscar</b></a>
						</div>
					  </div>
					  
			  	</form>	
			  	<br>
			  	<div class="table-responsive table-bordered">
	              	<table id="listado" class="table">
		                <thead>
						  <tr>
						  	<th>Cliente</th>
						  	<th>Factura</th>
							<th>Fecha</th>
							<th>Importe</th>
							<th>Moneda</th>
							<th>Importe USD</th>
							<th></th>


			              </tr>
		                </thead>
		                <tbody style="text-align: center; font-weight: 800;">
		 					
					          
				         
				        </tbody>
			         	<tfoot>
				            <tr>
				                <th colspan="5" style="text-align:right">Total Pag:</th>
				                <th></th>
				            </tr>
				        </tfoot>
	              	</table>
	            </div>  
            </div>
        </div>    	
    </div>    
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
<script type="text/javascript">


//CALENDARIO

$(function() {
 		$('.select2').select2();

 		var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('input[name="periodo"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });

		consultaAjax();
	})



	/*
	Para formatear numeros a moneda USD PY
	 */
	const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD',
					  minimumFractionDigits: 2
					});

		
		


			function consultaAjax(){
				//MM/YYYY
				var str = $('#periodo').val();
				if(str != ''){ 

				var d1 = moment(str+' 23:59:59', "MM/YYYY HH:mm:ss",true);
				var d2 = moment(str+' 23:59:59', "MM/YYYY HH:mm:ss",true).endOf('month');

				if(d1 != 'Invalid date'){
					$('#periodo_facturacion_low').val(d1.format('MM-DD-YYYY'));
					$('#periodo_facturacion_higth').val(d2.format('MM-DD-YYYY'));
				}else {
					$('#periodo_facturacion_higth').val('');
					$('#periodo_facturacion_low').val('');
				}
					
				// console.log(d1.format('DD/MM/YYYY'));
				// console.log(d2.format('DD/MM/YYYY'));
				}

					



				 $('#monto').html('0,00');

		$.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });

		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){

					
			 table =   $('#listado').DataTable({
					        "destroy": true,
					        "ajax": {
					        		data: { "formSearch": $('#formFallaCaja').serializeArray() },
						            url: "{{route('ajaxFalloCaja')}}",
						            type: "GET",
						    error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                            $.unblockUI();

                        }
                        		// formatter.format(parseFloat(x.total_venta_proforma));
						  },   "columnDefs": [
						            {	"searchable": false,
						                "targets": [ 6 ],
						                "visible": false
						            }
						        ],
						 "columns":[
						 			{data:'usuario_n'},
						 			{data: function(x){
										console.log(x);
						        		return  `<a href="{{route('verFactura',['id'=>''])}}/${x.id_factura}"  class="bgRed" style="color:inherit;">
                    						<i class="fa fa-fw fa-search"></i>${x.nro_factura}</a><div style="display:none;">${x.id_factura}</div>`;		

						        	}},
						 			{data:'fecha_hora_facturacion_format'},
						 			{data: function(x){ return formatter.format(parseFloat(x.monto));}},
						 			{data: 'currency_code'},
						 			{data: function(x){ return formatter.format(parseFloat(x.monto_cotizado));}},
						 			{data: 'estado'}
						 			],


							"aaSorting":[[6,"desc"]]
						     ,"createdRow": function ( row, data, index, aData ) {
						     	// console.log(data.estado);
						     	if(data.estado == '+'){
						     		// console.log(data.monto_cotizado);

						     	} else if(data.estado == '-' ){
						     		$('td', row).css('color', '#C84747');
						     	}

						     }

						     ,"footerCallback": function ( row, data, start, end, display ) {
					            var api = this.api(), data;
					 
					            // Remove the formatting to get integer data for summation
					            var intVal = function ( i ) {
					                return typeof i === 'string' ?
					                   	i.replace(/[\$,]/g, '.')*1 :
					                    typeof i === 'number' ?
					                        i : 0;
					            };	


								// Total over all pages
					            total = api
					                .column( 5 )
					                .data()
					                .reduce( function (a, b) {
					                    return intVal(a) + intVal(b);
					                }, 0 );
					 
					            // Total over this page
					            pageTotal = api
					                .column( 5, { page: 'current'} )
					                .data()
					                .reduce( function (a, b) {
					                    return intVal(a) + intVal(b);
					                }, 0 );

					                // Update footer
						            $( api.column( 5 ).footer() ).html(
						                '$'+formatter.format(parseFloat(pageTotal)) +' ( $'+ formatter.format(parseFloat(total)) +' )'
						            );
						            $('#monto').html(formatter.format(parseFloat(total)));
						        }

						                    
					    }).on('xhr.dt', function ( e, settings, json, xhr ){
					    	  $.unblockUI();
								});
			 
					    	}, 300);


		

}//function











	

</script>

@endsection


