@extends('masters')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
@endsection
@section('content')
	<div style="margin-left: 1%; margin-right: 2%;">
<div class="row" style="height: 50%;">
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div style="height: 50%; border: 1px solid #999999; border-radius: 5px; margin-left: 15px; padding-left: 5px;padding-right: 5px; background-color: white;max-height: 450px;min-height: 400px;">
				<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">TOP DE RESERVAS POR VENDEDOR</h1>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr style="color:#e2076a">
								<th style="height: 50px;">Vendedor</th>
								<th>Agencia</th>
								<th>Cantidad</th>
								<th>Monto</th>
							</tr>
						</thead>
						<tbody id="top_reserva">
							
						</tbody>	
					</table>
				</div>		
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div style="height: 50%; border: 1px solid #999999; border-radius: 5px;margin-right: 15px; padding-left: 5px;padding-right: 5px; background-color: white;max-height: 450px;min-height: 400px;">
				<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">TOP DE AGENCIAS QUE MAS COTIZAN</h1>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr style="color:#e2076a">
								<th style="width: 55%;">Agencia</th>
								<th>Total Usuarios</th>
								<th>Usuario Usaron</th>
								<th>Cant. Busquedas</th>
							</tr>
						</thead>
						<tbody id="top_agencias">
						</tbody>	
					</table>
				</div>		
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div style="height: 49%; border: 1px solid #999999; border-radius: 5px; margin-left: 15px;background-color: white; max-height: 500px;min-height: 450px;">
				<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235 ">PROMOCION: TODOS GANAN CON DTP!!!</h1>
				<div class="row">
					<div class="col-md-4">
						<div class="row" style="margin-top: 15%;">
						 	<div class="col-md-9">
						 		<h5 class="subtitle hide-medium" style="text-align: left; font-size: x-large; margin-top: 10%; margin-left: 20px;font-weight: bold;">Total:</h5>
						 	</div>
						 	<div class="col-md-3">
						 		<h5 class="subtitle hide-medium" id="usuario_total" style="text-align: left; font-size: x-large; margin-top: 10%;font-weight: bold;"></h5>
						 	</div>
						 	<div class="col-md-9">
						 		<h5 class="subtitle hide-medium" style="text-align: left; font-size: x-large; margin-top: 5%;  margin-left: 20px; color:#FF0080;font-weight: bold;">Promoción:</h5>
						 	</div>
						 	<div class="col-md-3"> 
								<h5 class="subtitle hide-medium" id="usuario_promocion" style="text-align: left; font-size: x-large; margin-top: 5%; color:#FF0080; font-weight: bold;"></h5>	
							</div>
						 	<div class="col-md-9">
						 		<h5 class="subtitle hide-medium" style="text-align: left; font-size: x-large; margin-top: 5%;  margin-left: 20px; color:#00007c; font-weight: bold;">Restantes:</h5>
						 	</div>
						 	<div class="col-md-3">
								<h5 class="subtitle hide-medium" id="usuario_no_promocion" style="text-align: left; font-size: x-large; margin-top: 5%; color:#00007c; font-weight: bold;"></h5>	
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div id="actual" style="min-width: 210px; height: 300px; max-width: 500px; margin: 0 auto"></div>
					</div>	
				</div> 
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div style="height: 49%; border: 1px solid #999999; border-radius: 5px; margin-right: 15px;background-color: white;max-height: 500px;min-height: 450px;">
				<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">ACCESOS AL SISTEMA</h1>
				<div id="cuadro" style="min-width: 70%;height: 350px;max-width: 70%;margin: 0 auto;"></div>
			</div>
		</div>
	</div>
	</div>
@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
		<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script>
			cargaDatos();
			setInterval(cargaDatos, 10000);
			function cargaDatos(){	
				$.ajax({
						type: "GET",
						url: "{{route('getReservasReporte')}}",
						dataType: 'json',
						success: function(rsp){
											$("#top_agencias").html("");
											$.each(rsp, function(keys,values){
													var fila = "";
													fila += "<tr><td><b>"+values.razon_social+"</b></td><td>"+values.total_agencia+"</td><td>"+values.usuario_usaron+"</td><td><b>"+values.total_busquedas+"</b></td></tr>"
													$("#top_agencias").append(fila);
											});	
										}
						});

				$.ajax({
						type: "GET",
						url: "{{route('getReservasTotal')}}",
						dataType: 'json',
						success: function(rsp){
											$("#top_reserva").html("");
											$.each(rsp, function(keys,values){
													var fila = "";
													fila += "<tr><td><b>"+values.nombre_apellido+"</b></td><td>"+values.razon_social+"</td><td>"+values.totalusuario+"</td><td><b>"+formatNumber.new(values.totalcobrado, "$") +"</b></td></tr>"
													$("#top_reserva").append(fila);
											});	
										}
						});


				$.ajax({
						type: "GET",
						url: "{{route('getUsuariosPromo')}}",
						dataType: 'json',
						success: function(rsp){
							$('#usuario_total').html(rsp.total);
							$('#usuario_promocion').html(rsp.promo); 
							$('#usuario_no_promocion').html(rsp.restante);
							$('#actual').html("");
							$('#actual').highcharts({
							                    chart: {
							                        type: "pie"
							                    },
							                    title: {
							                        text: ""
							                    },
							                    colors: ['#FF0080', '#00007c'],
							                    xAxis: {
							                        type: 'category',
							                        allowDecimals: false,
							                        title: {
							                            text: ""
							                        }
							                    },
							                    yAxis: {
							                        title: {
							                            text: "Scores"
							                        }
							                    },
							                    series: [{
											        name: 'Detalle',
											        colorByPoint: true,
											        data: [
											            { name: 'Registrados', y: rsp.promo },
											            { name: 'Pendiente', y:  rsp.restante },
											        ]
											    }]
							        		});
							}
						});

				$.ajax({
						type: "GET",
						url: "{{route('getAccesosSemanales')}}",
						dataType: 'json',
						success: function(rsp){
								var categoria_json = new Array(); 
								var data_json = new Array();
								$.each(rsp, function(keys,values){
									var fecha = values.fecha.split('-');
									data_json.push([values.count]);
									categoria_json.push([fecha[2]+"/"+[fecha[0]]+"/"+fecha[1]]);
								})	

								$('#cuadro').highcharts({
									chart: {
											type: 'line'
											},
									title: {
											text: ''
											},
									colors: ['#00007c'],
									subtitle: {
											text: ''
											},
									xAxis: {
											categories: categoria_json
											},
									yAxis: {
											title: {
													text: ''
													},
											labels: {
													formatter: function () {
													                return this.value ;
													            }
													}
											},
									tooltip: {
											crosshairs: true,
											shared: true
											},
									plotOptions: {
												line: {
													   dataLabels: {
													                enabled: true
													            },
													            enableMouseTracking: false
													    }
												},
												series: [
														{
														name: 'Ingresos',
														marker: {
														            symbol: 'circle'
														        },
														data: data_json
														}
												]
											});
										}
									})	

			}	


			var formatNumber = {
				 separador: ".", // separador para los miles
				 sepDecimal: ',', // separador para los decimales
				 formatear:function (num){
				 num +='';
				 var splitStr = num.split('.');
				 var splitLeft = splitStr[0];
				 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
				 var regx = /(\d+)(\d{3})/;
				 while (regx.test(splitLeft)) {
				 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
				 }
				 return this.simbol + splitLeft +splitRight;
				 },
				 new:function(num, simbol){
				 this.simbol = simbol ||'';
				 return this.formatear(num);
				 }
				}
	</script>			
@endsection