@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent


@endsection
@section('content')


<section id="base-style">

	<div class="card" style="border-radius: 14px;">
		<div class="card-header" style="border-radius: 14px;">
			<h4 class="card-title">Reporte de Pagos Anticipos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


				<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="tableCttaCtte" data-toggle="tab" role="tab" aria-controls="tabIcon21" href="#cabecera">Reporte de Pagos Anticipos</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" id="tableCttaCtteDetalle" role="tab" aria-controls="tabIcon21" data-toggle="tab" href="#detalle">Reporte del Detalle de Pagos Anticipos</a>
					</li>

				</ul>
				<div class="tab-content">
					<section id="cabecera" class="tab-pane active box-home" role="tabpanel">
						@include('flash::message')

							<div class="table-responsive">
								<table id="listado" class="table" style="width: 100%">
									<thead style="text-align: center">
										<tr>
											<th>Fecha</th>
											<th>Moneda</th>
											<th>Monto</th>
											<th></th>
										</tr>
									</thead>

									<tbody style="text-align: center">
									</tbody>
								</table>
							</div>
					</section>
					<section id="detalle" class="tab-pane">
						@include('flash::message')

						<form id="verCtteDetalle" class="mt-1">
							<div class="row">
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Moneda</label>
										<input type="text" class="form-control" name="monedaDesc" id="monedaDesc"
											value="" readonly="" />
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Fecha</label>
										<input type="text" class="form-control" name="fechaDesc" id="fechaDesc" value=""
											readonly="" />
									</div>
								</div>

								<input type="hidden" class="form-control" name="fecha" id="fecha" value=""
									hidden="hidden" />
								<input type="hidden" class="form-control" name="moneda" id="moneda" value=""
									hidden="hidden" />

							</div>
						</form>

						<div class="table-responsive mt-1">
						<table id="listadoDetalle" class="table table-hover table-condensed nowrap" style="width: 100%;">
								<thead style="text-align: center">
									<tr>
										<th>Fecha</th>
										<th>Tipo</th>
										<th>Cliente</th>
										<th>Numero</th>
										<th>Forma Pago</th>
										<th>Moneda</th>
										<th>Documento</th>
										<th>Recibo</th>
										<th>Monto</th>
										<th></th>
									</tr>
								</thead>
								<tbody style="text-align: center">

								</tbody>
								<tfoot>
									<tr>
										<th style="text-align: center;">TOTAL</th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
									</tr>
								</tfoot>
							</table>
						</div>




					</section>	


				</div>
			</div>
		</div>
	</div>

</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	{{-- 
		--VARIOS DE ESTOS SCRIPS YA ESTA EN EL SCRIPTS.BLADE PRINCIPAL
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/dataTables.buttons.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jszip.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/buttons.html5.min.js')}}"></script>
	--}}
	<script>
		var tableCttaCtte;
		var tableCttaCtteDetalle;

		$(document).ready(function() 
		{
			$('.select2').select2();
			var periodo = '';
			// console.log(periodo);

    		tableCttaCtte = consultarCttaCtte ();
			//tableCttaCtteDetalle = consultarDetalleCttaCtte();
		});


		var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('input[name="periodo"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		$('input[name="periodo"]').val('');

		const formatter = new Intl.NumberFormat('de-DE', 
		{
			currency: 'USD',
			minimumFractionDigits: 2
		});


		$('.checkCol').on('click',function()
			{
				var form = $('#columLiqExcel').serializeArray();
				valor = [];
				// valor.push(0,1,2,8);
				valor.push(0,1,2,3,4,6,7,11);
				$.each(form, function(item,value){
					valor.push(parseInt(value.value));
				});

				buttonsLiq(valor,tableLiquidacionesDetalle);
			});

		function cargarReporteLiquidacionDetalle(id_vendedor, periodo, renta_total, comision_total)
		{
			$('#vendedorDetalle').val(id_vendedor).trigger('change.select2');
			$('#periodoDetalle').val(periodo).trigger('change.select2');
			$('#renta_total').val(renta_total);
			$('#comision_total').val(comision_total);
			$('#tableLiquidacionesDetalle').tab('show');

			consultarCttaCtte();

		}

		function limpiarFiltrosDetalle()
		{
			$('#periodoDetalle').val('').trigger('change.select2');
			$('#vendedorDetalle').val('').trigger('change.select2');
			consultarCttaCtte();
		}

		function limpiarFiltros()
		{
			// $('#periodo').val('').trigger('change.select2');
			$('#vendedor').val('1').trigger('change.select2');
			// consultarLiquidaciones();
		}

		function formatearFecha(texto)
 		{
	        if(texto != '' && texto != null)
	        {
	          	return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	        } 
	        else 
	        {
	         	return '';    
         	}
        }

		function consultarCttaCtte()
		{ 
			var tableLiquidaciones = $("#listado").DataTable
			({
				"destroy": true,
				"ajax": 
				{
					"url": "{{route('getDatosPagos')}}",
					"type": "GET",
					//"data": {"formSearch": $('#getDatosPagosDetalle').serializeArray() },
					error: function(jqXHR,textStatus,errorThrown)
					{
		                $.toast
		                ({
		                    heading: 'Error',
		                    text: 'Ocurrió un error en la comunicación con el servidor.',
		                    position: 'top-right',
		                    showHideTransition: 'fade',
		                    icon: 'error'
		                });

		                }			    
					},
				"columns": 
				[
					{ "data": function(x)
						{	
							var fecha_salida = x.fecha;
							if(jQuery.isEmptyObject(fecha_salida) == false){
								var fecha = fecha_salida.split('-');
								var fecha_final = fecha[2]+'/'+fecha[1]+'/'+fecha[0];
							}else{
								var fecha_final = "";	
							}
							return fecha_final;
						}
					},
					{ "data": "currency_code" },
					{ "data": "sum" },
					{ "data": function(x)
						{
							var btn = `<a onclick="cargarReporteCuentasDetalle('`+x.fecha+`',`+x.moneda_id+`,'`+x.currency_code+`')" class="btn btn-info text-white" style="important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-search"></i></a>`;

							return btn;
						} 
					}

					]
						
				});	

				//generarExcel([0,1,2,3,4,5,6], tableLiquidaciones);
				return tableLiquidaciones;
			};

			function cargarReporteCuentasDetalle(fecha, moneda, currency_code)
			{ 
				$("#fecha").val(fecha);
				$("#moneda").val(moneda);
				$("#monedaDesc").val(currency_code);
				var fechaD = fecha.split('-');
				var fecha_final_desc = fechaD[2]+'/'+fechaD[1]+'/'+fechaD[0];
				$("#fechaDesc").val(fecha_final_desc);

				var tableLiquidacionesDetalle = $("#listadoDetalle").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('getDatosPagosDetalle')}}",
						"type": "GET",
						"data": {"formSearch": $('#verCtteDetalle').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                }			    
					},
				
					"columns": 
					[
						{ "data": function(x)
							{	
								var fecha_salida = x.fecha;
								if(jQuery.isEmptyObject(fecha_salida) == false){
									var fecha = fecha_salida.split('-');
									var fecha_final = fecha[2]+'/'+fecha[1]+'/'+fecha[0];
								}else{
									var fecha_final = "";	
								}

								return fecha_final;
							}
					    },
						{ "data": "tipo" },
						{ "data": "cliente" },
						{ "data": function(x)
							{	
								if(x.tipo == 'Grupo'){
									var btnx = `<a href="verDetalle/`+x.id_grupo+`" style="color:#111; font-weight: bold;" class="bgRed">
                    					<i class="fa fa-fw fa-search"></i>`+x.descgrupo+`</a><div style="display:none;">`+x.id_grupo+`</div>`;
								}else{
									var btnx = `<a href="detallesProforma/`+x.id_proforma+`" style="color:#111; font-weight: bold;" class="bgRed">
                    					<i class="fa fa-fw fa-search"></i>`+x.id_proforma+`</a><div style="display:none;">`+x.id_proforma+`</div>`;

								}

								return btnx;
							}
					    },						
						{ "data": "denominacion" },
						{ "data": "currency_code" },
						{ "data": "nro_recibo" },
						{ "data": "nro_comprobante" },
						{ "data": "monto" },
						{ "data": function(x)
							{
								var btn = `<a onclick="eliminarFilaPagos(`+x.id+`)" title="Eliminar" class="btn btn-danger text-white" style="margin-left:5px;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-times"></i></a>`;

								return btn;
							} 
						}



						
					],

					"fnInitComplete": function(oSettings, json) 
						{
	                       $('#comision_total').val(json.data3);
	                       var renta_total = formatter.format(parseFloat(json.data2));
	                       $('#renta_total').val(renta_total);
	                    },
	                 "footerCallback": function ( row, data, start, end, display ) {
				             api = this.api();

  							total = api
				                .column( 8 )
				                .data()
				                .reduce( function (a, b) {
				                
				                    return parseFloat(a) + parseFloat(b);
				                }, 0 );
				 
				            // Total over this page
				            pageTotal = api
				                .column( 8 , { page: 'current'} )
				                .data()
				                .reduce( function (a, b) {
				                    return parseFloat(a) + parseFloat(b);
				                }, 0 );
				 
				            // Update footer
				            $( api.column( 8 ).footer() ).html(
				                new Intl.NumberFormat("de-DE").format(total)
				           	 );	

				        }     

			});
			$('#tableCttaCtteDetalle').tab('show');
			/*generarExcelDetalle([0,1,2,3,4,5], tableLiquidacionesDetalle);*/
			return tableLiquidacionesDetalle;

	}
		function eliminarFilaPagos(idFila){
		 	dataString = { id_fila :idFila};
			$.ajax({
					type: "GET",
					url: "{{route('getEliminarAdelanto')}}",
					dataType: 'json',
					async:false,
					data: dataString,
					error: function(){
								$.toast({
									heading: 'Error',
									ext: 'Ingrese algun dato para proceder al guardado',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
										});
					},
					success: function(rsp){
										if(rsp.status == 'OK'){
											$.toast({
												heading: 'Exito',
												text: rsp.mensaje,
												position: 'top-right',
												showHideTransition: 'slide',
											    icon: 'success'
											});  
											$('table#listadoPagoAdelanto tr#rows'+idFila).remove();
											$("#row"+idFila).remove();
											total = parseFloat($('#monto_total_0').val());
											console.log(total);
											console.log(rsp.monto);
											totalFinal = parseFloat(total) - parseFloat(rsp.monto);
											$('#monto_total_0').val(totalFinal);
										}else{
											$.toast({
												heading: 'Error',
												text: rsp.mensaje,
												position: 'top-right',
												showHideTransition: 'fade',
												icon: 'error'
													});
										}	
								}	
				});				


		}



	</script>
@endsection
