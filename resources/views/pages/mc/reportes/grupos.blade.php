@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#botonExcel {
		display: inline;
	}
	#botonExcel .dt-buttons {
		display: inline;
	}

	#botonExcelCabecera {
		display: inline;
	}
	#botonExcelCabecera .dt-buttons {
		display: inline;
	}

	


/*sdsdds*/
.checkbox label:after {
  content: '';
  display: table;
  clear: both;
}

.checkbox .cr {
  position: relative;
  display: inline-block;
  border: 1px solid #a9a9a9;
  border-radius: .25em;
  width: 1.3em;
  height: 1.3em;
  float: left;
  margin-right: .5em;
}

.checkbox .cr .cr-icon {
  position: absolute;
  font-size: .8em;
  line-height: 0;
  top: 50%;
  left: 15%;
}

.checkbox label input[type="checkbox"] {
  display: none;
}

.checkbox label input[type="checkbox"]+.cr>.cr-icon {
  opacity: 0;
}

.checkbox label input[type="checkbox"]:checked+.cr>.cr-icon {
  opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled+.cr {
  opacity: .5;
}

	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}

	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

	.rojo{
		color: red;
		font-weight: 700;
	}

	.verde{
		color: green;
		font-weight: 700;
	}

   .number-format {
       text-align: center;
   } 

</style>

<section id="base-style">
   <div class="card-content">
   		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Reporte de Liquidaciones</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">
			        <div class="card-body">
			            <ul class="nav nav-tabs nav-underline" role="tablist">
			                <li class="nav-item">
			                    <a class="nav-link active" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#cabecera" role="tab" aria-selected="true"><i class="fa fa-play"></i> Reporte de Pagos Anticipos</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#detalle" role="tab" aria-selected="false"><i class="fa fa-flag"></i> Reporte del Detalle de Pagos Anticipos</a>
			                </li>
			            </ul>
			            <div class="tab-content px-1 pt-1">
			                <div class="tab-pane active" id="cabecera" role="tabpanel" aria-labelledby="baseIcon-tab21">
			                	<div class="table-responsive">
					              <table id="listado" class="table" style="width: 100%">
					                <thead style="text-align: center">
				          				<tr>
				          					<th>Cliente</th>
				          					<th>Descripción</th>
				          					<th>Total Anticipo</th>
			                                <th>Saldo</th>
			                                <th></th>
				          				</tr>
					                </thead>
					                
					                <tbody style="text-align: center">
										
					                </tbody>
					              </table>
					            </div>  
			                </div>
			                <div class="tab-pane" id="detalle" role="tabpanel" aria-labelledby="baseIcon-tab22">
	   							  <table id="listadoDetalle" class="table" style="width: 100%">
					                <thead style="text-align: center">
				          				<tr>
				          					<th>Fecha</th>
				          					<th>Tipo</th>
				          					<th>Cliente</th>
				          					<th>Descripción</th>
				          					<th>Forma Pago</th>
				          					<th>Moneda</th>
				          					<th>Documento</th>
				          					<th>Recibo</th>
				          					<th>Monto</th>
				          				</tr>
					                </thead>
					                <tbody style="text-align: center">
										
					                </tbody>
					                <tfoot>
					                	<tr>
						                	<th style="text-align: center;">TOTAL</th>
						                	<th style="text-align: center;"></th>
						                	<th style="text-align: center;"></th>
						                	<th style="text-align: center;"></th>
						                	<th style="text-align: center;"></th>
						                	<th style="text-align: center;"></th>
						                	<th style="text-align: center;"></th>
						                	<th style="text-align: center;"></th>
						                	<th style="text-align: center;"></th>
						                </tr>
					                </tfoot>
					              </table>
			                </div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
    </div>
</section>	



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script>

	$(()=> {
	    $('.select2').select2();
        tablePagoAnticipo();
        calendar();

	});


	function tablePagoAnticipo() {
	    let table = $("#listado").DataTable({
	        "destroy": true,
	        "ajax": {
	            "url": "{{route('ajaxGrupoAnticipo')}}",
	            "type": "GET",
	            //"data": {"formSearch": $('#getDatosPagosDetalle').serializeArray() },
	            error: function (jqXHR, textStatus, errorThrown) {
	                $.toast({
	                    heading: 'Error',
	                    text: 'Ocurrió un error en la comunicación con el servidor.',
	                    position: 'top-right',
	                    showHideTransition: 'fade',
	                    icon: 'error'
	                });

	            }
	        },
	        "columns": [{
	                data: "cliente_n"
	            },
	            {
	                data: function (x) {
	                    let btn = '';
	                    if (x.tipo_data == 'P') {
	                        btn = `<a href="{{route('detallesProforma',['id'=>''])}}/${x.denominacion}" style="color:#111; font-weight: bold;" class="bgRed">
                                        <i class="fa fa-fw fa-search"></i> ${x.denominacion}</a>
                                        <div style="display:none;"></div>`;

	                    } else if (x.tipo_data == 'G') {

	                        btn = `<a href="{{route('verDetalle',['id'=>''])}}/${x.id_grupo}" style="color:#111; font-weight: bold;" class="bgRed">
                                        <i class="fa fa-fw fa-search"></i> ${x.denominacion}</a>
                                        <div style="display:none;"></div>`;

	                    } else if (x.tipo_data == 'F') {
	                        btn = `<a href="{{route('verFactura',['id'=>''])}}/${x.id_factura}" style="color:#111; font-weight: bold;" class="bgRed">
                                        <i class="fa fa-fw fa-search"></i> ${x.denominacion}</a>
                                        <div style="display:none;"></div>`;
	                    }

	                    return btn;
	                }
	            },
	            {
	                data: function (x) {
	                    return `<input type="text" class="number-format" style="border:0; background-color:#fff" disabled value="${x.monto}"> `;


	                }
	            },
	            {
	                data: function (x) {
	                    return `<input type="text" class="number-format" style="border:0; background-color:#fff" disabled value="${x.saldo}"> `;


	                }
	            },


	            {
	                data: function (x) {
	                    let btn = '';
	                    btn = `<a onclick="cargarReporteCuentasDetalle(${x.id_proforma_factura},${x.id_grupo},${x.id_proforma})" class="btn btn-info" style="padding-left: 6px;padding-right: 6px; color:#fff" role="button"><i class="fa fa-fw fa-search"></i></a>`;

	                    return btn;
	                }
	            }
	        ]

	    });

	    //REDIBUJAR TABLE Y LLAMAR A FORMAT NUMBER
	    table.on('draw', function () {
	        $('#listado tbody tr .number-format').inputmask("numeric", {
	            radixPoint: ",",
	            groupSeparator: ".",
	            digits: 2,
	            autoGroup: true,
	            // prefix: '$', //No Space, this will truncate the first character
	            rightAlign: false
	        });
	    });

	};

	function cargarReporteCuentasDetalle(id_proforma_factura='', id_grupo='', id_proforma='') {
	     let data = {
            id_proforma_factura:id_proforma_factura,
             id_grupo:id_grupo,
             id_proforma:id_proforma
         }

	    var grupoAnticpoDetalle = $("#listadoDetalle").DataTable({
	        "destroy": true,
	        "ajax": {
	            "url": "{{route('getDetallesGrupoAnticipo')}}",
	            "type": "GET",
	            "data": data,
	            error: function (jqXHR, textStatus, errorThrown) {}
	        },

	        "columns": [{
	                "data": function (x) {
	                    var fecha_salida = x.fecha;
	                    if (jQuery.isEmptyObject(fecha_salida) == false) {
	                        var fecha = fecha_salida.split('-');
	                        var fecha_final = fecha[2] + '/' + fecha[1] + '/' + fecha[0];
	                    } else {
	                        var fecha_final = "";
	                    }

	                    return fecha_final;
	                }
	            },
	            {
	                "data": "tipo"
	            },
	            {
	                "data": "cliente"
	            },
	            {
	                "data": function (x) {
	                    if (x.tipo == 'Grupo') {
	                        var btnx = `<a href="{{route('verDetalle',['id'=>''])}}/` + x.id_grupo + `" style="color:#111; font-weight: bold;" class="bgRed">
                    					<i class="fa fa-fw fa-search"></i>` + x.descgrupo + `</a><div style="display:none;">` + x.id_grupo + `</div>`;
	                    } else {
	                        var btnx = `<a href="{{route('detallesProforma',['id'=>''])}}/` + x.id_proforma + `" style="color:#111; font-weight: bold;" class="bgRed">
                    					<i class="fa fa-fw fa-search"></i>` + x.id_proforma + `</a><div style="display:none;">` + x.id_proforma + `</div>`;

	                    }

	                    return btnx;
	                }
	            },
	            {
	                "data": "denominacion"
	            },
	            {
	                "data": "currency_code"
	            },
	            {
	                "data": "nro_recibo"
	            },
	            {
	                "data": "nro_comprobante"
	            },
	            {
	                "data": "monto"
	            }




            ],
            "footerCallback": function (row, data, start, end, display) {
	            api = this.api();

	            total = api
	                .column(8)
	                .data()
	                .reduce(function (a, b) {

	                    return parseFloat(a) + parseFloat(b);
	                }, 0);

	            // Total over this page
	            pageTotal = api
	                .column(8, {
	                    page: 'current'
	                })
	                .data()
	                .reduce(function (a, b) {
	                    return parseFloat(a) + parseFloat(b);
	                }, 0);

	            // Update footer
	            $(api.column(8).footer()).html(
	                new Intl.NumberFormat("de-DE").format(total)
	            );

	        }

	    });
	    $('#tableCttaCtteDetalle').tab('show');


	}




    function calendar(){
        
    var Fecha = new Date();
	var fechaInicial = Fecha.getMonth() + 1 + "/" + Fecha.getFullYear();

	$('input[name="periodo"]').daterangepicker({
	    timePicker24Hour: true,
	    timePickerIncrement: 30,
	    locale: {
	        format: 'DD/MM/YYYY',
	        cancelLabel: 'Limpiar',
	        applyLabel: 'Aplicar',
	        fromLabel: 'Desde',
	        toLabel: 'Hasta',
	        customRangeLabel: 'Seleccionar rango',
	        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
	        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
	            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
	            'Diciembre'
	        ]
	    },
	    startDate: moment().subtract(30, 'days'),
	    // 						endDate: new Date(),
	});
	$('input[name="periodo"]').val('');


    }


	const formatter = new Intl.NumberFormat('de-DE', {
	    currency: 'USD',
	    minimumFractionDigits: 2
	});







	function formatearFecha(texto) {
	    if (texto != '' && texto != null) {
	        return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
	    } else {
	        return '';
	    }
	}






	</script>
@endsection
