<!DOCTYPE html>
<html lang="es" translate="no">
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
    @parent
@endsection
    
@section('content')
<style>
    .pagination .page-item {
        border-radius: 50%;
    }
    /* Estilos para la tabla con desplazamiento horizontal */
    .table-responsive {
        overflow-x: auto;
    }
</style>
<style type="text/css">
	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	/*.bgRed:hover{
		color: white;
	}*/

	/*background: #e2076a !important;padding-left: 6px;padding-right: 6px;*/



	/*==================SELECT STAR============================*/

	.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}


.click_estrella {
	cursor:pointer; 
	cursor: hand;
}

/*====================================================
	ESTILO PARA FOCUS
  ====================================================	
*/

input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }



</style>

<section id="base-style">
    <br>
<form id="ventaprestador">
    <div class="card">
        <div class="card-header">
            <div class="card-body">   
            <h4 class="card-title">Reporte General de Ventas</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        
            <div class="col-12 col-sm-3 col-md-3">
                <div class="form-group">
                   <label>Desde/Hasta</label>			
                       <div class="input-group">
                       <div class="input-group-prepend" style="">
                           <span id="fechcheck"class="input-group-text"><i class="fa fa-calendar"></i></span>
                       </div>
                      <input type="text" id="periodo_out" name="periodo_out" class="form-control">
                   </div>
               </div>	 
           </div>        
    
          
            <div class="col-md-3">
                <div class="form-group">
                    <label>Vendedor Agencia</label>
                    <select class="form-control" name="vendedor_id"  id="vendedor" style="width: 100%;" tabindex="12">
                        <option value="">Todos</option>
                        
                    </select>
                </div>
            </div>	

            <div class="col-md-3">
                <div class="form-group">
                    <label>Vendedor Empresa</label>
                    <select class="form-control finput-sm select2" name="vendedor_empresa" id="vendedor_empresa" style="width: 100%;" tabindex="6">
                        <option value="">Todos</option>
                        @foreach($vendedorEmpresa as $ven)
                        <option value="{{ $ven->id }}" @if($ven->id == request('vendedor_empresa')) selected @endif>{{ $ven->vendedorempresa }}</option>
                        @endforeach
                    
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Moneda</label>
                    <select class="form-control input-sm select2" name="id_moneda" id="moneda" style=" width: 100%;"  tabindex="6" >
                        <option value="">Todos</option>	
                            @foreach($currency as $key => $currency)
                            <option value="{{$currency->currency_id}}" @if($currency->currency_id == request('id_moneda')) selected @endif>{{$currency->currency_code}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
        </div> 
        <div class="row">
            <div class="col-12 col-sm-4 col-md-3">
                <div class="form-group">
                    <label for="formNombres">Nro.Factura</label>
                    <input type="text" class="form-control" tabindex="5" name="nro_factura"
                        id="nro_factura" value="" maxlength="100" />
                </div>
            </div>
            <div class="col-12 col-sm-4 col-md-3">
                <div class="form-group">
                    <label for="formNombres">Proforma</label>
                    <input type="text" class="form-control" tabindex="5" name="id_proforma"
                        id="id_proforma" value="" maxlength="100" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Producto</label>
                <select class="form-control finput-sm select2" name="producto_0" id="producto_0" style="width: 100%;" tabindex="6" >
                    <option value="">Todos</option>
                        @foreach($producto as $key=>$prod)
                        <option value="{{$prod['id']}}" @if($prod['id'] == request('producto_0')) selected @endif>{{$prod['denominacion']}}</option>
                        @endforeach	
                </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Proveedor</label>
                    <select class="form-control finput-sm select2" name="id_proveedor" id="id_proveedor" style="width: 100%;" tabindex="6">
                        <option value="">Todos</option>
                        @foreach($proveedor as $key=>$pro)
                        <option value="{{$pro['id']}}" @if($pro['id'] == request('id_proveedor')) selected @endif>{{$pro['proveedor']}}</option>
                        @endforeach	
                    </select>
                </div>
            </div>
            
        </div>
        <div class="row">
           

            <div class="col-md-3">
                <div class="form-group">
                    <label>Prestador</label>
                    <select class="form-control finput-sm select2" name="id_prestador" id="id_prestador" style="width: 100%;" tabindex="6">
                        <option value="">Todos</option>
                        @foreach($prestador as $key=>$pre)
                        <option value="{{$pre['id']}}" @if($pre['id'] == request('id_prestador')) selected @endif>{{$pre['denominacion_comercial']}}</option>
                        @endforeach	
                    </select>
                </div>
            </div> 
        </div>
    </form>   
        <div class="row">
             <div class="col-md-12">
             {{--   <a href="{{ route('facturapre_excel') }}" class="pull-right text-center btn btn-success btn-lg mr-1">Excel</a> --}}
             <button type="button" id="botonExcel" onclick="botonExcel()" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
             <button type="button" onclick="limpiar()" id="btnLimpiar" class="pull-right btn btn-light btn-lg text-white mr-1 mb-1" tabindex="17" ><b>Limpiar</b></button>
                <button type="button" class="pull-right btn btn-info btn-lg mr-1 mb-1" role="button" onclick="buscarFacturas()" tabindex="16"><b>Buscar</b></button>
            </div>
        </div>
 
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                 
                        <div class="row">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3">
                           
                            </div>
                        </div>
                        <div class="table-responsive">
                        <table id="vtaprestador" class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>PROVEEDOR</th>
                                    <th>PRESTADOR</th>
                                    <th>FECHA FACT.</th>
                                    <th>NUMERO FACT.</th>
                                    <th>CLIENTE</th>
                                    <th>CLIENTE TELEFONO</th>
                                    <th>CLIENTE EMAIL</th>
                                    <th>NUMERO PROFORMA</th>
                                    <th>ITEM/DESCRIPCION</th>
                                    <th>PRODUCTO</th>
                                    <th>CODIDO RESE.</th>
                                    <th>VENDEDOR EMPRESA</th>
                                    <th>VENDEDOR AGENCIA</th>
                                    <th>FECHA IN</th>
                                    <th>DESTINO FACTURA</th>
                                    <th>MONEDA VENTA</th>
                                    <th>VENTA</th>
                                    <th>MONEDA COSTO</th>
                                    <th>COSTO</th>
                                    <th>RENTA</th>
                                    <th>FECHA GASTO</th>
                                </tr>
                            </thead>
                            <tbody id="facturas-table">
                              
                                
                        
                            </tbody>
                        </table>
                 
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
 
    @include('layouts/gestion/scripts')
	<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
    <script>
        function buscarFacturas() {
      $('#vtaprestador').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ route('ventaPrestadorAjax') }}', 
            type: 'GET',
            data: $('#ventaprestador').serializeJSON()
        },
        columns: [
            { data: 'proveedor', name: 'proveedor' },
            { data: 'prestador', name: 'prestador' },
            { data: 'fecha_hora_facturacion', name: 'fecha_hora_facturacion' },
            { data: 'nro_factura', name: 'nro_factura' },
            { data: 'nombre', name: 'nombre' },
            { data: 'telefono', name: 'telefono' },
            { data: 'email', name: 'email' },
            { data: 'id_proforma', name: 'id_proforma' },
            { data: 'descripcion', name: 'descripcion' },
            { data: 'producto', name: 'producto' },
            { data: 'cod_confirmacion', name: 'cod_confirmacion' },
            { data: 'vendedorempresa', name: 'vendedorempresa' },
            { data: 'VendedorAgencia', name: 'VendedorAgencia' },
            { data: 'fecha_in', name: 'fecha_in' },
            { data: 'desc_destino', name: 'desc_destino' },
            { data: 'moneda_venta', name: 'moneda_venta' },
            { data: 'precio_venta', name: 'precio_venta' },
            { data: 'moneda_costo', name: 'moneda_costo' },
            { data: 'precio_costo', name: 'precio_costo' },
            { data: 'renta', name: 'renta' },
            { data: 'fecha_de_gasto', name: 'fecha_de_gasto' },
           
        ]
    });
}
//console.log(id_prestador);
/*function buscarConEnter(event) {
    if (event.key === 'Enter') {
        event.preventDefault(); // Evitar el envío del formulario
        buscarFacturas(); // Llamar a la función buscarFacturas()
    }
}*/
    
        /*function buscarFacturas() {
            var desde = document.getElementById('desde').value;
            var hasta = document.getElementById('hasta').value;
            var vendedor = document.getElementById('vendedor').value;

            var nroFactura  = document.getElementById('nro_factura').value;
            var idProforma = document.getElementById('id_proforma').value;

            var moneda = document.getElementById('moneda').value;
            var producto = document.getElementById('producto_0').value;
           
            var inputPrestador = document.getElementById('id_prestador').value;

            var inputvendedorEmpresa = document.getElementById('vendedor_empresa').value;

            var url = "{{ route('ventaPrestador') }}";
            url += "?desde=" + desde + "&hasta=" + hasta;
            if (vendedor) {
                url += "&vendedor_id=" + vendedor; // Agregar el ID del vendedor al URL si está seleccionado
            }

            if (nroFactura) {
                url += "&nro_factura=" + nroFactura; // Agregar el número de factura al URL si está especificado
            }

            if (idProforma) {
                url += "&id_proforma=" + idProforma; // Agregar el ID de proforma al URL si está especificado
            }

            if (moneda) {
                url += "&id_moneda=" + moneda;
            }

            if (producto) {
                url += "&producto_0=" + producto;
            }

            if (inputPrestador) {
                url += "&id_prestador=" + inputPrestador;
            }

            if (inputvendedorEmpresa) {
                url += "&vendedor_empresa=" + inputvendedorEmpresa;
            }

    window.location.href = url;
    
}*/
     /*   window.onload = function() {
            var params = new URLSearchParams(window.location.search);
            var desde = params.get('desde');
            var hasta = params.get('hasta');
            var vendedor = params.get('vendedor_id');
            var nroFactura = params.get('nro_factura'); // Obtener el valor del parámetro 'nro_factura'
            var idProforma = params.get('id_proforma'); // Obtener el valor del parámetro 'id_proforma'

            //var buscar = params.get('buscar'); // Nuevo: Obtener el valor del parámetro 'buscar'

            var moneda = params.get('id_moneda'); // Obtener el valor del parámetro 'id_moneda'
            var producto = params.get('producto_0'); // Obtener el valor del parámetro 'producto_0'
            var inputPrestador = params.get('id_prestador'); // Obtener el valor del parámetro 'id_prestador'
            var inputvendedorEmpresa = params.get('vendedor_empresa'); // Ob

            document.getElementById('desde').value = desde || '';
            document.getElementById('hasta').value = hasta || '';
            document.getElementById('vendedor').value = vendedor || '';
            document.getElementById('nro_factura').value = nroFactura || ''; // Asignar el valor de 'nro_factura' al campo correspondiente
            document.getElementById('id_proforma').value = idProforma || ''; // Asignar el valor de 'id_proforma' al campo correspondiente
            //document.querySelector('input[name="buscar"]').value = buscar || '';
            document.getElementById('moneda').value = moneda || ''; // Asignar el valor de 'id_moneda' al campo correspondiente
            document.getElementById('producto_0').value = producto || ''; // Asignar el valor de 'producto_0' al campo correspondiente
            document.getElementById('id_prestador').value = inputPrestador || ''; // Asignar el valor de 'id_prestador' al campo correspondiente
            document.getElementById('vendedor_empresa').value = inputvendedorEmpresa || ''; // Asignar el valor de 'vendedor_empresa' al campo correspondiente
        };*/
    

       /* $("#botonExcel").on("click", function(e) {
    e.preventDefault();

    window.location.href = "{{ route('generarExcelVentaPrestador') }}";
});*/

//excel
/*function botonExcel() {
            var desde = document.getElementById('desde').value;
            var hasta = document.getElementById('hasta').value;
            var vendedor = document.getElementById('vendedor').value;
            var nroFactura  = document.getElementById('nro_factura').value;
            var idProforma = document.getElementById('id_proforma').value;
            
            var moneda = document.getElementById('moneda').value;
            var producto = document.getElementById('producto_0').value;
            var inputPrestador = document.getElementById('id_prestador').value;
            var inputvendedorEmpresa = document.getElementById('vendedor_empresa').value;

            var url = "{{ route('generarExcelVentaPrestador') }}";
            url += "?desde=" + desde + "&hasta=" + hasta;
            if (vendedor) {
                url += "&vendedor_id=" + vendedor; // Agregar el ID del vendedor al URL si está seleccionado
            }
            if (nroFactura) {
                url += "&nro_factura=" + nroFactura; // Agregar el número de factura al URL si está especificado
            }

            if (idProforma) {
                url += "&id_proforma=" + idProforma; // Agregar el ID de proforma al URL si está especificado
            }
            
            
            if (moneda) {
                url += "&id_moneda=" + moneda;
            }

            if (producto) {
                url += "&producto_0=" + producto;
            }

            if (inputPrestador) {
                url += "&id_prestador=" + inputPrestador;
            }

            if (inputvendedorEmpresa) {
                url += "&vendedor_empresa=" + inputvendedorEmpresa;
            }



    window.location.href = url;
}*/

// DEFINE EL IDIOMA ESPAÑOL A TODOS LOS SELECT2



var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};
            $(document).ready(function() {

                $("#vendedor").select2({
                                language: lang_es_select2,
                                ajax: {
		                url: "{{route('get.vendedores.agencia')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });
            $("#producto_0").focusin(function(){
		   		$("#producto_0").select2('open');
		   	})

               
               let config = {
												timePicker24Hour: true,
												timePickerIncrement: 30,
												autoUpdateInput: false,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			};
            $('input[name="periodo_out"]').daterangepicker(config);
			$('input[name="periodo_out"]').val('');
			$('input[name="periodo_out"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
			$('input[name="periodo_out"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});

        });//READY DOCUMENT
        
                $(".select2").select2();

        function limpiar(){
          
    $('#vendedor').val('').trigger('change.select2');

    $('#desde').val('');
    $('#hasta').val('');
    $('#id_proforma').val('');
    $('#nro_factura').val('');
    $('#moneda').val('').trigger('change.select2');
    $('#producto_0').val('').trigger('change.select2');
    $('#id_prestador').val('').trigger('change.select2');
    $('#vendedor_empresa').val('').trigger('change.select2');
    $('#id_proveedor').val('').trigger('change.select2');

 
    //$('input[name="buscar"]').val('');

    // consultaAjax();
}
$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                 $('#ventaprestador').attr('method','post');
               $('#ventaprestador').attr('action', "{{route('generarExcelVentaPrestador')}}").submit();
            });

    </script>
@endsection


