
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
        	<br>
			<br>
			<br>
			<br>
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop" style="background-color: #fff;">
            	@include('flash::message')
				<div class="booking-information travelo-box">
					<div>
						<h1 class="subtitle hide-medium" style="font-size: xx-large;">Cargar Extracto</h1>
						<br>
						    <form method="post" action="{{url('mc/importExcel')}}" enctype="multipart/form-data">
						    {{csrf_field()}}
						    <div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<label class="control-label">Provedor:</label>
								</div>	
								<div class="col-xs-12 col-sm-6 col-md-6">
									<select name="proveedor" class="select2 form-control" required id="agenciaId">
										<option value="0">Seleccione un Proveedor</option>
										@foreach($proveedores as $key=>$proveedor)
											<option value="{{$proveedor['value']}}">{{$proveedor['label']}}</option>
										@endforeach
									</select>
								</div>	
							</div>
							<br>
						    <div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<label class="control-label">Archivo:</label>
								</div>
							    <div class="col-xs-12 col-sm-6 col-md-6">  	  
							        <input type="file" name="excel">
							    </div> 
						    </div>   
						    <br><br>
						        <input class="btn btn-primary" type="submit" value="Procesar" style="background: #e2076a !important;margin-left: 15px;">
						    </form>
						 	   
                    </div> 
                </div>           
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$('#agenciaId').select2();
    </script>
    @endsection
