@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	
	<style>
		.asistencia{
			display: none;
		}
	 	.error{
	 		color:red;
	 	},
	   	.cabeceraClass {
	   		padding: 10px;
	   		min-width: 250px;
	   		background-color:#E8EAEA;
	   		word-break: break-all;
	   		margin: 10px;

	   	},
		.modal-footer {
		    text-align: left;
		} 
		.nav-tabs>li>a {
		    color: #dacdcd;
		} 
		/*.verificarStyle {
			padding:5px 40px;
			margin-top: 5px;
		} */

		.base{
			cursor: pointer;
		}

		.modal-bodyProforma{
			  height:250px;
             overflow:auto;
			}

		.commentNone {
			display: none;
		}
		.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

		.text-bold{
			font-weight: 800;
			font-size: 15px;
		}


    .chat-content {
        text-align: left;
        float: left;       
        position: relative;
        display: block;
        padding: 8px 15px;
        margin: 0 20px 10px 0;
        clear: both;
        color: #404e67;
        background-color: #edeef0;
        border-radius: 4px;



    }
    .chat-content-left {
        text-align: right;
        position: relative;
        display: block;
        float: right;
        padding: 8px 15px;
        margin: 0 20px 10px 0;
        clear: both;
        color: #fff;
        background-color: #00b5b8;
        border-radius: 4px;
    }  
/*=============================================================================*/

.valoracion {
    position: relative;
    overflow: hidden;
    display: inline-block;
}

.valoracion input {
    position: absolute;
    top: -100px;
}


.valoracion label {
    float: right;
    color: #c1b8b8;
    font-size: 30px; 
}

.valoracion label:hover,
.valoracion label:hover ~ label,
.valoracion input:checked ~ label {
    color: #E2D532;
}


/*=============================================================================================*/
	.btnAdjuntar{
			/* background-color: #00a65a !important;
			border-color: #008d4c !important; 
			color:#FFFF !important;*/s
			font-weight:800 !important;  
			padding:10px !important;
	}


	.verificarStyle {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}
	
	.btnSolicitarVerificacion {
		padding:10px !important;
		/* background: #2d3e52 !important;
		border-color: #2d3e51 !important;
		color:#FFFF !important; */
	}

	.btnModificar {
		/* background: #e2076a !important; 
		border-color: #e25a7d !important;  */
		padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnAnular{
		/* background-color: #dd4b39 !important;
    	border-color: #d73925 !important; */
    	padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnHand {
		cursor:pointer; 
		cursor: hand;
	}

	.btnHand:hover {
  opacity: 0.8;
}

	#cierre .table tbody, {
	  padding: 0rem 0rem; 
	}

		  
	</style>
		
@endsection

@section('content')
<!-- Main content -->
<section id="base-style">
   	<div class="card-content">
		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Generar Cierre de Caja</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
                		<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body rounded-bottom rounded-lg">	
			        <ul class="nav nav-tabs nav-underline" role="tablist">
			            <li class="nav-item">
			                <a class="nav-link active" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#cabecera" role="tab" aria-selected="true">
			                	<i class="fa fa-play"></i> Resumen de Cierre de Caja</a>
			            </li>
			            <li class="nav-item">
			                <a class="nav-link" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#detalle" role="tab" aria-selected="false"><i class="fa fa-flag"></i> Detalle de Cierre de Caja</a>
			            </li>
			        </ul>
			        <div class="tab-content px-1 pt-1">
			            <div class="tab-pane active" id="cabecera" role="tabpanel" aria-labelledby="baseIcon-tab21">
			            	<div class="row">
				        		<div class="col-md-12">
				        			<form id="verCierreResumen" style="margin-top: 2%;" method="get">
				        				<!--<div class="row" style="height: 75px;">
											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
												<div class="form-group">
													<label>Fecha Emisión</label>
													<div class="input-group">
														<div class="input-group-prepend" style="">
															<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
														</div>
													   	<input type="text" readonly  class="form-control pull-right single-picker inputAdapt"  style="background-color: white;" name="fecha_creacion" id="fecha_emision" value="<?php echo date('d/m/Y')?>">
													</div>
												</div>
											</div>
				        				</div>-->
				        			</form>
				        			<div class="col-md-12" style="height:74px; height: 50px;">
										<button id="buscarCierre" class="btn btn-info pull-right btn-lg mr-1" style="display: none"><i class="glyphicon glyphicon-refresh"></i><b>Buscar</b></button>	
									</div>	
									<br>							
				        		</div>	
				        	</div>
			            	<div class="row">
				        		<div class="col-md-12"> 
				        			<div class="table-responsive table-bordered">
					        			<table id="resumenTabla" class="table">
					        				<thead>
					                            <tr>
					                            	<th>Forma de Cobro</th>
					                            	<th>Estado</th>
					                                <th>Moneda</th>
					                                <th>Monto</th>
					                            </tr>
					                        </thead>
					                        <tbody>
					                        </tbody>
					                    </table>
					                </div>    
				        		</div>
				        	</div>	 			            
			            </div>
			            <div class="tab-pane" id="detalle" role="tabpanel" aria-labelledby="baseIcon-tab22">
			            	<br>
				        	<div class="row">
				        		<div class="col-md-12">
						        	<div class="table-responsive table-bordered">
				                        <table id="cierre" class="table table-striped table-bordered" style="width: 100%;">
				                            <thead>
				                                 <tr>
				                                 	<th>Fecha</th>
				                                    <th>Recibo</th>
				                                    <th style="width:30%;">Cliente</th>
				                                    <th style="width:35%;">Detalle</th>
				                                    <th>Estado</th>
				                                    <th>Moneda</th>
				                                    <th>Total Recibo</th>
				                                </tr>
				                            </thead>
				                            <tbody>
				                            </tbody>
				                        </table>
				                    </div>
				                </div>  
			                </div>
							<div class="row">
				        		<div class="col-md-10">
				        		</div>
				        		<div class="col-md-2">
				        			<a  onclick ="cerrarCaja()" class=" text-center btn btn-info btn-lg" style="margin-top: 20px; color:#FFF;" role="button"><b>CERRAR CAJA</b></a>
	
				        		</div>
				        	</div>			        		
				        </div>	
			        </div>    	
				</div>
			</div>
		</div>
	</div>
</section>		
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script type="text/javascript">
			$('#resumenTabla').dataTable();
			$('.select2').select2();
			$("#fecha_emision").datepicker({ 
						format: "dd/mm/yyyy", 
						language: "es",
						clearBtn : true
						});
			$("#fecha_emision").datepicker("setDate", new Date());
			var Fecha = new Date();
			var fechaInicial =  Fecha.getMonth()+ "/" + Fecha.getFullYear();
			function resumenCierre(fecha){
				var dataString = $("#verCierreResumen").serialize();
				$.ajax({
					type: "GET",
					url: "{{route('getResumenCierreGenerar')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
						console.log(rsp);	
						var oSettings = $("#resumenTabla").dataTable().fnSettings();
						var iTotalRecords = oSettings.fnRecordsTotal();
						for (i=0;i<=iTotalRecords;i++) {
							$("#resumenTabla").dataTable().fnDeleteRow(0,null,true);
						}
						$.each(rsp, function (key, item){
							var dataTableRow = [
												'<b>'+item.denominacion+'</b>',
												'<b>'+item.estado_denominacion+'</b>',
												'<b>'+item.currency_code+'</b>',
												'<b>'+ new Intl.NumberFormat('de-DE').format(parseFloat(item.total).toFixed(2))+'</b>',
												//iconocancelar
											];

							var newrow = $("#resumenTabla").dataTable().fnAddData(dataTableRow);
							var nTr = $("#resumenTabla").dataTable().fnSettings().aoData[newrow[0]].nTr;
							var nTds = $('td', nTr);
						})
					}
				})	
				resumenDetalleCierre();
			}	

			function resumenDetalleCierre(){

				$('#cierre tbody').unbind();
				//$('#cierre').DataTable().clear().destroy();
				$('#cierre').dataTable({
				        "ajax": {
				        	"serverSide": true,
				            "url": "{{route('getDatosCierreGenerar')}}",
				            "data": $("#verCierreResumen").serializeArray(),
    						},
    						"destroy": true,
    						"processing": true,
    						"deferLoading": 57,
				            "columns": [
				            				{
								                "className":      'details-control',
								                "orderable":      false,
								                // "data":           "fecha",
								                "defaultContent": "",
								                "className": 'dt-body-right'
								            },
								            {
								                "className":      'details-control',
								                "orderable":      false,
								                "data":           "nro_recibo",
								                "defaultContent": "",
								                "className": 'dt-body-right'
								            },
								            { 
								            	"data": "cliente",
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "concepto",
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
											{ 
								            	"data": "estado",  
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },	

								            { 
								            	"data": "moneda",  
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
											{ 
								            	"data": "totalRecibo",  
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },	
										],
										"order": [[1, 'asc']]	
    					})	

					    $('#cierre tbody').on('click', 'td.dt-body-right', function () {
 							table = $('#cierre').dataTable();
 							
					        var tr = $(this).closest('tr');
					        var row = $('#cierre').DataTable().row(tr);
					        if ( row.child.isShown() ) {
					            // This row is already open - close it
					            tr.removeClass('shown');
					            row.child.hide();
					        }
					        else {
					            // Open this row
					            row.child( format(row.data())).show();
					            tr.addClass('shown');
					        }
					    } );
					  //  $("#buscarCierre").trigger('click');
				}

			function format(d) {
				
				var tabla = '<div class="table-responsive table-bordered" style="overflow-x: hidden;"><table cellpadding="6" cellspacing="0" border="0" style="width: 100%"> ';
					tabla+= '<tr>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px; width: 30%;"><b>Forma de Pago</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px; width: 30%;"><b>Nro Comprobante</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px; width: 20%;"><b>Moneda</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px; width: 20%;"><b>Importe</b></td>';
					tabla+= '</tr>';
					var totalGs = 0;
					var totalUs = 0;
					$.each(d.detalles, function (key, item){
						console.log(item);
						tabla+= '<tr>';
						if(item.id_tipo_pago == 1){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"> -- </td>';
						}

						if(item.id_tipo_pago == 2){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+' - '+item.banco+''+item.numero_cuenta+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.numero+'</td>';
						}


						if(item.id_tipo_pago == 3){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+' - '+item.banco_plaza+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.numero+'</td>';
						}

						if(item.id_tipo_pago == 4){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+' - '+item.banco+' - '+item.numero_cuenta+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.nro_comprobante+'</td>';
						}

						if(item.id_tipo_pago == 5){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+' - '+item.id_deposito_bancario+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.nro_comprobante+'</td>';
						}
						if(item.id_tipo_pago == 6){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.nro_comprobante+'</td>';
						}


						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b>'+item.moneda_cobro+'</b></td>';
						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b>'+new Intl.NumberFormat('de-DE').format(parseFloat(item.importe_cobro).toFixed(2))+'</b></td>';
						tabla+= '</tr>';
					})	
					tabla+= '</table></div>';
					console.log(tabla);
				    return tabla;
				}

			function cerrarCaja(){
				return swal({
                    title: "GESTUR",
                    text: "¿Desea generar el Cierre de Caja?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, generar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            generarCierre();
                        } else {
                            swal("Cancelado", "", "error");
                        }
            	});
			}

			function generarCierre(){
				var dataString = $("#verCierreResumen").serialize();
				$.ajax({
					type: "GET",
					url: "{{route('getCerrarCaja')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
						console.log(rsp.respuesta);
						if(rsp.respuesta == 'OK'){
							swal("Éxito", "", "success");
							setTimeout(window.location.replace("{{route('cierre')}}"), 30000);
								
						}else{
							swal("Cancelado", rsp.respuesta ,"error");
						}	
					}
				})	
			}

			resumenCierre($("#fecha_emision").val());

			$(document).ready(function() {
				$("#buscarCierre").click(()=>{
					$('#cierre').DataTable().clear().destroy();
					resumenCierre($("#fecha_emision").val());
				});  

			})	

	</script>



@endsection
