
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

	.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

.fuenteDtplus{
	font-weight: 800;
	font-size: 15px;
}
#montoDtplus{
	color: green;
}

</style>

<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Reporte Incentivo</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	<form id="consultaIncentivo" style="margin-top: 2%;" method="post" >
					<div class="row"  style="margin-bottom: 1px;">
			            <div class="col-md-3">
					        <div class="form-group">
					            <label>Agencia</label>
								<select class="form-control select2" name="idEmpresaAgencia"  id="idEmpresaAgencia" style="width: 100%;">
									<option value="">Seleccione Agencia</option>
									@foreach ($agencias as $agencia)
										<option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
									@endforeach
								</select>
					        </div>
			            </div>
			            <div class="col-md-3">
					        <div class="form-group">
					            <label>Vendedor</label> <span class="combo_1 combo"> </span> <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg combo_1"></i>
								<select class="form-control select2" name="vendedor"  id="vendedor" style="width: 100%;">
									<option value="">Seleccione Vendedor</option>

									@foreach ($vendedores as $vendedor)
										<option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
									@endforeach
									
								</select>
					        </div>
			            </div>
			            <div class="col-md-3">
					        <div class="form-group">
					            <label>Habilitado para  Cobro</label>
								<select class="form-control select2" name="filtroPago"  id="filtroPago" style="width: 100%;">
									<option value="">Todos</option>
									<option value="C">SI</option>
									<option value="P">NO</option>
								</select>
					        </div>
			            </div>
					    <div class="col-md-3">
							<div class="form-group">
							 	 <label>Email</label>
							     <input type="text" class="form-control" id="email"  name="email" value="" >
							</div>
						</div>
				    </div>
					<div class="row" style="margin-bottom: 1px;" >
						<div class="col-md-9">
							<div  class="readOnly fuenteDtplus" id="dtpPlusInfo" style="height:30px; width: 100%; border:1px solid #d2d6de; padding: 6px;"><span class="textDtplus">TOTAL A CANJEAR POR DTPLUS $ </span><span id="montoDtplus"></span></div>
						</div>
				 		<div class="col-md-3" style="padding-right: 25px;">
				    		<a  onclick ="consultaAjax()" class="pull-right text-center btn btn-info btn-lg" style="color:#fff;margin-right: 20px;" role="button"><b>Buscar</b></a>
						</div>
		  			</div>
				</form>    
				<div class="table-responsive table-bordered">
		            <table id="listado" class="table">
		                <thead>
						    <tr>
							  	<th>Fecha</th>
							  	<th>N° Factura</th>
								<th>Nombre Titular</th>
								<th>Incentivo</th>
								<th>Total Bruto Factura</th>
								<th>Estado Cobro</th>
				            </tr>
		                </thead>
		                <tbody style="text-align: center">
		 					
					          
				         
				        </tbody>
		            </table>
	            </div>  


            </div>
        </div>    	
    </div>    
</section>       


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
<script type="text/javascript">


//CALENDARIO

$(function() {

 $('.select2').select2();
 consultaAjax();
});

	
	$('#filtroPago').change(function(){

				var opcion = $('#filtroPago').val();

				if(opcion == 'P' && opcion == ''){
				$('#textDtplus').html('TOTAL A CANJEAR POR DTPLUS $');
				}
				if(opcion == 'C'){
				$('#textDtplus').html('TOTAL A CANJEAR POR FACTURAS A PAGAR $');
				}
		});



	/*
	Para formatear numeros a moneda USD PY
	 */
	const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD',
					  minimumFractionDigits: 2
					});

		
		


			function consultaAjax(){

				 $('#montoDtplus').html('0,00');

		$.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });

		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){

					
			 table =   $('#listado').DataTable({
					        "destroy": true,
					        "select": true,
					        "ajax": {
					        		data: { "formSearch": $('#consultaIncentivo').serializeArray() },
						            url: "{{route('getdatosDTPLus')}}",
						            type: "GET",
						    error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                            $.unblockUI();

                        }
                        		// formatter.format(parseFloat(x.total_venta_proforma));
						  },
						 "columns":[{data:'fecha'},
						 			{data:'numeroFactura'},
						 			{data:'pasajero'},
						 			{data: function(x){ 
						 				return formatter.format(parseFloat(x.dtplus));
						 			}},
						 			{data:function(x){ 
						 				return formatter.format(parseFloat(x.total));
						 			}},
						 			{data:function(x){
						 				var ico = '';

						 				if(x.estadoFactura == 'C'){
						 					ico = `<i class="fa fa-thumbs-o-up icoVerde"></i><div style="display:none">1</div>`;
						 				}else if(x.estadoFactura == 'P'){
						 					ico = `<i class="fa fa-exclamation-triangle icoAmarillo"></i><div style="display:none">2</div>`;
						 				}
						 				return ico;
						 			}}],


							// "aaSorting":[[2,"desc"]],
						     "createdRow": function ( row, data, index ) {}			
						                    
					    }).on('xhr.dt', function ( e, settings, json, xhr ){
								//Ajax event - fired when an Ajax request is completed.;
							 $.unblockUI();

							 var cobrado = formatter.format(parseFloat(json.otros.sumCobrado));
							 var pendiente = formatter.format(parseFloat(json.otros.sumPendiente));

							 if($('#filtroPago').val() == 'P'){

							 	if(pendiente != 'NaN' && pendiente != 'undefined'){
							 	 $('#montoDtplus').html(pendiente);
							 	  $('#montoDtplus').css('color','blue');
							 	}
							 }

							 if($('#filtroPago').val() == 'C' || $('#filtroPago').val() == ''){

							 	if(cobrado != 'NaN' && cobrado != 'undefined'){
							 	 $('#montoDtplus').html(cobrado);
							 	 $('#montoDtplus').css('color','green');
							 	}
							 }


								});

			 
					    	}, 300);


		

}//function











		  $('#idEmpresaAgencia').change(function(){
    		
		  	var idAgencia = {'idAgencia' : $('#idEmpresaAgencia').val(), 
		  					 'idEstado':true,
		  					 'flag':1};
		  		$('.cargandoImg').show();
            	$('.combo').html('  ');

  				$.ajax({
                        type: "GET",
                        url: "{{route('getDatosAgencia')}}",
                        dataType: 'json',
                        data: idAgencia,

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                           $('.combo').css('color','#DB6042');
                           $('.cargandoImg').hide();
                           $('.combo').html('<b> No se pudo actualizar. </b>');
                        },
                        success: function(rsp){

                        	console.log(rsp);

                            if(rsp.resp.length != 0 ){

                            	//validar si es null
                            	 function n(n){
                                if(n === null){ return '';  
                            	} 
                            	return n;
                                }

                                 function t(n){
                                if(n === null){ return '';  
                            	} 
                            	return '- '+n;
                                }

                                // console.log('dd');
                              $('#vendedor').empty();

                               var newOption = new Option('Seleccione Persona', '', false, false);
                                $('#vendedor').append(newOption);


                                $.each(rsp.resp, function (key, item){
                                var newOption = new Option(n(item.nombre)+' '+n(item.apellido)+' '+t(item.tipo_persona.denominacion), item.id, false, false);
                                $('#vendedor').append(newOption)
                            });

                            } else{ 

                            $('#vendedor').empty();
                            var newOption = new Option('Seleccione Persona' ,'', false, false);
                                $('#idPersona').append(newOption);   
                            // $('#vendedor').select2({ disabled: true });

                                    }//else

                           			$('.combo').css('color','#489946');
                                     $('.cargandoImg').hide();
                           			$('.combo').html('<b> Combo actualizado. </b>');
                          
                                }//funcion  
                });



    
  });



$('#vendedor').change(function(){


		var idAgencia = {'id_persona' : $('#vendedor').val(), 'idEstado':true};
		  		$('.cargandoImg').show();
            	$('.combo').html('  ');

  				$.ajax({
                        type: "GET",
                        url: "{{route('getDatoPersonaEstado')}}",
                        dataType: 'json',
                        data: idAgencia,

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                           $('.combo').css('color','#DB6042');
                           $('.cargandoImg').hide();
                           $('.combo').html('<b> No se pudo actualizar. </b>');
                        },
                        success: function(rsp){

                 
                        	if(rsp.resp.length > 0)
                        	{
                        	      $('#email').val(rsp.resp[0].email);
                           	      $('.combo').css('color','#489946');
                                  $('.cargandoImg').hide();
                           	      $('.combo').html('<b> Combo actualizado. </b>');

                           		}
                          
                                }//funcion  
                });







});


	

</script>

@endsection


