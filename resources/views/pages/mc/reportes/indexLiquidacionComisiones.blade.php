@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#buttonsLiq {
		display: inline;
	}
	#buttonsLiq .dt-buttons {
		display: inline;
	}


/*sdsdds*/
.checkbox label:after {
  content: '';
  display: table;
  clear: both;
}

.checkbox .cr {
  position: relative;
  display: inline-block;
  border: 1px solid #a9a9a9;
  border-radius: .25em;
  width: 1.3em;
  height: 1.3em;
  float: left;
  margin-right: .5em;
}

.checkbox .cr .cr-icon {
  position: absolute;
  font-size: .8em;
  line-height: 0;
  top: 50%;
  left: 15%;
}

.checkbox label input[type="checkbox"] {
  display: none;
}

.checkbox label input[type="checkbox"]+.cr>.cr-icon {
  opacity: 0;
}

.checkbox label input[type="checkbox"]:checked+.cr>.cr-icon {
  opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled+.cr {
  opacity: .5;
}

	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}

	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

	.rojo{
		color: red;
		font-weight: 700;
	}

	.verde{
		color: green;
		font-weight: 700;
	}

</style>
    <!-- Main content -->
<section class="content">
	<form id="consultaLiquidaciones" style="margin-top: 2%;" method="get">

	<div class="row">
        <div class="col-xs-12">
			<div class="box">
           		@include('flash::message') 
				<div class="box-header">
					<div class="col-md-11">
		             	<h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Liquidación de Comisiones</h1>
					</div>
				</div>  
            
            	<div class="box box-default" style="border-top-width: 1px;margin-bottom: 0px;">
					<div class="box-header with-border">
            			<h3 class="box-title" style="margin-left: 15px;">Filtros de búsqueda</h3>
					</div>

	
					<div class="row" style="margin-bottom: 1px;">
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
							<div class="form-group">
								<label>Vendedor</label>	
							
									<select class="form-control input-sm select2" name="vendedor" id="vendedor" style="padding-left: 0px;width: 100%;">
										@foreach($vendedor as $key=>$vended)
	                                        <option value="{{$vended->id}}">{{$vended->nombre}} {{$vended->apellido}}
	                                        </option>
	                                    @endforeach
									</select>
								</div>
							</div> 

							<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px; padding-top: 10px;">
								<div class="form-group">
									<label>Periodo</label>					            	
									<select class="form-control input-sm select2"   name="periodo" id="periodo" style="padding-left: 0px;width: 100%;">
										@foreach($listadoFechas as $key=>$listadoFecha)
											<option value="{{$listadoFecha}}">{{$listadoFecha}}</option>
										@endforeach	
									</select>
								</div>
							</div>

							<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px; padding-top: 10px;">
								<div class="form-group">
									<label>Renta</label>
				     				<input type="text" class="form-control" id="renta"  name="renta" value="" disabled="disabled">
								</div>
							</div>

							<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px; padding-top: 10px;">
								<div class="form-group">
									<label>Porcentaje Comisión</label>
				     				<input type="text" class="form-control" id="porcentaje"  name="porcentaje" value="" disabled="disabled">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-3 col-md-9" style="padding-left: 35px;padding-right: 25px; padding-top: 10px;">
								<div class="form-group">
									<label>Comentarios</label>
					     			<input type="text" class="form-control" id="comentario"  name="comentario" value="" disabled="disabled">
								</div>
							</div>
						</div>
										
						<div class="row">
							<div class="col-xs-12"  style="padding-left: 15px;padding-right: 25px; padding-top: 10px;">
								<button type="button" id="btnLimpiar" class="btn btn-default btn-lg pull-right"  style="margin: 0 0 10px 10px;" >Limpiar</button>	

								<div class="pull-right" id="buttonsLiq" ></div>

								<a  onclick ="mostrarLiquidaciones()" class="pull-right  btn btn-info btn-lg" style="margin: 0 5px;" role="button"><b>Buscar</b></a>

							</div>
						</div>	
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="table-responsive">
	              <table id="listado" class="table" style="width: 100%">
	                <thead style="text-align: center">
						<tr>
							<th>Agencia</th>
							<th>Vendedor</th>
							<th>Tipo</th>
							<th>Documento</th>
							<th>Fecha</th>
							<th style="align-content: center;">Monto</th>
							<th style="align-content: center;">Estado Cobro</th>
							<th>Estado Factura</th>
							<th>Renta F</th>
							<th>Renta C</th>
							<th>Comisión</th>
							<th>% Comisión</th>
							<th>A cobrar</th>
							<th>Ver</th>
			            </tr>
	                </thead>
	                
	                <tbody style="text-align: center">
						
	                </tbody>
	              </table>
	            </div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </form>
    </section>
    <!-- /.content -->

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/dataTables.buttons.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jszip.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/buttons.html5.min.js')}}"></script>
	<script>
		var tableLiq;

		$(document).ready(function() 
		{
			$('.select2').select2();
			tableLiq = mostrarLiquidaciones();
		});


			$("#listado").dataTable
			({
				"aaSorting":[[2,"asc"]]
			});

			$('input[name="periodo"]').daterangepicker
			({
				timePicker24Hour: true,
				timePickerIncrement: 30,
				locale: 
				{
					format: 'MM/YYYY',
					cancelLabel: 'Limpiar'
				},
													 
			});

			$("#btnLimpiar").click(function()
			{
				// $('#periodo').val('').trigger('change.select2');
				tableLiq = mostrarLiquidaciones();
			});

			$('#botonExcelLiq').on('click',function()
			{
				$("#requestLiqExcel").modal("show");
			});

			
			$('.checkCol').on('click',function()
			{
				var form = $('#columLiqExcel').serializeArray();
				valor = [];
				// valor.push(0,1,2,8);
				valor.push(0,1,2,3,4,6,7,11);
				$.each(form, function(item,value){
					valor.push(parseInt(value.value));
				});

				buttonsLiq(valor,tableLiq);
			});

			
				const formatter = new Intl.NumberFormat('de-DE', {
                      currency: 'USD',
                      minimumFractionDigits: 2
                    });

			mostrarLiquidaciones(); 

 			function formatearFecha(texto)
 			{
	            if(texto != '' && texto != null)
	            {
	          		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	            } 
	            else 
	            {
	         		return '';    
            	}
        	}

			function mostrarLiquidaciones()
			{ 
				var tableLiq = $("#listado").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('mostrarLiquidaciones')}}",
						"type": "GET",
						"data": {"formSearch": $('#consultaLiquidaciones').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					"aaSorting":[[2,"asc"]],
					"columns": 
					[
						
						{ "data": "agencia" },

						{ "data": "vendedor"},

						{ "data": "tipo_documento"},

						{ "data": "nro_documento" },

						{ "data": function(x)
							{
								var fecha = x.fecha.split(' ');
								var f = formatearFecha(fecha[0]);
								return f+' '+fecha[1];
							} 
						},

						{ "data": function(x)
							{
								// console.log(x.monto);
								var monto = 0;

								if (x.monto != null) 
								{
									monto = formatter.format(parseFloat(x.monto))
								}
								return monto;
							}
						},

						{ "data": "estado_cobro" },

						{ "data": "estado" },

						{ "data": function(x)
							{
								var renta_f = 0;

								if (x.renta_total) 
								{
									renta_f = formatter.format(parseFloat(x.renta_total));
								}
								return renta_f;
							}
						},

						{ "data": function(x)
							{
								return formatter.format(parseFloat(x.renta_comisionable));
							}
						},
						{ "data": function(x)
							{
								return formatter.format(parseFloat(x.comision));
							}
						},

						{ "data": "porcentaje_comision" },

						{ "data": function(x)
							{
								let m = '';

								if (x.a_cobrar == true)
								{
									m = `<i class='fa fa-thumbs-o-up icoVerde'></i>
									    <div style="display: none;">0</div>`
								}
								else
								{
									
									m = `<i class='fa fa-exclamation-triangle icoRojo'></i>
									    <div style="display: none;">2</div>`;
								}

								return m;
							} 
						},

						{ "data": function(x)
							{
								if (x.tipo_documento == 'FA')
								{
									var btn = `<a href="{{route('verFactura', '')}}/${x.id_documento}" class="btn btn-danger" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-search"></i></a>`;
								}
								else
								{
						       		var btn = `<a href="{{route('verNota', '')}}/${x.id_documento}" class="btn btn-danger" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-search"></i></a>`;
						       	}
						        return btn;
						    }
						}
					], 

					"fnInitComplete": function(oSettings, json) 
					{
						if (json.data4 != 0) 
						{
                       		$('#comentario').val(json.data2+' '+json.data4);
						}
						else
						{
							$('#comentario').val(json.data2);
						}

                       	$('#renta').val(json.data3);
                       	$('#porcentaje').val(json.data4);

                       	if (parseInt(json.data5) == 0) 
                       	{
                       		$('#comentario').css("color","red");
                       		$('#comentario').css("font-weight", "700");
                       		$('#comentario').css("font-size", "17px");
                       	}
                       	else
                       	{
                       		$('#comentario').css("color","green");
                       		$('#comentario').css("font-weight", "700");
                       		$('#comentario').css("font-size", "17px");
                       	}
                    }	
				});	
				buttonsLiq([0,1,2,3,4,5,6,7,8,9,10,11], tableLiq);
				return tableLiq;
			
	};

	function buttonsLiq(column,tableLiq)
	{
		$('#buttonsLiq').html('');
		var buttons = new $.fn.dataTable.Buttons(tableLiq, 
		{ 
			buttons: 
			[{	
				title: 'Reporte Liquidación de Comisiones',
				extend: 'excelHtml5',
				text: '<b>Excel</b>',
				className: 'pull-right text-center btn btn-success btn-lg',
				exportOptions: 
				{
					columns: ':visible'
				},
				exportOptions: 
				{
					columns:  column,
					format: 
					{
						//seleccionamos las columnas para dar formato para el excel
						body: function ( data, row, column, node ) 
						{
							if(column != 0 && column != 1 && column != 2 && column != 3 && column != 4 &&  
								column != 6 && column != 7 && column != 11)
							{
								if (data != '0') 
								{
									console.log(data);
									data = data.replace(".","");
									//cambiamos la cpunto
									data = data.replace(",",".");
																			
								}
									var numFinal = parseFloat(data);
									return  numFinal;
							} 
							return data;
						}
					}
				},
				
				//Generamos un nombre con fecha actual
				
				filename: function() 
				{
					var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
					return 'ReporteLiquidacionComisiones-'+date_edition;
				}
			}]
		}).container().appendTo('#buttonsLiq'); 
	}


	</script>
@endsection
