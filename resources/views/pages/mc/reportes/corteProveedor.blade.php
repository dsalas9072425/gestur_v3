
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">
	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	/*.bgRed:hover{
		color: white;
	}*/

	/*background: #e2076a !important;padding-left: 6px;padding-right: 6px;*/



	/*==================SELECT STAR============================*/

	.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}


.click_estrella {
	cursor:pointer; 
	cursor: hand;
}

/*====================================================
	ESTILO PARA FOCUS
  ====================================================	
*/

input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }



</style>
    <!-- Main content -->
   <section id="base-style">
   		@include('flash::message')
   		<div class="card-content">
		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Corte de Proveedores</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
				<div class="card-body pt-0">
				</div>		        
				<div class="card-body">
		     		<form id="filtrosForm">
		     				<div class="row">
								<div class="col-md-3">
									<div class="form-group">
								        <label>Proveedor</label>
								        <select class="form-control select2" name="id_vendedor_empresa" id="id_vendedor_empresa" style="width: 100%;" tabindex="5">
											<option value="">Todos</option>
											@foreach($proveedores as $key=>$proveedor)
												@php
													$ruc = $proveedor->documento_identidad;
													if($proveedor->dv){
														$ruc = $ruc."-".$proveedor->dv;
													}
												@endphp
												<option value="{{$proveedor->id}}">{{$ruc}} - {{$proveedor->nombre}} {{$proveedor->apellido}}</option>
											@endforeach									        
										</select>
							        </div>					
							    </div>
		     					<div class="col-12 col-sm-3 col-md-3">
				 					<div class="form-group">
							            <label>Fecha Desde/Hasta</label>			
							            	<div class="input-group">
										    <div class="input-group-prepend" style="">
										        <span id="fecha" class="input-group-text"><i class="fa fa-calendar"></i></span>
										    </div>
										    <input type="text" id="periodo" name="periodo" class="form-control">
										</div>
									</div>	
								</div> 

								<div class="col-md-3">
			 						<div class="form-group">
							            <label>Hora</label>
										<select class="form-control input-sm select2" name="id_estado" id="id_estado" style=" width: 100%;"  tabindex="4" >
											<option value="">Todos</option>
											<?php 
											for ($x = 0; $x <= 60; $x++) {
												echo "<option value=".$x.">".$x."</option>";
											} 
											?>

										</select>
						        	</div>
								</div>	
							</div>
					    </form>
						<div class="col-md-12">	
						<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg" style=" margin: 0 0 10px 10px;"><b>Excel</b></button>
							<button type="button" onclick="limpiar()"class="pull-right btn btn-light btn-lg text-white mr-1 mb-1" tabindex="7" ><b>Limpiar</b></button>
						 	<button type="button" class="pull-right  btn btn-info btn-lg mr-1 mb-1"  role="button" onclick="buscar()" tabindex=8><b>Buscar</b></button>
						</div>	 	
					</div>		 	
					<div class="table-responsive table-bordered">
		            	<table id="listado" class="table" style="width: 100%;">	
		            	    <thead>
								<tr style="text-align: center;">
							        <th>Fecha Venta</th>
							        <th>Hora Corte</th>
							        <th>Numero Pedido</th>
							        <th style="width: 30%;">Proveedor</th>
							        <th style="width: 30%;">Producto</th>
									<th>Sku</th>
							        <th>Cantidad</th>
							        <th>Precio Costo</th>
									<th>Numero Venta</th>
									<th>Estado Venta</th>
								</tr>
			                </thead>
			                <tbody style="text-align: center;">

			                </tbody>
			            </table>
			        </div>  
		        </div>
		    </div>
		</div>
    </div>	
</section>
    <!-- /.content -->



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>

	<script>
		$(document).ready(function() 
		{
			$('#fecha').click(function(){
				$('input[name="periodo"]').trigger('click');
			}) 

			var Fecha = new Date();
	        var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

			let select_1 = $('.select2').select2();
			let select_2 = $('.estado_proforma').select2({
									multiple:true,
									maximumSelectionLength: 2,
									placeholder: 'Todos'
								});
			let config = {
							timePicker24Hour: true,
							timePickerIncrement: 30,
							autoUpdateInput: false,
							locale: {
										format: 'DD/MM/YYYY',
										cancelLabel: 'Limpiar',
										applyLabel: 'Aplicar',					
										fromLabel: 'Desde',
										toLabel: 'Hasta',
										customRangeLabel: 'Seleccionar rango',
										daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',  'Diciembre']
									}
						};

			$('input[name="periodo"]').daterangepicker(config);
			$('input[name="periodo"]').val('');
			$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');});
			$('input[name="periodo"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});
			
			$('#listado').DataTable();
			// buscar();
		});

		const formatter = new Intl.NumberFormat('de-DE', {currency: 'USD',minimumFractionDigits: 0});


		function buscar()
		{				
			console.log('Egfkjhg');
			table =   $('#listado').DataTable({
					        "destroy": true,
					        "select": true,
							"order": [ 0, 'desc' ],
					        "ajax": 
					        {
					        	data: $('#filtrosForm').serializeJSON(),
						        url: "{{route('getCorteProveedor')}}",
						        type: "GET",
						    	error: function(jqXHR,textStatus,errorThrown)
						    	{
		                           $.toast({
			                            heading: 'Error',
			                            text: 'Ocurrió un error en la comunicación con el servidor.',
			                            position: 'top-right',
			                            showHideTransition: 'fade',
			                            icon: 'error'
			                        });
	                            	
	                            	$.unblockUI();
                        		}
						  	},

						 "columns":
						 [
						 			{data: "fecha_venta"},
						 			{data: "hora_corte"},
						 			{data: "numero_pedido"},
						 			{data: "proveedor"},
						 			{data: "producto"},
						 			{data: "sku"},
						 			{data: "cantidad"},
						 			{data: "precio_costo"},
						 			{data: "numero_venta"},
						 			{data: "estado_venta"}
						 		
						 	]		
						                    
					    })

		}//function

		$("#botonExcel").on("click", function(e){ 
			
                e.preventDefault();
                 $('#filtrosForm').attr('method','post');
               $('#filtrosForm').attr('action', "{{route('generarExcelCostoProveedor')}}").submit();
            });

		function limpiar()
		{
			$('#periodo').val('').trigger('change.select2');
			$('#id_vendedor_empresa').val('').trigger('change.select2');
			$('#id_estado').val('').trigger('change.select2');
		}

		function formatearFecha(texto)
 		{
	        if(texto != '' && texto != null)
	        {
	          	return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	        }
	        else
	        {
	         	return '';
         	}
        }

	</script>
@endsection