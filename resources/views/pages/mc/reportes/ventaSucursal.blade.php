
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">
	
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#buttonsSuc {
		display: inline;
	}
	#buttonsSuc .dt-buttons {
		display: inline;
	}

	#buttonsVend {
		display: inline;
	}
	#buttonsVend .dt-buttons {
		display: inline;
	}


/*sdsdds*/
.checkbox label:after {
  content: '';
  display: table;
  clear: both;
}

.checkbox .cr {
  position: relative;
  display: inline-block;
  border: 1px solid #a9a9a9;
  border-radius: .25em;
  width: 1.3em;
  height: 1.3em;
  float: left;
  margin-right: .5em;
}

.checkbox .cr .cr-icon {
  position: absolute;
  font-size: .8em;
  line-height: 0;
  top: 50%;
  left: 15%;
}

.checkbox label input[type="checkbox"] {
  display: none;
}

.checkbox label input[type="checkbox"]+.cr>.cr-icon {
  opacity: 0;
}

.checkbox label input[type="checkbox"]:checked+.cr>.cr-icon {
  opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled+.cr {
  opacity: .5;
}

</style>
 {{-- <link rel="stylesheet" href="{{asset('mC/css/buttons.dataTables.min.css')}}">  --}}

<section id="base-style">
   	<div class="card-content">
		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Venta de Sucursal</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
                		<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body rounded-bottom rounded-lg">	
					<ul class="nav nav-tabs" id="myTabs" role="tablist">
					  <li class="active">
					  	<a id="tabSuc" data-toggle="tab" href="#home">Reporte Sucursal</a>
					  </li>
					  <li>
					  	<a id="tabVend" data-toggle="tab" href="#menu1">Reporte Vendedores</a>
					  </li>
					</ul>
   					<div class="tab-content">
   						<section id="home" class="tab-pane fade in active content"> 
  							<form id="frmSucConsulta" style="margin-top: 2%;">
						            <div class="box-header">
						             <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Reporte Venta Sucursales</h1>
						            </div>  
		           					<div class="box-body">
										<div class="col-md-3">
									        <div class="form-group">
									            <label>Sucursales</label>
												<select class="form-control select2" name="idSucursal"  id="idSucursal" style="width: 100%;">
													<option value="">Todos</option>
													@foreach ($sucursales as $suc)
														<option value="{{$suc->id}}">{{$suc->nombre}}</option>
													@endforeach
												
												</select>
									        </div>
						            	</div>
										<div class="col-md-3">
											<div class="form-group">
											   <label>Periodo Fecha.</label>			
												   <div class="input-group">
												   <div class="input-group-addon" style="">
													   <i class="fa fa-calendar"></i>
												   </div>
												   <input type="text"  value="Mes anterior / Mes actual" class = "form-control input-sm" disabled/>
											   </div>
										   </div>	
									   </div>  
										<div class="col-md-12" style="margin-top: 20px;">
											<button type="button" onclick="cleanSuc()" id="btnLimpiar" class="btn btn-default btn-lg pull-right" style="margin: 0 5px;"><b>Limpiar</b></button>
				    						<button type="button" id="botonExcelSuc" class="pull-right text-center btn btn-success btn-lg" style="margin: 0 0 10px 10px;"><b>Excel</b></button>
											<a  onclick ="buscarSuc()" class="pull-right btn btn-info btn-lg" style="margin: 0 5px;" role="button"><b>Buscar</b></a>
											
										</div>
								 	</div>  
				           		 <!-- /.box -->
      							<input type="hidden"  id="periodoHiden" name="periodo" />
	    					</form> 
			            	<div class="table-responsive table-bordered">
				              <table id="listadoSuc" class="table" style="width: 100%;">
				                <thead>
								  <tr>
								  	<th>Sucursal</th>
								  	<th>Fecha</th>
								  	<th>Venta Proforma</th>
									<th>Neto Proforma</th>
									<th>Renta Proforma</th>Venta
									<th>Venta Factura</th>
									<th>Neto Factura</th>
									<th>Renta Factura</th>
									<th></th>

					              </tr>
				                </thead>
				               

						        <tbody  style="text-align: center">
				 					            
						         
						        </tbody>
				              </table>
				            </div>  
   						</section>
   						<section id="menu1" class="tab-pane fade  content"> 
   							<form id="frmVendedorConsulta" style="margin-top: 2%;">
						            <div class="box-header">
							             <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Reporte Venta Vendedores</h1>
						            </div>  
	            						   
				 					<div class="col-md-3" style="">
								        <div class="form-group">
								            <label>Sucursales</label>
											<select class="form-control select2" name="idSucursal"  id="idSucursalVend" style="width: 100%;">
												<option value="">Todos</option>
												@foreach ($sucursales as $suc)
													<option value="{{$suc->id}}">{{$suc->nombre}}</option>
												@endforeach
											
											</select>
								        </div>
						            </div>

						            <div class="col-md-3" style="">
								        <div class="form-group">
								            <label>Vendedor</label>
											<select class="form-control select2" name="idVendedor"  id="idVendedorVend" style="width: 100%;">
												<option value="">Todos</option>
												@foreach ($vendedor as $vend)
													<option value="{{$vend->id}}">{{$vend->nombre}} {{$vend->apellido}} </option>
												@endforeach
											
											</select>
								        </div>
									</div>
									<div class="col-md-3" style="padding-left: 25px;">
										<div class="form-group">
										   <label>Periodo Fecha.</label>			
											   <div class="input-group">
											   <div class="input-group-addon" style="">
												   <i class="fa fa-calendar"></i>
											   </div>
											   <input type="text"  value="Mes actual" class = "form-control input-sm" disabled style="height: 36px;"/>
										   </div>
									   </div>	
								   </div> 
									<div class="col-md-12" style="margin-top: 20px;">
										 <button type="button" onclick="cleanVend()" id="btnLimpiar" class="btn btn-default btn-lg pull-right" style="margin: 0 5px;"><b>Limpiar</b></button>
								   			<button type="button" id="botonExcelVend" class="pull-right text-center btn btn-info btn-lg" style="background: #e2076a !important; margin: 0 0 10px 10px;"><b>Exportar a Excel</b></button>
										<a  onclick ="buscarVend()" class="pull-right btn btn-info btn-lg" style="margin: 0 5px;" role="button"><b>Buscar</b></a>
									
									</div>
				          	</div>
				            <!-- /.box -->
	   					 </form>  	
			            	<div class="table-responsive table-bordered">
				              <table id="listadoVend" class="table" style="width: 100%;">
				                <thead>
								  <tr>
								  	<th>Nombre</th>
								  	<th>Venta Proforma</th>
									<th>Neto Proforma</th>
									<th>Renta Proforma</th>
									<th>Venta Factura</th>
									<th>Neto Factura</th>
									<th>Renta Factura</th>

					              </tr>
				                </thead>
				               

						        <tbody  style="text-align: center">
				 					            
						         
						        </tbody>
				              </table>
				            </div>  
  						</section>	
   					</div>	
		        </div>
		    </div>  
		</div>  
	</div>	
</section>	    	


      <div id="requestSucExcel" class="modal fade" role="dialog">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Seleccionar Columnas <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_cambio" style="font-weight: bold; height: 18px;"></span>
					</div>
				  	<div class="modal-body">
				  

						
		<form id="columSucExcel" class="col-xs-12">

			<!-- Checked checkbox -->
			<div class="checkbox disabled checkSuc">
			  <label>
			   <input  type="checkbox" name="0" value="0" checked disabled>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			   Sucursal
			   </label>
			</div>

			<!-- Default checkbox -->
			<div class="checkbox disabled checkSuc">
			  <label>
			   <input  type="checkbox" name="1" value="1" checked disabled>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			   Fecha
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkSuc">
			  <label>
			   <input class="ckSuc" type="checkbox" name="2" value="2" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			   Venta Proforma
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkSuc">
			  <label>
			   <input class="ckSuc" type="checkbox" name="3" value="3" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			   Neto Proforma
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkSuc">
			  <label>
			   <input class="ckSuc" type="checkbox" name="4" value="4" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			   Renta Proforma
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkSuc">
			  <label>
			   <input class="ckSuc" type="checkbox" name="5" value="5" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			  Venta Factura
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkSuc">
			  <label>
			   <input class="ckSuc" type="checkbox" name="6" value="6" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			  Neto Factura
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkSuc">
			  <label>
			   <input class="ckSuc" type="checkbox" name="7" value="7" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			  Renta Factura
			   </label>
			</div>

						
							          		
				</form>
				          		
							


								
					<div class="col-xs-12">
						<div  data-dismiss="modal" id="buttonsSuc" ></div>
					</div>
					


				  </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>



      <div id="requestVendExcel" class="modal fade" role="dialog">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Seleccionar Columnas <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_cambio" style="font-weight: bold; height: 18px;"></span>
					</div>
				  	<div class="modal-body">
				  

						
		<form id="columVendExcel" class="col-xs-12">

			<!-- Checked checkbox -->
			<div class="checkbox disabled">
			  <label>
			   <input type="checkbox" name="0" value="0" checked disabled>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			   Nombre
			   </label>
			</div>

			<!-- Default checkbox -->
			<div class="checkbox checkVend">
			  <label>
			   <input class="ckVend" type="checkbox" name="1" value="1" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			   Venta Proforma
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkVend">
			  <label>
			   <input class="ckVend" type="checkbox" name="2" value="2" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			   Neto Proforma
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkVend">
			  <label>
			   <input class="ckVend" type="checkbox" name="3" value="3" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			   Renta Proforma
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkVend">
			  <label>
			   <input class="ckVend" type="checkbox" name="4" value="4" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			  Venta Factura
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkVend">
			  <label>
			   <input class="ckVend" type="checkbox" name="5" value="5" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			  Neto Factura
			   </label>
			</div>
			<!-- Default checkbox -->
			<div class="checkbox checkVend">
			  <label>
			   <input class="ckVend" type="checkbox" name="6" value="6" checked>
			   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
			  Renta Factura
			   </label>
			</div>
		</form>
			
					<div class="col-xs-12">
						<div data-dismiss="modal"  id="buttonsVend" ></div>
					</div>
					


				  </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/js/dataTables.buttons.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jszip.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/buttons.html5.min.js')}}"></script> --}}
	<script>

	var tableSuc;
	var tableVend;

	$(function() {


	$('.select2').select2();
	  tableSuc = buscarSuc();
	 tableVend = buscarVend();

	 	var Fecha = new Date();
	var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

	$('#periodo').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													     cancelLabel: 'Limpiar'
													    },
												startDate: "01/"+fechaInicial,
        										endDate: new Date(),
								    			});

		$('#periodo').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });




	//CAMPO INPUT HIDDEN	
	$('#periodoHiden').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													     cancelLabel: 'Limpiar'
													    },
												startDate: "01/"+fechaInicial,
        										endDate: new Date(),
								    			});







	});





	/**
	 * REDIRECCION DE PESTAÑA
	 * @param  ID_SUCURSAL
	 */
	function loadReportSale(id_suc){
		$('#idSucursalVend').val(id_suc).trigger('change.select2');
		$('#idVendedorVend').val('').trigger('change.select2');
		$('#tabVend').tab('show');
		buscarVend();

	}

	/*
	Para formatear numeros a moneda USD PY
	 */
	const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD',
					  minimumFractionDigits: 2
					});
	


	



	{{--====================================== 
			SECCION SUCURSAL
	===========================================  --}}
	


	function buscarSuc(){


	
					
			 tableSuc =   $('#listadoSuc').DataTable({
					        "destroy": true,
					        "select": true,
					        "ajax": {
					        		"data": { "formSearch": $('#frmSucConsulta').serializeArray() },
						            "url": "{{route('ajaxReporteSucursal')}}",
						            "type": "GET",
						            	 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        }			
						        },"columns":[
						        			 {"data": "nombre"},
						        			 {"data": "fecha"},
						        			 {"data": function(x){ 
						        			 	console.log(x.total_venta_proforma);
						        			 	return formatter.format(parseFloat(x.total_venta_proforma));}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.total_neto_proforma))}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.total_renta_proforma))}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.total_venta_factura))}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.total_neto_factura))}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.total_renta_factura))}},
						        			 {"data": function(x){
						        			 	var btn = `<a onclick="loadReportSale(${x.id_sucursal_empresa})" class="btn btn-danger" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-search"></i></a>`;
						        			 	return btn;
						        			 }}	]
								           
						               
					    });

			 /*Se define las tablas que se van a mostrar en un principio y 
			 se envia el objeto table para cargar las configuraciones.*/
			  buttonSuc([0,1,2,3,4,5,6,7],tableSuc);  
			  $(".ckSuc").prop('checked', true); 

			  return tableSuc;
}
				
				

				function buttonSuc(column,tableSuc){
					var numFinal = 0;
					$('#buttonsSuc').html('');
						var buttons = new $.fn.dataTable.Buttons(tableSuc, { 
							    buttons: [{	
							    						title: 'Reporte Facturación Sucursal',
										                extend: 'excelHtml5',
										                 text: '<b>Excel</b>',
										                 className: 'pull-right text-center btn btn-success btn-lg',
								                exportOptions: {
								                    columns: ':visible'
								                },
								                exportOptions: {
									                    columns:  column,
									                    format: {
									                    		//seleccionamos las columnas para dar formato para el excel
												                body: function ( data, row, column, node ) {

												                	if(column != 0 &&  column != 1){
																		/*
												                		if(data != '0')
																		data = data.replace(".","");
																		//cambiamos la coma por un punto
																		if(data != '0')
																		data = data.replace(",",".");
																		//listo
																		
																		var numFinal = parseFloat(data);*/

																	numFinal = 0;
																	numFinal = clean_num(data);
												                    return  numFinal;
												                		} 
												                			return data;
												                		
												                }
												            }
									                },
									                //Generamos un nombre con fecha actual
									                filename: function() {
										               var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
										               // var selected_machine_name = $("#output_select_machine select option:selected").text()
										               return 'ReporteFacturacionSucursal-'+date_edition;
										           }

								            }]
							}).container().appendTo('#buttonsSuc'); 
				}





				function cleanSuc(){
		$('input[name="periodo"]').val('');
		$('#idSucursal').val('').trigger('change.select2');
		buscarSuc();
		
			
		
	}


		$('.checkSuc').on('click',function(){

		var form = $('#columSucExcel').serializeArray();
		valor = [];
		valor.push(0,1);
		$.each(form, function(item,value){
			valor.push(parseInt(value.value));
		});
		// colSuc.asignarColumnas(valor);
		// $('#buttonsSuc').html('');
		buttonSuc(valor,tableSuc);
	
	});



		$('#botonExcelSuc').on('click',function(){
			$("#requestSucExcel").modal("show");
		});





	{{--====================================== 
			SECCION VENDEDORES
	===========================================  --}}

	function buscarVend(){



					
			 tableVend =   $('#listadoVend').DataTable({
					        "destroy": true,
					        "select": true,
					        "ajax": {
					        		"data": { "formSearch": $('#frmVendedorConsulta').serializeArray() },
						            "url": "{{route('ajaxReporteVenta')}}",
						            "type": "GET",
						            	 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        }
						        },"columns":[{"data": function(x){ 
						        	var n = x.nombre;
						        	if(x.apellido != null){
						        		n +=' '+x.apellido
						        	}
						        	return n;
						        	}
						        },
						        			 {"data": function(x){ return formatter.format(parseFloat(x.total_venta_proforma))}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.neto_proforma))}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.renta_proforma))}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.total_venta_factura))}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.neto_factura))}},
						        			 {"data": function(x){ return formatter.format(parseFloat(x.renta_factura))}}
						        				]			
						                    
					    });

			buttonVend([ 0,1,2,3,4,5,6 ],tableVend);
			 $(".ckVend").prop('checked', true); 
			return tableVend;

}


	function buttonVend(column, tableVend){
		var numFinal = 0;

					//Eliminar boton en cada carga para evitar repeticion
					$('#buttonsVend').html('');

					var buttons = new $.fn.dataTable.Buttons(tableVend, { 
							    buttons: [{		title: 'Reporte Facturación Vendedores',
								                extend: 'excelHtml5',
								                 text: 'Exportar a Excel',className: 'btnExcel',
								                exportOptions: {
								                    columns: ':visible'
								                },
								                exportOptions: {
									                    columns: column,
									                    format: {
												                body: function ( data, row, column, node ) {

												                	if(column != 0){
																		/*
																		data = data.replace(".","");
																		//cambiamos la coma por un punto
																		data = data.replace(",",".");
																		//listo
																		var numFinal = parseFloat(data);

												                	// console.log(numFinal);
												                    // Strip $ from salary column to make it numeric
												                    */
																	numFinal = 0;
																	numFinal = clean_num(data);
												                    return  numFinal;
												                		} 
												                			return data;
												                		
												                }
												            }
									                },
									                 filename: function() {
										               var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
										               // var selected_machine_name = $("#output_select_machine select option:selected").text()
										               return 'ReporteFacturacionVendedores-'+date_edition;
										           }

								            }]
							}).container().appendTo('#buttonsVend'); 

	}
	



	function cleanVend(){
		$('#idSucursalVend').val('').trigger('change.select2');
		$('#idVendedorVend').val('').trigger('change.select2');
		buscarVend();

	}


	$('.checkVend').on('click',function(){

		var form = $('#columVendExcel').serializeArray();
		valor = [];
		valor.push(0,1);
		$.each(form, function(item,value){
			valor.push(parseInt(value.value));
		});
		// colSuc.asignarColumnas(valor);
		// $('#buttonsSuc').html('');
		buttonVend(valor,tableVend);
	
	});


	$('#botonExcelVend').on('click',function(){
			$("#requestVendExcel").modal("show");
	});


			/*{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
			NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}*/
			function clean_num(n,bd=false){

				if(n && bd == false){ 
				n = n.replace(/[,.]/g,function (m) {  
										if(m === '.'){
											return '';
										} 
										if(m === ','){
											return '.';
										} 
								});
				return Number(n);
				}
				if(bd){
				return Number(n);
				}
				return 0;

				}//
		

				
			

	</script>
@endsection