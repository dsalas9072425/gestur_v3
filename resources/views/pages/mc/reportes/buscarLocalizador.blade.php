@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
	        <div class="col-xs-12">
	          <div class="box">
		          <br>
		          <br>
	            <div class="box-header">
	              <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Reporte Extracto de Proveedores</h1>
	            </div> 
	            @include('flash::message')     
				<div class="row">
					<div class="col-xs-12 col-sm-1 col-md-1">
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2">
						<label class="control-label">Localizador(es):</label>
					</div>	
					<div class="col-xs-12 col-sm-7 col-md-7">
					<input type="text" placeholder="Localizadores" class="form-control" id="localizador" name="localizador" style="margin-bottom: 5px;">
					<b>Para mas de un localizador separarlos por comas (,) y sin espacios</b>
					</div>
					<div class="col-xs-12 col-sm-1 col-md-1">	
						<button type="button" style="width: 130px;  background-color: #3c8dbc; height: 40px;" id= "botonExcel" class="btn btn-primary"><i class="glyphicon glyphicon glyphicon-book"></i>Buscar</button>
					</div>
				</div>	            <!-- /.box-header -->
	            <div class="box-body">
	            	<div class="table-responsive">
						<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="localizacion">
				            <thead>
				                <tr>
				                    <th>Localizador</th>
				                    <th>Monto</th>
				                    <th>Moneda</th>
				                    <th>Proforma</th>
				                    <th>Estado</th>
				                    <th>Factura</th>
				                    <th>Vendedor</th>
				                    <th>Pagado</th>
				                </tr>
				            </thead>
				            <tbody>
				            </tbody>
			            </table>	            
			        </div>  
	            </div>
	            <!-- /.box-body -->
	          </div>
	          <!-- /.box -->
	          <!-- /.box -->
	        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$('#listado').dataTable({
								  "pageLength": 100
								} );

		$("#localizador").keypress(function(e) {
	        //no recuerdo la fuente pero lo recomiendan para
	        //mayor compatibilidad entre navegadores.
	        var code = (e.keyCode ? e.keyCode : e.which);
	        if(code==13){
	           $("#botonExcel").trigger('click');
	        }
    	});

		$("#botonExcel").on("click", function(e){
			idLocalizador = $('#localizador').val();
			$.blockUI({
		            centerY: 0,
		            message: "<h2>Procesando...</h2>",
		                css: {
		                    color: '#000'
		                    }
		                });
			$.ajax({
					type: "GET",
					url: "{{route('mc.getBuscarLocalizadores')}}",
					dataType: 'json',
					data: {
						dataAgencia: idLocalizador
			       			},
					success: function(rsp){
								$.unblockUI();
								console.log(rsp);
								var oSettings = $('#localizacion').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();
								for (i=0;i<=iTotalRecords;i++) {
									$('#localizacion').dataTable().fnDeleteRow(0,null,true);
								}

								$.each(rsp.localizadoresFactour, function (key, item){
										 inicioMoneda = parseInt(item.localizadorMoneda);
								         switch(inicioMoneda) {
			                                    case 1:
			                                        moneda = 'USD';
			                                    break;
			                                    case 2:
			                                        moneda = 'PYG';
			                                    break;
			                                    case 3:
			                                        moneda = 'EUR';
			                                    break;
			                                    case 4:
			                                        moneda = 'BRL';
			                                    break;
											}
											console.log(moneda);
											switch (item.proformaEstado) {
								                    case 'A':
								                        estado = 'ABIERTA';
								                        break;
								                    case 'F':
								                        estado = 'FACTURADA';
								                        break;
								                    case 'B':
								                        estado = 'BOQUEADA';
								                        break;
								                    case 'N':
								                        estado = 'ANULADA';
								                        break;
								                    case 'P':
								                        estado = 'PROCESO';
								                        break;
								                    case 'O':
								                        estado = 'OPERACIONES';
								                        break;
	    							    		} 	

									    var dataTableRow = [
															'<b>'+item.localizadorFactour+'</b>',
															'<b>'+item.localizadorPrecio+'</b>',
															'<b>'+moneda+'</b>',
															'<b>'+item.proformaId+'</b>',
															'<b>'+estado+'</b>',
															'<b>'+item.proformaNroFactura+'</b>',
															'<b>'+item.proformaVendedor+'</b>',
															'<b>'+item.pagado+'</b>'
															];									
									var newrow = $('#localizacion').dataTable().fnAddData(dataTableRow);
									var nTr = $('#localizacion').dataTable().fnSettings().aoData[newrow[0]].nTr;
							})
 					}
 			});		
		});


		function round(value, exp) {
		  if (typeof exp === 'undefined' || +exp === 0)
		    return Math.round(value);

		  value = +value;
		  exp = +exp;

		  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
		    return NaN;

		  // Shift
		  value = value.toString().split('e');
		  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

		  // Shift back
		  value = value.toString().split('e');
		  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
		}
		
	</script>
@endsection