
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		textarea#comentario { width: 100% }
	</style>
@endsection
@section('content')
 

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Listado de Pagos de Agencias</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
            	<form id="filtroProforma">
					<div class="row">

	            <div class="col-12 col-sm-3 col-md-3" style="height: 74px;">
			        <div class="form-group">
			            <label>Agencia</label>
						<select class="form-control select2" name="agencia_id"  id="agencia_id" tabindex="1" style="width: 100%;" >
							<option value="">Todos</option>
							@foreach($agencias as $key=>$agencia)
								<option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
							@endforeach
						</select>
			        </div>
	            </div>
				
	            <div class="col-12 col-sm-3 col-md-3" style="height: 74px;">
			        <div class="form-group">
			            <label>Moneda</label>
						<select class="form-control input-sm select2" name="id_moneda" id="moneda" style=" width: 100%;"  tabindex="7" >
							<option value="">Todos</option>
								@foreach($monedas as $key=>$moneda)
									<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
								@endforeach
							</select>
			        </div>
	            </div>
	    

	            <div class="col-12 col-sm-3 col-md-3" style="height: 74px;">
			        <div class="form-group">
			            <label>Estado</label>
			            <select class="form-control select2" name="estado_id" id="estado_id" style="width: 100%;" tabindex="9">
							<option value="">Todos</option>
							@foreach($estadoPago as $key=>$estado)
								<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
							@endforeach
			            </select>
			        </div>
				</div>
				
			</div>	
		</form>


			<div class="row" >
				<div class="col-12">
					<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right mr-1"><b>Limpiar</b></button>
				 	<button type="button" id= 'buscando' class="btn btn-info pull-right btn-lg mr-1"  role="button" onclick="consultaPagosServerSide()"><b>Buscar</b></button>
				</div>
			</div>
			

        
            	<div class="table-responsive">
	              <table id="listado" class="table" style="width: 100%;">
	                <thead>
						<tr style="text-align: center;">
							<th>Fecha</th>
							<th>Comprobante</th>
					        <th>Agencia</th>
					        <th>Importe</th>
					        <th>Estado</th>
					        <th>Usuario</th>
					        <th>Tipo Pago</th>
					        <th>Moneda</th>
					        <th>Recibo</th>
					        <th style="width: 120px;"></th>
			            </tr>
	                </thead>

	                <tbody style="text-align: center;">
	                </tbody>
	              </table>
				</div>  
				

			</div>
		</div>
	</section>
</section>

			
           
    
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content" style="margin-top: 25%;">
					<div class="modal-header">
						{{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
						<h4 class="modal-title">Comentarios de la Agencia</h4>
					</div>

					<div class="modal-body">
						<div class="content-fluid">
							<div class="row">
								<div class="col-12">
								<div readonly="readonly" id="formAcuerdoEspecial">
									<p></p>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>




    <div id="myModales" class="modal fade" role="dialog">
		  <div class="modal-dialog" >

		    <!-- Modal content-->
		    <div class="modal-content" style="margin-top: 25%;">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Aplicar Pago</h4>
		      </div>
		      	<div class="modal-body">
				        <input type="hidden"  class = "Requerido form-control" id="id" name="id"/>
				        <input type="hidden"  class = "Requerido form-control" id="tipo" name="tipo"/>
				    <div class ="row">    
				    	<div class="col-2">	
				    		Adjunto:
				    	</div>
				    	<div class="col-10">	
					    	<form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{ url('uploadDTPlus') }}" autocomplete="off">
			                    <div class="form-group">
			                        <div class="row">
			                            <div id="validation-errors"></div>
			                            <div class="col-12 col-sm-10 col-md-10" style="padding-left: 0px;padding-right: 0px;">
			                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
			                                <input type="hidden" name="marcador" value="1" />
			                                <input type="file" class="form-control control" name="image" id="image" style="margin-left: 3%; width: 96%"/>  <h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 10px;"><b>Formatos válidos: PNG, JPG, PDF, DOC</b></h4>
			                            </div> 
			                            <div class="col-12 col-sm-1 col-md-1" style="padding-left: 0px;padding-right: 0px;height: 10px;">
			                                <div id="check"></div>
			                            </div>
			                            <div class="col-12 col-sm-1 col-md-1">
			                                <div id="btn-delete-documento"></div>
			                            </div>
			                        </div>  
			                    </div>  
	                    	</form>
	                    	<input type="hidden" name="imagenRecibo" id="imagenRecibo" value="" />
				    	</div>	
				    </div>	
				    <div class ="row">    
				    	<div class="col-12 col-sm-2 col-md-2">	
				    		Nro Recibo:
				    	</div>
				    	<div class="col-12 col-sm-4 col-md-4">
							<select class="form-control select2" name="sucursal_id" style="width: 100%;"  id="sucursal_id" tabindex="1" style="margin-left: 10px;" >
								@foreach($sucursales as $key=>$sucursal)
									<option value="{{$sucursal->id}}">{{$sucursal->nombre}}</option>
								@endforeach
							</select>		
				    	</div>	
				    	<div class="col-12 col-sm-5 col-md-5" style="padding-left: 0px;margin-right: 10px;padding-right: 35px;">
				    		<input type="number" name="numero" id="numero" class="form-control"  value="" />
				    	</div>	
				    </div>		
        		</div><!-- /.box-body -->
		        <div class="box-footer">
		          <div class="input-group">
		             <button type="button" style="margin-left: 350px;" class="btn btn-danger btn-flat" onclick="guardarAplicar()"  id="btnChat">Aplicar Pago</button>
					 <button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px;background-color: #969dac;margin-left: 15px;" data-dismiss="modal">Cerrar</button>
		            </span>

		          </div>
		          <span id="errorChat" style="color:red;"><h6></h6></span>
		        </div><!-- /.box-footer-->
		      </div><!--/.direct-chat -->

		      </div>
	</div>

	<div id="modalAdjunto" class="modal fade" role="dialog">
	  	<div class="modal-dialog" style="width: 60%;margin-top: 15%; margin-left: 35%">
		<!-- Modal content-->
			<div class="modal-content" style="width: 550px;">
				<div id="modal-header" class="modal-header">
					<h2 class="modal-title titlepage" style="font-size: x-large;">Ver Adjuntos</h2>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
	                <!-- Post -->
	                <div class="post" style="margin-left: 4%;margin-right: 4%;">
	                  <!-- /.user-block -->
	                  	<div class="row margin-bottom">
	                  		<div id='output'>
	                  			
			                </div>    
						</div>
	                    <div class="row">
		                    <div class="col-12 col-sm-10 col-md-10">
							</div>
		                    <div class="col-12 col-sm-2 col-md-2">
								<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
							</div>
						</div>								

	                	</div>
	                <!-- /.post -->
					</div>
				</div>	
			</div>	
		</div>	

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

	<script>
		// $('.textarea').wysihtml5();

		// $('#formAcuerdoEspecial').wysihtml5({
		//         toolbar: {
		//             "font-styles": true, // Font styling, e.g. h1, h2, etc.
		//             "emphasis": true, // Italics, bold, etc.
		//             "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
		//             "html": false, // Button which allows you to edit the generated HTML.
		//             "link": true, // Button to insert a link.
		//             "image": false, // Button to insert an image.
		//             "color": false, // Button to change color of font
		//             "blockquote": true, // Blockquote
		//             "size": 'sm' // options are xs, sm, lg
		//         }
		//     });


		$('#formAcuerdoEspecial').summernote();
		$('.textarea').summernote();


			var Fecha = new Date();
			var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

			$( ".fecha" ).datepicker({ 
									    altFormat: 'dd/mm/yy',
									    dateFormat: 'yy-mm-dd'
										});

			$('input[name="periodo"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    }
									    			});
			$('input[name="periodo"]').val('');
			$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });
			$('.select2').select2();

			$('#estado_id').val(31).trigger('change.select2');

			consultaPagosServerSide();



        $(document).ready(function(){
              var options = { 
                            beforeSubmit:  showRequest,
                    success: showResponse,

                dataType: 'json' 
                    }; 
              $('body').delegate('#image','change', function(){
                $('#upload').ajaxForm(options).submit();      
              });

             /* $('body').delegate('#images','change', function(){
                $('#uploads').ajaxForm(options).submit();      
              });*/
        })

       $(".img-responsive").click(function(){
            var id = $(this).attr('id');
            $("#id").val(id);
            $("#imagen").val("");
            $("#check").html("");
            $("#btn-delete").html("");

       })

    function showRequest(formData, jqForm, options) { 
        $("#validation-errors").hide().empty();
        //$("#output").css('display','none');
        return true; 
      } 
    
    function showResponse(response, statusText, xhr, $form)  { 
        if(response.success == false){
            var arr = response.errors;
            $.each(arr, function(index, value){
                if (value.length != 0)
                  {
                    $("#validation-errorswarnigPagos").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                  }
            });
            $("#validation-errors").show();
        } else {

            $("#imagenRecibo").val(response.archivo); 
            if(response.tipo == 1){
                $("#check").html("<a href='"+response.file+"' target='_blank'><img class='img-responsive'/><i class='fa fa-check' aria-hidden='true'></i></a>");
                $("#btn-delete-documento").html("<button type='button' id='btn-imagen' data-id='"+response.archivo+"'><i class='glyphicon glyphicon-remove'></i></button>");
                $("#imagenDocumento").val('pagosDocumento/'+response.archivo); 
                eliminarImagen(response.indice);
     
            }                
            $("#btnAceptarCanje").css('display','block');
        }
    }


///////////////////////////////////////////////////////////////////////////////////
       function consultaPagosServerSide() {

       	var btn;
       	var pasajero;
       	var avion;

       	$.blockUI({
				    centerY: 0,
				    message: "<h2>Procesando...</h2>",
				    css: {
				          color: '#000'
				         }
				 });

       		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		
		  	var dataStrig = $('#filtroProforma').serializeArray();

  				$.ajax({
                        type: "GET",
                        url: "{{route('consultaPagosServerSide')}}",
                        dataType: 'json',
                        data: dataStrig,

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
                        success: function(rsp){

                                    		// console.log("response");
							console.log(rsp);

							 $.unblockUI();


							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

							$.each(rsp, function (key, item){
									var iconocancelar ='<a data-toggle="modal" id="'+item.id+'" onclick="tomarComentario('+item.id+', `'+item.tipo+'`)" href="#requestModal" class="btn btn-danger" role="button" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px;" ><i class="fa fa-comment"></i></a>';

									if(item.imagen != ''){
										  iconocancelar +='<a onclick="verVoucher('+item.id+',`'+item.tipo+'`)" data-toggle="modal" data-target="#modalAdjunto" class="btn btn-danger" role="button" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px; margin-left: 5px;" role="button"><i class="fa fa-files-o fa-lg"></i></a>';
									}

									if(item.imagenRecibo != ''){	
										   iconocancelar += '<a href="../uploads/'+item.imagenRecibo+'" class="btn btn-danger" role="button" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px;margin-left: 5px;" role="button" target="_blank"><i class="fa fa-file-text-o fa-lg"></i></a>';
									}	

									if(item.estado_id == 31){
										iconocancelar += '<a data-toggle="modal" id="'+item.id+'" onclick="aplicar('+item.id+', `'+item.tipo+'`)" href="#requestModal" class="btn btn-danger" role="button" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px; margin-left: 5px;"><i class="fa fa-check"></i></a>';
									}	
									
									console.log(iconocancelar);
									var dataTableRow = [
														item.fecha,
														item.nro_comprobante,
														item.agencia,
														item.monto,
														item.estado,
														item.usuario,
														item.tipo,
														item.moneda,
														item.recibo,	
														iconocancelar
														];

									var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
									var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
									$(nTr).attr('id',item.id);
									$('td', nTr).addClass('text-center');
									$('td', nTr).addClass('negrita');
							})
                        }
                });
	
		}
///////////////////////////////////////////////////////////////////////////////////
			
			function  aplicar(idComprobante, tipo){

				$('#id').val(idComprobante);
				$('#tipo').val(tipo);

				$('#myModales').modal('show');

			}
	
			function tomarComentario(idComprobante, tipo){

				console.log(idComprobante);
				console.log(tipo);
  				$.ajax({
                        type: "GET",
                        url: "{{route('comentarioPago')}}?idPago="+idComprobante+"&tipo="+tipo,
						data: idComprobante,
                        dataType: 'json',
                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
                        success: function(rsp){
                        	console.log(rsp);
                        	// $('#mensajeChat').val(rsp.mensaje);
							$('#formAcuerdoEspecial ~ iframe').contents().find('.note-editable').attr('contenteditable','false');  
							$('#formAcuerdoEspecial').summernote('code',rsp.mensaje);                      
							$('#myModal').modal('show');

                        }
                    })    
                        

			}

			function guardarAplicar(){
			

                   idComprobante =  $('#id').val();
                   tipo = $('#tipo').val();	
                   var dataString = 'id='+idComprobante;  
	               dataString += '&tipo='+ tipo; 
	               dataString += '&imagen='+ $('#imagenRecibo').val(); 
	               dataString += '&sucursal_id='+ $('#sucursal_id').val(); 
	               dataString += '&numero='+ $('#numero').val(); 
                   if($('#imagenRecibo').val() != "" ){ 
                   		$('#imagenRecibo').css('border-color', '#d2d6de');
                   		if($('#numero').val() != "" ){ 	
                   			$('#numero').css('border-color', '#d2d6de');
			                $.ajax({
									type: "GET",
									url: "{{route('getConfirmarPago')}}",
									dataType: 'json',
									data: dataString,
									success: function(rsp){
													console.log(rsp);
													$.toast({
															heading: 'Exito',
															text: rsp.mensaje,
															position: 'top-right',
															showHideTransition: 'slide',
														    icon: 'success'
															});  

													$('#myModales').modal('hide');
                 									$('#buscando').trigger('click');
											}
							})
		               }else{ 
		               		$('#numero').css('border-color', 'red');
	 						$.toast({
	                            heading: 'Error',
	                            text: 'Ingrese el numero de recibo.',
	                            position: 'top-right',
	                            showHideTransition: 'fade',
	                            icon: 'error'
	                        });
		               }    
	               }else{
	               		$('#imagenRecibo').css('border-color', 'red');
 						$.toast({
                            heading: 'Error',
                            text: 'Ingrese la imagen del recibo.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
	               }    

			}

		function verVoucher(proforma, tipo){

			$.ajax({
					type: "GET",
					url: "{{route('getAdjuntosVouchers')}}",
					dataType: 'json',
					data: {
							dataProforma: proforma,
							dataTipo: tipo,

			       			},
					success: function(rsp){
							console.log(rsp.length);
							$("#output").html('');
							console.log(rsp);
									console.log(rsp[0].imagen_documentos);
											console.log(rsp[0].imagen_documentos);
											var indice  = rsp[0].imagen_documentos;
											extension = indice.split('.');

						                if(extension[1] != 'pdf' && extension[1] != 'doc'){
						                 	$("#output").append('<div ><div class="col-sm-12" ><a href="http://192.168.0.75/dtpmundo/public/'+rsp[0].imagen_documentos+'" target="_blank"><img class="img-responsive" style="width:80px; height:120px;" src="http://192.168.0.75/dtpmundo/public/'+rsp[0].imagen_documentos+'" alt="Photo"></a><label>Transferencia o Deposito</label></div><div class="col-12 col-sm-1 col-md-1"></div></div>');

						                 }
					}
				})		
		}

	</script>
@endsection