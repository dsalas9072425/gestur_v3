
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">


	.ui-datepicker-calendar {
    display: none;
    }

	input.form-control:focus ,.select2-container--focus , button:focus{
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }


	.fonticon-wrap {
    float: right;
    width: 3px;
    height: 5px;
    line-height: 4.8rem;
    text-align: center;
    border-radius: 0.1875rem;
    margin-right: 1rem;
    margin-bottom: 1.5rem;
}

</style>


	@parent
@endsection
@section('content')


<section id="base-style">

	<div class="card" style="border-radius: 14px;">
		<div class="card-header" style="border-radius: 14px;">
			<h4 class="card-title">Reporte Asiento Contable</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


				<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="tabAsiento" data-toggle="tab" aria-controls="tabIcon21"
							href="#home" role="tab" aria-selected="true">
							Asiento</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tabDetalle" data-toggle="tab" aria-controls="tabIcon22" href="#menu1"
							role="tab" aria-selected="false">
							Asiento Detalle</a>
					</li>
				</ul>

				<div class="tab-content mt-1">
					<section id="home" class="tab-pane active" role="tabpanel">
						<form class="row" id="formAsiento">
							<div class="col-6 col-sm-3 col-md-3">
								<div class="form-group">
									<label>N° Asiento</label>
									<input type="text"  class="form-control" id="id_asiento" name="id_asiento">
								</div>
							</div>

							<div class="col-6 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Tipo</label>
									<select class="form-control select2" name="tipo_operacion" tabindex="3"
										style="width: 100%;" id="id_origen_asiento">
										<option value="">Todos</option>

										@foreach ($origenAsiento as $origen)
										<option value="{{$origen->id}}">{{$origen->abreviatura}}</option>
										@endforeach

									</select>
								</div>
							</div>

							<div class="col-12 col-sm-6 col-md-3">
								<div class="form-group">
									<label>Fecha</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
										<input type="text" autocomplete="false" name="periodo:p_date" tabindex="1"
											id="asiento_fecha" class="form-control date-picker" value=""
											autocomplete="off" />
									</div>
								</div>
							</div>

							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Usuario</label>
									<select class="form-control select2" name="id_usuario" id="id_usuario" tabindex="2"
										style="width: 100%;">
										<option value="">Todos</option>
										@foreach ($usuarios as $user)
										<option value="{{$user->id}}">{{$user->full_name}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Documento</label>
									<input type="text"  class="form-control" id="documento" name="documento">
								</div>
							</div>

							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Anulado</label>
									<select class="form-control select2" name="asiento_anulado" id="asiento_anulado" tabindex="2"
										style="width: 100%;">
										<option value="">Todos</option>
										<option value="1">SÍ</option>
										<option value="0" selected="selected">NO</option>
									</select>
								</div>
							</div>

							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Balanceado</label>
									<select class="form-control select2" name="balanceado" id="balanceado" tabindex="2"
										style="width: 100%;">
										<option value="" selected="selected">Todos</option>
										<option value="SI">SÍ</option>
										<option value="NO">NO</option>
									</select>
								</div>
							</div>

							<div class="col-12">
								<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
								<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
								<button onclick="tableAsiento()" class="btn btn-info btn-lg pull-right mr-1"  type="button"><b>Buscar</b></button>

							</div>
						</form>

							<div class="table-responsive table-bordered mt-1">
								<table id="listadoAsiento" class="table" style="width:100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Fecha</th>
											<th>Concepto</th>
											<th>Documento</th>
											<th>Operación</th>
											<th>Debe</th>
											<th>Haber</th>
											<th>Usuario</th>
											<th>Balanceado</th>
											<th>Anulado</th>
											<th></th>
										</tr>
									</thead>
									<tbody style="text-align: left; font-weight: 800;">
									</tbody>
									<tfoot>
                                        <tr>
                                            <th colspan="5" style="text-align: right;font-size: larger;font-size: small; color: #F46649;">TOTALES:</th>
                                            <th style="font-size: medium;"></th>
											<th style="font-size: medium;"></th>
                                            <th colspan="4"></th>
                                        </tr>
                                    </tfoot>
								</table>
							</div>
					</section>

					<section id="menu1" class="tab-pane" role="tabpanel">

								<form id="formAsientoDetalle">
									<input type="hidden" name="id_asiento" id="id_asiento_input" />
								

								<div class="row">
									<div class="col-3">
										<div class="form-group">
											<label>Asiento</label>
											<input type="text" class="form-control" maxlength="30" id="asiento" name="asiento" value="" readonly>
										</div>
									</div>

									<div class="col-3">
										<div class="form-group">
											<label>Documento</label>
											<input type="text" class="form-control" maxlength="30" id="documento_asiento" name="documento_asiento"
												value="" readonly>
										</div>
									</div>

									<div class="col-3">
								
										<div class="form-group">
											<label>Fecha Asiento</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text"><i class="fa fa-calendar"></i></span>
												</div>
												<input type="text" class="form-control" maxlength="30" id="fecha_asiento" name="fecha_asiento"
												value="" readonly>
											</div>
										</div>
									</div>

									<div class="col-3">

											<button onclick="agregarNuevoAsiento()" title="Agregar Nuevo Asiento" class="btn btn-success pull-right btnEditHidden"
												type="button" style="margin-right: 10px;margin-bottom: 10px; display:none;">
													<i class="ft-plus-circle"></i>
											</button>

											<button onclick="asientosBtn()" title="Ver Asientos Relacionados" class="btn btn-info pull-right btnVerAsientos"
											type="button" style="margin-right: 10px;margin-bottom: 10px;">
												Asientos Relacionados
										</button>
									
									</div>
								</div>

								<div class="row">
									<div class="col-12">
										<div class="table-responsive table-bordered">
											<table id="listadoAsientoDetalle" class="table" style="width:100%">
												<thead>
													<tr>
														<th>Cuenta</th>
														<th>Nombre</th>
														<th>Sucursal</th>
														<th>C.C</th>
														<th>Debe</th>
														<th>Haber</th>
														<th>Cotización</th>
														<th>Importe Original</th>
														<th></th>
													</tr>
												</thead>
												<tbody style="text-align: left; font-weight: 800;">
												</tbody>
												{{-- style="background-color:#E4F1FA;" --}}
												<tfoot>
													<tr>
														<th colspan="2" style="text-align:right;"> DIFERENCIA</th>
														<th id="diferencia_footer"></th>
														<th> TOTAL</th>
														<th id="total_debe"></th>
														<th id="total_haber"></th>
														<th colspan="3"></th>

													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								
									<div class="col-12">
										<div class="form-group">
											<label>CONCEPTO</label>
											<input type="text" autocomplete="false" id="concepto_datatable" class="form-control date-picker"
												value="" autocomplete="off" readonly />
										</div>
									</div>

									<div class="col-12">
										@foreach ($btn as $boton)
											<?php echo $boton; ?>
										@endforeach
											{{-- <button type="button" class="mr-1 btn btn-success btn-lg pull-right" tabindex="9"
											id="btnConfirmarAsiento" onclick="actualizarAsiento()"><b>Confirmar Asiento</b></button> --}}
									</div>
								</div>
							</form>
							</section>
				</div>
				<!--TAB CONTENT -->


			</div>
		</div>
	</div>
</section>

<div id="requestNuevoAsiento" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg" style="margin-left: 25%;margin-top: 10%;">
  <!-- Modal content-->
	  <div class="modal-content" style="width: 110%;">
	      <div class="modal-header">
	       
	        <h4 class="modal-title" id="" style="font-weight: 800;">
	        	Agregar Asiento Contable
	        </h4>
			
	      </div>
			<div class="modal-body">

			  <div class="table-responsive" style="overflow-x: hidden;">
			   
                <form class="row" id="formNuevoAsiento" method="post" autocomplete="off">			
						<div class="col-12">
							<div class="form-group">
								<label>Cuenta Contable(*)</label>
								<select class="form-control select2" name="plan_cuenta" data-value-type="number" 
									tabindex="1"  id="id_plan_cuenta" style="width: 100%;">
									<option value="0">Seleccione una opción</option>
									@foreach($plan_cuentas as $plan_cuenta)
										<option value="{{$plan_cuenta->id}}" data-nombre_cuenta="{{$plan_cuenta->descripcion}}" data-nro_cuenta="{{$plan_cuenta->cod_txt}}" data-id_moneda_cuenta="{{$plan_cuenta->id_moneda}}" data-tipo_cotizacion_cuenta="{{$plan_cuenta->tipo_cotizacion}}" data-currency_code="@if(isset($plan_cuenta->currency['currency_code'])){{$plan_cuenta->currency['currency_code']}}@endif">( {{$plan_cuenta->cod_txt}} ) {{$plan_cuenta->descripcion}}
										</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Sucursal (*)</label>
								<select class="form-control select2" name="id_sucursal_modal" id="id_sucursal_modal" data-value-type="number"
								tabindex="2"  style="width: 100%;">
									<option value="0">Seleccione una opción</option>
									@foreach ($sucursales as $sucursal)
										<option value="{{$sucursal->id}}">{{$sucursal->denominacion}}</option>
									@endforeach
								</select>
							</div>
						</div>
						
						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Centro Costo</label>
								<select class="form-control select2" name="centro_costo" id="centro_costo_id" data-value-type="number"
									tabindex="3"  style="width: 100%;">
									<option value="0">Seleccione una opción</option>
									@foreach ($centro_costos as $centro)
										<option value="{{$centro->id}}">{{$centro->nombre}}
										</option>
                               		 @endforeach
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Concepto (*)</label>
								<input type="text" class="form-control" maxlength="30" id="concepto" tabindex="4" name="concepto"
									value="">
							</div>
						</div>	
						
						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Debe/Haber (*)</label>
								<select class="form-control select2" name="es_debe" id="es_debe"
									tabindex="7"  style="width: 100%;">
									<option value="">Seleccione una opción</option>
									<option value="D">Debe</option>
									<option value="H">Haber</option>
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Moneda (*)</label>
								<select class="form-control select2" name="id_moneda_cuenta" id="id_moneda_cuenta" style="width: 100%;">
									<option value="0">Seleccione una moneda</option>
									@foreach($currency as $cu)
										<option value="{{$cu->currency_id}}">{{$cu->currency_code}}</option>
									@endforeach
								</select>
							</div>
						</div>
						
						<div class="col-12 col-sm-4 col-md-4">
                            <div class="form-group">
                                <label>Importe (*)</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text font-weight-bold" id="etiquetaCotizacion">N/A</span>
                                    </span>
                                    <input type="text" class="form-control triggerCotizado format-number-fp" maxlength="30" id="importe_asiento" tabindex="5" name="importe_asiento"
									value="">
                                </div>
                            </div>
						</div>
						
						<div class="col-12 col-sm-4 col-md-4">
                            <div class="form-group">
                                <label>Cotización (*)</label>
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text font-weight-bold">PYG</span>
                                    </span>
                                    <input type="text" class="form-control triggerCotizado format-number-gs" maxlength="30" id="cotizacion_contable" tabindex="6" name="cotizacion_contable"
									value="">
                                </div>
                            </div>
                        </div>						
								


						<div class="col-12 col-sm-4 col-md-4">
                            <div class="form-group">              
                                <div class="input-group">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text font-weight-bold">PYG</span>
                                    </span>
                                    <input type="text" class="form-control format-number-gs" maxlength="30" id="importe_cotizado" tabindex="8" name="importe_cotizado"
									value="" readonly>
                                </div>
                            </div>
                        </div>	

					<br>

					<div class="col-12">

                        <button type="button" class="mr-1 btn btn-success btn-lg pull-right" tabindex="9"
							id="btnMsgConfirmacion"><b>Guardar</b></button>
							
							<button id="idBtnCancelar" type="button" class="mr-1 btn btn-danger btn-lg pull-right" tabindex="10"
							 data-dismiss="modal"><b>Cancelar</b></button>

							 <button id="idBtnEliminar" type="button" class="mr-1 btn btn-danger btn-lg pull-right" tabindex="10"
							><b>Eliminar</b></button>
                    </div>
					
					<br>

				</form>
				<div id="idMensaje" class="row" style="display:none">
					<div class="col-12">
						<p>¿Está seguro que desea guardar el asiento?</p>
					</div>
					<div class="col-12">
						<button type="button" class="mr-1 btn btn-success btn-lg pull-right" tabindex="9"
							id="btnGuardarAsiento"><b>SÍ</b></button>
							
						<button id="idBtnNo" type="button" class="mr-1 btn btn-danger btn-lg pull-right" tabindex="10"
							 data-dismiss="modal"><b>NO</b></button>
					</div>
				</div>

				<div id="idMensajeEliminar" class="row" style="display:none">
					<div class="col-12">
						<p>¿Está seguro que desea eliminar el asiento?</p>
					</div>
					<div class="col-12">
						<button type="button" class="mr-1 btn btn-success btn-lg pull-right" tabindex="9" onclick="eliminarAsiento()"><b>SÍ</b></button>
							
						<button id="idBtnNoEliminar" type="button" class="mr-1 btn btn-danger btn-lg pull-right" tabindex="10"
							 data-dismiss="modal"><b>NO</b></button>
					</div>
				</div>
			  </div>
		  </div> 
		
  </div>
</div>
</div>

<input type="hidden" value="" id="hidden_origen_asiento">
<input type="hidden" value="" id="hidden_nro_documento">
<input type="hidden" id="hidden_id_detalle">

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<!-- /.content -->
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	
<script type="text/javascript">

const operaciones = {id_row:0, id_fila:0, tipoGuardar:true}

var oSettings = '';
var tablaDetalle =  $('#listadoAsientoDetalle').dataTable( {
       "pageLength": 100
});


$(function() {
	$('.select2').select2();
	let asiento = '{{$id}}';
	let origen = '{{$id_origen}}';
	let nro_documento = '{{$nro_doc}}';
	if(asiento != '' & asiento != '0'){
		initAsientoDetalleJs(asiento);
		$('#hidden_origen_asiento').val(origen);
		$('#hidden_nro_documento').val(nro_documento);
	} 
	
	// initAsiento();
	calendar();
	formatearGs();
});

function asientosBtn(){
	let id_origen_asiento = $('#hidden_origen_asiento').val();
	let nro_documento = $('#hidden_nro_documento').val();

	$('#id_origen_asiento').val(id_origen_asiento).trigger('change.select2');
	$('#documento').val($('#documento_asiento').val());
	$('#tabAsiento').tab('show');
	tableAsiento();
}

	

function initAsiento(){
	tableAsiento();
}	

  function limpiar()
  {
	$('#id_origen_asiento').val('').trigger('change.select2');
	$('#id_usuario').val('').trigger('change.select2');
	$('#id_asiento').val('');
	$('#asiento_fecha').val('');
	$('#documento').val('');
  }
		


			function tableAsiento() {

				const formatter = new Intl.NumberFormat('de-DE', {currency: 'USD',minimumFractionDigits: 0});

				$.blockUI({
					centerY: 0,
					message: "<h2>Procesando...</h2>",
					css: {
						color: '#000'
					}
				});

				// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
				setTimeout(function () {


					table = $('#listadoAsiento').DataTable({
						"destroy": true,
						"ajax": {
							data: $('#formAsiento').serializeJSON({
								customTypes: customTypesSerializeJSON
							}),
							url: "{{route('ajaxReporteAsiento')}}",
							type: "GET",
							error: function (jqXHR, textStatus, errorThrown) {

								$.toast({
									heading: 'Error',
									text: 'Ocurrió un error en la comunicación con el servidor',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
								});
								$.unblockUI();

							}

						},
						"columns": [{
								data: "id_asiento"
							},
							{
								data: "fecha_hora_format"
							},
							{
								data: "concepto"
							},
							{
								data: "nro_documento"
							},
							{
								data: "abreviatura"
							},
							{
								data: function (x) {
									// console.log(x); 
									return (!isNaN(parseFloat(x.debe))) ? formatter.format(parseFloat(x.debe)) : 0;
								}
							},
							{
								data: function (x) {
									return (!isNaN(parseFloat(x.haber))) ? formatter.format(parseFloat(x.haber)) : 0;
								}
							},
							{
								data: "usuario"
							},
							{
								data: "balanceado"
							},
							{
								data: function (x) 
								{
									if (x.activo == true)
									{
										return 'NO';
									}
									else
									{
										return 'SÍ';

									}
									
								}
							},
							{
								data: function (x) 
								{
									
									return `<button onclick="detalleAsiento(${x.id_asiento})" class='btn btn-info' type='button'><i class="fa fa-fw fa-search"></i></button>`;
								}
							}
						],
						"footerCallback": function ( row, data, start, end, display ) {
									total = this.api()
										.column(5)//numero de columna a sumar
										//.column(1, {page: 'current'})//para sumar solo la pagina actual
										.data()
										.reduce(function (a, b) {
											return parseFloat(clean_num(a)) + parseFloat(clean_num(b));
										}, 0 );

									$(this.api().column(5).footer()).html(formatter.format(parseFloat(total))); 

									total1 = this.api()
										.column(6)//numero de columna a sumar
										.data()
										.reduce(function (a, b) {
											return parseFloat(clean_num(a)) + parseFloat(clean_num(b));
										}, 0 );

									$(this.api().column(6).footer()).html(formatter.format(parseFloat(total1))); 
								} ,                    
						"createdRow": function (row, data, index, aData) {
						},
						"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
							if (aData['balanceado'] == "NO" )
							{
								$('td', nRow).css('background-color', '#f57567');
							}
							
						}

					}).on('xhr.dt', function (e, settings, json, xhr) {
						$.unblockUI();
					});

				}, 300);

			} //function

		function detalleAsiento(id)
		{
			$('#id_asiento_input').val(id);

			tableAsientoDetalle();
			$('#tabDetalle').tab('show');
			// $('#formAsientoDetalle').serializeJSON();
		}

		function calendar() {
			//CALENDARIO OPCIONES EN ESPAÑOL

			let locale = {
				format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',
				fromLabel: 'Desde',
				toLabel: 'Hasta',
				customRangeLabel: 'Seleccionar rango',
				daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
					'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
					'Diciembre'
				]
			};


			$('#asiento_fecha').daterangepicker({
				timePicker24Hour: true,
				timePickerIncrement: 30,
				locale: locale
			});
			$('#asiento_fecha').val('');
			$('#asiento_fecha').on('cancel.daterangepicker', function (ev, picker) {
				$(this).val('');
			});

		}


	//MONEDA
	const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });

	function initAsientoDetalleJs(data) {
		//DEFINIR FORMATO MONEDAS		
		// console.trace();
		if (data != 0) {
			detalleAsiento(data);
		}

		$('.select2').select2();
		tableAsientoDetalle();

		var Fecha = new Date();
		var fechaInicial = Fecha.getMonth() + 1 + "/" + Fecha.getFullYear();

	}

/*
	===========================================================================================================	
									SECCIÓN DE ABM DE ASIENTO DETALLE
	===========================================================================================================
*/
// -------------------------------------------------ABM--------------------------------------------------------------

// INSERTA UN NUEVO ASIENTO EN LA FILA DEL DATATABLE
function guardarAsiento()
{	
	//cotizar importe y cargar en campo disabled
	//ESPERA QUE TERMINE EL AJAX PARA CONTINUAR
	$.when(cotizarImporte()).then((a)=>{ 

	//OPERACION EN CASO DE OBTENER EL MONTO COTIZADO	
	$('#formNuevoAsiento').show();
	$('#idMensaje').hide();
	operaciones.id_row = operaciones.id_row + 1;
	var modalAsiento = $('#formNuevoAsiento').serializeJSON();
	//console.log(modalAsiento);
	var debe = 0;
	var haber = 0;
	var centro_costo = '';
	var moneda_cuenta_contable = $('#id_moneda_cuenta').val();

		// console.log(modalAsiento.importe_cotizado);
	if(modalAsiento.es_debe == 'D')
	{
		debe = clean_num(modalAsiento.importe_cotizado);
		
	}
	else
	{
		haber = clean_num(modalAsiento.importe_cotizado);
	}

	if(modalAsiento.centro_costo != 0)
	{
		centro_costo = $('#centro_costo_id option:selected').text();
	}

	var idRow = 'row_modificado'+operaciones.id_row;
	var input = `<input type="hidden"class="cuenta"  name="asiento_detalle[][cuenta]" value="${modalAsiento.plan_cuenta}"> 
				<input type="hidden" class="sucursal"  name="asiento_detalle[][sucursal]" value="${modalAsiento.id_sucursal_modal}">
				<input type="hidden" class="centro_costo" name="asiento_detalle[][centro_costo]" value="${modalAsiento.centro_costo}">
				<input type="hidden" class="debe" name="asiento_detalle[][debe]" value="${debe}">
				<input type="hidden" class="haber" name="asiento_detalle[][haber]" value="${haber}">
				<input type="hidden" class="cotizacion" name="asiento_detalle[][cotizacion]" value="${clean_num(modalAsiento.cotizacion_contable)}">
				<input type="hidden" class="importe_original" name="asiento_detalle[][importe_original]" value="${clean_num(modalAsiento.importe_asiento)}">
				<input type="hidden" class="importe_cotizado" name="asiento_detalle[][importe_cotizado]" value="${clean_num(modalAsiento.importe_cotizado)}">
				<input type="hidden" class="concepto" name="asiento_detalle[][concepto]" value="${modalAsiento.concepto}">
				<input type="hidden" class="moneda" name="asiento_detalle[][moneda]" value="${moneda_cuenta_contable}">`;

	debe = GUARANI(debe,{ formatWithSymbol: false }).format(true);
	haber = GUARANI(haber,{ formatWithSymbol: false }).format(true);

    var dataTableRow = 
	[
		input+`<span>${$('#id_plan_cuenta option:selected').attr('data-nro_cuenta')}</span>`,
		`<span>${$('#id_plan_cuenta option:selected').attr('data-nombre_cuenta')}</span>`,
		`<span>${$('#id_sucursal_modal option:selected').text()}</span>`,
		`<span>${centro_costo}</span>`,
		`<span>${debe}</span>`,
		`<span>${haber}</span>`,
		`<span>${modalAsiento.cotizacion_contable}</span>`,
		`<span>${modalAsiento.importe_asiento}</span>`,
		// `<span>${modalAsiento.concepto}</span>`,
		`<button onclick="getAsientoDetalle('${'rowNew'+operaciones.id_row}')" class='btn btn-info  btnEditHidden' style="display:none" type='button'><i class="fa fa-pencil-square-o"></i></button>`
	];
	// console.log(dataTableRow);

	if(validarCamposModal())
	{
		var newrow = $('#listadoAsientoDetalle').dataTable().fnAddData(dataTableRow);
		// set class attribute for the newly added row
		var nTr = $('#listadoAsientoDetalle').dataTable().fnSettings().aoData[newrow[0]].nTr;                      
		var nTds = $('td', nTr);
		nTds.addClass('rowNew'+operaciones.id_row);
		var row = $('#listadoAsientoDetalle').dataTable().fnGetNodes(newrow);
		$(row).attr('id', 'rowNew'+operaciones.id_row);
		$("#requestNuevoAsiento").modal("hide");
		sumarDebeHaber();
		levantarBtn();
	}




		
    },(b)=>{
		//OPERACION EN CASO DE ERROR

		$.toast({
            heading: '<b>Atención</b>',
            position: 'top-right',
            text: 'Ocurrio un error al cotizar los montos, no se puede almacenar el dato.',
            width: '400px',
            hideAfter: false,
            icon: 'info'
        });

		$('#formNuevoAsiento').show();
		$('#idMensaje').hide();
     });

	
  
}

//MODIFICA EL ASIENTO E INSERTA LOS CAMBIOS
function insertarAsientoModificado()
{	
	//cotizar importe y cargar en campo disabled
	//ESPERA QUE TERMINE EL AJAX PARA CONTINUAR
	$.when(cotizarImporte()).then((a)=>{ 

	//OPERACION EN CASO DE OBTENER EL MONTO COTIZADO	
	$('#formNuevoAsiento').show();
	$('#idMensaje').hide();
	var idFila = operaciones.id_fila;
	var modalAsiento = $('#formNuevoAsiento').serializeJSON();
	var centro_costo = '';
	var debe = 0;
	var haber = 0;
		// console.log(modalAsiento);
		// console.log(modalAsiento.importe_cotizado);

	if(modalAsiento.es_debe == 'D')
	{
		debe = modalAsiento.importe_cotizado;
	}
	else
	{
		haber = modalAsiento.importe_cotizado;
	}

	if(modalAsiento.centro_costo != 0)
	{
		centro_costo = $('#centro_costo_id option:selected').text();
	}
	var debe_format = GUARANI(clean_num(debe),{ formatWithSymbol: false }).format(true);
	var haber_format = GUARANI(clean_num(haber),{ formatWithSymbol: false }).format(true);
	var moneda_cuenta_contable = $('#id_moneda_cuenta').val();
	//console.log(debe, haber, modalAsiento.importe_cotizado);

	var contenido = `<td class="${idFila}"><input type="hidden" class="cuenta" name="asiento_detalle[][cuenta]" value="${modalAsiento.plan_cuenta}">
		<input type="hidden" class="sucursal" name="asiento_detalle[][sucursal]" value="${modalAsiento.id_sucursal_modal}">
		<input type="hidden" class="centro_costo" name="asiento_detalle[][centro_costo]" value="${modalAsiento.centro_costo}">
		<input type="hidden" class="debe" name="asiento_detalle[][debe]" value="${clean_num(debe)}">
		<input type="hidden" class="haber" name="asiento_detalle[][haber]" value="${clean_num(haber)}">
		<input type="hidden" class="cotizacion" name="asiento_detalle[][cotizacion]" value="${clean_num(modalAsiento.cotizacion_contable)}">
		<input type="hidden" class="importe_original" name="asiento_detalle[][importe_original]" value="${clean_num(modalAsiento.importe_asiento)}">
		<input type="hidden" class="importe_cotizado" name="asiento_detalle[][importe_cotizado]" value="${clean_num(modalAsiento.importe_cotizado)}">
		<input type="hidden" class="concepto" name="asiento_detalle[][concepto]" value="${modalAsiento.concepto}">
		<input type="hidden" class="moneda" name="asiento_detalle[][moneda]" value="${moneda_cuenta_contable}">
		<span>${$('#id_plan_cuenta option:selected').attr('data-nro_cuenta')}</span></td>
	<td class="${idFila}"><span>${$('#id_plan_cuenta option:selected').attr('data-nombre_cuenta')}</span></td>
	<td class="${idFila}"><span>${$('#id_sucursal_modal option:selected').text()}</span></td>
	<td class="${idFila}"><span>${centro_costo}</span></td>
	<td class="${idFila}"><span>${debe_format}</span></td>
	<td class="${idFila}"><span>${haber_format}</span></td>
	<td class="${idFila}"><span>${modalAsiento.cotizacion_contable}</span></td>
	<td class="${idFila}"><span>${modalAsiento.importe_asiento}</span></td>
	<!--<td class="${idFila}"><span>${modalAsiento.concepto}</span></td>-->
	<td class="${idFila}"><button onclick="getAsientoDetalle('${idFila}')" class='btn btn-info  btnEditHidden' style="display:none" type="button"><i
				class="fa fa-pencil-square-o"></i></button></td>`

	if(validarCamposModal())
	{
		$('#'+idFila).html(contenido);
		$("#requestNuevoAsiento").modal("hide");
		sumarDebeHaber();
		levantarBtn();
	} 

	},(b)=>{
		//OPERACION EN CASO DE ERROR

		$.toast({
            heading: '<b>Atención</b>',
            position: 'top-right',
            text: 'Ocurrió un error al cotizar los montos, no se puede almacenar el dato.',
            width: '400px',
            hideAfter: false,
            icon: 'info'
        });

		$('#formNuevoAsiento').show();
		$('#idMensaje').hide();
     });

}

// ELIMINA EL ASIENTO DE LA FILA DEL DATATABLE
function eliminarAsiento()
{
	$("#requestNuevoAsiento").modal("hide");
	$('#formNuevoAsiento').show();
	$('#idMensaje').hide();
	$('#idMensajeEliminar').hide();
	console.log($('#hidden_id_detalle').val());
	/*var oSettings = $('#listadoAsientoDetalle').dataTable().fnSettings();
	var iTotalRecords = oSettings.fnRecordsTotal();
	var resultadoFila = $('#hidden_id_detalle').val();*/
	//$('#listadoAsientoDetalle').dataTable().fnDeleteRow(resultadoFila,null,true); //elimina la fila
///////////////////////////////////////////////////////////////////////////
 	/*var row = $('#listadoAsientoDetalle').row(resultadoFila);
    var rowNode = row.node();
    row.remove();*/
    var resultadoFila = $('#hidden_id_detalle').val();
    $("#"+resultadoFila).remove();
///////////////////////////////////////////////////////////////////////////
	sumarDebeHaber();
}

// -------------------------------------------------FIN ABM--------------------------------------------------------------

function sumarDebeHaber()
{
	
	var asientoDetalle = $('#formAsientoDetalle').serializeJSON();
	var debe = 0;
	var haber = 0;
	var dif = 0;

	$.each(asientoDetalle.asiento_detalle, (index, value)=>{
		// debe += Number(value.debe);
		// haber += Number(value.haber);

		debe += Math.round(value.debe);
		haber += Math.round(value.haber);
	})
	dif = debe - haber;

	console.log('debe/haber sin'+debe, haber);
	operaciones.debe = debe;
	operaciones.haber = haber;
	debe = GUARANI(debe,{ formatWithSymbol: false }).format(true);
	haber = GUARANI(haber,{ formatWithSymbol: false }).format(true);
	dif = GUARANI(dif,{ formatWithSymbol: false }).format(true);
	console.log('debe/haber con'+debe, haber);

	$('#total_debe').html(debe);
	$('#total_haber').html(haber);
	$('#diferencia_footer').html(dif);
}


//OBTIENE LA COTIZACIÓN CONTABLE ACTUAL
function getCotizacion() 
{
	var idMonedaCuenta = $('#id_moneda_cuenta').val();
	var tipoCotizacionCuenta = $('#id_plan_cuenta option:selected').attr('data-tipo_cotizacion_cuenta');
	var idDetalleAsiento = $('#hidden_id_detalle').val();
	$.ajax({
		type: "GET",
		url: "{{route('getCotizacionContable')}}",
		data: 
		{
            tipoCotizacionCuenta : tipoCotizacionCuenta,
            idMonedaCuenta : idMonedaCuenta,
            idDetalleAsiento : idDetalleAsiento	
        },
		dataType: 'json',
		
			success: function (rsp) 
			{
				$('#cotizacion_contable').val(rsp.cotizacionContable);
				
				
				if (rsp.cotizacionContable == 1) 
				{	
					$('#cotizacion_contable').prop('readonly', true);	
				} 
				else
				{
					$('#cotizacion_contable').prop('readonly', false);	
				}

				formatearNumTabla(idMonedaCuenta);
				formatearGs();
			} 


	}); 

} 


//OBTIENE EL IMPORTE COTIZADO Y MUESTRA EN EL CAMPO DESHABILITADO
function cotizarImporte()
{
  return new Promise((resolve, reject) => { 
	var importeAsiento = clean_num($('#importe_asiento').val());
	var cotizacionContable = clean_num($('#cotizacion_contable').val());
	var idMonedaCuenta = $('#id_moneda_cuenta').val();
	console.trace(importeAsiento);

	$.ajax({
		type: "GET",
		url: "{{route('getImporteCotizado')}}",
		data: 
		{
            importeAsiento : importeAsiento,
			cotizacionContable : cotizacionContable,
            idMonedaCuenta : idMonedaCuenta
			
        },
		dataType: 'json',
			error()
			{
				reject();
			},
			success: function (rsp) 
			{   
				resolve();
				$('#importe_cotizado').val(Number(rsp.montoCotizado).toFixed(2));
				formatearNumTabla(idMonedaCuenta);
			} 


	});

  });
}


//AL SELECCIONAR UNA CUENTA CONTABLE SE OBTIENE LA COTIZACIÓN 
$('#id_moneda_cuenta').change(function(){
	getCotizacion();
	asignarMonedaInput();
});
      
// AJAX QUE CARGA EL DATATABLE DE ASIENTO DETALLE
function tableAsientoDetalle() 
{
	$.ajax({
                type: "GET",
                url: "{{ route('ajaxReporteAsientoDetalle') }}",
                dataType: 'json',
                data: $('#formAsientoDetalle').serializeJSON(),
				success: function(rsp)
				{
                    oSettings = tablaDetalle.fnSettings();
					
					var iTotalRecords = oSettings.fnRecordsTotal();
					var importe_cotizado = 0;

					for (i=0;i<=iTotalRecords;i++) 
					{
                        $('#listadoAsientoDetalle').dataTable().fnDeleteRow(0,null,true);
					}
					$('#asiento').val(rsp.data1.id);
					$('#documento_asiento').val(rsp.data1.nro_documento);
					$('#fecha_asiento').val(rsp.data1.fecha);
					$('#concepto_datatable').val(rsp.data1.concepto);
					$.each(rsp.data, function (key, item)
					{
						// console.log(item.haber);
						if(Boolean(item.debe) & item.debe > 0)
						{
							importe_cotizado = item.debe;
						}
						else
						{
							importe_cotizado = item.haber;
						}

						if(item.sucursal !== null){
							sucursal = item.sucursal
						}else{
							sucursal = '';
						}

						if(!isNaN(parseFloat(item.cotizacion))){ 
							cotizacion = formatter.format(parseFloat(item.cotizacion)); 
						}else{ 
							cotizacion = 1;
						}

						var inputs = `<input type="hidden" class="cuenta" name="asiento_detalle[][cuenta]" value="${item.id_cuenta_contable}"> 
									  <input type="hidden" class="sucursal" name="asiento_detalle[][sucursal]" value="${item.id_sucursal}">
								      <input type="hidden" class="centro_costo" name="asiento_detalle[][centro_costo]" value="${(Boolean(item.id_centro_costo))?item.id_centro_costo:0}">
									  <input type="hidden" class="debe" name="asiento_detalle[][debe]" value="${item.debe}">
									  <input type="hidden" class="haber" name="asiento_detalle[][haber]" value="${item.haber}">
									  <input type="hidden" class="cotizacion" name="asiento_detalle[][cotizacion]" value="${cotizacion}">
									  <input type="hidden" class="importe_original" name="asiento_detalle[][importe_original]" value="${item.monto_original}">
									  <input type="hidden" class="importe_cotizado" name="asiento_detalle[][importe_cotizado]" value="${importe_cotizado}">
									  <input type="hidden" class="concepto" name="asiento_detalle[][concepto]" value="${item.concepto_detalle}">
									  <input type="hidden" class="moneda" name="asiento_detalle[][moneda]" value="${item.id_moneda}">`;

                        var dataTableRows = 
						[
							inputs+`<span>${item.num_cuenta}</span>`,
							`<span>${item.cuenta_n}</span>`,
							`<span>${sucursal}</span>`,
							`<span>${item.centro_costo}</span>`,
							`<span>${formatter.format(parseFloat(item.debe))}</span>`,
							`<span>${formatter.format(parseFloat(item.haber))}</span>`,
							`<span>${cotizacion}</span>`,
							`<span class="format-number-fp">${item.monto_original}</span>`,
							// `<span>${item.concepto_detalle}</span>`,
							`<button onclick="getAsientoDetalle('row'+${item.id})" class='btn btn-info btnEditHidden' style="display:none" type='button'><i class="fa fa-pencil-square-o"></i></button>`
						];

						console.log(dataTableRows);

                        var newrow = $('#listadoAsientoDetalle').dataTable().fnAddData(dataTableRows);
                    // set class attribute for the newly added row
						var nTr = $('#listadoAsientoDetalle').dataTable().fnSettings().aoData[newrow[0]].nTr;
				
                        var nTds = $('td', nTr);
                        nTds.addClass('row'+item.id);

						var row = $('#listadoAsientoDetalle').dataTable().fnGetNodes(newrow);
    					$(row).attr('id', 'row'+item.id);
						importe_cotizado = 0;
						formatearGs()
                    })
                }
            }).done(function(){
                $.unblockUI();
				sumarDebeHaber();
				levantarBtn();
            });
}

//FUNCIÓN QUE LEVANTA EL MODAL DE AGREGAR ASIENTO
function agregarNuevoAsiento()
{
	operaciones.tipoGuardar = true;
	$('#hidden_id_detalle').val('');
	asignarMonedaInput();
	$("#requestNuevoAsiento").modal("show");
	$('#idBtnCancelar').show();
	$('#idBtnEliminar').hide();
	limpiarModal();
}

//GUARDAR EL ASIENTO COMPLETO
function actualizarAsiento()		
{
	return new Promise((resolve, reject) => { 
	if(validarTotalDebeHaber())
	{
		$.ajax({
		type: "POST",
		url: "{{route('actualizarAsiento')}}",
		data:$('#formAsientoDetalle').serializeJSON(),
		dataType: 'json',
		 error(){
			reject('Ocurrió un error en la comunicación con el servidor.');
		 },
			success: function (rsp) 
			{
				if(rsp.err){
					resolve('El asiento ha sido actualizado con éxito');
					$('#tabAsiento').tab('show');
				} else {
					reject('Ocurrió un error en la comunicación con el servidor.');
				}
				
				
			} 
		}); 
	} else {
		reject('Los totales DEBE/HABER no coinciden. Por favor verifique para continuar.')
	}

	});
}

//OBTIENE TODOS LOS DATOS DE LA FILA DE ASIENTO DETALLE A MODIFICAR
function getAsientoDetalle(idAsientoDetalle)
{
	// console.log($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.moneda').val());
	
	$('#hidden_id_detalle').val(idAsientoDetalle);

	$('#idBtnEliminar').show();
	//$('#idBtnCancelar').hide();
	operaciones.tipoGuardar = false; //define como va a actuar el boton Guardar (en este caso modifica)
	operaciones.id_fila = idAsientoDetalle;
	$('#id_plan_cuenta').val($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.cuenta').val()).trigger('change.select2');
	$('#id_sucursal_modal').val($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.sucursal').val()).trigger('change.select2');
	$('#id_moneda_cuenta').val($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.moneda').val()).trigger('change.select2');

	if(Boolean($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.centro_costo').val()))
	{
		$('#centro_costo_id').val($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.centro_costo').val()).trigger('change.select2');
	}
	else
	{
		$('#centro_costo_id').val('').trigger('change.select2');

	}

	if($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.concepto').val() === "null"){
	    concepto ="";		
	}else{
		concepto = $('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.concepto').val();
	}

	$('#concepto').val(concepto);
	

	$('#importe_asiento').val($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.importe_original').val());

	//console.log($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.cotizacion').val());

	$('#cotizacion_contable').val($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.cotizacion').val());

	if($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.debe').val() == 0)
	{
		$('#es_debe').val('H').trigger('change.select2');
		$('#importe_cotizado').val($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.haber').val());

	}
	else
	{
		$('#es_debe').val('D').trigger('change.select2');
		$('#importe_cotizado').val($('#'+idAsientoDetalle+' .'+idAsientoDetalle+' input.debe').val());

	}

	asignarMonedaInput();
	//getCotizacion();
	$('#requestNuevoAsiento').modal('show');
	// formatearNumTabla();
	// formatearGs();

}


function asignarMonedaInput()
{
	var currencyCode = $('#id_moneda_cuenta option:selected').text();
	$('#id_plan_cuenta_span').html(currencyCode);
	$('#etiquetaCotizacion').html("");
	$('#etiquetaCotizacion').html(currencyCode);
}


$( ".triggerCotizado" ).change(()=>{
	cotizarImporte();
});

$('#id_plan_cuenta').change(()=>{
	asignarMonedaInput();
});

$('#btnGuardarAsiento').click(()=>{
	//console.log(operaciones.tipoGuardar);
	if(operaciones.tipoGuardar)
	{
		guardarAsiento(); //GUARDA EL ASIENTO NUEVO
	}
	else
	{
		insertarAsientoModificado(); //MODIFICA EL ASIENTO
	}
});

$('#btnMsgConfirmacion').click(()=>{

	if(validarCamposModal())
	{
		$('#formNuevoAsiento').hide();
		$('#idMensaje').show();
	}

});

$('#idBtnEliminar').click(()=>{
	$('#formNuevoAsiento').hide();
	$('#idMensajeEliminar').show();
});

$('#idBtnNo').click(()=>{
	$('#formNuevoAsiento').show();
	$('#idMensaje').hide();
});

$('#idBtnNoEliminar').click(()=>{
	$('#formNuevoAsiento').show();
	$('#idMensajeEliminar').hide();
});


@if (count($btn)> 0)
$('.btnConfirmarAsiento').click(()=>{
	modalConfirmarAsiento();
});

function levantarBtn(){
	$('.btnEditHidden').show();
}
@else 
function levantarBtn(){

}
@endif

//--------------------MODAL-----------------------------

	function modalConfirmarAsiento()
	{

				swal({
					title: "Confirmar Asiento",
					text: "Esta seguro que desea confirmar las modificaciones ?",
					icon: "warning",
					showCancelButton: true,
					buttons: {
						cancel: {
							text: "No, Cancelar",
							value: null,
							visible: true,
							className: "btn-warning",
							closeModal: false,
						},
						confirm: {
							text: "Si , Confirmar",
							value: true,
							visible: true,
							className: "",
							closeModal: false
						}
					}
				}).then(isConfirm => {
					
					if (isConfirm) {
						$.when(actualizarAsiento()).then((a)=>{ 
							swal("Éxito",a , "success");
						},(b)=>{
							swal("Cancelado", b, "error");
						});
						
					} else {
						swal("Cancelado", "La operación fue cancelada", "error");
					}
				});
	}


// ---------------------EXTRAS---------------------------
function formatearNumTabla(idMoneda)
{
		$('.format-number-fp').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

}

function formatearGs()
{
		$('.format-number-fp').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

		$('.format-number-gs').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});
}

function clean_num(n, bd = false) {
                        n = n.toString()
                        if (n && bd == false) {
                            n = n.replace(/[,.]/g, function (m) {
                                if (m === '.') {
                                    return '';
                                }
                                if (m === ',') {
                                    return '.';
                                }
                            });
                            return Number(n);
                        }
                        if (bd) {
                            return Number(n);
                        }
                    return 0;
                }	

/*Para formatear numeros a moneda USD PY*/
const formatter = new Intl.NumberFormat('de-DE', {currency: 'USD',minimumFractionDigits: 0});

function limpiarModal()
{
	$('#id_plan_cuenta').val('0').trigger('change.select2');
	$('#centro_costo_id').val('0').trigger('change.select2');
	$('#id_sucursal_modal').val('0').trigger('change.select2');
	$('#es_debe').val('').trigger('change.select2');
	$('#concepto').val('');
	$('#cotizacion_contable').val('');
	$('#importe_cotizado').val('');
	$('#importe_asiento').val('');
}

function validarCamposModal()
{
	var plan_cuenta = $('#id_plan_cuenta').val();
	var concepto = $('#concepto').val();
	var sucursal = $('#id_sucursal_modal').val();
	var importe = $('#importe_asiento').val();
	var es_debe = $('#es_debe').val();

	if(plan_cuenta == 0)
						{
		if(concepto == '')
						{
			if(sucursal == 0 )
						   	{ 
				if(importe == '')
								{ 
					if(es_debe == 0)
									{
									$.toast({
							                    heading: 'Error',
							                    text: 'Seleccione el Debe/Haber.',
							                    position: 'top-right',
							                    showHideTransition: 'fade',
							                    icon: 'error'
							                });
									 $("#es_debe").next().children().children().each(function(){
				                        $(this).css( "border-color", "red" );
				                    });
									return false;
								}else{
									 $("#es_debe").next().children().children().each(function(){
				                        $(this).css( "border-color", "#aaa" );
				                    });
								}
								$.toast({
							                    heading: 'Error',
							                    text: 'Agregue el importe.',
							                    position: 'top-right',
							                    showHideTransition: 'fade',
							                    icon: 'error'
							                });
									$("#importe_asiento").css('border-color', 'red');
									return false;
								}else{
									$("#importe_asiento").css('border-color', "#aaa");
								}
							$.toast({
							                    heading: 'Error',
							                    text: 'Seleccione sucursal.',
							                    position: 'top-right',
							                    showHideTransition: 'fade',
							                    icon: 'error'
							                });
								 	$("#id_sucursal_modal").next().children().children().each(function(){
				                        $(this).css( "border-color", "#red" );
				                    });
									return false;
								}else{
									$("#id_sucursal_modal").next().children().children().each(function(){
				                        $(this).css( "border-color", "#aaa" );
				                    });
								}
						$.toast({
							                    heading: 'Error',
							                    text: 'Agregue concepto.',
							                    position: 'top-right',
							                    showHideTransition: 'fade',
							                    icon: 'error'
							                });
									$("#concepto").css('border-color', 'red');
									return false;
								}else{
									$("#concepto").css('border-color', "#aaa")
								}	
					$.toast({
							                    heading: 'Error',
							                    text: 'Seleccione cuenta contable.',
							                    position: 'top-right',
							                    showHideTransition: 'fade',
							                    icon: 'error'
							                });
									$("#id_plan_cuenta").next().children().children().each(function(){
				                        $(this).css( "border-color", "red" );
				                    });
									return false;
								}else{
									$("#id_plan_cuenta").next().children().children().each(function(){
				                        $(this).css( "border-color", "#aaa" );
				                    });
								}	
		return true;
}

function validarTotalDebeHaber()
{
	console.log('debe '+operaciones.debe);
	console.log('haber '+operaciones.haber);
	if(operaciones.debe != operaciones.haber)
	{
		// $.toast({
        //             heading: 'Error',
        //             text: 'Los totales DEBE/HABER no coinciden. Por favor verifique para continuar.',
        //             position: 'top-right',
        //             showHideTransition: 'fade',
        //             icon: 'error'
        //         });
		return false;
	}
		return true;
}

$("#botonExcel").on("click", function(e){ 
			// console.log('Inicil');
               e.preventDefault();
               $('#formAsiento').attr('method','post');
               $('#formAsiento').attr('action', "{{route('generarExcelAsiento')}}").submit();
            });


					   
</script>

@endsection


