@extends('masters')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
	
@endsection
<?php

?>
@section('content')
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Migración Compras</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	<form id="frmBusqueda" method="post" action="{{route('generarExcelCompra')}}">
            		<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>Operaciones</label>					            	
								<select class="form-control input-sm select2"  name="operacion" id="operacion" style="    padding-left: 0px;width: 100%;" required>
									<option value="">Todo</option>
									<option value="true">Migradas</option>
									<option value="false"  selected="selected" >No Migradas</option>

								</select>
							</div>
						</div> 
						<div class="col-md-3">  
							<div class="form-group adaptForm">
								<label>Fecha Inicial/Final</label>						 
								<div class="input-group">
									<div class="input-group-prepend" style="">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="periodo" id="periodo" required>								
								</div>
							</div>	
						</div> 

            		</div>	
					<div class="row">
						<div class="col-md-12" style="padding-left: 25px; padding-top: 10px;padding-right: 15px"> 
							<button type="button" id="btnBuscar" class="pull-right btn btn-info btn-lg"><b>Buscar</b></button>
							<button type="button" id="botonExcel" style=" margin-right: 15px;" class="pull-right text-center btn btn-success btn-lg"><b>Excel</b></button>
						</div>	
					</div>		
            	</form>	
            	<br>
            	<div class="table-responsive">
	              <table id="listado" class="table">
	                <thead>
						<tr>
							<th style="text-align: center" >Número Factura</th>
							<th style="text-align: center" >Tipo Factura</th>
							<th style="text-align: center" >Proveedor</th>
							<!-- <th>Exportar a Excel</th> -->
			            </tr>
	                </thead>
	                
	                <tbody style="text-align: center">
	                	
			        </tbody>
	              </table>
	            </div>  
            </div>
        </div>    	
    </div>    
</section>


@endsection

@section('scripts')
 @parent
	include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$('.select2').select2();

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
					
			$( "#periodo" ).daterangepicker({
                                  timePicker24Hour: true,
                                   timePickerIncrement: 30,
                                   locale: {
                                       format: 'DD/MM/YYYY '//H:mm'
                                   }
                                   });
		    $("#itemList").select2({
					    ajax: {
					            url: "{{route('destinoGrupo')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});
		});
		$("#btnBuscar").click(function(){
			$.blockUI();

 			var dataString = $("#frmBusqueda").serialize();
 			$.ajax({
					type: "GET",
 					url: "{{route('getMigracionCompra')}}",
 					dataType: 'json',
 					data: dataString,
 					success: function(rsp){ 
 						console.log(rsp);
 						$.unblockUI();
 						var oSettings = $('#listado').dataTable().fnSettings();
 						var iTotalRecords = oSettings.fnRecordsTotal();
 						for (i=0;i<=iTotalRecords;i++) {
 							$('#listado').dataTable().fnDeleteRow(0,null,true);
						} 						

 						$.each(rsp, function (key, item){
 							console.log(item);
 						
 							var dataTableRow = [
												item.com_numero,
					 							item.form_pag,
												item.com_prvnom
												];
							var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
 							var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
 						})
 					}
 				})
		});

		$("#botonExcel").on("click", function(e){
                e.preventDefault();
               $('#frmBusqueda').attr('action', "{{route('generarExcelCompra')}}").submit();
            });
		
	</script>
@endsection
