@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	@parent
	<style type="text/css">
	
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}
</style>
  <link rel="stylesheet" href="{{asset('mC/css/toast.min.css')}}">
@endsection
@section('content')

<section id="base-style">
	@include('flash::message') 

	<section class="card">
		<div class="card-header">
			<h4 class="card-title"> Envio de Extracto a Agencias</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


          		<div class="row" >
					<div class="col-12">
						<button type="button" id="btnSincronizar" class="btn btn-success btn-lg ml-5"><b>SI</b></button>
						<button type="button" id="btnCancelar"  class="btn btn-danger btn-lg ml-2" ><b>NO</b></button>
					</div>  
				</div>	

			</div>
		</div>
	</section>
</section>
			

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script src="{{asset('gestion/app-assets/js/toast.min.js')}}"></script>
	<script>
		$(document).ready(function(){		
			$("#btnSincronizar").click(function(){
					$("#btnSincronizar").prop("disabled", true);
					BootstrapDialog.confirm({
				            title: '<b>GESTUR</b>',
				            message: '¿Esta seguro que desea enviar los extractos?',
				            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
				            closable: true, // <-- Default value is false
				            draggable: true, // <-- Default value is false
				            btnCancelLabel: 'No', // <-- Default value is 'Cancel',
				            btnOKLabel: 'Si, Enviar', // <-- Default value is 'OK',
				            btnOKClass: 'btn-error', // <-- If you didn't specify it, dialog type will be used,

				            callback: function(result) {
				                // result will be true if button was click, while it will be false if users close the dialog directly.
				                if(result) 
								{
					                $.toast({
											heading: 'Exito',
											text: 'En proceso, los extractos están siendo enviados.',
											position: 'top-right',
											showHideTransition: 'slide',
									    	icon: 'success'
									});  
																			
				                    $.ajax({
										type: "GET",
										url: "http://mid.dtpmundo.com:9292/ControlDTP/resources/notificarDtpMundo?que=FacturasVencidas",
										dataType: 'json',
										data: {
												data: 1,
								       			},
										success: function(rsp){
										}	

								})	
							}
						}	
					});//BootstrapDialog


			});	

			$("#btnCancelar").click(function(){
						$.toast({
							    heading: 'Error',
							    text: 'Proceso cancelado',
							    position: 'top-right',
							    showHideTransition: 'fade',
							    icon: 'error'
							});

			});	

		});	
	</script>
@endsection