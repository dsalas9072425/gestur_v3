<!DOCTYPE html>
<html lang="es" translate="no">
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
    @parent
@endsection
    
@section('content')
<style>
    .pagination .page-item {
        border-radius: 50%;
    }
    /* Estilos para la tabla con desplazamiento horizontal */
    .table-responsive {
        overflow-x: auto;
    }
</style>
<style type="text/css">
	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	/*.bgRed:hover{
		color: white;
	}*/

	/*background: #e2076a !important;padding-left: 6px;padding-right: 6px;*/



	/*==================SELECT STAR============================*/

	.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}


.click_estrella {
	cursor:pointer; 
	cursor: hand;
}

/*====================================================
	ESTILO PARA FOCUS
  ====================================================	
*/

input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }



</style>

<section id="base-style">
    <br>
<form id="resumencobro">
    <div class="card">
        <div class="card-header">
            <div class="card-body">   
            <h4 class="card-title">Resumen Cobros</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        
            <div class="col-12 col-sm-3 col-md-3">
                <div class="form-group">
                   <label>Desde/Hasta</label>			
                       <div class="input-group">
                       <div class="input-group-prepend" style="">
                           <span id="fechcheck"class="input-group-text"><i class="fa fa-calendar"></i></span>
                       </div>
                      <input type="text" id="periodo_out" name="periodo_out" class="form-control" autocomplete="off">
                   </div>
               </div>	 
           </div>        
    


            <div class="col-md-3">
                <div class="form-group">
                    <label>Unidad Negocio</label>
                    <select class="form-control finput-sm select2" name="unidad_negocio" id="unidad_negocio" style="width: 100%;" tabindex="6">
                        <option value="">Todos</option>
                        @foreach($unidadNegocio as $unidad)
                        <option value="{{ $unidad->id }}" @if($unidad->id == request('unidad_negocio')) selected @endif>{{ $unidad->descripcion }}</option>
                        @endforeach
                    
                    </select>
                </div>
            </div>
     </div>

    </form>   
        <div class="row">
             <div class="col-md-12">
             {{--   <a href="{{ route('facturapre_excel') }}" class="pull-right text-center btn btn-success btn-lg mr-1">Excel</a> --}}
             <button type="button" id="botonExcel" onclick="botonExcel()" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
             <button type="button" onclick="limpiar()" id="btnLimpiar" class="pull-right btn btn-light btn-lg text-white mr-1 mb-1" tabindex="17" ><b>Limpiar</b></button>
                <button type="button" class="pull-right btn btn-info btn-lg mr-1 mb-1" role="button" onclick="buscarFacturas()" tabindex="16"><b>Buscar</b></button>
            </div>
        </div>
 
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                 
                        <div class="row">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3">
                           
                            </div>
                        </div>
                        <div class="table-responsive">
                        <table id="resumen_cobro" class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Forma de Cobro</th>
                                    <th>Cuenta Banco</th>
                                    <th>Total USD</th>
                                    <th>Total PYG</th>
                                    <th>Unidad Negocio</th>
                                </tr>
                            </thead>
                            <tbody id="facturas-table">
                              
                                
                        
                            </tbody>
                        </table>
                 
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
 
    @include('layouts/gestion/scripts')
	<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
    <script>
        function buscarFacturas() {
      $('#resumen_cobro').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ route('resumenCobrosAjax') }}', 
            type: 'GET',
            data: $('#resumencobro').serializeJSON()
        },
        columns: [
            { data: 'denominacion', name: 'denominacion' },
            { data: 'banco_cuenta', name: 'banco_cuenta' },
            { data: 'total_importe_pago_usd', name: 'total_importe_pago_usd' },
            { data: 'total_importe_pago_pyg', name: 'total_importe_pago_pyg' },
            { data: 'unidad_negocio', name: 'unidad_negocio' },           
        ]
    });
}

var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};
            $(document).ready(function() {

                $("#vendedor").select2({
                                language: lang_es_select2,
                                ajax: {
		                url: "{{route('get.vendedores.agencia')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });
            $("#producto_0").focusin(function(){
		   		$("#producto_0").select2('open');
		   	})

               
               let config = {
												timePicker24Hour: true,
												timePickerIncrement: 30,
												autoUpdateInput: false,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			};
            $('input[name="periodo_out"]').daterangepicker(config);
			$('input[name="periodo_out"]').val('');
			$('input[name="periodo_out"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
			$('input[name="periodo_out"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});

        });//READY DOCUMENT
        
                $(".select2").select2();

        function limpiar(){
          
    $('#vendedor').val('').trigger('change.select2');

    $('#periodo_out').val('');
    $('#hasta').val('');

    $('#unidad_negocio').val('').trigger('change.select2');

 
    //$('input[name="buscar"]').val('');

    // consultaAjax();
}

$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                 $('#resumencobro').attr('method','post');
               $('#resumencobro').attr('action', "{{route('generarExcelresumencobro')}}").submit();
            });

    </script>
@endsection


