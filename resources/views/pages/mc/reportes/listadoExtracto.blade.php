@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
      	<form id="frmAgencias" class="contact-form" action="" method="post">
	        <div class="col-xs-12">
	          <div class="box">
		          <br>
		          <br>
	            <div class="box-header">
	              <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Reporte Extracto de Proveedores</h1>
	            </div> 
	            @include('flash::message')     
				<div class="row">
					<div class="col-xs-12 col-sm-1 col-md-1">
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2">
						<label class="control-label">Extracto Provedor:</label>
					</div>	
					<div class="col-xs-12 col-sm-7 col-md-7">
						<select name="proveedor" class="select2 form-control" required id="agenciaId">
							<option value="0">Seleccione una Extracto Proveedor</option>
							@foreach($listadoExtracto as $key=>$extractoProveedor)
								<option value="{{$extractoProveedor['id']}}">{{$extractoProveedor['fecha']}} - <b>{{$extractoProveedor['proveedores']['infoprovider_name']}}</b></option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-1 col-md-1">	
						<button type="button" style="width: 130px;  background-color: #3c8dbc; height: 40px;" id= "botonExcel" class="pull-right text-center btn btn-success btn-lg" disabled="disabled">Excel</button>
					</div>
				</div>	            <!-- /.box-header -->
	            <div class="box-body">
	            	<div class="table-responsive">
						<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
				            <thead>
				                <tr>
				                    <th>#</th>
				                    <th>Localizador</th>
				                    <th>Monto</th>
				                    <th>Moneda</th>
				                    <th>Proforma</th>
				                    <th>Estado</th>
				                    <th>Factura</th>
				                    <th>Vendedor</th>
							        <th>Match</th>
				                    <th>Estado</th>
				                    <th>Comentario</th>
				                    <th></th>
				                </tr>
				            </thead>
				            <tbody>
				                @if(isset($extractoProveedores[0]['extracto_detalles']))
					                @foreach($extractoProveedores[0]['extracto_detalles'] as $key=>$extracto)
					                    <tr>
					                        <td>{{$extracto['id']}}</td>
					                        <td><b>{{$extracto['localizador']}}</b></td>
					                        <td>{{number_format($extracto['saldo'], 2, ',', ' ')}}/<b>{{number_format($extracto['monto_factour'], 2, ',', ' ')}}</b></td>
					                        <td>
					                        <?php 
					                            switch ($extracto['moneda_id']) {
							                        case 143:
							                            $moneda = 'USD';
							                            break;
							                        case 111:
							                            $moneda = 'PYG';
							                            break;
							                        case 43:
							                            $moneda = 'EUR';
							                            break;
							                        case 21:
							                            $moneda = 'BRL';
							                            break;
							                        case 0:
							                            $monedaF = '';
							                            break;
							                    } 
					                            switch ($extracto['moneda_factour_id']) {
							                        case 143:
							                            $monedaF = 'USD';
							                            break;
							                        case 111:
							                            $monedaF = 'PYG';
							                        	break;
							                        case 43:
							                            $monedaF = 'EUR';
							                            break;
							                        case 21:
							                            $monedaF = 'BRL';
							                            break;
							                        case 0:
							                            $monedaF = '';
							                            break;
							                    }  
					                        ?>	
					                        {{$moneda}}/{{$monedaF}}</td>
					                        <td>
							                    {{$extracto['n_proforma']}}
					                        </td>
					                        <td>
												<?php 
													$estadosArray = explode(",", $extracto['estado_proforma']);
													$stringEstados = "";
													$contador = 0;
													foreach($estadosArray as $key=>$estados){
							                            switch ($estados) {
									                        case 'A':
									                            $estado = 'ABIERTA';
									                            break;
									                        case 'F':
									                            $estado = 'FACTURADA';
									                            break;
									                        case 'B':
									                            $estado = 'BOQUEADA';
									                            break;
									                        case 'N':
									                            $estado = 'ANULADA';
									                            break;
									                        case 'P':
									                            $estado = 'PROCESO';
									                         	break;
									                        case 'O':
									                            $estado = 'OPERACIONES';
									                        	break;
														    case '':
														        $estado = '';
														        break;
									                    } 
											            if($key == 0){
											               	$stringEstados .= $estado;   
											            }else{
											                $stringEstados .= "/".$estado;
											            }
											            $contador = $contador+1; 
									                }    	
								                ?>	
								                <b>{{$stringEstados}}</b></td>
					                            <td>{{$extracto['factura_factour']}}</td>
					                            <td>{{$extracto['pasajero_factour']}}</td>
								                <td>
									                @if($extracto['match'] == 1)
									                    <b>{{'SI'}}</b>
									                @else
									                    <b>{{'NO'}}</b>
									                @endif
								                </td>
					                            <td>
									                @if($extracto['ok'] == 1)
									                    <b>{{'OK'}}</b>
									                @else
									                    <b>{{'ERROR'}}</b>
									            @endif
												</td>
					                            <td><b>{{$extracto['comentario']}}</b></td>
					                            <td><div id="boton{{$extracto['id']}}">
					                            	@if($extracto['ok'] != 1)
					                            		@if($extracto['verificado'] != 1)
								                            <a onclick="cargar({{$extracto['id']}})" data-toggle="modal" href="#requestModal" class="btn-mas-request"><i class="fa fa-pencil-square-o fa-lg"></i></a>
								                        @else
															<a onclick="cargarComentario({{$extracto['id']}})" data-toggle="modal" href="#requestComentario" class="btn-mas-request"><img alt="" style="width: 20px;" src="images/icon/2.png"></a>								                        
														@endif      
							                        @endif    
					                            </div></td>
					                    </tr>   	
					                @endforeach
					             @endif	
				            </tbody>
			            </table>	            
			        </div>  
	            </div>
	            <!-- /.box-body -->
	          </div>
	          <!-- /.box -->
	          <!-- /.box -->
	        </div>
	    </form>    
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

	<!-- Modal Request-->
	<div id="requestModal" class="modal fade" role="dialog">
		<form id="frmProforma" method="get" style="margin-top: 20%;">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Ingrese Comentario</h2>
					</div>
				  	<div class="modal-body">
				  		<div id="contenido">
				  			 <input type="hidden" class="input-text full-width" id="detalle_id" name="detalle_id">
				  			 <textarea class="form-control" rows="5" name="comentario" id="comment"></textarea>
				  		</div>
				  </div>
				  <div class="modal-footer">
					<button type="button" id="btnAceptarProforma" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Aceptar</button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
				  </div>
				</div>
	  		</div>
		 </form>
	</div>

	<!-- Modal Request-->
	<div id="requestComentario" class="modal fade" role="dialog">
		<form id="frmProforma" method="get" style="margin-top: 20%;">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Comentario</h2>
					</div>
				  	<div class="modal-body">
				  		<div id="contenido">
				  			 <div id="commentUsuario"></div>
				  		</div>
				  </div>
				  <div class="modal-footer">
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
				  </div>
				</div>
	  		</div>
		 </form>
	</div>

@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$('#listado').dataTable({
								  "pageLength": 100
								} );
		var id = "{{$id}}";

		$('#agenciaId option[value="'+id+'"]').attr("selected", "selected");

		$("#agenciaId").select2();

		if(id != 0){
			$("#botonExcel").prop("disabled", false);
		}else{
			$("#botonExcel").prop("disabled", true);
		}

		function cargar(idReserva){
			$("#detalle_id").val(idReserva);
			$("#comment").val('');
			botonComentario();
		}

		$("#agenciaId").change(function(){
			$("#botonExcel").prop("disabled", false);
			idAgencia = $(this).val();
			$.blockUI({
		            centerY: 0,
		            message: "<h2>Procesando...</h2>",
		                css: {
		                    color: '#000'
		                    }
		                });
			$.ajax({
					type: "GET",
					url: "{{route('mc.getListadoExtracto')}}",
					dataType: 'json',
					data: {
						dataAgencia: idAgencia
			       			},
					success: function(rsp){
								$.unblockUI();
								if(rsp[0]){
									var oSettings = $('#listado').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();

										for (i=0;i<=iTotalRecords;i++) {
											$('#listado').dataTable().fnDeleteRow(0,null,true);
										}
										$.each(rsp[0].extracto_detalles, function (key, item){
								            if(item.match == 1){
								            	var match = 'SI';
								            }else{
								            	var match = 'NO';
								            }
								            if(item.ok  == 1){
								                var ok = 'OK';
								            }else{
								                var ok = 'ERROR';
								            }

								            switch(item.moneda_id) {
						                        case 143:
						                            moneda = 'USD';
						                            break;
						                        case 111:
						                            moneda = 'PYG';
						                            break;
						                        case 43:
						                            moneda = 'EUR';
						                            break;
						                        case 21:
						                            moneda = 'BRL';
						                            break;
						                        case 0:
						                            monedaF = '';
						                            break;											
											}
								            switch(item.moneda_factour_id) {
						                        case 143:
						                            monedaF = 'USD';
						                            break;
						                        case 111:
						                            monedaF = 'PYG';
						                            break;
						                        case 43:
						                            monedaF = 'EUR';
						                            break;
						                        case 21:
						                            monedaF = 'BRL';
						                            break;
						                        case 0:
						                            monedaF = '';
						                            break;											
						                    }

								            var monedas = moneda+"/"+monedaF;

								            if(item.estado_proforma != ""){
								            	estadosArray = item.estado_proforma.split(',');
												stringEstados = "";
												contador = 0;
												$.each(estadosArray, function (key, item){
													switch (item) {
								                    case 'A':
								                        estado = 'ABIERTA';
								                        break;
								                    case 'F':
								                        estado = 'FACTURADA';
								                        break;
								                    case 'B':
								                        estado = 'BOQUEADA';
								                        break;
								                    case 'N':
								                        estado = 'ANULADA';
								                        break;
								                    case 'P':
								                        estado = 'PROCESO';
								                        break;
								                    case 'O':
								                        estado = 'OPERACIONES';
								                        break;
	    							                } 	
	    							                if(contador == 0){
											               	stringEstados += estado;   
											            }else{
											                stringEstados += "/"+estado;
											            }
											        contador = contador+1; 
	    							            })    
    							            }else{
    							            	 stringEstados ="";
    							            }    

							                var monto = round(item.saldo, 2)+"/<b>"+round(item.monto_factour, 2)+"</b>";
							                if(item.ok != 1){
							                	if(item.verificado != 1){
									                var observacion = '<div id="boton'+item.id+'"><a onclick="cargar('+item.id+')" data-toggle="modal" href="#requestModal" class="btn-mas-request"><i class="fa fa-pencil-square-o fa-lg"></i></a></div>';
							                	}else{
												    var observacion = '<a onclick="cargarComentario('+item.id+')" data-toggle="modal" href="#requestComentario" class="btn-mas-request"><img alt="" style="width: 20px;" src="images/icon/2.png"></a>';
							                	}
							                }else{
							                	 var observacion = '';
							                }
					                            		

									        var dataTableRow = [
																item.id,
																'<b>'+item.localizador+'</b>',
																monto,
																monedas,
																item.n_proforma,
																'<b>'+stringEstados+'</b>',
																item.factura_factour,
																item.pasajero_factour,
																'<b>'+match+'</b>',
																'<b>'+ok+'</b>',
																'<b>'+item.comentario+'</b>',
																observacion
																];

											var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
											// set class attribute for the newly added row 
											var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
											// and parse the row:
											var nTds = $('td', nTr);
										})	
										botonComentario();
								}else{
									console.log('No existe');
								}		
							}
 					});
		});
		
		$("#botonExcel").on("click", function(e){
				$("#botonExcel").prop("disabled", true);
				e.preventDefault();
    			$('#frmAgencias').attr('action',"{{route('generarExcelExtracto', ['id' =>'"+$("#agenciaId").val()+"'])}}").submit();
    			$("#botonExcel").prop("disabled", false);
		});

		function round(value, exp) {
		  if (typeof exp === 'undefined' || +exp === 0)
		    return Math.round(value);

		  value = +value;
		  exp = +exp;

		  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
		    return NaN;

		  // Shift
		  value = value.toString().split('e');
		  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

		  // Shift back
		  value = value.toString().split('e');
		  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
		}


		function botonComentario(){
			$("#btnAceptarProforma").click(function(){	
				var dataString = $("#frmProforma").serialize();
				$.ajax({
						type: "GET",
						url: "{{route('mc.detallesExtracto')}}",
						dataType: 'json',
						data: dataString,
						success: function(rsp){
							$("#boton"+rsp.valor).empty();
							$("#requestModal").modal('hide');
							$("#requestModal").modal('hide');
							$("#boton"+rsp.valor).html('<a onclick="cargarComentario('+rsp.valor+')" data-toggle="modal" href="#requestComentario" class="btn-mas-request"><img alt="" style="width: 20px;" src="images/icon/2.png"></a>')
						}
				});	
			});	
		}


		function cargarComentario(idComentario){
			$.ajax({
					type: "GET",
					url: "{{route('mc.getComentarios')}}",
					dataType: 'json',
					data: {
						idComentario: idComentario
			       			},
					success: function(rsp){
								$("#commentUsuario").html('<h4>'+rsp.mensaje+'</h4');
							}
 					});

		}
			
	</script>
@endsection