
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	  <link rel="stylesheet" href="{{asset('mC/css/select.dataTables.min.css')}}"> 
@endsection
@section('content')

<style type="text/css">
	

	.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

@-moz-document url-prefix() {
  fieldset { display: table-cell; }
}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	.select2-container *:focus {
        outline: none;
    }


    .correcto_col { 
		height: 74px;
     }

	 input.form-control:focus ,.select2-container--focus , button:focus{
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

	.input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
</style>


<section id="base-style">

	<div class="card" style="border-radius: 14px;">
		<div class="card-header" style="border-radius: 14px;">
			<h4 class="card-title">Vendedor Rentabilidad</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
                    <li class="nav-item">
						<a class="nav-link active" id="tabConfirm" data-toggle="tab" aria-selected="true" href="#pago"><b><i
									class="fa fa-fw fa-file"></i> Resumen</b></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tabHome" data-toggle="tab" aria-selected="true"
							href="#home"><b><i class="fa fa-fw fa-dollar"></i> Detalle </b></a>
					</li>
				</ul>
				{{--======================================================
							LISTA PAGO PROVEEDOR
				====================================================== --}}

				<div class="tab-content mt-1">
					<section id="home" class="tab-pane" role="tabpanel">
                        <div class="col-12 mt-1 mb-1">
								<button type="button" id="botonExcels" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
						</div>

						<div class="table-responsive">
							<table id="listadoDetalle" class="table table-hover table-condensed nowrap" style="width: 100%;">
								<thead>
									<tr>
                                        <th>Vendedor</th>
										<th>Moneda</th>
										<th>Total Factura</th>
										<th>Total Costos</th>
										<th>Total Renta</th>
									</tr>
								</thead>
								<tbody style="text-align: left">
								</tbody>
								<tfoot>
								</tfoot>
							</table>
						</div>
					</section>
					{{--======================================================
						VISTA PAGO
						====================================================== --}}
					<section id="pago" class="tab-pane  active" role="tabpanel">
                        <form id="consultaCuenta" autocomplete="off">
                                <div class="row">
                                    <div class="col-6 col-sm-4 col-md-3">
                                        <div class="form-group">
                                            <label>Vendedor</label>
                                            <select class="form-control select2" name="idVendedor" id="idVendedor"
                                                tabindex="1" style="width: 100%;">
                                                <option value="">Seleccione un Vendedor</option>
                                                @foreach ($vendedores as $vendedor)
												    <option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
											    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-3">
                                        <div class="form-group">
                                            <label>Desde / Hasta Fecha de Facturacion.</label>			
                                                <div class="input-group">
                                                <div class="input-group-prepend" style="">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="text" class = "Requerido form-control input-sm" id="fecha_facturacion" name="fecha_facturacion" tabindex="10"/>
                                            </div>
                                        </div>	
								    </div> 
                                    <div class="col-6 col-sm-4 col-md-3">
                                        <div class="form-group">
                                            <label>Moneda</label>
                                            <select class="form-control select2" name="idMoneda" id="idMoneda" tabindex="2"
                                                style="width: 100%;">
                                                <option value="">Todos</option>
                                                @foreach ($currency as $div)
                                                    <option value="{{$div->currency_id}}">{{$div->currency_code}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-3">
                                        <div class="form-group">
                                            <label>Negocio </label>
                                            <select class="form-control select2" name="negocio" id="negocio" style="width: 100%;">
                                                    <option value="">Seleccione Negocio</option>
                                                    @foreach($negocios as $key=>$negocio)
                                                        <option value="{{$negocio->id}}">{{$negocio->descripcion}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 mt-12 mb-12">
                                        <button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
                                        <button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
                                        <button tabindex="10" class="pull-right btn btn-info btn-lg mr-1" id="btnBuscar" type="button"><b>Buscar</b></button>
                                    </div>
                                </div>
                            </form>
							<div class="table-responsive">
							<table id="listadoResumen" class="table table-hover table-condensed nowrap" style="width: 100%;">
								<thead>
									<tr>
										<th>Vendedor</th>
										<th>Moneda</th>
										<th>Total Factura</th>
										<th>Total Costos</th>
										<th>Total Renta</th>
									</tr>
								</thead>
								<tbody style="text-align: left">
								</tbody>
							</table>
						</div>
					</section>
				</div>
			</div>
		</div>

</section>
{{-- BASE STYLE --}} 


    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script type="text/javascript" src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.min.js"></script>

	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
	});


    $('input[name="fecha_facturacion"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="fecha_facturacion"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });
		$('input[name="fecha_facturacion"]').val('');

    const formatter = new Intl.NumberFormat('de-DE', 
		{
			currency: 'USD',
			minimumFractionDigits: 2
		});

	
    $('#btnBuscar').click(()=>{
		tableResumen();
        tableDetallle();
	});
    
    function  tableResumen(){
			let resp;
			let form = $('#consultaCuenta').serializeJSON({
				customTypes: customTypesSerializeJSON
			});

			tableListado = $("#listadoResumen").DataTable({
				destroy: true,
				pageLength: 10,
				ajax: {
			 		url: "{{route('getResumenVendedorIncentivo')}}",
			 		data: form,
			 		error: function (jqXHR, textStatus, errorThrown) {
						$.toast({	
								heading: '<b>Atención</b>',
								position: 'top-right', 
								text: 'Ocurrio un error en la comunicación con el servidor.',
								hideAfter: false,
								icon: 'error'
							});
			 		}
			 	},
				 responsive: {
						details: false
					},
				"columns": [
                        {data: (x)=>{
							console.log(x);
							return x.vendedor_nombre+" "+x.vendedor_apellido;
						}, defaultContent: ''},

						{data: 'currency_code', defaultContent: ''},
						{data: (x)=>{
							return formatter.format(parseFloat(x.total));
						}, defaultContent: ''},
						{data: (x)=>{ 
							return formatter.format(parseFloat(x.costo));  
							}, defaultContent: ''},
                        {data: (x)=>{ 
							return formatter.format(parseFloat(x.renta));   
							}, defaultContent: ''}

/*                      { "data": function(x)
						{
							var btn = `<button onclick="cargarDetalle(`+x.id_cliente+`,'`+x.id_moneda+`')" class="btn btn-info" type="button">
							<i class="fa fa-fw fa-search"></i></button>`;

							return btn;
						} 
					}*/

				 	],
				 	"createdRow": function ( row, data, index ) {
								 
					}	
				});

		}

    function  tableDetallle(){
		//alert('UDhgk');
		const formatter = new Intl.NumberFormat('de-DE', {currency: 'USD',minimumFractionDigits: 0});
		let resp;
		let form = $('#consultaCuenta').serializeJSON({
			customTypes: customTypesSerializeJSON
		});

		tableListado = $("#listadoDetalle").DataTable({
						destroy: true,
						pageLength: 10,
                        processing: true,
                      //  searching: false,
                        bLengthChange: false,
                        "ajax": {
							"url": "{{route('getDetalleVendedorIncentivo')}}",
							"data": form,
                        },
						"columns": [
							{data: (x)=>{
								console.log(x);
								return x.vendedor_nombre+" "+x.vendedor_apellido;
							}, defaultContent: ''},

							{data: 'currency_code', defaultContent: ''},
							{data: (x)=>{
								return formatter.format(parseFloat(x.total));
							}, defaultContent: ''},
							{data: (x)=>{ 
								return formatter.format(parseFloat(x.costo));  
								}, defaultContent: ''},
							{data: (x)=>{ 
								return formatter.format(parseFloat(x.renta));   
								}, defaultContent: ''}
				 		]
                })
                $('#listadoDetalle tbody').on('click', 'td.dt-body-right', function () {
                    var tr = $(this).closest('tr');
                    var row = $('#listadoDetalle').DataTable().row(tr);
                                    if (row.child.isShown()) {
                                            // This row is already open - close it
                                            row.child.hide();
                                            tr.removeClass('shown');
                                        }
                                        else {

                                            // Open this row
                                            row.child(format(row.data())).show();
                                            tr.addClass('shown');
                                        }
                })
                $('a[href="#detalle"]').click();	
                desplegarTabla($('#listadoDetalle').DataTable());
		}

/////////////////////////////////////////////////////////////////////////////////////////////////
			function format(d) {
				var tabla = '<table cellpadding="6" cellspacing="0" border="0" style="background-color: #eaebee; width: 100%"> ';
					tabla+= '<tr>';
					tabla+= '<th style="width: 150px;">Cliente</th>';
					tabla+= '<th style="width: 150px;">Nro. Factura	</th>';
					tabla+= '<th style="width: 150px;">Proforma</th>';
					tabla+= '<th style="width: 150px;">Moneda</th>';
					tabla+= '<th style="width: 150px;">Total Factura</th>';
					tabla+= '<th style="width: 100px;">Total Costos</th>';
					tabla+= '<th style="width: 100px;">Total Renta</th>';
					tabla+= '</tr>';
                    indicador = 0;
					$.each(d.detalles, function (key, item){
					    tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.cliente+'</td>';
						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.nro_factura+'</td>';
						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.id_proforma+'</td>';
                        tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.moneda+'</td>';
						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+formatter.format(parseFloat(item.total))+'</td>';
                        tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+formatter.format(parseFloat(item.total_costos))+'</td>';
						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+formatter.format(parseFloat(item.renta))+'</td>';
    					tabla+= '</tr>';
					})	 
					tabla+= '</table>';
				    return tabla;  
				}
////////////////////////////////////////////////////////////////////////////////////////////////

		 function  desplegarTabla(table){
				$('#listadoDetalle').on('init.dt', function(e, settings){
					   var api = new $.fn.dataTable.Api( settings );
					   api.rows().every( function () {
					      var tr = $(this.node());
					      this.child(format(this.data())).show();
					      tr.addClass('shown');
					   });
					});
		 }


		 $("#botonExcel").on("click", function(e){ 
            e.preventDefault();
            $('#consultaCuenta').attr('method','post');
			$('#consultaCuenta').attr('action', "{{route('generarExcelIncentivoVendedor')}}").submit();
		});

		$("#botonExcels").on("click", function(e){ 
            e.preventDefault();
            $('#consultaCuenta').attr('method','post');
            $('#consultaCuenta').attr('action', "{{route('generarExcelIncentivoVendedorDetalle')}}").submit();
        });




</script>

@endsection