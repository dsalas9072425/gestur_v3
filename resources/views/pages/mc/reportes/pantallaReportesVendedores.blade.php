@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
<style>
	th.dt-center, td.dt-center { text-align: center; }
</style>
@endsection
@section('content')
<div style="margin-left: 1%; margin-right: 2%;">
	<div class="row" style="height: 98%;">
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div style="border: 1px solid #999999; border-radius: 5px; margin-left: 15px; padding-left: 5px;padding-right: 5px; background-color: white;max-height: 800px;min-height: 800px;">
				<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">ESTADISTICAS POR VENDEDOR</h1>
				<div id= "mensaje"></div>
				<div class="table-responsive">
					<table id="table" class="table">
						<thead>
							<tr style="color:#e2076a">
								<th style="height: 50px;">Vendedor</th>
								<th>Facturados</th>
								<th>Reservados</th>
								<th>Proyeccion</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						<tfoot>
							<th style="height: 50px;"><h4><b>TOTALES</b></h4></th>
								<th id="totalFacturados"></th>
								<th id="totalReservados"></th>
								<th id="totalProyeccion"></th>
						</tfoot>	
					</table>
				</div>		
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div class="row" style="height: 50%;">
				<div style="height: 50%;border: 1px solid #999999;border-radius: 5px;margin-right: 10px;padding-left: 5px;padding-right: 5px;background-color: white;margin-left: 10px;max-height: 400px;min-height: 400px;margin-bottom: 1px;">
					<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">ESTADISTICAS CUPOS</h1>
					<div id="actual" style="min-width: 230px; height: 320px; max-width: 520px; margin: 0 auto"></div>
				</div>
			</div>
			<div class="row" style="height: 50%;">
				<div style="height: 50%;border: 1px solid #999999;border-radius: 5px;margin-right: 10px;padding-left: 5px;padding-right: 5px;background-color: white;margin-left: 10px;max-height: 400px;min-height: 400px;margin-top: 1px;">
					<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">PROYECCION Y METAS</h1>
					<div id="cuadro" style="min-width: 260px; height: 320px; max-width: 700px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection 
@section('scripts')
	@include('layouts/scripts')
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script>
	$('#table').dataTable({
				        paging:false,
				        bFilter:false,
				        ordering:false,
				        bInfo: false,
				        columnDefs: [
								        {"className": "dt-center", "targets": "_all"}
								      ]
    					});
	cargaDatos();
	setInterval(cargaDatos, 10000);
	function cargaDatos(){	
				$.ajax({
						type: "GET",
						url: "{{route('mc.getDatosReporte')}}",
						dataType: 'json',
						success: function(rsp){
							if(rsp.estado = 'OK'){
								var totalFacturados = 0;
								var totalReservados = 0;
								var totalProyeccion = 0;
								$('#mensaje').html('<h4 style="margin-left: 10px; color: #FF0080"><b>'+rsp.mensaje+'</b></h4>');
								var oSettings = $('#table').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();
								for (i=0;i<=iTotalRecords;i++) {
									$('#table').dataTable().fnDeleteRow(0,null,true);
								}

								$.each(rsp.vendedoresCupos, function(keys,values){
									totalFacturados = parseFloat(totalFacturados)+parseFloat(values.facturados);
									totalReservados = parseFloat(totalReservados)+parseFloat(values.reservados);
									totalProyeccion = parseFloat(totalProyeccion)+parseFloat(values.proyeccion);
									var dataTableRow = [
														"<b>"+values.vendedor+"</b>",
														values.facturados,
														values.reservados,
														values.proyeccion
														];
									var newrow = $('#table').dataTable().fnAddData(dataTableRow);
														// set class attribute for the newly added row 
									var nTr = $('#table').dataTable().fnSettings().aoData[newrow[0]].nTr;
									$('#table').dataTable().fnSort( [ [1,'desc'] ] );	
								});
								
								$('#totalFacturados').html('<h4><b>'+totalFacturados+'</b></h4>');
								$('#totalReservados').html('<h4><b>'+totalReservados+'</b></h4>');
								$('#totalProyeccion').html('<h4><b>'+totalProyeccion+'</b></h4>');

								$('#actual').html("");
								$.each(rsp.estadisticasCupos, function(key,value){
								$('#actual').highcharts({
								                    chart: {
								                        type: "pie"
								                    },
								                    title: {
								                        text: ""
								                    },
								                    colors: ['#FF0080','#00007c','#983583'],
								                    xAxis: {
								                        type: 'category',
								                        allowDecimals: false,
								                        title: {
								                            text: ""
								                        }
								                    },
								                    yAxis: {
								                        title: {
								                            text: "Scores"
								                        }
								                    },
								                    series: [{
												        name: 'Share',
												        colorByPoint: true,
												        data: [
												            { name: 'DISPONIBLES (<h2 style="#FF0080">'+value.cuposDisponibles+'</h2>)', y: value.cuposDisponibles},
												            { name: 'FACTURADOS (<h2 style="#FF0080">'+value.cuposFacturados+'</h2>)', y: value.cuposFacturados},
												            { name: 'RESERVADOS (<h2 style="#FF0080">'+value.cuposReservados+'</h2>)', y: value.cuposReservados}
												        ]
												    }]
												});
												var cuposFacturados_json = new Array();
												var cuposReservados_json = new Array();
												var cuposTotal_json = new Array();
												var categoria_json = new Array();
												for (i = 0; i < 3; i++) {
													switch(i) {
								                        case 0:
								                        	categoria_json.push(['']);
								                            cuposFacturados_json.push([0]);
															cuposReservados_json.push([0]);
															cuposTotal_json.push([parseFloat(value.cuposTotal)]);
								                            break;
								                        case 1:
								                        	cupos = parseFloat(value.cuposReservados) + parseFloat(value.cuposFacturados);
								                        	cuposFacturados_json.push([value.cuposFacturados]);
															cuposReservados_json.push(cupos);
															cuposTotal_json.push([parseFloat(value.cuposTotal)]);
								                            break;
													}
												}
												$('#cuadro').highcharts({
															    chart: {
															        type: 'spline'
															    },
															    title: {
															        text: ''
															    },
															    subtitle: {
															        text: ''
															    },
															    xAxis: {
															        categories: categoria_json
															    },
															    yAxis: {
															        title: {
															            text: 'Montos'
															        },
															        labels: {
															            formatter: function () {
															                return this.value ;
															            }
															        }
															    },
															    tooltip: {
															        crosshairs: true,
															        shared: true
															    },
															    plotOptions: {
															        spline: {
															            marker: {
															                radius: 4,
															                lineColor: '#00007c',
															                lineWidth: 1
															            }
															        }
															    },
															    series: [
																	{
																        name: 'Cupos Proyectados',
																        marker: {
																            symbol: 'circle'
																        },
																        data: cuposReservados_json
																    },																	{
																        name: 'Cupos Facturados',
																        marker: {
																            symbol: 'circle'
																        },
																        data: cuposFacturados_json
																    },											
																    {
																        name: 'Cupos Totales',
																        marker: {
																            symbol: 'diamond'
																        },
																        data: cuposTotal_json
																    }
															    ]
														});



											});	
							}
						}
				})
	}						
	</script>
@endsection