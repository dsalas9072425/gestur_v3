
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">

	.ui-datepicker-calendar {
    display: none;
    }

	input.form-control:focus ,.select2-container--focus , button:focus{
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

</style>






	<section id="home" class="tab-pane fade in active content"> 
	  <div class="box box-default">
			
   		<form id="formAsiento" >
				<div class="box-body">
		            <div class="box-header">
		             <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Reporte Asiento</h1>
					</div>  
						      
					

					<div class="col-xs-12 col-sm-6 col-md-3" style="height: 74px;">  
						<div class="form-group">
						   <label>Fecha</label>						 
						   <div class="input-group">
							   <div class="input-group-addon">
								   <i class="fa fa-calendar"></i>
							   </div>
								  <input type="text" autocomplete="false" name="periodo:p_date" tabindex="1" id="asiento_fecha" class="form-control date-picker" value="" autocomplete="off"  />
						   </div>
					   </div>	
				   </div>

				   <div class="col-xs-6 col-sm-3 col-md-3" style="height: 74px;">
						<div class="form-group">
							 <label>Usuario</label>
							<select class="form-control select2" name="id_usuario"  id="" tabindex="2" style="width: 100%;">
								<option value="">Todos</option>
								
						   </select>
					   </div>
				   </div>


				   <div class="col-xs-6 col-sm-3 col-md-3" style="height: 74px;">
						<div class="form-group">
							 <label>Tipo Operación</label>
							<select class="form-control select2" name="tipo_operacion"  id="" tabindex="3" style="width: 100%;">
								<option value="">Todos</option>
								 
								   <option value="COMPRAS">COMPRAS</option>
								   <option value="PAGO">PAGO</option>
								   <option value="CLIENTES">CLIENTES</option>
								   <option value="COBRO">COBRO</option>
							   
						   </select>
					   </div>
				   </div>
   			
					<div class="col-xs-12 ">
						<a  onclick ="tableAsiento()" class=" text-center btn btn-info btn-lg pull-right" style="margin-top: 20px;" role="button"><b>Buscar</b></a>

		  			</div>
		
							

				</div>
		</form>  	
		

			<div class="box-body">
            	<div class="table-responsive table-bordered">
	              <table id="listadoAsiento" class="table" style="width:100%">
	                <thead>
					  <tr>
						<th>ID</th>
					  	<th>Documento</th>
					  	<th>Usuario</th>
						<th>Operación</th>
						<th>Fecha</th>
						<th>Debe</th>
						<th>Haber</th>
						<th></th>
		              </tr>
                    </thead>
                    
	                <tbody style="text-align: center; font-weight: 800;">
			        </tbody>
	              </table>
				</div> 
			</div>	




	  </div>		
    </section>
    <!-- ASIENTO -->

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
<script type="text/javascript">




$(function() {
	$('.select2').select2();

	calendar();

});

	

function initAsiento(){
	tableAsiento();
}	
		


			function tableAsiento(){



				$.blockUI({
												centerY: 0,
												message: "<h2>Procesando...</h2>",
												css: {
													color: '#000'
												}
											});

				// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
				setTimeout(function (){

							
					table =   $('#listadoAsiento').DataTable({
									"destroy": true,
									"ajax": {
											data: $('#formAsiento').serializeJSON({
												customTypes: customTypesSerializeJSON
											}),
											url: "{{route('ajaxReporteAsiento')}}",
											type: "GET",
									error: function(jqXHR,textStatus,errorThrown){

								$.toast({
									heading: 'Error',
									text: 'Ocurrio un error en la comunicación con el servidor.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
								});
									$.unblockUI();

								}
										
								},"columns":[
											{data:"id"},
											{data:"nro_factura"},
											{data:"usuario_n"},
											{data:"operacion"},
											{data:"fecha_format"},
											{data:function(x){ 
												return (!isNaN(parseFloat(x.debe))) ? formatter.format(parseFloat(x.debe)): 0;}},
											{data:function(x){ return (!isNaN(parseFloat(x.haber))) ? formatter.format(parseFloat(x.haber)): 0;}},
											{data: function(x){
												return `<a onclick="detalleAsiento(${x.id})" class='btn btn-danger' style='background: #e2076a !important;padding-left: 6px;padding-right: 6px;' role='button'><i class="fa fa-fw fa-search"></i></a>`;
											}}
											],
									"createdRow": function ( row, data, index, aData ) {
								
									}              
								}).on('xhr.dt', function ( e, settings, json, xhr ){
									$.unblockUI();
										});
					
									}, 300);

		}//function




		function detalleAsiento(id){
			$('#id_asiento_input').val(id);
			tableAsientoDetalle();
			$('#tabDetalle').tab('show');
		}


		function calendar() {

//CALENDARIO OPCIONES EN ESPAÑOL

let locale = {
	format: 'DD/MM/YYYY',
	cancelLabel: 'Limpiar',
	applyLabel: 'Aplicar',
	fromLabel: 'Desde',
	toLabel: 'Hasta',
	customRangeLabel: 'Seleccionar rango',
	daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
	monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
		'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
		'Diciembre'
	]
};


$('#asiento_fecha').daterangepicker({
	timePicker24Hour: true,
	timePickerIncrement: 30,
	locale: locale
});
$('#asiento_fecha').val('');
$('#asiento_fecha').on('cancel.daterangepicker', function (ev, picker) {
	$(this).val('');
});



}






	

</script>

@endsection


