@extends('masters')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
@endsection
@section('content')
	<div style="margin-left: 1%; margin-right: 2%;">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div style="height: 100%; border: 1px solid #999999; border-radius: 5px; margin-left: 15px; padding-left: 5px;padding-right: 5px; background-color: white;">
				<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">EMITIDOS VS FACTURADOS</h1>
					<div id="cuadro" style="min-width: 90%;height: 380px;max-width: 90%;margin: 0 auto;"></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div style="height: 100%; border: 1px solid #999999; border-radius: 5px;margin-right: 15px; padding-left: 5px;padding-right: 5px; background-color: white;">
				<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">DISPONIBLES - EN PROFORMAS</h1>
				<div class="table">
					<div class="row" style="margin-top: 15%; margin-bottom: 15%">
						<div class="col-md-1"></div>
						<div class="col-md-5">
							<span><b>Disponible</b></span>
							<div class="info-box bg-yellow" style="height: 160px;">
					            <span class="info-box-icon" style="height: 160px;"><i class="ion ion-ios-pricetag-outline" style="padding-top: 55%;"></i></span>
					            <div class="info-box-content">
					              	<span class="info-box-number" id="numero1" style="font-size: 90px;"></span>
					            </div>
				          	</div>
				        </div>   
						<div class="col-md-5">
							<span><b>En Proformas</b></span>
							<div class="info-box bg-red" style="height: 160px; background-color: red;">
					            <span class="info-box-icon" style="height: 160px;"><i class="ion ion-ios-pricetag-outline" style="padding-top: 55%;"></i></span>
					            <div class="info-box-content">
					              	<span class="info-box-number" id="numero2" style="font-size: 90px;"></span>
					            </div>
				          	</div>
				        </div> 
				        <div class="col-md-1"></div>  
					</div>	
				</div>		
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div style="height: 50%; border: 1px solid #999999; border-radius: 5px; margin-left: 15px; padding-left: 5px;padding-right: 5px; background-color: white;">
				<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">FACTURACIÓN</h1>
				<div class="row">
					<div class="col-md-12">
						<div id="container" style="min-width: 310px; height: 250px; max-width: 500px; margin: 0 auto"></div>
					</div>	
				</div> 
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6" style="padding:5px;">
			<div style="height: 50%; border: 1px solid #999999; border-radius: 5px;margin-right: 15px; padding-left: 5px;padding-right: 5px; background-color: white;">
				<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 5%;margin-bottom: 1%;font-weight: bold; color: #072235">FACTURAS - VENCIMIENTO</h1>
				<div id="cointainer" style="min-width: 60%;height: 250px;max-width: 90%;margin: 0 auto;"></div>
			</div>
		</div>
	</div>
	</div>
@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>
		<script>
		cargaDatos();
		setInterval(cargaDatos, 70000);
		function cargaDatos(){	

			$.ajax({
					type: "GET",
					url: "{{route('mc.getPantallaDisponible')}}",
					dataType: 'json',
					success: function(rsp){
									$("#numero1").html("");
									$("#numero1").append(rsp.RestTkTDispProfOut.Tickets.disponibles)
									$("#numero2").html("");
									$("#numero2").append(rsp.RestTkTDispProfOut.Tickets.proforma)
									$("#numero1").addClass('c100:hover > spa');
									$("#numero2").trigger('mouseover');
								}
					});

			$.ajax({
					type: "GET",
					url: "{{route('mc.getPantallaEmitidosFacturados')}}",
					dataType: 'json',
					success: function(rsp){
							//console.log(rsp);
							var categoria_json = new Array();
							var facturados_json = new Array(); 
							var emitidos_json = new Array(); 
							$.each(rsp, function(keys,values){
								$.each(values.Emitidos, function(keys,valuesEmitido){
									var fecha = valuesEmitido.fecha.split('-');
									emitidos_json.push([valuesEmitido.cantidad]);
									categoria_json.push([fecha[2]+"/"+[fecha[1]]+"/"+fecha[0]]);
								})	
								$.each(values.Facturados, function(keys,valuesFacturado){
									facturados_json.push([valuesFacturado.cantidad]);
								})	
							})	

							$('#cuadro').highcharts({
								chart: {
										type: 'line'
										},
								title: {
										text: ''
										},
								colors: ['#FF0080', '#00007c'],
								subtitle: {
										text: ''
										},
								xAxis: {
										categories: categoria_json
										},
								yAxis: {
										title: {
												text: ''
												},
										labels: {
												formatter: function () {
												                return this.value ;
												            }
												}
										},
								tooltip: {
										crosshairs: true,
										shared: true
										},
								plotOptions: {
											line: {
												   dataLabels: {
												                enabled: true
												            },
												            enableMouseTracking: false
												    }
											},
											series: [
													{
													name: 'Emitidos',
													marker: {
													            symbol: 'circle'
													        },
													data: emitidos_json
													},
													{
													name: 'Facturados',
													marker: {
													            symbol: 'circle'
													        },
													data: facturados_json
													}
											]
										});
									}
								})
			
			$.ajax({
					type: "GET",
					url: "{{route('mc.getPantallaFacturados')}}",
					dataType: 'json',
					success: function(rsp){
							//console.log(rsp);
							var categoria_json = new Array();
							var emitidos_json = new Array(); 
							$.each(rsp.RestFactDiaOut.Facturados, function(keys,valuesEmitido){
									var fecha = valuesEmitido.fecha.split('-');
									var monto = valuesEmitido.monto;
									emitidos_json.push([parseFloat(monto)]);
									categoria_json.push([fecha[2]+"/"+[fecha[1]]+"/"+fecha[0]]);
								})	

							$('#container').highcharts({
								chart: {
										type: 'bar'
										},
								title: {
										text: ''
										},
								colors: ['#00007c'],
								subtitle: {
										text: ''
										},
								xAxis: {
										categories: categoria_json
										},
								yAxis: {
										title: {
												text: ''
												},
										labels: {
												formatter: function () {
												                return this.value ;
												            }
												}
										},
								tooltip: {
										crosshairs: true,
										shared: true
										},
								plotOptions: {
											bar: {
										            dataLabels: {
										                enabled: true
										            }
										        }
											},
											series: [
													{
													name: 'Facturados',
													marker: {
													            symbol: 'circle'
													        },
													data: emitidos_json
													}
											]
										});
									}
								})
			
			$.ajax({
					type: "GET",
					url: "{{route('mc.getPantallaCobros')}}",
					dataType: 'json',
					success: function(rsp){
									var categoria_json = new Array(); 
									var montocob_json = new Array(); 
									var montopnd_json = new Array(); 
									var montoven_json = new Array(); 
									$.each(rsp.RestTableroCobrosOut.Vencimientos, function(key,value){
										var fecha = value.fecha.split('-');
		                       			categoria_json.push([fecha[2]+"/"+[fecha[1]]+"/"+fecha[0]]);
		                       			montocob_json.push([parseFloat(value.montocob)]);
		                       			montopnd_json.push([parseFloat(value.montopnd)]);
		                       			montoven_json.push([parseFloat(value.montoven)]);
		                    		})
								  $('#cointainer').html("");
								  $('#cointainer').highcharts({
						                   chart: {
													type: 'bar'
													},
											title: {
													text: ''
													},
											colors: ['#FF0080','#ff0000','#00007c'],
											subtitle: {
													text: ''
													},
											xAxis: {
													categories: categoria_json
													},
											yAxis: {
													title: {
															text: ''
															},
													labels: {
															formatter: function () {
															                return this.value ;
															            }
															}
													},
											tooltip: {
													crosshairs: true,
													shared: true
													},
											plotOptions: {
														bar: {
													            dataLabels: {
													                enabled: true
													            }
													        }
														},

						                    series: [
												{
								                    name: 'Monto Venta',
							                        data: montoven_json
						                   		 },
						                    	{
								                    name: 'Monto Pendiente',
							                        data: montopnd_json
						                   		 },
						                   		 {
								                    name: 'Monto Cobrado',
							                        data: montocob_json
						                   		 },
						                    	
						                    ]
						                }); 
								 }
						 });
				}

	</script>		
@endsection