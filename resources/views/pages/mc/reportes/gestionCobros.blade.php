@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}
	#ocultar {
		display: none;
	}

	.alert-danger {
	  color: #111;
	  background-color: #ef3434a6;
	  border-color: #ebccd1;
	}

	.ultra-alert-danger {
	  color: red;
	  font-weight: bold;
	/*  background-color: #a94442;
	  border-color: #ebccd1;*/
	}

	.color-warning {
	  color: #e6c619;
	   font-weight: bold;
	 /* background-color: #ffcc1347;
	  border-color: #faebcc;*/
	}

	.color-success {
	  color: green;
	   font-weight: bold;
	/*  background-color: #a94442;
	  border-color: #ebccd1;*/
	}

   .color-normal {
	  color: black;
	   font-weight: bold;
	/*  background-color: #a94442;
	  border-color: #ebccd1;*/
	}


</style>

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Estado de Facturas</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
    				<form id="consultaFactura">
    					<div class="row">

    						<div class="col-12 col-sm-4">
			 					<div class="form-group">
						            <label>Tipo Persona</label>
									<select class="form-control select2" name="tipo_persona_id[]" multiple="multiple" id="tipo_persona_id" tabindex="5" >
										<option value="">Todos</option>
										@foreach($tipoPersona as $persona)
										<option value="{{$persona->id}}" >{{$persona->denominacion}}</option>
										@endforeach
									</select>
						        </div>
							</div>

								<div class="col-12 col-sm-4">
								        <div class="form-group">
								            <label>Cliente</label>
											<select class="form-control select2" name="cliente_id"  id="cliente" tabindex="1" >
												<option value="">Seleccione Cliente</option>
												@foreach($clientes as $cliente)
												@php
													$ruc = $cliente->documento_identidad;
													if($cliente->dv){
														$ruc = $ruc."-".$cliente->dv;
													}
												@endphp
												<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->nombre_c}} {{$cliente->denominacion_comercial}}</option>
												@endforeach
											</select>
								        </div>
									</div>
									<div class="col-12  col-md-4">
										<div class="row" style="margin-left: 0px;margin-right: 0px;">		
											<div class="col-12 col-sm-6" style="padding-right: 0px;">
													<label>Moneda</label>
													<select class="form-control select2" name="moneda_id"  id="moneda" tabindex="2" style="width: 95%;margin-right: 25px;" >
														<option value="">Seleccione Moneda</option>
														
														@foreach($monedas as $moned)
														<option value="{{$moned->currency_id}}">{{$moned->currency_code}}</option>
														@endforeach
														
													</select>
											</div>    							
											<div class="col-12 col-sm-6" style="padding-left: 25px;padding-right: 0px;">
													<label>Estado</label>
													<select class="form-control select2" name="estado"  id="estado" tabindex="3" style="width: 95%" >
														<option value="">Seleccione Estado</option>
														<option value="VENCIDO">VENCIDO</option>
														<option value="A_VENCER">A VENCER</option>
													</select>
											</div>
										</div>	
									</div>	
						            <div class="col-12  col-md-4">
									 <div class="form-group">
									 	 <label>Nro. Factura</label>
									     <input type="text" class="form-control" id="num_factura"  name="numFactura" value="" maxlength="30">
										</div>
									</div>
									<div class="col-12 col-sm-4">
								        <div class="form-group">
								            <label>Sucursal Cartera</label>
											<select class="form-control select2" name="sucursal_id"  id="sucursal_id" tabindex="4" >
												<option value="">Seleccione Sucursal</option>
												@foreach($sucursalCartera as $sucursal)
												<option value="{{$sucursal->id}}">{{$sucursal->nombre}} {{$sucursal->denominacion_comercial}}</option>
												@endforeach
												
											</select>
								        </div>
									</div>
    						
						            <div class="col-12 col-sm-4">
								        <div class="form-group">
								            <label>Usuario</label>
											<select class="form-control select2" name="usuario_id"  id="usuario_id" tabindex="5" >
												<option value="">Seleccione Opción</option>
												@foreach($usuarios as $usuario)
												<option value="{{$usuario->id}}">{{$usuario->nombre}} {{$usuario->apellido}}</option>
												@endforeach
											</select>
								        </div>
									</div>

									<div class="col-12 col-sm-4">
								        <div class="form-group">
								            <label>Tipo de Cliente</label>
											<select class="form-control select2" name="cliente_especial"  id="" tabindex="6" >
												<option value="">Todos</option>
												<option value="true">Clientes Especiales</option>
												<option value="false">Sin Clientes Especiales</option>
											</select>
								        </div>
						            </div>	
									<div class="col-12 col-sm-4">
								        <div class="form-group">
								            <label>Comercio</label>
											<select class="form-control select2" name="comercio_id"  id="comercio" tabindex="1" >
												<option value="">Seleccione comercio</option>
												<option value="0">DTP</option>
												@foreach($comercioEmpresas as $comercio)
													<option value="{{$comercio->id}}">{{$comercio->descripcion}}</option>
												@endforeach
											</select>
								        </div>
									</div>
									<div class="col-12 col-sm-4">
								        <div class="form-group">
								            <label>Estado de Cobro</label>
											<select class="form-control select2" name="estado_cobro"  id="estado_cobro" tabindex="1" >
												<option value="1">Pendiente</option>
												<option value="2">Cobrado</option>	
											</select>
								        </div>
									</div>
	            				</div>	
								
							</div>
							
							<div class="col-12">
									<button type="button" id="botonExcel" class="btn btn-success btn-lg pull-right mr-1"><b>Excel</b></button> 
									<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg text-white pull-right mr-1"><b>Limpiar</b></button>
								    <button  onclick ="consultaFactura()" class="btn btn-info btn-lg pull-right mr-1" type="button"><b>Buscar</b></button>
							</div>
							
			    </form>  	
	

            	<div class="table-responsive table-bordered mt-1">
	              <table id="listado" class="table">
	                <thead>
					  <tr>
					  	<th></th>
						<th>Cliente</th>
					  	<th>Emisión</th>
					  	<th>Vencimiento</th>
						<th>Checkin</th>
						<!--<th>Cartera</th>-->
						<th style="width: 150px">N° Factura</th>
						<th>Pasajero</th>
						<th>Usuario</th>
						<th>Moneda</th>
						<th>Total</th>
						<th>Saldo</th>
						<th>Dias</th>
						<th>Estado</th>
						<th>Especial</th>
						<th>Comercio</th>
						<th>Estado Cobro</th>
		              </tr>
	                </thead>
	                <tbody id="lista_cotizacion" style="text-align: center">
				       
			        </tbody>
	              </table>
	            </div>  
   


			</div>
		</div>
	</section>
</section>



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
<script type="text/javascript">


	$("#listado").dataTable({
		 "order": [ 1, 'asc' ]
	});
	$('.select2').select2();
    let select_2 = $('#tipo_persona_id').select2({
									multiple:true,
									//maximumSelectionLength: 2,
									placeholder: 'Todos'
								});


		function limpiar(){
			$('#cliente').val('').trigger('change.select2');
			$('#moneda').val('').trigger('change.select2');
			$('#estado').val('').trigger('change.select2');
			$('#sucursal_id').val('').trigger('change.select2');
			$('#tipo_persona_id').val('').trigger('change.select2');
		}

		function validart(data){

			if(data == undefined || data == null || data === '')
					return '';
			return data;
		}



			function validar(data){

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}



			function consultaFactura() {

				 $.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });


				var dataString = $('#consultaFactura').serialize();
				$.ajax({
						type: "GET",
						url: "{{route('consultaFacturasCobro')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });
                              $.unblockUI();

                            },

						success: function(rsp){
							$.unblockUI();

							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

							$.each(rsp.facturas, function (key, item){
								diferencia = 0;
								if(jQuery.isEmptyObject(item.diferencia_check_in) == false){
									diferencia =  item.diferencia_check_in.replace("days", "");
								}	
								var c = '';

								if(validar(item)){

							  if(validar(item.fecha_hora_facturacion)){
							    c = "<span id='ocultar'>"+item.fecha_hora_facturacion_s+"</span>"+item.fecha_hora_facturacion;		
									} else {
								c = '<span id=ocultar></span>';		
									}

							//if(item.pasajero_factour == ""){
								var pasajero = item.pasajero_n;
							///}else{
							/*	var pasajero = item.pasajero_factour;
							}
							*/
						  }

						    if(item.nombrecomercio == null){
								var comercio = 'DTP';
								id_venta = 0;
							}else{
								var comercio = item.nombrecomercio;
								id_venta = item.id_venta;
							}
							
							if(item.expedient != null && item.expedient != 0){
								var boton = '<i class="fa fa-clock-o fa-lg" style ="color: #00acd6;" aria-hidden="true"></i><div style="display: none;">5</div>';
							}else{	
								if(item.ticketcount > 0){
								    var boton = '<i class="fa fa-plane evento fa-lg" style ="color: #00acd6;" aria-hidden="true"></i><div style="display: none;">5</div>';
								}else{
									if(item.estado_cobrado_id != 40){
										if (item.dias >0){
											var boton = '<i class="fa fa-thumbs-o-up" style="color:green"></i><div style="display: none;">1</div>';
										}
										//Aprobado
										if (item.dias <= 0 && parseInt(diferencia) >= 7){
											var boton = '<i class="fa fa-exclamation-triangle" style="color:yellow"></i><div style="display: none;">2</div>';
										}
										if(item.dias <= 0 && parseInt(diferencia)< 7 && parseInt(diferencia) >= 0){
											var boton = '<i class="fa fa-exclamation-triangle" style="color:red"></i><div style="display: none;">3</div>';
										}

										if(item.dias <= 0 && parseInt(diferencia)< 0){
											//var imagenes = `{{asset('images/warning.gif')}}`;
											var boton = '<img src="../images/warning.gif" style="width: 15px; height: 20px"><div style="display: none;">0</div>';
									    }
									}else{
										var boton = '<i class="fa fa-thumbs-o-up" style="color:green"></i><div style="display: none;">1</div>';
									}    
								}
							}

        	     			if(item.id > 89999){
			     				if(id_venta == 0){
			     					btn = `<a href="{{route('verFactura',['id'=>''])}}/${item.id}" style="font-weight: bold;" class="bgRed">
                    					<i class="fa fa-fw fa-search"></i>${item.nro_factura}</a><div style="display:none;">${item.id}</div>`;		
			     				}else{
									btn = `<a href="{{route('verFacturas',['id'=>''])}}/${item.id}" style="font-weight: bold;" class="bgRed">
                    					<i class="fa fa-fw fa-search"></i>${item.nro_factura}</a><div style="display:none;">${item.id}</div>`;	
  				     				}
			     			 	
			     			} else {
			     			 	btn = `<a  class="bgRed">${item.nro_factura}</a><div style="display:none;">${item.id}</div>`;
			     			}
			     			tdBase = '';

			     			especial = '';

							if(item.cliente_especial == true){
								especial = 'SI';
							}else{
								especial = 'NO';
							}	

							if(item.estado_cobrado_id == 40){
								dias = 0;
							}else{
								dias = item.dias;	
							}
							
							if(item.estado_cobrado_id == 40){
								estado = "COBRADO";
							}else{
								estado = item.venc_estado;
							}			

						 //console.log(item);
						var dataTableRow = [
							boton,
							item.cliente_n,
							c,
							item.vencimiento,
							item.check_in,
							//validart(item.cartera_n)+' '+validart(item.cartera_dc),
							btn,
							pasajero,
							item.usuario_nombre+' '+item.usuario_apellido,
							item.moneda,
							item.total_factura,
							item.saldo_factura,
							dias,
							estado,
							especial,
							comercio,
							item.estado_cobro_denominacion
											];

						var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
						var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
						$('td',$('#listado').dataTable().fnSettings().aoData[newrow[0]].anCells[0]).addClass('alert-error');
						var nTds = $('td', nTr);
						//nTds.style('width', .css("background-color"););
								//Pendiente
								if(item.estado_cobrado_id != 40){
									if (item.dias >0){
										nTds.addClass('');
									}
									//Aprobado
									if (item.dias <= 0 && parseInt(diferencia) >= 7){
										nTds.addClass('color-warning');
									}
									if(item.dias <= 0 && parseInt(diferencia) < 7 && parseInt(diferencia) >= 0){
										nTds.addClass('ultra-alert-danger');
									}

									if(item.dias <= 0 && parseInt(diferencia) < 0){
										nTds.addClass('ultra-alert-danger');
								    }
								}else{
									nTds.addClass('');
								}    

						})


						}//cierreFunc	
				});//Cierre ajax

			
			
		}

		$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
				$('#consultaFactura').attr("method", "post");               
				$('#consultaFactura').attr('action', "{{route('generarExcelReportePendiente')}}").submit();
            });


</script>

@endsection
