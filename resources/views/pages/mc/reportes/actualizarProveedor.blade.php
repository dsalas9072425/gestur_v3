@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	@parent
	<style type="text/css">
	
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

	.card,.card-header {
		border-radius: 14px !important;
		}
</style>
  <link rel="stylesheet" href="{{asset('mC/css/toast.min.css')}}">
@endsection
@section('content')
@include('flash::message') 

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Actualizar Proveedores en Booking Motor</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">


				<div class="alert alert-success alert-dismissible">
	                <h4><i class="icon fa fa-check"></i> Atenciòn!</h4>
	                Se han actualizado los proveedores exitosamente.
              	</div>
        
      
          		<div class="row">
					<div class="col-12">
						<a href="{{route('home')}}" class="btn btn-danger btn-lg" role="button"><b>VOLVER    </b></a>
					</div>  
				</div>	

			</div>
		</div>
	</section>
</section>
    
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')


@endsection