@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')


@include('flash::message')


<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Listado de Proveedores BKM</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

	  
	            	<div class="table-responsive">
						<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
				            <thead> 
				                <tr>
				                    <th>Código</th>
				                    <th>Descripción</th>
				                    <th>Tipo</th>
				                </tr> 
				            </thead>
				            <tbody>
					                 @foreach($listados->proveedores as $key=>$listado)
					                    <tr>
					                        <td><b>{{$listado->code}}</b></td>
					                        <td><b>{{$listado->name}}</b></td>
					                        <td><b>{{$listado->products[0]}}</b></td>
					                    </tr>   	
					                @endforeach 
				            </tbody>
			            </table>	            
					</div>  
					
	           
				</div>
			</div>
		</section>
	</section>
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$("#listado").dataTable();

	</script>	
@endsection