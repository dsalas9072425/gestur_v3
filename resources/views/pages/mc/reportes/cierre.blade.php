@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	
	<style>
		.asistencia{
			display: none;
		}
	 	.error{
	 		color:red;
	 	},
	   	.cabeceraClass {
	   		padding: 10px;
	   		min-width: 250px;
	   		background-color:#E8EAEA;
	   		word-break: break-all;
	   		margin: 10px;

	   	},
		.modal-footer {
		    text-align: left;
		} 
		.nav-tabs>li>a {
		    color: #dacdcd;
		} 
		/*.verificarStyle {
			padding:5px 40px;
			margin-top: 5px;
		} */

		.base{
			cursor: pointer;
		}

		.modal-bodyProforma{
			  height:250px;
             overflow:auto;
			}

		.commentNone {
			display: none;
		}
		.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

		.text-bold{
			font-weight: 800;
			font-size: 15px;
		}


    .chat-content {
        text-align: left;
        float: left;       
        position: relative;
        display: block;
        padding: 8px 15px;
        margin: 0 20px 10px 0;
        clear: both;
        color: #404e67;
        background-color: #edeef0;
        border-radius: 4px;



    }
    .chat-content-left {
        text-align: right;
        position: relative;
        display: block;
        float: right;
        padding: 8px 15px;
        margin: 0 20px 10px 0;
        clear: both;
        color: #fff;
        background-color: #00b5b8;
        border-radius: 4px;
    }  
/*=============================================================================*/

.valoracion {
    position: relative;
    overflow: hidden;
    display: inline-block;
}

.valoracion input {
    position: absolute;
    top: -100px;
}


.valoracion label {
    float: right;
    color: #c1b8b8;
    font-size: 30px; 
}

.valoracion label:hover,
.valoracion label:hover ~ label,
.valoracion input:checked ~ label {
    color: #E2D532;
}


/*=============================================================================================*/
	.btnAdjuntar{
			/* background-color: #00a65a !important;
			border-color: #008d4c !important; 
			color:#FFFF !important;*/s
			font-weight:800 !important;  
			padding:10px !important;
	}


	.verificarStyle {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}
	
	.btnSolicitarVerificacion {
		padding:10px !important;
		/* background: #2d3e52 !important;
		border-color: #2d3e51 !important;
		color:#FFFF !important; */
	}

	.btnModificar {
		/* background: #e2076a !important; 
		border-color: #e25a7d !important;  */
		padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnAnular{
		/* background-color: #dd4b39 !important;
    	border-color: #d73925 !important; */
    	padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnHand {
		cursor:pointer; 
		cursor: hand;
	}

	.btnHand:hover {
  opacity: 0.8;
}

	#cierre .table tbody, {
	  padding: 0rem 0rem; 
	}

		  
	</style>
		
@endsection
@section('content')
<!-- Main content -->
<section id="base-style">
   	<div class="card-content">
		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Cierre de Caja</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
                		<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body rounded-bottom rounded-lg">	
			        <ul class="nav nav-tabs nav-underline" role="tablist">
     					<li class="nav-item">
			                <a class="nav-link active" id="baseIcon-tab20" data-toggle="tab" aria-controls="tabIcon20" href="#cierreCaja" role="tab" aria-selected="true"><i class="fa fa-play"></i>Cierre de Caja</a>
			            </li>			            
			            <li class="nav-item">
			                <a class="nav-link" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#cabecera" role="tab" aria-selected="false">
			                	<i class="fa fa-play"></i> Resumen de Cierre de Caja</a>
			            </li>
			            <li class="nav-item">
			                <a class="nav-link" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#detalle" role="tab" aria-selected="false"><i class="fa fa-flag"></i> Detalle de Cierre de Caja</a>
			            </li>
			        </ul>
			        <div class="tab-content px-1 pt-1">
			            <div class="tab-pane active" id="cierreCaja" role="tabpanel" aria-labelledby="baseIcon-tab20">
			            	<div class="row">
				        		<div class="col-md-12">
				        			<form id="verCierre" style="margin-top: 2%;">
				        				<div class="row">
											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
												<div class="form-group">
													<label>Desde - Hasta Cierre Caja</label>
													<div class="input-group">
														<div class="input-group-prepend" style="">
															<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
														</div>
														<input type="text" class="form-control pull-right fecha" name="periodo" id="periodo" value="">
													</div>
												</div>
											</div>
											<div class="col-md-3" style="height:74px;">
												<label>Usuario</label>
												<select class="form-control select2" name="usuario"  id="usuario" style="width: 100%;">
														<option value=''>Todos</option>
														@foreach($usuarios as $usuario)
															<option value='{{$usuario->id}}'>{{$usuario->nombre_apellido}}</option>
														@endforeach
												</select>											
											</div>
											<div class="col-md-3" style="height:74px;">
												<label>Estado</label>
												<select class="form-control select2" name="activo"  id="activo" style="width: 100%;">
														<option value='true'>Activo</option>
														<option value='false'>Anulado</option>
												</select>											
											</div>
										</div>	 
										<input type="hidden" class="form-control" id="idCobro"  name="idCobro" value="" >
										<input type="hidden" class="form-control" id="moneda"  name="moneda" value="" >
										<input type="hidden" class="form-control" id="id_cierre"  name="id_cierre" value="" >
				        			</form>	
				        			<div class="row">
				        				<div class="col-md-12" style="height:74px;">
											<button id="buscarCierre" class="btn btn-info pull-right btn-lg mr-1"><i class="glyphicon glyphicon-refresh"></i><b>Buscar</b></button>
										</div>	
				        			</div>	
				        		</div>	
				        	</div>
				        	<br>	
			            	<div class="row">
				        		<div class="col-md-12"> 
				        			<table id="cierreTabla" class="table">
				        				<thead>
				                            <tr>
				                            	<th>Fecha</th>
				                                <th>Total GS</th>
				                                <th>Total US</th>
				                                <th>Usuario</th>
				                                <th></th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                        </tbody>
				                    </table>
				        		</div>
				        	</div>	 			            
			            </div>
			            <div class="tab-pane" id="cabecera" role="tabpanel" aria-labelledby="baseIcon-tab21">
			            	<div class="row">
				        		<div class="col-md-12">
				        			<!--<form id="verCierreResumen" style="margin-top: 2%;" method="get">
				        				<div class="row">
				        					<div class="col-md-3" style="height:74px;">
												<div class="form-group">
													<label>Desde - Hasta Facturación</label>						 
													<div class="input-group">
														<div class="input-group-addon" style="">
															<i class="fa fa-calendar"></i>
														</div>
														<input type="text" class="form-control pull-right fecha" name="periodo_fecha_facturacion" id="periodo_fecha_facturacion" value="">
													</div>
												</div>	
											</div>   
				        				</div>	

				        			</form>-->	
				        		</div>	
				        	</div>
				        	<br>	
			            	<div class="row">
				        		<div class="col-md-12"> 
				        			<table id="resumenTabla" class="table">
				        				<thead>
				                            <tr>
				                            	<th>Forma de Cobro</th>
				                                <th>Moneda</th>
				                                <th>Monto</th>
				                                <th></th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                        </tbody>
				                    </table>
				        		</div>
				        	</div>	 			            
			            </div>
			            <div class="tab-pane" id="detalle" role="tabpanel" aria-labelledby="baseIcon-tab22">
			            	<br>
			            	<div class="row">
			            		<div class="col-md-10"></div>
				        		<div class="col-md-2">
			            			<button type="button" id="botonExcel" class="btn btn-success btn-lg pull-right mr-1"><b>Excel</b></button> 
			            		</div>
			            	</div>		
				        	<div class="row">
				        		<div class="col-md-12">
						        	<div class="table-responsive">
				                        <table id="cierre" class="table table-striped table-bordered" style="width: 100%;">
				                            <thead>
				                                 <tr>
				                                    <th>Recibo</th>
				                                    <th style="width:30%;">Cliente</th>
				                                    <th style="width:35%;">Detalle</th>
				                                    <th>Moneda</th>
				                                    <th>Cotizacion</th>
				                                    <th>Total Pago</th>
				                                </tr>
				                            </thead>
				                            <tbody>
				                            </tbody>
				                        </table>
				                    </div>
				                </div>  
			                </div>
			                <br>
			                <br> 
		        		</div>	
			        </div>    	
				</div>
			</div>
		</div>
	</div>
</section>		
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
<script type="text/javascript" language="javascript" src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    
	<script type="text/javascript">
			$('#resumenTabla').dataTable({ordering: false});
			$('#cierreTabla').dataTable({                 
										language: {
												"decimal": "",
												"emptyTable": "No hay información",
												"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
												"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
												"infoFiltered": "(Filtrado de _MAX_ total entradas)",
												"infoPostFix": "",
												"thousands": ",",
												"lengthMenu": "Mostrar _MENU_ Entradas",
												"loadingRecords": "Cargando...",
												"processing": "Procesando...",
												"search": "Buscar:",
												"zeroRecords": "Sin resultados encontrados",
												"paginate": {
													"first": "Primero",
													"last": "Ultimo",
													"next": "Siguiente",
													"previous": "Anterior"
												}
											}
										});
			$('.select2').select2();

			var Fecha = new Date();
			var fechaInicial =  Fecha.getMonth()+ "/" + Fecha.getFullYear();

			$('input[name="periodo"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													     cancelLabel: 'Limpiar',
													     applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    },
												startDate: new Date(),
        										endDate: new Date(),
								    			});
			$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  }); 

			
			cargaCierre();

			$("#buscarCierre").click(function(){
					cargaCierre();
			});

			function cargaCierre(){
				var dataString = $("#verCierre").serialize();
				$.ajax({
					type: "GET",
					url: "{{route('getCierre')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
						var oSettings = $("#cierreTabla").dataTable().fnSettings();
						var iTotalRecords = oSettings.fnRecordsTotal();
						for (i=0;i<=iTotalRecords;i++) {
							$("#cierreTabla").dataTable().fnDeleteRow(0,null,true);
						}
						$.each(rsp, function (key, item){

								var btnReporte ='<div class="row" style="width: 150px;"><div class="col-md-4"><a href="reporteCierrePdf/'+item.id+'" class="btn btn-success"><i class="fa fa-id-card-o"></i></a></div>';
								
								var btndetalles ='<div class="col-md-4"><button id="'+item.id+'" onclick="resumenCierre('+item.id+');" class="btn btn-info"><i class="ft-folder"></i></button></div>';

								var btncancelar ='<div class="col-md-4"><button id="'+item.id+'" onclick="cerrarCaja('+item.id+');" class="btn btn-danger"><i class="ft-x"></i></button></div></div>';
								
								var fecha = item.fecha_cierre;
								fecha_formato = fecha.split('-');
								fecha_formateada = fecha_formato[2]+"/"+fecha_formato[1]+"/"+fecha_formato[0]

								var dataTableRow = [
													'<b>'+fecha_formateada+'</b>',
													'<b>'+ new Intl.NumberFormat('de-DE').format(parseFloat(item.total_gs).toFixed(2))+'</b>',
													'<b>'+ new Intl.NumberFormat('de-DE').format(parseFloat(item.total_us).toFixed(2))+'</b>',
													'<b>'+item.usuario.nombre+" "+item.usuario.apellido+'</b>',
													btnReporte+" "+btndetalles+" "+btncancelar
												];

								var newrow = $("#cierreTabla").dataTable().fnAddData(dataTableRow);
								var nTr = $("#cierreTabla").dataTable().fnSettings().aoData[newrow[0]].nTr;
								var nTds = $('td', nTr);

						})
					}
				})					
			}
	

			function resumenCierre(id){
				var dataString = $("#verCierre").serialize();
				$.ajax({
					type: "GET",
					url: "{{route('getResumenCierre')}}",
					dataType: 'json',
					data: 'id='+ id,
					success: function(rsp){
						var oSettings = $("#resumenTabla").dataTable().fnSettings();
						var iTotalRecords = oSettings.fnRecordsTotal();
						for (i=0;i<=iTotalRecords;i++) {
							$("#resumenTabla").dataTable().fnDeleteRow(0,null,true);
						}
						console.log(rsp);
						$.each(rsp, function (key, item){
							var iconocancelar ='<button id="'+item.id+'" onclick="resumenDetalleCierre('+item.forma_cobro_id+', '+item.moneda_id+', '+item.id_cierre+');" class="btn btn-info"><i class="ft-folder"></i></button>';

							var dataTableRow = [
												'<b>'+item.denominacion+'</b>',
												item.currency_code,
												'<b>'+new Intl.NumberFormat('de-DE').format(parseFloat(item.monto).toFixed(2))+'</b>',
												iconocancelar
											];

							var newrow = $("#resumenTabla").dataTable().fnAddData(dataTableRow);
							var nTr = $("#resumenTabla").dataTable().fnSettings().aoData[newrow[0]].nTr;
							var nTds = $('td', nTr);
						})
						$('a[href="#cabecera"]').click();
					}
				})	


			}	

			function resumenDetalleCierre(idCobro,moneda,cierre){
						$("#idCobro").val(idCobro);
						$("#moneda").val(moneda);
						$("#id_cierre").val(cierre);
						var oSettings = $("#cierre").dataTable().fnSettings();
						var iTotalRecords = oSettings.fnRecordsTotal();
						for (i=0;i<=iTotalRecords;i++) {
							$("#cierre").dataTable().fnDeleteRow(0,null,true);
						}
				      $('#cierre').DataTable({
				        "ajax": {
				        	"serverSide": true,
				        	
				            "url": "{{route('getDatoscierre')}}",
				            "data": {
        								"idCobro": idCobro,
        								"moneda": moneda,
        								"id_cierre": cierre,
									},
    						},
    						"destroy": true,
    						"processing": true,
    						"deferLoading": 57,
				            "columns": [
								            {
								                "className":      'details-control',
								                "orderable":      false,
								                "data":           "nro_recibo",
								                "defaultContent": "",
								                "className": 'dt-body-right'
								            },
								            { 
								            	"data": "cliente",
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "concepto",
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "moneda",  
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "cotizacion", 
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "total_pago",
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            }
										],
										/*"columnDefs": [
									       { "type": 'date-dd-mmm-yyyy', targets: 1 }
									     ],*/
										"order": [[1, 'asc']]	
    					})	

					    $('#cierre tbody').on('click', 'td.dt-body-right', function () {
							    var tr = $(this).closest('tr');
							    var row = $('#cierre').DataTable().row(tr);
							     if (row.child.isShown()) {
							            // This row is already open - close it
							            row.child.hide();
							            tr.removeClass('shown');
							        }
							        else {

							            // Open this row
							            row.child(format(row.data())).show();
							            tr.addClass('shown');
							        }
							} )
					    $('a[href="#detalle"]').click();	
					    desplegarTabla($('#cierre').DataTable());
			}

			function format(d) {
				var tabla = '<table cellpadding="6" cellspacing="0" border="0" style="background-color: #eaebee; width: 100%"> ';
					tabla+= '<tr>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px; width: 30%;"><b>Forma de Pago</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px; width: 30%;"><b>Comprobante</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px; width: 20%;"><b>Moneda</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px; width: 20%;"><b>Importe</b></td>';
					tabla+= '</tr>';
					var totalGs = 0;
					var totalUs = 0;
					$.each(d.detalles, function (key, item){
						console.log(item);
							if(item.id_moneda == 143){
								moneda = 'US';
							}else{
								moneda = 'GS'; 
							}

						tabla+= '<tr>';	
						if(item.forma_cobro_abreviatura == 'EFECT'){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"> -- </td>';
						}
						if(item.forma_cobro_abreviatura == 'DEP'){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b>'+item.nro_comprobante+' - '+item.banco+'</b></td>';
						}

						if(item.forma_cobro_abreviatura == 'CHEQ'){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+' - '+item.banco_plaza+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b>'+item.cheque+'</b></td>';
						}

						if(item.forma_cobro_abreviatura == 'TRANSF'){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+' - '+item.banco+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b>'+item.nro_comprobante+'</b></td>';
						}

						if(item.forma_cobro_abreviatura == 'TD'){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+' - '+item.banco+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b> </b></td>';
						}
						if(item.forma_cobro_abreviatura == 'TC'){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+' - '+item.banco+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b> </b></td>';
						}

						if(item.forma_cobro_abreviatura == 'RETEN'){
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b></b></td>';
						}/*else{
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.forma_cobro+'</td>';
							tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"> -- </td>';
						}*/

						tabla+= '<td><b>'+moneda+'</b></td>';
						tabla+= '<td><b>'+new Intl.NumberFormat('de-DE').format(parseFloat(item.importe_pago).toFixed(2))+'</b></td>';
						tabla+= '</tr>';
					})	
					tabla+= '</table>';
				    return tabla;  
				}


			function  desplegarTabla(table){
				$('#cierre').on('init.dt', function(e, settings){
					   var api = new $.fn.dataTable.Api( settings );
					   api.rows().every( function () {
					      var tr = $(this.node());
					      this.child(format(this.data())).show();
					      tr.addClass('shown');
					   });
					});
			}

			function cerrarCaja(id){
				return swal({
                    title: "GESTUR",
                    text: "¿Desea anular el Cierre de Caja?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, anular",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            anularCierre(id);
                        } else {
                            swal("Cancelado", "", "error");
                        }
            	});
			}


			function anularCierre(id){
				$.ajax({
					type: "GET",
					url: "{{route('anularCierreCaja')}}",
					dataType: 'json',
					data: 'id='+ id,
					success: function(rsp){
						if(rsp.respuesta == 'OK'){
							swal("Exito", "", "success");
							cargaCierre();
						}else{
							swal("Cancelado", rsp.respuesta ,"error");
						}	

					}
				});	


			}


			$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
				$('#verCierre').attr("method", "post");               
				$('#verCierre').attr('action', "{{route('generarExcelReporteCierre')}}").submit();
            });

	</script>



@endsection
