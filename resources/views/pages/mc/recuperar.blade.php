<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="GESTUR">
    <title>GESTUR</title>
    <link rel="apple-touch-icon" href="{{asset('gestion/app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('gestion/app-assets/images/logo/git.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    @include('layouts/meta')
    @include('layouts/gestion/styles')
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    @include('flash::message')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center" style="height: 90px;">
                                        <div class="p-1">
                                            <img src="{{asset('gestion/app-assets/images/logo/git.png')}}" alt="branding logo">
                                        </div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Ingresar correo registrado</span></h6>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form  id="login-form" class="login-form" action="{{route('doRecuperar')}}" method="get">
											<fieldset class="form-group position-relative has-icon-left mb-0">
                                                <input type="email" class="form-control form-control-lg" id="email" placeholder="Correo Registrado" name="email" required>
                                                <div class="form-control-position">
                                                    <i class="fa fa-envelope-o"></i>
                                                </div>
                                            </fieldset>  
                                            <br>
                                            <div class="form-group">

                                            </div>
                                            <button type="submit" id="ingresarCuenta" class="btn btn-primary btn-lg btn-block" style="background-color: #002b71" style="color:#ffffff;"><i class="fa fa-arrow-right"></i> Recuperar Contraseña</button> 
                                            <br /><br />
                                             <!--<a href="#myModal" data-toggle="modal" style="color:#ffffff;">Ha olvidado su contraseña?</a>--> 
                                        </form>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
        @include('layouts/scripts')
        <script type="text/javascript">
          

        </script>

</body>
<!-- END: Body-->

</html>
