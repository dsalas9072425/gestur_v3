@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	<style>
		.asistencia{
			display: none;
		}
		.select2-container--default .select2-selection--single {
			height: 30px;
		}	
		#pasajero.select2-selection.select2-selection--single{
			width: 56px;
		    padding-left: 5px;
		    padding-right: 10px;
		}
	</style>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content"> 
    <form id="frmProforma" method="post"  action="{{route('factour.doAddProforme')}}" style="margin-top: 2%;">
	     	<div class="row">
		        <div class="col-xs-12">
		          <div class="box">
		            <div class="box-header">
		             <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Detalles de Proforma</h1>
		             <input type="hidden" class = "Requerido form-control input-sm" id="proforma_id" name="proforma_id" value="{{$id}}" />
		            </div>  
				    <!-- SELECT2 EXAMPLE -->
				    <div class="box box-default" style="border-top-width: 1px;margin-bottom: 0px;">
				        <!-- /.box-header -->
				        <br>
				        <br>
				        <div class="box-body">
					      	<!-- /.box --> 
			            	<div class="table-responsive">
				              	<table id="lista_productos" class="table">
				                	<thead>
										<tr>
									        <th style="background-color: #2d3e52; color: white">Item</th>
									        <th style="background-color: #2d3e52; color: white">Cant.</th>
											<th style="background-color: #2d3e52; color: white">Producto</th>
											<th>Check In / Out</th>
											<th style="background-color: #2d3e52; color: white">Cant.<br> Pasajero</th>
									        <th>Confirmacion</th>
											<th style="background-color: #2d3e52; color: white; display: none" class="ticket" >Ticket</th>
											<th style="background-color: #2d3e52; color: white" class="asistencia">Dias</th>
											<th style="background-color: #2d3e52; color: white" class="asistencia">Asistencia</th>
									        <th>Proveedor</th>
									        <th>Prestador</th>
											<th>Detalle</th>
											<!--<th>Pasajero</th>-->
									        <th>Moneda</th>
											<th>Costo</th>
											<!--<th>Markup</th>-->
											<th>Comision Agencia</th>
											<th style="display: none">In/Out</th>
									        <th>Venta</th>
											<th>Fecha Pago <br> Proveedor</th>
											<th></th>
							            </tr>
				                	</thead>
				                	<tbody >
										<tr>
									        <td><span style="width: 10px;" id="0">1<span><input type="hidden" class = "Requerido form-control input-sm" id="item" name="item" value="1" /></td>
									        <td><input type="text" class = "Requerido form-control input-sm" name="cantidad" id="cantidad" style="width: 50px;"/></td>
											<td>
												<select class="form-control input-sm select2" name="producto" id="producto" style="width: 100%;">
													<option value="0">Seleccione Producto</option>
													@foreach($productos as $key=>$producto)
														<option value="{{$producto->id}}">{{$producto->denominacion}}</option>
													@endforeach	
							            		</select>
											</td>
											<td>
												<div id="periodo">
													<input type="text" style="width: 72px;" class = "Requerido form-control input-sm" id="periodo"  name="periodo" />
												</div>	
											</td>
									        <td><input type="text" class = "Requerido form-control input-sm" name="cantidad_personas" id="cantidad" style="width: 50px;"/></td>
									        <td><input type="text" style="width: 110px;" class = "Requerido form-control input-sm" id="confirmacion" name="confirmacion"/></td>
											<td class="ticket" style="display: none">
												<select class="form-control input-sm select2 producto" name="ticket" id="ticket" style="width: 100%;">
													<option value="0">Seleccione Ticket</option>
													@foreach($tickets as $key=>$ticket)
														<option value="{{$ticket->id}}">{{$ticket->fecha_emision}}</option>
													@endforeach	
							            		</select>
											</td>
									        <td class="asistencia"><input type="text" id="asistencia_dias" name="asistencia_dias" class = "Requerido form-control input-sm" value="0"/></td>
											<td class="asistencia">
												<select class="form-control input-sm select2 producto" name="asistencia" id="asistencia" style="width: 100%;">
													<option value="0">Seleccione Asistencia</option>
													@foreach($asistencias as $key=>$asistencia)
														<option value="{{$asistencia->id}}">{{$asistencia->tarifa_asistencia_nombre}}</option>
													@endforeach	
							            		</select>
											</td>
									        <td>
									        	<select class="form-control input-sm select2" name="proveedor" id="proveedor" style="width: 90%;">
													<option value="0">Seleccione Proveedor</option>
													@foreach($proveedores as $key=>$proveedor)
														<option value="{{$proveedor->id}}">{{$proveedor->nombre_apellido}}</option>
													@endforeach	
							            		</select>
									        </td>
									        <td>
									        	<select class="form-control input-sm select2" name="prestador" id="prestador" style="width: 100%;">
													<option value="0">Seleccione Prestador</option>
													@foreach($prestadores as $key=>$prestador)
														<option value="{{$prestador->id}}">{{$prestador->nombre_apellido}}</option>
													@endforeach	
							            		</select>
									        </td>
									        <td><input type="text" id="descripcion" name="descripcion" class = "Requerido form-control input-sm"/></td>
									        <!--<td>
									        	<input type="hidden" id="pasajeroId" name="pasajeroId" class = "Requerido form-control input-sm"/>
									        	<input type="hidden" id="cantPasajero" name="cantPasajero" class = "Requerido form-control input-sm" value="0" />
									        	<div class="row"> 
										        	<div class="col-md-10" style="padding-left: 15px;padding-right: 0px;">
										        		<div id="divPasajeros">
												        	<select class="form-control input-sm select2" name="pasajeroCombo" id="pasajeroCombo" style="width: 100%;">
																<option value="0">Seleccione Pasajero</option>
																@foreach($pasajeros as $key=>$pasajero)
																	@if($pasajeroProforma == $pasajero->id)
																		<option value="{{$pasajero->id}}" selected="selected" >{{$pasajero->nombre_apellido}}</option>
																	@else
																		<option value="{{$pasajero->id}}" selected="selected" >{{$pasajero->nombre_apellido}}</option>
																	@endif	
																@endforeach	
										            		</select>
										            	</div>	
										            </div>	
										        	<div class="col-md-1" style="padding-left: 0px;padding-right: 20px;padding-top: 5px;">
										            	<a data-toggle="modal" href="#requestModalPasajero" class="btn-mas-request" style="margin-left: 5px;"><i class="fa fa-plus fa-lg"></i></a>	
										            </div>	 
							            		</div> 	
									        </td>-->
											<td >
												<select class="form-control input-sm select2" name="pasajero" id="pasajero" style="    padding-left: 0px;width: 100%;">
													@foreach($currencys as $key=>$currency)
														@if($currency->currency_id == 143)
															<option value="{{$currency->currency_id}}" selected="selected">{{$currency->currency_code}}</option>
														@else
															<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
														@endif
													@endforeach	
												</select>
											</td>
											<td><input type="text" style="width: 80px;" class = "Requerido form-control input-sm" id="costo" name="costo"/></td>
											<!--<td><input type="text" style="width: 80px;" class = "Requerido form-control input-sm" id="markup" name="markup"/></td>-->
											<td><input type="text" id="comision_agencia" name="comision_agencia" class = "Requerido form-control input-sm"/></td>
									        <td><input type="text" style="width: 80px;" class = "Requerido form-control input-sm" id="venta" name="venta"/></th>
							            	<td>
										        <div class="input-group">
													<div class="input-group-addon" style="padding-top: 0px;padding-bottom: 0px;height: 29px;padding-left: 5px;padding-right: 5px;">
													    <i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control input-sm pull-right fecha" name="checkIn" id="checkIn" required>
												</div>
											</td>
											<td>
										        <a data-toggle="modal" href="#requestModalVoucher" id="voucher" class="btn-mas-request" style="margin-left: 5px;"><i class="glyphicon glyphicon-tags"></i></a>	
										        <a id="guardar" id="guardar" class="btn-mas-request" style="margin-left: 5px;"><i class="glyphicon glyphicon-ok"></i></a>	
											</td>
							            </tr>
				                	</tbody>
				            	</table>    	
				            </div>	
						</div>
		          	</div>
		          <!-- /.box -->
		          	<div class="row"  style="margin-bottom: 1px;">
		          		<div class="col-xs-12 col-sm-1 col-md-2" style="padding-left: 15px;margin-top: 5px;padding-bottom: 10px;">
		          			<button type="button" data-toggle="modal" href="#requestModalVoucher" style="width: 60px;background-color: #e2076a;height: 40px;margin-bottom: 5px;margin-top: 17px;margin-left: 10px;" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-plus"></i></button>
		          		</div>
						<div class="col-xs-12 col-sm-12 col-md-2" style="padding-left: 15px;margin-top: 5px;padding-bottom: 10px;">
						</div>
						<div class="col-xs-12 col-sm-2 col-md-2" style="padding-left: 15px;margin-top: 5px;padding-bottom: 10px;">
							<button type="button" id="btnAceptarPasajero" class="btn btn-danger" style="width: 120px;background-color: #e2076a;height: 40px;margin-bottom: 10px;margin-top: 15px;margin-left: 10px;"><b>CERRAR</b></button>
						</div>	
					</div>	
		          <!-- /.box -->
			       </div>
		        <!-- /.col -->
	      	</div>
	    </form>  	
    </section>
    <!-- /.content -->
    <div id="modalPasajeros" class="modal fade" role="dialog">
		<form id="frmPasajero" method="post" action="" style="margin-top: 12%;">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div id="modal-header" class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Detalle de Pasajero</h2>
					</div>
				  	<div class="modal-body">
						<div id="pasajerosDetalle">
						</div>
					</div> 
							<br>
						<div class="modal-footer">
							<div class="row"  style="margin-bottom: 1px;height: 20px;">
								<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
						        </div>	
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px; display: none">
						        </div>
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;">
						        </div>
								<div class="col-xs-12 col-sm-3 col-md-3" id="divCancelarPasajeros" style="padding-left: 15px;">
									<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
						        </div>
							</div>	
						</div>
				  	</div> 
				</div>
	  	</div>
	</div>		
    <div id="requestModalPasajero" class="modal fade" role="dialog">
		<form id="frmPasajero" method="post" action="" style="margin-top: 12%;">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div id="modal-header" class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Ingresar Pasajero</h2>
					</div>
				  	<div class="modal-body">
							<div class="row" id="modalPasajero">
								<div class="col-md-10" style="padding-left: 15px;padding-right: 0px;">
									<select class="form-control input-sm select2" name="pasajeroCombobox" id="pasajeroCombobox" style="width: 100%;">
										<option value="0">Seleccione Pasajero</option>
										@foreach($pasajeros as $key=>$pasajero)
											@if($pasajeroProforma == $pasajero->id)
												<option value="{{$pasajero->id}}" selected="selected" >{{$pasajero->nombre_apellido}}</option>
											@else
												<option value="{{$pasajero->id}}" selected="selected" >{{$pasajero->nombre_apellido}}</option>
											@endif	
										@endforeach	
									</select>
								</div>	
								<div class="col-md-1" style="padding-left: 0px;padding-right: 20px;padding-top: 0px;">
									<button type="button" id="btnCrearPasajero" style="padding-top: 3px;padding-bottom: 3px;padding-left: 8px;padding-right: 8px;margin-left: 2px;"><i class="fa fa-plus fa-lg"></i></button>
								</div>
							</div>		 
							<br>
							<div id="modalDatosPasajero" class="col-md-10" style="padding-left: 15px;padding-right: 0px;display: none">
						        <div class="row"  style="margin-bottom: 1px;">
						          	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
						          		<label>Nombre</label>
						          		<input type="text" required class = "Requerido form-control input-sm" name="nombre" id="nombre"/>
						          	</div>	
						          	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
						          		<label>Apellido</label>
						          		<input type="text" required class = "Requerido form-control input-sm" name="apellido" id="apellido"/>
						          	</div>	
								</div>	 
						        <div class="row"  style="margin-bottom: 1px;">
						          	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
						          		<label>Edad</label>
						          		<input type="text" required class = "Requerido form-control input-sm" name="edad" id="edad"/>
						          	</div>	
						          	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
						          		<label>Tipo</label>
										<select class="form-control input-sm select2" name="id_tipo_pasajero" id="tipo" style="width: 100%;">
											<option value="0">Seleccione</option>
											<option value="1">Adulto</option>
											<option value="2">Niños</option>
											<option value="3">Infantes</option>
							            </select>
						          	</div>	
								</div>
								<div class="row"  style="margin-bottom: 1px;height: 20px;">
						          	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
						          	</div>	
						          	<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
						          	</div>	
								</div>								
							</div> 
							<br>
						<div class="modal-footer">
							<div class="row"  style="margin-bottom: 1px;height: 20px;">
								<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">
						        </div>	
								<div class="col-xs-12 col-sm-3 col-md-3" id="divCrearPasajero" style="padding-left: 15px; display: none">
									<button type="button" id="btnCrearPasajeros" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Crear</button>
						        </div>
								<div class="col-xs-12 col-sm-3 col-md-3" id="divAceptarPasajeros" style="padding-left: 15px;">
									<button type="button" id="btnAceptarPasajeros" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Aceptar</button>
						        </div>
								<div class="col-xs-12 col-sm-3 col-md-3" id="divCancelarPasajeros" style="padding-left: 15px;">
									<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
						        </div>
							</div>	
						</div>
				  	</div> 
				</div>
	  	</div>
	 </form>
	</div>
    <div id="requestModalVoucher" class="modal fade" role="dialog">
		<form id="frmPasajero" method="post" action="" style="margin-top: 12%;">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div id="modal-header" class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Datos de Voucher</h2>
					</div>
				  	<div class="modal-body">
				  		<div class="row">
				  				<select class="form-control input-sm select2" name="proveedor" id="proveedor" style="width: 90%;">
									<option value="0">Seleccione Proveedor</option>
							    </select>

					  	<div class="row">
					  		<div class= "col-xs-12 col-sm-6 col-md-6">
					  			
					  		</div>
					  		<div class= "col-xs-12 col-sm-6 col-md-6">
					  			
					  		</div>
					  	</div>


				  	</div> 
				</div>
	  	</div>
	 </form>
	</div>

@endsection
@section('scripts')
	@include('layouts/mc/scripts')
	<script type="text/javascript" src="{{asset('mC/js/jquery.form.js')}}"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	<script>
		 $('.sidebar-toggle').trigger('click');
		$('.select2').select2();
		$('input[name="periodo"]').daterangepicker({
							        //timePicker: true,
							        timePicker24Hour: true,
							        timePickerIncrement: 30,
							        locale: {
							            format: 'MM/DD/YYYY '//H:mm'
							        }
							    });

		$(function () {
                $('.fecha').datepicker({
                    defaultDate: "11/1/2013",
                    timePicker: true,
                });
            });

		var tbody = $('#lista_productos tbody'); 
		   //Agregar fila nueva. 
		$('#guardar').click(function(){ 
	   		console.log('ingresar');
	   		var dataString = $("#frmPasajero").serialize();
	   		$.ajax({
						type: "GET",
						url: "{{route('factour.guardarFila')}}",
						dataType: 'json',
						data: {
								dataProforma:  $('#proforma_id').val(),
								dataItem:  $('#item').val(),
								dataCantidad: $('#cantidad').val(),
								dataProducto:  $('#producto').val(),
								dataTicket:  $('#ticket').val(),
								dataAsistenciaDias: $('#asistencia_dias').val(),
								dataAsistencia: $('#asistencia').val(),
								dataProveedor: $('#proveedor').val(),
								dataPrestador:  $('#prestador').val(),
								dataPeriodo:  $('#periodo').val(),
								dataPasajero:  $('#pasajero').val(),
								dataCosto: $('#costo').val(),
								dataConfirmacion: $('#confirmacion').val()
				       			},
						success: function(rsp){
							console.log(rsp);

						}
					});				
	   		});

		$('#btnCrearPasajero').click(function(){
			console.log('gnsdlkjafg'); 
			$('#modalDatosPasajero').css({ display: "block" });	
			$('#modalPasajero').css({ display: "none" });	
			$('#modal-header').css({ display: "none" });
			$('#divCrearPasajero').css({ display: "block" });	
			$('#divAceptarPasajeros').css({ display: "none" });	
	   	});

		$('#agregar').click(function(){ 
	   		console.log('ingresar');
	   		var dataString = $("#frmProforma").serialize();
	   		$.ajax({
						type: "GET",
						url: "{{route('factour.guardarFila')}}",
						dataType: 'json',
						data: dataString,
						success: function(rsp){
							console.log(rsp);
							/*base = rsp.id;
					   		fila_contenido = '';
					   		fila_contenido += '<tr id="'+rsp.id+'">';
					   		fila_contenido += '<th><span id="'+base+'">'+rsp.item+'<span><input type="hidden" class = "Requerido form-control input-sm" id="item-'+base+'" name="item_'+base+'"/></th>';
					   		fila_contenido += '<td><input type="text" class = "Requerido form-control input-sm" name="cantidad" id="cantidad-'+base+'" style="width: 76px;" value="'+$('#cantidad').val()+'"/></td>';
					   		fila_contenido += '<td><input type="text" class = "Requerido form-control input-sm" name="producto" id="producto-'+base+'" style="width: 100%" value="'+$('#producto :selected').text()+'"/></td>';
					   		fila_contenido += '<td><input type="text" class = "Requerido form-control input-sm" name="producto" id="ticket-'+base+'" style="width: 100%" value="'+$('#ticket :selected').text()+'"/></td>';
					   		fila_contenido += '<td><input type="text" class = "Requerido form-control input-sm" name="producto" id="asistencia_dias-'+base+'" style="width: 100%" value="'+$('#proveedor :selected').text()+'"/></td>';
					   		fila_contenido += '<td><input type="text" class = "Requerido form-control input-sm" name="producto" id="proveedor-'+base+'" style="width: 100%" value="'+$('#prestador :selected').text()+'"/></td>';
					   		fila_contenido += '<td><input type="text" class = "Requerido form-control input-sm" name="producto" id="prestador-'+base+'" style="width: 100%" value="'+$('#periodo').val()+'"/></td>';
					   		fila_contenido += '<td><input type="text" class = "Requerido form-control input-sm" name="producto" id="periodo-'+base+'" style="width: 100%" value="'+$('#pasajero :selected').text()+'"/></td>';
					   		fila_contenido += '<td><input type="text" class = "Requerido form-control input-sm" name="producto" id="pasajero-'+base+'" style="width: 100%" value="'+$('#costo').val()+'"/></td>';
					   		fila_contenido += '<td><input type="text" class = "Requerido form-control input-sm" name="producto" id="costo-'+base+'" style="width: 100%" value="'+$('#confirmacion').val()+'"/></td>';
					   		fila_contenido += '</tr>';
					   		tbody.prepend(fila_contenido);	*/					
					   	}
					});				
	   }); 

	  /* $('#btnCrearPasajeros').click(function(){
	   		console.log('Inicio');

	   })*/

	   $('#btnCrearPasajeros').click(function(){
				$.ajax({
						type: "GET",
						url: "{{route('factour.getPasajero')}}",
						dataType: 'json',
						data: {
								dataNombre: $('#nombre').val(),
								dataApellido: $('#apellido').val(),
								dataEdad:  $('#edad').val(),
								dataTipo: $('#tipo').val()
				       			},
						success: function(rsp){
							console.log(rsp);
							$('#divPasajeros').html("");
							cantidadPasajeros = parseInt($('#cantPasajero').val());
							detallePasajeros = $('#pasajeroId').val();
							if(cantidadPasajeros == 0){
								detallePasajeros = rsp.id
							}else{
								detallePasajeros = detallePasajeros + ', '+rsp.id
							}
							$('#pasajeroId').val(detallePasajeros);
 							cantidadPasajeros = cantidadPasajeros + 1;
							$('#cantPasajero').val(cantidadPasajeros);
							$('#divPasajeros').html('<a data-toggle="modal" href="#modalPasajeros" class="btn-mas-request pasajeros" style="margin-left: 5px;">'+cantidadPasajeros+' Pasajero(s)</a>');
							$("#requestModalPasajero").modal('hide');
							datosPasajero();
						}	
				});				
	   })

	   $('#btnAceptarPasajeros').click(function(){
	   		$("#requestModalPasajero").modal('hide');
	   		$('#divPasajeros').html("");
			$('#divPasajeros').html('<a data-toggle="modal" href="#modalPasajeros" class="btn-mas-request pasajeros" style="margin-left: 5px;">'+parseInt($('#cantPasajero').val())+' Pasajero(s)</a>');
			datosPasajero();
	   })
	   //Eliminar fila. 
	   $('#lista_productos').on('click', '.button_eliminar_producto', function(){
	      $(this).parents('tr').eq(0).remove();
	   });

	   $("#producto").change(function(){
	   		if($(this).val() == 1){
	   			$('.asistencia').css({ display: "none" });
	   			$('.ticket').css({ display: "block" });
	   		}
	   		if($(this).val() == 3){
	   			$('.asistencia').css({ display: "block" });
	   			$('.ticket').css({ display: "none" });
	   		}
	    });

	   	$('.producto').change(function(){
				$.ajax({
						type: "GET",
						url: "{{route('factour.getCosto')}}",
						dataType: 'json',
						data: {
								dataProducto: $('#producto').val(),
								dataTipo: $(this).attr('id'),
								dataValue: $(this).val(),
								dataCantidad: $('#cantidad').val(),
								dataDias: $('#asistencia_dias').val()
				       			},
						success: function(rsp){
							console.log(rsp);
							$('#costo').val(rsp);
						}	
				});				
	   	});	

	    $("#requestModalPasajero").on('hidden.bs.modal', function () {
	        $('#modalDatosPasajero').css({ display: "none" });
			$('#modalPasajero').css({ display: "block" });	
			$('#modal-header').css({ display: "block" });	
	    });

	    $("#pasajeroCombo").change(function(){
			$('#divPasajeros').html("");
			cantidadPasajeros = parseInt($('#cantPasajero').val());
			detallePasajeros = $('#pasajeroId').val();
			if(cantidadPasajeros == 0){
				detallePasajeros = $(this).val()
			}else{
				detallePasajeros = detallePasajeros + ', '+$(this).val()
			}
			$('#pasajeroId').val(detallePasajeros);
 			cantidadPasajeros = cantidadPasajeros + 1;
			$('#cantPasajero').val(cantidadPasajeros);
			$('#divPasajeros').html('<a data-toggle="modal" href="#modalPasajeros" class="btn-mas-request pasajeros" style="margin-left: 5px;"> 1 Pasajero(s)</a>');
			datosPasajero()
	    });


	    $("#pasajeroCombobox").change(function(){
			$('#divPasajeros').html("");
			cantidadPasajeros = parseInt($('#cantPasajero').val());
			detallePasajeros = $('#pasajeroId').val();
			if(cantidadPasajeros == 0){
				detallePasajeros = $(this).val()
			}else{
				detallePasajeros = detallePasajeros + ', '+$(this).val()
			}
			$('#pasajeroId').val(detallePasajeros);
 			cantidadPasajeros = cantidadPasajeros + 1;
			$('#cantPasajero').val(cantidadPasajeros);
			$('#divPasajeros').html('<a data-toggle="modal" href="#modalPasajeros" class="btn-mas-request pasajeros" style="margin-left: 5px;"> 1 Pasajero(s)</a>');
			datosPasajero()
	    });



		function datosPasajero(){
			$('.pasajeros').click(function(){
				pasajero = $('#pasajeroId').val();
				$.ajax({
						type: "GET",
						url: "{{route('factour.getDetallesPasajero')}}",
						dataType: 'json',
						data: {
								dataPasajero: pasajero
				       			},
						success: function(rsp){
							console.log(rsp);
							$mensaje = '';
							$.each(rsp, function (key, item){
								if(item.id_tipo_pasajero == 0 ||item.id_tipo_pasajero == null ){
									tipo = '';
								}
								if(item.id_tipo_pasajero == 1){
									tipo = 'Adulto';
								}
								if(item.id_tipo_pasajero == 2){
									tipo = 'Niños';
								}
								if(item.id_tipo_pasajero == 3){
									tipo = 'Infantes';
								}

								if(item.pasajero_edad != null){
									edad = 'Adulto';
								}else{
									edad = item.pasajero_edad;
								}

						        $mensaje += '<div class="row" style="margin-bottom: 1px;">';
						        $mensaje += '<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">';
						        $mensaje += '<label>Nombre</label>';
						        $mensaje += '<input type="text" value = "'+item.nombre_apellido+'" required class = "Requerido form-control input-sm" name="nombre" id="nombre"/>';
						        $mensaje += '</div>';	
						        $mensaje += '<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">';
						        $mensaje += '<label>Tipo</label>';
						        $mensaje += '<input type="text" value = "'+tipo+'" required class = "Requerido form-control input-sm" name="tipo" id="apellido"/>';
						        $mensaje += '</div>';	
								$mensaje += '</div>';	 
						        $mensaje += '<div class="row" style="margin-bottom: 1px;">';
						        $mensaje += '<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">';
						        $mensaje += '<label>Edad</label>';
						        $mensaje += '<input type="text" value = "'+edad+'" required class = "Requerido form-control input-sm" name="nombre" id="nombre"/>';
						        $mensaje += '</div>';	
						        $mensaje += '<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;">';
						        $mensaje += '</div>';	
								$mensaje += '</div>';	
								$mensaje += '<hr>'; 
								$mensaje += '<br>'; 

							})	
							$('#pasajerosDetalle').html($mensaje);

						}	

				})       			

			})
		}

	</script>
@endsection