@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <form id="frmDtPlus" method="post" action="" style="margin-top: 2%;">
          <div class="box">
            <div class="box-header">
              <br>
              <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Pago DTPlus</h1>
            </div>      
		    <br>    
		    <div class="row">
		    	<div class="col-xs-6" style="padding-left: 35px;">
			        <label>Cheque</label>
			        <select class="form-control select2" id="id_cheques" style="width: 100%;" required>
			        	<option value="0">Seleccione un Cheque</option>
						@foreach($listadoCheques as $key=>$cheques)
							<option value="{{$cheques['id']}}">{{$cheques['numero']}} - {{$cheques['banco']['nombre']}} </option>
						@endforeach
			        </select>
		    	</div>
		    	<div class="col-xs-6" style="padding-right: 35px;">
		    		<div id="monto">
		    			<label>Monto <span id="moneda"></span></label>
		    			<input type="text" class = "form-control" id="montos"/>
		    			<input type="hidden" class = "form-control" id="pago"/>
		    		</div>
		    	</div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-6" style="padding-left: 35px;">
			        <label>Boleta de Deposito Numero</label>
		    			<input type="text" class = "form-control" id="montos"/>
		    	</div>
		    	<div class="col-xs-6" style="padding-right: 35px;">
		    		<div id="monto">
		    			<label>Monto <span id="moneda"></span></label>
		    			<input type="text" class = "form-control" id="monto"/>
		    		</div>
		    	</div>
		    </div> 
		    <br>  
		    <br>
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="table-responsive">
	              <table id="listado" class="table">
	                <thead>
					  <tr>
			            <th></th>
			            <th>Fecha<br/>Pedido</th>
			            <th>Tipo</th>
						<th>Facturas</th>
			            <th>Monto</th>
			            <th></th>
		              </tr>
	                </thead>
	                <tbody>
	                	@foreach($dtpPlus as $key=>$dtp)
						  <tr>
				            <th><input type="checkbox" disabled="disabled" name="dtp_{{$dtp['id']}}" id="dtp_{{$dtp['id']}}" class="icheck" data="{{$dtp['monto']}}"></th>
				            <th><img class="img-responsive" src="../images/promociones/{{$dtp['opcion_cambio']}}.png" style="width: 65px;"></th>
				            <th>{{$dtp['fecha_pedido']}}</th>
							<th>{{$dtp['facturas']}}</th>
							<th>{{$dtp['monto']}}</th>
							@if($dtp['id_imagen'] != "")
								<th>
									<a data-toggle="modal" data="{{$dtp['id']}}" href="#requestModal" class="btn btn-danger">Factura</a>
								</th>
							@else	
								<th></th>
							@endif
			              </tr>
	                	@endforeach
			        </tbody> 
	              </table>
	              <br>
	              <button type="submit" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="background: #e2076a !important;width: 176px;height: 34px; margin-left: 85%;" name="guardar" value="Guardar"><b>Guardar</b></button>

	            </div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
         </form>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
	<div id="requestModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-top: 5%;">
        <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body" style="height: 680px;">
                        <div id="contenido">
                        </div>
                  </div>
            </div>
        </div>
    </div>    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$('.select2').select2();
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

	        $(".icheck").click(function(){
	        	var este  = $(this).attr('id');
	        	console.log(este);
	        	if(typeof parseFloat($("#montos").val()) === 'number'){
		        	var total = 0;
					$(".icheck").each(function(index) {
						if($(this).prop('checked')){
					      	valor = $(this).attr('data');
					      	total = parseFloat(total) + parseFloat(valor);
					      	console.log(total);
						}
					});
					$("#pago").val(total);
					resultado  =  parseFloat($("#montos").val()) - parseFloat(total);
					if(resultado < 0 ){
						alert('Inicio');
						$("#"+este).prop('disabled', false);
					}
				}	
	        });		

	        $(".btn-danger").click(function(){
	        	  console.log($(this).attr('data'));
		          dataString = "id="+$(this).attr('data');
		          $.ajax({
		              type: "GET",
		              url: "{{route('mc.chequesImg')}}",
		              dataType: 'json',
		              data: dataString,
		              success: function(rsp){
		              		console.log(rsp);	
		              		$("#contenido").html('<img class="img-responsive" src="../facturasDTPlus/'+rsp.imagen+'" style="height: 650px; width: 600px;">');
		              }
		          })      
	        });		

	        $("#id_cheques").on("change", function(){
		          dataString = "id="+$(this).val();
		          $.ajax({
		              type: "GET",
		              url: "{{route('mc.chequesList')}}",
		              dataType: 'json',
		              data: dataString,
		              success: function(rsp){
		              		console.log(rsp);	
		              		$("#moneda").html("  ( <b>"+rsp.moneda+"</b> )");
		              		$("#montos").val(rsp.monto);
		              		$("#montos").prop('disabled', true);
		              }
		          }) 
		          $(".icheck").each(function(index){
		          		$(this).prop('disabled', false);

		          })	

	        });


		});
	</script>
@endsection