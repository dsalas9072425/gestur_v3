{{-- Funcionamiento --}}

@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

@endsection
@section('content')
	@include('flash::message')
	

	<section id="base-style">

		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Asignar Línea de Crédito</h4>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-content collapse show" aria-expanded="true">
				<div class="card-body">

    				<form id="frmLineaCredito" method="post"  action="{{route('guardarCredito')}}">   
							<div class="row">

						        <input type="hidden" name="idPersona" id="idPersona" value="{{$idPersona}}" required>
						        <input type="hidden" name="idEmpresa" value="{{$idEmpresa}}" required>

						        <div class="col-xs-12 col-sm-3" >
							        <div class="form-group">
							            <label>Nombre/Razon Social</label>
							            @php
							            	if(isset($lineaCredito[0]->nombre)){
							            		$nombre = $lineaCredito[0]->nombre." ".$lineaCredito[0]->apellido;
							            	}else{
							            		$nombre ='';
							           		}
							            @endphp
							            <input type="text" class="Requerido form-control"  value="{{$nombre}}" readonly="readonly" />
							        </div>
							    </div>
						

							 
						        <div class="col-xs-12 col-sm-3" >
							        <div class="form-group">
							            <label>Denominacion Comercial</label>
							            @php
							            	if(isset($lineaCredito[0]->nombre)){
							            		$denominancion = $lineaCredito[0]->denominacion_comercial;
							            	}else{
							            		$denominancion ='';
							           		}
							            @endphp
							            <input type="text" class="Requerido form-control" value="{{$denominancion}}" readonly="readonly" />
							        </div>
							    </div>
							    
						        <div class="col-xs-12 col-sm-3" >
							        <div class="form-group">
							            <label>Documento/RUC</label>
							            @php
							            	if(isset($lineaCredito[0]->documento_identidad)){

							            		$documento_identidad = $lineaCredito[0]->documento_identidad;
												if($lineaCredito[0]->dv){
													$documento_identidad .= $documento_identidad."-".$lineaCredito[0]->dv;
												}
							            	}else{
							            		$documento_identidad = "";
							           		}
							            @endphp
							            <input type="text" class="Requerido form-control"  value="{{$documento_identidad}}" readonly="readonly" />
							        </div>
							    </div>

							    <div class="col-xs-12 col-sm-3" >
							        <div class="form-group">
							            <label>Moneda</label>
										<select class="form-control input-sm select2 producto posterior" name="moneda" id="moneda" style="width: 100%;" disabled="disabled">
												<option value="0">Dólares</option>
										</select>
							        </div>
							   </div>
						
								<div class="col-xs-12 col-sm-3">
							            <label>Línea de crédito actual</label> 	
										@php
							            	if(isset($personaLc[0]->monto)){
							            		$monto = number_format($personaLc[0]->monto,2,",",".");
							            	}else{
							            		$monto = 0;
							           		}
							            @endphp

										<input type="text" class="Requerido form-control" id="lineaCreditoActual"  value="{{$monto}}" readonly="readonly" />
							    </div>

							    <div class="col-xs-12 col-sm-3">
							            <label>Saldo actual</label>
										@php
							            	if(isset($personaLc[0]->saldo)){
							            		$saldo = number_format($personaLc[0]->saldo,2,",",".");
							            	}else{
							            		$saldo = 0;
							           		}
							            @endphp
     
										<input type="text" class="Requerido form-control" id="saldoActual" value="{{$saldo}}" readonly="readonly" />
							    </div>

							    <div class="col-xs-12 col-sm-3" >
							        <div class="form-group">
							            <label>Nueva línea crédito</label>
							            <input type="text" class="Requerido form-control numeric" name="lineaCredito" id="lineaCredito"  value="0" required oninput ="calcularSaldo()"/>
							        </div>
							   </div>

							   <div class="col-xs-12 col-sm-3" >
							        <div class="form-group">
							            <label>Nuevo saldo</label>
							            <input type="text" class="Requerido form-control numeric" name="nuevo_saldo" id="nuevo_saldo"  value="" numeric readonly="readonly"/>
							            <div class="form-group has-error">
							            	<span id="helpBlock2" class="help-block">* Nuevo saldo disponible para facturar</span>
							        	</div>
							        </div>
							   </div>
							</div>
						

							<div class="row pb-1">
								<div class="col-xs-12 col-sm-12">
									<button type="button" id="btnGuardar" class="btn btn-success btn-lg pull-right mr-4">Guardar</button>
								</div>
							</div>  
	


            	<div class="table-responsive table-bordered">
	              <table id="listado" class="table">
	                <thead>
					  <tr>
						<th>Nombre/Razon Social</th>
						<th>Denominación Comercial</th>
						<th>Documento/RUC</th>
						<th>Línea Crédito</th>
						<th>Fecha Alta</th>
						<th>Estado</th>

		              </tr>
	                </thead>
	                <tbody id="lista_cotizacion" style="text-align: center">
	 				
				           @foreach($selectPersona as $Personas)
				             <tr>
				                <td>{{$Personas->persona['nombre']}}</td>
				                <td>{{$Personas->persona['denominacion_comercial']}}</td>
				                <td>{{$Personas->persona['documento_identidad']}}</td>
				                <td>{{$Personas->monto}}</td>
				                <td>{{$Personas->fecha}}</td>
				                <td>{{$Personas->id_estado}}</td>
				            </tr>
				            @endforeach  
			        </tbody>
	              </table>
	            </div>  
            
    @endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-dialog.min.js')}}"></script>
	

	<script>
		$(document).ready(function() {
				$('.numeric').inputmask("numeric", {
        		radixPoint: ",",
        		groupSeparator: ".",
        		digits: 2,
        		autoGroup: true,
        		// prefix: '$', //No Space, this will truncate the first character
        		rightAlign: false,
        		// oncleared: function () { self.Value(''); }
			});
		})

		function clean_num(n,bd=false){

				  	if(n && bd == false){ 
					n = n.replace(/[,.]/g,function (m) {  
							 				 if(m === '.'){
							 				 	return '';
							 				 } 
							 				  if(m === ','){
							 				 	return '.';
							 				 } 
							 			});
					return Number(n);
				}
				if(bd){
					return Number(n);
				}
				return 0;

				}//


			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});


			/**
	 * Convierte un texto de la forma 2017-01-10 a la forma
	 * 10/01/2017
	 *
	 * @param {string} texto Texto de la forma 2017-01-10
	 * @return {string} texto de la forma 10/01/2017
	 *
	 */
	function formatoFecha(texto){
			if(texto != '' && texto != null){
		  return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
			} else {
		  return '';	
				
			}

		}

		function calcularSaldo()
		{
			var lineaCreditoActual = clean_num($('#lineaCreditoActual').val());
			var nuevaLineaCredito =  clean_num($('#lineaCredito').val());
			var saldoActual=  clean_num($('#saldoActual').val());

			if (nuevaLineaCredito == '') 
			{
				$('#nuevo_saldo').val('');
			}
			else
			{
				var diferencia = (nuevaLineaCredito - lineaCreditoActual ) + saldoActual;
				$('#nuevo_saldo').val(diferencia);
			}

			//console.log(nuevaLineaCredito+"-"+lineaCreditoActual+"+"+saldoActual);
		}

		$('#btnGuardar').click(function()
		{
			var lineaCreditoActual = clean_num($('#lineaCreditoActual').val());
			var nuevaLineaCredito =  clean_num($('#lineaCredito').val());

			if (nuevaLineaCredito < lineaCreditoActual) 
			{
				return swal({
                        title: "GESTUR",
                        text: "¿Está seguro de que desea disminuir la línea de crédito actual?",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, hacerlo",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
                                     swal("Exito", "", "success");
                                     nuevoCredito();
                                } else {
                                     swal("Cancelado", "", "error");
                                }
            	});


			}
			else
			{
				nuevoCredito();
			}
			});


		function nuevoCredito() 
		{

			var monto = $('#lineaCredito').val();

			if(monto != '')
			{

				var idPersona = $('#idPersona').val();
				var nuevoSaldo = $('#nuevo_saldo').val();
				var dataString = {'lineaCredito':monto,'idPersona':idPersona, 'nuevoSaldo':clean_num(nuevoSaldo)};

				$.ajax({
						type: "GET",
						url: "{{route('guardarCredito')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            position: 'top-right',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },
						success: function(rsp){

							if(rsp.err =='true'){
								$.toast({
							    heading: 'Success',
							    text: 'Se guardó con éxito.',
							    position: 'top-right',
							    showHideTransition: 'slide',
		    					icon: 'success'
							});

						

							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

							

							$.each(rsp.lineaCredito, function (key, item){

							var	fecha = formatoFecha(item.fecha);
								
								var dataTableRow = [
														item.persona.nombre,
														item.persona.denominacion_comercial,
														item.persona.documento_identidad,
														item.monto,
														fecha,
														item.id_estado

													];
								var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

							});

							
							window.location.href = "{{route('lineaCreditoIndex')}}";
						} else {
							  $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrió un error al guardar los datos.',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

						}//else

						
						}//cierreFunc	
				});	
			} else {

				$.toast({
                            heading: 'Error',
                            text: 'Complete el campo Linea Credito',
                             position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });
				}
			}
			
	</script>
@endsection