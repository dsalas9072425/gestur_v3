
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Línea de Crédito</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


				<form id="frmLineaCredito" style="margin-top: 2%;">

				</form>


				<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr>
								<th>Nombre/Razon Social</th>
								<th>Denominación Comercial</th>
								<th>Documento/RUC</th>
								<th>Tipo Persona</th>
								<th>Moneda</th>
								<th>Línea Crédito</th>
								<th>Saldo</th>
								<th></th>
							</tr>
						</thead>
						<tbody style="text-align: center">

							@foreach($personas as $persona)

							<tr>
								<td>
									@php
										$nombre = $persona->nombre;
										if($persona->apellido){
											$apellido = $persona->apellido;
										}else{
											$apellido ="";
										}
									@endphp
									{{$nombre." ".$apellido}}
								</td>
								<td>{{$persona->denominacion_comercial}}</td>
								<td>
									@php
										$ruc = $persona->documento_identidad;
										if($persona->dv){
											$ruc .= $ruc."-".$persona->dv;
										}
									@endphp
									{{$ruc}}
								</td>
								<td>{{$persona->denominacion}}</td>
								<td>{{$persona->moneda}}</td>
								<td>{{number_format($persona->monto,2,",",".")}}</td>
								<td>{{number_format($persona->saldo,2,",",".")}}</td>
								<td>@if($persona->monto > 0)
									<a type="button" href="{{route('lineaCreditoAdd',['id' =>$persona->id])}}"
										id="btnNuevo" class="btn btn-info"
										style="padding-left: 6px;padding-right: 6px;"><i
											class="fa fa-fw fa-edit"></i></a>
									@else
									<a type="button" style="padding-left: 6px;padding-right: 6px;"
										href="{{route('lineaCreditoAdd',['id' =>$persona->id])}}" id="btnNuevo"
										class="btn btn-success"><i class="fa fa-fw fa-plus"></i></a>
									@endif</td>
							</tr>

							@endforeach



						</tbody>
					</table>
				</div>




			</div>
		</div>
	</div>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script>
		//$(document).ready(function() {

		

			$("#listado").dataTable({
				 "aaSorting":[[4,"ASC"]]
				});



		

				
			$('#agenciaId').change(function(){

				 var id = $("#agenciaId option:selected").val();
				 var dir = '{{route('lineaCreditoAdd',['id' =>''])}}/'+ id;

				if(id != ''){
					$('#btnNuevo').removeAttr('disabled','disabled');
					$('#btnNuevo').attr('href',dir);
				} else {
					$('#btnNuevo').attr('disabled','disabled');
					$('#btnNuevo').removeAttr('href','#');
				}


			});	


			


			

	</script>
@endsection