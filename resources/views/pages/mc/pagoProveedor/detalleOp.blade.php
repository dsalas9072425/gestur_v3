<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Detalle OP</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 90%;
			margin:0 auto;
    		z-index:1;
	}
	
	table{
		width: 100%;

	}
	
	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 700;
	}

	.text{
		  overflow: hidden;
		  /*text-overflow: ellipsis;*/
		  white-space: nowrap;
		  display:block;
		  width:100%;
		  min-width:1px;
		}

	.f-10 {
		font-size: 10px !important;
	}

	.f-9 {
		font-size: 9px !important;
	}

	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}

	.f-15 {
		font-size: 15px !important;
	}
	.f-17 {
		font-size: 17px !important;
	}

	.c-text {
		text-align: center;
	}
	.r-text {
		text-align: left;
	}
	
	.r-text-detalle{
		margin-right: 20px;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 10px;
	}

	.b-buttom {
	border-bottom: 1px solid; 
	}

	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:120px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}



	</style>
</head>
<body>
	@forelse ($data as $dat)
		@php
			if($dat->id_moneda == 111){
				if($dat->cotizacionoperativa != ""){
					$cotizacion = $dat->cotizacionoperativa;
				}else{
					$cotizacion = $dat->cotizacion;
				}

			}else{
				if($dat->cotizacionoperativa != ""){
					$cotizacion = $dat->cotizacionoperativa;
				}else{
					$cotizacion = $dat->cotizacion;
				}
			}


        @endphp
		<br>
		<br>
		<div class="container espacio-10">
			<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
				<table>
					<tr>
						<td colspan="6" class="r-text">
								<br>
								<img src="{{$logo}}" style="width: 120px;height: 45px;">
						</td>
					</tr>
					<tr>
						<td colspan="6" class="c-text f-17">
							<b>PAGO DE OP</b>
						</td>
					</tr>
					<tr>
						<td colspan="6" class="r-text">
							<br>
						</td>
					</tr>	
					<tr>
						<td class="r-text f-15">
							Empresa :
						</td>
						<td colspan="4" class="r-text f-15">
							{{$empresa->denominacion}}
						</td>
					</tr>	
					<tr>
						<td class="r-text f-15">
							<label>Nro° OP :</label>
						</td>
						<td class="r-text f-15">
							<?php 
								if(isset($dat->nro_op)){
									$nro_op = $dat->nro_op;
								}else{
									$nro_op = 0;
								}
							?>
							{{$nro_op}}
						</td>
						<td class="r-text f-15">
							<label>Estado :</label>
						</td>
						<td class="r-text f-15">
							{{$dat->estado_n}}
						</td>
						<td class="r-text f-15">
							<label>Moneda : </label>
						</td>
						<td class="r-text f-15">
							{{$dat->currency_code}}
						</td>
						<td class="r-text f-15">
							<label>Cotización : </label>
						</td>
						<td class="r-text f-15">
							{{number_format($cotizacion,2,",",".")}}
						</td>

					</tr>
					<tr>
						<td colspan="6" class="r-text">
						</td>
					</tr>	
					<tr>
						<td class="r-text f-15">
							Proveedor :
						</td>
						<td colspan="4" class="r-text f-15">
							@php
								if(isset($dat->proveedor_n)){
									$proveedor_n= $dat->proveedor_n;
								}else{
									$proveedor_n="";
								}		
							@endphp

							{{$proveedor_n}}
						</td>
					</tr>	
				<table>	
				<br>
				<table>
					<tr>
						<td colspan="6" class="c-text b-buttom">
							<b>Resumen</b>
						</td>
					</tr>
					<tr>
						<td class="r-text">
							<label>Total :</label>
						</td>
						<td class="r-text">
							
						</td>
						<td class="r-text">
							{{number_format($dat->total_pago,2,",",".")}} {{$dat->currency_code}}
						</td> 
					</tr>
					<tr>
						<td class="r-text">
							<label>Retención IVA :</label>
						</td>
						<td class="r-text">
						</td>
						<td class="r-text">
							{{number_format($dat->total_retencion,2,",",".")}} {{$dat->currency_code}}
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>Gastos Adm. :</label>
						</td>
						<td class="r-text">
						</td>

						<td class="r-text">
							{{number_format($dat->total_gastos,2,",",".")}} {{$dat->currency_code}}
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>Descuento Adm :</label>
						</td>
						<td class="r-text">
						</td>
						<td class="r-text">
							{{number_format($dat->total_descuento,2,",",".")}} {{$dat->currency_code}}
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>NCredito + Anticipo :</label>
						</td>
						<td class="r-text">
						</td>
						<td class="r-text">
						<?php
							$total_nc_an = $dat->total_anticipo + $dat->total_nota_credito;
						?>
							{{number_format($total_nc_an,2,",",".")}} {{$dat->currency_code}}
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>A Pagar :</label>
						</td>
						<td class="r-text">
						</td>
						<?php 
							$anticipo = abs((float)$dat->total_anticipo); //Quitamos el signo negativo para sumar
							$nc =  abs((float)$dat->total_nota_credito);
			
							$positivo = (float)$dat->total_pago+(float)$dat->total_gastos;
							$negativo = (float) $dat->total_retencion + (float)$dat->total_descuento + $anticipo + $nc;
							$total_neto_op = $positivo - $negativo 
						?>
						<td class="r-text">
							{{number_format($total_neto_op,2,",",".")}} {{$dat->currency_code}}
						</td>
					</tr>

				<table>	
				<br>
			    <table>
			    	<tr>	
						<td colspan="2" class="r-text b-top">
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<br>
							<label>Entregado a :</label>
						</td>
						<td class="r-text">
							{{$dat->entregado}}
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>Concepto:</label>
						</td>
						<td class="r-text">
							{{$dat->concepto}} 
						</td>
					</tr>
					<tr>
						<td class="r-text" colspan="2">
							<br>
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>Creado por :</label>
						</td>
						<td class="r-text f-12">
							@php
								if(isset($dat->fecha_hora_creacion)){
									$fecha = explode(" ", $dat->fecha_hora_creacion);
									$fecha_format_creacion = date('d/m/Y', strtotime($fecha[0]))." ".$fecha[1];
								}else{
									$fecha_format_creacion ="";
								}		
							@endphp
							{{$dat->persona_creacion_n}} ({{$fecha_format_creacion}})
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>Verificado por :</label>
						</td>
						<td class="r-text f-12">
							@php
								if(isset($dat->fecha_hora_verificado)){
									$fecha = explode(" ", $dat->fecha_hora_verificado);
									$fecha_format_verificado = date('d/m/Y', strtotime($fecha[0]))." ".$fecha[1];
								}else{
									$fecha_format_verificado ="";
								}		
							@endphp

							{{$dat->persona_verificado_n}} ({{$fecha_format_verificado}})
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>Autorizado por :</label>
						</td>
						<td class="r-text f-12">
							@php
								if(isset($dat->fecha_hora_autorizado)){
									$fecha = explode(" ", $dat->fecha_hora_autorizado);
									$fecha_format_autorizado = date('d/m/Y', strtotime($fecha[0]))." ".$fecha[1];
								}else{
									$fecha_format_autorizado ="";
								}		
							@endphp
							{{$dat->persona_autorizado_n}} ({{$fecha_format_autorizado}})
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>Procesado por :</label>
						</td>
						<td class="r-text f-12">
							@php
								if(isset($dat->fecha_hora_procesado)){
									$fecha = explode(" ", $dat->fecha_hora_procesado);
									$fecha_format_procesado = date('d/m/Y', strtotime($fecha[0]))." ".$fecha[1];
								}else{
									$fecha_format_procesado ="";
								}			
							@endphp

							{{$dat->persona_procesado_n}} ({{$fecha_format_procesado}})
						</td>
					</tr>
					<tr>	
						<td class="r-text">
							<label>Anulado por :</label>
						</td>
						<td class="r-text f-12">
							{{$dat->persona_anulacion_n}} ({{$dat->fecha_format_anulacion}})
						</td>
					</tr>
			    </table>
				@if(isset($asiento_detalles[0]->num_cuenta))				
					<table>
						<tr>	
							<td colspan="5" class="c-text b-buttom"><b>Asiento:  {{$asiento[0]->id}} </b></td>
						</tr>	
						<tr>
							<td class="r-text b-buttom">Cuenta</td>
							<td class="r-text b-buttom">Denominación</td>
							<td class="r-text b-buttom">Suc.</td>
							<td class="r-text b-buttom">Debitos</td>
							<td class="r-text b-buttom">Creditos</td>
						</tr>
						@php 
							$debe = 0;
							$haber = 0;
							foreach($asiento_detalles as $key=>$adetalle){
								if($adetalle->sucursal !== null){
									$sucursal = $adetalle->sucursal;
								}else{
									$sucursal = '';
								}
								$debe = $debe + $adetalle->debe;
								$haber = $haber + $adetalle->haber;
						@endphp 		
						<tr>
							<td class="r-text f-11">{{$adetalle->num_cuenta}}</td>
							<td class="r-text f-11">{{$adetalle->cuenta_n}}</td>
							<td class="r-text f-11">{{$sucursal}}</td>
							<td class="r-text f-11">{{number_format($adetalle->debe,0,",",".")}}</td>
							<td class="r-text f-11">{{number_format($adetalle->haber,0,",",".")}}</td>
						</tr>		
						@php  
						}
						@endphp  

						<tr>
							<td style="border-top: solid 2px #000;text-align: right;" class="f-11" colspan="3"><b>TOTALES PYG</b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td style="border-top: solid 2px #000;" class="r-text f-11">{{number_format($debe,0,",",".")}}</td>
							<td style="border-top: solid 2px #000;" class="r-text f-11">{{number_format($haber,0,",",".")}}</td>
						</tr>		

					</table>
					<br>
				@endif		
			    <table>
			    	<tr>	
						<td colspan="5" class="c-text b-buttom"><b>Datos Proveedor</b></td>
					</tr>		
					<tr>
						<td class="r-text b-buttom">Proveedor</td>
						<td class="r-text b-buttom">Agente Retentor</td>
						<td class="r-text b-buttom">Banco</td>
						<td class="r-text b-buttom">Nro.Cuenta </td>
						<td class="r-text b-buttom">Beneficiario</td>
					</tr>
 					<tr>
						<td class="r-text f-11">
							@php
								if(isset($dat->proveedor_n)){
									$proveedor_n= $dat->proveedor_n;
								}else{
									$proveedor_n= "&nbsp;";
								}		
							@endphp


						{{$dat->proveedor_n}}</td>
						<td class="r-text f-11">@if($dat->agente_retentor == "")
									@php 
										echo 'NO';
									@endphp
									@else
									@php 
										echo 'SI';
										@endphp									
									@endif
						</td>
						<td class="r-text f-11">{{$dat->banco}}</td>
						<td class="r-text f-11">{{$dat->nro_cuenta}}</td>
						<td class="r-text f-11">{{$dat->cheque_emisor_txt}}</td>
					</tr>
				 </table>
				<br>
				 <table>
					<tr>	
						<td colspan="4" class="c-text b-buttom"><b>Items Seleccionados</b></td>
					</tr>	
					<tr>
						<td class="r-text b-buttom">Comprobante</td>
						@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1)
							@if($dat->fondo_fijo == 1)
								<td class="r-text b-buttom">Proveedor</th>
							@else	
								<td class="r-text b-buttom">Reserva</th>
							@endif
						@else
						<td class="r-text b-buttom">Reserva/ Fac. Venta</th>
						@endif
						<td class="r-text b-buttom">Fecha</td>
						<td class="r-text b-buttom">Importe</td>
					</tr>
					@foreach($facturas as $key=>$factura)
						<tr>
							<td class="r-text f-11">{{$factura->nro_documento}}</td>
							@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1)
								@if($dat->fondo_fijo == 1)
									<td class="r-text f-11">{{$factura->nombre}}</td>
								@else	
									<td class="r-text f-11">{{$factura->cod_confirmacion}}</td>
								@endif
							@else
								@if($factura->factura_proforma == '')
									<td class="r-text f-11">{{$factura->factura_venta}}</td>
								@else
									<td class="r-text f-11">{{$factura->factura_proforma}}</td>
								@endif
							@endif	
							<td class="r-text f-11">{{$factura->fecha_format}}</td>
							<?php
								if($factura->tipo_documento_n == "ANTICIPO"){
									$monto = -1 * $factura->monto;
								}else{
									$monto = $factura->monto;
								}
							?>
							<td class="r-text f-11">{{number_format($monto,2,",",".")}}</td>
						</tr>
					@endforeach
				</table>
				<br>
 				<table>
					<tr>	
						<td colspan="4" class="c-text b-buttom"><b>Gastos / Descuentos OP</b></td>
					</tr>	
					<tr>
						<td class="r-text b-buttom">Cuenta Contable</td>
						<td class="r-text b-buttom">Concepto</td>
						<td class="r-text b-buttom">Moneda</td>
						<td class="r-text b-buttom">Importe</td>
					</tr>
					@foreach($gastos as $key=>$gasto)
						<?php
							$moneda = 'USD';
							if(isset($gasto->moneda->currency_code)){
								$moneda = $gasto->moneda->currency_code;
							}
							$monto = number_format($gasto->monto,2,",",".");
							if($gasto->monto_moneda_original != ""){
							 	$monto = number_format($gasto->monto_moneda_original,2,",",".");
							}
						?>
						<tr>
							<td class="r-text f-11">{{$gasto->cuenta_contable->descripcion}}</td>
							<td class="r-text f-11">{{$gasto->concepto}}</td>
							<td class="r-text f-11">{{$moneda}}</td>
							<td class="r-text f-11">{{$monto}}</td>
						</tr>
					@endforeach	
				</table>
				<br>
				<table>
					<tr>	
						<td colspan="6" class="c-text b-buttom"><b>Forma Pago</b></td>
					</tr>	
					<tr>
						<td class="r-text b-buttom">Operación</td>
						<td class="r-text b-buttom">Beneficiario</td>
						<td class="r-text b-buttom">Banco</td>
						<td class="r-text b-buttom">Moneda</td>
						<td class="r-text b-buttom">Documento</td>
						<td class="r-text b-buttom">Importe Pago</td>
					</tr>
					@php 
						if(isset($fps->fp_detalle)){
							foreach($fps->fp_detalle as $key=>$fp){
							 if($fps['id_moneda'] == 111){
							 	$moneda ="GS";
							 }elseif($fps['id_moneda'] == 143){
							 	$moneda ="US";
							 }else{
							 	$moneda ="EU";
							 } 
					@endphp
						@php
								if(isset($fp->beneficiario_txt)){
									if(is_null($fp->beneficiario_txt)){
										$beneficiario_txt= "&nbsp;";
									}else{
										$beneficiario_txt= $fp->beneficiario_txt;
									}	
								}else{
									$beneficiario_txt= "&nbsp;";
								}	
								if(isset($fp->banco_detalle->numero_cuenta)){
									$numero_cuenta= $fp->banco_detalle->numero_cuenta;
								}else{
									$numero_cuenta= "&nbsp;";
								}	
								
								if(isset($fp->documento)){
									$documento= "/".$fp->documento;
								}else{
									$documento= "&nbsp;";
								}	
								if(isset($fp->nro_comprobante)){
									$nro_comprobante= $fp->nro_comprobante;
								}else{
									$nro_comprobante= "&nbsp;";
								}	

							@endphp


							<td class="r-text f-11">{{$fp->forma_pago->denominacion}}</td>
							<td class="r-text f-11">{{$beneficiario_txt}}</td>
							<td class="r-text f-11">{{$numero_cuenta}}</td>
							<td class="r-text f-11">{{$moneda}}</td>
							<td class="r-text f-11">{{$nro_comprobante}}</td>
							<td class="r-text f-11">{{number_format($fp->importe_pago,2,",",".")}}</td>
				    @php  
				    	}     
				      }
				    @endphp  
				</div>


				@empty
				<p style="font-size: 30px;font-weight: 800;">OP NO DISPONIBLE</p>
				@endforelse


</body>
</html>
