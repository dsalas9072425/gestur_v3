

@extends('masters')
@section('title', 'Panel de Control')
@section('styles')

	@parent
	  
@endsection
@section('content')

<style type="text/css">
	

</style>

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Aplicar OP</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
                <div class="col-12">
                     <div class="card-header">
                        <h4 class="card-title font-weight-bold" id="heading-labels">Forma de Pago</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements"><span class="badge badge-info ">Campos Obligatorios *</span> </div>
                      </div>
                    </div>
                    <div class="col-12" style="border: solid 2px #000; border-radius:15px;">
                        <div class="row">
                    
                    <form class="col-12" id="formFp" autocomplete="off">
                        <div class="row">
                            <div class="col-12 col-sm-3">
                                <div class="form-group">
                                    <label>Moneda * </label> 
                                    <select class="form-control select2" name="" id="fp_id_moneda" style="width: 100%;">
                                        @foreach ($currency as $div)
                                        <option value="{{$div->currency_id}}">{{$div->currency_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-6 col-sm-3">
                                <div class="form-group">
                                    <label>Cotización *</label>
                                    <input type="text" class="form-control numeric_s" id="fp_cotizacion" value="0" disabled>
                                </div>
                            </div>

                            <div class="col-6 col-sm-3">
                                <div class="form-group">
                                    <label>Indice de cotización</label>
                                    <input type="text" class="form-control" id="fp_indice_cotizacion" value="0" disabled>
                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-group">
                                    <label>Total a Pagar</label>
                                    <input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar numeric" id="fp_total_pago" disabled>
                                </div>
                            </div>
                            <div class="hidden">
                                <div class="form-group">
                                    <label>Diferencia</label>
                                    <input type="text" class="form-control input-fp control-num-moneda-fp" 
                                        id="fp_diferencia_pago" disabled>
                                </div>
                            </div> 
                            <div class="col-12" id="divFormaPago">
                                <div class="form-group">
                                    <label>Forma de Pago *</label>
                                    <select class="form-control select2" name="id_tipo_operacion" id="tipo_operacion"
                                        style="width: 100%;">
                                        @foreach ($tipo_operacion_pago as $operacion)
                                            <option value="{{$operacion->id}}" abreviatura="{{$operacion->abreviatura}}"  data-adjunto="{{$operacion->adjunto_obligatorio}}">{{$operacion->denominacion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                       
                            <div class="col-12"> 
                                <div class="row" id="tipo_operacion_div"></div>
                            </div>   

                        </div>
                    </form>
                          <form class="col-12" id="formFormaPago" autocomplete="off">
                                <div class="table-responsive">
                                    <table id="listadoPagos" class="table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Forma Pago</th>
                                                <th>Importe Pago</th>
                                                <th>Moneda</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody style="text-align: center" class="tbody_fp">
                                            <tr class="odd">
                                                <td valign="top" colspan="6" class="relleno_fp">No hay datos disponibles
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
      


                        </div>           
                </div> <!--RECUADRO -->
                <div class="row">
                    <div class="col-12">
                        <div class="card-header">
                            <h4 class="card-title font-weight-bold" id="heading-labels">Gastos / Descuentos Administrativos</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <span class="badge badge-info">Campos Obligatorios *</span>
                            </div>
                        </div>
                       
                    </div>
                    <div class="col-12" style="border: solid 2px #000; border-radius:15px;">
                        <div class="row">

                    <form class="col-12" id="formDataGastos" autocomplete="off">

                        <div class="row">

                            <div class="col-12 col-sm-6">
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <label>Cuenta Contable (*)</label>
                                    <select class="form-control select2" name="id_cuenta_contable"
                                        id="id_cuenta_contable" style="width: 100%;">
                                        <option value="0">Seleccione Cuenta Contable</option>
                                        @foreach ($data_plan as $cuenta)
                                            @if($cuenta->asentable)
                                            <optgroup label="{{$cuenta->descripcion}}">
                                                @endif
                                                <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}})
                                                    {{$cuenta->descripcion}}
                                                </option>
                                                @if($cuenta->asentable)
                                            </optgroup>
                                            @endif 
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <label>Concepto</label>
                                    <input type="text" class="form-control" name="concepto"
                                        id="concepto" placeholder="Concepto" required>
                                </div>
                            </div>


                            <div class="col-12 col-sm-3">
                                <div class="form-group">
                                    <label>Tipo (*)</label>
                                    <select class="form-control select2" name="id_tipo_gasto_descuento"
                                        id="id_tipo_gasto_descuento" style="width: 100%;">
                                                <option value="0">Seleccione Tipo</option>
                                                <option value="1">Gasto</option>
                                                <option value="2">Descuento</option>
                                    </select>
                                </div>
                            </div>
                        
                            <div class="col-12 col-sm-3">
                                <div class="form-group">
                                    <label>Moneda (*)</label>
                                    <select class="form-control select2" name="id_moneda_gasto" id="id_moneda_gasto" style="width: 100%;">
                                        @foreach ($currency as $div)
                                        <option value="{{$div->currency_id}}">{{$div->currency_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                     

                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label>Importe Gasto/Descuento *</label>
                                <div class="input-group">
                                    <input type="text" class="form-control format-number-control"  placeholder="Importe Gasto" 
                                    id="importe_pago" name="importe_pago" min="1" maxlength="15" required>
                                    <div class="input-group-append" id="button-addon2">
                                        <button class="btn btn-info" id="btnAddGasto" title="Agregar Gasto" type="button">
                                            <i class="fa fa-plus fa-lg"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>	
                    </form>
                    <form class="col-12" id="formGastos" autocomplete="off">

                            <div class="table-responsive">
                                <table id="listadoCuentas" class="table table-hover table-condensed nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Cuenta Contable</th>
                                            <th>Concepto</th>
                                            <th>Moneda</th>
                                            <th>Importe</th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody style="text-align: center" class="tbody_gastos">
                                        <tr class="odd">
                                            <td valign="top" colspan="6" class="relleno_gastos">No hay datos disponibles
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    </form>


                </div>
                </div>

    
                    <div class="col-12 form-horizontal mt-1">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">* Entregado a:</label>
                            <div class="col-sm-10" style="margin-bottom: 10px;">
                            @if(empty($data->entregado))
                                <input type="text" class="text-bold form-control clear_input_txt" id="cf_entrega" value="{{$data->proveedor_n}}">
                            @else
                                <input type="text" class="text-bold form-control clear_input_txt" id="cf_entrega" value="{{$data->entregado}}">
                            @endif
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">* Concepto:</label>
                            <div class="col-sm-10">
                                <input type="text" class="text-bold form-control clear_input_txt" id="cf_concepto">
                            </div>
                        </div>
                    </div>


                    <div class="col-12">
                        <button type="button"  id="btnVolver"  class="btn-mas-request btn btn-danger"><b>Volver</b></button>	
                        <button type="button"  id="btnAplicarOp" class="btn-mas-request btn btn-success pull-right" style="display: none;"><b>Confirmar</b></button>
                        <button type="button"  id="btnVerificarOp" class="btn-mas-request btn btn-success pull-right" style="display: none;" ><b>Confirmar</b></button>
                    </div>


                </div>

            </div>
		</div>
	</div>
</section>







@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>

	
<script type="text/javascript">

//BLOQUEAR EN LA CARGA DE DATOS
blockUI();


const operaciones = {
    data : {!!json_encode($data) !!},
    set_indice_cotizacion : true,
    total_neto: 0,
    id_moneda: 0,
    id: 0,
    id_estado: 0,
    total_pago: 0,
    local: '{{$local}}',
    id_proveedor: 0,
    adjunto_obligatorio: false,
    total : function(){
        return Number(this.data.total_neto_pago) + Number(this.data.total_gastos) - Number(this.data.total_descuento);
    }
}

$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

 $('.numeric_s').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 3,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});


	//DEFINIR FORMATO MONEDAS		
	const EURO = value => currency(value, {
		symbol: "",
		decimal: ',',
		separator: '.',
		precision: 2
	});
	const DOLAR = value => currency(value, {
		symbol: "",
		decimal: ',',
		separator: '.',
		precision: 2
	});
	const GUARANI = value => currency(value, {
		symbol: "",
		separator: '.',
		precision: 0
	});



//PRIMERA CARGA DE DATOS OP
loadDataOp();

function loadDataOp() {
    let total = operaciones.total();
    let id_moneda = operaciones.data.id_moneda;

    
    // $('#total_neto_op').val(formatCurrency(id_moneda, operaciones.data.total_neto_pago));
    // $('#total_retencion').val(formatCurrency(id_moneda, operaciones.data.total_retencion));
    // $('#total_op').val(formatCurrency(id_moneda, operaciones.total()));
    // $('#gastos_administrativos').val(formatCurrency(id_moneda, operaciones.data.total_gastos));
    $('.class_total_op').val(formatCurrency(id_moneda, total));
    $('.moneda_gasto').html(operaciones.data.currency_code);
}



$(document).ready(() => {
     main();
 });




 function controlEstado(){
	//DESACTIVAR BOTONES

	

	let estado = Number(operaciones.data.id_estado);
	//PENDIENTE
	if(estado === 52){
        //SE PUEDE EDITAR LOS GASTOS CARGADOS ANTERIORMENTE
        //CONFIRMAR ES VERIFICAR
        deleteGastoAnterior(true);
        deletePago(true);
        $('#btnVerificarOp').show();
	}
	//AUTORIZADO
	if(estado === 50){
        //YA NO SE PUEDE EDITAR LOS GASTOS 
        //CONFIRMAR ES PROCESAR LA OP
		
		deleteGastoAnterior(false); 
        deletePago(false);
        $('#btnAplicarOp').show();
	}


}//
 






/*-- === === === === === === === === === === === === === === === === === === === === === === === === =
                   CARGA INICIAL DE LOS ITEMS
=== === === === === === === === === === === === === === === === === === === === === === === === == */

//FUNCION AINICIAL
function operacionesLoadData() 
{
    blockUI();
    funcionalidades();
    Promise.all([initFormaPago(), initGastos()]).then(() => {
        if(!fp.habilitado){
            tipo_operacion($('#tipo_operacion').val());
        }
        formatMoneyFp(); //FORMATEO DATOS DESPUES DE LA CARGA
    }).catch(() => {
    }).finally(() => {
        $.unblockUI();
    });
}

function funcionalidades() 
{
  
    $('#fp_id_moneda').val(operaciones.data.id_moneda).trigger('change.select2');
    fp.id_moneda = operaciones.data.id_moneda;

    /*if (!operaciones.local) {
        $('#cf_entrega').val(operaciones.data.proveedor_n);
    }*/
    $('#id_beneficiario').val(operaciones.data.id_proveedor).trigger('change.select2');
}

function main() 
{
 let data = operaciones.data;

    //VALIDAR OP
    if(Object.keys(data).length ){
        controlEstado();
        operacionesLoadData();
        
        //CARGA DE SERVICIOS
        $('.select2').select2();
        initCalendar();
        fechasMomentJs();


        //CARGA DE DATOS OP
        formatNumberControl(); //FORMATEA LOS CAMPOS NUMERICOS


        formatMoneyFp();
        // initFormaPago();
        // initGastos();

    } else {
        $.toast({
            heading: '<b>Atención</b>',
            position: 'top-right',
            text: 'No existen datos de OP',
            width: '400px',
            hideAfter: false,
            icon: 'info'
        });
    }
    formatNumberControl();
}



$('#btnAplicarOp').click(()=>{
    modalAplicarOp();
});

$('#btnVolver').click(()=>{
    redirectControlOp();
});

$('#btnVerificarOp').click(()=>{
    if($('#listadoPagos tr.tabla_filas').length != 0){
        modalVerificar();
    }else{
         $.toast({
                heading: 'Error',
                text: 'Ingrese una forma de pago.',
                position: 'top-right',
                showHideTransition: 'fade',
                icon: 'error'
            });
    }

});



function redirectControlOp(){
    location.href ="{{ route('controlOp',['id'=>'']) }}/"+operaciones.data.id;
}




function initProcesado() {
    $('#cuadroResumen').removeClass('hidden');
    // itemsDatatable();//CARGA ITEMS DE OP
    loadGastos();
    loadFp();
    formatNumberControl();
}




/* === === === === === === === === === === === === === === === === === === === === === === === === =
                                    ABM GASTOS OP
=== === === === === === === === === === === === === === === === === === === === === === === === == */

function initGastos() {
    loadGastos();
}

const gastos_op = {
    total: 0,
    descuento : 0
}

$('#btnAddGasto').click(() => {
    insertGasto()
});

function deleteGastoAnterior(opt)
{
    operaciones.gasto_delete = opt;
}

function deletePago(opt){
    operaciones.pago_delete = opt;
}


function insertGasto() 
{
    $('#btnAddGasto').prop('disabled', true);
    let ok = 0;
    let id_cuenta_contable = $('#id_cuenta_contable').val();
    let concepto = $('#concepto').val();
    var texto_selecionado = $('#id_cuenta_contable :selected').text();
    var texto_selected = texto_selecionado.split(')');
    let texto_select = texto_selected[1].trim();
    let importe_gasto = ($('#importe_pago').val().trim() !== '' ? $('#importe_pago').val().trim() : ok++);
    let id_tipo_gasto_descuento = $('#id_tipo_gasto_descuento').val();
    let id_moneda_gasto = $('#id_moneda_gasto').val();
    var moneda_selecionado = $('#id_moneda_gasto :selected').text();
    var id_moneda = operaciones.data.id_moneda;
    let msj = '';
    var id_gasto = 0;
    let tipo = 'E';
    $('#msj_gasto').removeClass('bg-blue');


    if (ok != 0) {
        msjToast('Cargue un importe válido por favor.', 'error');

    } else {

        let dataString = {
            id_op: operaciones.data.id,
            concepto: concepto,
            id_cuenta_contable: id_cuenta_contable,
            importe: clean_num(importe_gasto),
            id_tipo_gasto_descuento : id_tipo_gasto_descuento,
            id_moneda_gasto : id_moneda_gasto,
            id_moneda_op :id_moneda
        }


        $.ajax({
            type: "GET",
            url: "{{route('insertGasto')}}",
            async: false,
            dataType: 'json',
            cache:false,
            data: dataString,
            error: function (jqXHR, textStatus, errorThrown) {
                //DATO NO ALMACENADO
                msjToast('Ocurrió un error al intentar almacenar los datos.', 'error', jqXHR, textStatus);
                ok++;
            },
            success: function (rsp) {

                if (rsp.err == true) {
                    //DATO ALMACENADO CON EXITO

                    operaciones.total_pago = Number(Number(rsp.total).toFixed(2));
                    msjToast('Línea creada con éxito.', 'success');
                    id_gasto = rsp.id_gasto;
                    setCotizar();
                    loadFp(); //RECARGA FORMA DE PAGO

                } else {

                    //OCUIRRIO UN ERROR AL ALMACENAR LOS DATOS
                    msjToast('Ocurrió un error al intentar almacenar los datos. Motivo: '+rsp.msj, 'error');
                    ok++;
                }



            }
        }); //ajax	



        if (ok == 0) {

            tipo = (id_tipo_gasto_descuento == 1) ? '+' : '-' ;

            let cantItem = $('#listadoCuentas tr').length - 1;
            let tabla = `
                        <tr id="rowGasto_${cantItem}" class="tabla_fila_gasto">
                            <td style="width: 30%; padding-right: 5px;padding-left: 5px;"><input type="text" readonly value = "${texto_select}" class = "form-control input-sm" /></td>
                            <td style="width: 30%; padding-right: 5px;padding-left: 5px;"><input type="text" readonly value = "${concepto}" class = "form-control input-sm"/></td>
                            <td style="width: 15%; padding-right: 5px;padding-left: 5px;"><input type="text" readonly value = "${moneda_selecionado}" class = "form-control input-sm"/></td>

                        `;

                        if(tipo == '-'){
							tabla += `
							<td style="width: 20%; padding-right: 5px;padding-left: 5px;">
							    <div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-minus"></i></span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly value="${importe_gasto}">
									</div>
								</div>
							</td>
							`;
						} else {
							tabla += `
							<td style="width: 20%; padding-right: 5px;padding-left: 5px;">
							  <div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-plus"></i></span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly
											value="${importe_gasto}">
									</div>
								</div>
							</td>
							
							`;
						}

                        tabla += ` <td style="width: 2%; padding-right: 5px;padding-left: 5px;">
                                <button type="button" id= "eliminar_${cantItem}" onclick="eliminarFilaGasto(${cantItem})" data-id="${id_gasto}" class="btn btn-danger"><b>Eliminar</b></button>
                            </td>
                        </tr>`;


            //VALIDAR QUE NO CONTIENE RELLENO
            if ($(".tbody_gastos tr td").hasClass("relleno_gastos")) {
                $(".tbody_gastos").html('');
            }

            let cont = 0;
            //ORDENAR
            $.each($('.tabla_fila_gasto'), function (index, value) {
                let num_cant = value.id.split('rowGasto_');
                // console.log(num_cant[1] != cont);
                if (num_cant[1] != cont) {
                    $('#' + value.id).attr('id', 'rowGasto_' + cont);
                    //MODIFICAR ONCLICK
                    // console.log($(value.children[2]));
                    $(value.children[2]).find('button').attr('onclick', 'eliminarFilaGasto(' + cont + ')');
                }
                cont++;

            });

            $('#listadoCuentas tbody').append(tabla);

            
            if(id_tipo_gasto_descuento == '1'){
            //SUMA GASTOS
            gastos_op.total = gastos_op.total + clean_num(importe_gasto);
            fp.set_total(fp.total + gastos_op.total);

            } else {
            //RESTA GASTOS
            gastos_op.descuento = gastos_op.descuento + clean_num(importe_gasto);
            fp.set_total(fp.total - gastos_op.descuento);

            }

            //	CALCULAR TOTAL
            $('#total_gasto_op').val(gastos_op.total);
            $('#importe_pago').val('');
            $('#concepto').val('');
            $('#id_tipo_gasto_descuento').val('0').trigger('change.select2');
            $('#id_cuenta_contable').val('0').trigger('change.select2');
            $('.class_total_op').val(operaciones.total_pago);
        }

        //LLAMAR A FORMATEAR NUMEROS
        formatNumberControl();

    } //ok != 0
    $('#btnAddGasto').prop('disabled', false);
} //function

function eliminarFilaGasto(row_id) 
{



    let ok = 0, cont = 0;
    let id_gasto = $("#eliminar_" + row_id).attr('data-id');
    // console.log($("#eliminar_" + row_id).attr('data-id'));
    $.ajax({
        type: "GET",
        url: "{{route('deleteGasto')}}",
        async: false,
        dataType: 'json',
        cache:false,
        data: {
            id_gasto: id_gasto,
            id_op: operaciones.data.id
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //DATO NO ALMACENADO
            msjToast('', 'error', jqXHR, textStatus);
            ok++;
        },
        success: function (rsp) {

            if (rsp.err == true) {
                //DATO ELIMINADO CON EXITO
                msjToast('Linea eliminada.', 'info');
                gastos_op.total = Number(Number(rsp.total_gastos).toFixed(2));
                operaciones.total_gasto = Number(Number(gastos_op.total).toFixed(2));
                operaciones.total_pago = Number(Number(rsp.total_op).toFixed(2));

                $('#total_gasto_op').val(gastos_op.total);
                $('.class_total_op').val(operaciones.total_pago);
                // setCotizar();


            } else {

                //OCUIRRIO UN ERROR AL ALMACENAR LOS DATOS
                msjToast('Ocurrió un error al intentar eliminar la línea.', 'error');
                ok++;
            }



        }
    }).done(() => {
        if (ok == 0) {
            $('#rowGasto_' + row_id).remove();
            loadFp();


            //VALIDAR QUE NO CONTIENE RELLENO
            let cantItem = $('#listadoCuentas tr').length - 1;
            if (cantItem == 0) {
                $(".tbody_gastos").html(`<tr class="odd"><td valign="top" colspan="6" class="relleno_gastos">No hay datos disponibles</td></tr>`);
            } else {

                //ORDENAR
                $.each($('.tabla_fila_gasto'), function (index, value) {
                    let num_cant = value.id.split('rowGasto_');
                    // console.log(num_cant[1] != cont);
                    if (num_cant[1] != cont) {
                        $('#' + value.id).attr('id', 'rowGasto_' + cont);
                        //MODIFICAR ONCLICK
                        // console.log($(value.children[2]));
                        $(value.children[2]).find('button').attr('onclick', 'eliminarFilaGasto(' + cont + ')');
                    }
                    cont++;

                });
            }
        }

    }); //ajax	
} //


function loadGastos() 
{
    return new Promise((resolve, reject) => {

        let id_table =  'listadoCuentas';

        $.ajax({
            type: "GET",
            url: "{{route('getGastos')}}",
            dataType: 'json',
            cache:false,
            data: {
                id_op: operaciones.data.id
            },

            error: function () {

                msjToast('', 'error', jqXHR, textStatus);
                reject(false);
            },
            success: function (rsp) {

                if (rsp.err == true) {
                    let cantItem = 0;
                    let total_gasto = 0;
                    let tabla;
                    let tipo = 'E';

                    if (rsp.data.length > 0) {
                        $('#' + id_table + ' tbody').empty();
                        gastos_op.total = 0;

                        $.each(rsp.data, (index, value) => {
                            // console.log(value);
                            gastos_op.total += clean_num(value.monto, true);

                            var moneda = ''   
                            if(jQuery.isEmptyObject(value.moneda) == false){
                                moneda = value.moneda.currency_code;
                            }else{
                                moneda = 'USD';
                            }
                            if(jQuery.isEmptyObject(value.monto_moneda_original) == false){
                                monto_mostrar = value.monto_moneda_original;
                            }else{
                                monto_mostrar = value.monto;
                            }
                            tabla = `
                                    <tr id="rowGasto_${cantItem}" class="tabla_fila_gasto">
                                        <td style="width: 30%; padding-right: 5px;padding-left: 5px;"><input type="text" readonly value = "${value.cuenta_contable.descripcion}" class = "form-control input-sm" /></td>
                                        <td style="width: 30%; padding-right: 5px;padding-left: 5px;"><input type="text" readonly value = "${value.concepto}" class = "form-control input-sm" /></td>
                                        <td style="width: 15%; padding-right: 5px;padding-left: 5px;"><input type="text" readonly value = "${moneda}" class = "form-control input-sm" /></td>`;
                       if(value.tipo == '-'){
							tabla += `
							<td style="width: 20%; padding-right: 5px;padding-left: 5px;">
							    <div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-minus"></i></span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly value="${value.monto_moneda_original}">
									</div>
								</div>
							</td>
							`;
						} else {
							tabla += `
							<td style="width: 20%; padding-right: 5px;padding-left: 5px;">
							  <div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-plus"></i></span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly
											value="${monto_mostrar}">
									</div>
								</div>
							</td>
							
							`;
						}            

                            if (operaciones.gasto_delete) {
                                tabla += `
                                        <td style="width: 2%; padding-right: 5px;padding-left: 5px;">
                                            <button type="button" id= "eliminar_${cantItem}" onclick="eliminarFilaGasto(${cantItem})" data-id="${value.id}" class="btn btn-danger btnDelGasto"><b>Eliminar</b></button>
                                        </td>
                                    </tr>`;
                            } else {
                                if(value.editable){
                                   // $('#divFormaPago').css('display', 'block');
                                    tabla += `
                                        <td style="width: 2%; padding-right: 5px;padding-left: 5px;">
                                            <button type="button" id= "eliminar_${cantItem}" onclick="eliminarFilaGasto(${cantItem})" data-id="${value.id}" class="btn btn-danger btnDelGasto"><b>Eliminar</b></button>
                                        </td>
                                    </tr>`;

                                    } else {
                                       // $('#divFormaPago').css('display', 'none');
                                        tabla += `<td style="width: 2%; padding-right: 5px;padding-left: 5px;">
                                                        <button type="button"  disabled class="btn btn-danger"><b>Eliminar</b></button>
                                                    </td>
                                                    </tr>`;
                                    }

                            }

                            $('#' + id_table + ' tbody').append(tabla);
                            cantItem++;
                        });

                        $('#total_gasto_op').val(gastos_op.total);
                        $('#gastos_total').val(gastos_op.total);

                    }
                    resolve({
                        resp: true
                    });
                    //LLAMAR A FORMATEAR NUMEROS
                    formatNumberControl();


                } else {
                    reject(false);
                    msjToast('Ocurrió un error al intentar obtener los datos.', 'error');
                }


            } //

        });

    }); //PROMISE

}

//HABILITA O DESHABILITA LOS GASTOS
function estateGastos(_switch)
{   
    $('#concepto').prop('disabled',_switch);
    $('#id_cuenta_contable').prop('disabled',_switch);
    $('#importe_pago').prop('disabled',_switch);
    $('#btnAddGasto').prop('disabled',_switch);
    $('.btnDelGasto').prop('disabled',_switch);

}





/* === === === === === === === === === === === === === === === === === == === === === === === === === ==
                                   ABM FORMA DE PAGO
   === === === === === === === === === === === === === === === === === == === === === === === === === == */


const fp = 
{
    diferencia: 0,
    id_moneda: $('#fp_id_moneda').val(),
    total: 0,
    txt_moneda: '',
    cotizacion: 0,
    requeridos: [],
    set_total: function (monto) {
        this.total = monto;
        $('.importe_a_pagar').val(monto);
    },
    setDiferencia : function(dif){
        this.diferencia = dif;
    }
}


function formaPago() 
{
    var indice = $('#tipo_operacion :selected').text();
    $('#operacion').val(indice);
}


function initFormaPago() 
{
    levantar_eventos_fp();
    Promise.all([loadFp()]).then(() => {
        //SI EXISTE FP
        // calcOldFp();


    }).catch(() => {
        //SI NO EXISTE FP
        calcNewFp();
    }).finally(() => {

    });
    cargarConcepto();
}


//RECALCULAR VALOR A LA MONEDA SELECCIONADA Y FORMATEAR CAMPOS
$('#fp_id_moneda').change(function () 
{   
    fp.id_moneda = $('#fp_id_moneda').val();
    $('#fp_indice_cotizacion').val(0);
    calcNewFp();
});

// let typingTimer;
// let doneTypingInterval = 500; // Tiempo de espera en milisegundos

// $('#fp_indice_cotizacion').keyup(function() {
//   operaciones.set_indice_cotizacion = false;
//   clearTimeout(typingTimer);
//   typingTimer = setTimeout(calcNewFp(true), doneTypingInterval);
// });



//SI NO HAY FORMA DE PAGO
function calcNewFp(block = false) 
{

    if(block){
        BlockFp();
    }

    Promise.all([getCotizacion(), setCotizar()]).then(() => {
        UnblockFp();
        formatNumberControl();
    }).catch(() => {
        BlockFp();
        console.log('Ocurrió un error al calcular los datos de forma de pago');
    }).finally(() => {
        //FIN PROCESO
    });
    formatMoneyFp();
}

function estateFp(_switch)
{   
    fp.habilitado = _switch;
    $('.importe_a_pagar').prop('disabled', _switch);
    $('#fp_cotizacion').prop('disabled', _switch);
    $('#tipo_operacion').prop('disabled',_switch);
    $('#btnAddFormaPago').prop('disabled',_switch);
}

/*
 *Se utiliza cuando existe una forma de pago
 */
 function calcOldFp() 
{

    $('#fp_diferencia_pago').val(fp.diferencia);
    $('#fp_id_moneda').val(fp.id_moneda).trigger('change.select2');
    // $('#fp_id_moneda').prop('disabled', true);
    $('#fp_total_pago').val(fp.total);
    $('.importe_a_pagar').val(fp.total);
    $('#fp_cotizacion').val(fp.cotizacion);

    formatMoneyFp();

    if (fp.diferencia <= 0) {
        console.log('La diferencia es mayor o igual al total');
        BlockFp();
    } else {
        console.log('La diferencia NO es mayor o igual al total');
        UnblockFp();
    }

    estateFp(true); //DESHABILITA CARGA DE PAGOS
}

//CARGA LAS FORMAS DE PAGO DE LA OP
function loadFp() 
{
    return new Promise((resolve, reject) => {
	
        let id_table =  'listadoPagos';
        let dataString = {
            id_op: operaciones.data.id
        }
        let tabla;

        $.ajax({
            type: "GET",
            url: "{{route('getFp')}}",
            data: dataString,
            cache:false,
            async: false,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                msjToast('', 'error', jqXHR, textStatus);
                reject(false);
            },
            success: function (rsp) {

                if (rsp.err == true) {

                    if (rsp.data != null) {

                        if (rsp.data.fp_detalle.length > 0) {
                            $('#' + id_table + ' tbody').empty();
                        }

                        let cantItem = 0,
                            moneda = rsp.data.moneda.currency_code,
                            diferencia = 0,
                            total_bd = 0;
                        fp.id_moneda = rsp.data.moneda.currency_id;
                        fp.set_total(clean_num(rsp.data.importe_total, true));
                        fp.cotizacion = rsp.data.cotizacion;


                        $.each(rsp.data.fp_detalle, (index, value) => {
                           /* if(value.forma_pago.id){
                                $('#cf_entrega').val( );

                            }*/
                           $('#tipo_operacion').val(value.forma_pago.id).change();
                        tabla = `
                        <tr id="rowPago_${cantItem}" class="tabla_filas">
                            <td><input type="text" readonly value = "${value.forma_pago.denominacion}" class = "form-control input-sm" /></td>
                            <td><input type="text" readonly value = "${value.importe_pago}" class="input_importe_pago form-control input-sm format-number-control"/></td>
                            <td><input type="text" readonly value = "${moneda}" class = "form-control input-sm"/></td>`;
                           // alert(operaciones.pago_delete);
                            if(operaciones.pago_delete){
                              //  $('#divFormaPago').css('display', 'block');
                                tabla += `
                                <td>
                                <button type="button" onclick="eliminarFilaPago(${cantItem})" data-id=${value.id}  class="btn btn-danger"><b>Eliminar</b></button>
                                </td> 
                            </tr>`;
                            } else {
                                $('#divFormaPago').css('display', 'none');
                                tabla += `
                                <td>
                                    <button type="button" disabled class="btn btn-danger"><b>Eliminar</b></button>
                                </td> 
                                </tr>`;
                            }
                           
                        


                            $('#' + id_table + ' tbody').append(tabla);
                            //TOTAL IMPORTE PAGADO HASTA EL MOMENTO
                            total_bd += clean_num(value.importe_pago, true);
                            cantItem++;
                            formaPago();
                        });

                        BlockFp();
                        // console.log('FP TOTAL EN LOAD FP',fp.total,'TOTAL BD',total_bd);
                        diferencia = fp.total - total_bd;
                        //CARGA LA DIFERENCIA DE FORMA DE PAGO
                        fp.setDiferencia(clean_num(diferencia, true));
                        // console.log('FP DIFERENCIA OBTENIA DE LOAD FP',fp.diferencia);
                        $('#forma_pago_total').val(fp.total);

    
                            calcOldFp();


                        //SI EXISTE FP
                        resolve({
                            resp: true
                        });

                        //LLAMAR FORMATEO
                        formatNumberControl()
                    } else {
                        //NO EXISTE FP 
                        reject(false);
                    }

                } else {
                    msjToast('Ocurrió un error al intentar recuperar los datos', 'error');

                    reject(false);
                }

            } //success
        }) //ajax
    }) //promise
} //

function levantar_eventos_fp() 
{


    //DESACTIVAR EVENTOS ANTERIORES
    $('#btnAddFormaPago').unbind();
    $('#fp_id_origen_pago').unbind();
    $('#fp_id_cuenta').unbind();
    $('#btnAdjuntoModal').unbind();

    //MODAL PARA ADJUNTAR DETALLES 
    $('#btnAdjuntoModal').click(() => {
        $('#modalAdjunto').modal({
            backdrop: 'static'
        });
    });


    $('#modalAdjunto').on('hidden.bs.modal', function (e) {
        $('#modalGastos').modal('handleUpdate');
    })


    // $('#fp_id_origen_pago').select2('destroy');
    // $('#fp_id_cuenta').select2('destroy');	

    //ACTIVAR EVENTOS NUEVOS
    $('#fp_id_origen_pago').select2();
    $('#fp_id_cuenta').select2();

    $('#fp_id_origen_pago').change(function (){
      ///  alert($('#fp_id_origen_pago').val());
       getCuentas($('#tipo_operacion').val()); 
        cargarConcepto();
    });

    $('#btnAddFormaPago').click(() => {
        cargarConcepto();
        insertFormaPago();
    });

    $('#fp_id_cuenta').change(() => {
        cargarConcepto();
    });

    $('#portador_check').click(() => {
        $("#beneficiario_texto").toggleClass("desactivar");
        $("#id_beneficiario").toggleClass("desactivar");

        ($("#beneficiario_texto").hasClass('desactivar')) ? $("#beneficiario_texto").prop('disabled', true): $("#beneficiario_texto").prop('disabled', false);
        ($("#id_beneficiario").hasClass('desactivar')) ? $("#id_beneficiario").prop('disabled', true): $("#id_beneficiario").prop('disabled', false);

    });

    $('#id_beneficiario').val(operaciones.id_proveedor).trigger('change.select2');
    //getCuentas();
    initCalendar();
    formatMoneyFp();

} //


function cargarConcepto() 
{  
        let msj = 'OP ' + operaciones.data.nro_op;

        if ($('#tipo_operacion :selected').text() != '')
            msj += ' ' + $('#tipo_operacion :selected').text().toUpperCase().substring(0, 4);

        msj += ' ' + operaciones.data.proveedor_n + ' {{$facturasRecortadas}}';

        // console.log(msj);

        $('#cf_concepto').val(msj);


}

function insertFormaPago() 
{
    BlockFp();
    let ok = 0,
        id_banco_detalle = $('#fp_id_cuenta').val(),
        id_forma_pago = $('#fp_id_origen_pago').val(),
        texto_cuenta = $('#fp_id_cuenta :selected').text(),
        texto_origen = $('#fp_id_origen_pago :selected').text(),
        tipo_operacion_txt = $('#tipo_operacion :selected').text(),
        documento = $('#fp_documento_forma_pago').val(),
        importe_pago = $('#fp_importe_pago').val(),
        id_moneda = fp.id_moneda,
        texto_moneda = $('#fp_id_moneda :selected').text(),
        moneda_cuenta = parseInt($("#fp_id_cuenta :selected").attr('moneda')),
        nro_comprobante = $('#nro_comprobante').val(),
        fecha_emision = $('#fecha_emision').val(),
        fecha_vencimiento = $('#fecha_vencimiento').val(),
        id_tipo_operacion = $('#tipo_operacion').val(),
        importe_cotizado = 0,
        monto_pago = (fp.id_moneda == 111) ? Number(round(fp.total,0)): Number(round(fp.total,2)),
        diferencia = 0,
        importe_pago_format,
        cantItem = $('#listadoPagos tr.tabla_filas').length,
        dif_validate = round(fp.diferencia),
        // indice_cotizacion = $('#fp_indice_cotizacion').val(),
        id_fp_detalle;

    let msj = '';

    //VALIDAR LOS CAMPOS REQUERIDOS
    if (!validarCamposFp()) {
        ok = 1;
         msjToast('Complete los campos obligatorios por favor.', 'info');
    }

    var importe = $('#fp_importe_pago').val();
    if (ok != 0) {
        //PENDIENTE CARGADOR
        msjToast('Complete los campos obligatorios por favor.', 'info');

    } else {

        if(id_moneda == 143){
            importe_pago= importe.replace(".", "").replace(",", ".");
            importe_pago_format = Number(round(importe_pago,2));
        }else{  
            importe_pago_format = clean_num(importe);
        } 

        if (cantItem == 1) {
            msjToast('Sólo se puede cargar una forma de pago.', 'info');
            ok++;
        }

        if($('#fp_id_cuenta').val() == "" ||$('#fp_id_cuenta').val() == 0) {
            msjToast('Seleccione una Cuenta de Fondo.', 'info');
            ok++;
        }


        if (!isNaN(importe_pago_format)) {
            if (importe_pago_format <= 0) {
                ok++;
                $('#carga_forma_paga').hide(2000);
                msjToast('El importe no puede ser cero o menor a cero.', 'error');
            } else if (importe_pago_format > monto_pago) {
                ok++;
                $('#carga_forma_paga').hide(2000);
                msjToast('El importe no puede ser mayor al monto de pago.', 'error');
            } else if (importe_pago_format > dif_validate) {
                ok++;
                $('#carga_forma_paga').hide(2000);
                msjToast('El importe de pago no debe ser mayor a la diferencia.', 'error');
            }

        } else {
            ok++;
            $('#carga_forma_paga').hide(2000);
            msjToast('Error desconocido al realizar los cálculos.', 'error');
        }
    }

    if (ok === 0) {
        diferencia = fp.diferencia - importe_pago_format;
            moneda_cuenta = $("#fp_id_cuenta :selected").attr('moneda');
            if(moneda_cuenta == 0 || moneda_cuenta === null || jQuery.isEmptyObject(moneda_cuenta) == true){
                indicador = 1;
            }else{
                if (parseInt($("#fp_id_cuenta :selected").attr('moneda')) == parseInt($('#fp_id_moneda').val())) {
                    indicador = 1;
                }else{
                    indicador = 0;
                }   
            }
            if(indicador == 1){
                if($('#portador_check').prop('checked')) {
                        sbeneficiario='';
                        ibeneficiario = 0;
                    }else{
                        sbeneficiario= $('#beneficiario_texto').val();
                        ibeneficiario = $('#id_beneficiario').val()
                    }
                    let dataString = {
                        id_op: operaciones.data.id,
                        total: fp.total,//TENER EN CUENTA PARA CAMBIAR
                        cotizacion: clean_num($('#fp_cotizacion').val()),
                        documento: documento,
                        importe_pago: fp.total,//TENER EN CUENTA PARA CAMBIAR
                        id_forma_pago: id_forma_pago,
                        id_banco_detalle: id_banco_detalle,
                        id_moneda: fp.id_moneda,
                        al_portador: $('#portador_check').is(":checked"),
                        nro_comprobante: nro_comprobante,
                        id_tipo_operacion: id_tipo_operacion,
                        fecha_emision: fecha_emision,
                        fecha_vencimiento: fecha_vencimiento,
                        beneficiario: sbeneficiario,
                        id_beneficiario: ibeneficiario,
                        nombre_imagen: $("#imagen").val(),
                        // indice_cotizacion : indice_cotizacion
                    }
                

                    $.ajax({
                        type: "GET",
                        url: "{{route('insertFp')}}",
                        data: dataString,
                        async: false,
                        dataType: 'json',
                        cache:false,
                        error: function (jqXHR, textStatus, errorThrown) {
                            msjToast('Ocurrió un error en la comunicación con el servidor.', 'error');
                            ok++;
                        },
                        success: function (rsp) {
                            if (rsp.err == true) {
                                id_fp_detalle = rsp.id_fp_detalle;
                                msjToast('Línea creada con éxito.', 'success');

                                estateFp(true); //DESHABILITA CARGA DE PAGOS
                            } else {
                                msjToast('Ocurrió un error al intentar almacenar la línea. Motivo: '+rsp.msj, 'error');
                                ok++;
                            }

                        } //success
                    }) //ajax
            }else{
                msjToast('La moneda de la Cuenta de Fondo no corresponde a la moneda de OP.', 'error');
            }

    } //if


    if (ok == 0) {
        if(indicador == 1){
            /*INSERTAR FILA*/
            let cantItem = $('#listadoPagos tr.tabla_filas').length;
            let tabla = `
            <tr id="rowPago_${cantItem}" class="tabla_filas">
                <td><input type="text" readonly value = "` + $('#tipo_operacion :selected').text() + `" class = "form-control input-sm" /></td>
                <td><input type="text" readonly value = "${importe_pago}" class = "input_importe_pago form-control input-sm format-number-control" name="datos[][importePago]"/></td>
                <td><input type="text" readonly value = "${texto_moneda}" class = "form-control input-sm"/></td>
                <td><button type="button" onclick="eliminarFilaPago(${cantItem})" data-id=${id_fp_detalle}  class="btn btn-danger"><b>Eliminar</b></button>
                </td>
            </tr>
            `;
            resultadoOperacion = tipo_operacion_txt.substring(0, 4);


            $('#cf_concepto').val('');

            $('#cf_concepto').val('OP ' + operaciones.data.nro_op + ' ' + resultadoOperacion.toUpperCase() + ' ' + texto_origen + ' ' + operaciones.data.proveedor_n + ' {{$facturasRecortadas}}');

            if ($('.tbody_fp tr td').hasClass('relleno_fp')) {
                $('.tbody_fp').html('');
            }
            $('#listadoPagos tbody').append(tabla);

            let cont = 0;
            $.each($('.tabla_filas'), function (index, value) {
                let num_cant = value.id.split('rowPago_');
                if (num_cant[1] != cont) {
                    $('#' + value.id).attr('id', 'rowPago_' + cont);
                    //MODIFICAR ONCLICK
                    $(value.children[5]).find('button').attr('onclick', 'eliminarFilaPago(' + cont + ')');
                }
                cont++;

            });

            //INSERTAR DIFERENCIA			
            fp.setDiferencia(clean_num(diferencia.toFixed(2), true));
            $('#fp_diferencia_pago').val(fp.diferencia);


            /*LIMPIAR CAMPOS*/
            $("#fp_documento_forma_pago").val('');
            // $('#fp_id_moneda').prop('disabled', true);

            $('#fp_id_forma_pago').val('1').trigger('change.select2');
            $('#cuenta_banco_select').val('').trigger('change.select2');
        }
    }


    if (fp.diferencia == 0 && ok == 0) {
        BlockFp();
    } else {
        UnblockFp();
    }

    //LLAMAR FORMATEO
    formatNumberControl()

} //function


function eliminarFilaPago(id) 
{

    let dataString = {
        id_fp_detalle: $('#rowPago_' + id).find('button').attr('data-id'),
        id_op: operaciones.data.id
    }
    let ok = 0;

    $.ajax({
        type: "GET",
        url: "{{route('deleteFp')}}",
        data: dataString,
        cache:false,
        async: false,
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            msjToast('', 'error', jqXHR, textStatus);
            ok++;
        },
        success: function (rsp) {
            if (rsp.err == false) {
                msjToast('Ocurrió un error al intentar eliminar la línea.', 'error');
                ok++;
            }

        } //success
    }).done(() => {
     
        if (ok == 0) {
            
            let monto_resta = $('#rowPago_' + id).find('input.input_importe_pago').val()
            let dato_monto_resta = clean_num(monto_resta);
            fp.setDiferencia(fp.diferencia + dato_monto_resta);
            // fp.diferencia = fp.diferencia + dato_monto_resta;

            $('#fp_diferencia_pago').val(fp.diferencia);
            $('#rowPago_' + id).remove();

            let cantItem = $('#listadoPagos tr').length - 1;
            if (cantItem == 0) {
                //HABILITAR COMBO DE MONEDA
                // $('#fp_id_moneda').prop('disabled', false);
                $('.tbody_fp').html(`<tr class="odd"><td valign="top" colspan="6" class="relleno_fp">No hay datos disponibles</td></tr>`);

            }
            estateFp(false); //HABILITA CARGA DE PAGOS
            estateGastos(false);//HABILITA CARGA DE GASTOS

            let  id_item = $('#tipo_operacion').val();
            tipo_operacion(id_item);

            //HABILITAR BTN AGREGAR FILA
            UnblockFp();
            msjToast('Línea Eliminada.', 'info');




        } //if
    }) //ajax









    // $('#carga_forma_paga').show();

    // $('#carga_forma_paga').hide(2000);
    // $('#btnGuardarFormaPago').prop('disabled',true);
} //function



/*================================================================================================
                                    LOGICA FORMA DE PAGO
================================================================================================*/

$('#tipo_operacion').on('select2:selecting', function (e) 
{
    operaciones.adjunto_obligatorio = e.params.args.data.element.dataset.adjunto;
    let id_item = e.params.args.data.id;
    tipo_operacion(id_item);
});


function tipo_operacion(id_operacion) 
{

    let tipo_operacion = id_operacion;
    let efectivo,
        tarjeta,
        cheque,
        transferencia,
        html = '';

    let list_beneficiario = [
        @foreach($beneficiario as $ben) {
            @php
				$ruc = $ben->documento_identidad;
				if($ben->dv){
				$ruc .= $ruc."-".$ben->dv;
				}
			@endphp
            id: {{$ben->id}},
            text: "{{$ruc}} {{$ben->denominacion_comercial}} {{$ben->nombre}} {{$ben->apellido}}"
        },
        @endforeach
    ];


    transferencia = `
        <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Cuenta Fondo Padre</label> 						 
                    <select class="form-control select2" name="" id="fp_id_origen_pago" style="width: 100%;">		
                     @foreach ($origen as $forma)
                         <option value="{{$forma->id}}">{{$forma->nombre}}</option>
                     @endforeach		
                    </select>
                </div> 
            </div>

            <div class="col-12  col-sm-6">
                    <div class="form-group">
                        <label>Cuenta de Fondo</label> 						 
                        <select class="form-control select2" name="" disabled id="fp_id_cuenta" style="width: 100%;">			
                        </select>
                    </div> 
            </div>


            
            <div class="col-12 col-sm-6">
                    <div class="form-group">
                         <label>Nro de Transferencia</label>
                       <input type="text" class="form-control clear_input_txt" id="nro_comprobante" value="0">
                   </div>
               </div>

               <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Fecha Emisión *</label>						 
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                        </div>
                            <input type="text"  class="form-control single-picker" id="fecha_emision" />
                    </div>
                </div>	
            </div>

              <!-- <div class="col-12 col-sm-6 adjuntoHiden">
                    <label>Subir Archivo Adjunto *</label>
                        <button type="button" id="btnAdjuntoModal" class="btn btn-primary btn-block">Adjunto</button>
                    </div>
                    <input type="hidden" name="nombreImagen" value="" id="imagen">
              </div> -->

           <div class="col-12">
                    <div class="form-group">
                        <label>Importe Pago *</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control control-num-moneda-fp input-fp importe_a_pagar" id="fp_importe_pago" name="fp_importe_pago">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary" id="btnAddFormaPago" title="Agregar Forma Pago" type="button"><i class="fa fa-plus fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                  </div> `;

    cheque = `			

        <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Cuenta Fondo Padre *</label> 						 
                    <select class="form-control select2" name="" id="fp_id_origen_pago" style="width: 100%;">		
                     @foreach ($origen as $forma)
                         <option value="{{$forma->id}}">{{$forma->nombre}}</option>
                     @endforeach		
                    </select>
                </div> 
            </div>


                <div class="col-12  col-sm-6">
                    <div class="form-group">
                        <label>Cuenta de Fondo *</label> 						 
                        <select class="form-control select2" name="" disabled id="fp_id_cuenta" style="width: 100%;">			
                        </select>
                    </div> 
                </div>

       <div class="col-4 col-sm-4">
         <div class="form-group">
               <label>Fecha Emisión *</label>						 
               <div class="input-group">
                   <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                   </div>
                      <input type="text"  class="form-control single-picker" id="fecha_emision" />
               </div>
           </div>	
       </div>	

       <div class="col-6  col-sm-4">
            <div class="form-group">
               <label>Fecha Vencimiento *</label>						 
               <div class="input-group">
                   <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                   </div>
                      <input type="text"  class="form-control single-picker" id="fecha_vencimiento" />
               </div>
           </div>
       </div>

        <div class="col-6 col-sm-4">
            <div class="form-group">
                <label>Nro de Cheque *</label>
                <input type="text" class="form-control clear_input_txt" id="nro_comprobante" value="0">
            </div>
       </div>


        <div class="col-12  col-sm-8">
            <div class="form-group" style="display:none">	
                <label>Elegir Nombre</label> 
                <select class="form-control select2" name="id_beneficiario" id="id_beneficiario" style="width: 100%;">
                </select>
            </div> 
            <div class="form-group">
                <label>Beneficiario</label>         
                <input type="text" class="form-control clear_input_txt" name="beneficiario_texto" id="beneficiario_texto" value="{{$data->proveedor_n}}">
            </div> 
        </div>

        <div class="col-12  col-sm-4">
            <label> </label> 
            <div class="checkbox">
                <label>
                <input type="checkbox" id="portador_check" name="portador"> Al portador
                </label>
            </div>
        </div>

         <!--<div class="col-12">
            
        </div>

        <div class="col-12 adjuntoHiden">
                    <label>Subir Archivo Adjunto</label>
                        <button type="button" id="btnAdjuntoModal" class="btn btn-primary btn-block">Adjunto</button>
                    </div>
                    <input type="hidden" name="nombreImagen" value="" id="imagen">
        </div> -->

           <div class="col-12">
                    <div class="form-group">
                        <label>Importe Pago *</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control control-num-moneda-fp input-fp importe_a_pagar" id="fp_importe_pago" name="fp_importe_pago">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary" id="btnAddFormaPago" title="Agregar Forma Pago" type="button"><i class="fa fa-plus fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                  </div>
           
           `;

    efectivo = `
                 <!-- <div class="col-12 adjuntoHiden" id="inputimagen">
                    <label>Subir Archivo Adjunto</label>
                        <button type="button" id="btnAdjuntoModal" class="btn btn-primary btn-block">Adjunto</button>
                    </div>
                    <input type="hidden" name="nombreImagen" value="" id="imagen">
              </div>

        <div class="col-12 col-sm-6" id="inputfp_documento_forma_pago">
               <div class="form-group">
                   <label>Documento </label>
                   <input type="text" class="form-control" name="documento" id="fp_documento_forma_pago" placeholder="Documento">
               </div>
           </div> -->

        <input type="hidden" class="form-control" value="0" name="documento" id="fp_documento_forma_pago" placeholder="Documento">

           <div class="col-12 col-sm-6" id="inputfp_importe_pago">
                    <div class="form-group">
                        <label>Importe Pago *</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control control-num-moneda-fp input-fp importe_a_pagar" id="fp_importe_pago" name="fp_importe_pago">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary" id="btnAddFormaPago" title="Agregar Forma Pago" type="button"><i class="fa fa-plus fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                  </div>
            `;


    canje = `
            <!--  <div class="col-12 adjuntoHiden" id="inputimagen">
                    <label>Subir Archivo Adjunto</label>
                        <button type="button" id="btnAdjuntoModal" class="btn btn-primary btn-block">Adjunto</button>
                    </div>
                    <input type="hidden" name="nombreImagen" value="" id="imagen">
              </div>  -->


           <div class="col-12" id="inputfp_importe_pago">
                    <div class="form-group">
                        <label>Importe Pago *</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control control-num-moneda-fp input-fp importe_a_pagar" id="fp_importe_pago" name="fp_importe_pago">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary" id="btnAddFormaPago" title="Agregar Forma Pago" type="button"><i class="fa fa-plus fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                  </div>
            `;


    tarjeta = `
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Entidad</label>                         
                    <select class="form-control select2" name="" id="fp_id_origen_pago" style="width: 100%;">       
                     @foreach ($origen as $forma)
                         <option value="{{$forma->id}}">{{$forma->nombre}}</option>
                     @endforeach        
                    </select>
                </div> 
            </div>

            <div class="col-12  col-sm-6">
                    <div class="form-group">
                        <label>Cuenta de Fondo</label>                        
                        <select class="form-control select2" name="" disabled id="fp_id_cuenta" style="width: 100%;">           
                        </select>
                    </div> 
            </div>
            
            <div class="col-12 col-sm-6">
                    <div class="form-group">
                         <label>Número</label>
                       <input type="text" class="form-control clear_input_txt" id="nro_comprobante" value="0">
                   </div>
               </div>
               <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Fecha Emisión *</label>						 
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                        </div>
                            <input type="text"  class="form-control single-picker" id="fecha_emision" />
                    </div>
                </div>	
            </div>

           <div class="col-12">
                    <div class="form-group">
                        <label>Importe Pago *</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control control-num-moneda-fp input-fp importe_a_pagar" id="fp_importe_pago" name="fp_importe_pago">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary" id="btnAddFormaPago" title="Agregar Forma Pago" type="button"><i class="fa fa-plus fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                  </div> `;
        


    //VALORES OBLIGATORIOS PREDETERMINADO
    fp.requeridos = ['fp_id_moneda', 'fp_cotizacion', 'tipo_operacion', 'fp_importe_pago'];

    $('#tipo_operacion_div').html('');

    //EFECTIVO
    if (tipo_operacion == '1') {
        fp.requeridos.push('fp_documento_forma_pago');
        $('#tipo_operacion_div').html(efectivo);
    }
    //CHEQUE
    if (tipo_operacion == '4') {
        fp.requeridos.push('fp_id_origen_pago', 'fp_id_cuenta', 'fecha_emision', 'fecha_vencimiento', 'nro_comprobante');
        $('#tipo_operacion_div').html(cheque);
        numMaxCheque();
        agregarBeneficiario();
        getCuentas(tipo_operacion)
    }

    //TRANSFERENCIA
    if (tipo_operacion == '6') {
        $('#tipo_operacion_div').html(transferencia);
         getCuentas(tipo_operacion)
    }

    //CANJE
    if (tipo_operacion == '10') {
        $('#tipo_operacion_div').html(canje);
    }

     //TARJETA DEBITO Y CREDITO
    if (tipo_operacion == '11') {
        $('#tipo_operacion_div').html(tarjeta);
         getCuentas(tipo_operacion)
    }

    //CANJE 2
    if (tipo_operacion == '12') {
        $('#tipo_operacion_div').html(canje);
    }

    // if (operaciones.adjunto_obligatorio) {
    //     $('.adjuntoHiden').show();
    //     fp.requeridos.push('imagen');
    // }

    $('#fp_importe_pago').val(formatCurrency(fp.id_moneda,fp.total));
    levantar_eventos_fp();

} //


/*================================================================================================
                                    LOGICA CHEQUE
================================================================================================*/
function agregarBeneficiario() 
{
    $("#id_beneficiario").select2({
        ajax: {
                url: "{{route('beneficiarioCheque')}}",
                dataType: 'json',
                placeholder: "Seleccione un Beneficiario",
                delay: 0,
                data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                                    };
                },
                processResults: function (data, params){
                            var results = $.map(data, function (value, key) {
                            return {
                                    children: $.map(value, function (v) {
                                        return {
                                                    id: key,
                                                    text: v
                                                };
                                        })
                                    };
                            });
                                    return {
                                        results: results,
                                    };
                },
                cache: true
                },
                escapeMarkup: function (markup) {
                                return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
    });
 

}
function numMaxCheque() 
{

    $.ajax({
        type: "GET",
        url: "{{route('numMaxCheque')}}",
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            // $('#msj_pago').css('background-color', '#C93F3F');
            // $('#msj_pago').html('Ocurrio un error en la comunicación con el servidor.');
            // ok++;
        },
        success: function (rsp) {
            $('#nro_comprobante').val(rsp.num);

        } //success
    })
}



/*================================================================================================
                        VERIFICAR OP
================================================================================================*/
function estadoOP(opt) 
{
   if($('#portador_check').prop('checked')) {
        sbeneficiario='';
    }else{
        sbeneficiario= $('#beneficiario_texto').val();
    }

    let dataStrig = {
        id_op: operaciones.data.id,
        opt: opt,
        beneficiario:sbeneficiario,
        cf_entrega: $('#cf_entrega').val()
    };
    let msj;
    if (opt === 1)
        msj = "La OP fue Verificada!";
    if (opt === 2)
        msj = "La OP fue Autorizada!";
    if (opt === 3)
        msj = "La OP fue Rechazada!";



        return new Promise((resolve, reject) => { 

        $.ajax({
            type: "GET",
            url: "{{route('estadoOp')}}",
            dataType: 'json',
            cache:false,
            data: dataStrig,
            error: function () {
                reject('Ocurrió un error en la comunación con el servidor.');
            },
            success: function (rsp) {

                if (rsp.err == true) {
                    resolve(msj);
                } else {
                    reject(rsp.msj); //VERIFICAR SI ES ARRAY
                }
            }
          }); //ajax
        });

} //


/*================================================================================================
                         LOGICA DE VALIDAR Y PROCESAR OP
================================================================================================*/
function validarCamposFp() 
{



    var ok = 0;

    var req = fp.requeridos;

    for (var j = 0; j < req.length; j++) {

        var id = req[j];

        var name = $("#" + id).get(0).tagName;
        if (name == 'INPUT' || name == 'TEXTAREA') {
            var input = $("#" + id).val();



            if (input == '') {
                $("#" + id).css('border-color', 'red');
                ok++;
            } else {
                $("#" + id).css('border-color', '#d2d6de');
            } //else



        } //if

        if (name == 'SELECT') {

            var select = $('#' + id).val();
            if (select == '') {

                $("#input" + id).find('.select2-container--default .select2-selection--single').css('border-color', 'red');
                ok++;
            } else {
                $("#input" + id).find('.select2-container--default .select2-selection--single').css('border', '1px solid #aaa');
            } //else


        } //if   


        if (name == 'A') {

            var inputHidden = $("#imagen").val();

            if (inputHidden == '') {
                $('#' + id).addClass('borderErrorBtn');
                ok++;
            } else {
                $('#' + id).removeClass('borderErrorBtn');
            } //else


        } //if

    } //for


    if (ok > 0) {
        return false;
    } else {
        return true;
    }






} //function


function procesarOp() 
{

    return new Promise((resolve, reject) => { 
       
            if ($('#cf_entrega').val() != "") {
                if (validarOp()) {
                    $.ajax({
                        type: "GET",
                        url: "{{route('procesarOp')}}",
                        dataType: 'json',
                        cache: false,
                        data: {
                            id_op: operaciones.data.id,
                            concepto: $('#cf_concepto').val(),
                            entregado: $('#cf_entrega').val()
                        },
                        error: function () {
                            reject('Ocurrió un error en la comunicación con el servidor.');
                        },
                        success: function (rsp) {
                            console.log(rsp);
                            if (rsp.err == true) {
                                resolve('Los datos fueron procesados.');
                            } else {
                                reject('Ocurrió un error al intentar aplicar op. Motivo: '+rsp.e);
                            }
                        }
                    }); //ajax
                } else {
                    reject('La diferencia de forma pago debe ser 0.');
                }
            } else {
                reject('Ingrese el nombre del encargado del recibo');
            }
    });

}

function validarOp() 
{   //SE QUITA LA DIFERENCIA YA QUE EL MONTO DE PAGO NO SE PUEDE MODIFICAR
    // if (fp.diferencia >= 0 & fp.diferencia <= 0.99) {
        return true;
    // }

    // return false;
} //




/*  === === === === === === MONEDA FP === === === === === ===  */



function getCotizacion() 
{
    // console.lgo('SE DISPARO GETCOTIZACION');
    return new Promise((resolve, reject) => {
        let cotizacion;
        $.ajax({
            async: false,
            type: "GET",
            url: "{{route('getCotizacionFPO')}}",
            data: {
                id_moneda_costo: fp.id_moneda,
                id_op: operaciones.data.id
            },
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo obtener la cotización.', 'error', jqXHR, textStatus);
                reject(false);
            },
            success: function (rsp) {
                console.log(rsp);
                if (rsp.cotizacion) {
                    let cot = Number(parseFloat(rsp.cotizacion));
                    
                    if (cot > 0) {
                        $('#fp_cotizacion').val(rsp.cotizacion);
                        if(operaciones.set_indice_cotizacion){
                            $('#fp_indice_cotizacion').val(rsp.indice_cotizacion);
                        }
                        cotizacion = rsp.cotizacion;

                      

                        resolve({
                            resp: true
                        });


                    } else {
                        msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.', 'info');
                        reject(false);
                    }
                } else {
                    msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.', 'info');
                    reject(false);
                }

            } //success
        });

    }); //promise


} //



function setCotizar() 
{
    // console.lgo('SE DISPARO SETCOTIZAR');
        // console.trace('Set Cotizar');
    return new Promise((resolve, reject) => {
        let data = {
            id_moneda: fp.id_moneda,
            id_op: operaciones.data.id,
            cotizacion: $('#fp_cotizacion').val(),
            // indice_cotizacion: $('#fp_indice_cotizacion').val()
        }
        $.ajax({
            type: "GET",
            url: "{{route('cotizarMontoFP')}}",
            data: data,
            dataType: 'json',
            cache:false,
            error: function (jqXHR, textStatus, errorThrown) {

                msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo cotizar los montos.', 'error', jqXHR, textStatus);
                reject(false);
            },
            success: function (rsp) {

                // if (rsp.info.length > 0) {
                   let total = Number(rsp.total);

                    if(fp.id_moneda == 111){
                        fp.set_total(Number(total.toFixed(0)));
                        fp.setDiferencia(Number(total.toFixed(0)));
                        $('#fp_total_pago').val(fp.total);
                        $('.importe_a_pagar').val( fp.total);
                        $('#fp_diferencia_pago').val(fp.diferencia);

                    } else {
                        fp.set_total(formatCurrency(fp.id_moneda, total));
                        // fp.diferencia = Number(total);
                        fp.setDiferencia(formatCurrency(fp.id_moneda, total));
                        $('#fp_total_pago').val(fp.total);
                        $('.importe_a_pagar').val( fp.total);
                        $('#fp_diferencia_pago').val(fp.diferencia);
                    }
                    formatMoneyFp();

                    resolve({
                        resp: true
                    });
                // } else {
                //     msjToast('Ocurrió un error al intentar cotizar los valores.', 'error');
                //     fp.set_total(0);
                //     reject(false);

                // }

            } //success
        }); //ajax

    }); //promise
} //

function getCuentas(id)
{
    if(jQuery.isEmptyObject(id)){
        tipoOperacion = 0;
    }else{
        tipoOperacion = id;
    }
    var newOption;
    dataString = 'id_banco='+$('#fp_id_origen_pago').val()+' &id_tipo_operacion='+tipoOperacion;
    $.ajax({
        async: false,
        type: "GET",
        url: "{{route('getListCuentas')}}",
        data: dataString,
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {

            $('#fp_id_cuenta').prop('disabled', true);
        },
        success: function (rsp) {
            if (rsp.cuentas.length > 0) {

                $('#fp_id_cuenta').empty();
                // newOption = new Option('Seleccione cuenta', '', false, false);
                $('#fp_id_cuenta').append(newOption);

                $.each(rsp.cuentas, function (key, item) {
                    $('#fp_id_cuenta').append($("<option value='"+item.id+"' moneda ='"+item.id_moneda+"'>"+item.denominacion + " " + item.currency_code + " (" + item.numero_cuenta + ")</option>"/*, {
                                                    text: item.id,
                                                    text: item.denominacion + ' ' + item.currency_code + ' (' + item.numero_cuenta + ')'
                                                    }*/));
                   /* newOption = new Option(item.denominacion + ' ' + item.currency_code + ' (' + item.numero_cuenta + ')', item.id, false, false);
                    $('#fp_id_cuenta').append(newOption);*/
                });
                $('#fp_id_cuenta').prop('disabled', false);
            } else {
                $('#fp_id_cuenta').empty();
                newOption = new Option('Seleccione cuenta', '', false, false);
                $('#fp_id_cuenta').append(newOption);
                $('#fp_id_cuenta').prop('disabled', true);
            }
        } //success
    }); //ajax


} //



/* ================================================================================
					                MODAL  
	================================================================================*/
	function modalAplicarOp() 
    {

	    swal({
	        title: "Aplicar OP",
	        text: "¿Está seguro que desea aplicar la OP?",
	        icon: "warning",
	        showCancelButton: true,
	        buttons: {
	            cancel: {
	                text: "No, Cancelar",
	                value: null,
	                visible: true,
	                className: "btn-warning",
	                closeModal: false,
	            },
	            confirm: {
	                text: "Sí, Aplicar",
	                value: true,
	                visible: true,
	                className: "",
	                closeModal: false
	            }
	        }
	    }).then(isConfirm => {

	        if (isConfirm) {
	            $.when(procesarOp()).then((a) => {
	                swal("Éxito", a, "success");
	                setTimeout(redirectControlOp, 1000);
	            }, (b) => {
	                swal("Cancelado", b, "error");
	            });

	        } else {
	            swal("Cancelado", "La operación fue cancelada", "error");
	        }
	    });
	}

    function modalVerificar() 
    {

	    swal({
	        title: "Verificar OP",
	        text: "¿Está seguro que desea verificar la OP?",
	        icon: "warning",
	        showCancelButton: true,
	        buttons: {
	            cancel: {
	                text: "No, Cancelar",
	                value: null,
	                visible: true,
	                className: "btn-warning",
	                closeModal: false,
	            },
	            confirm: {
	                text: "Sí, Verificar",
	                value: true,
	                visible: true,
	                className: "",
	                closeModal: false
	            }
	        }
	    }).then(isConfirm => {

	        if (isConfirm) {
	            $.when(estadoOP(1)).then((a) => {
	                swal("Éxito", a, "success");
	                setTimeout(redirectControlOp, 1000);
	            }, (b) => {
	                swal("Cancelado", b, "error");
	            });

	        } else {
	            swal("Cancelado", "La operación fue cancelada", "error");
	        }
	    });
	}


/* === === === === === === === === === === === === === === === === === ==
        FUNCIONES AUXILIARES
=== === === === === === === === === === === === === === === === === == */


const aux = {
    msj: []
}
//Para almacenar mensajes o mostrar mensajes en el momento segun error del ajax

function msjToast(msj, opt = '', jqXHR = false, textStatus = '') 
{


    if (msj != '') {
        aux.msj.push('<b>' + msj + '</b>');
    }

    if (opt === 'info') {
        $.toast().reset('all');

        $.toast({
            heading: '<b>Atención</b>',
            position: 'top-right',
            text: aux.msj,
            width: '400px',
            hideAfter: false,
            icon: 'info'
        });
        aux.msj = [];

    } else if (opt === 'error') {
        $.toast().reset('all');



        if (jqXHR === false && textStatus === '') {
            msj = aux.msj;
        } else {

            errCode = jqXHR.status;
            errMsj = jqXHR.responseText;

            msj = '<b>';
            if (errCode === 0)
                msj += 'Error de conectividad a internet, verifica tu conexión.';
            else if (errCode === 404)
                msj += 'Error al realizar la solicitud.';
            else if (errCode === 500) {
                if (aux.msj.length > 0) {
                    msj += aux.msj;
                } else {
                    msj += 'Ocurrió un error en la comunicación con el servidor.'
                }

            } else if (errCode === 406) {
                msj += errMsj;
            } else if (errCode === 422)
                msj += 'Complete todos los datos requeridos'
            else if (textStatus === 'timeout')
                msj += 'El servidor tarda mucho en responder.';
            else if (textStatus === 'abort')
                msj += 'El servidor tarda mucho en responder.';
            else if (textStatus === 'parsererror') {
                msj += 'Error al realizar la solicitud.';
            } else
                msj += 'Error desconocido, pongase en contacto con el área técnica.';
            msj += '</b>';
        }


        $.toast({
            heading: '<b>Error</b>',
            text: aux.msj,
            position: 'top-right',
            hideAfter: false,
            width: '400px',
            icon: 'error'
        });

        aux.msj = [];
    } else if (opt === 'success') {
        $.toast().reset('all');

        $.toast({
            heading: '<b>Éxito</b>',
            text: aux.msj,
            position: 'top-right',
            hideAfter: false,
            width: '400px',
            icon: 'success'
        });
        aux.msj = [];

    }
} //

function formatNumberControl() 
{
    /*FORMATEO DE MONEDAS EN CUALQUIER CAMPO NUMERO*/
    $('.format-number-control').inputmask("numeric", {
        radixPoint: ",",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        // prefix: '$', //No Space, this will truncate the first character
        rightAlign: false
    });

    // $('.format-number-control').keyup(function (){
    //  this.value = (this.value + '').replace(/[^0-9]/g, '');
    // });
}



function initCalendar() 
{

    $('.single-picker').datepicker({
        format: "dd/mm/yyyy",
        language: "es"
    });
}



function clean_num(n, bd = false) 
{

    if (n && bd == false) {
        n = n.replace(/[,.]/g, function (m) {
            if (m === '.') {
                return '';
            }
            if (m === ',') {
                return '.';
            }
        });
        return Number(n);
    }
    if (bd) {
        return Number(n);
    }
    return 0;

} //


function valid(n) 
{
    try {
        if (n & !n.isEmpty()) {
            return n
        }
        return '';
    } catch (error) {
        return '';
    }

}


function blockUI() 
{

    $.blockUI({
        centerY: 0,
        message: "<h2>Procesando...</h2>",
        css: {
            color: '#000'
        }
    });

}

function BlockFp() 
{
    fp.activo = false;
    // $('#btnAddFormaPago').prop('disabled', true);
    // $('#tipo_operacion').prop('disabled', true);
}

function UnblockFp() 
{
    fp.activo = true;
    // $('#btnAddFormaPago').prop('disabled', false);
    // $('#tipo_operacion').prop('disabled', false);
}


/*FORMATEO DE MONEDA EN FUNCION AL MONEDA SELECCIONADA EN FP*/

function formatMoneyFp() 
{

    var moneda = $('#fp_id_moneda').val();
    $('.control-num-moneda-fp').unbind();
    $('.control-num-moneda-fp').inputmask('remove');
    if (moneda == '111') {
        $('.control-num-moneda-fp').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 0,
            autoGroup: true,
            rightAlign: false
        });

    } else {

        $('.control-num-moneda-fp').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            rightAlign: false
        });



    } //else



} //		

function fechasMomentJs() 
{
    moment.updateLocale('es', {
        months: [
            "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
            "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
        ]
    });
}



/* ================================================================================
								FORMATEO DE VALORES
   ================================================================================*/

   function round(num, decimales = 2) {
            var signo = (num >= 0 ? 1 : -1);
            num = num * signo;
            if (decimales === 0) //con 0 decimales
                return signo * Math.round(num);
            // round(x * 10 ^ decimales)
            num = num.toString().split('e');
            num = Math.round(+(num[0] + 'e' + (num[1] ? (+num[1] + decimales) : decimales)));
            // x * 10 ^ (-decimales)
            num = num.toString().split('e');
        return signo * (num[0] + 'e' + (num[1] ? (+num[1] - decimales) : -decimales));
    }



	function formatCurrency(id_moneda, value) 
    {

		let importe = 0;


		// console.log('formatCurrency', id_moneda, value);

		//PYG
		if (id_moneda == 111) {
			importe = GUARANI(value, {
				formatWithSymbol: false
			}).format(true);
		} else {
            importe = DOLAR(value, {
				formatWithSymbol: false
			}).format(true);
		
        }


		return importe;
	}


// var forma_pago = $('#id_tipo_operacion:selected').text()
    $('#tipo_operacion').change(function(){
     
       cargarConcepto() 

    })

    if($('#listadoPagos >tbody >tr').length != 0){
        let msj = 'OP ' + operaciones.data.nro_op;

        if ($('#tipo_operacion :selected').text() != '')
            msj += ' ' + $('#tipo_operacion :selected').text().toUpperCase().substring(0, 4);

        msj += ' ' + operaciones.data.proveedor_n + ' {{$facturasRecortadas}}';

        // console.log(msj);

        $('#cf_concepto').val(msj);

    }

</script>
@endsection