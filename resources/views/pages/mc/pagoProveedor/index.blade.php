
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
	

	.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

@-moz-document url-prefix() {
  fieldset { display: table-cell; }
}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	.select2-container *:focus {
        outline: none;
    }

	 input.form-control:focus ,.select2-container--focus , button:focus{
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

	.input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
	




</style>

	@parent
	  <link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/vendors/css/tables/datatable/select.dataTables.min.css')}}">
@endsection
@section('content')



<section id="base-style">

	<div class="card" style="border-radius: 14px;">
		<div class="card-header" style="border-radius: 14px;">
			<h4 class="card-title">Pago Proveedor</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				 	<ul class="nav nav-tabs nav-underline" role="tablist">
     					<li class="nav-item">
			                <a class="nav-link active" id="baseIcon-tab20" data-toggle="tab" aria-controls="tabIcon20" href="#cierreCaja" role="tab" aria-selected="true"><b><i class="fa fa-play"></i>Proveedor</b></a>
			            </li>			            
			            <li class="nav-item">
			                <a class="nav-link" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#cabecera" role="tab" aria-selected="false">
			                	<b><i class="fa fa-fw fa-gear"></i> Confirmar Pago</b></a>
			            </li>
			        </ul>    
					<div class="tab-content px-1 pt-1">
			            <div class="tab-pane active" id="cierreCaja" role="tabpanel" aria-labelledby="baseIcon-tab20">
			            	<form id="consultaFactura"  autocomplete="off">
							<div class="row mb-1">
								<div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Proveedor</label>
										<select class="form-control select2" name="idProveedor" id="proveedor"
											tabindex="1" style="width: 100%;">
											<option value="">Todos</option>
											@foreach ($getProveedor as $pro)
												$nombre = '';
												@if($pro->activo == true)
													@php
													$activo = 'ACTIVO';
													@endphp
												@else
													@php
													$activo = 'INACTIVO';
													@endphp
												@endif
												@if(isset($pro->apellido))
													@if($pro->apellido != "")
															@php 
																$nombre = $pro->nombre." ".$pro->apellido;
															@endphp
													@endif
												@else
															@php
																$nombre = $pro->nombre;
															@endphp	

												@endif
												@if(isset($pro->denominacion_comercial))
													@if($pro->denominacion_comercial != "")
														@php 
															$nombre = $pro->nombre." ".$pro->apellido . " - ". $pro->denominacion_comercial;
														@endphp
														
													@endif
												@endif
												@php
													$ruc = $pro->documento_identidad;
													if($pro->dv){
														$ruc = $ruc."-".$pro->dv;
													}
												@endphp
												<option value="{{$pro->id}}">{{$ruc}} - {{$nombre}} <b>({{$activo}})</b></option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Moneda</label>
										<select class="form-control select2" name="idMoneda" id="idMoneda" tabindex="2"
											style="width: 100%;">
											<option value="">Todos</option>
											@foreach ($getDivisa as $div)
											<option value="{{$div->currency_id}}">{{$div->currency_code}}</option>
											@endforeach
										</select>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Proforma</label>
										<input type="number" class="form-control" id="idProforma" maxlength="15" tabindex="3"
											name="idProforma" value="">
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Nro. Factura</label>
										<input type="text" class="form-control" id="nroFactura" maxlength="25" tabindex="4" maxlength="20"
											name="nroFactura" value="">
									</div>
								</div>

							</div>
							<div class="row">

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Codigo</label>
										<input type="text" class="form-control" maxlength="40"  id="codigoConfirmacion" tabindex="5"
											name="codigoConfirmacion" value="">
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Checkin Desde - Hasta</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" maxlength="10"  class="form-control pull-right fecha calendar"
												tabindex="6" name="checkin_desde_hasta:p_date" id="checkin_desde_hasta"
												value="">
										</div>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Fecha Proveedor Desde - Hasta</label>
										<div class="input-group">
											<div class="input-group-prepend" style="">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" maxlength="10" class="form-control pull-right fecha calendar"
												tabindex="7" name="proveedor_desde_hasta:p_date"
												id="proveedor_desde_hasta" value="">
										</div>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Pagado </label>
										<select class="form-control select2" name="tieneFechaProveedor" tabindex="8"
											id="tieneFechaProveedor" style="width: 100%;">
											<option value="">Todos</option>
											<option value="SI">SI</option>
											<option selected="selected" value="NO">NO</option>


										</select>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Pendiente </label>
										<select class="form-control select2" name="pendiente" tabindex="9"
											id="filtro_pendiente" style="width: 100%;">
											<option value="">Todos</option>
											<option value="true">SI</option>
											<option selected="selected" value="false">NO</option>


										</select>
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Fecha Gasto Desde - Hasta</label>
										<div class="input-group">
											<div class="input-group-prepend" style="">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" maxlength="10" class="form-control pull-right fecha calendar"
												tabindex="7" name="gasto_desde_hasta:p_date"
												id="gasto_desde_hasta" value="">
										</div>
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3 bsp" style="display:none">
									<div class="form-group">
									<label>Periodo de Pago BSP</label>                       
									<select class="form-control input-sm select2" required  name="fecha_pago" id="fecha_pago" style="padding-left: 0px;width: 100%;">
										<option value="">Seleccione Fecha</option>
										@foreach($calendariobsp as $calendario)
											<option value="{{$calendario->fecha_de_pago}}">{{$calendario->periodo_desde_format}} - {{$calendario->periodo_hasta_format}}</option>
										@endforeach 
										
									</select>
									</div>
								</div>
								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Emisión Desde - Hasta</label>             
										<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
											<input type="text" class="form-control pull-right fecha" name="periodo_desde_hasta" id="periodo_desde_hasta" value="">
										</div>
									</div>  
								</div>

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Vencimiento</label>             
										<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
											<input type="text" class="form-control pull-right fecha" name="vencimiento" id="vencimiento" value="">
										</div>
									</div>  
								</div>

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Total Saldo USD</label>
										<input type="text" class="form-control numeric" maxlength="40"  id="totalSaldoUsd" tabindex="5" name="totalSaldoUsd" value="0">
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Total Saldo PYG</label>
										<input type="text" class="form-control numeric" maxlength="40"  id="totalSaldoPyg" tabindex="5" name="totalSaldoPyg" value="0">
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Extranjero </label>
										<select class="form-control select2" name="extranjero" tabindex="9"
											id="extranjero" style="width: 100%;">
											<option value="">Todos</option>
											<option value="1">SI</option>
											<option selected="selected" value="1455">NO</option>
										</select>
									</div>
								</div>

								 <div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Reprice </label>
										<select class="form-control select2" name="reprice" tabindex="9"
											id="reprice" style="width: 100%;">
											<option selected="selected" value="">Todos</option>
											<option value="true">SI</option>
											<option  value="false">NO</option>
										</select>
									</div>
								</div> 

							</div>

							<input type="hidden" id="items_selecionados" value="">
							<input type="hidden" id="total_pagar_proveedor" value="">
							<input type="hidden" id="saldo_total" value=""> 
							<input type="hidden" id="saldo_default" value=""> 
							<input type="hidden" id="tipoBase" value="">
							<div class="col-12">
								<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
								<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
								<button type="button" id="btnProcesar" tabindex="11"
									class="btn btn-success btn-lg pull-right mr-1"><b>Confirmar</b></button>
								<button onclick="loadDatatable()" tabindex="10" class="pull-right text-center btn btn-info btn-lg mr-1" type="button"><b>Buscar</b></button>
							</div>
						</form>


						<div class="table-responsive">
							<table id="listado" class="table table-hover table-condensed nowrap" style="width: 100%;">
								<thead>
									<tr style="text-align: center">
										<th>
											<input type="checkbox" class="selectAll">
										</th>
										<th>Provedor</th>
										<th>Fecha Proveedor</th>
										<th>Fecha Gasto</th>
										<th>Factura/Documento</th>
										<th>Total</th>
										<th>Saldo Documento</th>
										<th>Checkin</th>
										<th>Tipo</th>
										<th>Aerolinea</th>
										<th>Pagado</th>
										<th>Pasajero</th>
										<th>Moneda</th>
										<th></th>
										<th>Saldo</th>
										<th></th>
										<th>Pagar</th>
										<th>Código</th>
										<th>Producto</th>
										<th>Proforma</th>
										<th>Facturación</th>
										<th>Vendedor</th>
										<th>Origen</th>
										<th>x</th>
									</tr>
								</thead>
								<tbody style="text-align: center">
								</tbody>
								</tfoot>
									<tr>
										<th colspan="10" style="text-align: right;">TOTAL</th>
										<th>
											<input type="text" class="numeric" style="text-align: center;" maxlength="40"  id="totalFinal" tabindex="5" disabled value="0">
										</th>
										<th colspan="10">
										</th>
									</tr>	
								</tfoot>

							</table>
						</div>

			            </div>
			            <div class="tab-pane" id="cabecera" role="tabpanel" aria-labelledby="baseIcon-tab21">
			            			
					<div class="row">
						<div class="col-12 col-sm-3 ">
							<div class="form-group">
								<label>Sucursal</label>
								<select class="form-control select2" name="cf_sucursal" id="cf_sucursal" style="width: 100%;">
									@foreach ($sucursalEmpresa as $suc)
									<option value="{{$suc->id}}">{{$suc->denominacion}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-3 ">
							<div class="form-group">
								<label>Centro de Costo</label>
								<select class="form-control select2" name="cf_centro_costo" id="cf_centro_costo" style="width: 100%;">
									@foreach ($centro as $suc)
									<option value="{{$suc->id}}">{{$suc->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-3">
							<div class="form-group">
								<label>Operación Moneda</label>
								<input type="text" class="form-control" id="cf_currency_code" disabled name="" value="">
							</div>
						</div>

						<div class="col-12 col-sm-3">
							<div class="form-group">
								<label>Cotización </label>
								<input type="text" class="form-control control_space formatInput" readonly id="cf_cotizacion" name=""
									value="0">
							</div>
						</div>
					</div>

					<div class="col-12">
						<h4>Totales</h4>
						<hr style="margin-top: 1px;">

					</div>


					<!--======================================================
											MONTO COTIZADO
						====================================================== --}}-->

					<form class="row" id="formTotales" autocomplete="off">

						<div class="col-4">
							<div class="row no-gutters">

								<div class="col-12">
									<small><b>Montos cotizados <span class="bg-blue badge">USD</span></b> </small><i
										class="fa fa-fw fa-arrow-down"></i>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Total Factura</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-plus"></i></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly
												id="cotizado_total_factura" value="0">
										</div>

									</div>
								</div>


								<div class="col-12">
									<div class="form-group">
										<label>Anticipos Realizados</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cotizado_anticipo" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Nota Credito</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cotizado_nc" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Ret IVA 10%</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cotizado_retencion" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Neto a Pagar</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text font-weight-boldt">=</span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly value="0"
												id="cotizado_pago_neto">
										</div>
									</div>
								</div>

							</div>
						</div>



						<!--======================================================
													MONTO COSTO PROVEEDOR
							====================================================== -->

						<div class="col-4">
							<div class="row no-gutters">

								<div class="col-12">
									<small><b>Montos Compra <span class="bg-blue badge  cf_currency_code"></span></b>
									</small><i class="fa fa-fw fa-arrow-down"></i>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Total Factura</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-plus"></i></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly
												id="cf_total_pago" value="0">
										</div>

									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Anticipos Realizados</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cf_anticipo" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Nota Credito</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cf_nota_credito" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Ret IVA 10%</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cf_retencion" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Neto a Pagar</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><b>=</b></b></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly value="0"
												id="cf_neto_pago">
										</div>
									</div>
								</div>

							</div>
						</div>


					</form>


					<div class="row">
						<div class="col-12">
							<hr>
						</div>

						<div class="col-12">
							<div class="form-group">
								<label>Beneficiario : </label>
									<input type="text" readonly class="form-control clear_input_txt" id="cf_beneficiario">
							</div>
						</div>

						<div class="col-12 mt-2">
							<button type="button" class="btn btn-success btn-lg pull-right" onclick="sendData()"
								id="btnProcesarOP"><b>Crear OP</b></button>
							<button type="button" class="btn btn-danger btn-lg pull-right" style="margin-right: 10px;"
								onclick="cancelarOp()"><b>Cancelar Operación</b></button>
						</div>
					</div>
			        	</div> 			
			        </div> 

				</div>



			</div>
		</div>
	</div>
</section>




{{-- ========================================
   			MODAL NUMERO DE OP
   	========================================  --}}		

	   <div class="modal fade" id="modalNumOp" aria-labelledby="myModalLabel">
	   	<div class="modal-dialog">
	   		<div class="modal-content">
	   			<div class="modal-header">
	   				<button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
	   					<span aria-hidden="true">&times;</span>
	   				</button>
	   				<h4 class="modal-title"  style="font-weight: 800;">
	   					<i class="fa fa-fw fa-send-o"></i>
	   					NUMERO DE OP
	   				</h4>
	   			</div>
	   			<div class="modal-body">
	   				<div class="content-fluid">
	   					<div class="row">
							   <div class="col-12">
										<h1 style="text-align:center">SE GENERO LA OP NUMERO : <span id="num_op">01</span></h1>
							   </div>
							   <div class="col-12 text-center mt-1">
									<button type="button" class="btn btn-success btn-lg"
									id="confirmarOpModal" data-dismiss="modal"
									style="margin:auto"><b>Aceptar</b></button>
							   </div>

	   					</div>
	   				</div>
	   			</div>
	   		</div>
	   	</div>
	   </div>


    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.min.js"></script>

	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script> --}}
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script src="{{asset('gestion/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')}}"></script>
	<script src="//cdn.datatables.net/plug-ins/1.10.12/sorting/datetime-moment.js"></script>
	
<script type="text/javascript">


initConfig();
// debugDesarrollo();

$(document).ready(function(){
$('.select2').select2();
calendar();
// loadDatatable();	
controlNumFormat();
enterSearchEventDatatable();
});
	

	function debugDesarrollo(){
		$('.box-header .subtitle').html('Pago Proveedor Modo DEBUG');
		$('#tabConfirm').prop('disabled',false);
 		$('#btnProcesarOP').prop('disabled',false);
 		$('#tabConfirm').tab('show');
	}


	{{--==============================================
			FILTROS BUSQUEDA
	============================================== --}}
	function calendar(){
		{{--CALENDARIO OPCIONES EN ESPAÑOL --}}
		let locale = {
					format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};


				$('.calendar').daterangepicker({timePicker24Hour: true,
												timePickerIncrement: 30,
												autoUpdateInput: false,
												locale: locale
															});
				$('#proveedor_desde_hasta').val('');
				$('#proveedor_desde_hasta')
				.on('cancel.daterangepicker', function(ev, picker) {
							$(this).val('');})
				.on('apply.daterangepicker', function(ev, picker) {
							$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
				});		

				$('#checkin_desde_hasta').val('');
				$('#checkin_desde_hasta')
				.on('cancel.daterangepicker', function(ev, picker) {
							$(this).val('');})
				.on('apply.daterangepicker', function(ev, picker) {
							$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
				});	

				
				$('.single-picker').datepicker({ 
							format: "dd/mm/yyyy", 
							language: "es"
							});

				$('#gasto_desde_hasta').val('');
				$('#gasto_desde_hasta')
				.on('cancel.daterangepicker', function(ev, picker) {
							$(this).val('');})
				.on('apply.daterangepicker', function(ev, picker) {
							$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
				});	

	 }

	 $(function() {
			let locale = {
								format: 'DD/MM/YYYY',
								cancelLabel: 'Limpiar',
								applyLabel: 'Aplicar',					
									fromLabel: 'Desde',
									toLabel: 'Hasta',
									customRangeLabel: 'Seleccionar rango',
									daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
									monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
															'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
															'Diciembre']
								};


			$('input[name="periodo_desde_hasta"]').daterangepicker({
				autoUpdateInput: false,
				locale: locale
			});

			$('input[name="periodo_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
			});

			$('input[name="periodo_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
			});
			
			let locales = {
								format: 'DD/MM/YYYY',
								cancelLabel: 'Limpiar',
								applyLabel: 'Aplicar',					
									fromLabel: 'Desde',
									toLabel: 'Hasta',
									customRangeLabel: 'Seleccionar rango',
									daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
									monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
															'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
															'Diciembre']
								};


			$('input[name="vencimiento"]').daterangepicker({
				autoUpdateInput: false,
				locale: locales
			});

			$('input[name="vencimiento"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
			});

			$('input[name="vencimiento"]').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
			});

			$('input[name="vencimiento"]').daterangepicker({
				autoUpdateInput: false,
				locale: locale
			});

			$('input[name="vencimiento"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
			});

			$('input[name="vencimiento"]').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
			});


		});



	/*{{--============================================================================================
							DATATABLE Y LOGICA SELECCION PROVEEDOR
	============================================================================================ --}}*/

	function limpiar()
	{
		$('#proveedor').val('').trigger('change.select2');
		$('#idMoneda').val('').trigger('change.select2');
		$('#idProforma').val('');
		$('#nroFactura').val('');
		$('#codigoConfirmacion').val('');
		$('#checkin_desde_hasta').val('');
		$('#proveedor_desde_hasta').val('');
		$('#tieneFechaProveedor').val('NO').trigger('change.select2');
		$('#filtro_pendiente').val('false').trigger('change.select2');
	}

		 var table;	
		function loadDatatable(){
			$("#totalFinal").val(0);
			$("#totalSaldo").val(0);
			$("#cotizado_total_factura").val(0);
			$("#cotizado_anticipo").val(0);
			$("#cotizado_nc").val(0);
			$("#cotizado_retencion").val(0);
			$("#cotizado_pago_neto").val(0);
			$("#cf_total_pago").val(0);
			$("#cf_anticipo").val(0);
			$("#cf_nota_credito").val(0);
			$("#cf_neto_pago").val(0);
			let form = $('#consultaFactura').serializeJSON({
				customTypes: customTypesSerializeJSON
			});
			let input;
			$.fn.dataTable.moment( 'M/D/YYYY' );	
			if($("#proveedor").val() == 44407 || $("#proveedor").val() == 71300){		
				//Anular eventos de tabla al momento de destruir la instancia
				if(table){
						table.off('select deselect user-select draw');
				}

				table = $("#listado").DataTable({
					destroy: true,
					keys: true,
					ajax: {
						url: "{{route('getListProveedor')}}",
						data: form,
						error: function (jqXHR, textStatus, errorThrown) {
							msjToast('', 2, jqXHR.status, textStatus);
							$.unblockUI();
						}

					},
					"order": [[ 2, "asc" ]],
					"columns": [
						{
							data: function (x) {
								input = '';
								
								 bgClass = x.reprice_pago_pro ? 'bg-danger' : '';
								if (x.pagado_proveedor === true || Number(x.saldo) <= 0 || x.pendiente === true || x.reprice_pago_pro === true) {
									input = '<input type="hidden" class="activeCheck" value="true" /> ';
								} else {
									input = '<input type="hidden" class="activeCheck" value="false" /> ';
								
									 origenValue = x.origen;

									origenValue += ' REPRICE ' + origenValue;
									console.log(origenValue);
								input += `<input type="hidden" class="id_documento" value="${x.id}"> 
										<input type="hidden" class="id_tipo_documento" value="${x.id_tipo_documento}">
										<input type="hidden" class="id_proveedor" value="${x.id_proveedor}">
										<input type="hidden" class="currency_id" value="${x.id_moneda}">
										<input type="hidden" class="totalIva" value="${x.totaliva}"> 
										<input type="hidden" class="gravada10" value="${x.costo_gravada}">
										<input type="hidden" class="retencion_pago_parcial" value="${x.retencion_pago_parcial}">
										<input type="hidden" class="genero_retencion" value="${x.genero_retencion}">
										<input type="hidden" class="origen" value="${origenValue}">`;
								return `<div class="${bgClass}">${input}</div>`;
							}
						}

						},

						{
							data: function (x) {
								return `<span class="proveedor_nombre">${x.proveedor_n}</span>`;
							}
						},
						{
							data: function (x) {
								if(x.fecha_proveedor_format !== null){
									return `<b>${x.fecha_proveedor_format}</b>`;
								}else{
									return "";
								}
							}
						},
						{
							data: function (x) {
								if(x.fecha_de_gasto !== null){
									fecha = x.fecha_de_gasto.split('-');
									fecha_gasto = fecha[2]+"/"+fecha[1]+"/"+fecha[0];
								}else{
									fecha_gasto = x.fecha_de_gasto;
								}
								return fecha_gasto;
							}
						},
						{
							data: function (x) {
								if(x.id_tipo_documento ==20){
									return `<a href="{{route('anticipoDetalle',['id'=>''])}}/${x.id}"  class="bgRed">
											<i class="fa fa-fw fa-search"></i>${x.id}</a>
											<div style="display:none;">${x.id}</div>`;
								}else{
									return `<a href="{{route('verLibroCompra',['id'=>''])}}/${x.id}"  class="bgRed">
											<i class="fa fa-fw fa-search"></i>${x.nro_factura}</a>
											<div style="display:none;">${x.id}</div>`;								
								}							
							}
						},
						{
							data: function (x) {
								var input = `<input type="text" disabled style="text-align:center;" data-mask="false" class="input-style numeric" value="${x.importe}">`;
								return input;
							}
						},

						{
							"data": function (x) {
								let saldo = x.saldo_documento;
								if (saldo === null || saldo === undefined) {
									saldo = 0;
									
								}
								if(saldo  <= 0 ){
									color= 'green';
								}else{
									color= 'red';
								}
								var input = `<input type="text" disabled style="text-align:center; font-weight: 1000; color:${color}" data-mask="false" class="input-style numeric" value="${saldo}">`;
								return input;
							}
						},
						{data: "fecha_in_format", "title": "Check In", "render": function(data, type) {
							//console.log(moment(data).format('L'));
								return type === 'sort' ? data : data;
							}
						},
						{
							data: function (x) {
								return `<b>${x.tipo_doc_n}</b>`;
							}
						},
						{
							data: function (x) {
								return `<b>${x.aerolinea}</b>`;
							}
						},

						{
							data: function (x) {
								return `<b>${x.pagado_proveedor_txt}</b>`;
							}
						},
						{
							data: 'pasajero_n'
						},
						{
							data: function (x) {
								return `<span class="currency_code">${x.currency_code}</span> 
											`;
							}
						},
						{
							data: function (x) {
								signo = '';
								if(x.id_tipo_documento == '2'|| x.id_tipo_documento == '32'||x.id_tipo_documento == '20' ){		
									signo = '<b>-</b>';
								}
								return signo;
							}
						},
						{
							"data": function (x) {
								let saldo = x.saldo;
								if (saldo === null || saldo === undefined) {
									saldo = 0;
								}
								var input = `
											<input type="hidden" value="${saldo}" class="saldo_default ${x.currency_code}">
											<input type="text" value="${saldo}" data-mask="false" disabled  style="text-align:center;" class="saldo numeric">`;
								return input;
							}
						},
						{
							data: function (x) {
								signo = '';
								if(x.id_tipo_documento == '2'|| x.id_tipo_documento == '32'||x.id_tipo_documento == '20' ){		
									signo = '<b>-</b>';
								}
								return signo;
							}
						},
						{
							"data": function (x) {
								var input = `<input type="text" disabled data-mask="false" tipo="${x.id_tipo_documento}" style="text-align:center;" class="pago_saldo numeric" value="0">`;
								return input;
							}
						},

						{
							data: 'cod_confirmacion'
						},
						{
							data: 'denominacion_producto'
						},
						{
							data: function (x) {
								if (Boolean(x.id_proforma)) {
									return `<a href="{{route('detallesProforma',['id'=>''])}}/${x.id_proforma}"class="bgRed">
										<i class="fa fa-fw fa-search"></i>${x.id_proforma}</a>
										<div style="display:none;">${x.id_proforma}</div>`;
								} else {
									return '';
								}

							}
						},
						{
							data: 'fecha_facturacion_format'
						},
						{
							data: 'vendedor_n'
						},
						{
							data: function (x) {
        let origenValue = x.reprice_pago_pro ? 'REPRICE ' + x.origen : x.origen;
        return origenValue;
    }
						},
						{
							data: function (x) {
								if(x.id_tipo_documento ==20){
									return 1;
								}
								if(x.id_tipo_documento ==2){
									return 2;
								}
								if(x.id_tipo_documento ==32){
									return 2;
								}
								if(x.id_tipo_documento ==22){
									return 3;
								}
								if(x.id_tipo_documento ==1){
									return 3;
								}
								if(x.id_tipo_documento ==5){
									return 3;
								}
								if(x.id_tipo_documento ==33){
									return 3;
								}

							}
						}


					],

					"select": {
						style: 'multi',
						selector: 'td:first-child'
					},
					"columnDefs": [{
							orderable: false,
							className: 'select-checkbox',
							targets: 0
						},
						{
							targets: 0,
							className: 'style_check'
						},
						{
							targets: 14,
							className: 'col_input_saldo'
						},
						{
							targets: 16,
							className: 'col_input_pago_proveedor'
						},
						{
							targets: 23,
							visible: false,
							searchable: false
						},

					],
					"createdRow": function (row, data, iDataIndex) {
						if (data.pendiente == true) {
							$(row).css('background-color', '#EFF3BA');
						}
						if (data.reprice_pago_pro === true) {
							//$(row).addClass('bg-danger');
							$(row).css('background-color', 'orange');
						}

						$(row).attr('id', iDataIndex + '_fila');
						//ASIGNAR ID Y FUNCION
						$(row).find('td.col_input_pago_proveedor input').attr('id', iDataIndex + '_input');
						$(row).find('td.col_input_pago_proveedor').attr('onkeyup', 'validateKeyPressSaldo("' + iDataIndex + '","' + iDataIndex + '_input")');
						if ($(row).find('td .activeCheck').val() == 'true') {
							$($(row).find('td.select-checkbox').get(0)).removeClass('select-checkbox')
						}
						$(row).find('td:first-child').attr('id', iDataIndex + '_select');
					},
					//AL FINALIZAR LA CARGA AGREGAR CONTROL DE COMAS Y PUNTO
					"initComplete": function (settings, json) {
						$.unblockUI();
						totalSaldo()
					}
				}).on( 'select', function ( e, dt, type, indexes ) {
								let saldo_default = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo_default').val();
								let tipo = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').attr('tipo');
								let id_tipo_documento = $('#listado').find('tbody tr#'+indexes+'_fila td.select-checkbox input.id_tipo_documento').val();
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').val(saldo_default);
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo').val(0);

								console.log(id_tipo_documento);
								//Si es NC no habilitamos
								if(id_tipo_documento != 32 && id_tipo_documento != 2){
									$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').prop('disabled',false);
								}
								reCalcular(saldo_default,tipo,1);
							} )
							.on( 'deselect', function ( e, dt, type, indexes ) {
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').prop('disabled',true);
								/*{{--COLOCAR EL SALDO POR DEFECTO --}}*/
								let saldo_default = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo_default').val()
								let tipo = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').attr('tipo');
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').val(0);
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo').val(saldo_default);
								$('#listado').find('tbody tr#'+indexes+'_fila').css('background-color','');
								reCalcular(saldo_default,tipo,2);
							} )
							.on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
								/*{{--EVITAR EL SELECT SOBRE EL ELEMENTO YA PAGADO --}}*/
									if($(originalEvent.currentTarget).find('input.activeCheck').val() == 'true'){
									e.preventDefault();
									}
							}).on('draw', function () {
									/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

									$('#listado tbody tr .numeric[data-mask = false]').inputmask("numeric", {
										radixPoint: ",",
										groupSeparator: ".",
										digits: 2,
										autoGroup: true,
										// prefix: '$', //No Space, this will truncate the first character
										rightAlign: false
									});

									$('#listado tbody tr .numeric[data-mask = false]').attr('data-mask','true');

									$('#listado tbody tr .numeric[data-mask = true]').keyup(function (){
																		//this.value = (this.value + '').replace(/[^0-9]/g, '');
																		this.value = (this.value + '').replace(/(\.|\s)|(\,)/g,(m,p1,p2) => p1 ? "" : ".")
										});
							});
				}else{

					//Anular eventos de tabla al momento de destruir la instancia
					if(table){
						table.off('select deselect user-select draw');
					}

					table = $("#listado").DataTable({
					destroy: true,
					keys: true,
					ajax: {
						url: "{{route('getListProveedor')}}",
						data: form,
						error: function (jqXHR, textStatus, errorThrown) {
							msjToast('', 2, jqXHR.status, textStatus);
							$.unblockUI();
						}

					},
					"order": [[ 21, "asc" ]],
					"columns": [
						{
							data: function (x) {
								input = '';
								var bgClass = x.reprice_pago_pro ? 'bg-danger' : '';
								if (x.pagado_proveedor === true || Number(x.saldo) <= 0 || x.pendiente == true || x.reprice_pago_pro === true) {
									input = '<input type="hidden" class="activeCheck" value="true" /> ';
								} else {
									input = '<input type="hidden" class="activeCheck" value="false" /> ';
								}
								 origenValue = x.origen;
					
									origenValue += 'REPRICE ' + origenValue;
							console.log(origenValue);
								input += `<input type="hidden" class="id_documento" value="${x.id}"> 
										<input type="hidden" class="id_tipo_documento" value="${x.id_tipo_documento}">
										<input type="hidden" class="id_proveedor" value="${x.id_proveedor}">
										<input type="hidden" class="currency_id" value="${x.id_moneda}">
										<input type="hidden" class="totalIva" value="${x.totaliva}"> 
										<input type="hidden" class="gravada10" value="${x.costo_gravada}">
										<input type="hidden" class="genero_retencion" value="${x.genero_retencion}">
										<input type="hidden" class="retencion_pago_parcial" value="${x.retencion_pago_parcial}">
										<input type="hidden" class="origen" value="${origenValue}">`;
								return `<div class="${bgClass}">${input}</div>`;
							}

						},

						{
							data: function (x) {
								return `<span class="proveedor_nombre">${x.proveedor_n}</span>`;
							}
						},
						{
							data: function (x) {
								if(x.fecha_proveedor_format !== null){
									return `<b>${x.fecha_proveedor_format}</b>`;
								}else{
									return "";
								}
							}
						},
						{
							data: function (x) {
								if(x.fecha_de_gasto !== null){
									fecha = x.fecha_de_gasto.split('-');
									fecha_gasto = fecha[2]+"/"+fecha[1]+"/"+fecha[0];
								}else{
									fecha_gasto = x.fecha_de_gasto;
								}
								return fecha_gasto;
							}
						},
						{
							data: function (x) {
								if(x.id_tipo_documento ==20){
									return `<a href="{{route('anticipoDetalle',['id'=>''])}}/${x.id}"  class="bgRed">
											<i class="fa fa-fw fa-search"></i>${x.id}</a>
											<div style="display:none;">${x.id}</div>`;
								}else{
									return `<a href="{{route('verLibroCompra',['id'=>''])}}/${x.id}"  class="bgRed">
											<i class="fa fa-fw fa-search"></i>${x.nro_factura}</a>
											<div style="display:none;">${x.id}</div>`;								
								}							
							}
						},
/*
						{
							data: 'nro_factura'
						},*/
						{
							data: function (x) {
								var input = `<input type="text" disabled style="text-align:center;" data-mask="false" class="input-style numeric" value="${x.importe}">`;
								return input;
							}
						},
						{
							"data": function (x) {
								let saldo = x.saldo_documento;
								if (saldo === null || saldo === undefined) {
									saldo = 0;
								}
								if(saldo  <= 0 ){
									color= 'green';
								}else{
									color= 'red';
								}
								var input = `<input type="text" disabled style="text-align:center; font-weight: 1000; color:${color}" data-mask="false" class="input-style numeric" value="${saldo}">`;
								return input;
							}
						},
						{data: "fecha_in_format", "title": "Check In", "render": function(data, type) {
							//console.log(moment(data).format('L'));
								return type === 'sort' ? data : data;
							}
						},
						{
							data: function (x) {
								return `<b>${x.tipo_doc_n}</b>`;
							}
						},
						{
							data: function (x) {
								return `<b>${x.tipo_doc_n}</b>`;
							}
						},

						{
							data: function (x) {
								return `<b>${x.pagado_proveedor_txt}</b>`;
							}
						},
						{
							data: 'pasajero_n'
						},
						{
							data: function (x) {
								return `<span class="currency_code">${x.currency_code}</span> 
											`;
							}
						},
						{
							data: function (x) {
								signo = '';
								if(x.id_tipo_documento == '2'|| x.id_tipo_documento == '32'||x.id_tipo_documento == '20' ){		
									signo = '<b>-</b>';
								}
								return signo;
							}
						},
						{
							"data": function (x) {
								let saldo = x.saldo;
								if (saldo === null || saldo === undefined) {
									saldo = 0;
								}
								if(x.id_tipo_documento == 2||x.id_tipo_documento == 20){
									//saldo = -1 * parseFloat(saldo);
								}
								var input = `
											<input type="hidden" value="${x.id_tipo_documento}" class="tipo_default ${x.currency_code}" >
											<input type="hidden" value="${saldo}" class="saldo_default ${x.currency_code}">
											<input type="text" value="${saldo}" data-mask="false" disabled  style="text-align:center;" class="saldo numeric">`;
								return input;
							}
						},
						{
							data: function (x) {
								signo = '';
								if(x.id_tipo_documento == '2'|| x.id_tipo_documento == '32'||x.id_tipo_documento == '20' ){		
									signo = '<b>-</b>';
								}
								return signo;
							}
						},

						{
							"data": function (x) {
								var input = `<input type="text" disabled data-mask="false" tipo="${x.id_tipo_documento}" style="text-align:center;" class="pago_saldo numeric" value="0">`;
								return input;
							}
						},

						{
							data: 'cod_confirmacion'
						},
						{
							data: 'denominacion_producto'
						},
						{
							data: function (x) {
								if (Boolean(x.id_proforma)) {
									return `<a href="{{route('detallesProforma',['id'=>''])}}/${x.id_proforma}"class="bgRed">
										<i class="fa fa-fw fa-search"></i>${x.id_proforma}</a>
										<div style="display:none;">${x.id_proforma}</div>`;
								} else {
									return '';
								}

							}
						},
						{
							data: 'fecha_facturacion_format'
						},
						{
							data: 'vendedor_n'
						},
						{
							data: function (x) {
        let origenValue = x.reprice_pago_pro ? 'REPRICE ' + x.origen : x.origen;
        return origenValue;
    }
						},
						{
							data: function (x) {
								if(x.id_tipo_documento ==20){
									return 1;
								}
								if(x.id_tipo_documento ==2){
									return 2;
								}
								if(x.id_tipo_documento ==32){
									return 2;
								}
								if(x.id_tipo_documento ==22){
									return 3;
								}
								if(x.id_tipo_documento ==1){
									return 3;
								}
								if(x.id_tipo_documento ==5){
									return 3;
								}
								if(x.id_tipo_documento ==33){
									return 3;
								}

							}
						}


					],

					"select": {
						style: 'multi',
						selector: 'td:first-child'
					},
					"columnDefs": [{
							orderable: false,
							className: 'select-checkbox',
							targets: 0
						},
						{
							targets: 0,
							className: 'style_check'
						},
						{
							targets: 9,
							visible: false,
							searchable: false
						},

						{
							targets: 14,
							className: 'col_input_saldo'
						},
						{
							targets: 16,
							className: 'col_input_pago_proveedor'
						},
						{
							targets: 23,
							visible: false,
							searchable: false
						},

					],
					"createdRow": function (row, data, iDataIndex) {
						if (data.pendiente == true) {
							$(row).css('background-color', '#EFF3BA');
						}
						if (data.reprice_pago_pro === true) {
							$(row).addClass('bg-danger');
						}

						$(row).attr('id', iDataIndex + '_fila');
						//ASIGNAR ID Y FUNCION
						$(row).find('td.col_input_pago_proveedor input').attr('id', iDataIndex + '_input');
						$(row).find('td.col_input_pago_proveedor').attr('onkeyup', 'validateKeyPressSaldo("' + iDataIndex + '","' + iDataIndex + '_input")');
						if ($(row).find('td .activeCheck').val() == 'true') {
							$($(row).find('td.select-checkbox').get(0)).removeClass('select-checkbox')
						}
						$(row).find('td:first-child').attr('id', iDataIndex + '_select');
					},
					//AL FINALIZAR LA CARGA AGREGAR CONTROL DE COMAS Y PUNTO
					"initComplete": function (settings, json) {
						$.unblockUI();
						totalSaldo()
					}

				}).on( 'select', function ( e, dt, type, indexes ) {
								 saldo_default = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo_default').val();
								 tipo = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.tipo_default').val();
								 let id_tipo_documento = $('#listado').find('tbody tr#'+indexes+'_fila td.select-checkbox input.id_tipo_documento').val();
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').val(saldo_default);
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo').val(0);
								
								$('#tipoBase').val(tipo);

								//Si es NC no habilitamos
								if(id_tipo_documento != 32 && id_tipo_documento != 2){
									$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').prop('disabled',false);
								}

								reCalcular(saldo_default,tipo,1, indexes);
							} )
							.on( 'deselect', function ( e, dt, type, indexes ) {
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').prop('disabled',true);
								/*{{--COLOCAR EL SALDO POR DEFECTO --}}*/
								 saldo_default = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo_default').val()
								  tipo = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.tipo_default').val();
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').val(0);
								$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo').val(saldo_default);
								$('#listado').find('tbody tr#'+indexes+'_fila').css('background-color','');
								reCalcular(saldo_default,tipo,2,indexes );
							} )
							.on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
								/*{{--EVITAR EL SELECT SOBRE EL ELEMENTO YA PAGADO --}}*/
									if($(originalEvent.currentTarget).find('input.activeCheck').val() == 'true'){
									e.preventDefault();
									}
							}).on('draw', function () {
									/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

									$('#listado tbody tr .numeric[data-mask = false]').inputmask("numeric", {
										radixPoint: ",",
										groupSeparator: ".",
										digits: 2,
										autoGroup: true,
										// prefix: '$', //No Space, this will truncate the first character
										rightAlign: false
									});

									$('#listado tbody tr .numeric[data-mask = false]').attr('data-mask','true');

									$('#listado tbody tr .numeric[data-mask = true]').keyup(function (){
																		//this.value = (this.value + '').replace(/[^0-9]/g, '');
																		this.value = (this.value + '').replace(/(\.|\s)|(\,)/g,(m,p1,p2) => p1 ? "" : ".")
										});
							});


				}		
				datatablePosterior();		
						
		}//function		
		function datatablePosterior(){
			$("#listado").on("click", ".selectAll", function () {
				console.log('ENTRUO');
				var checkboxes = $("#listado tbody").find("input[type='checkbox']");
				checkboxes.prop("checked", $(this).prop("checked"));
			});

			$("#listado").on("click", "input[type='checkbox']:not(.selectAll)", function () {
					var allCheckboxes = $("#listado tbody").find("input[type='checkbox']").not(".selectAll");
					var selectedCheckboxes = allCheckboxes.filter(":checked");
					var selectAllCheckbox = $(".selectAll");

					if (selectedCheckboxes.length === allCheckboxes.length) {
							selectAllCheckbox.prop("checked", true);
					} else {
							selectAllCheckbox.prop("checked", false);
					}
			});

		}

		sumaFecha = function(d, fecha)
			{
			var Fecha = new Date();
			var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
			var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
			var aFecha = sFecha.split(sep);
			var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
			fecha= new Date(fecha);
			fecha.setDate(fecha.getDate()+parseInt(d));
			var anno=fecha.getFullYear();
			var mes= fecha.getMonth()+1;
			var dia= fecha.getDate();
			mes = (mes < 10) ? ("0" + mes) : mes;
			dia = (dia < 10) ? ("0" + dia) : dia;
			var fechaFinal = dia+sep+mes+sep+anno;
			return (fechaFinal);
			}

		function totalSaldo(){
			totalUsd = 0;
			totalPyg = 0;
			$('#listado').DataTable().rows().data().each(function(row, index){
				if(row.id_tipo_documento == 1 ||row.id_tipo_documento == 22 || row.id_tipo_documento == 5|| row.id_tipo_documento == 33){
					if(row.id_moneda == 111){
						totalPyg = totalPyg + parseFloat(row.saldo);
					}else{
						totalUsd = totalUsd + parseFloat(row.saldo);
					}
				}
				if(row.id_tipo_documento == 2 || row.id_tipo_documento == 20){
					if(row.id_moneda == 111){
						totalPyg = totalPyg - parseFloat(row.saldo);
					}else{
						totalUsd = totalUsd - parseFloat(row.saldo);
					}
				}
			});
			$('#totalSaldoPyg').val(totalPyg);
			$('#totalSaldoUsd').val(totalUsd);
		}

		function reCalcular(saldo_default,tipo,suma_resta, lugar){
			// console.log(saldo_default);
			// console.log(tipo);
			// console.log(suma_resta)
			// console.log(lugar);
			console.trace();
			if(saldo_default > 0){
				total = parseFloat(clean_num($('#totalFinal').val()));
			 	if(tipo == 2||tipo == 20){
					saldoFila = -1 * parseFloat(saldo_default);
				}else{
					saldoFila = parseFloat(saldo_default);
				} 

				console.log('saldoFila',saldoFila);

				if(suma_resta == 1){
					resultado = total + parseFloat(saldoFila);
				}else{
					resultado = total - parseFloat(saldoFila);
				}

				console.log('resultado',resultado);
			}else{
				total = parseFloat(clean_num($('#totalFinal').val()));
				saldo_default = parseFloat(clean_num($('#saldo_default').val()));
				tipo = $('#tipoBase').val();
			/*	console.log('======================================================');
				console.log(total);
				console.log(saldo_default);
				console.log(tipo);
				console.log('======================================================');

				/*if(tipo == 2||tipo == 20){
					saldoFila = -1 * parseFloat(saldo_default);
				}else{
					saldoFila = parseFloat(saldo_default);
				}*/
				if(suma_resta == 1){
					resultado = total + parseFloat(saldo_default);
				}else{
					resultado = total - parseFloat(saldo_default);
				}
			}

           // console.log(resultado);

		/*	var importe_total = 0 
			$(".pago_saldo").each(
			    function(index, value) {
					if($(this).val() != 0){
						if($(this).attr('tipo') == 2||$(this).attr('tipo') == 20){
							importeFila = parseFloat(clean_num($(this).val())) * -1;
						}else{
							importeFila = parseFloat(clean_num($(this).val()));
						}
						/*console.log('=================================================================');
						console.log($(this).attr('tipo'));
						console.log(importeFila);
						console.log('=================================================================');*/
					/*	importe_total = importe_total + importeFila;
					}
			     // }
			    }
			);*/
      		$("#totalFinal").val(resultado);
		}


		function resta(monto){
			var sum=0;
			montoSuma = parseFloat(monto);
			montoTotal = parseFloat(clean_num($('#totalFinal').val()));
			sum = montoTotal - montoSuma;
			$('#totalFinal').val(sum);	
		}		


		function sumatoria(monto){
			var sum=0;
			montoSuma = parseFloat(monto);
			montoTotal = parseFloat(clean_num($('#totalFinal').val()));
			sum = montoSuma + montoTotal;
			$('#totalFinal').val(sum);		 
		}		

			
		//INICIAR BUSQUEDA CON ENTER EN EL FORM
		function enterSearchEventDatatable(){
			$( "#proveedor" ).focus(() =>{});
			$("#consultaFactura").keypress((e) => {
				   if(e.which == 13) { loadDatatable(); } 
			});
		}
		

		/*function validateKeyPressSaldo(id,evento){
			let data = $('#listado').find('tbody tr');
			let monto = 0;
			let saldo = 0; 
			let saldo_diferencia = 0;
			let total = 0;
			saldo = $('#listado').find('tbody tr#'+id+'_fila td.col_input_saldo .saldo_default').val();
			tipo = $('#listado').find('tbody tr#'+id+'_fila td.col_input_saldo .tipo_default').val();
			monto = clean_num($('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').val());
			if(tipo == '2'|| tipo == '32'||tipo == '20' ){
				console.log(monto.toString().indexOf('-'));
				if(monto.toString().indexOf('-') == -1){
					monto = monto * -1 
				}
			}
			saldo_diferencia = saldo - monto;

			console.log(monto);

			if(saldo_diferencia < 0 | monto < 0){
				$('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').css('border-color','red');
			} else {
				$('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').css('border-color','');	
			}
			$('#listado').find('tbody tr#'+id+'_fila td.col_input_saldo .saldo').val(formatter.format(parseFloat(saldo_diferencia)));
			$('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').val(monto);

			var sum=0;
			$('.pago_saldo').each(function() {  
				sum += parseFloat(clean_num($(this).val()));  
			}); 
			$('#totalFinal').val(sum);		
		}*/

		function validateKeyPressSaldo(id,evento){
			let data = $('#listado').find('tbody tr');
			let monto = 0;
			let saldo = 0; 
			let saldo_diferencia = 0;
			let total = 0;
			/*{{--CALCULO DE DIFERENCIA--}}*/
					/*{{--NO LIMPIAR DATO DE BD --}}*/
			saldo = $('#listado').find('tbody tr#'+id+'_fila td.col_input_saldo .saldo_default').val();
			tipo = $('#listado').find('tbody tr#'+id+'_fila td.col_input_saldo .tipo_default').val();
			monto = clean_num($('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').val());

			//if(tipo == '2'|| tipo == '32'||tipo == '20' ){
				saldo_diferencia = parseFloat(saldo) - parseFloat(monto);
		/*	}else{
				saldo_diferencia = parseFloat(saldo) + parseFloat(monto);
			}*/

			if(saldo_diferencia < 0 | monto < 0){
				$('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').css('border-color','red');
			} else {
				$('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').css('border-color','');	
			}
			$('#listado').find('tbody tr#'+id+'_fila td.col_input_saldo .saldo').val(formatter.format(parseFloat(saldo_diferencia)));
			recalcularSaldo();
		}

		/*{{-- ==================================================================================================  
											OBTENER Y VALIDAR ITEMS SELECCIONADOS
			 ================================================================================================== --}}*/
			function recalcularSaldo(){
				var sum=0;
				$('.pago_saldo').each(function() {  
					tipo = $(this).attr('tipo');
					if(tipo == '2'|| tipo == '32'||tipo == '20' ){
						/*console.log('-----------------------------------------------');
						console.log(sum);
						console.log(parseFloat(clean_num($(this).val())));
						console.log('===============================================');*/
						sum = sum - parseFloat(clean_num($(this).val()));  
					}else{
						/*console.log('-------------------******----------------------');
						console.log(sum);
						console.log(parseFloat(clean_num($(this).val())));
						console.log('==================*******=====================');*/

						sum = sum + parseFloat(clean_num($(this).val()));
					}
				}); 
				$('#totalFinal').val(sum);
			}



			$('#btnProcesar').click(()=>{
				processSelection();
			});
			 
		function processSelection(){
			operaciones.clear();
			blockUI();
			/*{{--VALIDAR ITEMS --}}*/
			if(getItems()){
					if(validateValues(getItems())){
						/*{{--REDIRECCION Y PROCESO --}}*/
						processOrder()
					} else { $.unblockUI(); } 

			 } else {
			 	$.unblockUI();
			 	msjToast('Seleccione algún ítem para continuar',1);
			 }
		}//	 

		function getItems(){
			/*{{--validar cantidad de selecciones y 
				traer los que se encuentran en otras paginas --}}*/
			if(table.rows( '.selected' ).nodes().length != 0){
				 /*{{-- RETORNAR UN OBJETO JQUERY--}}*/
				return table.rows( '.selected' ).nodes().to$();
			}
				return false; 	
		}//

function validateValues(items) {

	let id_moneda;
	let id_proveedor;
	let saldo_default;
	let saldo;
	let pago_saldo;
	let id_libro_compra = null;
	let id_anticipo = null;
	let id_nc = null;
	let old_proveedor;
	let old_moneda;
	let neto_pago = 0;
	let total_pago = 0;
	let total_nc = 0;
	let total_anticipo = 0;
	let data = [];
	let flag_1 = 0; //QUE SEA MISMO PROVEEDOR Y MISMA MONEDA
	let flag_2 = 0; //QUE SALDO NO SEA NEGATIVO Y PAGO NO SEA CERO 
	let flag_3 = 0; //QUE SEA UN TIPO DE DOCUMENTO VALIDO
	let flag_4 = 0; //VALIDAR QUE LA MONEDA NO SEA EURO 
	let id_tipo_documento,
		iva10 = 0,
		gravada10 = 0,
		data_gravada = 0;
		totalIva = 0;

	/*RECORRER LOS ITEMS OBTENIDOS*/
	$.each(items, function (index, val) {
		//RECORRER Y RECUPERAR INPUTS
		id_tipo_documento 		= Number($(val).find('.id_tipo_documento').val());
		id_moneda 				= Number($(val).find('.currency_id').val());
		id_proveedor 			= Number($(val).find('.id_proveedor').val());
		id_documento 			= Number($(val).find('.id_documento').val());
		saldo 					= clean_num($(val).find('.saldo').val());
		retencion_pago_parcial	= Number($(val).find('.retencion_pago_parcial').val());


			if(id_tipo_documento == '2'|| id_tipo_documento == '32' ||id_tipo_documento == '20'){
				pago_saldo 		= clean_num($(val).find('.pago_saldo').val()) * -1;
			}else{
				pago_saldo 		= clean_num($(val).find('.pago_saldo').val());
			}

		//CALCULAR RETENCION DE LOS ITEMS QUE NO GENERARON

		if($(val).find('.retencion_pago_parcial').val() == 'false'){
			if($(val).find('.genero_retencion').val() == 'false'){
				// iva10 				+= Number($(val).find('.iva10').val());
				data_gravada 			= Number($(val).find('.gravada10').val());
				if(!isNaN(data_gravada)){
					gravada10 += Number($(val).find('.gravada10').val());
				} 
				data_iva 			= Number($(val).find('.totalIva').val());
				
				if(!isNaN(data_iva)){
				//	totalIva += Number($(val).find('.totalIva').val());
				} 

			}
		
			data_iva = Number($(val).find('.totalIva').val());
				
			if(!isNaN(data_iva)){
				totalIva += Number($(val).find('.totalIva').val());
			} 
		}
		/*if(!isNaN(data_iva)){
				totalIva += Number($(val).find('.totalIva').val());
		} */


		/*{{--REGLAS DE VALIDACION DE ITEMS --}}*/
		if (index === 0) {
			old_proveedor = id_proveedor;
			old_moneda = id_moneda;
		}

		/*OPERACIONES ARITMETICAS*/
		/*DOCUMENTO FACTURA*/
		if (id_tipo_documento == '1' | id_tipo_documento == '5' | id_tipo_documento == '22'| id_tipo_documento == '33') {
			total_pago += pago_saldo;
			id_libro_compra = id_documento;
		}
		/*DOCUMENTO NOTA CREDITO*/
		else if (id_tipo_documento == '2'| id_tipo_documento == '32') {
			total_nc += pago_saldo;
			id_nc = id_documento;
		//	id_libro_compra = id_documento;
		} /*DOCUMENTO ANTICIPO*/
		else if (id_tipo_documento == '20') {
			total_anticipo += pago_saldo;
			id_anticipo = id_documento;
		} else {
			flag_3++;
		}

		console.log(pago_saldo);

		//VALIDAR QUE LA MONEDA NO SEA EURO 
		// if (id_moneda != 143 && id_moneda != 111) {
		// 	flag_4++;
		// }
		//VALIDAR QUE SEA MISMO PROVEEDOR Y MISMA MONEDA
		if (id_proveedor !== old_proveedor || old_moneda !== id_moneda) {
			flag_1++;
		}
		//VALIDAR QUE SALDO NO SEA NEGATIVO Y PAGO NO SEA CERO
		/*if (saldo < 0 || pago_saldo <= 0 | pago_saldo == undefined) {
			flag_2++;
		}*/
		if (flag_1 != 0 || flag_2 != 0) {
			$('.selected#' + val.id).css('background-color', '#F89494');
		} else {
			$('.selected#' + val.id).css('background-color', '#B0BED9');
			//ALMACENAR ITEMS SELECCIONADOS
			data.push({
				operacion: {
					id_compra: id_libro_compra,
					id_anticipo: id_anticipo,
					id_nc: id_nc,
					monto_pago: pago_saldo
				}
			});
		}
		//FORMATEAR VALORES
		id_tipo_documento = null;
		id_documento = null;
		saldo = 0;	
		pago_saldo = 0; 	
		id_libro_compra = null;
		id_anticipo = null;
		id_nc = null;

	});


	//RETURN PARA CONTINUAR PROCESO
	totalResta = parseFloat(total_nc) + parseFloat(total_anticipo); 
	if (flag_1 != 0 | flag_2 != 0 | flag_3 != 0 | totalResta > parseFloat(total_pago).toFixed(2)) {
		/*if (flag_2 != 0) {
			msjToast('El pago del saldo no puede ser menor o igual a cero.');
		}*/
		if (flag_1 != 0) {
			msjToast('Seleccione el mismo proveedor y la misma moneda.');
		}
		if (flag_3 != 0) {
			msjToast('El tipo de documento seleccionado no es valido.');
		}
		// if (flag_4 != 0) {
		// 	msjToast('No se puede procesar en otras monedas que no sean USD y PYG.');
		// }
		if (total_nc + total_anticipo > parseFloat(total_pago).toFixed(2)) {
			msjToast('El total de las notas de créditos y anticipos supera el total de la factura.');
		}
		//msjToast('', 1);
		//return false;
	} else {
		//ALMACRENAR CABECERA DE DETALLES SELECCIONADOS
		if(total_nc  < 0){
			total_nc = total_nc * -1;
		}
		if(total_anticipo  < 0){
			total_anticipo = total_anticipo * -1;
		}

		operaciones.data = data;
		neto_pago = total_pago - (total_anticipo + total_nc);

		operaciones.total_pago = total_pago;
		operaciones.total_nc = total_nc;
		operaciones.total_anticipo = total_anticipo;
		operaciones.total_pago_neto = neto_pago;
		operaciones.iva10 = iva10;
		operaciones.gravada10 = gravada10;
		operaciones.totalIva = parseFloat(totalIva).toFixed(2);
		operaciones.cabecera = {
			id_proveedor: id_proveedor,
			proveedor_n: $('.selected').find('.proveedor_nombre').html(),
			id_moneda: id_moneda,
			currency_code: $('.selected').find('.currency_code').html()
		};
		return true
	}

} //




	function processOrder(){

		$.toast().reset('all');

		let cotizacion = 0;
		let cotizado = 0;
		let id_moneda = operaciones.cabecera.id_moneda;
		/*$('#tabConfirm').tab('show');
		$('#tabHome').prop('disabled',true);*/
		$('a[href="#cabecera"]').click();

		$('#cf_currency_code').val(operaciones.cabecera.currency_code);
		$('.cf_currency_code').html('PYG');

		if(id_moneda == 111){
			$('#cf_total_pago').val(operaciones.total_pago);
		}else{
			$('#cotizado_total_factura').val(operaciones.total_pago);
		}

		$('#cf_anticipo').val(operaciones.total_anticipo);	
		$('#cf_nota_credito').val(operaciones.total_nc);
		$('#cf_neto_pago').val(operaciones.total_pago_neto);
		$('#cf_beneficiario').val(operaciones.cabecera.proveedor_n);
		$('#iva_10').val(operaciones.iva10);
		$('#gravada_10').val(operaciones.gravada10);

		/*{{--VALIDAR QUE LOS ELEMENTOS AJAX SE FINALIZEN PARA EMITIR MENSAJE --}}*/
		Promise.all([getRetencion()]).then(() => {
			console.log('FINALIZA RETENCION CON EXITO');

					Promise.all([getCotizacion(),setCotizar()]).then(() => {
						console.log('FINALIZA COTIZACIONES CON EXITO');
						UnblockOp();	
					}).catch(() => {
						BlockOp();
						console.log('FINALIZA COTIZACIONES CON ERROR');
					});
				
			}).catch(() => {
				console.log('ERROR EN RETENCION');
				BlockOp();
			}).finally(() => {
				console.log('FIN DE PROCESO');
				$.unblockUI();
			});
 		
		
		
	}


	/*{{-- ============================================================================================ 
											RETENCION
	============================================================================================--}}*/	

	function getRetencion(){
	return new Promise((resolve, reject) => { 
		total_iva = parseFloat(operaciones.iva5) + parseFloat(operaciones.iva10);
		$.ajax({
		  async:false,	
          type: "GET",
          url: "{{route('getRetencion')}}",
          data:{ 
				id_proveedor: operaciones.cabecera.id_proveedor,
				data: operaciones.data,
				id_moneda: operaciones.cabecera.id_moneda	
		   },
          dataType: 'json',
          error: function(jqXHR,textStatus,errorThrown){
          	 msjToast('Ocurrio un error en la comunicación con el servidor y no se pudo obtener la retención.',2,jqXHR,textStatus);
          	 reject(false);
          },
          success: function(rsp){
					if(rsp.err === true){
						if(rsp.retencion === true){
							$('#ret_iva_10').val(rsp.monto);
							$('#cf_retencion').val(rsp.monto);
							operaciones.retencion = Number(rsp.monto);
							operaciones.total_pago_neto = operaciones.total_pago_neto - Number(rsp.monto);
							$('#cf_neto_pago').val(operaciones.total_pago_neto);
						} else {
							$('#ret_iva_10').val(0);
							$('#cf_retencion').val(0);
						}

						resolve({resp:true});
				
					} else {
						msjToast('Ocurrio un error en la comunicación con el servidor y no se pudo obtener la retención.',2);
						$('#ret_iva_10').val(0);
						console.log('OCURRIO UN ERROR EN LA CONSULTA');
						reject(false);
					}


                          }//success
                 });

			});//promise


		}//


				

/*{{-- ============================================================================================ 
						PROCESAR Y ENVIAR DATOS PARA CONFIRMAR PAGO
	============================================================================================--}}*/	

	function validar_envio(){
		let err = true;
		let msj='';

		$.toast().reset('all');

		if($('#cf_cotizacion').val().trim() == '0' | $('#cf_cotizacion').val().trim() == ''){
			$('#cf_cotizacion').css('border-color','red');
			err = false;
			msjToast('La cotización no puede ser cero');
		} else {
			$('#cf_cotizacion').css('border-color','');
		}

		// if($('#cf_entrega').val().trim() == ''){
		// 	 err = false;
		// 	$('#cf_entrega').css('border-color','red');
		// 	msjToast('Complete el campo Entregado a.');
		// } else {
		// 	$('#cf_entrega').css('border-color','');
		// }

		// if($('#cf_concepto').val().trim() == ''){
		// 	err = false;
		// 	msjToast('Complete el campo Concepto');
		// 	$('#cf_concepto').css('border-color','red');
		// } else {
		// 	$('#cf_concepto').css('border-color','');
		// }

		if(!err){
			msjToast('',1);
		}

		return err;
	}


	
  /* ============================================================================================ 
									MODAL PARA CONFIRMAR CREAR OP
	============================================================================================ */	



	//aqui
	function sendData(){
				$('#btnProcesarOP').prop('disabled',true);
				return swal({
                        title: "GESTUR",
                        text: "¿Desea crear la Orden de Pago?",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, crear",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
                                    // swal("Exito", "", "success");
									let info = {
										/*FP */
										sucursal:$('#cf_sucursal').val(),
										centro_costo:$('#cf_centro_costo').val(),
										cotizacion : clean_num($('#cf_cotizacion').val()),
										pagosDetalle:operaciones.data,
										pagosCabecera:operaciones.cabecera,
										id_moneda_fp:$('#fp_id_moneda').val(),
										/*PAGO PROVEEDOR */
										total_pago:operaciones.total_pago,
										total_pago_neto:operaciones.total_pago_neto,
										total_nc:operaciones.total_nc,
										total_anticipo:operaciones.total_anticipo,
										retencion : operaciones.retencion,
										gravada10:operaciones.gravada10
										// entregado: $('#cf_entrega').val(),
										// concepto : $('#cf_concepto').val()
									}
									if(validar_envio()){
										$.ajax({
										          type: "POST",
										          url: "{{route('setPagoProveedor')}}",
										          data: info,
										          dataType: 'json',
										           error: function(jqXHR,textStatus,errorThrown){

										           		msjToast('',2,jqXHR.status,textStatus);
												      	$('#btnProcesarOP').prop('disabled',false);
														swal("Cancelado", "Ocurrió un error en la comunicación con el servidor.", "error");
												  },
										          success: function(rsp){

										          		if(rsp.err === true){
															op.id = rsp.id_op;
															// openNewOp();
															cancelarOp();
															swal("Exito!", 'Se creó la OP Nº: '+rsp.id_op, "success");
															$('a[href="#cierreCaja"]').click();

										          		} else {
															swal("Cancelado", rsp.infoError, "error");
										          			// msjToast('Ocurrio un error al intentar procesar la operación.',2);
										          		}
														  $('#btnProcesarOP').prop('disabled',false);

										            }//success
							                 }).done(function(){
							                 	$('#btnProcesarOP').prop('disabled',false);
							                 });//AJAX

									 } else {
										swal("Cancelado", 'Complete los datos requeridos.', "error");
									 	$('#btnProcesarOP').prop('disabled',false);
									 }//if
                                } else {
									$('#btnProcesarOP').prop('disabled',false);
                                     swal("Cancelado", "La operación fue cancelada.", "error");
                                }
            	});
	}


	function cancelarOp(){

		operaciones.clear();
		$('#listadoPagos tbody').html('');
		$('#formTotales')[0].reset();
		{{--PESTAÑA --}}
		// loadDatatable();
		$('#tabHome').tab('show');

		//LIMPIAR DATATABLE.
		table
		.clear()
		.draw();

		$('#tabConfirm').prop('disabled',true);
		$('#btnProcesarOP').prop('disabled',true);

		$('.clear_input').val('0');
		$('.clear_input_txt').val('');
		$('#fp_id_moneda').prop('disabled',false);
		$('#btnAgregarFila').prop('disabled',false);
		


	}


	const op = {
		id:0
	}

	function openNewOp(){
		if(op.id != 0){
		//	$('#num_op').html(op.id);
			//$('#modalNumOp').modal('show');
		}
	}

	$('#modalNumOp').on('hidden.bs.modal', function (e) {
		cancelarOp();
		op.id = 0;
		$('#num_op').html('');
		})
	



	/*{{-- ==============================================
			CONFIG Y OTROS
		==============================================--}}*/

 function initConfig(){
 	$('#tabConfirm').prop('disabled',true);
 	$('#btnProcesarOP').prop('disabled',true);
 	//  blockUI();
 }


 function blockUI(){

 	     $.blockUI({
				  centerY: 0,
				  message: "<h2>Procesando...</h2>",
				  css: {
				    color: '#000'
				   }
				 });

 }


 		{{--Almacena mensajes para mostrar --}}
		function msjToast(msj,opt = false, jqXHR = false, textStatus = ''){
	

			if( msj != ''){ 
				operaciones.msj.push('<b>'+msj+'</b>');
			} 

			if(opt === 1){
				$.toast().reset('all');

				$.toast({	
				    heading: '<b>Atención</b>',
				    position: 'top-right', 
				    text: operaciones.msj,
				    width: '400px',
				    hideAfter: false,
				    icon: 'info'
				});
				operaciones.msj = [];

			} else if(opt === 2){
				$.toast().reset('all');

				

				if(jqXHR === false && textStatus === ''){
						msj = operaciones.msj;
				 } else{

						 errCode = jqXHR.status;
						 errMsj = jqXHR.responseText;

				 	msj ='<b>';
				 	if(errCode === 0 )
				 		msj += 'Error de conectividad a internet, verifica tu conexión.';
				 	else if(errCode === 404)
				 		msj += 'Error al realizar la solicitud.';
				 	else if(errCode === 500){
				 		if(operaciones.msj.length > 0){
				 		msj += operaciones.msj;
				 		} else { msj += 'Ocurrio un error en la comunicación con el servidor.'}

				 	}else if(errCode === 406){
				 		msj += errMsj;
				 	}
				 	else if(errCode === 422)
				 		 msj += 'Complete todos los datos requeridos'
				 	else if(textStatus === 'timeout')
				 		msj += 'El servidor tarda mucho en responder.';
				 	else if(textStatus === 'abort')
				 		msj += 'El servidor tarda mucho en responder.';
				 	else if (textStatus === 'parsererror'){
				 		msj += 'Error al realizar la solicitud.';
				 	}
				 	else 
				 		msj += 'Error desconocido, pongase en contacto con el area tecnica.';
				 	msj += '</b>';
				 }


				 	$.toast({
						    heading: '<b>Error</b>',
						    text: msj,
						    position: 'top-right', 
						     hideAfter: false,
						     width: '400px',
						    icon: 'error'
						});

				operaciones.msj = [];
			}
		}//


			$('.control_space').keypress(function(event) {
			
			var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
		});
	        
	     const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD'
					});


		/*{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
			NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}*/
		function clean_num(n,bd=false){

				  	if(n && bd == false){ 
					n = n.replace(/[,.]/g,function (m) {  
							 				 if(m === '.'){
							 				 	return '';
							 				 } 
							 				  if(m === ','){
							 				 	return '.';
							 				 } 
							 			});
					return Number(n);
				}
				if(bd){
					return Number(n);
				}
				return 0;

				}//

		$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});
		

		/*{{--FUNCION PARA ALMACENAR DATOS --}}	*/
		const operaciones = {
			data:{},
			cabecera:{},
			total_pago:0,
			total_nc:0,
			total_pago_neto:0,
			total_anticipo:0,
			iva10:0,
			gravada10:0,
			retencion:0,
			msj:[],
			clear : function(x){
				this.data = {};
				this.cabecera = {};
				this.total_pago = 0;
				this.total_nc = 0;
				this.total_pago_neto = 0;
				this.total_anticipo = 0;
				this.iva10 = 0;
				this.gravada10 = 0;
				this.retencion = 0;
			}
		}




	/*{{-- ==============================================
			COTIZACIONES
		==============================================--}}*/

	function getCotizacion(){
	return new Promise((resolve, reject) => { 
		let  cotizacion;


			{{--OBTENER COTIZACION --}}
		$.ajax({
		  async:false,	
          type: "GET",
          url: "{{route('getCotizacionFP')}}",
          data:{id_moneda_costo:operaciones.cabecera.id_moneda,
      			id_moneda_venta: $('#fp_id_moneda').val()},
          dataType: 'json',
          error: function(jqXHR,textStatus,errorThrown){
          	 msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo obtener la cotización.',2,jqXHR,textStatus);
          	 reject(false);
          },
          success: function(rsp){

                          if(rsp.info.length > 0){
                          	let cot = Number(rsp.info[0].cotizacion);
                          	

                          	if( cot > 0){
	                          	$('#cf_cotizacion').val(rsp.info[0].cotizacion);
	                          	cotizacion = rsp.info[0].cotizacion;
	                          	resolve({resp:true});

                              } else {
                              	msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.',1);
                              	reject(false);
                              }
                            } else {
                            	msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.',1);
                            	reject(false);
                            }

                          }//success
                 });

			});//promise


		}//


	function setCotizar(opt=0){
	return new Promise((resolve, reject) => { 

	if((operaciones.cabecera.id_moneda != $('#fp_id_moneda').val()) | opt == 1){
		let data = {};
		data = {
			cabecera:{ id_moneda_costo:operaciones.cabecera.id_moneda,
      			       id_moneda_venta: $('#fp_id_moneda').val()
      			     },
      		total_factura: operaciones.total_pago,
      		//total_nc: (operaciones.total_nc * -1),
			total_nc: operaciones.total_nc,
      		//total_anticipo: (operaciones.total_anticipo * -1),
			total_anticipo: operaciones.total_anticipo,
      		total_pago_neto: operaciones.total_pago_neto,
			retencion:operaciones.retencion   	     
		}

	
			$.ajax({
          type: "GET",
          url: "{{route('cotizarMontosProveedor')}}",
          data:data,
          dataType: 'json',
          error: function(jqXHR,textStatus,errorThrown){

          		 msjToast('Ocurrio un error en la comunicación con el servidor y no se pudo cotizar los montos.',2,jqXHR,textStatus);
          		 reject(false);
          },
          success: function(rsp){
                          if(rsp.info.length > 0){
                          	if(rsp.moneda == '111'){
                          		//alert($('#fp_id_moneda').val());
                          		$('#cotizado_total_factura').val(formatter.format(parseFloat(rsp.info[0].total_factura)));
                          	}else{
                          		//alert($('#fp_id_moneda').val());
                          		$('#cf_total_pago').val(formatter.format(parseFloat(rsp.info[0].total_factura)));
                          	}

                          	$('#cotizado_anticipo').val(formatter.format(parseFloat(rsp.info[0].total_anticipo)));
							$('#cf_anticipo').val(formatter.format(parseFloat(rsp.info[0].total_anticipo_gs)));
							$('#cotizado_retencion').val(formatter.format(parseFloat(rsp.info[0].total_retencion)));
							$('#cf_retencion').val(formatter.format(parseFloat(rsp.info[0].total_retencion_gs)));
							$('#cotizado_nc').val(formatter.format(parseFloat(rsp.info[0].total_nc)));
							$('#cf_nota_credito').val(formatter.format(parseFloat(rsp.info[0].total_nc_gs)));
							
							$('#cotizado_pago_neto').val(formatter.format(parseFloat(clean_num($('#cotizado_total_factura').val())) - (parseFloat(clean_num($('#cotizado_anticipo').val())) + parseFloat(clean_num($('#cotizado_nc').val())) +parseFloat(clean_num($('#cotizado_retencion').val())))));
							$('#cf_neto_pago').val(formatter.format(parseFloat(clean_num($('#cf_total_pago').val())) - (parseFloat(clean_num($('#cf_anticipo').val())) + parseFloat(clean_num($('#cf_nota_credito').val())) +parseFloat(clean_num($('#cf_retencion').val())))));
                         	resolve({resp:true});

                          } else {
                          	 msjToast('Ocurrio un error al intentar cotizar los valores.',1);
                          	 reject(false);

                          }

                          }//success
                 })//ajax
	


		 } else {
	
		 	$('#cotizado_total_factura').val(operaciones.total_pago);
            $('#cotizado_anticipo').val(operaciones.total_anticipo);
            $('#cotizado_nc').val(operaciones.total_nc);
            $('#cotizado_pago_neto').val(operaciones.total_pago_neto);


			 resolve({resp:true});
			
		 }	


		 });//promise

	}


			$("#botonExcel").on("click", function(e){ 
			// console.log('Inicil');
                e.preventDefault();
                $('#consultaFactura').attr('method','post');
               	$('#consultaFactura').attr('action', "{{route('generarExcelPagoProv')}}").submit();
            });


		$('#fp_id_moneda').change(function(){
			
			Promise.all([getCotizacion(),setCotizar()]).then(() => {
				UnblockOp();	
			}).catch(() => {
				BlockOp();
			}).finally(() => {
				/*{{--FIN PROCESO DE VALIDACION --}}	*/	
				$.unblockUI();
			});
	
			
			});		

		 function BlockOp(){
		 	$('#btnAgregarFila').prop('disabled',true);
		 	$('#btnProcesarOP').prop('disabled',true);
		 }	

		  function UnblockOp(){
		 	$('#btnAgregarFila').prop('disabled',false);
		 	$('#btnProcesarOP').prop('disabled',false);
		 }	


	function controlNumFormat(){
		$('.formatInput').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false
		});
	}
	

		function sumarTotales(value,documento){
			if(documento == 22 || documento == 5){
				total = parseFloat(clean_num($("#totalSaldo").val()));
				/*console.log('--------------------------------------');
				console.log(total);
				console.log(value);
				console.log('--------------------------------------');*/
				suma = total + parseFloat(value);
				$("#totalSaldo").val(suma);
			}	
		}

		$('#proveedor').change(function(){
			if($(this).val() == 44407 || $(this).val() == 71300){
				$(".bsp").css('display','block');
			}else{
				$(".bsp").css('display','none');
			}
		});



</script>

@endsection
