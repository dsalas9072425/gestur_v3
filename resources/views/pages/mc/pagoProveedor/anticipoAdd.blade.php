@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
    .correcto_col {
        height: 74px;
    }
    					
    input.form-control:focus ,.select2-container--focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
     .mr-1 {
         margin-right: 5px; 
     }
     .labelError{
         color: #F74343 ;
     }
</style>

@parent
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title"> Crear Anticipo Proveedor</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

        <form class="row" id="formDataAnticipo" autocomplete="off">
        <input type="hidden" name="nombreImagen" value="" id="imagens">

            <div class="col-12 col-sm-4">
                <div class="form-group">
                    <label>Forma Pago (*)</label>
                    <select class="form-control select2" name="id_tipo_operacion" id="tipo_operacion"
                        style="width: 100%;">
                        @foreach ($tipo_operacion_pago as $operacion)
                            <option value="{{$operacion->id}}" data-adjunto="{{$operacion->adjunto_obligatorio}}">{{$operacion->denominacion}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label>Sucursal (*)</label>
                        <select class="form-control select2" name="id_sucursal" id="ant_sucursal" data-value-type="number" style="width: 100%;" required>
                            @foreach ($sucursalEmpresa as $sucursal)
                            <option value="{{$sucursal->id}}">{{$sucursal->denominacion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label>Centro Costo (*)</label>
                        <select class="form-control select2" name="id_centro_costo" id="ant_centro_costo" data-value-type="number" style="width: 100%;">
                            @foreach ($centro as $c)
                            <option value="{{$c->id}}">{{$c->nombre}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                    <div class="col-6 col-sm-4">
                        <div class="form-group">
                            <label>Cambio (*)</label>
                        <input type="text" class="form-control format-number-cotizacion" name="cotizacion" id="cotizacion" data-value-type="convertNumber" value="{{$cotizacion_contable_venta}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label>Moneda (*)</label>
                            
                            <select class="form-control select2" name="id_moneda" id="ant_id_moneda" data-value-type="number" style="width: 100%;" required>
                                @foreach ($currency as $div)
                                <option value="{{$div->currency_id}}">{{$div->currency_code}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                <div class="col-6 col-sm-4">
                    <div class="form-group">
                        <label>Importe (*)</label>
                        <input type="text" class="form-control format-number-pago" name="importe"  id="importe_anticipo" data-value-type="convertNumber" maxlength="18"  value="0" required>
                    </div>
                </div>

                <div class="col-6  col-sm-4">
                    <div class="form-group">
                       <label>Fecha Pago *</label>						 
                       <div class="input-group">
                           <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                           </div>
                              <input type="text"  name="fecha_pago:s_date" class="form-control single-picker" id="fecha_pago" />
                       </div>
                   </div>
               </div>


                <div class="col-12">
                    <div class="card-header">
                        <h4 class="card-title font-weight-bold" id="heading-labels">Forma de Pago</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <span class="badge badge-info">Campos Obligatorios *</span>
                        </div>
                    </div>
                </div>
           
                <div class="col-12"> 
                    <div class="row" id="tipo_operacion_div" style="border: 2px solid black;"></div>
                </div>   


                <div class="col-12">
                        <div class="table-responsive">
                            <table id="listadoPagos" class="table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Operación</th>
                                        <th>Importe Pago</th>
                                        <th>Moneda</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody style="text-align: center" class="tbody_fp">
                                    <tr class="odd">
                                        <td valign="top" colspan="6" class="relleno_fp">No hay datos disponibles
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                </div>  

                  <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>Beneficiario (*)</label>
                            <select class="form-control select2" name="id_beneficiario" id="ant_beneficiario" style="width: 100%;">
                                 <option value="">Seleccione un Beneficiario</option>
                                @foreach ($beneficiario as $b)
                                    @php
                                        $ruc = $b->documento_identidad;
                                        if(isset($b->dv)){
                                            $ruc = $ruc."-".$b->dv;
                                        }
                                    @endphp
                                    <option value="{{$b->id}}" data="{{$b->nombre}}" tipo="{{$b->tipo_proveedor}}">{{$ruc}} - {{$b->nombre}} {{$b->apellido}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'V')
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>Ventas </label>
                                <select class="form-control select2" disabled name="id_venta" id="id_venta" data-value-type="number"  style="width: 100%;">
                                    <option value="">
                                    </option>
                                </select>
                            </div>
                        </div>
                    @else   
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>Grupo </label>
                                <select class="form-control select2" disabled name="id_grupo" id="id_grupo" data-value-type="number"  style="width: 100%;">
                                
                                    <option value="">
                                    </option>
                                
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>Proformas </label>
                                <select class="form-control select2" disabled name="id_proforma" id="id_proforma" data-value-type="number"  style="width: 100%;">
                                
                                    <option value=""></option>
                                    
                                </select>
                            </div>
                        </div>
                    @endif

                    <div class="col-11">
                        <div class="form-group">
                             <label>Cuenta Contable (*)</label>
                            <select class="form-control select2" disabled name="id_cuenta_contable" id="ant_cuenta_contable" data-value-type="number" style="width: 100%;">
                                    

                                    @foreach ($cuentas_contables as $cuenta)

                                    @if($cuenta->asentable)
                                    <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif   
                                        
                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                        </option>
                                    @if($cuenta->asentable)
                                    </optgroup>
                                    @endif  
                                    

                                    @endforeach

                                
                             </select>
                        </div>
                    </div>

                    <div class="col-1">
                        <button onclick="modalModificarCuenta()" style="margin-top:27px;" type="button" class="btn btn-info text-white">
                            <i class="fa fa-edit"></i>
                        </button>
                        </div>

                    <div class="col-12">
                            <div class="form-group">
                                <label>Concepto (*)</label>
                                <input type="text" class="form-control text-bold" name="concepto" id="ant_concepto" value="">
                            </div>
                        </div>
             
              <div class="col-12">
                    <button type="button" class="mr-1 btn btn-success btn-lg pull-right" id="btnSaveAnticipo"><b>Guardar</b></button>
                    
                    <button type="button" class="mr-1 btn btn-danger btn-lg pull-right" id="btnVolver"><b>Volver</b></button> 
                    
              </div>         
        </form>
 
    </div>
</div>
</div>
</section>





<!--======================================================
        MODAL DE ADJUNTO IMAGEN
========================================================== -->
    <!-- /.content -->
    <div id="modalAdjunto" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
        <!-- Modal content-->
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto <i class="fa fa-refresh fa-spin cargandoImg"></i></h2>
                </div>
                <input type="hidden" class = " form-control input-sm" id="idLinea" name="cantidad_habitaciones"/>
                <div class="modal-body">
                    <!-- Post -->
                    <div class="post">
                      <!-- /.user-block -->
                        <div class="row margin-bottom">
                            <div id='output'>

                            </div>  
                              
                            </div>    
                            <form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('adjuntoDocumentosOp')}}" autocomplete="off">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-12">
                                        <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="file" class="form-control" name="image" id="image"/>  
										<h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: PNG, JPG, JPEG y PDF.</b>
                                        </h4>
                                        <div id="validation-errors"></div>

                                      </div> 
                                    </div>  
                                  </div>  
							  </form>
							  
                            <div class="row">
                                <div class= "col-12">
                                    {{-- <button type="button" id="btnAceptarAdjunto" class="btn btn-success pull-right font-weight-bold">Aceptar</button> --}}
                                    <button type="button" class="btn btn-danger font-weight-bold pull-right" data-dismiss="modal">Cerrar</button>                        
                                </div>
                            </div>
                        </div>
                    <!-- /.post -->
                    </div>
                </div>  
            </div>  
        </div>  

@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script> <!--FORMAT MONEDA -->
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script> <!--IMAGEN ADJUNTO -->

<script type="text/javascript">
    $(() => {
        // $('[data-toggle="popover"]').popover();  
        $('[data-toggle="tooltip"]').tooltip()
        main();
});

    const operaciones = {
        bloquear_anticipo : false,
        total_pago : 0,
        id_moneda : 0,
        requeridos_fp: [],
        requeridos_ant : ['tipo_operacion',
                          'ant_sucursal',
                          'ant_centro_costo',
                          'cotizacion',
                          'ant_id_moneda',
                          'importe_anticipo',
                          'ant_beneficiario',
                          'ant_cuenta_contable',
                          'ant_concepto',
                          'fecha_pago']
    }


    function main() {
        initCalendar();
        // formatNumber();
        $('.select2').select2();
        formatNumberCotizacion();
        formatNumberPago();
        loadOperacion();
        cuentaPredeterminada();
 
    } //

    	//DEFINIR FORMATO MONEDAS		
	const EURO = value => currency(value, {
		symbol: "",
		decimal: ',',
		separator: '.',
		precision: 2
	});
	const DOLAR = value => currency(value, {
		symbol: "",
		decimal: ',',
		separator: '.',
		precision: 2
	});
	const GUARANI = value => currency(value, {
		symbol: "",
		separator: '.',
		precision: 0
	});

    
    $('#ant_beneficiario').change(() => {
        console.log($('#ant_beneficiario :selected').attr('tipo') );
        if($('#ant_beneficiario :selected').attr('tipo') == 2){
            $('#id_proforma').prop('disabled',true); 
            $('#id_grupo').prop('disabled',true); 
        }else{
            obtenerDataSelect();
        }
    });


    //OBTENER DATOS DE SELECCION DE GRUPO Y PROFORMA
    function obtenerDataSelect(){

        $.ajax({
          type: "GET",
          url: "{{route('getLisAddAnticipoProveedor')}}",
          data:{ cliente_id: $('#ant_beneficiario').val()} ,
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){
                    $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });


          },
          success: function(rsp){

            $('#id_grupo').empty();
            $('#id_proforma').empty();  
            $('#id_venta').empty();

            $('#id_proforma').prop('disabled',false); 
            $('#id_grupo').prop('disabled',false); 
            $('#id_venta').prop('disabled',false); 

            if(rsp.ventas.length){
                var optionProforma = new Option('Seleccione Venta', '', false, false);
                $('#id_venta').append(optionProforma);
            
                $.each(rsp.ventas, function (key, item){
                    optionProforma = new Option(item.id, item.id, false, false);
                $('#id_venta').append(optionProforma)
                });
                $('#id_venta').prop('disabled',false); 
            } else {
                $('#id_venta').prop('disabled',true); 
            }

            if(rsp.proformas.length){
                var optionProforma = new Option('Seleccione Proforma', '', false, false);
                $('#id_proforma').append(optionProforma);
            
                $.each(rsp.proformas, function (key, item){
                    optionProforma = new Option(item.id, item.id, false, false);
                $('#id_proforma').append(optionProforma)
                });
                $('#id_proforma').prop('disabled',false); 
            } else {
                $('#id_proforma').prop('disabled',true); 
            }


            if(rsp.grupos.length){
                var optionGrupo = new Option('Seleccione Grupo', '', false, false);
                $('#id_grupo').append(optionGrupo);

                $.each(rsp.grupos, function (key, item){
                optionGrupo = new Option(item.denominacion, item.id, false, false);
                $('#id_grupo').append(optionGrupo)
                });
                $('#id_grupo').prop('disabled',false); 
            } else {  
                $('#id_grupo').prop('disabled',true); 
            }

            

                          }//success
                 }).done(function(){
                    // $('#btnProcesarOP').prop('disabled',false);
                 });//AJAX

    }




 /*================================================================================================
                                   VALORES DE ANTICIPO
================================================================================================*/
    $('#importe_anticipo').change(()=>{
        loadValues();
    });

    $('#ant_id_moneda').change(()=>{
        formatNumberPago();
        cuentaPredeterminada();
    });

    $('#ant_beneficiario').change(()=>{
        cuentaPredeterminada();
    });

    function loadValues(){
        operaciones.total_pago =  clean_num($('#importe_anticipo').val());
        operaciones.id_moneda = Number($('#ant_id_moneda').val());
        $('#fp_importe_pago').val(operaciones.total_pago);
    }



 /*================================================================================================
                                   ABM FORMA DE PAGO
================================================================================================*/

function insertFormaPago() 
{
    // BlockFp();
    operaciones.bloquear_anticipo = true;

    let ok = 0,
        id_banco_detalle        = $('#fp_id_cuenta').val()
        id_forma_pago           = $('#fp_id_origen_pago').val(),
        imagen_dir              = $("#imagens").val(),
        documento               = (Boolean( $('#fp_documento_forma_pago').val() )) ? $('#fp_documento_forma_pago').val(): null,
        importe_pago            = (Boolean( $('#fp_importe_pago').val().trim() )) ? $('#fp_importe_pago').val().trim() : ok++,
        nro_comprobante         = (Boolean( $('#nro_comprobante').val() )) ? $('#nro_comprobante').val() : null,
        fecha_emision           = (Boolean( $('#fecha_emision').val() )) ? $('#fecha_emision').val() : '',
        fecha_vencimiento       = (Boolean( $('#fecha_vencimiento').val() )) ? $('#fecha_vencimiento').val() : '',
        beneficiario_txt        = (Boolean( $('#beneficiario_texto').val() )) ? $('#beneficiario_texto').val() : null,
        al_portador             = $('#portador_check').is(":checked"),
        id_beneficiario_cheque  = $('#id_beneficiario_cheque').val(),
        //DATOS ADICIONALES
        texto_cuenta = $('#fp_id_cuenta :selected').text(),
        texto_origen = $('#fp_id_origen_pago :selected').text(),
        tipo_operacion_txt = $('#tipo_operacion :selected').text(),
        texto_moneda = $('#ant_id_moneda :selected').text(),
        importe_pago_format = 0,
        cantItem = $('#listadoPagos tr.tabla_filas').length,
        id_fp_detalle = 0;

    let msj = '';

    //VALIDAR LOS CAMPOS REQUERIDOS
    if (!validarCamposFp(operaciones.requeridos_fp)) {
        ok = 1;
        msjToast('Complete los campos requeridos');
    }


    if (ok != 0) {
        //PENDIENTE CARGADOR
        msjToast('Complete los campos obligatorios por favor.', 'info');

    } else {
        importe_pago_format = clean_num(importe_pago);

        if (cantItem == 1) {
            msjToast('Solo se puede cargar una forma de pago.', 'info');
            ok++;
        }

        if (!isNaN(importe_pago_format)) {
            if (importe_pago_format <= 0) {
                ok++;
                msjToast('El importe no puede ser cero o menor a cero.', 'error');
                }
            } else {
                    ok++;
                    msjToast('Error desconocido al realizar los calculos.', 'error');
            }
    }




    if (ok == 0) {
        console.log('este es el tipo de pago:' + $('#tipo_operacion :selected').text());

        /*INSERTAR FILA*/
        let cantItem = $('#listadoPagos tr.tabla_filas').length;
        let tabla = `
        <tr id="rowPago_${cantItem}" class="tabla_filas">
            <td>
              <input type="hidden" value="${documento}" name="documento"/>
              <input type="hidden" value="${id_banco_detalle}" name="id_banco_detalle" data-value-type="number"/>
              <input type="hidden" value="${fecha_emision}" name="fecha_emision" data-value-type="s_date"/>
              <input type="hidden" value="${fecha_vencimiento}" name="fecha_vencimiento" data-value-type="s_date"/>
              <input type="hidden" value="${beneficiario_txt}" name="beneficiario_txt"/>
              <input type="hidden" value="${al_portador}" name="al_portador"/>
              <input type="hidden" value="${nro_comprobante}" name="nro_comprobante"/>
              <input type="hidden" value="${id_beneficiario_cheque}" name="id_beneficiario_cheque"/>
              

              <input type="text" readonly value = "` + $('#tipo_operacion :selected').text() + `" class = "form-control input-sm" />
            </td>
            <td><input type="text" readonly value = "${importe_pago}" class = "input_importe_pago form-control input-sm format-number-control" /></td>
            <td><input type="text" readonly value = "${texto_moneda}" class = "form-control input-sm"/></td>
            <td><button type="button" onclick="eliminarFilaPago(${cantItem})" data-id=${id_fp_detalle}  class="btn btn-danger"><b>Eliminar</b></button>
            </td>
        </tr>
        `;

        if ($('.tbody_fp tr td').hasClass('relleno_fp')) {
            $('.tbody_fp').html('');
        }
        $('#listadoPagos tbody').append(tabla);

        let cont = 0;
        $.each($('.tabla_filas'), function (index, value) {
            let num_cant = value.id.split('rowPago_');
            if (num_cant[1] != cont) {
                $('#' + value.id).attr('id', 'rowPago_' + cont);
                //MODIFICAR ONCLICK
                $(value.children[5]).find('button').attr('onclick', 'eliminarFilaPago(' + cont + ')');
            }
            cont++;

        });


        //BLOQUEAR CAMPOS
        bloqCampos(true);
    }



}//

function eliminarFilaPago(id) 
{

            $('#rowPago_' + id).remove();
            let cantItem = $('#listadoPagos tr').length - 1;
            if (cantItem == 0) {
                //HABILITAR COMBO DE MONEDA
                $('#fp_id_moneda').prop('disabled', false);
                $('.tbody_fp').html(`<tr class="odd"><td valign="top" colspan="6" class="relleno_fp">No hay datos disponibles</td></tr>`);
            }
            //HABILITAR PARA AGREGAR FILA
            bloqCampos(false);
            msjToast('Linea Eliminada.', 'info');

} //function


//aqui
function bloqCampos(_switch)
{
    $('#cotizacion').prop('readonly',_switch);
    $('#ant_id_moneda').prop('disabled',_switch);
    $('#importe_anticipo').prop('readonly',_switch);
    $('#tipo_operacion').prop('disabled',_switch);
    $('#ant_sucursal').prop('disabled',_switch);
    $('#ant_centro_costo').prop('disabled',_switch);
    if(_switch){
        $('#tipo_operacion_div').html('');
    } else {
        loadOperacion();
    }
}






 /*================================================================================================
                                  TIPO PAGO
================================================================================================*/

$('#tipo_operacion').on('select2:selecting', function (e) 
{
    operaciones.adjunto_obligatorio = e.params.args.data.element.dataset.adjunto;
    let id_item = e.params.args.data.id;
    tipo_operacion(id_item);
});

function loadOperacion()
{   
    let id_item = $('#tipo_operacion').val();
    tipo_operacion(id_item);
}

function tipo_operacion(id_operacion) 
{

    let tipo_operacion = id_operacion;
    let efectivo,
        tarjeta,
        cheque,
        transferencia,
        html = '';




    transferencia = `
        <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Banco</label> 						 
                    <select class="form-control select2" name="" id="fp_id_origen_pago" style="width: 100%;">		
                     @foreach ($origen as $forma)
                         <option value="{{$forma->id}}">{{$forma->nombre}}</option>
                     @endforeach		
                    </select>
                </div> 
            </div>

            <div class="col-12  col-sm-6">
                    <div class="form-group">
                        <label>Cuenta</label> 						 
                        <select class="form-control select2" name="" disabled id="fp_id_cuenta" style="width: 100%;">			
                        </select>
                    </div> 
            </div>


            
            <div class="col-12 col-sm-6">
                    <div class="form-group">
                         <label>Nro de Transferencia</label>
                       <input type="text" class="form-control clear_input_txt" id="nro_comprobante" value="0">
                   </div>
               </div>

               <div class="col-12 col-sm-6 adjuntoHiden">
                    <label>Subir Archivo Adjunto *</label>
                        <button type="button" id="btnAdjuntoModal" class="btn btn-primary btn-block">Adjunto</button>
                    </div>
                    <input type="hidden" name="nombreImagen" value="" id="imagen">
              </div>

           <div class="col-12">
                    <div class="form-group">
                        <label>Importe Pago *</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control format-number-pago" id="fp_importe_pago" name="fp_importe_pago">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary" id="btnAddFormaPago" title="Agregar Forma Pago" type="button"><i class="fa fa-plus fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                  </div> `;

    cheque = `			

        <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Banco *</label> 						 
                    <select class="form-control select2" name="" id="fp_id_origen_pago" style="width: 100%;">		
                     @foreach ($origen as $forma)
                         <option value="{{$forma->id}}">{{$forma->nombre}}</option>
                     @endforeach		
                    </select>
                </div> 
            </div>


                <div class="col-12  col-sm-6">
                    <div class="form-group">
                        <label>Cuenta *</label> 						 
                        <select class="form-control select2" name="" disabled id="fp_id_cuenta" style="width: 100%;">			
                        </select>
                    </div> 
                </div>




       
       <div class="col-4 col-sm-4">
         <div class="form-group">
               <label>Fecha Emisión *</label>						 
               <div class="input-group">
                   <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                   </div>
                      <input type="text"  class="form-control single-picker" id="fecha_emision" />
               </div>
           </div>	
       </div>	

       <div class="col-6  col-sm-4">
            <div class="form-group">
               <label>Fecha Vencimiento *</label>						 
               <div class="input-group">
                   <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                   </div>
                      <input type="text"  class="form-control single-picker" id="fecha_vencimiento" />
               </div>
           </div>
       </div>

        <div class="col-6 col-sm-4">
            <div class="form-group">
                <label>Nro de Cheque *</label>
                <input type="text" class="form-control clear_input_txt" id="nro_comprobante" value="0">
            </div>
       </div>


        <div class="col-12  col-sm-8">
            <div class="form-group">	
                <label>Elegir Nombre</label> 
                <select class="form-control select2" name="id_beneficiario_cheque" id="id_beneficiario_cheque" style="width: 100%;">
                </select>
            </div> 
        </div>

        <div class="col-12  col-sm-4">
            <label> </label> 
            <div class="checkbox">
                <label>
                <input type="checkbox" id="portador_check" name="portador"> Al portador
                </label>
            </div>
        </div>

        <div class="col-12">
            <div class="form-group">
                <label>Beneficiario</label> 		
                <input type="text" class="form-control clear_input_txt" name="beneficiario_texto" id="beneficiario_texto" value="">
            </div> 
        </div>

        <div class="col-12 adjuntoHiden">
                    <label>Subir Archivo Adjunto</label>
                        <button type="button" id="btnAdjuntoModal" class="btn btn-primary btn-block">Adjunto</button>
                    </div>
                    <input type="hidden" name="nombreImagen" value="" id="imagen">
        </div>

           <div class="col-12">
                    <div class="form-group">
                        <label>Importe Pago *</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control format-number-pago" id="fp_importe_pago" name="fp_importe_pago">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary" id="btnAddFormaPago" title="Agregar Forma Pago" type="button"><i class="fa fa-plus fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                  </div>
           
           `;

    efectivo = `
            <div class="col-12 adjuntoHiden" id="inputimagen">
                    <label>Subir Archivo Adjunto</label>
                        <button type="button" id="btnAdjuntoModal" class="btn btn-primary btn-block">Adjunto</button>
                    </div>
                    <input type="hidden" name="nombreImagen" value="" id="imagen">
              </div> 

        <div class="col-12 col-sm-6" id="inputfp_documento_forma_pago">
               <div class="form-group">
                   <label>Documento </label>
                   <input type="text" class="form-control" name="documento" id="fp_documento_forma_pago" placeholder="Documento">
               </div>
           </div>

        

           <div class="col-12 col-sm-6" id="inputfp_importe_pago">
                    <div class="form-group">
                        <label>Importe Pago *</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control format-number-pago" id="fp_importe_pago" name="fp_importe_pago">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary" id="btnAddFormaPago" title="Agregar Forma Pago" type="button"><i class="fa fa-plus fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                  </div>
            `;
       
       tarjeta = `
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label>Entidad</label>                         
                    <select class="form-control select2" name="" id="fp_id_origen_pago" style="width: 100%;">       
                     @foreach ($origen as $forma)
                         <option value="{{$forma->id}}">{{$forma->nombre}}</option>
                     @endforeach        
                    </select>
                </div> 
            </div>

            <div class="col-12  col-sm-6">
                    <div class="form-group">
                        <label>Cuenta de Fondo</label>                        
                        <select class="form-control select2" name="" disabled id="fp_id_cuenta" style="width: 100%;">           
                        </select>
                    </div> 
            </div>
            
            <div class="col-12 col-sm-6">
                    <div class="form-group">
                         <label>Número</label>
                       <input type="text" class="form-control clear_input_txt" id="nro_comprobante" value="0">
                   </div>
               </div>

           <div class="col-12">
                    <div class="form-group">
                        <label>Importe Pago *</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control control-num-moneda-fp input-fp importe_a_pagar" id="fp_importe_pago" name="fp_importe_pago">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary" id="btnAddFormaPago" title="Agregar Forma Pago" type="button"><i class="fa fa-plus fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                  </div> `;
        


    //VALORES OBLIGATORIOS PREDETERMINADO
    operaciones.requeridos_fp = ['ant_id_moneda', 'cotizacion', 'tipo_operacion', 'importe_anticipo'];

    $('#tipo_operacion_div').html('');
    //EFECTIVO
    if (tipo_operacion == '1') {
     
        operaciones.requeridos_fp.push('fp_documento_forma_pago');
        $('#tipo_operacion_div').html(efectivo);
    }

    //CHEQUE
    if (tipo_operacion == '4') {

        operaciones.requeridos_fp.push('fp_id_origen_pago', 'fp_id_cuenta', 'fecha_emision', 'fecha_vencimiento', 'nro_comprobante');
        $('#tipo_operacion_div').html(cheque);

        numMaxCheque();
        agregarBeneficiario();
    }

    //TRANSFERENCIA
    if (tipo_operacion == '6') {

        $('#tipo_operacion_div').html(transferencia);
    }

        //TARJETA
    if (tipo_operacion == '11') {

        $('#tipo_operacion_div').html(tarjeta);
    }


    if (operaciones.adjunto_obligatorio) {
        $('.adjuntoHiden').show();
        operaciones.requeridos_fp.push('imagen');
    }

    $('#fp_importe_pago').val(formatCurrency(operaciones.id_moneda,operaciones.total_pago));
    levantar_eventos_fp();

} //

function levantar_eventos_fp() 
{


    //DESACTIVAR EVENTOS ANTERIORES
    $('#btnAddFormaPago').unbind();
    $('#fp_id_origen_pago').unbind();
    $('#btnAdjuntoModal').unbind();

    //MODAL PARA ADJUNTAR DETALLES 
    $('#btnAdjuntoModal').click(() => {
        $('#modalAdjunto').modal({
            backdrop: 'static'
        });
    });


    $('#modalAdjunto').on('hidden.bs.modal', function (e) {
        $('#modalGastos').modal('handleUpdate');
    })




    //ACTIVAR EVENTOS NUEVOS
    $('#fp_id_origen_pago').select2();
    $('#fp_id_cuenta').select2();

    $('#fp_id_origen_pago').change(function () {
        getCuentas();

    });

    $('#btnAddFormaPago').click(() => {
        insertFormaPago();
    });


    $('#portador_check').click(() => {
        $("#beneficiario_texto").toggleClass("desactivar");
        $("#id_beneficiario_cheque").toggleClass("desactivar");

        ($("#beneficiario_texto").hasClass('desactivar')) ? $("#beneficiario_texto").prop('disabled', true): $("#beneficiario_texto").prop('disabled', false);
        ($("#id_beneficiario_cheque").hasClass('desactivar')) ? $("#id_beneficiario_cheque").prop('disabled', true): $("#id_beneficiario_cheque").prop('disabled', false);

    });

    // $('#id_beneficiario_cheque').val(operaciones.id_proveedor).trigger('change.select2');
    getCuentas();//CUENTAS BANCO
    initCalendar(); //iniciar calendario
    formatNumberPago();

} //


  
    /*===========================CUENTA PREDETERMINADA==================================*/
    function cuentaPredeterminada()
    {
        let dataString = {
                cuentas:['ANTICIPO_PROVEEDOR'],
                id_moneda : $('#ant_id_moneda').val(),
                id_cliente: $('#ant_beneficiario').val()
            };

        $.ajax({
            type: "GET",
            url: "{{route('getCuentaPredeterminada')}}",
            data: dataString,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
           
            },
            success: function (rsp) {
                
                $.each(rsp.data, (index,value)=>{
                     $(`#ant_cuenta_contable`).val(value.id_cuenta_contable).trigger('change.select2');
                });

            } //success
        }).done(function () {
      
        }); //AJAX

    } 

    function modalModificarCuenta()
    {
      return  swal({
                    title: "GESTUR",
                    text: "Esta seguro que desea modificar la cuenta contable?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Modificar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            swal("Éxito", 'Cuenta Contable Habilitada', "success");
                            $('#ant_cuenta_contable').prop('disabled',false);
                        } else {
                            swal("Cancelado", '', "error");
                        }
                });
                              
    }

/*================================================================================================
                                   SAVE DATA
================================================================================================*/
//HABILITAR CAMPO PARA ENVIO DE FORMULARIO
function formPreSend(_switch){
    $('#cotizacion').prop('readonly',_switch);
    $('#ant_id_moneda').prop('disabled',_switch);
    $('#importe_anticipo').prop('readonly',_switch);
    $('#tipo_operacion').prop('disabled',_switch);
    $('#ant_sucursal').prop('disabled',_switch);
    $('#ant_centro_costo').prop('disabled',_switch);
    $('#ant_cuenta_contable').prop('disabled',_switch);
}

function saveDataAnticipo()
{   
    return new Promise((resolve, reject) => {

    $('#btnSaveAnticipo').prop('disabled',true);
    loadProccess();
    formPreSend(false); //ENVIAR CAMPOS DISABLED
    let valor = '';
                                           //LIMPIA ESPACIOS VACIOS
    let dataString = $('#formDataAnticipo').serializeJSON({ 
                    customTypes: customTypesSerializeJSON
                });
    formPreSend(true);  
     //LIMPIAR FORMULARIO DE DATOS INVALIDOS           
    // $.each(dataString,(index,value)=>{
    //     if(value == null | value == undefined | isNaN(value)){
    //         delete dataString[index];
    //     }
    //     });

        $.ajax({
              type: "GET",
              url: "{{route('saveAnticipo')}}",
              data:dataString ,
              dataType: 'json',
              cache:false,
               error: function(jqXHR,textStatus,errorThrown){
                    $('#btnSaveAnticipo').prop('disabled',false);
                      $.unblockUI();
                      reject('Ocurrio un error en la comunicación con el servidor.');
    		  },
              success: function(rsp){
                        $('#btnSaveAnticipo').prop('disabled',false);
                        if(rsp.err === true){
                            resolve(rsp.msj);
                        } else {
                            $.unblockUI();
                            reject('Ocurrio un error al intentar almacenar los datos.  '+rsp.msj);
                        }
                    }//success
            });//AJAX
     });          

}//

$('#btnSaveAnticipo').click((event) => 
    {
        if($('#cotizacion').val() != "" && $('#cotizacion').val() != 0 ){
            if($('#ant_beneficiario :selected').attr('tipo') == 2){
                if($('#listadoPagos tr.tabla_filas').length > 0){
                    if(validarCamposFp(operaciones.requeridos_ant)){
                        modalCrearAnticipo();
                    } else {
                        msjToast('Complete los campos obligatorios por favor.', 'error');
                    }    
                } else {
                            msjToast('Inserte una forma de pago por favor.', 'info');
               }
            }else{ 
                guardarAnticipo()
            }
        }else{
            $('#btnSaveAnticipo').prop('disabled',false);
            $.unblockUI();
            msjToast('Cotización desactualizada  o vacia no se puede realizar operaciones. Cargue la cotización por favor.', 'info');
      
        }
    });
    

    $('#btnVolver').click(() => {
        redirectView();
    });

    function guardarAnticipo(){
                 if(jQuery.isEmptyObject($('#id_grupo').val()) == false || jQuery.isEmptyObject($('#id_proforma').val()) == false || jQuery.isEmptyObject($('#id_venta').val()) == false){
                        if($('#listadoPagos tr.tabla_filas').length > 0){
                            if(validarCamposFp(operaciones.requeridos_ant)){
                                modalCrearAnticipo();
                            } else {
                                msjToast('Complete los campos obligatorios por favor.', 'error');
                            }    
                        } else {
                            msjToast('Inserte una forma de pago por favor.', 'info');
                        }
                }else{
                    tipoEmpresa = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}";

                    if(tipoEmpresa == 'V'){
                        if($('#ant_beneficiario :selected').attr('data') == 2){
                            if($('#listadoPagos tr.tabla_filas').length > 0){
                                if(validarCamposFp(operaciones.requeridos_ant)){
                                    modalCrearAnticipo();
                                } else {
                                    msjToast('Complete los campos obligatorios por favor.', 'error');
                                }    
                            } else {
                                msjToast('Inserte una forma de pago por favor.', 'info');
                            }
                        }else{
                            $.toast({
                                heading: 'Error',
                                text: 'Se debe seleccionar una venta.',
                                position: 'top-right',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });        
                        }
                    }else{
                        $.toast({
                            heading: 'Error',
                            text: 'Se debe seleccionar un grupo o una proforma.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });        
                    }
                }
    }


/*================================================================================================
                                     MODAL
================================================================================================*/
function modalCrearAnticipo() 
    {

	    swal({
	        title: "Generar Anticipo",
	        text: "FAVOR TENER EN CUENTA QUE LA MONEDA DEL ANTICIPO DEBE SER IGUAL A LA MONEDA DE LA FACTURA",
	        icon: "warning",
	        showCancelButton: true,
	        buttons: {
	            cancel: {
	                text: "No, Cancelar",
	                value: null,
	                visible: true,
	                className: "btn-warning",
	                closeModal: false,
	            },
	            confirm: {
	                text: "Si , Generar",
	                value: true,
	                visible: true,
	                className: "",
	                closeModal: false
	            }
	        }
	    }).then(isConfirm => {

	        if (isConfirm) {
	            $.when(saveDataAnticipo()).then((a) => {

                    swal({
                            title: "Exito!",
                            text: a,
                            type: "success",
                            confirmButtonText: "Aceptar"
                            }).then(isConfirm => {
                            if (isConfirm) {
                                redirectView();
                            }
                        });

	            }, (b) => {
	                swal("Cancelado", b, "error");
	            });

	        } else {
	            swal("Cancelado", "La operación fue cancelada", "error");
	        }
	    });
	}

/*================================================================================================
                         LOGICA DE VALIDAR Y PROCESAR ANTICIPO
================================================================================================*/
function validarCamposFp(req) 
{
    var ok = 0;
    for (var j = 0; j < req.length; j++) {

        var id = req[j];
        var name = $("#" + id).get(0).tagName;

        if (name == 'INPUT' || name == 'TEXTAREA') {
            var input = $("#" + id).val();



            if (input == '') {
                $("#" + id).css('border-color', 'red');
                ok++;
            } else {
                $("#" + id).css('border-color', '#d2d6de');
            } //else



        } //if

        if (name == 'SELECT') {

            var select = $('#' + id).val();
            if (select == '') {

                $("#input" + id).find('.select2-container--default .select2-selection--single').css('border-color', 'red');
                ok++;
            } else {
                $("#input" + id).find('.select2-container--default .select2-selection--single').css('border', '1px solid #aaa');
            } //else


        } //if   


        if (name == 'A') {

            var inputHidden = $("#imagens").val();

            if (inputHidden == '') {
                $('#' + id).addClass('borderErrorBtn');
                ok++;
            } else {
                $('#' + id).removeClass('borderErrorBtn');
            } //else


        } //if

    } //for


    if (ok > 0) {
        return false;
    } else {
        return true;
    }






} //function





/*================================================================================================
                                  COTIZADOR Y DATOS REQUERIDOS POR AJAX
================================================================================================*/



function getCotizacion() 
{
    // console.lgo('SE DISPARO GETCOTIZACION');
    return new Promise((resolve, reject) => {
        let cotizacion;
        $.ajax({
            async: false,
            type: "GET",
            url: "{{route('getCotizacionFP')}}",
            data: {
                id_moneda: operaciones.id_moneda
            },
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                msjToast('Ocurrio un error en la comunicación con el servidor y no se pudo obtener la cotización.', 'error', jqXHR, textStatus);
                reject(false);
            },
            success: function (rsp) {

                if (rsp.info.length > 0) {
                    let cot = Number(rsp.info[0].cotizacion);


                    if (cot > 0) {
                        $('#fp_cotizacion').val(rsp.info[0].cotizacion);
                        cotizacion = rsp.info[0].cotizacion;
                        resolve({
                            resp: true
                        });


                    } else {
                        msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.', 'info');
                        reject(false);
                    }
                } else {
                    msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.', 'info');
                    reject(false);
                }

            } //success
        });

    }); //promise


} //


function getCuentas()
{
    var newOption;

    $.ajax({
        async: false,
        type: "GET",
        url: "{{route('getListCuentas')}}",
        data: {
            id_banco: $('#fp_id_origen_pago').val()
        },
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {

            $('#fp_id_cuenta').prop('disabled', true);
        },
        success: function (rsp) {
            if (rsp.cuentas.length > 0) {

                $('#fp_id_cuenta').empty();
                // newOption = new Option('Seleccione cuenta', '', false, false);
                $('#fp_id_cuenta').append(newOption);

                $.each(rsp.cuentas, function (key, item) {
                    newOption = new Option(item.denominacion + ' ' + item.currency_code + ' (' + item.numero_cuenta + ')', item.id, false, false);
                    $('#fp_id_cuenta').append(newOption);
                });
                $('#fp_id_cuenta').prop('disabled', false);
            } else {
                $('#fp_id_cuenta').empty();
                newOption = new Option('Seleccione cuenta', '', false, false);
                $('#fp_id_cuenta').append(newOption);
                $('#fp_id_cuenta').prop('disabled', true);
            }
        } //success
    }); //ajax


} //

/*================================================================================================
                                    LOGICA CHEQUE
================================================================================================*/
function agregarBeneficiario() 
{
    $("#id_beneficiario_cheque").select2({
        ajax: {
                url: "{{route('beneficiarioCheque')}}",
                dataType: 'json',
                placeholder: "Seleccione un Beneficiario",
                delay: 0,
                data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                                    };
                },
                processResults: function (data, params){
                            var results = $.map(data, function (value, key) {
                            return {
                                    children: $.map(value, function (v) {
                                        return {
                                                    id: key,
                                                    text: v
                                                };
                                        })
                                    };
                            });
                                    return {
                                        results: results,
                                    };
                },
                cache: true
                },
                escapeMarkup: function (markup) {
                                return markup;
                }, // let our custom formatter work
                minimumInputLength: 3,
    });
 

}
function numMaxCheque() 
{

    $.ajax({
        type: "GET",
        url: "{{route('numMaxCheque')}}",
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            // $('#msj_pago').css('background-color', '#C93F3F');
            // $('#msj_pago').html('Ocurrio un error en la comunicación con el servidor.');
            // ok++;
        },
        success: function (rsp) {
            $('#nro_comprobante').val(rsp.num);

        } //success
    })
}

/* ==============================================================================================================================
                                        IMAGEN FORM LOGICA DE ADJUNTO
============================================================================================================================== */


var options = 
{
    beforeSubmit: showRequest,
    success: showResponse,
    dataType: 'json'
};

//OCULTAR OVERLAY      
$('.cargandoImg').hide();

$('body').delegate('#image', 'change', function () 
{
    //OBTENER TAMAÑO DE LA IMAGEN
    var input = document.getElementById('image');
    var file = input.files[0];
    var tamaño = file.size;

    if (tamaño > 0) {

        var tamaño = file.size / 1000;
        if (tamaño > 3000) {
            $("#image").val('');
            $("#validation-errors").empty();
            $("#validation-errors").append('<div class="alert alert-error"><strong>El tamaño de la imagen supera el limite de 3MB</strong><div>');
        } else {

            $('.cargandoImg').show();
            $('#upload').ajaxForm(options).submit();
        }

    } else {
        $("#image").val('');
        $("#validation-errors").empty();
        $("#validation-errors").append('<div class="alert alert-error"><strong>Existe un error con el archivo</strong><div>');
    }

});


function showRequest(formData, jqForm, options) 
{

    $("#validation-errors").hide().empty();
    return true;
}


function showResponse(response, statusText, xhr, $form) 
{
    $('.cargandoImg').hide();
    if (response.success == false) {
        $("#image").val('');
        $("#validation-errors").append('<div class="alert alert-error"><strong>' + response.errors + '</strong><div>');
        $("#validation-errors").show();

    } else {
        $("#imagen").val(response.archivo);
        $("#imagens").val(response.archivo);

        var eliminar = response.archivo;
        $('#output').html('');
        var divImagen = `
                    <div class="row">
                    <div class="col-sm-9" >
                   
                    <div class="fonticon-wrap"><i class="ft-file-text" style="font-size:70px;">X</i></div>
                    </div>

                    <div class="col-md-1">
                    <button type="button" id="btn-imagen" onclick="eliminarImagen('${eliminar}')">
                    <i class="glyphicon glyphicon-remove"></i>
                    </button>
                    </div>
                    </div>
                    `;

        $("#output").append(divImagen);

        $("#image").prop('disabled', true);
    }
}


function eliminarImagen(archivo) 
{
    $('.cargandoImg').show();

    $.ajax({
        type: "GET",
        url: "{{route('fileDeleteOp')}}",
        dataType: 'json',
        data: {
            dataFile: archivo
        },


        error: function (jqXHR, textStatus, errorThrown) {

            $.toast({
                heading: 'Error',
                text: 'Ocurrio un error al intentar eliminar la imagen.',
                position: 'top-right',
                showHideTransition: 'fade',
                icon: 'error'
            });

            $('.cargandoImg').hide();

        },
        success: function (rsp) {
            $('.cargandoImg').hide();

            if (rsp.rsp == true) {
                document.getElementById("image").value = "";
                $("#output").html('');

            } else {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error al intentar eliminar la imagen.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                document.getElementById("image").value = "";
                $("#output").html('');

            } //ELSE

            $("#image").prop('disabled', false);
        } //function


    });
} //FUNCTION 



/*================================================================================================
                                   AUXILIARES
================================================================================================*/
    function redirectView()
    {
      
        $.blockUI({
						centerY: 0,
						message: "<h2>Redirigiendo a vista de Anticipo...</h2>",
						css: {
							color: '#000'
						}
					});

                    location.href ="{{ route('indexAnticipo') }}";
    }

    function loadProccess()
    {
        $.blockUI({
						centerY: 0,
						message: "<h2>Procesando...</h2>",
						css: {
							color: '#000'
						}
					});
    }



    function initCalendar() 
    {
        $('.single-picker').datepicker({
            format: "dd/mm/yyyy",
            language: "es"
        });
    }

    function formatNumberCotizacion() 
    {
        $('.format-number-cotizacion').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 0,
            autoGroup: true,
            rightAlign: false
        });

    }


    function formatNumberPago() 
    {
        let moneda = $('#ant_id_moneda').val();
        operaciones.id_moneda = moneda;
        $('.control-num-moneda-fp').unbind();
        $('.control-num-moneda-fp').inputmask('remove');

            if (moneda == '111') {
                $('.format-number-pago').inputmask("numeric", {
                    radixPoint: ",",
                    groupSeparator: ".",
                    digits: 0,
                    autoGroup: true,
                    rightAlign: false
                });

            } else {

                $('.format-number-pago').inputmask("numeric", {
                    radixPoint: ",",
                    groupSeparator: ".",
                    digits: 2,
                    autoGroup: true,
                    rightAlign: false
                });
            } //else
    }


    function clean_num(n, bd = false) 
    {

        if (n && bd == false) {
            n = n.replace(/[,.]/g, function (m) {
                if (m === '.') {
                    return '';
                }
                if (m === ',') {
                    return '.';
                }
            });
            return Number(n);
        }
        if (bd) {
            return Number(n);
        }
        return 0;

    } //

  


function formatCurrency(id_moneda, value) 
    {

		let importe = 0;

		//PYG
		if (id_moneda == 111) {
			importe = GUARANI(value, {
				formatWithSymbol: false
			}).format(true);
		}
		//USD
		if (id_moneda == 143) {
			importe = DOLAR(value, {
				formatWithSymbol: false
			}).format(true);
		}

		return importe;
	}


const aux = {
    msj: []
}
//Para almacenar mensajes o mostrar mensajes en el momento segun error del ajax

function msjToast(msj, opt = '', jqXHR = false, textStatus = '') 
{


    if (msj != '') {
        aux.msj.push('<b>' + msj + '</b>');
    }

    if (opt === 'info') {
        $.toast().reset('all');

        $.toast({
            heading: '<b>Atención</b>',
            position: 'top-right',
            text: aux.msj,
            width: '400px',
            hideAfter: false,
            icon: 'info'
        });
        aux.msj = [];

    } else if (opt === 'error') {
        $.toast().reset('all');



        if (jqXHR === false && textStatus === '') {
            msj = aux.msj;
        } else {

            errCode = jqXHR.status;
            errMsj = jqXHR.responseText;

            msj = '<b>';
            if (errCode === 0)
                msj += 'Error de conectividad a internet, verifica tu conexión.';
            else if (errCode === 404)
                msj += 'Error al realizar la solicitud.';
            else if (errCode === 500) {
                if (aux.msj.length > 0) {
                    msj += aux.msj;
                } else {
                    msj += 'Ocurrio un error en la comunicación con el servidor.'
                }

            } else if (errCode === 406) {
                msj += errMsj;
            } else if (errCode === 422)
                msj += 'Complete todos los datos requeridos'
            else if (textStatus === 'timeout')
                msj += 'El servidor tarda mucho en responder.';
            else if (textStatus === 'abort')
                msj += 'El servidor tarda mucho en responder.';
            else if (textStatus === 'parsererror') {
                msj += 'Error al realizar la solicitud.';
            } else
                msj += 'Error desconocido, pongase en contacto con el area tecnica.';
            msj += '</b>';
        }


        $.toast({
            heading: '<b>Error</b>',
            text: aux.msj,
            position: 'top-right',
            hideAfter: false,
            width: '400px',
            icon: 'error'
        });

        aux.msj = [];
    } else if (opt === 'success') {
        $.toast().reset('all');

        $.toast({
            heading: '<b>Exito</b>',
            text: aux.msj,
            position: 'top-right',
            hideAfter: false,
            width: '400px',
            icon: 'success'
        });
        aux.msj = [];

    }
} //






</script>
@endsection