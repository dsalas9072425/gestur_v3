@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
<link rel="stylesheet" href="{{asset('mC/css/select.dataTables.min.css')}}">
@endsection
@section('content')

<style type="text/css">
    .correcto_col {
        height: 74px;
    }

    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
     .mr-1 {
         margin-right: 5px; 
     }
</style>


<section id="base-style">

	<div class="card" style="border-radius: 14px;">
		<div class="card-header" style="border-radius: 14px;">
			<h4 class="card-title">Detalle Anticipo Proveedor</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


        <form class="row" id="formDataAnticipo">

          
            <!--{{--<div class="col-12 col-sm-4">
                <div class="form-group">
                    <label>Forma Pago</label>
                    <select class="form-control select2" name="id_tipo_operacion" id="tipo_operacion" disabled
                        style="width: 100%;">
                        @foreach ($tipo_operacion_pago as $operacion)
                            <option value="{{$operacion->id}}" data-adjunto="{{$operacion->adjunto_obligatorio}}">{{$operacion->denominacion}}</option>
                        @endforeach
                    </select>
                </div>
            </div>--}}-->
                <div class="col-6 col-sm-4">
                    <div class="form-group">
                        @if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'V')
                            <label>Venta</label>
                        @else 
                            <label>Grupo/Proforma</label>
                        @endif
                        <?php 
                                $resultado = "";
                                if ($anticipo->id_grupo != 0){
                                    $resultado .=  $anticipo->id_grupo;
                                }
                                if ($anticipo->id_proforma != 0){
                                    if($resultado != ""){
                                        $resultado .= '/';
                                    }
                                    $resultado .= $anticipo->id_proforma;
                                }    
                                if ($anticipo->id_venta != 0){
                                    if($resultado != ""){
                                        $resultado .= '/';
                                    }
                                    $resultado .= $anticipo->id_venta;
                                }                        

                        ?>
                        <input type="text" class="form-control format-number-pago" id="grupo_proforma" disabled value="{{$resultado}}">
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label>Sucursal</label>
                        <select class="form-control select2" id="id_sucursal" disabled style="width: 100%;">
                            @foreach ($sucursalEmpresa as $sucursal)
                            <option value="{{$sucursal->id}}">{{$sucursal->denominacion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label>Centro Costo</label>
                        <select class="form-control select2" id="id_centro_costo" disabled style="width: 100%;">
                            @foreach ($centro as $c)
                            <option value="{{$c->id}}">{{$c->nombre}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>

              {{-- SE LLEGA A UN ACUERDO DE NO MOSTRAR LA COTIZACION PORQUE SE GUARDA CERO (NO BORRAR ESTE COMENTARIO PORFA)--}}
                    {{-- <div class="col-6 col-sm-4">
                        <div class="form-group">
                            <label>Cambio (*)</label>
                        <input type="text" class="form-control format-number-cotizacion" name="cotizacion" id="cotizacion" data-value-type="convertNumber" value="{{$anticipo->}}">
                        </div>
                    </div> --}}

                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label>Moneda</label>
                            
                            <select class="form-control select2" id="id_moneda" disabled style="width: 100%;">
                                @foreach ($currency as $div)
                                <option value="{{$div->currency_id}}">{{$div->currency_code}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                <div class="col-6 col-sm-4">
                    <div class="form-group">
                        <label>Importe</label>
                        <input type="text" class="form-control format-number-pago" id="at_importe" disabled value="0">
                    </div>
                </div>

                <div class="col-6  col-sm-4">
                    <div class="form-group">
                       <label>Fecha Pago</label>						 
                       <div class="input-group">
                           <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                           </div>
                              <input type="text"  name="fecha_pago" disabled class="form-control single-picker" id="fecha_pago" />
                       </div>
                   </div>
               </div>

               <div class="col-6 col-sm-4">
                <div class="form-group">
                    <label>Número de OP</label>
                    <input type="text" class="form-control format-number-pago" id="id_op" disabled  value="0">
                </div>
            </div>

            <div class="col-6 col-sm-4">
                <div class="form-group">
                    <label>Número de Anticipo</label>
                    <input type="text" class="form-control format-number-pago"  id="id_anticipo" disabled  value="0">
                </div>
            </div>
            <div class="col-6 col-sm-4">
                <div class="form-group">
                    <label>Activo</label>
                        <select class="form-control select2" id="estado" disabled style="width: 100%;">
                            <option value="1">SI</option>
                            <option value="">ANULADO</option>
                        </select>
                </div>
            </div>
            <div class="col-6 col-sm-4">
                    <div class="form-group">
                        <label>Saldo</label>
                        <input type="text" class="form-control format-number-pago" id="saldo" disabled value="0">
                    </div>
                </div>

            @if($anticipo->activo == "")
                <div class="col-6 col-sm-4">
                    <div class="form-group">
                        <label>Usuario Anulacion</label>
                        <input type="text" class="form-control format-number-pago"  id="id_anticipo" disabled  value="{{$anticipo->usuarioAnulacion->nombre}} {{$anticipo->usuarioAnulacion->apellido}}">
                    </div>
                </div>
                <div class="col-6 col-sm-4">
                    <div class="form-group">
                        <label>Fecha Anulacion</label>
                        <input type="text" class="form-control format-number-pago"  id="id_anticipo" disabled  value="{{date('d/m/Y h:m:s', strtotime($anticipo->fecha_hora_anulacion))}}">
                    </div>
                </div>
            @endif           
                <div class="col-12">
                    <div class="card-header">
                        <h4 class="card-title font-weight-bold" id="heading-labels">Forma de Pago</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    </div>
                </div>
           


                <div class="col-12">
                        <div class="table-responsive">
                            <table id="listadoPagos" class="table" style="width:100%">
                                <thead>
                                    <tr style="text-align: center">
                                        <th>Operación</th>
                                        <th>Importe Pago</th>
                                        <th>Moneda</th>
                                        <th>Documento</th>
                                        <th>Comprobante</th>
                                        <th>Cuenta</th>
                                        <th>Emision</th>
                                        <th>Vencimiento</th>
                                    </tr>
                                </thead>

                                <tbody style="text-align: center" class="tbody_fp">
                                    @foreach ($forma_pago as $forma_p)
                                        <tr>
                                            <td>{{$forma_p->fp_detalle[0]->forma_pago->denominacion}}</td>
                                            <td>{{$forma_p->fp_detalle[0]->importe_pago}}</td>
                                            <td>{{$forma_p->moneda->currency_code}}</td>
                                            <td>{{$forma_p->fp_detalle[0]->documento}}</td>
                                            <td>
                                                <?php if($forma_p->fp_detalle[0]->nro_comprobante !== 'null'){
                                                    echo $forma_p->fp_detalle[0]->nro_comprobante;
                                                 }else{
                                                    echo '';
                                                 }?>
                                            </td>
                                            <td>{{$forma_p->fp_detalle[0]->banco_detalle->banco_cabecera->nombre}}</td>
                                            <td>{{$forma_p->fp_detalle[0]->fecha_emision_format}}</td>
                                            <td>{{$forma_p->fp_detalle[0]->fecha_vencimiento_format}}</td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                </div>  


               

                <div class="col-12">
                        <div class="form-group">
                            <label>Beneficiario</label>
                            <select class="form-control select2" name="id_beneficiario:number" disabled id="id_beneficiario" style="width: 100%;">
                                @foreach ($beneficiario as $b)
                                <option value="{{$b->id}}">{{$b->full_name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Contable</label>
                            <select class="form-control select2" name="id_cuenta_contable:number" disabled id="id_cuenta_contable" style="width: 100%;">
                                    

                                    @foreach ($cuentas_contables as $cuenta)

                                    @if($cuenta->asentable)
                                    <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif   
                                        
                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                        </option>
                                    @if($cuenta->asentable)
                                    </optgroup>
                                    @endif  
                                    

                                    @endforeach

                                
                             </select>
                        </div>
                    </div>

                    <div class="col-12">
                            <div class="form-group">
                                <label>Concepto</label>
                                <input type="text" class="form-control text-bold" disabled id="concepto" name="concepto" value="">
                            </div>
                        </div>
             
              <div class="col-12">
                    <button type="button" class="mr-1 btn btn-success btn-lg pull-right" id="btnVolver"><b>Volver</b></button> 
                    @if($anticipo->activo != "")    
                        <button type="button" class="mr-1 btn btn-danger btn-lg pull-right btnAnularAnticipo" id="btnAnularAnticipo"><b>Anular</b></button> 
                    @endif
                    @foreach ($btn as $boton )
                    <?php echo $boton; ?>
                        
                    @endforeach
              </div>          



        </form>


    </div>
</div>
</div>
</section>
   




	  <!-- {{--============================================================================================
											MODAL ANULAR ANTICIPO
		 ============================================================================================ --}}-->

		 <div class="modal fade" id="modalAnularAnticipo" role="dialog">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="closeModal"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <h4 class="modal-title"><i class="fa fa-fw fa-remove"></i> Anular OP</h4>
					</div>
					<div class="modal-body">


                     <div class="row">
                         <div class="col-12">
                                <h3>
                                        ¿ Esta seguro que desea anular este anticipo ?    
                                    </h3>    
                         </div>
                           
                        
            
                    </div>   



					</div>
					<div class="modal-footer">
                      <button type="button" class="btn btn-success btn-lg" id="btnCancelarAnulacion"><b>Cancelar</b></button>
                      <button type="button" class="btn btn-danger btn-lg" id="btnAnularConfirmacion"><b>Si, Anular</b></button>
					</div>
				  </div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
              
            <input type="hidden" value="{{$anticipo->id}}" id="id_anticipo">
<!--======================================================
        MODAL DE FACTURA
========================================================== -->
    <!-- /.content -->
    <div id="modalFacturas" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 100%;margin-top: 5%;margin-left: 4%;">
        <!-- Modal content-->
            <div class="modal-content" style="width: 160%;">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Aplicar a factura</h2>
					<button type="button" class="closeModal"  data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <div class="modal-body" style="padding-top: 0px;">
					<form id="aplicarFactura">
						<div class="table-responsive table-bordered font-weight-bold">
							<table id="listado" class="table" style="width: 100%;font-size: small;">
								<thead>
									<tr>
										<th style="width: 15%">Factura</th>
										<th>Fecha</th>
										<th>Monto</th>
										<th>Moneda</th>
										<th>Saldo</th>
										<th>Proveedor</th>
										<th>Tipo Factura</th>
                                        <th>Cod.<br>Confirmación</th>
                                        <th></th>
									</tr>
								</thead>
								<tbody style="text-align: center">

								</tbody>
							</table>
						</div>
						<div class="row">
                            <input type="hidden" class = "form-control" name="id_libro" id="id_libro" style="font-weight: bold;font-size: 15px;"/>
							<input type="hidden" class = "form-control" name="tipo" id="tipo" value="P" style="font-weight: bold;font-size: 15px;"/>
							<input type="hidden" class = "form-control" name="id_facturas" id="id_facturas" value="0" style="font-weight: bold;font-size: 15px;"/>
							<input type="text" class = "form-control" name="anticipo_id" id="anticipo_id" value="{{$anticipo->id}}" style="font-weight: bold;font-size: 15px;display:none"/>
							<div class="col-12 col-sm-4 col-md-3"></div>
							<div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
									<label>Saldo Factura</label>
									<input type="text" class = "form-control numeric" name="saldo_factura" value="" id="saldo_factura" style="font-weight: bold;font-size: 15px;" disabled="disabled"/>
								</div>
							</div>	
							<div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
									<label>Saldo Anticipo</label>
									<input type="text" class = "form-control numeric" name="saldo_credito" value="{{$anticipo->saldo}}" id="saldo_credito" style="font-weight: bold;font-size: 15px;"/>
								</div>
							</div>	
							<div class="col-12 col-sm-4 col-md-3">
								<br>
								<button  onclick ="guardarAnticipo()" class="pull-right btn btn-info btn-lg mr-1" type="button"><b>Guardar</b></button>
							</div>
						</div>	
					</form>	
                </div>
            </div>  
        </div>  
    </div>  

@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>

<script type="text/javascript">
    $(() => {
        main();
    });


    function main() {
        loadAnticipo();
        formatNumber();
        $('.select2').select2();
    } //

 
   

    $('#btnVolver').click(() => {
        redirectView();
    });

    $('.btnAnularAnticipo').click(()=>{
        modalAnular();
    })

    $('#btnAnularConfirmacion').click(()=>{
        anularAnticipo();
    });

    $('#btnCancelarAnulacion').click(()=>{
        ocultarModalAnular();
    });

    function anularAnticipo(){

        $('#btnAnularConfirmacion').prop('disabled',true);

        $.ajax({
          type: "GET",
          url: "{{route('anularAnticipo')}}",
          data:{ id_anticipo: $('#id_anticipo').val()} ,
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){

                    $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

		      	$('#btnAnularConfirmacion').prop('disabled',false);
		  },
          success: function(rsp){
          		if(rsp.err === true){
                            redirectView();
          		} else {

                    $.toast({
                            heading: 'Error',
                            text: rsp.msj,
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        
                        $('#btnAnularConfirmacion').prop('disabled',false);
          		
          		}
            
                          }//success
                 });//AJAX


    }

    function redirectView(){

        ocultarModalAnular();
        $.blockUI({
						centerY: 0,
						message: "<h2>Redirigiendo a vista de Anticipo...</h2>",
						css: {
							color: '#000'
						}
					});

                    location.href ="{{ route('indexAnticipo') }}";
    }

  


    function formatNumber() {

        $('.format-number-control').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false
        });

    }




function loadAnticipo(){
    console.log('{{$anticipo->activo}}');
    $("#id_sucursal").val(validarData('{{$anticipo->id_sucursal}}')).trigger('change.select2');
    $('#id_op').val('{{$anticipo->op->nro_op}}');
    $('#id_anticipo').val('{{$anticipo->id}}');
    $('#fecha_pago').val(formatoFecha('{{$anticipo->fecha_pago}}'));
    $('#fecha_vencimiento').val(formatoFecha('{{$anticipo->fecha_vencimiento}}'));
    $('#id_moneda').val(validarData('{{$anticipo->id_moneda}}'));
    $('#nro_cheque').val(validarData('{{$anticipo->documento}}'));
    $('#id_centro_costo').val(validarData('{{$anticipo->id_centro_costo}}'));
    $('#at_cotizacion').val('{{$anticipo->cotizacion}}');
    $('#at_importe').val(validarData('{{$anticipo->importe}}'));
    $('#id_beneficiario').val('{{$anticipo->id_beneficiario}}');
    $('#saldo').val('{{$anticipo->saldo}}');
    $('#id_cuenta_contable').val('{{$anticipo->id_cuenta_contable}}');
    $('#concepto').val('{{$anticipo->concepto}}');
    $('#estado').val(validarData('{{$anticipo->activo}}')).trigger('change.select2');
    dato = "{{$anticipo->saldo}}";
	estado = "{{$anticipo->id_estado}}";
    if(dato.includes("e-") == true){
        dato = 0;
    }
	if(dato == 0 || parseInt(estado)== 65){
		$('.aplicarAtc').css('display', 'none');
	};
    $('#at_saldo').val('{{$anticipo->saldo}}');

}

function modalAnular(){

    $('#modalAnularAnticipo').modal('show');
}
function ocultarModalAnular(){
    $('#modalAnularAnticipo').modal('hide');
}



   function validarData(data){
    //    console.log(data);
    if(data == 'null'){
        return '';
    }
    return data;
   }

        function formatoFecha(texto){
            if(texto != '' && texto != null && texto != '0'){
                return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
            } else {
                return '';
            }
        }

        const formatter = new Intl.NumberFormat('de-DE', {
                                                            currency: 'USD',
                                                            minimumFractionDigits: 2
                                                        });

        $('#listado').dataTable();

        $('.aplicarAtc').on('click',function(){ 	
		    $.ajax({
                type: "GET",
                url: "{{route('getFacturasAplicarProveedor')}}",
				data: {
						datamoneda: $("#id_moneda").val(),
						idProveedor: "{{$anticipo->id_beneficiario}}",
                        baseC: "{{$anticipo->cotizacion}}"
					},
                dataType: 'json',
                error: function(jqXHR,textStatus,errorThrown){},
                success: function(rsp){
										console.log(rsp);
										var oSettings = $('#listado').dataTable().fnSettings();
										var iTotalRecords = oSettings.fnRecordsTotal();

										for (i=0;i<=iTotalRecords;i++) {
											$('#listado').dataTable().fnDeleteRow(0,null,true);
										}

										$.each(rsp, function (key, item){
											accion = '<button type="button" id="btnAplicar" class="btn btn-danger" onclick="agregarAplicar('+item.saldo_cotizado+','+item.id+')" style="width: 40px;background-color: #e2076a;padding-right: 5px;padding-left: 5px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-plus fa-lg"></i></button>';
											var dataTableRow = [
															item.nro_documento,
															item.fecha_format,
                                                            (!isNaN(parseFloat(item.importe))) ? formatter.format(parseFloat(item.importe)) : 0,
															item.currency_code,
                                                            formatter.format(parseFloat(item.saldo)),
															item.proveedor_nombre,
															item.denominacion,
                                                            item.cod_confirmacion,
                                                            accion
															];

										var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
										var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
										})	
									}	
			});	 
			$("#modalFacturas").modal("show");
	});	

    function agregarAplicar(saldo,id){
        $("#id_libro").val(id);
        $("#saldo_factura").val(saldo);
    }

    function guardarAnticipo(){ 
		if(jQuery.isEmptyObject($('#saldo_factura').val()) == false){
            console.log(parseFloat(clean_num($('#saldo_factura').val())));
            console.log(parseFloat(clean_num($('#saldo_credito').val())));
		 	if(parseFloat(clean_num($('#saldo_credito').val())) <= parseFloat(clean_num($('#saldo_factura').val()))){
				$('#saldo_factura').prop('disabled',false);
				var dataString = $('#aplicarFactura').serialize();	
				$.ajax({
						type: "GET",
						url: "{{route('guardarAnticipoFactura')}}",
						dataType: 'json',
						data: dataString,
						error: function(jqXHR,textStatus,errorThrown){},
						success: function(rsp){
							if(rsp =="OK"){
								$("#modalFacturas").modal('hide');
								$.toast({ 
										heading: 'Exito',
										text: 'Se han aplicado el anticipo a la/s factura/s.',
										position: 'top-right',
										showHideTransition: 'slide',
										icon: 'success'
									});  
							 	location.reload();	
							}else{
								$('#saldo_factura').prop('disabled',true);
								$.toast({
									heading: 'Error',
									text: rsp,
									showHideTransition: 'fade',
									position: 'top-right',
									icon: 'error'
								});				
							}	
						}	
					});
			 }else{	
				$.toast({
						heading: 'Error',
						text: 'El monto a aplicar del anticipo tiene que ser mayor o igual al saldo de la factura',
						showHideTransition: 'fade',
						position: 'top-right',
						icon: 'error'
					});				
			} 		
		}else{	
			$.toast({
					heading: 'Error',
					text: 'Seleccione una factura',
					showHideTransition: 'fade',
					position: 'top-right',
					icon: 'error'
				});				
		}
	}

	function clean_num(n,bd=false){
			if(n && bd == false){ 
			n = n.replace(/[,.]/g,function (m) {  
					 				 if(m === '.'){
					 				 	return '';
					 				 } 
					 				  if(m === ','){
					 				 	return '.';
					 				 } 
					 			});
			return Number(n);
		}
		if(bd){
			return Number(n);
		}
		return 0;
		}

        $('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});



</script>
@endsection