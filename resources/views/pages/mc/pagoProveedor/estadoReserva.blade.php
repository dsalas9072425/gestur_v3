
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
	

	.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

@-moz-document url-prefix() {
  fieldset { display: table-cell; }
}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	.select2-container *:focus {
        outline: none;
    }

	 input.form-control:focus ,.select2-container--focus , button:focus{
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

	.input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
	




</style>

	@parent
	  <link rel="stylesheet" type="text/css" href="{{asset('gestion/app-assets/vendors/css/tables/datatable/select.dataTables.min.css')}}">
@endsection
@section('content')



<section id="base-style">

	<div class="card" style="border-radius: 14px;">
		<div class="card-header" style="border-radius: 14px;">
			<h4 class="card-title">Estado Reserva</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				 	<ul class="nav nav-tabs nav-underline" role="tablist">
						<!--<li class="nav-item">
			                <a class="nav-link active" id="baseIcon-tab20" data-toggle="tab" aria-controls="tabIcon20" href="#cierreCaja" role="tab" aria-selected="true"><b><i class="fa fa-play"></i>Proveedor</b></a>
			            </li>			            
			            <li class="nav-item">
			                <a class="nav-link" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#cabecera" role="tab" aria-selected="false">
			                	<b><i class="fa fa-fw fa-gear"></i> Confirmar Pago</b></a>
			            </li>-->
			        </ul>    
					<div class="tab-content px-1 pt-1">
			            <div class="tab-pane active" id="cierreCaja" role="tabpanel" aria-labelledby="baseIcon-tab20">
			            	<form id="consultaFactura"  autocomplete="off">
							<div class="row mb-1">
								<div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Proveedor</label>
										<select class="form-control select2" name="idProveedor" id="proveedor"
											tabindex="1" style="width: 100%;">
											<option value="">Todos</option>
											@foreach ($getProveedor as $pro)
												@if($pro->activo == true)
													@php
													$activo = 'ACTIVO';
													@endphp
												@else
													@php
													$activo = 'INACTIVO';
													@endphp
												@endif
												@php
													$ruc = $pro->documento_identidad;
													if($pro->dv){
														$ruc = $ruc."-".$pro->dv;
													}
												@endphp

												<option value="{{$pro->id}}">{{$ruc}}  - {{$pro->nombre}} <b>({{$activo}})</b></option>

											@endforeach
										</select>
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Moneda</label>
										<select class="form-control select2" name="idMoneda" id="idMoneda" tabindex="2"
											style="width: 100%;">
											<option value="">Todos</option>
											@foreach ($getDivisa as $div)
											<option value="{{$div->currency_id}}">{{$div->currency_code}}</option>
											@endforeach
										</select>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Proforma</label>
										<input type="number" class="form-control" id="idProforma" maxlength="15" tabindex="3"
											name="idProforma" value="">
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Nro. Factura</label>
										<input type="text" class="form-control" id="nroFactura" maxlength="25" tabindex="4" maxlength="20"
											name="nroFactura" value="">
									</div>
								</div>

							</div>
							<div class="row">

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Codigo</label>
										<input type="text" class="form-control" maxlength="40"  id="codigoConfirmacion" tabindex="5"
											name="codigoConfirmacion" value="">
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Checkin Desde - Hasta</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" maxlength="10"  class="form-control pull-right fecha calendar"
												tabindex="6" name="checkin_desde_hasta:p_date" id="checkin_desde_hasta"
												value="">
										</div>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Fecha Proveedor Desde - Hasta</label>
										<div class="input-group">
											<div class="input-group-prepend" style="">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" maxlength="10" class="form-control pull-right fecha calendar"
												tabindex="7" name="proveedor_desde_hasta:p_date"
												id="proveedor_desde_hasta" value="">
										</div>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Pagado </label>
										<select class="form-control select2" name="tieneFechaProveedor" tabindex="8"
											id="tieneFechaProveedor" style="width: 100%;">
											<option value="">Todos</option>
											<option value="SI">SI</option>
											<option selected="selected" value="NO">NO</option>


										</select>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3 ">
									<div class="form-group">
										<label>Pendiente </label>
										<select class="form-control select2" name="pendiente" tabindex="9"
											id="filtro_pendiente" style="width: 100%;">
											<option value="">Todos</option>
											<option value="true">SI</option>
											<option selected="selected" value="false">NO</option>


										</select>
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Fecha Gasto Desde - Hasta</label>
										<div class="input-group">
											<div class="input-group-prepend" style="">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" maxlength="10" class="form-control pull-right fecha calendar"
												tabindex="7" name="gasto_desde_hasta:p_date" id="gasto_desde_hasta" value="">
										</div>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Total Saldo</label>
										<input type="text" class="form-control numeric" maxlength="40"  id="totalSaldo" tabindex="5" name="totalSaldo" value="0">
									</div>
								</div>


							</div>

							<input type="hidden" id="items_selecionados" value="">
							<input type="hidden" id="total_pagar_proveedor" value="">
							<input type="hidden" id="saldo_total" value="">


							<div class="col-12">
								<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
								<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
								<button onclick="loadDatatable()" tabindex="10" class="pull-right text-center btn btn-info btn-lg mr-1" type="button"><b>Buscar</b></button>
							</div>
						</form>
						<div class="table-responsive">
							<table id="listado" class="table table-hover table-condensed nowrap" style="width: 100%;">
								<thead>
									<tr style="text-align: center">
										<th>Provedor</th>
										<th>Fecha Proveedor</th>
										<th>Fecha Gasto</th>
										<th>Factura/Documento</th>
										<th>Checkin</th>
										<th>Tipo</th>
										<th>Pagado</th>
										<th>Pasajero</th>
										<th>Moneda</th>
										<th>Saldo</th>
										<th>Código</th>
										<th>Producto</th>
										<th>Total</th>
										<th>Proforma</th>
										<th>Facturación</th>
										<th>Vendedor</th>
										<th>Origen</th>
									</tr>
								</thead>
								<tbody style="text-align: center">
								</tbody>
						</table>
						</div>

			            </div>
			            <div class="tab-pane" id="cabecera" role="tabpanel" aria-labelledby="baseIcon-tab21">
			            			
					<div class="row">
						<div class="col-12 col-sm-3 ">
							<div class="form-group">
								<label>Sucursal</label>
								<select class="form-control select2" name="cf_sucursal" id="cf_sucursal" style="width: 100%;">
									@foreach ($sucursalEmpresa as $suc)
									<option value="{{$suc->id}}">{{$suc->denominacion}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-3 ">
							<div class="form-group">
								<label>Centro de Costo</label>
								<select class="form-control select2" name="cf_centro_costo" id="cf_centro_costo" style="width: 100%;">
									@foreach ($centro as $suc)
									<option value="{{$suc->id}}">{{$suc->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-3">
							<div class="form-group">
								<label>Operación Moneda</label>
								<input type="text" class="form-control" id="cf_currency_code" disabled name="" value="">
							</div>
						</div>

						<div class="col-12 col-sm-3">
							<div class="form-group">
								<label>Cotización </label>
								<input type="text" class="form-control control_space formatInput" readonly id="cf_cotizacion" name=""
									value="0">
							</div>
						</div>
					</div>

					<div class="col-12">
						<h4>Totales</h4>
						<hr style="margin-top: 1px;">

					</div>


					<!--======================================================
											MONTO COTIZADO
						====================================================== --}}-->

					<form class="row" id="formTotales" autocomplete="off">

						<div class="col-4">
							<div class="row no-gutters">

								<div class="col-12">
									<small><b>Montos cotizados <span class="bg-blue badge">USD</span></b> </small><i
										class="fa fa-fw fa-arrow-down"></i>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Total Factura</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-plus"></i></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly
												id="cotizado_total_factura" value="0">
										</div>

									</div>
								</div>


								<div class="col-12">
									<div class="form-group">
										<label>Anticipos Realizados</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cotizado_anticipo" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Nota Credito</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cotizado_nc" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Ret IVA 10%</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cotizado_retencion" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Neto a Pagar</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text font-weight-boldt">=</span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly value="0"
												id="cotizado_pago_neto">
										</div>
									</div>
								</div>

							</div>
						</div>



						<!--======================================================
													MONTO COSTO PROVEEDOR
							====================================================== -->

						<div class="col-4">
							<div class="row no-gutters">

								<div class="col-12">
									<small><b>Montos Compra <span class="bg-blue badge  cf_currency_code"></span></b>
									</small><i class="fa fa-fw fa-arrow-down"></i>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Total Factura</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-plus"></i></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly
												id="cf_total_pago" value="0">
										</div>

									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Anticipos Realizados</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cf_anticipo" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Nota Credito</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cf_nota_credito" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Ret IVA 10%</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cf_retencion" class="form-control clear_input_txt formatInput"
												readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Neto a Pagar</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><b>=</b></b></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly value="0"
												id="cf_neto_pago">
										</div>
									</div>
								</div>

							</div>
						</div>


					</form>


					<div class="row">
						<div class="col-12">
							<hr>
						</div>

						<div class="col-12">
							<div class="form-group">
								<label>Beneficiario : </label>
									<input type="text" readonly class="form-control clear_input_txt" id="cf_beneficiario">
							</div>
						</div>

						<div class="col-12 mt-2">
							<button type="button" class="btn btn-success btn-lg pull-right" onclick="sendData()"
								id="btnProcesarOP"><b>Crear OP</b></button>
							<button type="button" class="btn btn-danger btn-lg pull-right" style="margin-right: 10px;"
								onclick="cancelarOp()"><b>Cancelar Operación</b></button>
						</div>
					</div>
			        	</div> 			
			        </div> 

				</div>



			</div>
		</div>
	</div>
</section>




{{-- ========================================
   			MODAL NUMERO DE OP
   	========================================  --}}		

	   <div class="modal fade" id="modalNumOp" aria-labelledby="myModalLabel">
	   	<div class="modal-dialog">
	   		<div class="modal-content">
	   			<div class="modal-header">
	   				<button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
	   					<span aria-hidden="true">&times;</span>
	   				</button>
	   				<h4 class="modal-title"  style="font-weight: 800;">
	   					<i class="fa fa-fw fa-send-o"></i>
	   					NUMERO DE OP
	   				</h4>
	   			</div>
	   			<div class="modal-body">
	   				<div class="content-fluid">
	   					<div class="row">
							   <div class="col-12">
										<h1 style="text-align:center">SE GENERO LA OP NUMERO : <span id="num_op">01</span></h1>
							   </div>
							   <div class="col-12 text-center mt-1">
									<button type="button" class="btn btn-success btn-lg"
									id="confirmarOpModal" data-dismiss="modal"
									style="margin:auto"><b>Aceptar</b></button>
							   </div>

	   					</div>
	   				</div>
	   			</div>
	   		</div>
	   	</div>
	   </div>


    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.min.js"></script>

	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script> --}}
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script src="{{asset('gestion/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')}}"></script>
	<script src="//cdn.datatables.net/plug-ins/1.10.12/sorting/datetime-moment.js"></script>
	
<script type="text/javascript">




initConfig();
// debugDesarrollo();

$(document).ready(function(){
$('.select2').select2();
calendar();

controlNumFormat();
});
	

	function debugDesarrollo(){
		$('.box-header .subtitle').html('Pago Proveedor Modo DEBUG');
		$('#tabConfirm').prop('disabled',false);
 		$('#btnProcesarOP').prop('disabled',false);
 		$('#tabConfirm').tab('show');
	}


	{{--==============================================
			FILTROS BUSQUEDA
	============================================== --}}
	function calendar(){
		{{--CALENDARIO OPCIONES EN ESPAÑOL --}}
		let locale = {
					format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};

				$('.calendar').daterangepicker({timePicker24Hour: true,
												timePickerIncrement: 30,
												autoUpdateInput: false,
												locale: locale
															});
				$('#proveedor_desde_hasta').val('');
				$('#proveedor_desde_hasta')
				.on('cancel.daterangepicker', function(ev, picker) {
							$(this).val('');})
				.on('apply.daterangepicker', function(ev, picker) {
							$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
				});		

				$('#checkin_desde_hasta').val('');
				$('#checkin_desde_hasta')
				.on('cancel.daterangepicker', function(ev, picker) {
							$(this).val('');})
				.on('apply.daterangepicker', function(ev, picker) {
							$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
				});	

				$('.single-picker').datepicker({ 
							format: "dd/mm/yyyy", 
							language: "es"
							});
				
				var d = new Date();
				mes = (d.getMonth()+1)
				if(mes<10){
					mes = "0"+mes
				}
				var strDate =  d.getDate() + "/" + mes + "/" +d.getFullYear();
			     console.log(strDate);
				
		 		fecha_gasto = sumaFecha(7,strDate);
				console.log(fecha_gasto);
				
				$('#gasto_desde_hasta').val(strDate+' - '+fecha_gasto);
				$('#gasto_desde_hasta')
				.on('cancel.daterangepicker', function(ev, picker) {
							$(this).val('');})
				.on('apply.daterangepicker', function(ev, picker) {
							$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
				});		


	 }


	 sumaFecha = function(d, fecha)
			{
			var Fecha = new Date();
			var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
			var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
			var aFecha = sFecha.split(sep);
			var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
			fecha= new Date(fecha);
			fecha.setDate(fecha.getDate()+parseInt(d));
			var anno=fecha.getFullYear();
			var mes= fecha.getMonth()+1;
			var dia= fecha.getDate();
			mes = (mes < 10) ? ("0" + mes) : mes;
			dia = (dia < 10) ? ("0" + dia) : dia;
			var fechaFinal = dia+sep+mes+sep+anno;
			return (fechaFinal);
			}



	/*{{--============================================================================================
							DATATABLE Y LOGICA SELECCION PROVEEDOR
	============================================================================================ --}}*/

	function limpiar()
	{
		$('#proveedor').val('').trigger('change.select2');
		$('#idMoneda').val('').trigger('change.select2');
		$('#idProforma').val('');
		$('#nroFactura').val('');
		$('#codigoConfirmacion').val('');
		$('#checkin_desde_hasta').val('');
		$('#proveedor_desde_hasta').val('');
		$('#tieneFechaProveedor').val('NO').trigger('change.select2');
		$('#filtro_pendiente').val('false').trigger('change.select2');
	}

		 var table;	
		function loadDatatable(){
			$("#totalFinal").val(0);
			$("#totalSaldo").val(0);
			let form = $('#consultaFactura').serializeJSON({
				customTypes: customTypesSerializeJSON
			});
			let input;
			$.fn.dataTable.moment( 'M/D/YYYY' );			 
			table = $("#listado").DataTable({
			 	destroy: true,
			 	keys: true,
			 	ajax: {
			 		url: "{{route('getListProveedor')}}",
			 		data: form,
			 		error: function (jqXHR, textStatus, errorThrown) {
			 			msjToast('', 2, jqXHR.status, textStatus);
			 			$.unblockUI();
			 		}

			 	},
			 	"order": [[ 3, "desc" ]],
			 	"columns": [
			 		{
			 			data: function (x) {
			 				return `<span class="proveedor_nombre">${x.proveedor_n}</span>`;
			 			}
			 		},
			 		{
			 			data: function (x) {
			 				return `<b>${x.fecha_proveedor_format}</b>`;
			 			}
			 		},
					 {
			 			data: function (x) {
							if(x.fecha_de_gasto !== null){
								fecha = x.fecha_de_gasto.split('-');
								fecha_gasto = fecha[2]+"/"+fecha[1]+"/"+fecha[0];
							}else{
								fecha_gasto = x.fecha_de_gasto;
			 				}
							return fecha_gasto;
			 			}
			 		},
					{
			 			data: 'nro_factura'
			 		},
    				{data: "fecha_in_format", "title": "Check In", "render": function(data, type) {
					 	//console.log(moment(data).format('L'));
			                return type === 'sort' ? data : data;
			        	}
					},
					{
						 data: 'tipo_doc_n'
					 },
					 {
			 			data: function (x) {
			 				return `<b>${x.pagado_proveedor_txt}</b>`;
			 			}
			 		},
			 		{
			 			data: 'pasajero_n'
			 		},
			 		{
			 			data: function (x) {
			 				return `<span class="currency_code">${x.currency_code}</span> 
							        	`;
			 			}
			 		},
			 		{
			 			"data": function (x) {
			 				let saldo = x.saldo;
			 				if (saldo === null || saldo === undefined) {
			 					saldo = 0;
			 				}
			 				var input = `
					           		 	<input type="hidden" value="${saldo}" class="saldo_default">
										<input type="text" value="${saldo}" data-mask="false" disabled  style="text-align:center;" class="saldo numeric">`;
			 				return input;
			 			}
			 		},
			 		{
			 			data: 'cod_confirmacion'
			 		},
					 {
			 			data: 'denominacion_producto'
			 		},
			 		{
			 			data: function (x) {
			 				var input = `<input type="text" disabled style="text-align:center;" data-mask="false" class="input-style numeric" value="${x.importe}">`;
			 				return input;
			 			}
			 		},
			 		{
			 			data: function (x) {
			 				if (Boolean(x.id_proforma)) {
			 					return `<a href="{{route('detallesProforma',['id'=>''])}}/${x.id_proforma}"class="bgRed">
						        	<i class="fa fa-fw fa-search"></i>${x.id_proforma}</a>
						        	<div style="display:none;">${x.id_proforma}</div>`;
			 				} else {
			 					return '';
			 				}

			 			}
			 		},
	
			 		{
			 			data: 'fecha_facturacion_format'
			 		},
			 		{
			 			data: 'vendedor_n'
			 		},
			 		{
			 			data: 'origen'
			 		}

			 	],

			 	"select": {
			 		style: 'multi',
			 		selector: 'td:first-child'
			 	},
			 	"columnDefs": [{
			 			orderable: false,
			 			targets: 0
			 		},
			 		{
			 			targets: 0,
			 			className: 'style_check'
			 		},
			 		{
			 			targets: 9,
			 			className: 'col_input_saldo'
			 		},
			 		{
			 			targets: 10,
			 			className: 'col_input_pago_proveedor'
			 		}

			 	],
			 	// "aaSorting":[[2,"desc"]],
			 	"createdRow": function (row, data, iDataIndex) {
			 		if (data.pendiente == true) {
			 			$(row).css('background-color', '#EFF3BA');
			 		}

			 		$(row).attr('id', iDataIndex + '_fila');
			 		//ASIGNAR ID Y FUNCION
			 		$(row).find('td.col_input_pago_proveedor input').attr('id', iDataIndex + '_input');
			 		$(row).find('td.col_input_pago_proveedor').attr('onkeyup', 'validateKeyPressSaldo("' + iDataIndex + '","' + iDataIndex + '_input")');
			 		if ($(row).find('td .activeCheck').val() == 'true') {
			 			$($(row).find('td.select-checkbox').get(0)).removeClass('select-checkbox')
			 		}
			 		$(row).find('td:first-child').attr('id', iDataIndex + '_select');
			 		/*console.log('--------------------------------------');
			 		console.log(data);
			 		console.log('--------------------------------------');*/
			 		sumarTotales(data.saldo, data.id_tipo_documento);

			 	},
			 	//AL FINALIZAR LA CARGA AGREGAR CONTROL DE COMAS Y PUNTO
			 	"initComplete": function (settings, json) {
			 		$.unblockUI();
			 		totalSaldo()
			 	}
			 });
					table
						.on( 'select', function ( e, dt, type, indexes ) {
							let saldo_default = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo_default').val();
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').val(saldo_default);
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo').val(0);
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').prop('disabled',false);
							reCalcular();
						} )
						.on( 'deselect', function ( e, dt, type, indexes ) {
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').prop('disabled',true);
							/*{{--COLOCAR EL SALDO POR DEFECTO --}}*/
							let saldo_default = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo_default').val()
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').val(0);
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo').val(saldo_default);
							$('#listado').find('tbody tr#'+indexes+'_fila').css('background-color','');
							reCalcular();
						} )
						.on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
							/*{{--EVITAR EL SELECT SOBRE EL ELEMENTO YA PAGADO --}}*/
								if($(originalEvent.currentTarget).find('input.activeCheck').val() == 'true'){
								e.preventDefault();
								}
						}).on('draw', function () {
								/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

								$('#listado tbody tr .numeric[data-mask = false]').inputmask("numeric", {
									radixPoint: ",",
									groupSeparator: ".",
									digits: 2,
									autoGroup: true,
									// prefix: '$', //No Space, this will truncate the first character
									rightAlign: false
								});

								$('#listado tbody tr .numeric[data-mask = false]').attr('data-mask','true');

								$('#listado tbody tr .numeric[data-mask = true]').keyup(function (){
																	//this.value = (this.value + '').replace(/[^0-9]/g, '');
																	this.value = (this.value + '').replace(/(\.|\s)|(\,)/g,(m,p1,p2) => p1 ? "" : ".")
									});
						});
						
						
						
		}//function			
		
		/*$('.pago_saldo').change(function(){
			alert($(this).val());
			sumatoria();
		});	*/
		sumaFecha = function(d, fecha)
			{
			var Fecha = new Date();
			var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
			var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
			var aFecha = sFecha.split(sep);
			var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
			fecha= new Date(fecha);
			fecha.setDate(fecha.getDate()+parseInt(d));
			var anno=fecha.getFullYear();
			var mes= fecha.getMonth()+1;
			var dia= fecha.getDate();
			mes = (mes < 10) ? ("0" + mes) : mes;
			dia = (dia < 10) ? ("0" + dia) : dia;
			var fechaFinal = dia+sep+mes+sep+anno;
			return (fechaFinal);
			}

		function totalSaldo(){
			/*var importe_total = 0
			$(".saldo").each(
			    function(index, value) {
			      //if ( $.isNumeric($(this).val())){
			      importe_total = importe_total + parseFloat(clean_num($(this).val()));
			      console.log(importe_total);
			     // }
			    }
			);
      		$("#totalSaldo").val(importe_total);*/
		}

		function reCalcular(){
			//alert('Entro');
			var importe_total = 0
			$(".pago_saldo").each(
			    function(index, value) {
			      //if ( $.isNumeric($(this).val())){
			      importe_total = importe_total + parseFloat(clean_num($(this).val()));
			      console.log(importe_total);
			     // }
			    }
			);
      		$("#totalFinal").val(importe_total);
		}


		function resta(monto){
			var sum=0;
			montoSuma = parseFloat(monto);
			montoTotal = parseFloat(clean_num($('#totalFinal').val()));
			console.log('Monto '+montoSuma);
			console.log('Total '+montoTotal);
			sum = montoTotal - montoSuma;
			$('#totalFinal').val(sum);	
		}		


		function sumatoria(monto){
			var sum=0;
			montoSuma = parseFloat(monto);
			montoTotal = parseFloat(clean_num($('#totalFinal').val()));
			console.log('Monto '+montoSuma);
			console.log('Total '+montoTotal);
			sum = montoSuma + montoTotal;
			$('#totalFinal').val(sum);		 
		}		

			


function validateValues(items) {

	let id_moneda;
	let id_proveedor;
	let saldo_default;
	let saldo;
	let pago_saldo;
	let id_libro_compra = null;
	let id_anticipo = null;
	let old_proveedor;
	let old_moneda;
	let neto_pago = 0;
	let total_pago = 0;
	let total_nc = 0;
	let total_anticipo = 0;
	let data = [];
	let flag_1 = 0; //QUE SEA MISMO PROVEEDOR Y MISMA MONEDA
	let flag_2 = 0; //QUE SALDO NO SEA NEGATIVO Y PAGO NO SEA CERO 
	let flag_3 = 0; //QUE SEA UN TIPO DE DOCUMENTO VALIDO
	let flag_4 = 0; //VALIDAR QUE LA MONEDA NO SEA EURO 
	let id_tipo_documento,
		iva10 = 0,
		gravada10 = 0,
		data_gravada = 0;


	/*RECORRER LOS ITEMS OBTENIDOS*/
	$.each(items, function (index, val) {
		console.log($(val));
		//RECORRER Y RECUPERAR INPUTS
		id_tipo_documento 	= Number($(val).find('.id_tipo_documento').val());
		id_moneda 			= Number($(val).find('.currency_id').val());
		id_proveedor 		= Number($(val).find('.id_proveedor').val());
		id_documento 		= Number($(val).find('.id_documento').val());
		saldo 				= clean_num($(val).find('.saldo').val());
		pago_saldo 			= clean_num($(val).find('.pago_saldo').val());

		//CALCULAR RETENCION DE LOS ITEMS QUE NO GENERARON
		console.log(Boolean($(val).find('.genero_retencion').val()));
		if($(val).find('.genero_retencion').val() == 'false'){
			// iva10 				+= Number($(val).find('.iva10').val());
			data_gravada 			= Number($(val).find('.gravada10').val());
			if(!isNaN(data_gravada)){
				gravada10 += Number($(val).find('.gravada10').val());
			} 
		}
		
		
		// console.log(gravada10);
		// debugger;

		/*{{--REGLAS DE VALIDACION DE ITEMS --}}*/
		if (index === 0) {
			old_proveedor = id_proveedor;
			old_moneda = id_moneda;
		}

		/*OPERACIONES ARITMETICAS*/
		/*DOCUMENTO FACTURA*/
		if (id_tipo_documento == '1' | id_tipo_documento == '22') {
			total_pago += pago_saldo;
			id_libro_compra = id_documento;
		}
		/*DOCUMENTO NOTA CREDITO*/
		else if (id_tipo_documento == '2') {
			total_nc += pago_saldo;
			id_libro_compra = id_documento;
		} /*DOCUMENTO ANTICIPO*/
		else if (id_tipo_documento == '20') {
			total_anticipo += pago_saldo;
			id_anticipo = id_documento;
		} else {
			flag_3++;
		}


		//VALIDAR QUE LA MONEDA NO SEA EURO 
		// if (id_moneda != 143 && id_moneda != 111) {
		// 	flag_4++;
		// }
		//VALIDAR QUE SEA MISMO PROVEEDOR Y MISMA MONEDA
		if (id_proveedor !== old_proveedor || old_moneda !== id_moneda) {
			flag_1++;
		}
		//VALIDAR QUE SALDO NO SEA NEGATIVO Y PAGO NO SEA CERO
		if (saldo < 0 || pago_saldo <= 0 | pago_saldo == undefined) {
			flag_2++;
		}
		if (flag_1 != 0 || flag_2 != 0) {
			$('.selected#' + val.id).css('background-color', '#F89494');
		} else {
			$('.selected#' + val.id).css('background-color', '#B0BED9');
			//ALMACENAR ITEMS SELECCIONADOS
			data.push({
				operacion: {
					id_compra: id_libro_compra,
					id_anticipo: id_anticipo,
					monto_pago: pago_saldo
				}
			});
		}
		//FORMATEAR VALORES
		id_tipo_documento = null;
		id_documento = null;
		saldo = 0;	
		pago_saldo = 0; 	
		id_libro_compra = null;
		id_anticipo = null;


	});

	//RETURN PARA CONTINUAR PROCESO
	if (flag_1 != 0 | flag_2 != 0 | flag_3 != 0 | total_nc + total_anticipo > total_pago) {
		if (flag_2 != 0) {
			msjToast('El pago del saldo no puede ser menor o igual a cero.');
		}
		if (flag_1 != 0) {
			msjToast('Seleccione el mismo proveedor y la misma moneda.');
		}
		if (flag_3 != 0) {
			msjToast('El tipo de documento seleccionado no es valido.');
		}
		// if (flag_4 != 0) {
		// 	msjToast('No se puede procesar en otras monedas que no sean USD y PYG.');
		// }
		if (total_nc + total_anticipo > total_pago) {
			msjToast('El total de las notas de créditos y anticipos supera el total de la factura.');
		}
		msjToast('', 1);
		return false;
	} else {
		//ALMACRENAR CABECERA DE DETALLES SELECCIONADOS
		operaciones.data = data;
		neto_pago = total_pago - total_anticipo - total_nc;
		console.log(total_pago);
		console.log(total_anticipo);
		console.log(total_nc);
		console.log(neto_pago);

		operaciones.total_pago = total_pago;
		operaciones.total_nc = total_nc;
		operaciones.total_anticipo = total_anticipo;
		operaciones.total_pago_neto = neto_pago;
		operaciones.iva10 = iva10;
		operaciones.gravada10 = gravada10;
	


		operaciones.cabecera = {
			id_proveedor: id_proveedor,
			proveedor_n: $('.selected').find('.proveedor_nombre').html(),
			id_moneda: id_moneda,
			currency_code: $('.selected').find('.currency_code').html()
		};
		return true
	}

} //




	function processOrder(){

		$.toast().reset('all');

		let cotizacion = 0;
		let cotizado = 0;
		let id_moneda = operaciones.cabecera.id_moneda;
		/*$('#tabConfirm').tab('show');
		$('#tabHome').prop('disabled',true);*/
		$('a[href="#cabecera"]').click();

		$('#cf_currency_code').val(operaciones.cabecera.currency_code);
		$('.cf_currency_code').html('PYG');

		if(id_moneda == 111){
			$('#cf_total_pago').val(operaciones.total_pago);
		}else{
			$('#cotizado_total_factura').val(operaciones.total_pago);
		}

		$('#cf_anticipo').val(operaciones.total_anticipo);	
		$('#cf_nota_credito').val(operaciones.total_nc);
		$('#cf_neto_pago').val(operaciones.total_pago_neto);
		$('#cf_beneficiario').val(operaciones.cabecera.proveedor_n);
		$('#iva_10').val(operaciones.iva10);
		$('#gravada_10').val(operaciones.gravada10);
		// $('#cf_concepto').val('');
		// $('#cf_concepto').val('PAGO OP');
		console.log(operaciones.total_pago_neto);

		// if(id_moneda == 111){
		// 	$('#fp_id_moneda').val('111').trigger('change.select2');
		// } else {
		// 	$('#fp_id_moneda').val('143').trigger('change.select2');
		// }
		// getCuentas();

		/*{{--VALIDAR QUE LOS ELEMENTOS AJAX SE FINALIZEN PARA EMITIR MENSAJE --}}*/
		Promise.all([getRetencion()]).then(() => {
			console.log('FINALIZA RETENCION CON EXITO');

					Promise.all([getCotizacion(),setCotizar()]).then(() => {
						console.log('FINALIZA COTIZACIONES CON EXITO');
						UnblockOp();	
					}).catch(() => {
						BlockOp();
						console.log('FINALIZA COTIZACIONES CON ERROR');
					});
				
			}).catch(() => {
				console.log('ERROR EN RETENCION');
				BlockOp();
			}).finally(() => {
				console.log('FIN DE PROCESO');
				$.unblockUI();
			});
 		
		
		
	}


	/*{{-- ============================================================================================ 
											RETENCION
	============================================================================================--}}*/	

	function getRetencion(){
	return new Promise((resolve, reject) => { 


		$.ajax({
		  async:false,	
          type: "GET",
          url: "{{route('getRetencion')}}",
          data:{ id_proveedor: operaciones.cabecera.id_proveedor,
				 total_gravada: operaciones.gravada10,
				 id_moneda: operaciones.cabecera.id_moneda},
          dataType: 'json',
          error: function(jqXHR,textStatus,errorThrown){
          	 msjToast('Ocurrio un error en la comunicación con el servidor y no se pudo obtener la retención.',2,jqXHR,textStatus);
          	 reject(false);
          },
          success: function(rsp){
			


					if(rsp.err === true){
						if(rsp.retencion === true){
						$('#ret_iva_10').val(rsp.monto);
						$('#cf_retencion').val(rsp.monto);
						operaciones.retencion = Number(rsp.monto);
						operaciones.total_pago_neto = operaciones.total_pago_neto - Number(rsp.monto);
						$('#cf_neto_pago').val(operaciones.total_pago_neto);
						console.log('MONTO RETENCION',rsp.monto);
						} else {
						$('#ret_iva_10').val(0);
						$('#cf_retencion').val(0);
						}

						resolve({resp:true});
						

					} else {
						msjToast('Ocurrio un error en la comunicación con el servidor y no se pudo obtener la retención.',2);
						$('#ret_iva_10').val(0);
						console.log('OCURRIO UN ERROR EN LA CONSULTA');
						reject(false);
					}


                          }//success
                 });

			});//promise


		}//


				

/*{{-- ============================================================================================ 
						PROCESAR Y ENVIAR DATOS PARA CONFIRMAR PAGO
	============================================================================================--}}*/	

	function validar_envio(){
		let err = true;
		let msj='';

		$.toast().reset('all');

		if($('#cf_cotizacion').val().trim() == '0' | $('#cf_cotizacion').val().trim() == ''){
			$('#cf_cotizacion').css('border-color','red');
			err = false;
			msjToast('La cotización no puede ser cero');
		} else {
			$('#cf_cotizacion').css('border-color','');
		}

		// if($('#cf_entrega').val().trim() == ''){
		// 	 err = false;
		// 	$('#cf_entrega').css('border-color','red');
		// 	msjToast('Complete el campo Entregado a.');
		// } else {
		// 	$('#cf_entrega').css('border-color','');
		// }

		// if($('#cf_concepto').val().trim() == ''){
		// 	err = false;
		// 	msjToast('Complete el campo Concepto');
		// 	$('#cf_concepto').css('border-color','red');
		// } else {
		// 	$('#cf_concepto').css('border-color','');
		// }

		if(!err){
			msjToast('',1);
		}

		return err;
	}


	const op = {
		id:0
	}

	function openNewOp(){
		console.log('abrio modal');
		if(op.id != 0){
		//	$('#num_op').html(op.id);
			//$('#modalNumOp').modal('show');
		}
	}

	$('#modalNumOp').on('hidden.bs.modal', function (e) {
		cancelarOp();
		op.id = 0;
		$('#num_op').html('');
		})
	



	/*{{-- ==============================================
			CONFIG Y OTROS
		==============================================--}}*/

 function initConfig(){
 	$('#tabConfirm').prop('disabled',true);
 	$('#btnProcesarOP').prop('disabled',true);
 	//  blockUI();
 }


 function blockUI(){

 	     $.blockUI({
				  centerY: 0,
				  message: "<h2>Procesando...</h2>",
				  css: {
				    color: '#000'
				   }
				 });

 }


 		{{--Almacena mensajes para mostrar --}}
		function msjToast(msj,opt = false, jqXHR = false, textStatus = ''){
	

			if( msj != ''){ 
				operaciones.msj.push('<b>'+msj+'</b>');
			} 

			if(opt === 1){
				$.toast().reset('all');

				$.toast({	
				    heading: '<b>Atención</b>',
				    position: 'top-right', 
				    text: operaciones.msj,
				    width: '400px',
				    hideAfter: false,
				    icon: 'info'
				});
				operaciones.msj = [];

			} else if(opt === 2){
				console.log('Ingreso opt == 1');
				$.toast().reset('all');

				

				if(jqXHR === false && textStatus === ''){
						msj = operaciones.msj;
				 } else{

						 errCode = jqXHR.status;
						 errMsj = jqXHR.responseText;

				 	msj ='<b>';
				 	if(errCode === 0 )
				 		msj += 'Error de conectividad a internet, verifica tu conexión.';
				 	else if(errCode === 404)
				 		msj += 'Error al realizar la solicitud.';
				 	else if(errCode === 500){
				 		if(operaciones.msj.length > 0){
				 		msj += operaciones.msj;
				 		} else { msj += 'Ocurrio un error en la comunicación con el servidor.'}

				 	}else if(errCode === 406){
				 		msj += errMsj;
				 	}
				 	else if(errCode === 422)
				 		 msj += 'Complete todos los datos requeridos'
				 	else if(textStatus === 'timeout')
				 		msj += 'El servidor tarda mucho en responder.';
				 	else if(textStatus === 'abort')
				 		msj += 'El servidor tarda mucho en responder.';
				 	else if (textStatus === 'parsererror'){
				 		msj += 'Error al realizar la solicitud.';
				 		console.log('ParseError');
				 	}
				 	else 
				 		msj += 'Error desconocido, pongase en contacto con el area tecnica.';
				 	msj += '</b>';
				 }


				 	$.toast({
						    heading: '<b>Error</b>',
						    text: msj,
						    position: 'top-right', 
						     hideAfter: false,
						     width: '400px',
						    icon: 'error'
						});

				operaciones.msj = [];
			}
		}//


			$('.control_space').keypress(function(event) {
			
			var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
		});
	        
	     const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD'
					});


		/*{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
			NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}*/
		function clean_num(n,bd=false){

				  	if(n && bd == false){ 
					n = n.replace(/[,.]/g,function (m) {  
							 				 if(m === '.'){
							 				 	return '';
							 				 } 
							 				  if(m === ','){
							 				 	return '.';
							 				 } 
							 			});
					return Number(n);
				}
				if(bd){
					return Number(n);
				}
				return 0;

				}//

		$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});
		

		/*{{--FUNCION PARA ALMACENAR DATOS --}}	*/
		const operaciones = {
			data:{},
			cabecera:{},
			total_pago:0,
			total_nc:0,
			total_pago_neto:0,
			total_anticipo:0,
			iva10:0,
			gravada10:0,
			retencion:0,
			msj:[],
			clear : function(x){
				this.data = {};
				this.cabecera = {};
				this.total_pago = 0;
				this.total_nc = 0;
				this.total_pago_neto = 0;
				this.total_anticipo = 0;
				this.iva10 = 0;
				this.gravada10 = 0;
				this.retencion = 0;
			}
		}




	/*{{-- ==============================================
			COTIZACIONES
		==============================================--}}*/

	function getCotizacion(){
	return new Promise((resolve, reject) => { 
		let  cotizacion;


			{{--OBTENER COTIZACION --}}
		$.ajax({
		  async:false,	
          type: "GET",
          url: "{{route('getCotizacionFP')}}",
          data:{id_moneda_costo:operaciones.cabecera.id_moneda,
      			id_moneda_venta: $('#fp_id_moneda').val()},
          dataType: 'json',
          error: function(jqXHR,textStatus,errorThrown){
          	 msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo obtener la cotización.',2,jqXHR,textStatus);
          	 reject(false);
          },
          success: function(rsp){

                          if(rsp.info.length > 0){
                          	let cot = Number(rsp.info[0].cotizacion);
                          	

                          	if( cot > 0){
	                          	$('#cf_cotizacion').val(rsp.info[0].cotizacion);
	                          	cotizacion = rsp.info[0].cotizacion;
	                          	resolve({resp:true});

                              } else {
                              	msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.',1);
                              	reject(false);
                              }
                            } else {
                            	msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.',1);
                            	reject(false);
                            }

                          }//success
                 });

			});//promise


		}//


	function setCotizar(opt=0){
	return new Promise((resolve, reject) => { 

	if((operaciones.cabecera.id_moneda != $('#fp_id_moneda').val()) | opt == 1){
		let data = {};
		data = {
			cabecera:{ id_moneda_costo:operaciones.cabecera.id_moneda,
      			       id_moneda_venta: $('#fp_id_moneda').val()
      			     },
      		total_factura: operaciones.total_pago,
      		total_nc: operaciones.total_nc,
      		total_anticipo: operaciones.total_anticipo,
      		total_pago_neto: operaciones.total_pago_neto,
			retencion:operaciones.retencion   	     
		}

	
			$.ajax({
          type: "GET",
          url: "{{route('cotizarMontosProveedor')}}",
          data:data,
          dataType: 'json',
          error: function(jqXHR,textStatus,errorThrown){

          		 msjToast('Ocurrio un error en la comunicación con el servidor y no se pudo cotizar los montos.',2,jqXHR,textStatus);
          		 reject(false);
          },
          success: function(rsp){
                          if(rsp.info.length > 0){
                          	if(rsp.moneda == '111'){
                          		//alert($('#fp_id_moneda').val());
                          		$('#cotizado_total_factura').val(formatter.format(parseFloat(rsp.info[0].total_factura)));
                          	}else{
                          		//alert($('#fp_id_moneda').val());
                          		$('#cf_total_pago').val(formatter.format(parseFloat(rsp.info[0].total_factura)));
                          	}

                          	$('#cotizado_anticipo').val(formatter.format(parseFloat(rsp.info[0].total_anticipo)));
                          	$('#cotizado_nc').val(formatter.format(parseFloat(rsp.info[0].total_nc)));
							$('#cf_anticipo').val(formatter.format(parseFloat(rsp.info[0].total_anticipo_gs)));
							$('#cotizado_retencion').val(formatter.format(parseFloat(rsp.info[0].total_retencion)));
							//   getRetencion();

							$('#cotizado_pago_neto').val(formatter.format(parseFloat(clean_num($('#cotizado_total_factura').val())) - (parseFloat(clean_num($('#cotizado_anticipo').val())) + parseFloat(clean_num($('#cotizado_nc').val())) +parseFloat(clean_num($('#cotizado_retencion').val())))));

							$('#cf_neto_pago').val(formatter.format(parseFloat(clean_num($('#cf_total_pago').val())) - (parseFloat(clean_num($('#cf_anticipo').val())) + parseFloat(clean_num($('#cf_nota_credito').val())) +parseFloat(clean_num($('#cf_retencion').val())))));


                          	resolve({resp:true});

                          } else {
                          	 msjToast('Ocurrio un error al intentar cotizar los valores.',1);
                          	 reject(false);

                          }

                          }//success
                 })//ajax
	


		 } else {
	
		 	$('#cotizado_total_factura').val(operaciones.total_pago);
            $('#cotizado_anticipo').val(operaciones.total_anticipo);
            $('#cotizado_nc').val(operaciones.total_nc);
            $('#cotizado_pago_neto').val(operaciones.total_pago_neto);


			 resolve({resp:true});
			
		 }	


		 });//promise

	}


			$("#botonExcel").on("click", function(e){ 
			// console.log('Inicil');
                e.preventDefault();
                $('#consultaFactura').attr('method','post');
               	$('#consultaFactura').attr('action', "{{route('generarExcelPagoProv')}}").submit();
            });


		$('#fp_id_moneda').change(function(){
			
			Promise.all([getCotizacion(),setCotizar()]).then(() => {
				UnblockOp();	
			}).catch(() => {
				BlockOp();
			}).finally(() => {
				/*{{--FIN PROCESO DE VALIDACION --}}	*/	
				$.unblockUI();
			});
	
			
			});		

		 function BlockOp(){
		 	$('#btnAgregarFila').prop('disabled',true);
		 	$('#btnProcesarOP').prop('disabled',true);
		 }	

		  function UnblockOp(){
		 	$('#btnAgregarFila').prop('disabled',false);
		 	$('#btnProcesarOP').prop('disabled',false);
		 }	


	function controlNumFormat(){
		$('.formatInput').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false
		});
	}
	

		function sumarTotales(value,documento){
			if(documento == 22 || documento == 5){
				total = parseFloat(clean_num($("#totalSaldo").val()));
				/*console.log('--------------------------------------');
				console.log(total);
				console.log(value);
				console.log('--------------------------------------');*/
				suma = total + parseFloat(value);
				$("#totalSaldo").val(suma);
			}	
		}



</script>

@endsection
