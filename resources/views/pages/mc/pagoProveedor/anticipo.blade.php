@extends('masters')
@section('title', 'Panel de Control')
@section('styles')

<style type="text/css">
    .correcto_col {
        height: 74px;
    }
    					
    .bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

    input.form-control:focus ,.select2-container--focus, button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }


    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
</style>



@parent

@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Anticipo Proveedor</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">           
                    <div class="card-body pt-0">
                        <button id="btnAddAnticipo"  title="Agregar Anticipo" class="btn btn-success pull-right"
                            type="button">
                            <div class="fonticon-wrap">
                                <i class="ft-plus-circle"></i>
                            </div>
                        </button>
                    </div>
           

    <div class="card-body">
        <form class="row" id="formAnticipo" autocomplete="off">
                <div class="col-12 col-sm-3 col-md-3">
		 				<div class="form-group">
					        <label>Fecha Anticipo</label>			
					            <div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class = "Requerido form-control input-sm" id="fecha_anticipo" name="fecha_anticipo" tabindex="10"/>
								</div>
						</div>	
					</div> 

                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Proveedor</label>
                            <select class="form-control select2" name="id_beneficiario" tabindex="1" id="id_beneficiario" style="width: 100%;">
                                <option value="">Todos
                                </option>
                                @foreach ($beneficiario as $b)
                                    @php
										$ruc = $b->documento_identidad;
										if($b->dv){
											$ruc = $ruc."-".$b->dv;
										}
									@endphp
                                <option value="{{$b->id}}"> {{$ruc}} - {{$b->full_name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Nro. Anticipo</label>
                            <input type="number" class="form-control" id="nro_anticipo" name="nro_anticipo" value="" >
                        </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Nro. OP</label>
                            <input type="number" class="form-control" id="nro_op" name="nro_op" value="" >
                        </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3">
                            <div class="form-group">
                                <label>Moneda</label>
                                <select class="form-control select2" name="id_moneda" tabindex="2" id="id_moneda" style="width: 100%;">
                                    <option value="">Todos
                                    </option>
                                    @foreach ($currency as $div)
                                    <option value="{{$div->currency_id}}">{{$div->currency_code}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                </div>


                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Estado</label>
                            <select class="form-control select2" name="estado" id="estado" tabindex="3" style="width: 100%;">
                                <option value="">Todos</option>
                                <option value="1" selected="selected" >Pendiente</option>
                                <option value="2">Aplicado</option>
                                <option value="3">Anulado</option>
                            </select>
                        </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Saldo PYG</label>
                            <input type="text" class="form-control format-number-control" id="total_gs" name="total_gs" value="0" disabled>
                        </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Saldo USD</label>
                            <input type="text" class="form-control format-number-control" id="total_usd" name="total_usd" value="0" disabled>
                        </div>
                </div>

            <div class="col-12">
            <button type="button" id="botonExcel" class="btn btn-success btn-lg pull-right mr-1"><b>Excel</b></button>
                <button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
                <button type="button" id="btnBuscar" class="btn btn-info btn-lg pull-right mr-1" tabindex="4" ><b>Buscar</b></button>
            </div>

        </form>

                <div class="table-responsive table-bordered mt-1">
                    <table id="listado" class="table" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="text-align: center;">N° Anticipo</th>
                                <th style="text-align: center;">Fecha Anticipo</th>
                                <th style="text-align: center;">N° OP</th>
                                <th style="text-align: center;">OP Aplicado</th>
                                <th style="text-align: center;">Proveedor</th>
                                @if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'V')
                                    <th style="text-align: center;">Venta</th>
                                @else 
                                    <th style="text-align: center;">Grupo/Poforma</th>
                                @endif
                                <th style="text-align: center;">Importe</th>
                                <th style="text-align: center;">Moneda</th>
                                <th style="text-align: center;">Saldo</th>
                                <th style="text-align: center;">Estado</th>
                               <!-- <th style="text-align: center;">Activo</th>-->
                                <th style="text-align: center;">Fecha Vencimiento</th>
                            </tr>
                        </thead>
                        <tbody style="text-align: center">
                        </tbody>
                    </table>
                </div>
            </div>
		</div>
	</div>
</section>



@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>

<script type="text/javascript">
    $(() => {
 
        main();
    });
    $(document).ready(function(){
        var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('input[name="fecha_anticipo"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="fecha_anticipo"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
		});
       
		$('#activo').val('true').trigger('change.select2');


    });

    function main() {
        calendar();
        formatNumber();
        $('.select2').select2();
        $('#pendiente').val('1').trigger('change.select2');
        datatable();
    } //

    $('#btnAddAnticipo').click(() => {
        redirectCreateAnticipo()
    });
    $('#btnBuscar').click(() => {
        datatable();
    });
    $('#btnSaveAnticipo').click(() => {
        saveDataAnticipo();
    });

    function limpiar()
		{
			$('#id_beneficiario').val('').trigger('change.select2');
			$('#id_moneda').val('').trigger('change.select2');
			$('#activo').val('').trigger('change.select2');
		}

    function redirectCreateAnticipo(){
        $.blockUI({
						centerY: 0,
						message: "<h2>Redirigiendo a vista de Anticipo...</h2>",
						css: {
							color: '#000'
						}
					});

       location.href ="{{ route('anticipoAdd') }}";
    }


    function calendar() {
        $('.single-picker').datepicker({
            format: "dd/mm/yyyy",
            language: "es"
        });
    }

    function formatNumber() {

        $('.format-number-control').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false
        });

    }
    function datatable() {
        var table;

        $.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });

       		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){

	



        table = $("#listado").DataTable({
            destroy: true,
            ajax: {
                url: "{{route('getListAnticipo')}}",
                data: $('#formAnticipo').serializeJSON(),
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                    $.unblockUI();
                }

            },
            "columns": [
                {
                    data: function(x){
                        return `<a href="{{route('anticipoDetalle',['id'=>''])}}/${x.id}"  class="bgRed">
						        	<i class="fa fa-fw fa-search"></i>${x.id}</a>
						        	<div style="display:none;">${x.id}</div>
						        	`;
                    }
                },
                {
                    data: 'fecha_format_pago'
                },

                {
                    data: function(x){
                        return `<a href="{{route('controlOp',['id'=>''])}}/${x.op.id}"  class="bgRed">
						        	<i class="fa fa-fw fa-search"></i>${x.op.nro_op}</a>
						        	<div style="display:none;">${x.id}</div>
						        	`;
                    }
                },

                {
                    data: 'op_aplicado'
                },
                {
                    data: function (x) {
                        if (Boolean(x.beneficiario_format)) {
                            return x.beneficiario_format.beneficiario_n
                        }
                        return '';
                    }
                },
                {
                    data: function (x) {
                        resultado = '';
                        @if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'V')
                            if (jQuery.isEmptyObject(x.id_venta)){
                                if(resultado != ""){
                                    resultado += '/';
                                }
                                resultado += x.id_venta;
                            }
                        @else    
                            if (jQuery.isEmptyObject(x.id_grupo)){
                                resultado +=  x.id_grupo;
                            }
                            if (jQuery.isEmptyObject(x.id_proforma)){
                                if(resultado != ""){
                                    resultado += '/';
                                }
                                resultado += x.id_proforma;
                            }
                        @endif    
                        return resultado;
                    }
                },

                {
                    data: function(x){
                        return `<input type="text" readonly class="input-style numeric" value="${x.importe}">`;
                    }
                },
                {
                    data: function (x) {
                        if (Boolean(x.currency)) {
                            return x.currency.currency_code
                        }
                        return '';
                    }
                },

                {
                    data: function(x){
                        return `<input type="text" readonly class="input-style numeric" value="${x.saldo}">`;

                    }
                },
                {
                    data: 'estado'
                },
                /*{
                    data: function(x){
                        if(x.pendiente == true){
                            return 'SI';
                        } else {
                            return 'NO';
                        }
                    }
                },*/
                {
                    data: 'fecha_format_vencimiento'
                }

            ],
            "aaSorting":[[0,"desc"]],
            "createdRow": function (row, data, iDataIndex) {
                        if(data.id_moneda == 111){
                            total = clean_num($('#total_gs').val());
                            suma =   parseFloat(total) + parseFloat(data.importe);
                            $('#total_gs').val(suma);
                        }
                        if(data.id_moneda == 143){
                            total = clean_num($('#total_usd').val());
                            suma =   parseFloat(total) + parseFloat(data.importe);
                            $('#total_usd').val(suma);
                        }
                    },
            //AL FINALIZAR LA CARGA AGREGAR CONTROL DE COMAS Y PUNTO
            "initComplete": function (settings, json) {
                $.unblockUI();
            }
        }).on('draw', function () {
	       		/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

			    $('#listado tbody tr .numeric').inputmask("numeric", {
					radixPoint: ",",
					groupSeparator: ".",
					digits: 2,
					autoGroup: true,
					 // prefix: '$', //No Space, this will truncate the first character
					rightAlign: false
				});

    	});
;


    }, 300);


    }

	$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
				$('#formAnticipo').attr("method", "post");               
				$('#formAnticipo').attr('action', "{{route('generarExcelListAnticipo')}}").submit();
            });

            $('.numeric').inputmask("numeric", {
					radixPoint: ",",
					groupSeparator: ".",
					digits: 2,
					autoGroup: true,
					 // prefix: '$', //No Space, this will truncate the first character
					rightAlign: false
				});
        function clean_num(n,bd=false){
            if(n && bd == false){ 
            n = n.replace(/[,.]/g,function (m) {  
                                    if(m === '.'){
                                        return '';
                                    } 
                                    if(m === ','){
                                        return '.';
                                    } 
                                });
            return Number(n);
            }
            if(bd){
                return Number(n);
            }
            return 0;
        }


</script>
@endsection
