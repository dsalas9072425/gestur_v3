
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	  <link rel="stylesheet" href="{{asset('mC/css/select.dataTables.min.css')}}"> 
@endsection
@section('content')

<style type="text/css">
	

	.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

@-moz-document url-prefix() {
  fieldset { display: table-cell; }
}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	.select2-container *:focus {
        outline: none;
    }


    .correcto_col { 
		height: 74px;
     }

	 input.form-control:focus ,.select2-container--focus , button:focus{
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

	.input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
	




</style>


<section id="base-style">

	<div class="card" style="border-radius: 14px;">
		<div class="card-header" style="border-radius: 14px;">
			<h4 class="card-title">APLICACION DE ANTICIPOS - PROVEEDOR</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


				{{--======================================================
							LISTA PAGO PROVEEDOR
				====================================================== --}}

				<div class="tab-content mt-1">
					<section id="home" class="tab-pane active" role="tabpanel">
						<form id="consultaCuenta" autocomplete="off">
							<div class="row">
								<div class="col-6 col-sm-4 col-md-4">
									<div class="form-group">
										<label>Proveedor</label>
                                        <select class="form-control select2" name="id_proveedor" id="id_proveedor" data-value-type="number"  style="width: 100%;">
                                            <option value="">Seleccionar Proveedor</option>
                                            @foreach ($proveedores as $b)
												@php
													$ruc = $b->documento_identidad;
													if($b->dv){
														$ruc = $ruc."-".$b->dv;
													}
												@endphp
                                                <option value="{{$b->id}}">{{$ruc}} - {{$b->nombre}} {{$b->apellido}}</option>
                                            @endforeach
                                        </select>
									</div>
								</div>
								<div class="col-6 col-sm-4 col-md-4">
									<div class="form-group">
										<label>Fecha Anticipo.</label>			
											<div class="input-group">
											<div class="input-group-prepend" style="">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" class = "Requerido form-control input-sm" id="fecha_emision" name="fecha_emision" tabindex="10"/>
										</div>
									</div>	
								</div> 

    							<div class="col-12 mt-1 mb-1">
									<button type="button" id="botonPdf" class="pull-right text-center btn btn-success btn-lg mr-1"><b>PDF</b></button>
									<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
									<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
									<button tabindex="10" class="pull-right btn btn-info btn-lg mr-1" id="btnBuscar" type="button"><b>Buscar</b></button>

								</div>
							</div>
						</form>
						<div class="table-responsive">
							<table id="listadoDetalle" class="table table-hover table-condensed nowrap" style="width: 100%;">
								<thead>
									<tr>
										<th>Fecha</th>
										<th>Proveedor</th>
										<th>Nro.OP</th>
										<th>Importe</th>
                                        <th>Saldo</th>
                                        <th>Moneda</th>
									</tr>
								</thead>
								<tbody style="text-align: left">
								</tbody>
							</table>
						</div>
					</section>

				</div>
			</div>
		</div>

</section>
{{-- BASE STYLE --}} 


    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script type="text/javascript" src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.min.js"></script>

	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
	});
	$('input[name="fecha_vencimiento"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="fecha_vencimiento"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });
		$('input[name="fecha_vencimiento"]').val('');

		$('input[name="fecha_emision"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="fecha_emision"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });
		$('input[name="fecha_emision"]').val('');

	$('#btnBuscar').click(()=>{
			tableDetallle();
	});

	function  tableDetallle(){
		//alert('UDhgk');
		const formatter = new Intl.NumberFormat('de-DE', {currency: 'USD',minimumFractionDigits: 0});
		let resp;
		let form = $('#consultaCuenta').serializeJSON({
			customTypes: customTypesSerializeJSON
		});

		tableListado = $("#listadoDetalle").DataTable({
						destroy: true,
						pageLength: 100,
                        processing: true,
                      //  searching: false,
                        bLengthChange: false,
                        "ajax": {
							"url": "{{route('getAplicacionAnticiposProveedor')}}",
							"data": form,
                        },
						"columns": [
						{data: (x)=>{
								if(jQuery.isEmptyObject(x.fecha) == false){
									fecha = x.fecha.split(' ');
									fecha_formateada  = formatoFecha(fecha[0])+' '+fecha[1];
								}else{
									fecha_formateada  = "";
								}	
								return fecha_formateada;
							},"defaultContent": ""},
						{data: 'beneficiario_nombre', "defaultContent": ""},
						{data: 'op_anticipo', "defaultContent": ""},
						{data: (x)=>{
							if(jQuery.isEmptyObject(x.importe) == false){
								var importe = formatter.format(parseFloat(x.importe));
								return importe;
						}else{
								return "Ningún dato disponible en esta tabla =(";
							}
							 
						},"defaultContent": "Ningún dato disponible en esta tabla =("},
						{data: (x)=>{
								if(jQuery.isEmptyObject(x.saldo) == false){
									var importe = formatter.format(parseFloat(x.saldo));
								}else{
									importe  = "";
								}	
 								return importe;
							},"defaultContent": "0"},
                        {data: 'moneda', "defaultContent": ""}
			 	],
					 "createdRow": function (row, data, iDataIndex) {
                        if(data.id_moneda == 111){
                            total = clean_num($('#total_pyg').val());
                            suma =   parseFloat(total) + parseFloat(data.saldo);
                            $('#total_pyg').val(suma);
                        }
                        if(data.id_moneda == 143){
                            total = clean_num($('#total_usd').val());
                            suma =   parseFloat(total) + parseFloat(data.saldo);
                            $('#total_usd').val(suma);
                        }
                    },
                })
                $('#listadoDetalle tbody').on('click', 'td.dt-body-right', function () {
                    var tr = $(this).closest('tr');
                    var row = $('#listadoDetalle').DataTable().row(tr);
                                    if (row.child.isShown()) {
                                            // This row is already open - close it
                                            row.child.hide();
                                            tr.removeClass('shown');
                                        }
                                        else {

                                            // Open this row
                                            row.child(format(row.data())).show();
                                            tr.addClass('shown');
                                        }
                })
                $('a[href="#detalle"]').click();	
                desplegarTabla($('#listadoDetalle').DataTable());
		}

/////////////////////////////////////////////////////////////////////////////////////////////////
        function format(d) {
				perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
				var tabla = '<table cellpadding="10" cellspacing="0" border="0" style="background-color: #eaebee; width: 100%"> ';
					tabla+= '<tr>';
					tabla+= '<th style="width: 150px;">Fecha</th>';
					tabla+= '<th style="width: 40%;">Concepto</th>';
					tabla+= '<th style="width: 150px;">Fecha Pago</th>';
                    tabla+= '<th style="width: 150px;">Nro OP</th>';
					tabla+= '<th style="width: 100px;">Moneda</th>';
					tabla+= '<th style="width: 120px;">Importe</th>';
					tabla+= '<th style="width: 120px;">Saldo</th>';
					tabla+= '<th style="width: 100px;">Proforma</th>';   
                    tabla+= '<th style="width: 100px;">Estado</th>';
                    tabla+= '<th style="width: 150px;">Vencimiento</th>';
					tabla+= '</tr>';
                    indicador = 0;
					$.each(d.detalles, function (key, item){
						if(jQuery.isEmptyObject(item.fecha_aplicacion) == false){
							fecha = item.fecha_aplicacion.split(' ');
						}else{
							fecha = "";
						}	
						perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
					    tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+formatoFecha(fecha[0])+' '+fecha[1]+'</td>';
						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.concepto+'</td>';
						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+formatoFecha(item.fecha_pago)+'</td>';
						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.nro_op+'</td>';
                        tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+item.moneda+'</td>';
                        tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+formatter.format(parseFloat(item.importe))+'</td>';
                        tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+formatter.format(parseFloat(item.saldo))+'</td>';
                        tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b>'+item.id_proforma+'</b></td>';
                        tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;"><b>'+item.estado+'</b></td>';
						tabla+= '<td style="padding-bottom: 5px;padding-top: 5px;">'+formatoFecha(item.fecha_vencimiento)+'</td>';
						tabla+= '</tr>';
					})	 
					tabla+= '</table>';
				    return tabla;  
				}
////////////////////////////////////////////////////////////////////////////////////////////////

		 function  desplegarTabla(table){
				$('#listadoDetalle').on('init.dt', function(e, settings){
					   var api = new $.fn.dataTable.Api( settings );
					   api.rows().every( function () {
					      var tr = $(this.node());
					      this.child(format(this.data())).show();
					      tr.addClass('shown');
					   });
					});
		 }


		const formatter = new Intl.NumberFormat('de-DE', 
		{
			currency: 'USD',
			minimumFractionDigits: 2
		});


				function cargarDetalle(value, moneda_id){
					console.log(value);
					$('#idProveedor').val(value).trigger('change.select2');
					$('#idMoneda').val(moneda_id).trigger('change.select2');
					$('#tabHome').tab('show');
					tableDetallle();
				}	
				function formatoFecha(texto){
						if(texto != '' && texto != null){
					return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
						} else {
					return '';	
							
						}

					}
		
		$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

		function clean_num(n,bd=false){
			if(n && bd == false){ 
			n = n.replace(/[,.]/g,function (m) {  
					 				 if(m === '.'){
					 				 	return '';
					 				 } 
					 				  if(m === ','){
					 				 	return '.';
					 				 } 
					 			});
			return Number(n);
		}
		if(bd){
			return Number(n);
		}
		return 0;
		}

		$("#botonExcel").on("click", function(e){ 
            e.preventDefault();
            $('#consultaCuenta').attr('method','post');
			$('#consultaCuenta').attr('action', "{{route('generarExcelAplicacionAnticiposProveedor')}}").submit();
		});

		$("#botonPdf").on("click", function(e){ 
			e.preventDefault();
            $('#consultaCuenta').attr('method','get');
			$('#consultaCuenta').attr('action', "{{route('aplicacionAnticiposProveedorPdf')}}").submit();
		
		});


</script>

@endsection