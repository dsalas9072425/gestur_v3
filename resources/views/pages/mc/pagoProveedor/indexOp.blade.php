
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	  <style type="text/css">
	

		.readOnly {
		 background-color: #eee;
	 border-color: rgb(210, 214, 222);
	 cursor: not-allowed !important;
	}
	
	.form-button-cabecera{
				margin-bottom: 5px !important;
			}	
	
	@-moz-document url-prefix() {
	  fieldset { display: table-cell; }
	}
	
		.bgRed {
			/*background-color: #C48433;*/
			font-size: 15px;
	
		display: inline-block;
		min-width: 10px;
		padding: 3px 7px;
		font-size: 15px;
		font-weight: 800;
		line-height: 1;
		color: #000;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		border-radius: 10px;
		border: 1px solid black;
		/*background: #e2076a*/
		}
	
		.select2-container *:focus {
			outline: none;
		}
	
	
		.correcto_col { 
			height: 74px;
		 }
	
		 #divBanco {
		border: 2px solid #D0D1C2;
		border-radius: 5px;
	
		 }
	
		 .title_bank{
			 border-bottom-right-radius: 5px;
			border-bottom-left-radius: 5px;
			background-color: #D0D1C2;
			padding: 3px 5px 5px 5px;
			font-weight: 800;
			margin-bottom: 2px;
	
		 }
	
		 .input-style {
			 border:none;
			 background-color: transparent;
			 text-align: center;
			 font-weight: bold;
		 }
		 .negrita{
			font-weight: bold;
		} 
	
	
	</style>
@endsection
@section('content')





<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Listado OP</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

				<form id="formProveedor">
					<div class="row">
						<div class="col-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Proveedor</label>
								<select class="form-control select2" name="id_proveedor"  id="proveedor" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach ($getProveedor as $pro)
										@if($pro->activo == true)
											@php 
												$activo = 'ACTIVO';
											@endphp
										@else
											@php 
												$activo = 'INACTIVO';
											@endphp
										@endif
										@php
										  $nombre = $pro->nombre;
										@endphp
										@if(isset($pro->denominacion_comercial))
										@if($pro->denominacion_comercial != "")
											@php 
												$nombre = $pro->nombre." ".$pro->apellido . " - ". $pro->denominacion_comercial;
											@endphp
										@endif
										@endif
										@php
											$ruc = $pro->documento_identidad;
											if($pro->dv){
												$ruc = $ruc."-".$pro->dv;
											}
										@endphp
										<option value="{{$pro->id}}">{{$ruc}} - {{$nombre}}  <b>({{$activo}})</b></option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-6 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Moneda</label>
								<select class="form-control select2" name="id_moneda"  id="idMoneda" tabindex="2" style="width: 100%;">
									<option value="">Todos</option>
									@foreach ($getDivisa as $div)
										<option value="{{$div->currency_id}}">{{$div->currency_code}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-3">
							<div class="form-group">
								<label>Nro OP </label>
								<input type="text" class="form-control" name="nro_op" id="nro_op" placeholder="Número de OP">
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Tipo</label>
								<select class="form-control select2" name="tipo_op"  id="tipo_op" style="width: 100%;">
									<option value="">Todos</option>
									<option value="2">OP</option>
									<option value="1">FF</option>
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3">  
							<div class="form-group">
								<label>Desde/Hasta Fecha Creación</label>						 
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text"  class="calendar form-control"  name="periodo_creacion:p_date" id="periodo_creacion" tabindex="4" />
								</div>
							</div>	
						</div> 
						<div class="col-12 col-sm-3 col-md-3">  
							<div class="form-group">
								<label>Desde/Hasta Fecha Proceso</label>						 
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text"  class="calendar form-control"  name="periodo_proceso:p_date" id="periodo_proceso" tabindex="4" />
								</div>
							</div>	
						</div> 
						<div class="col-12 col-sm-3 col-md-3">  
							<div class="form-group">
								<label>Desde/Hasta Fecha Verificación</label>						 
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text"  class="calendar form-control"  name="periodo_verificacion:p_date" id="periodo_verificacion" tabindex="4" />
								</div>
							</div>	
						</div> 
						<div class="col-12 col-sm-3 col-md-3">  
							<div class="form-group">
								<label>Desde/Hasta Fecha Autorización</label>						 
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text"  class="calendar form-control"  name="periodo_autorizacion:p_date" id="periodo_autorizacion" tabindex="4" />
								</div>
							</div>	
						</div> 
						<div class="col-12 col-sm-3 col-md-3">  
							<div class="form-group">
								<label>Desde/Hasta Fecha Anulación</label>						 
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text"  class="calendar form-control"  name="periodo_anulacion:p_date" id="periodo_anulacion" tabindex="4" />
								</div>
							</div>	
						</div> 
						<div class="col-12 col-sm-3">
							<div class="form-group">
								<label>Código de Confirmación</label>
								<input type="text" class="form-control" name="cod_confirmacion" id="cod_confirmacion" placeholder="Código de Confirmación">
							</div>
						</div>
						<div class="col-12 col-sm-3">
							<div class="form-group">
								<label>Proforma</label>
								<input type="text" class="form-control" name="proforma" id="proforma" placeholder="Proforma">
							</div>
						</div>

						<div class="col-6 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Estado</label>
								<select class="form-control select2" name="id_estado[]" id="id_estado" style="width: 100%;">
									@foreach ($estados as $estado)
										<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12" >
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style=" margin: 0px;"><b>Excel</b></button>
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
							<button  onclick ="loadDatatable()" class="pull-right btn btn-info btn-lg mr-2" type="button"><b>Buscar</b></button>
						</div>
					</div>
				</form>  
	           	<div class="table-responsive">
	              <table id="listado" class="table table-hover table-condensed nowrap text-center" style="width: 100%;">
	                <thead>
					  <tr>
					    <th></th>
						<th>OP</th>
						<th>Tipo</th>
						<th>Fecha Creación</th>
					  	<th>Proveedor</th>
					  	<th>Moneda</th>
						<th>Forma Pago</th>
					  	<th>Cuenta Fondo</th>
					  	<th>Monto</th>
					  	<th>Usuario Creación</th>
						<th>Estado</th>
					  	<th></th>
		              </tr>																												
	                </thead>
	                <tbody>
		         
			        </tbody>
	              </table>
	            </div>



			</div>
		</div>
	</div>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	
<script type="text/javascript">

	$('#id_estado').select2({
									multiple:true,
									maximumSelectionLength: 2,
									placeholder: 'Todos'
								});
	$('#id_estado').val([52,49,50]).trigger('change.select2');

$(document).ready(function () {
	$('.select2').select2();
	loadDatatable();
	calendar();
});




/*==============================================
		FILTROS BUSQUEDA
============================================== */
function calendar() {

	//CALENDARIO OPCIONES EN ESPAÑOL
	
	let locale = {
		format: 'DD/MM/YYYY',
		cancelLabel: 'Limpiar',
		applyLabel: 'Aplicar',
		fromLabel: 'Desde',
		toLabel: 'Hasta',
		customRangeLabel: 'Seleccionar rango',
		daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
			'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
			'Diciembre'
		]
	};


	$('.calendar').daterangepicker({
		timePicker24Hour: true,
		timePickerIncrement: 30,
		locale: locale
	}).on('cancel.daterangepicker', function (ev, picker) {
		$(this).val('');
	});
	$('.calendar').val('');


}





/*==============================================
		DATATABLE
============================================== */

var table;

function loadDatatable() {

	$.blockUI({
		centerY: 0,
		message: "<h2>Procesando...</h2>",
		css: {
			color: '#000'
		}
	});

	// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
	setTimeout(function () {

		
		//SE FILTRA Y QUITA ESPACIOS EN BLANCO
		let form = $('#formProveedor').serializeJSON({
				customTypes: customTypesSerializeJSON
			});
		let input;
		
		table = $("#listado").DataTable({
			destroy: true,
			keys: true,	
			ajax: {
				url: "{{route('reporteOp')}}",
				data: form,
				error: function (jqXHR, textStatus, errorThrown) {

					$.toast({
                            heading: 'Error',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

					$.unblockUI();
				}

			},
			"columns": [{
				    data: "id"
				},	
				{
					data: function (x) {
						return `<a href="{{route('controlOp',['id'=>''])}}/${x.id}"  class="bgRed">
						        	<i class="fa fa-fw fa-search"></i>${x.nro_op}</a>
						        	<div style="display:none;">${x.nro_op}</div>
						        	`;
					}
				},
				{
					data: function (x) 
					{
						var ff = '';
						var cantidad = x.cantidad_facturas;
						if (x.fondo_fijo == true) 
						{
							ff = 'FF';
						} 
						else
						{
							if ((x.fondo_fijo == false) ) 
							{
								ff = 'OP';
							}
							if(x.cantidad_anticipos > 0){
								ff = 'AN';
								var cantidad = x.cantidad_anticipos;
							}
						}
						resultado = ff +" ("+cantidad+")";
						return resultado;
					}
				},
				{
					data: "fecha_creacion"
				},	
				{
					data: "proveedor_n"
				},
				{
					data: "currency_code"
				},
				{
					data: "forma_pago"
				},
				{
					data: function(x){
						resultado = "";
						if(jQuery.isEmptyObject(x.cuenta_fondo) == false){
							resultado += x.cuenta_fondo+" ";
						}
						if(jQuery.isEmptyObject(x.numero_cuenta) == false){
							resultado += x.numero_cuenta;
						}

						return resultado;
                    }
				},
				{
					data: function(x){
                        return `<input type="text" readonly class="input-style numeric" value="${x.total_neto_pago}">`;
                    }
				},
				{
					data: function(x){
						resultado = "";
						if(jQuery.isEmptyObject(x.u_creacion_nombre) == false){
							resultado += x.u_creacion_nombre+" ";
						}
						if(jQuery.isEmptyObject(x.u_creacion_apellido) == false){
							resultado += x.u_creacion_apellido;
						}
						return resultado;
                    }
				},
				{
					data: "estado_n"
				},
				{
					data: function(x){
						//if(x.id_estado == 53||x.id_estado == 54){
							return `<a href="../resumenOp/${x.id}" class="btn btn-success" style="padding-left: 6px;padding-right: 6px;margin-right: 2px;" role="button"><i class="fa fa-fw fa-edit"></i></a>`;
						/*}else{
							return ``;
						}*/
                    }
				},
			],
			"order": [[ 0, "desc" ]],
			"columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false,
                    }
                ],
			"createdRow": function ( row, data, index ) {
					$(row).addClass('negrita');
								 
			}	

		}).on('xhr.dt', function (e, settings, json, xhr) {
			//Ajax event - fired when an Ajax request is completed.;
			$.unblockUI();
		}).on('draw', function () {
	       		/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

			    $('#listado tbody tr .numeric').inputmask("numeric", {
					radixPoint: ",",
					groupSeparator: ".",
					digits: 2,
					autoGroup: true,
					 // prefix: '$', //No Space, this will truncate the first character
					rightAlign: false
				});

    	});

	}, 300);
} //


		function limpiar()
		{
			$('#proveedor').val('').trigger('change.select2');
			$('#idMoneda').val('').trigger('change.select2');
			$('#id_estado').val('').trigger('change.select2');
			$('#periodo_creacion').val('');
			$('#nro_op').val('');
			// mostrarLibroCompra();

		}

		$("#botonExcel").on("click", function(e){ 
			e.preventDefault();
			$('#formProveedor').attr('method','post');
			$('#formProveedor').attr('action', "{{route('generarExcelOp')}}").submit();
		});


</script>

@endsection
