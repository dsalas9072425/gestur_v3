
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')

	@parent
	  
@endsection
@section('content')
<!-- En el encabezado de tu documento HTML -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

<style type="text/css">
	

.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

@-moz-document url-prefix() {
  fieldset { display: table-cell; }
}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

/*	.select2-container *:focus {
        outline: none;
    }*/


    .correcto_col { 
		height: 74px;
     }

     #divBanco {
	border: 2px solid #D0D1C2;
	border-radius: 5px;

     }

     .title_bank{
     	border-bottom-right-radius: 5px;
		border-bottom-left-radius: 5px;
		background-color: #D0D1C2;
    	padding: 3px 5px 5px 5px;
    	font-weight: 800;
    	margin-bottom: 2px;

     }

	 .text-bold {
		 font-weight: 800;
		 font-size: 16px;
	 }

	 .hr-m {
		margin: 0 0 5px 0;
	 }
	 .h3-m {
		 margin-bottom: 0;
	 }

	 .input-fp {
		font-weight: 800; 
		font-size: 20px;
	 }

	   .btn_op {
		 display: none;
		 margin-right: 7px; 
	 } 

	 .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
	.negrita {
		font-weight: bold;
	}

	.op_btn_imprimir
	{
		margin-right: 10px;
	}

	.adjuntoHiden {
		display: none;
	}


</style>




<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Pago OP</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

				<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="tabHome" data-toggle="tab" aria-controls="tabIcon21" href="#home"
							role="tab" aria-selected="true">
							<b><i class="fa fa-fw fa-user"></i> OP</b></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tabConfirm" data-toggle="tab" aria-controls="tabIcon22" href="#pago"
							role="tab" aria-selected="false">
							<b><i class="fa fa-fw fa-gear"></i> Resumen</b></a>
					</li>
				</ul>

				<div class="tab-content mt-1">
			<section id="home" class="tab-pane active" role="tabpanel">

				@forelse ($data as $dat)

				<!-- {{--======================================================OP VALIDA======================================== --}} -->

				<div class="row">

					<div class="col-6 col-sm-4 col-md-3">
						<div class="form-group">
							<label>Estado</label>
							<input type="text" class="form-control text-bold" readonly id="op_estado" name=""
								value="{{$dat->estado_n}}">
						</div>
					</div>


					<div class="col-6 col-sm-4 col-md-2">
						<div class="form-group">
							<label>Nro° OP</label>
							<input type="hidden" class="form-control text-bold" id="id_op" name=""
								value="{{$dat->id}}">

							<input type="text" class="form-control text-bold" readonly id="nro_op" name=""
								value="{{$dat->nro_op}}">
						</div>
					</div>

					<div class="col-6 col-sm-4 col-md-3">
						<div class="form-group">
							<label>Proveedor</label>
							<input type="text" class="form-control text-bold" readonly id="beneficiario_txt" name=""
								value="{{$dat->proveedor_n}}">
						</div>
					</div>


					<div class="col-6 col-sm-4 col-md-2">
						<div class="form-group">
							<label>Moneda</label>
							<input type="text" class="form-control text-bold" readonly name=""
								value="{{$dat->currency_code}}">
						</div>
					</div>

					<div class="col-3 col-sm-3 col-md-2">
	

						@if($permiso == 1 && $dat->id_estado == 52)
								@php 
									if(!$dat->cotizacion_especial){
										$cotizacion_sugerida = $cotizacion;
									}else{
										$cotizacion_sugerida = $dat->cotizacion_especial;
									}
								@endphp	
								<div class="row" style="margin-left: 0px; margin-right: 0px;">
									<label>Cotizacion</label><br>
									<div class="input-group">
										<div class="input-group-prepend" id="button-addon2" style="width: 100%;">
										<input type="text" class="form-control text-bold numeric" id="cotizacion_especial" name="" value="{{$cotizacion_sugerida}}">
										<button  class="btn btn-primary botonCotizacion" data-toggle="modal"  type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-hdd-o fa-lg"></i></button>
										</div>
									</div>	
								</div>
						@else 

						@php 
							if(!$dat->cotizacion_especial){
									$cotizacion_sugerida = $cotizacion;
								}else{
									$cotizacion_sugerida = $dat->cotizacion_especial;
								}
						@endphp	

							<div class="form-group">
								<label>Cotizacion</label>
								<input type="text" class="form-control text-bold numeric" readonly id="cotizacion_especial" name="" value="{{$cotizacion_sugerida}}">
							</div>
						@endif
					</div>
					<div class="col-3 col-sm-3 col-md-2">

						@if($permiso == 1 && $dat->id_estado == 52)
								@php 

								@endphp	
								<div class="row" style="margin-left: 0px; margin-right: 0px;">
									<label>Indice Cotización / Relación Monedas Extranjeras </label><br>
									<div class="input-group">
										<div class="input-group-prepend" id="button-addon2" style="width: 100%;">
										<input type="text" class="form-control text-bold numeric" id="cotizacion_especial_indice" name="" value="{{$dat->indice_cotizacion}}">
										<button  class="btn btn-primary botonCotizacion" data-toggle="modal"  type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-hdd-o fa-lg"></i></button>
										</div>
									</div>	
								</div>
						@else 
							<div class="form-group">
								<label>Indice Cotización / Relación Monedas Extranjeras</label>
								<input type="text" class="form-control text-bold numeric" readonly id="cotizacion_especial_indice" name="" value="{{$dat->indice_cotizacion}}">
							</div>
						@endif
				</div>
				<div class="col-3 col-sm-3 col-md-2 numeros_comprobantes" style="display: none;">
					<div class="form-group">
						<label>Numeros de Comprobantes</label>
						<input type="text" class="form-control text-bold" style="color:red;" name="numeros_comprobantes" 
						id="numeros_comprobantes"
						value="{{$dat->numeros_comprobantes}}">
					</div>
				</div>
				
				</div>
				<div class="row">

					<div class="col-12">
					<a href="../resumenOp/{{$dat->id}}" class="text-center btn btn-success btn-lg pull-right" style="margin-right: 20px;" role="button"><b>Descargar OP</b></a>

					<a href="{{route('reporteAsiento')}}?id_detalle_asiento={{$dat->id_asiento}}&nro_documento={{$dat->id}}&id_origen_asiento={{$id_origen_asiento}}"
							class="btn_op btn_asiento text-center btn btn-info btn-lg pull-right"
							style="margin-right: 20px;" role="button"><b>Ver Asiento</b></a>

						<button class="btn btn-info btn-lg pull-right btn_op btnAdjuntar"
								type="button"><b>Adjuntar Comprobante</b></button>

						<button class="btn btn-info btn-lg pull-right btn_op btnVerAdjunto"
								type="button"><b>Ver Comprobante</b></button>		


						@foreach ($btn as $boton)
						{{-- CARGA LOS BOTONES POR PERMISO DE PERSONAS --}}
						<?php echo $boton; ?>
						@endforeach
					</div>
				</div>



				<h4 class="mb-0 mt-1"> <b>Resumen</b></h4>
				<hr class="mt-0 ">
				<div class="row">


					<div class="col-12 col-sm-6 col-md-5">
						<form class="form-horizontal p-1" autocomplete="off" style="border: solid 2px #000; border-radius:15px;">


							<div class="form-group row">
								<label class="col-sm-3 col-form-label font-weight-bold">Total :</label>
								<div class="col-sm-9">
									<div class="input-group">
										<div class="input-group-append">
											<span
												class="input-group-text font-weight-bold">{{$dat->currency_code}}</span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly
											value="{{number_format($dat->total_pago,2,",",".")}}" id="total_pago_op">

									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label font-weight-bold" for="">Retención IVA :</label>
								<div class="col-sm-9">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-minus"></i></span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly
											id="total_retencion" value="{{number_format($dat->total_retencion,2,",",".")}}">
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label font-weight-bold" for="">Gastos Adm. :</label>
								<div class="col-sm-9">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-plus"></i></span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly
											value="{{number_format($dat->total_gastos,2,",",".")}}" id="gastos_administrativos">
									</div>
								</div>
							</div>
							
							<div class="form-group row">
								<label class="col-sm-3 col-form-label font-weight-bold" for="">Descuento Adm :</label>
								<div class="col-sm-9">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-minus"></i></span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly
											id="total_descuento" value="{{number_format($dat->total_descuento,2,",",".")}}">
									</div>
								</div>
							</div>	
							<div class="form-group row">
								<label class="col-sm-3 col-form-label font-weight-bold" for="">NCredito + Anticipo :</label>
								<div class="col-sm-9">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-minus"></i></span>
										</div>
										<?php
											$total_nc_an = $dat->total_anticipo + $dat->total_nota_credito;
										?>
										<input type="text" class="form-control text-bold format-number-control" readonly
											id="nc_ant" value="{{number_format($total_nc_an,2,",",".")}}">
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-3 col-form-label font-weight-bold" for="">A Pagar :</label>
								<div class="col-sm-9">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><b>=</b></span>
										</div>
										<?php 
											$total_neto = ((float)$dat->total_pago+(float)$dat->total_gastos) - ((float)$dat->total_retencion+(float)$dat->total_descuento+(float)$total_nc_an)
										?>
										<input type="text" class="form-control text-bold format-number-control" readonly
											value="{{number_format($total_neto,2,",",".")}}" id="total_op">
									</div>
								</div>
							</div>
						</form>
					</div>

				</div>

				<hr class="hr-m" style="margin: 0 0 20px 0">

				<div class="form-horizontal">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">* Entregado a :</label>
						<div class="col-sm-10" style="margin-bottom: 10px;">
							<input type="text" class="text-bold form-control clear_input_txt" readonly
								value="{{$dat->entregado}}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">* Concepto:</label>
						<div class="col-sm-10">
							<input type="text"  class="text-bold form-control clear_input_txt" readonly id="concepto" value="{{$dat->concepto}}">
						</div>
					</div>
				</div>


				<div class="row" style="margin: 0 0 20px 0">

					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Creado por :</label>
							@php
								if($dat->fecha_hora_creacion !=""){
									$fecha = explode(" ", $dat->fecha_hora_creacion);
									$fecha_format_creacion = date('d/m/Y', strtotime($fecha[0]))." ".$fecha[1];
								}else{
									$fecha_format_creacion ="";
								}		
							@endphp
							<input type="text" class="form-control text-bold" readonly name="" value="{{$dat->persona_creacion_n}} ({{$fecha_format_creacion}})">
						</div>
					</div>

					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Verificado por :</label>
							@php
								if($dat->fecha_hora_verificado !=""){
									$fecha = explode(" ", $dat->fecha_hora_verificado);
									$fecha_format_verificado = date('d/m/Y', strtotime($fecha[0]))." ".$fecha[1];
								}else{
									$fecha_format_verificado ="";
								}		
							@endphp
							<input type="text" class="form-control text-bold" readonly name="" value="{{$dat->persona_verificado_n}} ({{$fecha_format_verificado}})">
						</div>
					</div>

					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Autorizado por :</label>
							@php
								if(isset($dat->fecha_hora_autorizado)){
									$fecha = explode(" ", $dat->fecha_hora_autorizado);
									$fecha_format_autorizado = date('d/m/Y', strtotime($fecha[0]))." ".$fecha[1];
								}else{
									$fecha_format_autorizado ="";
								}		
							@endphp
							<input type="text" class="form-control text-bold" readonly name="" value="{{$dat->persona_autorizado_n}} ({{$fecha_format_autorizado}})">
						</div>
					</div>

					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Procesado por :</label>
							@php
								if(!empty($dat->fecha_hora_procesado)){
									$fecha = explode(" ", $dat->fecha_hora_procesado);
									$fecha_format_procesado = date('d/m/Y', strtotime($fecha[0]))." ".$fecha[1];
								}else{
									$fecha_format_procesado ="";
								}			
							@endphp
							<input type="text" class="form-control text-bold" readonly name=""
								value="{{$dat->persona_procesado_n}} ({{$fecha_format_procesado}})">
						</div>
					</div>


					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Anulado por :</label>
							<input type="text" class="form-control text-bold" readonly name=""
								value="{{$dat->persona_anulacion_n}} ({{$dat->fecha_format_anulacion}})">
						</div>
					</div>

				</div>


			</section>	



			<section id="pago" class="tab-pane" role="tabpanel">

				<h3 class="mt-1 mb-0"><b>Datos Proveedor</b></h3> 
				<hr class="mt-0">

				<div class="row">
					<div class="col-12">
						<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
							<table id="listadoProveedor" class="table table-hover table-condensed nowrap text-center"
								style="width: 100%;">
								<thead>
									<tr>
										<th>Proveedor</th>
										<th>Agente Retentor</th>
										<th>Banco</th>
										<th>Nro.Cuenta </th>
										<th>Beneficiario</th>
									</tr>
								</thead>
								<tbody style="text-align: center">
									<tr>
										<th>{{$dat->proveedor_n}}</th>
										<th>@if($dat->agente_retentor == "")
												@php 
													echo 'NO';
												@endphp
											@else
												@php 
													echo 'SI';
												@endphp									
											@endif
										</th>
										<th>{{$dat->banco}}</th>
										<th>{{$dat->nro_cuenta}}</th>
										<th>{{$dat->cheque_emisor_txt}}</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>


				<h3 class="mt-1 mb-0"><b>Items Seleccionados</b></h3>
				<hr class="mt-0">


				<div class="row">
					<div class="col-12">
						<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
							<table id="listadoItems" class="table table-hover table-condensed nowrap text-center"
								style="width: 100%;">
								<thead>
									<tr>
										<th>Comprobante</th>
										@if(Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa == 1)
											@if($dat->fondo_fijo == 1)
												<th>Proveedor</th>
											@else	
												<th>Reserva</th>
											@endif
										@else
											<th>Reserva/ Fac. Venta</th>
										@endif
										<th>Tipo de Documento</th>
										<th>Fecha</th>
										<th>Importe</th>
										<th>Saldo Factura</th>
										{{-- <th>Ver</th> --}}
										<th>Adjuntar/Ver</th>
										<th>Cuenta Contable</th>
										<th>Cotización</th>
									</tr>
								</thead>
								<tbody style="text-align: center">
								</tbody>
							</table>
						</div>
					</div>
				</div>



				<!--{{-- ============================================================================================================================
																DATATABLE RESUMEN DE ITEMS CARGADOS
			==============================================================================================================================--}}-->
				<div id="cuadroResumen" class="hidden">

					<h3 class="h3-m mt-1"><b>Gastos / Descuentos OP</b></h3>
					<hr class="hr-m">
					<div class="row">
						<div class="col-12">
							<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
								<table id="listCuentasLoad" class="table text-center" style="width:100%">
									<thead>
										<tr>
											<th>Cuenta Contable</th>
											<th>Concepto</th>
											<th>Moneda</th>
											<th>Importe</th>
										</tr>
									</thead>

									<tbody style="text-align: center">
										<tr>
											<td colspan="4"> No hay resultados</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<h3 class="h3-m mt-1"><b>Forma Pago</b></h3>
					<hr class="hr-m">
					<div class="row">
						<div class="col-12">
							<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
								<table id="listPagosLoad" class="table text-center" style="width:100%">
									<thead>
										<tr>
											<th>Operación</th>
											<th>Beneficiario</th>
											<th>Banco</th>
											<th>Moneda</th>
											<th>Documento</th>
											<th>Importe Pago</th>
										</tr>
									</thead>

									<tbody style="text-align: center">
										<tr>
											<td colspan="5"> No hay resultados</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!--DIV HIDDEN -->
				</div>



				<!-- ============================================================================================================================
																PIE DE OP CON RESUMEN DEL PROCESOS
			============================================================================================================================== -->
			

				<!--=================================================VISTA DE OP NO DISPONIBLE===============================================================  -->
				@empty
				<p style="font-size: 30px;font-weight: 800;">OP NO DISPONIBLE</p>
				@endforelse

				<section>
				</div>	

			</div>
			
		</div>
	</div>
</section>



{{-- ========================================
            MOSTRAR DOCUMENTO BANCARIO
    ========================================  --}}      

    <div class="modal fade" id="modalDocumentoMovimiento" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" style="margin-left: 9%;margin-top: 3%;">
        <div class="modal-content" style="width: 250%;height:700px">
          <div class="modal-header">
            <h4 class="modal-title"  style="font-weight: 800;">Adjunto <i style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
            <button type="button" class="closeModal btn-danger"  data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
             <div>
                <img src="" class="img-responsive hidden" id="imgMostrar" style="overflow: scroll;width: 100%;">
                <iframe id="verPdf" src="" class="hidden" style="width: 100%;height: 600px;"></iframe>
             </div>   
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

	

<!--======================================================
        MODAL DE ADJUNTO IMAGEN
========================================================== -->
    <!-- /.content -->
    <div id="modalAdjunto" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
        <!-- Modal content-->
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto <i class="fa fa-refresh fa-spin cargandoImg"></i></h2>
                </div>
                <input type="hidden" class = " form-control input-sm" id="idLinea" name="cantidad_habitaciones"/>
                <div class="modal-body">
                            <div class="content" id='output'>

                            </div>    
                            <form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('adjuntoDocumentosOpCopia')}}" autocomplete="off">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-12">
                                        <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="id_op" value= "{{$data[0]->id}}"/> 
										<input type="file" class="form-control" name="image[]" id="image" multiple accept=".pdf,.xls,.doc,.docx,.pptx,.pps,.jpeg,.bmp,.png,.jpg"/>  
										<h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: <span class="text-uppercase">pdf, xls, doc, docx, pptx, pps, jpeg, bmp, png y jpg.</span></b>
                                        </h4>
                                        <div id="validation-errors"></div>

                                      </div> 
                                    </div>  
                                  </div>  
							  </form>
							  
                            <div class="row">
                                <div class= "col-12">
                                    <button type="button" class="btn btn-danger font-weight-bold pull-right" data-dismiss="modal">Cerrar</button>                        
                                </div>
                            </div>
             
                    </div>
                </div>  
            </div>  
        </div>  


		
<!--======================================================
        MODAL DE ADJUNTO IMAGEN ITEM SELECCIONADOR RESUMEN
========================================================== -->
    <!-- /.content -->
	<div id="modalcapo" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 60%;margin-top: 13%;">
        <!-- Modal content-->
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto </h2>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                            <div class="content" id='outputcapo'>
                            </div>  
							
                             <form class="validator-form" id="uploadcapo" enctype="multipart/form-data" method="post" action="{{route('lcUploadItem')}}" autocomplete="off">
								<input type="hidden" name="id_libro_compra" id="id_libro_compra">
                                <div class="form-group">
                                  <div class="row">
                                     <div class="col-12 col-sm-12 col-md-12">
                                        <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="file" class="form-control" name="image" id="imagecapo" style="margin-left: 3%; width: 96%"/>  <h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: PNG, JPG y JPEG - La imagen para perfil de usuario sera valido en el proximo logueo de usuario.</b>
                                        </h4>
                                        <div id="validation-errors"></div>
                                      </div> 
                                    </div>  
                                  </div>  
                              </form>
							  
                            <div class="row">
                                <div class= "col-12">
                                    <button type="button" id="btnCancelarVSTour" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

                                    <button type="button" id="btnAceptarVoucher" class="btn btn-success pull-right" onclick="aceptarcapo()">Aceptar</button>
                                        
                                </div>
                            </div>
                        
                    </div>
                </div>  
            </div>  
        </div>  




<!-- DIV PARA LA IMPRESIÓN DE CHEQUES JOHANA 16/12/2019 -->
<div id='printDiv' style="display: none;">

</div>


	

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script> --}}
	
<script type="text/javascript">

var optionscapo = { 
                                beforeSubmit:  showRequestCapo,
                                success: showResponseCapo,
                                dataType: 'json' 
                        }; 

	/*{{--
		*CARGA LOS DATOS DE LA OP
		*SI SURGE UN ERROR SE CREA UNA CONSTANTE VACIA
	--}}*/
	var options = 
{
    beforeSubmit: showRequest,
    success: showResponse,
    dataType: 'json',
	error: function(){

		$.toast({
                heading: 'Error',
                text: 'Ocurrio un error al intentar subir la imagen.',
                position: 'top-right',
                showHideTransition: 'fade',
                icon: 'error'
            });

			$('.cargandoImg').hide();
	}
};

$('.cargandoImg').hide();

@forelse($data as $dat)

			$(document).ready(() => {
				$('.select2').select2();
				initCalendar();
				// main();
				controlEstado();
				fechasMomentJs();
				//montosOp();
				cargarImg();


	$('body').delegate('#image', 'change', function () {
    var input = document.getElementById('image');
    var files = input.files; // Obtener la lista de archivos seleccionados
	
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var tamaño = file.size;
	var error = true;
        if (tamaño > 0) {
            var tamañoEnKB = tamaño / 1024; // Convertir a KB
            if (tamañoEnKB > 3000) {
                $("#image").val('');
                $("#validation-errors").empty();
                $("#validation-errors").append('<div class="alert alert-error"><strong>El tamaño de la imagen supera el límite de 3MB</strong></div>');
				error = false;
				break;
            } else {
                $('.cargandoImg').show();
            }
        } else {
            $("#image").val('');
            $("#validation-errors").empty();
            $("#validation-errors").append('<div class="alert alert-error"><strong>Existe un error con el archivo</strong></div>');
			error = false;
			break;
        }
    }
			
			
			if (error) {
				$('#upload').ajaxForm(options).submit();
				
			}		
});


$('body').delegate('#imagecapo','change', function(){
                    //OBTENER TAMAÑO DE LA IMAGEN
                    var input = document.getElementById('imagecapo');
                    var file = input.files[0];
                    var tamaño = file.size;

                        if(tamaño > 0){

                        var tamaño = file.size/1000;
                        if(tamaño > 3000){
                            $("#imagecapo").val('');
                            $("#validation-errors").empty();
                            $("#validation-errors").append('<div class="alert alert-error"><strong>El tamaño de la imagen supera el limite de 3MB</strong><div>'); 
                        } else {

                            $('.cargandoImg').show();
                            $('#uploadcapo').ajaxForm(optionscapo).submit();  
                        }

                        } else {
                             $("#imagecapo").val('');
                             $("#validation-errors").empty();
                             $("#validation-errors").append('<div class="alert alert-error"><strong>Existe un error con el archivo</strong><div>'); 
                        }
                                        
                                });

			});

			/*{{--
				CONSTANTE CON DATOS PARA PROCESAR LA OP
			--}}*/

		const op = {
			total_neto: Number({{$dat->total_neto_pago}}),
			total_retencion : Number({{$dat->total_retencion}}),
			total_gastos : Number({{$dat->total_gastos}}),
			total_descuento : Number({{$dat->total_descuento}}),
			id_moneda: Number({{$dat->id_moneda}}),
			id: Number({{$dat->id}}),
			id_estado: Number({{$dat->id_estado}}),
			total_pago: 0,
			estado_op : true, //EDITABLE OP ?
			id_proveedor: Number({{$dat->id_proveedor}}),
			total:function(){ return this.total_neto + this.total_gastos - this.total_descuento;},
			adjunto_obligatorio: false,
			total_pago_op : function(){ return this.total_neto + this.total_retencion; },
			//adjunto1 : '{{$dat->adjunto}}', //cuidadito
			//adjunto1: '{{$dat->adjunto}}' ? '{{$dat->adjunto}}' : {!! json_encode($adjuntos) !!},
			//adjunto: '{{$dat->adjunto}}' ? '{{$dat->adjunto}}' : {!! json_encode($adjuntos) !!}
			adjunto: {!! json_encode($adjuntos) !!}
			

		}
		@empty
		const op = {
			total_neto: 0
		}

		@endforelse

		console.log(op);


function cargarConcepto() 
{  
        $('#operacion').prop('disabled', false)
		console.log($('#operacion').val());
		console.log($('#beneficiario_txt').val());
		console.log($('#nro_op').val());
        let msj = 'OP ' + $('#nro_op').val();

        if ($('#operacion').val() != '')
            msj += ' ' + $('#operacion').val().toUpperCase().substring(0, 4);

        msj += ' ' + $('#beneficiario_txt').val() + ' {{$facturasRecortadas}}';

        // console.log(msj);

        $('#concepto').val(msj);

}

function cargarPagos() 
{  


}

function main(){

	itemsDatatable();//CARGA ITEMS DE OP
	formatNumberControl();//FORMATEA LOS CAMPOS NUMERICOS
	controlEstado();//LLAMA A LAS FUNCIONES SEGUN EL ESTADO
	cargarPagos();
	formatMoneyFp();
	initFormaPago();
	initGastos();
/* ================================================================================
								FORMATEO DE VALORES
		================================================================================*/


	//DEFINIR FORMATO MONEDAS		
	const EURO = value => currency(value, {
		symbol: "",
		decimal: ',',
		separator: '.',
		precision: 2
	});
	const DOLAR = value => currency(value, {
		symbol: "",
		decimal: ',',
		separator: '.',
		precision: 2
	});
	const GUARANI = value => currency(value, {
		symbol: "",
		separator: '.',
		precision: 0
	});

}

function fechasMomentJs(){
	moment.updateLocale('es', {
    months : [
        "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ]
});
}


function controlEstado(){
	//DESACTIVAR BOTONES

	// $('.op_btn_rechazar').show('100');
	let str = $('#op_estado').val();
	$('#op_estado').val(str.toUpperCase());
	

	let estado = op.id_estado;
	//PENDIENTE
	if(estado === 52){
		$('#op_estado').css('color','#FF3C13');
		$('.op_btn_verificar').show('100');
		$('.op_btn_anular').show('100');
		
		initEditOp();

	}
	//VERIFICADO
	if(estado === 49){
		$('#op_estado').css('color','#6482F1');
		$('.op_btn_rechazar').show('100');
		$('.op_btn_autorizado').show('100');

		initProcesado();
		 
	}
	//AUTORIZADO
	if(estado === 50){
		$('#op_estado').css('color','#AEAE3E');
		$('.op_btn_procesar').show('100');
		$('.op_btn_rechazar').show('100');
		$('.btnAdjuntar').show();
		$('.numeros_comprobantes').show();
		initProcesado();
		
	}
	//PROCESADO
	if(estado === 53){
		$('#op_estado').css('color','#45AE3E');
		$('.op_btn_anular').show('100');
		op.estado_op =  false;
		$('.btn_asiento').show();
		$('.btnVerAdjunto').show();
		initProcesado();
		$('#numeros_comprobantes').prop('disabled',true);
		$('.numeros_comprobantes').show();
		
	

		@if(isset($formaPago[0]->fp_detalle[0]->id_tipo_pago))

			@if($formaPago[0]->fp_detalle[0]->id_tipo_pago == 4) 
				$('.op_btn_imprimir').show('100');
			@endif
		@endif
		
	}		

	// }
	//ANULADO
	if(estado === 54){
		$('#op_estado').css('color','red');
		op.estado_op =  false;
		initProcesado();
	}





}//

//ANULAR OP EN ESTADO PENDIENTE
$('.op_btn_anular').click(()=>{modalAnularOp();});
$('#btnConfirmarAnulacion').click(() =>{ anularOP();});

//RECHAZAR OP EN ESTADO VERIFICADO
$('.op_btn_rechazar').click(()=>{modalRechazarOp();});
$('#btnConfirmarRechazo').click(() =>{ rechazarOP();});


//VERIFICAR OP
$('.op_btn_verificar').click(()=>{redirectAplicarOp();});

//AUTORIZAR OP
$('.op_btn_autorizado').click(()=>{modalAutorizarOp();});
$('#btnConfirmarAutorizar').click(()=>{autorizarOp();});

//PROCESAR
$('.op_btn_procesar').click(()=>{ 
	if(validarAdjunto()){
		guardarNumeroComprobantes();
		redirectAplicarOp();
	} else {
		$.toast({
                heading: 'Atención',
                text: 'El adjunto es obligatorio, adjunte un documento por favor',
                position: 'top-right',
                showHideTransition: 'fade',
                icon: 'info'
            });
	}

	});


$('.op_btn_imprimir').click(()=>{imprimirCheque();});
$('.btnAdjuntar').click(()=>{ 
		$('#modalAdjunto').modal('show');
});
$('.btnVerAdjunto').click(()=>{ $('#modalAdjunto').modal('show');});



// $('.op_btn_autorizado').click(()=>{redirectAplicarOp();});



function redirectAplicarOp(){
    location.href ="{{ route('aplicarOp',['id'=>$dat->id]) }}";
}


function imprimirCheque() 
{
  $.ajax({
  	type: "GET",
  	url: "{{route('imprimirCheque')}}",
  	dataType: 'json',
  	data: 
  	{
  		id_op: op.id,
  	},
  	success: function(rsp)
  	{
  		// SE GENERA EL CHEQUE DINÁMICAMENTE
		cheque = '';
		cheque = `
		        <table style="width: 900px; margin-top: 20px; margin-left: 55px">
    <!-- <tr style="height: 30px"></tr> -->
                    <tr>
                        <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;"></td>
                        <td style="width: 400px"></td>
                        <td style="width: 500px"></td>
                        <td style="width: 180px"></td>
                        <td style="width: 180px; font-family: Arial; font-weight: bold; font-size: 18px; padding-bottom: 15px">****${rsp.data.importe}.#</td>
                    </tr>

                    <tr>
                        <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;">${rsp.data.fecha}</td>
                        <td style="width: 400px"></td>
                        <td style="width: 10px"></td>
                        <td style="width: 180px"></td>
                        <td style="width: 180px"></td>
                    </tr>

                    <tr>

                        <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold; ">${rsp.data.proveedor}</td>
                        <td style="width: 400px"></td>
                        <!-- <td style="width: 10px"></td>
                        <td style="width: 180px; text-align: center; font-family: Arial; font-weight: bold; font-size: 18px; text-align: left;">12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;19</td>
                         -->
                        <td colspan="2" style="width: 180px; text-align: center; font-family: Arial; font-weight: bold; font-size: 18px; text-align: center; padding-left: 195px;">${rsp.data.dia}&nbsp;&nbsp;&nbsp;&nbsp;${rsp.data.mes}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${rsp.data.anho}</td>
                        <td style="width: 180px"></td>
                    </tr>

                    <tr>
                        <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;  padding-left: 10px;"></td>
                        <!-- <td style="width: 400px"></td> -->
                        <td colspan="4" style="width: 500px; font-family: Arial; font-weight: bold; font-size: 18px; padding-left: 160px; height: 30px; padding-top: 10px">${rsp.data.proveedor}</td>
                       
                    </tr>

                    <tr>
                         <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;  padding-left: 10px;">${rsp.data.concepto}</td>
                        <!-- <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;"></td> -->
                        <!-- <td style="width: 400px"></td> -->
                        <td colspan="4" style="width: 500px; font-family: Arial; font-weight: bold; font-size: 18px; padding-left: 160px; padding-bottom: 50px; padding-top: 5px">${rsp.data.importeLetras} ----------</td>
                       
                    </tr>

                </table>       `;
		      
		$('#printDiv').html("");  
		$('#printDiv').html(cheque);

		var divToPrint = document.getElementById('printDiv');
		var newWin = window.open('', 'Print-Window');
		  newWin.document.open();
		  newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
		  newWin.document.close();
		  setTimeout(function() {
		    newWin.close();
		  }, 10);
  		
  	}	
  });

}

function estadoOP(opt) 
{

	return new Promise((resolve, reject) => {


	let msj;
	if (opt === 1)
		msj = "La OP fue Verificada!";
	if (opt === 2)
		msj = "La OP fue Autorizada!";
	if (opt === 3)
		msj = "La OP fue Rechazada!";
	if (opt === 4){
		msj = "La OP fue Anulada!";
		opt = 3;
	}
		
	
	let dataStrig = {
		id_op: op.id,
		opt: opt
	};


		$.ajax({
			type: "GET",
			url: "{{route('estadoOp')}}",
			dataType: 'json',
			data: dataStrig,
			error: function () {
				reject('Ocurrio un error en la comunación con el servidor.');
			},
			success: function (rsp) {

				if (rsp.err == true) {
					resolve(msj);
					location.reload(true);
				} else {
					reject(rsp.msj);
				}



			}
		}); //ajax

	});	

} //




	function montosOp()
	{
		let id_moneda = op.id_moneda;

		let total = op.total_neto
		$('#total_neto_op').val(op.total_neto);
		$('#total_pago_op').val(formatCurrency(id_moneda, op.total_pago_op));
		$('#total_retencion').val(formatCurrency(id_moneda, op.total_retencion));
		$('#gastos_administrativos').val(formatCurrency(id_moneda,op.total_gastos));
		$('#total_descuento').val(formatCurrency(id_moneda, op.total_descuento));
		$('#total_op').val(formatCurrency(id_moneda, op.total()));
		$('.class_total_op').val(formatCurrency(id_moneda, op.total()));
	}



/*{{-- === === === === === === === === === === === === === === === === === === === === === === === === =
								LOGICA DE VISTA EN ESTADO DIFERENTE A PROCESADO
	=== === === === === === === === === === === === === === === === === === === === === === === === == --}}*/

function initEditOp(){
	itemsDatatable();//CARGA ITEMS DE OP
	formatNumberControl();//FORMATEA LOS CAMPOS NUMERICOS
	cargarPagos();
}






/*{{-- === === === === === === === === === === === === === === === === === === === === === === === === =
								LOGICA DE VISTA EN ESTADO PROCESADO
	=== === === === === === === === === === === === === === === === === === === === === === === === == --}}*/

	function initProcesado() {
		$('#cuadroResumen').removeClass('hidden');
		itemsDatatable();//CARGA ITEMS DE OP
		loadGastos();
		loadFp();
		formatNumberControl();
		cargarPagos();
	}






/*{{-- === === === === === === === === === === === === === === === === === === === === === === === === =
								DATATABLE ITEMS SELECCIONADO
	=== === === === === === === === === === === === === === === === === === === === === === === === == --}}*/
//para abrir el modal al adjuntar en resumen item seleccionado


	function abrirModalAdjunto(id_libro) {
		let dataString = {idLibroCompra:id_libro};
          $.ajax({
                    type: "GET",
                    url: "{{ route('adjuntoLibroCompras')}}",
                    dataType: 'json',
                    data: dataString,
                    error: function(){
                            console.log('Error');
                    },
                    success: function(rsp){
                                    console.log(rsp);
                                    if(jQuery.isEmptyObject(rsp) == false){
                                        var archivoName =rsp;
                                        var extension = archivoName.split('.');
                                             archivo = extension[extension.length - 1];
                                             archivo = archivo.toLowerCase();

										let response = {archivo:archivoName,success:true};
										 showResponseCapo(response, null, null, null); 
                                    }else{
                                        $.toast({
                                                heading: '<b>Error</b>',
                                                position: 'top-right',
                                                text: 'Este Item no tiene adjunto.',
                                                width: '400px',
                                                hideAfter: false,
                                                icon: 'error'
                                            });
                                    }
                     }
                })               




	$('#id_libro_compra').val(id_libro);
    $("#modalcapo").modal("show");
}

function aceptarcapo() {
if($('#imagecapo').val() == ''){
	$.toast({
                heading: 'Atención',
                text: 'El adjunto es obligatorio, adjunte un documento por favor',
                position: 'top-right',
                showHideTransition: 'fade',
                icon: 'danger'
            });
}else{
	$("#modalcapo").modal("hide");
	console.log("#btnadjunto_"+$('#id_libro_compra').val());
	//$("#btnadjunto_"+$('#id_libro_compra').val()).attr('style','display: none;');
}
}

function itemsDatatable() 
{
	 $("#listadoItems").DataTable({
		destroy: true,
		paging: false,
		info: false,
		searching: false,
		ajax: {
			url: "{{route('cotrolSeleccionOp')}}",
			data: {
				id_op: op.id
			},
			error: function (jqXHR, textStatus, errorThrown) {

				// $.unblockUI();
			}

		},
		"columns": [
 

			{
				data: "nro_documento"
			}, 
			 {
				data: function(x){
							perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
							if(perfil == 1){
								@if($dat->fondo_fijo == 1)
									codigo = x.nombre;
								@else	
									codigo = x.cod_confirmacion;
								@endif
							}else{
								if(jQuery.isEmptyObject(x.factura_proforma) == false){
									codigo = x.factura_proforma;
								}else{
									codigo = x.factura_venta;
								}
							}
						if(jQuery.isEmptyObject(codigo) == true){
							codigo = x.id_proforma;
						}
	
					return codigo;
				}
			}, 
			{
				data: "tipo_documento_n"
			},
			{
				data: "fecha_format"
			},
			{
				data: function(x){
					//console.log(x);
					if(x.tipo_documento_n == "ANTICIPO" || x.tipo_documento_n == "NOTA DE CRÉDITO"){
						monto = -1 * x.monto;
					}else{
						monto =  x.monto;
					}
					return `<input type="text" readonly class="input-style numeric" value="${monto}">`;
				}
			},
			{
				data: function(x){
					if(jQuery.isEmptyObject(x.saldo_documento) == false){
						saldo =  x.saldo_documento;
						return `<input type="text" readonly class="input-style numeric" value="${saldo}">`;
					}else{
						return `<input type="text" readonly class="input-style" value="S/FAC">`;
					}
		
				}
			},
		/*	{
				data: function(x){
					console.log(x);
						boton = `<button onclick="getMovimientoDetalle(${x.id_libro})" class='btn btn-info btnEditHidden' style="margin-right: 10px;" type='button'><i class="fa fa-clone"></i></button>`;
						if('{{$data[0]->id_estado}}' == 52){
							boton +=`<button  class='btn btn-danger' onclick='anularDetalle(${x.id_op_detalle}, ${x.monto})' type='button'><i class='fa fa-fw fa-remove'></i></button>`;
						}else{
							boton +=``;
						}
						return boton;
					}
			},*/
					//arturo boton adjuntar en item
			{
				data: function(x) {
					//return `<button class='btn btn-success capoadjunto' id='btnadjunto_${x.id_libro}' onclick='abrirModalAdjunto(${x.id_libro})' type='button'><i class='fas fa-paperclip'></i></button>`;
					const buttonClass = (x.adjunto !== null && x.adjunto !== '') ? 'btn btn-success' : 'btn btn-danger';
        		return `<button class='capoadjunto ${buttonClass}' id='btnadjunto_${x.id_libro}' onclick='abrirModalAdjunto(${x.id_libro})' type='button'><i class='fas fa-paperclip'></i></button>`;
				}
			},
			{
			data: function(x) {
				if ( x.cuenta_10) {
				return x.cuenta_10;
				} else if ( x.cuenta_5) {
				return x.cuenta_5;
				} else if ( x.exenta) {
				return x.exenta;
				}else if ( x.retencion_iva) {
				return x.retencion_iva;
				}else if ( x.retencion_renta) {
				return x.retencion_renta;
				} else {
				return x.cuenta_10;
				}
			}},
			{
				data: function(x){
					
					return `<input type="text" readonly class="input-style numeric" value="${x.cambio}">`;
				}
			}


		],

		"columnDefs": [],
		// "aaSorting":[[2,"desc"]],
		"createdRow": function (row, data, iDataIndex) {
			$(row).addClass('negrita');


		},
		//AL FINALIZAR LA CARGA AGREGAR CONTROL DE COMAS Y PUNTO
		"initComplete": function (settings, json) {
			// $.unblockUI();
		}
	}).on('draw', function () {
	       		/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

			    $('#listadoItems tbody tr .numeric').inputmask("numeric", {
					radixPoint: ",",
					groupSeparator: ".",
					digits: 2,
					autoGroup: true,
					 // prefix: '$', //No Space, this will truncate the first character
					rightAlign: false
				});

    	});


}
 
/* === === === === === === === === === === === === === === === === === === === === === === === === =
										GASTOS OP
	=== === === === === === === === === === === === === === === === === === === === === === === === == */

function initGastos() 
{
	loadGastos();
}




function loadGastos() 
{
	return new Promise((resolve, reject) => { 

	let id_table  = 'listCuentasLoad';

	$.ajax({
		type: "GET",
		url: "{{route('getGastos')}}",
		dataType: 'json',
		data: {
			id_op: op.id
		},

		error: function () {

				msjToast('', 'error', jqXHR, textStatus);

			reject(false);
		},
		success: function (rsp) {

			if (rsp.err == true) {
				let cantItem = 0;
				let total_gasto = 0;
				let tabla;

				if(rsp.data.length > 0){
					$('#'+id_table+' tbody').empty();
			
				$.each(rsp.data, (index, value) => {
							var moneda = ''   
                            if(jQuery.isEmptyObject(value.moneda) == false){
                                moneda = value.moneda.currency_code;
                            }else{
                                moneda = 'USD';
                            }
                            if(jQuery.isEmptyObject(value.monto_moneda_original) == false){
                                monto_mostrar = value.monto_moneda_original;
                            }else{
                                monto_mostrar = value.monto;
                            }


					tabla = `
							<tr id="rowGasto_${cantItem}" class="tabla_fila_gasto">
							<td style="width: 30%; padding-right: 5px;padding-left: 5px;"><input type="text" readonly value = "${value.cuenta_contable.descripcion}" class = "form-control input-sm" /></td>
							<td style="width: 30%; padding-right: 5px;padding-left: 5px;"><input type="text" readonly value = "${value.concepto}" class = "form-control input-sm" /></td>
                            <td style="width: 15%; padding-right: 5px;padding-left: 5px;"><input type="text" readonly value = "${moneda}" class = "form-control input-sm" /></td>`;
													


						if(value.tipo == '-'){
							tabla += `
							<td style="width: 20%; padding-right: 5px;padding-left: 5px;">
							    <div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-minus"></i></span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly value="${value.monto_moneda_original}">
									</div>
								</div>
							</td>
							`;
						} else {
							tabla += `
							<td style="width: 20%; padding-right: 5px;padding-left: 5px;">
							  <div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-plus"></i></span>
										</div>
										<input type="text" class="form-control text-bold format-number-control" readonly
											value="${monto_mostrar}">
									</div>
								</div>
							</td>
							
							`;
						}

						tabla += `</tr>`;						
								
					$('#'+id_table+' tbody').append(tabla);
					cantItem++;
				});

	

				}
				resolve({resp:true});
				//LLAMAR A FORMATEAR NUMEROS
				formatNumberControl();

			} else {
				reject(false);
				msjToast('Ocurrió un error al intentar obtener los datos.', 'error');

			}


		} //

	});

  });
}





	/* === === === === === === === === === === === === === === === === === == === === === === === === === ==
									FORMA DE PAGO
	=== === === === === === === === === === === === === === === === === == === === === === === === === == */




function initFormaPago() 
{
	loadFp()	
}






function loadFp() {
    return new Promise((resolve, reject) => {
        let id_table = 'listPagosLoad';
        let dataString = {
            id_op: op.id
        };
        let tabla;

        $.ajax({
            type: "GET",
            url: "{{route('getFp')}}",// Usar la función route() para generar la URL
            data: dataString,
            async: false,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                msjToast('', 'error', jqXHR, textStatus);
                reject(false);
            },
            success: function (rsp) {
                console.log(rsp);

                if (rsp.err == true) {

                    if (rsp.data != null) {

                        if (rsp.data.fp_detalle.length > 0) {
                            $('#' + id_table + ' tbody').empty();
                        }

                        let cantItem = 0,
                            moneda = rsp.data.moneda.currency_code,
                            diferencia = 0,
                            total_bd = 0;

                        $.each(rsp.data.fp_detalle, (index, value) => {
                            let beneficiario = value.beneficiario_txt === "" ? "" : value.beneficiario_txt;

                            let comprobante = "";
                            if (!jQuery.isEmptyObject(rsp.data.fp_detalle[0])) {
                                comprobante = rsp.data.fp_detalle[0].nro_comprobante || "";
                            }

                            tabla = `
                                <tr id="rowPago_${cantItem}" class="tabla_filas">
                                    <td><input type="text" readonly value="${value.forma_pago.denominacion}" id="operacion" class="form-control input-sm" /></td>
                                    <td><input type="text" readonly value="${beneficiario}" id="beneficiario" class="form-control input-sm" /></td>
                                    <td><input type="text" readonly value="${value.banco_detalle.banco_cabecera.nombre} ${value.banco_detalle.numero_cuenta}" id="banco" class="form-control input-sm" /></td>
                                    <td><input type="text" readonly value="${moneda}" class="input_importe_pago form-control input-sm"/></td>
                                    <td><input type="text" readonly value="${comprobante}" class="form-control input-sm"/></td>
                                    <td><input type="text" readonly value="${value.importe_pago}" class="input_importe_pago form-control input-sm format-number-control"/></td>
                                </tr>`;

                            $('#' + id_table + ' tbody').append(tabla);
                            total_bd += clean_num(value.importe_pago, true);
                            cantItem++;
                        });

                        BlockFp();
                        if ($('#concepto').val() == "") {
                            cargarConcepto();
                        }

                        resolve({ resp: true });
                    } else {
                        reject(false);
                    }

                } else {
                    msjToast('Ocurrió un error al intentar recuperar los datos', 'error');
                    reject(false);
                }
            }
        });
    });
}





/* ==============================================================================================================================
                                        IMAGEN FORM LOGICA DE ADJUNTO
============================================================================================================================== */





function showRequest(formData, jqForm, options) 
{

    $("#validation-errors").hide().empty();
    return true;
}

function showResponse(rsp, statusText, xhr, $form) {
	//console.log(rsp.archivos.length);
    $('.cargandoImg').hide();
    for (var i = 0; i < rsp.archivos.length; i++) {
        var file = rsp.archivos[i];
		console.log(file);
        let adjunto, divImagen = ''; // Declaración de adjunto y divImagen aquí

        if (file.err == false) {
            $("#image").val('');
            $("#validation-errors").append('<div class="alert alert-error"><strong>' + file.errors + '</strong></div>');
            $("#validation-errors").show();
        } else {
            let file_name = file.archivo;

            //TIPO ADJUNTO
            console.log('sdsd', file.imagen);
            if (file.imagen) {
                adjunto = `
                <img class="card-img-top" style="max-width:150px;" src="{{asset('adjuntoDetalle/documentos_op')}}/${file_name}" alt="Adjunto de OP">
                `;
            } else {
                adjunto = `
                <img class="card-img-top" style="max-width:150px;" src="{{asset('adjuntoDetalle/documentos_op')}}/file.png" alt="Adjunto de OP">
                `;
            }

            divImagen = `
                <div class="row">
                    <div class="col-auto">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    ${adjunto}
                                    <p><a href="{{asset('adjuntoDetalle/documentos_op')}}/${file_name}" class="card-title"  target="_black"><b>VER</b></a></p>
                                    <a href="#" class="btn btn-danger"  onclick="eliminarImagen('${file_name}')"><b>Eliminar</b></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;
		
            $("#output").append(divImagen);
    
			op.adjunto.push({ archivo: file_name });
			cargarImg();
          
        }
    }
}



//const archivosSeleccionados = op.adjunto.map(file => file.archivo);
function eliminarImagen(archivo) 
{
    $('.cargandoImg').show();

    $.ajax({
        type: "GET",
        url: "{{route('fileDeleteOpCopia')}}",
        dataType: 'json',
        data: {
            dataFile: archivo,
			id_op:op.id
        },


        error: function (jqXHR, textStatus, errorThrown) {

            $.toast({
                heading: 'Error',
                text: 'Ocurrio un error al intentar eliminar la imagen.',
                position: 'top-right',
                showHideTransition: 'fade',
                icon: 'error'
            });

            $('.cargandoImg').hide();

        },
        success: function (rsp) {
            $('.cargandoImg').hide();

            if (rsp.rsp == true) {
                document.getElementById("image").value = "";
                $("#output").html('');
				$.toast({

                    heading: 'Atención',
                    text: 'El archivo fue eliminado.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'info'
                });
				//const objetosFiltrados = objetos.filter(objeto => objeto.archivo !== nombreArchivoExcluir);
				op.adjunto = op.adjunto.filter(objeto => objeto.archivo !== archivo);
				cargarImg();
				//op.adjunto = '';

            } else {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error al intentar eliminar la imagen.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                document.getElementById("image").value = "";
                $("#output").html('');

            } //ELSE
			
            //$("#image").prop('disabled', false);
        } //function


    });
} //FUNCTION 

function cargarImg() {
    if (Boolean(op.adjunto)) {
       	//let adjuntos = op.adjunto.split(','); // Obtener una lista de los nombres de los archivos adjuntos
        let divImagen = '';
        $('#output').html('');

        op.adjunto.forEach(function(file_name) {
			file_name = file_name.archivo;
			console.log(file_name);
            let extension = file_name.split('.').pop();
            let adjunto = '';

            // TIPO ADJUNTO
            if (extension == 'jpg' || extension == 'jpeg' || extension == 'bmp' || extension == 'png') {
                adjunto = `
                    <img class="card-img-top" style="max-width:150px;" src="/adjuntoDetalle/documentos_op/${file_name}" alt="Adjunto de OP">
                `;
            }else if (extension == 'pdf') {
                adjunto = `<i class="far fa-file-pdf" style="font-size: 100px; color: red;"></i>`;
            } else if (extension == 'xlsx' || extension === 'xls') {
                adjunto = `<i class="fas fa-file-excel" style="font-size: 100px; color: green;"></i>`;
            }else if (extension == 'docx' || extension === 'doc') {
                adjunto = `<i class="fas fa-file-word" style="font-size: 100px; color: blue;"></i>`;
            } else {
                adjunto = `
                    <img class="card-img-top" style="max-width:150px;" src="/images/file.png" alt="Adjunto de OP">
                `;
            }

            divImagen += `
                <div class="row">
                    <div class="col-auto">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    ${adjunto}
                                    <p><a href="/adjuntoDetalle/documentos_op/${file_name}" class="btn btn-info" target="_blank"><b>VER</b></a></p>
                                    <a href="#" class="card-title" style="font-size: 0.7rem; color: red;" onclick="eliminarImagen('${file_name}')"><b>Eliminar</b></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;
        });

        $("#output").append(divImagen);

        //$("#image").prop('disabled', true);
    }
}

//se agrega funcion ya que los adjuntos son array luegos de adjuntar multiple
function validarAdjunto() {
    console.log("op.adjunto:", op.adjunto);

    if (Array.isArray(op.adjunto) && op.adjunto.length > 0) {
        return true;
    }
    return false;
}


/*function validarAdjunto(){//funcion anterior donde validaba pero ahora ya es un array
console.log("op.adjunto:", op.adjunto);
	if(Boolean(op.adjunto)){
		return true;
	}
	return false;
}
*/

//item adjunto capos
//=====================IMAGEN FORM ========================//



                  function showRequestCapo(formData, jqForm, optionscapo) { 
                   
                                    $("#validation-errors").hide().empty();
                                    return true; 
                                  } 


                  function showResponseCapo(response, statusText, xhr, $form)  { 
                        $('.cargandoImg').hide();
                        if(response.success == false)
                          {
                       $("#imagecapo").val('');
                       $("#validation-errors").append('<div class="alert alert-error"><strong>'+ response.errors +'</strong><div>');
                       $("#validation-errors").show();

                        } else {
                            //$("#imagencapo").val(response.archivo); 
                          
                          var file_name = response.archivo;
                                $('#outputcapo').html('');


                                divImagen = `
                                    <div class="row ${file_name}">
                                        <div class="col-auto">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <img class="card-img-top" style="max-width:150px;" src="{{asset('adjuntoLc')}}/${file_name}" alt="Adjunto Iamgen Perfil">
                                                        <p><a href="{{asset('adjuntoLc')}}/${file_name}" class="btn btn-info" target="_black"><b>VER</b></a></p>
										                <a href="#" class="card-title" style="font-size: 0.7rem; color: red;" onclick="eliminarImagenCapo('${file_name}')"><b>Eliminar</b></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    `;

                   
                     $("#outputcapo").append(divImagen);
                                 
                             $("#imagecapo").prop('disabled',true);
                        }
                   }

                   function eliminarImagenCapo(archivo){
                        // $("#btn-imagen").on("click", function(e){
                            $.ajax({
                                type: "GET",
                                url: "{{route('fileAdjuntoEliminarLC')}}",//fileDelDTPlus
                                dataType: 'json',
                                data: {
                                            dataId: $('#id_libro_compra').val(),
                                            dataFile:archivo
                                            },
                                success: function(rsp){
                                    //$("#imagencapo").val("");
                                    $('#imagecapo').prop('disabled',false);
                                    $("#outputcapo").html('');
                                }
                        //   })      

                            });
                        } 


/* ==============================================================================================================================
                                        IMAGEN FORM LOGICA DE ITEM ADJUNTO
============================================================================================================================== */

// var options = 
// {
//     beforeSubmit: showRequestCapo,
//     success: showResponseCapo,
//     dataType: 'json',
// 	error: function(){

// 		$.toast({
//                 heading: 'Error',
//                 text: 'Ocurrio un error al intentar subir la imagen.',
//                 position: 'top-right',
//                 showHideTransition: 'fade',
//                 icon: 'error'
//             });

// 			$('.cargandoImg').hide();
// 	}
// };

// function showRequestCapo(formData, jqForm, options) 
// {

//     $("#validation-errors").hide().empty();
//     return true;
// }

// function showResponseCapo(rsp, statusText, xhr, $form) {
// 	//console.log(rsp.archivos.length);
//     $('.cargandoImg').hide();
//     for (var i = 0; i < rsp.archivos.length; i++) {
//         var file = rsp.archivos[i];
// 		console.log(file);
//         let adjunto, divImagen = ''; // Declaración de adjunto y divImagen aquí

//         if (file.err == false) {
//             $("#image").val('');
//             $("#validation-errors").append('<div class="alert alert-error"><strong>' + file.errors + '</strong></div>');
//             $("#validation-errors").show();
//         } else {
//             let file_name = file.archivo;

//             //TIPO ADJUNTO
//             console.log('sdsd', file.imagen);
//             if (file.imagen) {
//                 adjunto = `
//                 <img class="card-img-top" style="max-width:150px;" src="{{asset('adjuntoLc')}}/${file_name}" alt="Adjunto de OP">
//                 `;
//             } else {
//                 adjunto = `
//                 <img class="card-img-top" style="max-width:150px;" src="{{asset('adjuntoLc')}}/file.png" alt="Adjunto de OP">
//                 `;
//             }

//             divImagen = `
//                 <div class="row">
//                     <div class="col-auto">
//                         <div class="card">
//                             <div class="card-content">
//                                 <div class="card-body">
//                                     ${adjunto}
//                                     <p><a href="{{asset('adjuntoLc')}}/${file_name}" class="card-title"  target="_black"><b>VER</b></a></p>
//                                     <a href="#" class="btn btn-danger"  onclick="eliminarImagenCapo('${file_name}')"><b>Eliminar</b></a>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//             `;
		
//             $("#output").append(divImagen);
    
// 			op.adjunto.push({ archivo: file_name });
// 			cargarImgCapo();
          
//         }
//     }
// }



// //const archivosSeleccionados = op.adjunto.map(file => file.archivo);
// function eliminarImagenCapo(archivo) 
// {
//     $('.cargandoImg').show();

//     $.ajax({
//         type: "GET",
//         url: "{{route('fileDeleteOp')}}",
//         dataType: 'json',
//         data: {
//             dataFile: archivo,
// 			id_op:op.id
//         },


//         error: function (jqXHR, textStatus, errorThrown) {

//             $.toast({
//                 heading: 'Error',
//                 text: 'Ocurrio un error al intentar eliminar la imagen.',
//                 position: 'top-right',
//                 showHideTransition: 'fade',
//                 icon: 'error'
//             });

//             $('.cargandoImg').hide();

//         },
//         success: function (rsp) {
//             $('.cargandoImg').hide();

//             if (rsp.rsp == true) {
//                 document.getElementById("image").value = "";
//                 $("#output").html('');
// 				$.toast({

//                     heading: 'Atención',
//                     text: 'El archivo fue eliminado.',
//                     position: 'top-right',
//                     showHideTransition: 'fade',
//                     icon: 'info'
//                 });
// 				//const objetosFiltrados = objetos.filter(objeto => objeto.archivo !== nombreArchivoExcluir);
// 				op.adjunto = op.adjunto.filter(objeto => objeto.archivo !== archivo);
// 				cargarImgCapo();
// 				//op.adjunto = '';

//             } else {
//                 $.toast({
//                     heading: 'Error',
//                     text: 'Ocurrio un error al intentar eliminar la imagen.',
//                     position: 'top-right',
//                     showHideTransition: 'fade',
//                     icon: 'error'
//                 });
//                 document.getElementById("image").value = "";
//                 $("#output").html('');

//             } //ELSE
			
//             //$("#image").prop('disabled', false);
//         } //function


//     });
// } //FUNCTION 

// function cargarImgCapo() {
//     if (Boolean(op.adjunto)) {
//        	//let adjuntos = op.adjunto.split(','); // Obtener una lista de los nombres de los archivos adjuntos
//         let divImagen = '';
//         $('#output').html('');

//         op.adjunto.forEach(function(file_name) {
// 			file_name = file_name.archivo;
// 			console.log(file_name);
//             let extension = file_name.split('.').pop();
//             let adjunto = '';

//             // TIPO ADJUNTO
//             if (extension == 'jpg' || extension == 'jpeg' || extension == 'bmp' || extension == 'png') {
//                 adjunto = `
//                     <img class="card-img-top" style="max-width:150px;" src="/adjuntoLc/${file_name}" alt="Adjunto de OP">
//                 `;
//             }else if (extension == 'pdf') {
//                 adjunto = `<i class="far fa-file-pdf" style="font-size: 100px; color: red;"></i>`;
//             } else if (extension == 'xlsx' || extension === 'xls') {
//                 adjunto = `<i class="fas fa-file-excel" style="font-size: 100px; color: green;"></i>`;
//             }else if (extension == 'docx' || extension === 'doc') {
//                 adjunto = `<i class="fas fa-file-word" style="font-size: 100px; color: blue;"></i>`;
//             } else {
//                 adjunto = `
//                     <img class="card-img-top" style="max-width:150px;" src="/images/file.png" alt="Adjunto de OP">
//                 `;
//             }

//             divImagen += `
//                 <div class="row">
//                     <div class="col-auto">
//                         <div class="card">
//                             <div class="card-content">
//                                 <div class="card-body">
//                                     ${adjunto}
//                                     <p><a href="/adjuntoLc/${file_name}" class="btn btn-info" target="_blank"><b>VER</b></a></p>
//                                     <a href="#" class="card-title" style="font-size: 0.7rem; color: red;" onclick="eliminarImagenCapo('${file_name}')"><b>Eliminar</b></a>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//             `;
//         });

//         $("#output").append(divImagen);

//         //$("#image").prop('disabled', true);
//     }
// }

// function validarAdjunto(){

// 	if(Boolean(op.adjunto)){
// 		return true;
// 	}
// 	return false;
// }


/* ==============================================================================================================================
                                      			 MODALES
============================================================================================================================== */

function modalRechazarOp() 
{
		swal({
			title: "Rechazar OP",
			text: "¿Está seguro que desea rechazar la OP?",
			icon: "warning",
			showCancelButton: true,
			buttons: {
				cancel: {
					text: "No, Cancelar",
					value: null,
					visible: true,
					className: "btn-warning",
					closeModal: false,
				},
				confirm: {
					text: "Sí, Rechazar",
					value: true,
					visible: true,
					className: "",
					closeModal: false
				}
			}
		}).then(isConfirm => {

			if (isConfirm) {
				$.when(estadoOP(3)).then((a) => {
					swal("Éxito", a, "success");
				}, (b) => {
					swal("Cancelado", b, "error");
				});

			} else {
				swal("Cancelado", "La operación fue cancelada", "error");
			}
		});
}

function modalAutorizarOp() 
{
		swal({
			title: "Autorizar OP",
			text: "¿Está seguro que desea autorizar la OP?",
			icon: "warning",
			showCancelButton: true,
			buttons: {
				cancel: {
					text: "No, Cancelar",
					value: null,
					visible: true,
					className: "btn-warning",
					closeModal: false,
				},
				confirm: {
					text: "Sí, Autorizar",
					value: true,
					visible: true,
					className: "",
					closeModal: false
				}
			}
		}).then(isConfirm => {

			if (isConfirm) {
				$.when(estadoOP(2)).then((a) => {
					swal("Éxito", a, "success");
				}, (b) => {
					swal("Cancelado", b, "error");
				});

			} else {
				swal("Cancelado", "La operación fue cancelada", "error");
			}
		});
}

function modalAnularOp() 
{
		swal({
			title: "Anular OP",
			text: "¿Está seguro que desea anular la OP?",
			icon: "warning",
			showCancelButton: true,
			buttons: {
				cancel: {
					text: "No, Cancelar",
					value: null,
					visible: true,
					className: "btn-warning",
					closeModal: false,
				},
				confirm: {
					text: "Sí, Anular",
					value: true,
					visible: true,
					className: "",
					closeModal: false
				}
			}
		}).then(isConfirm => {

			if (isConfirm) {
				$.when(estadoOP(4)).then((a) => {
					swal("Éxito", a, "success");
				}, (b) => {
					swal("Cancelado", b, "error");
				});

			} else {
				swal("Cancelado", "La operación fue cancelada", "error");
			}
		});
}



/* {{-- === === === === === === MONEDA FP === === === === === === --}} */





function BlockFp(){
			 $('#btnAddFormaPago').prop('disabled',true);
		 }	

function UnblockFp(){
			 $('#btnAddFormaPago').prop('disabled',false);
		 }	







/*{{-- === === === === === === === === === === === === === === === === === ==
					FUNCIONES AUXILIARES
	=== === === === === === === === === === === === === === === === === == --}}*/

const aux = {
	msj: []
} 
{{--Para almacenar mensajes o mostrar mensajes en el momento segun error del ajax--}}

function msjToast(msj, opt = '', jqXHR = false, textStatus = '') {


	if (msj != '') {
		aux.msj.push('<b>' + msj + '</b>');
	}

	if (opt === 'info') {
		$.toast().reset('all');

		$.toast({
			heading: '<b>Atención</b>',
			position: 'top-right',
			text: aux.msj,
			width: '400px',
			hideAfter: false,
			icon: 'info'
		});
		aux.msj = [];

	} else if (opt === 'error') {
		$.toast().reset('all');



		if (jqXHR === false && textStatus === '') {
			msj = aux.msj;
		} else {

			errCode = jqXHR.status;
			errMsj = jqXHR.responseText;

			msj = '<b>';
			if (errCode === 0)
				msj += 'Error de conectividad a internet, verifica tu conexión.';
			else if (errCode === 404)
				msj += 'Error al realizar la solicitud.';
			else if (errCode === 500) {
				if (aux.msj.length > 0) {
					msj += aux.msj;
				} else {
					msj += 'Ocurrió un error en la comunicación con el servidor.'
				}

			} else if (errCode === 406) {
				msj += errMsj;
			} else if (errCode === 422)
				msj += 'Complete todos los datos requeridos'
			else if (textStatus === 'timeout')
				msj += 'El servidor tarda mucho en responder.';
			else if (textStatus === 'abort')
				msj += 'El servidor tarda mucho en responder.';
			else if (textStatus === 'parsererror') {
				msj += 'Error al realizar la solicitud.';
			} else
				msj += 'Error desconocido, pongase en contacto con el area técnica.';
			msj += '</b>';
		}


		$.toast({
			heading: '<b>Error</b>',
			text: aux.msj,
			position: 'top-right',
			hideAfter: false,
			width: '400px',
			icon: 'error'
		});

		aux.msj = [];
	} else if (opt === 'success') {
		$.toast().reset('all');

		$.toast({
			heading: '<b>Éxito</b>',
			text: aux.msj,
			position: 'top-right',
			hideAfter: false,
			width: '400px',
			icon: 'success'
		});
		aux.msj = [];

	}
} //



function formatNumberControl(){
	/*{{--FORMATEO DE MONEDAS EN CUALQUIER CAMPO NUMERO--}}*/
	$('.format-number-control').inputmask("numeric", {
		radixPoint: ",",
		groupSeparator: ".",
		digits: 2,
		autoGroup: true,
		// prefix: '$', //No Space, this will truncate the first character
		rightAlign: false
	});

	// $('.format-number-control').keyup(function (){
	//  this.value = (this.value + '').replace(/[^0-9]/g, '');
	// });
}



function initCalendar() {

	$('.single-picker').datepicker({
		format: "dd/mm/yyyy",
		language: "es"
	});
}

{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
		NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS--}}

function clean_num(n, bd = false) {

	if (n && bd == false) {
		n = n.replace(/[,.]/g, function (m) {
			if (m === '.') {
				return '';
			}
			if (m === ',') {
				return '.';
			}
		});
		return Number(n);
	}
	if (bd) {
		return Number(n);
	}
	return 0;

} //


function valid(n){
	try {
		if(n & !n.isEmpty()){
			return n
		}
	   return '';	
	} catch (error) {
		return '';	
	}
	
}


function blockUI(){

$.blockUI({
		centerY: 0,
		message: "<h2>Procesando...</h2>",
		css: {
		  color: '#000'
		 }
	   });

}





	function formatCurrency(id_moneda, value) {

		let importe = 0;


		console.log('formatCurrency', id_moneda, value);

		//PYG
		if (id_moneda == 111) {
			importe = GUARANI(value, {
				formatWithSymbol: false
			}).format(true);
		}
		//USD
		if (id_moneda == 143) {
			/*importe = DOLAR(value, {
				formatWithSymbol: false
			}).format(true);*/
		}

		return importe;
	}

(function($) {
   
  jQuery.isEmpty = function(obj){
    var isEmpty = false;
 
    if (typeof obj == 'undefined' || obj === null || obj === ''){
      isEmpty = true;
    }      
       
    if (typeof obj == 'number' && isNaN(obj)){
      isEmpty = true;
    }
       
    if (obj instanceof Date && isNaN(Number(obj))){
      isEmpty = true;
    }
       
    return isEmpty;
  }
 
})(jQuery);

	function anularDetalle(id_detalle, monto) {
		return swal({
                    title: "GESTUR",
                    text: "¿Seguro desea Eliminar el Detalle de la OP?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, eliminar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                          	//alert(id_detalle);
////////////////////////////////////////////////////////////////////////////////////////////////
								    $.ajax({
								        type: "GET",
								        url: "{{route('anularOpDetalle')}}",
								        dataType: 'json',
								        data: {
											id_op:id_detalle,
											monto: monto
								        },

								        error: function (jqXHR, textStatus, errorThrown) {

								            $.toast({
								                heading: 'Error',
								                text: 'Ocurrio un error al intentar eliminar la imagen.',
								                position: 'top-right',
								                showHideTransition: 'fade',
								                icon: 'error'
								            });
								        },
								        success: function (rsp) {
								        	if(rsp.codigo == "OK"){
								        		itemsDatatable();
												cargarPagos();
								        		swal("Éxito", rsp.mensaje, "success");	
								        		location. reload();
								        	}else{
								        		swal("Cancelado", rsp.mensaje, "error");
								        	}
								        } //function
								    });
///////////////////////////////////////////////////////////////////////////////////////////////
                        } else {
                            swal("Cancelado", "", "error");
                        }
            	});

	}
	
	function getMovimientoDetalle(idLibroCompra){
		let dataString = {idLibroCompra:idLibroCompra};
          $.ajax({
                    type: "GET",
                    url: "{{ route('adjuntoLibroCompras')}}",
                    dataType: 'json',
                    data: dataString,
                    error: function(){
                            console.log('Error');
                    },
                    success: function(rsp){
                                    console.log(rsp);
                                    if(jQuery.isEmptyObject(rsp) == false){
                                        var archivoName =rsp;
                                        var extension = archivoName.split('.');
                                             archivo = extension[extension.length - 1];
                                             archivo = archivo.toLowerCase();

                                        var getUrl = window.location;
                                        console.log(getUrl);
                                        var baseUrl = getUrl .protocol
                                        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                                            
                                            console.log(baseUrl);
                                        if(archivo === 'jpg' || archivo === 'png'|| archivo === 'jpeg'){
                                            $('#imgMostrar').removeClass('hidden');
                                            $('#verPdf').addClass('hidden');
                                            direccion = '../adjuntoLc/'+archivoName;
                                            $('#imgMostrar').attr('src',direccion);
                                        }
                                        if(archivo === 'pdf' || archivo === 'docx'){
                                            $('#imgMostrar').addClass('hidden');
                                            $('#verPdf').removeClass('hidden');
                                            direccion = '../adjuntoLc/'+archivoName;
                                            $('#verPdf').attr('src',direccion);
                                        }
                                        $('#modalDocumentoMovimiento').modal('show');

                                    }else{
                                        $.toast({
                                                heading: '<b>Error</b>',
                                                position: 'top-right',
                                                text: 'Este Item no tiene adjunto.',
                                                width: '400px',
                                                hideAfter: false,
                                                icon: 'error'
                                            });
                                    }
                     }
                })               

    }  


	$.ajax({
			type: "GET",
			url: "{{route('comprobarAsiento')}}",
			data: {
					id_data: op.id,
					option: 'OP'
				},
			dataType: 'json',
			error: function (jqXHR, textStatus, errorThrown) {},
			success: function (rsp) {
				if(rsp.status == 'ERROR'){
                    swal('El asiento Nro.'+rsp.id , 'El asiento generado por este proceso no está balanceado, favor pongase en contacto con Administración/Contabilidad para su verificación.',"error");
				}
			}
	})		


	$(".botonCotizacion").click(() => {
		$.ajax({
				type: "GET",
				url: "{{route('guardarCotizacionEspecialOp')}}",
				data: {
					id_op: op.id,
					indice_cotizacion : $("#cotizacion_especial_indice").val(),
					cotizacion: $("#cotizacion_especial").val()
				},
				dataType: 'json',
				error: function (jqXHR, textStatus, errorThrown) {
					$.toast({
								heading: '<b>Error</b>',
								text: 'Ocurrio un error en la comunicación con el servidor',
								position: 'top-right',
								hideAfter: false,
								width: '400px',
								icon: 'error'
							});
				},
				success: function (rsp) { 
					if (rsp.status == 'OK') {
						$.toast({
								heading: '<b>Éxito</b>',
								text: rsp.mensaje,
								position: 'top-right',
								hideAfter: false,
								width: '400px',
								icon: 'success'
							});

					} else {
						$.toast({
								heading: '<b>Error</b>',
								text: rsp.mensaje,
								position: 'top-right',
								hideAfter: false,
								width: '400px',
								icon: 'error'
							});
					}	

				} //success
			}); //ajax
	});

	function guardarNumeroComprobantes(){
		$.ajax({
				type: "GET",
				url: "{{route('op.guardarNumeroComprobantes')}}",
				data: {
					id_op: op.id,
					numeros_comprobantes : $("#numeros_comprobantes").val()
				},
				dataType: 'json',
				error: function (jqXHR, textStatus, errorThrown) {
					$.toast({
								heading: '<b>Error</b>',
								text: 'Ocurrio un error en la comunicación con el servidor',
								position: 'top-right',
								hideAfter: false,
								width: '400px',
								icon: 'error'
							});
				},
				success: function (rsp) { 
					if (rsp.status == 'OK') {
						$.toast({
								heading: '<b>Éxito</b>',
								text: rsp.mensaje,
								position: 'top-right',
								hideAfter: false,
								width: '400px',
								icon: 'success'
							});

					} else {
						$.toast({
								heading: '<b>Error</b>',
								text: rsp.mensaje,
								position: 'top-right',
								hideAfter: false,
								width: '400px',
								icon: 'error'
							});
					}	

				} //success
			}); //ajax
	}

</script>

@endsection
