@extends('masters')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
@endsection
@section('content')
    <div style="margin-left: 1%; margin-right: 2%;">
    	<br>
        <h1>Pagos</h1>
        <form action="" id="frmBusqueda" method="post">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="col-md-5">
                            <label>Desde</label>
                            <div class="input-group date">
								<div class="input-group-addon">
								    <i class="fa fa-calendar"></i>
								</div>
								<input type="text" name="desde" id="desde" class="form-control pull-right">
							</div> 	
                        </div>
                        <div class="col-md-5">
                            <label>Hasta</label>
                            <div class="input-group date">
								<div class="input-group-addon">
								    <i class="fa fa-calendar"></i>
								</div>
								<input type="text" name="hasta" id="hasta" class="form-control pull-right">
							</div> 	
                        </div>
                    	<div class="form-group col-md-2 fixheight">
                        <div class="col-md-2">
                            <label class="hidden-xs">&nbsp;</label>
                            <button type="button" style="width: 140px;  background-color: #e2076a; height: 40px;" id= "doConsultaPago" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Buscar Pago</button>
                            <label></label>
                            <a href="{{route('mc.generarPago')}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="width: 140px; padding-top: 10px;" role="button">Nuevo Pago</a>

                        </div>
                        <br>
                    </div>
				</div>
			</div>	
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12" id="divFacturas" >
						<div class="table-responsive">
							<br>
							<table id="pagos" class="table table-bordered table-hover">
								<thead>
									<tr class="bgblue">
										<th>Fecha</th>
										<th>N°Comprobante</th>
										<th>Tipo de Pago</th>
										<th>Banco</th>
										<th>Importe</th>
										<th>Moneda</th>
										<th>Usuario</th>
										<th>Agencia</th>
									</tr>
									</thead>
									<tbody>
									</tbody>
							</table>
						</div>
                    </div>
            	</div>
	        </form>
    </div>

@endsection

@section('scripts')
 @parent
	<script type="text/javascript">
			$("#desde").datepicker( "option", "dateFormat", 'dd/mm/yy' );
			$("#hasta").datepicker( "option", "dateFormat", 'dd/mm/yy' );
			$('#pagos').dataTable();
			$("#doConsultaPago").click(function(){
				var dataString = $("#frmBusqueda").serialize();
				/*$.blockUI({
		                centerY: 0,
		                message: '<div class="loadingC"><img style="width: 160px; height: 160px;" src="images/loading.gif" alt="Loading" /><br><h2>Procesando..... </h2></div>',
		                css: {
		                    color: '#000'
		                    }
		               });*/
				$.ajax({
					type: "GET",
					url: "{{route('mc.pagos')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
								console.log(rsp);
								var oSettings = $('#pagos').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();
								for (i=0;i<=iTotalRecords;i++) {
									$('#pagos').dataTable().fnDeleteRow(0,null,true);
								}
								$.each(rsp, function (key, item){
									var importe =  number_format(item.importe, 0)
										var dataTableRow = [
															item.fecha,
															'<b>'+item.nro_comprobante+'</b>',
															item.paymentmethod.paymentmethod_description,
															item.banco.nombre,
															importe,
															item.currency.currency_code,
															item.usuario.nombre_apellido,
															item.agencia.razon_social,
														];
										var newrow = $('#pagos').dataTable().fnAddData(dataTableRow);
								})	

					},
				});	

			});
			$("#doConsultaPago").trigger('click');

			function number_format(amount, decimals) {

			    amount += ''; // por si pasan un numero en vez de un string
			    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

			    decimals = decimals || 0; // por si la variable no fue fue pasada

			    // si no es un numero o es igual a cero retorno el mismo cero
			    if (isNaN(amount) || amount === 0) 
			        return parseFloat(0).toFixed(decimals);

			    // si es mayor o menor que cero retorno el valor formateado como numero
			    amount = '' + amount.toFixed(decimals);

			    var amount_parts = amount.split(','),
			        regexp = /(\d+)(\d{3})/;

			    while (regexp.test(amount_parts[0]))
			        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

			    return amount_parts.join(',');
			}
	</script>
@endsection

