@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
        	<br>
			<br>
			<br>
			<br>
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop" style="background-color: #fff;">
            	@include('flash::message')
				<div class="booking-information travelo-box">
					<div>
						<h1 class="subtitle hide-medium" style="font-size: xx-large;">Nueva Agencia</h1>
						<br>
						<form  method="get" class="validator-form" action="{{route('mc.doAddAgencias')}}" id="doEdit" name="agencias" enctype="multipart/form-data">
								<div class="form-group">
	                            	<div class="row">
	                            	
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Razón Social (*)</label>
	                                		<input type="text" required class = "Requerido form-control" name="razonSocial" id="razonSocial"/>
	                                    </div>
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Telefono</label>
	                                		<input type="text" class="form-control" name="telefono" id="telefono" />
										</div>
	                                </div>
	                            </div>
	                          	<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Email</label>
	                                		<input type="email" class="form-control" name="email" id="email"/>
	                                    </div>
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Ruc</label>
	                                		<input type="text" class="form-control" name="ruc" id="ruc"/>
	                                    </div>
	                                </div>
	                            </div>
								<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Comisión (*)</label>
	                                		<input type="number" min="0" max="1" step="0.01" required class = "Requerido form-control" name="comision" id="comision"/>
											<small>Obs: Se aceptan números enteros o decimales separados por puntos, ejemplo: 0.6</small>
	                                    </div>
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Activo (*)</label>
	                                		<select name="activo" required class = "Requerido form-control" class="input-text full-width required" id="activo">
												<option value="S">SI</option>
												<option value="N">NO</option>
											</select>
		                              	</div>
	                                </div>
	                            </div>
								
								<button type="submit" class="btn btn-primary" style=" background: #e2076a !important;" name="guardar" value="Guardar">Guardar</button>
	                    	</form>

                    </div> 
                </div>           
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
	
    </script>
    @endsection
