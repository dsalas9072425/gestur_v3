@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	<div>

            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
            	<form id="frmAgencias" class="contact-form" action="" method="post">
					<div class="booking-information travelo-box">
						<div>
							<h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Agencias</h1>
							<br>
							@include('flash::message')
							<br>
							<div class="row">
								<div class="form-group col-xs-12 col-sm-6">
									<a href="{{route('mc.agenciasAdd')}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px;" role="button">Nueva Agencia</a>
								</div>
							</div>
							<br>
							<div style= "background-color: #ffffff">
	            				<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
		                            <thead>
		                            	<tr>
		                                	<th>ID</th>
		                                  	<th>Razon Social</th>
		                                  	<th>Teléfono</th>
					                      	<th>Email</th>
		                                   	<th>Comisión</th>
		                                   	<th class="oculto">Ver</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                            @foreach($agencias as $agencia)
		                            	<tr>
		                            		<td><b>{{$agencia->id_agencia}}</b></td>
		                            	    <td><b>{{$agencia->razon_social}}</b></td>
		                                  	<td>{{$agencia->telefono}}</td>
					                      	<td>{{$agencia->email}}</td>
		                                   	<td><b>{{$agencia->comision}}</b></td>
		                                   	<td>
		                                   	<a href="{{route('mc.agenciasEdit', ['id' =>$agencia->id_agencia])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; background: #e2076a !important; width: 106px;" role="button">Editar</a>
		                                   	</td>
		                                </tr>   	
		                            @endforeach       	
		                            </tbody>
	                        	</table>
	                       	</div>	 
                        </div>
                    </div>    			
            </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
		});
	</script>
@endsection