@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
        	<br>
			<br>
			<br>
			<br>
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop" style="background-color: #fff;">
            	@include('flash::message')
				<div class="booking-information travelo-box">
					<div>
						<h1 class="subtitle hide-medium" style="font-size: xx-large;">Editar Agencia</h1>
						<br>
						<form  method="post" class="validator-form" action="{{route('mc.doEditAgencias')}}" id="doEdit" name="agencias" enctype="multipart/form-data">
								<div class="form-group">
	                            	<div class="row">
	                            	<input type="hidden" class="form-control" value="{{$agencias[0]->id_agencia}}" name="idAgencia" id="idAgencia"/>
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Razón Social (*)</label>
	                                		<input type="text" required class = "Requerido form-control" name="razonSocial" id="razonSocial"  value="{{$agencias[0]->razon_social}}"/>
	                                    </div>
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Telefono</label>
	                                		<input type="text" class="form-control" name="telefono" id="telefono" value="{{$agencias[0]->telefono}}"/>
										</div>
	                                </div>
	                            </div>
	                          	<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Email</label>
	                                		<input type="email" class="form-control" name="email" id="email"  value="{{$agencias[0]->email}}"/>
	                                    </div>
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Ruc</label>
	                                		<input type="text" class="form-control" name="ruc" id="ruc" value="{{$agencias[0]->ruc}}"/>
	                                    </div>
	                                </div>
	                            </div>
								<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Comisión (*)</label>
	                                		<input type="number" min="0" max="1" step="0.01" required class = "Requerido form-control" name="comision" id="comision"  value="{{$agencias[0]->comision}}"/>
											<small>Obs: Se aceptan números enteros o decimales separados por puntos, ejemplo: 0.6</small>
	                                    </div>
	                              		<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Activo (*)</label>
	                                		<select name="activo" required class = "Requerido form-control" class="input-text full-width required" id="activo">
												<option value="S">SI</option>
												<option value="N">NO</option>
											</select>
		                              	</div>

	                                </div>
	                            </div>
								
								<button type="submit" class="btn btn-primary" style=" background: #e2076a !important;" name="guardar" value="Guardar">Guardar</button>
	                    	</form>

                    </div> 
                </div>           
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/mc/scripts')
	<script>
	$(document).ready(function() {
		var activo = "{{$agencias[0]->activo}}";
		if(activo =="S"){
			$('#activo option[value="S"]').attr("selected", "selected");
		}else{
			$('#activo option[value="N"]').attr("selected", "selected");
		}

		var options = { 
	                beforeSubmit:  showRequest,
					success: showResponse,

			dataType: 'json' 
	        }; 
	 	$('body').delegate('#image','change', function(){
	 		$('#upload').ajaxForm(options).submit();  		
	 	}); 
	});		
	function showRequest(formData, jqForm, options) { 
		$("#validation-errors").hide().empty();
		$("#output").css('display','none');
	    return true; 
	} 
	function showResponse(response, statusText, xhr, $form)  { 
		if(response.success == false)
		{
			var arr = response.errors;
			$.each(arr, function(index, value)
			{
				if (value.length != 0)
				{
					$("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
				}
			});
			$("#validation-errors").show();
		} else {
			 $("#imagen").val(response.archivo);	
			 $("#output").html("<img src='"+response.file+"' />");
			 $("#output").css('display','block');
		}
	}
    </script>
    @endsection
