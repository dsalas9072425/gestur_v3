@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Nuevo Tipo Persona</h1>
            </div> 
            <br>
            <!-- /.box-header -->
            <div class="box-body">
                @include('flash::message')     
			    <form id="frmPaises" action="{{route('doAddTipoPersona')}}" method="get" autocomplete="nope"> 
			        {{ csrf_field() }}
					<div class="row">
					    <div class="col-md-3">
					    </div>
					    <div class="col-md-6">
							<label class="control-label">Denominación</label>
					        <input type="text" required class = "form-control" name="denominacion" id="denominacion" placeholder="Denominación" value=""/>
						</div>
					</div>	
					<div class="row">
					    <div class="col-md-3">
					    </div>	
					    <div class="col-md-6">
					    	<br>
							<label class="control-label">Abreviatura</label>
					        <input type="text" required class = "form-control" name="abreviatura" id="abreviatura"  value="" placeholder="Abreviatura" value=""/>
						</div>
					</div>	
					<div class="row">
					    <div class="col-md-3">
					    </div>
					    <div class="col-md-6">
							<br>
							<button type="submit" class="btn btn-primary" style=" background: #e2076a !important;" name="guardar" value="Guardar">Guardar</button>
						</div>
					</div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {


		});
	</script>
@endsection