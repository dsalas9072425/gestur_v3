@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style type="text/css">
		.error{
			color:red;
		}
	</style>
@endsection
@section('content')

<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Editar Reserva</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
	
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<form id="guardarEdicion" autocomplete="off">
					<div class="container">
						<div class="col-12">
							<div class="form-group">
								<input type="hidden" class="form-control" id="codigoReserva" name="codigoReserva"  value="{{$data[0]->booking_reference}}" >
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="url">Cliente</label>
								<input type="text" class="form-control" id="cliente" name="cliente" value="{{$data[0]->client_name}}" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="icono">Tarifa</label>
								<input type="text" class="form-control numeric" id="tarifa" name="tarifa" placeholder="" value="{{$data[0]->precio_total}}" onkeypress="return justNumbers(event)"; required>
							</div>
						</div>
                        <div class="col-12">
							<div class="form-group">
                                <label for="moneda">Tipo de Cambio</label>
                                <select class="form-control select2" name="moneda"  id="moneda" tabindex="1" style="width: 100%;">
                                    <option value="">Todos</option>
                                    @foreach($moneda as $moneda)
                                        <option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option> 
                                    @endforeach
                                </select>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="icono">Pais</label>
								<input type="text" class="form-control" id="pais" name="pais" placeholder="" value="{{$data[0]->destino_pais}}" style="width: 100%;" >
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="icono">Ciudad</label>
								<input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="" value="{{$data[0]->destino_ciudad}}" style="width: 100%;" >
							</div>
						</div>
    
						<div class="col-12">
							<div class="form-group">
								<label for="icono">Proveedor</label>
								<input type="text" class="form-control" id="proveedor" name="proveedor" placeholder="" value="{{$data[0]->supplier_name}}" style="width: 100%;" >
							</div>
						</div>
                        <div class="col-12">
							<div class="form-group">
								<label>Prestador</label>
								<select class="form-control select2" name="prestador" placeholder="Seleccione Persona" id="prestador" style="width: 100%;">
                                 <option value="{{$data[0]->lender_id}}">{{$data[0]->lender_name}}</option> 
								</select>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="servicio">Tipo de servicio<label class="error"></label></label>
                                <select class="form-control select2" name="servicio" required id="servicio" tabindex="2" style="width: 100%;">
                                    <option value="">Todos</option>
                                    @foreach($tipoReserva as $tipoReserva)
                                        <option value="{{$tipoReserva->id}}">{{$tipoReserva->denominacion}}</option> 
                                    @endforeach
                                 </select>    
								 
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="icono">Fecha Check-IN</label>
								<input type="text" class="form-control" id="checkin" name="checkin" placeholder="" value="{{$data[0]->booking_checkin}}" style="width: 100%;" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="icono">Fecha Check-OUT</label>
								<input type="text" class="form-control" id="checkout" name="checkout" placeholder="" value="{{$data[0]->booking_checkout}}" style="width: 100%;" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="icono">TKTL</label>
								<input type="text" class="form-control" id="tktl" name="tk" value="{{$data[0]->booking_deadline}}" style="width: 100%;" required>
							</div>
						</div>
			
					
						<a href="{{ route('verReservasNemo') }}" class="btn btn-secondary pull-right mb-1 ml-1">Cancelar</a>
						<button type="submit" id="guardarEdicion" class="btn btn-success pull-right mb-1 ml-1">Guardar</button>

 					</div>
				</form>		
			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>   
     <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/parsley.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
         
        var tipoServicio={{$data[0]->tipo_reserva_nemo_id}};
        var moneda={{$monedaSelect[0]->currency_id}};
        $("#servicio").val(tipoServicio).select2();
        $("#moneda").val(moneda).select2();
		$("#checkin, #checkout, #tktl").datepicker({
            dateFormat: "yyyy-mm-dd"
         });
         $('.numeric').inputmask("numeric", {
				    radixPoint: ",",
				    groupSeparator: ".",
				    digits: 2,
				    autoGroup: true,
				    rightAlign: false,
				    oncleared: function () { self.Value(''); }
			        });

            window.onload = (event) => {
                $('.select2').select2({
                    tag:true,
            // minimumInputLength: 2,
            //        allowClear: true,
            //        placeholder: '--Select Client--'
                });
                main();
            };
    function ordenarSelect(id_componente)
	    {
	      var selectToSort = jQuery('#' + id_componente);
	      var optionActual = selectToSort.val();
	      selectToSort.html(selectToSort.children('option').sort(function (a, b) {
	        return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
	      })).val(optionActual);
	    }

    function main() {


    $('#prestador').select2({
                                //maximumSelectionLength: 2,
                                placeholder: 'Presador'
                            });				
                            ordenarSelect('prestador');		

    $("#prestador").select2({
            ajax: {
                    url: "{{route('getPrestadoresListado')}}",
                    dataType: 'json',
                    placeholder: "TODOS",
                    delay: 0,
                    data: function (params) {
                                return {
                                    q: params.term, // search term
                                    page: params.page
                                        };
                    },
                    processResults: function (data, params){
                                var results = $.map(data, function (value, key) {
                                console.log(value);
                               /* return {
                                        children: $.map(value, function (v) {*/ 
                                            return {
                                                        id: value.pasajero_data+ " - "+value.tipo_persona,
                                                        text: value.pasajero_data+ " - "+value.tipo_persona
                                                    };
                                          /*  })
                                        };*/
                                });
                                        return {
                                            results: results,
                                        };
                    },
                    cache: true
                    },
                    escapeMarkup: function (markup) {
                                    return markup;
                    }, // let our custom formatter work
                    minimumInputLength: 3,
        });


}	

$('.select2').keypress(function(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
    });

//se agrega para que no envie vacio el tipo de servicio o se puede agregar n validaciones
	$("#guardarEdicion").validate({  // initialize plugin on the form
			debug: false,
			rules: {
				"servicio": {
					required: true
				},
	
			},
			messages: {
				"servicio": {
					required: "<br>Seleccione un Servicio"
				},

			}
		});



    $( "#guardarEdicion" ).submit(function(e) {
				$("#guardarEdicion").validate();
				if(!$("#guardarEdicion").isValid){
					e.preventDefault();
                	var dataString = $('#guardarEdicion').serialize();
					$.ajax({
						type: "GET",
						url: "{{route('guardarEditarNemo')}}",
						dataType: 'json',
						data: dataString,

							error: function(jqXHR,textStatus,errorThrown){
                            console.log(dataString);
							$.toast({
							heading: 'Error',
							text: errorThrown,
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});

							},
						success: function(rsp){
							if(rsp.estado == 1){
								$.toast({
								heading: 'Actualizado con exito.',
								text: 'La reserva se actualizo con exito',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'success'
								});
								window.location.href = "{{URL::to('verReservasNemo')}}"
							}else{		
								$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la actualización.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
									});
							}
						
						}
					});	
				}
            });

	</script>
@endsection