@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
<style type="text/css">
	.negrita{
		font-weight: bold;
	}

	.negrita:hover{
		  background-color: #F0F0F0;
	}

</style>

<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Lista de Reservas</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
                <form id="filtroReservas" class="form-inline">  
				        
            	  		<div class = "row">
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Código proveedor</label>
                                    <input type="text" class="form-control" name="proveedor"
                                    id="proveedor" value="" maxlength="100" />
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Código de referencia</label>
                                    <input type="text" class="form-control" name="referencia"
                                    id="referencia" value="" maxlength="100" />
								</div>
							</div>
							<div class="col-12 col-md-3">
								<div class="form-group">
									<label>Fecha de emisión</label>
                                    <input type="text" class="form-control" name="emision"
                                    id="emision" value="" maxlength="100" width=100%/>
								</div>
							</div>
						  
						</div>
						    
							

						<div class="col-12 mt-3">
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
							<button type="button" onclick="listarReservas()" id="btnGuardar" class="btn btn-info btn-lg pull-right mr-1"><b>Buscar</b></button>
						</div>		
            	  </form>


		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<table id="listadoReservasNemo" class="table">
                    <thead>
                        <th>Codigo de Reserva</th>
                        <th>Estado</th>
                        <th>Cliente</th>
                        <th>Tarifa</th>
                        <th>Fecha de reserva</th>
                        <th>Checkin</th>
                        <th>Checkout</th>
                        <th>Tktl</th>
                        <th>Moneda</th>
                        <th>Pais</th>
                        <th>Destino</th>
                        <th>Proveeder</th>
                        <th>Prestador</th>
                        <th>Tipo</th>
                        <th>Ver Respuesta</th>
                        <th>Ver Mensaje</th>
                        <th>Editar</th>
                    </thead>
                </table>
			</div>
		</div>

    </div>
</section>


<!--inicio modal al presional el boton ver respuesta!-->
<!-- Modal para mostrar respuesta -->
<div class="modal" tabindex="-1" role="dialog" id="respuestaGDS">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Respuesta del sistema NEMO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="respuestaContent">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>



<script>
$(document).ready(function() {
    listarReservas();
});
/*
$("#emision").datepicker({
    dateFormat: "yy-mm-dd"
});*/

function listarReservas() {
    // Crear dataTable
    $("#listadoReservasNemo").dataTable({
        "paging": true,
        "select": true,
        "searching": false,
        "processing": true,
        "destroy": true,
        "serverSide": true,
        "scrollX": true,
        "ajax": {
            "data": {
                "formSearch": $('#filtroReservas').serializeArray()
            },
            "url": "{{route('verDatosReservasNemo')}}",
        },
        "columns": [
            { data: 'booking_reference' },
            { data: 'booking_state_descripcion' },
            { data: 'client_name' },
            { data: 'precio_total' },
            { data: 'booking_creation_date' },
            { data: 'booking_checkin' },
            { data: 'booking_checkout' },
            { data: 'booking_deadline' },
            { data: 'moneda' },
            { data: 'destino_pais' },
            { data: 'destino_ciudad' },
            { data: 'supplier_name' },
            { data: 'lender_name' },
            { data: 'denominacion' },
            {
                data: 'id',
                render: function(data, type, row) {
                    var id = row['booking_reference'];
                    return (
                        `<a href="#" onclick="mostrarRespuesta('${id}',1)" class="btn btn-primary text-white pull-right" title="Editar Menú"><i class="fa fa-eye"></i></a>`
                    );
                },
            },
            {
                data: 'id',
                render: function(data, type, row) {
                    var id = row['booking_reference'];
                    return (
                        `<a href="#" onclick="mostrarRespuesta('${id}',0)" class="btn btn-primary text-white pull-right" title="Editar Menú"><i class="fa fa-eye"></i></a>`
                    );
                },
            },
            {
                data: 'id',
                render: function(data, type, row) {
                    var id = row['booking_reference'];
                    return (
                        `<a href="{{route('editarReservaNemo',['id'=>''])}}${id}" class="btn btn-primary text-white pull-right" title="Editar Menú"><i class="fa fa-pencil"></i></a>`
                    );
                },
            },
        ],
    });
}
/*
function mostrarRespuesta(id, tipo) {
    $.ajax({
        url: "{{route('getRespuestaGDS')}}",
        dataType: 'json',
        data: {
            'id': id,
            'tipo': tipo,
        },
        success: function(data) {
            if (data && data.reservaData && Array.isArray(data.reservaData)) {
                var reservaData = data.reservaData;
                var html = '';
                for (var i = 0; i < reservaData.length; i++) {
                    html += '<tr>';
                    html += '<td>' + reservaData[i].booking_reference + '</td>';
                    html += '<td>' + reservaData[i].booking_state_descripcion + '</td>';
                    html += '<td>' + reservaData[i].client_name + '</td>';
                    html += '<td>' + reservaData[i].precio_total + '</td>';
                    html += '<td>' + reservaData[i].booking_creation_date + '</td>';
                    html += '<td>' + reservaData[i].booking_checkin + '</td>';
                    html += '<td>' + reservaData[i].booking_checkout + '</td>';
                    html += '<td>' + reservaData[i].booking_deadline + '</td>';
                    html += '<td>' + reservaData[i].moneda + '</td>';
                    html += '<td>' + reservaData[i].destino_pais + '</td>';
                    html += '<td>' + reservaData[i].destino_ciudad + '</td>';
                    html += '<td>' + reservaData[i].supplier_name + '</td>';
                    html += '<td>' + reservaData[i].lender_name + '</td>';
                    html += '<td>' + reservaData[i].denominacion + '</td>';
                    html += '</tr>';
                }
                $('#reservaDataBody').html(html);
                $('#respuestaGDS').modal('show');
                console.log(data.respuesta);
            } else {
                console.error('La respuesta no contiene los datos de reserva esperados.');
            }
        },
        error: function() {
            console.error('Error en la solicitud Ajax.');
        }
    });
}

*/

function mostrarRespuesta(id, tipo) {
    $.ajax({
        url: "{{route('getRespuestaGDS')}}",
        dataType: 'json',
        data: {
            'id': id,
            'tipo': tipo,
        },
        success: function(data) {
            $('#respuestaContent').html(data.respuesta);
            $('#respuestaGDS').modal('show');
            console.log(data.respuesta);
        },
        error: function() {
        }
    });
}



function limpiar() {
    $('#emision').val('');
    $('#referencia').val('');
    $('#proveedor').val('');
    listarReservas();
}





</script>
@endsection