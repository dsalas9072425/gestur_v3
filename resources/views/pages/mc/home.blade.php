@extends('masters')

@section('title', 'Desarrollo Turístico Paraguayo')

@section('styles')
  @parent
  {{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css"> --}}
    <style type="text/css">
  
  .icoRojo{
    color:red;
  }

  .icoVerde{
    color:green;
  }

  .icoAmarillo{
    color:#C7A10A;
  }

  
  .pointerBoxTicket {
    cursor:pointer; 
    cursor: hand; 
  }

  .tableBg {
    font-weight: 800;
    font-size: large;
    text-align: center
  }
  .bgRed{ 
      background-color: #D13434;
      cursor:pointer; 
      cursor: hand; 
   }
  .bgYellow{ 
      background-color: #f39c12;
      cursor:pointer; 
      cursor: hand; 
   }

   .tableReserva{
      font-size: 9px;
      text-align: center
  }

   .overTable:hover{
      background-color: #F0F0F0;
  }
      
</style>
@endsection
@section('content')
    @include('flash::message')

     {{-- ========================================================================
                            VISTA PARA ADMIN_EMPRESA 
          ==========================================================================--}}
        @if($tipoUser[0]->id_tipo_persona == 3 || $tipoUser[0]->id_tipo_persona == 4 )


            <div class="row ">
                <div class="col-xs-12 col-md-3">
                  <div class="card">
                    <div class="card-content">
                        <div class="media align-items-stretch">
                            <div class="p-2 text-center bg-info bg-darken-2">
                                <i class="ft-shopping-cart font-large-2 white"></i>
                            </div>
                            <div class="p-2 bg-gradient-x-info white media-body">
                                <h5 class="text-bold-600">Facturación</h5>
                                <h5 class="text-bold-600 mb-0">
                                  <h5 class="text-bold-400 mb-0"><i class="ft-arrow-up"></i><span id="totalProformaPendiente"></span>000.000</span></h5>
                                <a href="{{ route('facturaIndex') }}"><div class="badge badge-primary">Ver</div></a>
                            </div>
                        </div>
                    </div>
                </div>
              </div>

              <div class="col-xs-12 col-md-3">
                <div class="card">
                  <div class="card-content">
                      <div class="media align-items-stretch">
                          <div class="p-2 text-center bg-teal bg-darken-2">
                              <i class="ft-trending-up font-large-2 white"></i>
                          </div>
                          <div class="p-2 bg-gradient-x-teal white media-body">
                              <h5>Renta / (Comision: <span id="comisionSpan">0</span> %)</h5>
                              <h5 class="text-bold-400 mb-0"><i class="ft-arrow-up"></i><span id="facturacionRenta"></span>000.000</span></h5>
                              <a href="{{ route('verLiquidaciones') }}"><div class="badge badge-primary">Ver</div></a>
                          </div>
                      </div>
                  </div>
              </div>
            </div> 



            <div class="col-xs-12 col-md-3">
              <div class="card">
                <div class="card-content">
                    <div class="media align-items-stretch">
                        <div class="p-2 text-center bg-yellow bg-lighten-1 bg-darken-2">
                          <i class="ft-sun font-large-2 white"></i>
                        </div>
                        <div class="p-2 bg-yellow bg-lighten-1 bg-darken-2 white media-body">
                            <h5>Meta</h5>
                            <h5 class="text-bold-400 mb-0"><i class="ft-arrow-up"></i><span id="meta">0000</span> USD</h5>
                            <a href="#"><div class="badge badge-primary">Ver</div></a>
                        </div>
                    </div>
                </div>
            </div>
          </div>


          <div class="col-xs-12 col-md-3">
            <div class="card">
              <div class="card-content">
                  <div class="media align-items-stretch">
                      <div class="p-2 text-center bg-danger bg-darken-2">
                          <i class="ft-triangle font-large-2 white"></i>
                      </div>
                      <div class="p-2 bg-gradient-x-danger white media-body">
                          <h5>Por facturar</h5>
                          <h5 class="text-bold-400 mb-0"><i class="ft-arrow-up"></i><span id="totalProformaPendiente"></span> USD</h5>
                          <a href="{{ route('listadosProformas') }}"><div class="badge badge-primary">Ver</div></a>
                      </div>
                  </div>
              </div>
          </div>
          </div>
      </div>
          
      <div class="row ">

        <div class="col-xs-12 col-md-6">
          <div class="card">
            <div class="card-header">
                <h4 class="card-title">Facturación/Meta</h4>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show" aria-expanded="true">
                <div class="card-body" style="height:500px;"> 
                  <div class="chart" id="line-chart" style="height:400px;"></div>
                </div>
            </div>
        </div>
        </div>


        <div class="col-xs-12 col-md-6" style="padding-left: 0px;">
          <div class="card">
            <div class="card-header">
                <h4 class="card-title">Reservas Pendientes Nemo</h4>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show" aria-expanded="true">
                <div class="card-body" style="padding-left: 15px;"> 
                    <!--<div id="donut-chart" style="height: 300px;"></div>-->
                      <table id="listadoReserva" class="table" style="width:100%">
                            <thead>
                              <tr>
                                <th style="font-size: 11px;text-align: center;">Fecha<br>Reserva</th>
                                <th style="font-size: 11px;text-align: center;">Deadline</th>
                                <th style="font-size: 11px;text-align: center;">Cod.<br>Proveedor</th>
                                <th style="font-size: 11px;text-align: center;">Cod.<br>Nemo</th>
                                <th style="font-size: 11px;text-align: center;">Cliente</th>
                                <th style="font-size: 11px;text-align: center;">Costo</th>
                                <th style="font-size: 11px;text-align: center;">Servicio</th>
                              </tr>
                            </thead>
                            <tbody class="tableReserva">

                            </tbody>
                      </table>
                </div>
            </div>
        </div>
        </div>

      </div>
        
    
  @endif 
     




  
  {{-- ========================================================================
                            VISTA PARA ADMIN_EMPRESA 
    ==========================================================================--}}
     @if($tipoUser[0]->id_tipo_persona == 2 || $tipoUser[0]->id_tipo_persona == 6) 
        @if($user->empresa == 1 ||$user->empresa == 17||$user->empresa == 21)
            <div class="row ">
              <!--==============================================
                      EMITIDOS VS FACTURADOS
                ================================================= -->
              <div class="col-xs-12 col-md-6">
                <div class="card" style="height: 550px;  overflow-x: auto;">
                  <div class="card-header">
                      <h4 class="card-title">EMITIDOS VS FACTURADOS</h4>
                      {{-- <div class="heading-elements">
                          <ul class="list-inline mb-0">
                              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                              <li><a data-action="close"><i class="ft-x"></i></a></li>
                          </ul>
                      </div> --}}
                  </div>
                  <div class="card-content collapse show" aria-expanded="true">
                      <div class="card-body"> 
                    <div id="cuadro"></div>
                      </div>
                  </div>
                 
              </div>
              </div>

                <!--==============================================
                      DISPONIBLES - EN PROFORMAS
                ================================================= -->
              <div class="col-xs-12 col-md-6">
                <div class="card" style="height: 550px; overflow-x: auto;">
                  <div class="card-header">
                      <h4 class="card-title">DISPONIBLES - EN PROFORMAS</h4>
                      {{-- <div class="heading-elements">
                          <ul class="list-inline mb-0">
                              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                              <li><a data-action="close"><i class="ft-x"></i></a></li>
                          </ul>
                      </div> --}}
                  </div>
                  <div class="card-content collapse show" aria-expanded="true">
                      <div class="card-body"> 
                        <div class="table-responsive">
                          <table id="listadoTicket" class="table" style="width:100%">
                            <thead>
                      <tr>
                        <th>ORIGEN</th>
                        <th>DISPONIBLES</th>
                        <th>EN PROFORMA</th>
                      </tr>
                            </thead>
                            
                            <tbody class="tableBg">

                        </tbody>
                          </table>
                      </div>


                      </div>
                  </div>
                 
              </div>
              </div>
            </div>  
        @endif     

   <!--==============================================
                FACTURACIÓN
      ================================================= -->
  <div class="row ">


    <div class="col-xs-12 col-md-6">
      <div class="card">
        <div class="card-header">
            <h4 class="card-title">FACTURACIÓN</h4>

        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
          
              <div id="container" style="min-width: 450px; height: 350px; max-width: 650px; margin: 0 auto">

            </div>
        </div>
       
    </div>
    </div>
  </div>



      <div class="col-xs-12 col-md-6">
        <div class="card">
          <div class="card-header">
              <h4 class="card-title">Facturación Neta por Sucursal</h4>
          </div>
          <div class="card-content collapse show" aria-expanded="true">
              <div class="card-body"> 
            
                <div id="cuadro2" style="min-width: 90%;height: 350px;max-width: 90%;margin: 0 auto;"></div>

              </div>
          </div>
        
      </div>
      </div>
    </div>
 @endif





   

@endsection



@section('scripts')
    @parent
  
  <!-- Morris.js charts -->
  <script src="{{asset('mC/bower_components/raphael/raphael.min.js')}}"></script>
  <script src="{{asset('mC/bower_components/morris.js/morris.min.js')}}"></script>

  <!-- FLOT CHARTS -->
  <script src="{{asset('mC/bower_components/Flot/jquery.flot.js')}}"></script>
  <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
  <script src="{{asset('mC/bower_components/Flot/jquery.flot.resize.js')}}"></script>
  <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
  <script src="{{asset('mC/bower_components/Flot/jquery.flot.pie.js')}}"></script>
  <!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
  <script src="{{asset('mC/bower_components/Flot/jquery.flot.categories.js')}}"></script>
  <!-- Page script -->


{{--Para usuarios tipo Admin_empresa--}}
 @if($tipoUser[0]->id_tipo_persona == 2 || $tipoUser[0]->id_tipo_persona == 4)
  <!-- highcharts -->
  <script src="{{asset('mC/js/highcharts.min.js')}}"></script>
  <script src="{{asset('mC/js/exporting.min.js')}}"></script>
  <script src="{{asset('mC/js/export-data.min.js')}}"></script>
 
 
@endif

  <script type="text/javascript">


     $("#listado").dataTable({
         "aaSorting":[[0,"desc"]],
         "searching": false
        });
     $("#listadoReserva").dataTable({
          "pageLength": 5
      });
{{--Para usuarios tipo vendedor empresa y  Supervisor Empresa--}}
 @if($tipoUser[0]->id_tipo_persona == 3 )

    
    $(function () {


  ajaxTable();
   function ajaxTable() {
    setTimeout(ajaxTable,90000);




        $.ajax({
            type: "GET",
            url: "{{route('ajaxTable')}}",
            dataType: 'json',

             error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },

            success: function(rsp){
        
              // console.log(rsp);
              var oSettings = $('#listado').dataTable().fnSettings();
              var iTotalRecords = oSettings.fnRecordsTotal();
              for (i=0;i<=iTotalRecords;i++) {
                $('#listado').dataTable().fnDeleteRow(0,null,true);
              }



              $.each(rsp.comision, function (key, item){

                var ico = `<i class="${item.ico}"></i><div style="display: none;">${item.peso}</div>`;

                var dataTableRow = [

                ico,
                 @if($tipoUser[0]->id_tipo_persona == 4)
                item.agente_n,
                 @endif
                item.agencia_n,
                item.vendedor_n,
                item.total_venta,
                item.meta


                          ];

                var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
                var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

            

              });



            }//cierreFunc 
        });//Cierre ajax

        $.ajax({
            type: "GET",
            url: "{{route('ajaxReserva')}}",
            dataType: 'json',

             error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },

            success: function(rsp){
        
              // console.log(rsp);
              var oSettings = $('#listadoReserva').dataTable().fnSettings();
              var iTotalRecords = oSettings.fnRecordsTotal();
              for (i=0;i<=iTotalRecords;i++) {
                $('#listadoReserva').dataTable().fnDeleteRow(0,null,true);
              }
              $.each(rsp, function (key, item){
                if(item.cod_confirmacion == null){
                  codigo = item.cod_nemo;
                }else{
                  codigo = item.cod_confirmacion;
                }  
                var dataTableRow = [
                        '<b>'+item.fecha_alta_format+'</b>',
                        '<b>'+item.deadline+'</b>',
                        '<b>'+item.cod_confirmacion+'</b>',
                        '<b>'+item.cod_nemo+'</b>',
                        '<b>'+item.nombres+'</b>',
                        '<b>'+item.costo_proveedor+'</b>',
                        '<b>'+item.producto_n+'</b>'
                  ];
                var newrow = $('#listadoReserva').dataTable().fnAddData(dataTableRow);
                var nTr = $('#listadoReserva').dataTable().fnSettings().aoData[newrow[0]].nTr;

            

              });
            }//cierreFunc 
        });//Cierre ajax

      
      
    }//function


    
    /*
     * END DONUT CHART
     */
    function fechaActual(){
      var n = new Date();
       n = moment(n).format("YYYY-MM-DD")
      return n;
    }
    function fechaMesActual(){
      var n = new Date();
       n = moment(n).format("YYYY-MM-01")
      return n;
    }

    





    


  });

function conversor(m){
        m = Math.round(m);
        m = new Intl.NumberFormat("de-DE").format(m);
        // m = parseFloat(m);
        
        return m;

}




    datosChart();
function datosChart(){
setTimeout(datosChart,90000);

  
    $.ajax({
                        type: "GET",
                        url: "{{route('indexGraphAjax')}}",

                       error: function(jqXHR,textStatus,errorThrown){

                          console.log("Ocurrio un error en la comunicación con el servidor");

                        },
                        success: function(rsp){

           


                          $('#totalFacturacion').html("");
                          $('#totalFacturacion').html((rsp.facturacionTotal.length > 0 ) ? conversor(rsp.facturacionTotal[0].total) : 0);
                          $('#facturacionRenta').html("");
                          $('#facturacionRenta').html((rsp.facturacionRenta.length > 0) ? conversor(rsp.facturacionRenta[0].total) : 0);
                          $('#totalProformaPendiente').html("");
                          $('#totalProformaPendiente').html((rsp.totalProformaPendiente.length > 0) ? conversor(rsp.totalProformaPendiente[0].total) : 0);





                          $('#comisionSpan').html("");
                          $('#comisionSpan').html((rsp.comisionVendedor.length > 0) ? rsp.comisionVendedor[0].get_porcentaje_comision : 0);
                          $('#meta').html("");
                          $('#meta').html((rsp.metaMes.length  > 0) ? conversor(rsp.metaMes[0].facturacion) : 0);
                          


                          //FACTURACION / META
                          if(rsp.grafico1.length > 0){

                            var meta = (rsp.metaMes.length  > 0) ? rsp.metaMes[0].facturacion : 0;
                           if(rsp.totalVenta < meta){
                              meta = rsp.totalVenta + 10000;
                              meta = parseInt( meta);
                              // console.log(meta);
                           }

                         var line = new Morris.Line({
                                element          : 'line-chart',
                                data             : rsp.grafico1,
                                resize           : true,
                                xkey             : 'y',
                                ymax             : meta, //meta
                                xLabelFormat     : function (x) { return  moment(x).format("DD/MM/YYYY"); },
                                yLabelFormat     : function (y) { return  parseInt(y); },
                                gridTextColor    : '#000',
                                gridTextWeight   : '900',
                                ykeys            : ['item1','sorned'],
                                labels           : ['item 1,sorned'],
                                hideHover        : 'auto',
                                lineColors       : ['#3c8dbc']
                              });
                          }

                            
                              /*
                               * DONUT CHART
                               * -----------
                               */
                              

                              var datos = {
                                'online' : function (){

                                  if(rsp.grafico2.autoTotal > 0 ){
                                  var autoTotal = rsp.grafico2.autoTotal * 100;
                                  var total = rsp.grafico2.total;
                                  var resp = (total > 0) ? autoTotal / total : 0; 
                                  return resp;
                                } else {return 0;}

                                },
                                'offline' : function(){

                                  if(rsp.grafico2.manualTotal > 0){
                                  var manualTotal = rsp.grafico2.manualTotal * 100;
                                  var total = rsp.grafico2.total;
                                  var resp = (total > 0) ? manualTotal / total : 0; 
                                  return resp;
                                  } else { return 0;}

                                }
                              }

                                

                            var donutData = [
                                { label: 'Online', data: datos.online(), color: '#3c8dbc' },
                                { label: 'Offline', data: datos.offline(), color: '#0073b7' }
                              ]

                              


                            /*  $.plot('#donut-chart', donutData, {
                                series: {
                                  pie: {
                                    show       : true,
                                    radius     : 1,
                                    innerRadius: 0.5,
                                    label      : {
                                      show     : true,
                                      radius   : 2 / 3,
                                      formatter: labelFormatter,
                                      threshold: 0.1
                                    }

                                  }
                                },
                                legend: {
                                  show: false
                                }
                              });*/

                              

                        
                          
                                }//funcion  
                });
    }





  @endif 




{{--Para usuarios tipo Admin_empresa--}}
 @if($tipoUser[0]->id_tipo_persona == 2 || $tipoUser[0]->id_tipo_persona == 2)


datosGrafico();
function datosGrafico(){
setTimeout(datosGrafico, 90000);



$.ajax({
          type: "GET",
          url: "{{route('VentaNetoSucursal')}}",
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){

                          console.log("Ocurrio un error en la comunicación con el servidor");

                        },
          success: function(rsp){

        // console.log(rsp);
               // 

              var categoria = new Array();
              var acumulado_fact = new Array(); 
              var series = new Array();
              var val_actual = 0;
              var val_anterior = 0;
              var cont = 0, nombre = '';


            if(rsp.data.length > 0){ 
              // console.log(rsp.grafico1);
              var lengthData = rsp.data.length - 1;

                $.each(rsp.data, function(keys,values){


                   if(lengthData == keys){
                    // console.log(keys);
                    val_anterior++;
                    acumulado_fact.push(parseFloat(values.acumulado_neto));
                   }
                 
                    val_actual = values.id_sucursal_empresa;

                  if(val_actual != val_anterior && val_anterior != 0){
                    val_anterior = values.id_sucursal_empresa;
                    

                           series.push( {
                     name: nombre,
                          marker: {
                                      symbol: 'circle'
                                  },
                          data: acumulado_fact

                  });

             
                acumulado_fact = [];

                      } else if(val_anterior == 0) {
                         val_anterior = values.id_sucursal_empresa;
                         nombre = values.nombre;
                      }

                 
             
              
                
                   

                   cont++;

                   nombre = values.nombre;
                  acumulado_fact.push(parseFloat(values.acumulado_neto));
                  categoria.push(values.fecha_factura);


                });

                // console.log(series);
                var Fecha = new Date();
                 var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

                 // console.log("01/"+fechaInicial);


                 Highcharts.setOptions({
                  lang: {
                      decimalPoint: ',',
                      thousandsSep: '.'
                  }
              });

   
           

              $('#cuadro2').highcharts({
                chart: {
                    type: 'column'
                    },
                title: {
                    text: ''
                    },
                colors: ['#FF0080', '#00007c', '#f39c12', '#3c8dbc'],
                subtitle: {
                    text: ''
                    },
                xAxis: {

                    categories: categoria

                    },
                yAxis: {
                    title: {
                        text: ''
                        },
                    labels: {
                        formatter: function () {
                                       return new Intl.NumberFormat("de-DE").format(this.value) ;
                                    }
                        }
                    },
                tooltip: {
                    crosshairs: true,
                    shared: true
                    },
                plotOptions: {
                      // line: {
                      //      dataLabels: {
                      //                   enabled: true
                      //               },
                      //               enableMouseTracking: false
                      //       }

                      },
                      series: series

                    });//.highcharts

                 }//if






               }

             });







   $.ajax({
          type: "GET",
          url: "{{route('indexHighchartsAjax')}}",
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){

                          console.log("Ocurrio un error en la comunicación con el servidor");

                        },
          success: function(rsp){

           

           var data = rsp.tableTicket;

           $("#listadoTicket").DataTable({
                           "destroy": true,
                           "data": data,
                           "columns":[{data:function(x){
                            //  console.log(x);
                                      if(x.id_tipo_ticket == 0){
                                        return `<a href="#">${x.descripcion}</a>`;
                                      }else{ 
                                        return `<a target="_blank" href="{{ route('indexTicket')}}?id_tipo_ticket=${x.id_tipo_ticket}&id_estado=">
                                          ${x.descripcion}</a>`;
                                      }  
                                      }},
                                      {data: function(x){
                                        if(x.id_tipo_ticket == 0){
                                          return `<a target="_blank" href="{{ route('buscarReservaCangoroo')}}?id_tipo=disponible"><span class="badge tableBg bgRed">${x.cant_ticket_disponible}</span></a>`;
                                      }else{ 
                                        return `<a target="_blank" href="{{ route('indexTicket')}}?id_tipo_ticket=${x.id_tipo_ticket}&id_estado=25">
                                        <span class="badge tableBg bgRed">${x.cant_ticket_disponible}</span></a>`;
                                      }  
                                      }},
                                      {data: function(x){
                                        if(x.id_tipo_ticket == 0){
                                          return `<a target="_blank" href="{{ route('buscarReservaCangoroo')}}?id_tipo=enproforma"><span class="badge tableBg bgYellow">${x.cant_ticket_en_proforma}</span></a>`;
                                        }else{ 
                                          return `<a target="_blank" href="{{ route('indexTicket')}}?id_tipo_ticket=${x.id_tipo_ticket}&id_estado=26">
                                        <span class="badge tableBg bgYellow">${x.cant_ticket_en_proforma}</span></a>`;
                                        }  

                                      }}
                                     ],
                                     "createdRow": function ( row, data, index ) {
                              $(row).addClass('overTable');
                            } 
                           
                          });
    
//////////////////////////////////////////////////////////////
                    //EMITIDOS VS FACTURADOS    
                    var categoria_json = new Array();
                    var facturados_json = new Array(); 
                    var emitidos_json = new Array(); 


                  if(rsp.grafico1.length > 0){ 
                    // console.log(rsp.grafico1);

                      $.each(rsp.grafico1, function(keys,values){
                        emitidos_json.push(parseFloat(values.emitidos));
                        categoria_json.push(values.fecha_emision_format);
                        facturados_json.push(parseFloat(values.facturados));
                      });
              


                      // console.log(emitidos_json);
                

                    $('#cuadro').highcharts({
                      chart: {
                          type: 'line'
                          },
                      title: {
                          text: ''
                          },
                      colors: ['#FF0080', '#00007c'],
                      subtitle: {
                          text: ''
                          },
                      xAxis: {
                          categories: categoria_json
                          },
                      yAxis: {
                          title: {
                              text: ''
                              },
                          labels: {
                              formatter: function () {
                                              return this.value ;
                                          }
                              }
                          },
                      tooltip: {
                          crosshairs: true,
                          shared: true
                          },
                      plotOptions: {
                            line: {
                                dataLabels: {
                                              enabled: true
                                          },
                                          enableMouseTracking: false
                                  }
                            },
                            series: [
                                {
                                name: 'Emitidos',
                                marker: {
                                            symbol: 'circle'
                                        },
                                data: emitidos_json
                                },
                                {
                                name: 'Facturados',
                                marker: {
                                            symbol: 'circle'
                                        },
                                data: facturados_json
                                }
                            ]
                          });//.highcharts

                      }//if


                    var categoria_json_a = new Array();
                    var emitidos_json_a = new Array(); 

                    var contador = 0;

                      if(rsp.grafico3.length > 0){
                    $.each(rsp.grafico3, function(keys,values){
                      contador++;
                      if(contador <= 10){
                            // console.log('Dato Bruto DB ='+values.redondeo_total+' Type '+typeof(m));
                        var m = values.redondeo_total;
                            m = Number(m); 
                          //     console.log(typeof(m));
                          //   // 
                          
                          
                          // console.log('Emitido = '+m);
                          // console.log('Categoria = '+values.fecha);
                        emitidos_json_a.push([m]);
                        categoria_json_a.push([values.fecha]);

                        // console.log('==========================');

                      }
                      });  



                      Highcharts.setOptions({
                        lang: {
                            decimalPoint: ',',
                            thousandsSep: '.'
                        }
                    });


                    $('#container').highcharts({
                      
                      chart: {
                          type: 'bar'
                          },
                      title: {
                          text: ''
                          },
                      colors: ['blue'],
                      subtitle: {
                          text: ''
                          },
                      xAxis: {
                          categories: categoria_json_a
                          },
                      yAxis: {
                          title: {
                              text: ''
                              },
                          labels: {
                              formatter: function () {
                                              return new Intl.NumberFormat("de-DE").format(this.value) ;
                                          }
                              }
                          },
                      tooltip: {
                          crosshairs: true,
                          shared: true,
                          pointFormat: "Total: {point.y:,.1f}"
                          },
                      plotOptions: {
                            bar: {
                                      dataLabels: {
                                          enabled: true
                                      }
                                  }
                            },
                            series: [
                                {
                                name: 'Facturados',
                                marker: {
                                            symbol: 'circle'
                                        },
                                data: emitidos_json_a
                                }
                            ]
                          });





                      }//if

////////////////////////////////////////////////////////////////////////////////



                  }//function
                })//ajax


}//function
     



   



@endif

 


  /*
   * Custom Label formatter
   * ----------------------
   */
  function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
  }

// $('.sidebar-toggle').click(function(){
//       $('.sidebar-toggle').trigger('click');

//   }) 
//  

   










  </script>
@endsection
