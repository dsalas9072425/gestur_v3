@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent


	<style type="text/css">
		th { white-space: nowrap; }
	</style>
@endsection
@section('content')
<section id="base-style">
  @include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Reporte BSP</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
              <form action="{{route('getCuenta')}}" id="frmBusqueda" method="get" style="padding: 20px">

                <div class="row">


                  <div class="col-12 col-md-3">
                    <div class="form-group">
                      <label>Aerolinea</label>                        
                      <select class="form-control input-sm select2" required  name="id_aerolinea" id="id_aerolinea" style="padding-left: 0px;width: 100%;">
                        <option value="">Todos</option>
                        @foreach($aerolineas as $aerolinea)
                             <option value="{{$aerolinea->id}}">{{$aerolinea->nombre}}</option>
                        @endforeach 
                      </select>
                    </div>
                  </div>
                  <div class="col-12 col-md-3">
                    <div class="form-group">
                      <label>Moneda</label>                       
                      <select class="form-control input-sm select2" required  name="id_currency" id="id_currency" style="padding-left: 0px;width: 100%;">
                        <option value="">Seleccione Moneda</option>
                        @foreach($currency as $curren)
                            <option value="{{$curren->currency_id}}">{{$curren->currency_code}}</option>
                        @endforeach 
                      </select>
                    </div>
                  </div>
                  <div class="col-12 col-md-3">
                    <div class="form-group">
                      <label>Periodo de Emisión</label>                       
                      <select class="form-control input-sm select2" required  name="fecha_pago" id="fecha_pago" style="padding-left: 0px;width: 100%;">
                        <option value="">Seleccione Fecha</option>
                        @foreach($calendariobsp as $calendario)
                            <option value="{{$calendario->fecha_de_pago}}">{{$calendario->periodo_desde_format}} - {{$calendario->periodo_hasta_format}}</option>
                        @endforeach 
                        
                      </select>
                    </div>
                  </div>
                  <div class="col-12 col-md-3">
                    <div class="form-group">
                        <label>Emisión Desde - Hasta</label>             
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                            <input type="text" class="form-control pull-right fecha" name="periodo_desde_hasta" id="periodo_desde_hasta" value="">
                        </div>
                    </div>  
                  </div>
                  <div class="col-12 col-md-3">
                    <div class="form-group">
                      <label>Office ID</label>                       
                      <select class="form-control input-sm select2" required  name="id_office" id="id_office" style="padding-left: 0px;width: 100%;">
                        <option value="">Seleccione Office ID</option>
                        @foreach($oficces as $oficce)
                            <option value="{{$oficce->id}}">{{$oficce->codigo}} - {{$oficce->nombre}}</option>
                        @endforeach 
                      </select>
                    </div>
                  </div>

                  <div class="col-9">
                    <button type="button" id="botonExcel" class="pull-right btn btn-success btn-lg mr-1"><b>Excel</b></button>
                    <button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
                    <button onclick="loadDatatable()" class="btn btn-info btn-lg pull-right mr-1"  type="button"><b>Buscar</b></button>
    
                  </div>

              </div>
            </form>   
          <div class="box-body">
            <div class="table-responsive">
              <table id="listado" class="table" style="width: 100%;">
                <thead>
                  <tr>
                    <th>CIA</th>
                    <th>TRNC</th>
                    <th>AEROLINEA</th>
                    <th>DOCUMENTO</th>
                    <th>EMISION</th>
                    <th>FOP</th>
                    <th>VALOR TRANSACCION</th>
                    <th>VALOR TARIFA</th>
                    <th>IMP</th>
                    <th>YQ</th>
                    <th>PY</th>
                    <th>PEN</th>
                    <th>%</th>
                    <th>COMISION</th>
                    <th>COM</th>
                    <th>NETO A PAGAR</th>
                  </tr>
                </thead>

                <tbody style="text-align: center">
                </tbody>
                <tfoot>
                  <tr>
                          <th style="text-align: center"></th>
                          <th style="text-align: center"></th>
                          <th style="text-align: center"></th>
                          <th style="text-align: center"></th>
                          <th style="text-align: center"></th>
                          <th style="text-align: center"></th>
                          <th style="text-align: center">VALOR TRANSACCION</th>
                          <th style="text-align: center">VALOR TARIFA</th>
                          <th style="text-align: center">IMP</th>
                          <th style="text-align: center">YQ</th>
                          <th style="text-align: center">PY</th>
                          <th style="text-align: center">PEN</th>
                          <th style="text-align: center"></th>
                          <th style="text-align: center">COMISION</th>
                          <th style="text-align: center">IMPUESTO S/ COMISION</th>
                          <th style="text-align: center">NETO A PAGAR</th>
                  </tr>
                </tfoot>
              </table>
            </div>  
          </div>
        </div>
    </div>
  </div>
</section>            

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
  <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>

	<script>
		$(document).ready(function() {
			$('.select2').select2();
      // loadDatatable();
});

$("#btnBuscar").click(function(){
     loadDatatable();
});


  function limpiar()
  {
      $('#id_aerolinea').val('').trigger('change.select2');
			$('#id_currency').val('').trigger('change.select2');
			$('#fecha_pago').val('').trigger('change.select2');
			$('#periodo_desde_hasta').val('');
  }

     var table; 
    function loadDatatable()
    {

          var api;
          var total = 0;
          // Remove the formatting to get integer data for summation
          var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

          {{--SE FILTRA Y QUITA ESPACIOS EN BLANCO --}}
          let form = $("#frmBusqueda").find(':input').filter(function () {
                        return $.trim(this.value).length > 0
                    }).serializeJSON();
          let input;


          $.blockUI({
                                    centerY: 0,
                                    message: "<h2>Procesando...</h2>",
                                    css: {
                                        color: '#000'
                                    }
                                });

        // RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
        setTimeout(function (){
          const formatter = new Intl.NumberFormat('de-DE', {currency: 'USD',minimumFractionDigits: 0});

          table = $("#listado").DataTable({
                destroy: true,
                ajax: {
                url: "{{route('consultaBsp')}}",
                  data:  form,
                  error:function (jqXHR,textStatus,errorThrown) {
                            $.unblockUI();
                                      $.toast({
                          heading: 'Error',
                          text: 'Hubo un error en la petición.',
                          showHideTransition: 'fade',
                          position: 'top-right',
                          icon: 'error'
                        });       

                        }
                      
                  },


                  "columns":[
                              {data:"id_aerolinea"},
                              {data:"codigo_amadeus"},
                              {data:"nombre_proveedor"},
                              {data: function(x){ 
                                var documento = x.numero_amadeus;
                                if(jQuery.isEmptyObject(x.over_tactico) == false){
                                  var over_tactico = "("+x.over_tactico+")";
                                }else{
                                  var over_tactico = "";
                                }
                                documentos = documento+""+over_tactico;
                                return documentos;
                              }},
                              {data:"fecha_formateada"},
                              {data:"tipo_pago"},
                              //{data:"total_venta"},
                              {data: function(x){ 
                                var total_venta = 0;
                                if(jQuery.isEmptyObject(x.total_venta) == false){
                                  var total_venta = formatter.format(parseFloat(x.total_venta));
                                }
                                return total_venta;
                              }},
                             // {data:"facial"},
                              {data: function(x){ 
                                var facial = 0;
                                if(jQuery.isEmptyObject(x.facial) == false){
                                  var facial = formatter.format(parseFloat(x.facial));
                                }
                                return facial;
                              }},
                             // {data:"impuestos"},
                              {data: function(x){ 
                                var impuestos = 0;
                                if(jQuery.isEmptyObject(x.impuestos) == false){
                                  var impuestos = formatter.format(parseFloat(x.impuestos));
                                }
                                return impuestos;
                              }},
                             // {data:"tasas_yq"},
                              {data: function(x){ 
                                var tasas_yq = 0;
                                if(jQuery.isEmptyObject(x.tasas_yq) == false){
                                  var tasas_yq = formatter.format(parseFloat(x.tasas_yq));
                                }
                                return tasas_yq;
                              }},
                             // {data:"iva"},
                              {data: function(x){ 
                                var iva = 0;
                                if(jQuery.isEmptyObject(x.iva) == false){
                                  var iva = formatter.format(parseFloat(x.iva));
                                }
                                return iva;
                              }},
                              {data:"penalidad"},
                              {data:"porcentaje_comision"},
                             // {data:"comision"},
                              {data: function(x){ 
                                var comision = 0;
                                if(jQuery.isEmptyObject(x.comision) == false){
                                  if(jQuery.isEmptyObject(x.pago_tc) == false){
                                    if(x.comision > 0){
                                      var comision = '-'+formatter.format(parseFloat(x.comision));
                                    }else{
                                      var comision = formatter.format(parseFloat(x.comision));
                                    }
                                  }else{
                                      var comision = formatter.format(parseFloat(x.comision));
                                  }
                                }
                                return comision;
                              }},
                             // {data:"iva_comision"},
                              {data: function(x){ 
                                var iva_comision = 0;
                                if(jQuery.isEmptyObject(x.iva_comision) == false){
                                  var iva_comision = formatter.format(parseFloat(x.iva_comision));
                                }
                                return iva_comision;
                              }},
                              { "data": function(x)
                                  {
                                    totalneta = 0
                                    if(x.id_estado != 33){
                                      if(x.total_venta != 0){
                                        if(x.tipo_pago == 'CC'){
                                            totalneta = 0 - parseFloat(x.comision) - parseFloat(x.iva_comision);
                                        }else{
                                            totalneta = x.total_neto_sin_comision;
                                        }
                                      }  
                                    }
                                    return formatter.format(parseFloat(totalneta));
                                  }
                                },
                        ],
                    "createdRow": function ( row, data, iDataIndex ) {
    
            },
              //AL FINALIZAR LA CARGA AGREGAR CONTROL DE COMAS Y PUNTO
              "initComplete": function(settings, json) {
                    $.unblockUI();
                  },
                  "footerCallback": function ( row, data, start, end, display ) {
                api = this.api();
    
          
    
                // Total over all pages
                total = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                    
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 6, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
    
                // Update footer
                $( api.column( 6 ).footer() ).html(
                    new Intl.NumberFormat("de-DE").format(total)
                );

                // Total over all pages
                total = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 7, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
            // Update footer
                $( api.column( 7 ).footer() ).html(
                  new Intl.NumberFormat("de-DE").format(total)
                );


                  // Total over all pages
                total = api
                    .column( 8 )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 8, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
            // Update footer
                $( api.column( 8 ).footer() ).html(
                  new Intl.NumberFormat("de-DE").format(total)
                );

                // Total over all pages
                total = api
                    .column( 9 )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 9, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
            // Update footer
                $( api.column( 9 ).footer() ).html(
                    new Intl.NumberFormat("de-DE").format(total)
                );

                // Total over all pages
                total = api
                    .column( 10 )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 10, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
            // Update footer
                $( api.column( 10 ).footer() ).html(
                    new Intl.NumberFormat("de-DE").format(total)
                );

                // Total over all pages
                total = api
                    .column( 12 )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 12, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return parseFloat(a) + parseFloat(clean_num(b));
                    }, 0 );
            // Update footer
                $( api.column( 12 ).footer() ).html(
                    new Intl.NumberFormat("de-DE").format(total)
                );

                // Total over all pages
                total = api
                    .column( 13 )
                    .data()
                    .reduce( function (a, b) {return sum(a,clean_num(b))}, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 13, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {return sum(a,clean_num(b))}, 0 );
            // Update footer
                $( api.column( 13 ).footer() ).html(
                    new Intl.NumberFormat("de-DE").format(total)
                );

                // Total over all pages
                total = api
                    .column( 14 )
                    .data()
                    .reduce( function (a, b) {return sum(a,clean_num(b))}, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 14, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {return sum(a,clean_num(b))}, 0 );
            // Update footer
                $( api.column( 14 ).footer() ).html(
                    new Intl.NumberFormat("de-DE").format(total)
                );

                  // Total over all pages
                total = api
                    .column( 15 )
                    .data()
                    .reduce( function (a, b) {return sum(a,clean_num(b))}, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 15, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {return sum(a,clean_num(b))}, 0 );
            // Update footer
                $( api.column( 15 ).footer() ).html(
                    new Intl.NumberFormat("de-DE").format(total)
                );






            }


            });//DATATABLE

          }, 300);
    }

    

    {{--SUMA DE VALORES PARA EL FOOTER --}}
    function sum(a,b){

      if(a == NaN || a == "" || typeof  a === "object"){
          a = 0;
          }
      if(b == NaN || b == "" || typeof  b === "object"){
          b = 0;
        }
      return parseFloat(a) + parseFloat(b);
 }


  $("#botonExcel").on("click", function(e){ 
      console.log('Inicil');
                  e.preventDefault();
                 $('#frmBusqueda').attr('method','post');
                 $('#frmBusqueda').attr('action', "{{route('generarExcelReporteBSP')}}").submit();
            });
	

//CALENDARIO

$(function() {

  let locale = {
						format: 'DD/MM/YYYY',
						cancelLabel: 'Limpiar',
						applyLabel: 'Aplicar',					
							fromLabel: 'Desde',
							toLabel: 'Hasta',
							customRangeLabel: 'Seleccionar rango',
							daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
							monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
													'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
													'Diciembre']
						};


  $('input[name="periodo_desde_hasta"]').daterangepicker({
      autoUpdateInput: false,
      locale: locale
  });

  $('input[name="periodo_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
  });

  $('input[name="periodo_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});

function clean_num(n, bd = false) {

if (n && bd == false) {
  n = n.replace(/[,.]/g, function (m) {
    if (m === '.') {
      return '';
    }
    if (m === ',') {
      return '.';
    }
  });
  return Number(n);
}
if (bd) {
  return Number(n);
}
return 0;
}

	</script>
@endsection
