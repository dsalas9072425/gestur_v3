
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')

  @parent
@endsection
@section('content')
<style type="text/css">
  
  .icoRojo{
    color:red;
  }

  .icoVerde{
    color:green;
  }

  .icoAmarillo{
    color:#C7A10A;
  }
  .correctorSelect {
    height: 74px;
  }
  .selectW {
    width: 100%;
  }
 
 .fa-load-style {
  font-size: 50px;
  color:green;
 }






</style>

<section id="base-style">

  <section class="card">
    <div class="card-header">
      <h4 class="card-title">Reporte BSP Cobranzas</h4>
      <div class="heading-elements">
        <ul class="list-inline mb-0">
          <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
        </ul>
      </div>
    </div>

    <div class="card-content collapse show" aria-expanded="true">
      <div class="card-body">
        <form id="frmBusqueda" method="get">

          <div class="row">

            <div class="col-12 col-sm-3 col-md-3">
              <div class="form-group">
                <label>Aerolinea</label>
                <select class="form-control input-sm select2 selectW" name="aerolinea_id" id="aerolinea_id">
                  <option value="">Seleccione Aerolinea</option>
                  @foreach($aerolineas as $aerolinea)
                  <option value="{{$aerolinea->id}}">{{$aerolinea->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>


            <div class="col-12 col-sm-3 col-md-3">
              <div class="form-group">
                <label>Moneda</label>
                <select class="form-control input-sm select2 selectW" name="moneda_id" id="moneda_id">
                  <option value="">Seleccione Moneda</option>
                  @foreach($monedas as $moneda)
                  <option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
                  @endforeach

                </select>
              </div>
            </div>


            <div class="col-12 col-sm-3 col-md-3">
              <div class="form-group">
                <label>Estado Cobro</label>
                <select class="form-control input-sm select2 selectW" name="estado_cobro" id="estado_cobro">
                  <option value="">Seleccione Opción</option>
                  <option value="SI">Pendiente</option>
                  <option value="NO">Pagado</option>
                </select>
              </div>
            </div>


            <div class="col-12 col-sm-3 col-md-3">
              <div class="form-group">
                <label>Emisión Desde - Hasta</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                  </div>
                  <input type="text" class="form-control pull-right fecha" name="fecha_emision_desdeHasta"
                    id="fecha_emision_desdeHasta" value="" />
                </div>
              </div>
            </div>



            <div class="col-12 col-sm-3 col-md-3">
              <div class="form-group">
                <label>Periodo de Emisión BCP</label>
                <select class="form-control input-sm select2 selectW" name="fecha_pago" id="fecha_pago">
                  <option value="">Seleccione Fecha</option>
                  @foreach($calendariobsp as $calendario)
                  <option value="{{$calendario->fecha_de_pago}}">{{$calendario->periodo_desde_format}} -
                    {{$calendario->periodo_hasta_format}}</option>
                  @endforeach

                </select>
              </div>
            </div>


            <div class="col-12 col-sm-4 col-md-3">
              <div class="form-group">
                <label for="">Estado <span class=""></span></label>
                <select class="form-control select2" name="estado_id" id="estado_id" style="width: 100%;">
                  <option value="">Seleccione Estado</option>
                  @foreach($estados as $estado)
                  <option value="{{$estado->id}}">{{$estado->denominacion}}</option>
                  @endforeach

                </select>
              </div>
            </div>

            <div class="col-12 mb-1">
              <button type="button" id="botonExcel" class="pull-right btn btn-success btn-lg mr-1"><b>Excel</b></button>
              <button type="button" id="btnLimpiar" class="btn btn-light btn-lg pull-right mr-1 font-weight-bold text-white">Limpiar</button>
              <button type="button" id="btnBuscar" class="btn btn-info btn-lg pull-right mr-1 font-weight-bold text-white">Buscar</button>
            </div>
          </div>
        </form>
				<div class="row">
					<div class="col-12 col-md-1" style="margin-left: 45px;">
					    <b style="color:red;">VOID</b>
					</div>
					<div class="col-12 col-md-3"></div>
				</div>	

        <div class="table-responsive">
          <table id="listado" class="table">
            <thead>
              <tr>
                <th>Estado Cobro</th>
                <th>Nro Fact.</th>
                <th>Proforma</th>
                <th>Cliente Ag.</th>
                <th>Nro. Tickets</th>
                <th>Nombre Pax</th>
                <th>Neto a Pagar</th>
                <th>Usuario Pedido</th>
                <th>Usuario Proforma</th>
                <th>Monto Seña</th>
                <th>Saldo Seña</th>
                <th>Usuario Emision</th>
              </tr>
            </thead>

            <tbody style="text-align: center">
            </tbody>
          </table>
        </div>


      </div>
    </div>
  </section>
</section>
   
@endsection
@section('scripts')
  @include('layouts/gestion/scripts')
  <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
  <script>
  

  $(document).ready(function(){

      $('.select2').select2();
      calendario();
  });//DOCUMENT READY




      function buscarTicket(data) 
      {
 

          $.blockUI({
            centerY: 0,
            message: "<h2>Procesando...</h2>",
            css: {
              color: '#000'
            }
          });

          // RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
          setTimeout(function () {

            let form = $('#frmBusqueda').serializeJSON({
              customTypes: customTypesSerializeJSON
            });
          let input;

          
            table = $("#listado").DataTable({
              destroy: true,
              searching: false,
              processing: true,
              serverSide: true,
              ajax: {
                url: "{{route('reporteBspCobranzasAjax')}}",
                data: form,
                error: function (jqXHR, textStatus, errorThrown) {

                  $.toast({
                                    heading: 'Error',
                                    text: 'Ocurrio un error en la comunicación con el servidor.',
                                    position: 'top-right',
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });

                  $.unblockUI();
                }

              },


            }).on('xhr.dt', function (e, settings, json, xhr) {
            //Ajax event - fired when an Ajax request is completed.;
            $.unblockUI();
          });

          }, 300);

      } //

      function calendario()
      {
            $('input[name="fecha_emision_desdeHasta"]').daterangepicker({
                            timePicker24Hour: true,
                            timePickerIncrement: 30,
                            locale: {
                                  format: 'DD/MM/YYYY',
                                  cancelLabel: 'Limpiar'
                                  },
                              
                              });
          //LIMPIAR CALENDARIO 
          $('input[name="fecha_emision_desdeHasta"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

          $('#fecha_emision_desdeHasta').val('');
      }


      $("#btnBuscar").click(function(){
        buscarTicket();
      });

       


    $("#btnLimpiar").click(function(){
    
      $('#moneda_id').val('').trigger('change.select2');
      $('#estado_id').val('').trigger('change.select2');
      $('#fecha_pago').val('').trigger('change.select2');
      $('#aerolinea_id').val('').trigger('change.select2');
      $('#estado_cobro').val('').trigger('change.select2');
      $('#fecha_emision_desdeHasta').val("");
      
  });

  $("#botonExcel").on("click", function(e){ 
      console.log('Inicil');
                  e.preventDefault();
                 $('#frmBusqueda').attr('method','post');
                 $('#frmBusqueda').attr('action', "{{route('generarExcelReporteBSPCobranza')}}").submit();
            });

    
    
  </script>
@endsection