
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">

        .bordeErrorInput {
         border:1px solid red;
        }
</style>

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Cuentas</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">

			<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="tabCuenta" data-toggle="tab" aria-controls="tabIcon21" href="#home" role="tab" aria-selected="true">
                       Cuenta de Fondo Padre</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tabCuentaDetalle" data-toggle="tab" aria-controls="tabIcon22" href="#sectionCuentaDetalle" role="tab" aria-selected="false">
                        Cuenta de Fondo</a>
                </li>
                
			</ul>
			

		<div class="tab-content">
		<section id="home"  class="tab-pane active" role="tabpanel">
			<div class="card-body">

                <div class="card-body pt-0">
                    <button title="Agregar Cuenta"
                        class="btn btn-success pull-right" type="button" id="btnAddCuenta">
                        <div class="fonticon-wrap">
                            <i class="ft-plus-circle"></i>
                        </div>
                    </button>
                </div>

				<form class="row" id="reporteCuentaBanco">

				

					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Activo</label>
							<select class="form-control select2" name="activo" id="activoSearch">
                                <option value="">Todos</option>
                                <option value="true">SI</option>
                                <option value="false">NO</option>

							</select>
						</div>
                    </div>



				</form>

				<div class="row">
					<div class="col-12 mb-1">
						<button type="button" onclick="limpiarCuenta()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
						<button type="button" class="btn btn-info pull-right btn-lg mr-1" onclick="consultaCuentas()"><b>Buscar</b></button>
					</div>
				</div>
            	<div class="table-responsive table-bordered">
		            <table id="listadoCuentaBanco" class="table" style="width: 100%;">
						<thead>
							<tr style="text-align: center;">
								<th>Cuenta Fondo Padre</th>
                                <th>Cuenta de Fondo</th>
                                <th>Activo</th>
								<!--<th>Usuario</th>
                                <th>Fecha</th>-->
                                <th></th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
						</tbody>
					</table>
				</div>


			</div>
		

		</section>

		<section id="sectionCuentaDetalle"  class="tab-pane" role="tabpanel">
			<div class="card-body">
                <div class="card-body pt-0">
                    <button  title="Agregar Cuenta Detalle"
                        class="btn btn-success pull-right" type="button" id="btnAddCuentaDetalle">
                        <div class="fonticon-wrap">
                            <i class="ft-plus-circle"></i>
                        </div>
                    </button>
                </div>

				<form class="row" id="formCuentaDetalle">
                    
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                        <label>Cuenta Fondo Padre</label>
                            <select class="form-control select2" name="id_banco" id="cuentaSearchCuentaDetalle"
                            tabindex="1" style="width: 100%;">
                            <option value="">Todos</option>
                            @foreach ($bancos as $bank)
                            @php
                                if($bank->activo == true){ $activo = 'ACTIVO'; }
                                else { $activo = 'INACTIVO';}
                            @endphp
                            <option value="{{$bank->id}}">{{$bank->nombre}}  <b>({{$activo}})</b></option>
                            @endforeach
                        </select>
                        </div>
                    </div>


					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Activo</label>
							<select class="form-control select2" name="activo" id="activoSearchCuentaDetalle">
                                <option value="">Todos</option>
                                <option value="true">SÍ</option>
								<option value="false">NO</option>
							</select>
						</div>
					</div>

					<div class="col-12">
						<button type="button" onclick="limpiarCuentaDetalle()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
						<button type="button" class="btn btn-info pull-right btn-lg mr-1" onclick="consultaCuentasDetalle()"><b>Buscar</b></button>
					</div>


				</form>


					


				<div class="table-responsive mt-1">
					<table id="listadoCuentaDetalle" class="table table-hover table-condensed nowrap" style="width: 100%;">
						<thead>
							<tr style="text-align: center;">
								 	<th>Cuenta Fondo Padre</th>
								 	<th>Nro. Cuenta de Fondo</th>
								 	<th>Moneda</th>
                                    <th>Tipo</th>
                                    <th>T. Debito</th>
                                    <th>Chequera</th>
                                    <th>Monto Sobregiro</th>
                                    <th>Fecha</th>
                                    <th>Activo</th>
                                    <th></th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
						</tbody>
					</table>
				</div>


			</div>

		</section>
		</div>

		</div>
	</div>
</section>



    <div class="modal fade" id="cuentaAbm" tabindex="-1" role="dialog" aria-labelledby="cuentaAbm" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalTitulo">INFORMACIÓN DE CUENTA FONDO PADRE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                 <form id="formModalCuenta" class="row">  

                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control modalData" name="nombre" id="nombreCuenta">
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>Dirección</label>
                            <input type="text" class="form-control modalData" name="direccion" id="direccionCuenta">
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control modalData" name="telefono" id="telefonoCuenta">
                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Cuenta de Fondo</label>
							<select class="form-control select2Cuenta modalData" name="cuenta_bancaria" id="cuentaBancaria">
								<option value="true">SI</option>
                                <option value="false">NO</option>
							</select>
						</div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Activo</label>
							<select class="form-control select2Cuenta modalData" name="activo" id="activoCuenta">
								<option value="true">SI</option>
                                <option value="false">NO</option>
							</select>
						</div>
                    </div>

                    <input type="hidden" name="id_cuenta" id="inputCuenta" value="">

                </form> 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
              <button type="button" class="btn btn-success" id="btnGuardarCuenta" style="display:none;">Guardar</button>
            </div>

          </div>
        </div>
    </div>



    <div class="modal fade" id="cuentaDetalleAbm" tabindex="-1" role="dialog" aria-labelledby="cuenta ABM"      aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" >INFORMACIÓN CUENTA DE FONDO</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                 <form id="formModalCuentaDetalle" class="row">  

                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                        <label>Cuenta Fondo Padre <label id="id_cuenta_bancoLabel" class="error"></label></label>
                            <select class="form-control select2CuentaDetalleContable modalDataCuentaDetalle" name="id_banco" id="id_cuenta_banco"
                            tabindex="1" style="width: 100%;">
                            @foreach ($bancos as $bank)
                            @php
                                if($bank->activo == false){ continue; }
                            @endphp
                            <option value="{{$bank->id}}">{{$bank->nombre}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>Nro. Cuenta de Fondo<label id="numeroCuentaDetalleLabel" class="error"></label></label>
                            <input type="text" class="form-control modalDataCuentaDetalle" name="nro_cuenta" id="numeroCuentaDetalle" required="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Moneda<label id="monedaCuentaDetalleLabel" class="error"></label></label>
							<select class="form-control select2ModalCuentaDetalle modalDataCuentaDetalle" name="id_moneda" id="monedaCuentaDetalle">
                                @foreach($monedas as $moneda)
                                        <option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
                                @endforeach
							</select>
						</div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Tipo de Cuenta<label id="tipoCuentaDetalleLabel" class="error"></label></label>
							<select class="form-control select2ModalCuentaDetalle modalDataCuentaDetalle" name="id_tipo_cuenta" id="tipoCuentaDetalle">
                                @foreach($tipo_cuenta as $tipo)
                                        <option value="{{$tipo->id}}">{{$tipo->denominacion}}</option>
                                @endforeach
							</select>
						</div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Tiene Tarjeta de Debito<label id="tarjetaDebitoCuentaDetalleLabel" class="error"></label></label>
							<select class="form-control select2ModalCuentaDetalle modalDataCuentaDetalle" name="tarjeta_debido" id="tarjetaDebitoCuentaDetalle">
                                <option value="true">SI</option>
                                <option value="false">NO</option>               
							</select>
						</div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Chequera<label id="chequeraCuentaDetalleLabel" class="error"></label></label>
							<select class="form-control select2ModalCuentaDetalle modalDataCuentaDetalle" name="chequera" id="chequeraCuentaDetalle">
                                <option value="true">SI</option>
                                <option value="false">NO</option>
							</select>
						</div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Monto de Sobregiro<label id="sobregiroCuentaDetalleLabel" class="error"></label></label>
							<input type="text" class="form-control modalDataCuentaDetalle" name="sobregiro" id="sobregiroCuentaDetalle">
						</div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>Saldo inicial<label id="saldoinicialCuentaDetalleLabel" class="error"></label></label>
                            <input type="text" class="form-control modalDataCuentaDetalle" name="saldoinicial" id="saldoinicialCuentaDetalle">
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Activo<label id="activoCuentaDetalleLabel" class="error"></label></label>
							<select class="form-control select2ModalCuentaDetalle modalDataCuentaDetalle" name="activo" id="activoCuentaDetalle">
								<option value="true">SI</option>
                                <option value="false">NO</option>
							</select>
						</div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Contable<label id="id_cuenta_contableLabel" class="error"></label></label>
                            <select class="form-control select2CuentaDetalleContable modalDataCuentaDetalle" name="id_cuenta_contable"
                                id="id_cuenta_contable" style="width: 100%;">
                                @foreach ($cuentas_contables as $cuenta)
                                    @if($cuenta->asentable)
                                    <optgroup label="{{$cuenta->descripcion}}">
                                        @endif
                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}})
                                            {{$cuenta->descripcion}}
                                        </option>
                                        @if($cuenta->asentable)
                                    </optgroup>
                                    @endif 
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="id_cuenta_detalle" id="inputCuentaDetalle" value="">

                </form> 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
              <button type="button" class="btn btn-success" id="btnGuardarCuentaDetalle" style="display:none;">Guardar</button>
            </div>

          </div>
        </div>
    </div>





@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>


	<script>
		{{--==========================================
					VARIABLES GLOBALES
			==========================================--}}
			var ordenamiento = [];
			var table;
            var msj_cuentas = '';

			$(()=>{
                configSelect2();
                consultaCuentas();
                $('#activoSearch').val('true').trigger('change.select2');
				calendar();
			});

	function configSelect2()
    {
                 $('.select2').select2({
                    minimumResultsForSearch : Infinity
                });

                 $('#cuentaSearchCuentaDetalle').select2();

				$('.select2Cuenta').select2({
                    dropdownParent: $('#cuentaAbm'),
                    minimumResultsForSearch : Infinity
                });
                $('.select2ModalCuentaDetalle').select2({
                    dropdownParent: $('#cuentaDetalleAbm'),
                    minimumResultsForSearch : Infinity
                });
                $('.select2CuentaDetalleContable').select2({
                    dropdownParent: $('#cuentaDetalleAbm')
                });

    }


/*  ====================================================================================
				              CONSULTA CUENTA ENTIDAD
	====================================================================================*/


	function consultaCuentas()
    {

			$.blockUI({
											centerY: 0,
											message: '<div class="loadingC"><br><h2>Procesando ...</h2></div>',
											css: {
												color: '#000'
											}
										});

			// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
			setTimeout(function (){
						
	 		table =   $('#listadoCuentaBanco').DataTable({
					"destroy": true,
					"ajax": {
							data: $('#reporteCuentaBanco').serializeJSON(),
							url: "{{route('ajaxListadoCuenta')}}",
							type: "GET",
					error: function(jqXHR,textStatus,errorThrown){

				   $.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});
					$.unblockUI();
				}
						// formatter.format(parseFloat(x.total_venta_proforma));
				  },
				 "columns":[
								{data:'nombre'},
								{data: (x)=>{ 
                                    if(x.cuenta_bancaria == true){
                                        return 'SI';
                                }
                                return 'NO';
                                }},
								{data: (x)=>{
                                    if(x.activo == true){
                                        return 'SI';
                                }
                                return 'NO';
                                }},
                                /*{data: (x)=>{ return x.usuario.nombre +' '+ x.usuario.apellido; }},
                                {data: (x)=>{ return x.fecha_creacion_format; }},*/
								{data: function(x){
									return `
                                    <button  class='btn btn-info' onclick='verInfoCuenta(${x.id})' type='button'><i class='fa fa-fw fa-search'></i>
                                    </button>

                                    <button  class='btn btn-info' onclick='editInfoCuenta(${x.id})' type='button'><i class='fa fa-fw fa-edit'></i>
                                    </button>
                                    `;
								}}
							 
							]
				
				}).on('xhr.dt', function ( e, settings, json, xhr ){
						//Ajax event - fired when an Ajax request is completed.;
					 $.unblockUI();
				});
				
	 
					}, 300);

    }//function

/*  ====================================================================================
				              ABM CUENTA / ENTIDAD
	====================================================================================*/

    function configCamposModal(opt)
    {
        $('#inputCuenta').val('');
        if(opt){
            $('.modalData').val('').trigger('change.select2');
            $('.modalData').val('');
            $('.modalData').prop('disabled',true); //DESACTIVAR CAMPOS
        } else {
            $('.modalData').val('').trigger('change.select2');
            $('.modalData').val('');
            $('.modalData').prop('disabled',false); //ACTIVAR CAMPOS
        }
   
    }

    function showModalCuenta(opt)
    {   
        if(opt)
        {
            $('#cuentaAbm').modal('show');
        }

        $('#cuentaAbm').modal('hide');
    }

    // $('#nuevoSaldo').keypress(function(){
    //   if ($('#id_moneda_det').val() === '111') {
    //         formatNumber(0);
    //     } else {
    //         formatNumber(2);
    //     }
    // });

  

    // function formatNumber(coma) {
    //     // console.log('SE dispara formateo');
    //     $('.format-number-control').inputmask("numeric", {
    //         radixPoint: ",",
    //         groupSeparator: ".",
    //         placeholder: '0',
    //         digits: coma,
    //         autoGroup: true,
    //         // prefix: '$', //No Space, this will truncate the first character
    //         rightAlign: false
    //     });

    // }
    $('.numeric').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
             // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false,
            oncleared: function () { self.Value(''); }
        });

    function verInfoCuenta(id)
    {   
        configCamposModal(true);
        getCuentaInfo(id);
        showModalCuenta(true);
    }

    function editInfoCuenta(id)
    {   
        configCamposModal(false); //FORMATEA CAMPOS
        $('#inputCuenta').val(id); //DEFINE ID CUENTA
        $('#btnGuardarCuenta').show();
        getCuentaInfo(id);
        showModalCuenta(true);
    }
    //EVENTO GUARDAR
    $('#btnGuardarCuenta').click(()=>{
        saveCuenta();
    });

    //EVENTO CIERRE MODAL
    $('#cuentaAbm').on('hidden.bs.modal', function (e) {
        configCamposModal(true);
        $('#btnGuardarCuenta').hide();
    });

    $('#btnAddCuenta').click(()=>{
        crearInfoCuenta();
    });

    function saveCuenta()
    {
        if(msj_cuentas.reset){
		    msj_cuentas.reset();
	    }

        $.when(setCuentaInfo()).then(
            (a)=>{ 
            msj_cuentas = $.toast({	
					heading: '<b>Éxito</b>',
					position: 'top-right', 
					text: 'Los datos fueron almacenados.',
					hideAfter: false,
					icon: 'success'
					});

                showModalCuenta(false);
                consultaCuentas();
                generarSelect();

        }, (b)=>{
            msj_cuentas = $.toast({	
					heading: '<b>Atención</b>',
					position: 'top-right', 
					text: 'Ocurrió un error en la comunicación con el servidor.',
					hideAfter: false,
					icon: 'error'
					});
        });
    }

    function crearInfoCuenta()
    {   
        $('#btnGuardarCuenta').show();
        configCamposModal(false);
        showModalCuenta(true);
    }


    function generarSelect(){
            $.ajax({
                    type: "GET",
                    url: "{{ route('getDataListado') }}",
                    dataType: 'json',
                    data: {activo: $('#activoSearch').val()},
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $.toast({   
                                heading: '<b>Atención</b>',
                                position: 'top-right', 
                                text: 'Ocurrió un error en la comunicación con el servidor.',
                                hideAfter: false,
                                icon: 'error'
                            });
                    },
                    success: function(rsp)
                    {
                        console.log(rsp);
                        $('#id_cuenta_banco').empty();
                        $('#id_banco_det').empty();
                        $('#id_categoria').empty();
                        $('#cuentaSearchCuentaDetalle').empty();

                        var newOption = new Option('Todos', 0, false, false);
                        $('#cuentaSearchCuentaDetalle').append(newOption); 
                        $.each(rsp, function (key, item){
                                var newOption = new Option(item.nombre, item.id, false, false);
                                $('#cuentaSearchCuentaDetalle').append(newOption);
                        })  
                        var newOption = new Option('Todos', 0, false, false);
                        $('#id_cuenta_banco').append(newOption); 
                         $.each(rsp, function (key, item){
                                var newOption = new Option(item.nombre, item.id, false, false);
                                $('#id_cuenta_banco').append(newOption);
                        })  
                         var newOption = new Option('Todos', 0, false, false);
                        $('#id_banco_det').append(newOption); 
                         $.each(rsp, function (key, item){
                                var newOption = new Option(item.nombre, item.id, false, false);
                                $('#id_banco_det').append(newOption);
                        })  
                     }
                }); 

    }

    function getCuentaInfo(id)
    {
        $.ajax({
					type: "GET",
					url: "{{ route('getDataCuenta') }}",
					dataType: 'json',
					data: {id_cuenta: id},

					error: function(jqXHR, textStatus, errorThrown)
                    {
                        $.toast({	
								heading: '<b>Atención</b>',
								position: 'top-right', 
								text: 'Ocurrió un error en la comunicación con el servidor.',
								hideAfter: false,
								icon: 'error'
							});
					},
					success: function(rsp)
                    {
                        let x = rsp.data[0];
                        //CARGAR LOS CAMPOS
                        $('#nombreCuenta').val(x.nombre);
                        $('#direccionCuenta').val(x.direccion);
                        $('#telefonoCuenta').val(x.telefonos);
                        $('#cuentaBancaria').val(`${x.cuenta_bancaria}`).trigger('change.select2');
                        $('#activoCuenta').val(`${x.activo}`).trigger('change.select2');
                       
					}
				});


    }

    function setCuentaInfo()
    {
      return new Promise((resolve, reject) => {   

        let dataString =  $('#formModalCuenta').serializeJSON({
            customTypes: customTypesSerializeJSON
        });

        $.ajax({
					type: "GET",
					url: "{{ route('setDataCuenta') }}",
					dataType: 'json',
					data: dataString,

					error: function(jqXHR, textStatus, errorThrown)
                    {
                        reject();
					},
					success: function(rsp)
                    {  
                        if(rsp.err == true){
                            resolve();
                        }
                    reject();
                        
					}
				});
       });      
    }

/*  ====================================================================================
				              ABM CUENTA DETALLE
	====================================================================================*/


    function configCamposModalDetalle(opt)
    {
        $('#inputCuentaDetalle').val('');
        let dat =  $('.modalDataCuentaDetalle');
        if(opt){
            dat.val('').trigger('change.select2');
            dat.val('');
            dat.prop('disabled',true); //DESACTIVAR CAMPOS
        } else {
            dat.val('').trigger('change.select2');
            dat.val('');
            dat.prop('disabled',false); //ACTIVAR CAMPOS
        }
   
    }

    function showModalCuentaDetalle(opt)
    {   
        if(opt)
        {
            $('#cuentaDetalleAbm').modal('show');
        }

        $('#cuentaDetalleAbm').modal('hide');
    }

    function showModalCuentaSaldoDetalle(opt)
    {   
        if(opt)
        {
            $('#nuevoSaldo').val('');
            $('#cuentaDetalleSaldo').modal('show');
        }

        $('#cuentaDetalleSaldo').modal('hide');
    }

    function verInfoCuentaDetalle(id)
    {   
        configCamposModalDetalle(true);
        getCuentaDetalleInfo(id);
        showModalCuentaDetalle(true);
    }

    function editInfoCuentaDetalle(id)
    {   
        configCamposModalDetalle(false); //FORMATEA CAMPOS
        $('#inputCuentaDetalle').val(id); //DEFINE ID CUENTA
        $('#btnGuardarCuentaDetalle').show();
        getCuentaDetalleInfo(id);
        $('#id_cuenta_banco').prop('disabled',true); //DESACTIVAR CUENTA BANCO
        $('#saldoinicialCuentaDetalle').prop('disabled',true); //DESACTIVAR CUENTA BANCO
        showModalCuentaDetalle(true);
    }

    function editSaldoCuentaDetalle(id)
    {
        $('#id_banco_det').prop('disabled',true); //DESACTIVAR CUENTA BANCO
        $('#nro_cuenta_det').prop('disabled',true); //DESACTIVAR CUENTA BANCO
        $('#id_moneda_det').prop('disabled',true); //DESACTIVAR CUENTA BANCO
        $('#id_tipo_cuenta_det').prop('disabled',true); //DESACTIVAR CUENTA BANCO
        $('#saldoActual').prop('disabled',true); //DESACTIVAR CUENTA BANCO
        $('#input_cuenta_det').val(id); //DEFINE ID CUENTA

        getCuentaDetalleSaldo(id);
        showModalCuentaSaldoDetalle(true);
    }

    //EVENTO GUARDAR
    $('#btnGuardarCuentaDetalle').click((event)=>{
        
        contador = 0;
         var contador = 0;
         $("#formModalCuentaDetalle").find(':input').each(function() {
             var elemento= this;
             if(elemento.id !='inputCuentaDetalle' && elemento.id !='saldoinicialCuentaDetalle'){
                     if(elemento.value == ""){
                        $(this).next().children().children().each(function(){
                            $(this).css( "border-color", "red" );
                        });
                        $("#"+elemento.id).css('border-color', 'red');
                        contador++;
                     }else{
                        $(this).next().children().children().each(function(){
                            $(this).css( "border-color", "#aaa" );
                        });
                        $("#"+elemento.id).css('border-color', '#aaa');         
                    }
                console.log(elemento.id+"  "+ elemento.value);

            }else{
                if(elemento.id =='saldoinicialCuentaDetalle'){
                    if($('#inputCuentaDetalle').val() ==""){
                        if(elemento.value == ""){
                            $(this).next().children().children().each(function(){
                                $(this).css( "border-color", "red" );
                            });
                            $("#"+elemento.id).css('border-color', 'red');
                            contador++;
                         }else{
                            $(this).next().children().children().each(function(){
                                $(this).css( "border-color", "#aaa" );
                            });
                            $("#"+elemento.id).css('border-color', '#aaa');         
                        }
                    }    
                }
            }
        });
         console.log(contador);
         if(contador == 0){
            saveCuentaDetalle();
        }

    });


    //EVENTO CIERRE MODAL
    $('#cuentaDetalleAbm').on('hidden.bs.modal', function (e) {
        configCamposModal(true);
        $('#btnGuardarCuenta').hide();
    });

    $('#btnAddCuentaDetalle').click(()=>{
        $("#formModalCuentaDetalle").find(':input').each(function() {
            var elemento= this;
            $(this).next().children().children().each(function(){
                   $(this).css( "border-color", "#aaa" );
                });
                $("#"+elemento.id).css('border-color', '#aaa');         
          });
        crearInfoCuentaDetalle();
    });



    function crearInfoCuentaDetalle()
    {   
        $('#btnGuardarCuentaDetalle').show();
        configCamposModalDetalle(false);
        showModalCuentaDetalle(true);
    }

    const formatter = new Intl.NumberFormat('de-DE',
        {
            currency: 'USD',
            minimumFractionDigits: 2
        });


    function getCuentaDetalleInfo(id)
    {
        $.ajax({
					type: "GET",
					url: "{{ route('getDataCuentaDetalle') }}",
					dataType: 'json',
					data: {id_cuenta_detalle: id},

					error: function(jqXHR, textStatus, errorThrown)
                    {
                        $.toast({	
								heading: '<b>Atención</b>',
								position: 'top-right', 
								text: 'Ocurrió un error en la comunicación con el servidor.',
								hideAfter: false,
								icon: 'error'
							});
					},
					success: function(rsp)
                    {
                        
                        let x = rsp.data;
   
                        //CARGAR LOS CAMPOS
                        $('#nombreCuentaDetalle').val(x.banco_cabecera.nombre);
                        $('#numeroCuentaDetalle').val(x.numero_cuenta);
                        $('#id_cuenta_banco').val(`${x.id_banco}`).trigger('change.select2');
                        $('#monedaCuentaDetalle').val(`${x.id_moneda}`).trigger('change.select2');
                        $('#tipoCuentaDetalle').val(`${x.id_tipo_cuenta}`).trigger('change.select2');
                        $('#tarjetaDebitoCuentaDetalle').val(`${x.tarjeta_debito}`).trigger('change.select2');
                        $('#chequeraCuentaDetalle').val(`${x.chequera}`).trigger('change.select2');
                        $('#sobregiroCuentaDetalle').val(x.monto_sobregiro);
                        $('#saldoinicialCuentaDetalle').val(x.saldo_inicial);
                        $('#activoCuentaDetalle').val(`${x.activo}`).trigger('change.select2');
                        $('#id_cuenta_contable').val(`${x.id_cuenta_contable}`).trigger('change.select2');
                                             
					}
				});
    }

    function getCuentaDetalleSaldo(id)
    {
        $.ajax({
                    type: "GET",
                    url: "{{ route('getDataCuentaDetalle') }}",
                    dataType: 'json',
                    data: {id_cuenta_detalle: id},

                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        $.toast({   
                                heading: '<b>Atención</b>',
                                position: 'top-right', 
                                text: 'Ocurrió un error en la comunicación con el servidor.',
                                hideAfter: false,
                                icon: 'error'
                            });
                    },
                    success: function(rsp)
                    {
                        let x = rsp.data;
                        //CARGAR LOS CAMPOS
                        $('#nro_cuenta_det').val(x.numero_cuenta);
                        $('#id_banco_det').val(`${x.id_banco}`).trigger('change.select2');
                        $('#id_moneda_det').val(`${x.id_moneda}`).trigger('change.select2');
                        $('#id_tipo_cuenta_det').val(`${x.id_tipo_cuenta}`).trigger('change.select2');
                        $('#saldoActual').val(formatter.format(parseFloat(x.saldo)));




                    }
                });


    }
    
 function saveCuentaDetalle()
    {
        if(msj_cuentas.reset){
            msj_cuentas.reset();
        }

        $.when(setCuentaDetalleInfo()).then(
            (a)=>{ 
            msj_cuentas = $.toast({ 
                    heading: '<b>Exito</b>',
                    position: 'top-right', 
                    text: 'Los datos fueron almacenados.',
                    hideAfter: false,
                    icon: 'success'
                    });
                showModalCuentaDetalle(false);
                consultaCuentasDetalle();
        }, (b)=>{
            msj_cuentas = $.toast({ 
                    heading: '<b>Atención</b>',
                    position: 'top-right', 
                    text: 'Ocurrió un error en la comunicación con el servidor.',
                    hideAfter: false,
                    icon: 'error'
                    });
        });
    }


     function setCuentaDetalleInfo()
    {
      return new Promise((resolve, reject) => {   

        let dataString =  $('#formModalCuentaDetalle').serializeJSON({
            customTypes: customTypesSerializeJSON
        });

        $.ajax({
                    type: "GET",
                    url: "{{ route('setDataCuentaDetalle') }}",
                    dataType: 'json',
                    data: dataString,
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        reject();
                    },
                    success: function(rsp)
                    {   console.log(rsp.err);
                        if(rsp.err == true){
                            resolve();
                        }
                          reject();
                    }
                });
       });      
    }



  

	function consultaCuentasDetalle()
    {

			$.blockUI({
											centerY: 0,
											message: '<div class="loadingC"><br><h2>Procesando ...</h2></div>',
											css: {
												color: '#000'
											}
										});

			// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
			setTimeout(function (){
				
	 		table =   $('#listadoCuentaDetalle').DataTable({
					"destroy": true,
					"ajax": {
							data: $('#formCuentaDetalle').serializeJSON(),
							url: "{{route('ajaxListadoCuentaDetalle')}}",
							type: "GET",
					error: function(jqXHR,textStatus,errorThrown){

				   $.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});
					$.unblockUI();
				}
				  },

 
				 "columns":[
								{
                                    data: (x)=>{ 
                                        var nombre='';
                                        // console.log(x.banco_cab);
                                        if (!jQuery.isEmptyObject(x.banco_cab)) 
                                        {
                                            nombre = x.banco_cab.nombre;
                                        }
                                        return nombre;
                                    }
                                },
                                {data:'numero_cuenta'},
                                {
                                    data: (x)=>{ 
                                        var moneda='';
                                        // console.log(x.banco_cab);
                                        if (!jQuery.isEmptyObject(x.currency)) 
                                        {
                                            moneda = x.currency.currency_code;
                                        }
                                        return moneda;
                                    }
                                },

                                {
                                    data: (x)=>{ 
                                        var tipo_cuenta='';
                                        // console.log(x.banco_cab);
                                        if (!jQuery.isEmptyObject(x.tipo_cuenta)) 
                                        {
                                            tipo_cuenta = x.tipo_cuenta.denominacion;
                                        }
                                        return tipo_cuenta;
                                    }
                                },

								{data: (x)=>{ 
                                    if(x.tarjeta_debito == true){
                                        return 'SI';
                                }
                                return 'NO';
                                }},
								{data: (x)=>{
                                    if(x.chequera == true){
                                        return 'SI';
                                }
                                return 'NO';
                                }},
                                {data: (x)=>{ return x.monto_sobregiro; }},
                                {data: (x)=>{ return x.fecha_creacion_format; }},
                                {data: (x)=>{
                                    if(x.activo == true){
                                        return 'SI';
                                }
                                return 'NO';
                                }},
								{
                                    data: function(x)
                                    {
    									return `
                                            <button title='Editar Cuenta' class='btn btn-info' onclick='editInfoCuentaDetalle(${x.id})' type='button'><i class='fa fa-fw fa-edit'></i>
                                            </button>

                                            <button title='Ver Cuenta' class='btn btn-info' onclick='verInfoCuentaDetalle(${x.id})' type='button'><i class='fa fa-fw fa-search'></i>
                                            </button>
                                                `;
								    }}
							 
							]
				
				}).on('xhr.dt', function ( e, settings, json, xhr ){
						//Ajax event - fired when an Ajax request is completed.;
					 $.unblockUI();
				});
				
	 
					}, 300);

    }//f

		function limpiarCuenta(){
			$('#activoSearch').val('').trigger('change.select2');
		}


		function limpiarCuentaDetalle(){
			$('#cuentaSearchCuentaDetalle').val('').trigger('change.select2');
			$('#activoSearchCuentaDetalle').val('').trigger('change.select2');
		}





/*  ====================================================================================
				                FUNCIONES AUXILIARES
	====================================================================================*/

function calendar()
        {

			$( ".fecha" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$('.calendario').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			});
			$('.calendario').val('');
			$('.calendario').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
		}
			

		  //DEFINIR FORMATO MONEDAS       
            const EURO = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
            const DOLAR = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
            const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });



    
                function formatCurrency(id_moneda, value){
                    
                    let importe = 0;
    

                    // console.log('formatCurrency', id_moneda, value);

                            //PYG
                            if(id_moneda == 111){
                                importe = GUARANI(Number(value),{ formatWithSymbol: false }).format(true);
                            } 
                            //USD
                            if(id_moneda == 143) {
                                importe = DOLAR(Number(value),{ formatWithSymbol: false }).format(true);
                            }
                            //EUR
                            if(id_moneda == 43) {
                                importe = EURO(Number(value),{ formatWithSymbol: false }).format(true);
                            }

                    return importe;     
                }

		




	</script>
@endsection