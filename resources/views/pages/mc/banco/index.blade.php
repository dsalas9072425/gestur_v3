
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}
	
	input .tachado
	{
		text-decoration: line-through;
		
	}

	.tachado
	{
		text-decoration: line-through;
		
	}

	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}


	#formMovimientoBancario select option, select{
		font-size: 12px;
		font-weight: 800;
	}

	.select2{
		width: 100% !important;
	}

	.no-input{
		border:0 solid white;
		background-color: #fff;
	}

</style>


<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Resumen Cuenta</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">

			<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="tabCuenta" data-toggle="tab" aria-controls="tabIcon21" href="#home" role="tab" aria-selected="true">
                        Cuenta</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tabMovimiento" data-toggle="tab" aria-controls="tabIcon22" href="#movimientoBancario" role="tab" aria-selected="false">
                        Movimiento Cuenta</a>
                </li>
                
			</ul>
			

		<div class="tab-content">
		<section id="home"  class="tab-pane active" role="tabpanel">
			<div class="card-body">

				<form class="row" id="reporteCuentaBanco">
					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Tipo</label>
							<select class="form-control select2" name="tipo_cuenta" id="tipo_cuenta">
								<option value="">Todos</option>
								<option value="1">Bancarias</option>
								<option value="false">No Bancarias</option>

							</select>
						</div>
					</div>

					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Cuenta de Fondo Padre</label>
							<select class="form-control select2" name="id_banco" id="id_banco">
								<option value="">Todos</option>
								@foreach($bancos as $banco)
									<option value="{{$banco->id}}">{{$banco->nombre}}</option>
								@endforeach

							</select>
						</div>
					</div>
					<div class="col-12 col-sm-3 col-md-4">
						<div class="form-group">
							<label>Cuenta de Fondo</label>
							<select class="form-control select2" name="id_cuenta_detalle" id="id_cuenta_detalle">
								<option value="">Todos</option>
								@foreach($cuentas as $cuenta)
									<option value="{{$cuenta->id}}">{{$cuenta->banco_n}} {{$cuenta->numero_cuenta}} {{$cuenta->currency_code}} {{$cuenta->denominacion}}</option>
								@endforeach

							</select>
						</div>
					</div>
					<div class="col-12 col-sm-3 col-md-2">
						<div class="form-group">
							<label>Activo</label>
							<select class="form-control select2" name="activo" id="activo">
								<option value="">Todos</option>
								<option value="1">SI</option>
								<option value="false">NO</option>
							</select>
						</div>
					</div>

				</form>

				<div class="row">
					<div class="col-12 mb-1">
						<button type="button"  class="btn btn-warning btn-lg pull-right mr-1 text-white" title="Historial de cuentas" data-toggle="modal" data-target="#modalHistorial"><i class="fa fa-history" aria-hidden="true"></i> </button>
						<button type="button" onclick="limpiar()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
						<button type="button" class="btn btn-info pull-right btn-lg mr-1" onclick="consultaCuentaBanco()"><b>Buscar</b></button>

					</div>
				</div>
            	<div class="table-responsive table-bordered">
		            <table id="listadoCuentaBanco" class="table" style="width: 100%;">
						<thead>
							<tr style="text-align: center;">
								<th></th>
								<th>Cuenta de Fondo Padre</th>
								<th>Nro. Cuenta de Fondo</th>
								<th>Moneda</th>
								<th>Saldo</th>
								<th>Tipo</th>
								<th>Activo</th>
								<th>Fecha</th>
								<th></th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
						</tbody>
					</table>
				</div>


			</div>
		</section>

		<section id="movimientoBancario"  class="tab-pane" role="tabpanel">

			<!--{{--<div class="card-body pt-0">
				<button href="{{route('personaAdd')}}" title="Agregar Movimiento Bancario"
					class="btn btn-success pull-right" type="button" id="btnModalMovimiento">
					<div class="fonticon-wrap">
						<i class="ft-plus-circle"></i>
					</div>
				</button>
			</div>--}}-->

			<div class="card-body">
				<form class="row" id="reporteMovimientoBancario">

					<div class="col-12 col-sm-4 col-md-4">
						<div class="form-group">
							<label>Cuenta de Fondo</label>
							<select class="form-control select2" name="id_cuenta_detalle" id="rm_cuenta_detalle">
								<option value="">Todos</option>
								@foreach($cuentas as $cuenta)
									<option value="{{$cuenta->id}}">{{$cuenta->banco_n}} {{$cuenta->numero_cuenta}} {{$cuenta->currency_code}} {{$cuenta->denominacion}}</option>
								@endforeach

							</select>
						</div>
					</div>

					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Desde/Hasta Fecha de Emisión</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								</div>
								<input type="text" class="form-control calendario_detalle" name="periodo_fecha_emision:p_date" id="periodo_emision" />
							</div>
						</div>
					</div>

					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Desde/Hasta Fecha de Operación</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								</div>
								<input type="text" class="form-control calendario_detalle" name="fecha_hora_creacion:p_date" id="periodo_emision" />
							</div>
						</div>
					</div>

					<div class="col-12">
						<button type="button" id="botonExcel" class="btn btn-success btn-lg pull-right mr-1"><b>Excel</b></button> 
						<button type="button" onclick="limpiarMovimientoBancario()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
						<button type="button" class="btn btn-info pull-right btn-lg mr-1" onclick="consultaMovimientoBancario()"><b>Buscar</b></button>
					</div>
				</form>
				<div class="table-responsive mt-1">
					<table id="listadoMovimientoBancario" class="table table-hover table-condensed nowrap" style="width: 100%;">
						<thead>
							<tr style="text-align: center;">
									<th style="color:transparent">Id</th>
								 	<th>Fecha Hora</th>
									<th>Fecha Hora Creacion</th>
								 	<th>Cuenta de Fondo Padre</th>
								 	<th>Cuenta de Fondo</th>
								 	<!--<th>Persona</th>
								 	<th>Importe</th>-->
								 	<th>Moneda</th>
								 		<!--<th>Cotización</th>-->
								 	<!--<th>Tipo</th>-->
								 	<th>Debito</th>
								 	<th>Credito</th>
									<th>Saldo</th>
								 	<th>Concepto</th>
								 	<th>Operación</th>
								    <th>Documento</th>
								 	<th>Suc</th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
						</tbody>
					</table>
				</div>


			</div>

		</section>



		</div>





		</div>
	</div>
</section>



	

     {{-- ========================================
   			GENERAR MOVIMIENTO BANCARIO
   	========================================  --}}		

	<div class="modal fade" id="modalMovimiento"  role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
			<h4 class="modal-title"  style="font-weight: 800;">Agregar Movimiento Bancario <i style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
			<button type="button" class="closeModal btn-danger"  data-dismiss="modal" aria-label="Close">
	        	<span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
				<form id="formMovimientoBancario" class="form-horizontal form-group-sm"> 
						

					<div class="form-group row">
					    <label for="" class="col-sm-3 control-label">Cuenta de Fondo Padre</label>
					    <div class="col-sm-9">
							<select class="form-control" name="id_banco"  id="banco_m" style="width: 100%;">
										@foreach ($bancos as $banco)
											<option value="{{$banco->id}}">{{$banco->nombre}}</option>
										@endforeach
							</select>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label for="" class="col-sm-3 control-label">Cuenta de Fondo</label>
					    <div class="col-sm-9">
							<select class="form-control" name="id_banco_detalle"  id="id_banco_detalle" style="width: 100%;">
								<option value="">Seleccione cuenta</option>
										@foreach ($cuentas as $cuenta)
											<option value="{{$cuenta->id}}" data-bank="{{$cuenta->id_banco}}">{{$cuenta->denominacion}} - {{$cuenta->numero_cuenta}} - {{$cuenta->currency_code}}</option>
										@endforeach
							</select>
					    </div>
					  </div>
					



					  <div class="form-group row">
							    <label for="" class="col-sm-3 control-label">Moneda</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" name="id_moneda"  readonly>
							    </div>
						</div>
					
						{{-- 'tipo_op' --}}
					 <div class="form-group row">
					    <label for="" class="col-sm-3 control-label">Tipo Operación</label>
					    <div class="col-sm-9">
							<select class="form-control input-sm" name="id_tipo_documento"   style="width: 100%;">
										@foreach ($tipo_op as $tipo)
											<option value="{{$tipo->id}}">{{$tipo->denominacion}}</option>
										@endforeach
							</select>
					    </div>
					  </div>

		
						<div class="form-group row">
							    <label for="" class="col-sm-3 control-label">Nro. O. Pago</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="nro_pago" readonly placeholder="">
							    </div>
						</div>


						<div class="form-group row">
							    <label for="" class="col-sm-3 control-label">Nro. de Operación</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" id="nro_operacion" placeholder="">
							    </div>
						</div>

						<div class="form-group row">
							    <label for="" class="col-sm-3 control-label">Fecha Emisión</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control fecha" name="fecha_emision"  placeholder="">
							    </div>
						</div>

						<div class="form-group row">
							    <label for="" class="col-sm-3 control-label">Fecha Vencimiento</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control fecha" name="fecha_vencimiento"  placeholder="">
							    </div>
						</div>

					


						<div class="form-group row">
						    <label for="" class="col-sm-3 control-label">Sucursal</label>
						    <div class="col-sm-9">
								<select class="form-control" name="id_sucursal"   style="width: 100%;">
										@foreach ($sucursales as $sucursal)
											<option value="{{$sucursal->id}}">{{$sucursal->nombre}}</option>
										@endforeach
								</select>
						    </div>
					    </div>


					    <div class="form-group row">
							    <label for="" class="col-sm-3 control-label">Cuenta</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" name="id_cuenta_contable"  placeholder="">
							    </div>
						</div>


						<div class="form-group row">
						    <label for="" class="col-sm-3 control-label">Centro</label>
						    <div class="col-sm-9">
								<select class="form-control" name="id_centro_costo"   style="width: 100%;">
										@foreach ($centro_costo as $centro)
											<option value="{{$centro->id}}">{{$centro->nombre}}</option>
										@endforeach
								</select>
						    </div>
					    </div>
						

						<div class="form-group row">
						    <label for="" class="col-sm-3 control-label">Concepto</label>
						    <div class="col-sm-9">
								<textarea class="form-control" name="concepto" rows="3"></textarea>
							</div>
					    </div>


						<div class="form-group row">
						    <label for="" class="col-sm-3 control-label">Persona</label>
						    <div class="col-sm-9">
								<select class="form-control select2" name="id_beneficiario"   style="width: 100%;">
										@foreach ($beneficiario as $ben)
											<option value="{{$ben->id}}">{{$ben->nombre}} {{$ben->apellido}}</option>
										@endforeach
								</select>
						    </div>
					    </div>

						 <div class="form-group row">
							    <label for="" class="col-sm-3 control-label">Cambio</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" name="cambio"  placeholder="">
							    </div>
						</div>

						<div class="form-group row">
							    <label for="" class="col-sm-3 control-label">Importe ( )</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" name="importe"  placeholder="">
							    </div>
						</div>

						<div class="form-group row">
							    <label for="" class="col-sm-3 control-label">Importe ML</label>
							    <div class="col-sm-9">
							      <input type="text" class="form-control" name="importe_ml"  placeholder="">
							    </div>
						</div>
	  		   </form>	
	      </div>
	      <div class="modal-footer">
	      	 <button type="button" id="btnSendMovimiento" class="btn btn-success btn-lg"><b>Confirmar</b></button>
	      </div>
	    </div>
	  </div>
	</div>

	
     {{-- ========================================
   			MODAL PARA VER HISTORIAL DE MOVIMIENTOS
   		  ========================================  --}}		


	<div class="modal fade" id="modalHistorial">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header">
			  <h5 class="modal-title">Consulta Historial</h5>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<form class="row" id="historial_formulario">
						<div class="col-12">
							<div class="form-group">
								<label>Cuenta</label>
								<select class="form-control select2" name="tipo_cuenta" id="">
									<option value="">Todos</option>
									<option value="1">Bancarias</option>
									<option value="false">No Bancarias</option>
	
								</select>
							</div>
						</div>

						<div class="col-12">
							<div class="form-group">
								<label>Tipo Cuenta</label>
								<select class="form-control select2" name="tipo_cuenta_bancaria" id="">
									<option value="">Todos</option>
									<option value="operativa">Cuenta Operativa</option>
									<option value="no_operativa">No Operativa</option>
	
								</select>
							</div>
						</div>
	
						<div class="col-12">
							<div class="form-group">
								<label>Cuenta de Fondo Padre</label>
								<select class="form-control select2" name="id_banco" id="">
									<option value="">Todos</option>
									@foreach($bancos as $banco)
										<option value="{{$banco->id}}">{{$banco->nombre}}</option>
									@endforeach
	
								</select>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label>Fecha de Historico</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control fecha_modal" name="fecha" id="" />
								</div>
							</div>
						</div>
	
	
		
	
					</form>
				</div>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
			  <button type="button" class="btn btn-primary" onclick="consultaCuentaBanco(true)" data-dismiss="modal">Buscar</button>
			</div>
		  </div>
		</div>
	  </div>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>





		{{--==========================================
					VARIABLES GLOBALES
			==========================================--}}
			var ordenamiento = [];
			var table;

			$(()=>{
				consultaCuentaBanco();
				$('.select2').select2();
				$('.calendario').val('');
				calendar();
			});

			$('#tipo_cuenta').val(1).trigger('change.select2');

			$('#btnModalMovimiento').on('click',function(){
				$('#modalMovimiento').modal('show');
			});


	


		

		function calendar(){

			$( ".fecha_modal" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									}).datepicker("setDate", new Date());

			$( ".fecha" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$('.calendario').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			});
			$('.calendario').val('');
			$('.calendario').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

			  $('.calendario_detalle').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    },
														startDate: new Date() ,
	        										endDate: new Date()
								    			});
			// $('.calendario_detalle').val('');
			$('.calendario_detalle').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
		}
			

		

		function eventos(){

						$('#btnSendMovimiento').click(function(){

							$.ajax({
									type: "GET",
									url: "{{ route('registrarMovimiento') }}",
									dataType: 'json',
									data: $('#formMovimientoBancario').serialize(),

										error: function(){
											console.log('Error');
										},
										success: function(rsp){
											console.log(rsp);
									
										}
								})

							});


						$('#banco_m').change(function(){
							let select = $(this).val();

							$('#cuenta_banco_m').val();
							$('#cuenta_banco_m option[data-bank='+select+']').show();
							$('#cuenta_banco_m option[data-bank!='+select+']').hide();
							
					});
		}



	





		{{--==========================================
					BUSQUEDA POR CUENTA DE BANCO
			==========================================--}}


	function consultaCuentaBanco(historial = false){

			$.blockUI({
											centerY: 0,
											message: '<div class="loadingC"><img src="images/loading.gif" alt="Procesando..." style="width:100px"/><br><h2>Cargando ...</h2></div>',
											css: {
												color: '#000'
											}
										});

			// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
			setTimeout(function (){

					let data_consulta = '';
					let url_consulta = '';

					if(historial){
						url_consulta = "{{route('banco.index.historico.ajax')}}";
						data_consulta = $('#historial_formulario').serializeJSON();
					} else {
						url_consulta = "{{route('ajaxReporteCuentaBanco')}}";
						data_consulta = $('#reporteCuentaBanco').serializeJSON();
					}
				
						
	 		table =   $('#listadoCuentaBanco').DataTable({
					"destroy": true,
					"ajax": {
							data: data_consulta,
							url: url_consulta,
							type: "GET",
					error: function(jqXHR,textStatus,errorThrown){

				   $.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});
					$.unblockUI();
				}

				  },
				 "columns":[
				 				{
				 					data: function(x)
				 					{
				 						if (x.saldodetalle < 0 || x.saldodetalle == null) 
				 						{
				 							return '<i class="fa fa-exclamation-triangle" style="color:red"></i><div style="display: none;">3</div>';
				 						}
				 						else
				 						{
				 							if (x.saldodetalle == 0) 
				 							{
				 								return '<i class="fa fa-exclamation-triangle" style="color:yellow"></i><div style="display: none;">2</div>';
				 							}
				 							else
				 							{
				 								return '<i class="fa fa-thumbs-o-up" style="color:green"></i><div style="display: none;">1</div>';
				 							}
				 						}
									}
								},
								{data:'banco_n'},
								{data:(x) => {
									if(x.numero_cuenta){
										return x.numero_cuenta;
									} else {
										return '0';
									}
								}},
								{data:'currency_code'},
								{data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.saldodetalle}">`;
											}
									 },
								{data:'tipo_cuenta_banco_n'},
								{data: function(x){
											status = '';

											if(x.tipo_data == 'actual'){
												if(x.activo == true){
													status = 'SI';
												}else{
													status = 'NO';
												}
											} else {
												return 'N/A';
											}
											
											return status;
										}
									 },
								{data: 'fecha_data'},
								{data: function(x){
									if(x.tipo_data == 'actual'){
										return `<button  class='btn btn-info' onclick='verMovimiento(${x.id_banco_detalle})' type='button'><i class='fa fa-fw fa-search'></i></button>`;
									} else {
										return '';
									}	
								}}
							 
							]
				
				}).on('xhr.dt', function ( e, settings, json, xhr ){
						//Ajax event - fired when an Ajax request is completed.;
					 $.unblockUI();
				}).on('draw', function () {
	       		/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

			    $('#listadoCuentaBanco tbody tr .format-number').inputmask("numeric", {
					radixPoint: ",",
					groupSeparator: ".",
					digits: 2,
					autoGroup: true,
					 // prefix: '$', //No Space, this will truncate the first character
					rightAlign: false
				});

    	});
				
	 
					}, 300);

}//function




			{{--==========================================
					MOVIMIENTO BANCARIO
			==========================================--}}



	function consultaMovimientoBancario(){

		$.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando....</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });

		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){
			// `<div class="click_estrella" onclick="mostrar_postVenta(${x.id})">`
					
			 table2 =   $('#listadoMovimientoBancario').DataTable({
					        "destroy": true,
							"order": [],
					        "ajax": {
					        		data: $('#reporteMovimientoBancario').serializeJSON({customTypes: customTypesSerializeJSON}),
						            url: "{{route('reporteBanco')}}",
						            type: "GET",
						    error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                            $.unblockUI();
                        }
                        		// formatter.format(parseFloat(x.total_venta_proforma));
						  },
						 "columns":[

									{data: "id"},
									{data: "fecha_emision_format"},
									{data: "fecha_hora_creacion_format"},
						 			{
						 				data: function(x)
						 				{
											if(x.id_tipo_documento == 35 | x.id_tipo_documento == 7 | x.id_tipo_documento == 34 
											  | x.id_tipo_documento == 13 |x.id_tipo_documento == 8 | x.id_tipo_documento == 3 
											  | x.id_tipo_documento == 16 | !x.activo){
												return `<input type="hidden" class="activeCheck" value="false" />${x.banco_n}`;
											  } else {
												return `<input type="hidden" class="activeCheck" value="true" />${x.banco_n}`;
											  }
	                              		 	
	                         		
						 				}
						 			},

									{data: "numero_cuenta"},
									//{data: "beneficiario_n"},
									/*{data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.monto}">`;
											}
									 },*/
									{data: "currency_code"},
									//
									//
									//{data: "cuenta_n"},
						 			{data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.debe}">`;
											}
									 },
									 {data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.haber}">`;
											}
									 },
									 {data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.saldo}">`;
											}
									 },
						 			{data: "concepto"},
						 			{data: "tipo_operacion_n"},
						 			{data: "nro_comprobante"},
						 			{data: "suc_nombre"},
						 		   ],
						 		   "columnDefs": [
												    {
												        targets: 0,
												        className: 'hover',
												        visible: false,
												    },

												  ],
						     "createdRow": function ( row, data, index ) {
						     	if ($(row).find('td .activeCheck').val() == 'false') {
                        					$(row).addClass('tachado bg-red bg-lighten-4')
                    			} 
						      
						        				}			
						                    
					    }).on('xhr.dt', function ( e, settings, json, xhr ){
								//Ajax event - fired when an Ajax request is completed.;
							 $.unblockUI();

						}).on('draw', function () {
	       		/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

			    $('#listadoMovimientoBancario tbody tr .format-number').inputmask("numeric", {
					radixPoint: ",",
					groupSeparator: ".",
					digits: 2,
					autoGroup: true,
					 // prefix: '$', //No Space, this will truncate the first character
					rightAlign: false
				});

    	});
					    
			 
					    	}, 300);

}//function


			{{--==========================================
					INTERACCION DE PESTAÑAS
			==========================================--}}

				function verMovimiento(num){
					$('#rm_cuenta_detalle').val(num).trigger('change.select2');
					consultaMovimientoBancario();
					$('#tabMovimiento').tab('show');
				}


	function anularMovimientoBancario(id_documento, id_tipo_documento, id_banco_detalle, id_moneda) {
		return swal({
                    title: "GESTUR",
                    text: "¿Está seguro que desea eliminar el movimiento?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Eliminar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                          	//alert(id_detalle);
////////////////////////////////////////////////////////////////////////////////////////////////
								    $.ajax({
								        type: "GET",
								        url: "{{route('anularMovimientoBancario')}}",
								        dataType: 'json',
								        data: {
											id_documento:id_documento,
											id_tipo_documento: id_tipo_documento,
											id_banco_detalle: id_banco_detalle, 
											id_moneda: id_moneda
								        },
								        error: function (jqXHR, textStatus, errorThrown) {
								            $.toast({
								                heading: 'Error',
								                text: 'Ocurrió un error al intentar eliminar la línea.',
								                position: 'top-right',
								                showHideTransition: 'fade',
								                icon: 'error'
								            });
								        },
								        success: function (rsp) {
								        	if(rsp.msj == "OK"){
								        		// itemsDatatable();
								        		swal("Éxito", rsp.msj, "success");	
								        		location. reload();
								        	}else{
								        		swal("Cancelado", rsp.msj, "error");
								        	}
								        } //function
								    });
///////////////////////////////////////////////////////////////////////////////////////////////
                        } else {
                            swal("Cancelado", "", "error");
                        }
            	});
	}

		function modificarElemento(id){	
			dataString = { id_movimiento: id};
			$.ajax({
					type: "GET",
					url: "{{route('getConceptos')}}",
					dataType: 'json',
					data: dataString,
							success: function(rsp){
							console.log(rsp);
							$('#idMovimiento').val(rsp[0].id);
							var creacion = rsp[0].fecha_hora_creacion.split(' ');
							var fechaCreacion = creacion[0].split('-');

							$('#fecha_creacion').val(fechaCreacion[2]+"/"+fechaCreacion[1]+"/"+fechaCreacion[0]);
							$('#concepto').val(rsp[0].concepto);
							$('#modalEditMovimiento').modal('show');
						}
				})	

		}

		function limpiar(){
			$('#id_cuenta_detalle').val('').trigger('change.select2');
			$('#id_banco').val('').trigger('change.select2');
			$('#tipo_cuenta').val('').trigger('change.select2');
		}


		function limpiarMovimientoBancario(){
			$('#rm_cuenta_detalle').val('').trigger('change.select2');
			$('#periodo_fecha').val('');

		}

		function guardarMovimiento(){
			$.ajax({
					type: "GET",
					url: "{{ route('editarMovimiento')}}",
					dataType: 'json',
					data: $('#formMovimientoBancarios').serialize(),
					error: function(){
							console.log('Error');
					},
					success: function(rsp){
									console.log(rsp);
									if(rsp.err == false){
										$.toast({
										    heading: 'Atención',
										    position: 'top-right', 
										    text: rsp.mensaje,
										    showHideTransition: 'slide',
										    icon: 'info'
										});
										$("#modalEditMovimiento").modal('hide');
										consultaMovimientoBancario()
									}else{
										$.toast({
				                            heading: 'Error',
				                            text: rsp.mensaje,
				                            position: 'top-right',
				                            showHideTransition: 'fade',
				                            icon: 'error'
				                        });
									}
									
								}
					})
		}	


		function mostrar_postVenta(id_proforma){
			$('.valoracion').html('');
			$('#comentarioPostVenta').html('');

			dataString = { id_proforma: id_proforma};


				$.ajax({
					type: "GET",
					url: "{{route('getpostventa')}}",
					dataType: 'json',
					data: dataString,
							success: function(rsp){
						
								
						}
				})	


		}

	$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
				$('#reporteMovimientoBancario').attr("method", "post");               
				$('#reporteMovimientoBancario').attr('action', "{{route('generarExcelMovimientoCuenta')}}").submit();
            });


	</script>
@endsection