@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
			.card,.card-header {
					border-radius: 14px !important;
					}
            .inputStyle{
                border: 0px;
                background-color:white;
                text-align: center;
            }    

            .select2{
                    width: 100% !important;
                }  

                .btnExcel  {
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;

            }

            .btnExcel {
                    padding: 10px 16px;
                font-size: 18px;
                line-height: 1.3333333;
                border-radius: 6px;
                }

                .btnExcel  {
                color: #fff;
                background-color: #5bc0de;
                border-color: #46b8da;
                }

            .btnExcel {
                    background-color: #e2076a !important; 
                    font-weight: 700;
                    /*margin: 0 0 10px 10px;*/
                }

                #botonExcel {
                    display: inline;
                }
                #botonExcel .dt-buttons {
                    display: inline;
                }  
	</style>
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Reporte Movimientos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
                <form class="row" id="formTransferencia" autocomplete="off">

                    <div class="col-12 col-sm-3 col-md-4">
						<div class="form-group">
							<label>Cuenta Origen</label>
							<select class="form-control select2" name="id_cuenta_origen" id="id_cuenta_origen">
								<option value="">Todos</option>
								@foreach($cuentas as $cuenta)
									<option value="{{$cuenta->id}}">{{$cuenta->banco_n}} {{$cuenta->denominacion}} {{$cuenta->numero_cuenta}} {{$cuenta->currency_code}}</option>
								@endforeach

							</select>
						</div>
                    </div>


                    <div class="col-12 col-sm-3 col-md-4">
						<div class="form-group">
							<label>Cuenta Destino</label>
							<select class="form-control select2" name="id_cuenta_destino" id="id_cuenta_destino">
								<option value="">Todos</option>
								@foreach($cuentas as $cuenta)
									<option value="{{$cuenta->id}}">{{$cuenta->banco_n}} {{$cuenta->denominacion}} {{$cuenta->numero_cuenta}} {{$cuenta->currency_code}}</option>
								@endforeach

							</select>
						</div>
                    </div>
                    
                    {{-- <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
							<label>Moneda</label>
							<select class="form-control select2" name="id_moneda" id="id_moneda">
								<option value="">Todos</option>
								@foreach($monedas as $moneda)
									<option value="{{$moneda->currency_code}}">{{$moneda->currency_code}}</option>
								@endforeach

							</select>
						</div>
                    </div> --}}

                    <div class="col-12 col-sm-3 col-md-4">
		 				<div class="form-group">
					        <label>Fecha Pedido</label>			
					            <div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class = "Requerido form-control input-sm" id="fecha_emision" name="fecha_emision" tabindex="10"/>
								</div>
						</div>	
					</div> 
                    <div class="col-12 col-sm-3 col-md-4">
                        <div class="form-group">
                            <label>Numero</label>
                            <input type="text" class="form-control"  name="delBancoNumOperacion" maxlength="5" value="">
                        </div>
                    </div>
                    <div class="col-12 col-sm-3 col-md-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <select class="form-control input-sm estado_proforma" name="id_estado_proforma[]" id="id_estado_proforma" style=" width: 100%;"  tabindex="5" >
                                @foreach($estados as $key=>$estado)
                                    <option value="{{$estado->id}}">{{$estado->denominacion}}</option>
                                @endforeach 
                            </select>
                        </div>
                     </div>      
                    <div class="col-12 col-sm-3 col-md-4">
                        <div class="form-group">
                            <label>Tipo de Movimiento</label>
                            <select class="form-control select2" name="tipo_transferencia" id="tipo_transferencia">
                                <option value="">Todos</option>
                                @foreach($tipoTransferencias as $key=>$tipoTransferencia)
                                    <option value="{{$tipoTransferencia->id}}">{{$tipoTransferencia->descripcion}}</option>
                                @endforeach 
                            </select>
                        </div>
                     </div>         
                     <div class="col-12 col-sm-3 col-md-4">
		 				<div class="form-group">
					        <label>Fecha de Proceso</label>			
					            <div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class = "Requerido form-control input-sm" id="fecha_proceso" name="fecha_proceso" tabindex="10"/>
								</div>
						</div>	
					</div> 
                    <div class="col-12 col-sm-6 col-md-5 col-lg-3">
                        <div class="form-group">
                            <label for="">Cuenta Bancaria</label>
                            <select class="select2 form-control" name="cuenta_bancaria" id="cuenta_bancaria" style="width: 100%;">
                                <option value="">Todos</option>
                                <option value="false">No</option>
                                <option value="true">Sí</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-3 col-md-4"></div> 
                    <div class="col-12 col-sm-3 col-md-4">
                        <br>
                        <div class="pull-right mr-1" id="botonExcel"></div>
                        <button type="button" onclick="limpiar()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
                        <button type="button" id="btnBuscar" class="btn btn-info pull-right btn-lg mr-1"><b>Buscar</b></button>
                    </div>

                </form>


               

                <div class="table-responsive table-bordered">
					<table id="listado" class="table" style="width:100%">
						<thead>
							<tr>
                                <th>N°</th>
                                <th></th>
                                <th>Estado</th>
                                <th>Acciones</th>
                                <th>Cuenta Origen</th>
                                <th>Importe Egreso</th>
                                <th>Moneda</th>
                                <th>Comprobante</th>
                                <th>Cuenta Destino</th>
                                <th>Importe Ingreso</th>
                                <th>Moneda</th>
                                <th>Comprobante</th>
                                <th>Cotización</th>
                                <th>Concepto</th>
                                <th>Fecha Pedido</th>
                                <th>Usuario</th>
                                <th>Fecha Proceso</th>
                                <th>Tipo Transferencia</th>
                                <th></th>
                                <th>Cuenta Bancaria</th>
                                
							</tr>
						</thead>
						<tbody style="text-align: center">

						</tbody>
					</table>
				</div>


			</div>
		</div>
	</div>
</section>

 {{-- ========================================
            EDITAR MOVIMIENTO BANCARIO
    ========================================  --}}      

    <div class="modal fade" id="modalEditMovimiento" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" style="margin-left: 25%;margin-top: 5%;">
        <div class="modal-content" style="width: 150%;">
          <div class="modal-header">
            <h4 class="modal-title"  style="font-weight: 800;">Modificar Movimiento Bancario <i style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
            <button type="button" class="closeModal btn-danger"  data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
                <form id="formMovimientoBancarios" class="form-horizontal form-group-sm" enctype="multipart/form-data"> 

                        <input type="hidden" class="form-control" id="idMovimiento" name="idMovimiento"  placeholder="">
                        <div class="form-group row">
                                <label for="" class="col-sm-3 control-label">Fecha Creación</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control fecha" id="fecha_creacion" name="fecha_creacion"  placeholder="">
                                </div>
                        </div>

                        <div class="form-group row">
                                <label for="" class="col-sm-3 control-label">Concepto</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" id="concepto" name="concepto"  placeholder="">
                                </div>
                        </div>
                        <div class="row">
                            <div class="content" id='mostrar_adjunto_modificar'></div>
                            <div class="col-12">
                                <small style="color:blue;">Para sustituir el archivo el archivo , solo suba otro documento</small>
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
                            <input type="file" class="form-control" name="image" id="image_modal_modificar"/>  
                            <h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-top: 10px;"><b>Formatos válidos: <span class="text-uppercase">pdf, xls, doc, docx, pptx, pps, jpeg, bmp, png y jpg.</span></b>
                            </h4>
                            <div id="validation_errors_adjunto_modificar"></div>
                        </div>


                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="id_adjunto" id="id_adjunto"/> 
               </form>  
          </div>
          <div class="modal-footer">
             <button type="button" onclick="guardarMovimiento()" id="btnEditarMovimiento" class="btn btn-success btn-lg"><b>Confirmar</b></button>
          </div>
        </div>
      </div>
    </div>

     {{-- ========================================
            EDITAR MONTO
    ========================================  --}}      

    <div class="modal fade" id="modalEditarMonto" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" style="margin-left: 25%;margin-top: 5%;">
          <div class="modal-content" style="width: 150%;">
            <div class="modal-header">
              <h4 class="modal-title"  style="font-weight: 800;">Modificar Monto <i id="spin_modificar_monto" style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
              <button type="button" class="closeModal btn-danger"  data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                  <form id="formEditarMonto" class="form-horizontal form-group-sm"> 
  
    
                          <div class="form-group row">
                              <label for="importe_modal_modificar" class="col-sm-3 control-label">Importe</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" id="importe_modal_modificar" name="importe"  placeholder="">
                              </div>
                          </div>
  
                          <div class="form-group row">
                              <label for="cotizacion_modal_modificar" class="col-sm-3 control-label">Cotizaciòn</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" id="cotizacion_modal_modificar" name="cotizacion"  placeholder="">
                              </div>
                          </div>

                          <div class="form-group row">
                            <label for="importe_cotizado_modal_modificar" class="col-sm-3 control-label">Importe Cotizado</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="importe_cotizado_modal_modificar" name="importe_cotizado"  placeholder="">
                            </div>
                        </div>
  
  
                          <input type="hidden" name="id_movimiento" id="modalEditarIdMovimiento" value="" />
                 </form>  
            </div>
            <div class="modal-footer">
               <button type="button" onclick="actualizarMonto()" id="btnEditarMonto" class="btn btn-success btn-lg"><b>Confirmar</b></button>
            </div>
          </div>
        </div>
      </div>

{{-- ========================================
            MOSTRAR DOCUMENTO BANCARIO
    ========================================  --}}      

    <div class="modal fade" id="modalDocumentoMovimiento" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" style="margin-left: 9%;margin-top: 3%;">
        <div class="modal-content" style="width: 190%;">
          <div class="modal-header">
            <h4 class="modal-title"  style="font-weight: 800;">Documento <i style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
            <button type="button" class="closeModal btn-danger"  data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
             <div>
                <img src="" class="img-responsive hidden" id="imgMostrar" style="overflow: scroll;width: 100%;">
                <iframe id="verPdf" src="" class="hidden" style="width: 100%;height: 400px;"></iframe>
             </div>   
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

<!--======================================================
        MODAL DE ADJUNTO IMAGEN
========================================================== -->
    <!-- /.content -->
    <div id="modalAdjunto" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
        <!-- Modal content-->
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto </h2>
                </div>
                <input type="hidden" class = " form-control input-sm" id="idLinea" name="cantidad_habitaciones"/>
                <div class="modal-body">
                            <div class="content" id='output'>

                            </div>    
                            <form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('uploadDocumentosBancario')}}" autocomplete="off">
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-12">
                                        <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="id_transaccion" id="id_transaccion"/> 
										<input type="file" class="form-control" name="image" id="image"/>  
										<h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: <span class="text-uppercase">pdf, xls, doc, docx, pptx, pps, jpeg, bmp, png y jpg.</span></b>
                                        </h4>
                                        <div id="validation-errors"></div>

                                      </div> 
                                    </div>  
                                  </div>  
							  </form>
							  
                            <div class="row">
                                <div class= "col-12">
                                    <button type="button" id ="aceptar" class="btn btn-info font-weight-bold pull-right">ACEPTAR</button>                        
                                </div>
                            </div>
             
                    </div>
                </div>  
            </div>  
        </div>  

@endsection
@section('scripts')
    @include('layouts/gestion/scripts')
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

    <script>

var tableLoad2;

$(()=>{ 
  
    $('.select2').select2();
   
    $('#id_estado_proforma').select2({
                                    multiple:true,
                                    placeholder: 'Todos'
                                });
    
    $('#id_estado_proforma').val('57').trigger('change');
     tableLoad2 = tableLoad();

 });

    $(document).ready(function(){
        var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('input[name="fecha_emision"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="fecha_emision"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
		});

		$('input[name="fecha_proceso"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="fecha_proceso"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
		});
        $('input[name="fecha_proceso"]').val(''); 
    });

    function initCalendar() {


        var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('input[name="fecha_emision"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="facturacion_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });

        $('input[name="fecha_proceso"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="fecha_proceso"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
		});
        $('input[name="fecha_proceso"]').val('');         
}


$('#btnBuscar').click(()=>{
    tableLoad();
});

/**
 * 
 * Vamos a modificar los datos antes de autorizar no despues
 * Eso dice Dani, pero seria mejor reconfirmarlo
*/


function tableLoad() {
    let form = $('#formTransferencia').serializeJSON({
        customTypes: customTypesSerializeJSON
    });

    tableLoad2 =  $("#listado").dataTable({
        destroy: true,
        ajax: {
            url: "{{route('ajaxReporteTransferenciaCuenta')}}",
            data: form,
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
						heading: '<b>Error</b>',
						position: 'top-right',
						text: 'Ocurrió un error en la comunicación con el servidor.',
						width: '400px',
						hideAfter: false,
						icon: 'error'
					});
            }

        },
        "columns": [
                {data: 'nro_transferencia'},
                {data: 'estado_n'},
                { "data": function(x){
                                    var acciones= ""; 
                                     if(x.id_estado == 57 && x.permiso == 1){
                                        acciones = '<div class="btn-group">';
                                        acciones += '<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                                        if(x.id_estado == 57){
                                            acciones += '<i class="fa fa-clock-o" style="color:yellow; font-size: large;"></i>';   
                                        }
                                        if(x.id_estado == 58){
                                                acciones += '<i class="fa fa-folder-open" style="color:green; font-size: large;"></i>'; 
                                        }
                                        if(x.id_estado == 59){
                                            acciones += '<i class="fa fa-thumbs-down" style="color:red; font-size: large;"></i>';       
                                        }

                                        acciones += '</button>';
                                        acciones += '<ul class="dropdown-menu" style=" width: 250px !important;">';
                                        acciones += '<li class="btnHand">'
                                        acciones += '<a onclick="movimientoTransferencia('+x.id+',1)"  class="verificarStyle" title ="Autorizar Movimiento Bancario" id=""  style="color: green;"><i class="fa fa-fw fa-check"></i><b>Autorizar Movimiento Bancario</b></a>';
                                        acciones += '</li>';  
                                        acciones += '<li class="btnHand">'
                                        acciones += '<a onclick="movimientoTransferencia('+x.id+',2)"  class="verificarStyle" title ="Rechazar Movimiento Bancario" id=""  style="color: red;"><i class="fa fa-fw fa-times"></i><b>Rechazar Movimiento Bancario</b></a>';
                                        acciones += '</li>';
                                            @if($permiso_modificar_transferencia)
                                                acciones += '<li class="btnHand">'
                                                acciones += '<a onclick="modificarTransferencia('+x.id+','+x.importe+','+x.cotizacion+','+x.importe_cotizado+')"  class="verificarStyle" title ="Modificar Monto" id=""  style="color: #c5c572;"><i class="fa fa-fw fa-check"></i><b>Modificar Monto</b></a>';
                                                acciones += '</li>';  
                                            @endif
                                        acciones += '</ul>';
                                        acciones += '</div>';
                                    }else{
                                        if(x.id_estado == 57){
                                            acciones += '<i class="fa fa-clock-o" style="color:blue;font-size: large;"></i>';   
                                        }
                                        if(x.id_estado == 58){
                                            if(x.id_usuario_descarga !== null){
                                                acciones += '<i id="" class="fa fa-thumbs-up" style="color:green;font-size: large;"></i>';   
                                            }else{
                                                acciones += '<i id="indicador'+x.id+'" class="fa fa-thumbs-up" style="color:blue;font-size: large;"></i>';   
                                            }
                                        }
                                        if(x.id_estado == 59){
                                            acciones += '<i class="fa fa-thumbs-down" style="color:red;font-size: large;"></i>';       
                                        }

                                        if(x.id_estado == 76){
                                            acciones += '<i class="fa fa-ban" aria-hidden="true" style="color:red;font-size: large;"></i>';       
                                        }

                                        
                                    }
                                    return acciones;               
                }},
                 { "data": function(x){
                                    var acciones= ""; 
                                     if(x.id_estado == 58){
                                        acciones = '<div class="btn-group">';
                                        acciones += '<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                                        acciones += '<i class="fa fa-folder-open" style="color:green; font-size: large;"></i>';   
                                        acciones += '</button>';
                                        acciones += '<ul class="dropdown-menu" style=" width: 250px !important;">';
                                            @if($permiso_modificar_transferencia)
                                                acciones += '<li class="btnHand">'
                                                acciones += '<a onclick="modificarElemento('+x.id+')"  class="verificarStyle" title ="Modificar Transferencia" id=""  style="color: #1ab0c3;"><i class="fa fa-pencil-square-o"></i><b>Modificar Transferencia</b></a>';
                                                acciones += '</li>';
                                            @endif

                                            acciones += '<li class="btnHand">'
                                            acciones += '<a onclick="anularTransferencia('+x.id+')"  class="verificarStyle" title ="Anular Transferencia" id=""  style="color: #1ab0c3;"><i class="fa fa-fw fa-times"></i><b>Anular Transferencia</b></a>';
                                            acciones += '</li>';  
                                            acciones += '<li class="btnHand">'
                                            acciones += '<a href="resumenTrasnferencia/'+x.id+'" class="verificarStyle actualizar estado" title ="Descargar Resumen" onclick="actualizar('+x.id+')"  style="color: #1ab0c3;"><i class="fa fa-file-pdf-o"></i><b>Descargar Resumen</b></a>';
                                            acciones += '</li>';  

                                        acciones += '</li>';
                                        acciones += '</ul>';
                                        acciones += '</div>';
                                    }
                                    return acciones;               
                }},
                {data: 'banco_origen_n'},
                {data: (x) =>{ return formatter.format(parseFloat(x.importe))}},
                {data:"b_origen_currency_code"},
                {data:"nro_comprobante_origen"},
                {data:"banco_destino_n"},
                {data: (x) =>{ return formatter.format(parseFloat(x.importe_cotizado))}},
                {data:"b_destino_currency_code"},
                {data:"nro_comprobante_destino"},
                {data: (x) =>{ return formatter.format(parseFloat(x.cotizacion))}},
                {data: "concepto"},
                {data: "fecha_transferencia_format"},
                {data: "usuario_n"},
                {data: "fecha_hora_procesado_format"},
                {data: "tipo_transferencia"},
                { "data": function(x){
                                    var acciones= ""; 
                                    acciones=`<button onclick="getMovimientoDetalle(${x.id})" class='btn btn-info btnEditHidden' type='button'><i class="fa fa-pencil-square-o"></i></button>`
                                    return acciones;               
                }},
                {
    data: "cuenta_bancaria",
    visible: false,
    render: function (data, type, row) {
        if (data === true) {
            return "Sí";
        } else if (data === false) {
            return "No";
        } else {
            return data; // Maneja otros valores si es necesario
        }
    }
},
            ],
            "order": [0, "desc"],
            "columnDefs": [
                    {
                        "targets": [ 1 ],
                        "visible": false
                    }
                ]
    });



             /*Se define las tablas que se van a mostrar en un principio y 
			 se envia el objeto table para cargar las configuraciones.*/
             buttonExcel([0,1,4,5,6,7,8,9,10,11,12,13,14,15,16,17,19],tableLoad2);  

			  return tableLoad2;

}

    function actualizar(id){ 
        $('#indicador'+id).css('color', 'green');
    }

    function modificarTransferencia(id,importe,cotizacion, importe_cotizado){ 
        
        $('#cotizacion_modal_modificar').val(cotizacion);
        $('#importe_modal_modificar').val(importe);
        $('#importe_cotizado_modal_modificar').val(importe_cotizado);
        $('#modalEditarIdMovimiento').val(id);
        $('#modalEditarMonto').modal('show');
    }

    function actualizarMonto(id){ 

        $('#spin_modificar_monto').show();
        $('#btnEditarMonto').prop('disabled',true);

            $.ajax({
                    type: "GET",
                    url: "{{route('modificarTransferencia')}}",
                    dataType: 'json',
                    data: $('#formEditarMonto').serialize(),
                    error: function(){
                        $.toast({	
							heading: '<b>Error</b>',
							position: 'top-right', 
							text: 'Ocurrió un error en la comunicación con el servidor',
							width: '400px',
							hideAfter: false,
							icon: 'error'
						});

                        $('#spin_modificar_monto').hide();
                        $('#btnEditarMonto').prop('disabled',false);
                    },
                    success: function(rsp){
                        $('#spin_modificar_monto').hide();
                        $('#modalEditarMonto').modal('hide');
                        $('#btnEditarMonto').prop('disabled',false);
                        swal("Éxito", 'Monto actualizado', "success");  
                        tableLoad()
                    }
                })  

    }

        

        function guardarMovimiento(){

            let formulario = new FormData($('#formMovimientoBancarios')[0])
          
            $.ajax({
                    type: "POST",
                    url: "{{ route('editarMovimiento')}}",
                    data: formulario,
                    processData: false,
  					contentType: false,
                    error: function(){
                        $.toast({	
							heading: '<b>Error</b>',
							position: 'top-right', 
							text: 'Ocurrió un error en la comunicación con el servidor',
							width: '400px',
							hideAfter: false,
							icon: 'error'
						});
                    },
                    success: function(rsp){
                                    console.log(rsp);


                                    if(rsp.err == false){
                                        $.toast({
                                            heading: 'Atención',
                                            position: 'top-right', 
                                            text: rsp.mensaje,
                                            showHideTransition: 'slide',
                                            icon: 'info'
                                        });
                                        $('#image_modal_modificar').val('');
                                        $("#modalEditMovimiento").modal('hide');
                                        tableLoad()
                                    }else{
                                        $.toast({
                                            heading: 'Error',
                                            text: rsp.mensaje,
                                            position: 'top-right',
                                            showHideTransition: 'fade',
                                            icon: 'error'
                                        });
                                    }
                                    
                                }
                    })
        }  

    function getMovimientoDetalle(id_transferencia){
          dataString = { id_movimiento: id_transferencia};
          $.ajax({
                    type: "GET",
                    url: "{{ route('movimientoBancarios')}}",
                    dataType: 'json',
                    data: dataString,
                    error: function(){
                            console.log('Error');
                    },
                    success: function(rsp){
                                    console.log(rsp);
                                    if(jQuery.isEmptyObject(rsp[0].adjunto) == false){
                                        var archivoName =rsp[0].adjunto;
                                        var extension = archivoName.split('.');
                                             archivo = extension[extension.length - 1];
                                             archivo = archivo.toLowerCase();

                                        var getUrl = window.location;
                                        console.log(getUrl);
                                        var baseUrl = getUrl .protocol
                                        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                                            
                                            console.log(baseUrl);
                                        if(archivo === 'jpg' || archivo === 'png'|| archivo === 'jpeg'){
                                            $('#imgMostrar').removeClass('hidden');
                                            $('#verPdf').addClass('hidden');
                                            direccion = '../uploadDocumento/'+archivoName;
                                            $('#imgMostrar').attr('src',direccion);
                                        }
                                        if(archivo === 'pdf' || archivo === 'docx'){
                                            $('#imgMostrar').addClass('hidden');
                                            $('#verPdf').removeClass('hidden');
                                            direccion = '../uploadDocumento/'+archivoName;
                                            $('#verPdf').attr('src',direccion);
                                        }
                                        $('#modalDocumentoMovimiento').modal('show');

                                    }else{
                                        $.toast({
                                                heading: '<b>Error</b>',
                                                position: 'top-right',
                                                text: 'El movimiento no tiene adjunto.',
                                                width: '400px',
                                                hideAfter: false,
                                                icon: 'error'
                                            });
                                    }
                     }
                })               

    }    

    function anularTransferencia(id_transferencia) {
        return swal({
                    title: "GESTUR",
                    text: "¿Está seguro que desea anular la transferencia?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Eliminar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            
                            $.ajax({
                                        type: "GET",
                                        url: "{{route('anularTransferencia')}}",
                                        dataType: 'json',
                                        data: {
                                            id_transferencia:id_transferencia
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            swal("Cancelado", 'Ocurrió un error al intentar anular la operación', "error");
                                        },
                                        success: function (rsp) {
                                            if(rsp.err){
                                                // itemsDatatable();
                                                swal("Éxito", rsp.msj, "success");  
                                                location. reload();
                                            }else{
                                                swal("Cancelado", 'Ocurrio un error al intentar anular la operación', "error");
                                            }
                                        } //function
                                    });
///////////////////////////////////////////////////////////////////////////////////////////////
                        } else {
                            swal("Cancelado", "", "error");
                        }
                });
    }

function movimientoTransferencia(id,tipo){
    if(tipo == 1){
        $('#output').html('');
        mensaje = 'Autorizar Transferencia';
        $('#id_transaccion').val(id);
        $('#modalAdjunto').modal('show');

    }else{
        mensaje = 'Rechazar Transferencia';
        transaccion(mensaje,id,tipo);
    }

}

$('#aceptar').click(()=>{
    if($("#image").val() != ""){
        id = $('#id_transaccion').val();
        mensaje = 'Autorizar Transferencia';
        tipo = 1;
        transaccion(mensaje,id,tipo);
        $('#modalAdjunto').modal('hide');
    }else{
        $.toast({
                heading: 'Error',
                text: 'No existe una imagen, subala por favor.',
                position: 'top-right',
                showHideTransition: 'fade',
                icon: 'error'
            });

    }   
})
////////////////////////////////////////////////////////////////////////////////////////////
function transaccion(mensaje,id,tipo){
            swal({
                    title: "Transferencia",
                    text: "¿Está seguro que desea "+mensaje+"?",
                    icon: "warning",
                    showCancelButton: true,
                    buttons: {
                        cancel: {
                            text: "No, Cancelar",
                            value: null,
                            visible: true,
                            className: "btn-warning",
                            closeModal: false,
                        },
                        confirm: {
                            text: "Sí, Aplicar",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(isConfirm => {

                    if (isConfirm) {
                             let form = {
                                'dataId': id,
                                'dataTipo':tipo
                            };

                          return  $.ajax({
                                type: "GET",
                                url: "{{route('getModificacionesTransferencias')}}",
                                data: form,
                                dataType: 'json',
                            });

                      
                    } else {
                        swal("Cancelado", "La operación fue cancelada", "error");
                    }
                }).then( data => {

                    swal.stopLoading();
                    if(data.err == false){
                          swal("Éxito", data.mensaje, "success");
                           tableLoad();
                    }else{
                         swal("Cancelado",data.mensaje, "error");
                    }

                
                }).catch(err => {
                    swal.stopLoading();
                    swal("Cancelado",'Ocurrio un error en la comunicación con el servidor', "error");
                });

        }
/////////////////////////////////////////////////////////////////////////////////////////////
var options = 
{
    beforeSubmit: showRequest,
    success: showResponse,
    dataType: 'json',
	error: function(){

		$.toast({
                heading: 'Error',
                text: 'Ocurrio un error al intentar subir la imagen.',
                position: 'top-right',
                showHideTransition: 'fade',
                icon: 'error'
            });

			$('.cargandoImg').hide();
	}
};

function buttonExcel(column,tableLoad2){

					var numFinal = 0, resultIndex = 0;
					$('#botonExcel').html('');
						var buttons = new $.fn.dataTable.Buttons(tableLoad2, { 
							    buttons: [{	
							    						title: 'Reporte Transferencia Cuenta',
										                extend: 'excelHtml5',
										                 text: '<b>Excel</b>',
										                 className: 'pull-right text-center btn btn-success btn-lg',
								                exportOptions: {
								                    columns: ':visible'
								                },
								                exportOptions: {
									                    columns:  column,
									                    format: {
									                    		//seleccionamos las columnas para dar formato para el excel
												                body: function ( data, row, column, node ) {
    												                	if(column == 3 || column == 7 || column == 10){
        																	numFinal = 0;
        																	numFinal = clean_num(data);
        												                    return  numFinal;
    												                		} 

    												                	return data;
												                }
												            }
									                },
									                //Generamos un nombre con fecha actual
									                filename: function() {
										               var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
										               // var selected_machine_name = $("#output_select_machine select option:selected").text()
										               return 'Reporte_Transferencia_Cuenta-'+date_edition;
										           }

								            }]
							}).container().appendTo('#botonExcel'); 
				}

                function limpiar() {


                    $('#id_cuenta_origen').val('').trigger('change.select2');
                    $('#id_cuenta_destino').val('').trigger('change.select2');
                    $('#fecha_emision').val('');
                }


                /*{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
                            NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}*/
                function clean_num(n, bd = false) {

                    if (n && bd == false) {
                        n = n.replace(/[,.]/g, function (m) {
                            if (m === '.') {
                                return '';
                            }
                            if (m === ',') {
                                return '.';
                            }
                        });
                        return Number(n);
                    }
                    if (bd) {
                        return Number(n);
                    }
                    return 0;
                }

                    /*
                    Para formatear numeros a moneda USD PY
                    */
                    const formatter = new Intl.NumberFormat('de-DE', {
                        currency: 'USD',
                        minimumFractionDigits: 2
                    });

                    $('body').delegate('#image', 'change', function () 
{
    //OBTENER TAMAÑO DE LA IMAGEN
    var input = document.getElementById('image');
    var file = input.files[0];
    var tamaño = file.size;

    if (tamaño > 0) {

        var tamaño = file.size / 1000;
        if (tamaño > 3000) {
            $("#image").val('');
            $("#validation-errors").empty();
            $("#validation-errors").append('<div class="alert alert-error"><strong>El tamaño de la imagen supera el limite de 3MB</strong><div>');
        } else {

            $('.cargandoImg').show();
            $('#upload').ajaxForm(options).submit();
        }

    } else {
        $("#image").val('');
        $("#validation-errors").empty();
        $("#validation-errors").append('<div class="alert alert-error"><strong>Existe un error con el archivo</strong><div>');
    }

});


function showRequest(formData, jqForm, options) 
{

    $("#validation-errors").hide().empty();
    return true;
}


function showResponse(rsp, statusText, xhr, $form) 
{
    $('.cargandoImg').hide();

    if (rsp.err == false) {
        $("#image").val('');
        $("#validation-errors").append('<div class="alert alert-error"><strong>' + rsp.errors + '</strong><div>');
        $("#validation-errors").show();

    } else {
        // $("#imagen").val(response.archivo);

        let file_name = rsp.archivo;
		let adjunto, divImagen = ''; 
        $('#output').html('');

		//TIPO ADJUNTO
		console.log('sdsd',rsp.imagen)
		if(rsp.imagen){
			adjunto = `
			<img class="card-img-top" style="max-width:150px;" src="{{asset('uploadDocumento')}}/${file_name}" alt="Adjunto de OP">
			`;
		} else {
			adjunto = `
			<img class="card-img-top" style="max-width:150px;" src="{{asset('uploadDocumento')}}/file.png" alt="Adjunto de OP">
			`;
		}


         divImagen = `
                    <div class="row" id="${file_name}">
						<div class="col-auto">
							<div class="card">
								<div class="card-content">
									<div class="card-body">
										${adjunto}
										<p><a href="{{asset('uploadDocumento')}}/${file_name}" class="card-title"  target="_black"><b>VER</b></a></p>
										<a href="#" class="btn btn-danger"  onclick="eliminarImagen('${file_name}')"><b>Eliminar</b></a>
									</div>
								</div>
							</div>
						</div>
                    </div>
                    `;

        $("#output").append(divImagen);
		op.adjunto = file_name;

        $("#image").prop('disabled', true);
    }

}

        function eliminarImagen(archivo){
			       // $("#btn-imagen").on("click", function(e){
			          $.ajax({
			              type: "GET",
			              url: "{{route('fileAdjuntoEliminarDocumento')}}",//fileDelDTPlus
			              dataType: 'json',
			              data: {
									dataFile:archivo
					       			},
			              success: function(rsp){
			                $("#imagen").val("");
			                $('#output').html('');
			              }
			       //   })      

			        });
		}  

  

    
    </script>
    @endsection
        
