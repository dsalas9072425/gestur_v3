<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Detalle OP</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 90%;
			margin:0 auto;
    		z-index:1;
	}
	
	table{
		width: 100%;

	}
	
	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 700;
	}

	.text{
		  overflow: hidden;
		  /*text-overflow: ellipsis;*/
		  white-space: nowrap;
		  display:block;
		  width:100%;
		  min-width:1px;
		}

	.f-10 {
		font-size: 10px !important;
	}

	.f-9 {
		font-size: 9px !important;
	}

	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}

	.f-15 {
		font-size: 15px !important;
	}
	.f-17 {
		font-size: 17px !important;
	}

	.c-text {
		text-align: center;
	}
	.r-text {
		text-align: left;
	}
	
	.r-text-detalle{
		margin-right: 20px;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 10px;
	}

	.b-buttom {
	border-bottom: 1px solid; 
	}

	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:120px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}



	</style>
</head>
<body>
	@forelse ($data as $dat)
		@php
			/*echo '<pre>';
			print_r($dat);*/
            $logoEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->logoEmpresa;
            $empresaLogo = "logoEmpresa/".$logoEmpresa;
        @endphp
		<br>
		<br>
		<div class="container espacio-10">
			<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
				<table>
					<tr>
						<td colspan="6" class="r-text">
								<br>
								<img src="{{$logo}}" style="width: 120px;height: 45px;">
						</td>
					</tr>
					<tr>
						<td colspan="6" class="c-text f-17">
							<b>TRANSFERENCIA</b>
						</td>
					</tr>
					<tr>
						<td colspan="6" class="r-text">
							<br>
						</td>
					</tr>	
					<tr>
						<td class="r-text f-15">
							Empresa :
						</td>
						<td colspan="5" class="r-text f-15">
							{{$empresa->denominacion}}
						</td>
					</tr>	
					<tr>
						<td class="r-text f-15">
							<label>Nro TRANSFERENCIA :</label>
						</td>
						<td class="r-text f-15">
							<?php 
								if(isset($dat->nro_transferencia)){
									$nro_transferencia = $dat->nro_transferencia;
								}else{
									$nro_transferencia = 0;
								}
							?>
							{{$nro_transferencia}}
						</td>
						<td class="r-text f-15">
							<label>Estado :</label>
						</td>
						<td class="r-text f-15">
								{{$dat->estado_n}}
						</td>
						<td class="r-text f-15">
							<label>Fecha Transferencia :</label>
						</td>
						<td class="r-text f-15">
								{{$dat->fecha_transferencia_format}}
						</td>

						<td class="r-text f-15">
							<label>Tipo :</label>
						</td>
						<td class="r-text f-15">
								{{$dat->tipo_transferencia}}
						</td>

					</tr>
				</table>	
				<table>
						<tr>	
							<td colspan="5" class="c-text b-buttom b-top"><b>Datos Transferencia</b></td>
						</tr>	
						<tr>
							<td class="r-text">Cuenta Origen</td>
							<td class="r-text">{{$dat->banco_origen_n}}</td>
							<td class="r-text">Cuenta Destino </td>
							<td class="r-text">{{$dat->banco_destino_n}}</td>
						</tr>
						<tr>
							<td class="r-text">Comprobante</td>
							<td class="r-text">{{$dat->nro_comprobante_origen}}</td>
							<td class="r-text"></td>
							<td class="r-text">{{$dat->nro_comprobante_destino}}</td>
						</tr>
						<tr>
							<td class="r-text">Moneda Origen</td>
							<td class="r-text">{{$dat->b_origen_currency_code}}</td>
							<td class="r-text">Moneda Destino </td>
							<td class="r-text">{{$dat->b_destino_currency_code}}</td>
						</tr>

						<tr>
							<td class="r-text">Importe</td>
							<td class="r-text">{{number_format($dat->importe,2,",",".")}}</td>
							<td class="r-text">Cotizacion </td>
							<td class="r-text">{{number_format($dat->cotizacion,0,",",".")}}</td>
						</tr>
						<tr>
							<td class="r-text"><br>Concepto</td>
							<td colspan = "3"class="r-text">{{$dat->concepto}}</td>
						</tr>
					</table>
					@if(isset($asiento_detalles[0]->num_cuenta))	
					<br>			
					<table>
						<tr>	
							<td colspan="5" class="c-text b-buttom"><b>Asiento:  {{$asiento_detalles[0]->id_asiento}}</b></td>
						</tr>	
						<tr>
							<td class="r-text b-buttom">Cuenta</td>
							<td class="r-text b-buttom">Denominación</td>
							<td class="r-text b-buttom">Suc.</td>
							<td class="r-text b-buttom">Debitos</td>
							<td class="r-text b-buttom">Creditos</td>
						</tr>
						@php 
							$debe = 0;
							$haber = 0;
							foreach($asiento_detalles as $key=>$adetalle){
								if($adetalle->sucursal !== null){
									$sucursal = $adetalle->sucursal;
								}else{
									$sucursal = '';
								}
								$debe = $debe + $adetalle->debe;
								$haber = $haber + $adetalle->haber;
						@endphp 		
						<tr>
							<td class="r-text f-11">{{$adetalle->num_cuenta}}</td>
							<td class="r-text f-11">{{$adetalle->cuenta_n}}</td>
							<td class="r-text f-11">{{$sucursal}}</td>
							<td class="r-text f-11">{{number_format($adetalle->debe,0,",",".")}}</td>
							<td class="r-text f-11">{{number_format($adetalle->haber,0,",",".")}}</td>
						</tr>		
						@php  
						}
						@endphp  

						<tr>
							<td style="border-top: solid 2px #000;text-align: right;" class="f-11" colspan="3"><b>TOTALES PYG</b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td style="border-top: solid 2px #000;" class="r-text f-11">{{number_format($debe,0,",",".")}}</td>
							<td style="border-top: solid 2px #000;" class="r-text f-11">{{number_format($haber,0,",",".")}}</td>
						</tr>		

					</table>
					<br>
				@endif	
				@empty
				<p style="font-size: 30px;font-weight: 800;">TRANSFERENCIA NO DISPONIBLE</p>
			@endforelse


</body>
</html>
