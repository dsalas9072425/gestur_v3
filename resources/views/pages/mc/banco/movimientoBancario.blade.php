
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">

	#formMovimientoBancario select option, select{
		font-size: 12px;
		font-weight: 800;
	}

	.select2{
		width: 100% !important;
	}

	.no-input{
		border:0 solid white;
		background-color: #fff;
	}


	input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

</style>


<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Movimiento Bancario</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
				<form id="formMovimientoBancario" class="row">
					<input type="hidden" class="form-control" name="tipo_operacion" id="tipo_operacion" maxlength="150" value="0">

					<div class="col-12 col-sm-4" id="inputdelBancoCuenta">
						<div class="form-group">
							<label>Tipo de Operación</label>
							<select class="form-control select2" name="id_tipo_movimiento" id="id_tipo_movimiento" required>
                                <option value="1" data-currency_code="0" data-id_currency="0">Depósito</option>
                                <option value="0" data-currency_code="0" data-id_currency="0">Extracción</option>
                                
							</select>
						</div>
                    </div>
	
					<div class="col-12 col-sm-4" id="inputalBancoCuenta">
						<div class="form-group">
							<label>Cuenta de Fondo</label>
							<select class="form-control select2" name="alBancoCuenta" id="alBancoCuenta" required>
                                <option value="">Seleccione Cuenta</option>
                                @foreach($bancoDetalle as $banco)
									<option value="{{$banco->id}}" data-currency_code="{{$banco->currency['currency_code']}}"  data-cuenta="{{$banco->banco_cab['cuenta_bancaria']}}" data-id_currency="{{$banco->id_moneda}}">{{$banco->banco_cab['nombre']}} - {{$banco->tipo_cuenta['denominacion']}} {{$banco->numero_cuenta}} {{$banco->currency['currency_code']}} </option>
                                @endforeach
							</select>
						</div>
                    </div>

                    <div  class="col-12 col-sm-4">
                        <div class="form-group">
                            <label>Cuenta Contable</label>
                            <select class="form-control select2" name="id_cuenta_contable" id="id_cuenta_contable" data-value-type="number" tabindex="19"  style="width: 100%;">

                                <option value="">Seleccione Cuenta Contable</option>
                                @foreach ($cuentasContables as $cuenta)

                                @if($cuenta->asentable)
                                <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    

                                    <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                    </option>
                                </optgroup>
                                @endif

 
                                @endforeach


                            </select>
                        </div>
                    </div>


					<div id="divCheque" class="col-12" style="display: none">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-4" id="inputfecha_emision">
								<div class="form-group">
									<label>Fecha de Emisión</label>
									<div class="input-group">
										<div class="input-group-prepend click_celandar">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
										<input type="text" class="form-control pull-right calendar"  name="fecha_emision_ch:s_date" id="fecha_emision_ch"
											value="" maxlength="10">
									</div>
								</div> 
							</div>
							<div class="col-12 col-sm-4 col-md-4" id="inputfecha_emision">
								<div class="form-group">
									<label>Fecha de Vencimiento</label>
									<div class="input-group">
										<div class="input-group-prepend click_celandar">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
										<input type="text" class="form-control pull-right calendar"  name="fecha_vencimiento:s_date" id="fecha_vencimiento"
											value="" maxlength="10">
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									<label>Número Cheque</label>
									<input type="number" class="form-control" name="num_cheque" value="0">
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-12 col-sm-4">
								<div class="form-group">
									<label>Al Portador</label><br>
									<input type="checkbox" id="portador_check" class="form-control" style="width: 50px; height: 18px;margin-top: 10px;" name="portador" value="true">
								</div>
							</div>
							<div class="col-12 col-sm-8">
								<div class="form-group">
									<label>Beneficiario</label>
									<input type="text" class="form-control" id="beneficiario"  name="beneficiario" maxlength="150" value="">
								</div>
							</div>
						</div>	
						<div class="col-12">
							<hr class="mt-0">
						</div>
					</div>	
					<div class="col-12 col-sm-4 col-md-4">
						<div class="form-group">
							<label>Sucursal</label>
							<select class="form-control select2" name="id_sucursal">
								@foreach($sucursalEmpresa as $suc)
									<option value="{{$suc->id}}">{{$suc->denominacion}}</option>
								@endforeach	
							</select>
						 </div>
					</div>

					<div class="col-12 col-sm-4 col-md-4">
						<div class="form-group">
							<label>Centro Costo</label>
							<select class="form-control select2" name="id_centro_costo">
								@foreach($centro as $cent)
									<option value="{{$cent->id}}">{{$cent->nombre}}</option>
								@endforeach
							</select>
						 </div>
					</div>

					<div class="col-12 col-sm-4 col-md-4" id="inputfecha_emision">
						<div class="form-group">
							<label>Fecha</label>
							<div class="input-group">
								<div class="input-group-prepend click_celandar">
									<span class="input-group-text"><i class="fa fa-calendar"></i></span>
								</div>
								<input type="text" class="form-control pull-right calendar"  name="fecha_emision:s_date" id="fecha_emision"
									value="" maxlength="10">
							</div>
						</div>
					</div>

					<div class="col-12 col-sm-6 col-md-4" id="inputimporte">
						<div class="form-group">
							<label>Importe</label>
								<div class="input-group">
									<input type="text" class="form-control h3 font-weight-bold format-number" name="importe:convertNumber" id="importe" maxlength="15" value="0" >
									<div class="input-group-append">
										<span class="input-group-text font-weight-bold" id="txtMoneda">---</span>
									</div>
								</div>
							</div>
					</div>


					<div class="col-12 col-sm-6 col-md-4" id="inputcotizacion">
						<div class="form-group">
							<label>Cotización</label>
								<input type="text" class="form-control format-number" maxlength="10" name="cotizacion:convertNumber" id="cotizacion" value="0" >
							</div>
					</div>

					<div class="col-12 col-sm-4">
						<div class="form-group">
							<label>Número Comprobante</label>
								<input type="text" class="form-control" name="nro_comprobante" maxlength="150" value="0">
							</div>
					</div>

					<input type="hidden" class="form-control" name="imagen" id="imagen">
					<div class="col-12">
						<div class="form-group">
							<label>Concepto</label>
								<input type="text" class="form-control" name="concepto" value="" maxlength="500">
							</div>
					</div>
				</form>	
 				<div class="row"> 
				    <form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{ url('uploadDocumentosBancario') }}" autocomplete="off">
		                <div class="form-group">
		                    <div class="row">
		                        <div class="col-md-12">
			                        <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
			                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
			                        <input type="file" class="form-control" name="image" id="image" style="margin-left: 3%; width: 96%"/>  <h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: PNG, JPG, PDF</b></h4>
			                        <div id="output"></div>
		                            <div id="validation-errors"></div>
		                        </div> 
		                    </div>  
		                </div>  
		            </form>
		        </div>    
					<div class="col-12">
						<button type="button" class="btn btn-info pull-right mt-2 mb-2" onclick="modalConfirmarMovimiento()">
							<i class="fa fa-check-circle"></i><b>Confirmar Movimiento</i> 
						</button>
					</div>
            </div>
        </div>
    </div>
</section>
<!-- DIV PARA LA IMPRESIÓN DE CHEQUES JOHANA 16/12/2019 -->
<div id='printDiv' style="display: none;">

</div>



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>

		$(() => {
			initSelect2();
			eventformatNumber();
			initCalendar();

		});


		const operaciones = {
			id_moneda_costo: 0,
			id_moneda_venta: 0,
			moneda: '',
			clear: function () {
				this.id_moneda_costo = 0;
				this.id_moneda_venta = 0;
				this.moneda = '';
			}
		}

		function initSelect2() {
			$('.select2').select2();
		}

		$('#alBancoCuenta').change((e) => 
		{
			resetValue();
			Promise.all([eventFiltrarBanco()]).then(() => {
				initSelect2();
			}).catch(() => {

			}).finally(() => {

			});

			operaciones.id_moneda_venta = Number($('#alBancoCuenta option:selected').attr('data-id_currency'));

			eventCampos();

		});

		// $('#importe ,#cotizacion').keyup(function () {
		// 	clearTimeout($.data(this, 'timer'));
		// 	var wait = setTimeout(eventCotizarMonto, 500);
		// 	$(this).data('timer', wait);
		// });


		$('#portador_check').click(()=>{
			console.log('NJFHDJHGJ');
	  		if($('#portador_check').prop('checked')) {
	           	$('#beneficiario').prop('disabled', true);
	        }else{
	             $('#beneficiario').prop('disabled', false);
	       }
	     })  
		function eventCampos() 
		{
			if ($('#alBancoCuenta').val() != '') 
			{
				console.log('event campos '+operaciones.id_moneda_costo);
				if (operaciones.id_moneda_costo == '111') 
				{
					$('#cotizacion').val('1');
					$('#cotizacion').prop('disabled', true);
				} 
				else
				{
					$('#cotizacion').val('0');
					$('#cotizacion').prop('disabled', false);
				}
			
				if($('#alBancoCuenta option:selected').attr('data-cuenta') != 1){
					////////////////////////////////////////////////////////////////
					return swal({
							title: "Cheque",
							text: "¿Desea generar un cheque para esta operación?",
							showCancelButton: true,
							closeOnClickOutside: false,
							closeOnConfirm: true,
							buttons: {
								cancel: {
									text: "No, Cancelar",
									value: null,
									visible: true,
									className: "btn-warning",
									closeModal: false,
								},
								confirm: {
									text: "Sí, Imprimir",
									value: true,
									visible: true,
									className: "",
									closeModal: false
								}
							}
						}).then(isConfirm => {
							if (isConfirm) {
								swal("Éxito", "El cheque se imprimirá una vez que se confirme la transacción", "success");
								$('#tipo_operacion').val(4);
								$('#divCheque').css('display','block');
							} else {
								swal("Cancelado", "", "error");

							}
						});
					////////////////////////////////////////////////////////////////
				}else{
					$('#divCheque').css('display','none');	
				}
			// } 
			// else 
			// {
			// 	$('#cotizacion').val('0');
			// 	// $('#cotizacion').prop('disabled', true);				
			}

		}


		function eventFiltrarBanco() 
		{
			return new Promise((resolve, reject) => {

				operaciones.id_moneda_costo = Number($('#alBancoCuenta option:selected').attr('data-id_currency'));
				operaciones.moneda = $('#alBancoCuenta option:selected').attr('data-currency_code');
				$('#txtMoneda').html(operaciones.moneda);
				let optionAlBanco = $('#alBancoCuenta').val();

			});
		}

		function resetValue() {
			$('#importeMe').val('0');
			$('#importe').val('0');
			$('#divCheque').css('display','none');	
			operaciones.clear();
		}

		function eventformatNumber() {

			$('.format-number').inputmask("numeric", {
			    radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				 // prefix: '$', //No Space, this will truncate the first character
				rightAlign: false,
				oncleared: function () { self.Value(''); }
			});

		}



		function initCalendar() {

			$('.calendar').datepicker({
				format: "dd/mm/yyyy",
				language: "es"
			});


			$('.click_celandar').click(() => {
				$('.calendar').datepicker('show');
			});
		}


		/*{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
				NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}*/
		function clean_num(n, bd = false) {

			if (n && bd == false) {
				n = n.replace(/[,.]/g, function (m) {
					if (m === '.') {
						return '';
					}
					if (m === ',') {
						return '.';
					}
				});
				return Number(n);
			}
			if (bd) {
				return Number(n);
			}
			return 0;

		} //


		
				/* === === === === === === === === === === === === === === === === === === === === === =
							VALIDACION DE FORMULARIO
				=== === === === === === === === === === === === === === === === === === === === === = */
		function accionFormulario() {

			var ok = 0;
			//id del formulario a validar
			var req = ['alBancoCuenta',  'importe','fecha_emision','image'];

			

			// console.log('=========REQUERIDOS===========');
			for (var j = 0; j < req.length; j++) {

				var id = req[j];
				// console.log(id);

				var name = $('#' + id).get(0).tagName;
				// console.log(name);


				if (name == 'INPUT' || name == 'TEXTAREA') {
					var input = $("#" + id).val();

					//REGLA PERSONALIZADA
					// if(id == 'cotizacion'){
					// 	if(operaciones.id_moneda_venta == operaciones.id_moneda_venta){
					// 		continue;
					// 	}
					// }

					if (input == '' | clean_num(input) <= 0) {

						$("#" + id).css('border-color', 'red');
						$("#input" + id).addClass('inputError');
						ok++;
					} else {
						$("#" + id).css('border-color', '#d2d6de');
						$("#input" + id).removeClass('inputError');
					} //else


				} //if

				if (name == 'SELECT') {

					var select = $("#" + id).val();
					// console.log(select == '');
					if (select == '') {

						$("#input" + id).find('.select2-container--default .select2-selection--single').css('border-color', 'red');
						$("#input" + id).addClass('inputError');
						ok++;
					} else {
						$("#input" + id).removeClass('inputError');
						$("#input" + id).find('.select2-container--default .select2-selection--single').css('border', '1px solid #aaa');
					} //else


				} //if   

			} //for

			if (ok > 0) {
				return false;
			} else {
				return true;
			}

		}


		function confirmarMovimiento(){

			return new Promise((resolve, reject) => { 

				let form = $('#formMovimientoBancario').serializeJSON({ customTypes: customTypesSerializeJSON });
					form['moneda_costo'] = operaciones.id_moneda_costo;
					form['moneda_venta'] = operaciones.id_moneda_venta;

			if(accionFormulario()){
				console.log(form);
					$.ajax({
						async: false,
						type: "GET",
						url: "{{route('generarMovimientoBancario')}}",
						data: form,
						dataType: 'json',
						error: function (jqXHR, textStatus, errorThrown) {
							reject('Ocurrió un error en la comunicación con el servidor.');
						},
						success: function (rsp) {
							console.log(rsp);
							if(rsp.err == true){
								respuestas = rsp.msj.split(' ');
								resolve('Movimiento N°: '+respuestas[1]+'_'+rsp.chequeId);

							}else {
								reject(rsp.msj);
							}

						} //success
					})

				} else {
					reject('El formulario se encuentra incompleto.');
				}

			});
		}

		function redirectReporteTrasferencia(){
		// location.href ="{{ route('reporteTransferencia') }}";
	 }


		function modalConfirmarMovimiento() {


				return swal({
					title: "Movimiento Bancario",
					text: "¿Está seguro que desea realizar la operación?",
					showCancelButton: true,
					buttons: {
						cancel: {
							text: "No, Cancelar",
							value: null,
							visible: true,
							className: "btn-warning",
							closeModal: false,
						},
						confirm: {
							text: "Sí, Confirmar",
							value: true,
							visible: true,
							className: "",
							closeModal: false
						}
					}
				}).then(isConfirm => {


					if (isConfirm) {
						$.when(confirmarMovimiento()).then((a)=>{ 
							console.log(a);
							respuesta = a.split('_');

							if(respuesta[1]){
								imprimirCheque(respuesta[1]);
								setTimeout(function () { 
														cerrar();
								}, 4500);
							}							

							//redirectReporteTrasferencia();
						},(b)=>{
							swal("Cancelado", b, "error");
						});
						

					} else {
						swal("Cancelado", "", "error");

					}
				});
		}


		function imprimirCheque(id) 
		{
		  $.ajax({
		  	type: "GET",
		  	url: "{{route('imprimirChequeTransaccion')}}",
		  	dataType: 'json',
		  	data: 
		  	{
		  		id: id,
		  	},
		  	success: function(rsp)
		  	{
		  		// SE GENERA EL CHEQUE DINÁMICAMENTE
				cheque = '';
				cheque = `
				        <table style="width: 900px; margin-top: 20px; margin-left: 55px">
		    <!-- <tr style="height: 30px"></tr> -->
		                    <tr>
		                        <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;"></td>
		                        <td style="width: 400px"></td>
		                        <td style="width: 500px"></td>
		                        <td style="width: 180px"></td>
		                        <td style="width: 180px; font-family: Arial; font-weight: bold; font-size: 18px; padding-bottom: 15px">****${rsp.data.importe}.#</td>
		                    </tr>

		                    <tr>
		                        <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;">${rsp.data.fecha}</td>
		                        <td style="width: 400px"></td>
		                        <td style="width: 10px"></td>
		                        <td style="width: 180px"></td>
		                        <td style="width: 180px"></td>
		                    </tr>

		                    <tr>

		                        <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold; ">${rsp.data.proveedor}</td>
		                        <td style="width: 400px"></td>
		                        <!-- <td style="width: 10px"></td>
		                        <td style="width: 180px; text-align: center; font-family: Arial; font-weight: bold; font-size: 18px; text-align: left;">12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;19</td>
		                         -->
		                        <td colspan="2" style="width: 180px; text-align: center; font-family: Arial; font-weight: bold; font-size: 18px; text-align: center; padding-left: 195px;">${rsp.data.dia}&nbsp;&nbsp;&nbsp;&nbsp;${rsp.data.mes}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${rsp.data.anho}</td>
		                        <td style="width: 180px"></td>
		                    </tr>

		                    <tr>
		                        <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;  padding-left: 10px;"></td>
		                        <!-- <td style="width: 400px"></td> -->
		                        <td colspan="4" style="width: 500px; font-family: Arial; font-weight: bold; font-size: 18px; padding-left: 160px; height: 30px; padding-top: 10px">${rsp.data.proveedor}</td>
		                       
		                    </tr>

		                    <tr>
		                         <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;  padding-left: 10px;">${rsp.data.concepto}</td>
		                        <!-- <td style="width: 200px; font-size: 12px; font-family: Arial; font-weight: bold;"></td> -->
		                        <!-- <td style="width: 400px"></td> -->
		                        <td colspan="4" style="width: 500px; font-family: Arial; font-weight: bold; font-size: 18px; padding-left: 160px; padding-bottom: 50px; padding-top: 5px">${rsp.data.importeLetras} ----------</td>
		                       
		                    </tr>

		                </table>       `;
				      
				$('#printDiv').html("");  
				$('#printDiv').html(cheque);

				var divToPrint = document.getElementById('printDiv');
				var newWin = window.open('', 'Print-Window');
				  newWin.document.open();
				  newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
				  newWin.document.close();
				  setTimeout(function() {
				    newWin.close();
				  }, 10);
		  		
		  	}	
		  });

		}
		var options = { 
		                beforeSubmit:  showRequest,
		                success: showResponse,
					    dataType: 'json' 
		            }; 
		$('body').delegate('#image','change', function(){
		    $('#upload').ajaxForm(options).submit();      
		});

		function cerrar(){
			console.log('jvbvjvkh');
            location.href ="{{ route('reporteTransferencia') }}";
		}

		function showRequest(formData, jqForm, options) { 
			    $("#validation-errors").hide().empty();
			    return true; 
			} 
			
			function showResponse(response, statusText, xhr, $form)  { 
			            if(response.success == false)
			              {
			                var arr = response.errors;
			                console
			                $.each(arr, function(index, value)
			                {
			                  if (value.length != 0)
			                  {
			                    $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
			                  }
			                });
			                $("#validation-errors").show();
			            } else {
			                 $("#imagen").val(response.archivo); 
			                 var envio = response.id+", '"+response.archivo+"'";
			                 if(response.rsp == false){
			                 	$("#output").append('<div id= "'+response.id+'"><div class="col-sm-2" id= "'+response.id+'"><a href="'+response.file+'" target="_blank"><img class="img-responsive" style="width:120px; height:120px;" src="'+response.file+'" alt="Photo"></a></div><div class="col-md-12 col-sm-1 col-md-1"><button type="button" id="btn-imagen" onclick="eliminarImagen('+envio+')"><i class="ft-x-circle" style="font-size: 18px;"></i></button></div></div>');
			                 }else{
			                      $("#output").append('<div id= "'+response.id+'"><div class="col-sm-2"><a href="'+response.file+'" target="_blank"><img class="img-responsive" style="width:120px;" src="{{asset('images/file.png')}}" alt="Photo"></a></div><div class="col-md-12 col-sm-1 col-md-1"><button type="button" id="btn-imagen" data-id="'+response.archivo+'" onclick="eliminarImagen('+envio+')"><i class="ft-x-circle" style="font-size: 18px;"></i></button></div></div>');
			                 }     
			                 $("#btnAceptarCanje").css('display','block');
			            }
			       }

			      function eliminarImagen(id, archivo){
			       // $("#btn-imagen").on("click", function(e){
			          $.ajax({
			              type: "GET",
			              url: "{{route('fileAdjuntoEliminarDocumento')}}",//fileDelDTPlus
			              dataType: 'json',
			              data: {
									dataId: id,
									dataFile:archivo
					       			},
			              success: function(rsp){
			                $("#imagen").val("");
			                $("#"+id).remove();
			              }
			       //   })      

			        });
			      }  
				


	</script>
	@endsection