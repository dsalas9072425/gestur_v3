@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Ajuste Fecha Cuenta</h4>
			<br>
			<p>
			* <b>AJUSTAR EL SALDO POR FECHA GENERA UN RECALCULO DE CADA MOVIMIENTO A PARTIR DE LA FECHA Y EL SALDO DEFINIDO</b> <br>
			* <b>ELIMINAR LOS MOVIMIENTOS MANUALES ES UNA OPERACIÓN INREVERSIBLE</b><br>
			* <b>LOS AJUSTES DE SALDO FECHA CUENTA ELIMINA OTROS AJUSTES DE SALDO FECHA CUENTA QUE SE ENCUENTREN EN EL RANGO DEL RECALCULO</b><br>
		</p>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
				    <form  method="post"  action="{{route('saldo_fecha_cuenta.form')}}">
						<div class="row">


							<div class="col-md-12">
									<div class="form-group">
										<label>Saldo</label>					            	
										<input type="number" step="0.01" name="saldo" id="saldo" class="form-control" required>
									</div>
								</div>

								<div class="col-xs-12  col-sm-3 col-md-4">
									<div class="form-group">
										<label>Fecha</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" class="form-control fecha" name="fecha" id="fecha" required />
										</div>
									</div>
								</div>
						


								<div class="col-12 col-sm-3 col-md-4">
									<div class="form-group">
										<label>Cuenta de Fondo</label>
										<select class="form-control select2" name="id_cuenta_detalle" id="id_cuenta_detalle">
											@foreach($cuentas as $cuenta)
												<option value="{{$cuenta->id}}">{{$cuenta->banco_n}} {{$cuenta->numero_cuenta}} {{$cuenta->currency_code}} {{$cuenta->denominacion}}</option>
											@endforeach
			
										</select>
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-4">
									<div class="form-group">
										<label>Eliminar movimientos manuales</label>
										<select class="form-control select2" name="movimiento_manuales" id="movimiento_manuales">
											<option selected value="NO">NO</option>
											<option value="SI">SI</option>
										</select>
									</div>
								</div>

						    		<div class="col-md-12">
										<br>
										<button type="submit" class="btn btn-success btn-lg" style="margin-top: 10px;">RECALCULAR</button>
									</div>
								
						</div> 
        
            	</form>	
            </div>	
        </div>    
    </div>   
</section>



    <!-- /.content -->

    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')	
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script>
		$(document).ready(function() {
			$('.select2').select2();

			$( ".fecha" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd',
									position: { my: "left bottom", at: "left bottom" },
									setDate: new Date(),
									required:true
									});

		});

		
	</script>
	

@endsection
