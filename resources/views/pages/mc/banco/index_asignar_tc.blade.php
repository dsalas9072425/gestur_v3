
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')



<section id="base-style">
	@include('flash::message')
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Asignar Tarjetas</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">

			<div class="card-body">


				<form class="row" id="formTab_1" action="{{route('asignar_tc.save')}}" method="POST">

					<div class="col-12">
						<div class="alert alert-secondary" role="alert">
							<ul>
								<li>Para quitar todas las tarjetas de un usuario seleccione una persona y no seleccione ninguna tarjeta y haga clic en Asignar</li>
							</ul>
						  </div>
					</div>

					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Tarjetas</label>
							<select class="form-control select2_tc" name="id_tarjetas[]" multiple id="id_tarjetas">
								@foreach ($tarjetas as $tarjeta)
									<option value="{{$tarjeta->id_tarjeta}}">{{$tarjeta->banco.' - '.$tarjeta->nro_tarjeta}}</option>
								@endforeach
							</select>
						</div>
                    </div>

					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Persona</label>
							<select class="form-control select2" name="id_persona" id="id_persona" required>
								<option value="">Seleccionar Persona</option>
								@foreach ($personas as $persona)
									<option value="{{$persona->id}}">{{$persona->id}} - {{$persona->getFullNameAttribute()}}</option>
								@endforeach
							</select>
						</div>
                    </div>



						<div class="col-12 mb-1">
							<button type="submit" class="btn btn-info pull-left btn-lg mr-1"><b>Asignar</b></button> <br>
							
						</div>
					


				</form>

			
            	<div class="table-responsive table-bordered">
		            <table id="listado_1" class="table" style="width: 100%;">
						<thead>
							<tr style="text-align: center;">
								<th>ID</th>
								<th>Nombre</th>
								<th>Tipo Persona</th>
                                <th>Tarjetas</th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
							@foreach ($personas as $persona)
								<tr>
									<td>{{$persona->id}}</td>
									<td>{{$persona->getFullNameAttribute()}}</td>
									<td>{{$persona->tipo_persona->denominacion}}</td>
									<td>
										@if(count($persona->tarjetas))
											@foreach ($persona->tarjetas as $tarjeta)
												<span class="badge badge-secondary">{{$tarjeta->banco_detalle->banco_cabecera->nombre.'-'.$tarjeta->banco_detalle->numero_cuenta . '-'.$tarjeta->banco_detalle->currency->currency_code}}</span>
											@endforeach
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>


			</div>
		

	





		</div>
	</div>
</section>



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>

		$('.select2').select2();
		$('.select2_tc').select2({
			placeholder: 'Seleccionar Tarjeta',
			allowClear: true
		});
		
		$('#listado_1').DataTable();





	</script>
@endsection