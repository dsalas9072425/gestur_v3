@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
	border-radius: 14px !important;
	}
	</style>
	
@endsection
@section('content')

<section id="base-style">
@include('flash::message') 
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Agregar Promoción</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmPromocion" method="post" action="{{route('doAddPromocion')}}">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-6" style="padding-left: 20px;">
							<label class="control-label">Descripción</label>
							<input type="text" name="descripcion" class="Requerido form-control" value=""
								id="descripcion" required="required" />
						</div>

						<div class="col-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Fecha Inicio Promoción</label>
								<div class="input-group">
									<div class="input-group-prepend" style="">
										<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right single-picker" name="fecha_inicio"
										tabindex="5" data-value-type="s_date" id="fecha_inicio" maxlength="10"  value="" required="required">
								</div>
							</div>
                   		 </div>
							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Fecha Vencimiento Promoción</label>
									<div class="input-group">
										<div class="input-group-prepend" style="">
											<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
										</div>
										<input type="text" class="form-control pull-right single-picker" name="fecha_vencimiento"
											tabindex="5" data-value-type="s_date" id="fecha_vencimiento" maxlength="10"  value="" required="required">
									</div>
								</div>
                   		 </div>
						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 20px;">
							<label class="control-label">Porcentaje Comisión</label>
							<input type="text" name="porcentaje_comision" class="Requerido form-control numeric" value=""
								id="porcentaje_comision" required="required" />
						</div>
						<div class="col-12 col-sm-9 col-md-9">
							<a href="{{ route('reportePromocion') }}" type="button" class="btn btn-danger btn-lg pull-right mr-1">Volver</a>
							<button type="submit" class="btn btn-success btn-lg pull-right mr-1" name="guardar" value="Guardar">Guardar</button>						</div>
						</div>

					</div>
				</form>

			</div>
		</div>
	</section>
	</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script>

    	$(document).ready(function() {
	        $('#fecha_inicio').datepicker({
				format: "dd/mm/yyyy",
				language: "es",
				orientation: "bottom"
			});

	        $('#fecha_vencimiento').datepicker({
				format: "dd/mm/yyyy",
				language: "es",
				orientation: "bottom"
			});

			$('.numeric').inputmask("numeric", {
				radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				// prefix: '$', //No Space, this will truncate the first character
				rightAlign: false,
				oncleared: function () { 
					$(this).val(0);
				}
			});

		});
		
	</script>
@endsection