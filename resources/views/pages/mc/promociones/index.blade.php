
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
	</style>
@endsection
@section('content')

<section id="base-style">
@include('flash::message') 
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Promoción</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmPromocion" method="post">
					<div class="row">
						<div class="col-12">
							<a href="{{ route('agregarPromocion') }}" class="btn btn-success pull-right" role="button">
								<div class="fonticon-wrap style-icon">
									<i class="ft-plus-circle"></i>
								</div>
							</a>
						</div>
						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label class="control-label">Descripción</label>
								<input type="text" name="descripcion" class="Requerido form-control" value=""
									id="descripcion" required="required" />
							</div>
						</div>

						<!--<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Periodo</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i
												class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="periodo" id="periodo"
										required>
								</div>
							</div>
						</div>-->

						<div class="col-11 mb-1">
							<button type="button" onclick="buscarPromocion()" id="btnGuardar" class="btn btn-info btn-lg pull-right mb-1">Buscar</button>
						</div>
					</div>
				</form>


				<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr style="text-align: center;">
								<th>Fecha</th>
								<th>Descripción</th>
								<th>Fecha Inicio</th>
								<th>Fecha Vencimiento</th>
								<th>% Comisión</th>
								<th>Activo</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="lista_promocion" style="text-align: center">
						</tbody>
					</table>
				</div>




			</div>
		</div>
	</section>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
		//$(document).ready(function() {

			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

			var inicioPeriodo, finPeriodo = 'vacio';

			var Fecha = new Date();
			var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

			//Activar calendario
			$( "#periodo" ).daterangepicker({ 
								   timePicker24Hour: true,
							        timePickerIncrement: 30,
							       locale: {
											format: 'DD/MM/YYYY',
											cancelLabel: 'Limpiar'
													    },
									startDate: "01/"+fechaInicial,
        							endDate: new Date()
									});

			$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

			var a = moment().subtract(1, 'day');
			$("#periodo").data('daterangepicker').setStartDate(a);

		

		//Tomas las fechas seleccionadas
		$('#vencimiento').on('apply.daterangepicker', function(ev, picker) {
		  inicioPeriodo = picker.startDate.format('MM/DD/YYYY');
		  finPeriodo = picker.endDate.format('MM/DD/YYYY');

		});

		
		function buscarPromocion() {

			var dataString = $('#frmPromocion').serialize();

			$.ajax({
						type: "GET",
						url: "{{route('consultaPromocion')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
									heading: 'Error',
									text: 'Ocurrio un error en la comunicación con el servidor.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
                                });

                            },
						success: function(rsp){
							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}
							console.log(rsp);

							$.each(rsp, function (key, item){
								  console.log(item);
								  fechaMostrar = '';
								  fechaInicio = ''; 
								  fechaVencimiento = ''; 
								if(jQuery.isEmptyObject(item.fecha) != true){
									var fecha = item.fecha;
									var fechaIntermedia = fecha.split(' ');
									var fechaFinal = fechaIntermedia[0].split('-');
									fechaMostrar = fechaFinal[2]+'/'+fechaFinal[1]+'/'+fechaFinal[0]+' '+fechaIntermedia[1];
								}
								if(jQuery.isEmptyObject(item.fecha_alta) != true){
									var fechaInicio = item.fecha_alta;
									var fechaIntermedia = fechaInicio.split(' ');
									var fechaFinalInicio = fechaIntermedia[0].split('-');
									fechaInicio = fechaFinalInicio[2]+'/'+fechaFinalInicio[1]+'/'+fechaFinalInicio[0];
								}
								if(jQuery.isEmptyObject(item.fecha_baja) != true){
									var fechaVencimiento = item.fecha_baja;
									var fechaIntermedia = fechaVencimiento.split(' ');
									var fechaFinalVencimiento = fechaIntermedia[0].split('-');
									fechaVencimiento = fechaFinalVencimiento[2]+'/'+fechaFinalVencimiento[1]+'/'+fechaFinalVencimiento[0];
								}

								if(item.activo == ''){
									activo = 'NO';
								}else{
									activo = 'SI';
								}

								var boton = "<a href='editarPromocion/"+item.id+"'class='btn btn-info' style='padding-left: 6px;padding-right: 6px;' role='button'><i class='fa fa-fw fa-edit'></i></a>";										


								var dataTableRow = [
														fechaMostrar,
														item.descripcion,
														fechaInicio,
														fechaVencimiento,
														item.porcentaje_comision,
														activo,
														boton
													];
								var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

							});

					


						}//cierreFunc	
				});	
			
		}
				
			

	</script>
@endsection