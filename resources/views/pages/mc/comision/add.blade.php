
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
	</style>
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Mis Recomendados</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0 mb-1">
				<div class="card-body pt-0">
                    <button title="Agregar Cliente"
                        class="btn btn-success pull-right" type="button" id="btnAddData_modal">
                        <div class="fonticon-wrap">
                            <i class="ft-plus-circle"></i>
                        </div>
                    </button>
                </div>

				<form class="row" id="formTab_1">

				

					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Activo</label>
							<select class="form-control select2" name="activo" id="activoSearch" style="width:100%;">
                                <option value="">Todos</option>
                                <option value="true">SI</option>
                                <option value="false">NO</option>

							</select>
						</div>
					</div>
					
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Email</label>
							<input type="text" class="form-control" name="correo" id="email">
						</div>
                    </div>



				</form>

				<div class="row">
					<div class="col-12 mb-1">
						<button type="button" onclick="limpiarData()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
						<button type="button" class="btn btn-info pull-right btn-lg mr-1" onclick="consultaListado()"><b>Buscar</b></button>
					</div>
				</div>
				
				<div class="table-responsive table-bordered">
		            <table id="listado_1" class="table" style="width: 100%;">
						<thead>
							<tr style="text-align: center;">
                                <th>Email</th>
                                <th>Documento</th>
								<th>Fecha</th>
								<th>Nombre/Apellido</th>
								<th>Estado</th>
								<th></th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
						</tbody>
					</table>
				</div>




			</div>
		</div>
	</section>
</section>



<div class="modal fade" id="altaCliente" tabindex="-1" role="dialog" aria-labelledby="altaCliente" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="modalTitulo">ALTA CLIENTE</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
			<form id="frmData" autocomplete="off">
				<div class="row justify-content-md-center">
					<input type="hidden" class="form-control modalData" name="id" id="id">
					<div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Email</label>
								<input type="text" class="form-control modalData" name="correo" id="correo">
						</div>
					</div>

					<div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Documento</label>
								<input type="text" class="form-control modalData" name="documento" id="documento">
						</div>
					</div>

					<div class="col-12">
						<div class="form-group">
							<label>Nombre y Apellido</label>
								<input type="text" class="form-control modalData" name="nombre_apellido" id="nombre_apellido">
						</div>
					</div>
					<div class="col-12">
						<div class="form-group">
							<label>Teléfono Celular</label>
								<input type="text" class="form-control modalData" name="telefono" id="telefono">
						</div>
					</div>

				</div>
			</form>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
		  <button type="button" class="btn btn-info" style="order-color: #11a578 !important; background-color: #16d39a !important;" id="btnGuardar">Guardar</button>
		  <button type="button" class="btn btn-info" onclick="doEdit()" style="display: none; order-color: #11a578 !important; background-color: #16d39a !important;" id="btnEditar">Guardar</button>
		</div>

	  </div>
	</div>
</div>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>


		$(document).ready(()=>{
			consultaListado();
			$('.select2').select2();
			$("#activoSearch").val('true').select2();
		});


	//EVENTO CIERRE MODAL
	$('#altaCliente').on('hidden.bs.modal', function (e) {
        configCamposModal(true);
        // $('#btnGuardarData').hide();
    });

    $('#btnAddData_modal').click(()=>{
        crearInfoCuenta();
        $('.modalData').val('');
        $('.modalData').prop('disabled',false);
    });

	$('#btnGuardar').click(()=>{
		guardarDato();
	});

	
    function limpiarData(){
			$('#activoSearch').val('').trigger('change.select2');
			$('#email').val('');
		}

				

    function guardarDato() 
    {	
		
									
		let correo = $('#correo').val();
		let documento = $('#nombre_apellido').val();
		let telefono = $('#telefono').val();
		if(correo != '' & documento != '' & telefono != ''){
			var dataString = $('#frmData').serialize();

			$.ajax({
						type: "GET",
						url: "{{route('ajaxAltaClienteComision')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },
						success: function(rsp)
						{	
							if(rsp.count > 0){

								$.toast({
									heading: 'Atención',
									text: 'Este cliente ya fue asociado a otro vendedor.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'warning'
                                });
							} else {

								if(rsp.save == 'true'){
									$.toast({
										heading: 'Atención',
										text: 'Ocurrio un error al intentar guardar los datos.',
										position: 'top-right',
										showHideTransition: 'fade',
										icon: 'warning'
                                	});
								} else {
									$.toast({
									heading: 'Exito',
									text: 'Los datos fueron almacenados.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'success'
									});
									$('#correo').val('');
									$('#documento').val('');
									location.reload(true);
								}
							
							}
							

						}//cierreFunc	
					});	
			
		} else {
								$.toast({
										heading: 'Atención',
										text: 'Complete los campos por favor.',
										position: 'top-right',
										showHideTransition: 'fade',
										icon: 'warning'
                                	});	
		}
			
	}




	function consultaListado()
    {

							$.blockUI({
											centerY: 0,
											message: `<div class="loadingC"><img src="{{asset('images/loading.gif')}}" alt="Procesando.." style="width:10%"/><br><h2>Procesando..</h2></div>`,
											css: {
												color: '#000'
											}
										});

			// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
			setTimeout(function (){
						
	 		table =   $('#listado_1').DataTable({
					"destroy": true,
					"ajax": {
							data: $('#formTab_1').serializeJSON(),
							url: "{{route('getAjaxClienteComision')}}",
							type: "GET",
					error: function(jqXHR,textStatus,errorThrown){

				   $.toast({
					heading: 'Error',
					text: 'Ocurrio un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});
					$.unblockUI();
				}
				  },
				 "columns":[
								{data:'e_mail_cliente'},
                                {data:'documento_cliente'},
								{data:'fecha_alta_format'},
								{data: (x)=>{ 
									return x.nombre_apellido; 
									}},
								{data: (x)=>{ 
									if(x.activo == true){
										var status = 'Activo';
									}else{
										var status = 'Inactivo';
									}
									return status; 
									}},
								{data: (x)=>{ 	
									btn = '<button class="btn btn-info" style="margin-right: 5px;" onclick="editCliente('+x.id+')" type="button"><i class="fa fa-fw fa-edit"></i></button><button class="btn btn-info" onclick="anularCliente('+x.id+')" type="button"><i class="fa fa-fw fa-times"></i></button>';
									return btn;
									}}
                              
							]
				
				}).on('xhr.dt', function ( e, settings, json, xhr ){
					 $.unblockUI();
				});
				
	 
					}, 300);

    }//function
				



    function crearInfoCuenta()
    {   
        // $('#btnGuardarData').show();
        configCamposModal(false);
        showModalData(true);
    }	

	    function configCamposModal(opt)
    {
        if(opt){
            $('.modalData').val('').trigger('change.select2');
            $('.modalData').val('');
            $('.modalData').prop('disabled',true); //DESACTIVAR CAMPOS
        } else {
            $('.modalData').val('').trigger('change.select2');
            $('.modalData').val('');
            $('.modalData').prop('disabled',false); //ACTIVAR CAMPOS
        }
   
    }		
 	function showModalData(opt)
    {   
        if(opt){
        $('#altaCliente').modal('show');
        }

        $('#altaCliente').modal('hide');
    }
	

    function editCliente(id)
    {   
		
		$.ajax({
				type: "GET",
				url: "{{route('editCliente')}}",
				dataType: 'json',
				data: {
						dataCliente: id,
					  },
				success: function(rsp){
								$('#btnGuardar').hide();
								$('#btnEditar').show();
								$('#altaCliente').modal('show');
								$('#id').val(rsp.id);
								$('#correo').val(rsp.e_mail_cliente);
								$('#correo').prop('disabled',true);
								$('#documento').val(rsp.documento_cliente);
								$('#nombre_apellido').val(rsp.nombre_apellido);
								$('#telefono').val(rsp.celular);
							}
				});	

    }


  function doEdit() 
    {	
		
		let correo = $('#correo').val();
		let documento = $('#documento').val();
		let telefono = $('#telefono').val();
		if(correo != '' & documento != ''& telefono != ''){
			var dataString = $('#frmData').serialize();

			$.ajax({
						type: "GET",
						url: "{{route('ajaxEditClienteComision')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },
						success: function(rsp)
						{	
							if(rsp.count > 0){

								$.toast({
									heading: 'Atención',
									text: 'Este cliente ya fue asociado a otro vendedor.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'warning'
                                });
							} else {

								if(rsp.save == 'true'){
									$.toast({
										heading: 'Atención',
										text: 'Ocurrio un error al intentar guardar los datos.',
										position: 'top-right',
										showHideTransition: 'fade',
										icon: 'warning'
                                	});
								} else {
									$.toast({
									heading: 'Exito',
									text: 'Los datos fueron almacenados.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'success'
									});
									$('#correo').val('');
									$('#documento').val('');
									location.reload(true);
								}
							
							}
							

						}//cierreFunc	
					});	
			
		} else {
								$.toast({
										heading: 'Atención',
										text: 'Complete los campos por favor.',
										position: 'top-right',
										showHideTransition: 'fade',
										icon: 'warning'
                                	});	
		}
			
	}





			
 	function anularCliente(id)
    {   
     	return swal({
                        title: "GESTUR",
                        text: "¿Está seguro de que desea eliminar su recomendado?",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, eliminar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
										$.ajax({
											type: "GET",
											url: "{{route('anularCliente')}}",
											dataType: 'json',
											data: {
													dataCliente: id,
												  },
											success: function(rsp){
															if(rsp.save == true){
																 swal("Exito", "", "success");
																 location.reload(true);
															}else{
																swal("Cancelado", "", "error");
															}
														}
											});	

                                    
                                } else {
                                     swal("Cancelado", "", "error");
                                }
            	});
    }






	</script>
@endsection