@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Nueva Sucursal Empresa</h1>
            </div> 
            <br>
            <!-- /.box-header -->
            <div class="box-body">
                @include('flash::message')     
			    <form id="frmPaises" action="{{route('doAddTipoPersona')}}" method="get" autocomplete="nope"> 
			        {{ csrf_field() }}
					<div class="row">
						<div class="col-md-1">
					    </div>
					    <div class="col-md-5">
							<label class="control-label">Denominación</label>
					        <input type="text" required class = "form-control" name="denominacion" id="denominacion" placeholder="Denominación" value=""/>
					    </div>
					    <div class="col-md-5">
							<label class="control-label">RUC</label>
					        <input type="text" required class = "form-control" name="ruc" id="ruc" placeholder="RUC" value=""/>
						</div>
						<div class="col-md-1">
					    </div>
					</div>
					<br>	
					<div class="row">
						<div class="col-md-1">
					    </div>
					    <div class="col-md-5">
							<label class="control-label">Dirección</label>
					        <input type="text" required class = "form-control" name="direccion" id="direccion" placeholder="Denominación" value=""/>
					    </div>
					    <div class="col-md-5">
							<label class="control-label">Encargado</label>
							<select name="id_encargado" required class = "form-control" id="id_encargado">
									<option value="0">Seleccione un Encargado</option>
								@foreach($personas as $key=>$persona)
									<option value="{{$persona['id']}}">{{$persona['nombre_apellido']}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-1">
					    </div>
					</div>	
					<br>
					<div class="row">
						<div class="col-md-1">
					    </div>
					    <div class="col-md-5">
							<label class="control-label">Telefono</label>
					        <input type="text" required class = "form-control" name="telefono" id="telefono" placeholder="Telefono" value=""/>
					    </div>
					    <div class="col-md-5">
							<label class="control-label">Empresa</label>
							<select name="id_empresa" required class = "form-control" id="id_empresa">
								<option value="0">Seleccione una Empresa</option>
								@foreach($empresas as $key=>$empresa)
									<option value="{{$empresa['id']}}">{{$empresa['denominacion']}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-1">
					    </div>
					</div>	
					<div class="row">
					    <div class="col-md-1">
					    </div>
					    <div class="col-md-6">
							<br>
							<button type="submit" class="btn btn-primary" style=" background: #e2076a !important;" name="guardar" value="Guardar">Guardar</button>
						</div>
					</div>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {


		});
	</script>
@endsection