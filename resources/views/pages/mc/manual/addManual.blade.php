@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')

<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Subir manual actualizado</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
                         

		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
            <form id="subirManual"  enctype="multipart/form-data">
				<div class="form-group">
					<label for="manual">Adjuntar archivo</label>
					<input type="file" class="form-control-file" id="pdf" name="pdf">
				</div>
				<button type="submit" class="pull-right btn btn-success mb-5">Guardar</button>
            </form>
			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script>
		$(document).ready(function() {
			$('#subirManual').submit(function(event) {
				event.preventDefault(); // Evita que se envíe el formulario normalmente
		

					let formulario = $(this);
					let datos = new FormData(formulario[0]);

					$.ajax({
					url: "{{route('subirManual')}}",
					type: 'POST',
					data: datos,
					processData: false,
					contentType: false,
					success: function(data) {
						if(data.estado == 1){
							$.toast({
							heading: 'Exito',
							text: 'Ok subido con exito.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'success'
								});
								$('input[name=pdf').val('');
						}else if(data.estado == 2){
							$.toast({
							heading: 'Error',
							text: 'El archivo debe ser en formato PDF.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});
						}else{
							$.toast({
							heading: 'Error',
							text: 'Ocurrio un problema recibiendo el archivo.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});
						}
	
					},
					error: function(jqXHR, textStatus, errorThrown) {
						$.toast({
							heading: 'Error',
							text: 'Ocurrio un error en la comunicación con el servidor.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});
					}
					});
      	  });
		});
	</script>
@endsection