@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')

<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Ver actualizacion de manual</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
                         
				<a href="{{ url('addManual')}}" title="Agregar Menu" class="btn btn-success pull-right mt-2"
					role="button" id="addTicketTipo">
					<div class="fonticon-wrap">
						<i class="ft-plus-circle"></i>
					</div>
				</a>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<div class="table-responsive">
					<table id="listadoManual" class="table text-center" style="width: 100%;">
						<thead style="text-align: center">
							<th>Fecha</th>
							<th>Usuario</th>
							<th>Archivos</th>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
			$(document).ready(function() {
				verListadoManual();

      	  });
        function verListadoManual(){
			
            $("#listadoManual").dataTable({
				"paging":true,
				"searching": true,
				"processing": true,
				"destroy": true,
				"ajax": {
				"url": "{{route('verDatosManualesSubidos')}}",
				}
				});	
		}
	</script>
@endsection