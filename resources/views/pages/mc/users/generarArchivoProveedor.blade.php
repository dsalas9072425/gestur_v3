@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	<div>

            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
            	<form id="frmAgencias" class="contact-form" action="" method="post">
					<div class="booking-information travelo-box">
						<div>
							<h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Proveedores</h1>
							<br>
							<!--<div class="row">
								<div class="form-group col-xs-12 col-sm-6">
									<a href="{{route('mc.usuariosAdd')}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px;" role="button">Nuevo Proveedor</a>
								</div>
							</div>-->
							<br>
							<div id="resultado"></div>
							<div style= "background-color: #ffffff">
								<br>
	            				<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
		                            <thead>
		                            	<tr>
		                                	<th>Id</th>
		                                  	<th>Descripción</th>
		                                   	<th>Activo</th>
	                              		 </tr>
		                            </thead>
		                            <tbody>
		                             @foreach($proveedors as $proveedor)
			                                <tr>
						                      	<td><b>{{$proveedor['infoprovider_id']}}</b></td>
			                                   	<td><b>{{$proveedor['infoprovider_name']}}</b></td>
			                                   	<td>
													@if($proveedor['habilitado'] == true)
														<input id="check_{{$proveedor['infoprovider_id']}}" onclick='cancelar({{$proveedor['infoprovider_id']}})' checked="checked" type="checkbox">
													@else
														<input id="check_{{$proveedor['infoprovider_id']}}" onclick='cancelar({{$proveedor['infoprovider_id']}})' type="checkbox">
													@endif	
													</td>
			                                </tr>	
		                                @endforeach   	
		                            </tbody>
	                       		 </table>
	                       		 <br>
								<div class="row">
									<div class="col-sm-10">
									</div>
									<div class="col-sm-1">
										  <button type="button" style="width: 220px;  background-color: #e2076a;" id= "refresh" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="	glyphicon glyphicon-refresh"></i> Actualizar Proveedores</button>
									</div>
								</div>
								<br>
	                       	</div>	 
                        </div>
                    </div>    			
            </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/mc/scripts')
	<script>
				$(document).ready(function() {
					$("#listado").dataTable({
						 "aaSorting":[[0,"desc"]]
						});

				});
				function cancelar(idProveedor){
					    console.log($( "#check_"+idProveedor ).val());
				        BootstrapDialog.confirm({
				            title: 'GESTUR',
				            message: 'Seguro desea modificar el estado de este Proveedor?',
				            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
				            closable: true, // <-- Default value is false
				            draggable: true, // <-- Default value is false
				            btnCancelLabel: 'No', // <-- Default value is 'Cancel',
				            btnOKLabel: 'Si', // <-- Default value is 'OK',
				            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
				            callback: function(result) {
				                // result will be true if button was click, while it will be false if users close the dialog directly.
				                if(result) 
								{
				                    $.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });
									
									//alert("{{route('getCancelarReservas')}}?idProveedor="+idProveedor);
								$.ajax({
								type: "GET",
								url: "{{route('mc.getCancelarProveedor')}}?idProveedor="+idProveedor,
										data: idProveedor,
										success: function(rsp){
												console.log(rsp);
												$.unblockUI();
												if(rsp == 'false'){
													var mensaje = 'Se ha desactivado el Proveedor';
													$( "#check_"+idProveedor ).prop( "checked", false );
												}else{
													var mensaje = 'Se ha activado el Proveedor';
													$( "#check_"+idProveedor ).prop( "checked", true );
												}
				                                $("#buscarReservas").trigger('click');
					                                BootstrapDialog.show({
					                                    title: 'GESTURs',
					                                    message: mensaje
					                                });
					                            }

										})
				                }else {
				                	if($("#check_"+idProveedor ).val() == 'on'){
				                		$("#check_"+idProveedor ).prop( "checked", false );
				                	}else{
										$("#check_"+idProveedor ).prop( "checked", true );
				                	}
				                    return false;
				                }
				            },
				        });
				}
				$('#refresh').click(function(){
						$.ajax({
								type: "GET",
								url: "{{route('mc.getActualizacion')}}",
										success: function(rsp){
										console.log(rsp);
										if(rsp.codRetorno == 0){
												$.ajax({
														type: "GET",
														url: "{{route('mc.getGenerarArchivo')}}",
																success: function(rsps){
																		$("#resultado").html('<div class="alert alert-success" role="alert">'+rsps+'</div>');
										                			}
									                	});
											}else{
												$("#resultado").html('<div class="alert alert-danger" role="alert">'+rsp.descRetorno+'</div>');											
											}
			                            }
								})	
						})

	</script>
@endsection