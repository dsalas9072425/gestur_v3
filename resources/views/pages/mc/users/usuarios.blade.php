@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Usuarios</h1>
            </div>      
            <div class="row">
		        <div class="col-xs-12">
		            <a href="{{route('mc.usuariosAdd')}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px;margin-left: 10px;" role="button">Nuevo Usuario</a>
		        </div>
		    </div>        
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="table-responsive">
	              <table id="listado" class="table">
	                <thead>
					  <tr>
			            <th style="display: none;">ID_PORTAL</th>
						@if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia == 1)<th>BKM</th>@endif
			            <th>Email</th>
						<th>Nombre</th>
			            <th>Activo</th>
						<th>Perfil</th>
			            <th>Caducidad</th>
						<th>Agencia</th>
						<th>Sucursal</th>
			            <th class="oculto">Ver</th>
		              </tr>
	                </thead>
	                <tbody>
	 					@foreach($usuarios as $usuario)
				            <tr>
				                <td style="display: none;"><b>{{$usuario->id_usuario}}</b></td>
									@if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia == 1)<td>{{md5($usuario->email)}}</td>@endif
				                <td>{{$usuario->email}}</td>
								<td>{{$usuario->nombre_apellido}}</td>
				                <td>
				                    @if($usuario->activo =="S")
				                        <span class="label label-success">Activo</span>
				                    @else
				                        <span class="label label-danger">No Activo</span>
				                    @endif
				                </td>
							    <td><b>{{ $usuario['perfil']['des_perfil']}}</b></td>
				                <td>{{$usuario->fecha_validez}}</td>
				                <td><b>{{$usuario->agencia->razon_social}}</b></td>
				                <td>{{$usuario->sucursal->descripcion_agencia}}</td>
				                <td><a href="{{route('mc.usuariosEdit', ['id' =>$usuario->id_usuario])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; background: #e2076a !important; width: 106px;" role="button">Editar</a></td>
				            </tr>	
			            @endforeach
			        </tbody>
	              </table>
	            </div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/mc/scripts')
	<script>
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
		});
	</script>
@endsection