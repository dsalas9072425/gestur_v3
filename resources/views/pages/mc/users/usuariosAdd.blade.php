@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
        	<br>
			<br>
			<br>
			<br>
            <div id="main" class="col-sm-12 col-md-12" style="background-color: #fff;">
            	@include('flash::message')
						<h1 class="subtitle hide-medium" style="font-size: xx-large;">Nuevo Usuario</h1>
						<br>
			               <form id="frmUsuarios" action="{{route('mc.doAddUser')}}" method="get" autocomplete="nope"> 
			               		{{ csrf_field() }}
					                <div class="row">
					                    <div class="col-md-6">
											<label class="control-label">Perfil(*)</label>
											<select name="perfil_id" required class = "form-control" id="perfil_id">
												@foreach($listadoPerfil as $key=>$perfil)
													<option value="{{$perfil['value']}}">{{$perfil['label']}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-6">
					                        <label class="control-label">Agencia (*)</label>
					                            <select name="agencia_id" required class="form-control" id="agencia_id">
													@foreach($listadoAgencia as $key=>$agencia)
														<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
													@endforeach
												</select>
					                    </div>
					                </div>
					                <div class="row">
					                    <div class="col-md-6">
											<label class="control-label">Sucursal (*)</label>
					                            <select name="sucursal_id" required class="form-control" id="sucursal_id">
													@foreach($listadoSucursal as $key=>$sucursal)
														<option value="{{$sucursal['value']}}">{{$sucursal['label']}}</option>
													@endforeach
												</select>
										</div>
										<div class="col-md-6">
											<label class="control-label">Nombres y Apellidos (*)</label>
					                            <input type="text" required class = "form-control" name="nombreApellido" id="nombreApellido" placeholder="Nombres y Apellido" value=""/>
					                    </div>
					                </div>
									<div class="row">
										<div class="col-md-6">
											<label class="control-label">Email (*)</label>
					                        <input type="email" required class = "form-control" name="email" id="email" placeholder="Email" value="" autocapitalize="off"/>										
										</div>
										<div class="col-md-6" style="display:none">
											<label class="control-label">Password (*)</label>
											<input type="hidden" required class = "form-control" name="password" placeholder="Password" value=""/>	
										</div>
									</div>	
									<div class="row">	
										<div class="col-md-6">
					                        <label class="control-label">Agente DTP(*)</label>
					                        <select name="codigo_agente" class = "form-control" id="codigo_agente">
												@foreach($listAgenteDtp as $key=>$agenteDTP)
													<option value="{{$agenteDTP['value']}}">{{$agenteDTP['label']}}</option>
												@endforeach
											</select>
					                    </div>
										<div class="col-md-6">
											<label class="control-label">Activo (*)</label>
											<select name="activo" required class = "form-control" id="activo">
												<option value="true">SI</option>
												<option value="false">NO</option>
											</select>
										</div>
					                 </div>
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary" style=" background: #e2076a !important;" name="guardar" value="Guardar">Guardar</button>
								</div>
                            </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/mc/scripts')
	<script>
		$('#agencia_id').change(function(){
				$.ajax({
					type: "GET",
					url: "{{route('getSucursales')}}",
					dataType: 'json',
					data: {
						dataSucursal: $(this).val()
			       		},
					success: function(rsp){
								$('#sucursal_id').empty()
								$('#sucursal_id').trigger("chosen:updated");
							$.each(rsp, function(key,value){
								$('#sucursal_id').append('<option value="'+value.value+'">'+value.label+'</option>');
								$('#sucursal_id').trigger("chosen:updated");
							})	
					}	
				})
			})
		$('#agencia_id').trigger('change');

		$(document).ready(function() {			
			$("#password").val('');
			
			$("#frmUsuarios").submit(function(e){
				//e.preventDefault();
				var $form = $('#frmUsuarios');
				console.log($form.attr('action') + "?" +$form.serialize());
				
				//$form.unbind('submit').submit();
				//alert('yolo');
				
			});
			
		})
	</script>
@endsection