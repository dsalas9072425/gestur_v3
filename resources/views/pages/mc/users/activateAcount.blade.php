@extends('masters')
@section('title', 'Agregar Contraseña')
@section('styles')
	@parent
@endsection

@section('content')
<section id="content" class="gray-area">
    <div class="container">
        <div id="main">
            <br>
            @include('flash::message')
            <div class="block">
                <h2></h2>
                <div class="tab-container full-width-style white-bg">
                    <ul class="tabs">
                        <li><a href="#one-tab" data-toggle="tab"><i class="soap-icon-user circle"></i>Agregar<br>Contraseña</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="one-tab">
                            <h2 class="tab-content-title">Actualizar Contraseña</h2>
                            <form class="edit-profile-form" id ="frmPassword" action="{{route('activarPassword')}}" method="POS">
                             {{csrf_field()}}
                            <input type="hidden" value="{{$usuario[0]->id_usuario}}" name="usuer_id">
                                <input type="hidden" value="true" class="input-text full-width" name="form_pass">
                                     <div class="row">
                                        <div class="form-group col-xs-12 col-sm-6">
                                            <label>Nombre de Usuario:</label>
                                            <input value="{{$usuario[0]->nombre_apellido}}" class = "Requerido form-control"  name="nombre_apellido" disabled="disabled">
                                        </div>    
                                        <div class="form-group col-xs-12 col-sm-6">
                                            <label>Email:</label>
                                            <input value="{{$usuario[0]->email}}" class = "Requerido form-control" name="email" disabled="disabled">
                                        </div>
                                    </div>
                                	<div class="row">
                                    	<div class="form-group col-xs-12 col-sm-6">
	                                        <label>Nueva Contraseña:</label>
	                                        <input type="password" value="" minlength="7" maxlength="10" id= "password" required class = "Requerido form-control" name="pass">
                                    	</div>    
	                                	<div class="form-group col-xs-12 col-sm-6">
	                                        <label>Confirmar Contraseña:</label>
	                                        <input type="password" value="" minlength="7" maxlength="10" required id= "confirm_password"  class = "Requerido form-control" name="confirm_pass">
	                                    </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-left" id= "agregar">Aceptar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
	@include('layouts/mc/scripts')
	<script>
			var password = document.getElementById("password");
  			var confirm_password = document.getElementById("confirm_password");

			function validatePassword(){
			  if(password.value != confirm_password.value) {
			    confirm_password.setCustomValidity("Las contraseñas no coinciden");
			  } else {
			    confirm_password.setCustomValidity('');
			  }
			}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;

	</script>
@endsection