@extends('masters')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
@endsection
<?php
include("../comAerea.php");
include("../destinationFligth.php");
?>
@section('content')

<section id="content" class="gray-area">
    <div class="container">
        <div id="main">
            @include('flash::message')
            <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large; margin-top: 5%">Accesos</h1>
            <div class="block sectiontop">
				<ul class="nav nav-tabs">
					  <li class="active"><a data-toggle="tab" href="#home">Accesos</a></li>
					  <!--<li><a data-toggle="tab" href="#menu1">Vuelos</a></li>-->
				</ul>
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
                            <br>
                            <form action="" id="frmBusqueda" method="post" style="margin-left: 1%; margin-right: 2%;"> 
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Desde</label>
			                                    <div class="input-group date">
											        <div class="input-group-addon">
											            <i class="fa fa-calendar"></i>
											        </div>
											        <input type="text" name="desde" id="desde" class="form-control pull-right">
											    </div> 	
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Agencia</label>
                                                <select name="agencia_id" required class = "Requerido form-control" class="chosen-select-deselect" id="agencia_id">
					                            	<option value="0">Seleccione Agencia</option>
													@foreach($agencias as $key=>$agencia)
														<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
													@endforeach
												</select>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Usuario</label>
					                            <select name="usuario_id" required class = "Requerido form-control" class="chosen-select-deselect" id="usuario_id">
					                            	<option value="0">Seleccione Usuario</option>
													@foreach($usuarios as $key=>$usuario)
														<option value="{{$usuario['value']}}">{{$usuario['label']}}</option>
													@endforeach
												</select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                        <div class="col-md-12">
                                            <label class="hidden-xs">&nbsp;</label>
                                            <button type="button" style="width: 160px;  background-color: #e2076a;" id= "buscarReservas" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-search"></i> Buscar Accesos</button>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="hidden-xs">&nbsp;</label>
                                            <button id="limpiarFiltros" style="width: 160px;" type="reset" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                                        </div>
                                        <!--<div class="col-md-12">
                                             <label class="hidden-xs">&nbsp;</label>
                                            <button type="button" style="width: 160px; background-color: #fdb714;" id="botonExcel" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="fa fa-file-excel-o"></i> Exportar a Excel</span></button>

                                        </div>-->
										
                                    </div>
								</div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas" style= "overflow-x: scroll; overflow-y: hidden;">
										<div class="table-responsive">
											<table id="reservas" class="table table-bordered table-hover">
												<thead>
													<tr class='bgblue'>
														<th>Fecha</th>
														<th>E-mail</th>
														<th>Usuario</th>
														<th>Agencia</th>
														<th>Sucursal</th>
													</tr>
												</thead>
												<tbody>

												</tbody>
											</table>
										</div>
                                    </div>
                                </div>
								<div class="row" id="totalReservas">
								</div>
                            </form>
					</div>
				</div>
            </div>
        </div>
    </div>
	
</section>
@endsection

@section('scripts')
	@include('layouts/mc/scripts')
		<script>
			$("#desde").datepicker();

			$('#reservas').dataTable({
				 "aaSorting":[[0,"desc"]]
				});

		    $('#agencia_id').change(function(){
				$.ajax({
					type: "GET",
					url: "{{route('getUsuariosAgencia')}}",
					dataType: 'json',
					data: {
						dataAgencia: $(this).val()
			       		},
					success: function(rsp){
								$('#usuario_id').empty()
								$('#usuario_id').append('<option value="">Seleccione Usuario</option>');
							$.each(rsp, function(key,value){
								$('#usuario_id').append('<option value="'+value.value+'">'+value.label+'</option>');
								$('#usuario_id').trigger("chosen:updated");
							})	
					}	
				})
			})
			$("#buscarReservas").click(function(){
				var dataString = $("#frmBusqueda").serialize();
				$.ajax({
					type: "GET",
					url: "{{route('mc.getAccesos')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
							console.log(rsp);
							var oSettings = $('#reservas').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#reservas').dataTable().fnDeleteRow(0,null,true);
							}
							var iconocancelar ='<a href="#" target="_blank"><i class="fa fa-times"></i></a>';
							$.each(rsp, function (key, item){
								var fechaF = '<b style="color:#7db921;">'+item.fecha+'</b>';
								var usuarioF = '<b style="color:#2d3e52;">'+item.usuario+'</b>';
								var dataTableRow = [
														fechaF,
														item.email,
														usuarioF,
														item.agencia,
														item.sucursal,
													];
								var newrow = $('#reservas').dataTable().fnAddData(dataTableRow);
												})	
					}
				})	
			});	

			$("#buscarReservas").trigger('click');

		</script>
@endsection