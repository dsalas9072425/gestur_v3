@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
        	<br>
			<br>
			<br>
			<br>
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop" style="background-color: #fff;">
            	@include('flash::message')
				<div class="booking-information travelo-box">
					<div>
						<h1 class="subtitle hide-medium" style="font-size: xx-large;">Editar Usuario</h1>
						<br>
			               <form id="frmUsuarios" class="contact-form" action="{{route('mc.doEditUser')}}" method="post" autocomplete="off">
			               {{ csrf_field() }}
					            <div class="form-group">
					                <div class="row">
                            		<input type="hidden" class="form-control" value="{{$usuario[0]->id_usuario}}" name="idSucursalAgencia" id="idSucursalAgencia"/>
					                    <div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Perfil(*)</label>
											<select name="perfil_id" required class="form-control" id="perfil_id">
												@foreach($listadoPerfil as $key=>$perfil)
													<option value="{{$perfil['value']}}">{{$perfil['label']}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
					                        <label class="control-label">Agencia (*)</label>
					                            <select name="agencia_id" required class="form-control" id="agencia_id">
													@foreach($listadoAgencia as $key=>$agencia)
														<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
													@endforeach
												</select>
					                    </div>
					                </div>
					            </div>
					            <div class="form-group">
					                <div class="row">
					                    <div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Sucursal (*)</label>
					                            <select name="sucursal_id" required class="form-control" id="sucursal_id">
													@foreach($listadoSucursal as $key=>$sucursal)
														<option value="{{$sucursal['value']}}">{{$sucursal['label']}}</option>
													@endforeach
												</select>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Nombres y Apellidos (*)</label>
					                            <input type="text" required class="form-control" name="nombreApellido" id="nombreApellido" placeholder="Nombres y Apellido" value="{{$usuario[0]->nombre_apellido}}"/>
					                    </div>
					                </div>
					            </div>
								<div class="form-group">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Email (*)</label>
					                        <input type="email" required class="form-control" name="email" placeholder="Email" value="{{$usuario[0]->email}}" disabled="disabled" />										
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Password</label>
											<input type="password" class="form-control" name="password" id="password" placeholder="Password" />	
											<small><b>Obs:<b> En caso de enviar este campo en blanco se mantendra la contraseña anterior</small>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
					                        <label class="control-label">Agente DTP(*)</label>
					                        <select name="codigo_agente" required class="form-control" id="codigo_agente">
												@foreach($listAgenteDtp as $key=>$agenteDTP)
													<option value="{{$agenteDTP['value']}}">{{$agenteDTP['label']}}</option>
												@endforeach
											</select>
					                    </div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Activo (*)</label>
											<select name="activo" required class="form-control" id="activo">
												<option value="true">SI</option>
												<option value="false">NO</option>
											</select>
										</div>
					                 </div>
					            </div>
								<div class="form-group">
									<br>
									<button type="submit"class="btn btn-info text-center btn transaction_normal hide-small normal-button" style=" background: #e2076a !important;" name="guardar" value="Guardar">Guardar</button>
								</div>
                            </form>
                    </div> 
                </div>           
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/mc/scripts')
		<script>
		$(document).ready(function() {
			$('#agencia_id option[value="{{$usuario[0]->id_agencia}}"]').attr("selected", "selected");
			$('#agencia_id').trigger("chosen:updated");
			$('#perfil_id option[value="{{$usuario[0]->id_perfil}}"]').attr("selected", "selected");

			valor ="{{$usuario[0]->id_perfil}}";
			perfil = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil}}";
			if(perfil == 2){
				$('#perfil_id').prop('disabled', 'disabled');
			}

			$('#sucursal_id option[value="{{$usuario[0]->id_sucursal_agencia}}"]').attr("selected", "selected");
			$('#sucursal_id').trigger("chosen:updated");
			$('#codigo_agente option[value="{{$usuario[0]->codigo_agente}}"]').attr("selected", "selected");
			$('#codigo_agente').trigger("chosen:updated");

			$('#agencia_id').change(function(){
				$.ajax({
					type: "GET",
					url: "{{route('getSucursales')}}",
					dataType: 'json',
					data: {
						dataSucursal: $(this).val()
			       		},
					success: function(rsp){
							$('#sucursal_id').empty();
							$('#sucursal_id').trigger("chosen:updated");
							$.each(rsp, function(key,value){
								$('#sucursal_id').append('<option value="'+value.value+'">'+value.label+'</option>')
								$('#sucursal_id').trigger("chosen:updated");
							})	
					}	
				})
			})
			$('#agencia_id').trigger('change');
			var activo = "{{$usuario[0]->activo}}";
			if(activo =="S"){
				$('#activo option[value="true"]').attr("selected", "selected");
			}else{
				$('#activo option[value="false"]').attr("selected", "selected");
			}
		})
	</script>
@endsection