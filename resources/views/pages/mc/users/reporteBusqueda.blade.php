@extends('masters')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="{{asset('mc/css/mdb.css')}}" >
@endsection
@section('content')
<div style="margin-left: 1%; margin-right: 2%;">
			<br>
            @include('flash::message')
            <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large; margin-top: 5%">Busquedas</h1>
            <br>

				<ul class="nav nav-tabs">
					  <li class="active"><a data-toggle="tab" href="#home">Hoteles</a></li>
					  <li><a data-toggle="tab" href="#menu1">Vuelos</a></li>
				</ul>
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
                            <form action="" id="frmBusqueda" method="post" style="margin-left: 2%;margin-right: 1%;margin-top: 0px;"> 
                                <div class="row">
                                	<br>
                                	<br>
                                    <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <label>Agencia</label>
					                            <select name="agencia_id" required class = "form-control" id="agencia_id">
					                            	<option value="0">Seleccione Agencia</option>
													@foreach($agencias as $key=>$agencia)
														<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
													@endforeach
												</select>
                                            </div>
											<div class="col-xs-12 col-sm-6 col-md-6">
                                                <label>Usuario</label>
					                            <select name="usuario_id" required class = "form-control" id="usuario_id">
					                            	<option value="0">Seleccione Usuario</option>
													@foreach($usuarios as $key=>$usuario)
														<option value="{{$usuario['value']}}">{{$usuario['label']}}</option>
													@endforeach
												</select>
                                            </div>
                                        </div>    
                                        <div class="row">    
			                                <div class="col-xs-12 col-sm-6 col-md-6">
			                                    <label>Desde</label>
			                                    <div class="input-group date">
											        <div class="input-group-addon">
											            <i class="fa fa-calendar"></i>
											        </div>
											        <input type="text" name="desde" id="desde" class="form-control pull-right">
											    </div> 	
			                                </div>
											<div class="col-xs-12 col-sm-6 col-md-6">
												<div class="form-group" style="display:none">
													<label>Tiempo de Refresh</label>
													<select name="refreshTime" class= "form-control" id="estadorefresh">
														<option value="0">Ninguno</option>
														<option value="10000">10 Segundos</option>
														<option value="30000">30 Segundos</option>
														<option value="60000">60 Segundos</option>
													</select>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                        <div class="col-md-12"> 
                                            <button type="button" style="width: 140px;background-color: #e2076a;margin-bottom: 5px;padding-right: 5px;padding-left: 5px;padding-top: 5px; color:#fff;" id= "busquedaBtn" class="btn text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-search"></i> Busqueda</button>
                                        </div>
                                    </div>
								</div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas">
										<div class="table-responsive">
											<div id="container" style="min-width: 660px; height: 550px; margin: 0 auto"></div>
										</div>
                                    </div>
                                </div>
                            </form>
					</div>
					<div id="menu1" class="tab-pane fade">
						<form action="" id="frmBusquedaFligth" method="post" style="margin-left: 2%;margin-right: 1%;margin-top: 0px;"> 
                                <div class="row">
                                	<br>
                                	<br>
                                    <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <label>Agencia</label>
					                            <select name="agencia_id" required class= "form-control" id="agencia_id_fligth">
					                            	<option value="0">Seleccione Agencia</option>
													@foreach($agencias as $key=>$agencia)
														<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
													@endforeach
												</select>
                                            </div>
											<div class="col-xs-12 col-sm-6 col-md-6">
                                                <label>Usuario</label>
					                            <select name="usuario_id" required class = "Requerido form-control" class="chosen-select-deselect" id="usuario_id_fligth">
					                            	<option value="0">Seleccione Usuario</option>
													@foreach($usuarios as $key=>$usuario)
														<option value="{{$usuario['value']}}">{{$usuario['label']}}</option>
													@endforeach
												</select>
                                            </div>
                                        </div>    
                                        <div class="row">
			                                <div class="col-xs-12 col-sm-6 col-md-6">
			                                    <label>Fecha Busqueda</label>
			                                    <div class="input-group date">
											        <div class="input-group-addon">
											            <i class="fa fa-calendar"></i>
											        </div>
											        <input type="text" name="desde" id="desdeFligth" class="form-control pull-right">
											    </div> 	
			                                </div>
											<div class="col-xs-12 col-sm-6 col-md-6">
												<div class="form-group" style="display:none">
													<label>Tiempo de Refresh</label>
													<select name="refreshTime" class = "Requerido form-control" class="input-text full-width" id="estadorefreshFligth">
														<option value="0">Ninguno</option>
														<option value="10000">10 Segundos</option>
														<option value="30000">30 Segundos</option>
														<option value="60000">60 Segundos</option>
													</select>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                        <div class="col-md-12"> 
                                            <button type="button" style="width: 140px;background-color: #e2076a;margin-bottom: 10px;padding-right: 5px;padding-left: 5px;padding-top: 5px; color:#fff;" id= "busquedaBtnFligth" class="btn text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-search"></i> Busqueda</button>
                                        </div>
                                    </div>
								</div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas">
										<div class="table-responsive">
											<div id="containers" style="min-width: 660px; height: 450px; margin: 0 auto"></div>
										</div>
                                    </div>
                                </div>
						</form>
					</div>
				</div>
</div>        
@endsection

@section('scripts')
	@include('layouts/mc/scripts')
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script type="text/javascript">
		$('.nav-tabs a').on('shown.bs.tab', function(event){
		    var x = $(event.target).text();         // active tab
		    var y = $(event.relatedTarget).text();  // previous tab
		});

		$("#desde").datepicker({
								dateFormat: 'yy-mm-dd',
								});

		$("#desdeFligth").datepicker({
								dateFormat: 'yy-mm-dd',
								});

		$("#busquedaBtn").css("background-color", "#e2076a");
		$("#busquedaBtnFligth").css("background-color", "#e2076a");

		$(document).ready(function() {
			crearDiagrama();
			crearDiagramaFligth();
			$("#busquedaBtn").click(function(){
				crearDiagrama();
			})	

			$("#busquedaBtnFligth").click(function(){
				crearDiagramaFligth();
			})	
		})
		$('#agencia_id').select2();
		$('#usuario_id').select2();
		$('#agencia_id').change(function(){
				$.ajax({
					type: "GET",
					url: "{{route('mc.getUsuariosAgencia')}}",
					dataType: 'json',
					data: {
						dataAgencia: $(this).val()
			       		},
					success: function(rsp){
								$('#usuario_id').empty()
								//$('#usuario_id').trigger("chosen:updated");
								$('#usuario_id').append('<option value="">Seleccione Usuario</option>');
							$.each(rsp, function(key,value){
								$('#usuario_id').append('<option value="'+value.value+'">'+value.label+'</option>');
								//$('#sucursal_id').trigger("chosen:updated");
							})	
					}	
				})
			})

			$('#agencia_id_fligth').change(function(){
				$.ajax({
					type: "GET",
					url: "{{route('mc.getUsuariosAgencia')}}",
					dataType: 'json',
					data: {
						dataAgencia: $(this).val()
			       		},
					success: function(rsp){
								$('#usuario_id_fligth').empty()
								//$('#usuario_id').trigger("chosen:updated");
								$('#usuario_id_fligth').append('<option value="">Seleccione Usuario</option>');
							$.each(rsp, function(key,value){
								$('#usuario_id_fligth').append('<option value="'+value.value+'">'+value.label+'</option>');
								//$('#sucursal_id').trigger("chosen:updated");
							})	
					}	
				})
			})

		function crearDiagrama(){	
			var dataString = $("#frmBusqueda").serialize();	
			$.blockUI({
		                centerY: 0,
		                message: '<div class="loadingC"><img style="width: 160px; height: 160px;" src="loading.gif" alt="Loading" /><br><h2>Procesando..... </h2></div>',
		                css: {
		                    color: '#000'
		                    }
		               });

			$.ajax({
					type: "GET",
					url: "{{route('mc.getBusquedas')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
								$.unblockUI();
								var processed_json = new Array(); 
								$.each(rsp, function(key,value){
	                       			 processed_json.push([rsp[key].name, rsp[key].y]);
	                    		})
							  $('#container').html("");
							  $('#container').highcharts({
					                    chart: {
					                        type: "pie"
					                    },
					                    title: {
					                        text: "Reporte de Busqueda"
					                    },
					                    xAxis: {
					                        type: 'category',
					                        allowDecimals: false,
					                        title: {
					                            text: ""
					                        }
					                    },
					                    yAxis: {
					                        title: {
					                            text: "Scores"
					                        }
					                    },
					                    series: [{
						                    name: 'Visitas',
					                        data: processed_json
					                    }]
					                }); 
							 }
					 });
		}

		function crearDiagramaFligth(){	
			var dataString = $("#frmBusquedaFligth").serialize();	
			$.ajax({
					type: "GET",
					url: "{{route('mc.getBusquedasFlight')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
								var processed_json = new Array(); 
								$.each(rsp, function(key,value){
	                       			 processed_json.push([rsp[key].name, rsp[key].y]);
	                    		})
							  $('#containers').html("");
							  $('#containers').highcharts({
					                    chart: {
					                        type: "pie"
					                    },
					                    title: {
					                        text: "Reporte de Busqueda de Vuelos"
					                    },
					                    xAxis: {
					                        type: 'category',
					                        allowDecimals: false,
					                        title: {
					                            text: ""
					                        }
					                    },
					                    yAxis: {
					                        title: {
					                            text: "Scores"
					                        }
					                    },
					                    series: [{
						                    name: 'Vuelos',
					                        data: processed_json
					                    }]
					                }); 
							 }
					 });
		}

		$('#estadorefresh').change(function(){
				setInterval(crearDiagrama, $("#estadorefresh").val());
		})
		$('#estadorefreshFligth').change(function(){
				setInterval(crearDiagramaFligth, $("#estadorefreshFligth").val());
		})	
</script>

	
@endsection