
@extends('masters')
@section('title', 'Asignar Permisos')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	.checkLabel {
	
	font-size: 15px !important;
	font-weight: 700 !important;
	}
	.input-icheck {
		 padding: 10px;
	}


input[type=checkbox] {
  transform: scale(1.5);
}
.card,.card-header {
	border-radius: 14px !important;
}

</style>



<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Editar Permisos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

    <form id="frmPermisos">
    	<div class="row">

    		<div class="col-xs-12 col-sm-6 col-md-6">
    			<div class="form-group">
    				<label>Personas</label>
    				<select class="form-control select2" tabindex="1" name="id_persona" id="id_persona"
    					style="width: 100%;">
    					<option value="">Seleccione Persona</option>

    					@foreach ($personas as $persona)
    					<option value="{{$persona->id}}">{{$persona->nombre}} {{$persona->apellido}} -
    						{{$persona->tipopersona->denominacion}}</option>
    					@endforeach
    				</select>
    			</div>
    		</div>
    	</div>
    	<div class="row">
			@foreach ($permisos as $key => $permiso)
				@if ($permiso->id == 62 && $idEmpresa == 1)
					<div class="col-xs-6 col-sm-4">
						<div class="checkbox">
							<label class="checkLabel">
								<input type="checkbox" name="{{$permiso->id}}" id="{{$permiso->id}}" class="input-icheck">
								{{$permiso->desc}}
							</label>
						</div>
					</div>
				@elseif ($permiso->id != 62)
					<div class="col-xs-6 col-sm-4">
						<div class="checkbox">
							<label class="checkLabel">
								<input type="checkbox" name="{{$permiso->id}}" id="{{$permiso->id}}" class="input-icheck">
								{{$permiso->desc}}
							</label>
						</div>
					</div>
				@endif
			@endforeach
		</div>



    	<div class="col-xs-12">
    		<button type="button" id="btnGuardar" class="btn btn-success btn-lg pull-right" style="margin: 0 0 10px 10px;">GUARDAR</button>
    	</div>

    </form>








				   








@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>

$(document).ready(function(){

	$('.select2').select2();
		$('.select2').val('').trigger('change.select2');

		var idPersona = '{{$idPersona}}';
		if(idPersona != 0 && idPersona != ''){
			$('#id_persona').val(idPersona).trigger('change.select2');
		}//if
		obtenerPermisos(idPersona);

});

	


	$('#id_persona').change(function(){

		obtenerPermisos('');
	});


	function obtenerPermisos(id){


		 					$.blockUI({
                                        centerY: 0,
                                        message: "<h2>Obteniendo Permisos. . . </h2>",
                                        css: {
                                            color: '#000'
                                        }
                                    });

		 if(id == '')					
		var id = $('#id_persona').val();

		console.log(id);


		$.ajax({
                        type: "GET",
                        url: "{{route('obtenerPermisos')}}",
                        dataType: 'json',
                        data: {id:id},
					    retries       : 3,     
					    retryInterval : 1000,   

                          error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                           $.unblockUI();

                        },
                        success: function(rsp){

                        	$.unblockUI();
                        	$('.input-icheck').removeAttr('checked');
                        	
                        	var n = '';
                        	$.each(rsp.permisos, function(index,item){
                        		// console.log(item.id_permiso)

                        		if(item.id_permiso != ''){
                        		// $('#'+item.id_permiso).val('1').trigger('change.select2');
                        		 $("#"+item.id_permiso).attr("checked",item.id_permiso);
                        	}

                        	});


                        		
                          
                                }//funcion  
                });
	}











	$('#btnGuardar').on('click',function(){


		var StringData =  $('#frmPermisos').serialize();


			 $.ajax({
                        type: "GET",
                        url: "{{route('doGuardarPermiso')}}",
                        dataType: 'json',
                        data: StringData,

                          error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
                        success: function(rsp){

                        	console.log(rsp.rsp);

                        	if(rsp.rsp == true){

                        		location.href ="{{ route('indexPermisos')}}";

                        	} else {
                        		 $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        	}//else

                        		
                          
                                }//funcion  
                });










	});
		
				
			

	</script>
@endsection