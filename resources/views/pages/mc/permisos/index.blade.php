
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.card,.card-header {
			border-radius: 14px !important;
	}

	</style>
@endsection
@section('content')


<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Permisos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

				<form id="frmProductoConsulta">
					<div class="row">

						<div class="col-xs-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Personas</label>
								<select class="form-control select2" tabindex="1" name="id_persona" id="id_persona"
									style="width: 100%;">
									<option value="">Seleccione Opción</option>


									@foreach ($personas as $persona)
									<option value="{{$persona->id}}">{{$persona->nombre}} {{$persona->apellido}} -
										{{$persona->tipopersona->denominacion}}</option>
									@endforeach

								</select>
							</div>
						</div>

						<div class="col-sm-12 pb-1">
							<button for="" id="btnEditar" class="btn  btn-lg btn-success">EDITAR / ASIGNAR</button>
							<button id="btnBuscar" class="btn btn-info btn-lg">BUSCAR</button>
						</div>
					</div>
				</form>


				<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Permiso</th>
							</tr>
						</thead>


						<tbody style="text-align: center">
							@foreach ($permisosPersona as $permisos)
							<tr>
								<td>{{$permisos->nombre}} {{$permisos->apellido}}</td>
								<td>{{$permisos->desc}}</td>
							</tr>
							@endforeach

						</tbody>
					</table>
				</div>



			</div>
		</div>
	</section>
</section>




@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script>
	$("#listado").dataTable();
	$('.select2').select2();

		
		$('#btnBuscar').on('click',function(){

			var id = $('#id_persona').val();
				
					$.ajax({
                        type: "GET",
                        url: "{{route('getPermisosPersona')}}",
                        dataType: 'json',
                        data: {id:id},
					    retries       : 3,     
					    retryInterval : 1000,   

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
                        success: function(rsp){

                        	// console.log(rsp);

                       		var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

                       		$.each(rsp.data, function (key, item){
								var dataTableRow = [	
														item.nombre+' '+item.apellido,
														item.desc
													];
								var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

							});
                          
                                }//funcion  
                });

		});
		
		


		$('#btnEditar').on('click',function(e){
			e.preventDefault();

			var id = $('#id_persona').val();
				location.href ="{{route('editPermisos')}}?id="+id;

		});			
			

	</script>
@endsection