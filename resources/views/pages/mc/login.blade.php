<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrador DTP | Acceso</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('mC/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('mC/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('mC/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('mC/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('mC/css/blue.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
 </head>
<body class="hold-transition login-page" style="margin:0;background-image:url({{asset('images/fondo_factour.jpg')}});background-repeat: no-repeat; background-position: center;background-size: cover;">

<div class="login-box">
  <div class="login-logo">
    <a href="{{route('mc.login')}}"><img src="{{asset('images/logo_factour.png')}}" class="img-circle" style="border-radius: 0%; width: 65%;" alt="User Image"></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" style="border-radius: 10px;">
    <br>
    <br>
    <form id="login-form" action="{{route('mc.doLogin')}}" method="get">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="usuario" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control"  name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <!--<input type="checkbox"> Recordarme-->
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" style="background-color:#FF0080; border-color: #FF0080;" class="btn btn-primary btn-block btn-flat">Ingresar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <a href="#">Restablecer mi contraseña</a><br>
    <!-- <a href="register.html" class="text-center">Registrarme</a>*-->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('mC/js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('mC/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('mC/js/icheck.min.js')}}"></script>

<script src="{{asset('mC/js/mensajes.js')}}"></script>

<script src="{{asset('mC/js/ajax-setup.js')}}"></script>

<script src="{{asset('mC/js/jquery.blockUI.js')}}"></script>

<script type="text/javascript">
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });

    $('#login-form').submit(function(e){
          var msg = "";
          e.preventDefault();
          $('#login-page').html('<div class="loading"><img src="images/loading.gif" alt="loading" /><br/>Un momento, por favor...</div>');
          $.ajax({
            url: $('#login-form').attr( 'action' ),
            type: 'GET',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: $('#login-form').serialize(),
            success: function(jsonRsp){
              if(jsonRsp.codRetorno==0){
                if(jsonRsp.idPerfil != 3||jsonRspidPerfil == 8){
                   window.location.replace("{{route('mc.homemc')}}");
                }else{
                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se puede iniciar sesión</h2><p>Acceso reservado a Administradores.<br>Favor vuelva a intentar</p><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                mostrarMensaje(msg);
                }
              }else{
                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se puede iniciar sesión</h2><p>'+ jsonRsp.desRetorno +'.<br>Favor vuelva a intentar</p><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                mostrarMensaje(msg);
              }
            },
            error: function(){
                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se puede acceder</h2><p><br>Favor vuelva a intentar</p><p><br><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                mostrarMensaje(msg);
            }
          });
        });
  });
</script>
</body>
</html>

