@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Excel de comisiones</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
				<form action="{{route('liquidar_dtp_puntos.generar_excel_template')}}">
					<div class="row">
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
							<div class="form-group">
								<label>Mes</label>					            	
								<select class="form-control input-sm select2" name="mes_generar" id="" style="padding-left: 0px;width: 100%;">
									<option value="">Seleccione Mes</option>
									@if(!empty($mes))
										@foreach($mes as $key=>$mess)
											<option value="{{$key}}">{{$mess}}</option>
										@endforeach	
										@endif
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
							<div class="form-group">
								<label>Año</label>					            	
								<select class="form-control input-sm select2"  name="anho_generar" id="" style="padding-left: 0px;width: 100%;">
									<option value="">Seleccione Año</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
									<option value="2024">2024</option>
									<option value="2025">2025</option>
									<option value="2026">2026</option>
									<option value="2027">2027</option>
								</select>
							</div>
						</div> 
					</div>
					<div class="row">
						<div class="col-md-12" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
							<button   class="btn btn-info btn-lg">Generar Plantilla</button>
						</div>
					</div>
				</form>
				
				<hr>
				<div class="row mt-1">
					<div class="col-md-12">
						<form  method="post"  action="{{route('liquidar_dtp_puntos.cargar_excel_comision')}}" enctype="multipart/form-data">
							<div class="row">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
									<div class="form-group">
										<label>Mes Desde</label>					            	
										<select class="form-control input-sm select2" name="mes_desde" id="" style="padding-left: 0px;width: 100%;">
											<option value="">Seleccione Mes</option>
											@if(!empty($mes))
												@foreach($mes as $key=>$mess)
													<option value="{{$key}}">{{$mess}}</option>
												@endforeach	
												@endif
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
									<div class="form-group">
										<label>Año Desde</label>					            	
										<select class="form-control input-sm select2"  name="anho_desde" id="" style="padding-left: 0px;width: 100%;">
											<option value="">Seleccione Año</option>
											<option value="2022">2022</option>
											<option value="2023">2023</option>
											<option value="2024">2024</option>
											<option value="2025">2025</option>
											<option value="2026">2026</option>
											<option value="2027">2027</option>
										</select>
									</div>
								</div> 
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
									<div class="form-group">
										<label>Mes Hasta</label>					            	
										<select class="form-control input-sm select2" name="mes_hasta" id="" style="padding-left: 0px;width: 100%;">
											<option value="">Seleccione Mes</option>
											@if(!empty($mes))
												@foreach($mes as $key=>$mess)
													<option value="{{$key}}">{{$mess}}</option>
												@endforeach	
												@endif
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
									<div class="form-group">
										<label>Año Hasta</label>					            	
										<select class="form-control input-sm select2"  name="anho_hasta" id="" style="padding-left: 0px;width: 100%;">
											<option value="">Seleccione Año</option>
											<option value="2022">2022</option>
											<option value="2023">2023</option>
											<option value="2024">2024</option>
											<option value="2025">2025</option>
											<option value="2026">2026</option>
											<option value="2027">2027</option>
										</select>
									</div>
								</div> 
								<div class="col-xs-12" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
									<div class="form-group">
										<label for="exampleFormControlFile1">Subir Plantilla con comisiones</label>
										<input type="file" class="form-control-file" id="exampleFormControlFile1" name="file" required>
									  </div>
								</div>

							</div>
						
							
							<div class="row">
								<div class="col-md-12" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
									<button class="btn btn-success btn-lg" type="submit">PROCESAR PLANTILLA</button>
								</div>
							</div>
						</form>	
					</div>
				
				</div>

            </div>	
        </div>    
    </div>   
</section>



    <!-- /.content -->

    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')	
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script>
		$(document).ready(function() {
			$('.select2').select2();

			var Fecha = new Date();
			var mes = (Fecha.getMonth() +1) - 1;
			console.log(mes);
			if(mes < 10){
				mes = '0'+mes;
			}else{
				mes = mes;
			}

			$('#periodo_mes').val(mes).trigger('change.select2');
			$('#periodo_anho').val(Fecha.getFullYear()).trigger('change.select2');

			$("#listado").dataTable({
				 	"aaSorting":[[0,"desc"]]
			});

		});

		
	</script>
	

@endsection
