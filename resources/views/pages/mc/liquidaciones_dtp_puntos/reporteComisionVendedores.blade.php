@extends('masters')
@section('title', 'Reporte Puntos')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	
	.btnExcel {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 1.42857143;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;

}

.btnExcel {
	padding: 10px 16px;
	font-size: 18px;
	line-height: 1.3333333;
	border-radius: 6px;
}

.btnExcel {
	color: #fff;
	background-color: #5bc0de;
	border-color: #46b8da;
}

.btnExcel {
	margin: 0 0 10px 10px;
}

#botonExcelCabecera {
	display: inline;
}

#botonExcelCabecera .dt-buttons {
	display: inline;
}

/*sdsdds*/
.checkbox label:after {
  content: '';
  display: table;
  clear: both;
}

.checkbox .cr {
  position: relative;
  display: inline-block;
  border: 1px solid #a9a9a9;
  border-radius: .25em;
  width: 1.3em;
  height: 1.3em;
  float: left;
  margin-right: .5em;
}

.checkbox .cr .cr-icon {
  position: absolute;
  font-size: .8em;
  line-height: 0;
  top: 50%;
  left: 15%;
}

.checkbox label input[type="checkbox"] {
  display: none;
}

.checkbox label input[type="checkbox"]+.cr>.cr-icon {
  opacity: 0;
}

.checkbox label input[type="checkbox"]:checked+.cr>.cr-icon {
  opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled+.cr {
  opacity: .5;
}

	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}

	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

	.rojo{
		color: red;
		font-weight: 700;
	}

	.verde{
		color: green;
		font-weight: 700;
	}
	
	.bgBlack,.bgGreen,.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
	}	

	.bgBlack {
    color: #000;
    border: 1px solid black;
	}

	.bgGreen {
    color: #65c758;
    border: 1px solid #65c758;
	}

	.bgRed {
    color: rgb(185, 39, 39);
    border: 1px solid rgb(185, 39, 39);
	}



</style>
<section id="base-style">
	<div class="card-content">
		<div class="card" style="border-radius: 14px;">
			<div class="card-header" style="border-radius: 15px;">
				<h4 class="card-title">Reporte de Puntos Vendedores</h4>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-content collapse show" aria-expanded="true">
				<div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">
					<div class="card-body">

						<ul class="nav nav-tabs nav-underline" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="baseIcon-tab21" data-toggle="tab"
									aria-controls="tabIcon21" href="#cabecera" role="tab" aria-selected="true"><i
										class="fa fa-play"></i> Reporte Puntos</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22"
									href="#detalle" role="tab" aria-selected="false"><i class="fa fa-flag"></i> Facturas</a>
							</li>
						</ul>

						<div class="tab-content">

							<div class="tab-pane active mt-1" id="cabecera" role="tabpanel">

								<form id="verLiquidaciones" autocomplete="off">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label>Vendedor</label>
												<select class="form-control input-sm select2" name="vendedor" id="vendedor" style="width:100%;">
														<option value="">Todos</option>
													@foreach($vendedores as $key=>$vendedor)
													<option value="{{$vendedor->id}}">{{$vendedor->documento_identidad}} - {{$vendedor->nombre}}
														{{$vendedor->apellido}} - {{$vendedor->empresa}}
													</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label>Mes</label>					            	
												<select class="form-control input-sm select2" name="periodo_mes" id="periodo_mes" style="padding-left: 0px;width: 100%;">
													@if(!empty($mes))
														@foreach($mes as $key=>$mess)
															<option value="{{$key}}">{{$mess}}</option>
														@endforeach	
														@endif
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label>Año</label>					            	
												<select class="form-control input-sm select2"  name="periodo_anho" id="periodo_anho" style="padding-left: 0px;width: 100%;">
													<option value="2022">2022</option>
													<option value="2023">2023</option>
													<option value="2024">2024</option>
													<option value="2025">2025</option>
													<option value="2026">2026</option>
													<option value="2027">2027</option>
												</select>
											</div>
										</div> 
									
									</div>
								</form>

								<div class="row">
									<div class="col-12">
										<div class="pull-right mr-1" id="botonExcelCabecera"></div>
										<button type="button" onclick="limpiarFiltros()" id="btnLimpiar"
											class="pull-right btn  btn-light mr-1 btn-lg font-weight-bold text-white">Limpiar</button>
										<a onclick="consultarLiquidaciones()"
											class="pull-right  btn btn-info mr-1 btn-lg font-weight-bold text-white"
											role="button">Buscar</a>

									</div>
								</div>

								<div class="table-responsive table-bordered">
									<table id="listado" class="table" style="width: 100%, font-size: 0.9rem;">
										<thead>
											<tr>
												<th>Vendedor</th>
												<th>Agencia</th>
												<th>Meta <br>Puntos</th>
												<th>Total Neto <br> Facturas</th>
												<th>Total <br>Comision <br> USD</th>
												<th>Total <br>Puntos </th>
												<th style="width: 7%;">Ver</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="detalle" role="tabpanel">

								<form id="verLiquidacionesDetalle" class="mt-1">
									<input type="hidden"  id="anho" name="anho" value="0">
									<input type="hidden"  id="mes" name="mes" value="0">
									<input type="hidden"  id="id_vendedor" name="id_vendedor" />

									{{-- <div class="row"> --}}
									
								</form>
								<div class="row justify-content-end">
									<div class="col-md-3">
										<div class="pull-right mr-1" id="botonExcelDetalle"></div>
									</div>
								</div>
								
							<div class="table-responsive table-bordered">
								<table id="listadoDetalle" class="table" style="width: 100%">
									<thead style="text-align: center">
										<tr>
											<th>Nro Factura</th>
											<th>Tipo</th>
											<th>Fecha Factura</th>
											<th>Total Neto</th>
											<th>Moneda</th>
											<th>Estado <br> Cobro</th>
											<th>Promoción</th>
											<th>Puntos</th>
											<th>Monto Comisión</th>
										</tr>
									</thead>
									<tbody style="text-align: center">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script >
	var tableLiquidaciones;
var tableLiquidacionesDetalle;

$(document).ready(function () {
	$('.select2').select2();
	var periodo = '{{$periodo}}';

	if (periodo != '') {
		$('#periodo').val(periodo).trigger('change.select2');
	}

	
});

const formatter = new Intl.NumberFormat('de-DE', {
	currency: 'USD',
	minimumFractionDigits: 2
});
const formatos = new Intl.NumberFormat('de-DE', {
	currency: 'PYG',
	minimumFractionDigits: 0
});


$('.checkCol').on('click', function () {
	var form = $('#columLiqExcel').serializeArray();
	valor = [];
	// valor.push(0,1,2,8);
	valor.push(0, 1, 2, 3, 4, 6, 7, 11);
	$.each(form, function (item, value) {
		valor.push(parseInt(value.value));
	});

	buttonsLiq(valor, tableLiquidacionesDetalle);
});


function cargarReporteLiquidacionDetalle( id_vendedor,anho,mes) {

	$('#id_vendedor').val(id_vendedor);
	$('#anho').val(anho);
	$('#mes').val(mes);
	$('#tableLiquidacionesDetalle').tab('show');
	$('#baseIcon-tab22').trigger('click');
	consultarDetalleLiquidaciones();

}

function limpiarFiltrosDetalle() {
	$('#periodoDetalle').val('').trigger('change.select2');
	$('#vendedorDetalle').val('').trigger('change.select2');
	// consultarDetalleLiquidaciones();
}

function limpiarFiltros() {
	// $('#periodo').val('').trigger('change.select2');
	$('#vendedor').val('1').trigger('change.select2');

}

function formatearFecha(texto) {
	if (texto != '' && texto != null) {
		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
	} else {
		return '';
	}
}


function consultarLiquidaciones() {
	

	var tableLiquidaciones = $("#listado").DataTable({
		"destroy": true,
		"ajax": {
			"url": "{{route('liquidar_dtp_puntos.reporte_puntos.cabecera')}}",
			"type": "GET",
			"data": {
				"formSearch": $('#verLiquidaciones').serializeArray()
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});

			}
		},
		"columns": [
			{
				"data": "vendedor"
			},
			{
				"data": "agencia"
			},
			{
				"data": "meta"
			},
			{
				"data": function (x) {
					return formatter.format(x.total_neto_facturas);
				}
			},
			{
				"data": function (x) {
					return formatter.format(x.total_comision);
				}
			},
			{
				"data": function (x) {
					return formatter.format(x.total_puntos);
				}
			},
			{
				"data": function (x) {
				
					var btn = `<div>`;
						

					let anho = $('#periodo_anho').val();
					let mes = $('#periodo_mes').val();
					btn += `<a onclick="cargarReporteLiquidacionDetalle(${x.id}, '${anho}', '${mes}')" class="btn btn-info" style="padding-left: 6px;padding-right: 6px;color:#fff;margin-right: 10px;" role="button"><i class="fa fa-fw fa-search"></i></a>`;
					btn += '</div>';
					return btn;
				}
			},

		]

	});

	generarExcel([0, 1, 2, 3, 5], tableLiquidaciones);
	return tableLiquidaciones;
};

// <th>Nro Factura</th>
// 											<th>Tipo</th>
// 											<th>Fecha Factura</th>
// 											<th>Total Neto</th>
// 											<th>Moneda</th>
// 											<th>Estado <br> Cobro</th>
// 											<th>Promoción</th>
// 											<th>Puntos</th>
// 											<th>Monto Comisión</th>

function consultarDetalleLiquidaciones() {
	var tableLiquidacionesDetalle = $("#listadoDetalle").DataTable({
		"destroy": true,
		"ajax": {
			"url": "{{route('liquidar_dtp_puntos.reporte_puntos.detalle')}}",
			"type": "GET",
			"data": {
				"formSearch": $('#verLiquidacionesDetalle').serializeArray()
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});

			}
		},

		"columns": [{
				"data": function (x) {
					btn = ``;
					if(x.tipo == 'FACTURA'){
					btn = `<a href="{{route('verFactura',['id'=>''])}}/${x.id_factura}"  class="bgBlack">
												<i class="fa fa-fw fa-search"></i><span>${x.nro_factura}</span></a>`;
					} else {
						btn = `<a href="{{route('verNota',['id'=>''])}}/${x.id_factura}"  class="bgBlack">
												<i class="fa fa-fw fa-search"></i><span>${x.nro_factura}</span></a>`;
					}	
						
					return btn;
				}
			},
			{ "data": function(x)
				{
				
					if(x.tipo == 'FACTURA'){
						return 'Factura';
					} else {
						return 'Nota Credito';
					}
				} 
			},
			{
				"data": function (x) {
					fecha = x.fecha_hora_facturacion.split(' ');
					f = formatearFecha(fecha[0]);
					return f;
				}
			},
			{
				"data": (x) => {
					return formatter.format(x.total_neto_factura);
				}
			},
			{
				"data": function (x) {
					if (x.id_moneda_venta == 143) {
						return 'USD';
					} else if (x.id_moneda_venta == 111) {
						return 'PYG';
					} else if (x.id_moneda_venta == 43) {
						return 'EUR';
					}


				}
			},
			{
				"data": "estado"},
			{
				"data": "promocion"
			},
			{
				"data": function(x)
						{
							console.log(x);
							if(x.tipo == 'FACTURA'){
								return ''+x.total_puntos;
							} else {
								return '-'+x.total_puntos;
							}
						} 
			},
			{
				"data": (x) => {
					return formatter.format(x.total_usd);
				}
			},



		],
	});

	generarExcelDetalle([0, 1, 2, 3, 4, 5,6,7,8], tableLiquidacionesDetalle);
	return tableLiquidacionesDetalle;
}


function generarExcelDetalle(column, tableLiq) {
	$('#botonExcelDetalle').html('');
	var buttons = new $.fn.dataTable.Buttons(tableLiq, {
		buttons: [{
			title: 'Reporte Puntos Factura',
			extend: 'excelHtml5',
			text: '<b>Excel</b>',
			className: 'pull-right text-center btn btn-success btn-lg',
			exportOptions: {
				columns: ':visible'
			},
			exportOptions: {
				columns: column,
				format: {
					body: function (data, row, column, node) {
						if (column != 0 && column != 1 && column != 2 && column != 3 && column != 4 && column != 5 && column != 6) {
							if (data != '0') {
								data = data.replace(".", "");
								//cambiamos la cpunto
								data = data.replace(",", ".");

							}
							var numFinal = parseFloat(data);
							return numFinal;
						} else if(column == 0){
							return $(data).find('span').html();
						
						}
						return data;
					}
				}
			},

			//Generamos un nombre con fecha actual

			filename: function () {
				var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
				return 'ReportePuntosFactura-' + date_edition;
			}
		}]
	}).container().appendTo('#botonExcelDetalle');
}

function generarExcel(column, table) {
	$('#botonExcelCabecera').html('');
	var buttons = new $.fn.dataTable.Buttons(table, {
		buttons: [{
			title: 'Reporte Puntos',
			extend: 'excelHtml5',
			text: '<b>Excel</b>',
			className: 'pull-right text-center btn btn-success btn-lg',
			exportOptions: {
				columns: ':visible'
			},
			exportOptions: {
				columns: column,
				format: {
					//seleccionamos las columnas para dar formato para el excel
					body: function (data, row, column, node) {
						if (column != 0 && column != 1 && column != 6 ) {
					
							if (data != '0') {
								data = data.replace(".", "");
								//cambiamos la cpunto
								data = data.replace(",", ".");

							}
							var numFinal = parseFloat(data);
							return numFinal;
						}

						return data;
					}
				}
			},

			//Generamos un nombre con fecha actual

			filename: function () {
				var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
				return 'ReportePuntos-' + date_edition;
			}
		}]
	}).container().appendTo('#botonExcelCabecera');
}
$('#comision_total').val(0);
$('.numeric').inputmask("numeric", {
	radixPoint: ",",
	groupSeparator: ".",
	digits: 2,
	autoGroup: true,
	// prefix: '$', //No Space, this will truncate the first character
	rightAlign: false,
	oncleared: function () {}
});

function clean_num(n, bd = false) {
	if (n && bd == false) {
		n = n.replace(/[,.]/g, function (m) {
			if (m === '.') {
				return '';
			}
			if (m === ',') {
				return '.';
			}
		});
		return Number(n);
	}
	if (bd) {
		return Number(n);
	}
	return 0;
}

function formatNumber(num) {
	if (!num || num == 'NaN') return '-';
	if (num == 'Infinity') return '&#x221e;';
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
		num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
	return (((sign) ? '' : '-') + num + ',' + cents);
}

/*
 * Levantar modal para seleccionar factura
 */
function pagarComision(id_vendedor, id_liquidacion) {
	//Asignar el selector al vendedor actual de la linea
	$('#idProveedorFactura').val(id_vendedor).trigger('change.select2');
	$('#id_liquidacion_modal').val(id_liquidacion);
	consultarGetFacturas();
	$("#modalFacturas").modal("show");
}

function consultarGetFacturas() {

	var tableLiquidacionesDetalle = $("#listadoFacturaLc").DataTable({

		"destroy": true,
		"ajax": {
			"url": "{{route('liquidar_dtp_puntos.get_facturas_aplicar')}}",
			"type": "GET",
			"data": {
				"formSearch": $('#aplicarFactura').serializeArray()
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});
			}
		},

		"columns": [{
				"data": "id"
			},
			{
				"data": "nro_documento"
			},
			{
				"data": "ruc_proveedor"
			},
			{
				"data": "fecha_documento"
			},
			{
				"data": (x) => {
					return formatter.format(parseFloat(x.total));
				}
			},
			{
				"data": (x) => {
					btn = `<a onclick="asignarFactura(${x.id})" 
									  class="btn btn-info" 
									  style="padding-left: 6px;padding-right: 6px;color:#fff;margin-right: 10px;" 
									  title = "Asignar Factura"
									  role="button"><i class="fa fa-magnet" aria-hidden="true"></i></a>`;

					return btn;
				}
			},
		],
	});
}



let locale = {
	format: 'DD/MM/YYYY',
	cancelLabel: 'Limpiar',
	applyLabel: 'Aplicar',
	fromLabel: 'Desde',
	toLabel: 'Hasta',
	customRangeLabel: 'Seleccionar rango',
	daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
	monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
		'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
		'Diciembre'
	]
};

$('input[name="proveedor_desde_hasta"]').daterangepicker({
	autoUpdateInput: false,
	locale: locale
});

$('input[name="proveedor_desde_hasta"]').on('apply.daterangepicker', function (ev, picker) {
	$(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
});

$('input[name="proveedor_desde_hasta"]').on('cancel.daterangepicker', function (ev, picker) {
	$(this).val('');
});




</script>
@endsection
