@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	
	.btnExcel {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 1.42857143;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;

}

.btnExcel {
	padding: 10px 16px;
	font-size: 18px;
	line-height: 1.3333333;
	border-radius: 6px;
}

.btnExcel {
	color: #fff;
	background-color: #5bc0de;
	border-color: #46b8da;
}

.btnExcel {
	margin: 0 0 10px 10px;
}

#botonExcelCabecera {
	display: inline;
}

#botonExcelCabecera .dt-buttons {
	display: inline;
}

/*sdsdds*/
.checkbox label:after {
  content: '';
  display: table;
  clear: both;
}

.checkbox .cr {
  position: relative;
  display: inline-block;
  border: 1px solid #a9a9a9;
  border-radius: .25em;
  width: 1.3em;
  height: 1.3em;
  float: left;
  margin-right: .5em;
}

.checkbox .cr .cr-icon {
  position: absolute;
  font-size: .8em;
  line-height: 0;
  top: 50%;
  left: 15%;
}

.checkbox label input[type="checkbox"] {
  display: none;
}

.checkbox label input[type="checkbox"]+.cr>.cr-icon {
  opacity: 0;
}

.checkbox label input[type="checkbox"]:checked+.cr>.cr-icon {
  opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled+.cr {
  opacity: .5;
}

	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}

	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

	.rojo{
		color: red;
		font-weight: 700;
	}

	.verde{
		color: green;
		font-weight: 700;
	}
	
	.bgBlack,.bgGreen,.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
	}	

	.bgBlack {
    color: #000;
    border: 1px solid black;
	}

	.bgGreen {
    color: #65c758;
    border: 1px solid #65c758;
	}

	.bgRed {
    color: rgb(185, 39, 39);
    border: 1px solid rgb(185, 39, 39);
	}



</style>
<section id="base-style">
	<div class="card-content">
		<div class="card" style="border-radius: 14px;">
			<div class="card-header" style="border-radius: 15px;">
				<h4 class="card-title">Reporte de Liquidaciones</h4>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-content collapse show" aria-expanded="true">
				<div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">
					<div class="card-body">

						<ul class="nav nav-tabs nav-underline" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="baseIcon-tab21" data-toggle="tab"
									aria-controls="tabIcon21" href="#cabecera" role="tab" aria-selected="true"><i
										class="fa fa-play"></i> Reporte Liquidaciones</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22"
									href="#detalle" role="tab" aria-selected="false"><i class="fa fa-flag"></i> Reporte
									Liquidaciones Detalle</a>
							</li>
						</ul>

						<div class="tab-content">

							<div class="tab-pane active mt-1" id="cabecera" role="tabpanel">

								<form id="verLiquidaciones" autocomplete="off">
									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label>Vendedor</label>
												<select class="form-control input-sm select2" name="vendedor"
													id="vendedor" style="width:100%;">
													{{-- @if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2) --}}
														<option value="1">Todos</option>
													{{-- @endif --}}
													@foreach($vendedores as $key=>$vendedor)
													<option value="{{$vendedor->id}}">{{$vendedor->documento_identidad}} - {{$vendedor->nombre}}
														{{$vendedor->apellido}} - {{$vendedor->empresa}}
													</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Agencia</label>
												<select class="form-control input-sm select2" name="agencia"
													id="agencia" style="width:100%;">
													<option value="1">Todos</option>
													@foreach($agencias as $agencia)
													<option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Periodo</label>
												<select class="form-control input-sm select2" name="periodo"
													id="periodo" style="width:100%;">
													@foreach($listadoFechas as $key=>$listadoFecha)
													<option value="{{$listadoFecha}}">{{$listadoFecha}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Alcanzo la meta</label>
												<select class="form-control input-sm select2" name="alcanzo_meta"
													id="alcanzo_meta" style="width:100%;">
													<option value="">TODOS</option>
													<option value="SI">SI</option>
													<option value="NO">NO</option>
												</select>
											</div>
										</div>
									</div>
								</form>

								<div class="row">
									<div class="col-12">
										<div class="pull-right mr-1" id="botonExcelCabecera"></div>
										<button type="button" onclick="limpiarFiltros()" id="btnLimpiar"
											class="pull-right btn  btn-light mr-1 btn-lg font-weight-bold text-white">Limpiar</button>
										<a onclick="consultarLiquidaciones()"
											class="pull-right  btn btn-info mr-1 btn-lg font-weight-bold text-white"
											role="button">Buscar</a>

									</div>
								</div>

								<div class="table-responsive table-bordered">
									<table id="listado" class="table" style="width: 100%, font-size: 0.9rem;">
										<thead>
											<tr>
												<th>ID</th>
												<th>Periodo Liquidación</th>
												<th>Email <br> Enviado</th>
												<th>Fecha Liquidación</th>
												<th>Vendedor</th>
												<th>Agencia</th>
												<th>Factura LC</th>
												<th>Meta <br>Puntos</th>
												<th>Puntos <br> Alcanzados</th>
												<th>Total <br>Comision <br> USD</th>
												<th>Total <br>Pendiente <br> USD</th>
												<th>Total <br>a Cobrar <br> USD</th>
												<th>Llego a la Meta</th>
												<th style="width: 7%;"></th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="detalle" role="tabpanel">

								<form id="verLiquidacionesDetalle" class="mt-1">
									<input type="hidden" class="Requerido form-control" id="vendedorId"
										name="vendedorId" />
									<div class="row">
										<input type="hidden" class="form-control" id="renta_total" name="renta_total"
											value="0" disabled="disabled">
										<input type="hidden" class="Requerido form-control" id="id_liquidacion"
											name="id_liquidacion" />
										<div class="col-md-3">
											<div class="form-group">
												<label>Vendedor</label>
												<select class="form-control input-sm select2" name="vendedorDetalle"
													id="vendedorDetalle">
													@foreach($vendedores as $key=>$vendedor)
													<option value="{{$vendedor->id}}">{{$vendedor->nombre}}
														{{$vendedor->apellido}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Periodo</label>
												<select class="form-control input-sm select2" name="periodoDetalle"
													id="periodoDetalle">
													@foreach($listadoFechas as $key=>$listadoFecha)
													<option value="{{$listadoFecha}}">{{$listadoFecha}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Comision Total</label>
												<input type="text" class="form-control" id="comision_total"
													name="comision_total" value="0" disabled="disabled">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Comision a Cobrar</label>
												<input type="text" class="form-control" id="comision_a_cobrar"
													name="comision_a_cobrar" value="0" disabled="disabled">
											</div>
										</div>

										<div class="col-md-3">
											<div class="form-group">
												<label>Total Facturado Neto</label>
												<input type="text" class="form-control" id="total_neto_factura"
													name="total_neto_factura" value="0" disabled="disabled">
											</div>
										</div>
									
									</div>
									<div class="row">
									
								</form>
								<div class="col-md-3">
									<br>
									<button type="button" onclick="limpiarFiltrosDetalle()" id="btnLimpiar"
										class="pull-right btn btn-light btn-lg mr-1 text-white"><b>Limpiar</b></button>
									<div class="pull-right mr-1" id="botonExcelDetalle"></div>
									<button onclick="consultarDetalleLiquidaciones()"
										class="pull-right  btn btn-info btn-lg  mr-1"
										type="button"><b>Buscar</b></button>
								</div>
							</div>
							<div class="table-responsive table-bordered">
								<table id="listadoDetalle" class="table" style="width: 100%">
									<thead style="text-align: center">
										<tr>
											<th>Nro Factura</th>
											<th>Tipo</th>
											<th>Fecha Factura</th>
											<th>Total Neto</th>
											<th>Moneda</th>
											<th>Estado <br> Cobro</th>
											<th>Promoción</th>
											<th>Puntos</th>
											<th>Monto Comisión</th>
											<th>Saldo <br> Anterior</th>
										</tr>
									</thead>
									<tbody style="text-align: center">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--======================================================
        MODAL DE FACTURA LIBRO COMPRA
========================================================== -->
    <!-- /.content -->
    <div id="modalFacturas" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 100%;margin-top: 5%;margin-left: 4%;">
        <!-- Modal content-->
            <div class="modal-content" style="width: 160%;">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Facturas Libro Compra</h2>
					<button type="button" class="closeModal"  data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <div class="modal-body" style="padding-top: 0px;">
					<form id="aplicarFactura">
						<div class="row">
								<div class="col-12 col-sm-6 col-md-3">
									<div class="form-group mt-2">
										<label>Vendedor</label>
										<select class="form-control input-sm" name="idProveedor" id="idProveedorFactura" style="width:100%;" tabindex="1">
											<option value="">TODOS</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-sm-6 col-md-2">  
									<div class="form-group mt-2">
									   <label>Fecha Desde/Hasta Factura</label>						 
									   <div class="input-group">
										   <div class="input-group-prepend" style="">
											   <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
										   </div>
										   <input type="text" class="form-control pull-right fecha" name="proveedor_desde_hasta" tabindex="2" id="proveedor_desde_hasta" value="">
									   </div>
								   </div>	
							   </div> 

							   <div class="col-12 col-sm-6 col-md-2">  
										<div class="form-group mt-2">
										<label>Nro Documento</label>						 
											<input type="text" class="form-control pull-right fecha" name="numero_documento" tabindex="3" id="numero_documento" value="">
									</div>	
								</div> 

								<div class="col-12 col-sm-6 col-md-2">  
										<div class="form-group mt-2">
										<label>Nro LC</label>						 
											<input type="text" class="form-control pull-right fecha" name="numero_lc" tabindex="4" id="numero_lc" value="">
									</div>	
								</div> 

							   	<div class="col-12 col-sm-6 col-md-2">  
									<div class="form-group mt-3">
									   <label></label>						 
									   <a type="button" onclick="consultarGetFacturas()" class="btn btn-lg btn-info" id="buscarFactura">Buscar</a>
								   </div>	
							   </div> 
						</div>
						<div class="row">
							<div class="col-12">
								<div class="table-responsive table-bordered font-weight-bold">
									<table id="listadoFacturaLc" class="table" style="width: 100%;font-size: small;">
										<thead>
											<tr>
												<th class="text-center">LC</th>
												<th class="text-center">Proveedor</th>
												<th class="text-center">Documento</th>
												<th class="text-center">Ruc</th>
												<th class="text-center">Fecha</th>
												<th class="text-center">Total</th>
												<th></th>
											</tr>
										</thead>
										<tbody style="text-align: center">
		
										</tbody>
									</table>
								</div>
							</div>
						</div>
					
						<input type="hidden" name="id_liquidacion" id="id_liquidacion_modal">
					</form>	
                </div>
            </div>  
        </div>  
    </div>  

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script >
	var tableLiquidaciones;
var tableLiquidacionesDetalle;

$(document).ready(function () {
	$('.select2').select2();
	var periodo = '{{$periodo}}';

	if (periodo != '') {
		$('#periodo').val(periodo).trigger('change.select2');
	}

	//	tableLiquidaciones = consultarLiquidaciones();
	//	tableLiquidacionesDetalle = consultarDetalleLiquidaciones();

	
	$("#idProveedorFactura").select2({
					language: lang_es_select2,
					ajax: {
							url: "{{route('get.vendedores.agencia')}}",
							dataType: 'json',
							placeholder: "TODOS",
							delay: 0,
							data: function (params) {
										return {
											q: params.term, // search term
											page: params.page
												};
							},
							cache: true
							},
							escapeMarkup: function (markup) {
											return markup;
							}, // let our custom formatter work
							minimumInputLength: 2,
	});
	
});

const formatter = new Intl.NumberFormat('de-DE', {
	currency: 'USD',
	minimumFractionDigits: 2
});
const formatos = new Intl.NumberFormat('de-DE', {
	currency: 'PYG',
	minimumFractionDigits: 0
});




$('.checkCol').on('click', function () {
	var form = $('#columLiqExcel').serializeArray();
	valor = [];
	// valor.push(0,1,2,8);
	valor.push(0, 1, 2, 3, 4, 6, 7, 11);
	$.each(form, function (item, value) {
		valor.push(parseInt(value.value));
	});

	buttonsLiq(valor, tableLiquidacionesDetalle);
});
if ('{{$indicador}}' == 1) {
	periodo = '{{$periodo}}';
	id_liquidacion = parseInt('{{$idliquidacion}}');
	id_vendedor = parseInt('{{$id_vendedor}}');
	cargarReporteLiquidacionDetalle(id_liquidacion, id_vendedor, periodo, null, null)
}

function cargarReporteLiquidacionDetalle(id_liquidacion, id_vendedor, periodo, comision_total, comision_a_cobrar, total_neto_factura) {
	$('#vendedorDetalle').val(id_vendedor).trigger('change.select2');
	$('#vendedorId').val(id_vendedor);
	$('#periodoDetalle').val(periodo).trigger('change.select2');

	$('#comision_total').val(comision_total);
	$('#comision_a_cobrar').val(comision_a_cobrar);
	$('#total_neto_factura').val(total_neto_factura);
	
	$('#id_liquidacion').val(id_liquidacion);
	$('#tableLiquidacionesDetalle').tab('show');
	$('#baseIcon-tab22').trigger('click');
	consultarDetalleLiquidaciones();

}

function limpiarFiltrosDetalle() {
	$('#periodoDetalle').val('').trigger('change.select2');
	$('#vendedorDetalle').val('').trigger('change.select2');
	// consultarDetalleLiquidaciones();
}

function limpiarFiltros() {
	// $('#periodo').val('').trigger('change.select2');
	$('#vendedor').val('1').trigger('change.select2');
	// consultarLiquidaciones();
}

function formatearFecha(texto) {
	if (texto != '' && texto != null) {
		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
	} else {
		return '';
	}
}


function consultarLiquidaciones() {

	$("#com_total_usd").val(0);
	

	var tableLiquidaciones = $("#listado").DataTable({
		"destroy": true,
		"ajax": {
			"url": "{{route('liquidar_dtp_puntos.cabecera')}}",
			"type": "GET",
			"data": {
				"formSearch": $('#verLiquidaciones').serializeArray()
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});

			}
		},
		"columns": [{
				"data": "id"
			},


			{
				"data": function (x) {
					var str = x.mes_anho;
					var month = str.substring(0, 2);
					var year = str.substring(2, 6);
					return month + '/' + year;
				}
			},
			{
				"data": function (x) {
					return x.notificado ? 'SI' : 'NO';
				}
			},
			{
				"data": function (x) {
					var fecha = x.fecha_hora.split(' ');
					var f = formatearFecha(fecha[0]);
					return f + ' ' + fecha[1];
				}
			},
			{
				"data": function (x) {
					return x.nombre_completo;
				}
			},
			{
				"data": function (x) {
					return x.agencia;
				}
			},
			{
				"data": function (x) {
					btn = '';
					
					if (x.id_libros_compras) {
						color_class = x.id_estado_op == 53 ? 'bgGreen' : 'bgRed'; 
						btn = `<a href="{{route('verLibroCompra',['id'=>''])}}/${x.id_libros_compras}" class="${color_class}"><i class="fa fa-fw fa-search"></i>${x.id_libros_compras}</a>`;
					}


					return btn;
				}
			},
			{
				"data": function (x) {
					return formatter.format(x.meta);
				}
			},
			{
				"data": function (x) {
					return formatter.format(x.total_puntos_alcanzados);
				}
			},
			{
				"data": function (x) {
					return formatter.format(x.total_facturado);
				}
			},
			{
				"data": function (x) {
					return formatter.format(x.saldo_pendiente);
				}
			},
			{
				"data": function (x) {
						return formatter.format(x.total_cobrado);
				}
			},
			{
				"data": function (x) {
					return x.meta_cumplida ? 'SI' : 'NO';
				}
			},
			{
				"data": function (x) {
					var periodo = $("#periodo").val();
					// var renta_total = formatter.format(parseFloat(x.total_facturado));
					var btn = `<div>`;
					if (x.total_puntos_alcanzados > 0.0 | x.saldo_pendiente > 0.0) {
						btn += `<a onclick="cargarReporteLiquidacionDetalle(${x.id}, ${x.id_vendedor}, '${periodo}', '${x.total_facturado}' , '${x.total_cobrado}', '${x.total_neto_facturas}')" class="btn btn-info" style="padding-left: 6px;padding-right: 6px;color:#fff;margin-right: 10px;" role="button"><i class="fa fa-fw fa-search"></i></a>`;
						btn += `<a onclick="descargarVoucher(${x.id})" class="btn btn-info" style="padding-left: 6px;padding-right: 6px;color:#fff;margin-right: 10px;" role="button"><i class="fa fa-file-pdf-o"></i></a>`;

					//Si ya tiene libro compra no volvemos  a asignar
						if(x.total_cobrado > 0.0){
							btn += `<a onclick="pagarComision(${x.id_vendedor},${x.id}, '${x.nombre_completo}')" class="btn btn-success" style="padding-left: 6px;padding-right: 6px;color:#fff;margin-right: 10px;" role="button"><i class="fa fa-check-circle-o" aria-hidden="true"></i></a>`;
						}
						
					}

					btn += '</div>';
					return btn;
				}
			},

		]

	});

	generarExcel([0, 1, 2, 3,4,6,7,8,9,10,11], tableLiquidaciones);
	return tableLiquidaciones;
};

function consultarDetalleLiquidaciones() {
	var tableLiquidacionesDetalle = $("#listadoDetalle").DataTable({
		"destroy": true,
		"ajax": {
			"url": "{{route('liquidar_dtp_puntos.detalle')}}",
			"type": "GET",
			"data": {
				"formSearch": $('#verLiquidacionesDetalle').serializeArray()
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});

			}
		},

		"columns": [{
				"data": function (x) {
					btn = ``;
					if(x.tipo == 'FACTURA'){
					btn = `<a href="{{route('verFactura',['id'=>''])}}/${x.id_factura}"  class="bgBlack">
												<i class="fa fa-fw fa-search"></i><span>${x.nro_factura}</span></a><div style="display:none;">${x.id_factura}</div>`;
					} else {
						btn = `<a href="{{route('verNota',['id'=>''])}}/${x.id_factura}"  class="bgBlack">
												<i class="fa fa-fw fa-search"></i><span>${x.nro_factura}</span></a><div style="display:none;">${x.id_factura}</div>`;
					}	

					return btn;
				}
			},
			{ "data": function(x)
				{
				
					if(x.tipo == 'FACTURA'){
						return 'Factura';
					} else {
						return 'Nota Credito';
					}
				} 
			},
			{
				"data": function (x) {
					fecha = x.fecha_hora_facturacion.split(' ');
					f = formatearFecha(fecha[0]);
					return f;
				}
			},
			{
				"data": (x) => {
					return formatter.format(x.total_neto_factura);
				}
			},
			{
				"data": function (x) {
					if (x.id_moneda_venta == 143) {
						return 'USD';
					} else if (x.id_moneda_venta == 111) {
						return 'PYG';
					} else if (x.id_moneda_venta == 43) {
						return 'EUR';
					}


				}
			},
			{
				"data": function(x)
						{
							if(x.cobrado){
								return 'COBRADO';
							} else {
								return 'PENDIENTE';
							}
						} 
			},
			{
				"data": "promocion"
			},
			{
				"data": function(x)
						{
							if(x.tipo == 'FACTURA'){
								return ''+x.puntos;
							} else {
								return '-'+x.puntos;
							}
						} 
			},
			{
				"data": (x) => {
					return formatter.format(x.monto);
				}
			},
			{
				"data": (x) => {
					if(x.saldo){
						return 'SI';
					} else {
						return 'NO';
					}
				}
			},



		],
	});

	generarExcelDetalle([0, 1, 2, 3, 4, 5,6,7,8,9], tableLiquidacionesDetalle);
	return tableLiquidacionesDetalle;
}

function anularLiquidacion(id) {
	return swal({
		title: "GESTUR",
		text: "¿Está seguro que desea anular esta Liquidacion de Comisiones?",
		showCancelButton: true,
		buttons: {
			cancel: {
				text: "No",
				value: null,
				visible: true,
				className: "btn-warning",
				closeModal: false,
			},
			confirm: {
				text: "Sí, Anular",
				value: true,
				visible: true,
				className: "",
				closeModal: false
			}
		}
	}).then(isConfirm => {
		if (isConfirm) {
			$.ajax({
				type: "GET",
				url: "{{route('anularLiquidacion')}}",
				dataType: 'json',
				data: {
					id_liquidacion: id,
				},
				error: function (jqXHR, textStatus, errorThrown) {
					$.toast({
						heading: 'Error',
						text: 'Ocurrió un error al intentar eliminar la Liquidacion.',
						position: 'top-right',
						showHideTransition: 'fade',
						icon: 'error'
					});
				},
				success: function (rsp) {
					if (rsp.status == "OK") {
						swal("Éxito", rsp.mensaje, "success");
						location.reload();
					} else {
						swal("Cancelado", rsp.mensaje, "error");
					}
				}
			})
		} else {
			swal("Cancelado", "", "error");
		}
	})
}

function generarExcelDetalle(column, tableLiq) {
	$('#botonExcelDetalle').html('');
	var buttons = new $.fn.dataTable.Buttons(tableLiq, {
		buttons: [{
			title: 'Reporte Liquidación de Comisiones Detalle',
			extend: 'excelHtml5',
			text: '<b>Excel</b>',
			className: 'pull-right text-center btn btn-success btn-lg',
			exportOptions: {
				columns: ':visible'
			},
			exportOptions: {
				columns: column,
				format: {
					body: function (data, row, column, node) {
						if (column != 0 && column != 1 && column != 2 && column != 4  && column != 5  && column != 6  && column != 9) {
							if (data != '0') {
								data = data.replace(".", "");
								//cambiamos la cpunto
								data = data.replace(",", ".");

							}
							var numFinal = parseFloat(data);
							return numFinal;
						} else if(column == 0){
							return $(data).find('span').html();
						
						}
						return data;
					}
				}
			},

			//Generamos un nombre con fecha actual

			filename: function () {
				var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
				return 'ReporteLiquidacionComisionesDetalle-' + date_edition;
			}
		}]
	}).container().appendTo('#botonExcelDetalle');
}

function generarExcel(column, table) {
	$('#botonExcelCabecera').html('');
	var buttons = new $.fn.dataTable.Buttons(table, {
		buttons: [{
			title: 'Reporte Liquidación de Comisiones',
			extend: 'excelHtml5',
			text: '<b>Excel</b>',
			className: 'pull-right text-center btn btn-success btn-lg',
			exportOptions: {
				columns: ':visible'
			},
			exportOptions: {
				columns: column,
				format: {
					//seleccionamos las columnas para dar formato para el excel
					body: function (data, row, column, node) {
						if (column != 0 && column != 1 && column != 2 && column != 3 && column != 4 && column != 5 && column != 9 && column != 10) {
					
							if (data != '0') {
								data = data.replace(".", "");
								//cambiamos la cpunto
								data = data.replace(",", ".");

							}
							var numFinal = parseFloat(data);
							return numFinal;
						}
						return data;
					}
				}
			},

			//Generamos un nombre con fecha actual

			filename: function () {
				var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
				return 'ReporteLiquidacionComisiones-' + date_edition;
			}
		}]
	}).container().appendTo('#botonExcelCabecera');
}
$('#comision_total').val(0);
$('.numeric').inputmask("numeric", {
	radixPoint: ",",
	groupSeparator: ".",
	digits: 2,
	autoGroup: true,
	// prefix: '$', //No Space, this will truncate the first character
	rightAlign: false,
	oncleared: function () {}
});

function clean_num(n, bd = false) {
	if (n && bd == false) {
		n = n.replace(/[,.]/g, function (m) {
			if (m === '.') {
				return '';
			}
			if (m === ',') {
				return '.';
			}
		});
		return Number(n);
	}
	if (bd) {
		return Number(n);
	}
	return 0;
}

function formatNumber(num) {
	if (!num || num == 'NaN') return '-';
	if (num == 'Infinity') return '&#x221e;';
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
		num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
	return (((sign) ? '' : '-') + num + ',' + cents);
}

/*
 * Levantar modal para seleccionar factura
 */
function pagarComision(id_vendedor, id_liquidacion, text) {
	//Asignar el selector al vendedor actual de la linea
	let newOption = new Option(text, id_vendedor, false, false);
	$('#idProveedorFactura').append(newOption);
	$('#idProveedorFactura').val(id_vendedor).trigger('change.select2');

	$('#id_liquidacion_modal').val(id_liquidacion);
	consultarGetFacturas();
	$("#modalFacturas").modal("show");
}

function consultarGetFacturas() {

	var tableLiquidacionesDetalle = $("#listadoFacturaLc").DataTable({

		"destroy": true,
		"ajax": {
			"url": "{{route('liquidar_dtp_puntos.get_facturas_aplicar')}}",
			"type": "GET",
			"data": {
				"formSearch": $('#aplicarFactura').serializeArray()
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});
			}
		},

		"columns": [
			{
				"data": "id"
			},
			{
				"data": "proveedor"
			},
			{
				"data": "nro_documento"
			},
			{
				"data": "ruc_proveedor"
			},
			{
				"data": "fecha_documento"
			},
			{
				"data": (x) => {
					return formatter.format(parseFloat(x.total));
				}
			},
			{
				"data": (x) => {
					btn = `<a onclick="asignarFactura(${x.id})" 
									  class="btn btn-info" 
									  style="padding-left: 6px;padding-right: 6px;color:#fff;margin-right: 10px;" 
									  title = "Asignar Factura"
									  role="button"><i class="fa fa-magnet" aria-hidden="true"></i></a>`;

					return btn;
				}
			},
		],
	});
}



let locale = {
	format: 'DD/MM/YYYY',
	cancelLabel: 'Limpiar',
	applyLabel: 'Aplicar',
	fromLabel: 'Desde',
	toLabel: 'Hasta',
	customRangeLabel: 'Seleccionar rango',
	daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
	monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
		'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
		'Diciembre'
	]
};

$('input[name="proveedor_desde_hasta"]').daterangepicker({
	autoUpdateInput: false,
	locale: locale
});

$('input[name="proveedor_desde_hasta"]').on('apply.daterangepicker', function (ev, picker) {
	$(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
});

$('input[name="proveedor_desde_hasta"]').on('cancel.daterangepicker', function (ev, picker) {
	$(this).val('');
});


function asignarFactura(id_libro_compra) {


	$.ajax({
		type: "GET",
		url: "{{route('liquidar_dtp_puntos.asignarFacturaDtpPuntos')}}",
		data: {
			id_libro_compra: id_libro_compra,
			id_liquidacion: $('#id_liquidacion_modal').val()
		},
		dataType: 'json',
		error: function (jqXHR, textStatus, errorThrown) {
			$.toast({
				heading: 'Error',
				text: 'Ocurrió un error en la comunicación con el servidor.',
				position: 'top-right',
				showHideTransition: 'fade',
				icon: 'error'
			});
		},
		success: function (rsp) {

			if (rsp.err) {
				$.toast({
					heading: 'Error',
					text: rsp.msg,
					showHideTransition: 'fade',
					position: 'top-right',
					icon: 'error'
				});
			} else {
				$.toast({
					heading: 'Exito',
					text: rsp.msg,
					position: 'top-right',
					showHideTransition: 'slide',
					icon: 'success'
				});

				$("#modalFacturas").modal("hide");
				$('#id_liquidacion_modal').val();
				consultarLiquidaciones();
			}

		}
	});


}

			var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			}


	function descargarVoucher(id_liquidacion){
		var urlVoucher = "{{route('liquidar_dtp_puntos.reporte_puntos.voucherpdf')}}?id_liquidacion="+id_liquidacion;
		window.open(urlVoucher, '_blank');
	}


</script>
@endsection
