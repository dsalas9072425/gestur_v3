
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')



<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Editar Parametros</h4>
			<small>Solo es posible editar los parametros que estan permitidos.</small>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">

			<div class="card-body">

                <div class="card-body pt-0">
                    <a class="btn btn-success pull-right" href="{{ url('addParametros')}}">
                        <div class="fonticon-wrap">
                            <i class="ft-plus-circle"></i>
                        </div>
					</a>
                </div>

				<form class="row" id="form_1">

                    @foreach ($param as $parametro)

                    <div class="col-12 col-sm-6 col-md-3">
						<div class="form-group">
                          <label>{{$parametro->descripcion}}</label>
                          <input type="text" class="form-control modalData" maxlength="50" name="{{$parametro->id}}"  value="{{$parametro->valor}}">
						</div>
                    </div>
                        
                    @endforeach

				</form>

				<div class="row">
					<div class="col-12 mb-1">
						<button type="button" class="btn btn-success pull-right btn-lg mr-1" onclick="setDataInfo()"><b>GUARDAR</b></button>
					</div>
				</div>
            


			</div>
		

	





		</div>
	</div>
</section>



	

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>
    function setDataInfo()
    {
      return new Promise((resolve, reject) => {   
        let dataString =  $('#form_1').serializeJSON({
            customTypes: customTypesSerializeJSON
        });
        $.ajax({
					type: "GET",
					url: "{{ route('setDataParametro') }}",
					dataType: 'json',
					data: dataString,

					error: function(jqXHR, textStatus, errorThrown)
                    {
						$.toast({
							heading: 'Error',
							text: 'Ocurrio un error en la comunicación con el servidor.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});
					},
					success: function(rsp)
                    {  
						if(rsp.estado == 1){
								$.toast({
								heading: 'Actualizado con exito.',
								text: 'El parametro fue actualizado con exito.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'success'
								});
							}else{		
								$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la actualizacion.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
									});
							}
                        
					}
				});
       });      
    }
	</script>
@endsection