
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')



<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Configuración Empresa</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">

			<div class="card-body">
				<form id="formularioConfiguracion">
                    <div class="row">
                        <div class="col-12 col-sm-3 col-md-3"></div>
			            <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Empresa:</label>
								<select class="form-control select2" name="id_empresa"  id="id_empresa" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($empresas as $empresa)
										<option value="{{$empresa->id}}">{{$empresa->denominacion}}</option> 
									@endforeach
								</select>
					        </div>
			            </div>
                        <div class="col-12 col-sm-3 col-md-3"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Tipo de Impresión</label>
								<select class="form-control select2" name="tipo_impresion"  id="tipo_impresion" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									<option value="1">Duplicado 1 Hoja</option>
									<option value="2">Tiplicado 3 Hojas</option>
									<option value="3">Tiplicado 3 Hojas</option>
									<option value="4">Tiplicado 3 Hojas</option>
								</select>
					        </div>
			            </div>
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group"> 
					            <label>Tipo de Calculo Vencimiento</label>
								<select class="form-control select2" name="tipo_calculo_vencimiento"  id="tipo_calculo_vencimiento" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									<option value="E">Fecha de Emision de Factura</option> 
									<option value="G">Fecha de Gasto</option> 
                                </select>
					        </div>
			            </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">  
					            <label>Tipo de Empresa</label>
								<select class="form-control select2" name="tipo_empresa"  id="tipo_empresa" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									<option value="D">DTP</option>
									<option value="A">AGENCIA</option>
									<option value="V">VENTA</option>
								</select>
					        </div>
			            </div>
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Es Exportador</label>
								<select class="form-control select2" name="es_exportador"  id="es_exportador" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
                                    <option value="1">SI</option>
                                    <option value="0">NO</option>
								</select>
					        </div>
			            </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Plan del Sistema</label>
								<select class="form-control select2" name="id_plan_sistema"  id="id_plan_sistema" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($planes as $plan)
										<option value="{{$plan->id}}">{{$plan->descripcion}}</option> 
									@endforeach
								</select>
					        </div>
			            </div>
                        <div class="col-12 col-sm-6 col-md-6"> 
					        <div class="form-group">
					            <label>Agente Retentor</label>
								<select class="form-control select2" name="agente_retentor"  id="agente_retentor" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									<option value="1">SI</option>
									<option value="0">NO</option>
                                </select>
					        </div>
			            </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Control de Rentabilidad</label>
								<select class="form-control select2" name="control_de_rentabilidad"  id="control_de_rentabilidad" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									<option value="I">Ítem (Se controla ítem por Ítem)</option>
									<option value="V">Venta (Se controla por Venta)</option>
                                </select>
					        </div>
			            </div>
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Imprimir Detalle Factura</label>
								<select class="form-control select2" name="imprimir_detalle_factura"  id="imprimir_detalle_factura" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
                                    <option value="1">SI</option>
									<option value="0">NO</option>
								</select>
					        </div>
			            </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Representante</label>
                                <input type="text" class="form-control format-number-control" id="representante" name="representante" data-value-type="convertNumber">
					        </div>
			            </div>
                        <div class="col-12 col-sm-6 col-md-6"> 
					        <div class="form-group">
					            <label>Ruc Representante</label>
                                <input type="text" class="form-control format-number-control" id="ruc_representante" name="ruc_representante" data-value-type="convertNumber" >
					        </div>
			            </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Dias de Vencimiento</label>
                                <input type="text" class="form-control format-number-control" id="dias_vencimiento" name="dias_vencimiento" data-value-type="convertNumber" tabindex="18" value="0">
					        </div>
			            </div>
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Proveedor Ticket</label>
								<select class="form-control select2" name="proveedor_ticket"  id="proveedor_ticket" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($aerolineas as $aerolinea)
										<option value="{{$aerolinea->id}}">{{$aerolinea->nombre}}</option> 
									@endforeach
								</select>
					        </div>
			            </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Markup Minimo de Venta</label>
                                <input type="text" class="form-control format-number-control" id="markup_minimo_venta" name="markup_minimo_venta" data-value-type="convertNumber" value="0">
					        </div>
			            </div>
                        <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Venta Minimo Incentivo</label>
                                <input type="text" class="form-control format-number-control" id="venta_minima_incentivo" name="venta_minima_incentivo" data-value-type="convertNumber" value="100">
					        </div>
			            </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
					        <div class="form-group">
					            <label>Pie de Factura</label>
                                <textarea class="form-control" name="pie_factura_txt"  id="pie_factura_txt" rows="6"></textarea>
					        </div>
			            </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12">
					        <div class="form-group">
					            <label>Cabecera de factura</label>
                                <textarea class="form-control" name="cabecera_factura_txt"  id="cabecera_factura_txt" rows="6"></textarea>
					        </div>
			            </div>
                    </div>
					
				</form>
				<div class="row">
					<div class="col-12 mb-1">
						<button type="button" class="btn btn-success pull-right btn-lg mr-1" onclick="setDataInfo()"><b>GUARDAR</b></button>
					</div>
				</div>
            </div>
		
		</div>
	</div>
</section>



	

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.4.1/tinymce.min.js" integrity="sha512-in/06qQzsmVw+4UashY2Ta0TE3diKAm8D4aquSWAwVwsmm1wLJZnDRiM6e2lWhX+cSqJXWuodoqUq91LlTo1EA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script>
        $('.select2').select2();
        $('#id_empresa').on( 'change',function() {
                if($('#id_empresa').val() != ""){
					tinymce.init({
						selector: 'textarea#cabecera_factura_txt',
						style_formats: [
								{title: 'F-5', block: 'span', classes: 'f-5'},
								{title: 'F-6', block: 'span', classes: 'f-6'},
								{title: 'F-7', block: 'span', classes: 'f-7'},
								{title: 'F-8', block: 'span', classes: 'f-8'},
								{title: 'F-9', block: 'span', classes: 'f-9'},
								{title: 'F-10', block: 'span', classes: 'f-10'},
								{title: 'F-11', block: 'span', classes: 'f-11'},
								{title: 'F-12', block: 'span', classes: 'f-12'},

							],
						});
                    $.ajax({
                            type: "GET",
                            url: "{{ route('getConfiguracionEmpresa') }}",
                            dataType: 'json',
                            data: { idEmpresa: $('#id_empresa').val()},

                            error: function(jqXHR,textStatus,errorThrown){
                            },
                            success: function(rsp){
                                
								if(rsp.agente_retentor == true){
									valor_agente_retentor = '1';
								}else{
									valor_agente_retentor = '0';
								}
								if(rsp.es_exportador == true){
									valor_es_exportador = '1';
								}else{
									valor_es_exportador = '0';	
								}
								if(rsp.imprimir_detalle_factura == true){
									valor_imprimir_detalle_factura = '1';
								}else{
									valor_imprimir_detalle_factura = '0';
								}
                                $('#tipo_impresion').select2().val(rsp.tipo_impresion).trigger("change");
                                $('#tipo_calculo_vencimiento').select2().val(rsp.tipo_calculo_vencimiento).trigger("change");
                                $('#tipo_empresa').select2().val(rsp.tipo_empresa).trigger("change"); 
                                $('#es_exportador').select2().val(valor_es_exportador).trigger("change"); 
                                $('#id_plan_sistema').select2().val(rsp.id_plan_sistema).trigger("change"); 
                                $("#agente_retentor").select2().val(valor_agente_retentor).trigger("change"); 
                                $('#control_de_rentabilidad').select2().val(rsp.control_de_rentabilidad).trigger("change"); 
                                $('#imprimir_detalle_factura').select2().val(valor_imprimir_detalle_factura).trigger("change"); 
                                $('#representante').val(rsp.representante); 
                                $('#ruc_representante').val(rsp.ruc_representante); 
                                $('#dias_vencimiento').val(rsp.dias_vencimiento); 
                                $('#proveedor_ticket').select2().val(rsp.proveedor_ticket).trigger("change"); 
                                $('#markup_minimo_venta').val(rsp.markup_minimo_venta); 
                                $('#venta_minima_incentivo').val(rsp.venta_minima_incentivo); 
                                $('#pie_factura_txt').html(rsp.pie_factura_txt);
								//tinymce.activeEditor.setContent(rsp.cabecera_factura_txt);
								if(rsp.head_factura_txt == null){
									tinymce.get("cabecera_factura_txt").setContent("");
								}else{
									tinymce.get("cabecera_factura_txt").setContent(rsp.head_factura_txt);
								}
								
                            }
                            
                            
                        });
                }        

        });
    
		
        function setDataInfo(){
			//estiramos el dato de la caja de texto e incluimos en el array para guardar los cambios
            var dataString = $("#formularioConfiguracion").serialize()+ tinymce.get("cabecera_factura_txt").getContent();
			$.ajax({
					type: "GET",
					url: "{{route('guardarConfiguracion')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
                            if(rsp.err = 'OK'){
                                clearInfo();
                                $.toast({
                                    heading: 'Exito',
                                    text: 'Se han guardado las Configuraciones.',
                                    showHideTransition: 'fade',
                                    position: 'top-right',
                                    icon: 'success'
                                });

                        }else{
                            $.toast({
                                    heading: 'Error',
                                    text: 'No se han guardado las Configuraciones.',
                                    showHideTransition: 'fade',
                                    position: 'top-right',
                                    icon: 'error'
                                });

                        }


					}
		    });
        }     


        function clearInfo(){
            $('#id_empresa').select2().val('').trigger("change");
            $('#tipo_impresion').select2().val('').trigger("change");
            $('#tipo_calculo_vencimiento').select2().val('').trigger("change");
            $('#tipo_empresa').select2().val('').trigger("change"); 
            $('#es_exportador').select2().val('').trigger("change"); 
            $('#id_plan_sistema').select2().val('').trigger("change"); 
            $("#agente_retentor").select2().val('').trigger("change"); 
            $('#control_de_rentabilidad').select2().val('').trigger("change"); 
            $('#imprimir_detalle_factura').select2().val('').trigger("change"); 
            $('#representante').val(''); 
            $('#ruc_representante').val(''); 
            $('#dias_vencimiento').val(''); 
            $('#proveedor_ticket').select2().val('').trigger("change"); 
            $('#markup_minimo_venta').val(''); 
            $('#venta_minima_incentivo').val(''); 
            $('#pie_factura_txt').val("");
			tinymce.get("cabecera_factura_txt").setContent("");
        }

	</script>
@endsection