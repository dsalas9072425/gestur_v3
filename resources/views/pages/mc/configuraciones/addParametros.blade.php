@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Agregar parametros</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<div class="row">
					<form id="agregarParametros" style="width:100%">
						<div class="col-lg-3 col-sm-6 col-md-3">
							<div class="form-group">
								<label>Clave</label>
								<input type="text" class="form-control" maxlength="50" name="clave"  value="">
							</div>
						</div>
						<div class="col-lg-3 col-sm-6 col-md-3">
							<div class="form-group">
								<label>Valor</label>
								<input type="number" class="form-control" maxlength="50" name="valor"  value="">
							</div>
						</div>
						<div class="col-lg-3 col-sm-6 col-md-3">
							<div class="form-group">
								<label>Descripcion</label>
								<input type="text" class="form-control" maxlength="50" name="descripcion"  value="N/A">
							</div>
						</div>
						<div class="col-lg-3 col-sm-6 col-md-3">
							<div class="form-group">
								<label for="editable">Editable</label>
								<select class="form-control" id="editable" class="editable" name="editable">
								<option value="true">Si</option>
								<option value="false">No</option>
								</select>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<a href="{{url('abmParametros')}}" class="btn btn-secondary pull-left mr-2 text-white" id="volver">Volver</a>
			<a class="btn btn-primary pull-left text-white" id="enviarNuevoParametro">Guardar</a>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$("#enviarNuevoParametro").click(function(e){
			e.preventDefault();
			var parametros = $( "#agregarParametros" ).serialize();
			$.ajax({
						type: "GET",
						url: "{{route('agregarNuevoParametro')}}",
						dataType: 'json',
						data: parametros,

							error: function(jqXHR,textStatus,errorThrown){

							$.toast({
							heading: 'Error',
							text: 'Ocurrio un error en la comunicación con el servidor.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});

							},
						success: function(rsp){
							if(rsp.estado == 1){
								$.toast({
								heading: 'Agregado con exito.',
								text: 'Se creo con exito el nuevo item de menu',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'success'
								});
								$('#agregarParametros')[0].reset();
							}else{		
								$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la insercion.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
									});
							}
						
						}
					});	
			}); 
	</script>
@endsection