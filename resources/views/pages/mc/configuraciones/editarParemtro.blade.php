@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
    <style type="text/css">
    .btn-common {
        width: 150px; /* Ajusta el valor según tus necesidades */
    }
    </style>
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
	@include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Editar Parametro</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 


               

                    <Form action="{{route('parametros.update', $parametro->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
	            	<div class="row"  style="margin-bottom: 1px;">
						<div class="col-xs-12 col-sm-3 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label>Clave</label>					            	
                                <input type="text" id="clave" name="clave" value="{{ $parametro->clave }}" required class="form-control">

                            </div>
						</div>	
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Valor</label>
                                <input type="number" required class = "form-control" name="valor" id="valor" placeholder="Valor" value="{{ $parametro->valor }}" required>
                            </div>
                        </div>

					</div>
                    <div class="row"  style="margin-bottom: 1px;">
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label class="control-label">Descripcion</label>
                                <input type="text" required class = "form-control" name="descripcion" id="descripcion" value="{{ $parametro->descripcion }}" required>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3 col-md-4" >
                            
                        </div>

                    </div>
					
			</div>	
	            	
            <a href="{{ route('parametros.index') }}" class="btn btn-secondary btn-md pull-right mb-1 btn-common" >Cancelar</a>
            <button type="submit" class="btn btn-success btn-md pull-right mb-1 btn-common" tabindex="17"><b>GUARDAR</b></button>
      
				
				</form>	
				
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')

@endsection