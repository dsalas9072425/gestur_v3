
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')



<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Permisos Empresa</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">

			<div class="card-body">
				<form id="formularioConfiguracion">
                    <div class="row">
                        <div class="col-12 col-sm-3 col-md-3"></div>
			            <div class="col-12 col-sm-6 col-md-6">
					        <div class="form-group">
					            <label>Empresa:</label>
								<select class="form-control select2" name="id_empresa"  id="id_empresa" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($empresas as $empresa)
										<option value="{{$empresa->id}}">{{$empresa->denominacion}}</option> 
									@endforeach
								</select>
					        </div>
			            </div>
                        <div class="col-12 col-sm-3 col-md-3"></div>
                    </div>
                    <br>
                    <div class="row">
                        

                    </div>
				</form>
				<div class="row">
					<div class="col-12 mb-1">
						<button type="button" class="btn btn-success pull-right btn-lg mr-1" onclick="setDataInfo()"><b>GUARDAR</b></button>
					</div>
				</div>
            </div>
		
		</div>
	</div>
</section>



	

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>



	</script>
@endsection