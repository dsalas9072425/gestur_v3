
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')



<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Cuenta Contable Empresa</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">

			<div class="card-body">
				<form id="formularioConfiguracion">
                    <div class="row">
						<div class="col-12 col-sm-3 col-md-3">
					        <div class="form-group">
					            <label>Empresa</label>
								<select class="form-control select2" name="id_empresa" id="id_empresa" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($empresas as $empresa)
										<option value="{{$empresa->id}}">{{$empresa->denominacion}}</option> 
									@endforeach
								</select>
					        </div>
			            </div>
						<div class="col-12 col-sm-2 col-md-2">
					        <div class="form-group">
					            <label>Parametro</label>
								<select class="form-control select2" name="parametro"  id="parametro" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($parametros as $key=>$parametro)
										<option value="{{$parametro}}">{{$parametro}}</option> 
									@endforeach
								</select>
					        </div>
			            </div>
						<div class="col-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Cuenta Contable</label>
								<select class="form-control select2" name="cuenta_contable" id="cuenta_contable" data-value-type="number" tabindex="19"  style="width: 100%;">
									<option value="">Seleccione una cuenta</option>
									@foreach ($cuentas_contables as $cuenta)
										@if($cuenta->asentable)
											<optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
										@endif
										<option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
										</option>
										@if($cuenta->asentable)
											</optgroup>
										@endif
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-1 col-md-1">
							<label>Valor</label>
                            <input type="text" class="form-control format-number-control" id="valor" name="valor" data-value-type="convertNumber" value="">
						</div>
						<div class="col-12 col-sm-2 col-md-2">
							<div class="form-group">
								<label>Moneda</label>
								<select class="form-control select2" name="divisa_id" id="divisa_id" style="width: 100%;">
									<option value="">Seleccione Moneda</option>
									@foreach($divisa as $divisas)
									<option value="{{$divisas->currency_id}}">{{$divisas->currency_code}} -
										{{$divisas->hb_desc}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-1 col-md-1">
							<br>
							<button type="button" class="btn btn-success pull-right btn-lg mr-1" onclick="setDataInfo()"><i class="fa fa-pencil-square-o"></i></button>
						</div>
                    </div>
				</form>
				<div class="row">
					<div class="table-responsive mt-1">
						<table id="listado" class="table table-striped table-bordered file-export" style="width: 100%">
							<thead style="text-align: center">
								<tr>
									<th>Empresa</th>
									<th>Parametro</th>
									<th>Cuenta Contable</th>
									<th>Valor</th>
									<th>Moneda</th>
									<th></th>
								</tr>
							</thead>
							<tbody style="text-align: center">
							</tbody>
						</table>
					</div>  
				</div>
            </div>
		
		</div>
	</div>
</section>



	

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>
		$('.select2').select2();
        $('#id_empresa').on( 'change',function() {

			dataString = {
            	id_empresa: $('#id_empresa').val(),
	        };

			$.ajax({
				type: "GET",
				url: "{{route('obtenerPlanCuentas')}}",
				data: dataString,
				dataType: 'json',
				error: function (jqXHR, textStatus, errorThrown) {
					$.toast({
						heading: 'Error',
						text: 'Ocurrió un error en la comunicación con el servidor.',
						position: 'top-right',
						showHideTransition: 'fade',
						icon: 'error'
					});
					$('#btnSave').prop('disabled', false);
				},
				success: function (rsp) {
					console.log(rsp);
					$('#cuenta_contable').empty();
			     	//if (rsp.data.length) {
									$('#cuenta_contable').empty();
									var newOption = new Option('Seleccione Cuenta Contable', '', false, false);
									$('#cuenta_contable').append(newOption);
									$.each(rsp, function (key, item){
										var newOption = new Option(item.cod_txt+' '+item.descripcion, item.id, false, false);
										$('#cuenta_contable').append(newOption)
									});
					//}
				} //success
			}).done(function () {
				// $('#btnSave').prop('disabled',false);
			}); //AJAX

			listado()
		});

		function listado()
		{
			// console.log('mostrarLibroCompra');
			 table = $("#listado").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('gesturCuentaContable')}}",
						"type": "GET",
						"data": {"formSearch": $('#formularioConfiguracion').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					"aaSorting":[[3,"asc"]],
					"columns": 
					[
						{ "data": "empresa" },
						{ "data": function(x){
							var acciones= ""; 
							acciones=`<b>${x.parametro}</b>`;
						    return acciones;               
                		}},

						{ "data": "cuenta_contable" },
						{ "data": "valor" },
						{ "data": "moneda" },
						{ "data": function(x){
							var acciones= ""; 
							acciones=`<button onclick="getAnular(${x.id})" class='btn btn-danger btnEditHidden' type='button'><i class="fa fa-close"></i></button>`
						    return acciones;               
                		}},
					], 
					"initComplete": function (settings, json) {
					 $('[data-toggle="tooltip"]').tooltip();
					 
			 		}

					
				});	
				
				return table;
		}


		function setDataInfo(){
					$.ajax({
                            type: "GET",
                            url: "{{ route('guardarCuentaEmpresa') }}",
                            dataType: 'json',
                            data: { 
									idEmpresa: $('#id_empresa').val(),
									parametro: $('#parametro').val(),
									cuenta_contable: $('#cuenta_contable').val(),
									valor: $('#valor').val(),
									divisa_id: $('#divisa_id').val()
								 },

                            error: function(jqXHR,textStatus,errorThrown){
                            },
                            success: function(rsp){
								if(rsp.err = 'OK'){
										listado();
										$.toast({
											heading: 'Exito',
											text: 'Se han guardado la Cuenta Contable Empresa.',
											showHideTransition: 'fade',
											position: 'top-right',
											icon: 'success'
										});
								}else{
									$.toast({
											heading: 'Error',
											text: 'No se han guardado la Cuenta Contable Empresa.',
											showHideTransition: 'fade',
											position: 'top-right',
											icon: 'error'
										});
								}

                            }
                            
                            
                        });
				}			


			function getAnular(id){

				$.ajax({
                            type: "GET",
                            url: "{{ route('anularCuentaEmpresa') }}",
                            dataType: 'json',
                            data: { 
									cuenta_contable: id
								 },

                            error: function(jqXHR,textStatus,errorThrown){
                            },
                            success: function(rsp){
								if(rsp.err = 'OK'){
										listado();
										$.toast({
											heading: 'Exito',
											text: 'Se ha anulado la Cuenta Contable Empresa.',
											showHideTransition: 'fade',
											position: 'top-right',
											icon: 'success'
										});
								}else{
									$.toast({
											heading: 'Error',
											text: 'No se ha anulado la Cuenta Contable Empresa.',
											showHideTransition: 'fade',
											position: 'top-right',
											icon: 'error'
										});
								}

                            }
                            
                            
                        });
				}			



	</script>
@endsection