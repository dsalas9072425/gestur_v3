
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Listado Parametros</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-md-9">
                    
                        <a href="{{ route('parametros.create') }}" class="btn btn-info">Crear Parametro</a>

                    </div>




                    
                    <div class="col-md-3">
                        <form action="{{ route('parametros.index') }}">
                            <LAbel>Buscar:</LAbel>
                            <input type="search" name="buscar" class="form-control">                            
                        </form>
                    </div>
                </div>


                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>CLAVE</th>
                            <th>VALOR</th>
                            <th>DESCRIPCION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($parametros  as $parametro)

                        <tr>
                            <td>{{ $parametro->id }}</td>
                            <td>{{ $parametro->clave }}</td>
                            <td>{{ $parametro->valor }}</td>
                            <td>{{ $parametro->descripcion }}</td>
                          
                            <td>
                                <a href="{{route('parametros.edit', $parametro->id)}}" class="btn btn-warning">
                                    <span class="fa fa-edit"></span>
                                </a>
                        
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $parametros->links() }}
                TOTAL DATOS: {{ $parametros->total() }}
  

            </div>
        </div>

    </div>
</div>

@endsection