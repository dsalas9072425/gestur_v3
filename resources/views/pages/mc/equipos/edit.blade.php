@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
	border-radius: 14px !important;
	}
	</style>
	
@endsection
@section('content')

<section id="base-style">
	@include('flash::message') 	
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Edita Equipo</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmEquipo" method="post" action="{{route('doEditEquipo')}}">
					<div class="row">
						<div class="col-12 col-sm-1 col-md-1"></div>
						<div class="col-12 col-sm-4 col-md-4">
							<input type="hidden" class="form-control" id="id" tabindex="7" name="id" value="{{$equipo->id}}">
							<div class="form-group">
								<label>Nombre de Equipo</label>
								<input type="text" class="form-control" id="nombre_equipo" tabindex="7" name="nombre_equipo" value="{{$equipo->nombre_equipo}}">
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Persona Responsable</label>
								<select class="form-control select2" name="id_persona_responsable" data-value-type="number" tabindex="1"  id="id_persona_responsable" style="width: 100%;" required>
									<option value="" data-ruc="">Seleccione Persona</option>    
									@foreach ($vendedores as $vendedor)
											@php
												$ruc = $vendedor->documento_identidad;
												if($vendedor->dv){
													$ruc = $ruc."-".$vendedor->dv;
												}
											@endphp
											<option value="{{$vendedor->id}}" data-ruc="{{$ruc}}" >{{$vendedor->full_data}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 20px;">
							<label>Activo</label>					            	
							<select class="form-control input-sm select2" name="activo" id="activo" style="padding-left: 0px;width: 100%;">
								<option value=" ">Seleccione Opcion</option>
								<option value="true">SI</option>
								<option value="false">NO</option>
							</select>
						</div> 
					</div>
					<br>
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12">
							<h4>Vendedores</h4>
						</div>
						<div id="vendedoresDiv" class="col-12 col-sm-12 col-md-12"></div>
						<div class="row col-md-12">
							<div class="col-12 col-sm-1 col-md-1"></div>
							<div class="col-12 col-sm-6 col-md-6" style="padding-right: 0px;">
								<div class="form-group">
									<select class="form-control select2" name="id_vendedor" data-value-type="number" tabindex="1"  id="id_vendedor" style="width: 100%;" required>
										<option value="" data-ruc="">Seleccione Persona</option>    
										@foreach ($vendedores as $vendedor)
											@php
												$ruc = $vendedor->documento_identidad;
												if($vendedor->dv){
													$ruc = $ruc."-".$vendedor->dv;
												}
											@endphp
											<option value="{{$vendedor->id}}" data-ruc="{{$ruc}}" >{{$vendedor->full_data}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-12 col-sm-1 col-md-1" style="padding-left: 30px;">
								<a onclick="guardarVendedor()" title="Guardar Fila" class="btn btn-success" style="margin-left:5px;padding-left: 6px;padding-right: 6px;width: 59.22222px;" role="button"><i class="fa fa-fw fa-plus" style="color: white;"></i></a>							
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 mb-2">
							<!--<a href="{{ route('cotizacionIndex') }}" type="button" class="btn btn-danger btn-lg pull-right mr-1">Volver</a>-->
							<button type="button" onclick="guardarEquipo()" class="btn btn-success btn-lg pull-right mr-1">Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$("#id_persona_responsable").val('{{$equipo->id_persona_responsable}}').trigger('change.select2');
			@foreach ($equipoDetalles as $equipoDetalle)
					para_vendedor = '<div class="row col-md-12 div_vendedor"id= "vendedorFila_{{$equipoDetalle->id}}" style="padding-right: 10px;">'+
							'<div class="col-12 col-sm-1 col-md-1"></div>'+
							'<div class="col-12 col-sm-6 col-md-6" style="padding-right: 0px;padding-left: 0px;">'+
							'<div class="form-group">'+
							'<input type="hidden" class="form-control" name="detalle[{{$equipoDetalle->id}}][id]" value="{{$equipoDetalle->vendedor->id}}">'+
							'<input type="text" class="form-control" name="detalle[{{$equipoDetalle->id}}][vendedor]" value="{{$equipoDetalle->vendedor->nombre}}">'+
							'</div>'+
							'</div>'+
							'<div class="col-12 col-sm-1 col-md-1" style="padding-left: 30px;">'+
							'<a onclick="eliminarVendedor({{$equipoDetalle->id}})" title="Guardar Fila" class="btn btn-danger" style="margin-left:5px;padding-left: 6px;padding-right: 6px;width: 59.22222px;" role="button"><i class="fa fa-fw fa-close" style="color: white;"></i></a>'+							
							'</div>'+
							'</div>';
					$("#vendedoresDiv").append(para_vendedor);
			@endforeach

			activo='{{$equipo->activo}}';

			if(activo == ''){
				base = 'false';
			}else{
				base = 'true';
			}

			$("#activo").val(base).select2();

		});

		function guardarVendedor(){
			id_vendedor = $("#id_vendedor").val();
			text_vendedor = $('#id_vendedor :selected').text();

			console.log(id_vendedor);
			console.log(text_vendedor);
            var sumaVendedor = 0;
            $('.div_vendedor').each(function(){
                sumaVendedor += 1;
            });


			para_vendedor = '<div class="row col-md-12 div_vendedor"id= "vendedorFila_'+sumaVendedor+'" style="padding-right: 10px;">'+
							'<div class="col-12 col-sm-1 col-md-1"></div>'+
							'<div class="col-12 col-sm-6 col-md-6" style="padding-right: 0px;padding-left: 0px;">'+
							'<div class="form-group">'+
							'<input type="hidden" class="form-control" name="detalle['+sumaVendedor+'][id]" value="'+id_vendedor+'">'+
							'<input type="text" class="form-control" name="detalle['+sumaVendedor+'][vendedor]" value="'+text_vendedor+'">'+
							'</div>'+
							'</div>'+
							'<div class="col-12 col-sm-1 col-md-1" style="padding-left: 30px;">'+
							'<a onclick="eliminarVendedor('+sumaVendedor+')" title="Guardar Fila" class="btn btn-danger" style="margin-left:5px;padding-left: 6px;padding-right: 6px;width: 59.22222px;" role="button"><i class="fa fa-fw fa-close" style="color: white;"></i></a>'+							
							'</div>'+
							'</div>';

			$("#vendedoresDiv").append(para_vendedor);
		}


		function eliminarVendedor(id){
			$("#vendedorFila_"+id).remove();
		}


		function guardarEquipo(){
			$("#frmEquipo").submit();
		}

	</script>
@endsection