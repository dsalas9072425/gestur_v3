
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
	</style>
@endsection
@section('content')

<section id="base-style">
@include('flash::message') 
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Equipos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmPromocion" method="post">
					<div class="row">
						<div class="col-12">
							<a href="{{ route('agregarEquipo') }}" class="btn btn-success pull-right" role="button">
								<div class="fonticon-wrap style-icon">
									<i class="ft-plus-circle"></i>
								</div>
							</a>
						</div>
						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Nombre de Equipo</label>
								<input type="text" class="form-control" id="nombre_equipo" tabindex="7" name="nombre_equipo" value="">
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Persona Responsable</label>
								<select class="form-control select2" name="id_persona_responsable" data-value-type="number" tabindex="1"  id="id_persona_responsable" style="width: 100%;" required>
									<option value="" data-ruc="">Seleccione Persona</option>    
									@foreach ($vendedores as $vendedor)
											@php
												$ruc = $vendedor->documento_identidad;
												if($vendedor->dv){
													$ruc = $ruc."-".$vendedor->dv;
												}
											@endphp
											<option value="{{$vendedor->id}}" data-ruc="{{$ruc}}" >{{$vendedor->full_data}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-11 mb-1">
							<button type="button" onclick="buscarPromocion()" id="btnGuardar" class="btn btn-info btn-lg pull-right mb-1">Buscar</button>
						</div>
					</div>
				</form>


				<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr style="text-align: center;">
								<th>Fecha Alta</th>
								<th>Descripción</th>
								<th>Nombre de Responsable</th>
								<th>Activo</th>
								<th style="width: 90px;"></th>
							</tr>
						</thead>
						<tbody id="lista_promocion" style="text-align: center">
						</tbody>
					</table>
				</div>




			</div>
		</div>
	</section>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
		//$(document).ready(function() {

			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

		
		function buscarPromocion() {

			var dataString = $('#frmPromocion').serialize();

			$.ajax({
						type: "GET",
						url: "{{route('getEquipos')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
									heading: 'Error',
									text: 'Ocurrio un error en la comunicación con el servidor.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
                                });

                            },
						success: function(rsp){
							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}
							console.log(rsp);

							$.each(rsp, function (key, item){
								  console.log(item);
								   
								  nombre = item.nombre; 
								if(jQuery.isEmptyObject(item.apellido) != true){

									nombre += item.apellido;
								}
								if(item.activo == ''){
									activo = 'NO';
								}else{
									activo = 'SI';
								}

								var boton = "<a href='editarEquipo/"+item.id+"'class='btn btn-info' style='padding-left: 6px;padding-right: 6px;margin-right: 10px;' role='button'><i class='fa fa-fw fa-edit'></i></a>";										

								boton += `<a onclick="eliminar(`+item.id+`,'`+activo+`')" class='btn btn-danger' style='padding-left: 6px;padding-right: 6px;color: white;' role='button'><i class='fa fa-fw fa-close'></i></a>`;										

								var dataTableRow = [
														item.fecha_hora_creacion_d,
														item.nombre_equipo,
														nombre,
														activo,
														boton
													];
								var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

							});

					


						}//cierreFunc	
				});	
			
		}
				
		function eliminar(id,estado) {
				return swal({
                        title: "GESTUR",
                        text: " ¿Esta seguro que desea inactivar este Equipo?.",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, Inactivar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
											$.ajax({
													type: "GET",
													url: "{{route('deleteEquipo')}}",
													dataType: 'json',
													data: {
															id_equipo: id,
															estado_esquipo:estado 
															},
													error: function(jqXHR,textStatus,errorThrown)
													{
													},
													success: function(rsp)
													{
														if(rsp.status = 'OK'){
															swal("Éxito", "Se ha eliminado el Equipo", "success");
															location.reload();
														}else{
															swal("Cancelado", "No se ha eliminado el Equipo", "error");
														}
													}	
												})
								}else {
                            		swal("Cancelado", "", "error");
                       			}
							});

		}



	</script>
@endsection