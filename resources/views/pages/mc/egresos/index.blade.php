@extends('masters')
@section('title', 'Egresos')
@section('styles')
	@parent
	
	<style>
		.asistencia{
			display: none;
		}
	 	.error{
	 		color:red;
	 	},
	   	.cabeceraClass {
	   		padding: 10px;
	   		min-width: 250px;
	   		background-color:#E8EAEA;
	   		word-break: break-all;
	   		margin: 10px;

	   	},
		.modal-footer {
		    text-align: left;
		} 
		.nav-tabs>li>a {
		    color: #dacdcd;
		} 
		/*.verificarStyle {
			padding:5px 40px;
			margin-top: 5px;
		} */

		.base{
			cursor: pointer;
		}

		.modal-bodyProforma{
			  height:250px;
             overflow:auto;
			}

		.commentNone {
			display: none;
		}
		.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

		.text-bold{
			font-weight: 800;
			font-size: 15px;
		}


    .chat-content {
        text-align: left;
        float: left;       
        position: relative;
        display: block;
        padding: 8px 15px;
        margin: 0 20px 10px 0;
        clear: both;
        color: #404e67;
        background-color: #edeef0;
        border-radius: 4px;



    }
    .chat-content-left {
        text-align: right;
        position: relative;
        display: block;
        float: right;
        padding: 8px 15px;
        margin: 0 20px 10px 0;
        clear: both;
        color: #fff;
        background-color: #00b5b8;
        border-radius: 4px;
    }  
/*=============================================================================*/

.valoracion {
    position: relative;
    overflow: hidden;
    display: inline-block;
}

.valoracion input {
    position: absolute;
    top: -100px;
}


.valoracion label {
    float: right;
    color: #c1b8b8;
    font-size: 30px; 
}

.valoracion label:hover,
.valoracion label:hover ~ label,
.valoracion input:checked ~ label {
    color: #E2D532;
}


/*=============================================================================================*/
	.btnAdjuntar{
			/* background-color: #00a65a !important;
			border-color: #008d4c !important; 
			color:#FFFF !important;*/s
			font-weight:800 !important;  
			padding:10px !important;
	}


	.verificarStyle {
		/* background-color: #00c0ef !important;
		border-color: #00acd6 !important; 
		color:#FFFF !important; */
		font-weight:800 !important; 
		padding:10px !important;
	}
	
	.btnSolicitarVerificacion {
		padding:10px !important;
		/* background: #2d3e52 !important;
		border-color: #2d3e51 !important;
		color:#FFFF !important; */
	}

	.btnModificar {
		/* background: #e2076a !important; 
		border-color: #e25a7d !important;  */
		padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnAnular{
		/* background-color: #dd4b39 !important;
    	border-color: #d73925 !important; */
    	padding:10px !important;
		/* color:#FFFF !important; */
	}

	.btnHand {
		cursor:pointer; 
		cursor: hand;
	}

	.btnHand:hover {
  opacity: 0.8;
}

	#cierre .table tbody, {
	  padding: 0rem 0rem; 
	}

		  
	</style>
		
@endsection
@section('content')
<!-- Main content -->
<section id="base-style">
   	<div class="card-content">
		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Egresos</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
                		<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body rounded-bottom rounded-lg">	
			        <ul class="nav nav-tabs nav-underline" role="tablist">	
						<li class="nav-item active">
			                <a class="nav-link" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#primera_ventana" role="tab" aria-selected="true">
			                	<i class="fa fa-play"></i> Resumen de Egresos Cuentas</a>
			            </li>		            
			            <li class="nav-item">
			                <a class="nav-link" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#cabecera" role="tab" aria-selected="false">
			                	<i class="fa fa-play"></i> Detalle de Egreso Cuenta</a>
			            </li>
			            <li class="nav-item">
			                <a class="nav-link" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#detalle" role="tab" aria-selected="false"><i class="fa fa-flag"></i> Detalle de Egresos</a>
			            </li>
			        </ul>
			        <div class="tab-content px-1 pt-1">
						<div class="tab-pane active" id="primera_ventana" role="tabpanel" aria-labelledby="baseIcon-tab21">
							<div class="row">
								<div class="col-md-12">
									<form id="verCierre">
										<div class="row">
											<div class="form-group">
												<label>Fecha</label>
												<div class="input-group">
													<div class="input-group-prepend">
														<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
													</div>
													<input type="text" class="form-control fecha_filtro" name="fecha" id="" />
												</div>
											</div>

							
										</div>	 
						
									</form>	
									<div class="row">
										<div class="col-md-12">
											<button id="buscarCierre" class="btn btn-info pull-right btn-lg mr-1"><i class="glyphicon glyphicon-refresh"></i><b>Buscar</b></button>
										</div>	
									</div>	
								</div>	
							</div>

							<br>	
							<div class="row">
								<div class="col-md-12"> 
									<table id="cuenta_resumen" class="table">
										<thead>
											<tr>
												<th>Cuenta</th>
												<th>Nro. Cuenta</th>
												<th>Moneda</th>
												<th>Monto</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>	 			            
						</div>
			            <div class="tab-pane" id="cabecera" role="tabpanel" aria-labelledby="baseIcon-tab21">

				        	<br>	
			            	<div class="row">
				        		<div class="col-md-12"> 
				        			<table id="resumenTabla" class="table">
				        				<thead>
				                            <tr>
				                            	<th>Forma de Pago</th>
				                                <th>Moneda</th>
				                                <th>Monto</th>
				                                <th></th>
				                            </tr>
				                        </thead>
				                        <tbody>
				                        </tbody>
				                    </table>
				        		</div>
				        	</div>	 			            
			            </div>
			            <div class="tab-pane" id="detalle" role="tabpanel" aria-labelledby="baseIcon-tab22">
			            	<br>
			            	{{-- <div class="row">
			            		<div class="col-md-10"></div>
				        		<div class="col-md-2">
			            			<button type="button" id="botonExcel" class="btn btn-success btn-lg pull-right mr-1"><b>Excel</b></button> 
			            		</div>
			            	</div>		 --}}
				        	<div class="row">
				        		<div class="col-md-12">
						        	<div class="table-responsive">
				                        <table id="cierre" class="table table-striped table-bordered" style="width: 100%;">
				                            <thead>
				                                 <tr>
				                                    <th>Nro OP</th>
				                                    <th>Moneda</th>
				                                    <th>Monto</th>
				                                    <th>Estado</th>
				                                    <th>Cotizaciòn</th>
				                                </tr>
				                            </thead>
				                            <tbody>
				                            </tbody>
				                        </table>
				                    </div>
				                </div>  
			                </div>
			                <br>
			                <br> 
		        		</div>	
			        </div>    	
				</div>
			</div>
		</div>
	</div>
</section>		
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
<script type="text/javascript" language="javascript" src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    
	<script type="text/javascript">
			$('#resumenTabla').dataTable();
			$('#cierreTabla').dataTable({                 
										language: {
												"decimal": "",
												"emptyTable": "No hay información",
												"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
												"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
												"infoFiltered": "(Filtrado de _MAX_ total entradas)",
												"infoPostFix": "",
												"thousands": ",",
												"lengthMenu": "Mostrar _MENU_ Entradas",
												"loadingRecords": "Cargando...",
												"processing": "Procesando...",
												"search": "Buscar:",
												"zeroRecords": "Sin resultados encontrados",
												"paginate": {
													"first": "Primero",
													"last": "Ultimo",
													"next": "Siguiente",
													"previous": "Anterior"
												}
											}
										});
	

			resumenCuenta();

            $("#buscarCierre").click(function(){
				resumenCuenta();
			});

			function resumenCuenta(){
				
				$('#cuenta_resumen').DataTable({
				        "ajax": {
				            "url": "{{route('egresos.ajax.cuentas')}}",
				            "data": {
										"fecha": $(".fecha_filtro").val()
									},
    						},
    						"destroy": true,
    						"processing": true,
				            "columns": [
								{data: 'cuenta'},
								{data: 'numero_cuenta'},
								{data: 'moneda'},
								{data: function(x){ return new Intl.NumberFormat('de-DE').format(parseFloat(x.total).toFixed(2));  }},
								{data: function(x){
									return '<button  onclick="cargaCierre('+x.id_banco_detalle+', '+x.id_moneda+');" class="btn btn-info"><i class="ft-folder"></i></button>';
								}}
							]
						});
			}

			
			function cargaCierre(id_banco_detalle, id_moneda){
				let fecha = $('.fecha_filtro').val();
				$('a[href="#cabecera"]').click();

				$.ajax({
					type: "GET",
					url: "{{route('egresos.ajax.index')}}",
					dataType: 'json',
                    data: {
						id_banco_detalle,id_moneda,fecha
					},
					success: function(rsp){
                        console.log(rsp);

						var oSettings = $("#resumenTabla").dataTable().fnSettings();
						var iTotalRecords = oSettings.fnRecordsTotal();
						for (i=0;i<=iTotalRecords;i++) {
							$("#resumenTabla").dataTable().fnDeleteRow(0,null,true);
						}

                        let ver_detalle,dataTableRow,newrow,nTr,nTds;
						$.each(rsp, function (key, item){
							ver_detalle ='<button  onclick="resumenDetalleCierre('+item.id_tipo_pago+', '+item.id_moneda+', '+id_banco_detalle+');" class="btn btn-info"><i class="ft-folder"></i></button>';

							dataTableRow = [
												'<b>'+item.forma_pago+'</b>',
												item.currency_code,
												'<b>'+new Intl.NumberFormat('de-DE').format(parseFloat(item.total_pagado).toFixed(2))+'</b>',
												ver_detalle
											];

							newrow = $("#resumenTabla").dataTable().fnAddData(dataTableRow);
							nTr = $("#resumenTabla").dataTable().fnSettings().aoData[newrow[0]].nTr;
							nTds = $('td', nTr);
						});	
					
					},
					error: function(rsp){
						console.log(rsp);
					}
				})	


			}	

			
			function resumenDetalleCierre(id_forma_pago,id_moneda, id_banco_detalle){
	
						var oSettings = $("#cierre").dataTable().fnSettings();
						var iTotalRecords = oSettings.fnRecordsTotal();
						for (i=0;i<=iTotalRecords;i++) {
							$("#cierre").dataTable().fnDeleteRow(0,null,true);
						}
				      $('#cierre').DataTable({
				        "ajax": {
				        	"serverSide": true,
				        	
				            "url": "{{route('egresos.ajax.detalle')}}",
				            "data": {
										"id_banco_detalle" : id_banco_detalle,
        								"id_forma_pago": id_forma_pago,
        								"id_moneda": id_moneda,
        								"fecha": $(".fecha_filtro").val()
									},
    						},
    						"destroy": true,
    						"processing": true,
    						"deferLoading": 57,
				            "columns": [

								            {
								                "className":      'details-control',
								                "orderable":      false,
								                "data":           function(x){
													return `<a href="{{route('controlOp',['id'=>''])}}/${x.id}"  class="bgRed">
															<i class="fa fa-fw fa-search"></i>${x.nro_op}</a>
															<div style="display:none;">${x.nro_op}</div>
															`;
													
												},
								                "defaultContent": "",
								                "className": 'dt-body-right'
								            },
								            { 
								            	"data": "currency_code",
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": function(x){
								            		return new Intl.NumberFormat('de-DE').format(parseFloat(x.total).toFixed(2));
								            	},
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "estado", 
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "cotizacion",
								            	"defaultContent": "",
								            	"className": 'dt-body-right'
								            }
										],
									
										"order": [[1, 'asc']]	
    					})	

					    $('#cierre tbody').on('click', 'td.dt-body-right', function () {
							    var tr = $(this).closest('tr');
							    var row = $('#cierre').DataTable().row(tr);
							     if (row.child.isShown()) {
							            // This row is already open - close it
							            row.child.hide();
							            tr.removeClass('shown');
							        }
							        else {

							            // Open this row
							            row.child(format(row.data())).show();
							            tr.addClass('shown');
							        }
							} )
					    $('a[href="#detalle"]').click();	
					    desplegarTabla($('#cierre').DataTable());
			}

			function format(d) {
				let tabla = '<table cellpadding="6" cellspacing="0" border="0" style="background-color: #eaebee; width: 100%"> ';
					tabla+= '<tr>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px;"><b>Forma de Pago</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px;"><b>Beneficiario</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px;"><b>Banco</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px;"><b>Moneda</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px;"><b>Documento</b></td>';
					tabla+= '<td style="padding-bottom: 0px;padding-top: 0px;"><b>Importe Pago</b></td>';
					tabla+= '</tr>';
					
					let beneficiario = '';

					$.each(d.detalles, function (key, item){

						if(!item.beneficiario_txt)
							item.beneficiario_txt = '';

						if(!item.nro_comprobante)
							item.nro_comprobante = '';
		
						tabla+= '<tr>';	
						tabla+= '<td><b>'+item.forma_pago+'</b></td>';
						tabla+= '<td><b>'+item.beneficiario_txt+'</b></td>';
						tabla+= '<td><b>'+item.banco+'</b></td>';
						tabla+= '<td><b>'+item.currency_code+'</b></td>';
						tabla+= '<td><b>'+item.nro_comprobante+'</b></td>';
						tabla+= '<td><b>'+new Intl.NumberFormat('de-DE').format(parseFloat(item.importe_pago).toFixed(2))+'</b></td>';
						tabla+= '</tr>';
					})	
					tabla+= '</table>';
				    return tabla;  
			}


			function  desplegarTabla(table){
				$('#cierre').on('init.dt', function(e, settings){
					   var api = new $.fn.dataTable.Api( settings );
					   api.rows().every( function () {
					      var tr = $(this.node());
					      this.child(format(this.data())).show();
					      tr.addClass('shown');
					   });
					});
			}

			$( ".fecha_filtro" ).datepicker({ 
				altFormat: 'dd/mm/yy',
				dateFormat: 'yy-mm-dd'
			}).datepicker("setDate", new Date(new Date().getTime() - (24 * 60 * 60 * 1000)));



			$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
				$('#verCierre').attr("method", "post");               
				$('#verCierre').attr('action', "{{route('generarExcelReporteCierre')}}").submit();
            });

	</script>



@endsection
