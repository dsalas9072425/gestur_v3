<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Detalle Cierre</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 90%;
			margin:0 auto;
    		z-index:1;
	}
	
	table{
		width: 100%;

	}
	
	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 700;
	}

	.text{
		  overflow: hidden;
		  /*text-overflow: ellipsis;*/
		  white-space: nowrap;
		  display:block;
		  width:100%;
		  min-width:1px;
		}

	.f-10 {
		font-size: 10px !important;
	}

	.f-9 {
		font-size: 9px !important;
	}

	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}

	.f-15 {
		font-size: 15px !important;
	}
	.f-17 {
		font-size: 17px !important;
	}

	.c-text {
		text-align: center;
	}
	.r-text {
		text-align: left;
	}
	
	.r-text-detalle{
		margin-right: 20px;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 10px;
	}

	.b-buttom {
	border-bottom: 1px solid; 
	}

	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:120px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}



	</style>
</head>
<body>
		@php
            $logoEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->logoEmpresa;
            $empresaLogo = "logoEmpresa/".$logoEmpresa;

            function formatMoney($num,$currency){
                if($currency != 111){
                    return number_format($num, 2, ",", ".");	
                } 
                return number_format($num,0,",",".");

            }

        @endphp
		<br>
		<br>
		<div class="container espacio-10">
			<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
                <br>
				<table>
					<tr>
						<td colspan="6" class="r-text">
								<br>
								<img src="{{asset($empresaLogo)}}" style="width: 120px;height: 45px;">
						</td>
					</tr>
					<tr>
						<td colspan="6" class="c-text f-17">
							<b>REPORTE DEL CIERRE DE CAJA</b>
						</td>
					</tr>
					<tr>
						<td colspan="6" class="r-text">
							<br>
						</td>
					</tr>	
					<tr>
						<td class="r-text f-15">
						</td>
						<td colspan="4" class="r-text f-15">

						</td>
					</tr>	
					</tr>	
				</table>	
                <br>
                <br>
                <br>
                <br>
                <table border = 1 cellspacing=0 cellpadding=2 style="width: 680px;margin-left: 25px;" class ="f-11">	
                    <tr style="background-color: lightgray;">
						<th class="c-text">
                            Fecha
						</th>
                        <th class="c-text">
                            Total GS
						</th>
						<th class="c-text">
                            Total US
						</th>
						<th class="c-text">
                            Usuario
						</th>
					</tr>
                    @foreach($cierreCajas as $cierreCaja)
                        <tr>
                            <td class="c-text">
                                {{date('d/m/Y H:m:i', strtotime($cierreCaja->fecha_creacion))}}
                            </td>
                            <td class="c-text">
                                {{number_format($cierreCaja->total_gs,0,",",".")}}
                            </td>
                            <td class="c-text">
                                {{number_format($cierreCaja->total_us,2,",",".")}}
                            </td>
                            <td class="c-text">
                                {{$cierreCaja->nombre}} {{$cierreCaja->apellido}}
                            </td>
                        </tr>
                        <tr>
                            <td colspan = "4">
                                <table border = 1 cellspacing=0 cellpadding=2 class ="f-11">	
                                    <tr style="background-color: lightgray;">
                                        <th class="r-text">
                                            Fecha
                                        </th>
                                        <th class="c-text">
                                            Total GS
                                        </th>
                                        <th class="c-text">
                                            Total US
                                        </th>
                                    </tr>
                                    @foreach($cierreCajaDetalles as $cierreCajaDetalle)
                                        <tr>
                                            <td class="r-text">
                                                {{$cierreCajaDetalle->denominacion}}
                                            </td>
                                            <td class="c-text">
                                                {{$cierreCajaDetalle->currency_code}}
                                            </td>
                                            <td class="c-text">
                                                    {{formatMoney($cierreCajaDetalle->total,$cierreCajaDetalle->id_moneda)}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>	

                            </td>
                        </tr>
                    @endforeach
                </table>	
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>

        </div>
    </div>
	<div class="container espacio-10">
		<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
			<br>
			<table>
				<tr>
					<td colspan="6" class="r-text">
							<br>
							<img src="{{asset($empresaLogo)}}" style="width: 120px;height: 45px;">
					</td>
				</tr>
				<tr>
					<td colspan="6" class="c-text f-17">
						<b>REPORTE COBRO ANTICIPOS</b>
					</td>
				</tr>
				<tr>
					<td colspan="6" class="r-text">
						<br>
					</td>
				</tr>	
				<tr>
					<td class="r-text f-15">
					</td>
					<td colspan="4" class="r-text f-15">

					</td>
				</tr>	
				</tr>	
			</table>	
			<br>
			<br>
			<br>
			<br>
			<table border = 1 cellspacing=0 cellpadding=2 style="width: 680px;margin-left: 25px;" class ="f-11">	
				<tr style="background-color: lightgray;">
					<th class="c-text">
						Fecha Cobro
					</th>
					<th class="c-text">
						Total GS
					</th>
					<th class="c-text">
						Total US
					</th>
					<th class="c-text">
						Usuario
					</th>
				</tr>
				@foreach($cierreCajasAnticipo as $cierreCajaAnticipo)
					<tr>
						<td class="c-text">
							{{date('d/m/Y H:m:i', strtotime($cierreCajaAnticipo->fecha_hora_cobro))}}
						</td>
						<td class="c-text">
							{{number_format($cierreCajaAnticipo->total_guaranies,0,",",".")}}
						</td>
						<td class="c-text">
							{{number_format($cierreCajaAnticipo->total_dolares,2,",",".")}}
						</td>
						<td class="c-text">
							{{$cierreCajaAnticipo->nombre}} {{$cierreCajaAnticipo->apellido}}
						</td>
					</tr>
					<tr>
						<td colspan = "4">
							<table border = 1 cellspacing=0 cellpadding=2 class ="f-11">	
								<tr style="background-color: lightgray;">
									<th class="r-text">
										Fecha
									</th>
									<th class="c-text">
										Total GS
									</th>
									<th class="c-text">
										Total US
									</th>
								</tr>
								
							</table>	

						</td>
					</tr>
				@endforeach
			</table>	
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>

	</div>
</div>
	<div class="container espacio-10">
		<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
			<br>
			<table>
				<tr>
					<td colspan="6" class="r-text">
							<br>
							<img src="{{asset($empresaLogo)}}" style="width: 120px;height: 45px;">
					</td>
				</tr>
				<tr>
					<td colspan="6" class="c-text f-17">
						<b>REPORTE RECIBOS ANULADOS</b>
					</td>
				</tr>
				<tr>
					<td colspan="6" class="r-text">
						<br>
					</td>
				</tr>	
				<tr>
					<td class="r-text f-15">
					</td>
					<td colspan="4" class="r-text f-15">

					</td>
				</tr>	
				</tr>	
			</table>	
			<br>
			<br>
			<br>
			<br>
			<table border = 1 cellspacing=0 cellpadding=2 style="width: 680px;margin-left: 25px;" class ="f-11">	
				<tr style="background-color: lightgray;">
					<th class="c-text">
						Fecha Cobro
					</th>
					<th class="c-text">
						Fecha Anulacion
					</th>
					<th class="c-text">
						Total GS
					</th>
					<th class="c-text">
						Total US
					</th>
					<th class="c-text">
						Usuario Anulado
					</th>
				</tr>
				@foreach($cierreCajasRecibo_anulados as $cierreCajasRecibo_anulado)
					<tr>
						<td class="c-text">
							{{date('d/m/Y H:m:i', strtotime($cierreCajasRecibo_anulado->fecha_hora_cobro))}}
						</td>
						<td class="c-text">
							{{date('d/m/Y H:m:i', strtotime($cierreCajasRecibo_anulado->fecha_hora_anulacion))}}
						</td>
						<td class="c-text">
							{{number_format($cierreCajasRecibo_anulado->total_guaranies,0,",",".")}}
						</td>
						<td class="c-text">
							{{number_format($cierreCajasRecibo_anulado->total_dolares,2,",",".")}}
						</td>
						<td class="c-text">
							{{$cierreCajasRecibo_anulado->nombre_anulado}} {{$cierreCajasRecibo_anulado->apellido_anulado}}
						</td>
					</tr>
					<tr>
						<td colspan = "4">
							<table border = 1 cellspacing=0 cellpadding=2 class ="f-11">	
								<tr style="background-color: lightgray;">
									<th class="r-text">
										Fecha
									</th>
									<th class="c-text">
										Total GS
									</th>
									<th class="c-text">
										Total US
									
									<th class="c-text">

									</th>
								</th>
								</tr>
								
							</table>	

						</td>
					</tr>
				@endforeach
			</table>	
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>

	</div>
</div>
</body>
</html>
