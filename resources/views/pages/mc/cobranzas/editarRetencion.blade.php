@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.table th, .table td {
    			padding: 5px;;
		}
	</style>
@endsection
@section('content')

@include('flash::message') 

<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Editar Retencion</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<form id="edit">
					<div class="row">
						<input type="hidden" value="{{$resultado[0]->id}}" name="retencion_id"/>	
						<div class="col-12 col-sm-6">
							<div class="form-group"> 
								<label>Fecha Documento</label> 						 
								<input type="text"  class="form-control font-weight-bold" value="{{date('d/m/Y',strtotime($resultado[0]->fecha_hora))}}" maxlength="10" id="factura" data-value-type="s_date" name="factura" disabled="disabled" />
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group"> 
								<label>Factura</label> 	
								@if(isset($resultado[0]->libroVenta))
								<?php $documento =  $resultado[0]->libroVenta->nro_documento;  ?>
								@else
								<?php $documento =  0;  ?>
								@endif					 
								<input type="text"  class="form-control font-weight-bold" value="{{$documento}}" maxlength="10" id="factura" data-value-type="s_date" name="factura" disabled="disabled" />
							</div> 
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label>Tipo Retención</label> 						 
								<select class="form-control select2" name="id_tipo_retencion" id="tipo_retencion" style="width: 100%;">		
									<option value="1">Retenciones generadas</option>
									<option value="2">Retenciones a generar</option>
									<option value="3">Retenciones pendientes</option>
								</select>
							</div> 
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label>Fecha Retencion</label>						 
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text"  class="form-control single-picker" value="{{date('d/m/Y',strtotime($resultado[0]->fecha_hora))}}" maxlength="10" id="fecha_retencion" data-value-type="s_date" name="fecha_documento" />
								</div>
							</div>	
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label>Numero Retención </label>
								<input type="text" class="form-control" name="num_retencion" maxlength="30" value="{{$resultado[0]->numero}}" id="fp_documento_forma_pago" placeholder="Numero Retención">
							</div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
								<label>Timbrado Retención </label>
								<input type="text" class="form-control" name="num_retencion" maxlength="30" value="{{$resultado[0]->timbrado}}" id="fp_documento_forma_pago" placeholder="Timbrado Retención">
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label>Importe Retencion *</label>
								<div class="input-group">
									<input type="text" class="form-control font-weight-bold" value="{{$resultado[0]->importe}}" readonly maxlength="15" id="importe_edit_retencion" data-value-type="convertNumber" name="importe_pago">
								</div>
							</div>
						</div>
						<div class="col-12">
							<button type="button" class="btn btn-danger"  onclick="window.location.href=`{{route('listadoRetencion')}}`;"><b>Cancelar</b></button>
							<button type="button" id="btnEdit" class="btn btn-success pull-right"><b>Guardar</b></button>
						</div>
					</div>   
				</form>
			</div>
		</div>
	</section>
</section>
							   

@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(".select2").select2();

		console.log('{{$resultado[0]->tipo_retencion}}');

		$('#id_tipo_retencion').val("{{$resultado[0]->tipo_retencion}}").trigger('change.select2');

		$('.single-picker').datepicker({
			format: "dd/mm/yyyy",
			language: "es"
		});

		$('#btnEdit').on('click',function(){
			var dataString = $('#edit').serialize();

			$.ajax({
					type: "GET",
					url: "{{route('getUpdateRetencion')}}",
					dataType: 'json',
					data: dataString,
						error: function(){
							setTimeout($('.load_solicitud_ticket').hide(),1000);
							$('.msj_err_modal_solicitud').html('Ocurrio un error, vuelva a intentarlo.');
							},
						success: function(rsp){
							if(rsp.status =='OK'){
									$.toast({
											heading: 'Exito',
											text: rsp.mensaje,
											position: 'top-right',
											showHideTransition: 'slide',
											icon: 'success'
										});  
								setTimeout("redirigir()", 600);
								
							}else{
								$.toast({
									heading: 'Error',
									text: rsp.mensaje,
									showHideTransition: 'fade',
									position: 'top-right',
									icon: 'error'
								});				

							}	
						}
					})
			})


	function redirigir(){
		window.location.replace(`{{ route('listadoRetencion') }}`);
	}



	</script>


@endsection