@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
	.negrita{
		font-weight: bold;
	}
	.seletc2-sm {
		height: 30px !important;
	}


</style>	

	@parent
@endsection
@section('content')

<section id="base-style">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Recibos Emitidos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
			<form class="box-body" id="formRecibo" autocomplete="off">
				

            	<div class="row">

				<div class="col-12 col-sm-6 col-md-3">
						<div class="form-group">
							<label>Cliente</label>
							<select class="form-control select2 adaptForm" name="id_cliente"  id="id_cliente" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>

							</select>
						 </div>
				</div>
				<div class="col-12 col-sm-3 col-md-3">
		 				<div class="form-group">
					        <label>Desde / Hasta Fecha Emisión.</label>			
					        <div class="input-group">
								<div class="input-group-prepend" style="">
								    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
								</div>
								<input type="text"  class = "Requerido form-control input-sm" id="periodo" name="periodo" tabindex="10"/>
							</div>
						</div>	
				</div> 

				<div class="col-12 col-sm-6 col-md-3">
						<div class="form-group">
							<label>Estado</label>
							<select class="form-control input-sm select2" name="id_estado[]" id="id_estado" name="id_estado" style="width: 100%;">
									@foreach($estadoCobro as $estado)
											<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
									@endforeach
							</select>
						 </div>
				</div>


				<div class="col-12 col-sm-6 col-md-3 hidden-xs">
						<div class="form-group">
							<label>Moneda</label>
							<select class="form-control select2" name="id_moneda"  id="id_moneda"  style="width: 100%;">
									<option value="">Todos</option>
									@foreach($currency as $cu)
											<option value="{{$cu->currency_id}}">{{$cu->currency_code}}</option>
									@endforeach
								
								
							</select>
						 </div>
				</div>
				<div class="col-6 col-sm-6 col-md-3">  
						<div class="form-group adaptForm">
						   <label>Nro. Recibo</label>						 
						   <div class="input-group">
						   		<input type="text" class ="Requerido form-control numeric input-sm text-bold" name="num_recibo" id="num_recibo"/>
						   </div>
					   </div>	
				</div> 

				<div class="col-6 col-sm-6 col-md-3">  
						<div class="form-group adaptForm">
						   <label>Factura</label>						 
						   <div class="input-group">
						   		<input type="text" class ="Requerido form-control numeric input-sm text-bold" name="factura" id="senha"/>
						   </div>
					   </div>	
				</div> 

				<div class="col-6 col-sm-6 col-md-3">  
						<div class="form-group adaptForm">
						   <label>Tipo</label>						 
						   <select class="form-control select2" name="tipo"  id="tipo" style="width: 100%;">
								<option value="1">Resumen</option>
								<option value="2">Detalle</option>
							</select>
					   </div>	
				</div> 
				<div class="col-6 col-sm-6 col-md-3">  
						<div class="form-group adaptForm">
							<label>Cierre Caja</label>
							<select class="form-control select2" name="id_cierre"  id="id_cierre" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($cierres as $cierre)
										<option value="{{$cierre->id}}">{{date('d/m/Y', strtotime($cierre->fecha_cierre))}} </option>
									@endforeach
							</select>
					   </div>	
				</div> 

				<div class="col-6 col-sm-6 col-md-9"> </div> 
				<div class="col-3 d-none d-sm-block">
						<br>
						<!--<button type="button" class="btn btn-info  btnImprimirDuplicados btn-lg " style="margin-top:5px; margin-right:10px;" ><b>Imprimir Duplicados</b></button>-->
						<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style="margin-top:5px; margin-right:10px;"><b>Excel</b></button>
						<button type="button" class="btn btn-info  btnBuscar btn-lg pull-right " style="margin-top:5px; margin-right:10px;" ><b>BUSCAR</b></button>
					</div> 
				
				</div> 
				<input type="hidden" name="option" value="3"> {{--SOLO IMPRIMIR DUPLICADOS --}}
			</form>      
			

		
 
				<div class="table-responsive">
	              <table id="listado" class="table table-hover table-condensed nowrap" style="width: 100%;">
	                <thead>
					  <tr class="text-center">
			            <th>Agencia</th>
			            <th>Nro</th>
						<th>Monto</th>
						<th>Tipo</th>
						<th>Estado</th>
						<th>Moneda</th>
						<th>Fecha</th>
						<th>Concepto</th>
						<th>Sucursal</th>
						<th>Forma Cobro</th>
						<th>Nro.Comprobante</th>
						<th>Banco/Cuenta</th>
						<th>Fecha Confirmacion</th>
						<th>Usuario</th>
			            <th></th> 
		              </tr>
	                </thead>
	                <tbody>
					  
			        </tbody>
				  </table>
				</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>

	<script type="text/javascript">
		const operaciones = {
			fuente:15
		}

		var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('input[name="periodo"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
				  });	


		$('#id_estado').select2({
									multiple:true,
									maximumSelectionLength: 2,
									placeholder: 'Todos'
								});
		$('#id_estado').val([31,40]).trigger('change.select2');
		
		var tableListado;

		$(document).ready(function() {
			// detectMovile();
			tableRecibo();
			calendar();
			normalStyleform();
			$('.select2').select2();
			// MediaQuery();

		$('.btnBuscar').click(()=>{
			var table = $('#myTable').DataTable();
			table.destroy();
			tableRecibo();
		});

	});

	
	 	$('.btnImprimirDuplicados').click((e)=>{
	 		$.blockUI({
                centerY: 0,
                message: "<h2>Procesando Impresión...</h2>",
                css: {
                    color: '#000'
                }
            });
			e.preventDefault();
                $('#formRecibo').attr('action', "{{route('imprimirReciboDuplicadoLista')}}").submit();
            $.unblockUI(); 
		 });


		function normalStyleform(){
			if ($('#id_cliente').hasClass("select2-hidden-accessible")) {
				$('#id_cliente').select2('destroy');
			}

			if ($('#id_estado').hasClass("select2-hidden-accessible")) {
				$('#id_estado').select2('destroy');
			}

			$('#id_cliente').select2({
				containerCssClass: "seletc2-sm",
				data: operaciones.dataClient
				
			});	

			$('#id_estado').select2({
				minimumResultsForSearch: 'Infinity',
				containerCssClass: "seletc2-sm",
				data: operaciones.dataEstado
			});


			$('.adaptForm').removeClass('form-group-sm');
			$('.inputAdapt').removeClass('input-sm');

			operaciones.fuente = 15;
					$("table.dataTable").css("font-size", 15);
					$('#fecha_emision').removeAttr('readonly');

		}


		function smStyleform(){
			if ($('#id_cliente').hasClass("select2-hidden-accessible")) {
				$('#id_cliente').select2('destroy');
			}

			if ($('#id_estado').hasClass("select2-hidden-accessible")) {
				$('#id_estado').select2('destroy');
			}

			$('#id_cliente').select2({
				containerCssClass: "seletc2-sm",
				data: operaciones.dataClient
				
			});	

			$('#id_estado').select2({
				minimumResultsForSearch: 'Infinity',
				containerCssClass: "seletc2-sm",
				data: operaciones.dataEstado
			});


			$('.adaptForm').addClass('form-group-sm');
			$('.inputAdapt').addClass('input-sm');

			operaciones.fuente = 10;
				$("table.dataTable").css("font-size", 10);
				$('#fecha_emision').prop('readonly',true);

		}

			operaciones.dataClient = [
				@foreach($clientes as $cliente)
				@php
					$ruc = trim($cliente->documento_identidad);
					if(isset($cliente->dv)){
						$ruc = trim($ruc."-".$cliente->dv);
					}
					$nombre_apellido = trim($cliente->nombre." ".$cliente->apellido)
				@endphp

				{ id:'{{$cliente->id}}', text:'{{$ruc}} - {{$nombre_apellido}} - {{$cliente->id}}' },
				@endforeach
			];

		
			operaciones.dataEstado = [
				@foreach($estadoCobro as $estado)
				{id:'{{$estado->id}}',text:'{{$estado->denominacion}}'},
				@endforeach
				];


				function  tableRecibo(responsive = false){
			let resp;
			// $('#formRecibo [data-toggle="popover"]').popover();
			let form = $('#formRecibo').serializeJSON({
				customTypes: customTypesSerializeJSON
			});
			 
			if($("#tipo").val() == 1){
				tableListado = $("#listado").DataTable({
					destroy: true,
					ajax: {
						url: "{{route('ajaxTableRecibo')}}",
						data: form,
						error: function (jqXHR, textStatus, errorThrown) {
							console.log('ERrror');
						}

					},
					responsive: {
							details: false
						},

		
					"columns": [

						{data: function(x){
							let name = (Boolean(x.cliente)) ? x.cliente : '';
							//  return name.substring(0,10)+'...';
							return `<div class="fonticon-wrap" data-toggle="popover" data-trigger="hover" data-placement="top" data-container="body" 
										data-original-title="Resumen" data-content="RETENCION: ${x.detalle_count}, USUARIO: ${x.usuario}">
								<i class="fa fa-info-circle"></i>   ${name}</div>`;
						}, className:'not-mobile'},
						{data: function(x){
							return `<a href="{{route('controlRecibo',['id'=>''])}}/${x.id}"class="bgRed">
										<i class="fa fa-fw fa-search"></i>${x.nro_recibo}</a>
										<div style="display:none;">${x.nro_recibo}</div>`;
						}, className:'not-mobile'},
						{data: function(x){
								if(x.id_estado == 55){
									return 0;
								}
							return x.importe;
						}, className:'not-mobile'},
						{data:function(x){
							let tipo_id = (Boolean(x.tipo_recibo)) ? x.tipo_recibo: '';
							let tipo_n = (Boolean(x.tipo_recibo)) ? x.tipo_recibo: '';
							return '<span class="hidden">'+tipo_id+'</span>'+tipo_n;
						}, className:'not-mobile'},
						{data:function(x){
							return '<span class="hidden">'+x.orden+'</span>'+x.estado_recibo;
						}, className:'not-mobile'},
						{data: 'moneda', className:'not-mobile'},
						{data: 'fecha_creacion_format', className:'not-mobile'},
						{data: 'concepto_cortado', className:'not-mobile'},
						{data: 'sucursal', className:'not-mobile'},
						{data: function(x){ 
							let fecha = '';
							return fecha;
						}, className:'not-mobile'},
						{data: function(x){ 
							let fecha = '';
							return fecha;
						}, className:'not-mobile'},

						{data: function(x){ 
							let fecha = '';
							return fecha;
						}, className:'not-mobile'},

						{data: function(x){ 
							let fecha = '';
							if(Boolean(x.fecha_hora_cobro_format)){
								fecha = x.fecha_hora_cobro_format
							}
							return fecha;
						}, className:'not-mobile'},

						{data: function(x){
							let nombre = '';
							if(Boolean(x.usuario)){
								nombre = x.usuario
							}
							return nombre;
						}, className:'not-mobile'},
						{data: function(x){
							let cantidad = '';
							adjunto = '<i class="fa fa-fw fa-circle  rojo " title="Inactivo Gestur" style="color: red;"></i>';
							if(Boolean(x.recibo_adjunto)){
								recibo_adjunto = x.recibo_adjunto;
								cantidad = recibo_adjunto.length
								if(cantidad > 0){
									adjunto = '<i class="fa fa-fw fa-circle  verde " title="Tiene Adjunto" style="color: green;"></i>';
								}else{
									adjunto = '<i class="fa fa-fw fa-circle  rojo " title="No Tiene Adjunto" style="color: red;"></i>';
								}
							}
							return adjunto;
						}, className:'not-mobile'},

					],
					"order": [],
					"createdRow": function ( row, data, index ) {
							$(row).addClass('negrita');
									
						},
					"initComplete": function(settings, json) {

							$('[data-toggle="popover"]').popover({
								'trigger': 'hover',
								'placement': 'left',
								'container': '#formRecibo'
								});
						},
					"columnDefs": [
						{
							"targets": [ 9],
							"visible": false,
						},
						{
							"targets": [ 10 ],
							"visible": false,
						},
						{
							"targets": [ 11 ],
							"visible": false,
						}
					]	
					}).draw(true).columns.adjust();

				}else{

					$("#forma").css('display', 'block');
					tableListado = $("#listado").DataTable({
					destroy: true,
					ajax: {
						url: "{{route('ajaxTableReciboFormaPago')}}",
						data: form,
						error: function (jqXHR, textStatus, errorThrown) {
							console.log('ERrror');
						}

					},
					responsive: {
							details: false
						},

					"columns": [
						{data: function(x){
							let name = (Boolean(x.cliente)) ? x.cliente.full_name : '';
							//  return name.substring(0,10)+'...';
							return `<div class="fonticon-wrap" data-toggle="popover" data-trigger="hover" data-placement="top" data-container="body" 
										data-original-title="Resumen" data-content="RETENCION: ${x.detalle_count}, USUARIO: ${x.cliente}">
								<i class="fa fa-info-circle"></i>   ${x.cliente}</div>`;

														
						}, className:'not-mobile'},

						{data: function(x){
							console.log(x);
							return `<a href="{{route('controlRecibo',['id'=>''])}}/${x.id}"class="bgRed">
										<i class="fa fa-fw fa-search"></i>${x.nro_recibo}</a>
										<div style="display:none;">${x.nro_recibo}</div>`;
						}, className:'not-mobile'},
						{data:function(x){
								if(x.id_estado == 55){
									return 0;
								}
							return x.importe;
						}, className:'not-mobile'},
						{data: 'tipo_recibo_denominacion', className:'not-mobile'},
						{data:function(x){
							return '<span class="hidden">'+x.orden+'</span>'+x.estado;
						}, className:'not-mobile'},
						{data: 'moneda', className:'not-mobile'},
						{data: 'fecha_creacion_format', className:'not-mobile'},
						{data: 'concepto_cortado', className:'not-mobile'},
						{data: 'sucursal', className:'not-mobile'},//para sucursal
						{data: 'forma_cobro', className:'not-mobile'},
						{data: 'nro_comprobante', className:'not-mobile'},
						{data: 'banco', className:'not-mobile'},
						{data: 'fecha_hora_cobro_format', className:'not-mobile'},
						{data: 'usuario_generado', className:'not-mobile'},
						{data: function(x){
							let cantidad = '';
							adjunto = '<i class="fa fa-fw fa-circle  rojo " title="Inactivo Gestur" style="color: red;"></i>';
							if(Boolean(x.recibo_adjunto)){
								recibo_adjunto = x.recibo_adjunto;
								cantidad = recibo_adjunto.length
								if(cantidad > 0){
									adjunto = '<i class="fa fa-fw fa-circle  verde " title="Tiene Adjunto" style="color: green;"></i>';
								}else{
									adjunto = '<i class="fa fa-fw fa-circle  rojo " title="No Tiene Adjunto" style="color: red;"></i>';
								}
							}
							return adjunto;
						}, className:'not-mobile'},

					],
					"order": [],
					"createdRow": function ( row, data, index ) {
							$(row).addClass('negrita');
									
						},
						"initComplete": function(settings, json) {

							$('[data-toggle="popover"]').popover({
								'trigger': 'hover',
								'placement': 'left',
								'container': '#formRecibo'
								});
						}	
					});



				}
		}	
		

		function calendar(){
			$('.single-picker').datepicker({ 
						format: "dd/mm/yyyy", 
						language: "es",
						clearBtn : true
						});
		 }//

		 
		 
		 function detectMovile(){
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			

				} 
		 }//

		
			$("#botonExcel").on("click", function(e){ 
			// console.log('Inicil');
                e.preventDefault();
                $('#formRecibo').attr('method','post');
                $('#formRecibo').attr('action', "{{route('generarExcelRecibos')}}").submit();
            });
		
		
	</script>
@endsection
