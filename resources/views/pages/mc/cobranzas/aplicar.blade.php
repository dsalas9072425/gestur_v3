@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	  <link rel="stylesheet" href="{{asset('mC/css/select.dataTables.min.css')}}"> 
@endsection
@section('content')

<style type="text/css">

	.readOnly {
		background-color: #fff;
		border-color: rgb(210, 214, 222);
		cursor: not-allowed !important;
	}

.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

@-moz-document url-prefix() {
  fieldset { display: table-cell; }
}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

/*	.select2-container *:focus {
        outline: none;
    }*/


    .correcto_col { 
		height: 74px;
     }

     #divBanco {
	border: 2px solid #D0D1C2;
	border-radius: 5px;

     }

     .title_bank{
     	border-bottom-right-radius: 5px;
		border-bottom-left-radius: 5px;
		background-color: #D0D1C2;
    	padding: 3px 5px 5px 5px;
    	font-weight: 800;
    	margin-bottom: 2px;

     }

	 .text-bold {
		 font-weight: 800;
		 font-size: 16px;
	 }

	 .hr-m {
		margin: 0 0 5px 0;
	 }
	 .h3-m {
		 margin-bottom: 0;
	 }

	 .input-fp {
		font-weight: 800; 
		font-size: 20px;
	 }

	   .btn_op {
		 display: none;
		 margin-right: 7px; 
	 } 

	 .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
	.negrita {
		font-weight: bold;
	}

    .seletc2-md {
		height: 30px !important;
	}
    #listadoPagos input {
        font-weight: bold;
        text-align:center;
	}
	
	/*
	CUSTOM SELECT AND OPTION
	*/

	select, option{
		font-weight: bold;
	}
	
	.custom-select{
		font-weight: bold;
	}

	.lineaCompleta {
		background-color: #E8FAE4;
	}



</style>

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Forma Cobro Recibo</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body" style="padding-bottom: 60px;">

				<div class="col-12 col-sm-12 col-md-10 p-0 m-0 m-sm-auto">
				<form id="formFp" autocomplete="off">  

				
					<div class="row">

						<div class="col-12 col-sm-6">
							<div class="form-group">
								 <label>Total a Pagar</label>
								<input type="text" class="form-control input-fp" id="fp_total_pago" value ="0" disabled>
						   </div>
					   </div>



						<div class="col-12 col-md-6">
							<div class="form-group">
								 <label>Retención</label>
							<input type="text" class="form-control font-weight-bold"  id="retencion_aviso" value ="{{$cantidad_facturas}} FACTURAS CON RETENCION" disabled>
						   </div>
					   </div>

					</div>
					<div class="row">

						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Forma de Cobro</label> 		
								<select class="form-control custom-select font-weight-bold" name="id_tipo_operacion" id="fp_tipo_operacion" style="width: 100%;">
										<option selected disabled value="">Seleccione Forma de Cobro</option>
										@foreach ($tipo_operacion_pago as $operacion)	
											<option abreviatura="{{$operacion->abreviatura}}" value="{{$operacion->id}}" >{{$operacion->denominacion}}</option>
										@endforeach	
												
								</select>
							</div> 
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Moneda</label>
								 <select class="form-control custom-select font-weight-bold" name="id_moneda"  id="fp_id_moneda" style="width: 100%;" >
									@foreach ($currency as $moneda)		
									<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
									@endforeach				
								</select>
							</div>
						</div> 
					    <div class="col-12 col-md-3">
							<div class="form-group">
								 <label>Cotización </label>
								<input type="text" class="form-control control-num-moneda-fp input-fp cotizacion" id="fp_cotizacion"  name="cotizacion:convertNumber" onchange="actualizar()"  value="0">
						   </div>
					   </div>

					    <div class="col-12 col-md-3">
						<div class="form-group">
							 <label>Indice Cotización </label>
							<input type="number" class="form-control input-fp" id="fp_indice_cotizacion"  name="indice_cotizacion"  value="0" disabled>
					   </div>
				   </div> 
				</div>
				
					
				<input type="hidden" class="numerico control-num-moneda-fp"  id="total_real" value = "" />
				<input type="hidden" id="tipo" value = "0" />  
				<div id="tipo_operacion_div" class="col-12" style="border: 3px solid #A9CCE3; border-radius:5px; padding-top:5px;"></div>	   
				</form>

				<form id="formFormaPago" class="row" autocomplete="off"> 
						<div class="col-12">
							<div class="table-responsive">
								<table id="listadoPagos" class="table" style="width:100%">
									<thead>
										<tr>
											<th class="text-center">Operación</th>
											<th class="text-center">Cotización Día</th>
											<th class="text-center">Cotización Modificada</th>
											<th class="text-center">Importe Pago</th>
											<th></th>
											<th></th>
										</tr>
									</thead>
			 
									<tbody class="tbody_fp" >
									</tbody>          
								</table>
							  </div>
    							<div class="form-group row">
									<label  class="col-sm-2 col-form-label">Diferencia :</label>
									<div class="col-sm-10">
										<input type="text" class="text-bold form-control clear_input_txt" readonly id="fp_diferencia_pago" value="0">
									</div>
								  </div>

						</div>	
                	   	<div class="col-12 col-md-12">
                	   		<?php 
                	   			if(empty($datos[0]->concepto_forma_cobro)){
                	   				$concepto = $datos[0]->concepto;	
                	   			}else{
                	   				$concepto = $datos[0]->concepto_forma_cobro;
                	   			}
                	   		?>
                    	  	<div class="form-group">
								<label  class="col-md-2 control-label" style="padding-left: 0px;">Concepto:</label>
								<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
									<input type="text"  class="text-bold form-control clear_input_txt" id="cf_concepto" value="">
								</div>
					  		</div>
						</div>
						
						</form>

						<div class="col-12 col-md-12" style="padding-left: 0px;padding-right: 0px;">
							<div class="row" id='output'>
							<input type="hidden" id="cantidad_adjunto" value="{{count($adjuntos)}}">		

							@foreach($adjuntos as $adjunto)
									<?php
	                  					$tamanho = $adjunto->comprobante;
	                  					$tamanhoInicial = explode('.', $tamanho);
										$ruta = 'uploadComprobanteRecibo/'.$adjunto->comprobante;
									?>
									<div id="cuadro_{{$adjunto->id}}">
										<div class="col-md-2" style="margin-bottom: 20px;">
											@if(isset($tamanhoInicial[1]))
												@if($tamanhoInicial[1] == 'pdf')
													<a href="{{asset($ruta)}}" target="_blank"><img class="img-responsive" style="width:120px;" src="{{asset('images/file.png')}}" alt="Photo"></a>
												@else	
													<a href="{{asset($ruta)}}" target="_blank"><img class="img-responsive" style="width:120px; height: 120px;" src="{{asset($ruta)}}" alt="Photo"></a>
												@endif	
											@else
												<a href="{{asset($ruta)}}" target="_blank"><img class="img-responsive" style="width:120px; height: 120px;" src="{{asset($ruta)}}" alt="Photo"></a>
											@endif
										</div>
		                                <div class="col-md-1">
											@if($datos[0]->estado->id == 31)
												<button type="button" id="btn-imagen" data-id="{{$adjunto->id}}" onclick='eliminarImagen("{{$adjunto->id}}")'>
													<i class="ft-x-circle" style="font-size: 18px;"></i>
												</button>
											@endif
		                                </div>
	                                </div>  

								@endforeach   							
							</div>    
							<form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('uploadDocumentosRecibos')}}" autocomplete="off">
								<div class="form-group">
								<div class="row">
									<div class="col-12">
										<label class="control-label" >Adjuntar comprobantes: </label>
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="id_recibo" id="id_recibo" value="{{$id}}"/> 
										<input type="file" class="form-control" name="image" id="image"/>  
										<h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: <span class="text-uppercase">pdf, xls, doc, docx, pptx, pps, jpeg, bmp, png y jpg.</span></b>
										</h4>
										<div id="validation-errors"></div>
									</div> 
									</div>  
								</div>  
							</form>
						</div>
					<br>
	                <div class="col-12">
	                	<button type="button"  id="btnVolver"  class="btn-mas-request btn btn-danger" ><b>Volver</b></button>	
						@if(count($btn) != 2)		   
							@foreach ($btn as $boton)
							
							{{-- CARGA LOS BOTONES POR PERMISO DE PERSONAS --}}
							<?php echo $boton; ?>
							@endforeach
						@else 
							<?php 
								echo $btn[0];
							 ?>	   
						@endif	
	               	</div>	
	          
          
				</div>
              
            </div>
        </div> 

		<input type="hidden" id="fp_cotizacion_actual">
</section>     

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script> --}}
	<script>
        const operaciones = {
            dataTipoOperaciones : [],
            dataMoneda : [],
            id_recibo: Number('{{$id}}'),
			diferencia: 0,
			total_pago: Number('{{$datos[0]->total_pago}}'),
			id_moneda_recibo: Number('{{$datos[0]->id_moneda}}'),
			moneda_code : '{{$datos[0]->moneda->currency_code}}',
			id_estado : Number('{{$datos[0]->id_estado}}'),
			set_indice_cotizacion:true
		}
		 
	    var msj_retencion = '';
	    var msj_cuentas = '';
	    var typingTimer;
	    var doneTypingInterval = 500; // Tiempo de espera en milisegundos
		

		$(document).ready(()=>{
			$("input.form-control").inputmask();
			$('.select2').select2({minimumResultsForSearch: 'Infinity'});
			getCotizacion();
			loadData();
			formatMoneyFp();
			formatTotalPago();
			calendarCabecera();
			getCotContable() 

		});

		function formatTotalPago(){
			let importe = operaciones.total_pago;
			importe = operaciones.moneda_code +' '+ formatCurrency(operaciones.id_moneda_recibo, importe);
			$('#fp_total_pago').val(importe);
		}
		
	function actualizar(){
		mostrarFormaCobro($('select[name="id_tipo_operacion"] option:selected').attr('abreviatura'));
	}

     $('#btnVolver').click(()=>{
        redirectReciboControl();
     });
     
     function redirectReciboControl(){
		 location.href ="{{ route('controlRecibo',['id'=>$id]) }}";
	 }
	 

	 function calendarCabecera(){
		 $('.calendar-cab').datepicker({
		format: "dd/mm/yyyy",
		language: "es"
	});

	$('.calendar-cab').datepicker('update',new Date());
	 }

	 //TODO: error al queres cobrar en guaranies, no cotiza
     $('#fp_id_moneda').change(()=>{

		if($('#fp_id_moneda').val() == 111){

			// $('#fp_indice_cotizacion').prop('disabled', true);
			// $('#fp_indice_cotizacion').prop('disabled', true);
			$('#fp_indice_cotizacion').val(0);

		} else {
			$('#fp_cotizacion').prop('disabled', false);
			$('#fp_indice_cotizacion').val(0);

			// if($('#fp_id_moneda').val() != operaciones.id_moneda_recibo){
			// 	$('#fp_indice_cotizacion').prop('disabled', false);
			// } else {
			// 	$('#fp_indice_cotizacion').prop('disabled', true);
			// }

		}

		getCotizacion();
        mostrarFormaCobro($('select[name="id_tipo_operacion"] option:selected').attr('abreviatura'));
     });

	 
	 $('#fp_indice_cotizacion').keyup(function() {
		operaciones.set_indice_cotizacion = false;
		clearTimeout(typingTimer);
		typingTimer = setTimeout(levantar, doneTypingInterval);
	});

	function levantar(){
		getCotizacion();
		mostrarFormaCobro($('select[name="id_tipo_operacion"] option:selected').attr('abreviatura'));
	}


	 $('#fp_tipo_operacion').change(()=>{
		mostrarFormaCobro($('select[name="id_tipo_operacion"] option:selected').attr('abreviatura'));
	 });

	@if(isset($datos[0]->gestor->nombre))
		$('#cf_entrega').val('{{$datos[0]->gestor->nombre}} {{$datos[0]->gestor->apellido}}');
	@else 
		$('#cf_entrega').val('');
	@endif

	function mostrarFormaCobro(idOperacion) 
	{

		let tipo_operacion = $('#fp_tipo_operacion').val();
		//alert(tipo_operacion);
		let transferencia = `
				<div class="row">

					<div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Fecha Documento</label>						 
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fa fa-calendar"></i></span>
								</div>
									<input type="text"  class="form-control calendar-cab" maxlength="10" data-value-type="s_date" name="fecha_documento" />
							</div>
						</div>	
					</div>	

					<div class="col-6 col-md-6">
						<div class="form-group">
							<label>Banco</label> 						 
							<select class="form-control custom-select" name="banco" id="fp_id_origen_pago" style="width: 100%;">
								<option value="">Seleccione Banco</option>		
								@foreach ($origen as $forma)
									<option value="{{$forma->id}}">{{$forma->nombre}}</option>
								@endforeach		
							</select>
						</div> 
					</div>

					<div class="col-6 col-md-6">
							<div class="form-group">
								<label>Cuenta</label> 						 
								<select class="form-control custom-select" name="id_cuenta_banco" maxlength="30" disabled id="fp_id_cuenta" style="width: 100%;">			
								</select>
							</div> 
					</div>
					  
					<div class="col-md-6">
							<div class="form-group">
								 <label>Nro de Transferencia</label>
							   <input type="text" class="form-control clear_input_txt" id="nro_comprobante" value="0" maxlength="30" name="nro_comprobante">
						   </div>
					   </div>

					<div class="col-12">
						<div class="form-group">
							<label>Importe Pago *</label>
							<div class="input-group">
								<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar"  maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
								<div class="input-group-append" id="button-addon2">
									<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
								</div>
							</div>
						</div>
					  </div>


				</div>
				`;

		let cheque = `
			<div class="row">

				<div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Banco</label> 						 
							<select class="form-control custom-select" name="banco" id="" style="width: 100%;">		
								<option value="">Seleccione Banco</option>
								 @foreach ($bancos as $banco)
									 <option value="{{$banco->id}}">{{$banco->nombre}}</option>
								 @endforeach		
							</select>
						</div> 
					</div>

				   <div class="col-6 col-sm-6">
				 <div class="form-group">
					   <label>Fecha Emisión</label>						 
					   <div class="input-group">
						   <div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-calendar"></i></span>
						   </div>
							  <input type="text"  class="form-control single-picker" maxlength="10" id="fecha_documento" data-value-type="s_date" name="fecha_documento" />
					   </div>
				   </div>	
				   </div>	

				   <div class="col-6 col-sm-6">
					<div class="form-group">
					   <label>Numero</label>						 
						 <input type="number"  class="form-control" maxlength="10" id="" name="nro_comprobante"/>
				   </div>
				   </div>


				   <div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Importe Pago *</label>
							<div class="input-group">
								<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar" maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
								<div class="input-group-append" id="button-addon2">
									<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
								</div>
							</div>
						</div>
					  </div>

			</div>`;

			let cheque_diferido = `
			<div class="row">

				<div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Banco</label> 						 
							<select class="form-control custom-select" name="banco" id="" style="width: 100%;">		
								<option value="">Seleccione Banco</option>
								 @foreach ($bancos as $banco)
									 <option value="{{$banco->id}}">{{$banco->nombre}}</option>
								 @endforeach		
							</select>
						</div> 
					</div>

				   <div class="col-6 col-sm-6">
				 <div class="form-group">
					   <label>Fecha Cheque</label>						 
					   <div class="input-group">
						   <div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-calendar"></i></span>
						   </div>
							  <input type="text"  class="form-control single-picker" maxlength="10" id="fecha_documento" data-value-type="s_date" name="fecha_documento" />
					   </div>
				   </div>	
				   </div>	

				   <div class="col-6 col-sm-6">
					<div class="form-group">
					   <label>Numero</label>						 
						 <input type="number"  class="form-control" maxlength="10" id="" name="nro_comprobante"/>
				   </div>
				   </div>


				   <div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Importe Pago *</label>
							<div class="input-group">
								<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar" maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
								<div class="input-group-append" id="button-addon2">
									<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
								</div>
							</div>
						</div>
					  </div>

			</div>`;

		let efectivo = `
				<div class="row">

					<div class="col-12">
						<div class="form-group">
							<label>Importe Pago *</label>
							<div class="input-group">
								<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar" maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
								<div class="input-group-append" id="button-addon2">
									<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
								</div>
							</div>
						</div>
					  </div>

				</div>`;

		let retencion = `
				<div class="row">
					
					   <div class="col-12">
						<div class="form-group">
							<label>Importe Pago *</label>
							<div class="input-group">
							<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar" maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
								<div class="input-group-append">
									<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
								</div>
							</div>
						</div>
					  </div>

				</div>   `;

		let deposito = `
				<div class="row">

					<div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Banco</label> 						 
							<select class="form-control select2" name="banco" id="fp_id_origen_pago" style="width: 100%;">	
							<option value="">Seleccione Banco</option>	
							 @foreach ($origen as $forma)
								 <option value="{{$forma->id}}">{{$forma->nombre}}</option>
							 @endforeach		
							</select>
						</div> 
					</div>
					<div class="col-12 col-sm-6">
							<div class="form-group">
								<label>Cuenta</label> 						 
								<select class="form-control select2" name="id_cuenta_banco" disabled id="fp_id_cuenta" style="width: 100%;">			
								</select>
							</div> 
					</div>

					
					<div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Fecha Documento</label>						 
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fa fa-calendar"></i></span>
								</div>
									<input type="text"  class="form-control calendar-cab" maxlength="10" data-value-type="s_date" name="fecha_documento" />
							</div>
						</div>	
					</div>

					<div class="col-12 col-sm-6">
							<div class="form-group">
								 <label>Nro de Deposito</label>
							   <input type="number" class="form-control clear_input_txt" maxlength="30" name="nro_comprobante" id="" value="0">
						   </div>
					 </div>

				   <div class="col-12">
						<div class="form-group">
							<label>Importe Pago *</label>
							<div class="input-group">
								<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar font-weight-bold" maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
								<div class="input-group-append" id="button-addon2">
									<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
								</div>
							</div>
						</div>
				</div> `;

		let canje = `	
			<div class="row">

				<div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Retención</label> 						 
							<select class="form-control custom-select" name="retencion_canje" id="canje_retencion" style="width: 100%;">	
								<option value="false">Sin retención</option>	
								<option value="true">Con retención</option>
							</select>
						</div> 
					</div>

					<div class="col-12 col-sm-6">
						<div class="form-group">
							<label>Factura</label> 						 
							<select class="form-control custom-select" name="id_libro_compra_canje" id="id_libro_compra" disabled style="width: 100%;">
							</select>
						</div> 
					</div>
					

		
				<div class="col-12">
						<div class="form-group">
							<label>Importe Pago *</label>
							<div class="input-group">
								<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar" maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
								<div class="input-group-append" id="button-addon2">
									<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
								</div>
							</div>
						</div>
			</div>`;


		let credito = `
				<div class="row">
							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Procesadora</label> 						 
									<select class="form-control custom-select" name="id_procesadora" id="procesadora">	
										
										@foreach($pocesadora as $proc)
										<option value="{{$proc->id}}">{{$proc->denominacion}}</option>
										@endforeach		
									</select>
								</div> 
							</div>

							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Nro de Operación</label>
									<input type="text" class="form-control" id="num_operacion" maxlength="30" value="0" name="num_operacion">
								</div>
							</div>

							<div class="col-12 col-sm-6">
									<div class="form-group">
										<label>Fecha Documento</label>						 
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
												<input type="text"  class="form-control calendar-cab" maxlength="10" data-value-type="s_date" name="fecha_documento" />
										</div>
									</div>	
								</div>

								<div class="col-12 col-sm-6">
									<div class="form-group">
										<label>Importe Pago *</label>
										<div class="input-group">
											<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar" maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
											<div class="input-group-append" id="button-addon2">
												<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
											</div>
										</div>
									</div>
								</div>
							</div>
					`;


		let debito = `
				<div class="row">
							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Procesadora</label> 						 
									<select class="form-control custom-select" name="id_procesadora" id="procesadora">	
									
										@foreach($pocesadora as $proc)
											<option value="{{$proc->id}}">{{$proc->denominacion}}</option>
										@endforeach		
									</select>
								</div> 
							</div>

							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Nro de Operación</label>
									<input type="text" class="form-control" id="num_operacion" maxlength="30" value="0" name="num_operacion">
								</div>

							</div>

							<div class="col-12 col-sm-6">
									<div class="form-group">
										<label>Fecha Documento</label>						 
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
												<input type="text"  class="form-control calendar-cab"  maxlength="10" data-value-type="s_date" name="fecha_documento" />
										</div>
									</div>	
								</div>
								<div class="col-12 col-sm-6">
									<div class="form-group">
										<label>Importe Pago *</label>
										<div class="input-group">
											<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar" maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
											<div class="input-group-append" id="button-addon2">
												<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
											</div>
										</div>
									</div>
								</div>
							</div>
								`;

		let voucher = `
									<div class="row">
										<div class="col-6 col-sm-6">
											<div class="form-group">
												<label>Numero Comprobante</label>						 
												<input type="text" class="form-control" maxlength="20" id="" name="nro_comprobante"/>
											</div>
										</div>
										<div class="col-6 col-sm-6">
											<div class="form-group">
												<label>Tarjeta </label>						 
												<input type="text" class="form-control" id="num_operacion" maxlength="30" value="0" name="num_operacion">
											</div>
										</div>
										<div class="col-12 col-sm-6">
											<div class="form-group">
												<label>Vencimiento</label>						 
												<div class="input-group">
													<div class="input-group-prepend">
														<span class="input-group-text"><i class="fa fa-calendar"></i></span>
													</div>
														<input type="text"  class="form-control calendar-cab" maxlength="10" data-value-type="s_date" name="vencimiento" />
												</div>
											</div>	
										</div>

										<div class="col-12 col-sm-6">
											<div class="form-group">
												<label>Importe Voucher *</label>
												<div class="input-group">
													<input type="text" class="form-control control-num-moneda-fp input-fp importe_a_pagar" maxlength="15" id="fp_importe_pago" data-value-type="convertNumber" name="importe_pago">
													<div class="input-group-append" id="button-addon2">
														<button class="btn btn-primary" id="btnAddFormaPago" type="button">Guardar</button>
													</div>
												</div>
											</div>
										</div>
									</div>

									</div>`;


		$('#tipo_operacion_div').html(' ');
		//EFECTIVO
		if (idOperacion == 'EFECT') {
			$('#tipo_operacion_div').html(efectivo);
		}

		//CHEQUE
		if (idOperacion == 'CHEQ') {
			$('#tipo_operacion_div').html(cheque);
			//numMaxCheque();
		}

		//CHEQUE DIFERIDO
		if (idOperacion == 'CHEQ_DIF') {
			$('#tipo_operacion_div').html(cheque_diferido);
			//numMaxCheque();
		}

		//TRANSFERENCIA
		if (idOperacion == 'TRANSF') {
			$('#tipo_operacion_div').html(transferencia);
		}

		//RETENCION
		if (idOperacion == 'RETEN') {
			$('#tipo_operacion_div').html(retencion);
				//  f_retencion.initEventos();
		}

		//DEPOSITO
		if (idOperacion == 'DEP') {
			$('#tipo_operacion_div').html(deposito);
		}
		//CANJE
		if (idOperacion == 'FACT') {
			$('#tipo_operacion_div').html(canje);
			$('#fp_documento_forma_pago').unbind();
			$('#fp_documento_forma_pago').inputmask();
			logica_canje();

		}

		//TARJETA CREDITO
		if (idOperacion == 'TC') {
			$('#tipo_operacion_div').html(credito);
		}

		//TARJETA DEBITO
		if (idOperacion == 'TD') {
			$('#tipo_operacion_div').html(debito);
		}

		//VOUCHER TARJETA CREDITO
		if (idOperacion == 'VO') {
			$('#tipo_operacion_div').html(voucher);
		}

		initEventos();
		initCalendar();
		formatMoneyFp();
		$('.importe_a_pagar').val(operaciones.diferencia);
		
		getCtaCtb(); //CONTROL DE CUENTA
		calendarCabecera();
		$('.importe_a_pagar').change(function(event) {
			$('#total_real').val(clean_num($(this).val()));
			formatMoneyFp();
		});
	}

		$('.btn_aplicar').click(()=>{
			if($('#tipo_operacion_div').html() || $('#listadoPagos tbody tr').length > 0) {
				$('#tipo').val(1);
			    Promise.all([forma_cobro.validar_retencion()]).then().finally(()=>{
					console.log('Finalizó');
				});
			} else {    
			   $.toast({
						heading: '<b>Error</b>',
						text: 'Ingrese una Forma de Cobro.',
						position: 'top-right',
						hideAfter: false,
						icon: 'error'
					});
			}
		});


		$('.btnAplicarCobro').click(()=>{
			if($('#tipo_operacion_div').html() || $('#listadoPagos tbody tr').length > 0) {
				$('#tipo').val(0);
			    Promise.all([forma_cobro.validar_retencion()]).then().finally(()=>{
					console.log('Finalizó');
				});
			} else {    
			   $.toast({
						heading: '<b>Error</b>',
						text: 'Ingrese una Forma de Cobro.',
						position: 'top-right',
						hideAfter: false,
						icon: 'error'
					});
			}
			

		});

/*=====================================================================================================
                                LOGICA PARA CANJE
=====================================================================================================*/
	function logica_canje()
	{
		$('#canje_retencion').unbind();
		
		//OBTENER LAS FACTURAS
		$('#canje_retencion').click(()=>{
			let option_canje = $('#canje_retencion').val();

				//CON RETENCION
				if(option_canje == 'true'){
					$('#id_libro_compra').prop('disabled',false);
					$('#fp_importe_pago').prop('readonly',true);
					$('#fp_cotizacion').prop('readonly',true);
					$('#fp_id_moneda').prop('disabled',true);
					obtenerCanjeRetencion();
				} else {
				//SIN RETENCION
					$('#id_libro_compra').prop('disabled',true);
					$('#fp_importe_pago').prop('readonly',false);
					$('#fp_cotizacion').prop('readonly',false);
					$('#fp_id_moneda').prop('disabled',false);
				}
		});

		//OBTENER LOS VALORES
		$('#id_libro_compra').change(()=>{
			let monto = formatCurrency(operaciones.id_moneda_recibo,$('#id_libro_compra').find(':selected').data('importe'));
			obtenerDataCanjeRetencion();

			$('#fp_importe_pago').val(monto);
		});

	}	

	/*
	OBTIENE LOS CANJES PARA ANEXAR A LA FORMA DE COBRO
	EL VALOR OBTENIDO ES EL MONTO TOTAL MENOS LA RETENCION
	*/
	function obtenerCanjeRetencion()
	{
		var newOption,importe;

				$.ajax({
					async: false,
					cache:false,
					type: "GET",
					url: "{{route('getCanjeRetencion')}}",
					data: {
						id_recibo : operaciones.id_recibo
					},
					dataType: 'json',
					error: function (jqXHR, textStatus, errorThrown) {

						$('#id_libro_compra').prop('disabled', true);
					},
					success: function (rsp) {
						// console.log(rsp);
						if (rsp.data.length > 0) {

							$('#id_libro_compra').empty();

							$.each(rsp.data, function (key, item) {
								// console.log(item);
								importe = 0;
								if(Boolean(item.total_retencion)){
										importe = item.importe - item.total_retencion;
									} else {
										importe = item.importe;
									}

								newOption = new Option(item.nro_documento + ' (' + importe +')', item.id, false, false);
								$('#id_libro_compra').append(newOption);

									$('#id_libro_compra').append($(newOption).attr('data-importe',importe));
							});

						} else {
							$('#id_libro_compra').empty();
							$('#id_libro_compra').prop('disabled', true);
						}
					} //success
				}).done(()=>{
					obtenerDataCanjeRetencion();
				}); //ajax

	}

	/*
		OBTIENE LA MONEDA Y LA COTIZACION DEL CANJE AL SER SELECCIONADO
	*/
	function obtenerDataCanjeRetencion()
	{
		var newOption,importe;

				$.ajax({
					async: false,
					cache:false,
					type: "GET",
					url: "{{route('getDataCanjeRetencion')}}",
					data: {
						id_libro_compra : $('#id_libro_compra').val()
					},
					dataType: 'json',
					error: function (jqXHR, textStatus, errorThrown) {


					},
					success: function (rsp) {
						$('#fp_cotizacion').val(rsp.data.cotizacion_contable_venta);
						$('#fp_id_moneda').val(rsp.data.id_moneda).trigger('change.select2');
				
					} //success
				}); //ajax

	}




	/**
	 * VALIDAR SI LA FORMA DE COBRO ESTA PARAMETRIZADO SEGUN REQUERIMIENTO
	*/
	function getCtaCtb() 
	{
		var idFormaCobro = $('#fp_tipo_operacion').val();
		var idMoneda = $('#fp_id_moneda').val();

		if(!idFormaCobro){
			return ;
		}

		if(msj_cuentas.reset){
			msj_cuentas.reset();
		}

		$.ajax({
			type: "GET",
			cache:false,
			url: "{{route('getCtaCtb')}}",
			data: 
			{
				idFormaCobro:idFormaCobro,
				idMoneda : idMoneda
			},
			dataType: 'json',
			error: function (jqXHR, textStatus, errorThrown) {

				msj_cuentas = $.toast({
							heading: '<b>Error</b>',
							text: 'Ocurrió un error al intentar recuperar la cuenta contable asignada.',
							position: 'top-right',
							hideAfter: false,
							icon: 'error'
						});

					},
			
				success: function (rsp) 
				{
					if (rsp.err != true) {	
						msj_cuentas = $.toast({
							heading: '<b>Error</b>',
							text: rsp.msj,
							position: 'top-right',
							hideAfter: false,
							// width: '400px',
							icon: 'error',

						});

						$('#fp_tipo_operacion').val('').trigger('change');
						$('#tipo_operacion_div').html(' ');
					}

				} //success


		}); //ajax

	} //


function aplicarRecibo() 
{

	return new Promise((resolve, reject) => { 
	
	$.ajax({
		type: "GET",
		url: "{{route('estadoRecibo')}}",
		data: {
            id_recibo:operaciones.id_recibo,
            option : 1,
            concepto:$('#cf_concepto').val(),
			tipo:$('#tipo').val()
        },
		dataType: 'json',
		cache:false,
		error: function (jqXHR, textStatus, errorThrown) {

			msjToast('', 'error', jqXHR, textStatus);
			reject('Ocurrió un error en la comunicación con el servidor.');
		},
		success: function (rsp) {
            if(rsp.err){
                resolve('Los datos fueron procesados.');
            } else {
                reject(rsp.txtErr);
            }
            
		} //success
	}); //ajax

  });//promise
} //




function getCuentas() 
{
	var newOption;

	$.ajax({
		async: false,
		type: "GET",
		url: "{{route('getListCuentas')}}",
		data: {
			id_banco: $('#fp_id_origen_pago').val()
		},
		dataType: 'json',
		error: function (jqXHR, textStatus, errorThrown) {

			$('#fp_id_cuenta').prop('disabled', true);
		},
		success: function (rsp) {
			if (rsp.cuentas.length > 0) {

				$('#fp_id_cuenta').empty();
				// newOption = new Option('Seleccione cuenta', '', false, false);
				$('#fp_id_cuenta').append(newOption);

				$.each(rsp.cuentas, function (key, item) {
					/*newOption = new Option(item.denominacion + ' ' + item.currency_code + ' (' + item.numero_cuenta + ')', item.id, false, false);
					$('#fp_id_cuenta').append(newOption);*/
					$('#fp_id_cuenta').append($("<option value='"+item.id+"' moneda ='"+item.id_moneda+"'>"+item.denominacion + " " + item.currency_code + " (" + item.numero_cuenta + ")</option>"));
				});
				$('#fp_id_cuenta').prop('disabled', false);
			} else {
				$('#fp_id_cuenta').empty();
				newOption = new Option('Seleccione cuenta', '', false, false);
				$('#fp_id_cuenta').append(newOption);
				$('#fp_id_cuenta').prop('disabled', true);
			}
		} //success
	}); //ajax


} //


/*=====================================================================================================
                                COTIZACIONES
=====================================================================================================*/
function setCotizar(monto = null, opt = 1) 
{

	let data = $('#formFp').serializeJSON({ customTypes: customTypesSerializeJSON});
	data ['id_recibo'] = operaciones.id_recibo;
	data ['opt'] = opt;
	data ['indice_cotizacion'] = $('#fp_indice_cotizacion').val();

	if(monto){
		data ['importe_pago'] = monto;
	}

	$.ajax({
		type: "GET",
		url: "{{route('setMontoCotizado')}}",
		data: data,
		dataType: 'json',
		error: function (jqXHR, textStatus, errorThrown) {

			msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo cotizar los montos.', 'error', jqXHR, textStatus);
			reject(false);
		},
		success: function (rsp) {
            //FUNCION PENDIENTE DE CONTINUAR, RECIBE Y DEVUELVE COTIZADO UN MONTO
               if(rsp.err){
					$('#total_real').val(rsp.total);
					$('#fp_importe_pago').val(rsp.total);
				} else {
					msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo cotizar los montos.', 'error');
					// reject(false);
				}
				

		} //success
	}); //ajax
} //

function getCotizacion() 
{
	// console.lgo('SE DISPARO GETCOTIZACION');
	return new Promise((resolve, reject) => { 
	let cotizacion;
	$.ajax({
		async: false,
		type: "GET",
		url: "{{route('getCotizacionFPR')}}",
		data: {
			id_moneda_costo: $('#fp_id_moneda').val(),
			id_recibo: operaciones.id_recibo
		},
		dataType: 'json',
		error: function (jqXHR, textStatus, errorThrown) {
			msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo obtener la cotización.', 'error', jqXHR, textStatus);
			reject(false);
		},
		success: function (rsp) {

			if (rsp.cotizacion > 0) {
				let cot = Number(rsp.cotizacion);


				if (cot > 0) {
					$('#fp_cotizacion').val(rsp.cotizacion);
					operaciones.cotizacion = rsp.cotizacion;
					$('#fp_cotizacion_actual').val(rsp.cotizacion); //Campo que conserva la cotizacion del dia
				
					if(operaciones.set_indice_cotizacion){
						console.log(rsp.indice_cotizacion);
                        $('#fp_indice_cotizacion').val(rsp.indice_cotizacion);
                    }
					resolve({resp:true});


				} else {
					msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.', 'info');
					reject(false);
				}
			} else {
				msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.', 'info');
				reject(false);
			}

		} //success
	});

	});//promise


} //

function getCotContable() 
{
	// console.lgo('SE DISPARO GETCOTIZACION');
	$.ajax({
		async: false,
		type: "GET",
		url: "{{route('getContContable')}}",
		dataType: 'json',
		data: {
			id_moneda: $('#fp_id_moneda').val(),
			id_recibo:operaciones.id_recibo
		},
		error: function (jqXHR, textStatus, errorThrown) {
			msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo obtener la cotización.', 'error', jqXHR, textStatus);

		},
		success: function (rsp) {
					$('#fp_cotizacion').val(rsp);

		} //success
	});

} //
/*=====================================================================================================
                                 RETENCION
=====================================================================================================*/

		var f_retencion = 
		{
			initEventos : function()
			{
				$('#tipo_retencion').unbind();



				function logica_select_retencion(){
						let opt = $('#tipo_retencion').val();	

						//RETENCIONES GENERADAS
						if( opt === '1'){

							$.when(f_retencion.load(1)).then((a)=>{ },(b)=>{});


							//RETENCIONES A GENERAR
							} else if(opt === '2'){

							$.when(f_retencion.load(1)).then((a)=>{ },(b)=>{});

							//RETENCIONES PENDIENTES	
							} else if(opt === '3'){
							
							//FACTURAS EN RECIBOS APLICADOS
							$.when(f_retencion.load(2)).then((a)=>{ },(b)=>{ });
							}
				}

		
				//OBTENER FACTURAS DE RETENCION
				// f_retencion.load(1);
				logica_select_retencion();

				//VERIFICA<<<
				$('#tipo_retencion').change(()=>{
					logica_select_retencion();
				});

	



			},
			load: function(opt)
			{	
				var newOption;

				return new Promise((resolve, reject) => { 
					//LIMPIAR MENSAJES DE RETENCION
					if(msj_retencion.reset){
						 msj_retencion.reset();	
					}

					$.ajax({
						async: false,
						type: "GET",
						url: "{{route('getFactRetencionRecibo')}}",
						data: {
							'id_recibo':operaciones.id_recibo,
							 'option': opt
						},
						dataType: 'json',
						error: function (jqXHR, textStatus, errorThrown) {

							msj_retencion = $.toast({
									heading: '<b>Error</b>',
									text: 'Ocurrió un error al intentar recuperar las retenciones.',
									position: 'top-right',
									hideAfter: false,
									icon: 'error'
								});

								$('#factura_retencion').empty();
								$('#factura_retencion').prop('disabled',true);
								reject();
						},
						success: function (rsp) {
							if(rsp.cant_fact > 0){
								$('#factura_retencion').empty();
								$.each(rsp.facturas, function (key, item) {
									newOption = new Option(item.nro_documento, item.id, false, false);
									$('#factura_retencion').append($(newOption).attr('data-valor',item.monto_retencion));
									
								});
								$('#factura_retencion').prop('disabled',false);
								resolve();
							} else {
								$('#factura_retencion').prop('disabled',true);

								msj_retencion = $.toast({
									heading: '<b>Error</b>',
									text: '<b>No tiene retenciones habilitadas para aplicar.</b>',
									position: 'top-right',
									hideAfter: false,
									icon: 'info'
								});
								reject();
								
							}


						} //success
					});

				});//promise
			},

		}//

	/*====================================================================================================
									LOAD COBRO
	=====================================================================================================*/
		function loadData(){
			dataString = {id_recibo:operaciones.id_recibo,
						 cotizacion :$('#fp_cotizacion').val()}
			$.ajax({
					type: "GET",
					url: "{{route('getFpRecibo')}}",
					data: dataString,
					async: false,
					dataType: 'json',
					error: function (jqXHR, textStatus, errorThrown) {
							msjToast('Ocurrió un al intentar recuperar los datos de la forma de pagos.', 'error', jqXHR, textStatus);
							eject(false);
					},
					success: function (rsp) {
							if (rsp.err == true) {
								if(rsp.data != null){
									cantItem = 0,
									diferencia = 0,
									total_bd = 0, 
									importe_pago = 0,
									classCompletado = '';
									$.each(rsp.data,(index,value)=>{
										//FORMATEO SEGUN LA MONEDA
										importe_pago = value.currency_code +' '+ formatCurrency(value.id_moneda, Number(value.importe_pago));
										classCompletado = Boolean(value.datos_completos) ? 'lineaCompleta' : '';
										tabla = `<tr id="rowPago_${cantItem}" class="tabla_filas ${classCompletado}">
												<td><input type="text" readonly value = "${value.forma_cobro_cliente_n}" class = "form-control input-md" /></td>
												<td><input type="text" readonly value = "${value.cotizacion_operativa}" class = "form-control input-md" /></td>
												<td><input type="text" readonly value = "${value.cotizacion_contable}" class = "form-control input-md" /></td>
												<td><input type="text" readonly value = "${importe_pago}" class="input_importe_pago form-control input-md"/></td>`;
												//SI ES RECIBO PENDIENTE
												if(operaciones.id_estado == 31){
													tabla += `
													<td>
														<button type="button" onclick="eliminar(${cantItem})" data-id=${value.id}  
														class="btn btn-danger mr-1"><b>Eliminar</b></button> 
													</td>`;
												} else {
													tabla += `
													<td>
													</td>`;
												}
										
										if(value.id_tipo_pago != 9){
											if(value.id_tipo_pago == 5 & operaciones.id_estado == 40){
												tabla += `
													<td>
														<button type="button" onclick="edit(${cantItem},${value.id})" 
														class="btn btn-info mr-1"><b>Editar</b></button>
													</td>`;
											}else{
												tabla += `<td></td>`;
											}
											tabla += ``;
										} else{
											tabla += `<td>
														<button type="button" disabled class="btn btn-danger"><b>AUTOMATICO</b></button>
													</td> `;
										}
										tabla += `</tr>`;
										$('#formFormaPago tbody').append(tabla);
										total_bd += clean_num(value.importe_pago,true);
										cantItem++;

									});	
									operaciones.diferencia = rsp.diferencia
									$('#fp_diferencia_pago').val(operaciones.moneda_code +' '+ formatCurrency(operaciones.id_moneda_recibo, operaciones.diferencia));
									$('.importe_a_pagar').val(operaciones.diferencia);
									formatearNumTabla();
									//resolve({resp:true});
								}	else {
									//NO EXISTE FP 
									reject(false);
								}
								
							}//IF ERR

						} //success
					}) //ajax
			};
	/*=====================================================================================================
									CANCELAR COBRO 
	=====================================================================================================*/
			function cancel(row_id, id_detalle) 
			{	
				$('#formFormaPago tbody').html(``);
				//this.loadData();
				loadData();
			}

	/*=====================================================================================================
									INSERTAR COBRO 
	=====================================================================================================*/
			function insert(){
				moneda_cuenta = $("#fp_id_cuenta :selected").attr('moneda');
				if(moneda_cuenta == 0 || moneda_cuenta === null || jQuery.isEmptyObject(moneda_cuenta) == true){
					indicador = 1;
				}else{
					if (parseInt($("#fp_id_cuenta :selected").attr('moneda')) == parseInt($('#fp_id_moneda').val())) {
						indicador = 1;
					}else{
						indicador = 0;
					}   
				}

				if(indicador == 1){
					ok = 0, 
					id_fp_detalle= 0;
					cantItem = 0;
					fp_diferencia = 0;
					importe_pago = $('#fp_importe_pago').val();
					id_moneda =Number( $('#fp_id_moneda').val());
					txt_moneda = $('#fp_id_moneda option:selected').text();
					dataString = $('#formFp').serializeJSON({ customTypes: customTypesSerializeJSON});
					dataString['id_recibo'] = operaciones.id_recibo;
					dataString['id_cuenta_contable'] = $('#id_cuenta_contable').val();
					dataString['id_cta_banco'] = $('#id_cta_banco').val();
					dataString['total_real'] = /*clean_num(*/$('#total_real').val()/*)*/;
					dataString['id_moneda'] = $('#fp_id_moneda').val();
					dataString['tipo'] = $('select[name="id_tipo_operacion"] option:selected').attr('abreviatura');
					dataString['indice_cotizacion'] = $('#fp_indice_cotizacion').val();
					dataString['num_operacion'] = $('#num_operacion').val();

					$.ajax({
							type: "GET",
							url: "{{route('insertFpRecibo')}}",
							data: dataString,
							async: false,
							cache:false,
							dataType: 'json',
							error: function (jqXHR, Status, errorThrown) {
									msjToast('Ocurrió un error en la comunicación con el servidor.', 'error');
									ok++;
									// reject('Ocurrió un error en la comunicación con el servidor');
							},
							success: function (rsp) {
									if (rsp.err) {
										id_fp_detalle = rsp.id_fp_detalle;
										// msjToast('<b>Linea creada con éxito.</b>', 'success');
										operaciones.diferencia = rsp.diferencia
										$('#fp_diferencia_pago').val(operaciones.moneda_code +' '+ formatCurrency(operaciones.id_moneda_recibo, operaciones.diferencia));
										$('.importe_a_pagar').val(formatCurrency(operaciones.diferencia));
										//HABILITAR CAMPOS EN READONLY POR OPERACION CANJE
										$('#fp_cotizacion').prop('readonly',false);
										$('#fp_id_moneda').prop('disabled',false);
										msjToast('<b>Linea creada con éxito.</b>', 'success');
									} else {
										$.toast({
											heading: '<b>Error</b>',
											text: rsp.msj,
											position: 'top-right',
											hideAfter: false,
											// width: '400px',
											icon: 'error'
										});
										ok++;
										// reject(rsp.msj);
									}

								} //success
							}); //ajax

							if (ok == 0) {
								importe_pago = txt_moneda +' '+formatCurrency(id_moneda,clean_num(importe_pago));
								/*{{--INSERTAR FILA--}}*/
								cantItem = $('#listadoPagos tr.tabla_filas').length;
								let cotizacion = $('#fp_cotizacion').val();
								let cotizacion_actual = $('#fp_cotizacion_actual').val();
								
								tabla = `
										<tr id="rowPago_${cantItem}" class="tabla_filas">
											<td><input type="text" readonly value = "${$('#fp_tipo_operacion :selected').text()}" class = "form-control input-md" /></td>
											<td><input type="text" readonly value = "${cotizacion_actual}" class = "form-control input-md" /></td>
											<td><input type="text" readonly value = "${cotizacion}" class = "form-control input-md" /></td>
											<td><input type="text" readonly value = "${importe_pago}" class = "input_importe_pago form-control input-md"/></td>
											<td><button type="button" onclick="eliminar(${cantItem})" data-id=${id_fp_detalle}  class="btn btn-danger"><b>Eliminar</b></button>
											</td>
										</tr>
										`;
										// <td><input type="text" readonly value = "${$('#fp_id_moneda :selected').text()}" class = "form-control input-md"/></td>
								if($('.tbody_fp tr td').hasClass('relleno_fp')){
									$('.tbody_fp').html('');
								}
								$('#listadoPagos tbody').append(tabla);
								ordenar();//ORDENAR LA TABLA AL INSERTAR
								//LIMPIAR CAMPOS
								$('#fp_tipo_operacion').val('').trigger('change.select2');
								$('#tipo_operacion_div').html(' ');
								formatearNumTabla();
						}
				}else{
					$.toast({
								heading: '<b>Error</b>',
								text: 'La moneda de la Cuenta de Fondo no corresponde a la moneda del Recibo',
								position: 'top-right',
								hideAfter: false,
								icon: 'error'
							});
				}
			}
	/*=====================================================================================================
									ELIMINAR COBRO 
	=====================================================================================================*/

		function eliminar(row_id)
		{
				var dataString = '&id_fp_detalle='+$('#rowPago_'+row_id).find('button').attr('data-id');
					dataString += '&id_recibo='+operaciones.id_recibo;  

				ok = 0;

				$.ajax({
						type: "GET",
						url: "{{route('deletePago')}}",
						cache:false,
						data: dataString,
						///async: false,
						dataType: 'json',
						error: function (jqXHR, textStatus, errorThrown) {
							msjToast('Ocurrió al intentar eliminar la línea.', 'error');
							ok++;
						},			
						success: function (rsp) {
							if (rsp.err == false) {
								msjToast('Ocurrió al intentar eliminar la línea.', 'error');
								ok++;
							} else {
								msjToast('Línea eliminada.', 'info');
								operaciones.diferencia = rsp.diferencia
								$('#fp_diferencia_pago').val(formatCurrency(operaciones.id_moneda_recibo, operaciones.diferencia));
								$('.importe_a_pagar').val(operaciones.diferencia);
							}
					} //success
				}).done(()=>{
					if(ok == 0){
						$('#rowPago_' + row_id).remove();
						ordenar(); //ORDENAR AL ELIMINAR
					}//if
				}) //ajax

			}

	/*=====================================================================================================
									GUARDAR COBRO 
	=====================================================================================================*/
			function save(row_id, id_detalle){
				dataString = $('#fila_edit').serializeJSON({ customTypes: customTypesSerializeJSON});
				dataString['id_detalle'] = id_detalle;
				dataString['indice_cotizacion'] = $('#fp_indice_cotizacion').val();
				$.ajax({
						type: "GET",
						url: "{{route('saveFpRecibo')}}",
						cache:false,
						data: dataString,
						async: false,
						dataType: 'json',
						error: function (jqXHR, textStatus, errorThrown) {
								msjToast('Ocurrió un al intentar almacenar los datos', 'error', jqXHR, textStatus);
								reject(false);
						},
						success: function (rsp) {
								cancel();
								///forma_cobro.cancel();
								resolve();
						} //success
				}) //ajax
			}
	/*=====================================================================================================
									ORDENAR COBRO 
	=====================================================================================================*/

			function ordenar(){
				//ORDENA LA TABLA AL ELIMINAR O INSERTAR
				var cont = 0;
				$.each($('.tabla_filas'), function (index, value) {
					var num_cant = value.id.split('rowPago_');
					if (num_cant[1] != cont) {
						$('#' + value.id).attr('id', 'rowPago_' + cont);
						//MODIFICAR ONCLICK
						$(value.children[2]).find('button').attr('onclick', 'eliminar(' + cont + ')');
					}
					cont++;
				});
			}

	/*=====================================================================================================
									EDITAR COBRO 
	=====================================================================================================*/
			function edit(row_id,id_detalle){
  				dataString = {
								id_recibo:operaciones.id_recibo,
								id_detalle : id_detalle
							}
						
				$.ajax({
						type: "GET",
						cache:false,
						url: "{{route('getEditFpRecibo')}}",
						data: dataString,
						async: false,
						dataType: 'json',
						error: function (jqXHR, textStatus, errorThrown) {
									msjToast('Ocurrió un al intentar recuperar los datos de la forma de pago.', 'error', jqXHR, textStatus);
									reject(false);
						},
						success: function (rsp) {
 								forma_cobro = rsp.data[0];	
								fila_cobro = $('#rowPago_'+row_id);
								tipo_operacion = forma_cobro.id_tipo_pago;
								fila_cobro.html(''); //GRILLA A EDITAR DE COBRO RETENCION
								$('#tipo_operacion_div').html(' '); //FORMAS DE COBRO A INSERTAR ELIMINAR PARA EVITAR CONFLICTO
								$('#fp_tipo_operacion').val('').trigger('change.select2');

								retencion = `<td colspan="5">
									<form id="fila_edit">
										<div class="row">

											<div class="col-12 col-sm-6">
												<div class="form-group">
													<label>Tipo Retención</label> 						 
													<select class="form-control custom-select" name="id_tipo_retencion" id="tipo_retencion" style="width: 100%;">		
														<option value="1">Retenciones generadas</option>
														<option value="2">Retenciones a generar</option>
														<option value="3">Retenciones pendientes</option>
													</select>
												</div> 
											</div>

											<div class="col-12 col-sm-6">
												<div class="form-group">
													<label>Factura</label> 						 
													<select class="form-control custom-select" value="" name="id_factura_retencion" id="factura_retencion" style="width: 100%;">				
													</select>
												</div> 
											</div>

											
											<div class="col-12 col-sm-6">
												<div class="form-group">
													<label>Fecha Documento</label>						 
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text"><i class="fa fa-calendar"></i></span>
														</div>
															<input type="text"  class="form-control single-picker" value="${forma_cobro.fecha_documento_format}" maxlength="10" id="fecha_retencion" data-value-type="s_date" name="fecha_documento" />
													</div>
												</div>	
											</div>


											<div class="col-12 col-sm-6">
												<div class="form-group">
													<label>Numero Retención </label>
													<input type="text" class="form-control" name="num_retencion" maxlength="30" value="${ (Boolean(forma_cobro.num_retencion)) ? forma_cobro.num_retencion : ''}" id="fp_documento_forma_pago" placeholder="Documento">
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label>Importe Pago *</label>
													<div class="input-group">
														<input type="text" class="form-control font-weight-bold" value="${forma_cobro.importe_pago}" readonly maxlength="15" id="importe_edit_retencion" data-value-type="convertNumber" name="importe_pago">
														<div class="input-group-append">
															<button class="btn btn-primary" disabled type="button"><i class="fa fa-plus fa-lg"></i></button>
														</div>
													</div>
												</div>
											</div>

											<div class="col-12">
												<button type="button" onclick="forma_cobro.cancel(${row_id},${id_detalle})"  class="btn btn-danger"><b>Cancelar</b></button>
												<button type="button" onclick="save(${row_id},${id_detalle})"  class="btn btn-success pull-right"><b>Guardar</b></button>
											</div>
										</div>   
									</form>
								</td>`;

								//CARGAR FORMULARIO
								fila_cobro.html(retencion);
								// f_retencion.initEventos();
								initCalendar();
								f_retencion.initEventos();
								$('#factura_retencion').val(forma_cobro.id_lv_retencion).trigger('change.select2');
								//resolve();
							} //success
						}) //ajax
			}
	/*=====================================================================================================
									VALIDAR COBRO 
	=====================================================================================================*/
			function validar_retencion(){
				dataString = {
						'id_recibo':operaciones.id_recibo
				}

				$.ajax({
						type: "GET",
						cache:false,
						url: "{{route('verificarRetenciones')}}",
						data: dataString,
						async: false,
						dataType: 'json',
						error: function (jqXHR, textStatus, errorThrown) {
							modalErrorValidarFp();
						},
						success: function (rsp) {
								if(rsp.estado_err){

									if(rsp.continuar){
										forma_cobro.validar();
									} else {
										modalAvisoRetencion();
									}
								} else {
									// reject();
									modalErrorValidarFp();
								}
						} //success


							}).done(()=>{
								resolve(true);
							}); //ajax
			}	
	/*=====================================================================================================
									VALIDAR COBRO 
	=====================================================================================================*/
			function initEventos(){

				$('#fp_id_origen_pago').unbind();
				$('#btnAddFormaPago').unbind();
				$('#fp_id_origen_pago').change(function () {
						getCuentas();
				});
				$('#btnAddFormaPago').click(function(){
						insert();
				});
				setCotizar(operaciones.diferencia);
				/*$.when(setCotizar(operaciones.diferencia))
								.then((a)=>{ 
									$('.importe_a_pagar').val(a);
								},(b)=>{
									$('.importe_a_pagar').val(0);
							});*/
			}


	/*=====================================================================================================
									FORMA DE COBRO 
	=====================================================================================================*/

		var forma_cobro = 
		{

			/*initEventos : function()
			{
						$('#fp_id_origen_pago').unbind();
						$('#btnAddFormaPago').unbind();
						
						$('#fp_id_origen_pago').change(function () {
							console.log('ORIGEN BANCO');
							getCuentas();
						});

						$('#btnAddFormaPago').click(function(){
							console.log('EVENTO CLICK INSERTAR FORMA PAGO');
							//cargarConcepto();
							//forma_cobro.insert();
							insert();
						});
						$.when(setCotizar(operaciones.diferencia))
								.then((a)=>{ 
									$('.importe_a_pagar').val(a);
								},(b)=>{
									$('.importe_a_pagar').val(0);
								});

						

			},*/
			validar : function()
			{
				return new Promise((resolve, reject) => { 

						//VALIDAR FORMA COBRO SI EXISTE
						//LUEGO VALIDAR DIFERENCIA

						let dataString = {
							'id_recibo':operaciones.id_recibo
						}

						$.ajax({
						type: "GET",
						url: "{{route('validarFormaPago')}}",
						data: dataString,
						async: false,
						cache:false,
						dataType: 'json',
						error: function (jqXHR, textStatus, errorThrown) {
							modalErrorValidarFp();
							//  reject('');
						},
						success: function (rsp) {
							if(rsp.err){
									//CASO DIFERENCIA > 1
									if(rsp.option == 1){
										// modalRechazoDiferencia();
										modalErrorValidarFp(rsp.txt);
									//CASO DIFERENCIA < 1	
									} else if(rsp.option == 2){
										modalAvisoDiferencia();
									} else if(rsp.option == 3){
										modalConfirmarCobro();
									} else if(rsp.option == 4){
										modalAvisoRetencion();
									} else if(rsp.option == 5){
										modalAviso();
									} else {
										modalErrorValidarFp();
										// reject();
									}

							} else {
								// reject();
								modalErrorValidarFp();
							}
							
							
							} //success


							}).done(()=>{
								resolve(true);
							}); //ajax
						})//promise

			},
			validar_retencion : function()
			{
				return new Promise((resolve, reject) => { 
					forma_cobro.validar();
					resolve(true);

						// let dataString = {
						// 	'id_recibo':operaciones.id_recibo
						// }

						// $.ajax({
						// type: "GET",
						// url: "{{route('verificarRetenciones')}}",
						// data: dataString,
						// async: false,
						// cache:false,
						// dataType: 'json',
						// error: function (jqXHR, textStatus, errorThrown) {
						// 	modalErrorValidarFp();
						// },
						// success: function (rsp) {
						// 		if(rsp.estado_err){

						// 			if(rsp.continuar){
						// 				forma_cobro.validar();
						// 			} else {
						// 				modalAvisoRetencion();
						// 			}

						// 		} else {
						// 			// reject();
						// 			modalErrorValidarFp();
						// 		}
							
						// 	} //success


						// 	}).done(()=>{
						// 		resolve(true);
						// 	}); //ajax
						});//promise
			},	

		}//

		
	/*=====================================================================================================
									MODALES
	=====================================================================================================*/
				function modalAvisoDiferencia()
				{
					if($('#fp_id_origen_pago').val() == 0){
						title = "Diferencia de Cobro";
						text = "Existe una diferencia entre lo cobrado y el total de recibo. Si decide continuar se va a registrar una diferencia!";
					}else{
						title = "Diferencia de Cobro y sin Adjunto";
						text = "Existe una diferencia entre lo cobrado y el total de recibo. Si decide continuar se va a registrar una diferencia y sin Adjuntos !";

					}
					swal({
							title: title,
							text: text,
							icon: "warning",
							showCancelButton: true,
							buttons: {
								cancel: {
									text: "No, Cancelar",
									value: null,
									visible: true,
									className: "btn-warning",
									closeModal: false,
								},
								confirm: {
									text: "Sí, Continuar",
									value: true,
									visible: true,
									className: "",
									closeModal: false
								}
							}
						}).then(isConfirm => {
							
							if (isConfirm) {
								$.when(aplicarRecibo()).then((a)=>{ 
									swal("Éxito",a , "success");
									setTimeout( redirectReciboControl,1000);
								},(b)=>{
									swal("Cancelado", b, "error");
								});
								
							} else {
								swal("Cancelado", "La operación fue cancelada", "error");
							}
						});
				}

				function modalRechazoDiferencia()
				{
					swal("Atención!", "La diferencia es mayor al permitido (1 USD), no se puede cobrar !", "warning");
				}

				function modalAvisoRetencion(){

					swal({
							title: "Atención!",
							text: "Existen retenciones sin aplicar. ¿Desea continuar?",
							icon: "warning",
							showCancelButton: true,
							buttons: {
								cancel: {
									text: "No, Cancelar",
									value: null,
									visible: true,
									className: "btn-warning",
									closeModal: false,
								},
								confirm: {
									text: "Si , Continuar",
									value: true,
									visible: true,
									className: "",
									closeModal: false
								}
							}
						}).then(isConfirm => {
							
							if (isConfirm) {
								forma_cobro.validar();
							} else {
								swal("Cancelado", "La operación fue cancelada", "error");
							}
						});
				}

				function modalErrorValidarFp(txt = null){
					if(txt){
						swal("Error!",txt ,"error");
					} else {
						swal("Error!", "Ocurrió un error al intentar validar y cobrar!","error");
					}

				}

				function modalConfirmarCobro()
				{
					if($('#fp_id_origen_pago').val() == 0){
						title = "Confirmar Cobro";
						text = "¿Está seguro que desea realizar el cobro?";
					}else{
						title = "Confirmar Cobro sin Adjunto";
						text = "¿Está seguro que desea realizar el cobro, sin agregar Adjuntos?";

					}
					swal({
							title: title,
							text: text,
							icon: "warning",
							showCancelButton: true,
							buttons: {
								cancel: {
									text: "No, Cancelar",
									value: null,
									visible: true,
									className: "btn-warning",
									closeModal: false,
								},
								confirm: {
									text: "Si , Continuar",
									value: true,
									visible: true,
									className: "",
									closeModal: false
								}
							}
						}).then(isConfirm => {
							
							if (isConfirm) {
								$.when(aplicarRecibo()).then((a)=>{ 
									swal("Éxito",a , "success");
									setTimeout( redirectReciboControl,1000);
								},(b)=>{
									swal("Cancelado", b, "error");
								});
								
							} else {
								swal("Cancelado", "La operación fue cancelada", "error");
							}
						});
				}


/*======================================================================
                    AUX CLASS
======================================================================*/


function BlockFp(){
			fp.activo = false;
			 $('#btnAddFormaPago').prop('disabled',true);
			 $('#tipo_operacion').prop('disabled',true);
		 }	

function UnblockFp(){
			fp.activo = true;
			 $('#btnAddFormaPago').prop('disabled',false);
			 $('#tipo_operacion').prop('disabled',false);
		 }	


 function initCalendar() {

	$('.single-picker').datepicker({
		format: "dd/mm/yyyy",
		language: "es"
	});
}




//Para almacenar mensajes o mostrar mensajes en el momento segun error del ajax
function msjToast(msj, opt = '', jqXHR = false, textStatus = '') {

const aux = {
    msj: []
} 

if (msj != '') {
    aux.msj.push('<b>' + msj + '</b>');
}

if (opt === 'info') {
    $.toast().reset('all');

    $.toast({
        heading: '<b>Atención</b>',
        position: 'top-right',
        text: aux.msj,
        // width: '400px',
        hideAfter: false,
        icon: 'info'
    });
    aux.msj = [];

} else if (opt === 'error') {
    $.toast().reset('all');



    if (jqXHR === false && textStatus === '') {
        msj = aux.msj;
    } else {

        errCode = jqXHR.status;
        errMsj = jqXHR.responseText;

        msj = '<b>';
        if (errCode === 0)
            msj += 'Error de conectividad a internet, verifica tu conexión.';
        else if (errCode === 404)
            msj += 'Error al realizar la solicitud.';
        else if (errCode === 500) {
            if (aux.msj.length > 0) {
                msj += aux.msj;
            } else {
                msj += 'Ocurrió un error en la comunicación con el servidor.'
            }

        } else if (errCode === 406) {
            msj += errMsj;
        } else if (errCode === 422)
            msj += 'Complete todos los datos requeridos'
        else if (textStatus === 'timeout')
            msj += 'El servidor tarda mucho en responder.';
        else if (textStatus === 'abort')
            msj += 'El servidor tarda mucho en responder.';
        else if (textStatus === 'parsererror') {
            msj += 'Error al realizar la solicitud.';
        } else
            msj += 'Error desconocido, póngase en contacto con el área técnica.';
        msj += '</b>';
    }


    $.toast({
        heading: '<b>Error</b>',
        text: aux.msj,
        position: 'top-right',
        hideAfter: false,
        // width: '400px',
        icon: 'error'
    });

    aux.msj = [];
} else if (opt === 'success') {
    $.toast().reset('all');

    $.toast({
        heading: '<b>Éxito</b>',
        text: aux.msj,
        position: 'top-right',
        hideAfter: false,
        // width: '400px',
        icon: 'success'
    });
    aux.msj = [];

}

} //




function formatMoneyFp() {
let moneda = $('#fp_id_moneda').val();
$('.control-num-moneda-fp').unbind();
$('.control-num-moneda-fp').inputmask('remove');

if (moneda == '111') {
    $('.control-num-moneda-fp').inputmask("numeric", {
        radixPoint: ",",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        rightAlign: false
    });

} else {

    $('.control-num-moneda-fp').inputmask("numeric", {
        radixPoint: ",",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        rightAlign: false
    });



} //else



} //	

/*
FORMATEA SEGUN LA MONEDA DEL RECIBO
*/
function formatearNumTabla(){
	 
	if(operaciones.id_moneda_recibo === 111){

		$('.format-number-fp').inputmask("numeric", {
        radixPoint: ",",
        groupSeparator: ".",
        digits: 0,
        autoGroup: true,
        rightAlign: false
	});
	
	} else {
		$('.format-number-fp').inputmask("numeric", {
        radixPoint: ",",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        rightAlign: false
	});	
	}
   
}

			/* ================================================================================
										FORMATEO DE VALORES
				================================================================================*/


			//DEFINIR FORMATO MONEDAS		
			const EURO = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const DOLAR = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });



	
				function formatCurrency(id_moneda, value){
					
					let importe = 0;
	

					// console.log('formatCurrency', id_moneda, value);

							//PYG
							if(id_moneda == 111){
								importe = GUARANI(value,{ formatWithSymbol: false }).format(true);
							} 
							//USD
							if(id_moneda == 143) {
								importe = DOLAR(value,{ formatWithSymbol: false }).format(true);
							}
							if(id_moneda == 43) {
								importe = EURO(value,{ formatWithSymbol: false }).format(true);
							}


					return importe;		
				}



				function clean_num(n, bd = false) {

						n = n.toString()

						if (n && bd == false) {
							n = n.replace(/[,.]/g, function (m) {
								if (m === '.') {
									return '';
								}
								if (m === ',') {
									return '.';
								}
							});
							return Number(n);
						}
						if (bd) {
							return Number(n);
						}
					return 0;
				}	

			$('#fp_id_moneda option[value="{{$datos[0]->id_moneda}}"]').attr("selected",true)
			$('.numerico').inputmask("numeric", {
									radixPoint: ",",
									groupSeparator: ".",
									digits: 2,
									autoGroup: true,
									rightAlign: false
								});	

	 $(document).ready(function(){
          var options = { 
                        beforeSubmit:  showRequest,
                success: showResponse,

            dataType: 'json' 
                }; 
          $('body').delegate('#image','change', function(){
            $('#upload').ajaxForm(options).submit();      
          });

      })


      function showRequest(formData, jqForm, options) { 
			$("#validation-errors").hide().empty();
			$("#output").css('display','none');
			return true; 
      } 

      function showResponse(response, statusText, xhr, $form)  { 
            if(response.success == false)
            {
              var arr = response.errors;
              $.each(arr, function(index, value)
              {
                if (value.length != 0)
                {
                  $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                }
              });
              $("#validation-errors").show();
            } else {
			   cantidad = parseInt($("#cantidad_adjunto").val());
			   total_cantidad = cantidad + 1;
			   $("#cantidad_adjunto").val(total_cantidad);
			   espacio = response.archivo;
			   extension = espacio.split('.');
               $("#imagen").val(response.archivo);  
			   rutaImagen = "{{asset('images/file.png')}}";
			   	if(extension[1] == 'pdf'){
			   		$("#output").append('<div class="col-sm-2" id="cuadro_'+response.numero+'"><a href="'+response.file+'" target="_blank"><img class="img-responsive" style="width:120px; height:120px;" src="'+rutaImagen+'" alt="Photo"></a><button type="button" id="btn-imagen" data-id="'+response.numero+'" onclick="eliminarImagen('+response.numero+')"><i class="ft-x-circle" style="font-size: 18px;"></i></button></div>');
				}else{
					$("#output").append('<div class="col-sm-2" id="cuadro_'+response.numero+'"><a href="'+response.file+'" target="_blank"><img class="img-responsive" style="width:120px; height:120px;" src="'+response.file+'" alt="Photo"></a><button type="button" id="btn-imagen" data-id="'+response.numero+'" onclick="eliminarImagen('+response.numero+')"><i class="ft-x-circle" style="font-size: 18px;"></i></button></div>');
				}
			   $("#output").css('display','block');
            }
       }

      function eliminarImagen(id){
          dataString = "file="+id;
          $.ajax({
              type: "GET",
              url: "{{route('fileAdjuntoRecibo')}}",//fileDelDTPlus
              dataType: 'json',
			  cache:false,
              data: dataString,
              success: function(rsp){
					$("#imagen").val("");
					$("#cuadro_"+id).remove();
					cantidad = parseInt($("#cantidad_adjunto").val());
					total_cantidad = cantidad - 1;
					$("#cantidad_adjunto").val(total_cantidad);
              }

        });
      } 




	</script>	
@endsection
