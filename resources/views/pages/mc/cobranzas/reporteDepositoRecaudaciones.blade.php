@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
			.card,.card-header {
					border-radius: 14px !important;
					}
            .inputStyle{
                border: 0px;
                background-color:white;
                text-align: center;
            }    

            .select2{
                    width: 100% !important;
                } 

   .btnExcel  {
	display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

    #botonExcelDepositoDetalle {
		display: inline;
	}
	#botonExcelDepositoDetalle .dt-buttons {
		display: inline;
	}
       
	</style>
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Reporte Deposito Recaudaciones</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
                


                    <ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="tabHome" data-toggle="tab" aria-controls="tabIcon21" href="#home" role="tab" aria-selected="true">
                                Deposito</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tabDetalle" data-toggle="tab" aria-controls="tabIcon22" href="#tabDetalleDeposito" role="tab" aria-selected="false">
                                Deposito Detalle</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <section id="home"  class="tab-pane active" role="tabpanel">
                            <form class="row mt-1" id="formTransferencia" autocomplete="off">
                
                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label>Cuenta Banco</label>
                                        <select class="form-control select2" name="id_banco_detalle" id="cuentaBanco" required>
                                            
                                            <option value="">Todos</option>
                                            @foreach($bancoDetalle as $banco)
                                        <option value="{{$banco->id}}" data-currency_code="{{$banco->currency['currency_code']}}" data-id_currency="{{$banco->id_moneda}}">{{$banco->banco_cab['nombre']}} - {{$banco->tipo_cuenta['denominacion']}} {{$banco->numero_cuenta}} {{$banco->currency['currency_code']}} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                    <div class="col-12 col-sm-3 col-md-4">
						<div class="form-group">
							<label>Moneda</label>
							<select class="form-control select2" name="id_moneda" id="id_moneda">
								<option value="">Todos</option>
							    @foreach($currency as $curr)
									<option value="{{$curr->currency_id}}">{{$curr->currency_code}}</option>
								@endforeach 

							</select>
						</div>
                    </div>
                    


                    <div class="col-12 col-sm-3 col-md-4">
                        <div class="form-group">
                            <label>Desde - Hasta Fecha de Emisión</label>
                            <div class="input-group">
                                <div class="input-group-prepend click_celandar">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control pull-right calendar"
                                    tabindex="6" name="periodo_emision:p_date" id="periodo_emision" value="" maxlength="10">
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mb-1">
                        <button type="button" onclick="limpiarTabHome()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
                        <button type="button"  onclick="tableCabecera()" class="btn btn-info pull-right btn-lg mr-1"><b>Buscar</b></button>
                    </div>

                </form>


               

                <div class="table-responsive table-bordered">
					<table id="listadoCabecera" class="table" style="width:100%">
						<thead>
							<tr>
                                <th>Nro Deposito</th>
                                <th>Fecha</th>
                                <th>Nro Boleta</th>
                                <th>Banco</th>
                                <th>Tipo Cuenta</th>
                                <th>Num. Cuenta</th>
                                <th>Moneda</th>
                                <th>Total Deposito</th>
                                <th></th>
							</tr>
						</thead>
						<tbody style="text-align: center">

						</tbody>
					</table>
                </div>


       </section>



       <section id="tabDetalleDeposito"  class="tab-pane" role="tabpanel">

        <form id="formDepositoDetalle" class="row">
            <div class="col-10 mt-1 mb-1">
                <div id="botonAsiento" class="pull-right mt-1 mb-1"></div>
            </div>
            <div class="col-1 mt-1 mb-1">
                <div id="botonExcelDepositoDetalle" class="pull-right mt-1 mb-1"></div>
            </div>

            <input type="hidden" name="id_cabecera" value="" id="id_detalle_deposito">
        </form>
            
            <div class="table-responsive table-bordered">
                <table id="listadoDetalle" class="table" style="width:100%">
                    <thead>
                        <tr>
                            <th>Num. Recibo</th>
                            <th>Forma Cobro</th>
                            <th>Moneda</th>
                            <th>Importe</th>
                            <th>Banco</th>
                            <th>Fecha Hora</th>
                        </tr>
                    </thead>
                    <tbody style="text-align: center">

                    </tbody>
                </table>
            </div>
        
        
            </section>
      </div>


			</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
    @include('layouts/gestion/scripts')
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
    <script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	<script>

var tableCab;
var tableDetalle;

$(()=>{ 
    $('.select2').select2();
    tableCab  = tableCabecera();
    initCalendar();

 });

        //DEFINIR FORMATO MONEDAS		
        const EURO = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
		const DOLAR = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
		const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });


 {{--==============================================
			FILTROS BUSQUEDA
	============================================== --}}
	function initCalendar(){
	{{--CALENDARIO OPCIONES EN ESPAÑOL --}}
	let locale = {
				format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};


	$('.calendar').daterangepicker({timePicker24Hour: true,
									timePickerIncrement: 30,
									autoUpdateInput: false,
									locale: locale
								    			});
	$('#periodo_emision').val('');
	$('#periodo_emision')
	.on('cancel.daterangepicker', function(ev, picker) {
				  $(this).val('');})
	.on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
	});		
		


     }









function tableCabecera() {


    let var_destroy;
    let form = $('#formTransferencia').serializeJSON({
        customTypes: customTypesSerializeJSON
    });

    tableCab = $("#listadoCabecera").dataTable({
        destroy: true,
        ajax: {
            url: "{{route('ajaxreporteDepositoCabecera')}}",
            data: form,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERrror');
            }

        },
        "columns": [
                { data: (x)=> {
                    var_destroy = '';
                    if(Boolean(x.nro_deposito)){
                        var_destroy = x.nro_deposito;
                    }
                    return var_destroy;
                }},
                {data:'fecha_emision_format'},
                {data:'nro_operacion'},
                { data: (x)=> {
                    var_destroy = '';
                    if(Boolean(x.banco_nombre)){
                        var_destroy = x.banco_nombre;
                    }
                    return var_destroy;
                }},
                {data: (x)=>{
                    var_destroy = '';
                    if(Boolean(x.tipo_cuenta)){
                        var_destroy = x.tipo_cuenta;
                    }
                    return var_destroy;
                }},
                {data : (x)=>{
                    var_destroy = '';
                    if(Boolean(x.numero_cuenta)){
                        var_destroy = x.numero_cuenta;
                    }
                    return var_destroy;
                }},
                {data : (x)=>{
                    var_destroy = '';
                    if(Boolean(x.moneda)){
                        var_destroy = x.moneda;
                    }
                    return var_destroy;
                }},
                {data : (x)=>{
                    var_destroy = '';
                    if(Boolean(x.total_deposito)){
                        var_destroy = formatter.format(parseFloat(x.total_deposito));
                    }
                    return var_destroy;
                }},
                {data : function(x){
                            let btn =  `<button onclick="tabDetalleDeposito(${x.id})" type="button" id="btn-${x.id}" class="btn btn-sm btn-danger" >
                                <i class="fa fa-fw fa-search"></i></button>`;

                                if(x.adjunto){
                                     btn = btn +  `  <a href="{{url('comprobante_depositos')}}/${x.adjunto}" target="_blank" class="btn btn-sm btn-primary" >
                                    <i class="fa fa-fw fa-file"></i></a>`;
                                }

                                return btn;
                    }}


            ],
            "order": [[ 0, "desc" ]],
            "createdRow": function (row, data, iDataIndex) {


			 	}
    });

}

        function tabDetalleDeposito(id){
            console.log('=====>',id);
            $('#id_detalle_deposito').val(id);
            dataTableDetalle();
            $('#tabDetalle').tab('show');
            $.ajax({
                    type: "GET",
                    url: "{{route('getDesposito')}}",
                    data:{ deposito_id: id} ,
                    dataType: 'json',
                    error: function(jqXHR,textStatus,errorThrown){
                    },
                    success: function(rsp){
                        console.log(rsp);
                        asiento = rsp.resultado[0].id;
                        nro_documento = rsp.resultado[0].nro_documento;
                        origen_asiento = rsp.resultado[0].id_origen_asiento;
                        $('#botonAsiento').html(`<a href="{{route('reporteAsiento')}}?id_detalle_asiento=${asiento}&nro_documento=${nro_documento}&id_origen_asiento=${origen_asiento}"
							class="btn_op btn_asiento text-center btn btn-info btn-lg pull-right"
							style="margin-right: 20px;" role="button"><b>Ver Asiento</b></a>`);



                    }   
                })

        }
        

        function hola(){
            console.log('hola');
        }

            function dataTableDetalle() {


                $.blockUI({
												centerY: 0,
												message: "<h2>Procesando...</h2>",
												css: {
													color: '#000'
												}
											});

				// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
				setTimeout(function (){

                console.log('tableDetalle========>');
            let form = $('#formDepositoDetalle').serializeJSON({
                customTypes: customTypesSerializeJSON
            });

            tableDetalle = $("#listadoDetalle").DataTable({
                destroy: true,
                ajax: {
                    url: "{{route('ajaxreporteDepositoDetalle')}}",
                    data: form,
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERrror');
                    }

                },
                "columns": [
                       {data : 'nro_recibo'},
                       {data : 'tipo_cobro_n'},
                       {data : 'currency_code'},
                      // {data :(x)=>{ return formatCurrency(x.id_moneda,x.importe_cobro);}},
                       {data:function(x){
                            if(jQuery.isEmptyObject(x.importe_deposito) == false){
                                importe = formatter.format(parseFloat(x.importe_deposito));
                            }else{
                                importe = formatter.format(parseFloat(x.importe_cobro));
                            }
                            return importe;
				        }},
                       {data : 'banco_n'},
                       {data : 'fecha_hora_format'},

                    ],
                    "createdRow": function (row, data, iDataIndex) {
                        console.log(data);
                        },
		        //AL FINALIZAR LA CARGA AGREGAR CONTROL DE COMAS Y PUNTO
		        "initComplete": function (settings, json) {
                    let id = $('#id_detalle_deposito').val();
                    // $('#btn-'+id).unbind('click');
                    $.unblockUI();
		        }
            });



             /*Se define las tablas que se van a mostrar en un principio y 
			 se envia el objeto table para cargar las configuraciones.*/
             buttonExcelDetalle([0,1,2,3,4,5],tableDetalle);  

            //   return tableDetalle;
              


            }, 300);


            }//







            function buttonExcelDetalle(column,tableDetalle){
					var numFinal = 0, resultIndex = 0;
					$('#botonExcelDepositoDetalle').html('');
						var buttons = new $.fn.dataTable.Buttons(tableDetalle, { 
							    buttons: [{	
							    						title: 'Reporte Deposito Detalle',
										                extend: 'excelHtml5',
										                 text: '<b>Excel</b>',
										                 className: 'pull-right text-center btn btn-success btn-lg',
								                exportOptions: {
								                    columns: ':visible'
								                },
								                exportOptions: {
									                    columns:  column,
									                    format: {
									                    		//seleccionamos las columnas para dar formato para el excel
												                body: function ( data, row, column, node ) {
                    
												                	if(column == 3){
																	numFinal = 0;
																	numFinal = clean_num(data);
												                    return  numFinal;
												                		} 
												                			return data;
												                		
												                }
												            }
									                },
									                //Generamos un nombre con fecha actual
									                filename: function() {
										               var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
										               // var selected_machine_name = $("#output_select_machine select option:selected").text()
										               return 'Reporte_Deposito_Detalle-'+date_edition;
										           }

								            }]
							}).container().appendTo('#botonExcelDepositoDetalle'); 
				}



        function limpiarTabHome(){     
			$('#cuentaBanco').val('').trigger('change.select2');
            $('#id_moneda').val('').trigger('change.select2');
			$('#periodo_emision').val('');
		}



        		/*{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
			NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}*/
			function clean_num(n,bd=false){

                    if(n && bd == false){ 
                    n = n.replace(/[,.]/g,function (m) {  
                                            if(m === '.'){
                                                return '';
                                            } 
                                            if(m === ','){
                                                return '.';
                                            } 
                                    });
                    return Number(n);
                    }
                    if(bd){
                    return Number(n);
                    }
                    return 0;
            }//


   /*
	Para formatear numeros a moneda USD PY
	 */
	const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD',
					  minimumFractionDigits: 2
					});


    function formatCurrency(id_moneda, value){
					
					let importe = 0;
	

					console.log('formatCurrency', id_moneda, value);

							//PYG
							if(id_moneda == 111){
								importe = GUARANI(value,{ formatWithSymbol: false }).format(true);
								console.log('PYG',importe);
							} 
							//USD
							if(id_moneda == 143) {
								importe = DOLAR(value,{ formatWithSymbol: false }).format(true);
								console.log('USD',importe);
							}

					return importe;		
				}
	

    
    
    </script>
    @endsection
        