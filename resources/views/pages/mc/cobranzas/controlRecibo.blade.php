@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	  <link rel="stylesheet" href="{{asset('mC/css/select.dataTables.min.css')}}"> 
@endsection
@section('content')

<style type="text/css">
	

	.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

@-moz-document url-prefix() {
  fieldset { display: table-cell; }
}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

/*	.select2-container *:focus {
        outline: none;
    }*/


    .correcto_col { 
		height: 74px;
     }

     #divBanco {
	border: 2px solid #D0D1C2;
	border-radius: 5px;

     }

     .title_bank{
     	border-bottom-right-radius: 5px;
		border-bottom-left-radius: 5px;
		background-color: #D0D1C2;
    	padding: 3px 5px 5px 5px;
    	font-weight: 800;
    	margin-bottom: 2px;

     }

	 .text-bold {
		 font-weight: 800;
		 font-size: 16px;
	 }

	 .hr-m {
		margin: 0 0 5px 0;
	 }
	 .h3-m {
		 margin-bottom: 0;
	 }

	 .input-fp {
		font-weight: 800; 
		font-size: 20px;
	 }

	   .btn_op {
		 display: none;
		 margin-right: 7px; 
	 } 

	 .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
	.negrita {
		font-weight: bold;
	}

	.btn-cabecera{
		margin-right:10px; 
		display:none;
	}

	.divisor{
		background-color: #DED4D2 ; 
		height:2px;
		margin-bottom: 5px; 
	}

	#listPagosLoad.style-card{
		margin-bottom: 5px; 
		}



</style>

<section id="base-style">
	<div class="card-content">
		<div class="card" style="border-radius: 14px;">
			<div class="card-header" style="border-radius: 15px;">
				<h4 class="card-title">Control Recibo</h4>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-content collapse show" aria-expanded="true">
				<div class="card-body rounded-bottom rounded-lg">

					<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="tabHome" data-toggle="tab" aria-controls="tabIcon21" href="#home"
								role="tab" aria-selected="true">
								<b><i class="fa fa-fw fa-user"></i> Recibo</b></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="tabConfirm" data-toggle="tab" aria-controls="tabIcon22" href="#pago"
								role="tab" aria-selected="false">
								<b><i class="fa fa-fw fa-gear"></i> Resumen</b></a>
						</li>
					</ul>

					
					<div class="tab-content mt-1">
						<section id="home" class="tab-pane active" role="tabpanel">

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Cliente</label>
								@php
									$ruc = $datos[0]->cliente->documento_identidad;
									if($datos[0]->cliente->dv){
										$ruc = $ruc."-".$datos[0]->cliente->dv;
									}
								@endphp

								<input type="text" class="form-control text-bold" readonly id="beneficiario_txt" name=""
									value="{{$ruc}} - {{$datos[0]->cliente->nombre}} {{$datos[0]->cliente->apellido}} - {{$datos[0]->cliente->id}}">
							</div>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<label>Estado</label>
								<input type="text" class="form-control text-bold" readonly id="op_estado" name=""
									value="{{$datos[0]->estado->denominacion}}">
								<input type="hidden" class="form-control text-bold" id="id_recibo" name=""
									value="{{$datos[0]->id}}">
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-2">
							<div class="form-group">
								<label>Nro. Recibo</label>
								<input type="text" class="form-control text-bold" readonly id="id_recibo" name=""
									value="{{$datos[0]->nro_recibo}}">
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-2">
							<div class="form-group">
								<label>Moneda</label>
								<input type="text" class="form-control text-bold" readonly name=""
									value="{{$datos[0]->moneda->currency_code}}">
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-2">


								@if($permiso == 1 && $datos[0]->id_estado == 31)

									<div class="row" style="margin-left: 0px; margin-right: 0px;">
										<label>Indice Cotización</label><br>
										<div class="input-group">
                                            <div class="input-group-prepend" id="button-addon2" style="width: 100%;">
											<input type="text" class="form-control text-bold numeric" id="cotizacion_especial" name="" value="{{ $datos[0]->indice_cotizacion ? $datos[0]->indice_cotizacion : 0  }}">
											<button id="botonCotizacion" class="btn btn-primary" data-toggle="modal"  type="button" style="padding-bottom: 0px;padding-top: 2px;"><i class="fa fa-hdd-o fa-lg"></i></button>
                                            </div>
										</div>	
									</div>
							@else 
								<div class="form-group">
									<label>Indice Cotización</label>
									<input type="text" class="form-control text-bold numeric" readonly id="cotizacion_especial" name="" value="{{ $datos[0]->indice_cotizacion ? $datos[0]->indice_cotizacion : 0 }}">
								</div>
							@endif
						</div>
					</div>

					
					<div class="row">
						<button type="button" class="btn btn-success btn-lg pull-right"
							style="margin:0 3% 0 0;display: none;" id="btnAut" data-toggle="modal"
							data-target="#modalAutorizar">AUTORIZAR</button>
								
						<div class="col-12 col-md-12">

						<a href="{{route('reporteAsiento')}}?id_detalle_asiento={{$datos[0]->id_asiento}}&id_origen_asiento={{$id_origen_asiento}}&nro_documento={{$datos[0]->nro_recibo}}" class="btn-cabecera btn_asiento text-center btn btn-info btn-lg pull-right" style="margin-right: 20px;" role="button"><b>Ver Asiento</b></a>

							<button class="btn_imprimir text-center btn btn-info btn-lg pull-left btn-cabecera"
								type="button"><i class="fa fa-fw fa-print"></i>Original</button>
							<button class="btn_correo_du_tri text-center btn btn-info btn-lg pull-left btn-cabecera"
								type="button"><i class="fa fa-fw fa-print"></i> Duplicado</button>
							<button class="btn_correo text-center btn btn-info btn-lg pull-left btn-cabecera" type="button"><i class="fa fa-envelope"></i></button>
							
								<a href="{{route('indexRecibo')}}" class="btn-cabecera text-center btn btn-info btn-lg pull-right" style="margin-right: 20px;" role="button"><b>Ver Recibos</b></a> 

								<a role='button' href="{{route('indexRecibo')}}" class='btn btn-info btn-lg pull-right' title='Ver Recibos'><i class='fa fa-newspaper-o'></i><b>Ver Recibos</b></a>
								@if(isset($adjuntos[0]->id) || $tiene_efectivo == 1)		
									<a role='button' onclick='mostrarAdjunto()' class='btn btn-info btn-lg pull-right' style="margin-right: 10px;color: white;" title='Ver Adjuntos'><i class='fa fa-image'></i><b>Ver Adjuntos</b></a>
								@endif
								@foreach ($btn as $boton)
								{{-- CARGA LOS BOTONES POR PERMISO DE PERSONAS --}}
								<?php echo $boton; ?>
								@endforeach
								
						</div>
					</div>
		

					<div class="row mt-1" style="border: solid 2px #000; border-radius:15px;">
							<h3 class="h3-m font-weight-bold">Resumen</h3>
							<div class="w-100 divisor"></div>
					

								<div class="col-12 col-sm-6">	
									<div class="form-group">
										<label>Total :</label>
											<div class="input-group">
												<div class="input-group-append">
													<span class="input-group-text font-weight-bold">{{$datos[0]->moneda->currency_code}}
													</span>
												</div>
												<input type="text" class="form-control text-bold format-number-recibo" readonly
											value="{{$datos[0]->importe}}" id="total_neto_recibo">
											</div>
									</div>
								</div>

							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Total Nota de Credito :</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" class="form-control text-bold format-number-recibo"
												readonly value="{{$datos[0]->total_nc}}" id="gastos_administrativos">
									</div>
								</div>
						</div>

						<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Total Canje :</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" class="form-control text-bold format-number-recibo"
												readonly value="{{$datos[0]->total_canje}}" id="gastos_administrativos">
										</div>
								</div>
						</div>

						<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>Total Anticipo. :</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" class="form-control text-bold format-number-recibo"
												readonly value="{{$datos[0]->total_anticipo}}"
												id="gastos_administrativos">
	
										</div>
								</div>
						</div>
							<div class="col-12 col-sm-6">
								<div class="form-group">
									<label>A Pagar. :</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text">=</span>
											</div>
											<input type="text" class="form-control text-bold format-number-recibo"
												readonly value="{{$datos[0]->total_pago}}" id="total_op">
										</div>
								</div>
							</div>

					</div>

					<form class="mt-1">
						<div class="form-group row">
							<label  class="col-sm-2 col-form-label col-form-label font-weight-bold">Entregado a :</label>
							<div class="col-sm-10">
								@if(isset($datos[0]->gestor->nombre))	
								  <input type="text" disabled class="form-control form-control text-bold"  value="{{$datos[0]->gestor->nombre}} {{$datos[0]->gestor->apellido}}">
								 @else
								  <input type="text" disabled class="form-control form-control text-bold"  value="">
								 @endif
								</div>
						  </div>
						  <div class="form-group row">
						    <?php 
                	   			$concepto = $datos[0]->concepto;	
                	   		?>

							<label  class="col-sm-2 col-form-label col-form-label font-weight-bold">Concepto:</label>
							<div class="col-sm-10">
							  <input type="text" disabled class="form-control form-control text-bold"  value="{{$concepto}}">
							</div>
						  </div>
					</form>

					<div class="row">
						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Generado por :</label>
								<input type="text" class="form-control text-bold" readonly name=""
									value="{{$datos[0]->usuario_generado->nombre}} {{$datos[0]->usuario_generado->apellido}} ({{$datos[0]->fecha_hora_creacion}})">
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Cobrado por :</label>
								@if(isset($datos[0]->usuario_cobrado->nombre))
								<input type="text" class="form-control text-bold" readonly name=""
									value="{{$datos[0]->usuario_cobrado->nombre}} {{$datos[0]->usuario_cobrado->apellido}} ({{$datos[0]->fecha_hora_cobro}})">
								@else
								<input type="text" class="form-control text-bold" readonly name="" value="()">
								@endif
							</div>
						</div>


						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Aplicado por : </label>
								@if(isset($datos[0]->usuario_aplicado->nombre))
								<input type="text" class="form-control text-bold" readonly name=""
									value="{{$datos[0]->usuario_aplicado->nombre}} {{$datos[0]->usuario_aplicado->apellido}} ({{$datos[0]->fecha_hora_aplicado}})">
								@else
								<input type="text" class="form-control text-bold" readonly name="" value="()">
								@endif
							</div>
						</div>


						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Anulado por :</label>
								@if(isset($datos[0]->usuario_anulacion->nombre))
								<input type="text" class="form-control text-bold" readonly name=""
									value="{{$datos[0]->usuario_anulacion->nombre}} {{$datos[0]->usuario_anulacion->apellido}} ({{$datos[0]->fecha_hora_anulacion}})">
								@else
								<input type="text" class="form-control text-bold" readonly name="" value="()">
								@endif
							</div>
						</div>

					</div>

				</section>

				<section id="pago" class="tab-pane" role="tabpanel">

					<div class="row">
						<div class="col-12">
							<h3 class="h3-m font-weight-bold">Items seleccionados</h3>
							<div class="w-100 divisor"></div>
						
								<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
									<table id="listadoItems" class="table table-hover table-condensed nowrap"
										style="width: 100%; font-weight:800">
										<thead>
											<tr>
												<th>Nro. Comprobante</th>
												<th>Tipo</th>
												<th>Fecha</th>
												<th>Importe</th>
											</tr>
										</thead>
										<tbody>
											@forelse ($reciboDetalle as $detalle)

											<tr>
												<td>

													@if($detalle->id_venta_rapida != "")
														<a href="{{route('verVenta', ['id' =>$detalle->id_venta_rapida])}}" class="bgRed">
											        	<i class="fa fa-fw fa-search"></i>{{$detalle->nro_documento}}</a>
											        	<div style="display:none;">${x.id_proforma}</div>
													@elseif($detalle->id_anticipo != "")
														<a href="{{route('anticipoDetalleCliente', ['id' =>$detalle->id_anticipo])}}" class="bgRed">
												        <i class="fa fa-fw fa-search"></i>{{$detalle->nro_documento}}</a>
												        	<div style="display:none;">${x.id_proforma}</div>
													@elseif($detalle->id_proforma != "")
															<a href="{{route('verFactura', ['id' =>$detalle->id_factura])}}" class="bgRed">
												        	<i class="fa fa-fw fa-search"></i>{{$detalle->nro_documento}}</a>
												        	<div style="display:none;">${x.id_proforma}</div>
														@elseif($detalle->documento_tipo == 2)
															<a href="{{route('verNota', ['id' =>$detalle->id_factura])}}" class="bgRed"><i class="fa fa-fw fa-search"></i>{{$detalle->nro_documento}}</a>
																<div style="display:none;">${x.id_proforma}</div>
													@endif

												</td>
												<td>{{$detalle->documento_n}}</td>
												<td>{{$detalle->fecha_hora}}</td>
												<td>{{$detalle->importe}}</td>
											</tr>

											@empty
											<tr>
												<td colspan="5"> No hay resultados</td>
											</tr>
											@endforelse
										</tbody>
									</table>
								</div>
							



						</div>
					</div>

					<div class="row">
						<div class="col-12 mt-1">
						<h3 class="h3-m font-weight-bold">Forma de Cobro</h3>
						<div class="w-100 divisor"></div>
						
							<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
								<table id="listPagosLoad" class="table" style="width:100%">
									<thead>
										<tr>
											<th>Operación</th>
											{{-- <th>Importe Pago</th>
											<th>Moneda</th> --}}
										</tr>
									</thead>

									<tbody style="text-align: center">
										<tr>
											<td colspan="5"> No hay resultados</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</section>

					</div>

				</div>
			</div>
		</div>
</section>
			 
	<div class="modal fade" id="modalCargarCorreo" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
		  <div class="modal-content">
			<div class="modal-header">
			  <h5 class="modal-title">Enviar a  <i style="display: none; width: 20px;" class="fa fa-refresh fa-spin cargandoImg"></i></h5>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-12 col-sm-4 col-md-12">
						
						<div class="form-group">
							<label>Correo Cliente :</label>
							@if($correos_administrativos != "")
								<input type="text" class="form-control text-bold" readonly id="emailAdministrativo" value="{{$correos_administrativos}}">
							@else
								<input type="text" class="form-control text-bold" id="emailAdministrativo" value="">
							@endif	
						</div>
					</div>
				</div>	
				<div class="row"> 
					<div class="col-12 col-sm-4 col-md-12">
						<div class="form-group">
							<label>Con copia a:</label>
							<input type="text" type="email" class="form-control text-bold" id="email" value="">
						</div>
					</div>
				</div>		

			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-danger" data-dismiss="modal">No, Cancelar</button>
			  <button type="button" class="btn btn-success" id="btnEnviar">Si, Enviar</button>
			</div>
		  </div>
		</div>
	  </div>

 <!-- ========================================
   					MODAL ADJUNTOS
   		========================================  -->	
		   <div class="modal fade text-left" id="modalAdjunto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document" style="margin-left: 20%;margin-top: 10%;">
            <div class="modal-content" style="width: 80%">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel16">Adjuntos</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
	                <!-- Post -->
	                <div class="post">
	                  <!-- /.user-block -->
					  <div class="row" id='output'>
							@foreach($adjuntos as $adjunto)
									<?php
	                  					$tamanho = $adjunto->comprobante;
	                  					$tamanhoInicial = explode('.', $tamanho);
										$ruta = 'uploadComprobanteRecibo/'.$adjunto->comprobante;
									?>
									<div id="cuadro_{{$adjunto->id}}">
										<div class="col-md-2" style="margin-bottom: 20px;">
											@if(isset($tamanhoInicial[1]))
												@if($tamanhoInicial[1] == 'pdf')
													<a href="{{asset($ruta)}}" target="_blank"><img class="img-responsive" style="width:120px;" src="{{asset('images/file.png')}}" alt="Photo"></a>
												@else	
													<a href="{{asset($ruta)}}" target="_blank"><img class="img-responsive" style="width:120px; height: 120px;" src="{{asset($ruta)}}" alt="Photo"></a>
												@endif	
											@else
												<a href="{{asset($ruta)}}" target="_blank"><img class="img-responsive" style="width:120px; height: 120px;" src="{{asset($ruta)}}" alt="Photo"></a>
											@endif
										</div>
		                                <div class="col-md-1">
											@if($datos[0]->estado->id == 31)
												<button type="button" id="btn-imagen" data-id="{{$adjunto->id}}" onclick='eliminarImagen("{{$adjunto->id}}")'>
													<i class="ft-x-circle" style="font-size: 18px;"></i>
												</button>
											@endif
		                                </div>
	                                </div>  
								@endforeach   							
							</div>    
			                <br>
							@if($tiene_efectivo == 1)
								<form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('uploadDocumentosRecibos')}}" autocomplete="off">
									<div class="form-group">
									<div class="row">
										<div class="col-12">
											<label class="control-label" >Adjuntar comprobantes: </label>
											<input type="hidden" name="_token" value="{{ csrf_token() }}" />
											<input type="hidden" name="id_recibo" id="id_recibo" value="{{$id}}"/> 
											<input type="file" class="form-control" name="image" id="image"/>  
											<h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: <span class="text-uppercase">pdf, xls, doc, docx, pptx, pps, jpeg, bmp, png y jpg.</span></b>
											</h4>
											<div id="validation-errors"></div>
										</div> 
										</div>  
									</div>  
								</form>
							@endif
	                	</div>
	                <!-- /.post -->
					</div>
					<div class="modal-footer">
	                    <div class="row">
							<div class="col-md-3">
							</div>	
		                    <div class="col-md-6">
								<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>	
				</div>	
			</div>	
		</div>	
	</div>	

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script src="https://unpkg.com/currency.js@~1.2.0/dist/currency.min.js"></script>
	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script> --}}
	<script>

		const operaciones = {
			id_recibo: Number('{{$id}}'),
			id_estado: Number('{{$datos[0]->id_estado}}'),
			id_moneda: Number('{{$datos[0]->id_moneda}}')
		}



	$(document).ready(() => {
		// MediaQuery();
		controlEstado();
		formatMoneyFp();

	});

	// $('#modalCobro').modal('show'); 



	$(window).resize(function () {
		MediaQuery();
	});


	function MediaQuery() {
		if ($(window).width() < 768) {
			smStyleform();
		} else if ($(window).width() >= 768) {
			normalStyleForm();
		} else if ($(window).width() >= 992) {
			normalStyleForm();
		} else if ($(window).width() >= 1200) {
			normalStyleForm();
		}
	}


	function normalStyleForm() {
		$('.form-group').removeClass('form-group-sm');
		$('.btn-cabecera').removeClass('btn-sm');


	}

	function smStyleform() {
		$('.form-group').addClass('form-group-sm');
		$('.btn-cabecera').addClass('btn-sm');
	}

	@if(isset($datos[0]->gestor->nombre))
		$('#cf_entrega').val('{{$datos[0]->gestor->nombre}} {{$datos[0]->gestor->apellido}}');
	@else 
		$('#cf_entrega').val('');
	@endif
	/*
	==========================================================================
		 			CONTROL DE ESTADO
	==========================================================================
	*/


	function controlEstado() {
		let estado = operaciones.id_estado;
		$('.btn_correo').hide()
		//GENERADO

		if(estado === 31){
			$('.btn_anular_generado').show();
			$('.btn_correo').show();
			$('.btn_imprimir').show();
			eventos_generado();
			$('.btn_cobrar').show();
		}
		//COBRADO
		if (estado === 40) {
			$('.btn_correo').show();
			$('.btn_imprimir').show();
			$('#textBtnCobrar').html('Forma Cobro');
			$('.btn_aplicar').show();
			$('.btn_cobrar').show();
			$('.btn_anular_cobrado').show();
			eventos_generado();
			eventos_cobrado();
		}
		//APLICADO
		if (estado === 56) {
			$('.btn_correo_du_tri').show();
			$('.btn_correo').show();
			$('.btn_imprimir').show();
			// $('.btn_anular_generado').hide();
			$('.btn_anular_aplicado').show();
			$('.btn_asiento').show();
			eventos_aplicado();
		}
		//ANULADO
		if (estado === 55) {
			$('.btn_correo').hide();
			$('.btn_imprimir').show();
			$('.btn_anular_generado').hide();
			eventos_anulado();
		}
	}

	function eventos_generado() {
		$('.btn_cobrar').click(() => {
			location.href = "{{ route('aplicarRecibo',['id'=>$id]) }}";
		});
	}

	function eventos_cobrado() {
		loadFp();
	}

	function eventos_aplicado() {
		loadFp();
	}

	function eventos_anulado() {
		loadFp();
	}

	$('.btn_anular_generado').click(() => {
		modalAnularGenerado();
	});

	$('.btn_anular_aplicado').click(() => {
		modalAnularAplicado();

	 });
 	
 	$('.btn_anular_cobrado').click(()=>{
		modalAnularCobrado();
	 });

	$('.btn_aplicar').click(() => {
		modalAplicarRecibo();
	});



	$('.btn_correo').click(() => {
		$('#modalCargarCorreo').modal('show');
	});

	$('.btn_imprimir').click(() => {
		location.href = "{{ route('imprimirRecibo',['id'=>$id]) }}?option=1";
	});

	$('.btn_correo_du_tri').click(() => {
		location.href = "{{ route('imprimirRecibo',['id'=>$id]) }}?option=3";
	});

	$('#btnEnviar').click(() => {
		$('.cargandoImg').css('display', 'block');

		if($('#emailAdministrativo').val() != ""){
			$.ajax({
				type: "GET",
				url: "{{route('enviarCorreo')}}",
				data: {
					id_recibo: '{{$id}}',
					correos_administrativos: $('#emailAdministrativo').val(),
					concopia: $('#email').val()
				},
				dataType: 'json',
				error: function (jqXHR, textStatus, errorThrown) {

					// msjToast('Ocurrió un error al intentar anular', 'error', jqXHR, textStatus);
					//reject('Ocurrió un error al intentar anular');
				},
				success: function (rsp) {
					$('#modalCargarCorreo').modal('hide');
					if (rsp.status == 'OK') {
						swal("Éxito", rsp.mensaje, "success");
						$('.cargandoImg').css('display', 'none');
					} else {
						swal("Cancelado", rsp.mensaje, "error");
						$('.cargandoImg').css('display', 'none');
					}

				}
			})
		}else{
			$.toast({
                heading: 'Error',
                text: 'Ingrese un destinatario para el correo.',
                position: 'top-right',
                showHideTransition: 'fade',
                icon: 'error'
            });

            $('.cargandoImg').hide();
		}
	});


	/*
	===============================================================
							MODALES JS
	===============================================================
	*/
	
	function modalAnularCobrado(){
		
		swal({
							title: "Anular Recibo",
							text: "¿Está seguro que desea anular el cobro del recibo?",
							icon: "warning",
							showCancelButton: true,
							buttons: {
								cancel: {
									text: "No, Cancelar",
									value: null,
									visible: true,
									className: "btn-warning",
									closeModal: false,
								},
								confirm: {
									text: "Si , Continuar",
									value: true,
									visible: true,
									className: "",
									closeModal: false
								}
							}
						}).then(isConfirm => {
							
							if (isConfirm) {
								//OPCION PARA ANULAR RECIBOS APLICADOS
								$.when(aplicarRecibo(5)).then((a)=>{ 
									swal("Éxito",'El recibo fue anulado' , "success");
									setTimeout( ()=>{ location.reload(true);},1000);
								},(b)=>{
									swal("Cancelado", b, "error");
								});
								
							} else {
								swal("Cancelado", "La operación fue cancelada", "error");
							}
						});
	}


	function modalAnularAplicado() {

		swal({
			title: "Anular Recibo",
			text: "¿Está seguro que desea anular la aplicación del recibo?",
			icon: "warning",
			showCancelButton: true,
			buttons: {
				cancel: {
					text: "No, Cancelar",
					value: null,
					visible: true,
					className: "btn-warning",
					closeModal: false,
				},
				confirm: {
					text: "Si , Continuar",
					value: true,
					visible: true,
					className: "",
					closeModal: false
				}
			}
		}).then(isConfirm => {

			if (isConfirm) {
				//OPCION PARA ANULAR RECIBOS APLICADOS
				$.when(aplicarRecibo(4)).then((a) => {
					swal("Éxito", 'El recibo fue anulado', "success");
					setTimeout(() => {
						location.reload(true);
					}, 1000);
				}, (b) => {
					swal("Cancelado", b, "error");
				});

			} else {
				swal("Cancelado", "La operación fue cancelada", "error");
			}
		});
	}

	function modalAnularGenerado() {
		swal({
			title: "Anular Recibo",
			text: "¿Está seguro que desea anular el recibo?",
			icon: "warning",
			showCancelButton: true,
			buttons: {
				cancel: {
					text: "No, Cancelar",
					value: null,
					visible: true,
					className: "btn-warning",
					closeModal: false,
				},
				confirm: {
					text: "Si , Continuar",
					value: true,
					visible: true,
					className: "",
					closeModal: false
				}
			}
		}).then(isConfirm => {

			if (isConfirm) {
				//OPCION PARA ANULAR RECIBOS GENERADOS
				$.when(aplicarRecibo(3)).then((a) => {
					swal("Éxito", 'El recibo fue anulado', "success");
					setTimeout(() => {
						location.reload(true);
					}, 1000);
				}, (b) => {
					swal("Cancelado", b, "error");
				});

			} else {
				swal("Cancelado", "La operación fue cancelada", "error");
			}
		});
	}



	function modalAplicarRecibo() {
		swal({
			title: "Aplicar Recibo",
			text: "¿Está seguro que desea aplicar el recibo?",
			icon: "warning",
			showCancelButton: true,
			buttons: {
				cancel: {
					text: "No, Cancelar",
					value: null,
					visible: true,
					className: "btn-warning",
					closeModal: false,
				},
				confirm: {
					text: "Si , Continuar",
					value: true,
					visible: true,
					className: "",
					closeModal: false
				}
			}
		}).then(isConfirm => {

			if (isConfirm) {
				//OPCION PARA APLICAR RECIBOS
				$.when(aplicarRecibo(2)).then((a) => {
					swal("Éxito", 'El recibo fue aplicado', "success");
					setTimeout(() => {
						location.reload(true);
					}, 1000);
				}, (b) => {
					swal("Cancelado", b, "error");
				});

			} else {
				swal("Cancelado", "La operación fue cancelada", "error");
			}
		});
	}







	/*===============================================================
							LOGICA DE RECIBO Y ESTADOS
	===============================================================*/

	function aplicarRecibo(opt) {

		return new Promise((resolve, reject) => {

			$.ajax({
				type: "GET",
				url: "{{route('estadoRecibo')}}",
				data: {
					id_recibo: operaciones.id_recibo,
					option: opt
				},
				cache:false,
				dataType: 'json',
				error: function (jqXHR, textStatus, errorThrown) {

					// msjToast('Ocurrió un error al intentar anular', 'error', jqXHR, textStatus);
					reject('Ocurrió un error al intentar anular');
				},
				success: function (rsp) {
					
					if (rsp.err == true) {
						// location.reload(true);
						resolve(true);
					} else {
						if(rsp.e !=''){
							console.log('Debug Activado',rsp.e);
						}
						reject(rsp.txtErr);
					}

				} //success
			}); //ajax

		}); //promise
	} //

	function loadFp() {



		let dataString = {
				id_recibo: operaciones.id_recibo
			},
			tablaCabecera,
			tablaCuerpo,
			tablaPie,
			tabla;

		tableListado = $("#listPagosLoad").DataTable({
			destroy: true,
			scrollY: "300px",
			scrollCollapse: true,
			paging: false,
			ajax: {
				url: "{{route('getFpReciboControl')}}",
				data: dataString,
				error: function (jqXHR, textStatus, errorThrown) {
					msjToast('Ocurrió un al intentar recuperar los datos de la forma de pagos.', 'error', jqXHR, textStatus);
				}

			},
			responsive: {
				details: false
			},


			"columns": [

				{
					data: function (value) {

						tablaCabecera = `
							<tr class="tabla_filas">
							<td>
								<div class="card mb-1 text-left">
											<div class="card-body p-0">
												<h5 class="card-title"><b>Operación: ${value.forma_cobro_cliente_n}</b></h5>
											<div class="row">

												<div class="col-12 col-sm-6 col-md-4">	
													<div class="card-text text-wrap font-weight-bold">
														Importe: ${value.currency_code} ${formatCurrency(value.id_moneda, clean_num(value.importe_pago,true))} 
													</div>
												</div>
												`;
						tablaCuerpo = '';

						if (Boolean(value.banco_cuenta)) {
							if(value.abreviatura =='VO'){
								tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Tarjeta: ${value.tarjeta_num_operacion}</div>
													</div>
												`;
							}else{	
								tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Banco/Cuenta: ${value.banco_cuenta}</div>
													</div>
													`;

							}						
						}


						if (Boolean(value.nro_comprobante)) {
							tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Nro. Comprobante: ${value.nro_comprobante}</div>
													</div>
													`;
						}



						if (Boolean(value.fecha_operacion)) {
							tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Fecha Operación: ${value.fecha_operacion_format}</div>
													</div>
													`;
						}

						//RETENCION
						if (Boolean(value.num_retencion)) {
							tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
															Nro. Retención: ${value.num_retencion} </div>
													</div>
													`;
						}


						//CANJE
						if (Boolean(value.num_factura)) {
							tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Nro. Factura: ${value.num_factura} </div>
													</div>
													`;
						}

						if (Boolean(value.timbrado_factura)) {
							tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Timbrado Factura: ${value.timbrado_factura} </div>
													</div>
													`;
						}

						//TARJETAS
						if (Boolean(value.procesadora_n)) {
							tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Procesadora Tarjeta: ${value.procesadora_n} </div>
													</div>
													`;
						}

						//CHEQUE

						if (Boolean(value.banco_plaza_n)) {
							tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Banco Plaza: ${value.banco_plaza_n} </div>
													</div>
													`;
						}

						if (Boolean(value.fecha_emision)) {
							tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Fecha Emisón: ${value.fecha_emision_format} </div>
													</div>
													`;
						}

						if (Boolean(value.fecha_vencimiento)) {
							tablaCuerpo += `
													<div class="col-12 col-sm-6 col-md-4">	
														<div class="card-text text-wrap font-weight-bold style-card">
														Vencimiento: ${value.fecha_vencimiento} </div>
													</div>
													`;
						}




						tablaPie = `						
										</div>	
									</div>
								</div>
							</td>
							</tr>	
							`;

						tabla = tablaCabecera + tablaCuerpo + tablaPie;


						return tabla;
					}
				}
			],
			"createdRow": function (row, data, index) {
				$(row).addClass('negrita');

			}
		});

	}









	/*
	==========================================================================
		 			CLASES AUXILIARES
	==========================================================================
	Para almacenar mensajes o mostrar mensajes en el momento segun error del ajax
	*/
	function msjToast(msj, opt = '', jqXHR = false, textStatus = '') {

		const aux = {
			msj: []
		}

		if (msj != '') {
			aux.msj.push('<b>' + msj + '</b>');
		}

		if (opt === 'info') {
			$.toast().reset('all');

			$.toast({
				heading: '<b>Atención</b>',
				position: 'top-right',
				text: aux.msj,
				width: '400px',
				hideAfter: false,
				icon: 'info'
			});
			aux.msj = [];

		} else if (opt === 'error') {
			$.toast().reset('all');



			if (jqXHR === false && textStatus === '') {
				msj = aux.msj;
			} else {

				errCode = jqXHR.status;
				errMsj = jqXHR.responseText;

				msj = '<b>';
				if (errCode === 0)
					msj += 'Error de conectividad a internet, verifica tu conexión.';
				else if (errCode === 404)
					msj += 'Error al realizar la solicitud.';
				else if (errCode === 500) {
					if (aux.msj.length > 0) {
						msj += aux.msj;
					} else {
						msj += 'Ocurrio un error en la comunicación con el servidor.'
					}

				} else if (errCode === 406) {
					msj += errMsj;
				} else if (errCode === 422)
					msj += 'Complete todos los datos requeridos'
				else if (textStatus === 'timeout')
					msj += 'El servidor tarda mucho en responder.';
				else if (textStatus === 'abort')
					msj += 'El servidor tarda mucho en responder.';
				else if (textStatus === 'parsererror') {
					msj += 'Error al realizar la solicitud.';
				} else
					msj += 'Error desconocido, pongase en contacto con el area tecnica.';
				msj += '</b>';
			}


			$.toast({
				heading: '<b>Error</b>',
				text: aux.msj,
				position: 'top-right',
				hideAfter: false,
				width: '400px',
				icon: 'error'
			});

			aux.msj = [];
		} else if (opt === 'success') {
			$.toast().reset('all');

			$.toast({
				heading: '<b>Éxito</b>',
				text: aux.msj,
				position: 'top-right',
				hideAfter: false,
				width: '400px',
				icon: 'success'
			});
			aux.msj = [];

		}
	} //



	/* ================================================================================
								FORMATEO DE VALORES
		================================================================================*/


	//DEFINIR FORMATO MONEDAS		
	const EURO = value => currency(value, {
		symbol: "",
		decimal: ',',
		separator: '.',
		precision: 2
	});
	const DOLAR = value => currency(value, {
		symbol: "",
		decimal: ',',
		separator: '.',
		precision: 2
	});
	const GUARANI = value => currency(value, {
		symbol: "",
		separator: '.',
		precision: 0
	});




	function formatCurrency(id_moneda, value) {

		let importe = 0;

		//PYG
		if (id_moneda == 111) {
			importe = GUARANI(value, {
				formatWithSymbol: false
			}).format(true);
		}
		//USD
		if (id_moneda == 143) {
			importe = DOLAR(value, {
				formatWithSymbol: false
			}).format(true);
		}

		return importe;
	}




	function clean_num(n, bd = false) {

		if (n && bd == false) {
			n = n.replace(/[,.]/g, function (m) {
				if (m === '.') {
					return '';
				}
				if (m === ',') {
					return '.';
				}
			});
			return Number(n);
		}
		if (bd) {
			return Number(n);
		}
		return 0;
	}


	//FORMATEO DE VALORES DE RESUMEN
	function formatMoneyFp() {
		let moneda = operaciones.id_moneda;

		if (moneda == '111') {
			$('.format-number-recibo').inputmask("numeric", {
				radixPoint: ",",
				groupSeparator: ".",
				digits: 0,
				autoGroup: true,
				rightAlign: false
			});

		} else {

			$('.format-number-recibo').inputmask("numeric", {
				radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				rightAlign: false
			});

		} //else
	} //			


	$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			rightAlign: false,
			oncleared: function () {  }
		});
	
	$("#botonCotizacion").click(() => {
		$.ajax({
				type: "GET",
				url: "{{route('guardarCotizacionEspecial')}}",
				data: {
					id_recibo: operaciones.id_recibo,
					cotizacion: $("#cotizacion_especial").val()
				},
				dataType: 'json',
				error: function (jqXHR, textStatus, errorThrown) {
					
				},
				success: function (rsp) { 
					if (rsp.status == 'OK') {
						$.toast({
								heading: '<b>Éxito</b>',
								text: rsp.mensaje,
								position: 'top-right',
								hideAfter: false,
								width: '400px',
								icon: 'success'
							});

					} else {
						$.toast({
								heading: '<b>Error</b>',
								text: rsp.mensaje,
								position: 'top-right',
								hideAfter: false,
								width: '400px',
								icon: 'error'
							});
					}	

				} //success
			}); //ajax
	});

	$(document).ready(function(){
          var options = { 
                        beforeSubmit:  showRequest,
                success: showResponse,

            dataType: 'json' 
                }; 
          $('body').delegate('#image','change', function(){
            $('#upload').ajaxForm(options).submit();      
          });

      })


      function showRequest(formData, jqForm, options) { 
			$("#validation-errors").hide().empty();
			$("#output").css('display','none');
			return true; 
      } 

      function showResponse(response, statusText, xhr, $form)  { 
            if(response.success == false)
            {
              var arr = response.errors;
              console
              $.each(arr, function(index, value)
              {
                if (value.length != 0)
                {
                  $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                }
              });
              $("#validation-errors").show();
            } else {
			   cantidad = parseInt($("#cantidad_adjunto").val());
			   total_cantidad = cantidad + 1;
			   $("#cantidad_adjunto").val(total_cantidad);
			   espacio = response.archivo;
			   extension = espacio.split('.');
			   console.log(extension);
               $("#imagen").val(response.archivo);  
			   rutaImagen = "{{asset('images/file.png')}}";
			   	if(extension[1] == 'pdf'){
			   		$("#output").append('<div class="col-sm-2" id="cuadro_'+response.numero+'"><a href="'+response.file+'" target="_blank"><img class="img-responsive" style="width:120px; height:120px;" src="'+rutaImagen+'" alt="Photo"></a><button type="button" id="btn-imagen" data-id="'+response.numero+'" onclick="eliminarImagen('+response.numero+')"><i class="ft-x-circle" style="font-size: 18px;"></i></button></div>');
				}else{
					$("#output").append('<div class="col-sm-2" id="cuadro_'+response.numero+'"><a href="'+response.file+'" target="_blank"><img class="img-responsive" style="width:120px; height:120px;" src="'+response.file+'" alt="Photo"></a><button type="button" id="btn-imagen" data-id="'+response.numero+'" onclick="eliminarImagen('+response.numero+')"><i class="ft-x-circle" style="font-size: 18px;"></i></button></div>');
				}
			   $("#output").css('display','block');
            }
       }

      function eliminarImagen(id){
          dataString = "file="+id;
          $.ajax({
              type: "GET",
              url: "{{route('fileAdjuntoRecibo')}}",//fileDelDTPlus
              dataType: 'json',
              data: dataString,
              success: function(rsp){
					$("#imagen").val("");
					$("#cuadro_"+id).remove();
					cantidad = parseInt($("#cantidad_adjunto").val());
					total_cantidad = cantidad - 1;
					$("#cantidad_adjunto").val(total_cantidad);
              }

        });
      } 


	function mostrarAdjunto(){
		$('#modalAdjunto').modal('show');

	}

	$.ajax({
			type: "GET",
			url: "{{route('comprobarAsiento')}}",
			data: {
					id_data: operaciones.id_recibo,
					option: 'REC'
				},
			dataType: 'json',
			error: function (jqXHR, textStatus, errorThrown) {},
			success: function (rsp) {
				if(rsp.status == 'ERROR'){
                    swal('El asiento Nro.'+rsp.id , 'El asiento generado por este proceso no está balanceado, favor pongase en contacto con Administración/Contabilidad para su verificación.',"error");
				}
			}
	})		


	</script>	
@endsection