@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
    					
    input.form-control:focus ,.select2-container--focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
     .mr-1 {
         margin-right: 5px; 
     }
     .labelError{
         color: #F74343 ;
     }
</style>

@parent
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title"> CREAR ANTICIPO CLIENTE</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

        <form class="row" id="formDataAnticipo" autocomplete="off">

                <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="form-group">
                        <label>Sucursal (*)</label>
                        <select class="form-control select2" name="id_sucursal:number"  style="width: 100%;" required>
                            @foreach ($sucursalEmpresa as $sucursal)
                            <option value="{{$sucursal->id}}" @if($sucursal->id == 24) selected @endif>{{$sucursal->denominacion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="form-group">
                        <label>Centro Costo (*)</label>
                        <select class="form-control select2" name="id_centro_costo"  data-value-type="number" style="width: 100%;">
                            @foreach ($centro as $c)
                            <option value="{{$c->id}}">{{$c->nombre}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;">
                    <div class="form-group">
                        <label>Fecha de Pago</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                               <span id ="vencimiento" class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </div>
                               <input type="text" maxlength="10" class="form-control pull-right single-picker" name="fecha_pago" data-value-type="s_date"  value="{{date('d/m/Y')}}">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px; display:none">
                    <div class="form-group">
                        <label>Fecha de Venc</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                               <span id ="vencimiento" class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </div>
                               <input type="text" maxlength="10" class="form-control pull-right single-picker" name="fecha_vencimiento" data-value-type="s_date" value="{{date('d/m/Y')}}">
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="form-group">
                        <label>Moneda (*)</label>
                        
                        <select class="form-control select2" name="id_moneda" id="ant_id_moneda" data-value-type="number" style="width: 100%;" required>
                            @foreach ($currency as $div)
                            <option value="{{$div->currency_id}}">{{$div->currency_code}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="form-group">
                            <label>Cambio (*)</label>
                            <input type="text" class="form-control format-number-control" maxlength="15" name="cotizacion" data-value-type="convertNumber" value="">
                        </div>
                    </div>


                <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="form-group">
                        <label>Importe (*)</label>
                        <input type="text" class="form-control format-number-control" maxlength="25" name="importe"  data-value-type="convertNumber"   value="" required>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="form-group">
                            <label>Cliente (*)</label>
                            <select class="form-control select2" name="id_cliente" id="id_cliente" data-value-type="number"  style="width: 100%;">
                                <option value="">Seleccione Cliente</option>
                                @foreach ($beneficiario as $b)
                                    <option value="{{$b->id}}">{{$b->cliente_n}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>

                    @if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa == 'V')
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>Ventas </label>
                                <select class="form-control select2" disabled name="id_venta" id="id_venta" data-value-type="number"  style="width: 100%;">
                                    <option value="">
                                    </option>
                                </select>
                            </div>
                        </div>
                    @else   
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>Grupo </label>
                                <select class="form-control select2" disabled name="id_grupo" id="id_grupo" data-value-type="number"  style="width: 100%;">
                                
                                    <option value="">
                                    </option>
                                
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>Proformas </label>
                                <select class="form-control select2" disabled name="id_proforma" id="id_proforma" data-value-type="number"  style="width: 100%;">
                                
                                    <option value=""></option>
                                    
                                </select>
                            </div>
                        </div>
                    @endif

                    <div class="col-12 col-sm-6 col-md-4 col-lg-3" style="display:none">
                        <div class="form-group">
                            <label>Gestor</label>
                            <select class="form-control select2" name="id_gestor" id="gestor"
                                style="width: 100%;">
                                @foreach ($gestor as $gest)
                                <option value="{{$gest->id}}">{{$gest->nombre}} {{$gest->apellido}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-11">
                        <div class="form-group">
                            <label>Cuenta Contable (*)</label>
                            <select class="form-control select2" disabled name="id_cuenta_contable" id="cuenta_anticipo_cobranza" data-value-type="number"  style="width: 100%;">
                                    @foreach ($cuentas_contables as $cuenta)
                                        @if($cuenta->asentable)
                                        <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif   
                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                        </option>
                                        @if($cuenta->asentable)
                                        </optgroup>
                                        @endif  
                                    @endforeach
                             </select>
                        </div>
                    </div>

                    <div class="col-1">
                        <button id="btnEditExenta" onclick="modalModificarCuenta()" style="margin-top:27px;" type="button" class="btn btn-info text-white">
                            <i class="fa fa-edit"></i>
                        </button>
                        </div>

                    <div class="col-12">
                            <div class="form-group">
                                <label>Concepto (*)</label>
                                <input type="text" class="form-control text-bold"  maxlength="300" required name="concepto" value="">
                            </div>
                        </div>
             
              <div class="col-12">
                    <button type="button" class="mr-1 btn btn-success btn-lg pull-right" id="btnSaveAnticipo"><b>Guardar</b></button>
                    
                    <button type="button" class="mr-1 btn btn-danger btn-lg pull-right" id="btnVolver"><b>Volver</b></button> 
                    
              </div>         
        </form>
 
    </div>
</div>
</div>
</section>

@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
    $(() => {
        main();
});



    function main() {
        calendar();
        formatNumber();
        $('.select2').select2();
        obtenerDataSelect();
        cuentaPredeterminada();
    } //



    $('#btnVolver').click(() => {
        redirectView();
    });

    $('#id_cliente').change(() => {
        obtenerDataSelect();
    });

    $('#ant_id_moneda').change(()=>{
        cuentaPredeterminada();
    });

    

 
    $('#btnSaveAnticipo').click((event) => {
        if(jQuery.isEmptyObject($('#id_grupo').val()) == false || jQuery.isEmptyObject($('#id_proforma').val()) == false|| jQuery.isEmptyObject($('#id_venta').val()) == false){
            console.log($('#id_grupo').val());
            console.log($('#id_proforma').val());
            if($("#formDataAnticipo").valid()) {
                modalConfirmar()
             }   

            $("#formDataAnticipo").validate({
                submitHandler: function(form){
                   // modalConfirmar()
                },
                validClass: "labelSuccess",
                errorClass: "labelError",
                rules: {
                    importe: {required: true},
                    cotizacion : {required: true},
                    id_moneda : {required: true},
                    concepto : {required: true}
                },
                messages: {
                       importe: "Introduce un importe",
                    cotizacion: "Introduce una cotización",
                    id_moneda : "Seleccione una moneda",
                    concepto: "Ingrese un concepto por favor"
                }
            });
        }else{
            $.toast({
                    heading: 'Error',
                    text: 'Se debe seleccionar un grupo o una proforma.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });        
        }
});
    

   

    //OBTENER DATOS DE SELECCION DE GRUPO Y PROFORMA
    function obtenerDataSelect(){

        $.ajax({
          type: "GET",
          url: "{{route('getLisAddAnticipo')}}",
          data:{ cliente_id: $('#id_cliente').val()} ,
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){
                    $.toast({
                            heading: 'Error',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });


		  },
          success: function(rsp){

            $('#id_grupo').empty();
            $('#id_proforma').empty(); 
            $('#id_venta').empty(); 
            $('#id_proforma').prop('disabled',false); 
            $('#id_grupo').prop('disabled',false); 
            $('#id_venta').prop('disabled',false); 
            if(rsp.ventas.length){
                var optionProforma = new Option('Seleccione Venta', '', false, false);
                $('#id_venta').append(optionProforma);
            
                $.each(rsp.ventas, function (key, item){
                    optionProforma = new Option(item.id, item.id, false, false);
                $('#id_venta').append(optionProforma)
                });

            } else {
                $('#id_venta').prop('disabled',true); 
            }
            if(rsp.proformas.length){
                var optionProforma = new Option('Seleccione Proforma', '', false, false);
                $('#id_proforma').append(optionProforma);
            
                $.each(rsp.proformas, function (key, item){
                    optionProforma = new Option(item.id, item.id, false, false);
                $('#id_proforma').append(optionProforma)
                });

            } else {
                $('#id_proforma').prop('disabled',true); 
            }

            if(rsp.grupos.length){
                var optionGrupo = new Option('Seleccione Grupo', '', false, false);
                $('#id_grupo').append(optionGrupo);

                $.each(rsp.grupos, function (key, item){
                optionGrupo = new Option(item.denominacion, item.id, false, false);
                $('#id_grupo').append(optionGrupo)
                });

            } else {  
                $('#id_grupo').prop('disabled',true); 
            }

            

                          }//success
                 }).done(function(){
                 	// $('#btnProcesarOP').prop('disabled',false);
                 });//AJAX

    }

   
	
          

  
function saveDataAnticipo(){

    $('#btnSaveAnticipo').prop('disabled',true);
    $('#cuenta_anticipo_cobranza').prop('disabled',false);
    event.preventDefault();

    return new Promise((resolve, reject) => { 

       //LIMPIA ESPACIOS VACIOS
    let dataString = $('#formDataAnticipo').find(':input').filter(function () {
                    return $.trim(this.value).length > 0
                }).serializeJSON({ 
                    customTypes: customTypesSerializeJSON
                });

    $.ajax({
          type: "GET",
          url: "{{route('saveAnticipoCliente')}}",
          data:dataString ,
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){  
            reject('Ocurrió un error en la comunicación con el servidor.');
		  },
          success: function(rsp){
          		if(rsp.err === true){
                    resolve('Los datos fueron procesados.');     
          		} else {
                    reject('Ocurrió un error al intentar almacenar los datos.');
          		}
            
                          }//success
                 }).done(function(){
                     $('#btnSaveAnticipo').prop('disabled',false);
                     $('#cuenta_anticipo_cobranza').prop('disabled',true);
                 });//AJAX

    });          
}//




		/* ================================================================================
										MODAL GUARDAR ANTICIPO
		   ================================================================================*/
				function modalConfirmar(){
					swal({
							title: "Generar Anticipo",
							text: "¿Está seguro que desea generar el Anticipo ?",
							icon: "warning",
							showCancelButton: true,
							buttons: {
								cancel: {
									text: "No, Cancelar",
									value: null,
									visible: true,
									className: "btn-warning",
									closeModal: false,
								},
								confirm: {
									text: "Si , Continuar",
									value: true,
									visible: true,
									className: "",
									closeModal: false
								}
							}
						}).then(isConfirm => {
							
							if (isConfirm) {
								$.when(saveDataAnticipo()).then((a)=>{ 
									swal("Éxito",a , "success");
									setTimeout( redirectView,1000);
								},(b)=>{
									swal("Cancelado", b, "error");
								});
								
							} else {
								swal("Cancelado", "La operación fue cancelada", "error");
							}
						});
				}
    
    /*===========================CUENTA PREDETERMINADA==================================*/
    function cuentaPredeterminada()
    {
        let dataString = {
                cuentas:['ANTICIPOS_CLIENTES'],
                id_moneda : $('#ant_id_moneda').val()
            };

        $.ajax({
            type: "GET",
            url: "{{route('getCuentaPredeterminada')}}",
            data: dataString,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
           
            },
            success: function (rsp) {
                
                $.each(rsp.data, (index,value)=>{
                    if(value.parametro == 'ANTICIPOS_CLIENTES'){
                            $(`#cuenta_anticipo_cobranza`).val(value.id_cuenta_contable).trigger('change.select2');
                        }
                });

            } //success
        }).done(function () {
      
        }); //AJAX

    }

    function modalModificarCuenta()
    {
      return  swal({
                    title: "GESTUR",
                    text: "¿Está seguro que desea modificar la cuenta contable?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Modificar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            swal("Éxito", 'Cuenta Contable Habilitada', "success");
                            $('#cuenta_anticipo_cobranza').prop('disabled',false);
                        } else {
                            swal("Cancelado", '', "error");
                        }
                });
                              
    }


/* =========================================================================================
                                            CLASES AUX
   =========================================================================================*/



   function redirectView(){
      
      $.blockUI({
                      centerY: 0,
                      message: "<h2>Redirigiendo a vista de Anticipo...</h2>",
                      css: {
                          color: '#000'
                      }
                  });

                  location.href ="{{ route('indexAnticipoCliente') }}";
  }




  function calendar() {
      $('.single-picker').datepicker({
          format: "dd/mm/yyyy",
          language: "es",
          orientation: "bottom",
      });
  }

  function formatNumber() {

      $('.format-number-control').inputmask("numeric", {
          radixPoint: ",",
          groupSeparator: ".",
          digits: 2,
          autoGroup: true,
          // prefix: '$', //No Space, this will truncate the first character
          rightAlign: false
      });

  }


</script>
@endsection