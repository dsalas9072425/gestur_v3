@extends('masters')
@section('title', 'Panel de Control')
@section('styles')


<style type="text/css">
    .correcto_col {
        height: 74px;
    }

    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
     .mr-1 {
         margin-right: 5px; 
     }
</style>
@parent
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title"> Detalle Anticipo Cliente</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


        <form class="row" id="formDataAnticipo" autocomplete="off">

                <div class="col-12 col-sm-4 col-md-3">
                    <div class="form-group">
                        <label>Scursal</label>
                        <select class="form-control select2" disabled name="id_sucursal:number" id="id_sucursal" style="width: 100%;">
                            @foreach ($sucursalEmpresa as $sucursal)
                            <option value="{{$sucursal->id}}">{{$sucursal->denominacion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Nro Recibo :</label>
                        <input type="text" class="form-control" disabled id="nro_recibo"  value="">
                        </div>
                    </div>
                
                <div class="col-12 col-sm-4 col-md-3">
                    <div class="form-group">
                        <label>Fecha de Pago</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                               <span id ="vencimiento" class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </div>
                           <input type="text" class="form-control pull-right single-picker" disabled name="fecha_pago:s_date" id="fecha_pago" value="">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-3">
                    <div class="form-group">
                        <label>Fecha de Venc</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                               <span id ="vencimiento" class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </div>
                               <input type="text" class="form-control pull-right single-picker" disabled name="fecha_vencimiento:s_date" id="fecha_vencimiento" value="">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-3">
                    <div class="form-group">
                        <label>Moneda</label>
                        <select class="form-control select2" name="id_moneda:number" disabled id="id_moneda" style="width: 100%;">
                            @foreach ($currency as $div)
                            <option value="{{$div->currency_id}}">{{$div->currency_code}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-4 col-md-3">
                    <div class="form-group">
                        <label>Nro Cheque</label>
                        <input type="text" class="form-control text-bold" id="nro_cheque" disabled name="nro_cheque" value="">
                    </div>
                </div>
              
                <div class="col-12 col-sm-4 col-md-3">
                    <div class="form-group">
                        <label>Proforma</label>
                        <input type="text" class="form-control text-bold" id="proforma_id" disabled name="proforma_id" value="">
                    </div>
                </div>

                <div class="col-12 col-sm-4 col-md-3">
                    <div class="form-group">
                        <label>Centro Costo</label>
                        <select class="form-control select2" disabled name="id_centro_costo:number" id="id_centro_costo" style="width: 100%;">
                            @foreach ($centro as $c)
                            <option value="{{$c->id}}">{{$c->nombre}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
        
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Cambio</label>
                            <input type="text" class="form-control format-number-control" disabled id="at_cotizacion"  value="">
                        </div>
                    </div>

                <div class="col-12 col-sm-4 col-md-3">
                    <div class="form-group">
                        <label>Importe</label>
                        <input type="text" class="form-control format-number-control" disabled id="at_importe" name="importe:convertNumber" value="">
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-3">
                    <div class="form-group">
                        <label>Saldo</label>
                        <input type="text" class="form-control format-number-control" disabled id="at_saldo" name="importe:convertNumber" value="">
                    </div>
                </div>

                <div class="col-12">
                        <div class="form-group">
                            <label>Beneficiario</label>
                            <select class="form-control select2" name="id_beneficiario:number" disabled id="id_beneficiario" style="width: 100%;">
                                @foreach ($beneficiario as $b)
                                    <option value="{{$b->id}}">{{$b->cliente_n}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Contable</label>
                            <select class="form-control select2" name="id_cuenta_contable:number" disabled id="id_cuenta_contable" style="width: 100%;">
                                    @foreach ($cuentas_contables as $cuenta)
                                        @if($cuenta->asentable)
                                        <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif   
                                            <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                            </option>
                                        @if($cuenta->asentable)
                                        </optgroup>
                                        @endif  
                                    @endforeach
                             </select>
                        </div>
                    </div>

                    <div class="col-12">
                            <div class="form-group">
                                <label>Concepto</label>
                                <input type="text" class="form-control text-bold" disabled id="concepto" name="concepto" value="">
                            </div>
                        </div>
             
              <div class="col-12">
                    <button type="button" class="mr-1 btn btn-success btn-lg pull-right" id="btnVolver"><b>Volver</b></button> 
                    <button type="button" class="mr-1 btn btn-info btn-lg pull-right" onclick="verExtracto({{$anticipo->id}})" id="btnExtracto"><b>Extracto Anticipo</b></button> 
                    @foreach ($btn as $boton )
                        <?php echo $boton; ?>
                    @endforeach
              </div>          
        </form>
    </div>
</div>
</div>
</section>

<!--======================================================
        MODAL DE FACTURA
========================================================== -->
    <!-- /.content -->
    <div id="modalFacturas" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 100%;margin-top: 5%;margin-left: 4%;">
        <!-- Modal content-->
            <div class="modal-content" style="width: 160%;">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Aplicar a factura</h2>
					<button type="button" class="closeModal"  data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <div class="modal-body" style="padding-top: 0px;">
					<form id="aplicarFactura">
						<div class="table-responsive table-bordered font-weight-bold">
							<table id="listado" class="table" style="width: 100%;font-size: small;">
								<thead>
									<tr>
										<th style="width: 15%">Factura</th>
										<th>Fecha</th>
										<th>Proforma</th>
										<th>Monto</th>
										<th>Moneda</th>
										<th>Saldo</th>
										<th>Cliente</th>
										<th>Pasajero</th>
                                        <th></th>
									</tr>
								</thead>
								<tbody style="text-align: center">

								</tbody>
							</table>
						</div>
						<div class="row">
                            <input type="hidden" class = "form-control" name="id_libro" id="id_libro" value="0" style="font-weight: bold;font-size: 15px;"/>
							<input type="hidden" class = "form-control" name="tipo" id="tipo" value="C" style="font-weight: bold;font-size: 15px;"/>
							<input type="hidden" class = "form-control" name="id_facturas" id="id_facturas" style="font-weight: bold;font-size: 15px;"/>
							<input type="text" class = "form-control" name="anticipo_id" id="anticipo_id" value="{{$anticipo->id}}" style="font-weight: bold;font-size: 15px;display:none"/>
							<div class="col-12 col-sm-4 col-md-3"></div>
							<div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
									<label>Saldo Factura</label>
									<input type="text" class = "form-control numeric" name="saldo_factura" value="" id="saldo_factura" style="font-weight: bold;font-size: 15px;" disabled="disabled"/>
								</div>
							</div>	
							<div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
									<label>Saldo Anticipo</label>
									<input type="text" class = "form-control numeric" name="saldo_credito" value="{{$anticipo->saldo}}" id="saldo_credito" style="font-weight: bold;font-size: 15px;"/>
								</div>
							</div>	
							<div class="col-12 col-sm-4 col-md-3">
								<br>
								<button  onclick ="guardarAnticipo()" class="pull-right btn btn-info btn-lg mr-1" type="button"><b>Guardar</b></button>
							</div>
						</div>	
					</form>	
                </div>
            </div>  
        </div>  
    </div>  
	  <!-- {{--============================================================================================
											MODAL ANULAR ANTICIPO
		 ============================================================================================ --}}-->

		 <div class="modal fade" id="modalAnularAnticipo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="closeModal"  data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <h4 class="modal-title"><i class="fa fa-fw fa-remove"></i> Anular OP</h4>
					</div>
					<div class="modal-body">


                     <div class="row">
                         <div class="col-12">
                            <h3>¿ Esta seguro que desea anular este anticipo ? </h3>    
                         </div>
                    </div>   
					</div>
					<div class="modal-footer">
                      <button type="button" class="btn btn-success btn-lg" id="btnCancelarAnulacion"><b>Cancelar</b></button>
                      <button type="button" class="btn btn-danger btn-lg" id="btnAnularConfirmacion"><b>Si, Anular</b></button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
              
        <input type="hidden" value="{{$anticipo->id}}" id="id_anticipo">
    
    {{--==============================================
			VER EXTRACTO
	============================================== --}}
	<div class="modal fade" id="modalExtracto" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document" style="margin-left: 5%;margin-top: 7%;">
			<div class="modal-content" style="width: 260%;">
				<div class="modal-header">
					<h5 class="modal-title">Ver Extracto</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-12" style="margin-bottom: 5px;">
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Total de Anticipo:</label>
								@php 
									$totalFactura = $anticipo->importe;
								@endphp
								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm" style="font-weight: 800; font-size: 20px;" value='{{number_format($totalFactura,2,",",".")}}' id="monto_factura" disabled>
								</div>
							</div>	
							<?php  
							   $cobrado = (float)$totalFactura - (float)$anticipo->saldo;
							?>
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Total Aplicado :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm" style="font-weight: 800; font-size: 20px;" id="monto_cobrado" value='{{number_format($cobrado,2,",",".")}}' disabled>
								</div>
							</div>
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Saldo Anticipo:</label>
								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm" style="font-weight: 800; font-size: 20px;" id="monto_saldo" value='{{number_format($anticipo->saldo,2,",",".")}}' disabled>
								</div>
							</div>
						</div>
						<br>
							<div class="col-12">
								<div class="table-responsive">
									<table id="listadoExtractos" class="table" style="width:100%">
										<thead>
											<tr>
												<th>Factura</th>
												<th>Fecha</th>
												<th>Importe<br>Factura</th>
												<th>Moneda<br> Factura</th>
												<th>Cotizacion</th>
												<th>Importe<br>Cobro</th>
												<th>Moneda<br>Cobro</th>
												<th>Forma<br>Cobro</th>
                                                <th></th>
											</tr>
										</thead>
										<tbody style="text-align: center">
										</tbody>
									</table>
								</div>
							</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><b>Cerrar</b></button>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>

<script type="text/javascript">
    $(() => {
        main();
    });

    function main() {
        loadAnticipo();
        formatNumber();
        $('.select2').select2();
 
    } //

    $('#btnVolver').click(() => {
        redirectView();
    });

    $('.btnAnularAnticipo').click(()=>{
        modalAnular();
    })

    $('#btnAnularConfirmacion').click(()=>{
        anularAnticipo();
    });

    $('#btnCancelarAnulacion').click(()=>{
        ocultarModalAnular();
    });

    function anularAnticipo(){

        $('#btnAnularConfirmacion').prop('disabled',true);

        $.ajax({
          type: "GET",
          url: "{{route('anularAnticipo')}}",
          data:{ id_anticipo: $('#id_anticipo').val()} ,
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){

                    $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

		      	$('#btnAnularConfirmacion').prop('disabled',false);
		  },
          success: function(rsp){
          		if(rsp.err === true){
                            redirectView();
          		} else {

                    $.toast({
                            heading: 'Error',
                            text: rsp.msj,
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        
                        $('#btnAnularConfirmacion').prop('disabled',false);
          		
          		}
            
                          }//success
                 });//AJAX


    }

    function redirectView(){

        ocultarModalAnular();
        $.blockUI({
						centerY: 0,
						message: "<h2>Redirigiendo a vista de Anticipo...</h2>",
						css: {
							color: '#000'
						}
					});

                    location.href ="{{ route('indexAnticipo') }}";
    }

  


    function formatNumber() {

        $('.format-number-control').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false
        });

    }




function loadAnticipo(){

    $("#id_sucursal").val(validarData('{{$anticipo->id_sucursal}}')).trigger('change.select2');
    $('#id_op').val('{{$anticipo->id_op}}');
    $('#fecha_pago').val(formatoFecha('{{$anticipo->fecha_pago}}'));
    $('#fecha_vencimiento').val(formatoFecha('{{$anticipo->fecha_vencimiento}}'));
    $('#id_moneda').val(validarData('{{$anticipo->id_moneda}}'));
    $('#nro_cheque').val(validarData('{{$anticipo->documento}}'));
    $('#id_centro_costo').val(validarData('{{$anticipo->id_centro_costo}}'));
    $('#at_cotizacion').val('{{$anticipo->cotizacion}}');
    $('#at_importe').val(validarData('{{$anticipo->importe}}'));
    $('#id_beneficiario').val('{{$anticipo->id_beneficiario}}');
    $('#id_cuenta_contable').val('{{$anticipo->id_cuenta_contable}}');
    @if(isset($anticipo->recibo->nro_recibo))
        $('#nro_recibo').val('{{$anticipo->recibo->nro_recibo}}');
    @endif
    $('#proforma_id').val('{{$anticipo->id_proforma}}');
    $('#concepto').val('{{$anticipo->concepto}}');
    dato = "{{$anticipo->saldo}}";
	estado = "{{$anticipo->id_estado}}";
    if(dato.includes("e-") == true){
        dato = 0;
    }
	if(dato == 0 || parseInt(estado)== 65){
		$('.aplicarAtc').css('display', 'none');
	};
    $('#at_saldo').val('{{$anticipo->saldo}}');
}

function modalAnular(){

    $('#modalAnularAnticipo').modal('show');
}
function ocultarModalAnular(){
    $('#modalAnularAnticipo').modal('hide');
}



   function validarData(data){
    //    console.log(data);
    if(data == 'null'){
        return '';
    }
    return data;
   }
   const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD',
					  minimumFractionDigits: 2
					});

function formatoFecha(texto){
        if(texto != '' && texto != null && texto != '0'){
          return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
        } else {
            return '';
        }
        
        }


    $("#listado").dataTable();
	$('.aplicarAtc').on('click',function(){ 	
		$.ajax({
                type: "GET",
                url: "{{route('getFacturasAplicar')}}",
				data: {
						datamoneda: $("#id_moneda").val(),
						idCliente: "{{$anticipo->id_beneficiario}}",
                        baseC: "{{$anticipo->cotizacion}}"
					},
                dataType: 'json',
                error: function(jqXHR,textStatus,errorThrown){},
                success: function(rsp){
										console.log(rsp);
										var oSettings = $('#listado').dataTable().fnSettings();
										var iTotalRecords = oSettings.fnRecordsTotal();

										for (i=0;i<=iTotalRecords;i++) {
											$('#listado').dataTable().fnDeleteRow(0,null,true);
										}

										$.each(rsp, function (key, item){
											if(jQuery.isEmptyObject(item.vendedor_n) == false){
												vendedor = item.vendedor_n;
											}else{
												vendedor ="";
											}
											accion = '<button type="button" id="btnAceptarProforma" class="btn btn-danger" onclick="calcular('+item.saldo_cotizado+','+item.id+')" style="width: 40px;background-color: #e2076a;padding-right: 5px;padding-left: 5px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-plus fa-lg"></i></button>';
											var dataTableRow = [
															item.nro_factura,
															item.fecha_format,
															item.id_proforma,
                                                            (!isNaN(parseFloat(item.monto_factura))) ? formatter.format(parseFloat(item.monto_factura)) : 0,
															item.currency_code,
                                                            formatter.format(parseFloat(item.saldo_factura)),
															item.cliente_n,
															item.pasajero_n,
                                                            accion
															];

										var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
										var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
										})	
									}	
			});	
			$("#modalFacturas").modal("show");
	});	

    function calcular(saldo,id){
        $("#id_facturas").val(id);
        $("#saldo_factura").val(saldo);
    }

    function guardarAnticipo(){ 
		if(jQuery.isEmptyObject($('#saldo_factura').val()) == false){
			if(parseFloat(clean_num($('#saldo_credito').val())) <= parseFloat(clean_num($('#saldo_factura').val()))){
				$('#saldo_factura').prop('disabled',false);
				var dataString = $('#aplicarFactura').serialize();	
				$.ajax({
						type: "GET",
						url: "{{route('guardarAnticipoFactura')}}",
						dataType: 'json',
						data: dataString,
						error: function(jqXHR,textStatus,errorThrown){},
						success: function(rsp){
							if(rsp =="OK"){
								$("#modalFacturas").modal('hide');
								$.toast({ 
										heading: 'Exito',
										text: 'Se han aplicado el anticipo a la/s factura/s.',
										position: 'top-right',
										showHideTransition: 'slide',
										icon: 'success'
									});  
							 	location.reload();	
							}else{
								$('#saldo_factura').prop('disabled',true);
								$.toast({
									heading: 'Error',
									text: rsp,
									showHideTransition: 'fade',
									position: 'top-right',
									icon: 'error'
								});				
							}	
						}	
					});
			}else{	
				$.toast({
						heading: 'Error',
						text: 'El monto a aplicar del anticipo tiene que ser mayor o igual al saldo de la factura',
						showHideTransition: 'fade',
						position: 'top-right',
						icon: 'error'
					});				
			}		
		}else{	
			$.toast({
					heading: 'Error',
					text: 'Seleccione una factura',
					showHideTransition: 'fade',
					position: 'top-right',
					icon: 'error'
				});				
		}
	}

    function verExtracto(idFactura){	 
        $("#listadoExtractos").DataTable();
        $("#modalExtracto").modal('show');
        $.ajax({
                type: "GET",
                url: "{{route('getExtractoAnticipo')}}",
                dataType: 'json',
                data: {
                        idFactura:idFactura
                    },
                error: function(jqXHR,textStatus,errorThrown){
                    reject('Ocurrió un error en la comunicación con el servidor.');
                },
                success: function(rsp){
                                    var oSettings = $('#listadoExtractos').dataTable().fnSettings();
                                    var iTotalRecords = oSettings.fnRecordsTotal();

                                    for (i=0;i<=iTotalRecords;i++) {
                                        $('#listadoExtractos').dataTable().fnDeleteRow(0,null,true);
                                    }
                                    $.each(rsp.exacto, function (key, item){
                                                console.log(item);
                                                btn = `<a href="{{route('verFactura',['id'=>''])}}/${item.id_libro_venta}"  class="bgRed">
                                                    <i class="fa fa-fw fa-search"></i>${item.nro_factura}</a><div style="display:none;">${item.id_recibo_nc}</div>`;
                                                accion = '<button type="button" id="btnAceptarProforma" class="btn btn-danger" onclick="calcular('+item.saldo_factura+','+item.id+')" style="width: 40px;background-color: #e2076a;padding-right: 5px;padding-left: 5px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-plus fa-lg"></i></button>';
                                              
                                                btns = '<button type="button" id="btnCancelar" class="btn btn-danger" onclick="anular('+item.id_anticipo+')" style="width: 40px;padding-right: 5px;padding-left: 5px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-close fa-lg"></i></button>';

                                                var dataTableRow = [
                                                                    btn,
                                                                    item.fecha_aplicacion,
                                                                    formatter.format(parseFloat(item.importe_item_recibo)),
                                                                    item.moneda_recibo,
                                                                    formatter.format(parseFloat(item.cotizacion)),
                                                                    formatter.format(parseFloat(item.importe_cobrado)),
                                                                    item.moneda_cobro,
                                                                    item.tipo,
                                                                    btns
                                                                ];

                                            var newrow = $('#listadoExtractos').dataTable().fnAddData(dataTableRow);
                                            var nTr = $('#listadoExtractos').dataTable().fnSettings().aoData[newrow[0]].nTr;
                                    })
                                    var winSize = {
                                        wheight : $(window).height(),
                                        wwidth : $(window).width()
                                    };
                                    var modSize = {
                                        mheight : $('#modalExtracto').height(),
                                        mwidth : $('#modalExtracto').width()
                                    };
                                    $('#modalExtracto').css({
                                                'padding-left' :  ((winSize.wheight - (modSize.mheight/2))/2),
                                    });
                        }	
                    });
    }

	function clean_num(n,bd=false){
			if(n && bd == false){ 
			n = n.replace(/[,.]/g,function (m) {  
					 				 if(m === '.'){
					 				 	return '';
					 				 } 
					 				  if(m === ','){
					 				 	return '.';
					 				 } 
					 			});
			return Number(n);
		}
		if(bd){
			return Number(n);
		}
		return 0;
		}

        $('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});


        function anular(id){
            return swal({
                        title: "GESTUR",
                        text: "¿Desea anular esta Aplicación de Anticipo?",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                            text: "No",
                                            value: null,
                                            visible: true,
                                            className: "btn-warning",
                                            closeModal: true,
                                        },
                                confirm: {
                                            text: "Sí, guardar",
                                            value: true,
                                            visible: true,
                                            className: "",
                                            closeModal: true
                                        }
                                }
                        }).then(isConfirm => {
                                    if (isConfirm) {
                                                $.ajax({
                                                        type: "GET",
                                                        url: "{{route('anularAplicacionAnticipo')}}",
                                                        data: {
                                                                idAnticipo:id
                                                            },
                                                        dataType: 'json',
                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                swal("Cancelado", "Ocurrió un error en la comunicación con el servidor", "error");
                                                        },
                                                        success: function (rsp) {
                                                                if (rsp.status == 'OK') {
                                                                    swal("Éxito",rsp.mensaje , "success");
                                                                    setTimeout( redirectReciboControl,2000);
                                                                } else {
                                                                    swal("Cancelado", 'Ocurrió un error al anular la Aplicación del Anticipo ',"error");
                                                                } 
                                                            } //success
                                                        }).done(()=>{

                                                        }); //AJAX
                                            } else {
                                                swal("Cancelado", "La operación fue cancelada", "error");
                                            }
                        });


        }

        function redirectReciboControl(){
            location.reload();	
        }


</script>
@endsection