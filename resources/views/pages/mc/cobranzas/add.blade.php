@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.table th, .table td {
    			padding: 5px;;
		}
	</style>
@endsection
@section('content')

@include('flash::message') 

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Nuevo Pago de Anticipo</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
				<div class="card-body">

					<form id="frmGrupo" method="post" action="{{route('storeGrupo')}}">
								<div class="table-responsive" style="margin-left: 2%;">
		 							<table id="listadoPagoAdelanto" class="table" style="width:97%">
						                <thead>
											<tr>
												<th>Fecha</th>
												<th style="width: 126px;">Tipo</th>
												<th style="width: 150px;">Numero</th>
												<th>Forma de Pago</th>
												<th>Nª Recibo</th>
												<th>Nª Comprobante</th>
												<th>Moneda</th>
												<th>Monto</th>
												<th></th>
								            </tr>
						                </thead>
						                <tbody>
						                	
						                </tbody>
						                <tfoot>
							                <tr>
												<th style="text-align: center;">
													<div class="input-group">
										    			<div class="input-group-prepend">
										    				<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
										    			</div>
										    			<input class="form-control pull-right" name="datos[0]['fecha']" id="fecha_senha_0" placeholder="Fecha de Pago" >
										    		</div>
										    	</th>
										    	<th>
													<select class="form-control select2" name="datos[0]['tipobase']"  id="tipo_0" tabindex="1" style="width: 100%;">
														<option value="">Seleccione Tipo</option>
														<option value="1">Grupo</option>
														<option value="2">Proforma</option>
									            	</select>
												</th>
												<th>
													<!--<select class="form-control select2" name="datos[0]['numero']"  id="numero" tabindex="1" style="width: 100%;" required>
														<option value="0"></option>
									            	</select>-->
									            	<div id="numeroDiv">
	 													<select class="form-control select2" name="datos[0]['id_grupo']" id="numero" style="width: 100%;" tabindex="7">
	 													</select>	
	 												</div>	
	 											    <div id="numerosDiv" style="display: none">		
	 													<select  class="form-control select2" name="datos[0]['id_proforma']" id="numeros" style="width: 100%;" tabindex="7">
									            		</select>
									            	</div>	

												</th>
												<th>
													<select class="form-control select2" name="datos[0]['forma_pago']"  id="forma_pago_0" tabindex="1" style="width: 100%;" required>
														<option value="0">Seleccione Forma de Pago</option>
														@foreach($formaPagos as $key=>$formaPago)
															<option value="{{$formaPago->id}}">{{$formaPago->denominacion}}</option>
														@endforeach
									            	</select>
												</th>
												<th>
													<input type="text" class = "form-control" name="datos[0]['recibo']" id="recibo_0" placeholder="Nª Recibo" value=""/>
												</th>
												<th>
													<input type="text" class = "form-control" name="datos[0]['comprobante']" id="comprobante_0" placeholder="Nª Comprobante" value="" />
												</th>
												<th>
													<select class="form-control select2" name="datos[0]['moneda']"  id="moneda_0" tabindex="1" style="width: 100%;">
														@foreach($currency as $key=>$moneda)
																<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
														@endforeach
									            	</select>
												</th>
												<th>
													<input type="text" class = "form-control numerico" name="datos[0]['monto']" id="monto_0" placeholder="Monto de Pago" value="0" onchange="formatNumber.new(this)" onkeypress="return justNumbers(event);"/>
												</th>
												<th>
													<a onclick="guardarAdelanto()" title="Guardar Fila" class="btn btn-success" style="margin-left:5px;padding-left: 6px;padding-right: 6px;color:#fff;border-left-width: 0px;border-right-width: 0px;border-bottom-width: 0px;border-top-width: 0px;" role="button"><i class="fa fa-fw fa-plus"></i></a>
												</th>
											</tr>
						                </tfoot>
					              </table>
							   </div> 
					</form> 

				</div>
			</div>
		</section>
	</section>
							   

@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script>

		var d = new Date();

		var month = d.getMonth()+1;
		var day = d.getDate();

		var output = d.getFullYear() + '-' +
		    (month<10 ? '0' : '') + month + '-' +
		    (day<10 ? '0' : '') + day;

		console.log(output);		    


		$('.select2').select2();
		$( "#fecha_senha_0" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd',
								    maxDate: output,
								   /* widgetPositioning: {
												    horizontal: 'auto',
												    vertical: 'bottom'
									 				 }*/
									});

		$('#fecha_senha_0').datepicker("setDate", new Date(new Date()));

		$('#tipo_0').on('change',function(){

			if($('#tipo_0').val() == 1){
				$("#numeroDiv").show();
				$("#numerosDiv").hide();
				dataString = { tipo :$('#tipo_0').val()};
				$.ajax({
						type: "GET",
						url: "{{route('getListadoPago')}}",
						dataType: 'json',
						placeholder: "Seleccione un Grupo",
						async:false,
						data: dataString,
								error: function(){
										 $.toast({
												    heading: 'Error',
												    text: 'Ocurrio un error en la comunicación con el servidor',
												    position: 'top-right',
												    showHideTransition: 'fade',
												    icon: 'error'
												});
									},
								success: function(rsp){
											$("#numero").append($("<option></option>").attr("value", 0).text('Seleccione un Grupo'));
											$.each(rsp, function(index,value){	
												console.log(value.descripcion);
												$("#numero").append($("<option></option>").attr("value", value.id).text(value.descripcion)); 
											})	
									$("#numero").css('width', '250px');
								}
						})	
			}else{
				$("#numerosDiv").show();
				$("#numeroDiv").hide();
				$("#numeros").select2({
					    ajax: {
					            url: "{{route('datosProforma')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un Proforma",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 4,
			    });

			}	

		});	



		function guardarAdelanto(){
			console.log($("#numero").val());
			console.log($("#numeros").val() );
			if(parseInt($("#monto_0").val()) == 0 || parseInt($("#monto_0").val() == "")){
				$.toast({
						heading: 'Error',
						text: 'Ingrese un monto',
						position: 'top-right',
						showHideTransition: 'fade',
						icon: 'error'
					});							
			}else{
				if(($("#numero").val() === null) && ($("#numeros").val() === null)){
			       		$.toast({
							heading: 'Error',
							text: 'Ingrese un numero de Proforma o un Grupo',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
							});	

				}else{

					 dataString = { 
			        				id_grupo :$('#numero').val(), 
			        				fecha :$('#fecha_senha_0').val(), 
			        				formaPago :$('#forma_pago_0').val(), 
			        				recibo :$('#recibo_0').val(), 
			        				comprobante :$('#comprobante_0').val(), 
			        				moneda :$('#moneda_0').val(), 
			        				monto :$('#monto_0').val(), 
			        				tipo:$('#tipo_0').val(), 
			        				id_proforma:$('#numeros').val()
			        			};
					$.ajax({
							type: "GET",
							url: "{{route('getAdelanto')}}",
							dataType: 'json',
							async:false,
							data: dataString,
								error: function(){
										 $.toast({
												    heading: 'Error',
												    text: 'Ocurrio un error en la comunicación con el servidor',
												    position: 'top-right',
												    showHideTransition: 'fade',
												    icon: 'error'
												});
									},
								success: function(rsp){
									if(rsp.mensaje == 'OK'){
											console.log(rsp);
											if(jQuery.isEmptyObject(rsp.fecha) == false){
													var fechaIn = rsp.fecha;
													var $In = fechaIn.split('-');
													fecha_in = $In[2]+"/"+$In[1]+"/"+$In[0];
											}else{
													fecha_in = "";
											}	
							       		    var nFilas = $("#listadoPagoAdelanto tr").length;
							       		    i = nFilas - 1;

											 
											tablas = `<tr id="rows${rsp.id}">
											<td>
												<div class="input-group">
													<div class="input-group-prepend">
														<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
													</div>
													<input type="text" class="form-control pull-right fecha" name="data['${i}'][fecha]"
														placeholder="Fecha de Penalidad" value="${fecha_in}" value="">
												</div>
											</td>
											<td><input type="text" maxlength="2" required value="${rsp.tipo}" class="form-control input-sm"
													name="data['${i}'][monto]" style="text-align: -webkit-center;" /></td>

											<td><input type="text" maxlength="2" required value="${rsp.lista}" class="form-control input-sm"
													name="data['${i}'][monto]" style="text-align: -webkit-center;" /></td>

											<td><input type="text" maxlength="2" required value="${rsp.forma_pago}" class="form-control input-sm"
													name="data['${i}'][monto]" style="text-align: -webkit-center;" /></td>
											<td><input type="text" maxlength="2" required value="${rsp.nro_recibo}" class="form-control input-sm"
													name="data['${i}'][monto]" /></td>';
											<td><input type="text" maxlength="2" required value="${rsp.nro_comprobante}" class="form-control input-sm"
													name="data['${i}'][monto]" /></td>';
											<td><input type="text" maxlength="2" required value="${rsp.denominacionMoneda}" class="form-control input-sm"
													name="data['${i}'][monto]" /></td>';
											<td><input type="text" maxlength="2" required value="${formatter.format(parseFloat(rsp.monto))}"
													class="form-control input-sm" name="data['${i}'][monto]" /></td>
											<td>
												<a onclick="eliminarFilaPagos('${rsp.id}')" title="Eliminar" class="btn btn-danger"
													style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;"
													role="button">
													<i class="fa fa-times"></i></a>
											</td>
											</tr>`;


											$('#listadoPagoAdelanto tbody').append(tablas);
											$.toast({
												heading: 'Exito',
												text: 'Se ha realizado el pago',
												position: 'top-right',
												showHideTransition: 'slide',
												icon: 'success'
											});  
											$('#fecha_senha_0').val('');
											$('#recibo_0').val('');
											$('#comprobante_0').val('');
											$("#forma_pago_0").val(0).trigger('change.select2');
											$("#moneda_0").val(143).trigger('change.select2');
											$("#tipo_0").val(0).trigger('change.select2');
											$("#numero").val(0).trigger('change.select2');
											$("#monto_0").val(0);
											$("#numeros").val(0).trigger('change.select2');

									}else{
											$.toast({
												heading: 'Error',
												text: 'Ya se ha ingresado un comprobante con el mismo monto',
												position: 'top-right',
												showHideTransition: 'fade',
												icon: 'error'
													});							
										}		
								}	
						})	


				}	

			}	


		}	
		function eliminarFilaPagos(idFila){
		 	dataString = { id_fila :idFila};
			$.ajax({
					type: "GET",
					url: "{{route('getEliminarAdelanto')}}",
					dataType: 'json',
					async:false,
					data: dataString,
					error: function(){
								$.toast({
									heading: 'Error',
									ext: 'Ingrese algun dato para proceder al guardado',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
										});
					},
					success: function(rsp){
										if(rsp.status == 'OK'){
											$.toast({
												heading: 'Exito',
												text: rsp.mensaje,
												position: 'top-right',
												showHideTransition: 'slide',
											    icon: 'success'
											});  
											$('table#listadoPagoAdelanto tr#rows'+idFila).remove();
											$("#row"+idFila).remove();
											total = parseFloat($('#monto_total_0').val());
											console.log(total);
											console.log(rsp.monto);
											totalFinal = parseFloat(total) - parseFloat(rsp.monto);
											$('#monto_total_0').val(totalFinal);
										}else{
											$.toast({
												heading: 'Error',
												text: rsp.mensaje,
												position: 'top-right',
												showHideTransition: 'fade',
												icon: 'error'
													});
										}	
								}	
				});				


		}

		function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

		var formatNumber = {
		 separador: ".", // separador para los miles
		 sepDecimal: ',', // separador para los decimales
		 formatear:function (num){
		 num +='';
		 var splitStr = num.split('.');
		 var splitLeft = splitStr[0];
		 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
		 var regx = /(\d+)(\d{3})/;
		 while (regx.test(splitLeft)) {
		 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
		 }
		 return this.simbol + splitLeft +splitRight;
		 },
		 new:function(num, simbol){
		 this.simbol = simbol ||'';
		 return this.formatear(num);
		 }
		}
		formatNumber.new($('#monto_seña').val()) // retorna "123.456.779,18"

		$('.numerico').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

		const formatter = new Intl.NumberFormat('de-DE', {
							  currency: 'USD'
							});
		{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
			NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}
		function clean_num(n,bd=false){
			if(n && bd == false){ 
			n = n.replace(/[,.]/g,function (m) {  
					 				 if(m === '.'){
					 				 	return '';
					 				 } 
					 				  if(m === ','){
					 				 	return '.';
					 				 } 
					 			});
			return Number(n);
		}
		if(bd){
			return Number(n);
		}
		return 0;
		}

	</script>
@endsection
