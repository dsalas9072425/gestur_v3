
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	  <link rel="stylesheet" href="{{asset('mC/css/select.dataTables.min.css')}}"> 
@endsection
@section('content')

<style type="text/css">
	

	.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

.form-button-cabecera{
			margin-bottom: 5px !important;
		}	

@-moz-document url-prefix() {
  fieldset { display: table-cell; }
}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	.select2-container *:focus {
        outline: none;
    }


    .correcto_col { 
		height: 74px;
     }

	 input.form-control:focus ,.select2-container--focus , button:focus{
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

	.input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
	




</style>


<section id="base-style">

	<div class="card" style="border-radius: 14px;">
		<div class="card-header" style="border-radius: 14px;">
			<h4 class="card-title">Emitir Recibos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


				<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="tabHome" data-toggle="tab" aria-selected="true"
							href="#home"><b><i class="fa fa-fw fa-dollar"></i> Documentos </b></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tabConfirm" data-toggle="tab" aria-selected="true" href="#pago"><b><i
									class="fa fa-fw fa-file"></i> Emitir Recibo</b></a>
					</li>

				</ul>


				{{--======================================================
							LISTA PAGO PROVEEDOR
				====================================================== --}}

				<div class="tab-content mt-1">
					<section id="home" class="tab-pane active" role="tabpanel">

						<form id="consultaFactura" autocomplete="off">
							<small class="badge bg-blue">* Se puede seleccionar hasta 40 documentos</small>


							<div class="row">

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Cliente</label>
										<!--{{--<select class="form-control select2" name="idCliente" id="idCliente"
											tabindex="1" style="width: 100%;">
											<option value="">Seleccione un cliente</option>
											@foreach ($cliente as $pro)
												@if($pro->activo == true)
													@php
														$activo = 'ACTIVO';
													@endphp
												@else
													@php
														$activo = 'INACTIVO';
													@endphp
												@endif
												@php
													$ruc = $pro->documento_identidad;
													if($pro->dv){
														$ruc = $ruc."-".$pro->dv;
													}
												@endphp
											<option value="{{$pro->id}}"> {{$ruc}} - {{$pro->nombre}} - {{$pro->nombre}} {{$pro->apellido}} - {{$pro->id}} <b>({{$activo}})</b></option>

											@endforeach
										</select> --}}-->
										<select lass="form-control select2" name="idCliente" id="idCliente" tabindex="1" style="width: 100%;">												
											<option value="">Seleccione Cliente</option>
										</select>

									</div>
								</div>
								<input type="hidden" name="tipo_operacion" id="tipo_operacion" value="1">

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Moneda</label>
										<select class="form-control select2" name="idMoneda" id="idMoneda" tabindex="2"
											style="width: 100%;">
											<option value="">Todos</option>
											@foreach ($currency as $div)
											<option value="{{$div->currency_id}}">{{$div->currency_code}}</option>
											@endforeach
										</select>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Proforma</label>
										<input type="number" class="form-control" maxlength="15" id="idProforma" tabindex="3"
											name="idProforma" value="">
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Nro. Factura</label>
										<input type="text" class="form-control" maxlength="25" id="nroFactura" tabindex="4"
											name="nroFactura" value="">
									</div>
								</div>

					


								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Checkin Desde - Hasta</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" maxlength="23" class="form-control pull-right fecha calendar"
												tabindex="5" name="checkin_desde_hasta:p_date" id="checkin_desde_hasta"
												value="">
										</div>
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Fecha Facturación Desde - Hasta</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
											<input type="text" maxlength="23" class="form-control pull-right fecha calendar"
												tabindex="6" name="facturacion_desde_hasta:p_date"
												id="proveedor_desde_hasta" value="">
										</div>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3" style="display:none">
									<div class="form-group">
										<label>Estado Cobro </label>
										<select class="form-control select2" name="estadoCobro" tabindex="7"
											id="estadoCobro" style="width: 100%;">
											<option value="">Todos</option>
											@foreach ($estadoCobro as $estado)
											<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
											@endforeach
										</select>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3" style="display:none">
									<div class="form-group">
										<label>Pendiente </label>
										<select class="form-control select2" name="pendiente" tabindex="8"
											id="filtro_pendiente" style="width: 100%;">
											<option value="">Todos</option>
											<option value="true">SI</option>
											<option selected="selected" value="false">NO</option>


										</select>
									</div>
								</div>


								<div class="col-6 col-sm-3 col-md-3" style="display:none">
									<div class="form-group">
										<label>Cobrado</label>
										<select class="form-control select2" name="cobrado" tabindex="9"
											id="cobrado" style="width: 100%;">
											<option value="">Todos</option>
											<option value="SI">SI</option>
											<option selected="selected" value="NO">NO</option>


										</select>
									</div>
								</div>
				
								<div class="col-12 mt-1 mb-1">
									<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
									<button type="button" id="btnProcesar" tabindex="11" class="btn btn-success btn-lg pull-right mr-1"><b>Confirmar</b></button>
									<button tabindex="10" class="pull-right btn btn-info btn-lg mr-1" id="btnBuscar" type="button"><b>Buscar</b></button>

								</div>

							</div>
						</form>
						<div class="table-responsive">
							<table id="listado" class="table table-hover table-condensed nowrap" style="width: 100%;">
								<thead>
									<tr>
										<th></th>
										<th>Nro. Documento</th>
										<th>Tipo Documento</th>
										<th>Cliente</th>
										<th>Vencimiento</th>
										{{-- <th>Compromiso</th> --}}
										<th>Saldo</th>
										<th>Pagar</th>
										<th>Moneda</th>
										<th>Monto</th>
										<th>Estado</th>
										<th>Proforma</th>
										<th>Fecha Facturacion</th>
										<th>Vendedor</th>
										<th>Pendiente</th>
										<th>Libro</th>


									</tr>
								</thead>
								<tbody style="text-align: center">
								</tbody>
							</table>
						</div>

					</section>




					{{--======================================================
				VISTA PAGO
		====================================================== --}}

					<section id="pago" class="tab-pane" role="tabpanel">

						<div class="row">
							{{-- <div class="col-12 col-sm-3">
								<div class="form-group">
									<label>Sucursal</label>
									<select class="form-control select2" name="cf_sucursal" id="cf_sucursal"
										style="width: 100%;">
										@foreach ($sucursalEmpresa as $suc)
										<option value="{{$suc->id}}">{{$suc->denominacion}}</option>
										@endforeach
									</select>
								</div>
							</div> --}}

							{{-- <div class="col-12 col-sm-3">
								<div class="form-group">
									<label>Centro de Costo</label>
									<select class="form-control select2" name="cf_centro_costo" id="cf_centro_costo"
										style="width: 100%;">
										@foreach ($centro as $suc)
										<option value="{{$suc->id}}">{{$suc->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div> --}}

							<div class="col-12 col-sm-3">
								<div class="form-group">
									<label>Operación Moneda</label>
									<input type="text" class="form-control" id="cf_currency_code" disabled name=""
										value="">
								</div>
							</div>

							<div class="col-12 col-sm-3">
								<div class="form-group">
									<label>Cotización </label>
									<input type="text" class="form-control control_space formatInput" readonly
										id="cf_cotizacion" name="" value="0">
								</div>
							</div>

							<div class="col-12 col-sm-3">
								<div class="form-group">
									<label>Gestor</label>
									<select class="form-control select2" name="cf_gestor" id="cf_gestor"
										style="width: 100%;">
										@foreach ($gestor as $gest)
										<option value="{{$gest->id}}">{{$gest->nombre}} {{$gest->apellido}}</option>
										@endforeach
									</select>
								</div>
							</div>


							<div class="col-12 col-sm-3">
								<div class="form-group">
									<label>Fecha</label>
									<div class="input-group">
										<div class="input-group-prepend" style="">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
										<input type="text" class="form-control pull-right single-picker" tabindex="5"
											name="fecha_emision:s_date" maxlength="10" id="fecha_emision" value="">
									</div>
								</div>
							</div>
 
							<div class="col-12 col-sm-3" style="display: none">
								<div class="form-group">
									<label>Nro. Recibo </label>
									<input type="text" readonly class="form-control control_space" id="cf_numero_recibo" name="cf_numero_recibo" >
								</div>
							</div>




							<div class="col-12">
								<h4>Totales</h4>
								<hr style="margin-top: 1px;">

							</div>
						</div>

						{{--======================================================
						MONTO COTIZADO
		====================================================== --}}

						<form class="row" id="formTotales" autocomplete="off">
							<div class="col-4">
								<div class="row">

								<div class="col-12">
									<small><b>Montos cotizados <span class="bg-blue badge">USD</span></b> </small><i
										class="fa fa-fw fa-arrow-down"></i>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Total Recibo</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-plus"></i></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly
												id="cotizado_total_factura" value="0">
										</div>

									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Anticipos</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cotizado_anticipo"
												class="form-control clear_input_txt formatInput" readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Nota Credito</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cotizado_nc"
												class="form-control clear_input_txt formatInput" readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Canje</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cotizado_canje"
												class="form-control clear_input_txt formatInput" readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Total</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><b>=</b></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly
												value="0" id="cotizado_pago_neto">
										</div>
									</div>
								</div>
							</div>
						</div>




							{{--======================================================
						MONTO COSTO PROVEEDOR
		====================================================== --}}

							<div class="col-4">
								  <div class="row">
								<div class="col-12">
									<small><b>Montos Compra <span class="bg-blue badge  cf_currency_code"></span></b>
									</small><i class="fa fa-fw fa-arrow-down"></i>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Total Recibo</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-plus"></i></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly
												id="cf_total_pago" value="0">
										</div>

									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Anticipos</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cf_anticipo"
												class="form-control clear_input_txt formatInput" readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Nota Credito</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cf_nota_credito"
												class="form-control clear_input_txt formatInput" readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Canje</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fa fa-minus"></i></span>
											</div>
											<input type="text" id="cf_canje"
												class="form-control clear_input_txt formatInput" readonly value="0">
										</div>
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Total</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-equals"></i></span>
											</div>
											<input type="text" class="form-control clear_input_txt formatInput" readonly
												value="0" id="cf_neto_pago">
										</div>
									</div>
								</div>
							</div>
						 </div>

							{{--======================================================
						RETENCIONES PARA COMPRAS EN GUARANIES
		====================================================== --}}

						<!--	<div class="col-4">
								<div class="row">
								<div class="col-12">
									<small><b>Retención <span class="bg-blue badge">PYG</span></b> </small><i
										class="fa fa-fw fa-arrow-down"></i>
								</div>


								<div class="col-12 col-sm-6">
									<div class="form-group">
										<label>GRAVADA 10%</label>
										<input type="text" class="form-control clear_input_txt formatInput" readonly
											id="gravada_10" value="0">
									</div>
								</div>

								<div class="col-12 col-sm-6">
									<div class="form-group">
										<label>IVA 10%</label>
										<input type="text" class="form-control clear_input_txt formatInput" readonly
											id="iva_10" value="0">
									</div>
								</div>

								<div class="col-12">
									<div class="form-group">
										<label>Ret IVA 10%</label>
										<input type="text" class="form-control clear_input_txt formatInput" readonly
											id="ret_iva_10" value="0">
									</div>
								</div>

	
								<div class="col-12">
									<div class="form-group">
										<label>Total GS</label>
										<input type="text" class="form-control clear_input_txt formatInput" readonly
											id="cf_neto_pago_gs" value="0">
									</div>
								</div>
							</div>
						</div>

					-->
					</form>






						<div class="row">


							<div class="col-12">
								<hr>
							</div>
							<div class="col-12 form-horizontal">

								<div class="form-group">
									<label class="col-sm-2 control-label">Concepto:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control clear_input_txt" id="cf_concepto">
									</div>
								</div>


							</div>

							<div class="col-12" style="margin-top: 20px;">
								<button type="button" class="btn btn-success btn-lg pull-right font-weight-bold"
									id="btnProcesarOP"><b>Crear Recibo</b></button>
								<button type="button" class="btn btn-danger btn-lg pull-right"
									style="margin-right: 10px;" onclick="cancelarOp()" id=""><b>Cancelar
										Operación</b></button>
							</div>

						</div>




					</section>

				</div>


			</div>
		</div>

</section>
{{-- BASE STYLE --}} 







{{-- ========================================
   			MODAL NUMERO DE OP
   	========================================  --}}		

	   <div class="modal fade" id="modalNumOp" aria-labelledby="myModalLabel">
		<div class="modal-dialog">
		  <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
			  </button>
			  <h4 class="modal-title" id="" style="font-weight: 800;">
				  <i class="fa fa-fw fa-send-o"></i>
				  NÚMERO DE RECIBO
			  </h4>
			</div>

			<div class="modal-body">
			<div class="row">
			
						  <h1 style="text-align:center">RECIBO NÚMERO : <span id="num_recibo">01</span></h1> 	
						  <div class="row">
						
						  <button type="submit" class="mr-1 btn btn-success btn-lg center-block" id="confirmarOpModal" data-dismiss="modal" style="margin:auto"><b>Confirmar</b></button>
							
						  </div>
			  </div> 	
			  </div>			            	
							
		
		  </div>
		</div>
	  </div>











    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script type="text/javascript" src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.min.js"></script>

	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	
<script type="text/javascript">



var msj_anticipo = '';
initConfig();
// debugDesarrollo();

$(document).ready(function(){
$('.select2').select2();
calendar();
// loadDatatable();	
controlNumFormat();
// enterSearchEventDatatable();
// generarConcepto();
});
	






	/*-==============================================
			FILTROS BUSQUEDA
	============================================== */
	function calendar()
	{
			//CALENDARIO OPCIONES EN ESPAÑOL 
			let locale = {
						format: 'DD/MM/YYYY',
						cancelLabel: 'Limpiar',
						applyLabel: 'Aplicar',					
							fromLabel: 'Desde',
							toLabel: 'Hasta',
							customRangeLabel: 'Seleccionar rango',
							daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
							monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
													'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
													'Diciembre']
						};


			$('.calendar').daterangepicker({timePicker24Hour: true,
											timePickerIncrement: 30,
											autoUpdateInput: false,
											locale: locale
														});
			$('#proveedor_desde_hasta').val('');
			$('#proveedor_desde_hasta')
			.on('cancel.daterangepicker', function(ev, picker) {
						$(this).val('');})
			.on('apply.daterangepicker', function(ev, picker) {
						$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});		

			$('#checkin_desde_hasta').val('');
			$('#checkin_desde_hasta')
			.on('cancel.daterangepicker', function(ev, picker) {
						$(this).val('');})
			.on('apply.daterangepicker', function(ev, picker) {
						$(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
			});	


			$('.single-picker').datepicker({ 
						format: "dd/mm/yyyy", 
						language: "es"
						});
			$('.single-picker').datepicker('update', new Date());			
    }
     

     /*FUNCION PARA ALMACENAR DATOS --}}	*/
		const operaciones = {
			data:{},
			cabecera:{},
			total_pago:0,
			total_nc:0,
			total_pago_neto:0,
			total_anticipo:0,
			iva10:0,
			gravada10:0,
			retencion:0,
			num_canc: [],
			num_a_cta: [],
			msj:[],
			clear : function(x){
				this.data = {};
				this.cabecera = {};
				this.total_pago = 0;
				this.total_nc = 0;
				this.total_pago_neto = 0;
				this.total_anticipo = 0;
				this.iva10 = 0;
				this.num_canc = [];
				this.num_a_cta = [];
				this.gravada10 = 0;
				this.retencion = 0;
			}
		}





	/*============================================================================================
							DATATABLE Y LOGICA SELECCION PROVEEDOR
	============================================================================================ */

			
	function limpiar()
		{
			$('#idCliente').val('').trigger('change.select2');
			// $('#tipo_operacion').val('1').trigger('change.select2');
			$('#estado').val('1').trigger('change.select2');
			$('#idMoneda').val('').trigger('change.select2');
			$('#estadoCobro').val('').trigger('change.select2');
			$('#filtro_pendiente').val('false').trigger('change.select2');
			$('#cobrado').val('NO').trigger('change.select2');
			$('#idProforma').val('');
			$('#nroFactura').val('');
			$('#checkin_desde_hasta').val('');
			$('#proveedor_desde_hasta').val('');
		}


		$('#btnBuscar').click(()=>{
				let cliente = $('#idCliente').val();

				if(cliente == ''){

					$.toast({	
				    heading: '<b>Atención</b>',
				    position: 'top-right', 
				    text: 'Debe seleccionar un cliente para realizar la búsqueda',
				    width: '400px',
				    hideAfter: false,
				    icon: 'info'
						});	

				} else {
					loadDatatable();
				}
		});

		 var table;	
		function loadDatatable()
		{

			let form = $('#consultaFactura').serializeJSON({
				customTypes: customTypesSerializeJSON
			});
			let input;

			 table = $("#listado").DataTable({
			 	destroy: true,
			 	keys: true,
			 	ajax: {
			 		url: "{{route('listadoCobroAjax')}}",
			 		data: form,
			 		error: function (jqXHR, textStatus, errorThrown) {
			 			msjToast('', 2, jqXHR.status, textStatus);
			 			$.unblockUI();
			 		}

			 	},
			 	"columns": [

                 {
                     data: function(x){

                        input = '';
			 				if (Number(x.saldo) < 0 | x.pendiente == true) {
			 					input = '<input type="hidden" class="activeCheck" value="true" /> ';
			 				} else {
			 					input = '<input type="hidden" class="activeCheck" value="false" /> ';
                            }
                             
                        input += `	  <input type="hidden" class="id_documento" 		value="${x.id}"> 
   									  <input type="hidden" class="id_tipo_documento" 	value="${x.id_tipo_documento}">
									  <input type="hidden"  class="id_cliente" 			value="${x.cliente_id}">
								      <input type="hidden" class="id_moneda" 			value="${x.id_moneda_venta}">
									  <input type="hidden" class="iva10" 				value="${x.total_iva}">
									  <input type="hidden" class="gravada10" 			value="${x.total_gravadas}">
									  <input type="hidden" class="nro_documento" 		value="${x.nro_documento}">
									  <input type="hidden" class="origen_libro" 		value="${x.origen_libro}">`;
                                      
                                    //   //console.log(x.total_gravadas);

                         return input;
                     }
				 },
                 {
                     data:function(x){
						btn = ``;
						//console.log('Num_documento',x.id_tipo_documento);
						
						if(x.id_tipo_documento === 1 || x.id_tipo_documento === 5){
							//console.log(x.id_documento);
							if(x.id_documento > 89999){
			     			 		btn = `<a href="{{route('verFactura',['id'=>''])}}/${x.id_documento}"  class="bgRed">
                    						<i class="fa fa-fw fa-search"></i>${x.nro_documento}</a><div style="display:none;">${x.id_documento}</div>`;		
			     			 	} else {
			     			 		btn = `<a  class="bgRed">${x.nro_documento}</a><div style="display:none;">${x.id_documento}</div>`;
			     			 	}
						} else if (x.id_tipo_documento === 2)
							btn = `<a href="{{route('verNota',['id'=>''])}}/${x.id_documento}"  class="bgRed">
                    						<i class="fa fa-fw fa-search"></i>${x.nro_documento}</a><div style="display:none;">${x.id_documento}</div>`;		
						else {
							btn = x.nro_documento;
						}
			     			 	 
			     			 	
			     			  return btn; 
							 
							 

                         return x.nro_documento
                     }
				 },
                 {
                     data:'tipo_documento_n'
                 },
                 {
                    data: function(x){
                        return `<span class="cliente_nombre">${x.cliente}</span>`;
                    }
                 },
                 {
                     data:'vencimiento_format'
                 },
                 {
                    data:function(x){
                        let saldo = x.saldo;
			 				if (saldo === null || saldo === undefined) {
			 					saldo = 0;
			 				}
			 				var input = `
					           		 	<input type="hidden" value="${formatCurrency(x.id_moneda_venta,saldo)}" class="saldo_default">
										<input type="text" value="${formatCurrency(x.id_moneda_venta,saldo)}" data-mask="false" disabled style="text-align:center;" class="saldo numeric">`;
			 				return input;
			 			
                    }
                 },
                 {
                     data:function(x){
                        return `<input type="text" disabled data-mask="false" style="text-align:center;" class="pago_saldo numeric" value="0">`;
                     }
                 },
                 {
                    data: function (x) { return `<span class="currency_code">${x.moneda}</span>`; }
                 },
                 {
                     data: function(x){
                        var input = `<input type="text" disabled style="text-align:center;" class="input-style numeric" value="${x.importe}">`;
			 				return input;
                     }
                 },
                 {
                    data:'estado_cobro_n'
                 },
                 {
                     data: function(x){
                        if (Boolean(x.id_proforma)) {
			 					return `<a href="{{route('detallesProforma',['id'=>''])}}/${x.id_proforma}"class="bgRed">
						        	<i class="fa fa-fw fa-search"></i>${x.id_proforma}</a>
						        	<div style="display:none;">${x.id_proforma}</div>`;
			 				} else {
			 					return '';
			 				}
                     }
                 },
                 {
                     data: 'fecha_documento'
                 },
                 {
                     data: 'vendedor'
                 },
                 {
                     data:'pendiente_txt'
				 },
				 {
					data: 'origen_libro'
				 }
			 		

			 	],

			 	"select": {
			 		style: 'multi',
			 		selector: 'td:first-child'
			 	},
			 	"columnDefs": [
                     {
			 			orderable: false,
			 			className: 'select-checkbox style_check',
			 			targets: 0
                     },
                     {
			 			targets: 5,
			 			className: 'col_input_saldo'
			 		},
			 		{
			 			targets: 6,
			 			className: 'col_input_pago_proveedor'
			 		}

			 	],
			 	// "aaSorting":[[2,"desc"]],
			 	"createdRow": function (row, data, iDataIndex) {
                    if (data.pendiente == true) {
			 			$(row).css('background-color', '#EFF3BA');
			 		}

			 		$(row).attr('id', iDataIndex + '_fila');
			 		//ASIGNAR ID Y FUNCION
			 		$(row).find('td.col_input_pago_proveedor input').attr('id', iDataIndex + '_input');
			 		$(row).find('td.col_input_pago_proveedor').attr('onkeyup', 'validateKeyPressSaldo("' + iDataIndex + '","' + iDataIndex + '_input")');
			 		if ($(row).find('td .activeCheck').val() == 'true') {
			 			$($(row).find('td.select-checkbox').get(0)).removeClass('select-checkbox')
			 		}
			 		$(row).find('td:first-child').attr('id', iDataIndex + '_select');


			 	},
			 	//AL FINALIZAR LA CARGA AGREGAR CONTROL DE COMAS Y PUNTO
			 	"initComplete": function (settings, json) {
			 		$.unblockUI();
			 	}
			 });
	
				//DESTRUIR EVENTOS ANTERIORES
				table.off('select');
				table.off('deselect');
				table.off('user-select');
				table.off('draw');


					table
						.on( 'select', function ( e, dt, type, indexes ) {
							let saldo_default = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo_default').val();
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').val(saldo_default);
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo').val(0);
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').prop('disabled',false);
							//console.log('EVENT SELECT');
						} )
						.on( 'deselect', function ( e, dt, type, indexes ) {
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').prop('disabled',true);
							let saldo_default = $('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo_default').val()
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_pago_proveedor input').val(0);
							$('#listado').find('tbody tr#'+indexes+'_fila td.col_input_saldo input.saldo').val(saldo_default);
							$('#listado').find('tbody tr#'+indexes+'_fila').css('background-color','');
							
						} )
						.on( 'user-select', function ( e, dt, type, cell, originalEvent ) {
							if($(originalEvent.currentTarget).find('input.activeCheck').val() == 'true'){
								e.preventDefault();
								}
						
						}).on('draw', function () {
								/* AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT*/
								$('#listado tbody tr .numeric[data-mask = false]').inputmask("numeric", {
									radixPoint: ",",
									groupSeparator: ".",
									digits: 2,
									autoGroup: true,
									rightAlign: false
								});
								$('#listado tbody tr .numeric[data-mask = false]').attr('data-mask','true');

						});






		}//function		

		 /* ================================================================================================== 
                                            LOGICA DE ANTICIPO
             ================================================================================================== */
         
		// $('#tipo_operacion').val('1').trigger('change.select2');
		// $('#tipo_operacion').change(()=>{
		// 	getAnticipo();
		// });

		// $('#idCliente').change(()=>{

		// 	if($('#tipo_operacion').val() == 2){
		// 		getAnticipo();
		// 	}
		// });

		function getAnticipo()
		{
			if($('#tipo_operacion').val() == 1){//SI ES COBRANZA

				$('#anticipos').empty();
				newOption = new Option('Seleccione un anticipo','', false, false);
				$('#anticipos').append(newOption);
				$('#anticipos').prop('disabled',true);

			} else if($('#tipo_operacion').val() == 2){//SI ES ANTICIPO

				let cliente = $('#idCliente').val();
				$('#anticipos').empty();
				
				if(cliente == ''){

					$.toast({	
				    heading: '<b>Atención</b>',
				    position: 'top-right', 
				    text: '<b>Debe seleccionar un cliente para realizar la búsqueda con la operación Anticipo.</b>',
				    width: '400px',
				    hideAfter: false,
				    icon: 'info'
						});	
					$('#anticipos').prop('disabled',true);

				} else {
					loadAnticipo();
					$('#anticipos').prop('disabled',false);
				}
			}

		}

		function loadAnticipo()
		{
			if($('#idCliente').val()){
				
				let getAnticipoList = {
				id_cliente : $('#idCliente').val()
			}
					//CLEAR MSJ REPEAT
					if(msj_anticipo.reset){
						msj_anticipo.reset();	
					}


					$.ajax({
						type: "GET",
						url: "{{route('getAnticipoList')}}",
						async:false,
						data: getAnticipoList,
						dataType: 'json',
						error: function(jqXHR,textStatus,errorThrown){

							msj_anticipo = $.toast({
									heading: '<b>Error</b>',
									text: '<b>Ocurrió un error al intentar obtener los anticipos</b>',
									position: 'top-right',
									hideAfter: false,
									icon: 'error'
								});
							
						},
						success: function(rsp){
				
							if(rsp.data.length > 0){
								$('#anticipos').empty();

								$.each(rsp.data, function (key, item) {

									newOption = new Option(item.currency.currency_code +' '+item.importe, item.id, false, false);
									$('#anticipos').append(newOption); //atributo valor

								});
								$('#anticipos').prop('disabled',false);

							} else {
								$('#anticipos').prop('disabled',true);

								msj_anticipo = $.toast({
									heading: '<b>Atención</b>',
									text: '<b>No tiene anticipos!</b>',
									position: 'top-right',
									hideAfter: false,
									icon: 'info'
								});
								
							}

						}//success
				});

			}//if
			
		


		}

        
        /* ================================================================================================== 
                                            CONFIG INTERACCION FORMULARIO
             ================================================================================================== */
			
		//INICIAR BUSQUEDA CON ENTER EN EL FORM
		// function enterSearchEventDatatable(){
		// 	$( "#idCliente" ).focus(() =>{});
		// 	$("#consultaFactura").keypress((e) => {
		// 		   if(e.which == 13) { loadDatatable(); } 
		// 	});
		// }
		
        //VALIDAR SALDO CARGADO
		function validateKeyPressSaldo(id,evento){
			let data = $('#listado').find('tbody tr');
			let monto = 0;
			let saldo = 0; 
			let saldo_diferencia = 0;
			let total = 0;
			/*CALCULO DE DIFERENCIA*/
					/*NO LIMPIAR DATO DE BD */
			saldo = parseFloat(clean_num($('#listado').find('tbody tr#'+id+'_fila td.col_input_saldo .saldo_default').val()));
			monto = parseFloat(clean_num($('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').val()));
			saldo_diferencia = saldo - monto;
			console.log('---------------------------------------------------------');
			console.log(saldo);
			console.log(monto);
			console.log(saldo_diferencia);
			console.log('---------------------------------------------------------');
			if(saldo_diferencia < 0 | monto < 0){
				$('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').css('border-color','red');
			} else {
				$('#listado').find('tbody tr#'+id+'_fila td.col_input_pago_proveedor input').css('border-color','');	
			}
			$('#listado').find('tbody tr#'+id+'_fila td.col_input_saldo .saldo').val(formatter.format(parseFloat(saldo_diferencia)));
		}






		/* ==================================================================================================  
											OBTENER Y VALIDAR ITEMS SELECCIONADOS
			 ================================================================================================== */


			$('#btnProcesar').click(()=>{
				processSelection();
			});
			 
		function processSelection(){
			operaciones.clear();

			blockUI();
			/*VALIDAR ITEMS */
			if(getItems()){

					// if(validateValues(getItems())){
                        /*REDIRECCION Y PROCESO */
                        //console.log('ingreso a order');
                        // processOrder();
					
						Promise.all([validateValues(getItems())]).then(() => {
							processOrder();

				}).catch((e) => {
						BlockOp();
						$.unblockUI();
						//console.log('FINALIZA PROCESS ORDER CON ERROR');
						console.error(e);
                });

			 

			// } else { 
            //             //console.log('no order');
            //             $.unblockUI(); }



			 } else {
			 	$.unblockUI();
			 	msjToast('Seleccione algún ítem para continuar',1);
			 }
		}//	 

		function getItems()
		{

			try {
				//ITEMS > 0
				if(table.rows( '.selected' ).nodes().length > 0){
					// RETURN OBJECT JQUERY
					return table.rows( '.selected' ).nodes().to$();
				}

			} catch(error) {
				console.error(error);
				return false; 
			}	
		}//

function validateValues(items) {
	//console.log('se validan items seleccionados');
	return new Promise((resolve, reject) => { 

	let id_moneda,
	 	id_cliente,
	 	saldo_default,
	 	saldo,
	 	pago_saldo,
		id_libro_compra = null, 
		id_libro_venta = null,
	 	id_anticipo = null,
		id_retencion = null,
	 	data = [],
	 	id_tipo_documento,
		iva10 = 0,
		gravada10 = 0,
		origen_libro = 0,
		flag_1 = 0, 
		flag_2 = 0, 
		flag_3 = 0;

		

	/*RECORRER LOS ITEMS OBTENIDOS*/
	$.each(items, function (index, val) {
		//console.log($(val).find('.saldo_default').val());
		//RECORRER Y RECUPERAR INPUTS
		id_tipo_documento 	= Number($(val).find('.id_tipo_documento').val());
		id_moneda 			= Number($(val).find('.id_moneda').val());
		id_cliente 		    = Number($(val).find('.id_cliente').val());
		id_documento 		= Number($(val).find('.id_documento').val());
		saldo 				= clean_num($(val).find('.saldo').val());
		pago_saldo 			= clean_num($(val).find('.pago_saldo').val());
		saldo_default 		= clean_num($(val).find('.saldo_default').val(),true);
		origen_libro 		= $(val).find('.origen_libro').val();


		
		console.log(origen_libro,id_moneda);
		// debugger;

		/*REGLAS DE VALIDACION DE ITEMS */
		if (index === 0) {
			old_cliente = id_cliente;
			old_moneda = id_moneda;
		}

		console.log('nro',$(val).find('.nro_documento').val());
		//NUM FACTURA PARA CONCEPTO
		if(saldo_default == pago_saldo){
			operaciones.num_canc.push($(val).find('.nro_documento').val());
		} else {
			operaciones.num_a_cta.push($(val).find('.nro_documento').val());
		}
		//console.log('saldo_default',saldo_default,'pago_saldo',pago_saldo);


		if (origen_libro === 'LV') {id_libro_venta = id_documento;}/*libro_venta*/
		else if (origen_libro === 'LC') {id_libro_compra = id_documento;}/*libro_compra*/
        else if (origen_libro === 'ANT') {id_anticipo = id_documento;} /*libro_anticipo*/
		else if (origen_libro === 'RC') {id_retencion = id_documento;} /*libro_anticipo*/
        else {flag_3++;}//DOCUMENTO NO EXISTE

		if (old_cliente != id_cliente || old_moneda != id_moneda) {
			$('.selected#' + val.id).css('background-color', '#F89494');
		} else {
            $('.selected#' + val.id).css('background-color', '#B0BED9');
		}
		

			//ALMACENAR ITEMS SELECCIONADOS
			data.push({
					id_venta: id_libro_venta,
					id_compra : id_libro_compra,
					id_anticipo: id_anticipo,
					id_retencion : id_retencion,
					monto_pago: pago_saldo
			 });

	

		//FORMATEAR VALORES
		id_tipo_documento = null;
		id_documento = null;
		saldo = 0;	
		pago_saldo = 0; 	
		saldo_default = 0;
		id_libro_venta = null;
		id_anticipo = null;
		id_libro_compra = null;
		id_retencion = null;

		//console.log('recorre forach');
    });//each

	//console.log('salio del foreach');
		operaciones.data = data;
		operaciones.cabecera = {
                	id_cliente: id_cliente,
                	cliente_n: $('.selected').find('.cliente_nombre').html(),
                	id_moneda: id_moneda,
                	currency_code: $('.selected').find('.currency_code').html()
				}
				
		

		Promise.all([setCalcValues()]).then(() => {
				//console.log('Calculos recibidos con exito');
				generarConcepto();
				resolve({resp:true});
			}).catch((e) => {
				//console.log('Error al enviar calculos');
				console.error(e);
				reject(false);
			});

	})
} //




	function setCalcValues(){
		return new Promise((resolve, reject) => { 

		let dataString = {
            data : operaciones.data,
			id_tipo_recibo : $('#tipo_operacion').val()
		}
		
  
		$.ajax({
          type: "GET",
          url: "{{route('setCalcRecibo')}}",
          async:false,
          data: dataString,
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){

           		msjToast('',2,jqXHR.status,textStatus);
                  $('#btnProcesarOP').prop('disabled',false);
                  reject(false);
		  },
          success: function(rsp){
            if(rsp.err){
                //console.log('TRUE');
                operaciones.total_pago = rsp.total_pago;
                operaciones.total_nc = rsp.total_nc;
                operaciones.total_anticipo = rsp.total_anticipo;
                operaciones.total_pago_neto = rsp.total_pago_neto;
                operaciones.iva10 = rsp.iva10;
				operaciones.gravada10 = rsp.gravada10;
				// operaciones.concepto = rsp.concepto;
				operaciones.nro_recibo = rsp.nro_recibo;
				operaciones.total_canje = rsp.total_canje;
				$('#cf_numero_recibo').val(rsp.nro_recibo);
                resolve({resp:true});
        
            } else {
				//console.log(rsp.msj);
				$.toast({	
				    heading: '<b>Atención</b>',
				    position: 'top-right', 
				    text: rsp.msj,
				    width: '400px',
				    hideAfter: false,
				    icon: 'info'
				});
				//console.log('ERROR RECIBO AJAX');
				reject(false);
            }

            }//success
		});
		
		})//promise
	}






	function processOrder(){
        //console.log('ingreso PROCEESS ORDER');
		$.toast().reset('all');

		let cotizacion = 0;
		let cotizado = 0;
		let id_moneda = operaciones.cabecera.id_moneda;
		$('#tabConfirm').tab('show');
		$('#tabHome').prop('disabled',true);

		$('#cf_currency_code').val(operaciones.cabecera.currency_code);
		$('.cf_currency_code').html(operaciones.cabecera.currency_code);
		$('#cf_total_pago').val(operaciones.total_pago);
		$('#cf_anticipo').val(operaciones.total_anticipo);	
		$('#cf_nota_credito').val(operaciones.total_nc);
		$('#cf_neto_pago').val(operaciones.total_pago_neto);
		$('#cf_beneficiario').val(operaciones.cabecera.cliente_n);
		$('#iva_10').val(operaciones.iva10);
		$('#gravada_10').val(operaciones.gravada10);
		$('#cf_concepto').val('');
		$('#cf_concepto').val(operaciones.concepto);
		//$('#cf_numero_recibo').val(operaciones.nro_recibo);
		$('#cf_canje').val(operaciones.total_canje);

		//SI ES TIPO ANTICIPO Y TOTAL CERO APLICAR 
		if($('#tipo_operacion').val() == 2 && operaciones.total_pago_neto == 0){
			$('#btnProcesarOP').html('Aplicar Anticipo');
		} else {
			$('#btnProcesarOP').html('Crear Recibo');
		}


	

		/*VALIDAR QUE LOS ELEMENTOS AJAX SE FINALIZEN PARA EMITIR MENSAJE */
		Promise.all([getRetencion()]).then(() => {
			//console.log('FINALIZA RETENCION CON EXITO');

					Promise.all([getCotizacion(),setCotizar()]).then(() => {
						//console.log('FINALIZA COTIZACIONES CON EXITO');
						UnblockOp();	
					}).catch((e) => {
						BlockOp();
						//console.log('FINALIZA COTIZACIONES CON ERROR',e);
					});
				
			}).catch(() => {
				//console.log('ERROR EN RETENCION');
				BlockOp();
			}).finally(() => {
				//console.log('FIN DE PROCESO');
				$.unblockUI();
			});
 		
		
		
	}


	/* ============================================================================================ 
											RETENCION
	============================================================================================*/	

	function getRetencion(){
		return new Promise((resolve, reject) => { 


		$.ajax({
		  async:false,	
          type: "GET",
          url: "{{route('getRetencionRecibo')}}",
		  data:{ id_proveedor: operaciones.cabecera.id_proveedor,
				 total_gravada: operaciones.gravada10,
				 id_moneda: operaciones.cabecera.id_moneda,
				 total_iva: operaciones.iva10,
				 total_factura: operaciones.total_pago
				},
          dataType: 'json',
          error: function(jqXHR,textStatus,errorThrown){
          	 msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo obtener la retención.',2,jqXHR,textStatus);
          	 reject(false);
          },
          success: function(rsp){
					if(rsp.err === true){
						if(rsp.retencion === true){
						$('#ret_iva_10').val(rsp.monto);
						$('#cf_retencion').val(rsp.monto);
						operaciones.retencion = Number(rsp.monto);
						operaciones.total_pago_neto = operaciones.total_pago_neto - Number(rsp.monto);
						$('#cf_neto_pago').val(operaciones.total_pago_neto);
						//console.log('MONTO RETENCION',rsp.monto);
						} else {
						$('#ret_iva_10').val(0);
						$('#cf_retencion').val(0);
						}

						resolve({resp:true});
						

					} else {
						msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo obtener la retención.',2);
						$('#ret_iva_10').val(0);
						//console.log('OCURRIO UN ERROR EN LA CONSULTA');
						reject(false);
					}


                          }//success
                 });

			});//promise


		}//


				

/* ============================================================================================ 
						PROCESAR Y ENVIAR DATOS PARA CONFIRMAR PAGO
	============================================================================================*/	

	function validar_envio(){
		let err = true;
		let msj='';

		$.toast().reset('all');

		if($('#cf_cotizacion').val().trim() == '0' | $('#cf_cotizacion').val().trim() == ''){
			$('#cf_cotizacion').css('border-color','red');
			err = false;
			msjToast('La cotización no puede ser cero');
		} else {
			$('#cf_cotizacion').css('border-color','');
		}

		// if($('#cf_entrega').val().trim() == ''){
		// 	 err = false;
		// 	$('#cf_entrega').css('border-color','red');
		// 	msjToast('Complete el campo Entregado a.');
		// } else {
		// 	$('#cf_entrega').css('border-color','');
		// }

		// if($('#cf_concepto').val().trim() == ''){
		// 	err = false;
		// 	msjToast('Complete el campo Concepto');
		// 	$('#cf_concepto').css('border-color','red');
		// } else {
		// 	$('#cf_concepto').css('border-color','');
		// }

		if(!err){
			msjToast('',1);
		}

		return err;
	}

	$('#btnProcesarOP').click(()=>{

		//SI ES TIPO ANTICIPO Y TOTAL CERO APLICAR 
		if($('#tipo_operacion').val() == 2 && operaciones.total_pago_neto == 0){
			modalConfirmarOperacion("¿Está seguro que desea aplicar el recibo?", "Aplicar Recibo");
		} else {
			modalConfirmarOperacion("¿Está seguro que desea crear recibo?", "Crear Recibo");
		}

		
		
	});


	function modalConfirmarOperacion(txt_sub, txt_title)
	{

		
		swal({
							title: txt_title,
							text: txt_sub,
							icon: "warning",
							showCancelButton: true,
							buttons: {
								cancel: {
									text: "No, Cancelar",
									value: null,
									visible: true,
									className: "btn-warning",
									closeModal: false,
								},
								confirm: {
									text: "Sí, Continuar",
									value: true,
									visible: true,
									className: "",
									closeModal: false
								}
							}
						}).then(isConfirm => {
							
							if (isConfirm) {
								$.when(sendData()).then((a)=>{ 
									swal("Éxito",a , "success");
								},(b)=>{
									swal("Cancelado", b, "error");
								});
								
							} else {
								swal("Cancelado", "La operación fue cancelada", "error");
							}
						});
	}

	//aqui
	function sendData()
	{
		return new Promise((resolve, reject) => { 

			$('#btnProcesarOP').prop('disabled',true);

		let info = {
			/*FP */
			// sucursal:$('#cf_sucursal').val(),
			centro_costo:$('#cf_centro_costo').val(),
			cotizacion : clean_num($('#cf_cotizacion').val()),
			id_gestor: $('#cf_gestor').val(),
			fecha: moment($('#fecha_emision').val().trim(),'DD/MM/YYYY').format('YYYY-MM-DD'),
			concepto : $('#cf_concepto').val(),
			//nro_recibo: 0,
			nro_recibo: $('#cf_numero_recibo').val(),
			data : operaciones.data,
			id_tipo_recibo : $('#tipo_operacion').val()

		}
		

		// if(validar_envio()){
		
		$.ajax({
          type: "GET",
          url: "{{route('setCrearRecibo')}}",
          data: info,
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){

				reject('Ocurrió un error en la comunicación con el servidor');
           		// msjToast('',2,jqXHR.status,textStatus);
		      	$('#btnProcesarOP').prop('disabled',false);
		  },
          success: function(rsp){

				//console.log(rsp);
			  if(rsp.err){
				resolve('El recibo fue creado con éxito');
				cancelarOp();

			  } else{
					reject(rsp.msj);
			  }
          	
                    }//success
                 }).done(function(){
                 	$('#btnProcesarOP').prop('disabled',false);
                 });//AJAX

	

		});
	}


	function cancelarOp()
	{

		operaciones.clear();
		$('#listadoPagos tbody').html('');
		$('#formTotales')[0].reset();
		{{--PESTAÑA --}}
		// loadDatatable();
		$('#tabHome').tab('show');

		//LIMPIAR DATATABLE.
		table
		.clear()
		.draw();

		$('#tabConfirm').prop('disabled',true);
		$('#btnProcesarOP').prop('disabled',true);

		$('.clear_input').val('0');
		$('.clear_input_txt').val('');
		$('#fp_id_moneda').prop('disabled',false);
		$('#btnAgregarFila').prop('disabled',false);
		


	}


	const op = {
		id:0
	}

	function openNewOp(){
		//console.log('abrio modal');
		if(op.id != 0){
			// $('#num_op').html(op.id);
			$('#modalNumOp').modal('show');
		}
	}

	$('#modalNumOp').on('hidden.bs.modal', function (e) {
		cancelarOp();
		op.id = 0;
		$('#num_op').html('');
		})
	



	/* ==============================================
			CONFIG Y OTROS
		==============================================*/

 function initConfig(){
 	$('#tabConfirm').prop('disabled',true);
 	$('#btnProcesarOP').prop('disabled',true);
 	//  blockUI();
 }


 function blockUI()
 {

 	     $.blockUI({
				  centerY: 0,
				  message: "<h2>Procesando...</h2>",
				  css: {
				    color: '#000'
				   }
				 });

 }


 		{{--Almacena mensajes para mostrar --}}
		function msjToast(msj,opt = false, jqXHR = false, textStatus = '')
		{
	

			if( msj != ''){ 
				operaciones.msj.push('<b>'+msj+'</b>');
			} 

			if(opt === 1){
				$.toast().reset('all');

				$.toast({	
				    heading: '<b>Atención</b>',
				    position: 'top-right', 
				    text: operaciones.msj,
				    width: '400px',
				    hideAfter: false,
				    icon: 'info'
				});
				operaciones.msj = [];

			} else if(opt === 2){
				//console.log('Ingreso opt == 1');
				$.toast().reset('all');

				

				if(jqXHR === false && textStatus === ''){
						msj = operaciones.msj;
				 } else{

						 errCode = jqXHR.status;
						 errMsj = jqXHR.responseText;

				 	msj ='<b>';
				 	if(errCode === 0 )
				 		msj += 'Error de conectividad a internet, verifica tu conexión.';
				 	else if(errCode === 404)
				 		msj += 'Error al realizar la solicitud.';
				 	else if(errCode === 500){
				 		if(operaciones.msj.length > 0){
				 		msj += operaciones.msj;
				 		} else { msj += 'Ocurrió un error en la comunicación con el servidor.'}

				 	}else if(errCode === 406){
				 		msj += errMsj;
				 	}
				 	else if(errCode === 422)
				 		 msj += 'Complete todos los datos requeridos'
				 	else if(textStatus === 'timeout')
				 		msj += 'El servidor tarda mucho en responder.';
				 	else if(textStatus === 'abort')
				 		msj += 'El servidor tarda mucho en responder.';
				 	else if (textStatus === 'parsererror'){
				 		msj += 'Error al realizar la solicitud.';
				 		//console.log('ParseError');
				 	}
				 	else 
				 		msj += 'Error desconocido, póngase en contacto con el área técnica.';
				 	msj += '</b>';
				 }


				 	$.toast({
						    heading: '<b>Error</b>',
						    text: msj,
						    position: 'top-right', 
						     hideAfter: false,
						     width: '400px',
						    icon: 'error'
						});

				operaciones.msj = [];
			}
		}//


			$('.control_space').keypress(function(event) {
			
			var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
		});
	        
	     const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD'
					});


		/*SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
			NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS */
		function clean_num(n,bd=false){

				  	if(n && bd == false){ 
					n = n.replace(/[,.]/g,function (m) {  
							 				 if(m === '.'){
							 				 	return '';
							 				 } 
							 				  if(m === ','){
							 				 	return '.';
							 				 } 
							 			});
					return Number(n);
				}
				if(bd){
					return Number(n);
				}
				return 0;

				}//




	/* ==============================================
			COTIZACIONES
		==============================================*/

	function getCotizacion()
	{
		return new Promise((resolve, reject) => { 
			let  cotizacion;


				{{--OBTENER COTIZACION --}}
			$.ajax({
			async:false,	
			type: "GET",
			url: "{{route('getCotizacionFP')}}",
			data:{id_moneda_costo:operaciones.cabecera.id_moneda,
					id_moneda_venta: $('#fp_id_moneda').val()},
			dataType: 'json',
			error: function(jqXHR,textStatus,errorThrown){
				msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo obtener la cotización.',2,jqXHR,textStatus);
				reject(false);
			},
			success: function(rsp){

							if(rsp.info.length > 0){
								let cot = Number(rsp.info[0].cotizacion);
								

								if( cot > 0){
									$('#cf_cotizacion').val(rsp.info[0].cotizacion);
									cotizacion = rsp.info[0].cotizacion;
									resolve({resp:true});

								} else {
									msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.',1);
									reject(false);
								}
								} else {
									msjToast('Cotización desactualizada no se puede realizar operaciones. Cargue la cotización por favor.',1);
									reject(false);
								}

							}//success
					});

				});//promise

		}//


	function setCotizar(opt=0)
	{
			return new Promise((resolve, reject) => { 

			if((operaciones.cabecera.id_moneda != $('#fp_id_moneda').val()) | opt == 1){
				let data = {};
				data = {
					cabecera:{ id_moneda_costo:operaciones.cabecera.id_moneda,
							id_moneda_venta: $('#fp_id_moneda').val()
							},
					total_factura: operaciones.total_pago,
					total_nc: operaciones.total_nc,
					total_anticipo: operaciones.total_anticipo,
					total_pago_neto: operaciones.total_pago_neto,
					total_canje:operaciones.total_canje   	     
				}

			
					$.ajax({
				type: "GET",
				url: "{{route('cotizarMontosRecibo')}}",
				data:data,
				dataType: 'json',
				error: function(jqXHR,textStatus,errorThrown){

						msjToast('Ocurrió un error en la comunicación con el servidor y no se pudo cotizar los montos.',2,jqXHR,textStatus);
						reject(false);
				},
				success: function(rsp){
								if(rsp.info.length > 0){
									$('#cotizado_total_factura').val(formatter.format(parseFloat(rsp.info[0].total_factura)));
									$('#cotizado_anticipo').val(formatter.format(parseFloat(rsp.info[0].total_anticipo)));
									$('#cotizado_nc').val(formatter.format(parseFloat(rsp.info[0].total_nc)));
									$('#cotizado_pago_neto').val(formatter.format(parseFloat(rsp.info[0].total_pago_neto)));
									$('#cf_neto_pago_gs').val(formatter.format(parseFloat(rsp.info[0].total_guaranies)));
									$('#cf_anticipo').val(formatter.format(parseFloat(rsp.info[0].total_anticipo)));
									$('#cotizado_canje').val(formatter.format(parseFloat(rsp.info[0].total_canje)));
									//   getRetencion();

									resolve({resp:true});

								} else {
									msjToast('Ocurrió un error al intentar cotizar los valores.',1);
									reject(false);

								}

								}//success
						})//ajax
			


				} else {
			
					$('#cotizado_total_factura').val(operaciones.total_pago);
					$('#cotizado_anticipo').val(operaciones.total_anticipo);
					$('#cotizado_nc').val(operaciones.total_nc);
					$('#cotizado_pago_neto').val(operaciones.total_pago_neto);


					resolve({resp:true});
					
				}	

			});//promise
	}




$('#fp_id_moneda').change(function () 
{

	Promise.all([getCotizacion(), setCotizar()]).then(() => {
		UnblockOp();
	}).catch(() => {
		BlockOp();
	}).finally(() => {
		/*FIN PROCESO DE VALIDACION --}}	*/
		$.unblockUI();
	});


});

function BlockOp() {
	$('#btnAgregarFila').prop('disabled', true);
	$('#btnProcesarOP').prop('disabled', true);
}

function UnblockOp() {
	$('#btnAgregarFila').prop('disabled', false);
	$('#btnProcesarOP').prop('disabled', false);
}


function controlNumFormat() {
	$('.formatInput').inputmask("numeric", {
		radixPoint: ",",
		groupSeparator: ".",
		digits: 2,
		autoGroup: true,
		// prefix: '$', //No Space, this will truncate the first character
		rightAlign: false
	});
}



function generarConcepto() 
{

	let num_fact_canc = operaciones.num_canc,
		num_fact_a_cta = operaciones.num_a_cta,
		concepto = 'FCR';

		if($('#tipo_operacion').val() == 2){
			concepto = 'ANT ';
		}
	
	if (num_fact_canc.length) {

		$.each(num_fact_canc, (index, value) => {
			concepto += ','+value;
		});
	}

	if (num_fact_a_cta.length) {

		$.each(num_fact_a_cta, (index, value) => {
			concepto += ','+value;
		});

	}

		concepto += ' ' + operaciones.cabecera.cliente_n;
		operaciones.concepto = concepto;


}


			//DEFINIR FORMATO MONEDAS		
			const EURO = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const DOLAR = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });



	
				function formatCurrency(id_moneda, value){
					
					let importe = 0;
	

					console.log('formatCurrency', id_moneda, value);

							//PYG
							if(id_moneda == 111){
								importe = GUARANI(Number(value),{ formatWithSymbol: false }).format(true);
								console.log('PYG',importe);
							} 
							//USD
							if(id_moneda == 143) {
								importe = DOLAR(Number(value),{ formatWithSymbol: false }).format(true);
								console.log('USD',importe);
							}
							//USD
							if(id_moneda == 43) {
								importe = EURO(Number(value),{ formatWithSymbol: false }).format(true);
								console.log('EUR',importe);
							}


					return importe;		
				}


	$(document).ready(function() {
		$('#idCliente').select2({
								//maximumSelectionLength: 2,
								placeholder: 'Cliente'
								});

		ordenarSelect('idCliente');						

		$("#idCliente").select2({
					ajax: {
							url: "{{route('clientesCobro')}}",
							dataType: 'json',
							placeholder: "TODOS",
							delay: 0,
							data: function (params) {
										return {
											q: params.term, // search term
											page: params.page
												};
							},
							processResults: function (data, params){
										var results = $.map(data, function (value, key) {
										console.log(value);
										documento = value.documento_identidad;
										if(jQuery.isEmptyObject(value.dv ) == false){
											documento = documento +'-'+value.dv
										}
										if(value.activo == true){
											activo = 'ACTIVO';
										}else{
											activo = 'INACTIVO';
										}	
											/* return {
												children: $.map(value, function (v) {*/ 
													return {

																id: value.id,
																text: documento +' - '+ value.pasajero_data + ' _ '+value.id+' ('+ activo +')'
															};
												/*  })
												};*/
										});
										return {
													results: results,
												};
							},
							cache: true
							},
							escapeMarkup: function (markup) {
											return markup;
							}, // let our custom formatter work
							minimumInputLength: 3,
		});
	})
	
	function ordenarSelect(id_componente)
	    {
	      var selectToSort = jQuery('#' + id_componente);
	      var optionActual = selectToSort.val();
	      selectToSort.html(selectToSort.children('option').sort(function (a, b) {
	        return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
	      })).val(optionActual);
	    }


</script>

@endsection