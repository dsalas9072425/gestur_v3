@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
	.negrita{
		font-weight: bold;
	}
	.seletc2-sm {
		height: 30px !important;
	}


</style>	

	@parent
@endsection
@section('content')


<section id="base-style">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Listado Retención</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
			<form class="box-body" id="formRetencion">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Cliente</label>
							<select class="form-control select2"  name="cliente_id"  id="cliente_id" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($clientes as $cliente)
										@php
											$ruc = $cliente->documento_identidad;
											if($cliente->dv){
												$ruc = $ruc."-".$cliente->dv;
											}
										@endphp
										<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->nombre}} {{$cliente->apellido}} - {{$cliente->id}}</option>
									@endforeach
							</select>
						 </div>
					</div>
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label> Nro. Factura </label>
							<input type="text" class="form-control" name="factura" id="factura" />

						 </div>
					</div>
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Numero Retención </label>
							<input type="text" class="form-control" name="num_retencion" maxlength="30" value="" id="fp_documento_forma_pago" placeholder="Numero Retención">
						</div>
					</div>
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Tipo Retención</label>
							<select class="form-control select2" name="id_tipo_retencion" id="tipo_retencion" style="width: 100%;">		
								<option value="">Seleccione Tipo Retención</option>
								<option value="E">Empresa</option>
								<option value="C">Cliente</option>
							</select>
						 </div>
					</div>
					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Estado</label>
							<select class="form-control select2 adaptForm" name="estado"  id="estado" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($estados as $estado)
									<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
									@endforeach
							</select>
						 </div>
					</div>
					<div class="col-12 col-sm-6 col-md-2">
						<br>
						<button tabindex="10" class="pull-right btn btn-info btn-lg mr-1" id="btnBuscar" type="button"><b>Buscar</b></button>
					</div>	

				</div>

			</form>  
			<div class="table-responsive">
				<table id="listado" class="table table-hover table-condensed nowrap" style="width: 100%;">
					<thead>
						<tr class="text-center">
	                        <th>Fecha Retencion</th> 
							<th>Numero</th> 
							<th>Factura</th> 
							<th>Pendiente</th> 
	                        <th>Moneda</th> 
	                        <th>LV</th> 
	                        <th>LC</th> 
							<th>Tipo</th> 
							<th>Estado</th> 
	                        <th>Usuario</th> 
	                        <th>Fecha Insert</th> 
	                        <th>Cotizacion</th> 
							<th>Importe</th> 
							<th>Saldo</th> 
	                        <th style="padding-left: 10px;padding-right: 10px;"></th>
			             </tr>
					</thead>
					<tbody style="text-align: center">
					</tbody>
				</table>
			</div>

  			</div>
			</div>
		</div>
	</section>
	
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script src="https://unpkg.com/currency.js@~1.2.0/dist/currency.min.js"></script>

	<script type="text/javascript">
		var tableListado;

		$(document).ready(function() {
			tableRetencion();
			calendar();
			$('.select2').select2();
			$('#btnBuscar').click(()=>{
				tableRetencion();
			});

		});

		$('#id_cliente').select2();

		function  tableRetencion(responsive = false){
			let resp;
			// let form = $('#formRecibo').serializeJSON({
			// 	customTypes: customTypesSerializeJSON
            // });
            let form = $('#formRetencion').serializeJSON();

			tableListado = $("#listado").DataTable({
				destroy: true,
				ajax: {
			 		url: "{{route('ajaxListadoRetencion')}}",
			 		data: form,
			 		error: function (jqXHR, textStatus, errorThrown) {
			 			console.log('ERrror');
			 		}

			 	},
				"columns": [
					{data: 'fecha_retencion'},
					{data: 'numero'},
					{data: (x)=>{ 
						if(x.documento_compra == ''){
							resuelto = x.documento_venta;
						}else{
							resuelto = x.documento_compra;
						}
						return resuelto;
					}},						
					{data: (x)=>{ 
						if(x.pendiente == true){
							resuelto = 'SI';
						}else{
							resuelto = 'NO';
						}
						return resuelto;
					}},	
	                {data: 'currency_code'},
	                {data: 'id_libro_venta'},
	                {data: 'id_libro_compra'}, 
	                {data: (x)=>{ 
						if(x.tipo == 'E'){
							resuelto = 'Empresa';
						}else{
							resuelto = 'Cliente';
						}
						return resuelto;
					}},	
					{data: 'estados'}, 
	                {data: 'usuario'},
	                {data: 'fecha_hora'},
	                {data: (x)=>{ return formatCurrency(x.id_moneda,Number(x.cotizacion)); }},
	                {data: (x)=>{ return formatCurrency(x.id_moneda,Number(x.importe)); }},
					{data: (x)=>{ return formatCurrency(x.id_moneda,Number(x.saldo)); }},
					{data: (x)=>{ 
						btn = "";
						if(x.tipo == 'C'){
							btn = `<a href="{{route('modificarRetenciones',['id'=>''])}}/${x.id}"  class="bgRed">
	                    						<i class="fa fa-fw fa-search"></i></a><div style="display:none;">${x.id}</div>`;
	                    }						
						return btn; 
					}},
			 	]
			});

		}


		function calendar(){
			$('.single-picker').datepicker({ 
						format: "dd/mm/yyyy", 
						language: "es",
						clearBtn : true
						});
		 }//


		 /**
		 FORMATEO DE MONEDAS
		*/

			//DEFINIR FORMATO MONEDAS		
			const EURO = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const DOLAR = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });


			function formatCurrency(id_moneda, value){
					
					let importe = 0;
	

					console.log('formatCurrency', id_moneda, value);

							//PYG
							if(id_moneda == 111){
								importe = GUARANI(value,{ formatWithSymbol: false }).format(true);
							} 
							//USD
							if(id_moneda == 143) {
								importe = DOLAR(value,{ formatWithSymbol: false }).format(true);
							}

					return importe;		
				}

		 
	

		
			
		
	</script>
@endsection