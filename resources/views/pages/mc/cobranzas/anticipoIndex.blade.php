@extends('masters')
@section('title', 'Panel de Control')
@section('styles')

<style type="text/css">
  			

    input.form-control:focus ,.select2-container--focus, button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }


</style>

@parent

@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Anticipo Cliente</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">           
                    <div class="card-body pt-0">
                        <button id="btnAddAnticipo"  title="Agregar Anticipo" class="btn btn-success pull-right"
                            type="button">
                            <div class="fonticon-wrap">
                                <i class="ft-plus-circle"></i>
                            </div>
                        </button>
                    </div>
           

    <div class="card-body">
        <form class="row" id="formAnticipo">
                <div class="col-12 col-sm-3 col-md-3">
		 				<div class="form-group">
					        <label>Fecha Anticipo</label>			
					            <div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class = "Requerido form-control input-sm" id="fecha_anticipo" name="fecha_anticipo" tabindex="10"/>
								</div>
						</div>	
					</div> 

                    <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Beneficiario</label>
                            <select class="form-control select2" name="id_beneficiario" id="id_beneficiario" tabindex="1" id="" style="width: 100%;">
                                <option value="">Todos
                                </option>
                                @foreach ($beneficiario as $b)
                                <option value="{{$b->id}}">{{$b->cliente_n}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-3 col-md-3">
                            <div class="form-group">
                                <label>Nro. Anticipo</label>
                                <input type="text" class="form-control" id="nro_anticipo" name="nro_anticipo" value="" >
                            </div>
                    </div>
                    <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Nro. Recibo</label>
                            <input type="text" class="form-control" id="nro_recibo" name="nro_recibo" value="" >
                        </div>
                    </div>
                    <div class="col-12 col-sm-3 col-md-3">
                            <div class="form-group">
                                <label>Moneda</label>
                                <select class="form-control select2" name="id_moneda" id="id_moneda" tabindex="2" id="" style="width: 100%;">
                                    <option value="">Todos
                                    </option>
                                    @foreach ($currency as $div)
                                    <option value="{{$div->currency_id}}">{{$div->currency_code}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                </div>

                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Proforma</label>
                            <input type="number" class="form-control" id="nro_proforma" name="numProforma" value="" >
                        </div>
                </div>

                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Grupo</label>
                            <select class="form-control select2" name="grupo" id="grupo" tabindex="3" style="width: 100%;">
                                <option value="">Todos
                                @foreach($grupos as $grupo)
									<option value="{{$grupo->id}}">{{$grupo['denominacion']}}</option> 
								@endforeach
                            </select>
                        </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3">
                    <div class="form-group">
                        <label>Estado</label>
                        <select class="form-control select2_estado" name="estado[]" id="estado" tabindex="3" style="width: 100%;">
                            <option value=""></option>
                            <option value="1">Pendiente</option>
                            <option value="2">Aplicado</option>
                            <option value="3">Anulado</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Saldo PYG</label>
                            <input type="text" class="form-control format-number-control" id="total_gs" name="total_gs" value="0" disabled>
                        </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3">
                        <div class="form-group">
                            <label>Saldo USD</label>
                            <input type="text" class="form-control format-number-control" id="total_usd" name="total_usd" value="0" disabled>
                        </div>
                </div>

            <div class="col-12">
                <button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style="margin-right:10px;"><b>Excel</b></button>
                <button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
                <button type="button" id="btnBuscar" class="btn btn-info btn-lg pull-right mr-1" tabindex="4" ><b>Buscar</b></button>
            </div>

        </form>

                <div class="table-responsive table-bordered mt-1">
                    <table id="listado" class="table" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Fecha Anticipo</th>
                                <th>Beneficiario</th>
                                <th>N°</th>
                                <th>Recibo N°</th>
                                <th>Concepto</th>
                                <th>Importe</th>
                                <th>Moneda</th>
                                <th>Estado</th>
                                <th>Proforma</th>
                                <th>Usuario<b>Proforma</th>
                                <th>Grupo</th>
                                <th>Fecha Pago</th>
                                <th>Saldo</th>
                                <!--<th>Estado<br>Anticipo</th>-->
                            </tr>
                        </thead>
                        <tbody style="text-align: center">
                        </tbody>
                    </table>
                </div>
            </div>
		</div>
	</div>
</section>



@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>

<script type="text/javascript">
 $(document).ready(function(){




        var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('input[name="fecha_anticipo"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		// $('input[name="periodo"]').val('');
		$('input[name="fecha_anticipo"]').on('cancel.daterangepicker', function(ev, picker) {
				      $(this).val('');
		});
      //  $('input[name="fecha_anticipo"]').val('');
    });

    $(() => {
        
        main();
    });


    function main() {
        calendar();
        formatNumber();
        $('.select2').select2();
        // $('#estado').val(1).trigger('change.select2');

        $('.select2_estado').select2({
									multiple:true,
									placeholder: 'Todos'
								});

    //$("#tipoEstado").select2().val(1).trigger("change");
    $('.select2_estado').val([1,2,3]).trigger('change.select2');

        datatable();
        
    } //

    $('#btnAddAnticipo').click(() => {
        redirectCreateAnticipo()
    });
    $('#btnBuscar').click(() => {
        datatable();
    });
    $('#btnSaveAnticipo').click(() => {
        saveDataAnticipo();
    });

    function redirectCreateAnticipo(){
        $.blockUI({
						centerY: 0,
						message: "<h2>Redirigiendo a vista de Anticipo...</h2>",
						css: {
							color: '#000'
						}
					});

      location.href ="{{ route('anticipoAddCobranza') }}";
      //location.href ="{{ route('addPago') }}";
    }


    function calendar() {
        $('.single-picker').datepicker({
            format: "dd/mm/yyyy",
            language: "es"
        });
    }

    function limpiar()
		{
			$('#id_beneficiario').val('').trigger('change.select2');
			$('#id_moneda').val('').trigger('change.select2');
			$('#activo').val('').trigger('change.select2');
		}




    /* ========================================================================================== 
                                LOGICA TABLA
       ========================================================================================== */
    function datatable() 
    {
        var table;

        $.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });

       		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){

	



        table = $("#listado").DataTable({
            destroy: true,
            ajax: {
                url: "{{route('getListAnticipoCliente')}}",
                data: $('#formAnticipo').serializeJSON(),
                error: function (jqXHR, textStatus, errorThrown) {
                    
                    $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                    $.unblockUI();
                }

            },
            "columns": [
                
                {
                    data: 'fecha_format_anticipo'
                },
                {
                    data: function (x) {
                        if (Boolean(x.beneficiario_format)) {
                            return x.beneficiario_format.beneficiario_n
                        }
                        return '';
                    }
                },
                {
                    data: function(x){
                        return `<a href="{{route('anticipoDetalleCliente',['id'=>''])}}/${x.id}"  class="bgRed">
						        	<i class="fa fa-fw fa-search"></i>${x.id}</a>
						        	<div style="display:none;">${x.id}</div>
						        	`;
                    }
                },
                {
                    data: function(x){
                        if(jQuery.isEmptyObject(x.recibo) == false){
                            return `<a href="{{route('controlRecibo',['id'=>''])}}/`+x.id_recibo+`" class="bgRed">
                                        <i class="fa fa-fw fa-search"></i>${x.recibo.nro_recibo}</a>
                                        <div style="display:none;">${x.id}</div>
                                        `;
                        }else{
                            return ``;
                        }
                    }
                },
                {
                    data: 'concepto'
                },
                {
                    data: function(x){
                        return formatCurrency(x.id_moneda, parseFloat(x.importe));
                    }
                },
                {
                    data: function (x) {
                        if (Boolean(x.currency)) {
                            return x.currency.currency_code
                        }
                        return '';
                    }
                },
                {
                    data: function(x){ 
                        return (x.estado) ? x.estado.denominacion : '';
                    }
                },
                {
                    data: function (x) {
                        if (Boolean(x.proforma)) {
                            return x.proforma.id
                        }
                        return '';
                    }
                },
                {
                    data: 'usuario_proforma'
                },
                {
                    data: function (x) {
                        if (Boolean(x.grupo)) {
                            return x.grupo.denominacion
                        }
                        return '';
                    }
                },
                {
                    data: 'fecha_format_pago'
                },
                {
                    data: function(x){
                        return formatCurrency(x.id_moneda, parseFloat(x.saldo));
                    }
                },

               /* {
                    data: function(x){
                        if(parseFloat(x.saldo) > 0){
                            resultado = 'No Aplicado';
                        }else{
                            resultado = 'Aplicado';
                        }    
                        return resultado;
                    }
                },*/

            ],
            "aaSorting":[[0,"desc"]],
            "createdRow": function (row, data, iDataIndex) {
                        if(data.id_moneda == 111){
                            total = clean_num($('#total_gs').val());
                            suma =   parseFloat(total) + parseFloat(data.saldo);
                            $('#total_gs').val(suma);
                        }
                        if(data.id_moneda == 143){
                            total = clean_num($('#total_usd').val());
                            suma =   parseFloat(total) + parseFloat(data.saldo);
                            $('#total_usd').val(suma);
                        }
                    },
                    //AL FINALIZAR LA CARGA AGREGAR CONTROL DE COMAS Y PUNTO
                    "initComplete": function (settings, json) {
                        $.unblockUI();
                    }
                }).on('draw', function () {
                        /*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

                        $('#listado tbody tr .numeric').inputmask("numeric", {
                            radixPoint: ",",
                            groupSeparator: ".",
                            digits: 2,
                            autoGroup: true,
                            // prefix: '$', //No Space, this will truncate the first character
                            rightAlign: false
                        });


                });
        


            }, 300);


    }


    /* ========================================================================================== 
                                FORMATEO DE NUMEROS
       ========================================================================================== */
    	//DEFINIR FORMATO MONEDAS		
			const EURO = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const DOLAR = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });



	
        function formatCurrency(id_moneda, value)
        {
					
					let importe = 0;
	

					// console.log('formatCurrency', id_moneda, value);

							//PYG
							if(id_moneda == 111){
								importe = GUARANI(value,{ formatWithSymbol: false }).format(true);
							} 
							//USD
							if(id_moneda == 143) {
								importe = DOLAR(value,{ formatWithSymbol: false }).format(true);
							}

					return importe;		
                }
                

        function formatNumber() {

            $('.format-number-control').inputmask("numeric", {
                radixPoint: ",",
                groupSeparator: ".",
                digits: 2,
                autoGroup: true,
                // prefix: '$', //No Space, this will truncate the first character
                rightAlign: false
            });

        }

        $("#botonExcel").on("click", function(e){ 
			// console.log('Inicil');
                e.preventDefault();
                $('#formAnticipo').attr('method','post');
                $('#formAnticipo').attr('action', "{{route('generarExcelAnticipo')}}").submit();
            });

        function clean_num(n,bd=false){
            if(n && bd == false){ 
            n = n.replace(/[,.]/g,function (m) {  
                                    if(m === '.'){
                                        return '';
                                    } 
                                    if(m === ','){
                                        return '.';
                                    } 
                                });
            return Number(n);
            }
            if(bd){
                return Number(n);
            }
            return 0;
        }


</script>
@endsection