<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Recibo de Pago</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			width: 700px;
            /* height: 842; */
            /* 595 x 842 */

			/* width: 90%; */
			margin:0 auto;
    		z-index:1;
	}
	
	table{
		width: 100%;
        /* border: 1px solid black; */
	}

    td {
        /* border: 1px solid black; */
    }
	
	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 700;
	}

	.text{
		  overflow: hidden;
		  /*text-overflow: ellipsis;*/
		  white-space: nowrap;
		  display:block;
		  width:100%;
		  min-width:1px;
		}

	.f-10 {
		font-size: 10px !important;
	}

	.f-9 {
		font-size: 9px !important;
	}

	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}
	.c-text {
		text-align: center;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 20px;
	}


	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:120px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}

    .border-collapse td {
        border: 1px solid;

    }

    .mt-10{
        margin-top:10px;
    }

    .texto-vertical-2 {
    writing-mode: vertical-lr;
    transform: rotate(180deg);
}


    /*IMAGEN*/
    





	</style>
</head>
<body>
        <!-- ================================================================================================ 
                                                  INICIO DE  RECIBOS
        ================================================================================================ -->
    <?php 
        $banco_cuenta = '';
    ?>
    
        @foreach ($recibos as $recibo )<!--INICIO DE RECIBOS -->
          
           

    @php
        function formatMoney($num,$currency){
            // dd($f);
            if($currency != 111){
            return number_format($num, 2, ",", ".");	
            } 
            return number_format($num,0,",",".");

        }
        $empresaRuc = $empresa->ruc;
        $reciboDetalle = $recibo->detalle;
        $denominacionCliente = $recibo->cliente->nombre.' '.$recibo->cliente->apellido;
        $documento = $recibo->cliente->documento_identidad;
        $dvbase = '';
        $controlar = strpos($documento, '-');
        if ($controlar === false) {
            if($recibo->cliente->dv !=""){
                $dvbase = "-".$recibo->cliente->dv;
            }
        }
        $rucCliente = $documento.''.$dvbase;

        $monedaLetras = $recibo->moneda->hb_desc;
        $monedaCodigo = $recibo->moneda->currency_code;
        $montoLetras = $recibo->totalLetras;
        $concepto = $recibo->concepto;
        $observaciones = '';
        $total = $recibo->total_pago;
        $numRecibo = $recibo->nro_recibo;
        $fechaEmision = $recibo->fecha_creacion_format;   
        $estadoRecibo = $recibo->estado->denominacion;
        $detalleValores = ''; 
        //SI EL RECIBO ESTA APLICADO O COBRADO MOSTRAR LAS FORMAS DE PAGO
        if(!is_null($recibo->formaCobroDetalle) && ($recibo->id_estado == 56 || $recibo->id_estado == 40)){
            $detalleValores .= '<table class="c-text" FRAME="vsides" RULES="cols">'; 
            $detalleValores .= '<tr><td style="border-bottom: 1px solid;"><b>Forma de Cobro</b></td><td style="border-bottom: 1px solid;"><b>Importe</b></td><td style="border-bottom: 1px solid;"><b>Banco/Cuenta/Tarjeta</b></td><td style="border-bottom: 1px solid;"><b>Comprobante</b></td></tr>';
            foreach($fp_recibo as $value){
                if(isset($value->forma_cobro_cliente_n)){
                    $forma_pago = $value->forma_cobro_cliente_n;
                }else{
                    $forma_pago = '';
                }
                if($value->tipo_cuenta_bancaria != ""){
                    if(isset($value->banco_plaza_n)){
                        $banco_cuenta = $value->banco_plaza_n ." ".$banco_cuenta;
                    }else{
                        if(isset($value->banco_cuenta)){
                            $banco_cuenta = $value->banco_cuenta;
                        }else{
                            $banco_cuenta = '';
                        }
                    }
                }else{ 
                    $banco_cuenta = '';    
                }   

                if(isset($value->nro_comprobante)){
                    $nro_comprobante = $value->nro_comprobante;
                }else{
                    $nro_comprobante = '';
                }

                if($value->abreviatura = 'VO'){
                    $banco_cuenta = $value->tarjeta_num_operacion;
                }else{
                    $banco_cuenta = '';
                }
                
                $detalleValores .= '<tr><td style="border-bottom: 1px solid;">'.$forma_pago.'</td><td style="border-bottom: 1px solid;">'.formatMoney($value->importe_pago,$value->id_moneda).'  '.$value->currency_code.'</td><td style="border-bottom: 1px solid;">'.$banco_cuenta.'</td><td style="border-bottom: 1px solid;">'.$nro_comprobante.'</td></tr>';
              
            } 
            $detalleValores .= '</table>';    
        }
        
        $imprimir  = [];    
        // $copia = 'Original - Cliente';
        if($option == 1){ $imprimir = ['Original - Cliente']; }
        if($option == 2){ $imprimir = ['Original - Cliente','Duplicado']; }
        if($option == 3){ $imprimir = ['Duplicado']; }

        $opcionImpresion = 1;
        $cont = $option;
        $contadorFila = 0;

        // dd($cont);
    @endphp


	
	<!-- <div id="background">
        <p id="bg-text" style="margin-top: 100px;"><?php //echo $copia; ?></p>
    </div>    -->
    
    <!-- ================================================================================================ 
                                                     CABECERA
        ================================================================================================ -->

@foreach( $imprimir as $indice => $valuePrint)
    @if($recibo->id_estado == 55)
        <div id="background">
                <p id="bg-text" style="margin-top: 100px; margin-left: 10px"><?php echo "ANULADO";?></p>
        </div>   
    @endif    

<div class="container" style="margin-top:7%;">
    @php 
        if($indice != 0){
                echo '<span>----------------------------------------------------------------------------------------------------------------------------------</span>'; 
        }else{ 
                echo '<span style="color:#ffffff;">----------------------------------------------------------------------------------------------------------------------------------</span>'; 

        }   
    @endphp
    <table>
        <tr>
            <td>
                {{-- <div class="texto-vertical-2">ORIGINAL CLIENTE</div> --}}
            </td>
    
            <td>



    {{-- <div class="texto-vertical-2">TEXTO DE PRUEBA PARA SABER SI ES COPIA</div> --}}

	<table class="" cellpadding="0">
		<tr>
          <td style="width:70%">
            <table cellpadding="0" style="">

                	<colgroup>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                    </colgroup>
                    <tr>
                        <td colspan="2">
                                <img src="{{$logo}}" width="90px">
                        </td> 

                        <td class="c-text" colspan="10" style="font-size: 7px !important; font-weight:800;">
                                @php 
                                    $idEmpresa =Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa;

                                @endphp
                                <span class=""></span><br>
                                @php echo $empresa->head_factura_txt; @endphp


                        </td>
                </tr>
            </table>
          </td>
          <td style="width:30%">
              <table>
                	<colgroup>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                        <col style="width:10%"/>
                
                    </colgroup>


                    <tr style="font-size: 12px !important; font-weight:800;">
                        <td colspan="5" class="mt-10">RUC :  @php echo $empresaRuc; @endphp</td>
                    </tr>

                    <tr style="font-size: 12px !important; font-weight:800;" class="mt-10">
                        <td colspan="12">RECIBO N°: @php echo $numRecibo; @endphp</td>
                    </tr>

                    <tr style="font-size: 10px !important; font-weight:800;" class="mt-10">
                            <td colspan="12">FECHA : @php echo $fechaEmision; @endphp</td>
                    </tr>

                    <tr style="font-size: 10px !important; font-weight:800;" class="mt-10">
                            <td colspan="12">MONTO @php echo $monedaCodigo.' '.$total; @endphp</td>
                    </tr>
                    <tr style="font-size: 10px !important; font-weight:800;" class="mt-10">
                    <td colspan="12"> @php echo $valuePrint; @endphp</td>
              </tr>

              </table>
          
          </td>  		   
    	</tr>
    </table>   

    <table>
            <tr class="" style="font-size:8px;">
                <td>
                        @foreach($sucursalEmpresa as $sucursales) 
                            <span class="f-5">
                            <b><?php echo ucwords(strtolower($sucursales->nombre)); ?>:</b> {{$sucursales->direccion}}. Telefonos: {{$sucursales->telefono}}</span><br>
                        @endforeach 
                </td>
            </tr>
    </table>

    
    <table style=""  class="b-buttom b-top">

        	<colgroup>
                <col style="width:10%"/>
                <col style="width:10%"/>
                <col style="width:10%"/>
                <col style="width:10%"/>
                <col style="width:10%"/>
                <col style="width:10%"/>
                <col style="width:10%"/>
                <col style="width:10%"/>
                <col style="width:10%"/>
                <col style="width:10%"/>

        
            </colgroup>

            <tr style="font-size: 10px;">
                <td class="" colspan="7">
                    <b>Recibí de</b> @php echo $denominacionCliente; @endphp
                </td>

                <td class="f-10" colspan="3">
                        <b> RUC :</b> @php echo $rucCliente; @endphp
                </td>

            </tr>

            <tr style="font-size: 10px;">
                    <td class="" colspan="10"> 
                            <b>La Cantidad de @php echo $monedaLetras.'.--' ; @endphp  </b><span style="border-radius:10px; padding:1px;background-color:#B6AAAA;">@php echo $montoLetras.'.--' ; @endphp</span>
                    </td>

    
            </tr>

            <tr style="font-size: 10px;">
                <!--HASTA 763 CARACTERES -->
                <td colspan="7" style=" word-wrap: break-word;">
                        <b>Por concepto de :</b> Cobro de factura - @php echo $denominacionCliente.'.--' ; @endphp
                </td>
                <td colspan="3" style=" word-wrap: break-word;">
                      
                </td>
            </tr>


        </table>
           
	<table class="c-text b-col" style="border: 1px solid;">
        <colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
		</colgroup>
		<tr style="font-size: 10px !important;border: 1px solid;">
			<td width="15%" style="border: 1px solid;">
                <b>Factura N°</b>
			</td>
			<td width="15%" style="border: 1px solid;">
                <b>Vencimiento</b>
			</td>
			<td width="15%" style="border: 1px solid;">
                <b>Importe</b>
			</td>
            <td width="15%" style="border: 1px solid;">
                <b>Saldo</b>
            </td>
    
            <td width="40%" style="border: 1px solid;">
                <b>Concepto</b>
            </td>
        </tr>
        @php
        $contadorFila = 0;
        $cantDetalle = count($reciboDetalle) -1 ;
        $cantInsert = 10 - count($reciboDetalle); 
        //SABER SI ES PAR  O IMPAR  
        @endphp
        
        @foreach($reciboDetalle as $key => $value)
            <tr style="font-size: 9px !important;">
            @php
                $imprimir ='';
                    if(!empty($value->libroVenta)){
                            if((float)$value->libroVenta->saldo == 0){
                                if(isset($value->libroVenta->pasajero->nombre)){
                                    $nombrePasajero = '';
                                    if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa != 'A'){
                                        $nombrePasajero = "- ".$value->libroVenta->pasajero->nombre." ".$value->libroVenta->pasajero->apellido;
                                    }
                                    $concepto = 'Cancelación de factura. - '.$nombrePasajero;
                                }else{
                                    $concepto = 'Cancelación de factura.';
                                }    
                            }else{ 
                                if(isset($value->libroVenta->pasajero->nombre)){
                                    $nombrePasajero = '';
                                    if(Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa != 'A'){
                                        $nombrePasajero = "- ".$value->libroVenta->pasajero->nombre." ".$value->libroVenta->pasajero->apellido;
                                    }
                                    $concepto = 'A cuenta de factura. '.$nombrePasajero;
                                }else{
                                    $concepto = 'A cuenta de factura.';
                                }    
                            }   
                            if($value->libroVenta->vencimiento == ""){
                                $vencimiento = '';
                            }else{ 
                                $vencimiento = date('d/m/Y',strtotime($value->libroVenta->vencimiento));
                            }   
                            if($recibo->id_estado == 56){
                                $saldo = (float)$value->libroVenta->saldo;
                            }else{
                                if($value->libroVenta->saldo == 0){
                                    $saldo = (float)$value->libroVenta->saldo;
                                }else{
                                    $saldo = (float)$value->libroVenta->saldo - (float)$value->importe;
                                }    
                                $concepto .= '- Pendiente de Aplicación';
                            }    
                            $imprimir = " <td style='border: 1px solid;'>{$value->libroVenta->nro_documento}</td>
                                        <td style='border: 1px solid;'>{$vencimiento}</td>
                                        <td style='border: 1px solid;'>".formatMoney($value->importe,$value->id_moneda)."</td>
                                        <td style='border: 1px solid;'>".formatMoney($saldo,$value->libroVenta->id_moneda_venta)."</td>
                                        <td style='border: 1px solid;'>{$concepto}</td>";
                    }
                    if(!empty($value->libroCompra)){
                            if((float)$value->libroCompra->saldo == 0){
                                $concepto = 'Cancelación de factura.';
                            }else{ 
                                $concepto = 'A cuenta de factura.';
                            }    
                            if($value->libroCompra->vencimiento == ""){
                                $vencimiento = '';
                            }else{ 
                                $vencimiento = date('d/m/Y',strtotime($value->libroCompra->vencimiento));
                            }   
                            if($recibo->id_estado == 56){
                                if($value->libroCompra->saldo == 0){
                                    $saldo = (float)$value->libroCompra->saldo;
                                }else{
                                    $saldo = (float)$value->libroCompra->saldo - (float)$value->importe;
                                }    
                            }else{
                                $saldo = (float)$value->libroCompra->saldo - (float)$value->importe;
                                $concepto .= '- Pendiente de Aplicación';
                            }    

                            $imprimir =  " <td style='border: 1px solid;'>{$value->libroCompra->nro_documento}</td>
                                        <td style='border: 1px solid;'>{$vencimiento}</td>
                                        <td style='border: 1px solid;'>".formatMoney($value->importe,$value->id_moneda)."</td>
                                        <td style='border: 1px solid;'>".formatMoney($saldo,$value->libroCompra->id_moneda_venta)."</td>
                                        <td style='border: 1px solid;'>{$concepto}</td>";
                    }
                    if(!empty($value->anticipo)){
                            if((float)$value->anticipo->saldo == 0){
                                $concepto = 'Cancelación de anticipo.';
                            }else{ 
                                $concepto = 'A cuenta de factura.';
                            } 
                            if($value->anticipo->fecha_hora_creacion == ""){
                                $vencimiento = '';
                            }else{ 
                                $vencimiento = date('d/m/Y',strtotime($value->anticipo->fecha_hora_creacion));
                            }   
                            if($recibo->id_estado == 56){
                                if($value->anticipo->saldo == 0){
                                    $saldo = (float)$value->anticipo->saldo;
                                }else{
                                    $saldo = (float)$value->anticipo->saldo - (float)$value->importe;
                                }    
                            }else{
                                $saldo = (float)$value->anticipo->saldo - (float)$value->importe;
                                $concepto .= '- Pendiente de Aplicación';
                            }  
                              
                            $documento = $value->anticipo->documento;
                            if($documento == ""){
                                $documento = $value->anticipo->id;
                            }    

                            $imprimir =  "  <td style='border: 1px solid;'>{$documento}</td>
                                            <td style='border: 1px solid;'>{$vencimiento}</td>
                                            <td style='border: 1px solid;'>".formatMoney($value->importe,$value->id_moneda)."</td>
                                            <td style='border: 1px solid;'>".formatMoney($saldo,$value->id_moneda)."</td>
                                            <td style='border: 1px solid;'>{$concepto}</td>";
                    }
                    echo $imprimir;
                    echo " </tr>  ";
                    $contadorFila++; 

                if(isset($value->anticipo->aplicacion)){

                    $imprimir = '';
                    foreach ($value->anticipo->aplicacion as $aplicacion) {
                        $total_factura = $aplicacion->factura->id_tipo_facturacion == 1 ? $aplicacion->factura->total_bruto_factura : $aplicacion->factura->total_neto_factura; 
                        $contadorFila++; 
                        $vencimiento = $aplicacion->factura->vencimiento ? date('d/m/Y',strtotime($value->anticipo->fecha_hora_creacion)) : '';
                        $imprimir .=  " <tr style='font-size: 9px !important;'>
                                            <td style='border: 1px solid;'>{$aplicacion->factura->nro_factura}</td>
                                                <td style='border: 1px solid;'>{$vencimiento}</td>
                                                <td style='border: 1px solid;'>".formatMoney( $total_factura,$aplicacion->factura->id_moneda_venta)."</td>
                                                <td style='border: 1px solid;'>".formatMoney($aplicacion->factura->saldo_factura,$aplicacion->factura->id_moneda_venta)."</td>
                                                <td style='border: 1px solid;'>Aplicación de Anticipo</td></tr>";
                    }
                 
                  echo $imprimir;
                }

              
              
                @endphp    
              
         @endforeach     
        
        @for($e = 0; $e < $cantInsert; $e++)
            <tr style="font-size: 9px !important; color:#fff">
                <td>---------------</td>
                <td>---------------</td>
                <td>---------------</td>
                <td>---------------</td>
                <td>---------------</td>
            </tr>
            @php $contadorFila++; @endphp    
        @endfor
        @if(!empty($recibo->concepto_adicional))
            <tr style="font-size: 11px !important; text-align: center;">
                <td colspan="5">
                    <b>** {{$recibo->concepto_adicional}} **</b>
                </td>
            </tr>
        @else 
        <tr style="font-size: 11px !important; text-align: center;">
                <td colspan="5">
                </td>
            </tr>

        @endif 

        <tr style="font-size: 12px !important; text-align: left; border: 1px solid;">
            <td colspan="5" style="border: 1px solid;">
                <b> TOTAL @php echo $monedaLetras.":</b>   ".number_format($total, 2, ',', '.'); @endphp
            </td>
        </tr>
    </table>
        <table>
            <?php 
                if($valuePrint == 'Duplicado'){    
            ?>
                <tr>
                    <td colspan="2" style="font-size:11px; border: 1px solid;">
                            <b>Detalle de Valores:</b>  
                            <br> 
                            @php echo $detalleValores; @endphp
                            <br>
                    </td>
                </tr>
            <?php 
                } 
            ?>
            <tr>
                <td style="font-size:11px;">
                    <br>
                    <br>
                    Usuario: {{$recibo->usuario->nombre.' '.$recibo->usuario->apellido}}
                </td>
                <td style="font-size:11px;">
                    <br>
                    <br>
                    E-Mail: {{$recibo->usuario->email}}
                </td>
            </tr>
        </table>


    </td>
</tr>
</table>


    </div>		
 @endforeach <!--TIPO DE COPIAS -->
        


 @endforeach <!-- FINALIZACION DE RECIBOS-->   


</body>
</html>
