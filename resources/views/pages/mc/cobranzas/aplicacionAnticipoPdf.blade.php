<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>APLICACION DE ANTICIPOS - CLIENTE</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 5px;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 95%;
			margin: 5px auto;
    		z-index:1;
	}
	
	th {
        border:1px solid black;
      }

    table {
		margin: 0;
        border-spacing:0px;
		width: 100%;
      }

	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 700;
	}

	.text{
		  overflow: hidden;
		  /*text-overflow: ellipsis;*/
		  white-space: nowrap;
		  display:block;
		  width:100%;
		  min-width:1px;
		}

	.f-8 {
		font-size: 10px !important;
	}

	.f-9 {
		font-size: 9px !important;
	}

	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}

	.f-15 {
		font-size: 15px !important;
	}
	.f-17 {
		font-size: 17px !important;
	}

	.c-text {
		text-align: center;
	}
	.r-text {
		text-align: left;
	}
	
	.r-text-detalle{
		margin-right: 20px;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 10px;
	}

	.b-buttom {
	border-bottom: 1px solid; 
	}

	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:120px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}



	</style>
</head>
<body>
		<?php
			$logoEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->logoEmpresa;
			$empresaLogo = "logoEmpresa/".$logoEmpresa;
		?>
		<br>
		<br>
		<div class="container espacio-10">
			<div class="table-responsive" style="border: solid 2px #000;border-radius:15px;">
				<table style="">
					<tr>
						<td colspan="5" class="r-text">
									<br>
									<img src="{{asset($empresaLogo)}}" style="width: 120px;height: 45px;">
							</td>
							<td class="c-text f-8" style="padding-left: 750px;">
								<b>Fecha: </b><?php echo date('d/m/Y H:m:s')?><br>
								<b>Usuario: </b><?php echo Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido?>
							</td>
					</tr>
					<tr>
						<td colspan="6" class="c-text f-17">
							<br>
							<br>
							<br>
						</td>
					</tr>
					<tr>
						<td colspan="6" class="c-text f-17">
							<b>APLICACION DE ANTICIPOS - CLIENTE</b>
						</td>
					</tr>
				</table>
				<br>
								<table style="width: 100%;"  >
									<tr style="background-color: #d0d1d3;">
										<th class="c-text f-8">Fecha</th>
										<th class="c-text f-8">Cliente</th>	
										<th class="c-text f-8">Nro. Recibo</th>	
										<th class="c-text f-8">Importe</th>	
										<th class="c-text f-8">Saldo</th>	
										<th class="c-text f-8">Moneda</th>	
									</tr>
									@if(!empty($listado['data'][0]))
										@foreach($listado['data'] as $lista)
												<tr>
													<td style="border:1px solid black;" class="c-text f-8">{{$lista->fecha}}</td>
													<td style="border:1px solid black;" class="c-text f-8">{{$lista->beneficiario_nombre}} {{$lista->beneficiario_apellido}}</td>
													<td style="border:1px solid black;" class="c-text f-8">{{$lista->nro_recibo}}</td>
													<td style="border:1px solid black;" class="c-text f-8">{{$lista->importe}}</td>
													<td style="border:1px solid black;" class="c-text f-8">{{$lista->saldo}}</td>
													<td style="border:1px solid black;" class="c-text f-8">{{$lista->moneda}}</td>
												</tr>
												<tr>
													<td colspan="6" style="border:1px solid black;" class="c-text f-8">
														<table cellpadding="10" cellspacing="0" border="0" style=" width: 100%"> ';
															<tr style="background-color: #eaebee;">
																<th style="width: 80px;">Fecha</th>
																<th style="width: 170px">Concepto</th>
																<th style="width: 50px;">Fecha Pago</th>
																<th style="width: 70px;">Factura</th>
																<th style="width: 30px;">Moneda</th>
																<th style="width: 70px;">Importe</th>
																<th style="width: 70px;">Saldo</th>
																<th style="width: 60px;">Proforma</th>   
																<th style="width: 60px;">Estado</th>
																<th style="width: 50px;">Vencimiento</th>
															</tr>
															@if(!empty($lista->detalles))
																@foreach($lista->detalles as $datos)
																	<?php
																		if($datos->id_moneda == 111){
																			$importe =  number_format($datos->monto, 0, ",", ".");
																			$saldo = number_format($datos->saldo, 0, ",", ".");
																		}else{
																			$importe = number_format($datos->monto, 2, ",", ".");
																			$saldo = number_format($datos->saldo, 2, ",", ".");
																		} 

																		if($datos->fecha_pago != ""){
																			$fecha_pago = date('d/m/Y', strtotime($datos->fecha_pago));
																		}else{
																			$fecha_pago ='';
																		}
																		if($datos->fecha_vencimiento != ""){
																			$fecha_vencimiento = date('d/m/Y', strtotime($datos->fecha_vencimiento));
																		}else{
																			$fecha_vencimiento ='';
																		}

																	?>
																	<tr>
																		<th style="width: 80px;">{{date('d/m/Y H:m:s', strtotime($datos->fecha_aplicacion))}}</th>
																		<th style="width: 170px">{{$datos->concepto}}</th>
																		<th style="width: 50px;">{{$fecha_pago}}</th>
																		<th style="width: 70px;">{{$datos->nro_factura}}</th>
																		<th style="width: 30px;">{{$datos->moneda}}</th>
																		<th style="width: 70px;">{{$importe}}</th>
																		<th style="width: 70px;">{{$saldo}}</th>
																		<th style="width: 60px;">{{$datos->id_proforma}}</th>   
																		<th style="width: 60px;">{{$datos->estado}}</th>
																		<th style="width: 50px;">{{$fecha_vencimiento}}</th>
																	</tr>
																@endforeach
															@endif
														</table>
													</td>
												</tr>
												<?php
													/*$total_importe = $total_importe+$datos->importe;
													$saldo_neto = $saldo_neto+$datos->saldo;*/
												?>
											@endforeach
										@endif
								</table>
						<br>
					<br>
			</div>
		</div>
		<br>
</body>
</html>
