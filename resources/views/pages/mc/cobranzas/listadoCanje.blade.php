@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
	.negrita{
		font-weight: bold;
	}
	.seletc2-sm {
		height: 30px !important;
	}


</style>	

	@parent
@endsection
@section('content')


<section id="base-style">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Listado Canje</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
			<form class="box-body" id="formRecibo">
				

            	<div class="row">

				<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Cliente</label>
							<select class="form-control select2 adaptForm" name="id_cliente"  id="id_cliente" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($clientes as $cliente)
									@php
										$ruc = $cliente->documento_identidad;
										if($cliente->dv){
											$ruc = $ruc."-".$cliente->dv;
										}
									@endphp
									<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->cliente_n}} - {{$cliente->id}}</option>
									@endforeach
							</select>
						 </div>
				</div>


				<div class="col-12 col-sm-6 col-md-3">
						<div class="form-group">
							<label>Moneda</label>
							<select class="form-control select2" name="id_moneda"  id="id_moneda"  style="width: 100%;">
									<option value="">Todos</option>
								
								
							</select>
						 </div>
				</div>

				<div class="col-6 col-sm-6 col-md-3">  
						<div class="form-group adaptForm">
						   <label>Fecha Operación</label>						 
						   <div class="input-group">
							   <div class="input-group-prepend" style="">
									<span class="input-group-text"><i class="fa fa-calendar"></i></span>
							   </div>
							   <input type="text" readonly  class="form-control pull-right single-picker inputAdapt"  style="background-color: white;" name="fecha_creacion:s_date" id="fecha_emision" value="">
						   </div>
					   </div>	
				</div> 
				
		        <div class="col-6 d-block d-sm-none">
					<button  type="button" class="btn btn-info btn-block btnBuscar visible-xs" style="margin-top:25px;" ><b>BUSCAR</b></button>
				</div> 

				<div class="col-12 d-none d-sm-block">
						<button type="button" class="btn btn-info  btnBuscar btn-lg pull-right " style="margin-top:5px; margin-right:10px;" ><b>BUSCAR</b></button>
					</div> 
				
				</div> 
			</form>      
			

		
 
				<div class="table-responsive">
	              <table id="listado" class="table table-hover table-condensed nowrap" style="width: 100%;">
	                <thead>
					  <tr class="text-center">
                        <th></th> 
                        <th>Cliente</th> 
                        <th>Importe</th> 
                        <th>Fecha Documento</th> 
                        <th>Fecha Registro</th> 
		              </tr>
	                </thead>
	                <tbody class="text-center">
					  
			        </tbody>
				  </table>
				</div>

	
				

				</div>
			</div>
		</div>
	</section>
	
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script src="https://unpkg.com/currency.js@~1.2.0/dist/currency.min.js"></script>

	<script type="text/javascript">
	

		var tableListado;

		$(document).ready(function() {
			tableRecibo();
			calendar();
			$('.select2').select2();


		$('.btnBuscar').click(()=>{
			tableRecibo();
		});

	});





		
		


		function  tableRecibo(responsive = false){
			let resp;


			let form = $('#formRecibo').serializeJSON({
				customTypes: customTypesSerializeJSON
			});

			tableListado = $("#listado").DataTable({
				destroy: true,
				ajax: {
			 		url: "{{route('canjeTable')}}",
			 		data: form,
			 		error: function (jqXHR, textStatus, errorThrown) {
						$.toast({	
								heading: '<b>Atención</b>',
								position: 'top-right', 
								text: 'Ocurrio un error en la comunicación con el servidor.',
								hideAfter: false,
								icon: 'error'
							});
			 		}

			 	},
				 responsive: {
						details: false
					},

	
				"columns": [

                {data: function(x){ 
                    return `<a href="{{route('addLibroCompra')}}?canje=${x.id}" role="button" class="btn btn-primary"><i class="fa fa-fw fa-search"></i></a>`;
                }},    
                {data: 'cliente_n'},
                {data: (x)=>{ return formatCurrency(x.id_moneda,x.importe_pago); }},
                {data: 'fecha_documento_format'},
                {data: 'fecha_insert_detalle_format'},
                
			 	

			 	],
				 "createdRow": function ( row, data, index ) {
								 
					}	
				});

		}


		function calendar(){
			$('.single-picker').datepicker({ 
						format: "dd/mm/yyyy", 
						language: "es",
						clearBtn : true
						});
		 }//


		 /**
		 FORMATEO DE MONEDAS
		*/

			//DEFINIR FORMATO MONEDAS		
			const EURO = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const DOLAR = value => currency(value, { symbol: "", decimal: ',', separator: '.', precision: 2 });
			const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });


			function formatCurrency(id_moneda, value){
					
					let importe = 0;
	

					console.log('formatCurrency', id_moneda, value);

							//PYG
							if(id_moneda == 111){
								importe = GUARANI(value,{ formatWithSymbol: false }).format(true);
							} 
							//USD
							if(id_moneda == 143) {
								importe = DOLAR(value,{ formatWithSymbol: false }).format(true);
							}

					return importe;		
				}

		 
	

		
			
		
	</script>
@endsection