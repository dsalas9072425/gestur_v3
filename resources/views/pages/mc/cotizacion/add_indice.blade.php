@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
	border-radius: 14px !important;
	}
	</style>
	
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Indice Cotización</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmCotizacion" method="post" action="{{route('doAddCotizacion')}}">
					<div class="row">


					<div class="col-12 mb-2">
						<h6>(*) Campos requeridos - Se debe cargar la cotización en relación al Guaraní (USD → PYG, EUR → PYG, REAL → PYG)</h6>
					</div>	
						<div class="col-12 col-md-4">
							<div class="form-group">
								<label>Tipo de divisa Origen (*)</label>
								<select class="form-control select2" name="divisa_id_origen" id="divisa_id_origen" style="width: 100%;" required>
									<option value="">Seleccione Divisa</option>

									@foreach($divisa as $divisas)
									<option value="{{$divisas->currency_id}}">{{$divisas->currency_code}} -
										{{$divisas->hb_desc}}</option>
									@endforeach

								</select>
							</div>
						</div>

						<div class="col-12 col-md-4">
							<div class="form-group">
								<label>Tipo de divisa Destino(*)</label>
								<select class="form-control select2" name="divisa_id_destino" id="divisa_id_destino" style="width: 100%;" required>
									<option value="">Seleccione Divisa</option>

									@foreach($divisa as $divisas)
									<option value="{{$divisas->currency_id}}">{{$divisas->currency_code}} -
										{{$divisas->hb_desc}}</option>
									@endforeach

								</select>
							</div>
						</div>


						<div class="col-12 col-md-4">
							<label class="control-label">Indice Cotización (*)</label>
							<input type="number" step="any" name="indice_cotizacion" class="Requerido form-control" value=""
								id="indice_cotizacion" required="required" />
						</div>
					</div>

					<div class="row">
						<div class="col-12 mb-2">
							<a href="{{ route('cotizacionIndex') }}" type="button" class="btn btn-danger btn-lg pull-right mr-1">Volver</a>
							<button type="button" onclick="guardarCotizacion()" id="btnGuardar" class="btn btn-success btn-lg pull-right mr-1">Guardar</button>


						</div>
					</div>

				</form>


			</div>
		</div>
	</section>
	</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>

	<script>

			function validarCampos(){
				

				var tipo_origen  = $('#divisa_id_origen').val();
				var tipo_destino = $('#divisa_id_destino').val();
				var indice_cotizacion 		 = $('#indice_cotizacion').val().trim();

				if(indice_cotizacion =='' || indice_cotizacion == 0	 || tipo_destino == 0 || tipo_destino == '' || tipo_origen == '' || tipo_origen == 0 ){
					return false;
				}

				return true

			}

			$(document).ready(function() {
				$('.select2').select2();
				$('.select2').on('change', function() {
				$(this).trigger('blur');
				});

			});

		
			$('.numeric').inputmask("numeric", {
				radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				rightAlign: false,
				oncleared: function () { self.Value(''); }
			});



		   function guardarCotizacion() {
					var dataString = $('#frmCotizacion').serialize();
					console.log(dataString);

					if(validarCampos()){

			       $.ajax({
						type: "GET",
						url: "{{route('indice_cotizacion.save')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },
						success: function(rsp){

								$.toast({
									    heading: 'Success',
									    text: 'Los datos fueron almacenados.',
									    position: 'top-right',
									    showHideTransition: 'slide',
				    					icon: 'success'
									});
								window.location.replace("{{route('cotizacionIndex')}}");

	
						}//funcion	
				});				
					} else {
						 $.toast({
                            heading: 'Error',
                            text: 'Complete los campos requeridos.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });
					}

		}


			

	</script>
@endsection