
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
	</style>
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Cotización</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmProforma" method="post">
					<div class="row">
						<div class="col-12">
							<a href="{{ route('cotizacionAdd') }}" class="btn btn-success pull-right" role="button">
								<div class="fonticon-wrap style-icon">
									<i class="ft-plus-circle"></i>
								</div>
							</a>
						</div>
						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Moneda</label>
								<select class="form-control select2" name="divisa_id" id="divisa_id"
									style="width: 100%;" required>
									<option value="">Seleccione Moneda</option>

									@foreach($divisa as $divisas)
									<option value="{{$divisas->currency_id}}">{{$divisas->currency_code}} -
										{{$divisas->hb_desc}}</option>
									@endforeach

								</select>
							</div>
						</div>

						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Periodo</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i
												class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="periodo" id="periodo"
										required>
								</div>
							</div>
						</div>

						<div class="col-11 mb-1">
							<button type="button" onclick="buscarCotizacion()" id="btnGuardar" class="btn btn-info btn-lg pull-right mb-1">Buscar</button>
						</div>
					</div>
				</form>


				<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr style="text-align: center;">
								<th>Moneda</th>
								<th>Fecha</th>
								<th>Cotización Operativa</th>
								<th>Cotización Contable Compra</th>
								<th>Cotización Contable Venta</th>
								<th>Cotización JURCAIP</th>
							</tr>
						</thead>
						<tbody id="lista_cotizacion" style="text-align: center">
						</tbody>
					</table>
				</div>

				<hr>

				<form id="frmIndiceCotizacion" method="post">
					<div class="row">
						<div class="col-12">
							<p>INDICE COTIZACION</p>
						</div>
						<div class="col-12">
							<a href="{{ route('indice_cotizacion.add') }}" class="btn btn-success pull-right" role="button">
								<div class="fonticon-wrap style-icon">
									<i class="ft-plus-circle"></i>
								</div>
							</a>
						</div>
						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Moneda Origen</label>
								<select class="form-control select2" name="divisa_id_origen" id="divisa_id_origen"
									style="width: 100%;" required>
									<option value="">Seleccione Moneda</option>

									@foreach($divisa as $divisas)
									<option value="{{$divisas->currency_id}}">{{$divisas->currency_code}} -
										{{$divisas->hb_desc}}</option>
									@endforeach

								</select>
							</div>
						</div>

						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Moneda Destino</label>
								<select class="form-control select2" name="divisa_id_destino" id="divisa_id_destino"
									style="width: 100%;" required>
									<option value="">Seleccione Moneda</option>

									@foreach($divisa as $divisas)
									<option value="{{$divisas->currency_id}}">{{$divisas->currency_code}} -
										{{$divisas->hb_desc}}</option>
									@endforeach

								</select>
							</div>
						</div>

						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Periodo</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i
												class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="periodo_indice" id="periodo_indice"
										required>
								</div>
							</div>
						</div>

						<div class="col-11 mb-1">
							<button type="button" onclick="buscarIndiceCotizacion()" id="btnGuardar" class="btn btn-info btn-lg pull-right mb-1">Buscar</button>
						</div>
					</div>
				</form>


				<div class="table-responsive table-bordered">
					<table id="listado_indice" class="table">
						<thead>
							<tr style="text-align: center;">
								<th>Moneda Origen</th>
								<th>Moneda Destino</th>
								<th>Fecha</th>
								<th>Indice Cotización</th>
							</tr>
						</thead>
						<tbody id="lista_cotizacion_indice" style="text-align: center">
						</tbody>
					</table>
				</div>





			</div>
		</div>
	</section>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
		//$(document).ready(function() {

			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});




			var inicioPeriodo, finPeriodo = 'vacio';

			var Fecha = new Date();
			var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

			//Activar calendario
			$( "#periodo_indice" ).daterangepicker({ 
								   timePicker24Hour: true,
							        timePickerIncrement: 30,
							       locale: {
											format: 'DD/MM/YYYY',
											cancelLabel: 'Limpiar'
													    },
									startDate: "01/"+fechaInicial,
        							endDate: new Date()
									});

			$('input[name="periodo_indice"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

			var a = moment().subtract(1, 'day');
			$("#periodo_indice").data('daterangepicker').setStartDate(a);

			//Activar calendario
			$( "#periodo" ).daterangepicker({ 
								   timePicker24Hour: true,
							        timePickerIncrement: 30,
							       locale: {
											format: 'DD/MM/YYYY',
											cancelLabel: 'Limpiar'
													    },
									startDate: "01/"+fechaInicial,
        							endDate: new Date()
									});

			$('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

			var a = moment().subtract(1, 'day');
			$("#periodo").data('daterangepicker').setStartDate(a);

		


		





		function buscarCotizacion() {


			$("#frmProforma").validate({
			  submitHandler: function(form) {
			    alert('Enviado');
			  }
			});

			var dataString = $('#frmProforma').serialize();

			$.ajax({
						type: "GET",
						url: "{{route('consultaCot')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },
						success: function(rsp){


							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

							$.each(rsp.divisa, function (key, item){
								// console.log('ingreso');
							//formatear fecha	
							var fecha = item.fecha;
							var fechaIntermedia = fecha.split(' ');
							var fechaFinal = fechaIntermedia[0].split('-');
							fechaMostrar = fechaFinal[2]+'/'+fechaFinal[1]+'/'+fechaFinal[0]+' '+fechaIntermedia[1];


								var dataTableRow = [
														item.moneda.hb_desc,
														fechaMostrar,
														item.venta,
														item.cotizacion_contable_compra,
														item.cotizacion_contable_venta,
														item.cotizacion_jurcaip

													];
								var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

							});

					


						}//cierreFunc	
				});	
			
		}

		function buscarIndiceCotizacion() {


				$("#frmIndiceCotizacion").validate({
				submitHandler: function(form) {
					alert('Enviado');
				}
				});

				var dataString = $('#frmIndiceCotizacion').serialize();

				$.ajax({
							type: "GET",
							url: "{{route('consultaIndiceCot')}}",
							dataType: 'json',
							data: dataString,

							error: function(jqXHR,textStatus,errorThrown){

							$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la comunicación con el servidor.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
									});

								},
							success: function(rsp){


								var oSettings = $('#listado_indice').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();
								for (i=0;i<=iTotalRecords;i++) {
									$('#listado_indice').dataTable().fnDeleteRow(0,null,true);
								}

								$.each(rsp.divisa, function (key, item){
									// console.log('ingreso');
								//formatear fecha	
								var fecha = item.created_at;
								var fechaIntermedia = fecha.split(' ');
								var fechaFinal = fechaIntermedia[0].split('-');
								fechaMostrar = fechaFinal[2]+'/'+fechaFinal[1]+'/'+fechaFinal[0]+' '+fechaIntermedia[1];


									var dataTableRow = [
															item.moneda_origen.hb_desc,
															item.moneda_destino.hb_desc,
															fechaMostrar,
															item.indice

														];
									var newrow = $('#listado_indice').dataTable().fnAddData(dataTableRow);
									var nTr = $('#listado_indice').dataTable().fnSettings().aoData[newrow[0]].nTr;

								});

						


							}//cierreFunc	
					});	

			}
				
			

	</script>
@endsection