@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
	border-radius: 14px !important;
	}
	</style>
	
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Cotización</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmCotizacion" method="post" action="{{route('doAddCotizacion')}}">
					<div class="row">


					<div class="col-12 mb-2">
						<h6>(*) Campos requeridos - Se debe cargar la cotización en relación al Guaraní (USD → PYG, EUR → PYG, REAL → PYG)</h6>
					</div>	
						<div class="col-12 col-sm-6 col-md-6">
							<div class="form-group">
								<label>Tipo de divisa (*)</label>
								<select class="form-control select2" name="divisa_id" id="divisa_id" style="width: 100%;" required>
									<option value="">Seleccione Divisa</option>

									@foreach($divisa as $divisas)
									<option value="{{$divisas->currency_id}}">{{$divisas->currency_code}} -
										{{$divisas->hb_desc}}</option>
									@endforeach

								</select>
							</div>
						</div>

						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 20px;">
							<label class="control-label">Cotización Operativa (*)</label>
							<input type="text" step="any" name="venta" class="Requerido form-control numeric" value="0" id="ventaId" required="required" />
						</div>
						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 20px;">
							<label class="control-label">Cotización JURCAIP (*)</label>
							<input type="text" step="any" name="cotizacion_jurcaip" class="Requerido form-control numeric" value="0" id="cotizacion_jurcaip" required="required" />
						</div>

						<div class="col-12 col-sm-3 col-md-2" style="padding-left: 20px; display:none;">
							<label class="control-label">Cotización Contable Compra (*)</label>
							<input type="number" step="any" name="cotizacion_contable_compra" class="Requerido form-control" value=""
								id="cotizacion_contable_compra" required="required" />
						</div>

						<div class="col-12 col-sm-3 col-md-3" style="padding-left: 20px; display:none;">
							<label class="control-label">Cotización Contable Venta (*)</label>
							<input type="number" step="any" name="cotizacion_contable" class="Requerido form-control" value=""
								id="cotizacion_contable" required="required" />
						</div>
					</div>

					<div class="row">
						<div class="col-12 mb-2">
							<a href="{{ route('cotizacionIndex') }}" type="button" class="btn btn-danger btn-lg pull-right mr-1">Volver</a>
							<button type="button" onclick="guardarCotizacion()" id="btnGuardar" class="btn btn-success btn-lg pull-right mr-1">Guardar</button>


						</div>
					</div>

				</form>


				<!--<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr style="text-align: center;">
								<th>Descripción</th>
								<th>Fecha</th>
								<th>Cotización Operativa</th>
								<th>Cotización Contable Compra</th>
								<th>Cotización Contable Venta</th>
							</tr>
						</thead>
						<tbody id="lista_cotizacion" style="text-align: center">
						</tbody>
					</table>
				</div>-->
			</div>
		</div>
	</section>
	</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>

	<script>

			function validarCampos(){
				var tipo = $('#divisa_id').val();
				var compra = $('#cotizacion_contable').val();
				var venta = $('#ventaId').val();
				var cotizacion_jurcaip = $('#cotizacion_jurcaip').val(); 

				if(cotizacion_contable =='' || cotizacion_contable == 0 || tipo == '' || venta == '' || venta == 0 || cotizacion_jurcaip == '' || cotizacion_jurcaip == 0){
					return false;
				}

				return true

			}

			$(document).ready(function() {
				$('.select2').select2();
				$('.select2').on('change', function() {
				$(this).trigger('blur');
				});

			});

		
			$('.numeric').inputmask("numeric", {
				radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				rightAlign: false,
				oncleared: function () { self.Value(''); }
			});



		   function guardarCotizacion() {
					var dataString = $('#frmCotizacion').serialize();
					console.log(dataString);

					if(validarCampos()){

			       $.ajax({
						type: "GET",
						url: "{{route('doAddCotizacion')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },
						success: function(rsp){
							if(rsp.err == 'true'){

								$.toast({
									    heading: 'Success',
									    text: 'Los datos fueron almacenados.',
									    position: 'top-right',
									    showHideTransition: 'slide',
				    					icon: 'success'
									});
								window.location.replace("{{route('cotizacionIndex')}}");

							//buscarCotizacion();
							} else {
							
							$.toast({
                            heading: 'Error',
                            text: 'Los datos no fueron almacenados.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });
							
							}//else
						}//funcion	
				});				
					} else {
						 $.toast({
                            heading: 'Error',
                            text: 'Complete los campos requeridos.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });
					}

		}


			

	</script>
@endsection