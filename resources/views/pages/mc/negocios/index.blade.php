
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
	</style>
@endsection
@section('content')

<section id="base-style">
	@include('flash::message')
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Negocios</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmProforma" method="post">
					<div class="row">
						<div class="col-12">
							<a href="{{ route('negocioAdd') }}" class="btn btn-success pull-right" role="button">
								<div class="fonticon-wrap style-icon">
									<i class="ft-plus-circle"></i>
								</div>
							</a>
						</div>
						<div class="col-12 col-sm-4 col-md-3">
						</div>

						<div class="col-12 col-sm-3 col-md-3">
						</div>

						<div class="col-11 mb-1">
						</div>
					</div>
				</form>


				<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr style="text-align: center;">
								<th>Denominación</th>
								<th>Rentabilidad Minima</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="lista_cotizacion" style="text-align: center">
						@foreach($negocios as $negocio)
							<tr style="text-align: center;">
								<th>{{$negocio->descripcion}}</th>
								<th>{{$negocio->rentabilidad_minima}}</th>
								<th>
									<a href="{{route('negocioEdit', ['id' =>$negocio->id])}}" class="btn btn-success text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px;" role="button"><i class="fa fa-clone"></i></a>
									<a href="{{route('doDeleteNegocio', ['id' =>$negocio->id])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; background: #e2076a !important;" role="button"><i class="	fa fa-close"></i></a>
								</th>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
		//$(document).ready(function() {

			$('.select2').select2();

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

	</script>
@endsection