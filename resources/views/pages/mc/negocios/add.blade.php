@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
	border-radius: 14px !important;
	}
	</style>
	
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Agregar Negocio</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmCotizacion" method="post" action="{{route('doAddNegocio')}}">
					<div class="row">
						<div class="col-12 col-sm-5 col-md-5" style="padding-left: 20px;">
							<label class="control-label">Denominacion</label>
							<input type="text" step="any" name="denominacion" class="Requerido form-control" value="" id="denominacion" required="required" />
						</div>
						<div class="col-12 col-sm-5 col-md-5" style="padding-left: 20px;">
							<label class="control-label">Rentabilidad Minima</label>
							<input type="number" step="any" name="rentabilidad_minima" class="Requerido form-control" value="10" id="rentabilidad_minima" required="required" />
						</div>

					</div>

					<div class="row">
						<div class="col-12 mb-2">
							<a href="{{ route('negocioIndex') }}" type="button" class="btn btn-danger btn-lg pull-right mr-1">Volver</a>
							<button type="button" onclick="guardarNegocio()" id="btnGuardar" class="btn btn-success btn-lg pull-right mr-1">Guardar</button>


						</div>
					</div>

				</form>

			</div>
		</div>
	</section>
	</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>

		$(document).ready(function() {
		//	buscarCotizacion();

		});


		function guardarNegocio() {
				var dataString = $('#frmCotizacion').serialize();
				if($('#rentabilidad_minima').val() != ""){
					if($('#denominacion').val() != ""){
						$.ajax({
								type: "GET",
								url: "{{route('doAddNegocio')}}",
								dataType: 'json',
								data: dataString,

								error: function(jqXHR,textStatus,errorThrown){

								$.toast({
									heading: 'Error',
									text: 'Ocurrio un error en la comunicación con el servidor.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
										});

									},
								success: function(rsp){
									if(rsp.err == 'OK'){

										$.toast({
												heading: 'Success',
												text: 'Los datos fueron almacenados.',
												position: 'top-right',
												showHideTransition: 'slide',
												icon: 'success'
											});
										window.location.replace("{{route('negocioIndex')}}");

									//buscarCotizacion();
									} else {
									
									$.toast({
											heading: 'Error',
											text: 'Los datos no fueron almacenados.',
											position: 'top-right',
											showHideTransition: 'fade',
											icon: 'error'
										});
									
									}//else
								}//funcion	
						});	
					}else{
								$.toast({
											heading: 'Error',
											text: 'Ingrese la denominacion para el Negocio.',
											position: 'top-right',
											showHideTransition: 'fade',
											icon: 'error'
										});

					}	
				}else{
								$('#rentabilidad_minima').val(10);
								$.toast({
											heading: 'Error',
											text: 'Ingrese la rentabilidad minima para el Negocio.',
											position: 'top-right',
											showHideTransition: 'fade',
											icon: 'error'
										});


				}			
		}



	</script>
@endsection