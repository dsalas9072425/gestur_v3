@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Listado de Menu</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
                         
				<a href="{{ url('addMenu')}}" title="Agregar Menu" class="btn btn-success pull-right mt-2"
					role="button" id="addTicketTipo">
					<div class="fonticon-wrap">
						<i class="ft-plus-circle"></i>
					</div>
				</a>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<table id="listadoMenu" class="table">
                    <thead>
                        <th>Descripcion</th>
                        <th>URL</th>
                        <th>Icono</th>
                        <th>Tipo</th>
                        <th>Dependencia</th>
                        <th>Estado</th>
                        <th>Accion</th>
                    </thead>
                </table>
			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
        $(document).ready(function() {
            mostarListadoMenu();
        });
        function mostarListadoMenu() {
                //crear dataTable	
                $("#listadoMenu").dataTable({
                    "paging":true,
                    "searching": true,
                    "processing": true,
                    "destroy": true,
                    "search": {
                        return: true,
                    },
                    "ajax": {
                    "url": "{{route('verDatosMenu')}}",
                    },
                    "columns": [
                        { data: 'descripcion' },
                        { data: 'url' },
                        { data: 'icono',       
                            render: function (data, type) {
                                    return ('<i class="'+data+'"></i>');
                            },
                         },
                        { data: 'id_menu_padre' ,
                            render: function (data, type, row) {
                                if(data == 0){
                                    return ("Menu");
                                }else{
                                    return ("Submenu");
                                }
                            },
                        },
                        { data: 'id_menu_padre' ,
                            render: function (data, type, row) {
                                if(data == 0){
                                    return ("Menu padre sin dependencias");
                                }else{
                                    
                                    return ("Depende del menu <b>"+row['nombrePadre']['descripcion']+"</b>");
                                }
                            },
                        },
                        { data: 'activo' ,
                            render: function (data, type) {
                                if(data == true){
                                    return ('<i class="fa fa-fw fa-circle green " title="Activo" style="color: green;"></i>');							
									}else{                                
                                     return ('<i class="fa fa-fw fa-circle rojo " title="Inactivo" style="color: red;"></i>');	
                                }
                            },
                        },
                        { data: 'id' ,
                            render: function (data, type) {                                
                                return (`<a href="{{route('editarMenu',['id'=>''])}}/${data}" class="btn btn-primary text-white pull-right" title=Editar Menu"><i class="fa fa-pencil"></i></a>`);
                                
                            },
                        },
                    ]
                    });
        }
	</script>
@endsection