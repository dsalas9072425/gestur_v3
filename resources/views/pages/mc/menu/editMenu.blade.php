@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Editar Menu</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<form id="guardarEdicion" autocomplete="off">
					<div class="container">
						<div class="col-12">
							<div class="form-group">
								<label for="descripcion">Nombre</label>
								<input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Administracion" value="{{$menu->descripcion}}" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="url">URL</label>
								<input type="text" class="form-control" id="url" name="url" placeholder="indexMenu" value="{{$menu->url}}"required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="icono">Icono</label>
								<input type="text" class="form-control" id="icono" name="icono" placeholder="fa fa-paper-plane" value="{{$menu->icono}}" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="estado">Estado</label>
								<select class="form-control form-control-lg" id="estado" name="estado">
									<option value="1">Activo</option>
									<option value="0">Inactivo</option>
								</select>
							</div>
						</div>
						
						<div class="col-12">
							<div class="form-group">
								<label for="visibilidad">Visibilidad</label>
								<select class="form-control form-control-lg" id="visibilidad" name="visibilidad">
									<option value="1">Visible</option>
									<option value="0">Invisible</option>
								</select>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="submenu">Menu padre </label>
								<select class="form-control form-control-lg" id="submenu" name="submenu">
									<option selected value="0">Seleccione un menu padre</option>
                                    @foreach($datos as $menus)
									<option value="{{$menus->id}}" >{{$menus->descripcion}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<small>Si el menu es ingresado sin un menu padre es considerado como menu padre</small>
						<button type="submit" id="guardarEdicion" class="btn btn-success pull-right mb-1 ml-1">Guardar</button>
						<a href="{{url('indexMenu')}}" class="btn btn-secondary text-white pull-right">Volver</a>
						<input type="hidden" value="{{$menu->id_menu_padre}}" id="idPadre" name="idPadre">
						<input type="hidden" value="{{$menu->activo}}" id="estadoSelected" name="estadoSelected">
						<input type="hidden" value="{{$menu->visible}}" id="visibilidadSelected" name="visibilidadSelected">
						<input type="hidden" value="{{$menu->id}}" id="idEditado" name="id">
 					</div>
				</form>		
			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>

	//Adquirir datos de los input con los valores seleccionados.
	var padre= $("#idPadre").val();
	var estado= $("#estadoSelected").val();
	if(estado == ''){
		estado = 0;
	}
	var visible= $("#visibilidadSelected").val();
	if(visible == ''){
		visible = 0;
	}

	//Cambiar selectores segun preselecciones.
	$("#submenu").val(padre).select2();	
	$("#estado").val(estado).select2();	
	$("#visibilidad").val(visible).select2();

	$( "#guardarEdicion" ).submit(function(e) {
				$("#guardarEdicion").validate();
				if(!$("#guardarEdicion").isValid){
					e.preventDefault();
                	var dataString = $('#guardarEdicion').serialize();
					$.ajax({
						type: "GET",
						url: "{{route('guardarEditar')}}",
						dataType: 'json',
						data: dataString,

							error: function(jqXHR,textStatus,errorThrown){

							$.toast({
							heading: 'Error',
							text: 'Ocurrio un error en la comunicación con el servidor.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});

							},
						success: function(rsp){
							if(rsp.estado == 1){
								$.toast({
								heading: 'Actualizado con exito.',
								text: 'Se actualizo con exito el item de menu',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'success'
								});
								window.location.href = "{{URL::to('indexMenu')}}"
							}else{		
								$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la insercion.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
									});
							}
						
						}
					});	
				}
            });
	</script>
@endsection