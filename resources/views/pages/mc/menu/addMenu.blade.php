@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Agregar Menu</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<form id="addMenu" autocomplete="off">
					<div class="container">
						<div class="col-12">
							<div class="form-group">
								<label for="menu">Nombre</label>
								<input type="text" class="form-control" id="menu" name="menu" placeholder="Administracion" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="url">URL</label>
								<input type="text" class="form-control" id="url" name="url" placeholder="indexMenu" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="icono">Icono</label>
								<input type="text" class="form-control" id="icono" name="icono" placeholder="fa fa-paper-plane" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label for="submenu">Menu padre. </label>
								<small>Si el menu es ingresado sin un menu padre es considerado como menu padre</small>
								<select class="form-control form-control-lg" id="submenu" name="submenu">
									<option selected value="">Seleccione un menu padre</option>
								    @foreach($menuPadre as $menu)
									<option value="{{$menu->id}}">{{$menu->descripcion}}</option>
									@endforeach
								</select>
							</div>
							
						</div>
						<div class="col-12" style="display:none">
							<div class="form-group">
								<label for="estado">Estado</label>
								<select class="form-control form-control-lg" id="estado" name="estado">
									<option value="true" selected>Activo</option>
									<option value="false">Inactivo</option>
								</select>
							</div>
						</div>
						
						<div class="col-12"style="display:none">
							<div class="form-group">
								<label for="visibilidad">Visibilidad</label>
								<select class="form-control form-control-lg" id="visibilidad" name="visibilidad">
									<option value="true" selected>Visible</option>
									<option value="false">Invisible</option>
								</select>
							</div>
						</div>
						
						<button type="submit" class="btn btn-success pull-right mb-1 ml-1">Guardar</button>
						<a href="{{url('indexMenu')}}" class="btn btn-secondary pull-right mb-1">Volver</a>
					</div>
				</form>		
			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
		$("#submenu").select2();	

		//Enviamos el formulario para cargar un nuevo item de menu
		//$("#addMenu").validate();
		$( "#addMenu" ).submit(function(e) {
				$("#addMenu").validate();
				if(!$("#addMenu").isValid){
					e.preventDefault();
                	var dataString = $('#addMenu').serialize();
					$.ajax({
						type: "GET",
						url: "{{route('agregarNuevoMenu')}}",
						dataType: 'json',
						data: dataString,

							error: function(jqXHR,textStatus,errorThrown){

							$.toast({
							heading: 'Error',
							text: 'Ocurrio un error en la comunicación con el servidor.',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
								});

							},
						success: function(rsp){
							if(rsp.estado == 1){
								$.toast({
								heading: 'Agregado con exito.',
								text: 'Se creo con exito el nuevo item de menu',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'success'
								});
								$('#addMenu')[0].reset();
							}else{		
								$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la insercion.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
									});
							}
						
						}
					});	
				}
            });
	</script>
@endsection