
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	  <style type="text/css">
	

		.readOnly {
		 background-color: #eee;
	 border-color: rgb(210, 214, 222);
	 cursor: not-allowed !important;
	}
	
	.form-button-cabecera{
				margin-bottom: 5px !important;
			}	
	
	@-moz-document url-prefix() {
	  fieldset { display: table-cell; }
	}
	
		.bgRed {
			/*background-color: #C48433;*/
			font-size: 15px;
	
		display: inline-block;
		min-width: 10px;
		padding: 3px 7px;
		font-size: 15px;
		font-weight: 800;
		line-height: 1;
		color: #000;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		border-radius: 10px;
		border: 1px solid black;
		/*background: #e2076a*/
		}
	
		.select2-container *:focus {
			outline: none;
		}
	
	
		.correcto_col { 
			height: 74px;
		 }
	
		 #divBanco {
		border: 2px solid #D0D1C2;
		border-radius: 5px;
	
		 }
	
		 .title_bank{
			 border-bottom-right-radius: 5px;
			border-bottom-left-radius: 5px;
			background-color: #D0D1C2;
			padding: 3px 5px 5px 5px;
			font-weight: 800;
			margin-bottom: 2px;
	
		 }
	
	
		 .negrita{
			font-weight: bold;
		} 
		.input-sm{
			padding: 0.25rem 0.5rem;
	    	font-size: 0.875rem;
	    	line-height: 1;
	    	border-radius: 0.21rem;
	    	height: calc(1.375rem + 11px);
	    	font-weight: 700;
		} 
	
	</style>
@endsection
@section('content')





<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Archivo Hechauka Libro de Compras</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

    		<form id="formHechauka">

				<div class="row">
						<div class="col-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Proveedor</label>
									<select class="form-control select2" name="id_proveedor"  id="proveedor" tabindex="1" style="width: 100%;">
										<option value="">Todos</option>
										@foreach($proveedores as $proveedor)
											@php
												$ruc = $proveedor->documento_identidad;
												if($proveedor->dv){
													$ruc = $ruc."-".$proveedor->dv;
												}
											@endphp
											<option value="{{$proveedor->id}}">{{$ruc}} - {{$proveedor->nombre}} {{$proveedor->apellido}} </option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-12 col-sm-3 col-md-3">  
								<div class="form-group">
								   <label>Mes</label>						 
								   <div class="input-group">
									   <select class="form-control select2" name="mes" id="mes" tabindex="1" style="width: 100%;">
											<option value="">Todos</option>
											<option value="01">Enero</option>
											<option value="02">Febrero</option>
											<option value="03">Marzo</option>	
											<option value="04">Abril</option>
											<option value="05">Mayo</option>
											<option value="06">Junio</option>
											<option value="07">Julio</option>	
											<option value="08">Agosto</option>
											<option value="09">Setiembre</option>	
											<option value="10">Octubre</option>
											<option value="11">Noviembre</option>
											<option value="12">Diciembre</option>
										</select>
								   </div>
							   </div>	
						   </div> 

							<div class="col-12 col-sm-3 col-md-3">  
								<div class="form-group">
								   <label>Año</label>						 
								   <div class="input-group">
									  	<select class="form-control select2" name="anho" id="anho" tabindex="1" style="width: 100%;">
											<option value="">Todos</option>
											<?php
												$fecha = date('Y');
												for ($i = 2008; $i <= $fecha; $i++) {
												    echo '<option value="'.$i.'">'.$i.'</option>';
												}
											?>
										</select>
								   </div>
							   </div>	
						   </div> 

				 		<div class="col-12" >
				 			<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg" style=" margin: 0 0 10px 10px;"><b>Excel</b></button>
				 			<button type="button" id="botonTxt" class="pull-right text-center btn btn-success btn-lg mr-1" style=" margin: 0px;"><b>Descargar</b></button>
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
				 				<button  onclick ="loadDatatable()" class="pull-right btn btn-info btn-lg mr-2" type="button"><b>Buscar</b></button>
  						</div>
					</div>
				</form>  
				<br>
				<span style="text-transform: uppercase;font-weight: 550;letter-spacing: 0.05rem;font-size: 1.9rem;">Cabecera</span>
				<table id="listadoCabecera" class="table table-hover table-condensed nowrap text-left" style="width: 100%;">
					<tr>
						<th>Periodo:</th>
					  	<th><input type="text" class ="Requerido form-control input-sm text-bold" id="periodo" value="" disabled="disabled"/></th>
						<th>Tipo Reporte:</th>
						<th><input type="text" class ="Requerido form-control input-sm text-bold" id="tipo_reporte" value="" disabled="disabled"/></th>
					  	<th>Cod. Obligación</th>
					  	<th><input type="text" class ="Requerido form-control input-sm text-bold" id="cod_obligacion" value="" disabled="disabled"/></th>
					  	<th>Cod. Formulario</th>
						<th><input type="text" class ="Requerido form-control input-sm text-bold" id="cod_formulario" value="" disabled="disabled"/></th>
					</tr>
					<tr>
					  	<th>Ruc Agente</th>
					  	<th><input type="text" class ="Requerido form-control input-sm text-bold" id="ruc_formulario" value="" disabled="disabled"/></th>
						<th>Dv Agente</th>
						<th><input type="text" class ="Requerido form-control input-sm text-bold" id="dv_agente" value="" disabled="disabled"/></th>
					  	<th>Nombre Agente</th>
					  	<th colspan="3"><input type="text" class ="Requerido form-control input-sm text-bold" id="nombre_agente" value="" disabled="disabled"/></th>
					</tr>
					<tr>
						<th>Ruc Representante</th>
						<th><input type="text" class ="Requerido form-control input-sm text-bold" id="ruc_representante" value="" disabled="disabled"/></th>
					  	<th>Dv Represente</th>
					  	<th><input type="text" class ="Requerido form-control input-sm text-bold" id="dv_representante" value="" disabled="disabled"/></th>
						<th>Nombre Apellido</th>
						<th colspan="3"><input type="text" class ="Requerido form-control input-sm text-bold" id="nombre_apellido" value="" disabled="disabled"/></th>
					  	
					</tr>
					<tr>
						<th>Cant. Registros</th>
					  	<th><input type="text" class ="Requerido form-control input-sm text-bold" id="cant_registros" value="" disabled="disabled"/></th>
						<th>Monto Reportado</th>
						<th><input type="text" class ="Requerido form-control input-sm text-bold" id="monto_reportado" value="" disabled="disabled"/></th>
					  	<th>Exportador</th>
					  	<th><input type="text" class ="Requerido form-control input-sm text-bold" id="exportador" value="" disabled="disabled"/></th>
					</tr>
				</table>
				<br>
 				<span style="text-transform: uppercase;font-weight: 550;letter-spacing: 0.05rem;font-size: 1.9rem;">Detalles</span>
            	<div class="table-responsive">
	              <table id="listado" class="table table-hover table-condensed nowrap text-center" style="width: 100%;">
	                <thead>
					  <tr>
						<th>tipo_registro</th>
						<th>ruc_proveedor</th>
					  	<th>dv_proveedor</th>
					  	<th>nombre_proveedor</th>
					  	<th>timbrado_proveedor</th>
					  	<th>tipo_documento</th>
						<th>nro_documento</th>
						<th>fecha_documento</th>
					  	<th>monto_compra_10</th>
					  	<th>iva_credito_10</th>
					  	<th>monto_compra_5</th>
					  	<th>iva_credito_5</th>	
						<th>monto_compra_exenta</th>
						<th>tipo_operacion</th>
					  	<th>condicion_compra</th>
					  	<th>cuotas</th>
					  </tr>																												
	                </thead>
	                <tbody>
	     
			         
			        </tbody>
	              </table>
	            </div>



			</div>
		</div>
	</div>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	
<script type="text/javascript">

$(document).ready(function () {
	$('.select2').select2();
	mes = "{{date('Y')}}";
	anho = "{{date('m')}}";
	/*$('#mes').val('07').trigger('change.select2');*/
	$('#anho').val('2020').trigger('change.select2');

	//loadDatatable();
	calendar();
});





/*==============================================
		FILTROS BUSQUEDA
============================================== */
function calendar() {

	//CALENDARIO OPCIONES EN ESPAÑOL
	
	let locale = {
		format: 'DD/MM/YYYY',
		cancelLabel: 'Limpiar',
		applyLabel: 'Aplicar',
		fromLabel: 'Desde',
		toLabel: 'Hasta',
		customRangeLabel: 'Seleccionar rango',
		daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
			'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
			'Diciembre'
		]
	};


	$('.calendar').daterangepicker({
		timePicker24Hour: true,
		timePickerIncrement: 30,
		locale: locale
	}).on('cancel.daterangepicker', function (ev, picker) {
		$(this).val('');
	});
	$('.calendar').val('');


}





/*==============================================
		DATATABLE
============================================== */

	var table;

	function loadDatatable() {
		if($('#mes').val() != "" && $('#anho').val() != "" ){
			$.blockUI({
				centerY: 0,
				message: "<h2>Procesando...</h2>",
				css: {
					color: '#000'
				}
			});

			// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
			setTimeout(function () {
			
							$.ajax({
						type: "GET",
						url: "{{route('cabecerahechaukaLc')}}", 
						dataType: 'json',
						data: {
								dataProveedor: $('#proveedor').val(),
								dataMes: $('#mes').val(),
								dataAnho: $('#anho').val(),
							},
								success: function(rsp){
											console.log(rsp);
											$('#periodo').val($('#anho').val()+""+$('#mes').val()); 
											$('#tipo_reporte').val(rsp[0].tipo_reporte);
											$('#cod_obligacion').val(rsp[0].cod_obligacion);
											$('#cod_formulario').val(rsp[0].cod_formulario);
											$('#ruc_formulario').val(rsp[0].ruc_agente);
											$('#dv_agente').val(rsp[0].dv_agente);
											$('#nombre_agente').val(rsp[0].nombre_agente);
											$('#ruc_representante').val(rsp[0].ruc_representante);
											$('#dv_representante').val(rsp[0].dv_representante);
											$('#nombre_apellido').val(rsp[0].representante_legal);
											$('#cant_registros').val(rsp[0].cant_compras);
											$('#monto_reportado').val(parseInt(rsp[0].monto_reportado));
											$('#exportador').val(rsp[0].exportador);

							}
					})	
			
			
				//SE FILTRA Y QUITA ESPACIOS EN BLANCO
				let form = $('#formHechauka').serializeJSON({
						customTypes: customTypesSerializeJSON
					});
				let input;
				
				table = $("#listado").DataTable({
					destroy: true,
					keys: true,	
					ajax: {
						url: "{{route('detallehechaukaLc')}}",
						data: form,
						error: function (jqXHR, textStatus, errorThrown) {

							$.toast({
		                            heading: 'Error',
		                            text: 'Ocurrio un error en la comunicación con el servidor.',
		                            position: 'top-right',
		                            showHideTransition: 'fade',
		                            icon: 'error'
		                        });

							$.unblockUI();
						}

					},
					"columns": [/*{
							data: function (x) {
								return `<a href="{{route('controlOp',['id'=>''])}}/${x.id}"  class="bgRed">
								        	<i class="fa fa-fw fa-search"></i>${x.id}</a>
								        	<div style="display:none;">${x.id}</div>
								        	`;
							}
						},*/
						{
							data: "tipo_registro"
						},	
						{
							data: "ruc_proveedor"
						},
						{
							data: "dv_proveedor"
						},
						{
							data: "proveedor"
						},
						{
							data: "nro_timbrado"
						},
						{
							data: "tipo_documento"
						},	
						{
							data: "nro_documento"
						},
						{
							data: "fecha_documento"
						},
						{ "data": function(x)
								{
									return parseInt(x.monto_compra_10);
								}
						},
						{ "data": function(x)
								{
									return parseInt(x.iva_credito_10);
								}
						},	
						{ "data": function(x)
								{
									return parseInt(x.monto_compra_5);
								}
						},	
						{ "data": function(x)
								{
									return parseInt(x.iva_credito_5);
								}
						},			
						{ "data": function(x)
								{
									return parseInt(x.monto_compra_exenta);
								}
						},
						{
							data: "condicion_compra"
						},
						{
							data: "condicion_compra"
						},
						{
							data: "cant_cuota"
						},
						
					],
					"order": [[ 0, "desc" ]],

					"createdRow": function ( row, data, index ) {
							$(row).addClass('negrita');
										 
					}	

				}).on('xhr.dt', function (e, settings, json, xhr) {
					//Ajax event - fired when an Ajax request is completed.;
					$.unblockUI();
				}).on('draw', function () {
			       		/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

					    $('#listado tbody tr .numeric').inputmask("numeric", {
							radixPoint: ",",
							groupSeparator: ".",
							digits: 2,
							autoGroup: true,
							 // prefix: '$', //No Space, this will truncate the first character
							rightAlign: false
						});

		    	});

				}, 300);
			}else{
					$.toast({
			                heading: 'Error',
			                text: 'Por favor Ingrese el mes y el año para la busqueda',
			                position: 'top-right',
			                            showHideTransition: 'fade',
			                            icon: 'error'
			                        });
				}
		} 
		$("#botonTxt").on("click", function(e){ 
				if($('#mes').val() != "" && $('#anho').val() != "" ){
	                e.preventDefault();
	                $('#formHechauka').attr('method','post');
	               	$('#formHechauka').attr('action', "{{route('getDownloadLc')}}").submit();
	            }else{
					$.toast({
			                heading: 'Error',
			                text: 'Por favor Ingrese el mes y el año para generar el archivo',
			                position: 'top-right',
			                            showHideTransition: 'fade',
			                            icon: 'error'
			                        });
				}
            });
		function limpiar()
		{
			$('#proveedor').val('').trigger('change.select2');
			$('#mes').val('').trigger('change.select2');
			$('#anho').val('').trigger('change.select2');

		}

		$("#botonExcel").on("click", function(e){ 
            e.preventDefault();
            $('#formHechauka').attr('method','post');
            $('#formHechauka').attr('action', "{{route('generarExcelHechaukaLc')}}").submit();
        });


</script>

@endsection