
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
	</style>
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Cuentas de Gastos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmProforma" method="get">
					<div class="row">
						<div class="col-12">
							<a href="{{ route('agregarCuentasGasto') }}" class="btn btn-success pull-right" role="button">
								<div class="fonticon-wrap style-icon">
									<i class="ft-plus-circle"></i>
								</div>
							</a>
						</div>
						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Cuenta Gasto Padre</label>
								<select class="form-control select2" name="id_cuenta_padre" id="id_cuenta_padre" style="width: 100%;">
										<option value="">Seleccione Cuenta Gasto</option>	
											@foreach($cuentasCostos as $key=>$cuentasCosto)
											@php 	
											if(isset($cuentasCosto->cuentasPadre->denominacion)){
												$planCuentas = ' ('.$cuentasCosto->cuentasPadre->denominacion.')';
											 }else{
												$planCuentas = '';
											 }
											@endphp
											<option value="{{$cuentasCosto->id}}">{{$cuentasCosto->denominacion}} {{$planCuentas}}</option>
										@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-5 col-md-5" style="padding-left: 20px;">
							<label class="control-label">Cuenta de Gasto</label>
							<input type="text" name="denominacion" class="Requerido form-control" value=""id="denominacion" required="required" />
						</div>

						<div class="col-11 mb-1">
							<button type="button" onclick="buscarCotizacion()" id="btnGuardar" class="btn btn-info btn-lg pull-right mb-1">Buscar</button>
						</div>
					</div>
				</form>


				<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr style="text-align: center;">
								<th>Denominacion</th>
								<th>Cuenta Padre</th>
								<th>Fecha Alta </th>
								<th>Usuario Alta</th>
							</tr>
						</thead>
						<tbody id="lista_cotizacion" style="text-align: center">
						</tbody>
					</table>
				</div>




			</div>
		</div>
	</section>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>
		//$(document).ready(function() {

			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});


		function buscarCotizacion() {


			$("#frmProforma").validate({
			  submitHandler: function(form) {
			    alert('Enviado');
			  }
			});

			var dataString = $('#frmProforma').serialize();

			$.ajax({
						type: "GET",
						url: "{{route('consultaCuentaGasto')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },
						success: function(rsp){


								var oSettings = $('#listado').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();
								for (i=0;i<=iTotalRecords;i++) {
									$('#listado').dataTable().fnDeleteRow(0,null,true);
								}

								$.each(rsp, function (key, item){
								console.log(item);
								//formatear fecha	
								var fecha = item.fecha_alta;
								var fechaIntermedia = fecha.split(' ');
								var fechaFinal = fechaIntermedia[0].split('-');
								fechaMostrar = fechaFinal[2]+'/'+fechaFinal[1]+'/'+fechaFinal[0]+' '+fechaIntermedia[1];
								if(jQuery.isEmptyObject(item.cuentas_padre) == false){
									cuentaPadre = item.cuentas_padre.denominacion;
								}else{
									cuentaPadre = '';
								}
								if(jQuery.isEmptyObject(item.usuario) == false){
									usuario = item.usuario.nombre;
								}else{
									usuario = '';
								}

								var dataTableRow = [
														item.denominacion,
														cuentaPadre,
														fechaMostrar,
														usuario

													];
								var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

							});

					


						}//cierreFunc	
				});	
			
		}
				
			

	</script>
@endsection