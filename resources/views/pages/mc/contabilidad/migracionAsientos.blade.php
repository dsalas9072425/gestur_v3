@extends('masters')
@section('title', 'MIGRACIÓN ASIENTOS')
@section('styles')
	@parent
<style type="text/css">
	.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    cursor: pointer;
	}	
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#botonExcel {
		display: inline;
	}
	#botonExcel .dt-buttons {
		display: inline;
	}






	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}



	.correcto_col {
        height: 74px;
    }
    

</style>

@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">MIGRACIÓN DE ASIENTOS</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
		
		<div class="card-body">
			<form id="consultaMigracion" autocomplete="off">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-3">
	                        <div class="form-group">
	                            <label>Origen Asiento</label>
	                            <select class="form-control select2" name="id_origen_asiento" id="id_origen_asiento" data-value-type="number" tabindex="1"  style="width: 100%;">

	                                <option value="">Seleccione una opción</option>
	                                @foreach ($origenAsientos as $origenAsiento)
		                                <option value="{{$origenAsiento->id}}">{{$origenAsiento->abreviatura}} - {{$origenAsiento->descripcion}}
		                                </option>
	                                @endforeach
	                            </select>
	                        </div>
                   		 </div>
                    
						<div class="col-12 col-sm-6 col-md-3">  
		 					<div class="form-group">
					            <label>Fecha Desde - Hasta</label>						 
								<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class="form-control pull-right fecha" name="desde_hasta" tabindex="1" id="desde_hasta" value="">
								</div>
							</div>	
						</div>

						<div class="col-12 col-sm-6 col-md-3">
	                        <div class="form-group">
	                            <label>Tipo</label>
	                            <select class="form-control select2" name="tipo_doc" id="tipo_doc" data-value-type="number" tabindex="1"  style="width: 100%;">
		                            <option value="1">Resumen</option>
		                            <option value="2">Detalle</option>
	                            </select>
	                        </div>
                   		 </div>

                   		 <div class="col-12 col-sm-6 col-md-3">
	                        <div class="form-group">
	                            <label>Anulados</label>
	                            <select class="form-control select2" name="asiento_anulado" id="asiento_anulado" data-value-type="number" tabindex="1"  style="width: 100%;">
		                            <option value="1">SÍ</option>
		                            <option value="0" selected="selected">NO</option>
	                            </select>
	                        </div>
                   		 </div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-6 col-md-3">
	                        <div class="form-group">
	                            <label>Nro Ultimo Asiento</label>
								<input type="text" class="form-control" id="ultimo_asiento"  name="ultimo_asiento" value="">
	                        </div>
                   		 </div>
						<div class="col-9">
							<br>
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style=" margin: 0px;"><b>Excel</b></button>
					  		<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
							<button  onclick ="consultarMigracionAsiento()" class="pull-right text-center btn btn-info btn-lg mr-1"  type="button"><b>Buscar</b></button>

		  				</div>
					</div>
					

            	<div class="table-responsive mt-1">
	              <table id="listado" class="table table-striped table-bordered file-export" style="width: 100%">
	                <thead style="text-align: center">
						<tr>
							<th>Cuenta Contable</th>
							<th>Origen Asiento</th>
							<th>Debe</th>
							<th>Haber</th>
							<th>Moneda</th>
							<th>Fecha</th>
							<th>Sucursal</th>
							<th>Centro Costo</th>
							<th>Usuario</th>
			            </tr>
	                </thead>
	                <tbody style="text-align: center">
	                </tbody>
	              </table>
				</div>  


			</form>	
			</div>
		</div>
	</div>
</section>
   

    

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
			<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
		<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	<script>
	const operaciones = {id_row:0, id_fila:0, tipoGuardar:true}
	const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });
	var table;
	$(document).ready(function() {
			// table = mostrarLibroCompra();
		});



	
	$(function() 
	{
		let locale = {
					format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};

	  $('input[name="desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: locale
	  });

	  $('input[name="desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });
  });
		$('.select2').select2();
		
		
		function limpiar()
		{
			$('#sucursal').val('').trigger('change.select2');
			$('#desde_hasta').val('');
		}

		function validar(data)
		{
			if(data === undefined || data === null || data === '')
				return false;

			return true;
		}

		const formatter = new Intl.NumberFormat('de-DE', 
		{
            currency: 'USD',
            minimumFractionDigits: 2
        });

        function formatearFecha(texto)
 			{
	            if(texto != '' && texto != null)
	            {
	          		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	            } 
	            else 
	            {
	         		return '';    
            	}
        	}

		function consultarMigracionAsiento()
		{
			if($('#ultimo_asiento').val() != ""){
				 table = $("#listado").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('getConsultaMigracionAsiento')}}",
						"type": "GET",
						"data": {"formSearch": $('#consultaMigracion').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					"columns": 
					[
						
						{ "data": "cuenta_contable" },
						{ "data": "origen_asiento" },
						{ "data": function(x)
							{
								var debe = '';

								if (x.debe != null) 
								{
									debe = formatter.format(parseFloat(x.debe));
								}
								return debe;
							}
						},

						{ "data": function(x)
							{
								var haber = '';

								if (x.haber != null) 
								{
									haber = formatter.format(parseFloat(x.haber));
								}
								return haber;
							}
						},

						{ "data": "moneda" },

						{ "data": function(x)
							{
								var fecha = '';

								if (x.fecha_asiento != null) 
								{
									fecha = x.fecha_asiento.split(' ');
									fecha = formatearFecha(fecha[0]);
								}

								return fecha;
							} 
						},
						{ "data": "sucursal" },
						{ "data": "centro_costo" },
						{ "data": "usuario" },


					], 
					"initComplete": function (settings, json) {
			 		// $.unblockUI();
					 $('[data-toggle="tooltip"]').tooltip();
					 
			 	}
					
				});	
				
				return table;
			}else{
				$.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Debe ingresar el último asiento cargado en su sistema contable.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

			}	
		}	

		$("#botonExcel").on("click", function(e)
		{ 
			if($('#ultimo_asiento').val() != ""){
				e.preventDefault();
				$('#consultaMigracion').attr('method','post');
				$('#consultaMigracion').attr('action', "{{route('generarExcelMigracionAsiento')}}").submit();
			}else{
					$.toast
								({
									heading: 'Error',
									text: 'Debe ingresar el último asiento cargado en su sistema contable.',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
								});

				}	
		});


	
		$(document).ready(function () {
			var dtable = $('#listado').DataTable();
			$('.filter-button').on('click', function () {
		        //clear global search valuess
		        dtable.search('');
		        $('.filter').each(function(){ 
		        if(this.value.length){
		          dtable.column($(this).data('columnIndex')).search(this.value);
		        }
		        });
		        dtable.draw();
		    });
		    
		    $( ".dataTables_filter input" ).on( 'keyup change',function() {
		        dtable.columns().search('');
		       //clear input values
		       $('.filter').val('');
			});	
		})    


	</script>
@endsection
