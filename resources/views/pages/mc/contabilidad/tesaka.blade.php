@extends('masters')
@section('title', 'TESAKA')
@section('styles')
<style type="text/css">
	.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    cursor: pointer;
	}	
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#botonExcel {
		display: inline;
	}
	#botonExcel .dt-buttons {
		display: inline;
	}






	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}



	.correcto_col {
        height: 74px;
    }
    

</style>
	@parent
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Tesaka</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
		
		<div class="card-body">
			<form id="consultaTesaka" autocomplete="off">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-3">
	                        <div class="form-group">
	                            <label>Sucursal</label>
	                            <select class="form-control select2" name="sucursal" id="sucursal" data-value-type="number" tabindex="2"  style="width: 100%;">

	                                <option value="">Seleccione una opción</option>
	                                @foreach ($sucursalEmpresa as $sucursal)
		                                <option value="{{$sucursal->id}}">{{$sucursal->denominacion}}
		                                </option>
	                                @endforeach


	                            </select>
	                        </div>
                   		 </div>
                    
						<div class="col-12 col-sm-6 col-md-3">  
		 					<div class="form-group">
					            <label>Fecha Desde - Hasta</label>						 
								<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class="form-control pull-right fecha" name="desde_hasta" tabindex="1" id="desde_hasta" value="">
								</div>
							</div>	
						</div> 	

						<div class="col-12">
							<button type="button" id="botonTxt" class="pull-right text-center btn btn-success btn-lg mr-1" style=" margin: 0px;"><b>Descargar</b></button>
					  		<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
							<button  onclick ="consultarTesaka()" class="pull-right text-center btn btn-info btn-lg mr-1"  type="button"><b>Buscar</b></button>

		  				</div>
					</div>
					

            	<div class="table-responsive mt-1">
	              <table id="listado" class="table table-striped table-bordered file-export" style="width: 100%">
	                <thead style="text-align: center">
						<tr>
							<th></th>
							<th>Fecha</th>
							<th>Ruc</th>
							<th>Ret. Iva</th>
							<th>Ret. Renta</th>
							<th>Ret. Porcentaje</th>
							<th>Transacción N° Comprobante</th>
			            </tr>
	                </thead>
	                <tbody style="text-align: center">
	                </tbody>
	              </table>
				</div>  


			</form>	
			</div>
		</div>
	</div>
</section>
   

    

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
			<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
		<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	<script>
	const operaciones = {id_row:0, id_fila:0, tipoGuardar:true}
	const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });
	var table;
	$(document).ready(function() {
			// table = mostrarLibroCompra();
		});



	
	$(function() 
	{
		let locale = {
					format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};

	  $('input[name="desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: locale
	  });

	  $('input[name="desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });
  });
		$('.select2').select2();
		
		
		function limpiar()
		{
			$('#sucursal').val('').trigger('change.select2');
			$('#desde_hasta').val('');
			consultarTesaka();
		}

		function validar(data)
		{
			if(data === undefined || data === null || data === '')
				return false;

			return true;
		}

		const formatter = new Intl.NumberFormat('de-DE', 
		{
            currency: 'USD',
            minimumFractionDigits: 2
        });

        function formatearFecha(texto)
 			{
	            if(texto != '' && texto != null)
	            {
	          		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	            } 
	            else 
	            {
	         		return '';    
            	}
        	}

		function consultarTesaka()
		{
			 table = $("#listado").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('consultarTesaka')}}",
						"type": "GET",
						"data": {"formSearch": $('#consultaTesaka').serializeArray() },
						// "data": {"formSearch": $('#consultaTesaka').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					"columns": 
					[
						{
							data: function (x) 
							{
								return `<input type="checkbox" class="checkbox_class" id="check_input" name="datos[`+x.id+`]['check_input']">`;
							}
						},
						{ "data": "fecha_retencion" },
						{ "data": "ruc" },
						{ "data": "retencion_iva" },
						{ "data": "retencion_renta" },
						{ "data": "renta_porcentaje" },
						{ "data": "numero_comprobante_venta" }

					], 
					"initComplete": function (settings, json) {
			 		// $.unblockUI();
					 $('[data-toggle="tooltip"]').tooltip();
					 
			 	}
					
				});	
				
				return table;
		}	

		$("#botonTxt").on("click", function(e){ 

			if($('#sucursal').val() == "" && $('#desde_hasta').val() == "" )
			{
	            $.toast({
			                heading: 'Error',
			                text: 'Debe realizar la consulta primero',
			                position: 'top-right',
			                            showHideTransition: 'fade',
			                            icon: 'error'
			                        });
	        }
	        else
	        {
	        	var contador = 0;

	        	$("input[type=checkbox]").each(function(){
					if($(this).is(":checked"))
						contador++;
				});

	        	if (contador == 0) 
	        	{
	        		$.toast({
			                heading: 'Error',
			                text: 'Seleccione al menos un registro para realizar la descarga',
			                position: 'top-right',
			                            showHideTransition: 'fade',
			                            icon: 'error'
			                        });
	        	}
	        	else
	        	{
		        	e.preventDefault();
		            $('#consultaTesaka').attr('method','post');
		            $('#consultaTesaka').attr('action', "{{route('getDownloadTesaka')}}").submit();
	        	}
					
			}
        });
	

	
		$(document).ready(function () {
			var dtable = $('#listado').DataTable();
			$('.filter-button').on('click', function () {
		        //clear global search valuess
		        dtable.search('');
		        $('.filter').each(function(){ 
		        if(this.value.length){
		          dtable.column($(this).data('columnIndex')).search(this.value);
		        }
		        });
		        dtable.draw();
		    });
		    
		    $( ".dataTables_filter input" ).on( 'keyup change',function() {
		        dtable.columns().search('');
		       //clear input values
		       $('.filter').val('');
			});	
		})    


	</script>
@endsection
