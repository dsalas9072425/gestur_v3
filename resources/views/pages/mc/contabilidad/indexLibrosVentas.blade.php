@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
	.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    cursor: pointer;
	}	
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#botonExcel {
		display: inline;
	}
	#botonExcel .dt-buttons {
		display: inline;
	}


/*sdsdds*/
.checkbox label:after {
  content: '';
  display: table;
  clear: both;
}

.checkbox .cr {
  position: relative;
  display: inline-block;
  border: 1px solid #a9a9a9;
  border-radius: .25em;
  width: 1.3em;
  height: 1.3em;
  float: left;
  margin-right: .5em;
}

.checkbox .cr .cr-icon {
  position: absolute;
  font-size: .8em;
  line-height: 0;
  top: 50%;
  left: 15%;
}

.checkbox label input[type="checkbox"] {
  display: none;
}

.checkbox label input[type="checkbox"]+.cr>.cr-icon {
  opacity: 0;
}

.checkbox label input[type="checkbox"]:checked+.cr>.cr-icon {
  opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled+.cr {
  opacity: .5;
}

	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}

	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

	.rojo{
		color: red;
		font-weight: 700;
	}

	.verde{
		color: green;
		font-weight: 700;
	}

</style>
	@parent
@endsection
@section('content')

<section>

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Libros Ventas</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">
				<button id="btnAddLibroVenta" title="Generar Libro Venta" class="btn btn-success pull-right" type="button">
					<div class="fonticon-wrap">
						<i class="ft-plus-circle"></i>
					</div>
				</button>
			</div>
			<div class="card-body">
				<form id="consultaLibroVenta" method="get">
					<div class="row">
						<div class="col-12 col-sm-3 col-md-4">
					        <div class="form-group">
					            <label>Cliente</label>
								<select class="form-control select2" name="idProveedor"  id="proveedor" tabindex="1" style="width: 100%;">
									<option value="0">Seleccione Cliente</option>
							
									@foreach($clientes as $cliente)
										@php
											$ruc = $cliente->documento_identidad;
											if($cliente->dv){
												$ruc = $ruc."-".$cliente->dv;
											}
										@endphp
										<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->nombre}} {{$cliente->apellido}} - {{$cliente->id}}</option>
									@endforeach
								
								</select>
					        </div>
			            </div>

			            <div class="col-12 col-sm-3 col-md-4">
						 	<div class="form-group">
						 	 	<label>Nro. Documento</label>
						     	<input type="text" class="form-control" id="nroFactura"  name="nroFactura" value="">
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-4">  
		 					<div class="form-group">
					            <label>Fecha Factura Desde - Hasta</label>						 
								<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class="form-control pull-right fecha" name="desde_hasta" tabindex="1" id="desde_hasta" value="">
								</div>
							</div>	
						</div>
						<div class="col-12" >
								<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
					  			<button type="button" onclick="limpiar()" style="margin-right: 10px;" id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white" mr-1><b>Limpiar</b></button>
								<button  onclick ="mostrarLibroVenta()" class="pull-right btn btn-info btn-lg mr-1" type="button"><b>Buscar</b></a>
						  </div>
						  
					</div>	
				</form>
           
            	<div class="table-responsive mt-1">
	              <table id="listado" class="table nowrap" style="width: 100%">
	                <thead style="text-align: center">
						<tr>
							<th>Asiento</th>
							<th>LV</th>
							<th>Fecha</th>
	                        <th>Dia</th>
							<th>Documento</th>
							<th>Tickets del </th>
							<th>Tickets al</th>
	                        <th>Cliente</th>
	                        <th>Ruc</th>
	                        <th>Grav10</th>
	                        <th>Grav5</th>
	                        <th>Iva10</th>
	                        <th>Iva5</th>
	                        <th>Exenta</th>
	                        <th>Total</th>
	                        <th>Retencion</th>
							<th>Saldo</th>
			            </tr>
	                </thead>
	                
	                <tbody style="text-align: center">
						
	                </tbody>
	              </table>
				</div>  
   
			</div>
		</div>
	</div>
</section>
    <div id="requestVerAsiento" class="modal fade" role="dialog">
	  		<div class="modal-dialog modal-lg" style="margin-left: 4%;margin-top: 10%;">
				<div class="modal-content" style="width: 160%;">
					<div class="modal-header" style=" align-content: center;">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Asiento Contable<i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_c" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">

								<div class="row">
									<div class="col-12">
										<div class="table-responsive table-bordered">
											<table id="listadoAsientoDetalle" class="table" style="width:100%">
												<thead>
													<tr>
														<th>Cuenta</th>
														<th>Nombre</th>
														<th>Sucursal</th>
														<th>C.C</th>
														<th>Debe</th>
														<th>Haber</th>
														<th>Cotización</th>
														<th>Importe Original</th>
														<th></th>
													</tr>
												</thead>
												<tbody style="text-align: left; font-weight: 800;">
												</tbody>
												{{-- style="background-color:#E4F1FA;" --}}
												<tfoot>
													<tr>
														<th colspan="2" style="text-align:right;"> DIFERENCIA</th>
														<th id="diferencia_footer"></th>
														<th> TOTAL</th>
														<th id="total_debe"></th>
														<th id="total_haber"></th>
														<th colspan="3"></th>

													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								
									<div class="col-12">
										<div class="form-group">
											<label>CONCEPTO</label>
											<input type="text" autocomplete="false" id="concepto_datatable" class="form-control date-picker"
												value="" autocomplete="off" readonly />
										</div>
									</div>
								</div>
				    </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
		<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>

	<script>
	const operaciones = {id_row:0, id_fila:0, tipoGuardar:true}
	const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });
	var table;

	$(function() 
		{
			let locale = {
						format: 'DD/MM/YYYY',
					cancelLabel: 'Limpiar',
					applyLabel: 'Aplicar',					
						fromLabel: 'Desde',
						toLabel: 'Hasta',
						customRangeLabel: 'Seleccionar rango',
						daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
						monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											       'Diciembre']
					};

		  $('input[name="desde_hasta"]').daterangepicker({
		      autoUpdateInput: false,
		      locale: locale
		  });

		  $('input[name="desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
		      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
		  });

		  $('input[name="desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
		      $(this).val('');
		  });
	  });


	$(document).ready(function() 
		{
			// table = mostrarLibroVenta();
		});

		//MANEJAR DATOS OP PROCESADAS
		const data = 
		{
			idMoneda : '',
			idProveedor : '',
			items : [],
			setMoneda: function(d){this.idMoneda = d;},
			getMoneda: function(){return this.idMoneda;},
			setProveedor: function(d){this.idProveedor = d;},
			clientes: function(){return this.idProveedor;},
			clearData:function(){this.idMoneda = '';
								 this.idProveedor = '';},
			setItems: function(d){this.items = d;},
			getItems: function(){return this.items;}					 

		};
	
	$(function() 
	{

	  $('input[name="proveedor_desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: {
	          cancelLabel: 'Limpiar'
	      }
	  });

	  $('input[name="proveedor_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="proveedor_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });


	  $('input[name="checkin_desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: {
	          cancelLabel: 'Limpiar'
	      }
	  });

	  $('input[name="checkin_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="checkin_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });

  });
		$('.select2').select2();
	var tablaDetalle =  $('#listadoAsientoDetalle').dataTable();
		
		function limpiar()
		{
			$('#nroFactura').val('');
			$('#proveedor').val('0').trigger('change.select2');
			mostrarLibroVenta();

		}

		function validar(data)
		{

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}

		const formatter = new Intl.NumberFormat('de-DE', 
		{
            currency: 'USD',
            minimumFractionDigits: 2
        });

        function formatearFecha(texto)
 			{
	            if(texto != '' && texto != null)
	            {
	          		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	            } 
	            else 
	            {
	         		return '';    
            	}
        	}

		function mostrarLibroVenta()
		{
			// console.log('mostrarLibroCompra');
			 table = $("#listado").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('mostrarLibroVenta')}}",
						"type": "GET",
						"data": {"formSearch": $('#consultaLibroVenta').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					"aaSorting":[[2,"asc"], [4,'asc']],
					"columns": 
					[
						{ "data": function(x)
							{
								
								btn = `<a onclick="verAsiento(${x.id_asiento})" class="bgRed"><i class="fa fa-fw fa-search"></i>${x.id_asiento}</a>`;
								
								return btn;
							} 
						},
						{ "data": function(x)
							{	
								btn = `<a href="{{route('verLibroVenta',['id'=>''])}}/${x.id}" class="bgRed"><i class="fa fa-fw fa-search"></i>${x.id}</a>`;
								return btn;
							} 
						},
						{ "data": "fecha_documento" },

						{ "data": function(x)
							{
								day = '';
								fecha = x.fecha_hora.split(' ');
								console.log(fecha);

								d = fecha[0].split('-');
								day = d[2];
								return day;
							}
						},

						

						{ "data": "nro_documento" }, 


						{ "data": function(x)
							{
								var total = 0;
							
								//total = formatter.format(parseFloat(x.importe));
							
								return total;
							}
						},
						{ "data": function(x)
							{
								var total = 0;
							
								//total = formatter.format(parseFloat(x.importe));
							
								return total;
							}
						},
						{ "data": "cliente" },
						{ "data": "documento_identidad" },
						{ "data": function(x)
							{
								var total = 0;
							
								total = formatter.format(parseFloat(x.total_gravadas));
							
								return total;
							}
						},
						{ "data": function(x)
							{
								var total = 0;
							
								//total = formatter.format(parseFloat(x.total_iva));
							
								return total;
							}
						},
						{ "data": function(x)
							{
								var total = 0;
							
								total = formatter.format(parseFloat(x.total_iva));
							
								return total;
							}
						},
						{ "data": function(x)
							{
								var total = 0;
							
								//total = formatter.format(parseFloat(x.total_iva));
							
								return total;
							}
						},
						{ "data": function(x)
							{
								var total = 0;
							
								total = formatter.format(parseFloat(x.total_exentas));
							
								return total;
							}
						},
						{ "data": function(x)
							{
								var total = 0;
							
								total = formatter.format(parseFloat(x.importe));
							
								return total;
							}
						},
						{ "data": function(x)
							{
								var total = 0;
								if(jQuery.isEmptyObject(x.monto_retencion) == false){
									total = formatter.format(parseFloat(x.monto_retencion));
								}
							
								return total;
							}
						},
						{ "data": function(x)
							{
								var total = 0;
								if(jQuery.isEmptyObject(x.saldo) == false){
									total = formatter.format(parseFloat(x.saldo));
								}
							
								return total;
							}
						},

					], 
				});	
			
		}	


		function verAsiento(idLibroVenta)
	{
		/*$("#requestVerAsiento").modal("show");
		let dataString = {idLibroVenta:idLibroVenta};

		 $("#tableAsiento").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('verAsiento')}}",
						"data": dataString,
						"type": "GET",
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					// "aaSorting":[[2,"asc"]],
					"columns": 
				[
		
						{"data": function(x){
								console.log(x);
								return  x.cuenta_contable.cod_txt;
							} 
						},	

						{"data": function(x){
								console.log(x);
								return  x.cuenta_contable.descripcion;
							} 
						},

						{ "data": "moneda.currency_code"},

						{ "data": function(x)
							{
								console.log(x);
								var debe = 0;

								debe = formatter.format(parseFloat(x.debe));
								
								return debe;
							}
						},

						{ "data": function(x)
							{
								var haber = 0;

								haber = formatter.format(parseFloat(x.haber));
								
								return haber;
							}
						},

						{ "data": "cotizacion" },

						{ "data": "monto_original"},

						{ "data": function(x)
							{
								var fecha = '';

								if (x.asiento.fecha_hora != null) 
								{
									fecha = x.asiento.fecha_hora.split(' ');
								}

								return formatearFecha(fecha[0])+' '+fecha[1];
							} 
						},

						{ "data": function(x)
							{
								
								if (Boolean(x.asiento.usuario.apellido)) 
								{
									return x.asiento.usuario.nombre+" "+' ';
								}
								else
								{
									return x.asiento.usuario.nombre+" "+x.asiento.usuario.apellido;
								}
								
							} 
						},

					], 
				});	*/

/////////////////////////////////////////////////////////////////////////////////////////////////////
			$("#requestVerAsiento").modal("show");
			let dataString = {id_asiento:idLibroVenta};
			$.ajax({
                type: "GET",
                url: "{{ route('ajaxReporteAsientoDetalle') }}",
                dataType: 'json',
                data: dataString,
				success: function(rsp)
				{
                    oSettings = tablaDetalle.fnSettings();
					
					var iTotalRecords = oSettings.fnRecordsTotal();
					var importe_cotizado = 0;

					for (i=0;i<=iTotalRecords;i++) 
					{
                        $('#listadoAsientoDetalle').dataTable().fnDeleteRow(0,null,true);
					}

					$('#documento_asiento').val(rsp.data1.nro_documento);
					$('#fecha_asiento').val(rsp.data1.fecha);
					$('#concepto_datatable').val(rsp.data1.concepto);
					$.each(rsp.data, function (key, item)
					{
						// console.log(item.haber);
						if(Boolean(item.debe) & item.debe > 0)
						{
							importe_cotizado = item.debe;
						}
						else
						{
							importe_cotizado = item.haber;
						}

						if(item.sucursal !== null){
							sucursal = item.sucursal
						}else{
							sucursal = '';
						}

						var inputs = `<input type="hidden" class="cuenta" name="asiento_detalle[][cuenta]" value="${item.id_cuenta_contable}"> 
									  <input type="hidden" class="sucursal" name="asiento_detalle[][sucursal]" value="${item.id_sucursal}">
								      <input type="hidden" class="centro_costo" name="asiento_detalle[][centro_costo]" value="${(Boolean(item.id_centro_costo))?item.id_centro_costo:0}">
									  <input type="hidden" class="debe" name="asiento_detalle[][debe]" value="${item.debe}">
									  <input type="hidden" class="haber" name="asiento_detalle[][haber]" value="${item.haber}">
									  <input type="hidden" class="cotizacion" name="asiento_detalle[][cotizacion]" value="${item.cotizacion}">
									  <input type="hidden" class="importe_original" name="asiento_detalle[][importe_original]" value="${item.monto_original}">
									  <input type="hidden" class="importe_cotizado" name="asiento_detalle[][importe_cotizado]" value="${importe_cotizado}">
									  <input type="hidden" class="concepto" name="asiento_detalle[][concepto]" value="${item.concepto_detalle}">
									  <input type="hidden" class="moneda" name="asiento_detalle[][moneda]" value="${item.id_moneda}">`;

                        var dataTableRows = 
						[
							inputs+`<span>${item.num_cuenta}</span>`,
							`<span>${item.cuenta_n}</span>`,
							`<span>${sucursal}</span>`,
							`<span>${item.centro_costo}</span>`,
							`<span>${formatter.format(parseFloat(item.debe))}</span>`,
							`<span>${formatter.format(parseFloat(item.haber))}</span>`,
							`<span>${item.cotizacion}</span>`,
							`<span>${formatter.format(parseFloat(item.monto_original))}</span>`,
							// `<span>${item.concepto_detalle}</span>`,
							`<button onclick="getAsientoDetalle('row'+${item.id})" class='btn btn-info btnEditHidden' style="display:none" type='button'><i class="fa fa-pencil-square-o"></i></button>`
						];

                        var newrow = $('#listadoAsientoDetalle').dataTable().fnAddData(dataTableRows);
                    // set class attribute for the newly added row
						var nTr = $('#listadoAsientoDetalle').dataTable().fnSettings().aoData[newrow[0]].nTr;
				
                        var nTds = $('td', nTr);
                        nTds.addClass('row'+item.id);

						var row = $('#listadoAsientoDetalle').dataTable().fnGetNodes(newrow);
    					$(row).attr('id', 'row'+item.id);
						importe_cotizado = 0;

                    })
                }
            }).done(function(){
                $.unblockUI();
				sumarDebeHaber();
				//levantarBtn();
            });
/////////////////////////////////////////////////////////////////////////////////////////////////////
	}
	$("#botonExcel").on("click", function(e){ 
			// console.log('Inicil');
                e.preventDefault();
                 $('#consultaLibroVenta').attr('method','post');
                 $('#consultaLibroVenta').attr('action', "{{route('generarExcelLibroVenta')}}").submit();
            });

function sumarDebeHaber()
{
	
	var asientoDetalle = $('#formAsientoDetalle').serializeJSON();
	var debe = 0;
	var haber = 0;
	var dif = 0;

	$.each(asientoDetalle.asiento_detalle, (index, value)=>{
		debe += Number(value.debe);
		haber += Number(value.haber);
	})
	dif = debe - haber;

	console.log(debe, haber);
	operaciones.debe = debe;
	operaciones.haber = haber;
	debe = GUARANI(debe,{ formatWithSymbol: false }).format(true);
	haber = GUARANI(haber,{ formatWithSymbol: false }).format(true);
	dif = GUARANI(dif,{ formatWithSymbol: false }).format(true);
	$('#total_debe').html(debe);
	$('#total_haber').html(haber);
	$('#diferencia_footer').html(dif);
}

	

	function botonExcel(column,table)
	{
		// console.log('botoooooooooooooonm');
		$('#botonExcel').html('');
		var buttons = new $.fn.dataTable.Buttons(table, 
		{ 
			buttons: 
			[{	
				title: 'Libro de Compras',
				extend: 'excelHtml5',
				text: '<b>Excel</b>',
				className: 'btnExcel',
				exportOptions: 
				{
					columns: ':visible'
				},
				exportOptions: 
				{
					columns:  column,
					format: 
					{
						//seleccionamos las columnas para dar formato para el excel
						body: function ( data, row, column, node ) 
						{
							if(column == 3)
							{
								if (data != '0') 
								{
									// console.log(data);
									data = data.replace(".","");
									//cambiamos la cpunto
									data = data.replace(",",".");
																			
								}
									var numFinal = parseFloat(data);
									return  numFinal;
							} 
							return data;
						}
					}
				},
				
				//Generamos un nombre con fecha actual
				
				filename: function() 
				{
					var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
					return 'LibroCompras-'+date_edition;
				}
			}]
		}).container().appendTo('#botonExcel'); 
	}
	
		$(document).ready(function () {
			var dtable = $('#listado').DataTable();
			$('.filter-button').on('click', function () {
		        //clear global search valuess
		        dtable.search('');
		        $('.filter').each(function(){ 
		        if(this.value.length){
		          dtable.column($(this).data('columnIndex')).search(this.value);
		        }
		        });
		        dtable.draw();
		    });
		    
		    $( ".dataTables_filter input" ).on( 'keyup change',function() {
		        dtable.columns().search('');
		       //clear input values
		       $('.filter').val('');
			});	
		})    

		$('#btnAddLibroVenta').click(()=>{
			location.href = "{{ route('agregarLibroVenta')}}";
		});

	</script>
@endsection
