


@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
    .correcto_col {
        height: 74px;
    }
    					
    input.form-control:focus ,.select2-container--focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
     .mr-1 {
         margin-right: 5px; 
     }
     .labelError{
         color: #F74343 ;
     }
     input {
         font-weight: 800;
     }
</style>
@parent

@endsection
@section('content')


<section id="base-style">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Factura</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">

                <form class="row" id="formFactura" method="post" autocomplete="off">

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Comercio</label>
                            <?php 
                                if(isset($libroVenta->comercionEmpresa['descripcion'])){
                                    $comercio = $libroVenta->comercionEmpresa['descripcion'];
                                }else{
                                    $comercio = "";
                                }    
                            ?>
                            <input type="text" class="form-control" disabled value="{{$comercio}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Cliente</label>
                            <input type="text" class="form-control" disabled value="{{$libroVenta->cliente['nombre']}} {{$libroVenta->cliente['apellido']}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>RUC</label>
                            <input type="text" class="form-control" id="cliente_ruc" tabindex="3" disabled
                                name="cliente_ruc" value="{{$libroVenta->cliente['documento_identidad']}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Timbrado:</label>
                            <?php 
                                $timbrado = "";
                                if(isset($libroVenta->timbrado['numero'])){
                                    $timbrado = $libroVenta->timbrado['numero'];
                                } else {
                                    $timbrado = $libroVenta->timbrado_manual['nro_timbrado'];
                                }
                            ?>
                            <input type="text" class="form-control" disabled value="{{$timbrado}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Centro Costo</label>
                            <select class="form-control select2" name="id_centro_costo"  tabindex="9" style="width: 100%;" disabled>
                                <option value=""></option>
                                @foreach ($centro as $c)
                                <option value="{{$c->id}}" {{ $c->id == $libroVenta->id_centro_costo ? 'selected' : '' }} >{{$c->nombre}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Sucursal</label>
                            <select class="form-control select2" name="id_sucursal"
                                tabindex="10"  style="width: 100%;" disabled>
                                <option value=""></option>
                                @foreach ($sucursalEmpresa as $sucursal)
                                <option value="{{$sucursal->id}}" {{ $sucursal->id == $libroVenta->id_sucursal ? 'selected' : '' }}>{{$sucursal->denominacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Fecha Factura</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control pull-right single-picker" name="fecha_factura"
                                    disabled tabindex="5" id="fecha_factura"
                                    value="{{date('d/m/Y', strtotime($libroVenta->fecha_hora_documento))}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo de Facturación</label>
                            <input type="text" class="form-control" disabled
                                value="{{$libroVenta->tipoFacturacion['denominacion']}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Comprobante</label>
                            <input type="text" class="form-control" disabled
                                value="{{$libroVenta->tipoDocumento['denominacion']}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Vencimiento</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control single-picker" data-value-type="s_date" disabled
                                    id="vencimiento" tabindex="7" name="vencimiento"
                                    value="{{$libroVenta->vencimiento}}">
                            </div>
                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Nro Comprobante</label>
                            <input type="text" class="form-control" id="" tabindex="8" name="nro_comprobante" disabled
                                value="{{$libroVenta->nro_documento}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Moneda</label>
                            <input type="text" class="form-control" disabled
                                value="{{$libroVenta->currency['currency_code']}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Cotización</label>
                            <input type="text" class="form-control format-number-control" disabled id="lc_cotizacion"
                                data-value-type="convertNumber" tabindex="10" name="cotizacion"
                                value="{{$libroVenta->cotizacion_documento}}">
                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Total</label>
                            <input type="text" class="form-control format-number-control" disabled id="total_factura"
                                name="total_factura" data-value-type="convertNumber" tabindex="11"
                                value="{{$libroVenta->importe}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Exentas</label>
                            <input type="text" class="form-control format-number-control" disabled id="exenta"
                                name="exenta" data-value-type="convertNumber" tabindex="12"
                                value="{{$libroVenta->total_exentas}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 10%</label>
                            <input type="text" class="form-control format-number-control" disabled id="gravada_10"
                                name="gravada_10" data-value-type="convertNumber" tabindex="13"
                                value="{{$libroVenta->total_gravadas}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 5%</label>
                            <input type="text" class="form-control format-number-control" disabled id="gravada_5" name="gravada_5" data-value-type="convertNumber" tabindex="14"
                                 value="0">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 10%</label>
                            <input type="text" class="form-control format-number-control read-only" disabled id="iva_10" name="iva_10" data-value-type="convertNumber" tabindex="16" value="{{$libroVenta->total_iva}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 5%</label>
                            <input type="text" class="form-control format-number-control read-only" disabled id="iva_5" name="iva_5" data-value-type="convertNumber" tabindex="17"
                                value="0">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Estado</label>
                            <input type="text" class="form-control" disabled value="{{$libroVenta->estado['denominacion']}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            <label>Retención</label>
                            <input type="text" class="form-control format-number-control read-only" disabled id="retencion" name="retencion" tabindex="" data-value-type="convertNumber" value="{{$libroVenta->monto_retencion}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="form-group">
                            <label>Saldo</label>
                            @if($libroVenta->id_empresa ==  1)
                                <input type="text" class="form-control format-number-control read-only" id="saldo" name="saldo" tabindex="" data-value-type="convertNumber" value="{{$libroVenta->saldo}}">
                            @else
                                <input type="text" class="form-control format-number-control read-only" disabled id="saldo" name="saldo" tabindex="" data-value-type="convertNumber" value="{{$libroVenta->saldo}}">
                            @endif
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Grupo</label>
                            <input type="text" class="form-control" disabled value="{{$libroVenta->grupo ? $libroVenta->grupo->denominacion : '' }}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Negocio</label>
                            <input type="text" class="form-control" disabled value="{{$libroVenta->unidadNegocio ? $libroVenta->unidadNegocio->descripcion : '' }}">
                        </div>
                    </div>

                    <div class="col-12">
                    
                    @if($perfil == 2 && $libroVenta->id_empresa ==  1)
                        <button type="button" class="mr-1 btn btn-success btn-lg pull-right" tabindex="24" style="margin-left: 18px;" id="btnActualizar"><b>Actualizar Saldo</b></button>
                    @endif
                    @if($libroVenta->id_documento == "")
                        <button type="button" class="btn btn-info btn-lg pull-right text-white" data-toggle="modal" data-target="#modalNotaCredito" style="margin-left: 18px;"><b>Nota Crèdito</b></button>	
                    @endif
                    @if($permiso_anular == 1 && $libroVenta->origen == 'M' && $libroVenta->activo)
                        <button type="button" class="ml-1 btn btn-danger btn-lg pull-right" tabindex="23"
                            id="btnAnular"><b>ANULAR</b></button>
                    @endif

                        <button type="button" class="btn btn-light btn-lg pull-right text-white" tabindex="24" id="btnVolver"><b>Volver</b></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
	<!-- ========================================
   					MODAL VERIFICAR RENTA
   	========================================  -->		
        <div id="modalNotaCredito" class="modal fade" role="dialog">
			<div class="modal-dialog modal-xl" role="document" style="width: 80%;margin-left: 25%;margin-top: 10%;">
		    <!-- Modal content-->
		    <div class="modal-content" style="width: 50%;">
		      <div class="modal-header">
                <h4 class="modal-title">Generar Nota de Crèdito</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
                <form id="formNotaCredito" autocomplete="off">
                    <input type="hidden" name="id" class="form-control" value="{{$libroVenta->id}}">
                    <div class="row">          
                        <div class="col-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>Producto</label>
                                <select class="form-control select2" name="id_producto" id="id_producto" style="width: 100%;">
                                    @foreach ($productos as $producto)
                                        <option value="{{$producto->id}}">{{$producto->denominacion}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6">
                            <label>Monto Nota de Crèdito</label>
                            <input type="text" class="form-control format-number-control read-only" id="monto_nota" name="monto_nota" data-value-type="convertNumber" value="{{$libroVenta->saldo}}">
                        </div>
                    </div>
                </form>
              </div>
		      <div class="modal-footer" style="padding-bottom: 5px;padding-top: 5px;">
		        <button type="button" class="btn btn-success" onclick="nota_credito()" data-dismiss="modal">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrarModal">Cerrar</button>
		      </div>
		    </div>

		  </div>
		</div>


                  
    {{-- ========================================
            MODAL   ANULAR OP
        ========================================  --}}		

        <div class="modal fade" id="modalAnular" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="" style="font-weight: 800;">
                            <i class="fa fa-fw fa-remove"></i>
                            ANULAR FACTURA LIBRO VENTA
                        </h4>
                    </div>
            
                    <div class="modal-body">
                        <div class="content-fluid">
                            <div class="row">
            
                                <div class="col-12">
                                    <h3> Esta seguro que quiere anular la Factura ? </h3>
                                </div>
                                <div class="row">
                                    <div class="col-10"></div>
                                    <div class="col-2">
                                        <button type="submit" class="mr-1 btn btn-success btn-lg center-block"
                                            id="btnConfirmarAnulacion" style="margin:0px"><b>Confirmar</b></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>

@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>

<script type="text/javascript">


    $(() => {
        main();
        
});

function main() {
		formatNumber(2);
        $('.select2').select2();
        $('#timbrado').select2({maximumSelectionLength: 1});

    } //


    function formatNumber(coma) {
		console.log('SE dispara formateo');
        $('.format-number-control').inputmask("numeric", {
            radixPoint: ",",
			groupSeparator: ".",
			placeholder: '0',
            digits: coma,
            autoGroup: true,
            // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false
        });

    }
    

    $('#btnAnular').click(()=>{
        $('#modalAnular').modal('show');
    });
    
    $('#btnConfirmarAnulacion').click(()=>{

  

    });


    $('#btnActualizar').click(()=>{
        btnActualizar();
    });


    $('#btnVolver').click(()=>{
        redirectView();
    });

    function btnActualizar(){
      return swal({
                    title: "GESTUR",
                    text: "¿Desea modificar el saldo.?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Modificar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            $.ajax({
                                    type: "GET",
                                    url: "{{route('getActualizarSaldo')}}",
                                    dataType: 'json',
                                    data: {
                                            id_lv:"{{$libroVenta->id}}",
                                            saldo:$('#saldo').val()
                                            },
                                    error: function(jqXHR,textStatus,errorThrown)
                                    {
                                        swal("Cancelado", 'Ocurrio un error en la comunicación con el servidor', "error");
                                    },
                                    success: function(rsp)
                                    {
                                        if(rsp = 'OK'){
                                            swal("Éxito", 'Se ha actualizado el saldo', "success");
                                            location. reload();
                                        }else{
                                            swal("Cancelado", rsp, "error");
                                        }
                                    }   
                                })
                        } else {
                            swal("Cancelado", '', "error");
                        }
                });
        }          

    function redirectView(){
      
      $.blockUI({
                      centerY: 0,
                      message: "<h2>Redirigiendo a vista de Libro Venta...</h2>",
                      css: {
                          color: '#000'
                      }
                  });

                  location.href ="{{ route('indexLibrosCompras') }}";
  }

  function redirectViewNC(idNC){
      
      $.blockUI({
                      centerY: 0,
                      message: "<h2>Redirigiendo a vista de Nota de Credito...</h2>",
                      css: {
                          color: '#000'
                      }
                  });

                  location.href ="{{route('verNota',['id'=>''])}}/"+idNC;
  }


  id_lv = '{{$libroVenta->id}}';
  $.ajax({
			type: "GET",
			url: "{{route('comprobarAsiento')}}",
			data: {
					id_data: parseInt(id_lv),
					option: 'LV'
				},
			dataType: 'json',
			error: function (jqXHR, textStatus, errorThrown) {},
			success: function (rsp) {
				if(rsp.status == 'ERROR'){
                    swal('El asiento Nro.'+rsp.id , 'El asiento generado por este proceso no está balanceado, favor pongase en contacto con Administración/Contabilidad para su verificación.',"error");
				}
			}
	})		 


    function nota_credito(){
        return swal({
                    title: "GESTUR",
                    text: "¿Desea generar la Nota de Credito?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Generar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                                var dataString = $("#formNotaCredito").serialize();
                                $.ajax({
                                        type: "GET",
                                        url: "{{route('doGenerarNC')}}",
                                        dataType: 'json',
                                        data: dataString,
                                        error: function(jqXHR,textStatus,errorThrown){
                                            swal("Cancelado", 'Ocurrió un error en la comunicación con el servidor.', "error");
                                                // $.toast({
                                                //     heading: 'Error',
                                                //     position: 'top-right',
                                                //     text: 'Ocurrió un error en la comunicación con el servidor.',
                                                //     showHideTransition: 'fade',
                                                //     icon: 'error'
                                                // });
                                        },
                                    success: function(rsp)
                                    {
                                        if(rsp.status == 'OK'){
                                            swal("Éxito", 'Se ha generado la Nota de Credito', "success");
                                            redirectViewNC(parseInt(rsp.id))
                                        }else{
                                            swal("Cancelado", 'No se pudo generar la Nota de Credito', "error");
                                        }
                                    }   
                                })
                        } else {
                            swal("Cancelado", '', "error");
                        }
                });

    }


    $('#btnAnular').click(()=>{
        $('#modalAnular').modal('show');
    });
    
    $('#btnConfirmarAnulacion').click(()=>{

                
            $.ajax({
                type: "GET",
                url: "{{route('anularLibroVenta')}}",
                data: { id_libro: '{{$libroVenta->id}}'},
                dataType: 'json',
                error: function(jqXHR,textStatus,errorThrown){
                            $.toast({
                                    heading: 'Error',
                                    text: 'Ocurrio un error en la comunicación con el servidor.',
                                    position: 'top-right',
                                    hideAfter: false,
                                    icon: 'error'
                                });

                        //   $.unblockUI();
                        // $('#btnSave').prop('disabled',false);
                },
                success: function(rsp){

                        if(rsp.err === true){
                            $('#modalAnular').modal('hide');
                            redirectView();
                        } else {
                            $('#modalAnular').modal('hide');
                            $.toast({
                                    heading: 'Error',
                                    text: rsp.msj,
                                    position: 'top-right',
                                    hideAfter: false,
                                    icon: 'error'
                                });
                        }
                
                                }//success
                        });//AJAX


        });

</script>
@endsection