@extends('masters')
@section('title', 'LIBROS COMPRAS')
@section('styles')
<style type="text/css">
	.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    cursor: pointer;
	}	
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#botonExcel {
		display: inline;
	}
	#botonExcel .dt-buttons {
		display: inline;
	}






	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}



	.correcto_col {
        height: 74px;
    }
    

</style>
	@parent
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Fondo Fijo</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
		<div class="card-body pt-0">
			<button id="btnAddLibroCompra" title="Generar Factura" class="btn btn-success pull-right" type="button">
						<div class="fonticon-wrap">
							<i class="ft-plus-circle"></i>
									</div>
			</button>
		</div>

		<div class="card-body">
			<form id="consultaLibroCompra" autocomplete="off">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-3">
					        <div class="form-group">
					            <label>Proveedor</label>
								<select class="form-control select2" name="idProveedor"  id="proveedor" tabindex="1" style="width: 100%;">
									<option value="">Seleccione Proveedor</option>
							
									@foreach($getProveedor as $proveedor)
									@if($proveedor->activo == true)
										@php 
											$activo = 'ACTIVO';
										@endphp
									@else
										@php 
											$activo = 'INACTIVO';
										@endphp
									@endif
									@php
												$ruc = $proveedor->documento_identidad;
												if($proveedor->dv){
													$ruc = $ruc."-".$proveedor->dv;
												}
											@endphp

									<option value="{{$proveedor->id}}">{{$ruc}} - {{$proveedor->nombre}}  <b>({{$activo}})</b></option>
									@endforeach
								
								</select>
					        </div>
			            </div>

			            <div class="col-12 col-sm-6 col-md-3">
						 	<div class="form-group">
						 	 	<label>Proforma</label>
						     	<input type="text" class="form-control" id="idProforma" tabindex="2"  name="idProforma" value="">
							</div>
						</div>

			             <div class="col-12 col-sm-6 col-md-3">
						 	<div class="form-group">
						 	 	<label>Nro. Documento</label>
						     	<input type="text" class="form-control" id="nroFactura" tabindex="3" name="nroFactura" value="">
							</div>
						</div>

						<div class="col-12 col-sm-6 col-md-3">
								<div class="form-group">
									 <label>Estado</label>
									 <select class="form-control select2" name="estado_lc" tabindex="4" id="estado"  style="width: 100%;">
											<option value="1">Activo</option>
											<option value="false">Inactivo</option>
									</select>
							   </div>
						   </div>


						<div class="col-12 col-sm-6 col-md-3">
						 	<div class="form-group">
						 	 	<label>Moneda</label>
						     	<select class="form-control select2" name="idMoneda"  id="idMoneda" tabindex="5" style="width: 100%;">
									<option value="">Seleccione Moneda</option>
									@foreach($monedas as $moneda)
									<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
									@endforeach
									
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-6 col-md-3">
						 	<div class="form-group">
						 	 	<label>Código</label>
						     	<input type="text" class="form-control" id="codigoConfirmacion"  name="codigoConfirmacion" value="" maxlength="40" tabindex="6">
							</div>
						</div>


						<div class="col-12 col-sm-6 col-md-3">  
		 					<div class="form-group">
					            <label>Fecha Desde - Hasta</label>						 
								<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class="form-control pull-right fecha" name="proveedor_desde_hasta" tabindex="7" id="proveedor_desde_hasta" value="">
								</div>
							</div>	
						</div> 	

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label>Cuenta Contable</label>
                            <select class="form-control select2" name="id_cuenta_exenta" id="cuenta_exenta_lc" data-value-type="number" tabindex="19"  style="width: 100%;">

                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)

                                @if($cuenta->asentable)
                                <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif

                                    <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                    </option>
                                    @if($cuenta->asentable)
                                </optgroup>
                                @endif

 
                                @endforeach


                            </select>
                        </div>
                    </div>
						<div class="col-12">
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style=" margin: 0px;"><b>Excel</b></button>
					  			<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
								<button  onclick ="mostrarLibroCompra()" class="pull-right text-center btn btn-info btn-lg mr-1"  type="button"><b>Buscar</b></button>

		  				</div>
					</div>
					

            	<div class="table-responsive mt-1">
	              <table id="listado" class="table table-striped table-bordered file-export" style="width: 100%">
	                <thead style="text-align: center">
						<tr>
							<th>LC</th>
							<th>Asiento</th>
							<th>OP</th>
							<th>Proveedor</th>
							<th style="width:150px;">Código</th>
							<th>Moneda</th>
							<th>Monto</th>
							<th>Saldo</th>
							<th>Producto</th>
							<th>Tipo</th>
							<th>Documento</th>
							<th>Fecha Documento</th>
							<th>Checkin</th>
							<th>Vendedor</th>
							<th>Pasajero</th>
							<th>Fecha Proveedor</th>
							{{-- <th>Pagado</th>
							<th>Fecha OP</th> --}}
							<th></th>
			            </tr>
	                </thead>
	                <tbody style="text-align: center">
	                </tbody>
	              </table>
				</div>  


				
			</div>
		</div>
	</div>
</section>
   

    <div id="requestVerDetalle" class="modal fade" role="dialog">
	  		<div class="modal-dialog modal-lg" style="margin-left: 4%;margin-top: 10%;">
		<!-- Modal content-->
				<div class="modal-content" style="width: 160%;">
					<div class="modal-header" style=" align-content: center;">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Detalles del pago<i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_c" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">
						<div class="table-responsive">
				              <table id="detalleOp" class="table" style="width:100%">
				                <thead>
									<tr>
										<th>Fecha</th>
										<th>Proveedor</th>
										<th>Nro OP</th>
										<th>Total</th>
										<th>Neto</th>
										<th>Moneda</th>
										<th>Número de Control</th>
										<th>Importe ME</th>
						            </tr>
				                </thead>
				                
				                <tbody style="text-align: center">

						        </tbody>
				              </table>
			            </div>
				    </div> 
				  <div class="modal-footer">
					<button type="button"class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>


    <div id="requestVerAsiento" class="modal fade" role="dialog">
	  		<div class="modal-dialog modal-lg" style="margin-left: 4%;margin-top: 10%;">
		<!-- Modal content-->
				<div class="modal-content" style="width: 160%;">
					<div class="modal-header" style=" align-content: center;">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Asiento Contable<i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_c" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">
						<!--<div class="table-responsive">
				              <table id="tableAsiento" class="table nowrap" style="width:100%">
				                <thead>
									<tr>
										<th></th>										
										<th>Cuenta</th>
										<th>Nombre</th>
										<th>Moneda</th>
										<th>Debe</th>
										<th>Haber</th>
										<th>Cotización SET</th>
										<th>Importe Original</th>
										<th>Fecha/Hora</th>
										<th>Usuario</th>				
						            </tr>
				                </thead>
				                
				                <tbody style="text-align: left;">

						        </tbody>
				              </table>
			            </div>-->
								<div class="row">
									<div class="col-12">
										<div class="table-responsive table-bordered">
											<table id="listadoAsientoDetalle" class="table" style="width:100%">
												<thead>
													<tr>
														<th>Cuenta</th>
														<th>Nombre</th>
														<th>Sucursal</th>
														<th>C.C</th>
														<th>Debe</th>
														<th>Haber</th>
														<th>Cotización</th>
														<th>Importe Original</th>
														<th></th>
													</tr>
												</thead>
												<tbody style="text-align: left; font-weight: 800;">
												</tbody>
												{{-- style="background-color:#E4F1FA;" --}}
												<tfoot>
													<tr>
														<th colspan="2" style="text-align:right;"> DIFERENCIA</th>
														<th id="diferencia_footer"></th>
														<th> TOTAL</th>
														<th id="total_debe"></th>
														<th id="total_haber"></th>
														<th colspan="3"></th>

													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								
									<div class="col-12">
										<div class="form-group">
											<label>CONCEPTO</label>
											<input type="text" autocomplete="false" id="concepto_datatable" class="form-control date-picker"
												value="" autocomplete="off" readonly />
										</div>
									</div>
								</div>
				    </div> 
				  <div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>
	{{-- ========================================
            MOSTRAR DOCUMENTO BANCARIO
    ========================================  --}}      

    <div class="modal fade" id="modalDocumentoMovimiento" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" style="margin-left: 9%;margin-top: 3%;">
        <div class="modal-content" style="width: 250%;height:800px">
          <div class="modal-header">
            <h4 class="modal-title"  style="font-weight: 800;">Adjunto <i style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
            <button type="button" class="closeModal btn-danger"  data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
             <div>
                <img src="" class="img-responsive hidden" id="imgMostrar" style="overflow: scroll;width: 100%;">
                <iframe id="verPdf" src="" class="hidden" style="width: 100%;height: 600px;"></iframe>
             </div>   
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
			<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
		<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	<script>
	const operaciones = {id_row:0, id_fila:0, tipoGuardar:true}
	const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });
	var table;
	$(document).ready(function() {
			// table = mostrarLibroCompra();
		});


		$('#btnAddLibroCompra').click(()=>{
			location.href = "{{ route('addLibroCompraFijo')}}";
		});
	


	
	$(function() 
	{

		let locale = {
					format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};

	  $('input[name="proveedor_desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: locale
	  });

	  $('input[name="proveedor_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="proveedor_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });


	  $('input[name="checkin_desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: locale
	  });

	  $('input[name="checkin_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="checkin_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });

  });
		$('.select2').select2();
		
		var tablaDetalle =  $('#listadoAsientoDetalle').dataTable();
		
		function limpiar()
		{
			$('#idMoneda').val('').trigger('change.select2');
			$('#proveedor').val('').trigger('change.select2');
			$('#estado').val('1').trigger('change.select2');
			$('#idProforma').val('');
			$('#nroFactura').val('');
			$('#codigoConfirmacion').val('');
			$('#proveedor_desde_hasta').val('');
			// mostrarLibroCompra();

		}

		function validar(data)
		{

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}

		const formatter = new Intl.NumberFormat('de-DE', 
		{
            currency: 'USD',
            minimumFractionDigits: 2
        });

        function formatearFecha(texto)
 			{
	            if(texto != '' && texto != null)
	            {
	          		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	            } 
	            else 
	            {
	         		return '';    
            	}
        	}

		function mostrarLibroCompra()
		{
			// console.log('mostrarLibroCompra');
			 table = $("#listado").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('mostrarLibroCompraFijo')}}",
						"type": "GET",
						"data": {"formSearch": $('#consultaLibroCompra').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					"aaSorting":[[0,"desc"]],
					"columns": 
					[
						
						{ "data": function(x)
							{	
								btn = `<a href="{{route('verLibroCompra',['id'=>''])}}/${x.id}" class="bgRed"><i class="fa fa-fw fa-search"></i>${x.id}</a>`;
								return btn;
							} 
						},
						{ "data": function(x)
							{	
								btn = '';
								if(x.id_asiento != 0)
								{
									btn = `<a onclick="verAsientoCompra(${x.id_asiento})" class="bgRed"><i class="fa fa-fw fa-search"></i>${x.id_asiento}</a>`;
								}

								
								return btn;
							}

						},

						{ "data": function(x)
							{
								var btn = '';

								if (x.id_op != 0) 
								{
									btn = `<a onclick="verDetalle(${x.id})" class="bgRed"><i class="fa fa-fw fa-search"></i>OP</a>`;
								}
									return btn;
							} 
						}, 

						{ "data": "proveedor" },

						{ "data": "cod_confirmacion" },

						{ "data": "moneda" },

						{ "data": function(x)
							{
								return formatter.format(parseFloat(x.monto));
							}
						},	

						{ "data": function(x)
							{
								return formatter.format(parseFloat(x.saldo));
							}
						},

						{ "data": "producto" },
						{ "data": "tipo" },
						{ "data": "nro_documento" },
						//{ "data": "fecha_documento" },
						{ "data": function(x)
							{
								var fecha = '';

								if (x.fecha_creacion != null) 
								{
									fecha1 = x.fecha_creacion.split(' ');
									fecha2 = fecha1[0].split('-')
									fecha = fecha2[2]+"/"+fecha2[1]+"/"+fecha2[0]
								}

								return fecha;
							} 
						},

						{ "data": "check_in" },
						{ "data": "vendedor" },
						{ "data": "pasajero" },
						{ "data": "fecha_proveedor" },
						{ "data": function(x){
							if(jQuery.isEmptyObject(x.adjunto) == false){
								var acciones= ""; 
								acciones=`<button onclick="getMovimientoDetalle(${x.id})" class='btn btn-info btnEditHidden' type='button'><i class="fa fa-clone"></i></button>`
							}else{
								var acciones= "";
							}	
						    return acciones;               
                		}},

						

					], 
					"initComplete": function (settings, json) {
			 		// $.unblockUI();
					 $('[data-toggle="tooltip"]').tooltip();
					 
			 	}
					
				});	
				
				// botonExcel([2,3,4,5,6,7,8,9,10,11,12,13,14], table);
				return table;
		}	

		
	function verDetalle(idLibroCompra)
	{
		$("#requestVerDetalle").modal("show");
		let dataString = {idLibroCompra:idLibroCompra};

		 $("#detalleOp").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('verDetalle')}}",
						"data": dataString,
						"type": "GET",
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					// "aaSorting":[[2,"asc"]],
					"columns": 
					[
						{ "data": function(x)
							{
								var fecha = '';

								if (x.op_cabecera.fecha_hora_creacion != null) 
								{
									fecha = x.op_cabecera.fecha_hora_creacion.split(' ');
									fecha = formatearFecha(fecha[0]);
								}

								return fecha;
							} 
						},

						{ "data": "op_cabecera.proveedor.nombre" },

						// { "data": "op_cabecera.id" },
						{data: function(x){
						        	return `<a href="{{route('controlOp',['id'=>''])}}/${x.op_cabecera.id}"  class="bgRed">
						        	<i class="fa fa-fw fa-search"></i>${x.op_cabecera.id}</a>
						        	<div style="display:none;">${x.op_cabecera.id}</div>
						        	`;
				
						        }},

						{ "data": function(x)
							{
								var total = 0;

								if (x.op_cabecera.total_pago != null) 
								{
									total = formatter.format(parseFloat(x.op_cabecera.total_pago));
								}
								return total;
							}
						},

						{ "data": function(x)
							{
								var total_neto = 0;

								if (x.op_cabecera.total_neto_pago != null) 
								{
									total_neto = formatter.format(parseFloat(x.op_cabecera.total_neto_pago));
								}

								return total_neto;
							}
						},

						{ "data": "op_cabecera.currency.currency_code" },

						{ "data": "libro_compra.id_asiento" },

						{ "data": "libro_compra.importe_me" },

					], 
				});	
	}

	function verAsientoCompra(idLibroCompra)
	{
		/*$("#requestVerAsiento").modal("show");
		let dataString = {idLibroCompra:idLibroCompra};

		 $("#tableAsiento").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('verAsientoCompra')}}",
						"data": dataString,
						"type": "GET",
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					// "aaSorting":[[0,"asc"]],
					"columns": 
					[
						{"data": function(x){
							return Number(x.orden);
						} },
					
						{"data": function(x)
							{
								console.log(x);
								var cuenta = "";

								if (x.cuenta_contable.cod_txt != null) 
								{
									cuenta = x.cuenta_contable.cod_txt;
								}

								return cuenta;
							} 
						},	

						{"data": function(x){
								// console.log(x);
								var descripcion = "";

								if (x.cuenta_contable.descripcion != null) 
								{
									descripcion = x.cuenta_contable.descripcion;
								}

								return descripcion;
							} 
						},

						{ "data": "moneda.currency_code"},

						{ "data": function(x)
							{
								// console.log(x);
								var debe = 0;

								debe = formatter.format(parseFloat(x.debe));
								
								return debe;
							}
						},

						{ "data": function(x)
							{
								var haber = 0;

								haber = formatter.format(parseFloat(x.haber));
								
								return haber;
							}
						},

						{ "data": "cotizacion" },

						{ "data": "monto_original"},

						{ "data": function(x)
							{
								var fecha = '';

								if (x.asiento.fecha_hora != null) 
								{
									fecha = x.asiento.fecha_hora.split(' ');
								}

								return formatearFecha(fecha[0])+' '+fecha[1];
							} 
						},

						{ "data": function(x)
							{
								
								if (x.asiento.usuario.apellido == null) 
								{
									return x.asiento.usuario.nombre+" "+' ';
								}
								else
								{
									return x.asiento.usuario.nombre+" "+x.asiento.usuario.apellido;
								}
								
							} 
						},

					], 
					"columnDefs": [{
			 			targets: 0,
						visible: false
			 		}],
				});	*/
/////////////////////////////////////////////////////////////////////////////////////////////////////
			$("#requestVerAsiento").modal("show");
			let dataString = {id_asiento:idLibroCompra};
			$.ajax({
                type: "GET",
                url: "{{ route('ajaxReporteAsientoDetalle') }}",
                dataType: 'json',
                data: dataString,
				success: function(rsp)
				{
                    oSettings = tablaDetalle.fnSettings();
					
					var iTotalRecords = oSettings.fnRecordsTotal();
					var importe_cotizado = 0;

					for (i=0;i<=iTotalRecords;i++) 
					{
                        $('#listadoAsientoDetalle').dataTable().fnDeleteRow(0,null,true);
					}

					$('#documento_asiento').val(rsp.data1.nro_documento);
					$('#fecha_asiento').val(rsp.data1.fecha);
					$('#concepto_datatable').val(rsp.data1.concepto);
					$.each(rsp.data, function (key, item)
					{
						// console.log(item.haber);
						if(Boolean(item.debe) & item.debe > 0)
						{
							importe_cotizado = item.debe;
						}
						else
						{
							importe_cotizado = item.haber;
						}

						if(item.sucursal !== null){
							sucursal = item.sucursal
						}else{
							sucursal = '';
						}

						var inputs = `<input type="hidden" class="cuenta" name="asiento_detalle[][cuenta]" value="${item.id_cuenta_contable}"> 
									  <input type="hidden" class="sucursal" name="asiento_detalle[][sucursal]" value="${item.id_sucursal}">
								      <input type="hidden" class="centro_costo" name="asiento_detalle[][centro_costo]" value="${(Boolean(item.id_centro_costo))?item.id_centro_costo:0}">
									  <input type="hidden" class="debe" name="asiento_detalle[][debe]" value="${item.debe}">
									  <input type="hidden" class="haber" name="asiento_detalle[][haber]" value="${item.haber}">
									  <input type="hidden" class="cotizacion" name="asiento_detalle[][cotizacion]" value="${item.cotizacion}">
									  <input type="hidden" class="importe_original" name="asiento_detalle[][importe_original]" value="${item.monto_original}">
									  <input type="hidden" class="importe_cotizado" name="asiento_detalle[][importe_cotizado]" value="${importe_cotizado}">
									  <input type="hidden" class="concepto" name="asiento_detalle[][concepto]" value="${item.concepto_detalle}">
									  <input type="hidden" class="moneda" name="asiento_detalle[][moneda]" value="${item.id_moneda}">`;

                        var dataTableRows = 
						[
							inputs+`<span>${item.num_cuenta}</span>`,
							`<span>${item.cuenta_n}</span>`,
							`<span>${sucursal}</span>`,
							`<span>${item.centro_costo}</span>`,
							`<span>${formatter.format(parseFloat(item.debe))}</span>`,
							`<span>${formatter.format(parseFloat(item.haber))}</span>`,
							`<span>${item.cotizacion}</span>`,
							`<span>${formatter.format(parseFloat(item.monto_original))}</span>`,
							// `<span>${item.concepto_detalle}</span>`,
							`<button onclick="getAsientoDetalle('row'+${item.id})" class='btn btn-info btnEditHidden' style="display:none" type='button'><i class="fa fa-pencil-square-o"></i></button>`
						];

                        var newrow = $('#listadoAsientoDetalle').dataTable().fnAddData(dataTableRows);
                    // set class attribute for the newly added row
						var nTr = $('#listadoAsientoDetalle').dataTable().fnSettings().aoData[newrow[0]].nTr;
				
                        var nTds = $('td', nTr);
                        nTds.addClass('row'+item.id);

						var row = $('#listadoAsientoDetalle').dataTable().fnGetNodes(newrow);
    					$(row).attr('id', 'row'+item.id);
						importe_cotizado = 0;

                    })
                }
            }).done(function(){
                $.unblockUI();
				sumarDebeHaber();
				//levantarBtn();
            });
/////////////////////////////////////////////////////////////////////////////////////////////////////
	}

	/*function botonExcel(column,table)
	{
		// console.log('botoooooooooooooonm');
		$('#botonExcel').html('');
		var buttons = new $.fn.dataTable.Buttons(table, 
		{ 
			buttons: 
			[{	
				title: 'Libro de Compras',
				extend: 'excelHtml5',
				text: 'Exportar a Excel',
				className: 'btnExcel',
				exportOptions: 
				{
					columns: ':visible'
				},
				exportOptions: 
				{
					columns:  column,
					format: 
					{
						//seleccionamos las columnas para dar formato para el excel
						body: function ( data, row, column, node ) 
						{
							if(column == 3)
							{
								if (data != '0') 
								{
									// console.log(data);
									data = data.replace(".","");
									//cambiamos la cpunto
									data = data.replace(",",".");
																			
								}
									var numFinal = parseFloat(data);
									return  numFinal;
							} 
							return data;
						}
					}
				},
				
				//Generamos un nombre con fecha actual
				
				filename: function() 
				{
					var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
					return 'LibroCompras-'+date_edition;
				}
			}]
		}).container().appendTo('#botonExcel'); 
	}*/

		$("#botonExcel").on("click", function(e){ 
			
                e.preventDefault();
                $('#consultaLibroCompra').attr('method','post');
               	$('#consultaLibroCompra').attr('action', "{{route('generarExcelLibroCompraFondoFijo')}}").submit();
            });


	
		$(document).ready(function () {
			var dtable = $('#listado').DataTable();
			$('.filter-button').on('click', function () {
		        //clear global search valuess
		        dtable.search('');
		        $('.filter').each(function(){ 
		        if(this.value.length){
		          dtable.column($(this).data('columnIndex')).search(this.value);
		        }
		        });
		        dtable.draw();
		    });
		    
		    $( ".dataTables_filter input" ).on( 'keyup change',function() {
		        dtable.columns().search('');
		       //clear input values
		       $('.filter').val('');
			});	
		})    
function sumarDebeHaber()
{
	
	var asientoDetalle = $('#formAsientoDetalle').serializeJSON();
	var debe = 0;
	var haber = 0;
	var dif = 0;

	$.each(asientoDetalle.asiento_detalle, (index, value)=>{
		debe += Number(value.debe);
		haber += Number(value.haber);
	})
	dif = debe - haber;

	console.log(debe, haber);
	operaciones.debe = debe;
	operaciones.haber = haber;
	debe = GUARANI(debe,{ formatWithSymbol: false }).format(true);
	haber = GUARANI(haber,{ formatWithSymbol: false }).format(true);
	dif = GUARANI(dif,{ formatWithSymbol: false }).format(true);
	$('#total_debe').html(debe);
	$('#total_haber').html(haber);
	$('#diferencia_footer').html(dif);
}

function getMovimientoDetalle(idLibroCompra){
		let dataString = {idLibroCompra:idLibroCompra};
          $.ajax({
                    type: "GET",
                    url: "{{ route('adjuntoLibroCompras')}}",
                    dataType: 'json',
                    data: dataString,
                    error: function(){
                            console.log('Error');
                    },
                    success: function(rsp){
                                    console.log(rsp);
                                    if(jQuery.isEmptyObject(rsp) == false){
                                        var archivoName =rsp;
                                        var extension = archivoName.split('.');
                                             archivo = extension[extension.length - 1];
                                             archivo = archivo.toLowerCase();

                                        var getUrl = window.location;
                                        console.log(getUrl);
                                        var baseUrl = getUrl .protocol
                                        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                                            
                                            console.log(baseUrl);
                                        if(archivo === 'jpg' || archivo === 'png'|| archivo === 'jpeg'){
                                            $('#imgMostrar').removeClass('hidden');
                                            $('#verPdf').addClass('hidden');
                                            direccion = '../adjuntoLc/'+archivoName;
                                            $('#imgMostrar').attr('src',direccion);
                                        }
                                        if(archivo === 'pdf' || archivo === 'docx'){
                                            $('#imgMostrar').addClass('hidden');
                                            $('#verPdf').removeClass('hidden');
                                            direccion = '../adjuntoLc/'+archivoName;
                                            $('#verPdf').attr('src',direccion);
                                        }
                                        $('#modalDocumentoMovimiento').modal('show');

                                    }else{
                                        $.toast({
                                                heading: '<b>Error</b>',
                                                position: 'top-right',
                                                text: 'El movimiento no tiene adjunto.',
                                                width: '400px',
                                                hideAfter: false,
                                                icon: 'error'
                                            });
                                    }
                     }
                })               

    }    



	</script>
@endsection
