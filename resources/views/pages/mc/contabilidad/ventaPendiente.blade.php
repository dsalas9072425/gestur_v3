@extends('masters')
@section('title', 'LIBROS COMPRAS')
@section('styles')
<style type="text/css">
	.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    cursor: pointer;
	}	
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#botonExcel {
		display: inline;
	}
	#botonExcel .dt-buttons {
		display: inline;
	}






	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}



	.correcto_col {
        height: 74px;
    }
    

</style>
	@parent
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">COSTOS DE VENTAS</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
		<div class="card-body">
			<form id="consultaLibroCompra" autocomplete="off">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-3">
					        <div class="form-group">
					            <label>Proveedor</label>
								<select class="form-control select2" name="idProveedor"  id="proveedor" tabindex="1" style="width: 100%;">
									<option value="">Seleccione Proveedor</option>
							
									@foreach($proveedores as $proveedor)
									@if($proveedor->activo == true)
										@php 
											$activo = 'ACTIVO';
										@endphp
									@else
										@php 
											$activo = 'INACTIVO';
										@endphp
									@endif
                                    @php
										$ruc = $proveedor->documento_identidad;
										if($proveedor->dv){
											$ruc = $ruc."-".$proveedor->dv;
										}
									@endphp
									<option value="{{$proveedor->id}}">{{$ruc}} - {{$proveedor->nombre}}  <b>({{$activo}})</b></option>
									@endforeach
								
								</select>
					        </div>
			            </div>

			            <div class="col-12 col-sm-6 col-md-3">
						 	<div class="form-group">
						 	 	<label>Proforma</label>
						     	<input type="text" class="form-control" id="idProforma" tabindex="2"  name="idProforma" value="">
							</div>
						</div>

			             <div class="col-12 col-sm-6 col-md-3">
						 	<div class="form-group">
						 	 	<label>Nro. de Factura</label>
						     	<input type="text" class="form-control" id="nroFactura" tabindex="3" name="nroFactura" value="">
							</div>
						</div>

						<div class="col-12 col-sm-6 col-md-3">
						 	<div class="form-group">
						 	 	<label>Moneda</label>
						     	<select class="form-control select2" name="idMoneda"  id="idMoneda" tabindex="5" style="width: 100%;">
									<option value="">Seleccione Moneda</option>
									@foreach($monedas as $moneda)
									<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
									@endforeach
									
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-6 col-md-3">
						 	<div class="form-group">
						 	 	<label>Código</label>
						     	<input type="text" class="form-control" id="codigoConfirmacion"  name="codigoConfirmacion" value="" maxlength="40" tabindex="6">
							</div>
						</div>


						<div class="col-12 col-sm-6 col-md-3">  
		 					<div class="form-group">
					            <label>Fecha Desde/Hasta Factura</label>						 
								<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class="form-control pull-right fecha" name="proveedor_desde_hasta" tabindex="7" id="proveedor_desde_hasta" value="">
								</div>
							</div>	
						</div> 	
						<div class="col-12 col-sm-6 col-md-3">  
		 					<div class="form-group">
					            <label>Fecha Desde/Hasta Creación</label>						 
								<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class="form-control pull-right fecha" name="creacion_desde_hasta" tabindex="7" id="creacion_desde_hasta" value="">
								</div>
							</div>	
						</div> 	
                    	<div class="col-12 col-sm-3 col-md-3">  
								<div class="form-group">
								   <label>Mes</label>						 
								   <div class="input-group">
									   <select class="form-control select2" name="mes" id="mes" tabindex="1" style="width: 100%;">
											<option value="">Todos</option>
											<option value="01">Enero</option>
											<option value="02">Febrero</option>
											<option value="03">Marzo</option>	
											<option value="04">Abril</option>
											<option value="05">Mayo</option>
											<option value="06">Junio</option>
											<option value="07">Julio</option>	
											<option value="08">Agosto</option>
											<option value="09">Setiembre</option>	
											<option value="10">Octubre</option>
											<option value="11">Noviembre</option>
											<option value="12">Diciembre</option>
										</select>
								   </div>
							   </div>	
						   </div> 

							<div class="col-12 col-sm-3 col-md-3">  
								<div class="form-group">
								   <label>Año</label>						 
								   <div class="input-group">
									  	<select class="form-control select2" name="anho" id="anho" tabindex="1" style="width: 100%;">
											<option value="">Todos</option>
											<?php
												$fecha = date('Y');
												for ($i = 2008; $i <= $fecha; $i++) {
												    echo '<option value="'.$i.'">'.$i.'</option>';
												}
											?>
										</select>
								   </div>
							   </div>	
						   </div> 
							<div class="col-12 col-sm-6 col-md-3">
								<div class="form-group">
									 <label>Origen</label>
									 <select class="form-control select2" name="tipo_proveedor" tabindex="4" id="tipo_proveedor"  style="width: 100%;">
									 	<option value="">Todos</option>
										<option value="1">Exterior</option>
									 	<option value="2">Nacional</option>
									</select>
							   </div>
						   </div>
						   <div class="col-12 col-sm-6 col-md-3">
								<div class="form-group">
									 <label>Asociado</label>
									 <select class="form-control select2" name="asociado" tabindex="4" id="asociado"  style="width: 100%;">
										<option value="N">NO</option>
									 	<option value="S">SI</option>
									</select>
							   </div>
						   </div>

						<div class="col-9">
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style=" margin: 0px;"><b>Excel</b></button>
					  			<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
								<button  onclick ="mostrarLibroCompra()" class="pull-right text-center btn btn-info btn-lg mr-1"  type="button"><b>Buscar</b></button>

		  				</div>
					</div>
					

            	<div class="table-responsive mt-1">
	              <table id="listado" class="table table-striped table-bordered file-export" style="width: 100%">
	                <thead style="text-align: center">
						<tr>
						<th>Fecha</th>
							<th>Día</th>
							<th>Nro. Factura</th>
							<th>R.U.C.</th>
							<th>Proveedor</th>
							<th>Gravadas 10%</th>
							<th>Gravadas 5%</th>
							<th>IVA 10%</th>
							<th>IVA 5%</th>
							<th>Exentas</th>
							<th>Total</th>
							<th>Timbrado</th>
							<th>Tipo</th>
							<th>LC Asociado</th>
							<th>Proforma</th>
							<th>Origen</th>			           
						</tr>
	                </thead>
	                <tbody style="text-align: center">
	                </tbody>
	              </table>
				</div>  

			</div>
		</div>
	</div>
</section>
   
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
			<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
		<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
	<script>
	const operaciones = {id_row:0, id_fila:0, tipoGuardar:true}
	const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });
	var table;
	$(document).ready(function() {
			// table = mostrarLibroCompra();
		});


		$('#btnAddLibroCompra').click(()=>{
			location.href = "{{ route('addLibroCompra')}}";
		});
	


	
	$(function() 
	{

		let locale = {
					format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};

	  $('input[name="proveedor_desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: locale
	  });

	  $('input[name="proveedor_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="proveedor_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });

	  $('input[name="pago_desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: locale
	  });
	  $('input[name="pago_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });
	  $('input[name="pago_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });

	  $('input[name="creacion_desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: locale
	  });

	  $('input[name="creacion_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="creacion_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });

	  $('input[name="checkin_desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: locale
	  });

	  $('input[name="checkin_desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="checkin_desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });

  });
		$('.select2').select2();
		
		var tablaDetalle =  $('#listadoAsientoDetalle').dataTable();
		
		function limpiar()
		{
			$('#idMoneda').val('').trigger('change.select2');
			$('#proveedor').val('').trigger('change.select2');
			$('#estado').val('1').trigger('change.select2');
			$('#idProforma').val('');
			$('#nroFactura').val('');
			$('#codigoConfirmacion').val('');
			$('#proveedor_desde_hasta').val('');
			// mostrarLibroCompra();

		}

		function validar(data)
		{

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}

		const formatter = new Intl.NumberFormat('de-DE', 
		{
            currency: 'USD',
            minimumFractionDigits: 2
        });

        function formatearFecha(texto)
 			{
	            if(texto != '' && texto != null)
	            {
	          		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	            } 
	            else 
	            {
	         		return '';    
            	}
        	}

		function mostrarLibroCompra()
		{
			// console.log('mostrarLibroCompra');
			 table = $("#listado").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('mostrarVentasPendientes')}}",
						"type": "GET",
						"data": {"formSearch": $('#consultaLibroCompra').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					"aaSorting":[[3,"asc"]],
					"columns": 
					[
						{ "data": "fecha_hora_facturacion_format" },

						{ "data": "dia" },

						{ "data": "nro_factura" },

						{ "data": "documento_identidad" },

						{ "data": "proveedor" },

						{ "data": function(x)
							{
								return formatter.format(parseFloat(x.gravadas_10));
							}
						},	

						{ "data": function(x)
							{
								return formatter.format(parseFloat(x.gravadas_5));
							}
						},

						{ "data": function(x)
							{
								return formatter.format(parseFloat(x.iva10pago));
							}
						},

						{ "data": function(x)
							{
								return formatter.format(parseFloat(x.iva05pago));
							}
						},

						{ "data": function(x)
							{
								return formatter.format(parseFloat(x.exento));
							}
						},

						{ "data": function(x)
							{
								if(x.total == 0){
									return 0;
								}else{
									return formatter.format(parseFloat(x.total));
								}
							}
						},

						{ "data": "numero" },
						{ "data": "tipo" },
						{ "data": function(x)
							{
								btn = '';
								if(x.id_libro_compra !== null){
									btn = `<a href="{{route('verLibroCompra',['id'=>''])}}/${x.id_libro_compra}" class="bgRed"><i class="fa fa-fw fa-search"></i>${x.id_libro_compra}</a>`;
								}
								return btn;

							}
						},
						{ "data": function(x)
							{
								btn = '';
								if(x.id_proforma !== null){
									btn = `<a href="{{route('detallesProforma',['id'=>''])}}/${x.id_proforma}" class="bgRed"><i class="fa fa-fw fa-search"></i>${x.id_proforma}</a>`;
								}
								return btn;

							}
						},
						{ "data": "origen" }
					], 
					"initComplete": function (settings, json) {
			 		// $.unblockUI();
					 $('[data-toggle="tooltip"]').tooltip();
					 
			 		},
					/* "columnDefs": [
						{
							"targets": [3],
							"visible": false
						}
					]*/

					
				});	
				
				// botonExcel([2,3,4,5,6,7,8,9,10,11,12,13,14], table);
				return table;
		}	

		

		$("#botonExcel").on("click", function(e){ 
			
                e.preventDefault();
                $('#consultaLibroCompra').attr('method','post');
               	$('#consultaLibroCompra').attr('action', "{{route('generarExcelVentaPendiente')}}").submit();
            });


	
		$(document).ready(function () {
			var dtable = $('#listado').DataTable();
			$('.filter-button').on('click', function () {
		        //clear global search valuess
		        dtable.search('');
		        $('.filter').each(function(){ 
		        if(this.value.length){
		          dtable.column($(this).data('columnIndex')).search(this.value);
		        }
		        });
		        dtable.draw();
		    });
		    
		    $( ".dataTables_filter input" ).on( 'keyup change',function() {
		        dtable.columns().search('');
		       //clear input values
		       $('.filter').val('');
			});	
		})    
function sumarDebeHaber()
{
	
	var asientoDetalle = $('#formAsientoDetalle').serializeJSON();
	var debe = 0;
	var haber = 0;
	var dif = 0;

	$.each(asientoDetalle.asiento_detalle, (index, value)=>{
		debe += Number(value.debe);
		haber += Number(value.haber);
	})
	dif = debe - haber;

	console.log(debe, haber);
	operaciones.debe = debe;
	operaciones.haber = haber;
	debe = GUARANI(debe,{ formatWithSymbol: false }).format(true);
	haber = GUARANI(haber,{ formatWithSymbol: false }).format(true);
	dif = GUARANI(dif,{ formatWithSymbol: false }).format(true);
	$('#total_debe').html(debe);
	$('#total_haber').html(haber);
	$('#diferencia_footer').html(dif);
}




	</script>
@endsection
