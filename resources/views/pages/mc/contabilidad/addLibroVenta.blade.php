@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
<style type="text/css">
    .correcto_col {
        height: 74px;
    }
    					
    input.form-control:focus ,.select2-container--focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }

     .labelError{
         color: #F74343 ;
     }
</style>

@endsection
@section('content')
<section id="base-style">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Cargar Venta</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
                <form class="row" id="formFactura" method="post" autocomplete="off">
<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Cliente</label>
                            <select class="form-control select2" name="id_proveedor" data-value-type="number" tabindex="1"  id="id_proveedor" style="width: 100%;" required>
                                <option value="" data-ruc="">Seleccione Cliente</option>    
                                @foreach ($proveedores as $proveedor)
                                    @php
										$ruc = $proveedor->documento_identidad;
										if($proveedor->dv){
											$ruc = $ruc."-".$proveedor->dv;
										}
									@endphp
                                    <option value="{{$proveedor->id}}" data-ruc="{{$ruc}}" data-pais="{{$proveedor->id_pais}}">{{$proveedor->full_data}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>RUC</label>
                            <input type="text" class="form-control" maxlength="30" id="proveedor_ruc" tabindex="2" name="proveedor_ruc" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                            <label>Timbrado</label>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <button type="button" id="btnAddTimbrado" title="Agregar Timbrado"
                                class="btn-mas-request btn btn-info btn-sm"><i class="fa fa-plus fa-lg"></i></button>
                              </div>
                              <select placeholder="Seleccionar o Agregar" name="timbrado" tabindex="3" style="width: 90%;" id="timbrado" class="form-control select2">
                              </select>
                            </div>
                    </div>
            
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Documento</label>
                            <select class="form-control select2" name="id_tipo_documento" data-value-type="number" tabindex="4" id="select_tipo_documento" style="width: 100%;">
                                @foreach ($tipoDocumentos as $tipoDocumento)
                                    <option value="{{$tipoDocumento->id}}">{{$tipoDocumento->descripcion}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Facturacion</label>
                            <select class="form-control select2" name="id_tipo_facturacion" data-value-type="number" tabindex="4" id="select_id_tipo_facturacion" style="width: 100%;">
                                @foreach ($tipo_facturacions as $tipo_facturacion)
                                    <option value="{{$tipo_facturacion->id}}">{{$tipo_facturacion->denominacion}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Fecha Factura</label>
                            <div class="input-group">
                                <div class="input-group-prepend" style="">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control pull-right single-picker" name="fecha_factura" tabindex="5" data-value-type="s_date" id="fecha_factura" maxlength="10"  value="">
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Comprobante</label>
                            <select class="form-control select2" name="id_tipo_comprobante" data-value-type="number"
                                tabindex="6" id="select_tipo_factura" style="width: 100%;">
                                @foreach ($tipo_factura as $tipo)
                                <option value="{{$tipo->id}}">{{$tipo->denominacion}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3" style="display:none">
                        <div class="form-group">
                            <label>Costo de Venta</label>
                            <select class="form-control select2" name="costo_venta" id="costo_venta" style="width: 100%;">
                                <option value="true">SI</option>
                                <option value="false">NO</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="proveedor_nombre" name="proveedor_nombre">

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Nro Comprobante</label>
                            <input type="text" class="form-control" maxlength="30" id="nro_comprobante" tabindex="7" name="nro_comprobante" value="">
                        </div>
                    </div>
                    <!--<div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Nro. de Venta/Pedido</label>
                            <input type="text" class="form-control" maxlength="30" id="nro_venta_pedido" tabindex="7" name="nro_venta_pedido" value="">
                        </div>
                    </div>-->

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Vencimiento</label>
                            <div class="input-group">
                                <div class="input-group-prepend" style="">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control single-picker" data-value-type="s_date" readonly
                                    id="vencimiento" tabindex="8" name="vencimiento" maxlength="10"  value="">
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Centro Costo</label>
                            <select class="form-control select2" name="id_centro_costo"  data-value-type="number" tabindex="9" style="width: 100%;">
                                @foreach ($centro as $c)
                                <option value="{{$c->id}}">{{$c->nombre}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Sucursal</label>
                            <select class="form-control select2" name="id_sucursal" data-value-type="number"
                                tabindex="10"  style="width: 100%;" required>
                                @foreach ($sucursalEmpresa as $sucursal)
                                <option value="{{$sucursal->id}}">{{$sucursal->denominacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Moneda</label>
                            <select class="form-control select2" name="id_moneda" id="id_moneda" tabindex="11"
                                data-value-type="number" style="width: 100%;" required>
                                @foreach ($monedas as $div)
                                <option value="{{$div->currency_id}}">{{$div->currency_code}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Cotización</label>
                            <input type="text" maxlength="10" class="form-control format-number-control" readonly id="lc_cotizacion"
                                data-value-type="convertNumber" tabindex="12" name="cotizacion" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Saldo</label>
                            <input type="text" maxlength="25" class="form-control format-number-control" id="saldo"
                                name="saldo" data-value-type="convertNumber" tabindex="13" value="0">
                                <small>Si deja el campo en 0 se toma el total de la factura</small>
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Total</label>
                            <input type="text" maxlength="25" class="form-control format-number-control" id="total_factura"
                                name="total_factura" data-value-type="convertNumber" tabindex="13" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Exentas</label>
                            <input type="text" class="form-control format-number-control" id="exenta" name="exenta"
                                data-value-type="convertNumber" tabindex="14" maxlength="25" value="0">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 10%</label>
                            <input type="text" class="form-control format-number-control" id="gravada_10"
                                name="gravada_10" data-value-type="convertNumber" maxlength="25" tabindex="15" value="0">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 5%</label>
                            <input type="text" class="form-control format-number-control" id="gravada_5"
                                name="gravada_5" data-value-type="convertNumber" tabindex="16" value="0">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Retención IVA</label>
                            <input type="text" class="form-control format-number-control" id="ret_iva"
                                name="ret_iva" data-value-type="convertNumber" tabindex="17" value="0">
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Retención Renta</label>
                            <input type="text" class="form-control format-number-control" id="ret_renta"
                                name="ret_renta" data-value-type="convertNumber" tabindex="18" value="0">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 10%</label>
                            <input type="text" class="form-control format-number-control read-only" id="iva_10" name="iva_10" readonly data-value-type="convertNumber" maxlength="25" tabindex="19" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 5%</label>
                            <input type="text" class="form-control format-number-control read-only" id="iva_5" readonly name="iva_5" data-value-type="convertNumber" tabindex="20" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3"> 
                        <div class="form-group">
                            <label>Pasajero</label>
                            <input type="text" class="form-control read-only" id="pasajero" name="pasajero" value="">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3"> 
                        <div class="form-group">
                            <label>Código/Referencia</label>
                            <input type="text" class="form-control read-only" id="codigo_referencia" name="codigo_referencia" value="">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Selector de Grupos</label>
                            <select class="form-control select2" name="id_grupo" id="id_grupo" tabindex="21" style="width: 100%;">
                                <option value="">Seleccione un grupo</option>
                                @foreach ($grupos as $grupo)
                                    <option value="{{$grupo->id}}">{{$grupo->denominacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <?php 
							$contador = count($negocios);
					?>
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Negocio </label>
										<select class="form-control select2" name="id_unidad_negocio" id="id_unidad_negocio" style="width: 100%;">
		 									@if($contador > 1)
											 	<option value="">Seleccione Negocio</option>
												@foreach($negocios as $key=>$negocio)
													<option value="{{$negocio->id}}">{{$negocio->descripcion}}</option>
												@endforeach
											@else 
												@foreach($negocios as $key=>$negocio)
													<option value="{{$negocio->id}}" selected="selected">{{$negocio->descripcion}}</option>
												@endforeach
											@endif
										</select>
									</div>
								</div>
                    
                    
                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Exenta</label>
                            <select class="form-control select2" name="id_cuenta_exenta" id="cuenta_exenta_lc" data-value-type="number" tabindex="19"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                    @foreach ($cuentas_contables as $cuenta)
                                        @if($cuenta->asentable)
                                            <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif
                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}</option>
                                        @if($cuenta->asentable)
                                            </optgroup>
                                        @endif
                                    @endforeach
                                </select>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Gravada 10%</label>
                            <select class="form-control select2" name="id_cuenta_gravada10" id="cuenta_iva_10_lc" data-value-type="number" tabindex="20"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                    @foreach ($cuentas_contables as $cuenta)
                                        @if($cuenta->asentable)
                                            <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif
                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}</option>
                                        @if($cuenta->asentable)
                                            </optgroup>
                                        @endif
                                    @endforeach
                                </select>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Gravada 5%</label>
                            <select class="form-control select2" name="id_cuenta_gravada5" id="cuenta_iva_5_lc" data-value-type="number" tabindex="20"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                    @foreach ($cuentas_contables as $cuenta)
                                        @if($cuenta->asentable)
                                            <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif
                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}</option>
                                        @if($cuenta->asentable)
                                            </optgroup>
                                        @endif
                                    @endforeach
                                </select>
                            </select>
                        </div>
                    </div>

                   <div class="col-12">
                        <div class="form-group">
                            <label>Retención IVA</label>
                            <select class="form-control select2" name="id_retencion_iva" id="id_retencion_iva" data-value-type="number" tabindex="20"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                    @foreach ($cuentas_contables as $cuenta)
                                        @if($cuenta->asentable)
                                            <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif
                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}</option>
                                        @if($cuenta->asentable)
                                            </optgroup>
                                        @endif
                                    @endforeach
                                </select>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Retención Renta</label>
                            <select class="form-control select2" name="id_retencion_renta" id="id_retencion_renta" data-value-type="number" tabindex="20"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                    @foreach ($cuentas_contables as $cuenta)
                                        @if($cuenta->asentable)
                                            <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif
                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}</option>
                                        @if($cuenta->asentable)
                                            </optgroup>
                                        @endif
                                    @endforeach
                                </select>
                            </select>
                        </div>
                    </div>

                    <!--<div class="col-12">
                        <div class="form-group">
                            <label>Concepto</label>
                            <input type="text" class="form-control text-bold"  tabindex="22" name="concepto"
                                value="">
                        </div>
                    </div>-->
                    <input type="hidden" id="imagen" name="imagen" value="">
                    <div class="col-12">
                        <div class="col-6 col-lg-6 pt-1" id="inputImage">
                           <!-- <button type="button" form="" id="formImage" class="btn btn-info"  data-toggle="modal" data-target="#modalAdjunto">
                                <b><i class="fa fa-fw fa-image"></i> SUBIR IMAGEN</b>
                            </button>-->
                        </div>
                        @if($permiso == 1)
                            <button type="submit" class="mr-1 btn btn-info btn-lg pull-right" tabindex="23"
                                id="btnSaveMonto0"><b>Generar LV Monto CERO</b></button>
                        @endif

                        <button type="submit" class="mr-1 btn btn-success btn-lg pull-right" tabindex="23"
                            id="btnSave"><b>Confirmar</b></button>

                        <button type="button" class="mr-1 btn btn-danger btn-lg pull-right" tabindex="24"
                            id="btnVolver"><b>Volver</b></button>

                    </div>
                    <div id = "listado_numero_venta" style="display:none;"> 
<?php /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
                </form> 
            </div>			            	
       </div>
    </div>

    {{-- ========================================
   			MODAL AGREGAR TIMBRADO
   	========================================  --}}		

	   <div class="modal fade" id="modalAddTimbrado" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"  style="font-weight: 800;">
                      <i class="fa fa-file-text-o"></i>
                      TIMBRADOS
                  </h4>
                  <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
    
                <div class="modal-body">
                    <form class="row" id="formTimbrado" autocomplete="off">
                        <div class="col-12 col-sm-6 mb-1">
                                <div class="form-group">
                                    <label>Inicio</label>
                                    <input type="text" class="form-control single-picker" data-value-type="s_date" maxlength="10"   name="inicio" value="">
                                </div>
                            </div>
                                        
                        <div class="col-12 col-sm-6 mb-1">
                                <div class="form-group">
                                    <label>Vencimiento</label>
                                    <input type="text" class="form-control single-picker" data-value-type="s_date" maxlength="10"  id="timbrado_vencimiento"  name="vencimiento" value="">
                                </div>
                            </div>
 
                            <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Nro. Timbrado</label>
                                        <input type="text" class="form-control" onkeypress="return justNumbers(event);" maxlength="25" id="nro_timbrado" name="nro_timbrado"  value="">
                                    </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Denominación</label>
                                        <input type="text" class="form-control"  maxlength="50" name="denominacion"  value="">
                                    </div>
                            </div>

                            <div class="col-12">
                                   <button type="submit" class="mr-1 btn btn-success btn-lg pull-right"   id="btnConfirmarTimbrado"><b>Confirmar</b></button>  
                            </div> 
                          <input type="hidden" name="id_persona" id="id_provedor_input" value="">  
                    </form> 
                  </div>			            	
              </div>
            </div>
          </div>

</section>
@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
    $(() => {
        main();
    });

    function main() {
        calendar();
        formatNumber(2);
        $('.select2').select2();
        $('#cuenta_exenta_lc').prop('disabled',true);
        $('#cuenta_iva_10_lc').prop('disabled',true);
        $('#cuenta_iva_5_lc').prop('disabled',true);  
        $('#id_tipo_comprobante').val(2).trigger('change.select2');
        $('#id_retencion_iva').prop('disabled',true);
        $('#id_retencion_renta').prop('disabled',true); 
        $('#id_moneda').val(111).trigger('change.select2');
        $('#lc_cotizacion').prop('disabled',false);
        cotizacion();
        tipo_factura();

    } //

    $('#btnAddTimbrado').click(() => {
        modalTimbrado();
    });

    $('#id_proveedor').change(() => {
        $('#proveedor_ruc').val($('#id_proveedor option:selected').attr('data-ruc'));
        obtenerTimbrado();
        comprobarFactura();
    });   

    $('#btnSaveMonto0').click((event) => {
         comprobarDatos();
    });


    function comprobarDatos() {
        $("#formFactura").validate({
            submitHandler: function (form) {
                guardar();
            },
            validClass: "labelSuccess",
            errorClass: "labelError",
            rules: {
                id_sucursal: {
                    required: true
                },
                id_proveedor: {
                    required: true
                },
                timbrado: {
                    required: true
                },
                fecha_factura: {
                    required: true
                },
                cliente_ruc: {
                    required: true
                },
                id_tipo_comprobante: {
                    required: true
                },
                nro_comprobante: {
                    required: true
                },
                id_centro_costo: {
                    required: true
                },
                concepto: {
                    required: true
                },
                select_tipo_documento: {
                    required: true
                }
            },
            messages: {
                id_sucursal: "Seleccione una sucursal",
                id_proveedor: "Seleccione un cliente",
                timbrado: "Agregue un timbrado",
                fecha_factura: "Seleccione una fecha",
                cliente_ruc: "Agregue el RUC",
                id_tipo_comprobante: "Seleccione el tipo factura",
                nro_comprobante: "Agregue el número de comprobante",
                total_factura: "Agregue el total de factura",
                id_centro_costo: "Seleccione un centro de costo",
                concepto: "Agregue un concepto",
                select_tipo_documento : "Seleccione un tipo de documento",
            }
        });
    }  

    function obtenerTimbrado() 
    {
        $('#timbrado').empty();

        dataString = {
            id_proveedor: $('#id_proveedor').val(),
            timbrado: ''/*precarga.obj_libro.timbrado_factura*/
        };

        $.ajax({
            type: "GET",
            url: "{{route('obtenerDataCliente')}}",
            data: dataString,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrió un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                $('#btnSave').prop('disabled', false);
            },
            success: function (rsp) {
                if (rsp.data.length) {
                                $('#timbrado').empty();
                                var newOption = new Option('Seleccione Timbrado', '', false, false);
                                $('#timbrado').append(newOption);
                                $.each(rsp.data, function (key, item){
                                    var newOption = new Option('('+item.numero+') '+item.denominacion, item.id, false, false);
                                    $('#timbrado').append(newOption)
                                });
                }
                $('#cliente_ruc').val(rsp.ruc);
            } //success
        }).done(function () {
            // $('#btnSave').prop('disabled',false);
        }); //AJAX

    }

    function modalTimbrado() {
        $('#modalAddTimbrado').modal('show');
        $('#id_provedor_input').val($('#id_proveedor').val());
        $('#modalAddTimbrado').modal('show');
    }

    $('#nro_comprobante').on( 'change',function() {
        if($('#id_proveedor option:selected').attr('data-pais') == 1455){
            nro_documento = $(this).val();
            if(nro_documento.includes('-') == false){
                $('#nro_comprobante').val('');
                $.toast({ 
                        heading: 'Error',
                        text: 'El formato del comprobante no es correcto.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });
            }else{
                partesNro = nro_documento.split('-');
                console.log(partesNro[0].length); 
                console.log(partesNro[1].length); 
                console.log(partesNro[2].length); 
                if(partesNro[0].length== 3 && partesNro[1].length== 3 && partesNro[2].length==7){
                    console.log('Funciona');
                }else{
                    $('#nro_comprobante').val('');
                    $.toast({ 
                        heading: 'Error',
                        text: 'El formato del comprobante debe de ser xxx-xxx-xxxxxxx.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });

                }

            }
        }

        comprobarFactura();
    });

    $('#id_moneda').change(() => {
        cotizacion();
    });

    $('#timbrado').change(() => {
        comprobarFactura();
    });

    $('#select_tipo_documento').change(() => {
        cotizacion();
        $('#nro_comprobante').val('');
    });


    $('#select_tipo_factura').change(() => {
        tipo_factura();
    });

    $('#btnSave').click((event) => {
        validarFormularioFactura();
    });

    $('#gravada_5').on('input',()=>{
        calc_iva5();
    })

    $('#ret_iva').on('input',()=>{
        var ret_iva = clean_num($('#ret_iva').val()); 
        if(ret_iva> 0){
            $('#id_retencion_iva').prop('disabled',false);
        }
    })

    $('#ret_renta').on('input',()=>{
        var ret_renta = clean_num($('#ret_renta').val()); 
        if(ret_renta > 0){
            $('#id_retencion_renta').prop('disabled',false); 
        }   
    })

    $('#btnVolver').click(() => {
        redirectView();
    });

    $('#btnConfirmarTimbrado').click(() => {
        validarFormTimbrado();
    });

    function cotizacion() 
    {
        if ($('#id_moneda').val() === '111') 
        {
            $('#lc_cotizacion').prop('readonly', true);
            $('#lc_cotizacion').val(1);
            formatNumber(0);
        } 
        else 
        {
            if ($('#id_moneda').val() === '143') 
            {     
                $('#lc_cotizacion').prop('readonly', false);
                       // $('#lc_cotizacion').val();
           
                formatNumber(2);
                cargarCotizacionMoneda(143);
            }
            else if ($('#id_moneda').val() === '43') 
            {
                 $('#lc_cotizacion').prop('readonly', false);
                       // $('#lc_cotizacion').val();
           
                formatNumber(2);
                cargarCotizacionMoneda(43);
            }
            else if ($('#id_moneda').val() === '21') 
            {
                 $('#lc_cotizacion').prop('readonly', false);
                       // $('#lc_cotizacion').val();
           
                formatNumber(2);
                cargarCotizacionMoneda(21);
            }
        }
    }


    function tipo_factura() {

        let option = $('#select_tipo_factura').val();
        //CREDITO
        if (option === '1') {
            $('#vencimiento').prop('readonly', false);
            $('#vencimiento').datepicker('update');
            $('#vencimiento').val('');
        } else {
            $('#vencimiento').prop('readonly', true);
            $('#vencimiento').datepicker('remove');
            $('#vencimiento').val('');
        }
     }


    function validarFormTimbrado() {

        $("#formTimbrado").validate({
            submitHandler: function (form) {
                saveTimbrado();
            },
            validClass: "labelSuccess",
            errorClass: "labelError",
            rules: {
                inicio: {
                    required: true
                },
                vencimiento: {
                    required: true
                },
                nro_timbrado: {
                    required: true
                }
            },
            messages: {
                inicio: "Agregue la fecha de inicio.",
                vencimiento: "Agregue la fecha de Vencimiento.",
                nro_timbrado: "Agregue el número de timbrado"
            }
        });

    }

    function redirectView() {

        $.blockUI({
            centerY: 0,
            message: "<h2>Redirigiendo a vista de Libro Ventas...</h2>",
            css: {
                color: '#000'
            }
        });

        location.href = "{{ route('indexLibrosVentas') }}";
    }


    function comprobarFactura(){
        dataString = 'num_documento='+$('#nro_comprobante').val()+'&proveedor='+$('#id_proveedor').val()+'&timbrado='+$('#timbrado').val();
        $.ajax({
            type: "GET",
            url: "{{route('comprobarFacturaVenta')}}",
            data: dataString,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrió un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                $('#btnSave').prop('disabled', false);
            },
            success: function (rsp) {
                if(rsp != 'OK'){ 
                    $.toast({ 
                            heading: 'Error',
                            text: 'Ya existe una factura con este numero para este cliente y timbrado seleccionado.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        $('#nro_comprobante').val('');
                        $('#nro_comprobante').focus();
                }     
            }   
        });   
    }

    function saveTimbrado() 
    {
        event.preventDefault();


        $.ajax({
            async: false,
            type: "GET",
            url: "{{route('saveTimbradoVenta')}}",
            data: $('#formTimbrado').serializeJSON({
                customTypes: customTypesSerializeJSON
            }),
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
            },
            success: function (rsp) {
                if(rsp.err == true){
                    if(rsp.existe == true){

                        $.toast({
                            heading: 'Atención !',
                            text: 'El timbrado ya existe.!',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'info'
                        });

                        $("#timbrado").val(rsp.id_timbrado).trigger('change.select2');
                        $('#modalAddTimbrado').modal('hide');

                    } else {
                        obtenerTimbrado();
                        $('#modalAddTimbrado').modal('hide');
                    }

                } else {
                    $.toast({
                        heading: 'Error',
                        text: 'Ocurrio un error al almacenar el timbrado.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });
                }
                 } //success
        });

    }

    function calendar() {
        $('.single-picker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            orientation: "bottom"
        });

        $('#fecha_factura').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            orientation: "bottom"
        }).on('changeDate', function (e) {
            obtenerFechaVencimiento();
        });

        $('#vencimiento').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            orientation: "bottom"
        });
    }

    function formatNumber(coma) {
        $('.format-number-control').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            placeholder: '0',
            digits: coma,
            autoGroup: true,
            // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false
        });

    }


    function justNumbers(e) {
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
            return true;

        return /\d/.test(String.fromCharCode(keynum));
    }
    /* ===========================================================
                EVENTO KEY PRESS Y CALCULOS DE IVA
       ===========================================================*/

    // $('#exenta').on('input', function () {
        $('#exenta').change(function(){
            let grav_10 = parseFloat(clean_num($('#gravada_10').val())),
                grav_5 = parseFloat(clean_num($('#gravada_5').val())),
                exenta = parseFloat(clean_num($('#exenta').val())),
                total_factura = parseFloat(clean_num($('#total_factura').val())),
                ret_iva= parseFloat(clean_num($('#ret_iva').val())),
                ret_renta= parseFloat(clean_num($('#ret_renta').val()))
            r = 0;
            suma = 0,
            total = 0;
            if (exenta != '' & exenta > 0) {
                suma = exenta + grav_5 + grav_10+ret_iva+ret_renta;
                if (parseFloat(suma.toFixed(2)) > parseFloat(total_factura.toFixed(2))) {
                       $.toast
                          ({
                            heading: 'Error',
                            text: 'Hay diferencias con el total ingresado en la factura.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                          });
                        $('#exenta').val(0);
                }
            }

        if (exenta > 0 && exenta != '' ) {
           $('#cuenta_exenta_lc').prop('disabled', false);
        } else {
            $(`#cuenta_exenta_lc`).val('').trigger('change.select2');
            $('#cuenta_exenta_lc').prop('disabled', true);
        }

    });

   // $('#gravada_10').on('input', function () {
    $('#gravada_10').change(function(){
            let grav_10 = parseFloat(clean_num($('#gravada_10').val())),
                grav_5 = parseFloat(clean_num($('#gravada_5').val())),
                exenta = parseFloat(clean_num($('#exenta').val())),
                total_factura = parseFloat(clean_num($('#total_factura').val())),
                ret_iva= parseFloat(clean_num($('#ret_iva').val())),
                ret_renta= parseFloat(clean_num($('#ret_renta').val()))
            suma = 0,
            total = 0;

        if (grav_10 != '' & grav_10 > 0) {

            suma = exenta + grav_5 + grav_10+ret_iva+ret_renta;
            if (parseFloat(suma.toFixed(2)) > parseFloat(total_factura.toFixed(2))) {
               $.toast
                      ({
                        heading: 'Error',
                        text: 'Hay diferencias con el total ingresado en la factura.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                      });
                $('#gravada_10').val(0);
            }
        }
        calc_iva10();

    });


   // $('#gravada_5').on('input', function () {
    $('#gravada_5').change(function(){
            let grav_10 = parseFloat(clean_num($('#gravada_10').val())),
                grav_5 = parseFloat(clean_num($('#gravada_5').val())),
                exenta = parseFloat(clean_num($('#exenta').val())),
                total_factura = parseFloat(clean_num($('#total_factura').val())),
                ret_iva= parseFloat(clean_num($('#ret_iva').val())),
                ret_renta= parseFloat(clean_num($('#ret_renta').val()))
            suma = 0,
            total = 0;
        if (grav_5 != '' & grav_5 > 0) {
            suma = exenta + grav_5 + grav_10+ret_iva+ret_renta;
            if (parseFloat(suma.toFixed(2)) > parseFloat(total_factura.toFixed(2))) {
                   $.toast
                      ({
                        heading: 'Error',
                        text: 'Hay diferencias con el total ingresado en la factura.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                      });
                      $('#gravada_5').val(0);
            }
        }
        calc_iva5();

    });

    /* ================================================================================
                            LOGICA DE CALCULOS
   	        ================================================================================  */




    function calc_iva5() {
        var iva = clean_num($('#gravada_5').val());
        if (iva > 0) {
            let r = iva / 21;

             $('#cuenta_iva_5_lc').prop('disabled', false);
            // $('#btnEditIva5').prop('disabled', false);
            $('#iva_5').val(r);
        } else {
           $(`#cuenta_iva_5_lc`).val('').trigger('change.select2');
            $('#cuenta_iva_5_lc').prop('disabled', true);
            $('#iva_5').val('0');
        }



    }

    function calc_iva10() {
        let iva = clean_num($('#gravada_10').val());
        if (iva > 0) {
            let r = iva / 11;
            $('#cuenta_iva_10_lc').prop('disabled', false);
            $('#iva_10').val(r);
        } else {
           $(`#cuenta_iva_10_lc`).val('').trigger('change.select2');
           $('#cuenta_iva_10_lc').prop('disabled', true);
            $('#iva_10').val('0');
        }
    }

    $('#btnSave').click((event) => {
       // if($('#cliente option:selected').attr('data-pais') == 1455){
            validarFormularioFactura();
       /* }else{
            validarFormularioFacturaExterior()
        }*/
    });


    function validarFormularioFactura() {
        $("#formFactura").validate({
            submitHandler: function (form) {
                saveFactura();
            },
            validClass: "labelSuccess",
            errorClass: "labelError",
            rules: {
                id_sucursal: {
                    required: true
                },
                id_proveedor: {
                    required: true
                },
                timbrado: {
                    required: true
                },
                fecha_factura: {
                    required: true
                },
                cliente_ruc: {
                    required: true
                },
                id_tipo_comprobante: {
                    required: true
                },
                nro_comprobante: {
                    required: true
                },
                total_factura: {
                    required: true
                },
                id_centro_costo: {
                    required: true
                },
                concepto: {
                    required: true
                },
                select_tipo_documento: {
                    required: true
                }

            },
            messages: {
                id_sucursal: "Seleccione una sucursal",
                id_proveedor: "Seleccione un cliente",
                timbrado: "Agregue un timbrado",
                fecha_factura: "Seleccione una fecha",
                cliente_ruc: "Agregue el RUC",
                id_tipo_comprobante: "Seleccione el tipo factura",
                nro_comprobante: "Agregue el número de comprobante",
                total_factura: "Agregue el total de factura",
                id_centro_costo: "Seleccione un centro de costo",
                concepto: "Agregue un concepto",
                select_tipo_documento : "Seleccione un tipo de documento",
            }
        });
    }  

    function obtenerFechaVencimiento() 
    {

        $.ajax({
            async: false,
            type: "GET",
            url: "{{route('obtenerFechaVencimiento')}}",
            data: {
                'fecha_factura': moment($('#fecha_factura').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                'id_proveedor': $('#id_proveedor').val()
            },
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
            },
            success: function (rsp) {
                if (rsp.dias != 0) {
                    let dias = Number(rsp.dias);
                    let fecha = '';

                    if (dias < 0) {
                        dias = dias * -1;
                        fecha = moment($('#fecha_factura').val(), 'DD/MM/YYYY');
                        fecha = moment(fecha).subtract(dias, 'days').format('DD/MM/YYYY');
                    } else {
                        fecha = moment($('#fecha_factura').val(), 'DD/MM/YYYY');
                        fecha = moment(fecha).add(dias, 'd').format('DD/MM/YYYY');
                    }

                    $('#vencimiento').datepicker('update', fecha);
                }
            } //success
        });
    }

    function cargarCotizacionMoneda(id_moneda)
    {
        $.ajax({
                        type: "GET",
                        url: "{{route('getCotizacionLc')}}",
                        dataType: 'json',
                        data: {id_moneda:id_moneda},
                        error: function(jqXHR,textStatus,errorThrown)
                        {
                        },
                        success: function(rsp)
                        {
                            $('#lc_cotizacion').val(rsp.data);
                        }   
                    })
    }

    function saveFactura() 
    {
        event.preventDefault();
        $('#btnSave').prop('disabled', true);
        $('#id_proveedor').prop('disabled',false);
        $('#id_moneda').prop('disabled',false);
        if($('#exenta').val() == 0 && $('#gravada_10').val() == 0 && $('#gravada_5').val() == 0){
                    $.toast({
                            heading: 'Error',
                            text: 'Ingrese el detalle del total',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                    $('#btnSave').prop('disabled', false);
                    $('#id_proveedor').prop('disabled',true);
                    $('#id_moneda').prop('disabled',true);
        }else{
            if($('#exenta').val() == 0 || $('#exenta').val() != 0 && $('#cuenta_exenta_lc').val() != ""){
                if($('#gravada_10').val() == 0 || $('#gravada_10').val() != 0 && $('#cuenta_iva_10_lc').val() != ""){
                    if($('#gravada_5').val() == 0 ||$('#gravada_5').val() != 0 && $('#cuenta_iva_5_lc').val() != ""){
                        grav_10 = parseFloat(clean_num($('#gravada_10').val())),
                        grav_5 = parseFloat(clean_num($('#gravada_5').val())),
                        exenta = parseFloat(clean_num($('#exenta').val())),
                        total_factura = parseFloat(clean_num($('#total_factura').val())),
                        ret_iva= parseFloat(clean_num($('#ret_iva').val())),
                        ret_renta= parseFloat(clean_num($('#ret_renta').val()))   
                        saldo_manual= parseFloat(clean_num($('#saldo').val()))
                        saldo = total_factura - (grav_10 + grav_5 + exenta + ret_iva + ret_renta);

                        if(saldo_manual > total_factura){
                            $.toast({
                                        heading: 'Error',
                                        text: 'El monto del total supera el saldo de la factura',
                                        position: 'top-right',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });  
                            $('#btnSave').prop('disabled', false);
                            return;
                        }


                        if( saldo == 0){
                            guardar()
                        }else{
                            $.toast({
                                        heading: 'Error',
                                        text: 'El monto del total no coincide con la suma de las gravadas y las retenciones',
                                        position: 'top-right',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });  
                                    $('#btnSave').prop('disabled', false);
                       }          
                    }else{
                        $.toast({
                                    heading: 'Error',
                                    text: 'Ingrese una cuenta contable en "Cuenta Gravada 5%"',
                                    position: 'top-right',
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });  
                                $('#btnSave').prop('disabled', false);
                    }              
                }else{
                    $.toast({
                                heading: 'Error',
                                text: 'Ingrese una cuenta contable en "Cuenta Gravada 10%"',
                                position: 'top-right',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });  
                            $('#btnSave').prop('disabled', false);
                }                 
            }else{
                $.toast({
                            heading: 'Error',
                            text: 'Ingrese una cuenta contable en "Cuenta Exenta"',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        }); 
                        $('#btnSave').prop('disabled', false);
            } 
        }
    }

    function guardar(){
        return swal({
                    title: "GESTUR",
                    text: "¿Desea Guardar el Libro de Venta",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: true,
                                    },
                            confirm: {
                                        text: "Sí, guardar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: true
                                    }
                             }
                    }).then(isConfirm => {
                                if (isConfirm) {
                                            $('#cuenta_exenta_lc').prop('disabled',false);
                                            $('#cuenta_iva_10_lc').prop('disabled',false);
                                            $('#cuenta_iva_5_lc').prop('disabled',false);
                                            $('#id_retencion_iva').prop('disabled',false);
                                            $('#id_retencion_renta').prop('disabled',false);
                                            let dataString = $('#formFactura').serializeJSON({
                                                        customTypes: customTypesSerializeJSON
                                            });
                                            let op_num = null;
                                            $.ajax({
                                                    type: "GET",
                                                    url: "{{route('saveLibroVenta')}}",
                                                    data: dataString,
                                                    dataType: 'json',
                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                                $('#btnSave').prop('disabled', false);
                                                                $('#id_proveedor').prop('disabled',true);
                                                                $('#id_moneda').prop('disabled',true);
                                                                 $('#cuenta_exenta_lc').prop('disabled',true);
                                                                 $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                 $('#cuenta_iva_5_lc').prop('disabled',true);
                                                                 $('#id_retencion_iva').prop('disabled',true);
                                                                 $('#id_retencion_renta').prop('disabled',true);

                                                                swal("Cancelado", "Ocurrió un error en la comunicación con el servidor", "error");
                                                            },
                                                            success: function (rsp) {
                                                                if (rsp.status == 'OK') {
                                                                    libro = rsp.libro;
                                                                    libro = libro.toString();
                                                                    swal("Libro de Venta NRO: !", libro, "info")
                                                                    .then(isConfirm => {
                                                                                    redirectView();
                                                                        });
                                                                } else {
                                                                    $('#btnSave').prop('disabled', false);
                                                                    $('#id_proveedor').prop('disabled',true);
                                                                    $('#id_moneda').prop('disabled',true);
                                                                    $('#cuenta_exenta_lc').prop('disabled',true);
                                                                    $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                    $('#cuenta_iva_5_lc').prop('disabled',true);
                                                                    $('#id_retencion_iva').prop('disabled',true);
                                                                    $('#id_retencion_renta').prop('disabled',true);
                                                                    swal("Cancelado", 'Ocurrió un error al intentar generar el libro de venta',"error");
                                                                }
                                                            } //success
                                                        }).done(()=>{
                                                            $('#btnSave').prop('disabled', false);
                                                            $('#id_proveedor').prop('disabled',true);
                                                            $('#id_moneda').prop('disabled',true);
                                                        }); //AJAX
                                            } else {
                                                swal("Cancelado", "La operación fue cancelada", "error");
                                                $('#btnSave').prop('disabled', false);
                                            }
                                    });

                                    $('#btnSave').prop('disabled', false);
                                    $('#id_proveedor').prop('disabled',true);
                                    $('#id_moneda').prop('disabled',true);

                }


     function clean_num(n, bd = false) {
        if (n && bd == false) {
            n = n.replace(/[,.]/g, function (m) {
                if (m === '.') {
                    return '';
                }
                if (m === ',') {
                    return '.';
                }
            });
            return Number(n);
        }
        if (bd) {
            return Number(n);
        }
        return 0;
    } //


</script>
@endsection
