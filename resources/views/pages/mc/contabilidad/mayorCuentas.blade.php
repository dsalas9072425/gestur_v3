@extends('masters')
@section('title', 'MAYOR CUENTAS')
@section('styles')
	@parent
<style type="text/css">
	.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    cursor: pointer;
	}	
	.btnExcel  {
		display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;

}

 .btnExcel {
		padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
	}

	.btnExcel  {
	color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
	}

 .btnExcel {
		background-color: #e2076a !important; 
		font-weight: 700;
		/*margin: 0 0 10px 10px;*/
	}

	#botonExcel {
		display: inline;
	}
	#botonExcel .dt-buttons {
		display: inline;
	}






	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}



	.correcto_col {
        height: 74px;
    }
    

</style>

@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">MAYOR DE CUENTAS</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
		
		<div class="card-body">
			<form id="consultaMigracion" autocomplete="off">
					<div class="row">
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>Cuenta Contable</label>
                            <select class="form-control select2" name="id_cuenta_exenta" id="cuenta_exenta_lc" data-value-type="number" tabindex="19"  style="width: 100%;">

                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)

                                @if($cuenta->asentable)
                                <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif

                                    <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                    </option>
                                    @if($cuenta->asentable)
                                </optgroup>
                                @endif

 
                                @endforeach


                            </select>
                        </div>
                    </div>
						<div class="col-12 col-sm-6 col-md-4">  
		 					<div class="form-group">
					            <label>Fecha Desde - Hasta</label>						 
								<div class="input-group">
								    <div class="input-group-prepend" style="">
								        <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
								    </div>
								    <input type="text" class="form-control pull-right fecha" name="desde_hasta" tabindex="1" id="desde_hasta" value="">
								</div>
							</div>	
						</div>

			            <div class="col-12 col-sm-6 col-md-4">  
					        <div class="form-group">
					            <label>Moneda</label>
								<select class="form-control select2" name="idMoneda"  id="moneda" tabindex="1" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($monedas as $moneda)
									<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
									@endforeach
								</select>
					        </div>
			            </div>

						<div class="col-12">
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style=" margin: 0px;"><b>Excel</b></button>
					  		<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light text-white btn-lg pull-right mr-1"><b>Limpiar</b></button>
							<button  onclick ="consultarMayorCuentas()" class="pull-right text-center btn btn-info btn-lg mr-1"  type="button"><b>Buscar</b></button>

		  				</div>
					</div>
					

            	<div class="table-responsive mt-1">
	              <table id="listado" class="table table-striped table-bordered file-export" style="width: 100%">
	                <thead style="text-align: center">
						<tr>
							<th>Fecha</th>
							<th>Nro Comprobante</th>
							<th style="width: 40%;">Cuenta Contable</th>
							<th>Detalle</th>
							<th>Cotización</th>
							<th>Moneda</th>
							<th>Debe</th>
							<th>Haber</th>
							

			            </tr>
	                </thead>
	                <tbody style="text-align: center">
	                </tbody>

	                <tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th style="text-align:right;"> TOTALES:</th>
							<th class="text-center"></th>
							<th class="text-center"></th>
							<th class="text-center"></th>
						</tr>
					</tfoot>
	              </table>
				</div>  


			</form>	
			</div>
		</div>
	</div>
</section>
   

    

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
		<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
		<script type="text/javascript" src="{{asset('gestion/app-assets/js/currency.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>


	<script>
	const operaciones = {id_row:0, id_fila:0, tipoGuardar:true}
	const GUARANI = value => currency(value, { symbol: "", separator: '.', precision: 0 });
	var table;
	$(document).ready(function() {
			// table = mostrarLibroCompra();
		});

	const formatter = new Intl.NumberFormat('de-DE', 
		{
            currency: 'USD',
            minimumFractionDigits: 2
        });
	
	$(function() 
	{
		let locale = {
					format: 'DD/MM/YYYY',
				cancelLabel: 'Limpiar',
				applyLabel: 'Aplicar',					
					fromLabel: 'Desde',
					toLabel: 'Hasta',
					customRangeLabel: 'Seleccionar rango',
					daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										       'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										       'Diciembre']
				};

	  $('input[name="desde_hasta"]').daterangepicker({
	      autoUpdateInput: false,
	      locale: locale
	  });

	  $('input[name="desde_hasta"]').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD/MM/YYYY ') + ' - ' + picker.endDate.format('DD/MM/YYYY '));
	  });

	  $('input[name="desde_hasta"]').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });
  });
		$('.select2').select2();
		
		
		function limpiar()
		{
			$('#sucursal').val('').trigger('change.select2');
			$('#desde_hasta').val('');
		}

		function validar(data)
		{
			if(data === undefined || data === null || data === '')
				return false;

			return true;
		}

        function formatearFecha(texto)
 			{
	            if(texto != '' && texto != null)
	            {
	          		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	            } 
	            else 
	            {
	         		return '';    
            	}
        	}

//   function hallarSaldo()
// {
// 	$.ajax({
// 		type: "GET",
// 		url: "{{route('getSaldoCuenta')}}",
// 		"data": {"formSearch": $('#consultaMigracion').serializeArray() },
// 		dataType: 'json',
		
// 			success: function (rsp) 
// 			{
// 				console.log(rsp);
// 				// formatter.format(parseFloat(x.haber)
// 				//$('#saldo_footer').val(rsp.data);
// 			} 


// 	}); 

// }

		function consultarMayorCuentas()
		{
			var moneda = $('#moneda').val(); 

			 table = $("#listado").DataTable
				({
					"destroy": true,
					"order":[],
					"ajax": 
					{
						"url": "{{route('getConsultaMayorCuenta')}}",
						"type": "GET",
						"data": {"formSearch": $('#consultaMigracion').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					"columns": 
					[
						{ "data": function(x)
							{

								var fecha_documento = '';
								//if(x.fecha_base == null && x.fecha_op == null && x.fecha_normal == null){
									fecha = x.fecha_documento.split(' ');
									fecha_documento = formatearFecha(fecha[0]);
								/*}else{

									if (x.fecha_base != null) 
									{
										fecha = x.fecha_base.split(' ');
										fecha_documento = formatearFecha(fecha[0]);
									}
									if(x.fecha_normal != null){
										fecha = x.fecha_normal.split(' ');
										fecha_documento = formatearFecha(fecha[0]);
									}
									if(x.fecha_op != null){
										fecha = x.fecha_op.split(' ');
										fecha_documento = formatearFecha(fecha[0]);
									}
								}*/

								return fecha_documento;
							} },
						
						{ "data": "nro_documento" },
						{ "data": "cuenta_contable" },
						{ "data": function(x)
							{
								var concepto = '';

								if (x.concepto_detalle != null) 
								{
									concepto = x.concepto_detalle;
								}
								return concepto;
							}
						},
						{ "data": function(x){
							if(!isNaN(x.cotizacion)){
								return formatter.format(parseFloat(x.cotizacion));
							}
							return x.cotizacion;
						} },						
						{ "data": "currency_code" },						
						{ "data": function(x)
							{
								let debe = 0;
								let monto = x.columna_debe ? Number(x.columna_debe) : 0;
						
								if(moneda != 111 & monto > 0 & moneda != ''){
							
									debe = formatter.format(parseFloat(x.monto_original));
								} else {
									if (x.debe != null) 
									{
										debe = formatter.format(parseFloat(x.columna_debe));
									}
								}
							
								return debe;
							}
						},

						{ "data": function(x)
							{
								let haber = 0;
								let monto = x.columna_haber ? Number(x.columna_haber) : 0;
							
								if(moneda != 111 & monto > 0 & moneda != ''){
								
									haber = formatter.format(parseFloat(x.monto_original));
								} else {
									if (x.haber != null) 
									{
										haber = formatter.format(parseFloat(x.columna_haber));
									}
								}

								
								return haber;
							}
						},
					], 

					
				});	
				
				return table;
		}	

		$("#botonExcel").on("click", function(e)
		{ 
			e.preventDefault();
			$('#consultaMigracion').attr('method','post');
			$('#consultaMigracion').attr('action', "{{route('generarExcelMayorCuenta')}}").submit();
		});


	
		$(document).ready(function () {
			var dtable = $('#listado').DataTable();
			$('.filter-button').on('click', function () {
		        //clear global search valuess
		        dtable.search('');
		        $('.filter').each(function(){ 
		        if(this.value.length){
		          dtable.column($(this).data('columnIndex')).search(this.value);
		        }
		        });
		        dtable.draw();
		    });
		    
		    $( ".dataTables_filter input" ).on( 'keyup change',function() {
		        dtable.columns().search('');
		       //clear input values
		       $('.filter').val('');
			});	
		})    

		function clean_num(n,bd=false){
				if(n && bd == false){ 
				n = n.replace(/[,.]/g,function (m) {  
							if(m === '.'){
							return '';
							} 
							if(m === ','){
							return '.';
							} 
						});
				return Number(n);
			}
			if(bd){
				return Number(n);
			}
			return 0;
		}

		function replaceAll( text, busca, reemplaza ){
			while (text.toString().indexOf(busca) != -1)
				text = text.toString().replace(busca,reemplaza);
			return text;
		}

	</script>
@endsection
