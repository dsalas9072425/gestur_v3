@extends('masters')
@section('title', 'Panel de Control')
@section('styles')

<style type="text/css">
    .correcto_col {
        height: 74px;
    }
    					
    input.form-control:focus ,.select2-container--focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }

     .labelError{
         color: #F74343 ;
     }
</style>
@parent

@endsection
@section('content')

<section id="base-style">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Cargar Compra Fondo Fijo</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">

                <form class="row" id="formFactura" method="post" autocomplete="off">

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Proveedor</label>
                            <select class="form-control select2" name="id_cliente" data-value-type="number"  id="cliente" style="width: 100%;" required>
                                <option value="" data-ruc="">Seleccione Proveedor</option>
                                @foreach ($cliente as $cli)
                                    @php
										$ruc = $cli->documento_identidad;
										if($cli->dv){
											$ruc = $ruc."-".$cli->dv;
										}
									@endphp
                                    <option value="{{$cli->id}}" data-ruc="{{$ruc}}">{{$cli->full_data}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>RUC</label>
                            <input type="text" class="form-control" maxlength="30" id="cliente_ruc" tabindex="2" name="cliente_ruc"
                                value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <label>Timbrado</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <button type="button" id="btnAddTimbrado" title="Agregar Timbrado"
                            class="btn-mas-request btn btn-info btn-sm"><i class="fa fa-plus fa-lg"></i></button>
                          </div>
                          <select placeholder="Seleccionar o Agregar" name="timbrado" tabindex="3" id="timbrado" class="form-control select2" style="width: 93%;">
                          </select>
                        </div>
                    </div>
            
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Documento</label>
                            <select class="form-control select2" name="id_tipo_documento" data-value-type="number" tabindex="4" id="select_tipo_documento" style="width: 100%;">
                                @foreach ($tipoDocumentos as $tipoDocumento)
                                    <option value="{{$tipoDocumento->id}}">{{$tipoDocumento->descripcion}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Fecha Factura</label>
                            <div class="input-group">
                                <div class="input-group-prepend" style="">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control pull-right single-picker" name="fecha_factura"
                                    tabindex="5" data-value-type="s_date" id="fecha_factura" maxlength="10"  value="">
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Comprobante</label>
                            <select class="form-control select2" name="id_tipo_comprobante" data-value-type="number"
                                tabindex="6" id="select_tipo_factura" style="width: 100%;">
                                @foreach ($tipo_factura as $tipo)
                                <option value="{{$tipo->id}}">{{$tipo->denominacion}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Nro Comprobante</label>
                            <input type="text" class="form-control" maxlength="30" id="nro_comprobante" tabindex="7" name="nro_comprobante" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Vencimiento</label>
                            <div class="input-group">
                                <div class="input-group-prepend" style="">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control single-picker" data-value-type="s_date" readonly
                                    id="vencimiento" tabindex="8" name="vencimiento" maxlength="10"  value="">
                            </div>
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Centro Costo</label>
                            <select class="form-control select2" name="id_centro_costo"  data-value-type="number"
                                tabindex="9" style="width: 100%;">
                                @foreach ($centro as $c)
                                <option value="{{$c->id}}">{{$c->nombre}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Sucursal</label>
                            <select class="form-control select2" name="id_sucursal" data-value-type="number"
                                tabindex="10"  style="width: 100%;" required>
                                @foreach ($sucursalEmpresa as $sucursal)
                                <option value="{{$sucursal->id}}">{{$sucursal->denominacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Moneda</label>

                            <select class="form-control select2" name="id_moneda" id="id_moneda" tabindex="11"
                                data-value-type="number" style="width: 100%;" required>
                                @foreach ($currency as $div)
                                <option value="{{$div->currency_id}}">{{$div->currency_code}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Cotización</label>
                            <input type="text" maxlength="12" class="form-control format-number-control" readonly id="lc_cotizacion"
                                data-value-type="convertNumber" tabindex="12" name="cotizacion" value="">
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Total</label>
                            <input type="text" maxlength="25"class="form-control format-number-control" id="total_factura"
                                name="total_factura" data-value-type="convertNumber" tabindex="13" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Exentas</label>
                            <input type="text" class="form-control format-number-control" id="exenta" name="exenta"
                                data-value-type="convertNumber" tabindex="14" maxlength="25" value="0">
                        </div>
                    </div>
                    {{-- <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Base imponible</label>
                            <input type="text" class="form-control format-number-control"  name="base_imponible"
                                data-value-type="convertNumber" tabindex="15" value="0">
                        </div>
                    </div> --}}
                              <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 10%</label>
                            <input type="text" class="form-control format-number-control" id="gravada_10"
                                name="gravada_10" data-value-type="convertNumber" maxlength="25" tabindex="15" value="0">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 5%</label>
                            <input type="text" class="form-control format-number-control" id="gravada_5"
                                name="gravada_5" data-value-type="convertNumber" tabindex="16" value="0">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Retención IVA</label>
                            <input type="text" class="form-control format-number-control" id="ret_iva"
                                name="ret_iva" data-value-type="convertNumber" tabindex="17" value="0">
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Retención Renta</label>
                            <input type="text" class="form-control format-number-control" id="ret_renta"
                                name="ret_renta" data-value-type="convertNumber" tabindex="18" value="0">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 10%</label>
                            <input type="text" class="form-control format-number-control read-only" id="iva_10" name="iva_10" readonly data-value-type="convertNumber" maxlength="25" tabindex="19" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 5%</label>
                            <input type="text" class="form-control format-number-control read-only" id="iva_5" readonly name="iva_5" data-value-type="convertNumber" tabindex="20" value="">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="table-responsive mt-1">
                          <table id="listado" class="table table-striped table-bordered file-export" style="width: 100%">
                            <thead style="text-align: center">
                                <tr>
                                    <th></th>
                                    <th>Factura</th>
                                    <th>Código</th>
                                    <th>Fecha</th>
                                    <th><span id="concepto">Producto</span></th>
                                    <th>Moneda</th>
                                    <th>Precio</th>
                                </tr>
                            </thead>
                            <tbody style="text-align: center">
                            </tbody>
                            </tfoot>
                                <tr>
                                    <th colspan="6" style="text-align: right;">TOTAL</th>
                                    <th>
                                    <input type="hidden" id="indiceBandera" value="0">
                                    <input type="text" class="numeric" style="text-align: center;" maxlength="40"  id="totalFinal" tabindex="5" disabled value="0">
                                    </th>
                                    <th></th>
                                </tr>   
                            </tfoot>

                          </table>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Exenta</label>
                            <select class="form-control select2" name="id_cuenta_exenta" id="cuenta_exenta_lc" data-value-type="number" tabindex="19"  style="width: 100%;">

                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)

                                @if($cuenta->asentable)
                                <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif

                                    <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                    </option>
                                    @if($cuenta->asentable)
                                </optgroup>
                                @endif


                                @endforeach


                            </select>
                        </div>
                    </div>

                    <!--<div class="col-1">
                        <button id="btnEditExenta" onclick="modalModificarCuenta(2)" disabled style="margin-top:27px;" type="button" class="btn btn-info text-white">
                            <i class="fa fa-edit"></i>
                        </button>
                        </div>-->
                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Gravada 10%</label>
                            <select class="form-control select2" name="id_cuenta_gravada10" id="cuenta_iva_10_lc" data-value-type="number" tabindex="20"  style="width: 100%;">

                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)

                                @if($cuenta->asentable)
                                <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif

                                    <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                    </option>
                                    @if($cuenta->asentable)
                                </optgroup>
                                @endif


                                @endforeach


                            </select>
                        </div>
                    </div>
                    <!--<div class="col-1">
                    <button id="btnEditIva10" onclick="modalModificarCuenta(1)" disabled style="margin-top:27px;" type="button" class="btn btn-info text-white">
                        <i class="fa fa-edit"></i>
                    </button>
                    </div>-->

                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Gravada 5%</label>
                            <select class="form-control select2" name="id_cuenta_gravada5" id="cuenta_iva_5_lc" data-value-type="number" tabindex="20"  style="width: 100%;">

                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)

                                @if($cuenta->asentable)
                                <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif

                                    <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                    </option>
                                    @if($cuenta->asentable)
                                </optgroup>
                                @endif


                                @endforeach


                            </select>
                        </div>
                    </div>
                   <div class="col-12">
                        <div class="form-group">
                            <label>Retención IVA</label>
                            <select class="form-control select2" name="id_retencion_iva" id="id_retencion_iva" data-value-type="number" tabindex="20"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)
                                    @if($cuenta->asentable)
                                    <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif

                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                        </option>
                                        @if($cuenta->asentable)
                                    </optgroup>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Retención Renta</label>
                            <select class="form-control select2" name="id_retencion_renta" id="id_retencion_renta" data-value-type="number" tabindex="20"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)

                                    @if($cuenta->asentable)
                                    <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif

                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                        </option>
                                        @if($cuenta->asentable)
                                    </optgroup>
                                    @endif


                                @endforeach


                            </select>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Concepto</label>
                            <input type="text" class="form-control text-bold"  tabindex="22" name="concepto"
                                value="">
                        </div>
                    </div>
                    <input type="hidden" id="imagen" name="imagen">
                    <div class="col-12">
                        <div class="col-6 col-lg-6 pt-1" id="inputImage">
                            <button type="button" form="" id="formImage" class="btn btn-info"  data-toggle="modal" data-target="#modalAdjunto">
                                <b><i class="fa fa-fw fa-image"></i> SUBIR IMAGEN</b>
                            </button>
                        </div>

                    <div class="col-12">
                        <button type="submit" class="mr-1 btn btn-success btn-lg pull-right" tabindex="23"
                            id="btnSave"><b>Confirmar</b></button>

                        <button type="button" class="mr-1 btn btn-danger btn-lg pull-right" tabindex="24"
                            id="btnVolver"><b>Volver</b></button>

                    </div>
                    <div id = "listado_numero_venta" style="display:none;"> 

                    <input type="hidden" id="input_canje" name="id_canje" value="">
                </form>
            </div>
        </div>
    </div>
</section>




{{-- ========================================
   			MODAL AGREGAR TIMBRADO
   	========================================  --}}		

	   <div class="modal fade" id="modalAddTimbrado" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"  style="font-weight: 800;">
                      <i class="fa fa-file-text-o"></i>
                      TIMBRADOS
                  </h4>
                  <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
    
                <div class="modal-body">
                <form class="row" id="formTimbrado" autocomplete="off">


                        <div class="col-12 col-sm-6 mb-1">
                                <div class="form-group">
                                    <label>Inicio</label>
                                    <input type="text" class="form-control single-picker" data-value-type="s_date" maxlength="10"   name="inicio" value="">
                                </div>
                            </div>
                    
                    
                        <div class="col-12 col-sm-6 mb-1">
                                <div class="form-group">
                                    <label>Vencimiento</label>
                                    <input type="text" class="form-control single-picker" data-value-type="s_date" maxlength="10"  id="timbrado_vencimiento"  name="vencimiento" value="">
                                </div>
                            </div>

 
                            <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Nro. Timbrado</label>
                                        <input type="text" class="form-control" onkeypress="return justNumbers(event);" maxlength="25" id="nro_timbrado" name="nro_timbrado"  value="">
                                    </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Denominación</label>
                                        <input type="text" class="form-control"  maxlength="50" name="denominacion"  value="">
                                    </div>
                            </div>

                            <div class="col-12">
                                   <button type="submit" class="mr-1 btn btn-success btn-lg pull-right"   id="btnConfirmarTimbrado"><b>Confirmar</b></button>  
                            </div> 
                          <input type="hidden" name="id_persona" id="id_provedor_input" value="">  
                  </form> 
                  	
                  </div>			            	
                                
            
              </div>
            </div>
          </div>


<!--======================================================
        MODAL DE ADJUNTO IMAGEN
========================================================== -->
    <!-- /.content -->
    <div id="modalAdjunto" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 60%;margin-top: 13%;">
        <!-- Modal content-->
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto </h2>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                            <div class="content" id='output'>
                            </div>  
                             <form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('lcUpload')}}" autocomplete="off">
                                <div class="form-group">
                                  <div class="row">
                                     <div class="col-12 col-sm-12 col-md-12">
                                        <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="file" class="form-control" name="image" id="image" style="margin-left: 3%; width: 96%"/>  <h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: PNG, JPG y JPEG - La imagen para perfil de usuario sera valido en el proximo logueo de usuario.</b>
                                        </h4>
                                        <div id="validation-errors"></div>
                                      </div> 
                                    </div>  
                                  </div>  
                              </form>
                            <div class="row">
                                <div class= "col-12">
                                    <button type="button" id="btnCancelarVSTour" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

                                    <button type="button" id="btnAceptarVoucher" class="btn btn-success pull-right"  data-dismiss="modal">Aceptar</button>
                                        
                                </div>
                            </div>
                        
                    </div>
                </div>  
            </div>  
        </div>  

@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
    $(() => {
        main();
    });


    //INICIO
    function main() {
        calendar();
        formatNumber(2);
        $('.select2').select2();
        // select2MultipleTimbrado();
        libroCompraPreCarga();
       // obtenerTimbrado();
        $('#cuenta_exenta_lc').prop('disabled',true);
        $('#cuenta_iva_10_lc').prop('disabled',true);
        $('#cuenta_iva_5_lc').prop('disabled',true);  
        $('#id_retencion_iva').prop('disabled',true);
        $('#id_retencion_renta').prop('disabled',true); 

        $('#id_tipo_comprobante').val(2).trigger('change.select2');
        $('#id_moneda').val(111).trigger('change.select2');
        $('#lc_cotizacion').prop('disabled',false);
        cotizacion();
        tipo_factura();

    } //


    $('#cliente').on( 'change',function() {
     /*   alert($(this).val());*/
        if($(this).val() != ""){
            $('#cliente_ruc').val($('#cliente option:selected').attr('data-ruc'));
            proveedor_nombre = $('#cliente option:selected').text();
            proveedor = proveedor_nombre.split(' - ');
            $('#proveedor_nombre').val(proveedor[0]);
            // getRetencion();
            obtenerTimbrado();
            listadoFacturas();
            comprobar();
        }
    });
    function comprobar(){
        dataString = 'num_documento='+$('#nro_comprobante').val()+'&proveedor='+$('#cliente').val();
            $.ajax({
                type: "GET",
                url: "{{route('comprobarFactura')}}",
                data: dataString,
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                    $.toast({
                        heading: 'Error',
                        text: 'Ocurrió un error en la comunicación con el servidor.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });
                    $('#btnSave').prop('disabled', false);
                },
                success: function (rsp) {
                    if(rsp != 'OK'){ 
                        $.toast({ 
                                heading: 'Error',
                                text: 'Ya existe una factura con este numero para este proveedor.',
                                position: 'top-right',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });
                            $('#nro_comprobante').val('');
                            $('#nro_comprobante').focus();
                    }     
                }   
            });      
        }

    $('#nro_comprobante').on( 'change',function() {
        dataString = 'num_documento='+$(this).val()+'&proveedor='+$('#cliente').val();
        $.ajax({
            type: "GET",
            url: "{{route('comprobarFactura')}}",
            data: dataString,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrió un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                $('#btnSave').prop('disabled', false);
            },
            success: function (rsp) {
                if(rsp != 'OK'){ 
                    $.toast({ 
                            heading: 'Error',
                            text: 'Ya existe una factura con este numero para este proveedor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        $('#nro_comprobante').val('');
                        $('#nro_comprobante').focus();
                }     
            }   
        });      
    });


    //CONSTANTES
    const operaciones = {
        moneda: 0,
        save_timbrado_id : 0
    }

    const precarga = {
        num_factura: '',
        timbrado: 0,
        importe: 0,
        id_cliente: 0
    }

    /* ===========================================================
                EVENTO JQUERY
       ===========================================================*/


    $('#btnAddTimbrado').click(() => {
        modalTimbrado();
    });


  /*  $('#cliente').on( 'change',function() {
        if($(this).val() != ""){
            $('#cliente_ruc').val($('#cliente option:selected').attr('data-ruc'));
            obtenerTimbrado();
            listadoFacturas();
        }
    });*/


    $('#id_moneda').change(() => {
        cotizacion();
        //cuentaPredeterminada();
        listadoFacturas();
    });


    $('#select_tipo_factura').change(() => {
        tipo_factura();
    });

    $('#btnSave').click((event) => {
        validarFormularioFactura();
    });

    $('#gravada_5').on('input',()=>{
        calc_iva5();
    })

    $('#btnVolver').click(() => {
        redirectView();
    });

    $('#btnConfirmarTimbrado').click(() => {
        validarFormTimbrado();
    });

    $('#ret_iva').on('input',()=>{
        var ret_iva = clean_num($('#ret_iva').val()); 
        console.log(ret_iva);
        if(ret_iva> 0){
            $('#id_retencion_iva').prop('disabled',false);
        }
    })

    $('#ret_renta').on('input',()=>{
        var ret_renta = clean_num($('#ret_renta').val()); 
         console.log(ret_renta);
        if(ret_renta > 0){
            $('#id_retencion_renta').prop('disabled',false); 
        }   
    })




    /* ===========================================================
                EVENTO KEY PRESS Y CALCULOS DE IVA
       ===========================================================*/

  $('#exenta').on('input', function () {
            let grav_10 = clean_num($('#gravada_10').val()),
                grav_5 = clean_num($('#gravada_5').val()),
                exenta = clean_num($('#exenta').val()),
                total_factura = clean_num($('#total_factura').val()),
                ret_iva= clean_num($('#ret_iva').val()),
                ret_renta= clean_num($('#ret_renta').val()),
                r = 0;
            suma = 0,
            total = 0;
            if (exenta != '' & exenta > 0) {
                suma = exenta + grav_5 + grav_10+ret_iva+ret_renta;
                console.log('suma', suma);

                if (suma > total_factura) {
                       $.toast
                          ({
                            heading: 'Error',
                            text: 'Hay diferencias con el total ingresado en la factura.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                          });
                }
            }

        if (exenta > 0 && exenta != '' ) {
           $('#cuenta_exenta_lc').prop('disabled', false);
        } else {
            $(`#cuenta_exenta_lc`).val('').trigger('change.select2');
            $('#cuenta_exenta_lc').prop('disabled', true);
        }

    });

    $('#gravada_10').on('input', function () {

            let grav_10 = clean_num($('#gravada_10').val()),
                grav_5 = clean_num($('#gravada_5').val()),
                exenta = clean_num($('#exenta').val()),
                total_factura = clean_num($('#total_factura').val()),
                ret_iva= clean_num($('#ret_iva').val()),
                ret_renta= clean_num($('#ret_renta').val()),
            suma = 0,
            total = 0;


        if (grav_10 != '' & grav_10 > 0) {
            suma = exenta + grav_5 + grav_10+ret_iva+ret_renta;
            console.log('suma', suma);

            if (suma > total_factura) {
               $.toast
                      ({
                        heading: 'Error',
                        text: 'Hay diferencias con el total ingresado en la factura.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                      });
            }
        }
        calc_iva10();

    });


    $('#gravada_5').on('input', function () {

            let grav_10 = clean_num($('#gravada_10').val()),
                grav_5 = clean_num($('#gravada_5').val()),
                exenta = clean_num($('#exenta').val()),
                total_factura = clean_num($('#total_factura').val()),
                ret_iva= clean_num($('#ret_iva').val()),
                ret_renta= clean_num($('#ret_renta').val()),
            suma = 0,
            total = 0;
        if (grav_5 != '' & grav_5 > 0) {
            suma = exenta + grav_5 + grav_10+ret_iva+ret_renta;
            console.log('suma', suma);

            if (suma > total_factura) {
                   $.toast
                      ({
                        heading: 'Error',
                        text: 'Hay diferencias con el total ingresado en la factura.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                      });
            }
        }
        calc_iva5();

    });
 
//////////////////////////////////////////////////////////////////////////
 function listadoFacturas() 
    {
     table = $("#listado").DataTable
                ({
                    "destroy": true,
                    "ajax": 
                    {
                        "url": "{{route('mostrarListadoFacturasFF')}}",
                        "type": "GET",
                        "data": {"formSearch": $('#formFactura').serializeArray() },
                        error: function(jqXHR,textStatus,errorThrown)
                        {
                            $.toast
                            ({
                                heading: 'Error',
                                text: 'Ocurrió un error en la comunicación con el servidor.',
                                position: 'top-right',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });

                        }               
                    },
                    "aaSorting":[[0,"desc"]],
                    "columns": 
                            [
                                { "data": function(x)
                                    {
                                        btn = `<input class="form-check" id="bas_`+x.id+`" type="checkbox" onclick="calcular(`+x.precio+`,2,`+x.id+`,'`+x.id_proforma_venta+`_`+x.tipo+`_`+x.precio+`_`+x.id+`')" data="`+x.precio+`" data-tipo="`+x.tipo+`" data-id="2" name="indicador[][valor]" value="`+x.id_proforma_venta+`_`+x.tipo+`_`+x.precio+`_`+x.id+`">`;
                                        return btn;
                                    } 
                                },
                                 { "data": function(x)
                                    {
                                        nro_factura = ''
                                        if(jQuery.isEmptyObject(x.nro_factura) == false){
                                            nro_factura = x.nro_factura;
                                        }
                                         return nro_factura;
                                    } 
                                },
                                 { "data": function(x)
                                    {
                                        cod_confirmacion = ''
                                        if(jQuery.isEmptyObject(x.cod_confirmacion) == false){
                                            cod_confirmacion = x.cod_confirmacion;
                                        }
                                         return cod_confirmacion;
                                    } 
                                },

                                { "data": "fecha" },

                                { "data": "producto" },

                                { "data": "moneda" },
                                
                                { "data": function(x)
                                    {
                                        return formatter.format(parseFloat(x.precio));
                                    }
                                }
                            ], 
                    "initComplete": function (settings, json) {
                    // $.unblockUI();
                     $('[data-toggle="tooltip"]').tooltip();
                     
                }
                    
                }); 
                //calcular();
            }
////////////////////////////////////////////////////////////////////////////////////////////////////

        const formatter = new Intl.NumberFormat('de-DE', 
        {
            currency: 'USD',
            minimumFractionDigits: 2
        });


     /* ===========================================================
                PRECARGA DE DATOS - CASO FACTURA CANJE
       ===========================================================*/
    function libroCompraPreCarga() 
    {
        let obj_libro = {!!json_encode($forma_cobro_data) !!};
        precarga.obj_libro = obj_libro;

        if (Object.keys(obj_libro).length > 0 ) {

            $('#input_canje').val(obj_libro.id);
            $('#cliente').val(obj_libro.id_cliente).trigger('change.select2');
            $('#cliente').prop('disabled', true);

            // $('#fecha_factura').val(obj_libro.fecha_operacion_format);
            // $('#fecha_factura').prop('readonly', true);
            // $('#fecha_factura').unbind();
            $('#lc_cotizacion').val(obj_libro.cotizacion);
            $('#lc_cotizacion').prop('readonly', true);

            // $('#nro_comprobante').val(obj_libro.num_factura);
            // $('#nro_comprobante').prop('readonly', true);
            $('#total_factura').val(obj_libro.importe_pago);
            $('#total_factura').prop('readonly', true);
            // $('#gravada_10').val(obj_libro.importe_pago);
            // $('#gravada_10').prop('readonly', true);
            $('#id_moneda').val(obj_libro.id_moneda).trigger('change.select2');
            $('#id_moneda').prop('disabled', true);


            if (obj_libro.id_moneda == 111) {
                formatNumber(0);
                operaciones.moneda = 111;
            } else {
                formatNumber(2);
                operaciones.moneda = 143;
            }

            // $('#exenta').val(obj_libro.exenta);
            // $('#exenta').prop('readonly', true);
            $('#cliente_ruc').prop('readonly', true);

            // $('#nro_timbrado').val(obj_libro.timbrado_factura);
            // $('#nro_timbrado').prop('readonly', true);
            $('#id_provedor_input').val($('#cliente').val());
            // $('#modalAddTimbrado').modal('show');
            calc_iva10();
        }
    }

  function calcular(id, tipo,base,id_proforma_venta) 
    {
        total =  parseFloat(clean_num($('#totalFinal').val()));
        if($('#bas_'+base).is(':checked')) { 
            totalInicio = total + id;
            $('#listado_numero_venta').append('<input type="text" id="id_'+base+'" name="valor[]" value="'+id_proforma_venta+'">');
        }else{
            totalInicio = total - id;
            $('#id_'+base).remove();
        }
        $('#totalFinal').val(totalInicio);

     }

    /* ================================================================================
                            AJAX - OBTENER INFOS
   	        ================================================================================  */

    function saveTimbrado() 
    {
        event.preventDefault();


        $.ajax({
            async: false,
            type: "GET",
            url: "{{route('saveTimbrado')}}",
            data: $('#formTimbrado').serializeJSON({
                customTypes: customTypesSerializeJSON
            }),
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
            },
            success: function (rsp) {
                if(rsp.err == true){
                    if(rsp.existe == true){

                        $.toast({
                            heading: 'Atención !',
                            text: 'El timbrado ya existe.!',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'info'
                        });

                        $("#timbrado").val(rsp.id_timbrado).trigger('change.select2');
                        $('#modalAddTimbrado').modal('hide');

                    } else {
                        operaciones.save_timbrado_id = rsp.id_timbrado;
                        obtenerTimbrado();
                        $('#modalAddTimbrado').modal('hide');
                    }

                } else {
                    $.toast({
                        heading: 'Error',
                        text: 'Ocurrio un error al almacenar el timbrado.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });
                }
               
           
              
               


            } //success
        });

    }


    function obtenerFechaVencimiento() 
    {

        $.ajax({
            async: false,
            type: "GET",
            url: "{{route('obtenerFechaVencimiento')}}",
            data: {
                'fecha_factura': moment($('#fecha_factura').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                'id_proveedor': $('#cliente').val()
            },
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
            },
            success: function (rsp) {
                console.log('eee', rsp);

                if (rsp.dias != 0) {
                    let dias = Number(rsp.dias);
                    let fecha = '';

                    if (dias < 0) {
                        dias = dias * -1;
                        fecha = moment($('#fecha_factura').val(), 'DD/MM/YYYY');
                        fecha = moment(fecha).subtract(dias, 'days').format('DD/MM/YYYY');
                    } else {
                        fecha = moment($('#fecha_factura').val(), 'DD/MM/YYYY');
                        fecha = moment(fecha).add(dias, 'd').format('DD/MM/YYYY');
                    }

                    $('#vencimiento').datepicker('update', fecha);
                }
            } //success
        });
    }

    function saveFactura() 
    {

        event.preventDefault();
        $('#btnSave').prop('disabled', true); 
        $('#cliente').prop('disabled',false);
        $('#id_moneda').prop('disabled',false);
        
        if(parseFloat(clean_num($('#total_factura').val())) - (parseFloat(clean_num($('#exenta').val()))+parseFloat(clean_num($('#gravada_10').val()))+ parseFloat(clean_num($('#gravada_5').val()))+ parseFloat(clean_num($('#ret_iva').val()))+ parseFloat(clean_num($('#ret_renta').val()))) == 0){
            if($('#exenta').val() == 0 || $('#exenta').val() != 0 && $('#cuenta_exenta_lc').val() != ""){
                if($('#gravada_10').val() == 0 || $('#gravada_10').val() != 0 && $('#cuenta_iva_10_lc').val() != ""){
                    if($('#gravada_5').val() == 0 ||$('#gravada_5').val() != 0 && $('#cuenta_iva_5_lc').val() != ""){
                        if($('#totalFinal').val() > 0){ 
                            if($('#imagen').val() != ""){
                                if($('#total_factura').val() == $('#totalFinal').val()){
                                    return swal({
                                                title: "GESTUR",
                                                text: "¿Desea Guardar la Factura?",
                                                showCancelButton: true,
                                                buttons: {
                                                        cancel: {
                                                                text: "No",
                                                                value: null,
                                                                visible: true,
                                                                className: "btn-warning",
                                                                closeModal: false,
                                                            },
                                                        confirm: {
                                                                text: "Sí, guardar",
                                                                value: true,
                                                                visible: true,
                                                                className: "",
                                                                closeModal: false
                                                            }
                                                        }
                                            }).then(isConfirm => {
                                                    if (isConfirm) {
                                                        $('#cuenta_exenta_lc').prop('disabled',false);
                                                        $('#cuenta_iva_10_lc').prop('disabled',false);
                                                        $('#cuenta_iva_5_lc').prop('disabled',false);
                                                        $('#id_retencion_iva').prop('disabled',false);
                                                        $('#id_retencion_renta').prop('disabled',false);                                                        let dataString = $('#formFactura').serializeJSON({
                                                                customTypes: customTypesSerializeJSON
                                                            });
                                                            let op_num = null;

                                                            $.ajax({
                                                                    type: "GET",
                                                                    url: "{{route('saveLibroCompraFijo')}}",
                                                                    data: dataString,
                                                                    dataType: 'json',
                                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                                        $('#btnSave').prop('disabled', false);
                                                                        $('#cliente').prop('disabled',true);
                                                                        $('#id_moneda').prop('disabled',true);
                                                                        $('#cuenta_exenta_lc').prop('disabled',true);
                                                                        $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                        $('#cuenta_iva_5_lc').prop('disabled',true);
                                                                        $('#id_retencion_iva').prop('disabled',true);
                                                                        $('#id_retencion_renta').prop('disabled',true);
                                                                        swal("Cancelado", "Ocurrió un error en la comunicación con el servidor "+rsp.msj, "error");
                                                                    },
                                                                    success: function (rsp) {
                                                                        console.log(rsp.err);
                                                                        if (rsp.err == true) {
                                                                            op_num = rsp.op;
                                                                            op_num = op_num.toString();
                                                                            console.log(op_num.toString());

                                                                            if (rsp.op != 0) {
                                                                                swal("OP NRO: !", op_num, "info")
                                                                                .then(isConfirm => {
                                                                                        if (isConfirm) {redirectView();}
                                                                                    });
                                                                                
                                                                            } else {
                                                                                swal("Redirigiendo....", 'Sera redirigido a listado de Libro Compra', "info");
                                                                                redirectView();    
                                                                            }
                                                                        } else {
                                                                            $('#btnSave').prop('disabled', false);
                                                                            $('#cliente').prop('disabled',true);
                                                                            $('#id_moneda').prop('disabled',true);
                                                                            $('#cuenta_exenta_lc').prop('disabled',true);
                                                                            $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                            $('#cuenta_iva_5_lc').prop('disabled',true);
                                                                            $('#id_retencion_iva').prop('disabled',true);
                                                                            $('#id_retencion_renta').prop('disabled',true);
                                                                            
                                                                            swal("Cancelado", 'Ocurrió un error al intentar almacenar la factura ',"error");
                                                                        }
                                                                    } //success
                                                                }).done(()=>{
                                                                    $('#btnSave').prop('disabled', false);
                                                                    $('#cliente').prop('disabled',true);
                                                                    $('#id_moneda').prop('disabled',true);
                                                                //  $('#cuenta_exenta_lc').prop('disabled',true);
                                                                //   $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                //  $('#cuenta_iva_5_lc').prop('disabled',true);
                                                                }); //AJAX
                                                    } else {
                                                        swal("Cancelado", "La operación fue cancelada", "error");
                                                        $('#btnSave').prop('disabled', false);
                                                    }
                                            });

                                            $('#btnSave').prop('disabled', false);
                                            $('#cliente').prop('disabled',true);
                                            $('#id_moneda').prop('disabled',true);
                                }else{
                                    $.toast({
                                            heading: 'Error',
                                            text: 'El monto de las facturas seleccionadas y el total de la factura no coinciden',
                                            position: 'top-right',
                                            showHideTransition: 'fade',
                                            icon: 'error'
                                        });  
                                    $('#btnSave').prop('disabled', false);
                                }
                            }else{
                                $.toast({
                                        heading: 'Error',
                                        text: 'Ingrese una imagen',
                                        position: 'top-right',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });
                                $('#btnSave').prop('disabled', false);
                            }    
                         }else{
                            return swal({
                                            title: "GESTUR",
                                            text: "¿Desea Guardar la Factura?",
                                            showCancelButton: true,
                                            buttons: {
                                                    cancel: {
                                                            text: "No",
                                                            value: null,
                                                            visible: true,
                                                            className: "btn-warning",
                                                            closeModal: false,
                                                        },
                                                    confirm: {
                                                            text: "Sí, guardar",
                                                            value: true,
                                                            visible: true,
                                                            className: "",
                                                            closeModal: false
                                                        }
                                                    }
                                        }).then(isConfirm => {
                                                if (isConfirm) {
                                                        $('#cuenta_exenta_lc').prop('disabled',false);
                                                        $('#cuenta_iva_10_lc').prop('disabled',false);
                                                        $('#cuenta_iva_5_lc').prop('disabled',false);
                                                        let dataString = $('#formFactura').serializeJSON({
                                                            customTypes: customTypesSerializeJSON
                                                         });
                                                         let op_num = null;

                                                        $.ajax({
                                                                type: "GET",
                                                                url: "{{route('saveLibroCompraFijo')}}",
                                                                data: dataString,
                                                                dataType: 'json',
                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                    $('#btnSave').prop('disabled', false);
                                                                    $('#cliente').prop('disabled',true);
                                                                    $('#id_moneda').prop('disabled',true);
                                                                     $('#cuenta_exenta_lc').prop('disabled',true);
                                                                     $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                     $('#cuenta_iva_5_lc').prop('disabled',true);

                                                                    swal("Cancelado", "Ocurrió un error en la comunicación con el servidor", "error");
                                                                },
                                                                success: function (rsp) {
                                                                    console.log(rsp.err);
                                                                    if (rsp.err == true) {

                                                                        op_num = rsp.op;
                                                                        op_num = op_num.toString();
                                                                        console.log(op_num.toString());

                                                                        if (rsp.op != 0) {
                                                                            swal("OP NRO: !", op_num, "info")
                                                                            .then(isConfirm => {
                                                                                    if (isConfirm) {redirectView();}
                                                                                });
                                                                            
                                                                        } else {
                                                                            swal("Redirigiendo....", 'Sera redirigido a listado de Libro Compra', "info");
                                                                            redirectView();    
                                                                        }
                                                                    } else {
                                                                        $('#btnSave').prop('disabled', false);
                                                                        $('#cliente').prop('disabled',true);
                                                                        $('#id_moneda').prop('disabled',true);
                                                                        $('#cuenta_exenta_lc').prop('disabled',true);
                                                                        $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                        $('#cuenta_iva_5_lc').prop('disabled',true);

                                                                        swal("Cancelado", 'Ocurrió un error al intentar almacenar la factura. '+rsp.msj[0],"error");
                                                                    }
                                                                } //success
                                                            }).done(()=>{
                                                                $('#btnSave').prop('disabled', false);
                                                                $('#cliente').prop('disabled',true);
                                                                $('#id_moneda').prop('disabled',true);
                                                              //  $('#cuenta_exenta_lc').prop('disabled',true);
                                                             //   $('#cuenta_iva_10_lc').prop('disabled',true);
                                                              //  $('#cuenta_iva_5_lc').prop('disabled',true);
                                                            }); //AJAX
                                                } else {
                                                    swal("Cancelado", "La operación fue cancelada", "error");
                                                    $('#btnSave').prop('disabled', false);
                                                }
                                        });

                                        $('#btnSave').prop('disabled', false);
                                        $('#cliente').prop('disabled',true);
                                        $('#id_moneda').prop('disabled',true);
                     }       
                    }else{
                         $.toast({
                                heading: 'Error',
                                text: 'Ingrese una cuenta contable en "Cuenta Gravada 5%"',
                                position: 'top-right',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });  
                         $('#btnSave').prop('disabled', false);
                        }              
                }else{
                     $.toast({
                            heading: 'Error',
                            text: 'Ingrese una cuenta contable en "Cuenta Gravada 10%"',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });  
                     $('#btnSave').prop('disabled', false);
                     }                 
              }else{
                 $.toast({
                        heading: 'Error',
                        text: 'Ingrese una cuenta contable en "Cuenta Exenta"',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    }); 
                    $('#btnSave').prop('disabled', false);
                }                     
        }else{
             $.toast({
                    heading: 'Error',
                    text: 'La suma de los montos no corresponde al total. Inténtelo nuevamente',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
              $('#btnSave').prop('disabled', false);
              $('#cliente').prop('disabled',true);
              $('#id_moneda').prop('disabled',true);

        }                 
    } //

    function obtenerTimbrado() 
    {

        $('#timbrado').empty();

        dataString = {
            id_proveedor: $('#cliente').val(),
            timbrado: precarga.obj_libro.timbrado_factura
        };

        $.ajax({
            type: "GET",
            url: "{{route('obtenerDataCliente.lc')}}",
            data: dataString,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrió un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                $('#btnSave').prop('disabled', false);
            },
            success: function (rsp) {
                console.log(rsp);
                if (rsp.data.length) {
                    // $('#timbrado').select2({
                    //     data: rsp.data
                    // });

                    $('#timbrado').empty();

                               var newOption = new Option('Seleccione Timbrado', '', false, false);
                                $('#timbrado').append(newOption);

                                $.each(rsp.data, function (key, item){
                                var newOption = new Option('('+item.nro_timbrado+') '+item.denominacion, item.id, false, false);
                                $('#timbrado').append(newOption)
                            });

                    // $('#btnAddTimbrado').prop('disabled', true);
                }

                $('#cliente_ruc').val(rsp.ruc);


            } //success
        }).done(function () {
            // $('#btnSave').prop('disabled',false);
        }); //AJAX

    }



    /* ================================================================================
                            LOGICA DE CALCULOS
   	        ================================================================================  */




    function calc_iva5() {
        let iva = clean_num($('#gravada_5').val());
        if (iva > 0) {
            let r = iva / 21;

            if (operaciones.moneda == 111) {
                r = r.toFixed(0);
            } else {
                r = r.toFixed(2);
            }
            console.log('IVANXA'+iva);

             $('#cuenta_iva_5_lc').prop('disabled', false);
            // $('#btnEditIva5').prop('disabled', false);
            $('#iva_5').val(r);
        } else {
           $(`#cuenta_iva_5_lc`).val('').trigger('change.select2');
            $('#cuenta_iva_5_lc').prop('disabled', true);
            $('#iva_5').val('0');
        }



    }

    function calc_iva10() {
        console.log('Ingreso');
        // getRetencion();
        let iva = clean_num($('#gravada_10').val());

        console.log('IVA', iva);
        if (iva > 0) {
            let r = iva / 11;

            if (operaciones.moneda == 111) {
                r = r.toFixed(0);
            } else {
                r = r.toFixed(2);
            }
            $('#cuenta_iva_10_lc').prop('disabled', false);
            //$('#btnEditIva10').prop('disabled', false);
            $('#iva_10').val(r);
        } else {
           $(`#cuenta_iva_10_lc`).val('').trigger('change.select2');
           $('#cuenta_iva_10_lc').prop('disabled', true);
            $('#iva_10').val('0');
        }


    }



    /* ================================================================================
                            FUNCIONES AUX
       ================================================================================ */

    function clean_num(n, bd = false) {

        if (n && bd == false) {
            n = n.replace(/[,.]/g, function (m) {
                if (m === '.') {
                    return '';
                }
                if (m === ',') {
                    return '.';
                }
            });
            return Number(n);
        }
        if (bd) {
            return Number(n);
        }
        return 0;

    } //

    function redirectView() {

        $.blockUI({
            centerY: 0,
            message: "<h2>Redirigiendo a vista de Libro Compra...</h2>",
            css: {
                color: '#000'
            }
        });

        location.href = "{{ route('indexLibrosFondoFijo') }}";
    }

    function loadProccess() {
        $.blockUI({
            centerY: 0,
            message: "<h2>Procesando...</h2>",
            css: {
                color: '#000'
            }
        });
    }

    function formatNumber(coma) {
        $('.format-number-control').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            placeholder: '0',
            digits: coma,
            autoGroup: true,
            // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false
        });

    }


    function justNumbers(e) {
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
            return true;

        return /\d/.test(String.fromCharCode(keynum));
    }


    function calendar() {
        $('.single-picker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            orientation: "bottom"
        });

        $('#fecha_factura').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            orientation: "bottom"
        }).on('changeDate', function (e) {
            obtenerFechaVencimiento();
        });

        $('#vencimiento').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            orientation: "bottom"
        });
    }

    $('.numeric').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
             // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false,
            oncleared: function () { self.Value(''); }
        });



    function cotizacion() 
    {
        if ($('#id_moneda').val() === '111') 
        {
            $('#lc_cotizacion').prop('readonly', true);
            $('#lc_cotizacion').val(1);
            formatNumber(0);
            operaciones.moneda = 111;
        } 
        else 
        {
            if ($('#id_moneda').val() === '143') 
            {     
                $('#lc_cotizacion').prop('readonly', false);
                       // $('#lc_cotizacion').val();
           
                formatNumber(2);
                operaciones.moneda = 143;
                cargarCotizacionMoneda(143);
            }
            else
            {
                 $('#lc_cotizacion').prop('readonly', false);
                       // $('#lc_cotizacion').val();
           
                formatNumber(2);
                operaciones.moneda = 43;
                cargarCotizacionMoneda(43);
            }
        }
    }

    function cargarCotizacionMoneda(id_moneda)
    {
        $.ajax({
                        type: "GET",
                        url: "{{route('getCotizacionLc')}}",
                        dataType: 'json',
                        data: {id_moneda:id_moneda},
                        error: function(jqXHR,textStatus,errorThrown)
                        {
                        },
                        success: function(rsp)
                        {
                            console.log(rsp.data);
                            $('#lc_cotizacion').val(rsp.data);
                        }   
                    })
    }


    function tipo_factura() {

        let option = $('#select_tipo_factura').val();
        //CREDITO
        if (option === '1') {
            $('#vencimiento').prop('readonly', false);
            $('#vencimiento').datepicker('update');
            $('#vencimiento').val('');
        } else {
            $('#vencimiento').prop('readonly', true);
            $('#vencimiento').datepicker('remove');
            $('#vencimiento').val('');
        }

    }

  /*  function select2MultipleTimbrado() {
        $('#timbrado').select2({
            theme: 'bootstrap4',
            width: 'style'
        });
    }*/


      /* ================================================================================
   			                LOGICA DE VALIDACIONES DE FORM 
   	        ================================================================================  */

    function validarFormularioFactura() {

        $("#formFactura").validate({
            submitHandler: function (form) {
                saveFactura();
            },
            validClass: "labelSuccess",
            errorClass: "labelError",
            rules: {
                id_sucursal: {
                    required: true
                },
                id_cliente: {
                    required: true
                },
                timbrado: {
                    required: true
                },
                fecha_factura: {
                    required: true
                },
                cliente_ruc: {
                    required: true
                },
                id_tipo_comprobante: {
                    required: true
                },
                nro_comprobante: {
                    required: true
                },
                total_factura: {
                    required: true
                },
                id_centro_costo: {
                    required: true
                },
                concepto: {
                    required: true
                }
            },
            messages: {
                id_sucursal: "Seleccione una sucursal",
                id_cliente: "Seleccione un cliente",
                timbrado: "Agregue un timbrado",
                fecha_factura: "Seleccione una fecha",
                cliente_ruc: "Agregue el RUC",
                id_tipo_comprobante: "Seleccione el tipo factura",
                nro_comprobante: "Agregue el número de comprobante",
                total_factura: "Agregue el total de factura",
                // id_cuenta_exenta : "Seleccione la cuenta exenta",
                // id_cuenta_gravada : "",
                id_centro_costo: "Seleccione un centro de costo",
                concepto: "Agregue un concepto"

            }
           });

    }



    function validarFormTimbrado() {

        $("#formTimbrado").validate({
            submitHandler: function (form) {
                saveTimbrado();
            },
            validClass: "labelSuccess",
            errorClass: "labelError",
            rules: {
                inicio: {
                    required: true
                },
                vencimiento: {
                    required: true
                },
                nro_timbrado: {
                    required: true
                }
            },
            messages: {
                inicio: "Agregue la fecha de inicio.",
                vencimiento: "Agregue la fecha de Vencimiento.",
                nro_timbrado: "Agregue el número de timbrado"
            }
        });

    }

/*===========================CUENTA PREDETERMINADA==================================*/
//aqui
    

    function modalModificarCuenta(opt)
    {
      return  swal({
                    title: "GESTUR",
                    text: "¿Está seguro que desea modificar la cuenta contable?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Modificar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            swal("Éxito", 'Cuenta Contable Habilitada', "success");
                            if(opt == 1){
                                $('#cuenta_iva_10_lc').prop('disabled',false);
                            }
                            if(opt == 2){
                                $('#cuenta_exenta_lc').prop('disabled',false);
                            }
                            if(opt == 3){
                                $('#cuenta_iva_5_lc').prop('disabled',false);
                            }
                        } else {
                            swal("Cancelado", '', "error");
                        }
                });
                              
    }

  





    /* ================================================================================
   			                MODAL Y AVISOS
   	        ================================================================================  */
    function modalNumOp(num) {
        swal("Atención!", `El numero de Op creado es ${num}`, "info")
            .then((value) => {
                redirectView();
            });;
    }




    // Overlay with custom color
    $('.block-custom-overlay').on('click', function () {
        var block_ele = $(this).closest('.card');
        $(block_ele).block({
            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#E91E63',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                color: '#fff',
                backgroundColor: 'transparent'
            }
        });
    });


    function modalTimbrado() {

        if (Object.keys(precarga.obj_libro).length == 0) {
            $('#formTimbrado')[0].reset();
        }

        $('#modalAddTimbrado').modal('show');
        $('#id_provedor_input').val($('#cliente').val());
        $('#modalAddTimbrado').modal('show');
    }


(function($) {
   
  jQuery.isEmpty = function(obj){
    var isEmpty = false;
 
    if (typeof obj == 'undefined' || obj === null || obj === ''){
      isEmpty = true;
    }      
       
    if (typeof obj == 'number' && isNaN(obj)){
      isEmpty = true;
    }
       
    if (obj instanceof Date && isNaN(Number(obj))){
      isEmpty = true;
    }
       
    return isEmpty;
  }
 
})(jQuery);

//=====================IMAGEN FORM ========================//
var options = { 
                                beforeSubmit:  showRequest,
                                success: showResponse,
                                dataType: 'json' 
                        }; 

                $('body').delegate('#image','change', function(){
                    //OBTENER TAMAÑO DE LA IMAGEN
                    var input = document.getElementById('image');
                    var file = input.files[0];
                    var tamaño = file.size;

                        if(tamaño > 0){

                        var tamaño = file.size/1000;
                        if(tamaño > 3000){
                            $("#image").val('');
                            $("#validation-errors").empty();
                            $("#validation-errors").append('<div class="alert alert-error"><strong>El tamaño de la imagen supera el limite de 3MB</strong><div>'); 
                        } else {

                            $('.cargandoImg').show();
                            $('#upload').ajaxForm(options).submit();  
                        }

                        } else {
                             $("#image").val('');
                             $("#validation-errors").empty();
                             $("#validation-errors").append('<div class="alert alert-error"><strong>Existe un error con el archivo</strong><div>'); 
                        }
                                        
                                });


                  function showRequest(formData, jqForm, options) { 
                   
                                    $("#validation-errors").hide().empty();
                                    return true; 
                                  } 


                  function showResponse(response, statusText, xhr, $form)  { 
                        $('.cargandoImg').hide();
                        if(response.success == false)
                          {
                       $("#image").val('');
                       $("#validation-errors").append('<div class="alert alert-error"><strong>'+ response.errors +'</strong><div>');
                       $("#validation-errors").show();

                        } else {
                             $("#imagen").val(response.archivo); 
                          
                          var file_name = response.archivo;
                                $('#output').html('');


                                divImagen = `
                                    <div class="row ${file_name}">
                                        <div class="col-auto">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <img class="card-img-top" style="max-width:150px;" src="{{asset('adjuntoLc')}}/${file_name}" alt="Adjunto Iamgen Perfil">
                                                        <p><a href="{{asset('adjuntoLc')}}/${file_name}" class="btn btn-info" target="_black"><b>VER</b></a></p>
										                <a href="#" class="card-title" style="font-size: 0.7rem; color: red;" onclick="eliminarImagen('${file_name}')"><b>Eliminar</b></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    `;

                   
                     $("#output").append(divImagen);
                                 
                             $("#image").prop('disabled',true);
                        }
                   }

                   function eliminarImagen(id, archivo){
                        // $("#btn-imagen").on("click", function(e){
                            $.ajax({
                                type: "GET",
                                url: "{{route('fileAdjuntoEliminarLC')}}",//fileDelDTPlus
                                dataType: 'json',
                                data: {
                                            dataId: id,
                                            dataFile:archivo
                                            },
                                success: function(rsp){
                                    $("#imagen").val("");
                                    $('#image').prop('disabled',false);
                                    $("#output").html('');
                                }
                        //   })      

                            });
                        }  


</script>
@endsection
