@extends('masters')
@section('title', 'Panel de Control')
@section('styles')

<style type="text/css">
    .correcto_col {
        height: 74px;
    }
    					
    input.form-control:focus ,.select2-container--focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }

     .labelError{
         color: #F74343 ;
     }
</style>
@parent

@endsection
@section('content')

<section id="base-style">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Cargar Compra</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">

                <form class="row" id="formFactura" method="post" autocomplete="off">

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Proveedor</label>
                            <select class="form-control select2" name="id_cliente" data-value-type="number" tabindex="1"  id="cliente" style="width: 100%;" required>
                                <option value="" data-ruc="">Seleccione Proveedor</option>    
                                @foreach ($cliente as $cli)
                                    @php
										$ruc = $cli->documento_identidad;
										if($cli->dv){
											$ruc = $ruc."-".$cli->dv;
										}
									@endphp

                                    <option value="{{$cli->id}}" data-ruc="{{$ruc}}" data-pais="{{$cli->id_pais}}">{{$cli->full_data}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>RUC</label>
                            <input type="text" class="form-control" maxlength="30" id="cliente_ruc" tabindex="2" name="cliente_ruc"
                                value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                            <label>Timbrado</label>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <button type="button" id="btnAddTimbrado" title="Agregar Timbrado"
                                class="btn-mas-request btn btn-info btn-sm"><i class="fa fa-plus fa-lg"></i></button>
                              </div>
                              <select placeholder="Seleccionar o Agregar" name="timbrado" tabindex="3" style="width: 90%;" id="timbrado" class="form-control select2">
                              </select>
                            </div>
                    </div>
            
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Documento</label>
                            <select class="form-control select2" name="id_tipo_documento" data-value-type="number" tabindex="4" id="select_tipo_documento" style="width: 100%;">
                                @foreach ($tipoDocumentos as $tipoDocumento)
                                    <option value="{{$tipoDocumento->id}}">{{$tipoDocumento->descripcion}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Fecha Factura</label>
                            <div class="input-group">
                                <div class="input-group-prepend" style="">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control pull-right single-picker" name="fecha_factura"
                                    tabindex="5" data-value-type="s_date" id="fecha_factura" maxlength="10"  value="">
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Comprobante</label>
                            <select class="form-control select2" name="id_tipo_comprobante" data-value-type="number"
                                tabindex="6" id="select_tipo_factura" style="width: 100%;">
                                @foreach ($tipo_factura as $tipo)
                                <option value="{{$tipo->id}}">{{$tipo->denominacion}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3" style="display:none">
                        <div class="form-group">
                            <label>Costo de Venta</label>
                            <select class="form-control select2" name="costo_venta" id="costo_venta" style="width: 100%;">
                                <option value="true">SI</option>
                                <option value="false">NO</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="proveedor_nombre" name="proveedor_nombre">

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Nro Comprobante</label>
                            <input type="text" class="form-control" maxlength="30" id="nro_comprobante" tabindex="7" name="nro_comprobante" value="">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Nro. de Venta/Pedido</label>
                            <input type="text" class="form-control" maxlength="30" id="nro_venta_pedido" tabindex="7" name="nro_venta_pedido" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Vencimiento</label>
                            <div class="input-group">
                                <div class="input-group-prepend" style="">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control single-picker" data-value-type="s_date" readonly
                                    id="vencimiento" tabindex="8" name="vencimiento" maxlength="10"  value="">
                            </div>
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Centro Costo</label>
                            <select class="form-control select2" name="id_centro_costo"  data-value-type="number"
                                tabindex="9" style="width: 100%;">
                                @foreach ($centro as $c)
                                <option value="{{$c->id}}">{{$c->nombre}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Sucursal</label>
                            <select class="form-control select2" name="id_sucursal" data-value-type="number"
                                tabindex="10"  style="width: 100%;" required>
                                @foreach ($sucursalEmpresa as $sucursal)
                                <option value="{{$sucursal->id}}">{{$sucursal->denominacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Moneda</label>

                            <select class="form-control select2" name="id_moneda" id="id_moneda" tabindex="11"
                                data-value-type="number" style="width: 100%;" required>
                                @foreach ($currency as $div)
                                <option value="{{$div->currency_id}}">{{$div->currency_code}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Cotización</label>
                            <input type="text" maxlength="10" class="form-control format-number-control" readonly id="lc_cotizacion"
                                data-value-type="convertNumber" tabindex="12" name="cotizacion" value="">
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Total</label>
                            <input type="text" maxlength="25" class="form-control format-number-control" id="total_factura"
                                name="total_factura" data-value-type="convertNumber" tabindex="13" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Exentas</label>
                            <input type="text" class="form-control format-number-control" id="exenta" name="exenta"
                                data-value-type="convertNumber" tabindex="14" maxlength="25" value="0">
                        </div>
                    </div>
                    {{-- <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Base imponible</label>
                            <input type="text" class="form-control format-number-control"  name="base_imponible"
                                data-value-type="convertNumber" tabindex="15" value="0">
                        </div>
                    </div> --}}
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 10%</label>
                            <input type="text" class="form-control format-number-control" id="gravada_10"
                                name="gravada_10" data-value-type="convertNumber" maxlength="25" tabindex="15" value="0">
                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 5%</label>
                            <input type="text" class="form-control format-number-control" id="gravada_5"
                                name="gravada_5" data-value-type="convertNumber" tabindex="16" value="0">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 10%</label>
                            <input type="text" class="form-control format-number-control read-only" id="iva_10" name="iva_10" readonly data-value-type="convertNumber" maxlength="25" tabindex="19" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 5%</label>
                            <input type="text" class="form-control format-number-control read-only" id="iva_5" readonly name="iva_5" data-value-type="convertNumber" tabindex="20" value="">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Base de la Retención</label>
                            <input type="text" class="form-control format-number-control" id="base_retencion"
                                name="base_retencion" data-value-type="convertNumber" tabindex="16" value="0">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Retención IVA (0.9%)</label>
                            <input type="text" class="form-control format-number-control" id="ret_iva"
                                name="ret_iva" data-value-type="convertNumber" tabindex="17" value="0">
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Retención Renta (1%)</label>
                            <input type="text" class="form-control format-number-control" id="ret_renta"
                                name="ret_renta" data-value-type="convertNumber" tabindex="18" value="0">
                        </div>
                    </div>



                    <div class="col-12 col-sm-4 col-md-3"> 
                        <div class="form-group">
                            <label>Pasajero</label>
                            <input type="text" class="form-control read-only" id="pasajero" name="pasajero" value="">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3"> 
                        <div class="form-group">
                            <label>Código/Referencia</label>
                            <input type="text" class="form-control read-only" id="codigo_referencia" name="codigo_referencia" value="">
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="table-responsive mt-1">
                          <table id="listado" class="table table-striped table-bordered file-export" style="width: 100%">
                            <thead style="text-align: center">
                                <tr>
                                    <th></th>
                                    <th>Factura</th>
                                    <th>Código</th>
                                    <th>Fecha</th>
                                    <th><span id="concepto">Producto</span></th>
                                    <th>Moneda</th>
                                    <th>Precio</th>
                                    <th>Tipo</th>
                                </tr>
                            </thead>
                            <tbody style="text-align: center">
                            </tbody>
                            </tfoot>
                                <tr>
                                    <th colspan="6" style="text-align: right;">TOTAL</th>
                                    <th>
                                        <input type="hidden" id="indiceBandera" value="0">
                                        <input type="text" class="numeric" style="text-align: center;" maxlength="40"  id="totalFinal" tabindex="5" disabled value="0">
                                    </th>
                                    <th>
                                    <input type="hidden" class="numeric" style="text-align: center;" maxlength="40"  id="totalExenta" tabindex="5" disabled value="0">
                                    <input type="hidden" class="numeric" style="text-align: center;" maxlength="40"  id="totalGravada" tabindex="5" disabled value="0">
                                    </th>
                                </tr>   
                            </tfoot>

                          </table>
                        </div>
                    </div>
                    <input type="hidden" class="numeric" style="text-align: center;" maxlength="40"  id="totalComparacion" tabindex="5" disabled value="0">

                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Exenta</label>
                            <select class="form-control select2" name="id_cuenta_exenta" id="cuenta_exenta_lc" data-value-type="number" tabindex="19"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)
                                    @if($cuenta->asentable)
                                        <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif
                                    <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}</option>
                                    @if($cuenta->asentable)
                                        </optgroup>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <!--<div class="col-1">
                        <button id="btnEditExenta" onclick="modalModificarCuenta(2)" disabled style="margin-top:27px;" type="button" class="btn btn-info text-white">
                            <i class="fa fa-edit"></i>
                        </button>
                        </div>-->
                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Gravada 10%</label>
                            <select class="form-control select2" name="id_cuenta_gravada10" id="cuenta_iva_10_lc" data-value-type="number" tabindex="20"  style="width: 100%;">

                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)

                                @if($cuenta->asentable)
                                <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif

                                    <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                    </option>
                                    @if($cuenta->asentable)
                                </optgroup>
                                @endif


                                @endforeach


                            </select>
                        </div>
                    </div>
                    <!--<div class="col-1">
                    <button id="btnEditIva10" onclick="modalModificarCuenta(1)" disabled style="margin-top:27px;" type="button" class="btn btn-info text-white">
                        <i class="fa fa-edit"></i>
                    </button>
                    </div>-->

                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Gravada 5%</label>
                            <select class="form-control select2" name="id_cuenta_gravada5" id="cuenta_iva_5_lc" data-value-type="number" tabindex="20"  style="width: 100%;">

                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)

                                @if($cuenta->asentable)
                                <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                    @endif

                                    <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                    </option>
                                    @if($cuenta->asentable)
                                </optgroup>
                                @endif


                                @endforeach


                            </select>
                        </div>
                    </div>

                   <div class="col-12">
                        <div class="form-group">
                            <label>Retención IVA</label>
                            <select class="form-control select2" name="id_retencion_iva" id="id_retencion_iva" data-value-type="number" tabindex="20"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)
                                    @if($cuenta->asentable)
                                    <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif

                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                        </option>
                                        @if($cuenta->asentable)
                                    </optgroup>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Retención Renta</label>
                            <select class="form-control select2" name="id_retencion_renta" id="id_retencion_renta" data-value-type="number" tabindex="20"  style="width: 100%;">
                                <option value="">Seleccione una cuenta</option>
                                @foreach ($cuentas_contables as $cuenta)

                                    @if($cuenta->asentable)
                                    <optgroup label="({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}">
                                        @endif

                                        <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}}) {{$cuenta->descripcion}}
                                        </option>
                                        @if($cuenta->asentable)
                                    </optgroup>
                                    @endif


                                @endforeach


                            </select>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Concepto</label>
                            <input type="text" class="form-control text-bold"  tabindex="22" name="concepto"
                                value="">
                        </div>
                    </div>
                    <input type="hidden" id="imagen" name="imagen" value="">
                    <div class="col-12">
                        <div class="col-6 col-lg-6 pt-1" id="inputImage">
                            <button type="button" form="" id="formImage" class="btn btn-info"  data-toggle="modal" data-target="#modalAdjunto">
                                <b><i class="fa fa-fw fa-image"></i> SUBIR IMAGEN</b>
                            </button>
                        </div>

                        <button type="submit" class="mr-1 btn btn-success btn-lg pull-right" tabindex="23"
                            id="btnSave"><b>Confirmar</b></button>

                        <button type="button" class="mr-1 btn btn-danger btn-lg pull-right" tabindex="24"
                            id="btnVolver"><b>Volver</b></button>

                    </div>
                    <div id = "listado_numero_venta" style="display:none;"> 

                    </div>   
                    <input type="hidden" id="input_canje" name="id_canje" value="">
                    <input type="hidden" id="diferencia" name="diferencia" value="0">

                </form>
            </div>
        </div>
    </div>
</section>




{{-- ========================================
   			MODAL AGREGAR TIMBRADO
   	========================================  --}}		

	   <div class="modal fade" id="modalAddTimbrado" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"  style="font-weight: 800;">
                      <i class="fa fa-file-text-o"></i>
                      TIMBRADOS
                  </h4>
                  <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
    
                <div class="modal-body">
                <form class="row" id="formTimbrado" autocomplete="off">
                        <div class="col-12 col-sm-6 mb-1">
                                <div class="form-group">
                                    <label>Inicio</label>
                                    <input type="text" class="form-control single-picker" data-value-type="s_date" maxlength="10"   name="inicio" value="">
                                </div>
                            </div>
                                        
                        <div class="col-12 col-sm-6 mb-1">
                                <div class="form-group">
                                    <label>Vencimiento</label>
                                    <input type="text" class="form-control single-picker" data-value-type="s_date" maxlength="10"  id="timbrado_vencimiento"  name="vencimiento" value="">
                                </div>
                            </div>
 
                            <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Nro. Timbrado</label>
                                        <input type="text" class="form-control" onkeypress="return justNumbers(event);" maxlength="25" id="nro_timbrado" name="nro_timbrado"  value="">
                                    </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label>Denominación</label>
                                        <input type="text" class="form-control"  maxlength="50" name="denominacion"  value="">
                                    </div>
                            </div>

                            <div class="col-12">
                                   <button type="submit" class="mr-1 btn btn-success btn-lg pull-right"   id="btnConfirmarTimbrado"><b>Confirmar</b></button>  
                            </div> 
                          <input type="hidden" name="id_persona" id="id_provedor_input" value="">  
                  </form> 
                  	
                  </div>			            	
                       
              </div>
            </div>
          </div>

<!--======================================================
        MODAL DE ADJUNTO IMAGEN
========================================================== -->
    <!-- /.content -->
    <div id="modalAdjunto" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 60%;margin-top: 13%;">
        <!-- Modal content-->
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto </h2>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                            <div class="content" id='output'>
                            </div>  
                             <form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('lcUpload')}}" autocomplete="off">
                                <div class="form-group">
                                  <div class="row">
                                     <div class="col-12 col-sm-12 col-md-12">
                                        <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="file" class="form-control" name="image" id="image" style="margin-left: 3%; width: 96%"/>  <h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: PNG, JPG y JPEG - La imagen para perfil de usuario sera valido en el proximo logueo de usuario.</b>
                                        </h4>
                                        <div id="validation-errors"></div>
                                      </div> 
                                    </div>  
                                  </div>  
                              </form>
                            <div class="row">
                                <div class= "col-12">
                                    <button type="button" id="btnCancelarVSTour" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

                                    <button type="button" id="btnAceptarVoucher" class="btn btn-success pull-right"  data-dismiss="modal">Aceptar</button>
                                        
                                </div>
                            </div>
                        
                    </div>
                </div>  
            </div>  
        </div>  

@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
    var dia_limite = Number('{{$fecha_libre}}');
    var permisoCambioFecha = {{$permiso_modificar_fecha}};
    
    $(() => {
        main();
    });


    //INICIO
    function main() {
        calendar();
        formatNumber(2);
        $('.select2').select2();
        // select2MultipleTimbrado();
        libroCompraPreCarga();
        //obtenerTimbrado();
        $('#cuenta_exenta_lc').prop('disabled',true);
        $('#cuenta_iva_10_lc').prop('disabled',true);
        $('#cuenta_iva_5_lc').prop('disabled',true);  
        $('#id_tipo_comprobante').val(2).trigger('change.select2');
        $('#id_retencion_iva').prop('disabled',true);
        $('#id_retencion_renta').prop('disabled',true); 

        $('#id_moneda').val(111).trigger('change.select2');
        $('#lc_cotizacion').prop('disabled',false);
         cotizacion();
        tipo_factura();
        calcular_campo();
    } //


    //CONSTANTES
    const operaciones = {
        moneda: 0,
        save_timbrado_id : 0
    }

    const precarga = {
        num_factura: '',
        timbrado: 0,
        importe: 0,
        id_cliente: 0
    }


    /* ===========================================================
                EVENTO JQUERY
       ===========================================================*/


    $('#btnAddTimbrado').click(() => {
        modalTimbrado();
    });


    $('#cliente').change(() => {
        $('#cliente_ruc').val($('#cliente option:selected').attr('data-ruc'));
        proveedor_nombre = $('#cliente option:selected').text();
        proveedor = proveedor_nombre.split(' - ');
        $('#proveedor_nombre').val(proveedor[0]);
        // getRetencion();
        obtenerTimbrado();
        listadoFacturas();
        dataString = 'num_documento='+$('#nro_comprobante').val()+'&proveedor='+$('#cliente').val()+'&timbrado='+$('#timbrado').val();
        $.ajax({
            type: "GET",
            url: "{{route('comprobarFactura')}}",
            data: dataString,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrió un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                $('#btnSave').prop('disabled', false);
            },
            success: function (rsp) {
                if(rsp != 'OK'){ 
                    $.toast({ 
                            heading: 'Error',
                            text: 'Ya existe una factura con este numero para este proveedor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        $('#nro_comprobante').val('');
                        $('#nro_comprobante').focus();
                }     
            }   
        });      

    });


    $('#id_moneda').change(() => {
        cotizacion();
        listadoFacturas();
        //cuentaPredeterminada();
    });

    $('#select_tipo_documento').change(() => {
        cotizacion();
        listadoFacturas();
        calcular_campo();
        $('#nro_comprobante').val('');
    });


    $('#select_tipo_factura').change(() => {
        tipo_factura();
    });

    $('#btnSave').click((event) => {
        console.log('1');
        if($('#cliente option:selected').attr('data-pais') == 1455){
            console.log('validarFormularioFactura');
            validarFormularioFactura();
        }else{
            console.log('validarFormularioFacturaExterior');
            validarFormularioFacturaExterior()
        }
    });

    $('#gravada_5').on('input',()=>{
        calc_iva5();
    })

    $('#ret_iva').on('input',()=>{
        var ret_iva = clean_num($('#ret_iva').val()); 
        if(ret_iva> 0){
            $('#id_retencion_iva').prop('disabled',false);
        }
    })

    $('#ret_renta').on('input',()=>{
        var ret_renta = clean_num($('#ret_renta').val()); 
        if(ret_renta > 0){
            $('#id_retencion_renta').prop('disabled',false); 
        }   
    })

    $('#btnVolver').click(() => {
        redirectView();
    });

    $('#btnConfirmarTimbrado').click(() => {
        validarFormTimbrado();
    });





    /* ===========================================================
                EVENTO KEY PRESS Y CALCULOS DE IVA
       ===========================================================*/

    // $('#exenta').on('input', function () {
    $('#exenta').change(function(){
            let grav_10 = parseFloat(clean_num($('#gravada_10').val())),
                grav_5 = parseFloat(clean_num($('#gravada_5').val())),
                exenta = parseFloat(clean_num($('#exenta').val())),
                total_factura = parseFloat(clean_num($('#total_factura').val())),
                ret_iva= parseFloat(clean_num($('#ret_iva').val())),
                ret_renta= parseFloat(clean_num($('#ret_renta').val()))
            r = 0;
            suma = 0,
            total = 0;
            if (exenta != '' & exenta > 0) {
                suma = exenta + grav_5 + grav_10+ret_iva+ret_renta;
                if (parseFloat(suma.toFixed(2)) > parseFloat(total_factura.toFixed(2))) {
                       $.toast
                          ({
                            heading: 'Error',
                            text: 'Hay diferencias con el total ingresado en la factura.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                          });
                        $('#exenta').val(0);
                }
            }

        if (exenta > 0 && exenta != '' ) {
           $('#cuenta_exenta_lc').prop('disabled', false);
        } else {
            $(`#cuenta_exenta_lc`).val('').trigger('change.select2');
            $('#cuenta_exenta_lc').prop('disabled', true);
        }

    });

   // $('#gravada_10').on('input', function () {
    $('#gravada_10').change(function(){
            let grav_10 = parseFloat(clean_num($('#gravada_10').val())),
                grav_5 = parseFloat(clean_num($('#gravada_5').val())),
                exenta = parseFloat(clean_num($('#exenta').val())),
                total_factura = parseFloat(clean_num($('#total_factura').val())),
                ret_iva= parseFloat(clean_num($('#ret_iva').val())),
                ret_renta= parseFloat(clean_num($('#ret_renta').val()))
            suma = 0,
            total = 0;

        if (grav_10 != '' & grav_10 > 0) {

            suma = exenta + grav_5 + grav_10+ret_iva+ret_renta;
            if (parseFloat(suma.toFixed(2)) > parseFloat(total_factura.toFixed(2))) {
               $.toast
                      ({
                        heading: 'Error',
                        text: 'Hay diferencias con el total ingresado en la factura.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                      });
                $('#gravada_10').val(0);
            }
        }
        calc_iva10();

    });


   // $('#gravada_5').on('input', function () {
    $('#gravada_5').change(function(){
            let grav_10 = parseFloat(clean_num($('#gravada_10').val())),
                grav_5 = parseFloat(clean_num($('#gravada_5').val())),
                exenta = parseFloat(clean_num($('#exenta').val())),
                total_factura = parseFloat(clean_num($('#total_factura').val())),
                ret_iva= parseFloat(clean_num($('#ret_iva').val())),
                ret_renta= parseFloat(clean_num($('#ret_renta').val()))
            suma = 0,
            total = 0;
        if (grav_5 != '' & grav_5 > 0) {
            suma = exenta + grav_5 + grav_10+ret_iva+ret_renta;
            if (parseFloat(suma.toFixed(2)) > parseFloat(total_factura.toFixed(2))) {
                   $.toast
                      ({
                        heading: 'Error',
                        text: 'Hay diferencias con el total ingresado en la factura.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                      });
                      $('#gravada_5').val(0);
            }
        }
        calc_iva5();

    });
 
//////////////////////////////////////////////////////////////////////////
 function listadoFacturas() 
    {

            if($('#select_tipo_documento').val() == 3){
                $('#concepto').html('Concepto');
            }else{
                $('#concepto').html('Producto');
            }

            if($('#select_tipo_documento').val() == 3){
                table = $("#listado").DataTable
                        ({
                            "destroy": true,
                            "ajax": 
                            {
                                "url": "{{route('mostrarListadoFacturas')}}",
                                "type": "GET",
                                "data": {"formSearch": $('#formFactura').serializeArray() },
                                error: function(jqXHR,textStatus,errorThrown)
                                {
                                    $.toast
                                    ({
                                        heading: 'Error',
                                        text: 'Ocurrió un error en la comunicación con el servidor.',
                                        position: 'top-right',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });

                                }               
                            },
                            "aaSorting":[[0,"desc"]],
                            "columns": 
                            [
                                { "data": function(x)
                                    {
                                        btn = `<input class="form-check" type="checkbox" onclick="calcular(`+x.importe+`,1,`+x.id+`)" data="`+x.importe+`" data-valor="`+x.costo_exenta+`_`+x.costo_gravada+`_0" data-tipo="LC" data-id="1" name="lc"  id="bas_`+x.id+`" value="`+x.id_proforma_venta+`_`+x.tipo+`_`+x.precio+`_`+x.id+`">`;
                                        return btn;
                                    } 
                                },
                                { "data": function(x)
                                    {
                                        nro_factura = ''
                                        if(jQuery.isEmptyObject(x.factura) == false){
                                            nro_factura = x.factura.nro_factura;
                                        }else{
                                            nro_factura = x.nro_documento;
                                        }   
                                         return nro_factura;
                                    } 
                                },
                             
                                 { "data": function(x)
                                    {
                                        cod_confirmacion = ''
                                        if(jQuery.isEmptyObject(x.cod_confirmacion) == false){
                                            cod_confirmacion = x.cod_confirmacion;
                                        }
                                         return cod_confirmacion;
                                    } 
                                },


                                { "data": "fecha" },

                                { "data": "concepto" },

                                { "data": "currency.currency_code" },
                                
                                { "data": function(x)
                                    {

                                        if(x.currency.currency_code == 'PYG'){
                                            return formatto.format(parseFloat(x.importe));
                                        }else{
                                            return formatter.format(parseFloat(x.saldo));
                                        }
                                    }
                                },  
                                { "data": function(x)
                                    { 
                                        return 'LC';
                                    }
                                },  
                               
                            ], 
                            "initComplete": function (settings, json) {
                            // $.unblockUI();
                             $('[data-toggle="tooltip"]').tooltip();
                             
                        }
                            
                        }); 

            }else{    
                 table = $("#listado").DataTable
                        ({
                            "destroy": true,
                            "ajax": 
                            {
                                "url": "{{route('mostrarListadoFacturas')}}",
                                "type": "GET",
                                "data": {"formSearch": $('#formFactura').serializeArray() },
                                error: function(jqXHR,textStatus,errorThrown)
                                {
                                    $.toast
                                    ({
                                        heading: 'Error',
                                        text: 'Ocurrió un error en la comunicación con el servidor.',
                                        position: 'top-right',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    });

                                }               
                            },
                            "aaSorting":[[0,"desc"]],
                            "columns": 
                            [
                                { "data": function(x)
                                    {
                                        btn = `<input class="form-check" id="bas_`+x.id+`" type="checkbox" onclick="calcular(`+x.precio+`,2,`+x.id+`,'`+x.id_proforma_venta+`_`+x.tipo+`_`+x.precio+`_`+x.id+`')" data="`+x.precio+`" data-tipo="`+x.tipo+`" data-id="2" data-valor="`+x.exento+`_`+x.gravada+`_`+x.costo_proveedor+`"  name="indicador[][valor]" value="`+x.id_proforma_venta+`_`+x.tipo+`_`+x.precio+`_`+x.id+`">`;
                                        return btn;
                                    } 
                                },
                                 { "data": function(x)
                                    {
                                        nro_factura = ''
                                        if(jQuery.isEmptyObject(x.nro_factura) == false){
                                            nro_factura = x.nro_factura;
                                        }
                                         return nro_factura;
                                    } 
                                },
                                 { "data": function(x)
                                    {
                                        cod_confirmacion = ''
                                        if(jQuery.isEmptyObject(x.cod_confirmacion) == false){
                                            cod_confirmacion = x.cod_confirmacion;
                                        }
                                         return cod_confirmacion;
                                    } 
                                },

                                { "data": function(x)
                                    {
                                        var fecha = '';

                                        if (x.fecha != null) 
                                        {
                                            fecha = x.fecha.split(' ');
                                            fecha = formatearFecha(fecha[0]);
                                        }

                                        return fecha;
                                    } 
                                },

                                { "data": "producto" },

                                { "data": "moneda" },
                                
                                { "data": function(x)
                                    {
                                        if(x.moneda == 'PYG'){
                                            return formatto.format(parseFloat(x.precio));
                                        }else{
                                            return formatter.format(parseFloat(x.precio));
                                        }
                                        
                                    }
                                }, 

                                { "data": "tipo" }, 
                            ], 
                            "initComplete": function (settings, json) {
                            // $.unblockUI();
                             $('[data-toggle="tooltip"]').tooltip();
                             
                        }
                            
                    });
                    $('#totalFinal').val(0);
                    $('#totalExenta').val(0);
                    $('#totalGravada').val(0);
                    //calcular();
            }

        }
////////////////////////////////////////////////////////////////////////////////////////////////////

        const formatter = new Intl.NumberFormat('de-DE', 
        {
            currency: 'USD',
            minimumFractionDigits: 2
        });

        const formatto = new Intl.NumberFormat('de-DE', 
        {
            currency: 'PYG',
            minimumFractionDigits: 0
        });

     /* ===========================================================
                PRECARGA DE DATOS - CASO FACTURA CANJE
       ===========================================================*/
    function libroCompraPreCarga() 
    {
        let obj_libro = {!!json_encode($forma_cobro_data) !!};
        precarga.obj_libro = obj_libro;

        if (Object.keys(obj_libro).length > 0 ) {

            $('#input_canje').val(obj_libro.id);
            $('#cliente').val(obj_libro.id_cliente).trigger('change.select2');
            $('#cliente').prop('disabled', true);

            $('#lc_cotizacion').val(obj_libro.cotizacion);
            $('#lc_cotizacion').prop('readonly', true);

            $('#total_factura').val(obj_libro.importe_pago);
            $('#total_factura').prop('readonly', true);
            $('#id_moneda').val(obj_libro.id_moneda).trigger('change.select2');
            $('#id_moneda').prop('disabled', true);

            if (obj_libro.id_moneda == 111) {
                formatNumber(0);
                operaciones.moneda = 111;
            } 
            if (obj_libro.id_moneda == 143) {
                formatNumber(2);
                operaciones.moneda = 143;
            }
            if (obj_libro.id_moneda == 43) {
                formatNumber(2);
                operaciones.moneda = 43;
            }
            if (obj_libro.id_moneda == 21) {
                formatNumber(2);
                operaciones.moneda = 21;
            }

            // $('#exenta').val(obj_libro.exenta);
            // $('#exenta').prop('readonly', true);
            $('#cliente_ruc').prop('readonly', true);

            // $('#nro_timbrado').val(obj_libro.timbrado_factura);
            // $('#nro_timbrado').prop('readonly', true);
            $('#id_provedor_input').val($('#cliente').val());
            // $('#modalAddTimbrado').modal('show');
            calc_iva10();
        }
    }

    function formatearFecha(texto)
 			{
	            if(texto != '' && texto != null)
	            {
	          		return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	            } 
	            else 
	            {
	         		return '';    
            	}
        	}



  function calcular(id, tipo,base,id_proforma_venta) 
    {
        total =  parseFloat(clean_num($('#totalFinal').val()));
        monto_exenta_total = parseFloat(clean_num($('#totalExenta').val()));
        monto_gravada_total = parseFloat(clean_num($('#totalGravada').val()));

        if($('#bas_'+base).is(':checked')) { 
            totalInicio = total + id;
            $('#listado_numero_venta').append('<input type="text" id="id_'+base+'" name="valor[]" value="'+id_proforma_venta+'">');
            montos = $('#bas_'+base).attr('data-valor').split('_');
            
            var idEmpresaBase = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}"
            if(idEmpresaBase == 'D'){
                monto_exenta = id;
                monto_gravada = 0;
            }
            if(idEmpresaBase == 'A'){
                monto_gravada = parseFloat(montos[1]);
                monto_exenta = parseFloat(montos[0]);
            }
            if(idEmpresaBase == 'V'){
                monto_gravada = parseFloat(montos[2]);
                monto_exenta = 0;
            }
            total_exenta = monto_exenta+monto_exenta_total;
            total_gravada = monto_gravada+monto_gravada_total;
        }else{
            montos = $('#bas_'+base).attr('data-valor').split('_');
            var idEmpresaBase = "{{Session::get('datos-loggeo')->datos->datosUsuarios->tipo_empresa}}"
			if(idEmpresaBase == 'D'){
                monto_exenta = id;
                monto_gravada = 0;
            }
            if(idEmpresaBase == 'A'){
                monto_exenta = parseFloat(montos[0]);
                monto_gravada = parseFloat(montos[1]);
            }
            if(idEmpresaBase == 'V'){
                monto_gravada = parseFloat(montos[2]);
                monto_exenta = 0;
            }
            total_exenta = monto_exenta_total - monto_exenta;
            total_gravada = monto_gravada_total - monto_gravada;
            totalInicio = total - id;
            $('#id_'+base).remove();
        }
        $('#totalExenta').val(total_exenta);
        $('#totalGravada').val(total_gravada)
        $('#totalFinal').val(totalInicio);
     }
     
    function saveTimbrado() 
    {
        event.preventDefault();


        $.ajax({
            async: false,
            type: "GET",
            url: "{{route('saveTimbrado')}}",
            data: $('#formTimbrado').serializeJSON({
                customTypes: customTypesSerializeJSON
            }),
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
            },
            success: function (rsp) {
                if(rsp.err == true){
                    if(rsp.existe == true){

                        $.toast({
                            heading: 'Atención !',
                            text: 'El timbrado ya existe.!',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'info'
                        });

                        $("#timbrado").val(rsp.id_timbrado).trigger('change.select2');
                        $('#modalAddTimbrado').modal('hide');

                    } else {
                        operaciones.save_timbrado_id = rsp.id_timbrado;
                        obtenerTimbrado();
                        $('#modalAddTimbrado').modal('hide');
                    }

                } else {
                    $.toast({
                        heading: 'Error',
                        text: 'Ocurrio un error al almacenar el timbrado.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });
                }
                 } //success
        });

    }


    function obtenerFechaVencimiento() 
    {

        $.ajax({
            async: false,
            type: "GET",
            url: "{{route('obtenerFechaVencimiento')}}",
            data: {
                'fecha_factura': moment($('#fecha_factura').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                'id_proveedor': $('#cliente').val()
            },
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
            },
            success: function (rsp) {
                if (rsp.dias != 0) {
                    let dias = Number(rsp.dias);
                    let fecha = '';

                    if (dias < 0) {
                        dias = dias * -1;
                        fecha = moment($('#fecha_factura').val(), 'DD/MM/YYYY');
                        fecha = moment(fecha).subtract(dias, 'days').format('DD/MM/YYYY');
                    } else {
                        fecha = moment($('#fecha_factura').val(), 'DD/MM/YYYY');
                        fecha = moment(fecha).add(dias, 'd').format('DD/MM/YYYY');
                    }

                    $('#vencimiento').datepicker('update', fecha);
                }
            } //success
        });
    }

    function validarLibroCompra(){
        valor = 0;
        $('.form-check').each(function(){ 
            if($(this).is(':checked')) { 
                valor = 1;
            }    
        });  
        if($('#select_tipo_documento').val() == 1){
            if($('#costo_venta').val() == 'true'){
                if(valor == 1){
                    return true;
                }else{
                    return false;
                }
            }else{
                return true;
            }
        }else{
            return true;
        }
    }
    
    function saveFactura() 
    {
        event.preventDefault();
        $('#cliente').prop('disabled',false);
        $('#id_moneda').prop('disabled',false);
        let id_tipo_documento = $('#select_tipo_documento').val();
        if(validarLibroCompra() == true){
            var total = parseFloat(clean_num($('#total_factura').val()));
         //   var sumatoria = parseFloat(clean_num($('#exenta').val()))+parseFloat(clean_num($('#gravada_10').val()))+ parseFloat(clean_num($('#gravada_5').val()))+ parseFloat(clean_num($('#ret_iva').val()))+ parseFloat(clean_num($('#ret_renta').val()));
            if($('#imagen').val() != ""){

                if(id_tipo_documento == 16){
                           
                    if($('#ret_iva').val() == 0 && $('#ret_renta').val() == 0){
                        $.toast({
                                heading: 'Error',
                                text: 'Debe completar la de renta o el total de rentención iva',
                                position: 'top-right',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });
                            
                        return;
                    }

                    if($('#base_retencion').val() == 0){
                        $.toast({
                                heading: 'Error',
                                text: 'Debe ingresar la base de la retención',
                                position: 'top-right',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });
                            
                        return;
                    }

                    let total_retenciones = parseFloat(clean_num($('#ret_renta').val())) + parseFloat(clean_num($('#ret_iva').val()));

                    
                    if(total == 0){
                        $.toast({
                            heading: 'Error',
                            text: 'Debe colocar el total del documento',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });  
                        return;
                    }
                    

                    if(total_retenciones != total){
                        $.toast({
                            heading: 'Error',
                            text: 'El total de renta y el total de rentención iva no coincide con el total de la factura',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });  
                        return;
                    }


                    if(!$('#id_retencion_iva').val()){
                        $.toast({
                            heading: 'Error',
                            text: 'Ingrese una cuenta contable en "Retención IVA"',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });  
                                      
                      return;                    
                    }

                    if(!$('#id_retencion_renta').val()){
                        $.toast({
                            heading: 'Error',
                            text: 'Ingrese una cuenta contable en "Retención Renta"',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });  
                                      
                      return;                    
                    }

                    guardar();


                } else {
                    if($('#exenta').val() == 0 && $('#gravada_10').val() == 0 && $('#gravada_5').val() == 0){
                        $.toast({
                                heading: 'Error',
                                text: 'Ingrese el detalle del total',
                                position: 'top-right',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });
                     
                        $('#cliente').prop('disabled',true);
                        $('#id_moneda').prop('disabled',true);
                    }else{
                            if($('#exenta').val() == 0 || $('#exenta').val() != 0 && $('#cuenta_exenta_lc').val() != ""){
                                if($('#gravada_10').val() == 0 || $('#gravada_10').val() != 0 && $('#cuenta_iva_10_lc').val() != ""){
                                    if($('#gravada_5').val() == 0 ||$('#gravada_5').val() != 0 && $('#cuenta_iva_5_lc').val() != ""){
                                        $resultadoExenta = parseFloat(clean_num($('#exenta').val())) - parseFloat(clean_num($('#totalExenta').val()));
                                        $resultadoGravada = (parseFloat(clean_num($('#gravada_10').val()))+ parseFloat(clean_num($('#gravada_5').val()))) - parseFloat(clean_num($('#totalGravada').val()));
                                        diferencia = (parseFloat(clean_num($('#total_factura').val())) - parseFloat(clean_num($('#totalFinal').val())));

                                        tota_factura = (parseFloat(clean_num($('#gravada_10').val()))+ parseFloat(clean_num($('#gravada_5').val())) + parseFloat(clean_num($('#exenta').val())))
                                        // console.log(tota_factura, total);
                                        if(tota_factura.toFixed(2) != total.toFixed(2)){
                                            $.toast({
                                                    heading: 'Error',
                                                    text: 'El total de exenta y gravada no coincide con el total de la factura',
                                                    position: 'top-right',
                                                    showHideTransition: 'fade',
                                                    icon: 'error'
                                                });  

                                            return;
                                        }

                                        $('#diferencia').val(diferencia);
                                                if(parseInt($('#cliente option:selected').attr('data-pais')) != 1455){
                                                    guardar();
                                                }else{
                                                    if(parseFloat($('#totalFinal').val()) == 0 &&$('#costo_venta').val() == 'true'){
                                                        swal({
                                                                title: "Aviso",
                                                                text: "Debe selecionar un item - Favor verifique",
                                                                icon: "warning",
                                                                showCancelButton: true,
                                                                buttons: {
                                                                    cancel: {
                                                                        text: "Verificar los datos",
                                                                        value: null,
                                                                        visible: true,
                                                                        className: "btn-warning",
                                                                        closeModal: true,
                                                                    },
                                                                    confirm: {
                                                                        text: "Sí, Confirmar",
                                                                        value: true,
                                                                        visible: false,
                                                                        className: "",
                                                                        closeModal: true,
                                                                    }
                                                                }
                                                        }).then(isConfirm => {
                                                                if (isConfirm) {
                                                                        guardarSinConfirmacion(); 

                                                                } else {
                                                                    $('#btnSave').prop('disabled', false);
                                                                    swal("Cancelado", "", "error");
                                                                }
                                                        });
                                                    }else{
                                                        guardar();
                                                    }   
                                                } 
                                        /* }else{
                                                $.toast({
                                                    heading: 'Error',
                                                    text: 'El monto gravado no coincide con los montos gravados 5 y 10 ingresados',
                                                    position: 'top-right',
                                                    showHideTransition: 'fade',
                                                    icon: 'error'
                                                });  
                                
                                            }  
                                        }else{
                                            $.toast({
                                                    heading: 'Error',
                                                    text: 'El monto exento no coincide con los montos exentos de las facturas',
                                                    position: 'top-right',
                                                    showHideTransition: 'fade',
                                                    icon: 'error'
                                            });  
                                     
                                        }*/      
                                    }else{
                                        $.toast({
                                                heading: 'Error',
                                                text: 'Ingrese una cuenta contable en "Cuenta Gravada 5%"',
                                                position: 'top-right',
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            });  
                                       
                                        }              
                                }else{
                                    $.toast({
                                            heading: 'Error',
                                            text: 'Ingrese una cuenta contable en "Cuenta Gravada 10%"',
                                            position: 'top-right',
                                            showHideTransition: 'fade',
                                            icon: 'error'
                                        });  
                                   
                                    }                 
                            }else{
                                $.toast({
                                        heading: 'Error',
                                        text: 'Ingrese una cuenta contable en "Cuenta Exenta"',
                                        position: 'top-right',
                                        showHideTransition: 'fade',
                                        icon: 'error'
                                    }); 
                                   
                                } 
                    }
                }
     
            }else{
                $.toast({
                            heading: 'Error',
                            text: 'Ingrese una imagen',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                
                $('#cliente').prop('disabled',true);
                $('#id_moneda').prop('disabled',true);
            }    
        }else{
            $.toast({
                        heading: 'Error',
                        text: 'Seleccione una de las facturas para este Costo de Venta.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });
            
            $('#cliente').prop('disabled',true);
            $('#id_moneda').prop('disabled',true);
        }              
    } //
/////////////////////////////////////////////////////////////////////////////////////////////////
    function guardarSinConfirmacion(){
            $('#cuenta_exenta_lc').prop('disabled',false);
            $('#cuenta_iva_10_lc').prop('disabled',false);
            $('#cuenta_iva_5_lc').prop('disabled',false);
            $('#id_retencion_iva').prop('disabled',false);
            $('#id_retencion_renta').prop('disabled',false);
            $('#select_tipo_factura').prop('disabled',false);

            let dataString = $('#formFactura').serializeJSON({
                                                        customTypes: customTypesSerializeJSON
                                                     });
            $('#select_tipo_factura').prop('disabled',true);
            let op_num = null;
            $('#btnSave').prop('disabled', true);

             $.ajax({
                    type: "GET",
                    url: "{{route('saveLibroCompra')}}",
                    data: dataString,
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown) {
                                        $('#btnSave').prop('disabled', false);
                                        $('#cliente').prop('disabled',true);
                                        $('#id_moneda').prop('disabled',true);
                                        $('#cuenta_exenta_lc').prop('disabled',true);
                                        $('#cuenta_iva_10_lc').prop('disabled',true);
                                        $('#cuenta_iva_5_lc').prop('disabled',true);
                                        $('#id_retencion_iva').prop('disabled',true);
                                        $('#id_retencion_renta').prop('disabled',true);
                                       
                                            swal("Cancelado", "Ocurrió un error en la comunicación con el servidor", "error");
                                        },
                                        success: function (rsp) {
                                                        if (rsp.err == true) {
                                                            libro = rsp.libro;
                                                            libro = libro.toString();
                                                            op_num = rsp.op;
                                                            op_num = op_num.toString();
                                                            if (rsp.op != 0) {
                                                                swal("Libro de Compra NRO: !"+libro, "OP NRO: !"+op_num, "info")
                                                                        .then(isConfirm => {
                                                                                    redirectView();
                                                                        });
                                                                        
                                                            } else {
                                                                swal("Libro de Compra NRO: !", libro, "info")
                                                                        .then(isConfirm => {
                                                                                    redirectView();
                                                                        });
                                                                    }
                                                                } else {
                                                                    $('#btnSave').prop('disabled', false);
                                                                    $('#cliente').prop('disabled',true);
                                                                    $('#id_moneda').prop('disabled',true);
                                                                    $('#cuenta_exenta_lc').prop('disabled',true);
                                                                    $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                    $('#cuenta_iva_5_lc').prop('disabled',true);
                                                                    $('#id_retencion_iva').prop('disabled',true);
                                                                    $('#id_retencion_renta').prop('disabled',true);

                                                                    swal("Cancelado", rsp.msj[0],"error");

                                                                }
                                                            } //success
                                                        }).done(()=>{
                                                            $('#btnSave').prop('disabled', false);
                                                            $('#cliente').prop('disabled',true);
                                                            $('#id_moneda').prop('disabled',true);
                                                          //  $('#cuenta_exenta_lc').prop('disabled',true);
                                                         //   $('#cuenta_iva_10_lc').prop('disabled',true);
                                                          //  $('#cuenta_iva_5_lc').prop('disabled',true);
                                                        }); //AJAX
                    }                                    
//////////////////////////////////////////////////////////////////////////////////////////////////
    function guardar(){
                        return swal({
                                        title: "GESTUR",
                                        text: "¿Desea Guardar la Factura?",
                                        showCancelButton: true,
                                        buttons: {
                                                cancel: {
                                                        text: "No",
                                                        value: null,
                                                        visible: true,
                                                        className: "btn-warning",
                                                        closeModal: true,
                                                    },
                                                confirm: {
                                                        text: "Sí, guardar",
                                                        value: true,
                                                        visible: true,
                                                        className: "",
                                                        closeModal: true
                                                    }
                                                }
                                    }).then(isConfirm => {
                                            if (isConfirm) {
                                                    
                                                    $('#cuenta_exenta_lc').prop('disabled',false);
                                                    $('#cuenta_iva_10_lc').prop('disabled',false);
                                                    $('#cuenta_iva_5_lc').prop('disabled',false);
                                                    $('#id_retencion_iva').prop('disabled',false);
                                                    $('#id_retencion_renta').prop('disabled',false);
                                                    $('#btnSave').prop('disabled', true);
                                                    $('#select_tipo_factura').prop('disabled',false);

                                                    let dataString = $('#formFactura').serializeJSON({
                                                        customTypes: customTypesSerializeJSON
                                                     });
                                                     $('#select_tipo_factura').prop('disabled',true);

                                                     let op_num = null;

                                                    $.ajax({
                                                            type: "GET",
                                                            url: "{{route('saveLibroCompra')}}",
                                                            data: dataString,
                                                            dataType: 'json',
                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                $('#btnSave').prop('disabled', false);
                                                                $('#cliente').prop('disabled',true);
                                                                $('#id_moneda').prop('disabled',true);
                                                                 $('#cuenta_exenta_lc').prop('disabled',true);
                                                                 $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                 $('#cuenta_iva_5_lc').prop('disabled',true);
                                                                 $('#id_retencion_iva').prop('disabled',true);
                                                                 $('#id_retencion_renta').prop('disabled',true);

                                                                swal("Cancelado", "Ocurrió un error en la comunicación con el servidor", "error");
                                                            },
                                                            success: function (rsp) {
                                                                if (rsp.err == true) {
                                                                    libro = rsp.libro;
                                                                    libro = libro.toString();
                                                                    op_num = rsp.op;
                                                                    op_num = op_num.toString();

                                                                    if (rsp.op != 0) {
                                                                        swal("Libro de Compra NRO: !"+libro, "OP NRO: !"+op_num, "info")
                                                                        .then(isConfirm => {
                                                                                    redirectView();
                                                                        });
                                                                        
                                                                    } else {
                                                                        swal("Libro de Compra NRO: !", libro, "info")
                                                                        .then(isConfirm => {
                                                                                    redirectView();
                                                                        });
                                                                    }
                                                                } else {
                                                                    $('#btnSave').prop('disabled', false);
                                                                    $('#cliente').prop('disabled',true);
                                                                    $('#id_moneda').prop('disabled',true);
                                                                    $('#cuenta_exenta_lc').prop('disabled',true);
                                                                    $('#cuenta_iva_10_lc').prop('disabled',true);
                                                                    $('#cuenta_iva_5_lc').prop('disabled',true);
                                                                    $('#id_retencion_iva').prop('disabled',true);
                                                                    $('#id_retencion_renta').prop('disabled',true);
                                                                    swal("Cancelado", rsp.msj[0],"error");
                                                                }
                                                            } //success
                                                        }).done(()=>{
                                                            $('#btnSave').prop('disabled', false);
                                                            $('#cliente').prop('disabled',true);
                                                            $('#id_moneda').prop('disabled',true);
                                                          //  $('#cuenta_exenta_lc').prop('disabled',true);
                                                         //   $('#cuenta_iva_10_lc').prop('disabled',true);
                                                          //  $('#cuenta_iva_5_lc').prop('disabled',true);
                                                        }); //AJAX
                                            } else {
                                                swal("Cancelado", "La operación fue cancelada", "error");
                                                $('#btnSave').prop('disabled', false);
                                            }
                                    });

                                    $('#btnSave').prop('disabled', false);
                                    $('#cliente').prop('disabled',true);
                                    $('#id_moneda').prop('disabled',true);

    }

    //aqui
    function calcular_campo(){
        let tipo_doc = $('#select_tipo_documento').val();

        if(tipo_doc == 16){
            $('#gravada_10').prop('disabled',true);
            $('#gravada_5').prop('disabled',true);
            $('#exenta').prop('disabled',true);
            $('#ret_iva').prop('disabled',false);
            $('#ret_renta').prop('disabled',false);
            $('#base_retencion').prop('disabled',false);
            $('#select_tipo_factura').val('2').trigger('change.select2');
            $('#select_tipo_factura').prop('disabled',true);
            tipo_factura();
            
        } else {
            $('#gravada_10').prop('disabled',false);
            $('#gravada_5').prop('disabled',false);
            $('#exenta').prop('disabled',false);
            $('#ret_iva').prop('disabled',true);
            $('#ret_renta').prop('disabled',true);
            $('#base_retencion').prop('disabled',true);
            $('#select_tipo_factura').prop('disabled',false);
            tipo_factura();
            
        }
    }



    function obtenerTimbrado() 
    {

        $('#timbrado').empty();

        dataString = {
            id_proveedor: $('#cliente').val(),
            timbrado: precarga.obj_libro.timbrado_factura
        };

        $.ajax({
            type: "GET",
            url: "{{route('obtenerDataCliente.lc')}}",
            data: dataString,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrió un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                $('#btnSave').prop('disabled', false);
            },
            success: function (rsp) {
                if (rsp.data.length) {
                                $('#timbrado').empty();
                                var newOption = new Option('Seleccione Timbrado', '', false, false);
                                $('#timbrado').append(newOption);
                                $.each(rsp.data, function (key, item){
                                    var newOption = new Option('('+item.nro_timbrado+') '+item.denominacion, item.id, false, false);
                                    $('#timbrado').append(newOption)
                                });
                }
                $('#cliente_ruc').val(rsp.ruc);
                $('#costo_venta').val(rsp.costo_venta).trigger('change.select2');

            } //success
        }).done(function () {
            // $('#btnSave').prop('disabled',false);
        }); //AJAX

    }



    /* ================================================================================
                            LOGICA DE CALCULOS
   	        ================================================================================  */




    function calc_iva5() {
        var iva = clean_num($('#gravada_5').val());
        if (iva > 0) {
            let r = iva / 21;

            if (operaciones.moneda == 111) {
                r = r.toFixed(0);
            } else {
                r = r.toFixed(2);
            }

             $('#cuenta_iva_5_lc').prop('disabled', false);
            // $('#btnEditIva5').prop('disabled', false);
            $('#iva_5').val(r);
        } else {
           $(`#cuenta_iva_5_lc`).val('').trigger('change.select2');
            $('#cuenta_iva_5_lc').prop('disabled', true);
            $('#iva_5').val('0');
        }



    }

    function calc_iva10() {
        let iva = clean_num($('#gravada_10').val());
        if (iva > 0) {
            let r = iva / 11;

            if (operaciones.moneda == 111) {
                r = r.toFixed(0);
            } else {
                r = r.toFixed(2);
            }
            $('#cuenta_iva_10_lc').prop('disabled', false);
            $('#iva_10').val(r);
        } else {
           $(`#cuenta_iva_10_lc`).val('').trigger('change.select2');
           $('#cuenta_iva_10_lc').prop('disabled', true);
            $('#iva_10').val('0');
        }


    }



    /* ================================================================================
                            FUNCIONES AUX
       ================================================================================ */

    function clean_num(n, bd = false) {

        if (n && bd == false) {
            n = n.replace(/[,.]/g, function (m) {
                if (m === '.') {
                    return '';
                }
                if (m === ',') {
                    return '.';
                }
            });
            return Number(n);
        }
        if (bd) {
            return Number(n);
        }
        return 0;

    } //

    function redirectView() {

        $.blockUI({
            centerY: 0,
            message: "<h2>Redirigiendo a vista de Libro Compra...</h2>",
            css: {
                color: '#000'
            }
        });

        location.href = "{{ route('indexLibrosCompras') }}";
    }

    function loadProccess() {
        $.blockUI({
            centerY: 0,
            message: "<h2>Procesando...</h2>",
            css: {
                color: '#000'
            }
        });
    }
    $('#id_cliente').on( 'change',function() {
        comprobarFactura();
    });

    $('#nro_comprobante').on( 'change',function() {
        if($('#cliente option:selected').attr('data-pais') == 1455){
            nro_documento = $(this).val();
            if(nro_documento.includes('-') == false){
                $('#nro_comprobante').val('');
                $.toast({ 
                        heading: 'Error',
                        text: 'El formato del comprobante no es correcto.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });
            }else{
                partesNro = nro_documento.split('-');
                if(partesNro[0].length== 3 && partesNro[1].length== 3 && partesNro[2].length==7){
                }else{
                    $('#nro_comprobante').val('');
                    $.toast({ 
                        heading: 'Error',
                        text: 'El formato del comprobante debe de ser xxx-xxx-xxxxxxx.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });

                }

            }
        }

        comprobarFactura();
    });

    $('#timbrado').on( 'change',function() {
        comprobarFactura();
    });


    function formatNumber(coma) {
        $('.format-number-control').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            placeholder: '0',
            digits: coma,
            autoGroup: true,
            // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false
        });

    }


    function justNumbers(e) {
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
            return true;

        return /\d/.test(String.fromCharCode(keynum));
    }


    function comprobarFactura(){
        dataString = 'num_documento='+$('#nro_comprobante').val()+'&proveedor='+$('#cliente').val()+'&timbrado='+$('#timbrado').val();
        $.ajax({
            type: "GET",
            url: "{{route('comprobarFactura')}}",
            data: dataString,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrió un error en la comunicación con el servidor.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                $('#btnSave').prop('disabled', false);
            },
            success: function (rsp) {
                if(rsp != 'OK'){ 
                    $.toast({ 
                            heading: 'Error',
                            text: 'Ya existe una factura con este numero para este proveedor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        $('#nro_comprobante').val('');
                        $('#nro_comprobante').focus();
                }     
            }   
        });   
    }


    function calendar() {
        $('.single-picker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            orientation: "bottom"
        });

        $('#fecha_factura').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            orientation: "bottom"
        }).on('changeDate', function (e) {
            // obtenerFechaVencimiento();
        });

        $('#vencimiento').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            orientation: "bottom"
        });
    }


    function cotizacion() 
    {
        if ($('#id_moneda').val() === '111') 
        {
            $('#lc_cotizacion').prop('readonly', true);
            $('#lc_cotizacion').val(1);
            formatNumber(0);
            operaciones.moneda = 111;
        } 
        else 
        {
            if ($('#id_moneda').val() === '143') 
            {     
                $('#lc_cotizacion').prop('readonly', false);
                       // $('#lc_cotizacion').val();
           
                formatNumber(2);
                operaciones.moneda = 143;
                cargarCotizacionMoneda(143);
            }
            else if ($('#id_moneda').val() === '43') 
            {
                 $('#lc_cotizacion').prop('readonly', false);
                       // $('#lc_cotizacion').val();
           
                formatNumber(2);
                operaciones.moneda = 43;
                cargarCotizacionMoneda(43);
            }
            else if ($('#id_moneda').val() === '21') 
            {
                 $('#lc_cotizacion').prop('readonly', false);
                       // $('#lc_cotizacion').val();
           
                formatNumber(2);
                operaciones.moneda = 21;
                cargarCotizacionMoneda(21);
            }
        }
    }

    function cargarCotizacionMoneda(id_moneda)
    {
        $.ajax({
                        type: "GET",
                        url: "{{route('getCotizacionLc')}}",
                        dataType: 'json',
                        data: {id_moneda:id_moneda},
                        error: function(jqXHR,textStatus,errorThrown)
                        {
                        },
                        success: function(rsp)
                        {
                            $('#lc_cotizacion').val(rsp.data);
                        }   
                    })
    }


    function tipo_factura() {

        let option = $('#select_tipo_factura').val();
        //CREDITO
        if (option === '1') {
            $('#vencimiento').prop('readonly', false);
            $('#vencimiento').datepicker('update');
            $('#vencimiento').val('');
        } else {
            $('#vencimiento').prop('readonly', true);
            $('#vencimiento').datepicker('remove');
            $('#vencimiento').val('');
        }

    }

    function select2MultipleTimbrado() {
        $('#timbrado').select2({
            theme: 'bootstrap4',
            width: 'style'
        });
    }


      /* ================================================================================
   			                LOGICA DE VALIDACIONES DE FORM 
   	        ================================================================================  */

    function validarFormularioFactura() {

        $("#formFactura").validate({
            submitHandler: function (form) {
                saveFactura();
            },
            validClass: "labelSuccess",
            errorClass: "labelError",
            rules: {
                id_sucursal: {
                    required: true
                },
                id_cliente: {
                    required: true
                },
                timbrado: {
                    required: true
                },
                fecha_factura: {
                    required: true
                },
                cliente_ruc: {
                    required: true
                },
                id_tipo_comprobante: {
                    required: true
                },
                nro_comprobante: {
                    required: true
                },
                total_factura: {
                    required: true
                },
                id_centro_costo: {
                    required: true
                },
                concepto: {
                    required: true
                },
                select_tipo_documento: {
                    required: true
                }

            },
            messages: {
                id_sucursal: "Seleccione una sucursal",
                id_cliente: "Seleccione un cliente",
                timbrado: "Agregue un timbrado",
                fecha_factura: "Seleccione una fecha",
                cliente_ruc: "Agregue el RUC",
                id_tipo_comprobante: "Seleccione el tipo factura",
                nro_comprobante: "Agregue el número de comprobante",
                total_factura: "Agregue el total de factura",
                // id_cuenta_exenta : "Seleccione la cuenta exenta",
                // id_cuenta_gravada : "",
                id_centro_costo: "Seleccione un centro de costo",
                concepto: "Agregue un concepto",
                select_tipo_documento : "Seleccione un tipo de documento",
            }
           });

    }


    function validarFormularioFacturaExterior() {

        $("#formFactura").validate({
            submitHandler: function (form) {
                saveFactura();
            },
            validClass: "labelSuccess",
            errorClass: "labelError",
            rules: {
                id_sucursal: {
                    required: true
                },
                id_cliente: {
                    required: true
                },
                fecha_factura: {
                    required: true
                },
                cliente_ruc: {
                    required: true
                },
                id_tipo_comprobante: {
                    required: true
                },
                nro_comprobante: {
                    required: true
                },
                total_factura: {
                    required: true
                },
                id_centro_costo: {
                    required: true
                },
                concepto: {
                    required: true
                },
                select_tipo_documento: {
                    required: true
                }

            },
            messages: {
                id_sucursal: "Seleccione una sucursal",
                id_cliente: "Seleccione un cliente",
                fecha_factura: "Seleccione una fecha",
                cliente_ruc: "Agregue el RUC",
                id_tipo_comprobante: "Seleccione el tipo factura",
                nro_comprobante: "Agregue el número de comprobante",
                total_factura: "Agregue el total de factura",
                // id_cuenta_exenta : "Seleccione la cuenta exenta",
                // id_cuenta_gravada : "",
                id_centro_costo: "Seleccione un centro de costo",
                concepto: "Agregue un concepto",
                select_tipo_documento : "Seleccione un tipo de documento",
            }
        });
    }


    function validarFormTimbrado() {

        $("#formTimbrado").validate({
            submitHandler: function (form) {
                saveTimbrado();
            },
            validClass: "labelSuccess",
            errorClass: "labelError",
            rules: {
                inicio: {
                    required: true
                },
                vencimiento: {
                    required: true
                },
                nro_timbrado: {
                    required: true
                }
            },
            messages: {
                inicio: "Agregue la fecha de inicio.",
                vencimiento: "Agregue la fecha de Vencimiento.",
                nro_timbrado: "Agregue el número de timbrado"
            }
        });

    }

/*===========================CUENTA PREDETERMINADA==================================*/
//aqui
    

    function modalModificarCuenta(opt)
    {
      return  swal({
                    title: "GESTUR",
                    text: "¿Está seguro que desea modificar la cuenta contable?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Modificar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            swal("Éxito", 'Cuenta Contable Habilitada', "success");
                            if(opt == 1){
                                $('#cuenta_iva_10_lc').prop('disabled',false);
                            }
                            if(opt == 2){
                                $('#cuenta_exenta_lc').prop('disabled',false);
                            }
                            if(opt == 3){
                                $('#cuenta_iva_5_lc').prop('disabled',false);
                            }
                        } else {
                            swal("Cancelado", '', "error");
                        }
                });
                              
    }

  





    /* ================================================================================
   			                MODAL Y AVISOS
   	        ================================================================================  */
    function modalNumOp(num) {
        swal("Atención!", `El numero de Op creado es ${num}`, "info")
            .then((value) => {
                redirectView();
            });;
    }




    // Overlay with custom color
    $('.block-custom-overlay').on('click', function () {
        var block_ele = $(this).closest('.card');
        $(block_ele).block({
            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#E91E63',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                color: '#fff',
                backgroundColor: 'transparent'
            }
        });
    });

        $('.numeric').inputmask("numeric", {
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
             // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false,
            oncleared: function () { self.Value(''); }
        });

    function modalTimbrado() {

        if (Object.keys(precarga.obj_libro).length == 0) {
            $('#formTimbrado')[0].reset();
        }

        $('#modalAddTimbrado').modal('show');
        $('#id_provedor_input').val($('#cliente').val());
        $('#modalAddTimbrado').modal('show');
    }


(function($) {
   
  jQuery.isEmpty = function(obj){
    var isEmpty = false;
 
    if (typeof obj == 'undefined' || obj === null || obj === ''){
      isEmpty = true;
    }      
       
    if (typeof obj == 'number' && isNaN(obj)){
      isEmpty = true;
    }
       
    if (obj instanceof Date && isNaN(Number(obj))){
      isEmpty = true;
    }
       
    return isEmpty;
  }
 
})(jQuery);


//=====================IMAGEN FORM ========================//
                var options = { 
                                beforeSubmit:  showRequest,
                                success: showResponse,
                                dataType: 'json' 
                        }; 

                $('body').delegate('#image','change', function(){
                    //OBTENER TAMAÑO DE LA IMAGEN
                    var input = document.getElementById('image');
                    var file = input.files[0];
                    var tamaño = file.size;

                        if(tamaño > 0){

                        var tamaño = file.size/1000;
                        if(tamaño > 3000){
                            $("#image").val('');
                            $("#validation-errors").empty();
                            $("#validation-errors").append('<div class="alert alert-error"><strong>El tamaño de la imagen supera el limite de 3MB</strong><div>'); 
                        } else {

                            $('.cargandoImg').show();
                            $('#upload').ajaxForm(options).submit();  
                        }

                        } else {
                             $("#image").val('');
                             $("#validation-errors").empty();
                             $("#validation-errors").append('<div class="alert alert-error"><strong>Existe un error con el archivo</strong><div>'); 
                        }
                                        
                                });


                  function showRequest(formData, jqForm, options) { 
                   
                                    $("#validation-errors").hide().empty();
                                    return true; 
                                  } 


                  function showResponse(response, statusText, xhr, $form)  { 
                        $('.cargandoImg').hide();
                        if(response.success == false)
                          {
                       $("#image").val('');
                       $("#validation-errors").append('<div class="alert alert-error"><strong>'+ response.errors +'</strong><div>');
                       $("#validation-errors").show();

                        } else {
                             $("#imagen").val(response.archivo); 
                          
                          var file_name = response.archivo;
                                $('#output').html('');


                                divImagen = `
                                    <div class="row ${file_name}">
                                        <div class="col-auto">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <img class="card-img-top" style="max-width:150px;" src="{{asset('adjuntoLc')}}/${file_name}" alt="Adjunto Iamgen Perfil">
                                                        <p><a href="{{asset('adjuntoLc')}}/${file_name}" class="btn btn-info" target="_black"><b>VER</b></a></p>
										                <a href="#" class="card-title" style="font-size: 0.7rem; color: red;" onclick="eliminarImagen('${file_name}')"><b>Eliminar</b></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    `;

                   
                     $("#output").append(divImagen);
                                 
                             $("#image").prop('disabled',true);
                        }
                   }

                   function eliminarImagen(id, archivo){
                        // $("#btn-imagen").on("click", function(e){
                            $.ajax({
                                type: "GET",
                                url: "{{route('fileAdjuntoEliminarLC')}}",//fileDelDTPlus
                                dataType: 'json',
                                data: {
                                            dataId: id,
                                            dataFile:archivo
                                            },
                                success: function(rsp){
                                    $("#imagen").val("");
                                    $('#image').prop('disabled',false);
                                    $("#output").html('');
                                }
                        //   })      

                            });
                        }  


    $(document).ready(function ($) {
        $('#pasajero_principal').select2({
									multiple:true,
									placeholder: 'Pasajero'
								});

        ordenarSelect('pasajero_principal');

        $("#pasajero_principal").select2({
		        ajax: {
		                url: "{{route('getPasajeroPrincipal')}}",
		                dataType: 'json',
		                placeholder: "Seleccione un Beneficiario",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                processResults: function (data, params){
		                            var results = $.map(data, function (value, key) {
		                                        return {
		                                                    id: value.id,
		                                                    text: value.pasajero_data
		                                                };
		                            });
		                                    return {
		                                        results: results,
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });

    }); 
    function ordenarSelect(id_componente)
	    {
	      var selectToSort = jQuery('#' + id_componente);
	      var optionActual = selectToSort.val();
	      selectToSort.html(selectToSort.children('option').sort(function (a, b) {
	        return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
	      })).val(optionActual);
	    }


        $("#select_tipo_factura").change(function()
			{	
                if($("#select_tipo_factura").val() == 1){
                    if($('#fecha_factura').val() != ""){
                        $.ajax({
                                type: "GET",
                                url: "{{route('getPersonaNegociacion')}}",
                                dataType: 'json',
                                data: {
                                    dataId: $('#cliente').val()
                                        },
                                success: function(rsp){
                                        if(rsp != ""){
                                            var fechaGasto = $('#fecha_factura').val().split('/');
                                            var fechaGastoFormato = fechaGasto[2]+'-'+fechaGasto[1]+'-'+fechaGasto[0];
                                            var d = new Date(fechaGastoFormato);
                                            var resultado = sumarDias(d, rsp);
                                            var month = resultado.getMonth()+1;
                                            var day = resultado.getDate()+1;

                                            var output = (day<10 ? '0' : '') + day + "/" 
                                                        + (month<10 ? '0' : '') + month + '/'
                                                        + resultado.getFullYear();
                                            $.toast({ 
                                                heading: 'Error',
                                                text: 'El vencimiento de esta factura debería ser '+output+', favor verifique antes de guardar',
                                                position: 'top-right',
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            });
                                        }    
                                }
                        });	
                    }else{
                        $.toast({ 
                                                heading: 'Error',
                                                text: 'Ingrese la fecha de la factura ',
                                                position: 'top-right',
                                                showHideTransition: 'fade',
                                                icon: 'error'
                                            });
                        $('#fecha_factura').focus();
                    }	
                }	
			});

            function sumarDias(fecha, dias){
                fecha.setDate(fecha.getDate() + dias);
                return fecha;
            }

             // Función para validar la fecha al establecerla
            function validarFecha() {
                //fecha actual
                let fecha_actual = (new Date()).setHours(0, 0, 0, 0); 

                //fecha de la factura
                let fecha_factura = $("#fecha_factura").val().split('/');
                    fecha_factura = fecha_factura[2]+'-'+fecha_factura[1]+'-'+fecha_factura[0];
                    fecha_factura = new Date(fecha_factura);

                //Fecha actual menos 1 mes
                var fechaActualAnt = moment().date(1);
                var mesAnteriorAnt = fechaActualAnt.subtract(1, 'months');
                    mesAnteriorAnt = new Date(fechaActualAnt.format('YYYY-MM-DD'));

                //Fecha limite para carga de LC mes anterior
                let fechaLimite = new Date(new Date().setHours(0, 0, 0, 0)).setDate(dia_limite)

                //Fecha actual dia 1 del mes
                let fechaActualDia1 = new Date(new Date().setHours(0, 0, 0, 0)).setDate(1)


                // Comprobar si la fecha de la factura es futura
                if (fecha_factura > fecha_actual) {
                    $.toast({ 
                                heading: 'Error',
                                text: 'La fecha de la factura no puede mayor a la fecha actual',
                                position: 'top-right',
                                showHideTransition: 'fade',
                                icon: 'error'
                            });

                    $("#fecha_factura").val('');
                }

                if(!permisoCambioFecha){
                    
                    //La fecha de factura es menor al mes anterior
                    if(fecha_factura < mesAnteriorAnt){  
                        $.toast({ 
                                    heading: 'Error',
                                    text: 'La fecha de la factura no puede ser menor al mes anterior, requiere permisos',
                                    position: 'top-right',
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });

                        $("#fecha_factura").val('');
                    }


                    //Validamos si es del mes anterior
                    if (fecha_factura < fechaActualDia1) {
                
                        // Comprobar si la fecha actual es mayor a la fecha limite
                        if (fecha_actual > fechaLimite) {
                            
                            $.toast({ 
                                    heading: 'Error',
                                    text: 'Ya no esta permitido cargar factura de meses anteriores, el plazo maximo establecido hasta el '+dia_limite+' del mes.',
                                    position: 'top-right',
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });

                            $("#fecha_factura").val('');
                        }
                    } 
                }

            
                            


            }

            // Asociar la función de validación al evento de cambio en el campo de fecha
            $('#fecha_factura').on('change', validarFecha);


</script>
@endsection
