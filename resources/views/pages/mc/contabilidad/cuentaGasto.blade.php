@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
	border-radius: 14px !important;
	}
	</style>
	
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Cuentas de Gastos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmCuenta" method="get" >
					<div class="row">
						<div class="col-12 col-sm-1 col-md-1"></div>

						<div class="col-12 col-sm-5 col-md-5" style="padding-left: 20px;">
							<label class="control-label">Cuenta de Gasto</label>
							<input type="text" name="denominacion" class="Requerido form-control" value=""id="denominacion" required="required" />
						</div>

						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Cuenta de Gastos Padre</label>
								<select class="form-control select2" name="id_cuenta_padre" id="id_cuenta_padre" style="width: 100%;">
								<option value="">Seleccione Cuenta Gasto</option>	
											@foreach($cuentasCostos as $key=>$cuentasCosto)
											@php 	
											 if(isset($cuentasCosto->cuentasPadre->denominacion)){
												$planCuentas = ' ('.$cuentasCosto->cuentasPadre->denominacion.')';
											 }else{
												$planCuentas = '';
											 }
											@endphp
											<option value="{{$cuentasCosto->id}}">{{$cuentasCosto->denominacion}} {{$planCuentas}}</option>
										@endforeach								
									</select>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-12 mb-2">
							<a href="{{ route('indexCuentaContable') }}" type="button" class="btn btn-danger btn-lg pull-right mr-1">Volver</a>
							<button type="button" onclick="guardarCuenta()" id="btnGuardar" class="btn btn-success btn-lg pull-right mr-1">Guardar</button>


						</div>
					</div>

				</form>
			</div>
		</div>
	</section>
	</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>




		$(document).ready(function() {
		//	buscarCotizacion();


			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

		});

				function guardarCuenta() {
					var dataString = $('#frmCuenta').serialize();
					console.log(dataString);

					if($('#denominacion').val() != ""){

								$.ajax({
											type: "GET",
											url: "{{route('doGuardarCuenta')}}",
											dataType: 'json',
											data: dataString,

											error: function(jqXHR,textStatus,errorThrown){

											$.toast({
												heading: 'Error',
												text: 'Ocurrio un error en la comunicación con el servidor.',
												position: 'top-right',
												showHideTransition: 'fade',
												icon: 'error'
													});

												},
											success: function(rsp){
												if(rsp.status == 'OK'){

													$.toast({
															heading: 'Success',
															text: 'Los datos fueron almacenados.',
															position: 'top-right',
															showHideTransition: 'slide',
															icon: 'success'
														});
													window.location.replace("{{route('reporteCuentasGasto')}}");

												//buscarCotizacion();
												} else {
													
													$.toast({
															heading: 'Error',
															text: 'Los datos no fueron almacenados.',
															position: 'top-right',
															showHideTransition: 'fade',
															icon: 'error'
														});
												
												}//else
											}//funcion	
									});				
					} else {
						 $.toast({
                            heading: 'Error',
                            text: 'Complete los campos requeridos.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });
					}
				}

	</script>
@endsection