@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
    .correcto_col {
        height: 74px;
    }
    					
    input.form-control:focus ,.select2-container--focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }

    .input-style {
		 border:none;
		 background-color: transparent;
		 text-align: center;
	 }
     .mr-1 {
         margin-right: 5px; 
     }
     .labelError{
         color: #F74343 ;
     }
     input {
         font-weight: 800;
     }
</style>
@parent

@endsection
@section('content')


<section id="base-style">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Factura</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">

                <form class="row" id="formFactura" method="get" autocomplete="off">

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Origen</label>
                            <input type="text" class="form-control" disabled
                                value="@if($libroCompra->origen === 'A') AUTOMATICO @elseif($libroCompra->origen === 'M') MANUAL @else DESCONOCIDO(x)  @endif">
                                <input type="hidden" name="id_asiento" id="id_asiento" value="{{$libroCompra->id_asiento}}">
                                <input type="hidden" name="id_documento" id="id_documento" value="{{$libroCompra->id_documento}}">
                                <input type="hidden" name="id" id="id" value="{{$libroCompra->id}}">
                                <input type="hidden" name="id_proforma_detalle" id="id_proforma_detalle" value="{{$libroCompra->id_proforma_detalle}}">
                                <input type="hidden" name="id_proforma" id="id_proforma" value="{{$libroCompra->id_proforma}}">
                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Sucursal</label>
                            <?php 
                                if(isset($libroCompra->sucursal_empresa['denominacion'])){
                                    $sucursal = $libroCompra->sucursal_empresa['denominacion'];
                                }else{
                                    $sucursal = "";
                                }    
                            ?>

                            <input type="text" class="form-control" disabled value="{{$sucursal}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Proveedor</label>
                            <select id="cliente_id" name="cliente_id" class="form-control select2"
                            @if($reprice)
                                @else
                                    disabled
                                @endif
                            >
                                @foreach ($getProveedor as $proveedor)
                                <option value="{{ $proveedor->id }}" {{ $proveedor->id == $libroCompra->cliente->id ? 'selected' : '' }}>
                                    {{ $proveedor->documento_identidad }} - {{ $proveedor->nombre }} {{ $proveedor->apellido }} - {{ $proveedor->denominacion_comercial }}
                                </option>
                            @endforeach				
                            </select>
                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>RUC</label>
                            <input type="text" class="form-control" id="cliente_ruc" tabindex="3" disabled
                                name="cliente_ruc" value="{{$libroCompra->ruc}}">
                        </div>
                    </div>

                    <?php 
                        if(isset($libroCompra->OpTimbrado['nro_timbrado'])){
                            $OpTimbrado = $libroCompra->OpTimbrado['nro_timbrado'];
                        }else{
                            $OpTimbrado = "";
                        }    
                    ?>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Timbrado:</label>
                            <input type="text" class="form-control" disabled value="{{$OpTimbrado}}">

                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Fecha Factura</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control pull-right single-picker" name="fecha_factura" tabindex="5" id="fecha_factura" value="{{$libroCompra->fecha_hora_facturacion}}" 
                                     @if($permiso_modificar_fecha && $libroCompra->origen != 'M') 
                                        disabled
                                    @elseif(!$permiso_modificar_fecha)
                                        disabled
                                    @endif
                                >
                        
                                    
                            </div>
                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Comprobante</label>
                            <input type="text" class="form-control" disabled
                                value="{{$libroCompra->tipoDocumento['denominacion']}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Vencimiento</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" class="form-control single-picker" data-value-type="s_date" disabled
                                    id="vencimiento" tabindex="7" name="vencimiento"
                                    value="{{$libroCompra->fecha_prim_ven}}">
                            </div>
                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Nro Comprobante</label>
                            <input type="text" class="form-control" id="" tabindex="8" name="nro_comprobante" disabled
                                value="{{$libroCompra->nro_documento}}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Proforma / Venta</label>
                            <input type="text" class="form-control" maxlength="30" value="{{$libroCompra->nro_venta_pedido}}" id="nro_venta_pedido" tabindex="7" name="nro_venta_pedido" disabled>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Moneda</label>
                            <input type="text" class="form-control" disabled
                                value="{{$libroCompra->currency['currency_code']}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Cotización</label>
                            <input type="text" class="form-control format-number-control" disabled id="lc_cotizacion"
                                data-value-type="convertNumber" tabindex="10" name="cotizacion"
                                value="{{$libroCompra->cambio}}">
                        </div>
                    </div>


                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Total</label>
                            <input type="text" class="form-control format-number-control monto_lc"  id="total_factura"
                                name="total_factura" data-value-type="convertNumber" tabindex="11"
                                value="{{$libroCompra->importe}}"
                                @if($reprice)
                                @else
                                    disabled
                                @endif
                            >
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Exentas</label>
                            <?php 
                            echo $libroCompra->costo_exenta;
                                if($libroCompra->costo_exenta == ""){
                                    $exenta = $libroCompra->exenta;
                                }else{
                                    $exenta = $libroCompra->costo_exenta;
                                }
                            ?>
                            <input type="text" class="form-control format-number-control monto_lc" disabled  id="exenta" name="exenta" data-value-type="convertNumber" tabindex="12" value="{{$exenta}}"
                        
                            >
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 10%</label>
                            <input type="text" class="form-control format-number-control monto_lc"  disabled id="gravada_10" name="gravada_10" data-value-type="convertNumber" tabindex="13" value="{{$libroCompra->gravadas10}}"
                            >
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Gravadas 5%</label>
                            <input type="text" class="form-control format-number-control monto_lc" disabled id="gravada_5" name="gravada_5" data-value-type="convertNumber" tabindex="14" value="{{$libroCompra->gravadas5}}"

                            >
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 10%</label>
                            <input type="text" class="form-control format-number-control read-only" disabled id="iva_10" name="iva_10" data-value-type="convertNumber" tabindex="16" value="{{$libroCompra->iva10}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>IVA 5%</label>
                            <input type="text" class="form-control format-number-control read-only" disabled id="iva_5" name="iva_5" data-value-type="convertNumber" tabindex="17"
                                value="{{$libroCompra->iva5}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Base de la Retención</label>
                            <input type="text" class="form-control format-number-control read-only monto_lc"  disabled id="base_retencion" name="base_retencion" data-value-type="convertNumber" tabindex="17"
                                value="{{$libroCompra->base_retencion}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Retención IVA</label>
                            <input type="text" class="form-control format-number-control read-only monto_lc"  disabled id="retencion_iva" name="retencion_iva" data-value-type="convertNumber" tabindex="17"
                                value="{{$libroCompra->retencion_iva}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Retención Renta</label>
                            <input type="text" class="form-control format-number-control read-only monto_lc"  disabled id="retencion_renta" name="retencion_renta" data-value-type="convertNumber" tabindex="17"
                                value="{{$libroCompra->retencion_renta}}">
                        </div>
                    </div>

                    <?php 
                        if(isset($libroCompra->centroCosto['nombre'])){
                            $centroCosto = $libroCompra->centroCosto['nombre'];
                        }else{
                            $centroCosto = "";
                        }    
                    ?>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Diferencia</label>
                            <input type="text" class="form-control format-number-control read-only" disabled id="diferencia" name="diferencia" data-value-type="convertNumber" tabindex="17"
                                value="{{$libroCompra->diferencia}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Centro Costo</label>
                            <input type="text" class="form-control" disabled value="{{$centroCosto}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Usuario Creación</label>
                            <?php 
                                if(isset($libroCompra->usuario->nombre)){
                                 $nombreUsuario = $libroCompra->usuario->nombre." ".$libroCompra->usuario->apellido." (".$libroCompra->usuario->usuario.")";
                                }else{
                                    $nombreUsuario = "";
                                }                          
                            ?>
                            <input type="text" class="form-control" disabled value="{{$nombreUsuario}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Fecha de Creación</label>
                            <?php 
                                 if(isset($libroCompra->fecha)){
                                    $fecha = explode(' ', $libroCompra->fecha);
                                    $fechaCreacion = explode('-', $fecha[0]);
                                    $fecha_creacion = $fechaCreacion[2]."/".$fechaCreacion[1]."/".$fechaCreacion[0]." ".$fecha[1];
                                 }else{
                                    $fecha_creacion = "";
                                }     
                            ?>    
                            <input type="text" class="form-control" disabled value="{{$fecha_creacion}}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Estado</label>
                            <?php 
                                 if($libroCompra->activo == true){
                                    $estado = 'Activo';
                                 }else{
                                    $estado = "Anulado";
                                }     
                            ?>    
                            <input type="text" class="form-control" style="font-weight: 900;" disabled value="{{$estado}}">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Tipo Comprobante</label>
                            <?php 
                                if(isset($libroCompra->tipoHechauka->descripcion)){
                                    $tipo_documento = $libroCompra->tipoHechauka->descripcion;
                               }else{
                                   $tipo_documento = "";
                               }            
                                
                            ?>
                            <input type="text" class="form-control" disabled value="{{$tipo_documento}}">
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3">
                        <div class="form-group">
                            <label>Codigo Confirmacion</label>
                            <input type="text" class="form-control" id="cod_confirmacion" tabindex="3" 
                                name="cod_confirmacion" value="{{$libroCompra->cod_confirmacion}}"
                                @if($reprice)
                                @else
                                    disabled
                                @endif
                                >
                        </div>
                    </div>



                    @if($libroCompra->activo == false)
                        <div class="col-12 col-sm-4 col-md-3">
                            <div class="form-group">
                                <label>Usuario Anulación</label>
                                <?php 
                                    if(isset($libroCompra->usuarioAnulacion->nombre)){
                                    $usuario_anulacion = $libroCompra->usuarioAnulacion->nombre." ".$libroCompra->usuarioAnulacion->apellido." (".$libroCompra->usuarioAnulacion->usuario.")";
                                    }else{
                                        $usuario_anulacion = '';
                                    }
                                ?>
                                <input type="text" class="form-control" disabled value="{{$usuario_anulacion}}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-3">
                            <div class="form-group">
                                <label>Fecha Anulación</label>
                                <?php 
                                    if(isset($libroCompra->fecha_hora_anulacion)){
                                        $fecha = explode(' ', $libroCompra->fecha_hora_anulacion);
                                        $fechaCreacion = explode('-', $fecha[0]);
                                        $fecha_creacion = $fechaCreacion[2]."/".$fechaCreacion[1]."/".$fechaCreacion[0]." ".$fecha[1];
                                    }else{
                                        $fecha_creacion = "";
                                    }     
                                ?>    
                                <input type="text" class="form-control" disabled value="{{$fecha_creacion}}">
                            </div>
                        </div>
                    @endif                
                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Exenta</label>
                            <?php
                                if(isset($libroCompra->cuentaContableExenta['descripcion'])){
                                    $cuenta_exenta = $libroCompra->cuentaContableExenta['descripcion'];
                                }else{
                                    $cuenta_exenta = "";
                                }
                            ?>
                            <input type="text" class="form-control" disabled value="{{$cuenta_exenta}}">
                        </div>
                    </div>
                    <?php
                        if(isset($libroCompra->cuentaContableIva10['descripcion'])){
                            $cuenta_10 = $libroCompra->cuentaContableIva10['descripcion'];
                        }else{
                            $cuenta_10 = "";
                        }
                    ?>
		            <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Grav 5%</label>
                            <?php
                                if(isset($libroCompra->cuentaContableIva5['descripcion'])){
                                    $cuenta_5 = $libroCompra->cuentaContableIva5['descripcion'];
                                }else{
                                    $cuenta_5 = "";
                                }
                            ?>

                            <input type="text" class="form-control" disabled value="{{$cuenta_5}}">

                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Cuenta Grav 10%</label>
                            <?php
                                if(isset($libroCompra->cuentaContableIva10['descripcion'])){
                                    $cuentaContableIva10 = $libroCompra->cuentaContableIva10['descripcion'];
                                }else{
                                    $cuentaContableIva10 = "";
                                }
                            ?>

                            <input type="text" class="form-control" disabled value="{{$cuentaContableIva10}}">
                        </div>
                    </div> 
                    <?php
                        if(isset($libroCompra->cuentaContableRetencionIva['descripcion'])){
                            $cuenta_retencion_iva = $libroCompra->cuentaContableRetencionIva['descripcion'];
                        }else{
                            $cuenta_retencion_iva = "";
                        }
                    ?>

                     <div class="col-12">
                        <div class="form-group">
                            <label>Retención IVA</label>
                            <input type="text" class="form-control" disabled value="{{$cuenta_retencion_iva}}">
                        </div>
                    </div>
                    <?php
                        if(isset($libroCompra->cuentaContableRetencionIva['descripcion'])){
                            $contable_retencion = $libroCompra->cuentaContableRetencionIva['descripcion'];
                        }else{
                            $contable_retencion = "";
                        }
                    ?>

                     <div class="col-12">
                        <div class="form-group">
                            <label>Retención Renta</label>
                            <input type="text" class="form-control" disabled value="{{$contable_retencion}}">
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label>Concepto</label>
                            <input type="text" class="form-control text-bold" id="" disabled tabindex="22"
                                name="concepto" value="{{$libroCompra->concepto}}">
                        </div>
                    </div>

                    <div class="col-12">
                        <?php
                            if($libroCompra->activo && $libroCompra->origen == 'M' && $permiso_modificar_fecha){
                                echo '<button type="button" class="mr-1 btn btn-info btn-lg pull-right" tabindex="23"
                                id="btModificarFecha"><b>Modificar</b></button>';
                            }
                            if($libroCompra->activo && $reprice && $libroCompra->reprice_pago_pro!==true){
                                echo '<button type="button" class="mr-1 btn btn-info btn-lg pull-right" tabindex="23"
                                id="btReprice"><b>Reprice</b></button>';
                            }
                           
                            if($libroCompra->activo && $libroCompra->origen == 'M' && $permiso_anular){
                                echo '<button type="button" class="mr-1 btn btn-danger btn-lg pull-right" tabindex="23"
                                    id="btnAnular"><b>Anular</b></button>';                             }
                       
                                    echo '<button type="button" class="mr-1 btn btn-success btn-lg pull-right" tabindex="24"
                            id="btnVolver"><b>Volver</b></button>';
                            if($permiso == 1){
                               echo '<button type="button" class="mr-1 btn btn-danger btn-lg pull-right" tabindex="23"
                                    id="btModificar"><b>Modificar</b></button>';
                             }
                        ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>




{{-- 
@if($libroCompra->origen == 'M')


    ========================================
   			MODAL   ANULAR OP
   	========================================  --}}		

	   <div class="modal fade" id="modalAnular" aria-labelledby="myModalLabel">
	       <div class="modal-dialog">
	           <div class="modal-content">
	               <div class="modal-header">
	                   <button type="button" class="closeModal" data-dismiss="modal" aria-label="Close">
	                       <span aria-hidden="true">&times;</span>
	                   </button>
	                   <h4 class="modal-title" id="" style="font-weight: 800;">
	                       <i class="fa fa-fw fa-remove"></i>
	                       ANULAR FACTURA LIBRO COMPRA
	                   </h4>
	               </div>

	               <div class="modal-body">
	                   <div class="content-fluid">
	                       <div class="row">

	                           <div class="col-12">
	                               <h3> Esta seguro que quiere anular la Factura ? </h3>
	                           </div>
	                           <div class="row">
                                    <div class="col-10"></div>
                                    <div class="col-2">
                                        <button type="submit" class="mr-1 btn btn-success btn-lg center-block"
                                            id="btnConfirmarAnulacion" style="margin:0px"><b>Confirmar</b></button>
                                    </div>
                               </div>
	                       </div>
	                   </div>
	               </div>
	           </div>
	       </div>
	   </div>
       {{-- @endif--}}	


@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>

<script>

    $(document).ready(function () {
        // Selector de fecha
        $('#fecha_factura').datepicker({
            format: 'dd/mm/yyyy', // Formato de fecha deseado
            autoclose: true,
            todayHighlight: true,
            language: 'es' 
        });

    });
</script>
<script type="text/javascript">


    $(() => {
        main();
        
});
function main() {
		formatNumber(2);
        $('.select2').select2();
        $('#timbrado').select2({maximumSelectionLength: 1});

    } //


    function formatNumber(coma) {
		console.log('SE dispara formateo');
        $('.format-number-control').inputmask("numeric", {
            radixPoint: ",",
			groupSeparator: ".",
			placeholder: '0',
            digits: coma,
            autoGroup: true,
            // prefix: '$', //No Space, this will truncate the first character
            rightAlign: false
        });

    }
    
    editar();
    function editar(){
        permiso = '{{$permiso}}';
        if(parseInt(permiso) == 1){
            console.log('Entro!!!!!!!!');
            $('#exenta').prop('disabled',false);
            $('#gravada_10').prop('disabled',false);
            $('#gravada_5').prop('disabled',false);
            $('#retencion_iva').prop('disabled',false);
            $('#retencion_renta').prop('disabled',false);
            $('#lc_cotizacion').prop('disabled',false);
            $('#nro_comprobante').prop('disabled',false);
            $('#cod_confirmacion').prop('disabled',false);
        }
    }



    $('#btnAnular').click(()=>{
        $('#modalAnular').modal('show');
    });
    
    $('#btnConfirmarAnulacion').click(()=>{

        
    $.ajax({
          type: "GET",
          url: "{{route('anularLibroCompra')}}",
          data: { id_libro: '{{$libroCompra->id}}'},
          dataType: 'json',
           error: function(jqXHR,textStatus,errorThrown){
                    $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            hideAfter: false,
                            icon: 'error'
                        });

                //   $.unblockUI();
		      	// $('#btnSave').prop('disabled',false);
		  },
          success: function(rsp){

          		if(rsp.err === true){
                    $('#modalAnular').modal('hide');
                    redirectView();
				  } else {
                    $('#modalAnular').modal('hide');
                    $.toast({
                            heading: 'Error',
                            text: rsp.msj,
                            position: 'top-right',
                            hideAfter: false,
                            icon: 'error'
                        });
                  }
           
                          }//success
                 });//AJAX


    });

    $('#btnVolver').click(()=>{
        redirectView();
    });

    function procesarndo(){
			$.blockUI({
                    centerY: 0,
                    message: "<h2>Procesando...</h2>",
                    css: {
                        color: '#000'
                        }
                    });
		}

        function delase(){
			$.unblockUI();
		}

    function redirectView(){
      
      $.blockUI({
                      centerY: 0,
                      message: "<h2>Redirigiendo a vista de Libro Compra...</h2>",
                      css: {
                          color: '#000'
                      }
                  });

                  location.href ="{{ route('indexLibrosCompras') }}";
  }

  id_lc = '{{$libroCompra->id}}';
  $.ajax({
			type: "GET",
			url: "{{route('comprobarAsiento')}}",
			data: {
					id_data: parseInt(id_lc),
					option: 'LC'
				},
			dataType: 'json',
			error: function (jqXHR, textStatus, errorThrown) {},
			success: function (rsp) {
				if(rsp.status == 'ERROR'){
                    swal('El asiento Nro.'+rsp.id , 'El asiento generado por este proceso no está balanceado, favor pongase en contacto con Administración/Contabilidad para su verificación.',"error");
				}
			}
	})	

    $('#btModificar').click(()=>{
        guardarRepriceLibro();
    });

    $('#btModificarFecha').click(()=>{
        modificarFecha();
    });

    $('#btReprice').click(()=>{
        procesarndo();
        modificarReprice();
    });
/*
    editarFecha();
    function editarFecha() {
    let permiso = '{{$permiso}}'; // Supongo que $permiso es una variable en tu vista Blade

    if (parseInt(permiso) === 60) {
        $('#fecha_factura').prop('disabled', true);
    } else {
        $('#fecha_factura').prop('disabled', false);
    }

    return false; // Evita que se ejecute más código después de cambiar el estado del campo
}*/

    function modificarFecha() {
    let dataString = $('#formFactura').serializeJSON({
        customTypes: customTypesSerializeJSON
    });
    let id_asiento = $('#id_asiento').val(); // Obtén el valor de id_asiento del campo oculto
    $.ajax({
        type: "GET",
        url: `{{ route('modificarFechaLibro', ['id' => $libroCompra->id]) }}`,
        data: dataString,
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            swal("Cancelado", "Ocurrió un error en la comunicación con el servidor", "error");
        },
        success: function (rsp) {
            
                swal("Éxito", "Fecha actualizada correctamente", "success")
                    .then(function () {
                       
                        window.location.href = "{{ route('verLibroCompra', ['id' => $libroCompra->id]) }}";
                    });
            
        }
    });
}

//reprice arturo 
function modificarReprice() {
    let dataString = $('#formFactura').serializeJSON({
        customTypes: customTypesSerializeJSON
    });
    let id_asiento = $('#id_asiento').val(); // Obtén el valor de id_asiento del campo oculto
    $.ajax({
        type: "GET",
        url: `{{ route('modificarReprice', ['id' => $libroCompra->id]) }}`,
        data: dataString,
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI(); 
            swal("Cancelado", "Ocurrió un error en la comunicación con el servidor", "error");
        },
        success: function (rsp) {
            $.unblockUI(); 
            if(rsp.error==true) {
                swal("Cancelado",rsp.message,"error");
            }else{
                swal("Éxito", "Reprice actualizada correctamente", "success")
                    .then(function () {
                   
                        //window.location.href = "{{ route('verLibroCompra', ['id' => $libroCompra->id]) }}";
                        window.location.href = "{{ route('indexLibrosCompras') }}";

                    });
            }
                
        }
    });
}



//fin reprice 

    function guardarRepriceLibro(){

        let dataString = $('#formFactura').serializeJSON({
                                                            customTypes: customTypesSerializeJSON
                                                        });
        $.ajax({
                type: "GET",
                url: "{{route('guardarRepriceLibro')}}",
                data: dataString,
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                                                                    swal("Cancelado", "Ocurrió un error en la comunicación con el servidor", "error");
                                                                },
                success: function (rsp) {





                                        }
            })
    }


    $('.monto_lc').change(function() {
        val = $(this).val();
        id_input = 
        $(".monto_lc").each(function(){
            console.log($(this).val());
        });



    })

</script>
@endsection
