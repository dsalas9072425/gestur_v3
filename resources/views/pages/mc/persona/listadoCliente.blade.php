
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style>
	.card,.card-header {
border-radius: 14px !important;
}
</style>



<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Listado Clientes</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<form id="frmPersonaConsulta" autocomplete="off">
				<div class="card-body">
					<div class="row">


						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Nombre / Razon Social</label>
								<span class="combo_1 combo"> </span>
								<i style="display:none;" class="fa fa-refresh fa-spin cargandoImg combo_1"></i>
								<select class="form-control select2" tabindex="1" name="idPersona" id="idPersona"
									style="width: 100%;" />
								<option value="">Seleccione Persona</option>

								@foreach($selectPersona as $Personas)
									@php
										$ruc = $Personas->documento_identidad;
										if($Personas->dv){
											$ruc .= $ruc."-".$Personas->dv;
										}
									@endphp

								<option value="{{$Personas->id}}">
								{{$ruc}} - {{$Personas->full_data}}
								</option>
								@endforeach

								</select>
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Tipo Persona</label>
								<select class="form-control select2" tabindex="3" name="idTipoPersona"
									id="idTipoPersona" style="width: 100%;" />
								<option value="">TODOS</option>

								@foreach($tipoPersonas as $tipoPersona)
								<option value="{{$tipoPersona->id}}">{{$tipoPersona->denominacion}}</option>
								@endforeach

								</select>
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label for="formNombres">Código de Cliente <span class="formNombres"></span></label>
								<input type="text" class="Requerido form-control" tabindex="5" name="inputCod"
									id="inputCod" value="" maxlength="100" />
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Empresa / Agencia</label>
								<select class="form-control select2" tabindex="4" name="idEmpresaAgencia"
									id="idEmpresaAgencia" style="width: 100%;" />
								<option value="">TODOS</option>


								@foreach($empresaAgencia as $valor)
								<option value="{{$valor->id}}">{{$valor->nombre}} {{$valor->denominacion_comercial}}
								</option>
								@endforeach


								</select>
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label for="formNombres">RUC / CI <span class="formNombres"></span></label>
								<input type="text" class="Requerido form-control" tabindex="5" name="inputCi"
									id="inputCi" value="" maxlength="100" />
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label for="formNombres">Correo<span class="formNombres"></span></label>
								<input type="text" class="Requerido form-control" tabindex="6" name="inputEmail"
									id="inputEmail" value="" maxlength="100" />
							</div>
						</div>




					</div>


						<div class="col-12">
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
							<button type="button" onclick="buscarPersona()" id="btnGuardar" class="btn btn-info btn-lg pull-right mr-1"><b>Buscar</b></button>
						</div>

			</form>




			<div class="table-responsive table-bordered">
				<table id="listado" class="table table-hover" style="width: 100%;">
					<thead>
						<tr>
							<th>Cod.</th>
							<th>Empresa / Agencia</th>
							<th>Nombre/Razon Social</th>
							<th>Tipo Persona</th>
							<th>Correo</th>
							<th>Documento/RUC</th>
						</tr>
					</thead>
					<tbody style="text-align: center" class="">
					</tbody>
				</table>
			</div>


		</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script>

	$(document).ready(function() {
		$('.select2').select2({
				tag:true,
	 // minimumInputLength: 2,
     //        allowClear: true,
     //        placeholder: '--Select Client--'
			});

			//buscarPersona();
			main();
			
		});

	function main() {

	$('#estado').val('true').trigger('change.select2');
	document.getElementById("idEmpresaAgencia").tabIndex = "1";
	document.getElementById("idPersona").tabIndex = "2";
	document.getElementById("idTipoPersona").tabIndex = "3";
	document.getElementById("inputCi").tabIndex = "4";
	document.getElementById("inputEmail").tabIndex = "5";
	}	

	$('.select2').keypress(function(e) {
			var keycode = (e.keyCode ? e.keyCode : e.which);
		});

	
	
	function limpiar(){
		$('#idEmpresaAgencia').val('').trigger('change.select2');
		$('#idPersona').val('').trigger('change.select2');
		$('#inputCod').val('');
		$('#idTipoPersona').val('').trigger('change.select2');
		$('#inputCi').val('');
		$('#inputEmail').val('');
		buscarPersona();
	}

	


	// $( "#idPersona" ).focus(function() {
	// 	console.log('focus');
	// });

			function buscarPersona(){
			//Destruir dataTable	
			$("#listado").dataTable({
				"searching": false,
				"processing": true,
				"serverSide": true,
				"destroy": true,
				"ajax": {
				"url": "{{route('listadoCliente')}}",
			    "data": {
			        "formSearch": $('#frmPersonaConsulta').serializeArray() }
			    }

				});
		}//function





		  $('#idEmpresaAgencia').change(function(){
    		
		  	var idAgencia = {'idAgencia' : $('#idEmpresaAgencia').val()};
		  		$('.cargandoImg').show();
            	$('.combo').html('  ');

  				$.ajax({
                        type: "GET",
                        url: "{{route('getDatosAgencia')}}",
                        dataType: 'json',
                        data: idAgencia,

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                           $('.combo').css('color','#DB6042');
                           $('.cargandoImg').hide();
                           $('.combo').html('<b> No se pudo actualizar. </b>');
                        },
                        success: function(rsp){

                        	console.log(rsp);

                            if(rsp.resp.length != 0 ){

                            	//validar si es null
                            	 function n(n){
                                if(n === null){ return '';  
                            	} 
                            	return n;
                                }

                                 function t(n){
                                if(n === null){ return '';  
                            	} 
                            	return '- '+n;
                                }

                                // console.log('dd');
                              $('#idPersona').empty();

                               var newOption = new Option('Seleccione Persona', '', false, false);
                                $('#idPersona').append(newOption);


                                $.each(rsp.resp, function (key, item){
                                var newOption = new Option(n(item.nombre)+' '+n(item.apellido)+' '+t(item.tipo_persona.denominacion), item.id, false, false);
                                $('#idPersona').append(newOption)
                            });

                            } else{ 

                            $('#idPersona').empty();
                            var newOption = new Option('Seleccione Persona' ,'', false, false);
                                $('#idPersona').append(newOption);   
                            // $('#idPersona').select2({ disabled: true });

                                    }//else

                           			$('.combo').css('color','#489946');
                                     $('.cargandoImg').hide();
                           			$('.combo').html('<b> Combo actualizado. </b>');
                          
                                }//funcion  
                });



    
  });



		   $('#estado').change(function(){
		  getDatosPersonaEstado();
		   });


		   function getDatosPersonaEstado(){

		   	$('.cargandoImg').show();
            $('.combo').html('  ');

		   	 	var dataString = {'idEstado' : $('#estado').val(),
		   	 					  'idEmpresa': $('#idEmpresaAgencia').val()};

		   		$.ajax({
                        type: "GET",
                        url: "{{route('getDatoPersonaEstado')}}",
                        dataType: 'json',
                        data: dataString,

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                           $('.cargandoImg').hide();
                           $('.combo').html(' No se pudo actualizar. ');
                           $('.combo').css('color','#DB6042');
                        },
                        success: function(rsp){

                        	// console.log(rsp);

                            if(rsp.resp.length != 0 ){

                            	//validar si es null
                            	 function n(n){
                                if(n === null){ return '';  
                            	} 
                            	return n;
                                }

                                 function t(n){
                                if(n === null){ return '';  
                            	} 
                            	return '- '+n;
                                }

                                // console.log('dd');
                              $('#idPersona').empty();

                               var newOption = new Option('Seleccione Persona', '', false, false);
                                $('#idPersona').append(newOption);

                                $.each(rsp.resp, function (key, item){


                                var newOption = new Option(n(item.nombre)+' '+n(item.apellido)+' '+t(item.tipo_persona), item.id, false, false);
                                $('#idPersona').append(newOption);
                            })

                            } else{ 

                            $('#idPersona').empty();
                            var newOption = new Option('Seleccione Persona' ,'', false, false);
                                $('#idPersona').append(newOption);   
                            // $('#idPersona').select2({ disabled: true });

                                    }//else

                           			$('.combo').html('<b> Combo actualizado. </b>');
                           			$('.combo').css('color','#489946');
                           			
                          
                                }//funcion  
                }).done(function(){
                	 $('.cargandoImg').hide();
                });

		   }

		   $("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                $('#frmPersonaConsulta').attr('method','post');
                $('#frmPersonaConsulta').attr('action', "{{route('generarExcelPersona')}}").submit();
            });

	</script>
@endsection