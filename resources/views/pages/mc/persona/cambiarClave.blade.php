{{-- Funcionamiento --}}

@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent

@endsection
@section('content')


<style type="text/css">
 .col-center{
  float: none;
  margin-left: auto;
  margin-right: auto;
}   

.input-bold{
    /*font:small-caption;*/
    /*font-family: caption*/
     font: bold 100% ,sans-serif;
    font-size:20px;
}

.eye{
    background: #0A77C7 !important;
    color:white;
    cursor: pointer;
}
</style>


<section id="base-style">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><i class="fa fa-pencil"></i> Cambiar Clave</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
                <form id="frmPassword" method="get" action="" autocomplete="off" style="margin-top: 2%;">



                    <div class="row">
                        <div class="col-3 col-center">
                            <div class="form-group">
                                <label>CLAVE ANTERIOR (*)</label>

                                <div class="input-group">
                                    <div class="input-group-prepend" id="btnVer1">
                                        <button class="btn btn-warning" type="button"><i class="fa fa-eye"></i></button>
                                    </div>
                                    <input type="password" class="form-control input-bold" name="clave_anterior"
                                        id="input1">
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-3 col-center">
                            <div class="form-group">
                                <label>NUEVA CLAVE (*)</label>

                                <div class="input-group">
                                    <div class="input-group-prepend" id="btnVer2">
                                        <button class="btn btn-warning" type="button"><i class="fa fa-eye"></i></button>
                                    </div>
                                    <input type="password" class="form-control input-bold" name="clave_nueva"
                                        id="input2">
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 1px;">
                        <div class="col-3 col-center">
                            <div class="form-group text-center">
                                <button type="button" form="frmPersona" id="btnGuardar" class="btn btn-lg btn-success">
                                    <b>CAMBIAR CLAVE</b></button>
                            </div>
                        </div>
                    </div>




                </form>
            </div>
        </div>
    </div>
</section>


@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script>



    $('#btnVer1').click(function(){
        if($('#input1').attr('type') == 'text'){
         $('#input1').attr('type','password');
        } else {
         $('#input1').attr('type','text');    
        }
    });


     $('#btnVer2').click(function(){
        if($('#input2').attr('type') == 'text'){
         $('#input2').attr('type','password');
        } else {
         $('#input2').attr('type','text');    
        }
    });


       $('#btnGuardar').click(function(){
         $('#input2').attr('type','text');  
         $('#input1').attr('type','text');

         var clave1 = $('#input2').val();
         var clave2 = $('#input1').val();
     
         $('#input2').attr('type','password');
        $('#input1').attr('type','password'); 

        if( clave1 != '' && clave2 != ''){
          $.blockUI({
                                        centerY: 0,
                                        message: "<h2>Procesando...</h2>",
                                        css: {
                                            color: '#000'
                                        }
                                    });
                                    


        var dataString = $('#frmPassword').serialize();
        

  $.ajax({
                        type: "GET",
                        url: "{{route('cambiarClave')}}",
                        dataType: 'json',
                        data: dataString,

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                            $.unblockUI();

                        },
                        success: function(rsp){

                            $.unblockUI();
                            if(rsp.response == true){

                                 $.toast({
                                heading: 'Success',
                                text: 'Cambio de password realizado con exito.',
                                position: 'top-right',
                                 hideAfter: false,
                                icon: 'success'
                            });

                            } else {
                                msg = 'Clave anterior incorrecta'
                                if(rsp.err != 0){ msg = 'Ocurrio un error al intentar cambiar la clave' }
                                 
                           $.toast({
                            heading: 'Error',
                            text: msg,
                            position: 'top-right',
                            hideAfter: false,
                            icon: 'error'
                               
                        });
                            }//else
                          
                          
                                }//funcion  
                });


 } else {
     $.unblockUI();
     $.toast({
          heading: 'Error',
          text: 'Todos los campos son requeridos (*).',
          showHideTransition: 'fade',
           position: 'top-right',
          icon: 'error'
      });
 }
    
  });



</script>

@endsection