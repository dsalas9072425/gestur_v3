{{-- Funcionamiento --}}

@extends('masters')
@section('title', 'Editar Personas')
@section('styles')
@parent

@endsection
@section('content')
<link rel="stylesheet" href="{{asset('gestion/app-assets/css/tagify.css')}}">

<style type="text/css">
    .frm > div {
    margin-bottom: 10px;
}

.error {
    border: 1px solid #c80000;
}


.tagify-container{
    min-height: 100px;
    height: auto;
    width:100%;
}

.borderErrorBtn {
    border:5px solid red;
}

    .question {
        color:#8F96EC  ;
    }

    .question2 {
         color:#8F96EC  ;
    }

    .card,.card-header {
	border-radius: 14px !important;
}

</style>


<section id="base-style">

    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Formulario de Personas</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">

<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="tabPersona" data-toggle="tab" aria-controls="tabIcon21" href="#home" role="tab"
            aria-selected="true">
            <i class="fa fa-play"></i>Persona</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="tabDtpMundo" data-toggle="tab" aria-controls="tabIcon22" href="#menuDtpMundo" role="tab"
            aria-selected="false">
            <i class="fa fa-flag"></i>Online</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="tabFacturacion" data-toggle="tab" aria-controls="tabIcon23" href="#menuFacturacion"
            role="tab" aria-selected="false">
            <i class="fa fa-cog"></i>Facturación</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="tabNegociacion" data-toggle="tab" aria-controls="tabIcon23" href="#menuNegociacion"
            role="tab" aria-selected="false">
            <i class="fa fa-cog"></i>Negociación</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="tabAdministrativo" data-toggle="tab" aria-controls="tabIcon23"
            href="#menuAdministrativo" role="tab" aria-selected="false">
            <i class="fa fa-cog"></i>Datos Administrativos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="tabComercial" data-toggle="tab" aria-controls="tabIcon23" href="#menuComercial"
            role="tab" aria-selected="false">
            <i class="fa fa-cog"></i>Comercial</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="tabLocalizacion" data-toggle="tab" aria-controls="tabIcon23" href="#menuLocalizacion"
            role="tab" aria-selected="false">
            <i class="fa fa-cog"></i>Localización</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" style="display:none;" id="tabEmpresaPersona" data-toggle="tab" aria-controls="tabIcon23" href="#sucursalEmpresaSection" role="tab" aria-selected="false">
            <i class="fa fa-cog"></i>Empresa Persona</a>
    </li>

</ul>

    <form id="frmPersona" method="post" action="{{route('guardarPersona')}}">
        <div class="tab-content">
        <section id="home" class="tab-pane active box-home" role="tabpanel">

            <div class="pt-1">
                <div class="pull-right"> 
                    <button class="btnNextTab btn btn-success btn-lg pull-right" id="btnNextPersona">
                        Siguiente</button></div>
                <h1 class="subtitle hide-medium">Editar Persona</h1>
                <h6>(*) Campo Obligatorio</h6>
            </div>
            <div class="row">
                @if($cambiar_tipo_persona)
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="">Tipo (*)</label>
                            <select class="form-control select2" name="tipoPersona" id="formTipoPersona"
                                style="width: 100%;">
                                <option value="">Seleccione Tipo Persona</option>

                                @foreach($tipoPersona as $personas)
                                <option value="{{$personas->id}}">{{$personas->denominacion}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                @else 
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="">Tipo</label>
                            <input type="text" class="form-control" value="{{isset($persona->tipoPersona) ? $persona->tipoPersona->denominacion : 'ERROR'}}" readonly>
                        </div>
                    </div>
                    <input type="hidden" name="tipoPersona" value="{{$persona->id_tipo_persona}}">
                @endif
                <div class="col-12 col-sm-6 col-md-5 col-lg-3">
                    <div class="form-group">
                        <label for="">Diplomatico (*)</label>
                        <select class="select2 form-control" name="diplomatico" id="diplomatico" style="width: 100%;">
                            <option value="false" @if($persona->diplomatico == false) selected @endif>No</option>
                            <option value="true" @if($persona->diplomatico == true) selected @endif>Sí</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                   <div class="row">
                        <div class="col-12 col-sm-6 col-md-10" id="inputDocumentoIdentidad">
                            <div class="form-group">
                                <label for="formDocumentoIdentidad">Documento de Identidad <span class="formDocumentoIdentidad"></span><span class="mensaje_err_documento"></span></label>
                                <input type="text" class=" form-control" name="documentoIdentidad" id="formDocumentoIdentidad" value="{{$persona->documento_identidad}}" maxlength="100" />
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-2" style="padding-left: 0px;">
                            <div class="form-group">
                                <label for="formDv">DV <span class="formDv"></span><span class="mensaje_err_documento"></span></label>
                                <input type="text" class=" form-control" name="dv" id="formDv" disabled="disabled" value="{{$persona->dv}}" maxlength="15" />
                            </div>
                        </div> 
                    </div>
                </div>    
                <div class="col-12 col-sm-6 col-md-3" id="inputTipoDocumento">
                    <div class="form-group">
                        <label for="formTipoDocumento">Tipo de Documento <span class="formTipoDocumento"></span>
                            <span class="msj_doc_permiso"></span></label>
                        <select class="form-control select2" name="tipoDocumento" disabled id="formTipoDocumento"
                            style="width: 100%;">
                            <option value="">Seleccione Tipo Doc.</option>

                            @foreach($tipoIdentidad as $identidad)
                            <option value="{{$identidad->id}}">{{$identidad->denominacion}}</option>

                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3" id="inputNombres">
                    <div class="form-group">
                        <label for="formNombres"><span id="nombrePersona"> Nombre o Razón Social </span> <span
                                class="formNombres"></span></label>
                        <input type="text" class="Requerido form-control" name="nombres" id="formNombres"
                            value="{{$persona->nombre}}" maxlength="100" />
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3" id="inputApellido">
                    <div class="form-group">
                        <label for="formApellido">Apellido <span class="formApellido"></span></label>
                        <input type="text" class="Requerido form-control" name="apellido" id="formApellido"
                            value="{{$persona->apellido}}" maxlength="100" />
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3" id="inputDenominacionComercial">
                    <div class="form-group">
                        <label for="formDenominacionComercial">Denominación Comercial <span
                                class="formDenominacionComercial"></span></label>
                        <input type="text" class="Requerido form-control" name="denominacionComercial"
                            id="formDenominacionComercial" value="{{$persona->denominacion_comercial}}"
                            maxlength="200" />
                    </div>
                </div>


                <div class="col-12 col-sm-6 col-md-3" id="inputFechaNacimiento">
                    <div class="form-group">
                        <label for="formFechaNacimiento">Fecha de Nacimiento o Creación <span class="formFechaNacimiento"></span></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                            </div>
                            <input type="text" class="form-control pull-right fecha" name="fechaNacimiento"
                                id="formFechaNacimiento" value="{{$persona->fecha_nacimiento}}" maxlength="10">
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3" id="inputCorreo">
                    <div class="form-group">
                        <label>Correo Electronico Principal <span class="formCorreo"></span> <span
                                class="mensaje_err"></span></label>
                        <input type="email" class="Requerido form-control" name="correo" id="formCorreo" value="{{$persona->email}}" maxlength="100" />
                    </div>
                </div>
                <input type="hidden" value="0" class=" form-control" name="validacionCorreo" id="validacionCorreo"/>    
                <div class="col-12 col-sm-6 col-md-3" id="inputCelular">
                    <div class="form-group">
                        <label for="formCelular">Celular <span class="formCelular"></span></label>
                        <input type="text" class="Requerido form-control" name="celular" id="formCelular"
                            value="{{$persona->celular}}" maxlength="30" placeholder=" Máximo 30 caracteres" />
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3" id="inputTelefono">
                    <div class="form-group">
                        <label for="formTelefono"><span id="cambioTel">Teléfono</span> <span
                                class="formTelefono"></span></label>
                        <input type="text" class="Requerido form-control" name="telefono" id="formTelefono"
                            value="{{$persona->telefono}}" maxlength="30" placeholder="Máximo 30 caractere" />
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputAgencia">
                    <div class="form-group">
                        <label for="formAgencia">Agencia / Empresa <span class="formAgencia"></span></label>
                        <select class="form-control select2" name="empresa" id="formAgencia" style="width: 100%;">
                            @if($agencias != null)
                            @foreach($agencias as $agencia)
                            <option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
                            @endforeach
                            @endif

                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputSucursalEmpresa">
                    <div class="form-group">
                        <label for="formSucursalEmpresa">Sucursal Agencia / Empresa <span
                                class="formSucursalEmpresa"></span></label>
                        <select class="form-control select2" name="sucursalEmpresa" id="formSucursalEmpresa"
                            style="width: 100%;" disabled>

                            <option value="">Seleccione una Sucursal</option>

                            @if($sucursalAgencias != null)
                            @foreach($sucursalAgencias as $sucursal)

                            <option value="{{$sucursal->id}}">{{$sucursal->nombre}}</option>

                            @endforeach
                            @endif


                        </select>
                    </div>
                </div>


                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputSucursalContable">
                    <div class="form-group">
                        <label for="formSucursalContable">Sucursal Contable <span class="formSucursalContable"></span></label>
                        <select class="form-control select2" name="id_sucursal_contable" id="formSucursalContable" style="width: 100%;" disabled>
                            <option value="">Seleccione una Sucursal</option>
                            @foreach($sucursal_contable as $sucursal_cont)
                                 <option value="{{$sucursal_cont->id}}">{{$sucursal_cont->nombre}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>




                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputActivoPersona">
                    <div class="form-group">
                        <label for="">Activo en GESTUR<span class="formActivoPersona"></span></label>
                        <select class="form-control select2" name="activoPersona" id="formActivoPersona"
                            style="width: 100%;">
                            <option value="1">SI</option>
                            <option value="false">NO</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3" id="inputPassword">
                    <div class="form-group">
                        <label for="formPassword">Password <span class="formPassword"></span></label>
                        <input type="password" class="form-control" name="password" id="formPassword" maxlength="100"
                            value="{{$persona->password}}" disabled placeholder="Máximo 100 caracteres" />
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputTipoProveedor">
                    <div class="form-group">
                        <label for="">Tipo Proveedor<span class="formTipoProveedor"></span></label>
                        <select class="form-control select2" name="tipoProveedor" id="formTipoProveedor" style="width: 100%;">
                            @foreach($tipoProveedores as $tipoProveedor)
                                <option value="{{$tipoProveedor->id}}">{{$tipoProveedor->denominacion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputRetenerIva">
                    <div class="form-group">
                        <label for="">Retener IVA<span class="formRetenerIva"></span></label>
                        <select class="form-control select2" name="retenerIva" id="formRetenerIva" style="width: 100%;">
                            <option value="true">SI</option>
                            <option value="false">NO</option>
                        </select>
                   </div>
                </div>

                <div class="col-12 col-lg-12 pt-1" id="inputImage">
                    <a type="button" form="" id="formImage" class="btn btn-info" data-toggle="modal"
                        data-target="#modalAdjunto"><b><i class="fa fa-fw fa-image"></i> SUBIR IMAGEN</b></a>
                </div>

            </div>
        </section>

        <section id="menuDtpMundo" class="tab-pane box-menuDtpMundo" role="tabpanel">

            <div class="row">

                <div class="col-12 mt-1">
                    <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
                   <h1 class="subtitle hide-medium" >Online</h1>
               </div>  
    


                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputActivoDtpMundo">
                    <div class="form-group">
                        <label>Activo Online<span class="formActivoDtpMundo"></span></label>
                        <select class="form-control select2" name="activoDtpMundo" id="formActivoDtpMundo"
                            style="width: 100%;">
                            <option value="1">SI</option>
                            <option value="false">NO</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputProveedorOnline">
                    <div class="form-group">
                        <label>Proveedor Online<span class="formProveedorOnline"></span></label>
                        <select class="form-control select2" name="activoProveedorOnline" id="formProveedorOnline"
                            style="width: 100%;">
                            <option value="false">NO</option>
                            <option value="1">SI</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3" id="inputTokenNemo">
                    <div class="form-group">
                        <label for="online">Token Nemo <span class="formTokenNemo"></span></label>
                        <input type="text" class="form-control" name="token_nemo" id="formTokenNemo" value="{{$persona->token_nemo}}" />
                    </div>
                </div>


                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputCodigoAmadeus">
                    <div class="form-group">
                        <label> <span id="codigoAmadeus-n">Codigo Proveedor</span> <span
                                class="formCodigoAmadeus"></span></label>
                        <input type="text" class="form-control" value="{{$persona->usuario_amadeus}}"
                            name="CodigoAmadeus" id="formCodigoAmadeus" maxlength="10" />
                    </div>
                </div>


                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputformPuedeReservar">
                    <div class="form-group">
                        <label>Puede Reservar <span class="formPuedeReservar"></span></label>
                        <select class="form-control select2" name="puedeReservar" id="formPuedeReservar"
                            style="width: 100%;">
                            <option value="1">SI</option>
                            <option value="false">NO</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputPuedeReservarEnGastos">
                    <div class="form-group">
                        <label>Puede Reservar en Gastos <span class="formPuedeReservarEnGastos"></span></label>
                        <select class="form-control select2" name="puedeReservarEnGastos" id="formPuedeReservarEnGastos"
                            style="width: 100%;">
                            <option value="false">NO</option>
                            <option value="1">SI</option>
                        </select>
                    </div>
                </div>




                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputFacturacionAutomaticaEnGastos"
                   >
                    <div class="form-group">
                        <label>Facturación Automática en Gastos <span
                                class="formFacturacionAutomaticaEnGastos"></span></label>
                        <select class="form-control select2" name="facturacionAutomaticaEnGastos"
                            id="formFacturacionAutomaticaEnGastos" style="width: 100%;">
                            <option value="false">NO</option>
                            <option value="1">SI</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputVoucher">
                    <div class="form-group">
                        <label for="">Descargar Voucher (Factura con Saldo)<span class="formVoucher"></span></label>
                        <select class="form-control select2" name="voucher" id="formVoucher" style="width: 100%;">
                            <option value="">Seleccione opción</option>
                            <option value="1">SI</option>
                            <option value="false">NO</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3" id="inputTelefono">
                    <div class="form-group">
                        <label for="formTelefono"><span id="cambioTel">Fecha de Alta</span> <span
                                class="formTelefono"></span></label>
                        <input type="text" class="Requerido form-control" name="fecha_alta" id="fecha_alta"
                            value="{{$persona->fecha_alta}}" maxlength="30" placeholder="Máximo 30 caractere" disabled/>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3" id="inputTelefono">
                    <div class="form-group">
                        <label for="formTelefono"><span id="cambioTel">Usuario Alto</span> <span
                                class="formTelefono"></span></label>
                        @if(isset($persona->usuarioAuditoria->nombre))
                            <input type="text" class="Requerido form-control" name="telefono" id="formTelefono"
                                value="{{$persona->usuarioAuditoria->nombre}} {{$persona->usuarioAuditoria->apellido}}" maxlength="30" disabled/>
                        @else
                            <input type="text" class="Requerido form-control" name="telefono" id="formTelefono"
                                    value="" maxlength="30" disabled/>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-sm-3 col-md-3" >
                            <div class="form-group">
                                <label for="kontrol">Kontol <span class="kontrol"></span></label>
                                <input type="text" class=" form-control" name="kontrol" id="kontrol" maxlength="15" readonly />
                            </div>
                        </div>
                
                <div class="col-12 col-sm-6 col-md-3" id="inputCodigoNemo">
                    <div class="form-group">
                        <label for="online">Codigo Nemo <span class="formCodigoNemo"></span></label>
                        <input type="text" class="form-control" name="codigoNemo" id="formCodigoNemo" value="{{$persona->cod_nemo}}" />
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputCodigoCangoro">
                    <div class="form-group">
                        <label for=""><span id="codigoCangoro-n">Codigo Cangoroo</span> <span class="formCodigoCangoro"></span></label>
                        <input type="text" class="form-control" name="codigoCangoro" id="formCodigoCangoro" maxlength="10" value="{{$persona->cod_cangoroo}}" />
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputCodigoNemo">
                    <div class="form-group">
                        <label for=""><span id="usuario_sabre-n">Usuario Sabre</span> <span class="formCodigoNemo"></span></label>
                        <input type="text" class="form-control"  name="usuario_sabre" id="usuario_sabre" value="{{$persona->usuario_sabre}}" />
                    </div>
                </div>   
                <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputCodigoNemo">
                    <div class="form-group">
                        <label for=""><span id="usuario_amadeus-n">Usuario Amadeus</span> <span class="formCodigoNemo"></span></label>
                        <input type="text" class="form-control"  name="usuario_amadeus" id="usuario_amadeus" value="{{$persona->usuario_amadeus}}" />
                    </div>
                </div> 
                <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputCodigoNemo">
                    <div class="form-group">
                        <label for=""><span id="cod_copa-n">Usuario Copa</span> <span class="formCodigoNemo"></span></label>
                        <input type="text" class="form-control"  name="cod_copa" id="cod_copa" value="{{$persona->cod_copa}}" />
                    </div>
                </div>

            </div>
        </section>

        <section id="menuFacturacion" class="tab-pane box-menuFacturacion" role="tabpanel">

            <div class="row">

                <div class="col-12 mt-1">
                    <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
                   <h1 class="subtitle hide-medium" >Facturación</h1>
               </div>  
    

                    <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputIvaComisiones">
                        <div class="form-group">
                            <label>Iva Comisiones <span class="formIvaComisiones"></span></label>
                            <select class="form-control select2" name="ivaComisiones" id="formIvaComisiones"
                                style="width: 100%;">
                                <option value="">Seleccione una opción</option>
                                <option value="1">SI</option>
                                <option value="false">NO</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputTipoFacturacion">
                        <div class="form-group">
                            <label for="formTipoFacturacion">Facturacion <span
                                    class="formTipoFacturacion"></span></label>
                            <select class="form-control select2" name="tipoFacturacion" id="formTipoFacturacion"
                                style="width: 100%;">
                                <option value="">Seleccione Facturacion</option>

                                @foreach($tipoFacturacion as $tFacturacion)
                                <option value="{{$tFacturacion->id}}">{{$tFacturacion->denominacion}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>


            </div>
        </section>
        <section id="menuNegociacion" class="tab-pane box-menuNegociacion" role="tabpanel">

            <div class="row">

                <div class="col-12 mt-1">
                    <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
                   <h1 class="subtitle hide-medium" >Negociación</h1>
               </div> 


                    <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label>Linea de Credito</label>
                            <input type="text" class="Requerido form-control" value="{{$monto}}" disabled>
                        </div>
                    </div>


                    <div class="col-12 col-sm-6 col-md-3" id="inputPlazoPago">
                        <div class="form-group">
                            <label for="">Plazo de Pago <span class="formPlazoPago"></span></label>
                            <select class="form-control select2" name="plazoPago" id="formPlazoPago"
                                style="width: 100%;">
                                <option value="">Seleccione una opción</option>

                                @foreach($plazosPago as $plazo)
                                <option value="{{$plazo->id}}">{{$plazo->denominacion}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputPlazo">
                        <div class="form-group">
                            <label for="formPlazo">Plazo de Cobro <span class="formPlazo"></span></label>
                            <input type="number" class="form-control" name="plazo" id="formPlazo" value="{{$persona->plazo_cobro}}"/>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputPlazoCobro">
                        <div class="form-group">
                            <label>Plazo Cobro / Ticket <span class="formPlazoCobro"></span></label>
                            <select class="form-control select2" name="plazoCobro" id="formPlazoCobro"
                                style="width: 100%;">
                                <option value="">Seleccione una opción</option>
                                @foreach($plazoCobro as $plazoCob)
                                <option value="{{$plazoCob->id}}">{{$plazoCob->denominacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-12" id="inputAcuerdoEspecial" style="display:none">
                            <div class="form-group">
                                <label style="font-weight: 600;font-size: 18px;">Acuerdo Especial <span class="formAcuerdoEspecial"></span></label>
                                <textarea class="form-control" rows="5"  id="formAcuerdoEspecial"></textarea>
                            </div>
                    </div>
                    <input type="hidden" value="" id="acuerdo_id" name="acuerdo">   


            </div>
        </section>
        <section id="menuAdministrativo" class="tab-pane box-menuAdministrativo" role="tabpanel">
            <div class="row">

                
                <div class="col-12 mt-1">
                    <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
                   <h1 class="subtitle hide-medium" >Administrativos</h1>
               </div> 

                

                    <div class="col-12 col-sm-12 col-md-6" id="inputCorreoAdministrativo">
                        <div class="form-group">
                            <label>Correos Administrativos <span class="formCorreoAdministrativo"></span></label> <i
                                class="fa fa-fw fa-question-circle question"></i>
                            <textarea class="form-control" name="correoAdministrativo"
                                id="correo_administrativo"></textarea>

                            <div class="checkbox">

                                <label for="formNotificacionCorreo">
                                    <input type="checkbox" id="formNotificacionCorreo" name="notificacionCorreo"> Enviar
                                    Extracto

                                    <span class="formNotificacionCorreo"></span>
                                </label>
                            </div>
                        </div>

                    </div>



                    <div class="col-12 col-sm-12 col-md-3" id="inputTipoPersoneria">
                        <div class="form-group">
                            <label for="formTipoPersoneria">Personeria <span class="formTipoPersoneria"></span></label>
                            <select class="form-control select2" name="tipoPersoneria" id="formTipoPersoneria"
                                style="width: 100%;">
                                <option value="">Seleccione Personeria</option>

                                @foreach($tipoPersoneria as $tPersoneria)
                                <option value="{{$tPersoneria->id}}">{{$tPersoneria->denominacion}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>


                        <div class="col-12 col-sm-6 col-md-3" id="inputAgenteRetentor">
                            <div class="form-group">
                                <label for="formTipoPersoneria">Agente Retentor <span class="formAgenteRetentor"></span></label>
                                <select class="form-control select2" name="agenteRetentor" id="formAgenteRetentor" style="width: 100%;">
                                    <option value="false">NO</option>
                                    <option value="1">SI</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputBanco">
                                <div class="form-group">
                                    <label>Banco<span class="formBanco"></span></label>
                                    <select class="form-control select2" name="banco" id="formBanco">
                                        <option value="">Todos</option>
                                        @foreach($bancos as $banco)
                                            <option value="{{$banco->id}}">{{$banco->nombre}}</option>
                                        @endforeach

                                    </select>
                                </div> 
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputNroCuenta">
                                <div class="form-group">
                                    <label>Nro. de Cuenta  <span class="formNroCuenta"></span></label>
                                    <input type="text" class="form-control" name ="nroCuenta" value="{{$persona->nro_cuenta}}" id="formNroCuenta">
                                </div>
                            </div>    
                        <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputEmisorCheque">
                            <div class="form-group">
                                <label>Beneficiario <span class="formEmisorCheque"></span></label>
                            <input type="text" class="form-control" name ="emisorCheque" value="{{$persona->cheque_emisor_txt}}" id="formEmisorCheque">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputMigrar">
                            <div class="form-group">
                                <label for="">Generar Costo/LC<span class="formMigrar"></span></label>
                                <select class="form-control select2" name="migrar" id="formMigrar" style="width: 100%;">
                                    <option value="">Seleccione opción</option>
                                    <option value="1">SI</option>
                                    <option value="false">NO</option>
                                </select>
                            </div>
                        </div>

            </div>            
        </section>
        <section id="menuComercial" class="tab-pane box-menuComercial" role="tabpanel">

            <div class="row">

                       
                <div class="col-12 mt-1">
                    <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
                   <h1 class="subtitle hide-medium" >Comercial</h1>
               </div> 

              

                    <div class="col-12 col-sm-12 col-md-6" id="inputCorreoComerciales">
                        <div class="form-group">
                            <label>Correos Comerciales <span class="formCorreoComerciales"></span></label> <i
                                class="fa fa-fw fa-question-circle question2"></i>
                            <textarea class="form-control" id="correo_comerciales"
                                name="correoComerciales" /></textarea>

                        </div>

                    </div>



                    <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputSucursalCarteraId">
                        <div class="form-group">
                            <label for="formSucursalCarteraId">Sucursal Cartera<span
                                    class="formSucursalCarteraId"></span></label>
                            <select class="form-control select2" name="sucursalCarteraId" id="formSucursalCarteraId"
                                style="width: 100%;" disabled>
                                <option value="">Seleccione una Sucursal</option>
                                @foreach($sucursalCartera as $sucursal)
                                <option value="{{$sucursal->id}}">{{$sucursal->nombre}}
                                    {{$sucursal->denominacion_comercial}}</option>
                                @endforeach


                            </select>
                        </div>
                    </div>



                    <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputAgente">
                        <div class="form-group">
                            <label for="">Agente <span class="formAgente"></span></label>
                            <select class="form-control select2" name="agente" id="formAgente" style="width: 100%;">
                                <option value="">Seleccione Agente</option>

                                @foreach($agente as $agentes)
                                <option value="{{$agentes->id}}">{{$agentes->nombre}} {{$agentes->apellido}}
                                    {{$agentes->email}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>



            </div>
        </section>
        <section id="menuLocalizacion" class="tab-pane box-menuLocalizacion" role="tabpanel">

                        <div class="row">

                            <div class="col-12 mt-1">
                            <h1 class="subtitle hide-medium" >Localización</h1>
                        </div> 


                            <div class="col-12 col-sm-4 col-md-4" id="inputPais">
                                <label for="formPais">Pais <span class="formPais"></span></label>
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <button type="button" data-toggle="modal" href="#requestCrearPais" title="Crear Pais" id="" class="btn btn-info btn-sm">
                                        <i class="fa fa-fw fa-plus-circle"></i>
                                    </button>
                                </div>
                                <select class="select2 form-control  pais_select" name="pais" id="formPais" style="width:90%">
                                    <option value="">Seleccione Pais</option>
                                    @foreach($pais as $paises)
                                        <option value="{{$paises->cod_pais}}">{{$paises->name_es}}</option>

                                    @endforeach
                                </select>
                                </div>
                        </div>

                        <div class="col-12 col-sm-4 col-md-4" id="inputCiudad">
                            <label for="formPais">Ciudad <span class="formPais"></span></label>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="button" data-toggle="modal" href="#requestCrearCiudad" title="Crear Pais" id="" class="btn btn-info btn-sm">
                                    <i class="fa fa-fw fa-plus-circle"></i>
                                </button>
                            </div>
                            <select class="form-control select2" name="ciudad" id="formCiudad" style="width: 90%;">
                                <option value="">Seleccione Ciudad</option>
                            </select>
                            </div>
                    </div>




                    <div class="col-12 col-sm-4 col-md-4" id="inputDireccion">
                        <div class="form-group">
                            <label for="formDireccion">Dirección <span class="formDireccion"></span></label>
                            <input type="text" class="Requerido form-control" name="direccion" id="formDireccion"
                                value="{{$persona->direccion}}" maxlength="200" />
                        </div>
                    </div>


            </div>
        </section>
        <section id="sucursalEmpresaSection" class="tab-pane box-menusucursalEmpresaSection" role="tabpanel"> 

            <div class="row">

                <div class="col-12 mt-1">
                    <h1 class="subtitle hide-medium" >Empresa Persona</h1>
                </div>

                <div class="col-6 col-sm-6 col-md-4 col-lg-4" id="inputEmpresaPersona">
                    <div class="form-group">
                       <label for="formEmpresaPersona">Empresa<span class="formEmpresaPersona"></span></label>
                       <select class="form-control select2"  id="formEmpresaPersona"  style="width: 100%;">
                           @foreach($empresas as $key=>$empresa)
                                   <option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
                           @endforeach	
                       </select>
                   </div>
               </div>
                   
                <div class="col-6 col-sm-6 col-md-4 col-lg-4" id="inputSucursaEmpresaPersona">
                    <div class="form-group">
                        <label for="formSucursaEmpresaPersona">Sucursal<span
                                class="formSucursaEmpresaPersona"></span></label>
                        <select class="form-control select2" id="formSucursaEmpresaPersona"
                            style="width: 100%;" disabled>
                            <option value="">Seleccione una Sucursal</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4 col-lg-4" id="inputBtnAddEmpresaPersona">
                    <button onclick="addEmpresaPersona()" title="Guardar Fila" class="btn btn-success mt-2 formBtnAddEmpresaPersona" type="button"><i class="fa fa-fw fa-plus"></i> <b>Agregar</b></button>
                </div>


                        <div class="col-12 col-md-10 m-auto">
								<div class="table-responsive table-bordered">
		 							<table id="listadoEmpresaPersona" class="table" style="width:100%">
						                <thead>
											<tr style="text-align:center;">
												<th>Empresa</th>
												<th>Sucursal</th>
												<th></th>
								            </tr>
						                </thead>
						                <tbody class="tbody_empre_per">
                                            @forelse($empresa_persona as $key => $value)
                                            {{-- $emp_per --}}
                                            {{-- @php $value @endphp --}}
                                        <tr id="rowEmp_{{$key}}" class="tabla_filas">
                                            <td>
                                                <b>{{$value->empresas['denominacion']}}</b>
                                                <input type="hidden" value="{{$value->empresas['id_persona']}}"  name="data_empresa[][empresa_id]"></td>
                                                <td>
                                                    <b>{{$value->sucursal['nombre']}}</b>
                                                    <input type="hidden" readonly value="{{$value->id_sucursal_empresa}}" name="data_empresa[][sucursal_id]"></td>
                                                            <td>
                                                                <button type="button" onclick="deleteTableEmpresaPersona({{$key}})"  
                                                                class="btn btn-danger"><b>Eliminar</b>
                                                                </button>
                                                            </td>
                                                        </tr>
                                            @empty    
                                            <tr>
                                                <td colspan="3" style="text-align:center;" class="relleno_table">
                                                    <b> Sin datos.....</b>
                                                </td>
                                            </tr>       
                                            @endforelse
                                           
						                </tbody>
					              </table>
							   </div> 
                    </div>
        
            

                </div>            
        </section> 

        <input type="hidden" name="nombreImagen" value="{{$persona->logo}}" id="imagen">
        <input type="hidden" name="correo_administrativo_array" id="input_hidden_correo_administrativo"
            value="{{$persona->correo_administrativo}}">
        <input type="hidden" name="correo_comercial_array" id="input_hidden_correo_comercial"
            value="{{$persona->correo_comerciales}}">
        <input type="hidden" name="idPersona" id="id_persona" value="{{$idPersona}}">

    </div>
</form>


</div>
</div>
<div class="card-footer">
    <div class="col-12">
        <button type="button" form="frmPersona" id="btnGuardar"
            class="btn btn-success btn-lg pull-right">Guardar</button>
    </div>
</div>
</div>




<!--======================================================
        MODAL DE ADJUNTO IMAGEN
========================================================== -->
<!-- /.content -->
<div id="modalAdjunto" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 60%;margin-top: 13%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div id="modal-header" class="modal-header">
                <h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto <i
                        class="fa fa-refresh fa-spin cargandoImg"></i></h2>
            </div>
            <div class="modal-body">
                            <div class="content" id='output'>

                                <div class="row">
                                    <div class="col-auto">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <img class="card-img-top" style="max-width:150px;" src="{{asset('personasLogo')}}/{{$persona->logo}}" alt="Adjunto Imagen Perfil">
                                                    <p><a href="{{asset('personasLogo')}}/{{$persona->logo}}" class="btn btn-info" target="_black"><b>VER</b></a></p>
                                                    <a href="#" class="card-title" style="font-size: 0.7rem; color: red;"  onclick="eliminarImagen('{{$persona->logo}}')"><b>Eliminar</b></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                     

                 
                    <form class="validator-form" id="upload" enctype="multipart/form-data" method="post"
                        action="{{route('personaUpload')}}" autocomplete="off">
                        <input type="hidden" class="form-control input-sm" name="proforma_id" value=""
                            readonly="readonly" />
                        <div class="form-group">
                            <div class="row">
                                <div>

                                </div>
                                <div class="col-12 col-sm-12 col-md-12">
                                    <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <input type="file" class="form-control" name="image" id="image"
                                        style="margin-left: 3%; width: 96%" />
                                    <h4 class="modal-title titlepage"
                                        style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;">
                                        <b>Formatos válidos: PNG, JPG, BMP y JPEG</b></h4>
                                    <div id="validation-errors"></div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">

                        <div class="col-12">
                            <button type="button" id="btnAceptarVoucher" class="btn btn-success pull-right"
                                data-dismiss="modal">Aceptar</button>
        
                            <button type="button" id="btnCancelarVSTour" class="btn btn-danger"
                                 data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
              
            </div>
        </div>
    </div>
</div>



<div id="requestCrearPais" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title titlepage" style="font-size: x-large;">CREAR PAIS <i style="display:none;"
                        class="fa fa-refresh fa-spin cargandoImg"></i></h2>
                <h6>Una vez creado el país el combo país será recargado de forma automática</h6>

                <span id="mensaje_cambio_p" style="font-weight: bold;"></span>
            </div>
            <div class="modal-body">


                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="form-group">
                                <label>Pais</label>
                                <input type="text" class="form-control" value="" id="pais_nuevo" />
                            </div>
                        </div>

                        <div class="col-sm-6">

                            <div class="form-group">
                                <label>Cod Pais</label>
                                <input type="text" class="form-control" value="" id="cod_pais_nuevo" />
                            </div>
                        </div>
                    </div>
                    <button type="button" id="crearP" class="btn btn-success">CREAR</button>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;"
                    data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>



<div id="requestCrearCiudad" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title titlepage" style="font-size: x-large;">CREAR CIUDAD <i style="display:none;"
                        class="fa fa-refresh fa-spin cargandoImg"></i></h2>
                <span id="mensaje_cambio_c" style="font-weight: bold;"></span>
            </div>
            <div class="modal-body">

               
                <div class="container-fluid">
                    <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Pais </label>
                            <select class="form-control select2 pais_select" id="pais_select_relacion"
                                style="width: 100%;">
                                <option value="">Seleccione Pais</option>
                                @foreach($pais as $paises)
                                <option value="{{$paises->cod_pais}}">{{$paises->name_es}}</option>

                                @endforeach


                            </select>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Ciudad</label>
                            <input type="text" class="form-control" value="" id="ciudad_nuevo" />
                        </div>
                    </div>
                </div>

                <button type="button" id="crearC" class="btn btn-success">CREAR</button>

                </div>   
            </div>

            <div class="modal-footer">
                <button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;"
                    data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>







@endsection
@section('scripts')
@include('layouts/gestion/scripts')

<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.tagify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>

<script>



// $('#formAcuerdoEspecial').wysihtml5({
//         toolbar: {
//             "font-styles": true, // Font styling, e.g. h1, h2, etc.
//             "emphasis": true, // Italics, bold, etc.
//             "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
//             "html": false, // Button which allows you to edit the generated HTML.
//             "link": true, // Button to insert a link.
//             "image": false, // Button to insert an image.
//             "color": false, // Button to change color of font
//             "blockquote": true, // Blockquote
//             "size": 'sm' // options are xs, sm, lg
//         }
//     });

    const persona = {
        empresa : Number('{{$idEmpresa}}')
    }

    var msj_emp = '';

//Tomar campos inputs ocultos para realizar el cambios
$(()=> {
    {{--MOSTRAR SI ES DTP--}}
    if(persona.empresa !== 1 && persona.empresa !== 17){ 
        $('#tabDtpMundo').hide(); 
        $('#btnNextPersona').hide();
        
    } 

    //INICIO SELET2
    $('.select2').select2();
    document.getElementById("formDocumentoIdentidad").addEventListener('keyup', sanear);
    document.getElementById("formCelular").addEventListener('keyup', sanear);
    document.getElementById("formTelefono").addEventListener('keyup', sanear);
    getSucursalEmpresaPersona();


    $('.btnNextTab').click(function (e) {
        e.preventDefault();
        let contTab = 5;
        let ok = 0;
        $.each($("#myTabs li a"), function (index, val) {
            if ($(val).hasClass('active')) {
                $($('#myTabs a')[index + 1]).tab('show');
                return false;
            }

        });

    });




    msjHelp();//mensaje de ayuda


    //VARIABLE GLOBAL DE CORREOS   
    correo_administrativo = [];
    correo_comercial = [];
    $input = $('#correo_comerciales');
    $input2 = $('#correo_administrativo');
    inputTagEmail();
    //Cargar Correos
    $input.data('tagify').addTags('{{$persona->correo_comerciales}}');
    $input2.data('tagify').addTags('{{$persona->correo_administrativo}}');
    inputTagEmail();

  


    $("#formPais").val(validarData('{{$persona->id_pais}}')).trigger('change.select2');
    obtenerCiudad();

    selectCiudad()
});//


function inputTagEmail() 
{

    //ADMINISTRATIVO    
    $input.tagify({
            delimiters: ",",
            templates: {
                wrapper(input, settings) {
                    return `<tags class="tagify-container tagify  ${input.className}" id="formCorreoComerciales"  readonly>
                        <span contenteditable data-placeholder="Correo Administrativos" class="tagify__input"></span>
                    </tags>`;
                    // return 'asdasd';
                }
            },
            whitelist: [{
                "id": 1,
                "value": "some string"
            }]
        })
        .on('add', function (e, tagName) {
            correo_comercial.push(tagName.data.value);
        })
        .on("invalid", function (e, tagName) {
        });

    $input.on('remove', function (e, tagName) {
        // console.log(tagName.data.value);
        $.each(correo_comercial, function (index, val) {
            if (val == tagName.data.value) {
                correo_comercial.splice(index, 1);
                // break;
            }
        });

    });





    $input2.tagify({
            delimiters: ",",
            templates: {
                wrapper(input, settings) {
                    return `<tags class="tagify-container tagify  ${input.className}" id="formCorreoAdministrativo"  readonly>
                        <span contenteditable data-placeholder="Correo Administrativos" class="tagify__input"></span>
                    </tags>`;
                    // return 'asdasd';
                }
            },
            whitelist: [{
                "id": 1,
                "value": "some string"
            }]
        })
        .on('add', function (e, tagName) {
            // console.log('JQEURY EVENT: ', 'added', tagName.data)
            correo_administrativo.push(tagName.data.value);
        })
        .on("invalid", function (e, tagName) {
            // console.log('JQEURY EVENT: ',"invalid", e, ' ', tagName);
        });

    $input2.on('remove', function (e, tagName) {
        // console.log(tagName.data.value);
        $.each(correo_administrativo, function (index, val) {
            if (val == tagName.data.value) {
                correo_administrativo.splice(index, 1);
                // break;
            }
        });

    });


    // var jqTagify2 = $input.data('tagify');




} //FUNCTION EMAIL TAG

function msjHelp()
{
    
    //MENSAJE DE AYUDA
    //ADMINISTRATIVO
    $('.question').on('click', function () {

        var coordenadas = $(".question").offset();

        $.toast({

            heading: 'Markup',
            hideAfter: 7000,
            text: 'Digite los correos y oprima <b>Enter</b> <i class="fa fa-level-up" aria-hidden="true"></i> para registrar, además puede pegar una lista de correos separados por "," comas.',
            position: {
                left: coordenadas.left,
                top: coordenadas.top
            },
            icon: 'info',
            stack: false
        });


        });

        //COMERCIAL
        $('.question2').on('click', function () {

        var coordenadas = $(".question2").offset();

        $.toast({

            heading: 'Markup',
            hideAfter: 7000,
            text: 'Digite los correos y oprima <b>Enter</b> <i class="fa fa-level-up" aria-hidden="true"></i> para registrar, además puede pegar una lista de correos separados por "," comas.',
            position: {
                left: coordenadas.left,
                top: coordenadas.top
            },
            icon: 'info',
            stack: false
        });


        });

}

function sanear(e) {
    let contenido = e.target.value;
    e.target.value = contenido.replace(" ", "");
}


function validarData(data) {
    if (data == 'null') {
        return '';
    }
    return data;
}

function selectN(data) {

    if (data == 'null' || data == '') {
        return 'false';
    }
    return data;
}



/**
 * Convierte un texto de la forma 2017-01-10 a la forma
 * 10/01/2017
 *
 * @param {string} texto Texto de la forma 2017-01-10
 * @return {string} texto de la forma 10/01/2017
 *
 */
function formatoFecha(texto) {
    if (texto != '' && texto != null && texto != '0') {
        return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3/$2/$1');
    } else {
        return '';
    }

}


//=====================================
//  REALIZAR LAS SELECCIONES DEL SELECT2
//===================================== 

function poblarSelect() 
{
    $("#formTipoPersona").val(validarData({{$persona->id_tipo_persona}})).trigger('change.select2');

    campoTipoPersona('{{$persona->id_tipo_persona}}');

    var agencia = "{{$persona->id_persona}}";
    var sucursal = "{{$persona->id_sucursal_empresa}}";

    if (agencia != null && agencia != '') {
        $("#formAgencia").val(validarData({{$persona->id_persona}})).trigger('change.select2');
    }
    if ($('#imagen').val() != '') {
        $('#image').prop('disabled', true);
    }

    var fecha = '{{$persona->fecha_nacimiento}}';
    //alert(fecha);

    $("#formFechaNacimiento").val(formatoFecha(fecha)).trigger('change.select2');
    $("#formAgente").val(validarData('{{$persona->id_vendedor_empresa}}')).trigger('change.select2');
    $("#formTipoDocumento").val(validarData('{{$persona->id_tipo_identidad}}')).trigger('change.select2');
    $("#formTipoFacturacion").val(validarData('{{$persona->id_tipo_facturacion}}')).trigger('change.select2');
    $("#formTipoPersoneria").val(validarData('{{$persona->id_personeria}}')).trigger('change.select2');
    $("#formPlazoPago").val(validarData('{{$persona->id_plazo_pago}}')).trigger('change.select2');
   $("#formIvaComisiones").val(selectN('{{$persona->iva_de_comisiones}}')).trigger('change.select2');
   $("#formActivoPersona").val(selectN('{{$persona->activo}}')).trigger('change.select2');
   $("#formSucursalCarteraId").val(selectN('{{$persona->id_sucursal_cartera}}')).trigger('change.select2');
   $("#formPlazoCobro").val(selectN('{{$persona->id_plazo_cobro}}')).trigger('change.select2');
   $("#formProveedorOnline").val(selectN('{{$persona->proveedor_online}}')).trigger('change.select2');
   $('#formTipoProveedor').val(selectN('{{$persona->tipo_proveedor}}')).trigger('change.select2');
   if('{{$persona->retener_iva}}'== 1){
        valor = 'true';
   }else{
        valor = 'false'
   }
   $('#formRetenerIva').val(valor).trigger('change.select2');
   $('#formAgenteRetentor').val(selectN('{{$persona->agente_retentor}}')).trigger('change.select2');
   $('#formBanco').val(selectN('{{$persona->id_banco_cabecera}}')).trigger('change.select2');

   @if(isset($persona->sucursalEmpresa->id_sucursal_contable))
    $('#formSucursalContable').val('{{$persona->sucursalEmpresa->id_sucursal_contable}}').trigger('change.select2');
    @else
    $('#formSucursalContable').val('').trigger('change.select2');
    @endif
    
    //TAB DTP MUNDO
    $("#formPuedeReservar").val(selectN('{{$persona->puede_reservar}}')).trigger('change.select2');
    $("#formPuedeReservarEnGastos").val(selectN('{{$persona->puede_reservar_en_gastos}}')).trigger('change.select2');
    $("#formFacturacionAutomaticaEnGastos").val(selectN('{{$persona->facturacion_automatica_en_gastos}}')).trigger('change.select2');
    $("#formActivoDtpMundo").val(selectN('{{$persona->activo_dtpmundo}}')).trigger('change.select2');
    $("#formMigrar").val(selectN('{{$persona->migrar}}')).trigger('change.select2');
    $("#formVoucher").val(selectN('{{$persona->imprimir_voucher}}')).trigger('change.select2');

    if(sucursal != null && sucursal != ''){
    // $("#formSucursalEmpresa").attr('disabled',false);    
    $("#formSucursalEmpresa").val(validarData('{{$persona->id_sucursal_empresa}}')).trigger('change.select2'); 
    }

} //poblar select

var notificacionEmail = '{{$persona->notificacion_email}}';
if (notificacionEmail == 1) {
    $("#formNotificacionCorreo").prop("checked", '#formNotificacionCorreo');
}


        $('#formDocumentoIdentidad').keyup(function(e){
            agregarDv();
           });//function

           function agregarDv(){
                $.ajax({
                        type: "GET",
                        url: "{{ route('agregarDv') }}",
                        dataType: 'json',
                        data: { documento: $('#formDocumentoIdentidad').val().trim()},

                         error: function(jqXHR,textStatus,errorThrown){
                            $('.mensaje_err_documento').html(' No se pudo validar. ');
                            $('.mensaje_err_documento').css('color','red');
                            inputReq.VerificarDoc(1,false);
                      
                        },
                        success: function(rsp){
                           // console.log(rsp[0]);
                           if(jQuery.isEmptyObject(rsp[0]) == false){
                                if(jQuery.isEmptyObject(rsp[0].dv) == true){
                                    $('#documentoIdentidad').val(rsp[0].ruc);
                                    $('#formDv').val(rsp[0].dv);
                                    $('#formDv').prop('disabled',true);
                                    $('#formTipoDocumento').val(2).trigger('change.select2');
                                }else{
                                    $('#formDv').prop('disabled',false);
                                    $('#formTipoDocumento').val(1).trigger('change.select2');
                                }

                                if(jQuery.isEmptyObject(rsp[0].nombre) == false){
                                    denominacion = rsp[0].nombre;              
                                    if(denominacion.indexOf(",") == true){          
                                            base = denominacion.split(',');
                                            $('#formNombres').val(base[1]);
                                            $('#formApellido').val(base[0]);
                                    }else{
                                        $('#formNombres').val(denominacion);
                                        $('#formApellido').val('');
                                    }

                                }else{
                                    $('#formNombres').val('');
                                    $('#formApellido').val('');
                                }
                            }else{
                                $('#formDv').prop('disabled',false);
                                $('#formTipoDocumento').val('').trigger('change.select2');
                                $('#formNombres').val('');
                                $('#formApellido').val('');
                                $('#formDv').val('');
                           }                       
                       }
                })   
           }     



function marcarTab() {


    //SI EN ESA PESTAÑA HAY UN MENSAJE DE ERROR MARCAR PRESTAÑA
    if ($('.box-home').has(".inputError").length != 0) {
        $('#tabPersona').css('color', '#E7491F');
    } else {
        $('#tabPersona').css('color', '#328E20');
    }

    if ($('.box-menuDtpMundo').has(".inputError").length != 0) {
        $('#tabDtpMundo').css('color', '#E7491F');
    } else {
        $('#tabDtpMundo').css('color', '#328E20');
    }

    if ($('.box-menuFacturacion').has(".inputError").length != 0) {
        $('#tabFacturacion').css('color', '#E7491F');
    } else {
        $('#tabFacturacion').css('color', '#328E20');
    }

    if ($('.box-menuNegociacion').has(".inputError").length != 0) {
        $('#tabNegociacion').css('color', '#E7491F');
    } else {
        $('#tabNegociacion').css('color', '#328E20');
    }
    if ($('.box-menuAdministrativo').has(".inputError").length != 0) {
        $('#tabAdministrativo').css('color', '#E7491F');
    } else {
        $('#tabAdministrativo').css('color', '#328E20');
    }

    if ($('.box-menuComercial').has(".inputError").length != 0) {
        $('#tabComercial').css('color', '#E7491F');
    } else {
        $('#tabComercial').css('color', '#328E20');
    }

    if ($('.box-menuLocalizacion').has(".inputError").length != 0) {
        $('#tabLocalizacion').css('color', '#E7491F');
    } else {
        $('#tabLocalizacion').css('color', '#328E20');
    }




} //fucntion


var listaPrincipal =   ['Nombres',
                       'Apellido',
                       'DenominacionComercial',
                       'FechaNacimiento',
                       'Pais',
                       'Ciudad',
                       'ActivoPersona',
                       'Direccion',
                       'Correo',
                       'IvaComisiones',
                       'NotificacionCorreo',
                       'TipoDocumento',
                       'DocumentoIdentidad',
                       'Migrar',
                       'PuedeReservar',
                       'PuedeReservarEnGastos',
                       'FacturacionAutomaticaEnGastos',
                       'Celular',
                       'Telefono',
                       'PlazoCobro',
                       'CorreoAdministrativo',
                       'CorreoComerciales',
                       'ActivoDtpMundo',
                       'SucursalCarteraId',
                       'CodigoAmadeus',
                       'Agencia',
                       'SucursalEmpresa',
                       'Agente',
                       'TipoFacturacion',
                       'TipoPersoneria',
                       'PlazoPago',
                       'Password',
                       'Image',
                       'ProveedorOnline',
                       'AgenteRetentor',
                       'EmisorCheque',
                       'Voucher',
                       'SucursalContable',
                       'NroCuenta',
                       'Banco',
                       'TipoProveedor',
                       'RetenerIva',
                       'Plazo',
                       'TokenNemo',
                       'CodigoNemo',
                       'CodigoCangoro'
                       ];


//accionFormulario(listaPrincipal,[],'0');                  


const inputReq = {
    required: [],
    tipoSuc: '',
    tipoEmp: '',
    //VERIFICAR SI SE ASIGNA MISMA CATEGORIA PARA ANULAR BUSUQUEDA
    categ: true,
    correoAuth: true,
    setRequired: function (req) {
        this.required = req
    },
    getRequired: function () {
        return this.required
    },

    setTipoSucursal: function (option) {
        this.tipoSuc = option
    },
    getTipoSucursal: function () {
        return this.tipoSuc
    },

    setTipoEmpresa: function (option) {
        if (option == this.tipoEmp) {
            this.categ = false;
        } else {
            this.categ = true;
        }
        this.tipoEmp = option
    },
    getTipoEmpresa: function () {
        return this.tipoEmp
    },

    getCateg: function () {
        return this.categ
    },
    VerificarCorreo: function (o, b) {
        //opcion 1 asignar un valor
        if (o == '1') {
            this.correoAuth = b;
        }
        //opcion 2 obtener el valor
        if (o == '2') {
            return this.correoAuth;
        }
    }

};

function accionFormulario(mostrar = [], requerir, opcion) {

    var lista = [];

    if (opcion == '0') {
        $('#btnGuardar').attr('disabled', true);
        // inputReq.setRequired();
        //deshabilitarCampos y limpiar los requeridos
        for (var j = 0; j < mostrar.length; j++) {
            var id = mostrar[j];


            //deshabilitar campo
            $('#form' + id).attr('disabled', true);
            //limpiar span requerido
            $(".form" + id).html('');

            var name = $("#form" + id).get(0).tagName;

            if (name == 'INPUT') {


                //bordes de input en default
                $("#form" + id).css('border-color', '#d2d6de');
            }

            if (name == 'SELECT') {

                //estilo de borde en gris
                $("#input" + id).find('.select2-container--default .select2-selection--single').css('border', '1px solid #aaa');
            }

            if (name == 'A') {
                $("#form" + id).removeClass('borderErrorBtn');
                $('#form' + id).attr('data-toggle', '#');
            }




        } //for

    } //if

    {{-- === === === === === === === === === === === === === === === === === ==
            HABILITAR CAMPOS Y RESETEAR
         === === === === === === === === === === === === === === === === === === = --}}
    if (opcion == '1') {

        $('#btnGuardar').attr('disabled', false);
        let editDoc_permiso = '{{$permiso_doc}}';


        lista = mostrar;

        for (var j = 0; j < mostrar.length; j++) {
            var id = mostrar[j];


            if (id == 'CorreoAdministrativo') {
                $('#formCorreoAdministrativo').attr('readonly', false);
                continue;
            }

            if (id == 'CorreoComerciales') {
                $('#formCorreoComerciales').attr('readonly', false);
                continue;
            }

            if (id == 'DocumentoIdentidad' || id == 'TipoDocumento') {

                if (editDoc_permiso === '1') {
                    $('#form' + id).attr('disabled', false);
                } else {
                    $('.msj_doc_permiso').css('color', 'red');
                    $('.msj_doc_permiso').html('Sin permisos para editar.');
                }
                continue;
            }


            if (id != 'SucursalEmpresa' || id != 'Ciudad') {
                //habilitar campo
                $('#form' + id).attr('disabled', false);
            }


        } //for


        /*=====================================================
                SI HAY CAMPOS REQUERIDO SE HABILITAN AQUI
    =======================================================*/
        if (requerir != '' && requerir.length > 0) {

            lista = lista.concat(requerir);

            inputReq.setRequired(requerir);

            for (var j = 0; j < requerir.length; j++) {
                var id = requerir[j];


                if (id == 'CorreoAdministrativo') {
                    $('#formCorreoAdministrativo').attr('readonly', false);

                }

                if (id == 'CorreoComerciales') {
                    $('#formCorreoComerciales').attr('readonly', false);

                }

                if (id == 'DocumentoIdentidad' || id == 'TipoDocumento') {

                    if (editDoc_permiso === '1') {
                        $('#form' + id).attr('disabled', false);

                    } else {
                        $('.msj_doc_permiso').css('color', 'red');
                        $('.msj_doc_permiso').html('Sin permisos para editar.');

                        $('#form' + id).attr('disabled', true);
                    }

                }

                if (id == 'Image') {

                    $('#form' + id).attr('data-toggle', 'modal');
                    $('#form' + id).attr('disabled', false);
                    $(".form" + id).html('(*)');

                } else if (id == 'SucursalEmpresa' || id == 'Ciudad') {
                    $(".form" + id).html('(*)');
                } else {
                    //habilitar campo
                    $('#form' + id).attr('disabled', false);
                    $(".form" + id).html('(*)');
                }


            } //for

        } //if

        mostrar = lista;
        opcion = '3';

    } //if

    if (opcion == '3' && mostrar.length > 0) {

        var limpiarCampo = true;


        //deshabilitarCampos y limpiar los requeridos
        for (var j = 0; j < listaPrincipal.length; j++) {
            limpiarCampo = true;
            var id = listaPrincipal[j];

            for (var k = 0; k < mostrar.length; k++) {

                var idLista = mostrar[k];

                if (id == idLista) {
                    limpiarCampo = false;

                }
            } //for

            //limpiar solo los campos no habilitados
            if (limpiarCampo) {



                //deshabilitar campo
                $('#form' + id).attr('disabled', true);
                //limpiar span requerido
                $(".form" + id).html('');

                var name = $("#form" + id).get(0).tagName;

                if (name == 'INPUT') {
                     /* if(id == 'Correo' && $("#formTipoPersona").val()== 14){
                            $("#form"+id).css('border-color','#d2d6de');
                            $("#input"+id).removeClass('inputError');
                        }else{
                            if(inputReq.VerificarCorreo(2,'')){ 
                                 // $("#form"+id).css('border-color','#d2d6de');
                            } else {
                                $("#form"+id).css('border-color','red'); 
                                $("#input"+id).addClass('inputError');
                                ok++
                                }//else
                            }//if*/

                    //input vacio
                    $("#form" + id).val('');
                    //bordes de input en default
                    $("#form" + id).css('border-color', '#d2d6de');
                    $("#input" + id).removeClass('inputError');
                }

                if (name == 'SELECT') {
                        if(id == 'Banco' && $("#formTipoPersona").val()== 14){
                                    $("#input"+id).removeClass('inputError');    
                                    $("#input"+id).find('.select2-container--default .select2-selection--single').css('border','1px solid #aaa');  
                                }else{    
                                    $("#input"+id).find('.select2-container--default .select2-selection--single').css('border-color','red'); 
                                    $("#input"+id).addClass('inputError');
                                    ok++;
                                }    
                    //select en default
                    $("#form" + id).val('').trigger('change.select2');
                    //estilo de borde en gris
                    $("#input" + id).find('.select2-container--default .select2-selection--single').css('border', '1px solid #aaa');
                    $("#input" + id).removeClass('inputError');
                }
                if (name == 'A') {
                    $("#form" + id).removeClass('borderErrorBtn');
                    $('#form' + id).attr('data-toggle', '#');
                    $("#input" + id).removeClass('inputError');
                }


            } //if

        } //for

    } //if

    //validar los requeridos
    if (opcion == '2') {

        var ok = 0;

        var req = inputReq.getRequired();

        //El correo se valida si ya existe aunque no sea requerido
        // if($("#formCorreo").val().trim() != ''){
        //      if(!inputReq.VerificarCorreo(2,'')){ 
        //        req.push('Correo');
        //        }
        //    }

        for (var j = 0; j < req.length; j++) {

            var id = req[j];

            // console.log(id);
            var name = $("#form" + id).get(0).tagName;

            //CAMPOS DE CORREO
            if (name == 'TAGS') {


                if (id == 'CorreoAdministrativo') {
                    if (correo_administrativo.length == 0) {
                        $("#form" + id).css('border-color', 'red');
                        $("#input" + id).addClass('inputError');
                    } else {
                        $("#form" + id).css('border-color', '#d2d6de');
                        $("#input" + id).removeClass('inputError');
                    }
                    //VALIDAR SGTE 
                    continue;
                }

                if (id == 'CorreoComerciales') {

                    if (correo_comercial.length == 0) {
                        $("#form" + id).css('border-color', 'red');
                        $("#input" + id).addClass('inputError');
                    } else {
                        $("#form" + id).css('border-color', '#d2d6de');
                        $("#input" + id).removeClass('inputError');
                    }
                    //VALIDAR SGTE      
                    continue;
                }

            }

            if (name == 'INPUT' || name == 'TEXTAREA') {

                var input = $("#form" + id).val();
                if (input == '') {
                    $("#form" + id).css('border-color', 'red');
                    $("#input" + id).addClass('inputError');
                    ok++;
                } else {
                    $("#form" + id).css('border-color', '#d2d6de');
                    $("#input" + id).removeClass('inputError');
                } //else

            } //if

            if (name == 'SELECT') {

                var select = $("#form" + id).val();

                if (select == '') {
                    $("#input" + id).find('.select2-container--default .select2-selection--single').css('border-color', 'red');
                    $("#input" + id).addClass('inputError');
                    ok++;
                } else {
                    $("#input" + id).removeClass('inputError');
                    $("#input" + id).find('.select2-container--default .select2-selection--single').css('border', '1px solid #aaa');
                } //else


            } //if 

            if (name == 'A') {

                var inputHidden = $("#imagen").val();

                if (inputHidden == '') {
                    $("#input" + id).addClass('inputError');
                    $('#form' + id).addClass('borderErrorBtn');
                    ok++;
                } else {
                    $('#form' + id).removeClass('borderErrorBtn');
                    $("#input" + id).removeClass('inputError');
                } //else


            } //if  

        } //for


            if(ok > 0){
                $('#formDv').prop('disabled',true);
                return false;
            } else {
                $('#formDv').prop('disabled',false);
                return true;
            }

    } //if



} //function

$('#formMigrar').change(function(){
        id_pais = $("#formPais option:selected").val();
        tipo_persona = $("#formTipoPersona option:selected").val();
        console.log('INDF '+id_pais);
        if( id_pais == 1455){
            $("#formMigrar").val('false').trigger('change.select2');
        }    
    });




$('#formPais').change(function(){
        id_pais = $("#formPais option:selected").val();
        tipo_persona = $("#formTipoPersona option:selected").val();
        console.log('INDF '+id_pais);
        if( id_pais == 1455){
            $("#formMigrar").val('false').trigger('change.select2');
        }

        obtenerCiudad();
  });



//RELACION PAIS CIUDAD
//PENDIENTE:  RESOLVER EL BORRADO DE SELECT2 AL CAMBIAR DE PAIS
function obtenerCiudad() {



    var id = $("#formPais").val();
    var idPais = {
        'idPais': id
    };
    if (id != '') {
        $.ajax({
            type: "GET",
            url: "{{route('obtenerCiudad')}}",
            dataType: 'json',
            data: idPais,
            tryCount: 0,
            retryLimit: 3,
            error: function (xhr, textStatus, errorThrown) {
                //REINTENTOS DE CONEXION
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                    //try again
                    $.ajax(this);
                    return;
                }
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error al obtener la ciudad recargue la pagina por favor.',
                    position: 'top-right',
                    icon: 'info'
                });
                $('#btnGuardar').prop('disabled', true);

                return;

            },
            success: function (rsp) {
                if (rsp.ciudades.length != 0) {
                    $('#formCiudad').empty();
                    $('#formCiudad').select2({
                        placeholder: "Seleccione Ciudad",
                        disabled: false
                    });
                    $.each(rsp.ciudades, function (key, item) {
                        var newOption = new Option(item.denominacion, item.id, false, false);
                        $('#formCiudad').append(newOption);
                    })

                } else {

                    $('#formCiudad').empty();
                    var newOption = new Option('Seleccione Ciudad', '', false, false);
                    $('#formCiudad').append(newOption);
                    $('#formCiudad').select2({
                        disabled: true
                    });
                } //else

            } //funcion  
        });


    } //if

} //function



//=====================IMAGEN FORM ========================//
var options = {
    beforeSubmit: showRequest,
    success: showResponse,
    dataType: 'json'
};


$('.cargandoImg').hide();

$('body').delegate('#image', 'change', function () {

    //OBTENER TAMAÑO DE LA IMAGEN
    var input = document.getElementById('image');
    var file = input.files[0];
    var tamaño = file.size;

    if (tamaño > 0) {

        var tamaño = file.size / 1000;
        if (tamaño > 3000) {

            $("#image").val('');
            $("#validation-errors").empty();
            $("#validation-errors").append('<div class="alert alert-error"><strong>El tamaño de la imagen supera el limite de 3MB</strong><div>');
        } else {

            $('.cargandoImg').show();
            $('#upload').ajaxForm(options).submit();
        }

    } else {
        $("#image").val('');
        $("#validation-errors").empty();
        $("#validation-errors").append('<div class="alert alert-error"><strong>Existe un error con el archivo</strong><div>');
    }



});


function showRequest(formData, jqForm, options) {

    $("#validation-errors").hide().empty();
    return true;
}


function showResponse(response, statusText, xhr, $form) {
    $('.cargandoImg').hide();
    if (response.success == false) {
        $("#image").val('');
        $("#validation-errors").append('<div class="alert alert-error"><strong>' + response.errors + '</strong><div>');
        $("#validation-errors").show();

    } else {
        $("#imagen").val(response.archivo);

        var file_name    = response.archivo;
        $('#output').html('');

        divImagen = `
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <img class="card-img-top" style="max-width:150px;" src="{{asset('personasLogo')}}/${file_name}" alt="Adjunto Iamgen Perfil">
                                                        <p><a href="{{asset('personasLogo')}}/${file_name}" class="btn btn-info" target="_black"><b>VER</b></a></p>
										                <a href="#" class="card-title" style="font-size: 0.7rem; color: red;" onclick="eliminarImagen('${file_name}')"><b>Eliminar</b></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    `;


        $("#output").append(divImagen);

        $("#image").prop('disabled', true);
    }
}

function eliminarImagen(archivo) {

    $('.cargandoImg').show();
    var idPersona = $('#id_persona').val();
    if (archivo == 'factour.png' || archivo == 'blanco.png') {

        document.getElementById("image").value = "";
        $("#output").html('');
        $("#image").prop('disabled', false);
        $('.cargandoImg').hide();

    } else {

        $.ajax({
            type: "GET",
            url: "{{route('fileDeletePersona')}}", //fileDelDTPlus
            dataType: 'json',
            data: {
                dataFile: archivo,
                idPersona: idPersona
            },

            error: function (jqXHR, textStatus, errorThrown) {
                $.toast({
                    heading: 'Error',
                    text: 'Ocurrio un error al intentar eliminar la imagen.',
                    position: 'top-right',
                    showHideTransition: 'fade',
                    icon: 'error'
                });
                $('.cargandoImg').hide();

            },
            success: function (rsp) {
                $('.cargandoImg').hide();

                if (rsp.rsp == true) {
                    document.getElementById("image").value = "";
                    $("#output").html('');

                } else {
                    $.toast({
                        heading: 'Error',
                        text: 'Ocurrio un error al intentar eliminar la imagen.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                    });
                    document.getElementById("image").value = "";
                    $("#output").html('');

                }

                $("#image").prop('disabled', false);
            }


        });

    }

} //function 





// =======================CALENDARIO============================
$('#formFechaNacimiento').datepicker({
    timePicker: true,
    minDate: 0,
    format: 'dd/mm/yyyy',
    language: "es",
    orientation: "bottom"
});



$('#formNombres').change(function(){
        $('#formNombres').css('border-color','#d2d6de');
        if($('#formTipoPersona').val() == 15){
            $.ajax({
                type: "GET",
                url: "{{route('datosPrestadores')}}",//fileDelDTPlus
                dataType: 'json',
                data: {  
                        dataFile:$('#formNombres').val()
                    },
                success: function(rsp){
                        if(rsp == false){
                            $.toast({
                                    heading: 'Error',
                                    position: 'top-right',
                                    text: 'El nombre de este prestador ya existe.',
                                    showHideTransition: 'fade',
                                    icon: 'error'
                                });
                            $('#formNombres').css('border-color','#E7491F');
                            $('#formNombres').val('');
                            $('#formNombres').focus();
                        }               
                }
            })
        }    
});




// =============== OBTENER LA SUCURSAL SI ES UNA AGENCIA/EMPRESA =================
$('#formAgencia').change(function () {
    obtenerSucursal();
});

function obtenerSucursal() {

    var id = $("#formAgencia option:selected").val();
    var option = inputReq.getTipoSucursal();
   // alert(id);
    var idAgencia = {
        'idAgencia': id,
        'tipoSucursal': option,
        'idEmpresa':$('#formAgencia').val()
    };
    if (id != '' & option != '') {
        $('#formSucursalEmpresa').empty();
        $.ajax({
            type: "GET",
            url: "{{route('obtenerSucursalAgencia')}}",
            dataType: 'json',
            data: idAgencia,

            error: function (jqXHR, textStatus, errorThrown) {

                $.toast({
                    heading: 'Error',
                    position: 'top-right',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    showHideTransition: 'fade',
                    icon: 'error'
                });

            },
            success: function (rsp) {
               if (rsp.sucursalAgencia.length != 0) {
                  $('#formSucursalEmpresa').empty(); 
                  $.each(rsp.sucursalAgencia, function (key, item) {
                        var datosSucursal = item.nombre;
                        if(jQuery.isEmptyObject(item.denominacion_comercial) == false){
                            datosSucursal = datosSucursal+' - '+item.denominacion_comercial;
                        }
                        var newOption = new Option(datosSucursal, item.id, false, false);
                        $('#formSucursalEmpresa').append(newOption);
                    }); //each
                    var select = '{{$persona->id_sucursal_empresa}}';

                   // alert(select);

                    if (select != '' && select != null) {
                        $("#formSucursalEmpresa").val(validarData(select)).trigger('change.select2');
                    } else {
                        $("#formSucursalEmpresa").val('').trigger('change.select2');
                    }
                    $('#formSucursalEmpresa').select2({
                        disabled: false
                    });

                    //$("#formSucursalEmpresa").val('').trigger('change.select2');

                } else {
                    //alert('EntroTJrohgod');
                    $('#formSucursalEmpresa').empty();
                    var newOption = new Option('Seleccione Sucursal', '', false, false);
                    $('#formSucursalEmpresa').append(newOption);
                    $('#formSucursalEmpresa').select2({
                        disabled: true
                    });
                } //else

            } //funcion  
        });


    } //if
}



function obtenerEmpresaAgencia() {
    if (inputReq.getCateg()) {
        id = inputReq.getTipoEmpresa();
        $.ajax({
            type: "GET",
            url: "{{route('obtenerEmpresaAgencia')}}",
            dataType: 'json',
            data: {
                id: id
            },

            error: function (jqXHR, textStatus, errorThrown) {

                $.toast({
                    heading: 'Error',
                    position: 'top-right',
                    text: 'Ocurrio un error en la comunicación con el servidor.',
                    showHideTransition: 'fade',
                    icon: 'error'
                });

            },
            success: function (rsp) {

                if (rsp.response.length > 0) {
                    // formAgencia
                    $('#formAgencia').empty();
                    $('#formAgencia').select2({
                                                disabled: false
                                                });

                   /* var newOption = new Option("Seleccione una opción", "", false, false);
                    $('#formAgencia').append(newOption);*/

                    $.each(rsp.response, function (key, item) {
                        var newOption = new Option(item.nombre, item.id, false, false);
                        $('#formAgencia').append(newOption);
                    });

                    var select = '{{$persona->id_persona}}';
                                        
                    if (select != '' && select != null) {
                       // alert('select');
                        $("#formAgencia").val(validarData(select)).trigger('change.select2');
                    } else {
                        $("#formAgencia").val('').trigger('change.select2');
                    }
                    $("#formAgencia").trigger('change');

                } else {
                    $('#formAgencia').empty();
                    var newOption = new Option("Seleccione una opción", "", false, false);
                    $('#formAgencia').append(newOption);
                } //else
               // $('#formAgencia').val("");

            } //funcion  

        }).done(function () {

          //  obtenerSucursal();
        });


    } //if


}


// ===============AJAX PARA GUARDAR FORMULARIO=================
$('#btnGuardar').click(function (event) {

    $.toast().reset('all');
    event.preventDefault();
    $('#btnGuardar').prop('disabled', true);


    let correo_admin;
    let correo_comer;
    //ALMACENAR CORREOS
    $.each(correo_administrativo, function (index, val) {
        if (index == 0) {
            correo_admin = val;
        } else {
            correo_admin += ',' + val;
        }
    });
    $('#input_hidden_correo_administrativo').val(correo_admin);

    $.each(correo_comercial, function (index, val) {
        if (index == 0) {
            correo_comer = val;
        } else {
            correo_comer += ',' + val;
        }
    });
    $('#input_hidden_correo_comercial').val(correo_comer);

   // alert(accionFormulario('', '', '2'));

    if (accionFormulario('', '', '2')) {
          if($('#validacionCorreo').val() == 0){   
                $('#formDv').prop('disabled', false);
                $('#acuerdo_id').val($('#formAcuerdoEspecial ~ iframe').contents().find('.wysihtml5-editor').html());
                var dataString = $('#frmPersona').serializeJSON();

                $.ajax({
                    type: "GET",
                    url: "{{route('actualizarPersona')}}",
                    dataType: 'json',
                    data: dataString,
                    error: function (jqXHR, textStatus, errorThrown) {

                        $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            hideAfter: false,
                            icon: 'error'
                        });
                    },
                    success: function (rsp) {
                        $('#btnGuardar').prop('disabled', false);

                        if (rsp.dato === 'true') {
                            $.toast({
                                heading: 'Success',
                                text: 'Los datos fueron almacenados.',
                                position: 'top-right',
                                hideAfter: false,
                                showHideTransition: 'slide',
                                icon: 'success'
                            });

                            $('#frmPersona').trigger("reset");
                            window.location.href = "{{ route('personaIndex') }}";


                        } else {
                            $.toast({
                                heading: 'ATENCIÓN',
                                position: 'top-right',
                                hideAfter: false,
                                loaderBg: '#9EC600',
                                text: rsp.dato,
                                showHideTransition: 'fade',
                                icon: 'warning'
                            });



                        } //else
                    } //funcion  
                });


            }else{
                 marcarTab(); 
                 $.toast({
                            heading: 'Error',
                            text: 'Ya existe el correo.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                            });;
                $('#formDv').prop('disabled', true);
                $('#tabPersona').css('color','#E7491F');
                $('#formCorreo').css('border-color','#E7491F');
                $('#btnGuardar').prop('disabled',false);
          }    

    } else {
        $.toast({
            heading: 'Error',
            text: 'Complete los campos requeridos (*).',
            position: 'top-right',
            showHideTransition: 'fade',
            icon: 'error'
        });
        $('#btnGuardar').prop('disabled', false);

        //VALIDAR TABS
        marcarTab();

    }

    $('#btnGuardar').prop('disabled', false);


});




// ===============VALIDAR CORREO ==============+
    $('#formCorreo').keyup(function(e){

            let contenido = e.target.value;
            e.target.value = contenido.replace(" ", "");

            validarCorreo();

           });//function


function validarCorreo() 
{

    var correoGuardado = '{{$persona->email}}';

    var dataString = {
        'e': $('#formCorreo').val().trim(),
        'idTipoPersona': $('#formTipoPersona').val()
    };
    $('.mensaje_err').html('');
    $('.mensaje_err').css('color', 'green');


    if ($('#formCorreo').val().trim() != '') {

        if ($('#formCorreo').val().trim() != correoGuardado) {

            var patron = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

            var correo = $('#formCorreo').val().trim();
            var esCoincidente = patron.test(correo);

            if (esCoincidente) {



                $.ajax({
                    type: "GET",
                    url: "{{route('validarCorreo')}}",
                    dataType: 'json',
                    data: dataString,

                    error: function (jqXHR, textStatus, errorThrown) {
                        $('.mensaje_err').html(' El correo ya existe ');
                        $('.mensaje_err').css('color', 'red');
                        inputReq.VerificarCorreo(1, false);

                    },
                    success: function (rsp) {
                            if(rsp.resp != '0'){
                                /*$('.mensaje_err').html(' El correo ya existe ');
                                $('.mensaje_err').css('color','red');*/
                                inputReq.VerificarCorreo(1,false);
                                $('#validacionCorreo').val(1);
                            } else {
                                /*  $('.mensaje_err').html(' Correo Válido ');
                                  $('.mensaje_err').css('color','green');*/
                                  $('#validacionCorreo').val(0);
                            }

                    }


                });

            } else {
                $('.mensaje_err').html(' Formato no valido. ');
                $('.mensaje_err').css('color', 'red');
                inputReq.VerificarCorreo(1, false);
                $('#validacionCorreo').val(1);
            } //else


        } else {
            inputReq.VerificarCorreo(1, true);
        } //else   
    } //if




    // ===============GENERAR HASH CORREO ==============+
    pasado = "{{$persona->prefijo_nemo}}";
    dataStg = '';
    dataStg = {
            'online': $('#formCorreo').val(),
            'indice': "{{$persona->prefijo_nemo}}"
        }

    if(pasado == ''){
        $('#online').val('{{md5($persona->email)}}');
    }else{
        $.ajax({
            type: "GET",
            url: "{{route('generarHash')}}",
            dataType: 'json',
            data: dataStg,

            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error comunicación con el servidor para generar hash');
                $('#online').val('');
            },
            success: function (rsp) {
                $('#online').val(rsp.resp);

            }


        });
    }
    $.ajax({
            type: "GET",
            url: "{{route('generarHash')}}",
            dataType: 'json',
            data: dataStg,

            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error comunicación con el servidor para generar hash');
                $('#kontrol').val('');
            },
            success: function (rsp) {
                $('#kontrol').val(rsp.resp+'.');

            }


        });
} //function



//CREA CIUDAD
$('#crearC').on('click', function () 
{


    $('.cargandoImg').show();
    var ciudad = $('#ciudad_nuevo').val().trim();
    var paisRelacion = $('#pais_select_relacion').val().trim();
    var ok = 0;
    var opcion = 0;




    //CIUDAD NUEVO CON RELACION DE PAIS
    if (ciudad == '' || paisRelacion == '') ok++

    opcion = 1;

    dataString = {
        ciudad: ciudad,
        paisRelacion: paisRelacion,
        opcion: opcion,
        operacion: 1
    }

    if (ok == 0) {

        $.ajax({
            type: "GET",
            url: "{{route('crearCiudadPais')}}",
            dataType: 'json',
            data: dataString,

            error: function (jqXHR, textStatus, errorThrown) {
                $('#mensaje_cambio_c').css('color', 'red');
                $('#mensaje_cambio_c').html('Ocurrio un error en la comuniación con el servidor.');
                $('.cargandoImg').hide();

            },
            success: function (rsp) {
                $('.cargandoImg').hide();

                if (rsp.resp == true) {

                    $('#mensaje_cambio_c').css('color', 'green');
                    $('#mensaje_cambio_c').html('Los datos fueron creados con éxito.');
                    $('.cargandoImg').hide();

                    $('#ciudad_nuevo').val('');
                    $("#pais_select_relacion").val('').trigger('change.select2');


                } else {

                    //validar nombre y retornar mensaje de error   
                    $('#mensaje_cambio_c').css('color', 'red');
                    $('#mensaje_cambio_c').html(rsp.m);
                    $('.cargandoImg').hide();

                }

            }


        }).done(function () {
            $(".pais_select").val(paisRelacion).trigger('change.select2');
            obtenerCiudad();

        }); //function done;


    } else {
        $('#mensaje_cambio_c').css('color', 'red');
        $('#mensaje_cambio_c').html('Complete los campos por favor');
        $('.cargandoImg').hide();

    }


});




//CREA PAISES
$('#crearP').on('click', function () 
{


    $('.cargandoImg').show();
    var pais = $('#pais_nuevo').val().trim();
    var cod_pais = $('#cod_pais_nuevo').val().trim();
    var ok = 0;
    var opcion = 0;
    var paisNuevo = '';



    //PAIS
    if (pais == '') ok++
    if (cod_pais == '') ok++

    opcion = 2;


    dataString = {
        pais: pais,
        codPais: cod_pais,
        opcion: opcion,
        operacion: 1
    }

    if (ok == 0) {

        $.ajax({
            type: "GET",
            url: "{{route('crearCiudadPais')}}",
            dataType: 'json',
            async: false,
            data: dataString,

            error: function (jqXHR, textStatus, errorThrown) {
                $('#mensaje_cambio_p').css('color', 'red');
                $('#mensaje_cambio_p').html('Ocurrio un error en la comuniación con el servidor.');
                $('.cargandoImg').hide();

            },
            success: function (rsp) {
                $('.cargandoImg').hide();

                if (rsp.resp == true) {

                    nuevoPais = rsp.id;
                    $('#mensaje_cambio_p').css('color', 'green');
                    $('#mensaje_cambio_p').html('Los datos fueron creados con éxito.');
                    $('.cargandoImg').hide();

                    $('#pais_nuevo').val('');
                    $('#ciudad_nuevo').val('');
                    $("#pais_select_relacion").val(paisNuevo).trigger('change.select2');
                    $('#cod_pais_nuevo').val('');

                } else {


                    $('#mensaje_cambio_p').css('color', 'red');
                    $('#mensaje_cambio_p').html(rsp.m);
                    $('.cargandoImg').hide();

                }

            }


        }).done(function () {

            //RECARGA EL COMBO DE PAISES
            dataString = {
                operacion: 2
            }


            $.ajax({
                type: "GET",
                url: "{{route('crearCiudadPais')}}",
                dataType: 'json',
                data: dataString,

                success: function (rsp) {


                    $('.pais_select').empty();

                    var newOption = new Option("Seleccione Pais", '', false, false);
                    $('.pais_select').append(newOption);

                    $.each(rsp.p, function (key, item) {
                        var newOption = new Option(item.name_es, item.cod_pais, false, false);
                        $('.pais_select').append(newOption);
                    });

                    $(".pais_select").val(nuevoPais).trigger('change.select2');


                } //success


            }); //function ajax







        }); //function done





    } else {
        $('#mensaje_cambio_p').css('color', 'red');
        $('#mensaje_cambio_p').html('Complete los campos por favor');
        $('.cargandoImg').hide();

    }


});


function acuerdoView()
{

    $('#inputAcuerdoEspecial').show();

    $.ajax({
                type: "GET",
                url: "{{route('getAjaxAcuerdo')}}",
                dataType: 'json',
                data: {id_persona:$('#id_persona').val()},

                success: function (rsp) {
                    $('#formAcuerdoEspecial ~ iframe').contents().find('.wysihtml5-editor').html(rsp.data.acuerdos_especiales);
                } //success


            }); //function ajax 
}

/**
 * LOGICA PERSONA EMPRESA
*/
$('#formEmpresaPersona').change(()=>{
    getSucursalEmpresaPersona();
});

 const data_control_emp = {
     arr : [],
    set_data : function(d){ this.arr.push(d)},
    get_data : function(){ return this.arr;}
}

function getSucursalEmpresaPersona()
{
    let empresa_id = $('#formEmpresaPersona').val();
    $("#formSucursaEmpresaPersona").prop('disabled',true); 
    $('#formSucursaEmpresaPersona').empty();

    $.ajax({
                type: "GET",
                url: "{{route('getSucursalEmpresaPersona')}}",
                dataType: 'json',
                data: {empresa_id:empresa_id},
                error: function (jqXHR, textStatus, errorThrown) 
                {
                    $.toast({
                        heading: 'ATENCIÓN',
                        position: 'top-right',
                        hideAfter: false,
                        loaderBg: '#9EC600',
                        text: 'Ocurrio un error al intentar recuperar los datos de sucursal..',
                        showHideTransition: 'fade',
                        icon: 'warning'
                    });
                },
                    success: function (rsp) {
                        if(rsp.data.length > 0){
                           
                            $('#formSucursaEmpresaPersona').empty();
                            $.each(rsp.data, function (key, item) {
                                var newOption = new Option(item.nombre, item.id, false, false);
                                $('#formSucursaEmpresaPersona').append(newOption);
                            });
                            $("#formSucursaEmpresaPersona").prop('disabled',false);
                            $('#formAgencia').trigger('change');
                        } else {
                            $("#formSucursaEmpresaPersona").prop('disabled',true); 
                        }
                        
                }


            }); 

}

function addEmpresaPersona()
{
    let empresa_text = $('#formEmpresaPersona option:selected').text();
    let empresa_id = $('#formEmpresaPersona').val();
    let sucursal_txt = $('#formSucursaEmpresaPersona option:selected').text();
    let sucursal_id = $('#formSucursaEmpresaPersona').val();
    let ok = 0;

    	//CLEAR MSJ REPEAT
        if(msj_emp.reset){
			msj_emp.reset();	
		}

    //SI AMBOS EXISTEN ENTONCES ES DUPLICADO
    $.each(data_control_emp.get_data(),(index,value)=>{
        if(value.empresa_id == empresa_id){ ok++;}
        if(value.sucursal_id == sucursal_id){ ok++;}

        if(ok == 2)
        {
            return false;
        } else {
            ok = 0;
        }
    });

    if(empresa_id == '' | sucursal_id == ''){
        msj_emp = $.toast({
                        heading: 'ATENCIÓN',
                        position: 'top-right',
                        hideAfter: false,
                        loaderBg: '#9EC600',
                        text: 'Empresa y Sucursal es obligatorio.',
                        showHideTransition: 'fade',
                        icon: 'warning'
            });
     return '';    
    }

    if(ok == 2 ){
        msj_emp = $.toast({
                        heading: 'ATENCIÓN',
                        position: 'top-right',
                        hideAfter: false,
                        loaderBg: '#9EC600',
                        text: 'Los datos que intenta ingresar son duplicados.',
                        showHideTransition: 'fade',
                        icon: 'warning'
                    });
        return '';            
    }
    //INSERT ARR CONTROL
    data_control_emp.set_data({empresa_id:empresa_id,sucursal_id:sucursal_id});

    //INSERT TABLE
    let cantItem = $('#listadoEmpresaPersona tr.tabla_filas').length;
			let tabla = `
				<tr id="rowEmp_${cantItem}" class="tabla_filas">
                        <td>
                            <b>${empresa_text}</b>
                            <input type="hidden" value="${empresa_id}"  name="data_empresa[][empresa_id]"></td>
                            <td>
                                <b>${sucursal_txt}</b>
                                <input type="hidden" readonly value="${sucursal_id}" name="data_empresa[][sucursal_id]"></td>
                                        <td>
                                            <button type="button" onclick="deleteTableEmpresaPersona(${cantItem})"  
                                            class="btn btn-danger"><b>Eliminar</b>
                                            </button>
										</td>
									</tr>
                                    `;
                                    
                                    if($('.tbody_empre_per tr td').hasClass('relleno_table')){
										$('.tbody_empre_per').html('');
									}
				
                                $('#listadoEmpresaPersona tbody').append(tabla);

}

function orderTableEmpresaPersona()
{
    //ORDENA LA TABLA AL ELIMINAR O INSERTAR
    let cont = 0;
						$.each($('.tabla_filas'), function (index, value) {
								let num_cant = value.id.split('rowEmp_');
								if (num_cant[1] != cont) {
										$('#' + value.id).attr('id', 'rowEmp_' + cont);
										//MODIFICAR ONCLICK
										$(value.children[2]).find('button').attr('onclick', 'deleteTableEmpresaPersona(' + cont + ')');
								}
								cont++;

						});
}

function deleteTableEmpresaPersona(class_id)
{
    $('#rowEmp_' + class_id).remove();
    orderTableEmpresaPersona();
}


$("#formTipoPersona").change(function () {
        var tipoPersona = $(this).val();
    //////////////////////////////////////////////////////////////////////////////////
        id_pais = $("#formPais option:selected").val();
        tipo_persona = $("#formTipoPersona option:selected").val();
        console.log('INDF '+id_pais);
        if( id_pais == 1455){
            $("#formMigrar").val('false').trigger('change.select2');
        }
    ///////////////////////////////////////////////////////////////////////////////////

    campoTipoPersona($(this).val());
});




function campoTipoPersona(id) {

    validarCorreo();

    $('#cambioTel').html('Teléfono ');
    $('.cambioAmadeus-n').html('Codigo Amadeus');
    $('#nombrePersona').html('Nombre o Razon Social');

    switch (id) {

        //ADMIN GESTUR
        case '1':
            inputReq.setTipoEmpresa('2');
            inputReq.setTipoSucursal('21');
            obtenerEmpresaAgencia();
            accionFormulario(['Celular',
                    'NotificacionCorreo',
                    'Image',
                    'Migrar',
                    'Pais', 'Ciudad',
                    'NotificacionCorreo',
                    'TokenNemo',
                    'Migrar', 
                    "TipoProveedor",
                    "CodigoCangoro"
                ],
                ['Nombres',
                    'Apellido',
                    'FechaNacimiento',
                    'TipoFacturacion',
                    'ActivoDtpMundo',
                    'ActivoPersona',
                    'Direccion',
                    'Telefono',
                    'Correo',
                    'Password',
                    'Agencia',
                    'SucursalEmpresa',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    "RetenerIva"
                ], '1');

            break;

            //EMPRESA
        case '20':
            inputReq.setTipoSucursal('');
            accionFormulario(['DenominacionComercial',
                    'Direccion',
                    'Image',
                    'Celular',
                    'CorreoAdministrativo',
                    'CorreoComerciales',
                    'Telefono',
                    'Correo',
                    'TokenNemo',
                    'Migrar', 
                    "TipoProveedor"
                ],
                ['Nombres',
                    'TipoFacturacion',
                    'FechaNacimiento',
                    'ActivoDtpMundo',
                    'Pais',
                    'Ciudad',
                    'ActivoPersona',
                    'Direccion',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    "RetenerIva"
                ], '1');
            break;

            //ADMIN EMPRESA
        case '2':
            inputReq.setTipoEmpresa('2');
            inputReq.setTipoSucursal('21');
            obtenerEmpresaAgencia();

           
            acuerdoView();

            accionFormulario(['Celular',
                    'NotificacionCorreo',
                    'Image',
                    'Migrar',
                    'TokenNemo',
                    'Migrar', 
                    "TipoProveedor",
                    "CodigoCangoro"
                ],
                ['Nombres',
                    'Apellido',
                    'FechaNacimiento',
                    'Pais',
                    'Ciudad',
                    'Direccion',
                    'TipoFacturacion',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'Telefono',
                    'Correo',
                    'SucursalEmpresa',
                    'Password',
                    'Agencia',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    "RetenerIva"
                ], '1');
            break;

            //SUCURSAL EMPRESA
        case '21':
            inputReq.setTipoEmpresa('2');
            inputReq.setTipoSucursal('');
            obtenerEmpresaAgencia();
            accionFormulario(['DenominacionComercial',
                    'Correo',
                    'Image',
                    'Celular',
                    'TokenNemo',
                    'Migrar', 
                    "TipoProveedor"
                ],
                ['Nombres',
                    'FechaNacimiento',
                    'Pais',
                    'Ciudad',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'Direccion',
                    'TipoFacturacion',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'Telefono',
                    'Agencia',
                    'SucursalContable',
                    "RetenerIva"
                ], '1');

            $('#nombrePersona').html('Nombre Sucursal');


            break;


            //VENDEDOR EMPRESA 
        case '3':
            inputReq.setTipoEmpresa('2');
            //sucursales del tipo empresa
            inputReq.setTipoSucursal('21');
            obtenerEmpresaAgencia();
            getSucursalEmpresaPersona();
         //   obtenerSucursal();

            acuerdoView();

            accionFormulario(['Telefono',
                    'Pais',
                    'Ciudad',
                    'Direccion',
                    'SucursalEmpresa',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'FechaNacimiento',
                    'Image',
                    'Migrar',
                    'CodigoAmadeus',
                    'Celular',
                    'TokenNemo', 
                    "TipoProveedor",
                    "CodigoCangoro"
                ],
                ['Nombres',
                    'Apellido',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'Agencia',
                    'TipoFacturacion',
                    'Password',
                    'Correo',
                    'PuedeReservar',
                    'PuedeReservarEnGastos',
                    "RetenerIva"              
                    ], '1');



            break;

            //MARKETING_EMPRESA
        case '7':
            inputReq.setTipoEmpresa('2');
            //sucursales del tipo empresa
            inputReq.setTipoSucursal('21');
            obtenerEmpresaAgencia();
            accionFormulario(['Telefono',
                    'Pais',
                    'Ciudad',
                    'Direccion',
                    'SucursalEmpresa',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'FechaNacimiento',
                    'Image',
                    'Migrar',
                    'CodigoAmadeus',
                    'Celular',
                    'TokenNemo', 
                    "TipoProveedor"
                ],
                ['Nombres',
                    'Apellido',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'Agencia',
                    'TipoFacturacion',
                    'Password',
                    'Correo',
                    "RetenerIva"
                ], '1');



            break;

            //ADMINISTRATIVO EMPRESA
        case '5':
            inputReq.setTipoEmpresa('2');
            inputReq.setTipoSucursal('21');
            obtenerEmpresaAgencia();

            accionFormulario(['Telefono',
                    'Pais',
                    'Ciudad',
                    'Direccion',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'Image',
                    'Migrar',
                    'FechaNacimiento',
                    'Celular',
                    'TokenNemo', 
                    "TipoProveedor"
                ],
                ['Nombres',
                    'Apellido',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'SucursalCarteraId',
                    'TipoFacturacion',
                    'Password',
                    'Correo',
                    "RetenerIva"
                ], '1');



            break;

            //OPERATIVO EMPRESA
        case '6':
            inputReq.setTipoEmpresa('2');
            inputReq.setTipoSucursal('21');
            obtenerEmpresaAgencia();

            
            acuerdoView();

            accionFormulario(['Telefono',
                    'Pais',
                    'Ciudad',
                    'Direccion',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'FechaNacimiento',
                    'Image',
                    'Migrar',
                    'Celular',
                    'CodigoAmadeus',
                    'TokenNemo', 
                    "TipoProveedor"
                ],
                ['Nombres',
                    'Apellido',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'Agencia',
                    'SucursalEmpresa',
                    'TipoFacturacion',
                    'Password',
                    'Correo',
                    "RetenerIva"
                ], '1');

            break;

            //SUPERVISOR EMPRESA
        case '4':
            inputReq.setTipoEmpresa('2');
            inputReq.setTipoSucursal('21');
            obtenerEmpresaAgencia();

            accionFormulario(['Apellido',
                    'Telefono',
                    'Pais',
                    'Ciudad',
                    'Direccion',
                    'Image',
                    'Migrar',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'FechaNacimiento',
                    'Celular',
                    'TokenNemo', 
                    "TipoProveedor",
                    "CodigoCangoro"
                ],
                ['Nombres',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'SucursalEmpresa',
                    'TipoFacturacion',
                    'Agencia',
                    'Password',
                    'Correo',
                    "RetenerIva"
                ], '1');

            break;
            //AGENCIA
        case '8':
            inputReq.setTipoSucursal('');
            acuerdoView();
            
            accionFormulario(['Celular',
                    'IvaComisiones',
                    'NotificacionCorreo',
                    'Agente',
                    'Image',
                    'PlazoCobro',
                    'NotificacionCorreo',
                    'Correo',
                    'TokenNemo',
                             'Migrar', 
                    "TipoProveedor"
                ],
                ['Nombres',
                    'DenominacionComercial',
                    'SucursalCarteraId',
                    'Agente',
                    'ActivoPersona',
                    'PuedeReservar',
                    'PuedeReservarEnGastos',
                    'FacturacionAutomaticaEnGastos',
                    'TipoFacturacion',
                    'TipoPersoneria',
                    'ActivoDtpMundo',
                    'CorreoAdministrativo',
                    'CorreoComerciales',
                    'Pais',
                    'Ciudad',
                    'Direccion',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'Telefono',
                    "RetenerIva"
                ], '1');

            $('#cambio_n').html('Markup DTP MUNDO');
            break;
            //ADMIN AGENCIA

        case '22':
            //obtener agencias
            inputReq.setTipoEmpresa('1');
            inputReq.setTipoSucursal('9');
            obtenerEmpresaAgencia();
            accionFormulario(['Pais',
                    'Ciudad',
                    'Direccion',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'Image',
                    'FechaNacimiento',
                    'Telefono',
                    'Migrar',
                    'SucursalEmpresa',
                    'TokenNemo',
                             'Migrar', 
                    "TipoProveedor"
                ],
                ['Nombres',
                    'Apellido',
                    'TipoFacturacion',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'Correo',
                    'Celular',
                    'Agencia',
                    'SucursalEmpresa',
                    'Password',
                    "RetenerIva"
                ], '1');

            break;

            //SUCURSAL AGENCIA
        case '9':
            //obtener agencias
            inputReq.setTipoEmpresa('1');
            inputReq.setTipoSucursal('');
            obtenerEmpresaAgencia();

            accionFormulario(['CorreoAdministrativo',
                'Celular',
                'Image',
                'Telefono',
                'TokenNemo',
                             'Migrar', 
                    "TipoProveedor"
            ], [
                'Nombres',
                'ActivoPersona',
                'ActivoDtpMundo',
                'FechaNacimiento',
                'Pais',
                'Ciudad',
                'Direccion',
                'TipoFacturacion',
                'Agencia',
                "RetenerIva"
            ], '1');

            $('#nombrePersona').html('Nombre Sucursal');

            break;

            //VENDEDOR AGENCIA 
        case '10':

            inputReq.setTipoEmpresa('1');
            inputReq.setTipoSucursal('9');
            obtenerEmpresaAgencia();

               if(persona.empresa === 1){

                        accionFormulario(['Telefono',
                                        'Pais',
                                        'Ciudad',
                                        'Direccion',
                                        'FechaNacimiento',
                                        'Image',
                                        'TokenNemo', 
                                         "TipoProveedor",
                                         "CodigoCangoro",
                                         "RetenerIva"
                                        ], 
                                        ['Nombres',
                                        'Apellido',
                                        'Agencia',
                                        'TipoDocumento',
                                        'TipoFacturacion',
                                        'ActivoPersona',
                                        'ActivoDtpMundo',
                                        'PuedeReservar',
                                        'PuedeReservarEnGastos',
                                        'DocumentoIdentidad',
                                        'SucursalEmpresa',
                                        'Celular',
                                        'Migrar',
                                        'Agente',
                                        'Password',
                                        'Correo' ],'1');
                        } else {
                        accionFormulario(['Telefono',
                                        'Pais',
                                        'Ciudad',
                                        'Direccion',
                                        'FechaNacimiento',
                                        'Image', 
                                         "TipoProveedor"], 
                                        ['Nombres',
                                        'Apellido',
                                        'Agencia',
                                        'TipoDocumento',
                                        'TipoFacturacion',
                                        'ActivoPersona',
                                        'DocumentoIdentidad',
                                        'SucursalEmpresa',
                                        'Celular',
                                        'Agente',
                                        'Password',
                                        'Correo',
                                        "RetenerIva" ],'1');

                        }





           break;

            //ADMINISTRATIVO AGENCIA
        case '12':
            inputReq.setTipoEmpresa('1');
            inputReq.setTipoSucursal('9');
            obtenerEmpresaAgencia();

            accionFormulario(['Pais',
                    'Ciudad',
                    'Direccion',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'Image',
                    'Migrar',
                    'FechaNacimiento',
                    'Telefono',
                    'TokenNemo', 
                    "TipoProveedor",
                    "CodigoCangoro"
                    
                ],
                ['Nombres',
                    'Apellido',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'TipoFacturacion',
                    'Correo',
                    'Celular',
                    'Agencia',
                    'SucursalEmpresa',
                    'Password',
                    "RetenerIva"
                ], '1');


            break;





            //PRESTADOR
        case '15':
            inputReq.setTipoSucursal('');
            accionFormulario(['DenominacionComercial',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'Correo',
                    'CorreoAdministrativo',
                    'Celular',
                    'Pais',
                    'Ciudad',
                    'Image',
                    'Migrar',
                    'TipoPersoneria',
                    'TokenNemo', 
                    "TipoProveedor",
                    "CodigoCangoro"
                ],
                ['Nombres',
                    'TipoFacturacion',
                    'ActivoDtpMundo',
                    'ActivoPersona',
                    'Direccion',
                    'Telefono',
                    "RetenerIva"
                ], '1');

            $('#cambioTel').html('Teléfono / Teléfono de Eemergencia');



            break;


            //PROVEEDOR
        case '14':
         inputReq.setTipoSucursal('');
         accionFormulario( ['DenominacionComercial',
                            'Celular',
                            'Image',
                            'CorreoAdministrativo',
                            'CorreoComerciales',
                            'CodigoAmadeus',
                            'Migrar',
                            "NroCuenta",
                            "EmisorCheque",
                            'TokenNemo', 
                            "TipoProveedor",
                            "CodigoNemo",
                            "CodigoCangoro"
                            ],
                            ['Nombres',
                            'Pais', 
                            'Ciudad',   
                            'PlazoPago',
                            'TipoPersoneria',                
                            'TipoDocumento',
                            'DocumentoIdentidad',
                            'Correo',
                            'TipoFacturacion',
                            'ActivoDtpMundo',
                            'ActivoPersona',
                            'Direccion',
                            'Telefono',
                            'ProveedorOnline',
                            'AgenteRetentor',
                            "Banco", 
                            "TipoProveedor",
                            "RetenerIva"
                            ],'1');
            
          $('#cambioTel').html('Teléfono / Teléfono de Eemergencia');
          $('.cambioAmadeus-n').html('Codigo Proveedor');
            break;

            //PASAJERO
        case '13':
            inputReq.setTipoSucursal('');
            accionFormulario(['Apellido',
                    'FechaNacimiento',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'Celular',
                    'Image',
                    'Pais',
                    'Ciudad',
                    'Telefono',
                    'ActivoPersona',
                    'Direccion',
                    'Correo',
                    "RetenerIva",
                    'Migrar', 
                    "TipoProveedor"
                ],
                ['Nombres',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'TipoFacturacion',
                    "RetenerIva",
                ], '1');



            break;

            //CLIENTE DIRECTO
        case '11':
            inputReq.setTipoSucursal('');
            accionFormulario(['Apellido',
                    'FechaNacimiento',
                    'TipoDocumento',
                    'DocumentoIdentidad',
                    'Celular',
                    'Image',
                    'Pais',
                    'Ciudad',
                    'Telefono',
                    'Direccion',
                    'Correo',
                    'Migrar',
                    'PlazoPago',
                    'Plazo', 
                    "TipoProveedor",
                    "CodigoCangoro"
                ],
                ['Nombres',
                    'ActivoPersona',
                    'ActivoDtpMundo',
                    'TipoFacturacion',
                    "RetenerIva",
                ], '1');



            break;
            //COBRADOR
            case '24':

                    obtenerEmpresaAgencia('2');
                    inputReq.setTipoSucursal('21');

                    accionFormulario(['Telefono',
                                    'Pais',
                                    'Ciudad',
                                    'Direccion',
                                    'TipoDocumento',
                                    'DocumentoIdentidad',
                                    'Image',
                                    'FechaNacimiento',
                                    'Celular',
                             'Migrar', 
                    "TipoProveedor"], 
                                    ['Nombres',
                                    'Apellido',
                                    'ActivoPersona',
                                    'ActivoDtpMundo',
                                    'SucursalEmpresa',
                                    'Agencia',
                                    'TipoFacturacion',
                                    'Password',
                                    'Correo',
                                    "RetenerIva" ],'1');


                break;


        default:
            inputReq.setTipoSucursal('');
            accionFormulario(listaPrincipal, '', '0');

    }

}
validarCorreo();
    $("#formSucursalEmpresa").val('{{$persona->id_sucursal_empresa}}').trigger('change.select2');
  //  $("#formAgencia").val('{{$persona->id_persona}}').trigger('change.select2');
    setTimeout(poblarSelect, 300);
    console.log('{{$ciudad}}');

   
    $('#tabLocalizacion').on('click', function (){
        $("#formCiudad").val('{{$ciudad}}').trigger('change.select2'); 
        $("#formCiudad").select2().val('{{$ciudad}}').trigger("change");
     })

function selectCiudad(){
    $("#formCiudad").val('{{$ciudad}}').trigger('change.select2'); 
    $("#formCiudad").select2().val('{{$ciudad}}').trigger("change");

}
</script>

@endsection
