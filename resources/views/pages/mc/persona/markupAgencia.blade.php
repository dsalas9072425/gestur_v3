
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">
	.question {
		color:#8F96EC  ;
	}
	.card,.card-header {
	border-radius: 14px !important;
}

</style>
 {{-- <link rel="stylesheet" href="{{asset('gestion/app-assets/vendors/css/select.dataTables.min.css')}}">  --}}
 

 <section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Markup Agencia</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
   		 <form id="frmPersonaConsulta">
				<div class="row">
					<div class="col-12 col-md-3 col-lg-3">
						<div class="form-group">
							<label for="formUsuario">Markup </label> <i class="fa fa-fw fa-question-circle question"></i> <span
								id="mensaje_m" style="color:red;"></span>
							<input type="text" class="form-control markupMask" name="" id="markupGeneral"
								onkeypress="return justNumbers(event)" value="" />
							<div class="checkbox">
								<label>
									<input type="checkbox" id="check-total"> Asignar a seleccionados.
								</label>
							</div>
						</div>
					</div>
					<div class="col-12">
						<button type="button" id="btnTodos" class="btn btn-light btn-lg text-white"><b>Seleccionar Todos</b></button>
						<button type="button" id="btnGuardar" class="btn btn-success btn-lg "><b>Guardar</b></button>
					</div>
				</div>
	    </form>  	



            	<div class="table-responsive table-bordered">
	              <table id="listado" class="table" style="width: 100%;">
	                <thead>
					  <tr>
					  	<th></th>
					  	<th>Empresa / Agencia</th>
						<th>Denominación Comercial</th>
						<th>Correo</th>
						<th>Documento/RUC</th>
						<th>Markup</th>

		              </tr>
	                </thead>
			        <tbody  style="text-align: center">
			        </tbody>
	              </table>
	            </div>  




			</div>
		</div>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')}}"></script> --}}
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script>


	

	
	   $('.markupMask').mask('Z.##', {

              translation: {
                  'Z': {
                    pattern: /^0/,
                     fallback: '0'
                  }
                },
                reverse: true,
             placeholder: "0,__"});


	   $('.question').on('click',function(){

	   	var coordenadas = $(".question").offset();

	   	$.toast({

    heading: 'Markup',
     hideAfter: 5000,
    text: 'Define un markup para todos los items seleccionados.',
    position: {
        left: coordenadas.left,
        top: coordenadas.top
    },
    icon: 'info',
    stack: false
});


	   })



		$('#btnGuardar').on('click',function(){

		
		var n = table.rows( { selected: true } ).nodes();	
		// var n = document.getElementsByClassName('selected');
		var dataUser = new Array();
		var band = '0';
		var markupInput = $('#markupGeneral').val();
		var text = '';
		var ok = 0;
		$('#mensaje_m').html('');	

		var regex = /^0.\d/;
        var found = markupInput.match(regex);

		band = ($('#check-total').is(':checked') == true) ? '1':'0';

		if(band == '1' && found === null){
			ok++;
			if(markupInput == ''){
				text = 'El campo Markup esta vacio.';		
				$('#mensaje_m').html(text);	
				} else{
                text = 'El markup para todos es mayor que 1.';
                $('#mensaje_m').html(text);	
        		}//else
        		   
		} 
		
		

		if(n.length == 0){
			 text = 'No existen campos seleccionados.';
			 ok++;

		}


		


		if(ok == 0){ 

			

			$.each(n , function(index,item){
			if(band == '1'){
				markup = markupInput;
			} else {
			var markup = $(item).find('.inputMarkup').val();
			}
			var idAgencia = $(item).find('.inputMarkup').attr('id');	

			dataUser.push({idAgencia:idAgencia, markup: markup});
		});

			console.log(dataUser);

			$.ajax({
	                    type: "POST",
	                    url: "{{route('modificarMarkupAgencia')}}",
                        dataType: 'json',
                        data: {data:dataUser},

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
                        success: function(rsp){

                        	if(rsp.resp == true){

                        		$.toast({
												heading: 'Los cambios fueron almacenados.',
												text: rsp.mensaje,
												position: 'top-right',
												showHideTransition: 'slide',
								    			icon: 'success'
											});

                        		table.ajax.reload();		
                        	} else {
                        		   $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error al almacenar los datos	.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        	}//else
                        	 
                        	
                          
                                }//funcion  
                });
		
	
		} else {
		
			  $.toast({
                            heading: 'Error',
                            text: text,
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
		}



		});




		       function justNumbers(e)
        {
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }





					
			var table =   $('#listado').DataTable({
					        "destroy": true,
					        "select": true,
					        "ajax": {
						            "url": "{{route('markupAgencia')}}",
						            "type": "GET",
						            	 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        }
						        },
					        
					        "columnDefs": [ {
						            orderable: false,
						            className: 'select-checkbox',
						            targets:   0
						        } ],

						"select": {
						            style: 'multi',
						            selector: 'td:first-child',
						             "page":'all'
						        },
						           

						"aaSorting":[[1,"asc"]],

						"columns":[
								{"data": null, defaultContent: ''},	
					            {"data":"nombre"},
					            {"data":"denominacion_comercial"},
					            {"data":"email"},
					            {"data":"documento_identidad"},
					           	{"data":function(x){ 
					           		// "input", "data": null, defaultContent: ''
					           		 var input = `<input type="text" value="${x.comision_pactada}" id="${x.id}" class="inputMarkup" disabled onkeypress="return justNumbers(event)">`;
					           		 return input;
					           	}},	
					        ]
						                    
					    });



					

		
					table.on( 'select', function ( e, dt, type, indexes ) {
						var g = table.rows( {selected:true} ).nodes().to$().children(':last-child');
						// console.log(g);
							$.each(g , function(index,item){
								var z = item.getElementsByClassName('inputMarkup');
								var z = z[0].removeAttribute("disabled");
						
							});
						
					} );
					table.on( 'deselect', function ( e, dt, type, indexes ) {
							var g = table.rows( {selected:false} ).nodes().to$().children(':last-child');
							// console.log(g);
							$.each(g , function(index,item){
								var z = item.getElementsByClassName('inputMarkup');
								z[0].setAttribute("disabled",'disabled');
							});


					});


					$('#btnAsignarMarkup').on('click', function(){
						var g = table.rows( {selected:true} ).nodes().to$().children(':last-child');
						$.each(g , function(index,item){
								var z = item.getElementsByClassName('inputMarkup');
								var z = z[0].setAttribute("value",$('#markupGeneral').val());
						
							});
					});



					$("#btnTodos").on( "click", function(e) {
						table.rows(  ).select();
					   // if ($(this).is( ":checked" )) {
					       
					   // } else {
					   //     table.rows(  ).deselect();
					   // }
					});



		
		

				
			

	</script>
@endsection