
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')



<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">ABM Centro Costo</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">

			<div class="card-body">

                <div class="card-body pt-0">
                    <button title="Agregar Centro Costo"
                        class="btn btn-success pull-right" type="button" id="btnAddData_modal">
                        <div class="fonticon-wrap">
                            <i class="ft-plus-circle"></i>
                        </div>
                    </button>
                </div>

				<form class="row" id="formTab_1">

				

					<div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Activo</label>
							<select class="form-control select2" name="activo" id="activoSearch">
                                <option value="">Todos</option>
                                <option value="true">SI</option>
                                <option value="false">NO</option>

							</select>
						</div>
                    </div>



				</form>

				<div class="row">
					<div class="col-12 mb-1">
						<button type="button" onclick="limpiarData()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
						<button type="button" class="btn btn-info pull-right btn-lg mr-1" onclick="consultaListado()"><b>Buscar</b></button>
					</div>
				</div>
            	<div class="table-responsive table-bordered">
		            <table id="listado_1" class="table" style="width: 100%;">
						<thead>
							<tr style="text-align: center;">
                                <th>Nombre</th>
                                <th>Codigo</th>
                                <th>Activo</th>
								<th>Usuario</th>
                                <th>Fecha</th>
                                <th></th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
						</tbody>
					</table>
				</div>


			</div>
		

	





		</div>
	</div>
</section>



	

		


    <div class="modal fade" id="cuentaAbm" tabindex="-1" role="dialog" aria-labelledby="cuentaAbm" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalTitulo">INFORMACIÓN TIPO CUENTA</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                 <form id="formModal_1" class="row">  

                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control modalData" maxlength="50" name="nombre" id="nombre_modal">
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>Codigo</label>
                            <input type="text" class="form-control modalData" maxlength="50"  name="codigo" id="codigo_modal">
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-4">
						<div class="form-group">
							<label>Activo</label>
							<select class="form-control select2Data_modal modalData" name="activo" id="activo_modal">
								<option value="true">SI</option>
                                <option value="false">NO</option>
							</select>
						</div>
                    </div>

                    <input type="hidden" name="id" id="input_modal_id" value="">

                </form> 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
              <button type="button" class="btn btn-success" id="btnGuardarData" style="display:none;">Guardar</button>
            </div>

          </div>
        </div>
    </div>








@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>





		{{--==========================================
					VARIABLES GLOBALES
			==========================================--}}
			var ordenamiento = [];
			var table;
            var msj_cuentas = '';

			$(()=>{
                configSelect2();
                consultaListado();
                $('#activoSearch').val('true').trigger('change.select2');
				calendar();
			});

	function configSelect2()
    {
                 $('.select2').select2({
                    minimumResultsForSearch : Infinity
                });

				$('.select2Data_modal').select2({
                    dropdownParent: $('#cuentaAbm'),
                    minimumResultsForSearch : Infinity
                });

    }


/*  ====================================================================================
				              CONSULTA DATA
	====================================================================================*/


	function consultaListado()
    {

			$.blockUI({
											centerY: 0,
											message: '<div class="loadingC"><img src="images/loading.gif" alt="Procesando..." style="width:100%"/><br><h2>Reservando ...</h2></div>',
											css: {
												color: '#000'
											}
										});

			// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
			setTimeout(function (){
						
	 		table =   $('#listado_1').DataTable({
					"destroy": true,
					"ajax": {
							data: $('#formTab_1').serializeJSON(),
							url: "{{route('ajaxListadoCentroCosto')}}",
							type: "GET",
					error: function(jqXHR,textStatus,errorThrown){

				   $.toast({
					heading: 'Error',
					text: 'Ocurrio un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});
					$.unblockUI();
				}
				  },
				 "columns":[
								{data:'nombre'},
                                {data:'codigo'},
								{data: (x)=>{
                                    if(x.activo == true){
                                        return 'SI';
                                }
                                return 'NO';
                                }},
                                {data: (x)=>{ return x.usuario.nombre +' '+ x.usuario.apellido; }},
                                {data: (x)=>{ return x.fecha_creacion_format; }},
								{data: function(x){
									return `
                                    <button  class='btn btn-info' onclick='verInfoData(${x.id})' type='button'><i class='fa fa-fw fa-search'></i>
                                    </button>

                                    <button  class='btn btn-info' onclick='editInfoData(${x.id})' type='button'><i class='fa fa-fw fa-edit'></i>
                                    </button>
                                    `;
								}}
							 
							]
				
				}).on('xhr.dt', function ( e, settings, json, xhr ){
					 $.unblockUI();
				});
				
	 
					}, 300);

    }//function

/*  ====================================================================================
				                    ABM DATA
	====================================================================================*/

    function configCamposModal(opt)
    {
        setIdEditModal('');
        if(opt){
            $('.modalData').val('').trigger('change.select2');
            $('.modalData').val('');
            $('.modalData').prop('disabled',true); //DESACTIVAR CAMPOS
        } else {
            $('.modalData').val('').trigger('change.select2');
            $('.modalData').val('');
            $('.modalData').prop('disabled',false); //ACTIVAR CAMPOS
        }
   
    }

    function setIdEditModal(id)
    {
        $('#input_modal_id').val(id); //DEFINE ID
    }

    function showModalData(opt)
    {   
        if(opt){
        $('#cuentaAbm').modal('show');
        }

        $('#cuentaAbm').modal('hide');
    }

    function verInfoData(id)
    {   
        configCamposModal(true);
        getDataInfo(id);
        showModalData(true);
    }

    function editInfoData(id)
    {   
        configCamposModal(false); //FORMATEA CAMPOS
        setIdEditModal(id); //DEFINE ID CUENTA
        $('#btnGuardarData').show();
        getDataInfo(id);
        showModalData(true);
    }
    //EVENTO GUARDAR
    $('#btnGuardarData').click(()=>{
        saveData();
    });

    //EVENTO CIERRE MODAL
    $('#cuentaAbm').on('hidden.bs.modal', function (e) {
        configCamposModal(true);
        $('#btnGuardarData').hide();
    });

    $('#btnAddData_modal').click(()=>{
        crearInfoCuenta();
    });

    function saveData()
    {
        if(msj_cuentas.reset){
		    msj_cuentas.reset();
	    }

        $.when(setDataInfo()).then(
            (a)=>{ 
            msj_cuentas = $.toast({	
					heading: '<b>Exito</b>',
					position: 'top-right', 
					text: 'Los datos fueron almacenados.',
					hideAfter: false,
					icon: 'success'
					});

                showModalData(false);
                consultaListado();

        }, (b)=>{
            msj_cuentas = $.toast({	
					heading: '<b>Atención</b>',
					position: 'top-right', 
					text: 'Ocurrio un error en la comunicación con el servidor.',
					hideAfter: false,
					icon: 'error'
					});
        });
    }

    function crearInfoCuenta()
    {   
        $('#btnGuardarData').show();
        configCamposModal(false);
        showModalData(true);
    }


    function getDataInfo(id)
    {
        $.ajax({
					type: "GET",
					url: "{{ route('getDataCentroCosto') }}",
					dataType: 'json',
					data: {id: id},

					error: function(jqXHR, textStatus, errorThrown)
                    {
                        $.toast({	
								heading: '<b>Atención</b>',
								position: 'top-right', 
								text: 'Ocurrio un error en la comunicación con el servidor.',
								hideAfter: false,
								icon: 'error'
							});
					},
					success: function(rsp)
                    {
                        let x = rsp.data[0];
                        //CARGAR LOS CAMPOS
                        $('#nombre_modal').val(x.nombre);
                        $('#codigo_modal').val(x.codigo);
                        $('#activo_modal').val(`${x.activo}`).trigger('change.select2');
                       
					}
				});


    }

    function setDataInfo()
    {
      return new Promise((resolve, reject) => {   

        let dataString =  $('#formModal_1').serializeJSON({
            customTypes: customTypesSerializeJSON
        });

        $.ajax({
					type: "GET",
					url: "{{ route('setDataCentroCosto') }}",
					dataType: 'json',
					data: dataString,

					error: function(jqXHR, textStatus, errorThrown)
                    {
                        reject();
					},
					success: function(rsp)
                    {  
                        if(rsp.err == true){
                            resolve();
                        }
                    reject();
                        
					}
				});
       });      
    }





/*  ====================================================================================
				                FUNCIONES AUXILIARES
	====================================================================================*/

	

    function limpiarData(){
			$('#activoSearch').val('').trigger('change.select2');
		}


		

function calendar()
        {

			$( ".fecha" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$('.calendario').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													    cancelLabel: 'Limpiar',
													    applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    }
								    			});
			$('.calendario').val('');
			$('.calendario').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });
		}
			

		
		




	</script>
@endsection