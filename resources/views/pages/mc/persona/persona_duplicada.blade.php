<!DOCTYPE html>
<html lang="es" translate="no">
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
    @parent
@endsection
    
@section('content')
<style>
.status-container {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    padding: 2px 4px; /* Ajusta el espacio interno del contenedor */
    border-radius: 4px; /* Ajusta el radio de los bordes */
}

.bg-success {
    background-color: #28a745;
}

.bg-danger {
    background-color: #dc3545;
}

.status-text {
    font-size: 12px; /* Ajusta el tamaño del texto */
}
</style>
<style>
    .pagination .page-item {
        border-radius: 50%;
    }
    /* Estilos para la tabla con desplazamiento horizontal */
    .table-responsive {
        overflow-x: auto;
    }
</style>
<style type="text/css">
	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	/*.bgRed:hover{
		color: white;
	}*/

	/*background: #e2076a !important;padding-left: 6px;padding-right: 6px;*/



	/*==================SELECT STAR============================*/

	.checkbox-menu li label {
    display: block;
    padding: 3px 10px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    margin:0;
    transition: background-color .4s ease;
}
.checkbox-menu li input {
    margin: 0px 5px;
    top: 2px;
    position: relative;
}

.checkbox-menu li.active label {
    background-color: #cbcbff;
    font-weight:bold;
}

.checkbox-menu li label:hover,
.checkbox-menu li label:focus {
    background-color: #f5f5f5;
}

.checkbox-menu li.active label:hover,
.checkbox-menu li.active label:focus {
    background-color: #b8b8ff;
}


.click_estrella {
	cursor:pointer; 
	cursor: hand;
}

/*====================================================
	ESTILO PARA FOCUS
  ====================================================	
*/

input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }



</style>

<section id="base-style">
    <br>
<form id="personadupli">
    <div class="card">
        <div class="card-header">
            <div class="card-body">   
            <h4 class="card-title">Registro Personas</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
        
            <div class="col-12 col-sm-4 col-md-3">
                <div class="form-group">
                    <label>Nombre/Apellido</label>
                    <input type="text" class="form-control" tabindex="5" name="nombre" id="nombre" value="" maxlength="100" />
                </div>
            </div>      

            <div class="col-12 col-sm-4 col-md-3">
                <div class="form-group">
                    <label>Denominacion Comercial</label>
                    <input type="text" class="form-control" tabindex="5" name="denominacion_comercial" id="denominacion_comercial" value="" maxlength="100" />
                </div>
            </div>  

            <div class="col-12 col-sm-4 col-md-3">
                <div class="form-group">
                    <label>Correo</label>
                    <input type="text" class="form-control" tabindex="5" name="correo" id="correo" value="" maxlength="100" />
                </div>
            </div>  
          
           

            <div class="col-12 col-sm-4 col-md-3">
                <div class="form-group">
                    <label for="formNombres">Ruc/CI</label>
                    <input type="text" class="form-control" tabindex="5" name="ruc"
                        id="ruc" value="" maxlength="100" />
                </div>
            </div>

          
        </div> 
    </form>   
        <div class="row">
             <div class="col-md-12">
             {{--   <a href="{{ route('facturapre_excel') }}" class="pull-right text-center btn btn-success btn-lg mr-1">Excel</a> --}}
             {{-- <button type="button" id="botonExcel" onclick="botonExcel()" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button> --}}
             <button type="button" onclick="limpiar()" id="btnLimpiar" class="pull-right btn btn-light btn-lg text-white mr-1 mb-1" tabindex="17" ><b>Limpiar</b></button>
                <button type="button" class="pull-right btn btn-info btn-lg mr-1 mb-1" role="button" onclick="buscarPersonas()" tabindex="16"><b>Buscar</b></button>
            </div>
        </div>
 
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                 
                        <div class="row">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3">
                           
                            </div>
                        </div>
                        <div class="table-responsive">
                        <table id="persona_duplicada" class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Cod</th>
                                    <th>Nombre/Apellido</th>
                                    <th>Denominacion Comercial</th>
                                    <th>Ruc</th>
                                    <th>Correo</th>
                                    <th>Cantidad Fac.</th>
                                    <th>Cantidad Pro.</th>
                                    <th>Tipo Persona</th>
                                    <th>Estado</th>
                                    <th>Accion</th>
                                    {{-- <th>Cant. Proforma</th>
                                    <th>Cant. Factura</th> --}}
                                </tr>
                            </thead>
                            <tbody id="facturas-table">
                              
    

                    
                        
                            </tbody>
                        </table>
                 
                    </div>
                    {{-- modal para inacticar --}}
                   {{-- modal para inacticar --}}
{{-- modal para inacticar --}}
@foreach ($personas as $persona)
    <div class="modal fade" id="ModalEliminar{{$persona->id}}" tabindex="-1" aria-labelledby="exampleModalLabel{{$persona->id}}" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel{{$persona->id}}">Eliminar Usuario</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    ¿Está seguro de eliminar el Usuario {{ $persona->nombre }}?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <form action="{{ route('personasDuplicadaEdit.edit', $persona->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endforeach
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
 
    @include('layouts/gestion/scripts')
	<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
    <script>
        function buscarPersonas() {
      $('#persona_duplicada').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ route('personasDuplicadaAjax') }}', 
            type: 'GET',
            data: $('#personadupli').serializeJSON()
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'nombre', name: 'nombre' },
            { data: 'denominacion_comercial', name: 'denominacion_comercial' },
            { data: 'documento_identidad', name: 'documento_identidad' },
            { data: 'email', name: 'email' },
            { "data": function(x)
						{
							var suma_factura = 0;
                            var cant_fac = x.cantidad_facturas;
                            var cant_fac_cli = x.cantidad_facturas_cliente;
                            suma_factura = cant_fac + cant_fac_cli;
							return suma_factura;
						} 
			},
           // { data: 'cantidad_facturas', name: 'cantidad_facturas' },
           { "data": function(x)
						{
							var suma_proforma = 0;
                            var cant_pro = x.cantidad_proformas;
                            var cant_pro_cli = x.cantidad_facturas_cliente;
                            suma_proforma = cant_pro + cant_pro_cli;
							return suma_proforma;
						} 
			},

           // { data: 'cantidad_proformas', name: 'cantidad_proformas' },
            { data: 'tipo_per', name: 'tipo_per' },
            {
                data: 'estado', 
                name: 'estado',
                createdCell: function (cell, cellData, rowData, rowIndex, colIndex) {
                    $(cell).addClass('text-white');
                    var container = $('<div style="height: 39px;width: 108px;">').addClass('status-container');
                    var statusText = $('<span>').text(cellData).addClass('status-text');
                    
                    if (cellData === 'ACTIVO') {
                        container.addClass('bg-success');
                    } else {
                        container.addClass('bg-danger');
                    }
                    container.append(statusText);
                    $(cell).empty().append(container);
                }
            },
            {
                data: null,
                render: function (data, type, row) {
                    var actionText = (row.estado === 'ACTIVO') ? 'INACTIVAR' : 'ACTIVAR';
                    var actionClass = (row.estado === 'ACTIVO') ? 'btn-danger' : 'btn-success';
                    
                    var editUrl = '{{ route("personasDuplicadaEdit.edit", ":id") }}';
                    editUrl = editUrl.replace(':id', data.id);
                    
                    return '<a href="' + editUrl + '" class="btn ' + actionClass + '">' + actionText + '</a>';
                }
            },
           // { data: 'cantidad_proformas', name: 'cantidad_proformas' },
            //{ data: 'cantidad_facturas', name: 'cantidad_facturas' },

           
        ]
    });
}




function limpiar(){
          
    $('#vendedor').val('').trigger('change.select2');

    $('#nombre').val('');
    $('#denominacion_comercial').val('');
    $('#ruc').val('');
    $('#correo').val('');
    
}

/*
$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                 $('#ventaprestador').attr('method','post');
               $('#ventaprestador').attr('action', "{{route('generarExcelVentaPrestador')}}").submit();
            });*/

    </script>
@endsection


