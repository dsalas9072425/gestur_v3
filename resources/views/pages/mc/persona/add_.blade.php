{{-- Funcionamiento --}}

@extends('masters')
@section('title', 'Agregar Personas')
@section('styles')
@parent

@endsection
@section('content')
<link rel="stylesheet" href="{{asset('gestion/app-assets/css/tagify.css')}}">
{{-- <link rel="stylesheet" href="{{asset('mC/css/dropzone.css')}}"> --}}


<style type="text/css">
    .frm > div {
    margin-bottom: 10px;
}

.tagify-container{
    min-height: 100px;
    height: auto;
    width:100%;
}

.error {
    border: 1px solid #c80000;
}

.bordeErrorInput {
 border:1px solid red;
}

.borderErrorBtn {
    border:5px solid red;
}
.readOnly {
     background-color: #eee;
 border-color: rgb(210, 214, 222);
 cursor: not-allowed !important;
}

  .question {
        color:#8F96EC  ;
    }

    .question2 {
         color:#8F96EC  ;
    }
</style>


<section id="base-style">

    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Formulario de Personas</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">

            <ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="tabPersona" data-toggle="tab" aria-controls="tabIcon21" href="#home" role="tab" aria-selected="true">
                        <i class="fa fa-play"></i>Persona </a>
                </li>
                @if(Session::get('datos-loggeo')->datos->datosUsuarios->id_plan_sistema == 3)
                    <li class="nav-item">
                        <a class="nav-link" id="tabDtpMundo" data-toggle="tab" aria-controls="tabIcon22" href="#menuDtpMundo" role="tab" aria-selected="false">
                            <i class="fa fa-flag"></i>Online</a>
                    </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" id="tabFacturacion" data-toggle="tab" aria-controls="tabIcon23" href="#menuFacturacion" role="tab" aria-selected="false">
                        <i class="fa fa-cog"></i>Facturación</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tabNegociacion" data-toggle="tab" aria-controls="tabIcon23" href="#menuNegociacion" role="tab" aria-selected="false">
                        <i class="fa fa-cog"></i>Negociación</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tabAdministrativo" data-toggle="tab" aria-controls="tabIcon23"  href="#menuAdministrativo" role="tab" aria-selected="false">
                        <i class="fa fa-cog"></i>Datos Administrativos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tabComercial" data-toggle="tab" aria-controls="tabIcon23" href="#menuComercial" role="tab" aria-selected="false">
                        <i class="fa fa-cog"></i>Comercial</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tabLocalizacion" data-toggle="tab" aria-controls="tabIcon23" href="#menuLocalizacion" role="tab" aria-selected="false">
                        <i class="fa fa-cog"></i>Localización</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="display:none;" id="tabEmpresaPersona" data-toggle="tab" aria-controls="tabIcon23" href="#sucursalEmpresaSection" role="tab" aria-selected="false">
                        <i class="fa fa-cog"></i>Empresa Persona</a>
                </li>
                
            </ul>


  


<form id="frmPersona" method="post" action="{{route('guardarPersona')}}" autocomplete="off">
    <div class="tab-content">
        

        <section id="home"  class="tab-pane active box-home" role="tabpanel">

            <div class="pt-1">
                <div class="pull-right">  
                    <button class="btnNextTab btn btn-success btn-lg pull-right" id="btnNextPersona"> Siguiente</button>
                </div>
               <h1 class="subtitle hide-medium" >Persona</h1>
           </div>  

                <div class="row no-gutters">
                    <div class="col-12 col-sm-6 col-md-5 col-lg-3">
                        <div class="form-group">
                            <label for="formTipoPersona">Tipo (*)</label>
                            <select class="select2 form-control" name="tipoPersona" id="formTipoPersona" style="width: 100%;">
                                <option value="">Seleccione Tipo Persona</option>

                                @foreach($tipoPersona as $personas)
                                <option value="{{$personas->id}}">{{$personas->denominacion}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    </div>


                    <div class="row"> 
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-10" id="inputDocumentoIdentidad">
                                    <div class="form-group">
                                        <label for="formDocumentoIdentidad">Documento de Identidad <span class="formDocumentoIdentidad"></span><span class="mensaje_err_documento"></span></label>
                                        <input type="text" class=" form-control" name="documentoIdentidad" id="formDocumentoIdentidad"  value="" maxlength="15" />
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-2" style="padding-left: 0px;">
                                    <div class="form-group">
                                        <label for="formDv">DV <span class="formDv"></span></label>
                                        <input type="text" class=" form-control" name="dv" id="formDv" disabled="disabled" value="" maxlength="15" />
                                    </div>
                                </div> 
                            </div>
                        </div>    
                        <div class="col-12 col-sm-6 col-md-3" id="inputTipoDocumento">
                            <div class="form-group">
                                <label for="formTipoDocumento">Tipo de Documento <span class="formTipoDocumento"></span></label>
                                <select class="form-control select2" name="tipoDocumento" id="formTipoDocumento" style="width: 100%;">
                                    <option value="">Seleccione Tipo Doc.</option>
    
                                    @foreach($tipoIdentidad as $identidad)
                                        <option value="{{$identidad->id}}">{{$identidad->denominacion}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-3"  id="inputNombres">
                            <div class="form-group">
                                <label for="formNombres"><span id="nombrePersona">Nombre o Razón Social</span> <span class="formNombres"></span></label>
                                <input type="text" class=" form-control" name="nombres" id="formNombres"  value="" maxlength="100" />
                            </div>
                        </div>
    
                        <div class="col-12 col-sm-6 col-md-3" id="inputApellido">
                            <div class="form-group">
                                <label for="formApellido">Apellido <span class="formApellido"></span></label>
                                <input type="text" class=" form-control" name="apellido" id="formApellido" value="" maxlength="100" />
                            </div>
                        </div>
    
                        <div class="col-12 col-sm-6 col-md-3" id="inputDenominacionComercial">
                            <div class="form-group">
                                <label for="formDenominacionComercial">Denominación Comercial <span class="formDenominacionComercial"></span></label>
                                <input type="text" class=" form-control" name="denominacionComercial"
                                       id="formDenominacionComercial" value="" maxlength="200" />
                            </div>
                        </div>
    
                        <div class="col-12 col-sm-6 col-md-3" id="inputFechaNacimiento">
                            <div class="form-group">
                                <label for="formFechaNacimiento">Fecha de Nacimiento o Creación <span class="formFechaNacimiento"></span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="fechaNacimiento" id="formFechaNacimiento" value="" maxlength="10">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-3" id="inputCorreo">
                            <div class="form-group">
                                <label>Correo Electronico Principal  <span class="formCorreo"></span>  <span class="mensaje_err"></span> </label>
                                <input type="email" class=" form-control" name="correo" id="formCorreo" value="" maxlength="100" />
                            </div>
                        </div>
                         <input type="hidden" class=" form-control" name="validacionCorreo" id="validacionCorreo"/>    
                        <div class="col-12 col-sm-6 col-md-3" id="inputCelular">
                            <div class="form-group">
                                <label for="formCelular">Celular <span class="formCelular"></span></label>
                                <input type="text" class=" form-control" name="celular" id="formCelular"
                                value="" maxlength="30" placeholder=" Máximo 30 caracteres" />
                            </div>
                        </div>
    
                        <div class="col-12 col-sm-6 col-md-3" id="inputTelefono">
                            <div class="form-group">
                                <label for="formTelefono"><span id="cambioTel">Telefono</span> <span class="formTelefono"></span></label>
                                <input type="text" class=" form-control" name="telefono" id="formTelefono" value="" placeholder="Máximo 30 caracteres" maxlength="30" />
                            </div>
                        </div>
    
    
    
                     <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputAgencia">
                            <div class="form-group">
                                <label for="formAgencia">Agencia / Empresa <span class="formAgencia"></span></label>
                                <select class="form-control select2" name="empresa" id="formAgencia" style="width: 100%;">
                                    <option value="">Seleccione una opción</option>
                                </select>
                            </div>
                     </div>
    
                      <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputSucursalEmpresa">
                            <div class="form-group">
                                <label for="formSucursalEmpresa">Sucursal Agencia / Empresa <span class="formSucursalEmpresa"></span></label>
                                <select class="form-control select2" name="sucursalEmpresa" id="formSucursalEmpresa" style="width: 100%;" disabled>
                                    <option value="">Seleccione una Sucursal</option>
    
                                </select>
                            </div>
                        </div>


                        <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputSucursalContable">
                            <div class="form-group">
                                <label for="formSucursalContable">Sucursal Contable <span class="formSucursalContable"></span></label>
                                <select class="form-control select2" name="id_sucursal_contable" id="formSucursalContable" style="width: 100%;" disabled>
                                    <option value="">Seleccione una Sucursal</option>
                                    @foreach($sucursal_contable as $sucursal_cont)
                                         <option value="{{$sucursal_cont->id}}">{{$sucursal_cont->nombre}}</option>
                                    @endforeach
    
                                </select>
                            </div>
                        </div>
                      
    
        
                        
                         <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputActivoPersona">
                            <div class="form-group">
                                <label for="">Activo en GESTUR<span class="formActivoPersona"></span></label>
                                <select class="form-control select2" name="activoPersona" id="formActivoPersona" style="width: 100%;">
                                    <option value="1">SI</option>
                                    <option value="false">NO</option>
                                </select>
                            </div>
                        </div>
    
    
                                          
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="form-group">
                                <label for="formPassword">Password <span class="formPassword"></span></label>
                                <input type="text" class=" form-control" name="password" id="formPassword" maxlength="100" value="" />
                                {{-- <small>* Máximo 100 caracteres</small> --}}
                            </div>
                        </div>    

                        <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputTipoProveedor">
                            <div class="form-group">
                                <label for="">Tipo Proveedor<span class="formTipoProveedor"></span></label>
                                <select class="form-control select2" name="tipoProveedor" id="formTipoProveedor" style="width: 100%;">
                                    @foreach($tipoProveedores as $tipoProveedor)
                                         <option value="{{$tipoProveedor->id}}">{{$tipoProveedor->denominacion}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputRetenerIva">
                            <div class="form-group">
                                <label for="">Retener IVA<span class="formRetenerIva"></span></label>
                                <select class="form-control select2" name="retenerIva" id="formRetenerIva" style="width: 100%;">
                                    <option value="true">SI</option>
                                    <option value="false">NO</option>
                                </select>
                            </div>
                        </div>

                       <div class="col-12 col-lg-12 pt-1" id="inputImage">
                            <button type="button" form="" id="formImage" class="btn btn-info"  data-toggle="modal" data-target="#modalAdjunto">
                                <b><i class="fa fa-fw fa-image"></i> SUBIR IMAGEN</b>
                            </button>
                       </div>
    
           
      
                  
                    <input type="hidden" name="nombreImagen" value="" id="imagen">
    
    
    
                    
            </div>
        </section>
        @if(Session::get('datos-loggeo')->datos->datosUsuarios->id_plan_sistema == 3)
            <section id="menuDtpMundo" class="tab-pane box-menuDtpMundo" role="tabpanel"> 

                <div class="row">

                <div class="col-12 mt-1">
                    <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
                   <h1 class="subtitle hide-medium" >Online</h1>
               </div>  


                         <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputActivoDtpMundo">
                            <div class="form-group">
                                <label>Activo Online<span class="formActivoDtpMundo"></span></label>
                                <select class="form-control select2" name="activoDtpMundo" id="formActivoDtpMundo" style="width: 100%;">
                                    <option value="1">SI</option>
                                    <option value="false">NO</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputProveedorOnline">
                            <div class="form-group">
                                <label>Proveedor Online<span class="formProveedorOnline"></span></label>
                                <select class="form-control select2" name="activoProveedorOnline" id="formProveedorOnline" style="width: 100%;">
                                    <option value="false">NO</option>
                                    <option value="1">SI</option>
                                </select>
                            </div>
                        </div>

                         {{-- <div class="col-12 col-sm-3 col-md-3" id="inputTokenNemo"> --}}
                            <div class="col-12 col-sm-3 col-md-3" id="">
                            <div class="form-group">
                                {{-- <label for="online">Online <span class="formTokenNemo"></span></label> --}}
                                <label for="online">Online <span class=""></span></label>
                                {{-- <input type="text" class=" form-control" name="token_nemo" id="formTokenNemo" /> --}}
                                <input type="text" class=" form-control" disabled/>
                            </div>
                        </div>

                        <input type="hidden" name="token_nemo">

                          <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputCodigoAmadeus">
                            <div class="form-group">
                                <label for=""><span id="codigoAmadeus-n">Codigo Proveedor</span> <span class="formCodigoAmadeus"></span></label>
                                <input type="text" class="form-control" value="" name="CodigoAmadeus" id="formCodigoAmadeus" maxlength="10" />
                            </div>
                        </div>  

                         <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputformPuedeReservar">
                            <div class="form-group">
                                <label>Puede Reservar <span class="formPuedeReservar"></span></label>
                                <select class="form-control select2" name="puedeReservar" id="formPuedeReservar" style="width: 100%;">
                                    <option value="true">SI</option>
                                    <option value="false">NO</option>
                                </select>
                            </div>
                        </div>

                         <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputPuedeReservarEnGastos">
                            <div class="form-group">
                                <label>Puede Reservar en Gastos <span class="formPuedeReservarEnGastos"></span></label>
                                <select class="form-control select2" name="puedeReservarEnGastos" id="formPuedeReservarEnGastos" style="width: 100%;">
                                    <option value="false">NO</option>
                                    <option value="true">SI</option>
                                </select>
                            </div>
                        </div>

                        


                         <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputFacturacionAutomaticaEnGastos">
                            <div class="form-group">
                                <label>Facturación Automática en Gastos <span class="formFacturacionAutomaticaEnGastos"></span></label>
                                <select class="form-control select2" name="facturacionAutomaticaEnGastos" id="formFacturacionAutomaticaEnGastos" style="width: 100%;">
                                    <option value="false">NO</option>
                                    <option value="true">SI</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputVoucher">
                            <div class="form-group">
                                <label for="">Descargar Voucher (Factura con Saldo)<span class="formVoucher"></span></label>
                                <select class="form-control select2" name="voucher" id="formVoucher" style="width: 100%;">
                                    <option value="">Seleccione opción</option>
                                    <option value="1">SI</option>
                                    <option value="false">NO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-3 col-md-3" >
                            <div class="form-group">
                                <label for="kontrol">Kontol <span class="kontrol"></span></label>
                                <input type="text" class=" form-control" name="kontrol" id="kontrol" maxlength="15" readonly />
                            </div>
                        </div>

                    </div> 
            </section> 
        @endif    
        <section id="menuFacturacion" class="tab-pane box-menuFacturacion" role="tabpanel"> 
            
            <div class="row">

            <div class="col-12 mt-1">
                <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
            <h1 class="subtitle hide-medium" >Facturación</h1>
            </div>  
    

                    <div class="col-12 col-sm-3 col-md-3 col-lg-3" id="inputIvaComisiones">
                         <div class="form-group">
                            <label>Iva Comisiones <span class="formIvaComisiones"></span></label>
                            <select class="form-control select2" name="ivaComisiones" id="formIvaComisiones" style="width: 100%;">
                                <option value="">Seleccione una opción</option>
                                  <option value="true">SI</option>
                                    <option value="false">NO</option>
                            </select>
                        </div>
                    </div>


                    <div class="col-12 col-sm-3 col-md-3 col-lg-3" id="inputTipoFacturacion">
                        <div class="form-group">
                            <label for="formTipoFacturacion">Facturacion <span class="formTipoFacturacion"></span></label>
                            <select class="form-control select2" name="tipoFacturacion" id="formTipoFacturacion" style="width: 100%;">
                                <option value="">Seleccione Facturacion</option>

                                @foreach($tipoFacturacion as $tFacturacion)
                                <option value="{{$tFacturacion->id}}">{{$tFacturacion->denominacion}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>
                         
           </div>
                   
        </section>

        <section id="menuNegociacion" class="tab-pane box-menuNegociacion" role="tabpanel"> 

            <div class="row">

            <div class="col-12 mt-1">
                <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
                <h1 class="subtitle hide-medium" >Negociación</h1>
            </div>  
                    <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputCredito">
                        <div class="form-group">
                            <label for="formCredito">Linea de Credito <span class="formCredito"></span></label>
                            <input type="number" class="form-control" name="lineaCredito" id="formCredito" value="" />
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3" id="inputPlazoPago">
                        <div class="form-group">
                            <label for="formPlazoPago">Plazo de Pago <span class="formPlazoPago"></span></label>
                            <select class="form-control select2" name="plazoPago" id="formPlazoPago" style="width: 100%;">
                                <option value="">Seleccione una opción</option>

                                @foreach($plazosPago as $plazo)
                                <option value="{{$plazo->id}}">{{$plazo->denominacion}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputPlazo">
                        <div class="form-group">
                            <label for="formPlazo">Plazo de Cobro <span class="formPlazo"></span></label>
                            <input type="number" class="form-control" name="plazo" id="formPlazo" value=""/>
                        </div>
                    </div>

                     <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputPlazoCobro">
                        <div class="form-group">
                            <label>Plazo de Cobro / Ticket <span class="formPlazoCobro"></span></label>
                            <select class="form-control select2" name="plazoCobro" id="formPlazoCobro" style="width: 100%;">
                                  <option value="">Seleccione una opción</option>
                                @foreach($plazoCobro as $plazoCob)
                                <option value="{{$plazoCob->id}}">{{$plazoCob->denominacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="col-12" id="inputAcuerdoEspecial" style="display:none">
                        <div class="form-group">
                            <label style="font-weight: 600;font-size: 18px;">Acuerdo Especial <span class="formAcuerdoEspecial"></span></label>
                            <textarea class="form-control" rows="5"  id="formAcuerdoEspecial"></textarea>
                        </div>
                    </div>

                </div>
                                    
        </section>

        <section id="menuAdministrativo" class="tab-pane box-menuAdministrativo" role="tabpanel"> 
 
                <div class="row">  

                    <div class="col-12 mt-1">
                        <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
                        <h1 class="subtitle hide-medium" >Administrativos</h1>
                    </div>

                            <div class="col-12 col-sm-12 col-md-6" id="inputCorreoAdministrativo">
                                <div class="form-group">
                                    <label>Correos Administrativos <span class="formCorreoAdministrativo"></span></label> <i class="fa fa-fw fa-question-circle question"></i>
                                    <textarea class="form-control" name="correoAdministrativo" id="correo_administrativo" /></textarea>

                                    <div class="checkbox">
                                        
                                        <label for="formNotificacionCorreo">
                                            <input type="checkbox" id="formNotificacionCorreo" name="notificacionCorreo"> Enviar Extracto
                                        <span class="formNotificacionCorreo"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="col-12 col-sm-12 col-md-3" id="inputTipoPersoneria">
                                <div class="form-group">
                                    <label for="formTipoPersoneria">Personeria <span class="formTipoPersoneria"></span></label>
                                    <select class="form-control select2" name="tipoPersoneria" id="formTipoPersoneria" style="width: 100%;">
                                        <option value="">Seleccione Personeria</option>

                                        @foreach($tipoPersoneria as $tPersoneria)
                                        <option value="{{$tPersoneria->id}}">{{$tPersoneria->denominacion}}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </div>

        

                            <div class="col-12 col-sm-6 col-md-3" id="inputAgenteRetentor">
                                <div class="form-group">
                                    <label for="formTipoPersoneria">Agente Retentor <span class="formAgenteRetentor"></span></label>
                                    <select class="form-control select2" name="agenteRetentor" id="formAgenteRetentor" style="width: 100%;">
                                        <option value="false">NO</option>
                                        <option value="1">SI</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputBanco">
                                <div class="form-group">
                                    <label>Banco <span class="formBanco"></span></label>
                                    <select class="form-control select2" name="banco" id="formBanco">
                                        <option value="">Todos</option>
                                        @foreach($bancos as $banco)
                                            <option value="{{$banco->id}}">{{$banco->nombre}}</option>
                                        @endforeach

                                    </select>
                                </div> 
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputNroCuenta">
                                <div class="form-group">
                                    <label>Nro. de Cuenta  <span class="formNroCuenta"></span></label>
                                    <input type="text" class="form-control" name ="nroCuenta" value="" id="formNroCuenta">
                                </div>
                            </div>

                            <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputEmisorCheque">
                                <div class="form-group">
                                    <label>Beneficiario <span class="formEmisorCheque"></span></label>
                                    <input type="text" class="form-control" name ="emisorCheque" value="" id="formEmisorCheque">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-3" id="inputMigrar">
                            <div class="form-group">
                                <label for="">Generar Costo/LC<span class="formMigrar"></span></label>
                                <select class="form-control select2" name="migrar" id="formMigrar" style="width: 100%;">
                                    <option value="">Seleccione opción</option>
                                    <option value="1">SI</option>
                                    <option value="false">NO</option>
                                </select>
                            </div>
                        </div>


                    </div>       
        </section>

        <section id="menuComercial" class="tab-pane box-menuComercial" role="tabpanel"> 
            
            <div class="row">

                <div class="col-12 mt-1">
                    <button class="btnNextTab btn btn-success btn-lg pull-right"> Siguiente</button>
                    <h1 class="subtitle hide-medium" >Comercial</h1>
                </div>
                
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6" id="inputCorreoComerciales">
                        <div class="form-group">
                            <label>Correos Comerciales <span class="formCorreoComerciales"></span></label> <i class="fa fa-fw fa-question-circle question2"></i>
                            <textarea class="form-control" value=""  id="correo_comerciales" name="correoComerciales" rows="4"></textarea>
                          
                        </div>
                    </div>

                    
                     <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputSucursalCarteraId" >
                        <div class="form-group">
                            <label for="formSucursalCarteraId">Sucursal Cartera<span class="formSucursalCarteraId"></span></label>
                            <select class="form-control select2" name="sucursalCarteraId" id="formSucursalCarteraId" style="width: 100%;" disabled>
                                <option value="">Seleccione una Sucursal</option>
                            @foreach($sucursalCartera as $sucursal)
                              <option value="{{$sucursal->id}}">{{$sucursal->nombre}} {{$sucursal->denominacion_comercial}}</option>
                            @endforeach

                            </select>
                        </div>
                    </div>

                    
                    <div class="col-12 col-sm-4 col-md-3 col-lg-3" id="inputAgente">
                        <div class="form-group">
                            <label for="formAgenteDtp">Agente <span class="formAgente"></span></label>
                            <select class="form-control select2" name="agente" id="formAgente" style="width: 100%;">
                                <option value="">Seleccione Agente</option>

                                @foreach($agente as $agentes)
                                <option value="{{$agentes->id}}">{{$agentes->nombre}} {{$agentes->apellido}} {{$agentes->email}}</option>

                                @endforeach

                            </select>
                        </div>
                    </div>


        
            </div>            
        </section> 

        <section id="menuLocalizacion" class="tab-pane box-menuLocalizacion" role="tabpanel"> 

            <div class="row">

                <div class="col-12 mt-1">
                    <h1 class="subtitle hide-medium" >Localización</h1>
                </div>

                
          

                        <div class="col-12 col-sm-3 col-md-4" id="inputPais">
                            <label for="formPais">Pais <span class="formPais"></span></label>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="button" data-toggle="modal" href="#requestCrearPais" title="Crear Pais" id="" class="btn btn-info btn-sm">
                                    <i class="fa fa-fw fa-plus-circle"></i>
                                </button>
                            </div>
                            <select class="select2 form-control  pais_select" name="pais" id="formPais" style="width:90%">
                                <option value="">Seleccione Pais</option>
                                @foreach($pais as $paises)
                                    <option value="{{$paises->cod_pais}}">{{$paises->name_es}}</option>

                                @endforeach
                            </select>
                            </div>
                    </div>
        

                    <div class="col-12 col-sm-4 col-md-4" id="inputCiudad">
                        <label for="formPais">Ciudad <span class="formCiudad"></span></label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <button type="button" data-toggle="modal" href="#requestCrearCiudad" title="Crear Pais" id="" class="btn btn-info btn-sm">
                                <i class="fa fa-fw fa-plus-circle"></i>
                            </button>
                          </div>
                          <select class="form-control select2" name="ciudad" id="formCiudad" style="width: 90%;">
                            <option value="">Seleccione Ciudad</option>
                        </select>
                        </div>
                </div>



                        <div class="col-12 col-sm-3 col-md-4" id="inputDireccion">
                            <div class="form-group">
                                <label for="formDireccion">Dirección <span class="formDireccion"></span></label>
                                <input type="text" class="form-control" name="direccion" id="formDireccion" value="" maxlength="200" />
                            </div>
                        </div>


                    {{--  <iframe src="https://www.google.com/maps/embed?pb="  height="450" frameborder="0" style="border:0; width:100%;" allowfullscreen></iframe> --}}
                            


                                    

                </div>            
        </section>  

        <section id="sucursalEmpresaSection" class="tab-pane box-menusucursalEmpresaSection" role="tabpanel"> 

            <div class="row">

                <div class="col-12 mt-1">
                    <h1 class="subtitle hide-medium" >Empresa Persona</h1>
                </div>

                <div class="col-6 col-sm-6 col-md-4 col-lg-4" id="inputEmpresaPersona">
                    <div class="form-group">
                       <label for="formEmpresaPersona">Empresa<span class="formEmpresaPersona"></span></label>
                       <select class="form-control select2"  id="formEmpresaPersona" style="width: 100%;">
                           @foreach($empresas as $key=>$empresa)
                                   <option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
                           @endforeach	
                       </select>
                   </div>
               </div>
                   
                <div class="col-6 col-sm-6 col-md-4 col-lg-4" id="inputSucursaEmpresaPersona">
                    <div class="form-group">
                        <label for="formSucursaEmpresaPersona">Sucursal<span
                                class="formSucursaEmpresaPersona"></span></label>
                        <select class="form-control select2" id="formSucursaEmpresaPersona"
                            style="width: 100%;" disabled>
                            <option value="">Seleccione una Sucursal</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4 col-lg-4" id="inputBtnAddEmpresaPersona">
                    <button onclick="addEmpresaPersona()" title="Guardar Fila" class="btn btn-success mt-2 formBtnAddEmpresaPersona" type="button"><i class="fa fa-fw fa-plus"></i> <b>Agregar</b></button>
                </div>


                        <div class="col-12 col-md-10 m-auto">
								<div class="table-responsive table-bordered">
		 							<table id="listadoEmpresaPersona" class="table" style="width:100%">
						                <thead>
											<tr style="text-align:center;">
												<th>Empresa</th>
												<th>Sucursal</th>
												<th></th>
								            </tr>
						                </thead>
						                <tbody class="tbody_empre_per">
                                            <tr>
                                                <td colspan="3" style="text-align:center;" class="relleno_table">
                                                    <b> Sin datos.....</b>
                                                </td>
                                            </tr>  
						                </tbody>
					              </table>
							   </div> 
                    </div>
        
            

                </div>           
        </section> 


<input type="hidden" name="correo_administrativo_array" id="input_hidden_correo_administrativo" value="">
<input type="hidden" name="correo_comercial_array" id="input_hidden_correo_comercial" value="">

</div>
</form>





</div>
</div>
<div class="card-footer">
    <div class="col-12">
        <button type="button" form="frmPersona" id="btnGuardar"
            class="btn btn-success btn-lg pull-right">Guardar</button>
    </div>
</div>
</div>




  
       
 
   
</section>
{{-- BASE STYLE --}} 
<!--======================================================
        MODAL DE ADJUNTO IMAGEN
========================================================== -->
    <!-- /.content -->
    <div id="modalAdjunto" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 60%;margin-top: 13%;">
        <!-- Modal content-->
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Agregar Adjunto <i class="fa fa-refresh fa-spin cargandoImg"></i></h2>
                </div>
                <div class="modal-body" style="padding-top: 0px;">
                    
                    
                            <div class="content" id='output'>

                            </div>  
 
                            <form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('personaUpload')}}" autocomplete="off">
                                <div class="form-group">
                                  <div class="row">
                           
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <input type="hidden" id="idTipoPersonas" name="tiposPersonas" value="" />
                                        <input type="file" class="form-control" name="image" id="image" style="margin-left: 3%; width: 96%"/>  <h4 class="modal-title titlepage" style="font-size: inherit;color:#072235;margin-left: 30px;margin-top: 10px;"><b>Formatos válidos: PNG, JPG y JPEG - La imagen para perfil de usuario sera valido en el proximo logueo de usuario.</b>
                                        </h4>
                                        <div id="validation-errors"></div>

                                      </div> 
                                    </div>  
                                  </div>  
                              </form>
                            <div class="row">
                                <div class= "col-12">
                                    <button type="button" id="btnCancelarVSTour" class="btn btn-danger" data-dismiss="modal">Cerrar</button>

                                    <button type="button" id="btnAceptarVoucher" class="btn btn-success pull-right"  data-dismiss="modal">Aceptar</button>
                                        
                                </div>
                            </div>
                        
                    </div>
                </div>  
            </div>  
        </div>  
        
                <div id="requestCrearPais" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                        <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="modal-title titlepage" style="font-size: x-large;">CREAR PAIS <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2><br>
                                        <h6 class="modal-title titlepage">Una vez creado el país el combo país será recargado de forma automática</h6>
                                        
                                        <span id="mensaje_cambio_p" style="font-weight: bold;"></span>
                                    </div>

                                    <div class="modal-body">
                                      
                                        <div class="container-fluid">
                                            <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Pais</label>                      
                                                            <input type="text" class="form-control" value="" id="pais_nuevo" />
                                                        </div>   
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Cod Pais</label>                      
                                                            <input type="text" class="form-control" value="" id="cod_pais_nuevo" />
                                                        </div>   
                                                    </div>
                                            </div>
                                            <button type="button" id="crearP" class="btn btn-success" >CREAR</button>
                                        </div>
                            </div>
                                        
                            <div class="modal-footer">
                                <button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
                            </div>

                        </div>
                    </div>
                </div>




<div id="requestCrearCiudad" class="modal fade" role="dialog">
            <div class="modal-dialog">
        <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title titlepage" style="font-size: x-large;">CREAR CIUDAD <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
                        <span id="mensaje_cambio_c" style="font-weight: bold;"></span>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Pais </label>
                                            <select class="form-control select2 pais_select" id="pais_select_relacion" style="width: 100%;">
                                                <option value="">Seleccione Pais</option>
                                             @foreach($pais as $paises)
                                                    <option value="{{$paises->cod_pais}}">{{$paises->name_es}}</option>

                                                @endforeach
      
                                                
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                            <div class="form-group">
                                        <label>Ciudad</label>                      
                                          <input type="text" class="form-control" value="" id="ciudad_nuevo" />
                                    </div> 
                                      </div>
                                </div>
                            <button type="button" id="crearC" class="btn btn-success" >CREAR</button>
                        </div>  
                  </div> 

                  <div class="modal-footer">
                    <button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
                  </div>
            </div>
        </div>
    </div>


    

            




  
  
         



@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.tagify.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
{{-- <script type="text/javascript" src="{{asset('mC/gestion/dropzone.js')}}"></script> --}}
<script>

   
    const persona = {
        empresa : Number('{{$idEmpresa}}')
    }
    var msj_emp = '';

    $(document).ready(()=>{
        if(persona.empresa !== 1){ 
            $('#tabDtpMundo').hide(); 
            $('#btnNextPersona').hide();
            }

            getSucursalEmpresaPersona();
    });

  
    $('.btnNextTab').click(function (e) {
        e.preventDefault();
        let contTab = 5;
        let ok=0;
        $.each($("#myTabs li a"), function(index, val) {

            console.log('>>>>',index,val);

               if($(val).hasClass('active')){
                   console.log('TRUE');
                  $($('#myTabs a')[index+1]).tab('show');
                   return false;
               }

          });

    });

    //NO FUNCIONA
//     $('#formAcuerdoEspecial').wysihtml5({
//     toolbar: {
//         "font-styles": true, // Font styling, e.g. h1, h2, etc.
//         "emphasis": true, // Italics, bold, etc.
//         "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
//         "html": false, // Button which allows you to edit the generated HTML.
//         "link": true, // Button to insert a link.
//         "image": false, // Button to insert an image.
//         "color": false, // Button to change color of font
//         "blockquote": true, // Blockquote
//         "size": 'sm' // options are xs, sm, lg
//     }
// });



//  $('#myTabs a').click(function (e) {
//   e.preventDefault();
//   $(this).tab('show');
// })

  // console.log(index);



// var dropzone_0 = new Dropzone("div#dropzone_Contact_image", {
//         url: "/contacts/upload_image",
//         thumbnailWidth: 145,
//         thumbnailHeight: 145,
//         parallelUploads: 1,
//         uploadMultiple: false,
//         clickable: true
//     });


//MENSAJE DE AYUDA
    //ADMINISTRATIVO
   $('.question').on('click',function(){

        var coordenadas = $(".question").offset();

       var myToast =  $.toast({

    heading: 'Markup',
     hideAfter: 7000,
    text: 'Digite los correos y oprima <b>Enter</b> <i class="fa fa-level-up" aria-hidden="true"></i> para registrar, además puede pegar una lista de correos separados por "," comas.',
    position: {
        left: coordenadas.left,
        top: coordenadas.top
    },
    icon: 'info',
    stack: false
});


       })

   //COMERCIAL
    $('.question2').on('click',function(){

        var coordenadas = $(".question2").offset();

      var myToast =   $.toast({

    heading: 'Markup',
     hideAfter: 7000,
    text: 'Digite los correos y oprima <b>Enter</b> <i class="fa fa-level-up" aria-hidden="true"></i> para registrar, además puede pegar una lista de correos separados por "," comas.',
    position: {
        left: coordenadas.left,
        top: coordenadas.top
    },
    icon: 'info',
    stack: false
});


       })





     //VARIABLE GLOBAL DE CORREOS   
     correo_administrativo = [];
     correo_comercial = [];
     $input = $('#correo_comerciales');
     $input2 = $('#correo_administrativo');
     inputTagEmail();


    function inputTagEmail(){ 
 

    $input.tagify({ 
            pattern:false,
            delimiters: ",", 
            templates : {
                    wrapper(input, settings){
                        console.log(settings);
                        return `<tags class="tagify-container tagify  ${input.className}" id="formCorreoComerciales"  readonly>
                            <span contenteditable data-placeholder="Correo Administrativos" class="tagify__input"></span>
                        </tags>`;
                    // return 'asdasd';
                    }
                },
            whitelist : [
                {"id":1,"value":"some string"}
            ]
        })
        .on('add', function(e, tagName){
            // console.log('JQEURY EVENT: ', 'added', tagName.data.value)
            correo_comercial.push(tagName.data.value);
        })
        .on("invalid", function(e, tagName) {
            // console.log('JQEURY EVENT: ',"invalid", e, ' ', tagName)

        });

    $input.on('remove', function(e, tagName){
    // console.log(tagName.data.value);
     $.each(correo_comercial, function(index,val){
        if(val == tagName.data.value){
        correo_comercial.splice(index, 1);
        // break;
            }
     });

});


// var jqTagify = $input.data('tagify').addTags('sdsdsdsdsd;sidjkasjdasd;ashdjashkjasd;dhjgdhj');


 $input2.tagify({ 
            delimiters: ",", 
            templates : {
                    wrapper(input, settings){
                        console.log(settings);
                        return `<tags class="tagify-container tagify  ${input.className}" id="formCorreoAdministrativo"  readonly>
                            <span contenteditable data-placeholder="Correo Administrativos" class="tagify__input"></span>
                        </tags>`;
                    // return 'asdasd';
                    }
                },
            whitelist : [
                {"id":1,"value":"some string"}
            ]
        })
        .on('add', function(e, tagName){
            // console.log('JQEURY EVENT: ', 'added', tagName.data)
            correo_administrativo.push(tagName.data.value);
        })
        .on("invalid", function(e, tagName) {
            // console.log('JQEURY EVENT: ',"invalid", e, ' ', tagName);
        });

    $input2.on('remove', function(e, tagName){
    // console.log(tagName.data.value);
     $.each(correo_administrativo, function(index,val){
        if(val == tagName.data.value){
        correo_administrativo.splice(index, 1);
        // break;
            }
     });

});


    // var jqTagify2 = $input.data('tagify');




  }//FUNCTION EMAIL TAG


  function resetCampoCorreosTag(){

     var jqTagify = $input.data('tagify');
     var jqTagify2 = $input.data('tagify');

     jqTagify.removeAllTags();
     jqTagify2.removeAllTags();

     $('#formCorreoComerciales').attr('readonly',true);
     $('#formCorreoAdministrativo').attr('readonly',true);

     correo_administrativo = [];
     correo_comercial = [];

  }

function campoCorreo(id,option){

    if(option == 1){
        $('#formCorreoAdministrativo').addClass('form-control');
        $('#formCorreoAdministrativo').addClass('readOnly');
    }
}





$('.select2').select2();
$('#formCredito').mask('000.000.000.000.000', {reverse: true});

document.getElementById("formDocumentoIdentidad").addEventListener('keyup', sanear);
document.getElementById("formCelular").addEventListener('keyup', sanear);
document.getElementById("formTelefono").addEventListener('keyup', sanear);


    function sanear(e)
        {
            let contenido = e.target.value;
            e.target.value = contenido.replace(" ", "");
        }

     //MARCAR LAS PESTAÑAS QUE CONTIENEN ERROR EN INPUT   
    function marcarTab(){


    //SI EN ESA PESTAÑA HAY UN MENSAJE DE ERROR MARCAR PRESTAÑA
    if($('.box-home').has(".inputError").length != 0){
        $('#tabPersona').css('color','#E7491F');
    } else {
        $('#tabPersona').css('color','#328E20');
    }

    if($('.box-menuDtpMundo').has(".inputError").length != 0){
        $('#tabDtpMundo').css('color','#E7491F');
    } else {
        $('#tabDtpMundo').css('color','#328E20');
    }

     if($('.box-menuFacturacion').has(".inputError").length != 0){
        $('#tabFacturacion').css('color','#E7491F');
    } else {
        $('#tabFacturacion').css('color','#328E20');
    }

     if($('.box-menuNegociacion').has(".inputError").length != 0){
        $('#tabNegociacion').css('color','#E7491F');
    } else {
        $('#tabNegociacion').css('color','#328E20');
    }
     if($('.box-menuAdministrativo').has(".inputError").length != 0){
        $('#tabAdministrativo').css('color','#E7491F');
    } else {
        $('#tabAdministrativo').css('color','#328E20');
    }

     if($('.box-menuComercial').has(".inputError").length != 0){
        $('#tabComercial').css('color','#E7491F');
    } else {
        $('#tabComercial').css('color','#328E20');
    }

     if($('.box-menuLocalizacion').has(".inputError").length != 0){
        $('#tabLocalizacion').css('color','#E7491F');
    } else {
        $('#tabLocalizacion').css('color','#328E20');
    }




   }//fucntion  





var listaPrincipal =   ['Nombres',
                       'Apellido',
                       'DenominacionComercial',
                       'FechaNacimiento',
                       'Pais',
                       'Ciudad',
                       'ActivoPersona',
                       'Direccion',
                       'Correo',
                       'Credito',
                       'IvaComisiones',
                       'PuedeReservar',
                       'PuedeReservarEnGastos',
                       'FacturacionAutomaticaEnGastos',
                       'NotificacionCorreo',
                       'TipoDocumento',
                       'Migrar',
                       'DocumentoIdentidad',
                       'Celular',
                       'Telefono',
                       'PlazoCobro',
                       'CorreoAdministrativo',
                       'CorreoComerciales',
                       'ActivoDtpMundo',
                       'SucursalCarteraId',
                       'CodigoAmadeus',
                       'Agencia',
                       'SucursalEmpresa',
                       'Agente',
                       'TipoFacturacion',
                       'TipoPersoneria', 
                       'PlazoPago',
                       'Password',
                       'Image',
                       'ProveedorOnline',
                       'Voucher',
                       'AgenteRetentor',
                       'EmisorCheque',
                       'SucursalContable',
                       'NroCuenta',
                       'Banco',
                       'TipoProveedor',
                       'RetenerIva',
                       'Plazo',
                       'TokenNemo'
                       ];



     accionFormulario(listaPrincipal,[],'0');                  


const inputReq = {
      required : [],
      tipoSuc:'',
      correoAuth: false,
      documentAuth:false,
      setRequired : function(req){ this.required = req},
      getRequired : function(){return this.required},
      setTipoSucursal: function(option){ this.tipoSuc = option},
      getTipoSucursal: function(){return this.tipoSuc},
      VerificarCorreo : function(o,b){ 

        if(o == 1){this.correoAuth = b;}
        if(o == 2){return this.correoAuth;}
    },
      VerificarDoc : function(o,b){ 

        if(o == 1){this.documentAuth = b;}
        if(o == 2){return this.documentAuth;}
    }






};

    function accionFormulario(mostrar,requerir,opcion){

     
    {{--=====================================================
                COLOCAR CAMPOS EN DAFAULT
    =======================================================--}}  
    if(opcion == '0'){
        $('#btnGuardar').attr('disabled',true);
            // inputReq.setRequired();
            //deshabilitarCampos y limpiar los requeridos
             for(var j = 0; j < mostrar.length;j++){
                var id = mostrar[j];


            //deshabilitar campo
            $('#form'+id).attr('disabled',true);
            //limpiar span requerido
            $(".form"+id).html('');

           // console.log($("#form"+id).get(0));


            if(jQuery.isEmptyObject($("#form"+id).get(0)) == false){
                var name = $("#form"+id).get(0).tagName;

                if(name == 'INPUT'){

                    //bordes de input en default
                        $("#form"+id).css('border-color','#d2d6de');
                        }

                if(name == 'SELECT'){

                 
                    //estilo de borde en gris
                    $("#input"+id).find('.select2-container--default .select2-selection--single').css('border','1px solid #aaa');
                        }   

                 if(name == 'A'){
                    $("#form"+id).removeClass('borderErrorBtn');
                    $('#form'+id).attr('data-toggle','#');
                        }          
            }


     }//for

 }//if
     
    {{--=====================================================
                HABILITAR CAMPOS Y RESETEAR
    =======================================================--}}   
    if(opcion == '1'){
            $('#btnGuardar').attr('disabled',false);
            


            //CARGA LOS DATOS QUE SE VAN HABILITAR Y MOSTRAR
            for(var j = 0; j < mostrar.length;j++){
                  var id = mostrar[j];

              if(id == 'CorreoAdministrativo'){
                $('#formCorreoAdministrativo').attr('readonly',false);
              } 

              if(id == 'CorreoComerciales'){
                 $('#formCorreoComerciales').attr('readonly',false);
              }   
         

              if(id == 'Image'){

                $('#form'+id).attr('data-toggle','modal');
                $('#form'+id).attr('disabled',false);
            }  

       
            if(id != 'SucursalEmpresa' || id != 'Ciudad') {
                 //habilitar campo
                $('#form'+id).attr('disabled',false);
                }


            }//for


             {{--=====================================================
                        SI HAY CAMPOS REQUERIDO SE HABILITAN AQUI
            =======================================================--}}  
            if(requerir != '' && requerir.length > 0){

            inputReq.setRequired(requerir);

            for(var j = 0; j < requerir.length;j++){
                var id = requerir[j];

                if(id == 'CorreoAdministrativo'){
                    $('#formCorreoAdministrativo').attr('readonly',false);
                  } 

                  if(id == 'CorreoComerciales'){
                     $('#formCorreoComerciales').attr('readonly',false);
                  }   
         
                 if(id == 'Image'){

                    $('#form'+id).attr('data-toggle','modal');
                    $('#form'+id).attr('disabled',false);
                    $(".form"+id).html('(*)');

                  } else if (id == 'SucursalEmpresa' || id == 'Ciudad'){

                   $(".form"+id).html('(*)');

                 }  else {
                     //habilitar campo
                    $('#form'+id).attr('disabled',false);
                    $(".form"+id).html('(*)');
                }

            }//for

        }//if

    }//if

    {{-- ======================================================
                VALIDAR LOS CAMPOS REQUERIDOS
        ======================================================--}}
    if(opcion == '2'){

        var ok = 0;

        var req = inputReq.getRequired();
     

        console.log('=========REQUERIDOS===========');
         for(var j = 0; j < req.length;j++){

                var id = req[j];
                 if(jQuery.isEmptyObject($("#form"+id).get(0)) == false){
                    var name = $("#form"+id).get(0).tagName;
                     console.log(name);

                     //CAMPOS DE CORREO
                     if(name == 'TAGS'){

                         if(id == 'CorreoAdministrativo'){
                            if(correo_administrativo.length == 0){
                                 $("#form"+id).css('border-color','red'); 
                                 $("#input"+id).addClass('inputError');
                                 console.log(id);
                             } else {
                                 $("#form"+id).css('border-color','#d2d6de');
                                 $("#input"+id).removeClass('inputError');
                             }
                            //VALIDAR SGTE 
                           continue; 
                        }

                         if(id == 'CorreoComerciales'){

                                 if(correo_comercial.length == 0){
                                    $("#form"+id).css('border-color','red');
                                    $("#input"+id).addClass('inputError'); 
                                    console.log(id);
                                 } else {
                                    $("#form"+id).css('border-color','#d2d6de');
                                    $("#input"+id).removeClass('inputError');
                                 }
                             //VALIDAR SGTE      
                            continue; 
                         }

                     }

                     // console.log('INICIO ='+ok);

                    if(name == 'INPUT' || name == 'TEXTAREA'){
                        var input = $("#form"+id).val();

                        if(input == ''){
                            console.log('ENTRO1');
                            $("#form"+id).css('border-color','red'); 
                            $("#input"+id).addClass('inputError');
                            console.log(id);
                            ok++;
                        } else {
                              console.log('ENTRO2');
                              $("#form"+id).css('border-color','#d2d6de');
                              $("#input"+id).removeClass('inputError');
                        } //else
                        console.log('INPUT ='+ok);

                    /*    if(id == 'Correo'  && $("#formTipoPersona").val()== 14){
                            $("#form"+id).css('border-color','#d2d6de');
                            $("#input"+id).removeClass('inputError');



                        }else{
                            if(inputReq.VerificarCorreo(2,'')){ 
                                 // $("#form"+id).css('border-color','#d2d6de');
                            } else {
                                $("#form"+id).css('border-color','red'); 
                                $("#input"+id).addClass('inputError');
                                ok++
                                }//else
                            }//if*/
                          
                        // console.log('VERIFICARzz ='+ok);

                          if(id == 'DocumentoIdentidad'){
                                if(!inputReq.VerificarDoc(2,'')){ 
                                    $("#form"+id).css('border-color','red'); 
                                    $("#input"+id).addClass('inputError');
                                    ok++
                                }//if
                            }//if    
                        }//if
                      //  console.log('=========FIN===========');

                    if(name == 'SELECT'){

                       var select =  $("#form"+id).val();
                            if(select == ''){
                                if(id == 'Banco' && $("#formTipoPersona").val()== 14){
                                    $("#input"+id).removeClass('inputError');    
                                    $("#input"+id).find('.select2-container--default .select2-selection--single').css('border','1px solid #aaa');
                                }else{    
                                    $("#input"+id).find('.select2-container--default .select2-selection--single').css('border-color','red'); 
                                    $("#input"+id).addClass('inputError');
                                    ok++;
                                    console.log('SELECT111 ='+ok+" - "+id);   
                                } 
                               
                            } else {
                             $("#input"+id).removeClass('inputError');    
                             $("#input"+id).find('.select2-container--default .select2-selection--single').css('border','1px solid #aaa');
                            }//else
                      //      console.log('SELECT ='+ok);

                       }//if   

                     //console.log('VERIFICARzz ='+ok);

                     if(name == 'A'){

                       var inputHidden = $("#imagen").val();

                        if(inputHidden == ''){
                             $("#input"+id).addClass('inputError');
                            $('#form'+id).addClass('borderErrorBtn');
                            console.log(id);

                            ok++;
                        } else {
                             $('#form'+id).removeClass('borderErrorBtn');
                             $("#input"+id).removeClass('inputError');
                        }//else
                            
                        
                        }//if
                }
            }//for

            console.log('TOTAL ='+ok);
            if(ok > 0){
                $('#formDv').prop('disabled',true);
                return false;
            } else {
                $('#formDv').prop('disabled',false);
                return true;
            }


    }//if

  


}//function

$('#formMigrar').change(function(){
        id_pais = $("#formPais option:selected").val();
        tipo_persona = $("#formTipoPersona option:selected").val();
        console.log('INDF '+id_pais);
        if( id_pais == 1455){
            $("#formMigrar").val('false').trigger('change.select2');
        }    
    });

    //RELACION PAIS CIUDAD
    $('#formPais').change(function(){
        id_pais = $("#formPais option:selected").val();
        tipo_persona = $("#formTipoPersona option:selected").val();
        console.log('INDF '+id_pais);
        if( id_pais == 1455){
            $("#formMigrar").val('false').trigger('change.select2');
        }

        obtenerCiudad();
  });
  
  function obtenerCiudad(){ 
    var id = $("#formPais option:selected").val();
    var idPais = {'idPais':id};

    if($("#formPais option:selected").val() != ''){


    console.log(idPais);

            $.ajax({
                        type: "GET",
                        url: "{{route('obtenerCiudad')}}",
                        dataType: 'json',
                        data: idPais,
                        tryCount : 0,
                        retryLimit : 3,
                         error : function(xhr, textStatus, errorThrown ) {
                            //REINTENTOS DE CONEXION
                                this.tryCount++;
                                if (this.tryCount <= this.retryLimit) {
                                    //try again
                                    $.ajax(this);
                                    return;
                                }   
                                $.toast({
                                        heading: 'Error',
                                        text: 'Ocurrio un error al obtener la ciudad.',
                                        position: 'top-right',
                                        icon: 'info'
                                    });

                                return;
                         
                        },
                        success: function(rsp){

                            console.log(rsp.ciudades.length);
                            if(rsp.ciudades.length != 0 ){
                                console.log('dd');
                              $('#formCiudad').empty();
                              $('#formCiudad').select2({placeholder: 
                                "Seleccione Ciudad",disabled: false});

                                $.each(rsp.ciudades, function (key, item){
                                var newOption = new Option(item.denominacion, item.id, false, false);
                                $('#formCiudad').append(newOption);
                            })

                            } else{ 

                            $('#formCiudad').empty();
                            var newOption = new Option('Seleccione Ciudad' ,'', false, false);
                                $('#formCiudad').append(newOption);   
                            $('#formCiudad').select2({ disabled: true });

                                    }//else
                          
                                }//funcion  
                });

    }

    
  };



//=====================IMAGEN FORM ========================//
     var options = { 
                                beforeSubmit:  showRequest,
                                success: showResponse,
                                dataType: 'json' 
                        }; 

                  //OCULTAR OVERLAY      
                 $('.cargandoImg').hide();       

                $('body').delegate('#image','change', function(){
                    //OBTENER TAMAÑO DE LA IMAGEN
                    var input = document.getElementById('image');
                    var file = input.files[0];
                    var tamaño = file.size;

                        if(tamaño > 0){

                        var tamaño = file.size/1000;
                        if(tamaño > 3000){
                            $("#image").val('');
                            $("#validation-errors").empty();
                            $("#validation-errors").append('<div class="alert alert-error"><strong>El tamaño de la imagen supera el limite de 3MB</strong><div>'); 
                        } else {

                            $('.cargandoImg').show();
                            $('#upload').ajaxForm(options).submit();  
                        }

                        } else {
                             $("#image").val('');
                             $("#validation-errors").empty();
                             $("#validation-errors").append('<div class="alert alert-error"><strong>Existe un error con el archivo</strong><div>'); 
                        }
                                        
                                });


                  function showRequest(formData, jqForm, options) { 
                   
                                    $("#validation-errors").hide().empty();
                                    return true; 
                                  } 


                  function showResponse(response, statusText, xhr, $form)  { 
                        $('.cargandoImg').hide();
                        if(response.success == false)
                          {
                       $("#image").val('');
                       $("#validation-errors").append('<div class="alert alert-error"><strong>'+ response.errors +'</strong><div>');
                       $("#validation-errors").show();

                        } else {
                             $("#imagen").val(response.archivo); 
                          
                          var file_name = response.archivo;
                                $('#output').html('');


                                divImagen = `
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <img class="card-img-top" style="max-width:150px;" src="{{asset('personasLogo')}}/${file_name}" alt="Adjunto Iamgen Perfil">
                                                        <p><a href="{{asset('personasLogo')}}/${file_name}" class="btn btn-info" target="_black"><b>VER</b></a></p>
										                <a href="#" class="card-title" style="font-size: 0.7rem; color: red;" onclick="eliminarImagen('${file_name}')"><b>Eliminar</b></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    `;

                   
                     $("#output").append(divImagen);
                                 
                             $("#image").prop('disabled',true);
                        }
                   }

                  function eliminarImagen(archivo){
                      $('.cargandoImg').show();

                      $.ajax({
                          type: "GET",
                          url: "{{route('fileDeletePersona')}}",//fileDelDTPlus
                          dataType: 'json',
                          data: {  
                               dataFile:archivo
                                    },


                            error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error al intentar eliminar la imagen.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                            $('.cargandoImg').hide();

                        },
                          success: function(rsp){
                            $('.cargandoImg').hide();

                            if(rsp.rsp == true){
                            document.getElementById("image").value = "";
                            $("#output").html('');
                                
                            } else {
                                     $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error al intentar eliminar la imagen.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                            document.getElementById("image").value = "";
                            $("#output").html('');

                            }//ELSE
                            
                            $("#image").prop('disabled',false);
                          }//function
                       

                    });
                  }//FUNCTION 




//==============CALENDARIO===================
$('#formFechaNacimiento').datepicker({
    timePicker: true,
    minDate: 0,
    format: 'dd/mm/yyyy',
    language: 'es',
    orientation: "bottom"
});





// =============== OBTENER LA SUCURSAL SI ES UNA AGENCIA/EMPRESA =================
        $('#formAgencia').change(function(){
            var id = $("#formAgencia option:selected").val();
            var option = inputReq.getTipoSucursal();

            var idAgencia = {'idAgencia':id,'tipoSucursal':option,'idEmpresa':$(this).val()};
            
            if(id != '' & option != '' ){

               $.ajax({
                        type: "GET",
                        url: "{{route('obtenerSucursalAgencia')}}",
                        dataType: 'json',
                        data: idAgencia,

                          error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
                        success: function(rsp){

                            if(rsp.sucursalAgencia.length != 0 ){
                              $('#formSucursalEmpresa').empty();
                              $('#formSucursalEmpresa').select2({disabled: false});

                              // var newOption = new Option('Seleccione Sucursal', '0', false, false);
                              //   $('#formSucursalEmpresa').append(newOption);

                                $.each(rsp.sucursalAgencia, function (key, item){
                                    var datosSucursal = item.nombre;
                                    if(jQuery.isEmptyObject(item.denominacion_comercial) == false){
                                        datosSucursal = datosSucursal+' - '+item.denominacion_comercial;
                                    }
                                    var newOption = new Option(datosSucursal, item.id, false, false);
                                    $('#formSucursalEmpresa').append(newOption);
                                })

                            } else{ 

                            $('#formSucursalEmpresa').empty();   
                             var newOption = new Option('Sin sucursal', '', false, false);
                                $('#formSucursalEmpresa').append(newOption);
                            $('#formSucursalEmpresa').select2({ disabled: true });
                                    }//else
                          
                                }//funcion  
                });


        }//if
    
  });



function obtenerEmpresaAgencia(id){
             $.ajax({
                        type: "GET",
                        url: "{{route('obtenerEmpresaAgencia')}}",
                        dataType: 'json',
                        data: {id:id},

                          error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
                        success: function(rsp){
                     //   alert(id);
                       if(rsp.response.length > 0 ){
                        //formAgencia
                         $('#formAgencia').empty();
                         total = $('#formTipoPersona').val()
                         if(total == 5 || total == 7|| total == 6|| total == 21|| total == 3){
                          }else{
                            var newOption = new Option("Seleccione una opción", '', false, false);
                            $('#formAgencia').append(newOption);
                          } 

                          $.each(rsp.response, function (key, item){
                                var newOption = new Option(item.nombre, item.id, false, false);
                                $('#formAgencia').append(newOption);
                            });

                          $('#formAgencia').trigger('change');
                       } else {

                        $('#formAgencia').select2({placeholder: 
                                "Error",disabled: true})
                       }//else
                          
                                }//funcion  
                       
                });

}


// ===============AJAX PARA GUARDAR FORMULARIO=================
$('#btnGuardar').click( function(event){
    event.preventDefault();
    $.toast().reset('all');
    $('#btnGuardar').prop('disabled',true);


    let correo_admin;
    let correo_comer;
     //ALMACENAR CORREOS
    $.each(correo_administrativo,function(index,val){
        if(index == 0){
        correo_admin = val;
        } else {
        correo_admin +=','+val;
        }
    }); 
       $('#input_hidden_correo_administrativo').val(correo_admin);

     $.each(correo_comercial,function(index,val){
        if(index == 0){
        correo_comer = val;
        } else {
        correo_comer += ','+val;
        }
    }); 
      $('#input_hidden_correo_comercial').val(correo_comer);

       // alert(accionFormulario('','','2'));

        if(accionFormulario('','','2')){
           if($('#validacionCorreo').val() == 0){
           
            $('#acuerdo_id').val($('#formAcuerdoEspecial ~ iframe').contents().find('.wysihtml5-editor').html());
            var dataString = $('#frmPersona').serializeJSON();
 


                $.ajax({
                        type: "GET",
                        url: "{{route('guardarPersona')}}",
                        dataType: 'json',
                        data: dataString,

                         error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                           $('#btnGuardar').prop('disabled',false);

                        },
                        success: function(rsp){
                            $('#btnGuardar').prop('disabled',false);
                           
                            if(rsp.dato == 'true'){

                            $.toast({
                                    heading: 'Success',
                                    text: 'Los datos fueron almacenados.',
                                    position: 'top-right',
                                    showHideTransition: 'slide',
                                    icon: 'success'
                                });
                            

                                  window.location.href = "{{ route('personaIndex') }}";


                            } else {
                                // console.log(rsp.complete_debug);
                            $.toast({
                            heading: 'Error',
                            text: 'Ocurrió un error al almacenar los datos.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });;


                            
                            }//else
                         }//funcion  
                });

          }else{
             marcarTab(); 
             $.toast({
                        heading: 'Error',
                        text: 'Ya existe el correo.',
                        position: 'top-right',
                        showHideTransition: 'fade',
                        icon: 'error'
                        });;

             $('#tabPersona').css('color','#E7491F');
             $('#formCorreo').css('border-color','#E7491F');
            $('#btnGuardar').prop('disabled',false);

          }    

        } else {
             $.toast({
            heading: 'Error',
            text: 'Complete los campos requeridos (*).',
            position: 'top-right',
            showHideTransition: 'fade',
             icon: 'error'
                        });
             $('#btnGuardar').prop('disabled',false);

             //VALIDAR TABS
                marcarTab();
        } 
   });


// ===============VALIDAR DOCUMENTO DE IDENTIDAD ==============+

     $('#formDocumentoIdentidad').keyup(function(e){
            validarDocumento();
           });//function

           function agregarDv(){
                $.ajax({
                        type: "GET",
                        url: "{{ route('agregarDv') }}",
                        dataType: 'json',
                        data: { documento: $('#formDocumentoIdentidad').val().trim()},

                         error: function(jqXHR,textStatus,errorThrown){
                            $('.mensaje_err_documento').html(' No se pudo validar. ');
                            $('.mensaje_err_documento').css('color','red');
                            inputReq.VerificarDoc(1,false);
                      
                        },
                        success: function(rsp){
                           // console.log(rsp[0]);
                           if(jQuery.isEmptyObject(rsp[0]) == false){
                                if(jQuery.isEmptyObject(rsp[0].dv) == true){
                                    $('#documentoIdentidad').val(rsp[0].ruc);
                                    $('#formDv').val(rsp[0].dv);
                                    $('#formDv').prop('disabled',true);
                                    $('#formTipoDocumento').val(2).trigger('change.select2');
                                }else{
                                    $('#formDv').prop('disabled',false);
                                    $('#formTipoDocumento').val(1).trigger('change.select2');
                                }

                                if(jQuery.isEmptyObject(rsp[0].nombre) == false){
                                    denominacion = rsp[0].nombre;              
                                    if(denominacion.indexOf(",") == true){          
                                            base = denominacion.split(',');
                                            $('#formNombres').val(base[1]);
                                            $('#formApellido').val(base[0]);
                                    }else{
                                        $('#formNombres').val(denominacion);
                                        $('#formApellido').val('');
                                    }
                                }else{
                                    $('#formNombres').val('');
                                    $('#formApellido').val('');
                                }
                            }else{
                                $('#formDv').prop('disabled',false);
                                $('#formTipoDocumento').val('').trigger('change.select2');
                                $('#formNombres').val('');
                                $('#formApellido').val('');
                                $('#formDv').val('');
                           }
                        }
                })   
           }    

           function validarDocumento(){

                   $.ajax({
                        type: "GET",
                        url: "{{ route('validarCedula') }}",
                        dataType: 'json',
                        data: { documento: $('#formDocumentoIdentidad').val().trim()},

                         error: function(jqXHR,textStatus,errorThrown){
                            $('.mensaje_err_documento').html(' No se pudo validar. ');
                            $('.mensaje_err_documento').css('color','red');
                            inputReq.VerificarDoc(1,false);
                        },
                        success: function(rsp){

                            console.log(rsp);
                            if(rsp.err == false){
                                $('.mensaje_err_documento').html(' El Nro Documento ya existe.');
                                $('.mensaje_err_documento').css('color','red');
                                inputReq.VerificarDoc(1,false);
                                $('#btnGuardar').prop('disabled',true);
                            }else{
                                $('.mensaje_err_documento').html('');
                                inputReq.VerificarDoc(1,true);
                                $('#btnGuardar').prop('disabled',false);
                                agregarDv();
                            }


                        }
                           
                          
                    });
   
     

    }//function












// ===============VALIDAR CORREO ============================+
    $('#formCorreo').keyup(function(e){

            let contenido = e.target.value;
            e.target.value = contenido.replace(" ", "");

            validarCorreo();

           });//function

        function validarCorreo(){
            var dataString = {'e' : $('#formCorreo').val().trim(),
                              'idTipoPersona' : $('#formTipoPersona').val()}
            $('.mensaje_err').html('');
            $('.mensaje_err').css('color','green');

            if($('#formCorreo').val().trim() != ''){
                var patron = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
                var correo = $('#formCorreo').val().trim();
                var esCoincidente = patron.test(correo);

                if(esCoincidente) {
                    $.ajax({
                            type: "GET",
                            url: "{{ route('validarCorreo') }}",
                            dataType: 'json',
                            data: dataString,
                            error: function(jqXHR,textStatus,errorThrown){
                                $('.mensaje_err').html('El correo ya existe');
                                $('.mensaje_err').css('color','red');
                                inputReq.VerificarCorreo(1,false);
                                $('#validacionCorreo').val(1);
                            },
                            success: function(rsp){
                                console.log(rsp.resp);
                                if(rsp.resp != '0'){
                                    /*$('.mensaje_err').html(' El correo ya existe ');
                                    $('.mensaje_err').css('color','red');*/
                                    inputReq.VerificarCorreo(1,false);
                                    $('#validacionCorreo').val(1);
                                }else{
                                    /*  $('.mensaje_err').html(' Correo Válido ');
                                    $('.mensaje_err').css('color','green');*/
                                    $('#validacionCorreo').val(0);
                                }
                            }
                        
                        });
                }else{
                    $('.mensaje_err').html(' Formato no valido. ');
                    $('.mensaje_err').css('color','red');
                    inputReq.VerificarCorreo(1,false);
                    $('#validacionCorreo').val(1);
                }
            } 


       // ===============GENERAR HASH CORREO ==============+
       dataStg = '';
        dataStg = {
            'online': $('#formCorreo').val(),
            'indice': 0
         }

        // console.log(dataString);
    
        $.ajax({
                        type: "GET",
                        url: "{{ route('generarHash') }}",
                        dataType: 'json',
                        data: dataStg,

                         error: function(jqXHR,textStatus,errorThrown){
                          console.log('Error comunicación con el servidor para generar hash');
                           $('#online').val('');
                        },
                        success: function(rsp){
                            $('#online').val(rsp.resp);
                            $('#kontrol').val(rsp.resp+'.');

                        }
                           
                          
                    });

    }//function

 



      //CREA CIUDAD
    $('#crearC').on('click',function(){


        $('.cargandoImg').show();
        var ciudad = $('#ciudad_nuevo').val().trim();
        var paisRelacion = $('#pais_select_relacion').val().trim();
        var ok = 0;
        var opcion  =0;
     



            //CIUDAD NUEVO CON RELACION DE PAIS
           if(ciudad == '' || paisRelacion == '') ok++

            opcion = 1;
 
            dataString = {
            ciudad : ciudad,
            paisRelacion : paisRelacion,
            opcion : opcion,
            operacion: 1
        }

            if(ok == 0){

           $.ajax({
                        type: "GET",
                        url: "{{ route('crearCiudadPais') }}",
                        dataType: 'json',
                        data: dataString,

                         error: function(jqXHR,textStatus,errorThrown){
                            $('#mensaje_cambio_c').css('color','red');
                            $('#mensaje_cambio_c').html('Ocurrio un error en la comuniación con el servidor.');
                            $('.cargandoImg').hide();
                        
                        },
                        success: function(rsp){
                             $('.cargandoImg').hide();

                             if(rsp.resp == true){

                            $('#mensaje_cambio_c').css('color','green');
                            $('#mensaje_cambio_c').html('Los datos fueron creados con éxito.');
                            $('.cargandoImg').hide();

                             $('#ciudad_nuevo').val('');
                             $("#pais_select_relacion").val('').trigger('change.select2');
                           

                             } else {

                             //validar nombre y retornar mensaje de error   
                            $('#mensaje_cambio_c').css('color','red');
                            $('#mensaje_cambio_c').html(rsp.m);
                            $('.cargandoImg').hide();

                             }

                        }
                           
                          
                    }).done(function(){

                        $(".pais_select").val(paisRelacion).trigger('change.select2');
                            obtenerCiudad(); 
                       
                    });//function done


            } else {
                $('#mensaje_cambio_c').css('color','red');
                $('#mensaje_cambio_c').html('Complete los campos por favor');
                $('.cargandoImg').hide();

            }//else


     });




      //CREA PAISES
    $('#crearP').on('click',function(){


        $('.cargandoImg').show();
        var pais = $('#pais_nuevo').val().trim();
        var cod_pais = $('#cod_pais_nuevo').val().trim();
        var ok = 0;
        var opcion  =0;
        var paisNuevo = '';


     
            //PAIS
             if(pais == '') ok++
             if(cod_pais == '') ok++  

             opcion = 2;
        

            dataString = {
            pais : pais,
            codPais :cod_pais, 
            opcion : opcion,
            operacion: 1
        }

            if(ok == 0){

           $.ajax({
                        type: "GET",
                        url: "{{ route('crearCiudadPais') }}",
                        dataType: 'json',
                        data: dataString,

                         error: function(jqXHR,textStatus,errorThrown){
                            $('#mensaje_cambio_p').css('color','red');
                            $('#mensaje_cambio_p').html('Ocurrio un error en la comuniación con el servidor.');
                            $('.cargandoImg').hide();
                        
                        },
                        success: function(rsp){
                             $('.cargandoImg').hide();

                             if(rsp.resp == true){

                             nuevoPais = rsp.id;   
                            $('#mensaje_cambio_p').css('color','green');
                            $('#mensaje_cambio_p').html('Los datos fueron creados con éxito.');
                            $('.cargandoImg').hide();

                             $('#pais_nuevo').val('');
                             $('#ciudad_nuevo').val('');
                              $("#pais_select_relacion").val(paisNuevo).trigger('change.select2');
                             $('#cod_pais_nuevo').val('');

                             } else {


                            $('#mensaje_cambio_p').css('color','red');
                            $('#mensaje_cambio_p').html(rsp.m);
                            $('.cargandoImg').hide();

                             }

                        }
                           
                          
                    }).done(function(){

                        //RECARGA EL COMBO DE PAISES
                         dataString = {operacion: 2}


                         $.ajax({
                        type: "GET",
                        url: "{{ route('crearCiudadPais') }}",
                        dataType: 'json',
                        data: dataString,

                        success: function(rsp){

                            
                            $('.pais_select').empty();
  
                         var newOption = new Option("Seleccione Pais", '', false, false);
                                $('.pais_select').append(newOption);

                          $.each(rsp.p, function (key, item){
                                var newOption = new Option(item.name_es, item.cod_pais, false, false);
                                $('.pais_select').append(newOption);
                            });

                           $(".pais_select").val(nuevoPais).trigger('change.select2');

                        }//success
                           
                          
                    });//function ajax


                    });//function done



            } else {
                $('#mensaje_cambio_p').css('color','red');
                $('#mensaje_cambio_p').html('Complete los campos por favor');
                $('.cargandoImg').hide();

            }


     });


/**
 * LOGICA PERSONA EMPRESA
*/

$('#formEmpresaPersona').change(()=>{
    getSucursalEmpresaPersona();
});

 const data_control_emp = {
     arr : [],
    set_data : function(d){ this.arr.push(d)},
    get_data : function(){ return this.arr;}
}

function getSucursalEmpresaPersona()
{
    empresa_id = $('#formEmpresaPersona').val();
    $("#formSucursaEmpresaPersona").prop('disabled',true); 
    $('#formSucursaEmpresaPersona').empty();
    console.log(empresa_id);
    if(jQuery.isEmptyObject(empresa_id) == false ){
            $.ajax({
                        type: "GET",
                        url: "{{route('getSucursalEmpresaPersona')}}",
                        dataType: 'json',
                        data: {empresa_id:empresa_id},
                        error: function (jqXHR, textStatus, errorThrown) 
                        {
                            $.toast({
                                heading: 'ATENCIÓN',
                                position: 'top-right',
                                hideAfter: false,
                                loaderBg: '#9EC600',
                                text: 'Ocurrio un error al intentar recuperar los datos de sucursal..',
                                showHideTransition: 'fade',
                                icon: 'warning'
                            });
                        },
                            success: function (rsp) {
                                if(rsp.data.length > 0){
                                    $('#formSucursaEmpresaPersona').empty();
                                    $.each(rsp.data, function (key, item) {
                                        var newOption = new Option(item.nombre, item.id, false, false);
                                        $('#formSucursaEmpresaPersona').append(newOption);
                                    });
                                    $("#formSucursaEmpresaPersona").prop('disabled',false);
                                } else {
                                    $("#formSucursaEmpresaPersona").prop('disabled',true); 
                                }
                                
                        }


                    }); 
                }
}


function addEmpresaPersona()
{
    let empresa_text = $('#formEmpresaPersona option:selected').text();
    let empresa_id = $('#formEmpresaPersona').val();
    let sucursal_txt = $('#formSucursaEmpresaPersona option:selected').text();
    let sucursal_id = $('#formSucursaEmpresaPersona').val();
    let ok = 0;

    	//CLEAR MSJ REPEAT
        if(msj_emp.reset){
			msj_emp.reset();	
		}

    //SI AMBOS EXISTEN ENTONCES ES DUPLICADO
    $.each(data_control_emp.get_data(),(index,value)=>{
        console.log(value);
        if(value.empresa_id == empresa_id){ ok++;}
        if(value.sucursal_id == sucursal_id){ ok++;}

        if(ok == 2)
        {
            return false;
        } else {
            ok = 0;
        }
    });

    if(empresa_id == '' | sucursal_id == ''){
        msj_emp = $.toast({
                        heading: 'ATENCIÓN',
                        position: 'top-right',
                        hideAfter: false,
                        loaderBg: '#9EC600',
                        text: 'Empresa y Sucursal es obligatorio.',
                        showHideTransition: 'fade',
                        icon: 'warning'
            });
     return '';    
    }

    if(ok == 2 ){
        msj_emp = $.toast({
                        heading: 'ATENCIÓN',
                        position: 'top-right',
                        hideAfter: false,
                        loaderBg: '#9EC600',
                        text: 'Los datos que intenta ingresar son duplicados.',
                        showHideTransition: 'fade',
                        icon: 'warning'
                    });
        return '';            
    }
    //INSERT ARR CONTROL
    data_control_emp.set_data({empresa_id:empresa_id,sucursal_id:sucursal_id});

    //INSERT TABLE
    let cantItem = $('#listadoEmpresaPersona tr.tabla_filas').length;
			let tabla = `
				<tr id="rowEmp_${cantItem}" class="tabla_filas">
                        <td>
                            <b>${empresa_text}</b>
                            <input type="hidden" value="${empresa_id}"  name="data_empresa[][empresa_id]"></td>
                            <td>
                                <b>${sucursal_txt}</b>
                                <input type="hidden" readonly value="${sucursal_id}" name="data_empresa[][sucursal_id]"></td>
                                        <td>
                                            <button type="button" onclick="deleteTableEmpresaPersona(${cantItem})"  
                                            class="btn btn-danger"><b>Eliminar</b>
                                            </button>
										</td>
									</tr>
                                    `;
                                    
                                    if($('.tbody_empre_per tr td').hasClass('relleno_table')){
										$('.tbody_empre_per').html('');
									}
				
                                $('#listadoEmpresaPersona tbody').append(tabla);

}

function orderTableEmpresaPersona()
{
    //ORDENA LA TABLA AL ELIMINAR O INSERTAR
    let cont = 0;
						$.each($('.tabla_filas'), function (index, value) {
							// console.log($(value.children[2]).find('button').attr('onclick'));
								let num_cant = value.id.split('rowEmp_');
								if (num_cant[1] != cont) {
										$('#' + value.id).attr('id', 'rowEmp_' + cont);
										//MODIFICAR ONCLICK
										$(value.children[2]).find('button').attr('onclick', 'deleteTableEmpresaPersona(' + cont + ')');
								}
								cont++;

						});
}

function deleteTableEmpresaPersona(class_id)
{
    $('#rowEmp_' + class_id).remove();
    orderTableEmpresaPersona();
}


$("#formTipoPersona").change(function () {
    validarCorreo();
    resetCampoCorreosTag();
    $('#tabEmpresaPersona').hide();

    var tipoPersona = $(this).val();
    //////////////////////////////////////////////////////////////////////////////////
    id_pais = $("#formPais option:selected").val();
        tipo_persona = $("#formTipoPersona option:selected").val();
        console.log('INDF '+id_pais);
        if( id_pais == 1455){
            $("#formMigrar").val('false').trigger('change.select2');
        }
    ///////////////////////////////////////////////////////////////////////////////////

    $('#idTipoPersonas').val(tipoPersona);

    $('#cambioTel').html('Telefono ');
    $('#codigoAmadeus-n').html('Codigo Amadeus');   
    $('#nombrePersona').html('Nombre o Razon Social');
    $('#cambio_n').html('Incentivo / Comisión ');  

     accionFormulario(listaPrincipal,[],'0');

    switch (tipoPersona) {

//ADMIN GESTUR
       case '1':
        obtenerEmpresaAgencia('2');
       inputReq.setTipoSucursal('21');

            accionFormulario(['Celular',
                              'Image',
                              'NotificacionCorreo', 
                              'Pais',
                              'Ciudad',
                              'NotificacionCorreo',
                              'TokenNemo'
                              ],
                              ['Nombres',
                              'Apellido',
                              'FechaNacimiento',
                             'TipoFacturacion',
                             'ActivoDtpMundo',
                              'ActivoPersona',
                              'Direccion',
                              'Telefono',
                              'Correo',
                              'Password',
                              'Agencia',
                              'SucursalEmpresa',
                              'TipoDocumento',
                              'DocumentoIdentidad',
                              "RetenerIva"],'1');

         


           break;
      

         //EMPRESA
        case '20':   
        inputReq.setTipoSucursal('');


      accionFormulario(['DenominacionComercial',
                       'Direccion',
                        'Image',
                       'Celular',
                       'CorreoAdministrativo',
                       'CorreoComerciales',
                       'Telefono',
                       'Correo',
                       'TokenNemo'
                    ],
                        ['Nombres',
                         'TipoFacturacion',
                        'FechaNacimiento',
                        'ActivoDtpMundo',
                        'Pais',
                        'Ciudad',
                        'ActivoPersona',
                        'Direccion',
                        'TipoDocumento',
                        'DocumentoIdentidad',
                        "RetenerIva"
                        ],'1');
            break;

        //ADMIN EMPRESA
        case '2':
         obtenerEmpresaAgencia('3');
        inputReq.setTipoSucursal('21');
        //$('#tabEmpresaPersona').show();

             accionFormulario(['Celular',
                              'Image',
                              'NotificacionCorreo',
                              'TokenNemo'
                              ],
                              ['Nombres',
                              'Apellido',
                              'FechaNacimiento',
                              'Pais',
                              'Ciudad',
                              'Direccion',
                             'TipoFacturacion',
                               'ActivoPersona',
                               'ActivoDtpMundo',
                              'Telefono',
                              'Correo',
                              //'SucursalEmpresa',
                              'Password',
                              'Agencia',
                              'TipoDocumento',
                              'DocumentoIdentidad',
                              "RetenerIva"
                              ],'1');

        
            



            break;


        //SUCURSAL EMPRESA
        case '21':
            obtenerEmpresaAgencia('2');
           // inputReq.setTipoSucursal('21');

             accionFormulario(['DenominacionComercial',
                              'Correo',
                              'Image',
                              'Celular',
                              'TokenNemo'
                            ],
                              ['Nombres',
                              'FechaNacimiento',
                              'Pais',
                              'Ciudad',
                              'ActivoPersona',
                              'ActivoDtpMundo',
                              'Direccion',
                              'TipoDocumento',
                              'TipoFacturacion',
                              'DocumentoIdentidad',
                              'Telefono',
                              'Agencia',
                              "RetenerIva"
                              //'SucursalContable'
                              ],'1');
            
            $('#nombrePersona').html('Nombre Sucursal');



            break;


        //VENDEDOR EMPRESA 
        case '3':
        obtenerEmpresaAgencia('2');
        //sucursales del tipo empresa
         inputReq.setTipoSucursal('21');
         //$('#tabEmpresaPersona').show();
         getSucursalEmpresaPersona();
        $('#formAgencia').trigger('change');  

         accionFormulario(['Telefono',
                            'Pais',
                            'Ciudad',
                            'Direccion',
                            'SucursalEmpresa',
                            'TipoDocumento',
                            'DocumentoIdentidad',
                            'FechaNacimiento',
                             'Image',
                            'CodigoAmadeus',
                            'Celular',
                            'TokenNemo'
                            ], 
                            ['Nombres',
                             'Apellido',
                            'ActivoPersona',
                            'ActivoDtpMundo',
                             'Agencia',
                             'TipoFacturacion',
                             'Password',
                             'PuedeReservar',
                             'PuedeReservarEnGastos',
                             'Correo',
                             'Plazo',
                             "RetenerIva"
                             ],'1');

         


            break;

             //ADMINISTRATIVO EMPRESA
        case '5':

         obtenerEmpresaAgencia('2');
         inputReq.setTipoSucursal('21');
         accionFormulario(['Telefono',
                            'Pais',
                            'Ciudad',
                            'Direccion',
                            'TipoDocumento',
                            'DocumentoIdentidad',
                            'Image',
                            'FechaNacimiento',
                            'Celular',
                            'Agencia',
                            'TokenNemo'
                            //'SucursalEmpresa',
                            ], 
                            ['Nombres',
                             'Apellido',
                            'ActivoPersona',
                            'ActivoDtpMundo',
                            'SucursalCarteraId', 
                            //'SucursalEmpresa',
                            // 'Agencia',
                             'TipoFacturacion',
                             'Password',
                             'Correo',
                             'Plazo',
                             "RetenerIva"
                              ],'1');
            break;

            //OPERATIVO EMPRESA
        case '6':
        obtenerEmpresaAgencia('2');
         inputReq.setTipoSucursal('21');


           accionFormulario(['Telefono',
                            'Pais',
                            'Ciudad',
                            'Direccion',
                            'TipoDocumento',
                            'DocumentoIdentidad',
                            'FechaNacimiento',
                            'CodigoAmadeus',
                             'Image',
                            'Celular',
                             'CodigoAmadeus',
                             'TokenNemo'], 
                            ['Nombres',
                             'Apellido',
                            'ActivoPersona',
                            'ActivoDtpMundo',
                             'Agencia',
                             'SucursalEmpresa',
                             'TipoFacturacion',
                             'Password',
                             'Correo',
                             'Plazo',
                             "RetenerIva"
                             ],'1');
            break;

          case '7':
                obtenerEmpresaAgencia('2');
                //sucursales del tipo empresa
                 inputReq.setTipoSucursal('21');

                 accionFormulario(['Telefono',
                                    'Pais',
                                    'Ciudad',
                                    'Direccion',
                                    'SucursalEmpresa',
                                    'TipoDocumento',
                                    'DocumentoIdentidad',
                                    'FechaNacimiento',
                                     'Image',
                                    'CodigoAmadeus',
                                    'Celular',
                                    'TokenNemo'], 
                                    ['Nombres',
                                     'Apellido',
                                    'ActivoPersona',
                                    'ActivoDtpMundo',
                                     'Agencia',
                                     'TipoFacturacion',
                                     'Password',
                                     'Correo',
                                     "RetenerIva"
                                     ],'1');

            break;
        //SUPERVISOR EMPRESA
        case '4':
        obtenerEmpresaAgencia('2');
         inputReq.setTipoSucursal('21');

         accionFormulario(['Apellido',
                            'Telefono',
                            'Pais',
                            'Ciudad',
                            'Direccion',
                            'TipoDocumento',
                           'Image',
                            'DocumentoIdentidad',
                            'FechaNacimiento',
                            'Celular',
                            'TokenNemo'], 
                            ['Nombres',
                            'ActivoPersona',
                            'ActivoDtpMundo',
                             'SucursalEmpresa',
                             'TipoFacturacion',
                             'Agencia',
                             'Password',
                             'Correo',
                             "RetenerIva" ],'1');
            
        


            break;


            //AGENCIA
        case '8':
        inputReq.setTipoSucursal('');
        $('#inputAcuerdoEspecial').show();


         accionFormulario(['Celular',
                           'IvaComisiones',
                           'NotificacionCorreo',
                           'Agente',
                           'Image',
                           'PlazoCobro',
                           'NotificacionCorreo',
                           'Correo',
                           'TokenNemo'
                           ],
                           ['Nombres',
                           'DenominacionComercial',
                           'SucursalCarteraId',
                           'Agente',
                           'ActivoPersona',
                           'PuedeReservar',
                           'PuedeReservarEnGastos',
                           'FacturacionAutomaticaEnGastos',
                           'TipoFacturacion',
                           'TipoPersoneria',
                           'ActivoDtpMundo',
                           'CorreoAdministrativo',
                           'CorreoComerciales',
                           'Pais',
                           'Ciudad',
                           'Direccion',
                           'TipoDocumento',
                           'DocumentoIdentidad',
                           'Telefono',
                           "RetenerIva"
                           /*'Voucher'*/],'1');

          //$('#formAgencia').prop('disabled',true);
 
          $('#cambio_n').html('Markup DTP MUNDO');

          // obtenerEmpresaAgencia('2');
           break;


        //ADMIN AGENCIA
        case '22':
        //obtener agencias
         obtenerEmpresaAgencia('1');
         inputReq.setTipoSucursal('9');

          accionFormulario(['Pais',
                            'Ciudad',
                            'Direccion',
                            'TipoDocumento',
                            'DocumentoIdentidad',
                             'Image',
                            'FechaNacimiento', 
                            'Telefono',
                            'TokenNemo'
                            ], 
                            ['Nombres',
                            'Apellido',
                            'TipoFacturacion',
                            'ActivoPersona',
                            'ActivoDtpMundo',
                            'Correo',
                            'Celular',
                             'Agencia',
                             'SucursalEmpresa',
                             'Password',
                             "RetenerIva"],'1');
            break;
       
       //SUCURSAL AGENCIA
        case '9':
        //obtener agencias
         obtenerEmpresaAgencia('1');
         inputReq.setTipoSucursal('');

         accionFormulario(['CorreoAdministrativo',
                           'Celular',
                           'Image',
                           'Telefono',
                           'TokenNemo'],[
                           'Nombres',
                           'ActivoPersona',
                           'ActivoDtpMundo',
                           'FechaNacimiento',
                           'Pais',
                           'Ciudad',
                           'Direccion',
                           'TipoFacturacion',
                           'Agencia',
                           "RetenerIva"],'1');
                
         $('#nombrePersona').html('Nombre Sucursal');

            break;


        //VENDEDOR AGENCIA 
        case '10':
         obtenerEmpresaAgencia('1');
        //sucursales del tipo empresa
         inputReq.setTipoSucursal('9');

         if(persona.empresa === 1){

            accionFormulario(['Telefono',
                            'Pais',
                            'Ciudad',
                            'Direccion',
                            'FechaNacimiento',
                            'Image',
                            'TokenNemo'], 
                            ['Nombres',
                            'Apellido',
                            'Agencia',
                            'TipoDocumento',
                            'TipoFacturacion',
                            'ActivoPersona',
                            'ActivoDtpMundo',
                            'PuedeReservar',
                            'PuedeReservarEnGastos',
                            'DocumentoIdentidad',
                            'SucursalEmpresa',
                            'Celular',
                            'Agente',
                            'Password',
                            'Correo',
                             'Plazo',
                             "RetenerIva" ],'1');
        } else {
            accionFormulario(['Telefono',
                            'Pais',
                            'Ciudad',
                            'Direccion',
                            'FechaNacimiento',
                            'Image',
                            'TokenNemo'], 
                            ['Nombres',
                            'Apellido',
                            'Agencia',
                            'TipoDocumento',
                            'TipoFacturacion',
                            'ActivoPersona',
                            // 'ActivoDtpMundo',
                            // 'PuedeReservar',
                            // 'PuedeReservarEnGastos',
                            'DocumentoIdentidad',
                            'SucursalEmpresa',
                            'Celular',
                            'Agente',
                            'Password',
                            'Correo',
                            "RetenerIva" ],'1');

        }
       
       
            break;

        //ADMINISTRATIVO AGENCIA
        case '12':
        obtenerEmpresaAgencia('1');
        inputReq.setTipoSucursal('9');


        accionFormulario(['Pais',
                            'Ciudad',
                            'Direccion',
                            'TipoDocumento',
                            'DocumentoIdentidad',
                             'Image',
                            'FechaNacimiento', 
                            'Telefono',
                            'TokenNemo'
                            ], 
                            ['Nombres',
                            'Apellido',
                            'ActivoPersona',
                            'ActivoDtpMundo',
                            'TipoFacturacion',
                            'Correo',
                            'Celular',
                             'Agencia',
                             'SucursalEmpresa',
                             'Password',
                             'Plazo',
                             "RetenerIva"
                             ],'1');
         


            break;





        //PRESTADOR
        case '15':
        inputReq.setTipoSucursal('');

        accionFormulario( ['DenominacionComercial',               
                            'TipoDocumento',
                            'DocumentoIdentidad',
                            'Correo',
                            'CorreoAdministrativo',
                            'Celular',
                            'Pais',
                            'Ciudad', 
                            'Image',
                            'TipoPersoneria',
                            'TokenNemo'
                            ],
                            ['Nombres',   
                             'TipoFacturacion',
                             'ActivoDtpMundo',
                            'ActivoPersona', 
                            'Direccion',
                            'Telefono',
                             'Plazo',
                             "RetenerIva"
                            ],'1');

         $('#cambioTel').html('Teléfono / Teléfono de Eemergencia');

            break;


        //PROVEEDOR
        case '14':
        inputReq.setTipoSucursal('');

         accionFormulario( ['DenominacionComercial',
                            'Celular',
                            'Image',
                            'CorreoAdministrativo',
                            'CorreoComerciales',
                            'CodigoAmadeus',
                             'Plazo',
                             "NroCuenta",
                             "EmisorCheque",
                             'TokenNemo'
                            ],
                            ['Nombres',
                            'Pais', 
                            'Ciudad',   
                            'PlazoPago',
                            'TipoPersoneria',                
                            'TipoDocumento',
                            'DocumentoIdentidad',
                            'TipoFacturacion',
                            'ActivoDtpMundo',
                            'ActivoPersona',
                            'Direccion',
                            'Migrar',
                            'Telefono',
                            'ProveedorOnline',
                            'Correo',
                            'AgenteRetentor',
                             "Banco", 
                            "TipoProveedor",
                            "RetenerIva"
                            ],'1');
            
         $('#tipoProveedor').val(2).trigger('change.select2');
         $('#cambioTel').html('Telefono / Telefono de Emergencia');
         $('#codigoAmadeus-n').html('Codigo Proveedor'); 
            break;

        //PASAJERO
        case '13':
        inputReq.setTipoSucursal('');

         accionFormulario( ['Apellido',
                            'FechaNacimiento',                     
                            'TipoDocumento',
                            'DocumentoIdentidad',
                            'Celular',
                            'Image',
                            'Pais',
                            'Ciudad',
                            'Telefono',
                            'ActivoPersona',
                            'Direccion',
                            'Correo'
                            ],
                            ['Nombres',
                            'ActivoPersona',
                           'ActivoDtpMundo',
                           'TipoFacturacion',
                           "RetenerIva"],'1');
            break;

         //CLIENTE DIRECTO
        case '11':
        inputReq.setTipoSucursal('');

         accionFormulario( ['Apellido',
                            'FechaNacimiento',                     
                            'TipoDocumento',
                            'DocumentoIdentidad',
                            'Celular',
                            'Image',
                            'Pais',
                            'Ciudad',
                            'Telefono',
                            'Direccion',
                            'Correo',
                            'Nombres',
                            'PlazoPago',
                            'Plazo'
                            ],
                            [
                            'ActivoPersona',
                            'ActivoDtpMundo',
                            'TipoFacturacion',
                            "RetenerIva"],'1');


            break;

            //COBRADOR
            case '24':

                    obtenerEmpresaAgencia('2');
                    inputReq.setTipoSucursal('21');

                    accionFormulario(['Telefono',
                                    'Pais',
                                    'Ciudad',
                                    'Direccion',
                                    'TipoDocumento',
                                    'DocumentoIdentidad',
                                    'Image',
                                    'FechaNacimiento',
                                    'Celular'], 
                                    ['Nombres',
                                    'Apellido',
                                    'ActivoPersona',
                                    'ActivoDtpMundo',
                                    'SucursalEmpresa',
                                    'Agencia',
                                    'TipoFacturacion',
                                    'Password',
                                    'Correo',
                                    "RetenerIva" ],'1');
                               break;

        default:
        inputReq.setTipoSucursal('');
          accionFormulario(listaPrincipal,'','0');  

    }



});


validarCorreo();


</script>

@endsection
