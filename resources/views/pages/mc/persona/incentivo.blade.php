
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.card,.card-header {
			border-radius: 14px !important;
		}

	</style>
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Incentivo Vendedor Agencia</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">
				<form id="frmPersonaConsulta">
				<div class="row">

					<div class="col-12 col-sm-4 col-md-3">
						<div class="form-group">
							<label>Agencia</label>
							<select class="form-control select2" tabindex="0" name="idEmpresaAgencia"
								id="idEmpresaAgencia" style="width: 100%;" />
							<option value="">Seleccione Persona</option>
								@foreach($empresaAgencia as $valor)
								<option value="{{$valor->id}}">{{$valor->nombre}} {{$valor->denominacion_comercial}}
								</option>
								@endforeach
							</select>
						</div>
					</div>


					<div class="col-12 col-sm-4 col-md-3">
						<div class="form-group">
							<label>Vendedor Agencia</label>
							<select class="form-control select2" tabindex="1" name="idPersona" id="idPersona"
								style="width: 100%;" />
							<option value="">Seleccione Persona</option>

							@foreach($selectPersona as $persona)
							<option value="{{$persona->id}}">{{$persona->nombre}} {{$persona->apellido}}</option>
							@endforeach

							</select>
						</div>
					</div>

					<div class="col-12 col-sm-4 col-md-3">
						<div class="form-group">
							<label for="formNombres">RUC / CI <span class="formNombres"></span></label>
							<input type="text" class="form-control" tabindex="4" name="inputCi" id="inputCi" value=""
								maxlength="100" />
						</div>
					</div>

					<div class="col-12 col-sm-4 col-md-3">
						<div class="form-group">
							<label for="formNombres">Correo<span class="formNombres"></span></label>
							<input type="text" class="form-control" tabindex="5" name="inputEmail" id="inputEmail"
								value="" maxlength="100" />
						</div>
					</div>


						<div class="col-12">
							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
							<button type="button" onclick="buscarPersona()" id="btnGuardar" class="btn btn-info btn-lg pull-right mr-1"><b>Buscar</b></button>
						</div>

			</div>
		</form>


			<div class="table-responsive table-bordered">
				<table id="listado" class="table" style="width: 100%;">
					<thead>
						<tr>
							<th>Empresa / Agencia</th>
							<th>Nombre/Razon Social</th>
							<th>Correo</th>
							<th>Documento/RUC</th>
							<th>Incentivo</th>
							<th></th>
						</tr>
					</thead>


					<tbody style="text-align: center">
					</tbody>
				</table>
			</div>




		</div>
		</div>
	</section>
</section>


				<!-- ========================================================= 
											MODAL
					=========================================================-->

      <div id="requestIncentivo" class="modal fade" role="dialog">
      	<div class="modal-dialog">
      		<!-- Modal content-->
      		<div class="modal-content">
      			<div class="modal-header">
      				<h2 class="modal-title titlepage" style="font-size: x-large;">Modificar Incentivo <i
      						style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>

      				<span id="mensaje_cambio" style="font-weight: bold; height: 18px;"></span>
      			</div>
      			<div class="modal-body">
      				<div class="container-fluid">
						  <div class="row">
      						<div class="col-sm-6">
      							<div class="form-group">
      								<label for="formUsuario">Nombre</label>
      								<input type="text" class="form-control" name="" id="vend-nombre" value=""
      									readonly />
      							</div>
      						</div>

      						<div class="col-sm-6">
      							<div class="form-group">
      								<label for="formUsuario">Apellido </label>
      								<input type="text" class="form-control" name="" id="vend-apellido" value=""
      									readonly />
      							</div>
      						</div>

      						<div class="col-sm-6">
      							<div class="form-group">
      								<label>Modificar Comision</label>
      								<select class="form-control select2" name="selectComision" id="selectComision"
      									style="width: 100%;">
      									<option value="0">0</option>
      									<option value="1">1</option>
      									<option value="2">2</option>
										<option value="3">3</option>
      									<option value="4">4</option>
      									<option value="5">5</option>
										<option value="6">6</option>
      									<option value="7">7</option>
      									<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="10.5">10,5</option>
										<option value="11">11</option>
										<option value="11.5">11,5</option>
										<option value="12">12</option>
										<option value="12.5">12,5</option>
      									<option value="13">13</option>
      									<option value="13.5">13,5</option>
      									<option value="14">14</option>
      									<option value="14.5">14,5</option>
										<option value="15">15</option>
      								</select>
      							</div>
      						</div>
									
							 <div class="col-sm-12">
								<button type="button" id="cambiarIncentivo" class="btn btn-success">Cambiar</button>
      							<input type="hidden" id="id_persona" value="">	 
							</div> 
						</div>	  
      				</div>

      				

      			</div>
      			<div class="modal-footer">
      				<button type="button" id="" class="btn btn-danger btn-lg" data-dismiss="modal">Salir</button>
      			</div>
      		</div>
      	</div>
      </div>







@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	
	<script>

	$(document).ready(function() {
		$('.select2').select2({
				tag:true
			});

			buscarPersona();
	
		});




	
	function limpiar(){
		$('#idEmpresaAgencia').val('').trigger('change.select2');
		$('#idPersona').val('').trigger('change.select2');
		$('#inputCi').val('');
		$('#inputEmail').val('');
		// buscarPersona();
	}



			function buscarPersona(){
			$("#listado").dataTable({
				"searching": false,
				"processing": true,
				"serverSide": true,
				destroy: true,

				"ajax": {
				"url": "{{route('incentivoVendedor')}}",
			    "data": {
			        "formSearch": $('#frmPersonaConsulta').serializeArray() },
					error: function (jqXHR, textStatus, errorThrown) {
						$.toast({
                          heading: 'Error',
                          text: 'Ocurrio un error en la comunicación con el servidor.',
                          showHideTransition: 'fade',
                          position: 'top-right',
                          icon: 'error'
                        });  
			 		}
			    }

				});
		}//function





		  $('#idEmpresaAgencia').change(function(){
    		
		  	var idAgencia = {'idAgencia' : $('#idEmpresaAgencia').val()};

  				$.ajax({
                        type: "GET",
                        url: "{{route('getDatosVendedores')}}",
                        dataType: 'json',
                        data: idAgencia,

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
                        success: function(rsp){

                        	// console.log(rsp);

                            if(rsp.resp.length != 0 ){

                            	//validar si es null
                            	 function n(n){
                                if(n === null){ return '';  
                            	} 
                            	return n;
                                }

                                 function t(n){
                                if(n === null){ return '';  
                            	} 
                            	return '- '+n;
                                }

                                // console.log('dd');
                              $('#idPersona').empty();

                               var newOption = new Option('Seleccione Persona', '', false, false);
                                $('#idPersona').append(newOption);

                                $.each(rsp.resp, function (key, item){


                                var newOption = new Option(n(item.nombre)+' '+n(item.apellido), item.id, false, false);
                                $('#idPersona').append(newOption);
                            })

                            } else{ 

                            $('#idPersona').empty();
                            var newOption = new Option('Seleccione Persona' ,'', false, false);
                                $('#idPersona').append(newOption);   
                            // $('#idPersona').select2({ disabled: true });

                                    }//else
                          
                                }//funcion  
                });



    
  });



		 

		 



		  function modalEdit(e){
		  	$('#id_persona').val(e);

		  	$('#mensaje_cambio').css('color','blue');
			$('#mensaje_cambio').html('Recuperando comisión...');
			$('.cargandoImg').show();

			$('#selectComision').prop('disabled',true);
			$('#cambiarIncentivo').prop('disabled',true);
		  	$("#requestIncentivo").modal("show");

		  	var dataString = {idPersona : e};
		  	$.ajax({
                        type: "GET",
                        url: "{{route('obtenerIncentivo')}}",
                        dataType: 'json',
                        data: dataString,

                       error: function(jqXHR,textStatus,errorThrown){

                         	$('#mensaje_cambio').css('color','red');
						  	$('#mensaje_cambio').html('Ocurrio un error en la comuniación con el servidor');
						  	$('.cargandoImg').hide();

                        },
                        success: function(rsp){
                        	$('#mensaje_cambio').html('');
                        	$('#selectComision').prop('disabled',false);
							$('#cambiarIncentivo').prop('disabled',false);
						  	$('.cargandoImg').hide();

						  	var val = 0;

						  	$('#vend-apellido').val((rsp.incentivo.apellido != 'null') ? rsp.incentivo.apellido : '' );
						  	$('#vend-nombre').val((rsp.incentivo.nombre != 'null') ? rsp.incentivo.nombre : '' );

						  	

                        	if(rsp.incentivo.comision_pactada != 'null'){
                        	 	var val = parseFloat(rsp.incentivo.comision_pactada);
         						    val = String(val);
                        	 } 

                        	 $("#selectComision").val(val).trigger('change.select2');
                          
                                }//funcion  
                });//ajax



		  	
		
		  }




		$('#cambiarIncentivo').on('click', function(){


			$('#mensaje_cambio').css('color','blue');
			$('#mensaje_cambio').html('Procesando...');
			$('.cargandoImg').show();

			var dataString = { idPersona : $('#id_persona').val(), newIncentivo : $('#selectComision').val() };
		

			$.ajax({
                        type: "GET",
                        url: "{{route('modificarIncentivo')}}",
                        dataType: 'json',
                        data: dataString,

                       error: function(jqXHR,textStatus,errorThrown){

                            $('#mensaje_cambio').css('color','red');
						  	$('#mensaje_cambio').html('Ocurrio un error en la comuniación con el servidor');
						  	$('.cargandoImg').hide();

                        },
                        success: function(rsp){

                        	if(rsp.resp == true){
                        	$('#mensaje_cambio').css('color','green');
						  	$('#mensaje_cambio').html('La operación fue realizada con exito.');
						  	$('.cargandoImg').hide();

                        	} else {

                            $('#mensaje_cambio').css('color','red');
						  	$('#mensaje_cambio').html('Ocurrio un error al guardar.');
						  	$('.cargandoImg').hide();
                        	}

                          
                          
                                }//funcion  
                });//ajax


		});//ajax			
						
		

		
				
			

	</script>
@endsection