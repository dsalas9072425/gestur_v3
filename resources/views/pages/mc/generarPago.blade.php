@extends('masters')

@section('title', 'Gestión de Pagos')
@section('styles')
	@parent
@endsection
@section('content')
<div style="margin-left: 1%; margin-right: 2%; background-color: white">
    @include('flash::message')
   <br>
    <h1>Gestión de Pagos</h1>
    <br>
    <div class="block sectiontop">
    	<form id="frmPago">
    		<div id="warnigPagos" class="bg-red" style="background-color:#dd4b39 !important;"></div>
    		<br>
		    <table id="pagos" class="table table-bordered table-hover">
		    	<thead>
		    		<tr>	
		    			<th>Tipo de Pago</th>
		    			<th>N° Comprobante</th>
		    			<th>Banco</th>
		    			<th>Importe</th>
		    			<th>Moneda</th>
		    			<th></th>
		    		</tr>
		    	</thead>
		    	<tbody id="cuentas">
		    		<tr>	
		    			<td>
		    				<select class="form-control select2" id="paymentmethod_id" required style="width: 100%;" name="paymentmethod_id">
								<option value="0">Seleccione Tipo de Pago</option>
								@foreach($paymentmethod as $key=>$paymentmet)
										<option value="{{$paymentmet['paymentmethod_id']}}">{{$paymentmet['paymentmethod_description']}}</option>
								@endforeach
							</select>
						</td>
		    			<td><input type="text" name="n_comprobante" class="form-control" required id="n_comprobante"></td>
		    			<td>
		    				<select class="form-control select2" id="banco_id" required style="width: 100%;" name="banco_id">
								<option value="0">Seleccione Banco</option>
								@foreach($banco as $key=>$banc)
									<option value="{{$banc['id']}}">{{$banc['nombre']}}</option>
								@endforeach
							</select>
		    			</td>
		    			<td><input type="text" name="importe" class="form-control" required id="importe"></td>
		    			<td>
		    				<select class="form-control select2" id="currency_id" required style="width: 100%;" name="currency_id">
								<option value="0">Seleccione Moneda</option>
								@foreach($moneda as $key=>$currency)
									<option value="{{$currency['currency_id']}}">{{$currency['currency_code']}}</option>
								@endforeach
							</select>
		    			</td>
		    			<td><button type="button" style="width: 140px;  background-color: #e2076a; height: 35px;" id= "doGenerarPagos" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i>Registrar</button></td>
		    		</tr>
		    	</tbody>
		    </table>
		    <br>
		    <br>
		</form>    
    </div>
</div>    

@endsection

@section('scripts')
    @parent
    <script>
		$(document).ready(function() {
			$("#doGenerarPagos").click(function(){
				var dataString = $("#frmPago").serialize();
				var indicador = 0;
				$('.form-control').each(function() {
					if($(this).prop('value')== 0 ||$(this).prop('value')==""){
						console.log($(this).prop('value'));
						$(this).css( "border-color", "red" );
						indicador = 1;
					}
				})

				if(indicador == 0){	
					$("#warnigPagos").html('');
					$.ajax({
						type: "GET",
						url: "{{route('mc.doGuardarPago')}}",
						dataType: 'json',
						data: dataString,
						success: function(rsp){
								if(rsp == 0){
									msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i>Pagos</h2><p>Se ha guardado exitosamente.</p><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
		                			mostrarMensaje(msg);
									window.location.replace("{{route('mc.indexPagoMc')}}");
								}else{
					                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i>Pagos</h2><p>Favor vuelva a intentar nuevamente</p><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
					                mostrarMensaje(msg);
								}	
								/*var fila = "";
								fila += "<tr>";
								fila += "<td id=>"+$('select[name="paymentmethod_id"] option:selected').text()+"</td><td>"+$('#n_comprobante').val()+"</td><td>"+$('select[name="banco_id"] option:selected').text()+"</td><td>"+$('#importe').val()+"</td><td>"+$('select[name="currency_id"] option:selected').text()+"</td><td></td>";
								fila += "<tr>";
								$("#cuentas").append(fila);*/
						}		
					})
				}else{
					$("#warnigPagos").html('<b>INGRESE LOS DATOS SOLICITADOS<b/>');
				}		
			})	
		})


				$("#importe").change(function() {
						$("#importe").val(number_format($(this).prop('value'), 0));
				})	



			function number_format(amount, decimals) {

			    amount += ''; // por si pasan un numero en vez de un string
			    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

			    decimals = decimals || 0; // por si la variable no fue fue pasada

			    // si no es un numero o es igual a cero retorno el mismo cero
			    if (isNaN(amount) || amount === 0) 
			        return parseFloat(0).toFixed(decimals);

			    // si es mayor o menor que cero retorno el valor formateado como numero
			    amount = '' + amount.toFixed(decimals);

			    var amount_parts = amount.split(','),
			        regexp = /(\d+)(\d{3})/;

			    while (regexp.test(amount_parts[0]))
			        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

			    return amount_parts.join(',');
			}



	</script>		
@endsection
