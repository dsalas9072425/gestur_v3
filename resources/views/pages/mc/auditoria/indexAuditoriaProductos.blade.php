	@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
				border-radius: 14px !important;
			}
	</style>
@endsection
@section('content')

@include('flash::message') 

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Auditoría Productos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

		<form id="formProducto" autocomplete="off"> 	
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body"> 

					<div class="row">
							
							<div class="col-12 col-sm-3 col-md-4">
								<div class="form-group">
									<label>Productos</label>
									<select class="form-control select2" name="id_producto" placeholder="Seleccione Persona" id="id_producto" style="width: 100%;">
										<option value="">TODOS</option>
									</select>
								</div>
							</div>

							<div class="col-12 col-sm-3 col-md-4">
								<div class="form-group">
									<label>Evento</label>					            	
									<select class="form-control input-sm select2" name="evento" id="evento" style="width: 100%;">
										<option value="" selected>Seleccione una opción</option>			
										<option value="UPDATE">UPDATE</option>	
										<option value="DELETE">DELETE</option>					
									</select>
								</div>
							</div> 

							<div class="col-12 col-sm-3 col-md-4">
								<div class="form-group">
									<label>Fecha</label>						 
									<div class="input-group">
									    <div class="input-group-prepend">
									    	<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									    </div>
									    <input type="text" class="form-control pull-right fecha" name="periodo" id="periodo" value="">
									</div>
								</div>							
							</div>



						</div>
										
						<div class="row">
							<div class="col-12 pb-1">
								<button type="button" id="btnLimpiar" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>	
								<button type="button" onclick="consultaPersona()" class="btn btn-info btn-lg pull-right mr-1"><b>Buscar</b></button>
							</div>
						</div>	
			
		 
				

            	<div class="table-responsive">
	              <table id="listado" class="table" style="width: 100%;">
	                <thead style="text-align: center">
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>Tipo</th>
							<th>LC Auto</th>
							<th>Voucher</th>
							<th>Incentivo</th>
							<th>Comisionable</th>
							<th>Imprimir Fact</th>
							<th>Visible</th>
							<th>Activo</th>
							<th>Usuario Auditoría</th>
							<th>Fecha Hora Auditoría</th>
							<th>Evento</th>
			            </tr>
	                </thead>
	                
	                <tbody style="text-align: center">
	            
			        </tbody>
	              </table>
				</div>  
				


        
			</div>
		</div>
	</form>
	</section>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>
		$(document).ready(function() {
			$('.select2').select2();
			calendar();

			$("#id_producto").select2({
		        ajax: {
		                url: "{{route('getProductoListado')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                processResults: function (data, params){
		                            var results = $.map(data, function (value, key) {
		                            console.log(value);
		                           /* return {
		                                    children: $.map(value, function (v) {*/ 
		                                        return {
		                                                    id: value.id,
		                                                    text: value.text
		                                                };
		                                      /*  })
		                                    };*/
		                            });
		                                    return {
		                                        results: results,
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
		    });
	});

	function calendar(){
		

					//CALENDARIO OPCIONES EN ESPAÑOL 
					let locale = {
						format: 'DD/MM/YYYY',
						cancelLabel: 'Limpiar',
						applyLabel: 'Aplicar',					
							fromLabel: 'Desde',
							toLabel: 'Hasta',
							customRangeLabel: 'Seleccionar rango',
							daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
							monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
													'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
													'Diciembre']
						};

			$('input[name="periodo"]').daterangepicker({
				timePicker24Hour: true,
				timePickerIncrement: 30,
				locale: locale
													 
			}).on('cancel.daterangepicker', function(ev, picker) {
						$(this).val('');});
	}

	$("#btnLimpiar").click(function(){
				$('#usuario').val('').trigger('change.select2');
				$('#fecha').val('').trigger('change.select2');
				$('#evento').val('').trigger('change.select2');
			});





	// $("#listado").dataTable({
	// 			 "aaSorting":[[0,"desc"]]
	// 			});


	function consultaPersona() 
	   {

				var btn;
				var pasajero;
				var avion;

				$.blockUI({
												centerY: 0,
												message: "<h2>Procesando...</h2>",
												css: {
													color: '#000'
												}
											});

					// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
				setTimeout(function ()
				{

					//Destruir dataTable	
					// $("#listado").dataTable().fnDestroy();	

					$("#listado").dataTable({
						"searching": false,
						"processing": true,
						destroy:true,
						
						"ajax": {
						"url": "{{route('getAuditoriaProductos')}}",
						"data": $('#formProducto').serializeJSON() },
							

						"columns":[

									{data: "id_producto"},
									{data: "denominacion"},
									{data: "tipo_producto_txt"},
									{data: "lc_txt"},
									{data: "genera_voucher_txt"},
									{data: (x) => { return ''; }},
									{data: "es_comisionable_txt"},
									{data: "imprimir_en_factura_txt"},
									{data: "visible_txt"},
									{data: "estado"},
									{data: "usuario_email"},
									{data: "fecha_hora_auditoria_format"},
									{data: "evento"},
								
								],
							"aaSorting":[],	   

						}).on('xhr.dt', function ( e, settings, json, xhr ){
										//Ajax event - fired when an Ajax request is completed.;
									$.unblockUI();
										});


				}, 300);


					// $.unblockUI();

	
		}












	
	</script>
@endsection