@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
	@include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Agregar Timbrado</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
            	<form id="frmProforma" method="post" action="{{route('numeracion_recibo.store')}}">
	            	<div class="row"  style="margin-bottom: 1px;">

						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Establecimiento</label>
								<input type="text" required class = "form-control" name="Establecimiento" id="establecimiento" placeholder="Establecimiento" style="text-transform: uppercase" required>
							</div>
						</div>	
						<div class="col-12 col-sm-3 col-md-3" >
							<div class="form-group">
								<label class="control-label">Expedicion</label>
								<input type="text" required class = "form-control" name="expedicion" id="expedicion" placeholder="Expedicion" style="text-transform: uppercase" required>
							</div>
						</div>


						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Sucursal</label>					            	
								<select class="form-control input-sm select2" name="sucursal" id="sucursal" style=" padding-left: 0px;width: 100%;" required>
									<option value=" ">Seleccione Sucursal</option>
									@foreach($sucursalEmpresas as $sucursalEmpresa)
										<option value="{{$sucursalEmpresa->id}}">{{$sucursalEmpresa->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Centro de Costo</label>					            	
								<select class="form-control input-sm select2" name="centroCosto" id="centroCosto" style=" padding-left: 0px;width: 100%;" required>
									<option value=" ">Seleccione estado</option>
									@foreach($centroCostos as $centroCosto)
										<option value="{{$centroCosto->id}}">{{$centroCosto->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>
			
					</div>

				</form>	
				<div class="col-12">
					<button type="submit" form="frmProforma" id="btnGuardar" class="btn btn-success btn-lg pull-right mb-1" tabindex="17"><b>GUARDAR</b></button>
				</div>
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

	<script>
		var datepickers = $('#inici'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		var datepickers = $('#vencimient'); 
			    if (datepickers.length > 0) { 
			    datepickers.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		var datepicker = $('#fecha_autorizacion'); 
			    if (datepicker.length > 0) { 
			    datepicker.datepicker({ 
			     format: "dd/mm/yyyy", 
			    // startDate: new Date() 
			    }); 
			    } 

		$('#inicio').click(function(){
			$('input[name="inicio"]').trigger('click');	
		}) 	
		$('#vencimiento').click(function(){
			$('input[name="vencimiento"]').trigger('click');	
		}) 	
		$('#fecha_autorizacion').click(function(){
			$('input[name="fecha_autorizacion"]').trigger('click');	
		}) 	


		$('.select2').select2();
			
	</script>
@endsection