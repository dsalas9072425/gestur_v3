@extends('masters')
@section('title', 'Numeración de Recibo')
@section('styles')
  @parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
  @include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Numeración de Recibo</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
          <br>
          <div class="ard-body pt-0" style="margin-right: 45px;">
            <a href="{{route('numeracion_recibo.create')}}" title="Agregar Timbrado" class="btn btn-success pull-right"	role="button">
              <div class="fonticon-wrap">
                <i class="ft-plus-circle"></i>
              </div>
            </a>
          </div>		        
          <br>
          <br>
            <div class="card-body"> 
                <table id="listado" class="table">
                  <thead>
                    <tr>
                      <th>Establecimiento</th>
                      <th>Expedicion</th>
                      <th>Numero Actual</th>
                      <th>Sucursal</th>
                      <th>Centro<br>Costo</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
            </div> 
        </div> 
    </div>        
</section>


@endsection
@section('scripts')
  @include('layouts/gestion/scripts')
  <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

  <script>
    $('#listado').dataTable();



        $.ajax({
          type: "GET",
          url: "{{route('numeracion_recibo.ajax.index')}}",
          dataType: 'json',
         // data: dataString,
          success: function(rsp){
            var oSettings = $("#listado").dataTable().fnSettings();
            var iTotalRecords = oSettings.fnRecordsTotal();
            for (i=0;i<=iTotalRecords;i++) {
              $("#listado").dataTable().fnDeleteRow(0,null,true);
            }
            $.each(rsp, function (key, item){
                var iconoEditar ='<a href="editTimbrado/'+item.id+'" class="btn btn-success" style="padding-left: 6px;padding-right: 6px;margin-right: 2px;" role="button"><i class="fa fa-fw fa-edit"></i></a>';
                var iconoEditar ='';



                var dataTableRow = [

                          item.establecimiento,
                          item.expedicion,
                          item.numeracion,
                          item.sucursal.nombre,
                          item.centro_costo.nombre,
                          iconoEditar
                        ];
                var newrow = $("#listado").dataTable().fnAddData(dataTableRow);

                // set class attribute for the newly added row 
                var nTr = $("#listado").dataTable().fnSettings().aoData[newrow[0]].nTr;

            })
          }
        })    


      
  </script>
@endsection
