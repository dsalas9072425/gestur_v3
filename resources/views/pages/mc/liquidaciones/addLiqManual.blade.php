@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
	border-radius: 14px !important;
	}
	</style>
	
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Agregar Liquidacion</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">
				<form id="frmCotizacion">
					<div class="row">
                        <div class="col-md-6">
							<input type="hidden" class = "Requerido form-control input-sm text-bold" id="fecha" value="{{date('d/m/Y')}}"/>

							<div class="form-group">
								<label>Periodo</label>					            	
								<select class="form-control input-sm select2"   name="periodo" id="periodo" style="width:100%;">
									@foreach($listadoFechas as $key=>$listadoFecha)
										<option value="{{$listadoFecha}}">{{$listadoFecha}}</option>
									@endforeach	
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Factura</label>					            	
								<select class="form-control input-sm select2" name="nro_factura" id="nro_factura" style="width:100%;">
									@foreach($facturas as $key=>$factura)
										<option value="{{$factura->id_factura}}">{{$factura->nro_factura}} / {{$factura->usuario_nombre}} </option>
									@endforeach	
								</select>
							</div>
						</div>
                    </div>
					<div class="row">
						<div class="col-12 mb-2">
							<button type="button" onclick="agregarLiquidacion()" id="btnAgregar" class="btn btn-success btn-lg pull-right mr-1">Agregar</button>
							</div>
                    </div>				
				</form>
				<form id="frmLiquidacion">
					<div class="row">
						<div class="col-12 mb-2">
							<div class="table-responsive table-bordered">
								<table id="listado" class="table">
									<thead>
										<tr style="text-align: center;">
											<th>Fecha</th>
											<th>Periodo</th>
											<th>Factura</th>
											<th></th>
										</tr>
									</thead>
									<tbody style="text-align: center">
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 mb-2">
							<a href="{{ route('cotizacionIndex') }}" type="button" class="btn btn-danger btn-lg pull-right mr-1">Volver</a>
							<button type="button" onclick="guardarLiquidacion()" id="btnGuardar" class="btn btn-success btn-lg pull-right mr-1">Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>

	<script>
	$(document).ready(function(){
		$('.select2').select2();
    });     

	function agregarLiquidacion(){

			cantidad =  $("#listado tbody tr").length;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			var tbody = $('#listado tbody');
			fila_contenido = '';
			fila_contenido += '<tr id="fila_'+cantidad+'">';
			fila_contenido += '<th><input type="text" class="Requerido form-control indicador baseDatos" name="datos[' + cantidad + '][fecha]" id="fecha_' + cantidad + '" value="' + $('#fecha').val() + '" disabled/></th>';
			fila_contenido += '<th><input type="text" class="Requerido form-control baseDatos" name="datos[' + cantidad + '][periodo]" id="periodo_' + cantidad + '" value="' + $('#periodo').val() + '" disabled/>',
			fila_contenido += '<th><input type="hidden" class="Requerido form-control baseDatos" name="datos[' + cantidad + '][id_factura]" id="factura_' + cantidad + '" value="' + $('#nro_factura').val() + '"/><input type="text" class="Requerido form-control baseDatos" name="datos[' + cantidad + '][nro_factura]" id="factura_' + cantidad + '" value="' + $('#nro_factura :selected').text() + '" disabled/></th>';
			fila_contenido += '<th><button type="button" onclick="eliminarLiquidacion('+ cantidad +')" id="btnGuardar" class="btn btn-danger btn-lg mr-1">X</button></th>';
			fila_contenido += '</tr>';
			tbody.append(fila_contenido);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

	function guardarLiquidacion(){
			$('.baseDatos').prop("disabled", false);
			var dataString = $('#frmLiquidacion').serialize();
		       $.ajax({
						type: "GET",
						url: "{{route('doAddLiqMan')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },
						success: function(rsp){
							if(rsp.status == 'OK'){
								$.toast({
									    heading: 'Success',
									    text: 'Los datos fueron almacenados.',
									    position: 'top-right',
									    showHideTransition: 'slide',
				    					icon: 'success'
									});
									window.location.replace("{{route('verLiquidaciones')}}");
							} else {
								$.toast({
										heading: 'Error',
										text: 'Los datos no fueron almacenados.',
										position: 'top-right',
										showHideTransition: 'fade',
										icon: 'error'
                                });
							
							}//else
						}//funcion	
				});				
		}


		function eliminarLiquidacion(id){
			$('#fila_'+id).remove();		
		}

	</script>
@endsection