@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Liquidar Comisiones</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
				    <form  method="post"  action="{{route('generarLiquidaciones')}}">
						<div class="row">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
									<div class="form-group">
										<label>Mes</label>					            	
										<select class="form-control input-sm select2" name="periodo_mes" id="periodo_mes" style="padding-left: 0px;width: 100%;">
											<option value="">Seleccione Mes</option>
											@if(!empty($mes))
												@foreach($mes as $key=>$mess)
													<option value="{{$key}}">{{$mess}}</option>
												@endforeach	
												@endif
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 15px; padding-top: 10px;">
									<div class="form-group">
										<label>Año</label>					            	
										<select class="form-control input-sm select2"  name="periodo_anho" id="periodo_anho" style="padding-left: 0px;width: 100%;">
											<option value="">Seleccione Año</option>
											<option value="2020">2020</option>
											<option value="2021">2021</option>
											<option value="2022">2022</option>
											<option value="2023">2023</option>
											<option value="2024">2024</option>
											<option value="2025">2025</option>
										</select>
									</div>
								</div> 

						    	<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 25px;">
						    		<div class="col-md-6">
										<br>
										<button type="submit" class="btn btn-success btn-lg" style="margin-top: 10px;">Liquidar</button>
									</div>
								</div>
						</div> 
        
            	</form>	
            </div>	
        </div>    
    </div>   
</section>



    <!-- /.content -->

    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')	
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script>
		$(document).ready(function() {
			$('.select2').select2();

			var Fecha = new Date();
			var mes = (Fecha.getMonth() +1) - 1;
			console.log(mes);
			if(mes < 10){
				mes = '0'+mes;
			}else{
				mes = mes;
			}

			$('#periodo_mes').val(mes).trigger('change.select2');
			$('#periodo_anho').val(Fecha.getFullYear()).trigger('change.select2');

			$("#listado").dataTable({
				 	"aaSorting":[[0,"desc"]]
			});

		});

		
	</script>
	

@endsection
