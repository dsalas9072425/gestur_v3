@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')

<style type="text/css">
	
	.btnExcel {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 1.42857143;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;

}

.btnExcel {
	padding: 10px 16px;
	font-size: 18px;
	line-height: 1.3333333;
	border-radius: 6px;
}

.btnExcel {
	color: #fff;
	background-color: #5bc0de;
	border-color: #46b8da;
}

.btnExcel {
	margin: 0 0 10px 10px;
}

#botonExcelCabecera {
	display: inline;
}

#botonExcelCabecera .dt-buttons {
	display: inline;
}

/*sdsdds*/
.checkbox label:after {
  content: '';
  display: table;
  clear: both;
}

.checkbox .cr {
  position: relative;
  display: inline-block;
  border: 1px solid #a9a9a9;
  border-radius: .25em;
  width: 1.3em;
  height: 1.3em;
  float: left;
  margin-right: .5em;
}

.checkbox .cr .cr-icon {
  position: absolute;
  font-size: .8em;
  line-height: 0;
  top: 50%;
  left: 15%;
}

.checkbox label input[type="checkbox"] {
  display: none;
}

.checkbox label input[type="checkbox"]+.cr>.cr-icon {
  opacity: 0;
}

.checkbox label input[type="checkbox"]:checked+.cr>.cr-icon {
  opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled+.cr {
  opacity: .5;
}

	.readOnly {
	    background-color: #eee;
	 	border-color: rgb(210, 214, 222);
	 	cursor: not-allowed !important;
	}

	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}

	.rojo{
		color: red;
		font-weight: 700;
	}

	.verde{
		color: green;
		font-weight: 700;
	}
	.bgRed {
	font-size: 15px;
	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
	}	

</style>
<section id="base-style">
   <div class="card-content">
   		<div class="card" style="border-radius: 14px;">
		    <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Reporte de Liquidaciones</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
		            </ul>
		        </div>
		    </div>
		    <div class="card-content collapse show" aria-expanded="true">
		        <div class="card-body rounded-bottom rounded-lg" style="padding-top: 0px;">
			        <div class="card-body">

			            <ul class="nav nav-tabs nav-underline" role="tablist">
			                <li class="nav-item">
			                    <a class="nav-link active" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#cabecera" role="tab" aria-selected="true"><i class="fa fa-play"></i> Reporte Liquidaciones</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#detalle" role="tab" aria-selected="false"><i class="fa fa-flag"></i> Reporte Liquidaciones Detalle</a>
			                </li>
						</ul>
						
			            <div class="tab-content">

			                <div class="tab-pane active mt-1" id="cabecera" role="tabpanel">
			               
			                	<form id="verLiquidaciones" autocomplete="off">
			                		<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label>Vendedor</label>	
												<select class="form-control input-sm select2" name="vendedor" id="vendedor" style="width:100%;">
													@if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == 2)
														<option value="1">Todos</option>
													@endif
														@foreach($vendedores as $key=>$vendedor)
															<option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
														@endforeach
												</select>
											</div>
										</div> 
										<div class="col-md-3">
											<div class="form-group">
												<label>Periodo</label>					            	
												<select class="form-control input-sm select2"   name="periodo" id="periodo" style="width:100%;">
													@foreach($listadoFechas as $key=>$listadoFecha)
														<option value="{{$listadoFecha}}">{{$listadoFecha}}</option>
													@endforeach	
												</select>
											</div>
										</div>
			                		</div>	
								</form>
								
								<div class="row">
									<div class="col-12">
										<div class="pull-right mr-1" id="botonExcelCabecera" ></div>
										<button type="button" onclick="limpiarFiltros()" id="btnLimpiar" class="pull-right btn  btn-light mr-1 btn-lg font-weight-bold text-white">Limpiar</button>	
										<a  onclick ="consultarLiquidaciones()" class="pull-right  btn btn-info mr-1 btn-lg font-weight-bold text-white" role="button">Buscar</a>

									</div>
								</div>
									  
							<div class="table-responsive table-bordered">		
								<table id="listado" class="table" style="width: 100%, font-size: 0.9rem;">
			                        <thead>
										<tr>
											<th>ID</th>
											<th>Periodo Liquidación</th>
											<th>Fecha Liquidación</th>
											<th>Vendedor</th>
											<th>Renta Total</th>
											<th>Comison PYG</th>
											<th>Comision USD</th>
											<th>Comision EUR</th>
											<th>Ver</th>
										</tr>
			                        </thead>
			                        <tbody>
			                        </tbody>
								</table>
							</div>
						</div>
			                <div class="tab-pane" id="detalle" role="tabpanel">

			                	<form id="verLiquidacionesDetalle" class="mt-1">
									<input type="hidden" class = "Requerido form-control" id="vendedorId" name="vendedorId"/>
									<div class="row">
										<input type="hidden" class="form-control" id="renta_totals"  name="renta_total" value="0" disabled="disabled">
										<input type="hidden" class = "Requerido form-control" id="id_liquidacion" name="id_liquidacion"/>
										<div class="col-md-3">
											<div class="form-group">
												<label>Vendedor</label>	
												<select class="form-control input-sm select2" name="vendedorDetalle" id="vendedorDetalle">
													@foreach($vendedores as $key=>$vendedor)
														<option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
													@endforeach
												</select>
											</div>
										</div> 
										<div class="col-md-3">
											<div class="form-group">
												<label>Periodo</label>					            	
												<select class="form-control input-sm select2"   name="periodoDetalle" id="periodoDetalle">
													@foreach($listadoFechas as $key=>$listadoFecha)
														<option value="{{$listadoFecha}}">{{$listadoFecha}}</option>
													@endforeach	
												</select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Renta Total</label>
								     			<input type="text" class="form-control" id="renta_total"  name="renta_total" value="0" disabled="disabled">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Porcentaje Comision</label>
							     				<input type="text" class="form-control" id="porcentaje_comision"  name="porcentaje_comision" value="0" disabled="disabled">
											</div>
										</div>
									</div> 	
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label>Comison Total PYG</label>
							     				<input type="text" class="form-control" id="comision_total_gs"  name="comision_total_gs" value="0" disabled="disabled">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Comision Total USD</label>
							     				<input type="text" class="form-control" id="comision_total_usd"  name="comision_total_usd" value="0" disabled="disabled">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label>Comision Total EUR</label>
							     				<input type="text" class="form-control" id="comision_total_eur"  name="comision_total_eur" value="0" disabled="disabled">
											</div>
										</div>
				               		</form>	
									<div class="col-md-3">
										<br>
										<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1"><b>Excel</b></button>
										<button type="button" onclick="limpiarFiltrosDetalle()" id="btnLimpiar" class="pull-right btn btn-light btn-lg mr-1 text-white"><b>Limpiar</b></button>	
										<button  onclick ="consultarDetalleLiquidaciones()" class="pull-right  btn btn-info btn-lg  mr-1" type="button"><b>Buscar</b></button>
									</div>
								</div>		
								<div class="table-responsive table-bordered">	
								<table id="listadoDetalle" class="table" style="width: 100%">
					                <thead style="text-align: center">
										<tr>
											<th>Documento</th>
											<th>Documento</th>
											<th>Tipo</th>
											<th>Fecha Factura</th>
											<th>Moneda</th>
											<th>Proforma</th>
											<th>Proforma</th>
											<th>Cliente</th>
											<th>Renta Comisionable</th>
											<th>% Comisión</th>
											<th>Monto Comisión</th>
											<th>Renta Extra</th>
											<th>Comisión Renta Extra</th>
							            </tr>
					            	</thead>
					                <tbody style="text-align: center">
					                </tbody>
								</table>
								</div>
			                </div>
			            </div>
			        </div>
		        </div>
		    </div>
		</div>
    </div>
</section>	

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>

	<script>
		var tableLiquidaciones;
		var tableLiquidacionesDetalle;

		$(document).ready(function() 
		{
			$('.select2').select2();
			var periodo = '{{$periodo}}';

			if (periodo != '') 
			{
				$('#periodo').val(periodo).trigger('change.select2');
			}

		//	tableLiquidaciones = consultarLiquidaciones();
		//	tableLiquidacionesDetalle = consultarDetalleLiquidaciones();
		});

		const formatter = new Intl.NumberFormat('de-DE', 
		{
			currency: 'USD',
			minimumFractionDigits: 2
		});
		const formatos = new Intl.NumberFormat('de-DE', 
		{
			currency: 'PYG',
			minimumFractionDigits: 0
		});


		$('.checkCol').on('click',function()
			{
				var form = $('#columLiqExcel').serializeArray();
				valor = [];
				// valor.push(0,1,2,8);
				valor.push(0,1,2,3,4,6,7,11);
				$.each(form, function(item,value){
					valor.push(parseInt(value.value));
				});

				buttonsLiq(valor,tableLiquidacionesDetalle);
			});
		if('{{$indicador}}' == 1){
			periodo = '{{$periodo}}';
			id_liquidacion = parseInt('{{$idliquidacion}}');
			id_vendedor = parseInt('{{$id_vendedor}}');
			cargarReporteLiquidacionDetalle(id_liquidacion,id_vendedor, periodo, null, null)
		}

		function cargarReporteLiquidacionDetalle(id_liquidacion,id_vendedor, periodo, renta_total, comision_total)
		{
			$('#vendedorDetalle').val(id_vendedor).trigger('change.select2');
			$('#vendedorId').val(id_vendedor);
			$('#periodoDetalle').val(periodo).trigger('change.select2');
			$('#renta_total').val(renta_total);
			$('#comision_total').val(comision_total);
			$('#id_liquidacion').val(id_liquidacion);
			$('#tableLiquidacionesDetalle').tab('show');
			$('#baseIcon-tab22').trigger('click');
			consultarDetalleLiquidaciones();

		}

		function limpiarFiltrosDetalle()
		{
			$('#periodoDetalle').val('').trigger('change.select2');
			$('#vendedorDetalle').val('').trigger('change.select2');
			// consultarDetalleLiquidaciones();
		}

		function limpiarFiltros()
		{
			// $('#periodo').val('').trigger('change.select2');
			$('#vendedor').val('1').trigger('change.select2');
			// consultarLiquidaciones();
		}

		function formatearFecha(texto)
 		{
	        if(texto != '' && texto != null)
	        {
	          	return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	        } 
	        else 
	        {
	         	return '';    
         	}
        }

		function consultarLiquidaciones()
		{ 
			$("#com_total_gs").val(0);
			$("#com_total_usd").val(0);
			$("#com_total_eur").val(0);

			var tableLiquidaciones = $("#listado").DataTable
			({
				"destroy": true,
				"ajax": 
				{
					"url": "{{route('consultarLiquidaciones')}}",
					"type": "GET",
					"data": {"formSearch": $('#verLiquidaciones').serializeArray() },
					error: function(jqXHR,textStatus,errorThrown)
					{
		                $.toast
		                ({
		                    heading: 'Error',
		                    text: 'Ocurrió un error en la comunicación con el servidor.',
		                    position: 'top-right',
		                    showHideTransition: 'fade',
		                    icon: 'error'
		                });

		                }			    
					},
				"columns": 
				[
					{ "data": "id" },
					
					{ "data": function(x)
						{
							var str = x.periodo;
							var month = str.substring(0,2);
							var year = str.substring(2,6);
							return month+'/'+year;
						} 
					},

					{ "data": function(x)
						{
							var fecha = x.fecha_liquidacion.split(' ');
							var f = formatearFecha(fecha[0]);
							return f+' '+fecha[1];
						} 
					},

					{ "data": function(x)
						{
							return x.vendedor.nombre+" "+x.vendedor.apellido;
						} 
					},

					{ "data": function(x)
						{
							return formatter.format(parseFloat(x.renta_total));
						} 
					}, 

					{ "data": function(x)
						{
							if(jQuery.isEmptyObject(x.comision_total_gs) == false){
								total_gs = parseInt(x.comision_total_gs);
							}else{
								total_gs = 0;
							}
							return formatter.format(x.comision_total_gs);
						} 
					}, 
					{ "data": function(x)
						{
							if(jQuery.isEmptyObject(x.comision_total_usd) == false){
								total_usd = parseInt(x.comision_total_usd)
							}else{
								total_usd = 0;
							}
							return formatter.format(x.comision_total_usd);
						} 
					}, 
					{ "data": function(x)
						{
							if(jQuery.isEmptyObject(x.comision_total_eur) == false){
								total_eur = parseFloat(x.comision_total_eur);
							}else{
								total_eur = 0;
							}
							return formatter.format(total_eur);
						} 
					}, 
					{ "data": function(x)
						{
							var periodo = $("#periodo").val();
							var renta_total = formatter.format(parseFloat(x.renta_total));
							var btn = ``;
							btn += `<a onclick="cargarReporteLiquidacionDetalle(${x.id}, ${x.id_vendedor}, '${periodo}', '${renta_total}', '${x.comision_total}')" class="btn btn-info" style="padding-left: 6px;padding-right: 6px;color:#fff;margin-right: 10px;" role="button"><i class="fa fa-fw fa-search"></i></a>`;
							if ("{{Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil}}" == 2){	
								btn += `<a onclick="anularLiquidacion(${x.id})" class="btn btn-danger" style="padding-left: 6px;padding-right: 6px;color:#fff;" role="button"><i class="fa fa-fw fa-close"></i></a>`;
							}
							return btn;
						} 
					}, 

					]
						
				});	

				generarExcel([0,1,2,3,4], tableLiquidaciones);
				return tableLiquidaciones;
			};

			function consultarDetalleLiquidaciones()
			{ 
				var tableLiquidacionesDetalle = $("#listadoDetalle").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('consultarDetalleLiquidaciones')}}",
						"type": "GET",
						"data": {"formSearch": $('#verLiquidacionesDetalle').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
				
					"columns": 
					[
						{ "data": function(x){
								let btn = x.documento;
								if(x.id_tipo_documento == 1){
									btn = `<a href="{{route('verFactura',['id'=>''])}}/${x.id_documento}"  class="bgRed">
									<i class="fa fa-fw fa-search"></i>${x.documento}</a><div style="display:none;">${x.id_documento}</div>`;
								} else if(x.id_tipo_documento == 2){
									btn = `<a href="{{route('verNota',['id'=>''])}}/${x.id_documento}"  class="bgRed">
									<i class="fa fa-fw fa-search"></i>${x.documento}</a><div style="display:none;">${x.id_documento}</div>`;
								}
							
								return btn;
							} 
						},
						{ "data": "documento" },
						{ "data": "tipo_documento" },
						{ "data": function(x)
							{
								var fecha = '';
								var f = '';
								if(jQuery.isEmptyObject(x.fecha_hora_facturacion) == false) 
								{
									fecha = x.fecha_hora_facturacion.split(' ');
									f = formatearFecha(fecha[0]);
								}
								else
								{
									if(jQuery.isEmptyObject(x.fecha_hora_nota_credito) == false)
									{
										fecha = x.fecha_hora_nota_credito.split(' ');
										f = formatearFecha(fecha[0]);
									}
								}
								return f;
							} 
						},

						{ "data": "moneda" },
						{ "data": function(x)
							{
							btn = `<a href="{{route('detallesProforma',['id'=>''])}}/${x.id_proforma}"  class="bgRed">
							<i class="fa fa-fw fa-search"></i>${x.id_proforma}</a><div style="display:none;">${x.id_proforma}</div>`;
							return btn;
							} 
						},
						{ "data": "id_proforma" },
						{ "data": function(x)
							{
								var nombre = x.nombre;
								if(jQuery.isEmptyObject(x.apellido) == false) 
								{
									nombre =x.nombre +' '+x.apellido;
								}
								return nombre;
							} 
						},

						{ "data": function(x)
							{
								
								var renta_comisionable = 0;

								if(jQuery.isEmptyObject(x.f_renta_comisionable) == false)
								{
									renta_comisionable = x.f_renta_comisionable;
								}
								else
								{
									if(jQuery.isEmptyObject(x.nc_renta_comisionable) == false)
									{
										renta_comisionable = x.nc_renta_comisionable;
									}
								}

								return formatter.format(parseFloat(renta_comisionable));
							}
						},
						{ "data": function(x)
							{
								if(x.fact_por_liq !== null && x.fact_por_liq >= 0){
									return formatter.format(parseFloat(x.fact_por_liq));
								}
								return 'No tiene';
							}
						},

						{ "data": function(x)
							{
								var monto_comision = 0;
								if(jQuery.isEmptyObject(x.monto_comision) == false)
								{
									monto_comision = x.monto_comision;
								}
								return formatter.format(parseFloat(monto_comision));
							}
						},

						{ "data": function(x)
							{
								var renta_extra = 0;
								if(jQuery.isEmptyObject(x.renta_extra) == false)
								{
									renta_extra = x.renta_extra;
								}
								return formatter.format(parseFloat(renta_extra));
							}
						},

						{ "data": function(x)
							{
								var comision_renta_extra = 0;
								if(jQuery.isEmptyObject(x.comision_renta_extra) == false)
								{
									comision_renta_extra = x.comision_renta_extra;
								}
								return formatter.format(parseFloat(comision_renta_extra));
							}
						},
					],
					'columnDefs' : [
						//hide the second & fourth column
						{ 'visible': false, 'targets': [1,6] }
					],
					"fnInitComplete": function(oSettings, json) 
						{
							console.log(json)
	                       var renta_total = formatter.format(parseFloat(json.data2));
	                       $('#renta_total_detalle').val(renta_total);
						   var porcentaje_comision = formatter.format(parseFloat(json.data3));
	                       $('#porcentaje_comision').val(porcentaje_comision);
						   var comision_total_gs = 0;
							if(jQuery.isEmptyObject(json.data4) == false){
								comision_total_gs = formatter.format(parseFloat(json.data4));
							}
	                       $('#comision_total_gs').val(comision_total_gs);
						   var comision_total_usd = 0;
							if(jQuery.isEmptyObject(json.data5) == false){
								comision_total_usd = formatter.format(parseFloat(json.data5));
							}
	                       $('#comision_total_usd').val(comision_total_usd);
						   var comision_total_eur = 0;
							if(jQuery.isEmptyObject(json.data6) == false){
								comision_total_eur = formatter.format(parseFloat(json.data6));
							}
	                       $('#comision_total_eur').val(comision_total_eur);
	                    }
			});
			console.log(tableLiquidacionesDetalle);
			//generarExcelDetalle([0,1,2,3,4,6,5,7,8,9,10,11,12,13], tableLiquidacionesDetalle);
			return tableLiquidacionesDetalle;
		}

		   function anularLiquidacion(id){
					return swal({
								title: "GESTUR",
								text: "¿Está seguro que desea anular esta Liquidacion de Comisiones?",
								showCancelButton: true,
								buttons: {
										cancel: {
												text: "No",
												value: null,
												visible: true,
												className: "btn-warning",
												closeModal: false,
											},
										confirm: {
												text: "Sí, Anular",
												value: true,
												visible: true,
												className: "",
												closeModal: false
											}
										}
							}).then(isConfirm => {
									if (isConfirm) {
										 $.ajax({
												type: "GET",
												url: "{{route('anularLiquidacion')}}",
												dataType: 'json',
												data: {
													id_liquidacion:id,
												},
												error: function (jqXHR, textStatus, errorThrown) {
													$.toast({
														heading: 'Error',
														text: 'Ocurrió un error al intentar eliminar la Liquidacion.',
														position: 'top-right',
														showHideTransition: 'fade',
														icon: 'error'
													});
												},
												success: function (rsp) {
													if(rsp.status == "OK"){
														swal("Éxito", rsp.mensaje, "success");	
								        				location. reload();
													}else{
														swal("Cancelado", rsp.mensaje, "error");
													}
												}
											}) 
										}else{
											swal("Cancelado", "", "error");
										}	
							})	
			   }

	function generarExcelDetalle(column,tableLiq)
	{
		$('#botonExcelDetalle').html('');
		console.log(column);
		var buttons = new $.fn.dataTable.Buttons(tableLiq, 
		{ 
			buttons: 
			[{	
				title: 'Reporte Liquidación de Comisiones Detalle',
				extend: 'excelHtml5',
				text: '<b>Excel</b>',
				className: 'pull-right text-center btn btn-success btn-lg',
				exportOptions: 
				{
					columns: ':visible'
				},
				exportOptions: 
				{
					columns:  column,
					format: 
					{
						body: function ( data, row, column, node ) 
						{
							console.log(column);
							 if(column != 0 &&column != 1 && column != 2  && column != 3 && column != 4  && column != 5)
							{
								if (data != '0') 
								{
									data = data.replace(".","");
									//cambiamos la cpunto
									data = data.replace(",",".");
																			
								}
									var data = parseFloat(data);
									return  data;
							} 
							return data;
						}
					}
				},
				
				//Generamos un nombre con fecha actual
				
				filename: function() 
				{
					var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
					return 'ReporteLiquidacionComisionesDetalle-'+date_edition;
				}
			}]
		}).container().appendTo('#botonExcelDetalle'); 
	}

	function generarExcel(column,table) 
	{
		$('#botonExcelCabecera').html('');
		var buttons = new $.fn.dataTable.Buttons(table, 
		{ 
			buttons: 
			[{	
				title: 'Reporte Liquidación de Comisiones',
				extend: 'excelHtml5',
				text: '<b>Excel</b>',
				className: 'pull-right text-center btn btn-success btn-lg',
				exportOptions: 
				{
					columns: ':visible'
				},
				exportOptions: 
				{
					columns:  column,
					format: 
					{
						//seleccionamos las columnas para dar formato para el excel
						body: function ( data, row, column, node ) 
						{
							if(column != 0 && column != 1 && column != 2 && column != 3)
							{
								if (data != '0') 
								{
									data = data.replace(".","");
									//cambiamos la cpunto
									data = data.replace(",",".");
																			
								}
									var numFinal = parseFloat(data);
									return  numFinal;
							} 
							return data;
						}
					}
				},
				
				//Generamos un nombre con fecha actual
				
				filename: function() 
				{
					var date_edition = moment().format("DD-MM-YYYY HH[h]mm")
					return 'ReporteLiquidacionComisiones-'+date_edition;
				}
			}]
		}).container().appendTo('#botonExcelCabecera'); 
	}
	$('#comision_total').val(0);
	$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () {  }
		});
		function clean_num(n,bd=false){
				if(n && bd == false){ 
				n = n.replace(/[,.]/g,function (m) {  
										if(m === '.'){
											return '';
										} 
										if(m === ','){
											return '.';
										} 
									});
				return Number(n);
			}
			if(bd){
				return Number(n);
			}
			return 0;
	}
	
	function formatNumber(num) {
		if (!num || num == 'NaN') return '-';
		if (num == 'Infinity') return '&#x221e;';
		num = num.toString().replace(/\$|\,/g, '');
		if (isNaN(num))
			num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num * 100 + 0.50000000001);
		cents = num % 100;
		num = Math.floor(num / 100).toString();
		if (cents < 10)
			cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
			num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
		return (((sign) ? '' : '-') + num + ',' + cents);
	}
	$("#botonExcel").on("click", function(e){ 
        e.preventDefault();
        $('#verLiquidacionesDetalle').attr('method','post');
        $('#verLiquidacionesDetalle').attr('action', "{{route('generarExcelLiquidaciones')}}").submit();
    });


	</script>
@endsection
