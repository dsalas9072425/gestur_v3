@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
              <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Cheques Emitidos</h1>
            </div>      
            <div class="row">
		        <div class="col-xs-10">
		        </div>
		        <div class="col-xs-2">
		            <a href="{{route('mc.emitirCheques')}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px;margin-left: 10px;" role="button">Emitir Cheque</a>
		        </div>
		    </div> 
		    <br>       
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="table-responsive">
	              <table id="listado" class="table">
	                <thead>
					  <tr>
			            <th>#</th>
			            <th>Fecha</th>
						<th>Banco</th>
						<th>Cuenta</th>
			            <th>Moneda</th>
			            <th>Emitido</th>
						<th>Usuario</th>
			            <th>Monto</th>
		              </tr>
	                </thead>
	                <tbody>
	                	@foreach($listadoCheques as $key=>$cheques)
						  	<tr>				            
						  		<td>{{$cheques['id']}}</td>
					            <th>{{$cheques['fecha']}}</th>
								<th>{{$cheques['banco']['nombre']}}</th>
								<th>{{$cheques['cuenta']}}</th>
					            <th>{{$cheques['currency']['currency_code']}}</th>
					            <th>{{$cheques['tipo_cheque']['denominacion']}}</th>
								<th>{{$cheques['usuario']['nombre_apellido']}}({{$cheques['usuario']['usuario']}})</th>
					            <th>{{$cheques['importe']}}</th>
				            </tr>
				        @endforeach    
			        </tbody>
	              </table>
	            </div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
		});
	</script>
@endsection