@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
        	<br>
			<br>
			<br>
			<br>
            <div id="main" class="col-sm-12 col-md-10" style="background-color: #fff;">
            	@include('flash::message')
						<h1 class="subtitle hide-medium" style="font-size: xx-large;">Nuevo Cheque</h1>
						<br>
			               <form id="frmUsuarios" action="{{route('mc.doAddCheque')}}" method="get" autocomplete="nope"> 
			               		{{ csrf_field() }}
					                <div class="row">
					                    <div class="col-md-6">
											<label class="control-label">Banco (*)</label>
					                            <select name="banco_id" required class="form-control" id="sucursal_id">
													@foreach($bancos as $key=>$banco)
														<option value="{{$banco['id']}}">{{$banco['nombre']}}</option>
													@endforeach
												</select>
										</div>
					                    <div class="col-md-6">
											<label class="control-label">Moneda (*)</label>
					                            <select name="currency_id" required class="form-control" id="sucursal_id">
													@foreach($monedas as $key=>$moneda)
														<option value="{{$moneda['currency_id']}}">{{$moneda['currency_code']}}</option>
													@endforeach
												</select>
										</div>
					                </div>
					                <br/>
									<div class="row">
					                    <div class="col-md-6">
											<label class="control-label">Pago (*)</label>
					                            <select name="tipo_cheque_id" required class="form-control" id="sucursal_id">
													@foreach($tipo_cheque as $key=>$tipo_cheque)
														<option value="{{$tipo_cheque['id']}}">{{$tipo_cheque['denominacion']}}</option>
													@endforeach
												</select>
										</div>
										<div class="col-md-6">
											<label class="control-label">Numero (*)</label>
					                        <input type="text" required class = "form-control" name="numero" id="numero" placeholder="Numero" value="" />										
										</div>
									</div>	
									<br/>
									<div class="row">
					                    <div class="col-md-6">
											<label class="control-label">Fecha (*)</label>
							                    <div class="input-group date">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" name="fecha" id="fecha_baja" class="form-control pull-right fecha" required>
												</div> 	
										</div>
										<div class="col-md-6">
											<label class="control-label">Monto (*)</label>
					                        <input type="text" required class = "form-control" name="monto" id="monto" placeholder="Monto" value="" />										
										</div>
									</div>	
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-primary" style=" background: #e2076a !important;" name="guardar" value="Guardar">Guardar</button>
								</div>
                            </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>

		$( ".fecha" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

		$('#agencia_id').change(function(){
				$.ajax({
					type: "GET",
					url: "{{route('getSucursales')}}",
					dataType: 'json',
					data: {
						dataSucursal: $(this).val()
			       		},
					success: function(rsp){
								$('#sucursal_id').empty()
								$('#sucursal_id').trigger("chosen:updated");
							$.each(rsp, function(key,value){
								$('#sucursal_id').append('<option value="'+value.value+'">'+value.label+'</option>');
								$('#sucursal_id').trigger("chosen:updated");
							})	
					}	
				})
			})
		$('#agencia_id').trigger('change');

		$(document).ready(function() {			
			$("#password").val('');
			
			$("#frmUsuarios").submit(function(e){
				//e.preventDefault();
				var $form = $('#frmUsuarios');
				console.log($form.attr('action') + "?" +$form.serialize());
				
				//$form.unbind('submit').submit();
				//alert('yolo');
				
			});
			
		})
	</script>
@endsection