@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Paises</h1>
            </div> 
            @include('flash::message')      
            <div class="row">
		        <div class="col-xs-12">
		            <a href="{{route('paisAdd')}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px;margin-left: 10px;" role="button">Nuevo Pais</a>
		        </div>
		    </div>        
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="table-responsive">
	              <table id="listado" class="table">
	                <thead>
					  <tr>
						<th>Id</th>
						<th>Denominacion</th>
			            <th class="oculto">Ver</th>
		              </tr>
	                </thead>
	                <tbody>
	 					@foreach($paises as $pais)
				            <tr>
				                <td><b>{{$pais['id']}}</b></td>
				                <td>{{$pais['denominacion']}}</td>
				                <td>
				                	<a href="{{route('paisEdit', ['id' =>$pais['id']])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; background: #e2076a !important; width: 106px;" role="button">Editar</a>
									<a href="{{route('paisDelete', ['id' =>$pais['id']])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; width: 106px;" role="button">Eliminar</a>
				                </td>
				            </tr>	
			            @endforeach
			        </tbody>
	              </table>
	            </div>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
		});
	</script>
@endsection