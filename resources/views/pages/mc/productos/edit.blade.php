
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		table.dataTable td, table.dataTable th {
			padding-left: 3px;
    		padding-right: 5px;
		}	

	</style>

@endsection
@section('content')
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Editar Productos</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	<form id="frmProductoEdit" autocomplete="off">
            		<div class = "row">
            		    <div class="col-12 col-sm-4 col-md-3" id="inputApellido">
							<div class="form-group">
								<label>Denominación <span></span></label>
								<input type="text" class="form-control" name="denominacion" id="denominacion" value="{{$producto->denominacion}}" />
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3" style="height: 74px;" id="inputgrupo_producto">
							<div class="form-group">
								<label>Grupo Productos</label>
								<select class="form-control select2" name="grupo_producto"  id="grupo_producto" style="width: 100%;" >
									<option value="">Seleccione Opción</option>	
									@foreach ($grupo_producto as $grupo)
										<option value="{{$grupo->id}}">{{$grupo->denominacion}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3" id="inputimprimir_factura">
							<div class="form-group">
								<label>Imprimir en factura</label>
								<select class="form-control select2"  name="imprimir_factura"  id="imprimir_factura" style="width: 100%;" >
									<option value="1">SI</option>
									<option value="false">NO</option>
								</select> 
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Frase predeterminada <span></span></label>
								<input type="text" class="form-control" name="frase_predet" id="frase_predet" value="{{$producto->frase_predeterminada}}" maxlength="100" />
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Impuesto<span></span></label>
								<select class="form-control select2" name="porcentaje_gravada" id="porcentaje_gravada" style="width: 100%;" >
									<option value="100">IVA 10%</option>
									<option value="0">Exento</option>
								</select> 
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3" id="inputid_plan_cuenta">
		                            <div class="form-group" style="margin-bottom: 5px;">
		                                 <label>Cuenta Contable Costo(*)</label>
										<select class="form-control select2" name="id_plan_cuenta" id="id_plan_cuenta" style="width: 100%;">
											<option value="">Seleccione una cuenta</option>
		                                    @foreach ($data_plan as $cuenta)
		                                        @if($cuenta->asentable)
		                                            <optgroup label="{{$cuenta->descripcion}}">
		                                                @endif
		                                                <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}})
		                                                    {{$cuenta->descripcion}}
		                                                </option>
		                                                @if($cuenta->asentable)
		                                            </optgroup>
		                                        @endif 
		                                    @endforeach
		                               </select>
		                            </div>
		                        </div>

		                        <div class="col-12 col-sm-4 col-md-3" id="inputid_plan_cuenta_venta">
		                            <div class="form-group" style="margin-bottom: 5px;">
		                                 <label>Cuenta Contable Venta(*)</label>
										<select class="form-control select2" name="id_plan_cuenta_venta" id="id_plan_cuenta_venta" style="width: 100%;">
											<option value="">Seleccione una cuenta</option>
		                                    @foreach ($data_plan as $cuenta)
		                                        @if($cuenta->asentable)
		                                            <optgroup label="{{$cuenta->descripcion}}">
		                                                @endif
		                                                <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}})
		                                                    {{$cuenta->descripcion}}
		                                                </option>
		                                                @if($cuenta->asentable)
		                                            </optgroup>
		                                        @endif 
		                                    @endforeach
		                               </select>
		                            </div>
		                        </div>
						<!--<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Generar Costo/LC</label>
								<select class="form-control select2"  name="migrar"  id="migrar" style="width: 100%;" >
									<option value="1">SI</option>
									<option value="false">NO</option>
								</select> 
							</div>
						</div>-->

						<div class="col-12 col-sm-4 col-md-3" id="inputcomisionable">
							<div class="form-group">
								<label>Es comisionable</label>
								<select class="form-control select2"  name="comisionable"  id="comisionable" style="width: 100%;" >
									<option value="false">NO</option>
									<option value="1">SI</option>
								</select> 
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3" id="inputcomisiona_solo">
							<div class="form-group">
								<label>Comisiona estando solo</label>
								<select class="form-control select2"  name="comisiona_solo"  id="comisiona_solo" style="width: 100%;" >
									<option value="false">NO</option>
									<option value="1">SI</option>
								</select> 
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Comisión base <span></span></label>
								<input type="number" class="form-control" name="comision_base" id="comision_base" value="{{$producto->comision_base}}" />
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3" id="inputactivo">
							<div class="form-group">
								<label>Minimo Comisionable</label>
								<input type="number" class="form-control" maxlength="2" name="minimo_comisionable" id="minimo_comisionable" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" value="{{$producto->renta_minima_para_comisionar}}"/>
							</div> 
						</div>
						<div class="col-12 col-sm-4 col-md-3" id="inputactivo">
							<div class="form-group">
								<label>Activo</label>
								<select class="form-control select2"  name="activo"  id="activo" style="width: 100%;" >
									<option value="1">SI</option>
									<option value="false">NO</option>
								</select> 
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3" id="inputdesc_voucher">
							<div class="form-group">
								<label>Descripción Voucher <span></span></label>
								<input type="text" class="form-control" name="desc_voucher" id="desc_voucher" value="{{$producto->descrip_voucher}}" placeholder="Ejemplo: Actividades_Proveedor1_Proveedor2" />
								<small>El texto debe ir separado por guion bajo "_"</small>
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3" style="height: 74px;" id="inputgenera_voucher">
							<div class="form-group">
								<label>Genera Voucher</label>
								<select class="form-control select2"  name="genera_voucher"  id="genera_voucher" style="width: 100%;" >
									<option value="false">NO</option>
									<option value="1">SI</option>
								</select> 
							</div>
						</div>
 
						<div class="col-12 col-sm-4 col-md-3" id="inputmultiple_voucher">
							<div class="form-group">
								<label>Genera Multiple Voucher</label>
								<select class="form-control select2"  name="multiple_voucher"  id="multiple_voucher" style="width: 100%;" >
									<option value="false">NO</option>
									<option value="1">SI</option>
								</select> 
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3" id="inputtiene_dtplus">
							<div class="form-group">
								<label>Tiene Incentivo</label>
								<select class="form-control select2"  name="tiene_dtplus"  id="tiene_dtplus" style="width: 100%;" >
									<option value="false">NO</option>
									<option value="1">SI</option>
								</select> 
							</div>
						</div>
				
						<div class="col-12 col-sm-4 col-md-3" id="inputorigen_extranjero">
							<div class="form-group">
								<label>Origen Extranjero</label>
								<select class="form-control select2"  name="origen_extranjero"  id="origen_extranjero" style="width: 100%;" >
									<option value="false">NO</option>
									<option value="1">SI</option>
								</select> 
							</div>
						</div>
						            
						<div class="col-12 col-sm-4 col-md-3" id="inputprecio_costo">
							<div class="form-group">
								<label>Precio Costo</label>
							<input type="text" class="form-control numeric" minlength="2" name="precio_costo" id="precio_costo" maxlength="15" value ="{{$producto->precio_costo}}">
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3" id="inputprecio_venta">
							<div class="form-group">
								<label>Precio Venta</label>
							<input type="text" class="form-control numeric" minlength="2" name="precio_venta" id="precio_venta" maxlength="15" value ="{{$producto->precio_venta}}">
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3" id="inputvisible">
							<div class="form-group">
								<label>Visible para el Usuario</label>
								<select class="form-control select2"  name="visible"  id="visible" style="width: 100%;" >
									<option value="1">SI</option>
									<option value="false">NO</option>
								</select> 
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Proveedor</label>
								<select class="form-control select2"  name="id_proveedor"  id="id_proveedor" style="width: 100%;" >
									<option value="">Seleccione un proveedor</option>
									@foreach($proveedores as $prov)
										@php
											$ruc = $prov->documento_identidad;
											if($prov->dv){
												$ruc = $ruc."-".$prov->dv;
											}
										@endphp
										<option value="{{$prov->id}}">{{$ruc}} - {{$prov->nombre}}</option>
									@endforeach
								</select> 
							</div>
						</div>

						<div class="col-12 col-sm-4 col-md-3" id="inputcod_producto">
							<div class="form-group">
								<label>Código Producto <span></span></label>
								<input type="text" class="form-control" name="cod_producto" id="cod_producto" value="{{$producto->woo_sku}}" />
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3" id="inputid_tipo">
							<div class="form-group">
								<label>Tipo Producto</label>
								<select class="form-control select2"  name="tipo_producto"  id="tipo_producto" style="width: 100%;" >
									<option value="P">Proforma</option>
									<option value="V">Venta Rápida</option>
								</select> 
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3">						    
							<label>Sucursal</label>
							<select class="form-control select2" name="id_sucursal" id="id_sucursal" style="width: 100%;">
								<option value="">Todos</option>
								@foreach ($sucursales as $sucursal)
									<?php 
										$nombre = $sucursal->nombre;
										if($sucursal->denominacion_comercial != ""){
											$nombre = $nombre.' - '.$sucursal->denominacion_comercial;
										}	
									?>
									<option value="{{$sucursal->id}}">{{$nombre}}</option>
								@endforeach
							</select>
					    </div>
						<div class="col-12 col-sm-4 col-md-3">						    
							<label>Pago TC Detalle de Proforma</label>
							<select class="form-control select2" name="pago_tc_detalle_proforma" id="pago_tc_detalle_proforma" style="width: 100%;">
								<option {{$producto->pago_tc_detalle_proforma ? 'selected' : ''}} value="1">SI</option>
								<option {{$producto->pago_tc_detalle_proforma ? '' : 'selected'}} value="0">NO</option>
							</select>
					    </div>


						<input type="hidden" name="icono" value="" id="inputImg" />   
						<input type="hidden" name="id" value="{{$id}}" id="idProducto">
					</div>	
            	</form>	
            	<div class="row" id="inputimagen">
            		<div class="col-md-4">
		            	<form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('productoIconUpload')}}" autocomplete="off">
		                    <div class="form-group">
		                        <label>Imagen</label><span class="cargandoImg"> <i class="fa fa-refresh fa-spin"></i></span>
		                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
		                        <input type="file" class="form-control" name="image" id="image"/>  
		                        <div id="validation-errors"></div>
		                    </div>  
		                </form>	
		            </div>    
	    			<div class="col-md-1" style="width: 100px; height: 100px; border: solid 1px black; padding: 1px; background: #8A8EC1;">
						<img class="" id="output" src="" style="width: 90px; height: 90px; display: none;">
				    </div>
	                <div class="col-md-3 ">
	                    <button type="button" id="btnDeleteImg" disabled="disabled">
	                    	<i class="ft-x"></i>
	                    </button>
	                </div>
	            </div> 
	            <div class="row">   
					<div class="col-md-12" style="padding-left: 15px;">
						<button type="button" form="frmPersona" id="btnGuardar" class="btn btn-success btn-lg pull-right"><b>Guardar</b></button>
						<a role="button" href="{{ route('indexProducto') }}" class="btn btn-danger btn-lg pull-right" style="margin-right: 10px;"><b>Volver</b></a>
					</div>
				</div>	
            </div>
        </div>    	
    </div>  
</section>     
 
  
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	{{-- <script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script> --}}
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>

	<script>

$(document).ready(function(){


$('.select2').select2();
		 cargarImagen();
})

		
		$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

	idEmpresa = "{{Session::get('datos-loggeo')->datos->datosUsuarios->idEmpresa}}";
	
	if(idEmpresa != 1 && idEmpresa != 6&& idEmpresa != 3){
		$('#inputgenera_voucher').css('display','none'); 
		$('#inputmultiple_voucher').css('display','none'); 
		$('#inputdesc_voucher').css('display','none'); 
	//	$('#inputtiene_dtplus').css('display','none'); 
		$('#inputorigen_extranjero').css('display','none'); 
		$('#inputvisible').css('display','none'); 
		$('#inputprecio_costo').css('display','none'); 
		$('#inputprecio_venta').css('display','none'); 
		$('#inputid_proveedor').css('display','none'); 
		$('#inputid_tipo').css('display','none'); 
		$('#inputimagen').css('display','none'); 
		$('#inputcod_producto').css('display','none'); 

	}


   		function selectNValid(data) {

			if (data == 'null' || data == '') {
				return 'false';
			}
			return data;
		}

		function selectN(data) {

			if (data == 'null' || data == '') {
				return '';
			}
			return data;
		}


   $("#genera_voucher").val(selectNValid('{{$producto->genera_voucher}}')).trigger('change.select2');
   $("#multiple_voucher").val(selectNValid('{{$producto->multiple_voucher}}')).trigger('change.select2');
   $("#tiene_dtplus").val(selectNValid('{{$producto->tiene_dtplus}}')).trigger('change.select2');
   $("#comisionable").val(selectNValid('{{$producto->es_comisionable}}')).trigger('change.select2');
   $("#comisiona_solo").val(selectNValid('{{$producto->comisiona_estando_solo}}')).trigger('change.select2');
   $("#origen_extranjero").val(selectNValid('{{$producto->origen_extranjero}}')).trigger('change.select2');
   $("#imprimir_factura").val(selectNValid('{{$producto->imprimir_en_factura}}')).trigger('change.select2');
   $("#tipo_producto").val(selectNValid('{{$producto->tipo_producto}}')).trigger('change.select2');
   $("#porcentaje_gravada").val(selectNValid('{{$producto->porcentaje_gravada}}')).trigger('change.select2');
   $("#visible").val(selectNValid('{{$producto->visible}}')).trigger('change.select2');
   $("#activo").val(selectNValid('{{$producto->activo}}')).trigger('change.select2');
   $("#grupo_producto").val(selectN('{{$producto->id_grupos_producto}}')).trigger('change.select2');
  // $("#migrar").val(selectNValid('{{$producto->migrar}}')).trigger('change.select2');
   $("#id_plan_cuenta").val(selectN('{{$producto->id_plan_cuenta}}')).trigger('change.select2');
   $("#id_plan_cuenta_venta").val(selectN('{{$producto->id_cuenta_contable_venta}}')).trigger('change.select2');
   $("#id_proveedor").val(selectN('{{$producto->id_proveedor}}')).trigger('change.select2');
   $("#id_sucursal").val(selectN('{{$producto->id_sucursal}}')).trigger('change.select2');

	var lista = ['denominacion',
				'grupo_producto',
				'genera_voucher',
				'multiple_voucher',
				//'desc_voucher',
				'tiene_dtplus',
				'comisionable',
				'comisiona_solo',
				'comision_base',
				'frase_predet',
				'origen_extranjero',
				'porcentaje_gravada',
				'imprimir_factura',
				'visible',
				'activo',
				'id_plan_cuenta',
				'id_plan_cuenta_venta'];



	$('#btnGuardar').click( function(event){
		var ok = 0;
		   $.each(lista, function (key, item){

            
 
             var dato = $("#"+item).get(0).tagName;
             var val = $("#"+item).val();

            if(dato == 'INPUT'){

               if(val == ''){

                    $("#"+item).css('border-color','red'); 
                    ok++;
                } else {
                      $("#"+item).css('border-color','#d2d6de');
                } //else

                    }

            if(dato == 'SELECT'){
            	console.log('-'+val+'-');

                 if(val == ''){

                        $("#input"+item).find('.select2-container--default .select2-selection--single').css('border-color','red'); 
                        ok++;
                    } else {

                     $("#input"+item).find('.select2-container--default .select2-selection--single').css('border','1px solid #aaa');
                    }//else


                    }   
			});

		   if(ok == 0){


		   	 $.blockUI({
                                        centerY: 0,
                                        message: "<h2>Procesando...</h2>",
                                        css: {
                                            color: '#000'
                                        }
                                    });

		   	var dataString = $('#frmProductoEdit').serialize();

		   	  $.ajax({
                        type: "GET",
                        url: "{{route('actualizarProducto')}}",
                        dataType: 'json',
                        data: dataString,

                       error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                             $.unblockUI();

                        },
                        success: function(rsp){
					 $.unblockUI();

                           if(rsp.err == true){
                           		 window.location.href = "{{ route('indexProducto') }}";
                           } else {
                           

                           	  $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la operación de guardar.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                           }//else
                          
                          
                                }//funcion  
                });



		   } else {
		   	$.unblockUI();
		   	    $.toast({
                            heading: 'Error',
                            text: 'Complete todos los campos por favor !',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

		   }
	});			









	//variables de direccion de archivo img
	const variableImg = {

		  direccionImagen : '',
		  setImg : function(i){ this.direccionImagen = i;},
		  getImg : function(i){ return this.direccionImagen;}
      }





	//=====================IMAGEN FORM ========================//
	


      function cargarImagen(){

      	var n = '{{$producto->icono}}';

      	if(n != ''){
      	 					$("#imagen").val('{{$producto->icono}}');
      	 					variableImg.setImg('{{$producto->icono}}');
                             $("#image").prop('disabled',true); 
                             $('#btnDeleteImg').prop('disabled',false);
                             $('#inputImg').val('{{$producto->icono}}'); 
                          
                                $('#output').attr('src','../../iconosVoucher/{{$producto->icono}}');
                                $('#output').show();

      	}


      }

 

	


	 var options = { 
                                beforeSubmit:  showRequest,
                                success: showResponse,
                                dataType: 'json' 
                        }; 

                  //OCULTAR OVERLAY      
                 	$('.cargandoImg').hide();       

                $('body').delegate('#image','change', function(){
                    //OBTENER TAMAÑO DE LA IMAGEN
                    var input = document.getElementById('image');
                    var file = input.files[0];
                    var tamaño = file.size;

                        if(tamaño > 0){

                        var tamaño = file.size/1000;
                        if(tamaño > 1000){
                            $("#image").val('');
                            $("#validation-errors").empty();
                            $("#validation-errors").append('<div class="alert alert-error"><strong>El tamaño de la imagen supera el limite de 1MB</strong><div>'); 
                        } else {

                            $('.cargandoImg').show();
                            $('#upload').ajaxForm(options).submit();  

                        }

                        } else {
                             $("#image").val('');
                             $("#validation-errors").empty();
                             $("#validation-errors").append('<div class="alert alert-error"><strong>Existe un error con el archivo</strong><div>'); 
                        }
                                        
                                });


                  function showRequest(formData, jqForm, options) { 
                   
                                    $("#validation-errors").hide().empty();
                                    return true; 
                                  } 


                  function showResponse(response, statusText, xhr, $form)  { 
                        $('.cargandoImg').hide();
                        if(response.success == false)
                          {
                       $("#image").val('');
                       $("#validation-errors").append('<div class="alert alert-error"><strong>'+ response.errors +'</strong><div>');
                       $("#validation-errors").show();

                        } else {
                             $("#imagen").val(response.archivo);
                             $("#image").prop('disabled',true); 
                             $('#btnDeleteImg').prop('disabled',false); 
                              $('#inputImg').val(response.archivo); 
                          
                          		variableImg.setImg(response.archivo);
                                $('#output').attr('src',response.file);
                                $('#output').show();
               
                                 
                             
                        }
                   }

                  $('#btnDeleteImg').on('click',function(){


                      $('.cargandoImg').show();
                      var archivo = variableImg.getImg();
                      var idProducto = $('#idProducto').val();

                      $.ajax({
                          type: "GET",
                          url: "{{route('deleteIconProducto')}}",
                          dataType: 'json',
                          data: {  
                               dataFile:archivo,
                               idProducto: idProducto
                                    },


                            error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error al intentar eliminar la imagen.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                            $('.cargandoImg').hide();

                        },
                          success: function(rsp){
                            $('.cargandoImg').hide();

                            if(rsp.rsp == true){

                            document.getElementById("image").value = "";
                            variableImg.setImg('');
                            $('#output').attr('src','');
                            $('#output').hide();
                            $('#btnDeleteImg').prop('disabled',true); 
                            document.getElementById("image").value = "";
                            $("#image").prop('disabled',false);
                            $('#inputImg').val(''); 

                                $.toast({
			                        heading: 'Success',
			                        text: 'La imagen fue eliminada.',
			                        position: 'top-right',
			                        showHideTransition: 'slide',
			                        icon: 'success'
			                    });
                       
                                
                            } else {
                                     $.toast({
				                            heading: 'Error',
				                            text: 'Ocurrio un error al intentar eliminar la imagen.',
				                            position: 'top-right',
				                            showHideTransition: 'fade',
				                            icon: 'error'
				                        });
                            
                            }//ELSE
                            
                            
                          }//function
                       

                    });


                  });//FUNCTION 
































	</script>
@endsection