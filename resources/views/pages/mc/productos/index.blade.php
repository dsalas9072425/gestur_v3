
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Lista Productos</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">
				<a href="{{route('addProducto')}}" title="Agregar Grupo" class="btn btn-success pull-right"	role="button">
					<div class="fonticon-wrap">
						<i class="ft-plus-circle"></i>
					</div>
				</a>
			</div>		        

            <div class="card-body">
            	<form id="frmProductoConsulta" autocomplete="off">

            		<div class = "row">	
            			<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Genera Voucher</label>
								<select class="form-control select2" tabindex="1" name="genera_voucher"  id="genera_voucher" style="width: 100%;" >
									<option value="">Seleccione Opción</option>
									<option value="1">SI</option>
									<option value="false">NO</option>
								</select> 
							</div>
						</div>
		           	    <div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Tiene Incentivo</label>
								<select class="form-control select2" tabindex="2" name="tiene_dtplus"  id="tiene_dtplus" style="width: 100%;" >
									<option value="">Seleccione Opción</option>
									<option value="1">SI</option>
									<option value="false">NO</option>
					            </select>
							</div>
						</div>
					    <div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Es Comisionable</label>
								<select class="form-control select2" tabindex="3" name="comisionable"  id="comisionable" style="width: 100%;" >
									<option value="">Seleccione Opción</option>
									<option value="1">SI</option>
									<option value="false">NO</option>
								</select>
							</div>
						</div>
    	            	<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Imprimir en Factura</label>
								<select class="form-control select2" tabindex="4" name="imprimir_en_factura"  id="imprimir_en_factura" style="width: 100%;" >
									<option value="">Seleccione Opción</option>
									<option value="1">SI</option>
									<option value="false">NO</option>
							    </select>
							</div>
						</div>
            		
						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Estado</label>
								<select class="form-control select2" tabindex="5" name="activo"  id="activo" style="width: 100%;" >
									<option value="">Seleccione Opción</option>
									<option value="1">Activo</option>
									<option value="false">Inactivo</option>
								</select>
							</div>
						</div>							             
						<div class="col-12 col-sm-4 col-md-3" id="inputid_proveedor">
							<div class="form-group">
								<label>Proveedor</label>
								<select class="form-control select2"  name="id_proveedor"  id="id_proveedor" style="width: 100%;" >
									<option value="">Seleccione un proveedor</option>
									@foreach($proveedores as $prov)
										@php
											$ruc = $prov->documento_identidad;
											if($prov->dv){
												$ruc = $ruc."-".$prov->dv;
											}
										@endphp
										<option value="{{$prov->id}}">{{$ruc}} - {{$prov->nombre}}</option>
									@endforeach
								</select> 
							</div>
						</div>
    				    <div class="col-12 col-sm-4 col-md-3" id="inputApellido">
							<div class="form-group">
								<label>Denominación</label>
								<input type="text" class="form-control" tabindex="7" name="denominacion" id="denominacion" value="" />
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-3">
							<div class="form-group">
								<label>Tipo Producto</label>
								<select class="form-control select2" tabindex="8" name="tipo_producto"  id="tipo_producto" style="width: 100%;" >
									<option value="">Seleccione Opción</option>
									<option value="P">Proformas</option>
									<option value="V">Ventas Rápidas</option>
								</select>
							</div>
						</div>	
						 <div class="col-12 col-sm-4 col-md-3" id="inputApellido">
							<div class="form-group">
								<label>Código Producto</label>
								<input type="text" class="form-control" tabindex="9" name="cod_producto" id="cod_producto" value="" />
							</div>
						</div>
  						<div class="col-12 col-sm-3" id="inputid_plan_cuenta">
                            <div class="form-group" style="margin-bottom: 5px;">
                                 <label>Cuenta Contable Costo(*)</label>
								<select class="form-control select2" name="id_plan_cuenta" id="id_plan_cuenta" style="width: 100%;">
									<option value="">Seleccione una cuenta</option>
                                    @foreach ($data_plan as $cuenta)
                                        @if($cuenta->asentable)
                                            <optgroup label="{{$cuenta->descripcion}}">
                                                @endif
                                                <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}})
                                                    {{$cuenta->descripcion}}
                                                </option>
                                                @if($cuenta->asentable)
                                            </optgroup>
                                        @endif 
                                    @endforeach
                               </select>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3" id="inputid_plan_cuenta_venta">
                            <div class="form-group" style="margin-bottom: 5px;">
                                 <label>Cuenta Contable Venta(*)</label>
								<select class="form-control select2" name="id_plan_cuenta_venta" id="id_plan_cuenta_venta" style="width: 100%;">
									<option value="">Seleccione una cuenta</option>
                                    @foreach ($data_plan as $cuenta)
                                        @if($cuenta->asentable)
                                            <optgroup label="{{$cuenta->descripcion}}">
                                                @endif
                                                <option value="{{$cuenta->id}}">({{$cuenta->cod_txt}})
                                                    {{$cuenta->descripcion}}
                                                </option>
                                                @if($cuenta->asentable)
                                            </optgroup>
                                        @endif 
                                    @endforeach
                               </select>
                            </div>
                        </div>						
                        <div class="col-12">
							 <button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style="margin-left: 10px;"><b>Excel</b></button>
							<button type="button"  id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white" style="margin: 0 0 10px 10px;"><b>Limpiar</b></button>
							<button type="button"  id="btnBuscar" tabindex="8" class="btn btn-info btn-lg pull-right" style="margin: 0 0 10px 10px;"><b>Buscar</b></button>
						</div>
					</div>				
            	</form>	
            	<div class="table-responsive table-bordered">
		            <table id="listado" class="table" style="width: 100%;">
		                <thead>
						  <tr>
						  	<th>Denominación</th>
							<th>Grupo</th>
							<th>Precio<br> Costo</th>
							<th>Código<br> Producto</th>
							<th>Cuenta <br>Contable Costo</th>
							<th>Cuenta <br>Contable Venta</th>
							<th>LC <br>Automático</th>
							<th>Voucher</th>
							<th>Multiple<br> Voucher</th>
							<th>Incentivo</th>
							<th>Comisionable</th>
							<th>Comisiona <br>Solo</th>
							<th>Comision <br>Base</th>
							<th>Imprimir <br>en Fact.</th>
							<th>Visible</th>
							<th>Estado</th>
							<th></th>
			              </tr>
		                </thead>
		                <tbody  style="text-align: center">
					         
				        </tbody>
		            </table>
	            </div>  
            </div>	
        </div>    
    </div>    
</section>    


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script>

		$('body').keyup(function(e) {
    if(e.keyCode == 13) {
        buscar();
    }
});

	$(document).ready(function() {
		$('.select2').select2({
				tag:true
			});

		
$("#listado").dataTable();
			
			


	
			
		});


			function validar(data){

				if(data === undefined || data === null || data === '')
					return false;

				return true;
			}


	
	$('#btnLimpiar').on('click',function(){
		$('#genera_voucher').val('').trigger('change.select2');
		$('#tiene_dtplus').val('').trigger('change.select2');
		$('#comisionable').val('').trigger('change.select2');
		$('#imprimir_en_factura').val('').trigger('change.select2');
		$('#activo').val('').trigger('change.select2');
		$('#num_cuenta').val('');
		$('#denominacion').val('');
		$('#cod_producto').val('');
		$('#tipo_producto').val('').trigger('change.select2');
		// buscar();
	});



	

		  $('#btnBuscar').on('click', function(){  
		  	buscar();
		   });

		  function buscar(){ 
    		
		 	table = $("#listado").DataTable
				({
					"destroy": true,
					"ajax": 
					{
						"url": "{{route('getDatosProductos')}}",
						"type": "GET",
						"data": {"formSearch": $('#frmProductoConsulta').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                    $.toast
		                    ({
		                        heading: 'Error',
		                        text: 'Ocurrió un error en la comunicación con el servidor.',
		                        position: 'top-right',
		                        showHideTransition: 'fade',
		                        icon: 'error'
		                    });

		                }			    
					},
					"aaSorting":[[0,"desc"]],
					"columns": 
					[
						{ "data": "denominacion" },
						{ "data": "id_grupos_producto" },
						{ "data": "precio_costo" },
						{ "data": "woo_sku" },
						{ "data": "cuenta_contable_costo" },
						{ "data": "cuenta_contable_venta" },
						{ "data": "lc_txt" },
						{ "data": "genera_voucher_txt" },
						{ "data": "multiple_voucher_txt" },
						{ "data": "tiene_dtplus_txt" },
						{ "data": "es_comisionable_txt" },
						{ "data": "comisiona_estando_solo_txt" },
						{ "data": "comision_base" },
						{ "data": "imprimir_en_factura_txt" },
						{ "data": "visible_txt" },
						{ "data": "estado_val" },
						{ "data": "btn" },
					
					], 
					"initComplete": function (settings, json) {
			 		// $.unblockUI();
					 $('[data-toggle="tooltip"]').tooltip();
					 
			 	}
					
				});	
				
				// botonExcel([2,3,4,5,6,7,8,9,10,11,12,13,14], table);
				return table;

    
}


$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
                $('#frmProductoConsulta').attr('method','post');
                $('#frmProductoConsulta').attr('action', "{{route('generarExcelProducto')}}").submit();
            });

				
			

	</script>
@endsection