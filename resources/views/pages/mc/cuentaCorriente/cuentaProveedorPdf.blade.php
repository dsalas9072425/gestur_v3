<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Detalle OP</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 95%;
			margin:0 auto;
    		z-index:1;
	}
	
	th {
        border:1px solid black;
      }

    table {
        border-spacing:0px;
		width: 100%;
      }

	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 700;
	}

	.text{
		  overflow: hidden;
		  /*text-overflow: ellipsis;*/
		  white-space: nowrap;
		  display:block;
		  width:100%;
		  min-width:1px;
		}

	.f-8 {
		font-size: 10px !important;
	}

	.f-9 {
		font-size: 9px !important;
	}

	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}

	.f-15 {
		font-size: 15px !important;
	}
	.f-17 {
		font-size: 17px !important;
	}

	.c-text {
		text-align: center;
	}
	.r-text {
		text-align: left;
	}
	
	.r-text-detalle{
		margin-right: 20px;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 10px;
	}

	.b-buttom {
	border-bottom: 1px solid; 
	}

	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:120px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}



	</style>
</head>
<body>
		<?php
			$logoEmpresa = Session::get('datos-loggeo')->datos->datosUsuarios->logoEmpresa;
			$empresaLogo = "logoEmpresa/".$logoEmpresa;
		?>
		<br>
		<br>
		<div class="container espacio-10">
			<div class="table-responsive" style="border: solid 2px #000;border-radius:15px;">
				<table style="">
					<tr>
						<td colspan="5" class="r-text">
									<br>
									<img src="{{asset($empresaLogo)}}" style="width: 120px;height: 45px;">
							</td>
							<td class="c-text f-8" style="padding-left: 750px;">
								<b>Fecha: </b><?php echo date('d/m/Y H:m:s')?><br>
								<b>Usuario: </b><?php echo Session::get('datos-loggeo')->datos->datosUsuarios->nombreApellido?>
							</td>
					</tr>
					<tr>
						<td colspan="6" class="c-text f-17">
							<br>
							<br>
							<br>
						</td>
					</tr>
					<tr>
						<td colspan="6" class="c-text f-17">
							<b>CUENTAS POR PAGAR</b>
						</td>
					</tr>
				</table>
				<br>
								<table style="width: 100%;" >
									<tr>
										<th class="c-text f-8">Proveedor</th>
										<th class="c-text f-8">Nro. Documento</th>	
										<th class="c-text f-8">Tipo Documento</th>	
										<th class="c-text f-8">Emisión</th>	
										<th class="c-text f-8">Vencimiento</th>	
										<th class="c-text f-8">Atraso</th>	
										<th class="c-text f-8">Corte</th>	
										<th class="c-text f-8">Moneda</th>	
										<th class="c-text f-8">Importe Documento</th>	
										<th class="c-text f-8">Saldo</th>	
										<th class="c-text f-8">Proforma</th>	    
									</tr>
									@foreach($listado['data'] as $lista)
										<?php
											$total_importe = 0;
											$saldo_neto = 0;
										?>
										@foreach($lista->detalles as $datos)
											<?php
												if($datos['moneda'] == 'PYG'){	
													$importe  = number_format($datos['importe'], 0, ",", ".");
													$saldoneto  = number_format($datos['saldo_neto'], 0, ",", ".");
												}else{													
													$importe  = number_format($datos['importe'], 2, ",", ".");
													$saldoneto  = number_format($datos['saldo_neto'], 2, ",", ".");
												}	
											?>


											<tr>
												<td style="border:1px solid black;" class="c-text f-8">{{$lista->proveedor}}</td>
												<td style="border:1px solid black;" class="c-text f-8">{{$datos['nro_documento']}}</td>
												<td style="border:1px solid black;" class="c-text f-8">{{$datos['tipo_documento']}}</td>
												<td style="border:1px solid black;" class="c-text f-8">{{$datos['fecha_emision']}}</td>
												@if($datos['fecha_vencimiento'] == '31/12/1969')
													<td style="border:1px solid black;" class="c-text f-8"></td>
												@else
													<td style="border:1px solid black;" class="c-text f-8">{{$datos['fecha_vencimiento']}}</td>
												@endif
												<td style="border:1px solid black;" class="c-text f-8">{{$datos['atraso']}}</td>
												@if($datos['corte'] != "")
													<td style="border:1px solid black;" class="c-text f-8">{{$datos['corte']}}</td>
												@else
													<td style="border:1px solid black;" class="c-text f-8">0-30</td>
												@endif
												<td style="border:1px solid black;" class="c-text f-8">{{$datos['moneda']}}</td>
												<td style="border:1px solid black;" class="c-text f-8">{{$importe}}</td>
												<td style="border:1px solid black;" class="c-text f-8"><b>{{$saldoneto}}</b></td>
												<td style="border:1px solid black;" class="c-text f-8">{{$datos['proforma']}}</td>
											</tr>
											<?php
												$total_importe = $total_importe+$datos['importe'];
												$saldo_neto = $saldo_neto+$datos['saldo_neto'];
											?>
										@endforeach
											<?php
												if($lista->moneda == 'PYG'){
													$total_importe =  number_format($total_importe, 0, ",", ".");
													$saldo_neto = number_format($saldo_neto, 0, ",", ".");
												}else{
													$total_importe = number_format($total_importe, 2, ",", ".");
													$saldo_neto = number_format($saldo_neto, 2, ",", ".");
												}
											?>
											<tr style="background-color:#EAECEE" >
												<th colspan="8" class="l-text f-8">TOTAL: </th>
												<th class="c-text f-8">{{$total_importe}}</th>
												<th class="c-text f-8">{{$saldo_neto}}</th>
												<th colspan="3" class="c-text f-8"></th>
											</tr>
									@endforeach
								</table>
						<br>
					<br>
			</div>
		</div>
		<br>
</body>
</html>
