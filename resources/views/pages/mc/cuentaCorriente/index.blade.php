@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style type="text/css">
	
	.card,.card-header {
					border-radius: 14px !important;
					}
        .select2{
                width: 100% !important;
            }  
	</style>
@endsection
@section('content')
<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Cuenta Corriente</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


				<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="tableCttaCtte" data-toggle="tab" href="#cabecera">Reporte de
							Cuentas Corrientes</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tableCttaCtteDetalle" data-toggle="tab" href="#detalle">Reporte de
							Cuentas Corrientes Detalle</a>
					</li>
				</ul>

				<div class="tab-content">
					<!-- Main content -->
					<section id="cabecera" class="tab-pane active mt-1" role="tabpanel">
						@include('flash::message')
						<form id="verCuentaCorriente" method="get" class="row">

							

							<div class="col-12 col-sm-3 col-md-4">
								<div class="form-group">
									<label>Tipo </label>
									<select class="form-control input-sm select2" name="tipo" id="tipo">
										<option value="C">Cliente</option>
										<option value="P">Proveedor</option>
									</select>
								</div>
							</div>

							<div class="col-12 col-sm-3 col-md-4">
								<div class="form-group">
									<label>Persona <div class="spinner-border" id="spinnerLoadPersona" style="display:none; width: 1rem; height: 1rem;" role="status"><span class="sr-only"></span></div></label>
									<select class="form-control input-sm select2" name="persona" id="persona">
										<option value="">Todos</option>
										@foreach($personas as $persona)
										<option value="{{$persona->id}}">{{$persona->data_n}}
										</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-12 col-sm-3 col-md-4	">
								<div class="form-group">
									<label>Moneda</label>
									<select class="form-control select2" name="idMoneda" id="moneda" tabindex="1">
										<option value="">Todos</option>
										@foreach($currencys as $moneda)
										<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
										@endforeach
									</select>
								</div>
							</div>


							<div class="col-12 mb-1">
								<button type="button" onclick="limpiarFiltros()"
									class="btn btn-light btn-lg text-white pull-right mr-1"><b>Limpiar</b></button>
								<button type="button" onclick="consultarCttaCtte()"
									class="pull-right  btn btn-info btn-lg mr-1"><b>Buscar</b></button>
							</div>
						</form>


						<div class="table-responsive">
							<table id="listado" class="table nowrap" style="width: 100%">
								<thead style="text-align: center">
									<tr>
										<th>Persona</th>
										<th>Tipo</th>
										<th>Moneda</th>
										<th>Debe</th>
										<th>Haber</th>
										<th>Saldo</th>
										<th></th>
									</tr>
								</thead>

								<tbody style="text-align: center">

								</tbody>
							</table>
						</div>


					</section>


					<!-- 
	======================================================================================================	
	sección reporte de liquidaciones detalle 
	======================================================================================================
-->


					<section id="detalle" class="tab-pane mt-1" role="tabpanel">
						@include('flash::message')
						<form id="verCtteDetalle" class="row">

							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label class="control-label">Moneda</label>
									<input type="text" class="form-control" name="monedaDesc" id="monedaDesc" value=""
										readonly="" />
								</div>
							</div>
							{{-- <form action="{{route('getDetallesCuenta')}}" id="frmBusqueda" method="get"> --}}
							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Fecha Desde/Hasta</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
										<input type="text" class="form-control pull-right fecha" name="periodo"
											id="periodo" required>
									</div>
								</div>
							</div>

							<div class="col-12 col-sm-3 col-md-3">
								<div class="form-group">
									<label>Documentos</label>
									<select class="form-control input-sm select2" required name="documento" id="visible"
										style="padding-left: 0px;width: 100%;">
										<option value="31">Documentos Pendientes</option>
										<option value="0">Todos los Documentos</option>
									</select>
								</div>
							</div>

							<input type="hidden" class="form-control" name="persona_id" id="persona_id" value=""
								hidden="hidden" />
							<input type="hidden" class="form-control" name="tipo" id="tipoDetalle" value=""
								hidden="hidden" />
							<input type="hidden" class="form-control" name="moneda_id" id="moneda_id" value=""
								hidden="hidden" />


							<div class="col-12">
								<button type="button" id="btnBuscar"
									class="btn btn-info btn-lg pull-right mb-1"><b>Buscar</b></button>
							</div>

						</form>



						<div class="table-responsive">
							<table id="listadoDetalle" class="table nowrap" style="width: 100%">
								<thead style="text-align: center">
									<tr>
										<th>Fecha</th>
										<th>Tipo Documento</th>
										<th>Persona</th>
										<th>Documento</th>
										<th>Debe</th>
										<th>Haber</th>
										<th>Saldo</th>
									</tr>
								</thead>
								<tbody style="text-align: center">

								</tbody>
								<tfoot>
									<tr>
										<th style="text-align: center;">TOTAL</th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;"></th>
										<th style="text-align: center;" id="total_debe" class="format-number-control">
										</th>
										<th style="text-align: center;" id="total_haber" class="format-number-control">
										</th>
										<th style="text-align: center;" id="total_saldo" class="format-number-control">
										</th>
									</tr>
								</tfoot>
							</table>
						</div>


					</section>
					<!-- /.content -->
				</div>



			</div>
		</div>
	</div>
</section>




@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>
		var tableCttaCtte;
		var tableCttaCtteDetalle;

		$(document).ready(function() 
		{
			$('.select2').select2();
			var periodo = '';
			// console.log(periodo);

    		// tableCttaCtte = consultarCttaCtte ();
			//tableCttaCtteDetalle = consultarDetalleCttaCtte();
		});


		var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();

		$('.format-number-control').inputmask("numeric", {
		    radixPoint: ",",
		    groupSeparator: ".",
		    digits: 2,
		    autoGroup: true,
		    // prefix: '$', //No Space, this will truncate the first character
		    rightAlign: false
		});


		$('#tipo').change(()=>{
			cargarListaPersona();
		});


		function cargarListaPersona(){
			$('#spinnerLoadPersona').show();

			$.ajax({
					async: false,
					type: "GET",
					url: "{{route('getTipoPersona')}}",
					data: {'tipo':$('#tipo').val()},
					dataType: 'json',
					error: function (jqXHR, textStatus, errorThrown) {
						$.toast({
							heading: '<b>Error</b>',
							position: 'top-right',
							text: 'Ocurrio un error en la comuniciòn con el servidor !',
							width: '400px',
							hideAfter: false,
							icon: 'error'
						});

					},
					success: function (rsp) {

						if (rsp.err & rsp.result.length > 0) {
							$('#persona').empty();

								var newOption = new Option('Todos', '', false, false);
								$('#persona').append(newOption);

								$.each(rsp.result, function (key, item){
								var newOption = new Option(item.data_n, item.id, false, false);
								$('#persona').append(newOption)
								});

						} else {
							console.log('Error al cargar personas');
						}

					} //success
				}).done(()=>{
					setTimeout(function(){ $('#spinnerLoadPersona').hide(); }, 1500);
				})
		}


		$('input[name="periodo"]').daterangepicker({
													timePicker24Hour: true,
													timePickerIncrement: 30,
													locale: {
														    format: 'DD/MM/YYYY',
														    cancelLabel: 'Limpiar',
														    applyLabel: 'Aplicar',					
											                fromLabel: 'Desde',
											                toLabel: 'Hasta',
											                customRangeLabel: 'Seleccionar rango',
											                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
											                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
											                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
											                    'Diciembre']
														    },
													startDate: moment().subtract(30, 'days') ,
	        				// 						endDate: new Date(),
									    			});
		$('input[name="periodo"]').val('');

		const formatter = new Intl.NumberFormat('de-DE', 
		{
			currency: 'USD',
			minimumFractionDigits: 2
		});


		$('.checkCol').on('click',function()
			{
				var form = $('#columLiqExcel').serializeArray();
				valor = [];
				// valor.push(0,1,2,8);
				valor.push(0,1,2,3,4,6,7,11);
				$.each(form, function(item,value){
					valor.push(parseInt(value.value));
				});

				buttonsLiq(valor,tableLiquidacionesDetalle);
			});

		function cargarReporteLiquidacionDetalle(id_vendedor, periodo, renta_total, comision_total)
		{
			$('#vendedorDetalle').val(id_vendedor).trigger('change.select2');
			$('#periodoDetalle').val(periodo).trigger('change.select2');
			$('#renta_total').val(renta_total);
			$('#comision_total').val(comision_total);
			$('#tableLiquidacionesDetalle').tab('show');

			consultarCttaCtte();

		}

		function limpiarFiltrosDetalle()
		{
			// $('#tipo').val('').trigger('change.select2');
			// $('#persona').val('').trigger('change.select2');
			// $('#moneda').val('').trigger('change.select2');
			// consultarCttaCtte();
		}

		function limpiarFiltros()
		{
			$('#tipo').val('').trigger('change.select2');
			$('#persona').val('').trigger('change.select2');
			$('#moneda').val('').trigger('change.select2');
			consultarCttaCtte();
		}

		function formatearFecha(texto)
 		{
	        if(texto != '' && texto != null)
	        {
	          	return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	        } 
	        else 
	        {
	         	return '';    
         	}
        }

		function consultarCttaCtte()
		{ 

			let form = $('#verCuentaCorriente').serializeJSON({
		        customTypes: customTypesSerializeJSON
		    });


			var tableLiquidaciones = $("#listado").DataTable
			({
				"destroy": true,
				"ajax": 
				{
					"url": "{{route('getCuentaCorriente')}}",
					"type": "GET",
					"data": form,
					error: function(jqXHR,textStatus,errorThrown)
					{
		                $.toast
		                ({
		                    heading: 'Error',
		                    text: 'Ocurrió un error en la comunicación con el servidor.',
		                    position: 'top-right',
		                    showHideTransition: 'fade',
		                    icon: 'error'
		                });

		                }			    
					},
				"columns": 
				[
					{ "data": "nombre" },
					{ "data": function(x)
						{
							// console.log(x);
							tipo = "";
							if(x.tipo == 'P'){
								tipo = "Proveedor";
							}
							if(x.tipo == 'C'){
								tipo = "Cliente";
							}
							return tipo;
						} 
					},
					{ "data": "currency_code" },
					{ "data": function(x)
						{
							return formatter.format(parseFloat(x.debe));
						} 
					}, 
					{ "data": function(x)
						{
							return formatter.format(parseFloat(x.haber));
						} 
					}, 
					{ "data": function(x)
						{
							return formatter.format(parseFloat(x.saldo));
						} 
					}, 
				
					{ "data": function(x)
						{
							var btn = `<button onclick="cargarReporteCuentasDetalle('`+x.tipo+`',`+x.id_persona+`,`+x.id_moneda+`, '`+x.currency_code+`')" class="btn btn-danger" type="button">
							<i class="fa fa-fw fa-search"></i></button>`;

							return btn;
						} 
					}

					]
						
				});	

				return tableLiquidaciones;
			};

			function cargarReporteCuentasDetalle(tipo, persona,moneda,moneda_des)
			{ 
				$("#persona_id").val(persona);
				$("#tipoDetalle").val(tipo);
				$("#moneda_id").val(moneda);
				$("#monedaDesc").val(moneda_des);

				var tableLiquidacionesDetalle = $("#listadoDetalle").DataTable
				({
					"destroy": true,
					"ordering": false,
					"ajax": 
					{
						"url": "{{route('consultarDetalleCuentas')}}",
						"type": "GET",
						"data": {"formSearch": $('#verCtteDetalle').serializeArray() },
						error: function(jqXHR,textStatus,errorThrown)
						{
		                }			    
					},
				
					"columns": 
					[
						{ "data": function(x)
							{	if(Boolean(x.fecha_hora)){
								var fecha = x.fecha_hora.split(' ');
								var f = formatearFecha(fecha[0]);
								return f +' '+fecha[1];
							}
							return '';
								
							} 
						},
						{ "data": "denominacion" },
						{ "data": "nombre" },
						{ "data": "documento" },
						{ "data": function(x)
						{
							return formatter.format(parseFloat(x.debe));
						} 
					}, 
					{ "data": function(x)
						{
							return formatter.format(parseFloat(x.haber));
						} 
					}, 
					{ "data": function(x)
						{
							return formatter.format(parseFloat(x.saldo));
						} 
					}, 
				
					],

					"fnInitComplete": function(oSettings, json) 
						{
							// console.log(json.totales.total_debe);
							$('#total_debe').html(json.totales.total_debe);
							$('#total_haber').html(json.totales.total_haber);
							$('#total_saldo').html(json.totales.saldo);
	                    },
	                  

			});
			$('#tableCttaCtteDetalle').tab('show');
			

	}


	</script>
@endsection
