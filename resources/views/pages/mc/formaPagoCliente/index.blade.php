@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
  @parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
  @include('flash::message') 
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Forma de Pago Cliente</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-body pt-0">

				</div>	
       


        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
                <table id="listado" class="table">
                  <thead>
                    <tr>
                      <th>Denominacion</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach ($formas_pagos as $forma_pago)
                        <tr>
                          <td>{{ $forma_pago->denominacion }}</td>
                          <td>
                            @if( $forma_pago->select_cuenta)
                            <a href="{{route('forma_pago.asignarCuentaContable',[ 'id' => $forma_pago->id])}}" class="btn btn-info" title="Asignar Cuentas Contables" style="padding-left: 6px;padding-right: 6px;margin-right: 2px;" role="button">
                              <i class="fa fa-fw fa-tasks"></i>
                            </a>
                            @endif
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
                </table>
            </div> 
        </div> 
    </div>        
</section>


@endsection
@section('scripts')
  @include('layouts/gestion/scripts')

@endsection