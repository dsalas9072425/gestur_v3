@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">agregar GRUPOS</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmGrupo" method="post" action="{{route('storeGrupo')}}" style="margin-top: 2%;">
					@include('flash::message')     
					<div class="box box-default" style="border-top-width: 1px;margin-bottom: 0px;">
						<div class="box-header with-border">
							<h3 class="box-title">Datos del grupo</h3>
							<br>
							<small>Todos los campos son obligatorios</small>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							@if (count($errors) > 0)
							   <div class = "alert alert-danger">
							      <ul>
							         @foreach ($errors->all() as $error)
							            <li>{{ $error }}</li>
							         @endforeach
							      </ul>
							   </div>
							@endif
							<!-- /.box -->
							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Denominación</label>
										<input type="text" class = "form-control" name="denominacion" id="denominacion" placeholder="Denominación" value="{{ old('denominacion')}}" maxlength="70"/>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label>Posee ticket</label>
										<select data-placeholder="Seleccione destino" id="posse_ticket" name="posse_ticket"  style="width: 100%;" class="select2">
											<option value="true" selected="select">SI</option>
											<option value="false">NO</option>
										</select> 
									</div>
								</div>


								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Cantidad Bloqueos</label>
										<input type="number" class = "form-control" name="cantidad_bloqueos" id="cantidad_bloqueos" placeholder="Cantidad Bloqueos" value="0"/>
									</div>
								</div>

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
									<div class="form-group">
										<label class="control-label">Cantidad Liberados</label>
										<input type="number" class = "form-control" name="cantidad_liberados" id="cantidad_liberados" placeholder="Cantidad Liberados" value="0"/>
									</div>
								</div>

							</div>  

							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Código Bloqueo</label>
										<input type="text" class = "form-control" name="codigo_bloqueo" id="codigo_bloqueo" placeholder="Código Bloqueo" value="{{ old('codigo_bloqueo')}}"/>
									</div>
								</div> 	
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label>Destino</label>
										<select data-placeholder="Seleccione destino" id="itemList" name="destino"  style="width: 100%;">
											@foreach($valorDestinos as $key=>$destino)
											<option value="{{$destino['id']}}" selected="select">{{$destino['value']}}</option>
											@endforeach
										</select> 
									</div>
								</div>

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;">
									<div class="form-group">
										<label>Fecha de emisión</label>						 
										<div class="input-group">
											<div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right fecha" name="fecha_emision" id="fecha_emision">
										</div>
									</div>							
								</div>

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px; padding-right: 25px">
									<div class="form-group">
										<label>Fecha de salida</label>						 
										<div class="input-group">
											<div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right fecha" name="fecha_salida" id="fecha_salida">
										</div>
									</div>							
								</div> 	
							</div>

							<div class="row" style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px; padding-right: 15px;">
									<div class="form-group">
										<label>Fecha de seña</label>						 
										<div class="input-group">
											<div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right fecha" name="fecha_seña" id="fecha_seña">
										</div>
									</div>							
								</div> 	 	
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label>Moneda</label>					            	
										<select class="form-control input-sm select2"  name="moneda" id="moneda" style="padding-left: 0px;width: 100%;">
											<option value="">Seleccione moneda</option>
											@foreach($currencys as $key=>$currency)
											<option value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
											@endforeach	
										</select>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label>Estado</label>					            	
										<select class="form-control input-sm" disabled="disabled" name="estado" id="estado" style="    padding-left: 0px;width: 100%;">
											<option value="11">Disponible</option>
										</select>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
									<div class="form-group">
										<label>Aerolínea</label>					            	
										<select class="form-control input-sm select2" name="aerolinea" id="aerolinea" style="padding-left: 0px;width: 100%;">
											<option value="">Seleccione aerolínea</option>
											@foreach($aerolineas as $key=>$aerolinea)
											<option value="{{$aerolinea->id}}">{{$aerolinea->nombre}}</option>
											@endforeach	
										</select>
									</div>
								</div>
							</div>

							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">									
									<label>Comisión</label>
									<div class="form-group">
										<input type="radio" name="comision" class="comision" value=0> Neto<br>
										<input type="radio" name="comision" class="comision" value=1 checked="checked"> Comisionable<br>
									</div>
								</div>

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">PNR</label>
										<input type="text" class = "form-control" name="PNR" id="PNR" placeholder="PNR" value="{{ old('PNR') }}" style="text-transform: uppercase"/>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Importe Facial</label>
										<input type="text" class = "form-control numeric" name="facial" id="facial" placeholder="Importe Facial" value="{{ old('facial') }}" onkeypress="return justNumbers(event);"/>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
									<div class="form-group">
										<label class="control-label">Tasas</label>
										<input type="text" class = "form-control numeric" name="tasa" id="tasa" placeholder="Tasas" value="{{ old('tasa') }}" onkeypress="return justNumbers(event);"/>
									</div>
								</div>
							</div>

							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Variación Impuesto</label>
										<input type="text" class = "form-control numeric" name="variacion_impuesto" id="variacion_impuesto" placeholder="Variación Impuesto" value="{{ old('variacion_impuesto') }}" onkeypress="return justNumbers(event);"/>
									</div>
								</div>  

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Seña Aerolinea </label>
										<input type="text" class = "form-control numeric" name="monto_seña" id="monto_seña" placeholder="Monto seña" value="{{ old('monto_seña') }}" onchange="formatNumber.new(this)" onkeypress="return justNumbers(event);"/>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
									<div class="form-group">
										<label class="control-label">Seña Paquete </label>
										<input type="text" class = "form-control numeric" name="monto_paquete" id="monto_paquete" placeholder="Monto seña" onchange="formatNumber.new(this)" onkeypress="return justNumbers(event);"/>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label>Origen Ticket</label>					            
										<select class="form-control input-sm select2" name="tipo_ticket" id="tipo_ticket" style="padding-left: 0px;width: 100%;">
											<option value=" ">Seleccione tipo de ticket</option>
											@foreach($tipos_tickets as $key=>$tipo_ticket)
											<option value="{{$tipo_ticket->id}}">{{$tipo_ticket->descripcion}}</option>
											@endforeach	
										</select>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
									    <label>Cliente </label>
									    <select class="form-control select2" name="cliente_id"  id="cliente_id" tabindex="1" style="width: 100%;">
													<option value="">Seleccione Cliente</option>
													@foreach($clientes as $cliente)
														@php
															$ruc = $cliente->documento_identidad;
															if($cliente->dv){
																$ruc .= $ruc."-".$cliente->dv;
															}
														@endphp
														<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->nombre}} {{$cliente->apellido}} - {{$cliente->denominacion}}</option>
													@endforeach
									            </select>
									        </div>

								</div>

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
										<div class="form-group">
											<label>Usuario </label>
											<select class="form-control select2" name="id_usuario"  id="id_usuario" tabindex="1" style="width: 100%;">
														<option value="">Seleccione Cliente</option>
														@foreach($usuario as $us)
															<option value="{{$us->id}}">{{$us->usuario_n}}</option>
														@endforeach
													</select>
												</div>
	
									</div>
									
								<input type="hidden" name="imagen_documento" id="imagen">
								<div id="outputPenalidades" style="display:none " >

								</div>
								<div id="outputPagos" style="display:none ">

								</div>

							</form>	
							<form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{route('uploadDocument')}}" autocomplete="off">
									<div class="form-group">
										<div id="validation-errors"></div>
										<div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 15px;padding-right: 25px;">
											<label class="control-label" style="margin-left: 3%; width: 96%">Adjuntar Documento</label>
											<input type="hidden" name="_token" value="{{ csrf_token() }}" />
											<input type="file" class="form-control" name="image" id="image" style="margin-left: 3%; width: 96%"/> 
										</div> 
										<div class="row"  style="margin-bottom: 30px;">
											<div class="col-xs-12 col-sm-1 col-md-1">
											</div>
											<div class="col-xs-12 col-sm-1 col-md-1">
												<div id="output" style="width: 100px;height: 100px;">
												</div>
											</div> 
											<div class="col-xs-12 col-sm-1 col-md-1">
												<div id="btn-delete">
												</div>
											</div>
										</div> 	
									</div>  
							</form>
							<div class="col-xs-12 col-sm-12 col-md-12">
							<h3 class="box-title" style="padding-left: 10px;">Penalidades</h3>
								<div class="table-responsive" style="margin-left: 2%;">
					              <table id="listadoPenalidades" class="table" style="width:97%">
					                <thead>
										<tr>
											<th>Fecha</th>
											<th>Monto</th>
											<th></th>
							            </tr>
					                </thead>
					                <tbody  style="text-align: center">
					                </tbody>
					                <tfoot  style="text-align: center">
										<tr>
												<td>
													<div class="input-group">
										    			<div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">
										    				<i class="fa fa-calendar"></i>
										    			</div>
										    			<input type="text"  class="form-control pull-right fecha" name="fechaPenalidad_0" id="fecha_penalidad_0" placeholder="Fecha de Penalidad" value="">
										    		</div>
												</td> 
												<th>
													<input type="text" class = "form-control numeric" name="monto_penalidades_0" id="montoPenalidad_0" placeholder="Monto de Penalidad" value="0" onchange="formatNumber.new(this)" onkeypress="return justNumbers(event);"/>
												</td>
												<td>
													<a onclick="guardarFila()" title="Guardar Fila" class="btn btn-success" style="margin-left:5px;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-plus"></i></a>
												</td>
							            </tr>
							        </tfoot>
					              </table>
								</div>		
								<br>
								<br>
								<h3 class="box-title" style="padding-left: 10px;">Calendario de Pagos</h3>
								<div class="table-responsive" style="margin-left: 2%;">
					              <table id="listadoPagos" class="table" style="width:97%">
					                <thead>
										<tr>
											<th>Fecha</th>
											<th>Proveedor</th>
											<th>Monto</th>
											<th></th>
							            </tr>
					                </thead>
					                <tbody  style="text-align: center">
					                </tbody>
					                <tfoot  style="text-align: center">
										<tr>
												<td>
													<div class="input-group">
										    			<div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">
										    				<i class="fa fa-calendar"></i>
										    			</div>
										    			<input type="text"  class="form-control pull-right fecha" name="fechaPago_0" id="fecha_pago_0" placeholder="Fecha de Pago" value="">
										    		</div>
												</td> 
												<td>										            
													<select class="form-control select2" name="proveedorPago_0"  id="proveedor_0" tabindex="1" style="width: 100%;" required>
														<option value="0">Seleccione Proveedor</option>
														@foreach($proveedores as $proveedor)
															@php
																$ruc = $proveedor->documento_identidad;
																if($proveedor->dv){
																	$ruc .= $ruc."-".$proveedor->dv;
																}
															@endphp
															<option value="{{$proveedor->id}}">{{$ruc}} - {{$proveedor->nombre}} {{$proveedor->apellido}} {{$proveedor->denominacion}}</option>
														@endforeach
									            	</select>
									            </td> 	
												<th>
													<input type="text" class = "form-control numeric" name="monto_pago_0" id="monto_pago_0" placeholder="Monto de Pago" value="0" onchange="formatNumber.new(this)" onkeypress="return justNumbers(event);"/>
												</td>
												<td>
													<a onclick="guardarFilaPagos()" title="Guardar Fila" class="btn btn-success" style="margin-left:5px;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-plus"></i></a>
												</td>
							            </tr>
							        </tfoot>
					              </table>
								</div>	
								<br>
								<br>
		
								<br>
								<div class="row">
									<div class="col-xs-12 col-sm-11 col-md-11" style="padding-left: 15px;">
									</div>
									<div class="col-xs-12 col-sm-1 col-md-1" style="padding-left: 15px;margin-bottom: 10px;">
										<button type="submit" form="frmGrupo" id="btnGuardar" class="btn btn-success btn-lg" >Guardar</button>
									</div>
								</div>
								</div>
							</div>
						</div>	
						</div>
			</div>
		</div>
	</section>
	</section>



@endsection
@section('scripts')
@include('layouts/gestion/scripts')

<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script>
	$(document).ready(function() {

		$('.select2').select2();

		$("#listado").dataTable({
			"aaSorting":[[0,"desc"]]
		});

		$( "#fecha_seña" ).datepicker({ 
			altFormat: 'dd/mm/yy',
			dateFormat: 'yy-mm-dd'
		});

		$( "#fecha_salida" ).datepicker({ 
			altFormat: 'dd/mm/yy',
			dateFormat: 'yy-mm-dd'
		});

		$( "#fecha_emision" ).datepicker({ 
			altFormat: 'dd/mm/yy',
			dateFormat: 'yy-mm-dd'
		});

		$(".fecha" ).datepicker({ 
			altFormat: 'dd/mm/yy',
			dateFormat: 'yy-mm-dd'
		});

		$("#itemList").select2({
			ajax: {
				url: "{{route('destinoGrupo')}}",
				dataType: 'json',
				placeholder: "Seleccione un destino",
				delay: 0,
				data: function (params) {
					return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					            	var results = $.map(data, function (value, key) {
					            		return {
					            			children: $.map(value, function (v) {
					            				return {
					            					id: key,
					            					text: v
					            				};
					            			})
					            		};
					            	});
					            	return {
					            		results: results,
					            	};
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					        	return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
					    });

		$('.numeric').inputmask("numeric", {
        	radixPoint: ",",
        	groupSeparator: ".",
        	digits: 2,
        	autoGroup: true,
        	// prefix: '$', //No Space, this will truncate the first character
        	rightAlign: false,
        	oncleared: function () { self.Value(''); }
		});
	});

	var options = { 
		beforeSubmit:  showRequest,
		success: showResponse,
		dataType: 'json' 
	}; 

	$('body').delegate('#image','change', function(){
		$('#upload').ajaxForm(options).submit();  		
	});

	function showRequest(formData, jqForm, options) { 
		$("#validation-errors").hide().empty();
		$("#output").css('display','none');
		return true; 
	} 

	$("#posse_ticket").change(function(){
		console.log($(this).val());
		if($(this).val() == 'false'){
			$("#cantidad_bloqueos").prop("disabled", true);
			$("#cantidad_liberados").prop("disabled", true);
			$("#tasa").prop("disabled", true);
			$("#facial").prop("disabled", true);
			$("#PNR").prop("disabled", true);
			$("#iva").prop("disabled", true);
			$("#variacion_impuesto")
			$("#tipo_ticket").prop("disabled", true);
			$("#codigo_bloqueo").prop("disabled", true);
			$("#aerolinea").select2().enable(false);
			$("#monto_seña").prop("disabled", true);
			$("#variacion_impuesto").prop("disabled", true);
			$("#fecha_seña").prop("disabled", true);
			$("#fecha_emision").prop("disabled", true);
			$("#fecha_salida").prop("disabled", true);
			$(".comision").prop('disabled', true);
		}else{
			$("#cantidad_bloqueos").prop("disabled", false);
			$("#cantidad_liberados").prop("disabled", false);
			$("#tasa").prop("disabled", false);
			$("#facial").prop("disabled", false);
			$("#PNR").prop("disabled", false);
			$("#iva").prop("disabled", false);
			$("#variacion_impuesto").prop("disabled", false);
			$("#tipo_ticket").prop("disabled", false);
			$("#codigo_bloqueo").prop("disabled", false);
			$("#aerolinea").select2().enable(true);
			$("#monto_seña").prop("disabled", false);
			$("#variacion_impuesto").prop("disabled", false);
			$("#fecha_seña").prop("disabled", false);
			$("#fecha_emision").prop("disabled", false);
			$("#fecha_salida").prop("disabled", false);
			$(".comision").prop('disabled', false);
		}

	});

	function showResponse(response, statusText, xhr, $form)  { 
               
                        if(response.success == false)
                          {
                            var arr = response.errors;
                            
                            $.each(arr, function(index, value)
                            {
                              if (value.length != 0)
                              {
                                $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                              }
                            });
                            $("#validation-errors").show();
                        } else {
                             $("#imagen").val(response.archivo); 
                          
                          var eliminar = response.archivo;
                                $('#output').html('');
                                 $("#btn-delete").html('');
                                $("#output").html(`<img class="img-responsive" style="width:120px; height:120px;" src="{{asset('images/file.png')}}" alt="Photo">`);
			                    $("#output").css('display','block');
			                    $("#btn-delete").html( ` <button type="button" id="btn-imagen" onclick="eliminarImagen('${eliminar}')">
			                    <i class="glyphicon glyphicon-remove"></i>
			                    </button>`);
                   
                             $("#image").prop('disabled',true);
                        }
                   }

			       function eliminarImagen(archivo){
                     
                   // $("#btn-imagen").on("click", function(e){
                      $.ajax({
                          type: "GET",
                          url: "{{route('fileEliminar')}}",//fileDelDTPlus
                          dataType: 'json',
                          data: {  
                               dataFile:archivo
                                    },


                            error: function(jqXHR,textStatus,errorThrown){
                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error al intentar eliminar la imagen.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        },
                          success: function(rsp){
                            if(rsp.msg == true){
                            document.getElementById("image").value = "";
                            $("#output").html('');
                            $("#btn-delete").html('');
                                
                            } else {
                          
                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error al intentar eliminar la imagen.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                        }

                            $("#image").prop('disabled',false);
                          }//function
                   //   })      

                    });
                  } 
 

       	function guardarFilaSenha(){
       		if($("#fecha_senha_0").val() != "" && $("#monto_senha_0").val() != 0){
       		   console.log($("#frmPenalidades").serialize());
       		  // $("#outputPenalidades").
       		    console.log($("#fecha_senha_0").val());
       		    console.log($("#monto_senha_0").val());
       		    var nFilas = $("#listadoSenhales tr").length;
       		    i = nFilas - 1;
       		    detalle = '';
       		    tablas = '';
       		  	detalle += '<div id="row'+i+'"><input type="text" maxlength="2" required value = "'+$("#fecha_senha_0").val()+'" class = "Requerido form-control input-sm" name="senha['+i+'][fecha]" id="comentario"/>';	
				detalle += '<input type="text" maxlength="2" required value = "'+$("#monto_senha_0").val()+'" class = "Requerido form-control input-sm" name="senha['+i+'][monto]" id="comentario"/></div>';	

				tablas += '<tr id="rows'+i+'">';
				tablas += '<td><div class="input-group"><div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;"><i class="fa fa-calendar"></i></div><input type="text"  class="form-control pull-right fecha" name="senha['+i+'][fecha]" placeholder="Fecha de Penalidad" value = "'+$("#fecha_senha_0").val()+'" value=""></div></td>';
				tablas += '<td><input type="text" maxlength="2" required value = "'+$("#monto_senha_0").val()+'" class = "Requerido form-control input-sm" name="senha['+i+'][monto]"/></td>';
				tablas += '<td><a onclick="eliminarFilaSenhas('+i+')" title="Eliminar" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-times"></i></a></td>';
				tablas += '</tr>';

				console.log(tablas);

				$("#outputPenalidades").append(detalle);	
				$('#listadoSenhales tbody').append(tablas);

				$("#fecha_senha_0").val("");
				$("#monto_senha_0").val(0);
				$(".fecha" ).datepicker({ 
					altFormat: 'dd/mm/yy',
					dateFormat: 'yy-mm-dd'
				});
			}else{
				 $.toast({
						    heading: 'Error',
						    text: 'Ingrese algun dato para proceder al guardado',
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});

			}	

       	}

       	function guardarFila(){
       		if($("#fecha_penalidad_0").val() != "" && $("#montoPenalidad_0").val() != 0){
       		   console.log($("#frmPenalidades").serialize());
       		  // $("#outputPenalidades").
       		    console.log($("#fecha_penalidad_0").val());
       		    console.log($("#montoPenalidad_0").val());
       		    var nFilas = $("#listadoPenalidades tr").length;
       		    i = nFilas - 1;
       		    detalles = '';
       		    tablas = '';
       		  	detalles += '<div id="row'+i+'"><input type="text" maxlength="2" required value = "'+$("#fecha_penalidad_0").val()+'" class = "Requerido form-control input-sm" name="penalidad['+i+'][fecha]" id="comentario"/>';	
				detalles += '<input type="text" maxlength="2" required value = "'+$("#montoPenalidad_0").val()+'" class = "Requerido form-control input-sm" name="penalidad['+i+'][monto]" id="comentario"/></div>';	

				tablas += '<tr id="rows'+i+'">';
				tablas += '<td><div class="input-group"><div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;"><i class="fa fa-calendar"></i></div><input type="text"  class="form-control pull-right fecha" name="data['+i+'][fecha]" placeholder="Fecha de Penalidad" value = "'+$("#fecha_penalidad_0").val()+'" value=""></div></td>';
				tablas += '<td><input type="text" maxlength="2" required value = "'+$("#montoPenalidad_0").val()+'" class = "Requerido form-control input-sm" name="data['+i+'][monto]"/></td>';
				tablas += '<td><a onclick="eliminarFila('+i+')" title="Eliminar" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-times"></i></a></td>';
				tablas += '</tr>';

				console.log(tablas);

				$("#outputPenalidades").append(detalles);	
				$('#listadoPenalidades tbody').append(tablas);

				$("#fecha_penalidad_0").val("");
				$("#montoPenalidad_0").val(0);
				$(".fecha" ).datepicker({ 
					altFormat: 'dd/mm/yy',
					dateFormat: 'yy-mm-dd'
				});
			}else{
				 $.toast({
						    heading: 'Error',
						    text: 'Ingrese algun dato para proceder al guardado',
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});

			}	

       	}


		function eliminarFila(idFila){
			$('table#listadoPenalidades tr#rows'+idFila).remove();
			$("#row"+idFila).remove();

		}

		function eliminarFilaSenhas(idFila){
			$('table#listadoSenhales tr#rows'+idFila).remove();
			$("#row"+idFila).remove();

		}



      	function guardarFilaPagos(){
       		if($("#fecha_pago_0").val() != "" && $("#monto_pago_0").val() != 0 && $("#proveedor_0").val() != 0){
       		   console.log($("#frmPenalidades").serialize());
       		  // $("#outputPenalidades").
       		    console.log($("#fecha_pago_0").val());
       		    console.log($("#monto_pago_0").val());
       		    var data = $('#proveedor_0').select2('data');
       		    console.log(data[0].text);
       		    var nFilas = $("#listadoPagos tr").length;
       		    i = nFilas - 1;
       		    detalle = '';
       		    tablas = '';
       		  	detalle += '<div id="row'+i+'"><input type="text" maxlength="2" required value = "'+$("#fecha_pago_0").val()+'" class = "Requerido form-control input-sm" name="datos['+i+'][fecha]" id="comentario"/>';	
				detalle += '<input type="text" maxlength="2" required value = "'+$("#proveedor_0").val()+'" class = "Requerido form-control input-sm" name="datos['+i+'][proveedor]" id="comentario"/></div>';	
				detalle += '<input type="text" maxlength="2" required value = "'+$("#monto_pago_0").val()+'" class = "Requerido form-control input-sm" name="datos['+i+'][monto]" id="comentario"/></div>';	

				tablas += '<tr id="rows'+i+'">';
				tablas += '<td><div class="input-group"><div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;"><i class="fa fa-calendar"></i></div><input type="text"  class="form-control pull-right fecha" name="data['+i+'][fecha]" placeholder="Fecha de Penalidad" value = "'+$("#fecha_pago_0").val()+'" value=""></div></td>';
				tablas += '<td><input type="text" maxlength="2" required value = "'+data[0].text+'" class = "Requerido form-control input-sm" name="data['+i+'][monto]" style="text-align: -webkit-center;"/></td>';
				tablas += '<td><input type="text" maxlength="2" required value = "'+$("#monto_pago_0").val()+'" class = "Requerido form-control input-sm" name="data['+i+'][monto]"/></td>';
				tablas += '<td><a onclick="eliminarFilaPagos('+i+')" title="Eliminar" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-times"></i></a></td>';
				tablas += '</tr>';

				console.log(tablas);

				$("#outputPagos").append(detalle);	
				$('#listadoPagos tbody').append(tablas);

				$("#fecha_pago_0").val("");
				$("#monto_pago_0").val(0);
				$("#proveedor_0").val(0).select2();
				$(".fecha" ).datepicker({ 
					altFormat: 'dd/mm/yy',
					dateFormat: 'yy-mm-dd'
				});
			}else{
				 $.toast({
						    heading: 'Error',
						    text: 'Ingrese algun dato para proceder al guardado',
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});

			}	

       	}

		function eliminarFilaPagos(idFila){
			$('table#listadoPagos tr#rows'+idFila).remove();
			$("#row"+idFila).remove();

		}

		function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

		var formatNumber = {
		 separador: ".", // separador para los miles
		 sepDecimal: ',', // separador para los decimales
		 formatear:function (num){
		 num +='';
		 var splitStr = num.split('.');
		 var splitLeft = splitStr[0];
		 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
		 var regx = /(\d+)(\d{3})/;
		 while (regx.test(splitLeft)) {
		 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
		 }
		 return this.simbol + splitLeft +splitRight;
		 },
		 new:function(num, simbol){
		 this.simbol = simbol ||'';
		 return this.formatear(num);
		 }
		}
		formatNumber.new($('#monto_seña').val()) // retorna "123.456.779,18"


</script>
@endsection