@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		#select2-estado-container{
			    padding-right: 5px;
			    padding-left: 0px;
		}	
		table.dataTable td, table.dataTable th {
			padding-left: 3px;
    		padding-right: 5px;
		}	

	</style>
@endsection
@section('content')


<section id="base-style">
@include('flash::message') 
	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Grupos</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">           
            <div class="card-body pt-0">
                <a href="{{route('grupoAdd')}}" title="Agregar Grupo" class="btn btn-success pull-right" role="button"><div class="fonticon-wrap"><i class="ft-plus-circle"></i></div></a>
            </div>
      		<div class="card-body">
		        <form action="{{route('getGrupo')}}" id="frmBusqueda" method="get"> 
					<div class="row" style="margin-bottom: 1px;">
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 20px;padding-right: 25px;padding-top: 10px;">
							<div class="form-group">
								<label>Denominacion</label>						 
								<input type="text" class = "form-control" name="denominacion" id="denominacion" placeholder="Denominacion" value="" style="text-transform: uppercase"/>
							</div>							
						</div>
						<div class="col-xs-12 col-sm-2 col-md-2" style="padding-left: 0px;padding-right: 15px;padding-top: 10px;">
							<div class="form-group">
								<label class="control-label">PNR</label>
								<input type="text" class = "form-control" name="PNR" id="PNR" placeholder="PNR" value="" style="text-transform: uppercase"/>
							</div>
						</div> 
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 10px;padding-right: 15px;padding-top: 10px;">
							<div class="form-group">
								<label>Destinos</label>
								<select data-placeholder="Destinos" id="itemList" name="destino"  style="width: 100%;">
								</select> 
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 5px;padding-right: 20px;padding-top: 10px;">
							<div class="form-group">
								<label>Cliente </label>
								<select class="form-control select2" name="cliente_id"  id="cliente_id" tabindex="1" style="width: 100%;">
									<option value="">Seleccione Cliente</option>
									@foreach($clientes as $cliente)
										@php
											$ruc = $cliente->documento_identidad;
											if($cliente->dv){
												$ruc .= $ruc."-".$cliente->dv;
											}
										@endphp
										<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->nombre}} {{$cliente->apellido}} - {{$cliente->denominacion}}</option>
									@endforeach
								</select>
							</div>
						</div>	
						<div class="col-xs-12 col-sm-1 col-md-1" style="padding-left: 0px;padding-right: 25px;padding-top: 10px;">
							<div class="form-group">
								<label>Estado</label>					            	
								<select class="form-control input-sm select2"   name="estado" id="estado" style="    padding-left: 0px;width: 100%;">
									@foreach($estados as $key=>$e)
										<option value="{{$e->id}}">{{$e->denominacion}}</option>
									@endforeach	
								</select>
							</div>
						</div> 
					</div> 
					<div class="row"  style="margin-bottom: 1px;">
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 90%; padding-right: 15px; margin-bottom: 15px; padding-top: 10px;">
							<button type="button" id="btnBuscar" class="btn btn-info btn-lg">Buscar</button>
						</div>
					</div>	
		        </form>
		        <div class="table-responsive table-bordered mt-1">
		            <table id="listado" class="table" style="width: 100%;">
		                <thead>
		                    <tr>
								<th>Detalles</th>
								<th>Denominación</th>
								<th>Cód Bloqueo</th>
								<th>Destino</th>
								<th>Salida</th>
								<th>Estado</th>
								<!--<th>Comisión</th>-->
								<th>T</th> 
								<th>P</th> 
								<th>F</th>
								<th>D</th>
								<th>PNR</th>
								<!--<th>Seña</th>-->
								<th>Cliente</th>                    
							</tr>
		                </thead>
		                <tbody style="text-align: center">
		                </tbody>
		            </table>
		        </div>
        	</div>
		</div>
	</div>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(document).ready(function() {
			$('.select2').select2();

			$("#btnBuscar").trigger('click');
			
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
			$( ".fecha" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});
		$("#itemList").select2({
					    ajax: {
					            url: "{{route('destinoGrupo')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});
		});

		$("#btnBuscar").click(function(){


			$.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });





			var dataString = $("#frmBusqueda").serialize();
			$.ajax({
				type: "GET",
				url: "{{route('getGrupo')}}",
				dataType: 'json',
				data: dataString,
				success: function(rsp){ 
					console.log(rsp);

					var oSettings = $('#listado').dataTable().fnSettings();
					var iTotalRecords = oSettings.fnRecordsTotal();
					for (i=0;i<=iTotalRecords;i++) {
						$('#listado').dataTable().fnDeleteRow(0,null,true);
					}
					$.each(rsp, function (key, item){
						console.log(item.cliente);
						var boton = '<a href="verDetalle/'+item.id+'" class="btn btn-info" style="padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-search"></i></a>'

						var link = '<a href="editGrupo/'+item.id+'" class="btn btn-success" style="padding-left: 6px;padding-right: 6px;margin-right: 2px;" role="button"><i class="fa fa-fw fa-edit"></i></a><a href="indexTicket?id='+item.id+'" class="btn btn-info" style="padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-plane"></i></a>';
						var fecha_salida = item.fecha_salida;
						if(jQuery.isEmptyObject(fecha_salida) == false){
							var fecha = fecha_salida.split('-');
							var fecha_final = fecha[2]+'/'+fecha[1]+'/'+fecha[0];
						}else{
							var fecha_final = "";	
						}

						if(jQuery.isEmptyObject(item.cliente) == false){
							var cliente = item.cliente.nombre;	
						}else{
							var cliente ="";
						}

						var dataTableRow = [
						boton+" "+link,
						item.denominacion,
						item.codigo_bloqueo,
						item.destino.desc_destino,
						fecha_final,
						item.estado.denominacion,
						//item.comision,
						item.t,
						item.p,
						item.f,
						item.d,
						item.pnr,
						//item.monto_senha,
						cliente
						];
						var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
					// set class attribute for the newly added row
					var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
					})

				}
			}).done(function(){
				$.unblockUI();
			});
		});
	
	</script>
@endsection