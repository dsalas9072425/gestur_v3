@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Editar Grupo</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">
			<form  method="post"  action="{{route('updateGrupo', $grupo->id)}}">
			        {!! method_field('PUT') !!}					
					@include('flash::message')    
					<div class="row"  style="margin-bottom: 1px;">
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
							<div class="form-group">
								<label class="control-label">PNR</label>
								<input type="text" class = "form-control numeric" name="PNR" id="PNR" placeholder="PNR" value="{{$grupo->pnr}}"/>
							</div>
						</div>  
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
							<div class="form-group">
								<label class="control-label">Estado</label>
								<select class="form-control input-sm select2" required  name="estado" id="estado" style="    padding-left: 0px;width: 100%;">
									<optgroup label="Estado actual"></optgroup>
									<option value="{{$grupo->estado->id}}">{{$grupo->estado->denominacion}}</option>
									<optgroup label="Estado a asignar"></optgroup>
									@foreach($estados as $key=>$estado)
										<option value={{$estado->id}}> {{$estado->denominacion}}</option>
									@endforeach	
								</select>	
							</div>
						</div> 
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
							<div class="form-group">
								<label class="control-label">Código Bloqueo</label>
								<input type="text" class = "form-control" name="cod_bloqueo" id="cod_bloqueo" placeholder="PNR" value="{{$grupo->codigo_bloqueo}}"/>
							</div>
						</div> 
						<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
							<br>
							<button type="submit" class="btn btn-success btn-lg" >Actualizar</button>
						</div> 
					</div>
				</form>
			</div>
		</div>
	</section>
</section>



@endsection
@section('scripts')
@include('layouts/gestion/scripts')

<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script>
$(document).ready(function() {
			$('.select2').select2();
			
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

			$( "#fecha_seña" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$( "#fecha_salida" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$( "#fecha_emision" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$("#itemList").select2({
					    ajax: {
					            url: "{{route('destinoGrupo')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});
		
			


		});

		var options = { 
	                beforeSubmit:  showRequest,
					success: showResponse,
					dataType: 'json' 
	        }; 

	 	$('body').delegate('#image','change', function(){
	 		$('#upload').ajaxForm(options).submit();  		
	 	});

		function showRequest(formData, jqForm, options) { 
			$("#validation-errors").hide().empty();
			$("#output").css('display','none');
		    return true; 
		} 
		function showResponse(response, statusText, xhr, $form){ 

			if(response.success == false)
			{
				var arr = response.errors;
				console
				$.each(arr, function(index, value)
				{
					if (value.length != 0)
					{
						$("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
					}
				});
				$("#validation-errors").show();
			} else {
				 $("#imagen").val(response.archivo);	
				 $("#output").html("<img class='img-responsive' src='"+response.file+"'/>");
				 $("#output").css('display','block');
				 $("#btn-delete").html("<button type='button' id='btn-imagen' data-id='"+response.archivo+"'><i class='glyphicon glyphicon-remove'></i></button>");
				 eliminarImagen();
			}
		}
</script>
@endsection