@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
<section class="content"> 
    @include('flash::message')     
	    <div class="row">
		    <div class="box">
		        <div class="box-header">
		            <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Grupo Detalles</h1>
		        </div>  
		        
		        <div id="proformaBase">
					<div class="box box-default" style="border-top-width: 1px;margin-bottom: 0px;">
						<div class="box-header with-border">
						  <h3 class="box-title">Datos del grupo</h3>
						</div>
						        <!-- /.box-header -->
						<div class="box-body">
							      	<!-- /.box -->
						    <div class="row"  style="margin-bottom: 1px;">
							    <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
									<div class="form-group">
									    <label class="control-label">Denominación</label>
									    <input type="hidden" class = "form-control" id="id_grupo" placeholder="Denominación" value="{{$grupo->id}}" readonly="readonly" style="text-transform: uppercase"/>

					        			<input type="text" required class = "form-control" name="denominacion" id="denominacion" placeholder="Denominación" value="{{$grupo->denominacion}}" readonly="readonly" style="text-transform: uppercase"/>
									</div>
							    </div>

							    <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
									    <label class="control-label">Cantidad Bloqueos</label>
					        			<input type="text" required class = "form-control" name="cant_bloqueos" id="cant_bloqueos" placeholder="Cantidad Bloqueos"  value="{{$grupo->cantidad}}" readonly="readonly"/>
									</div>
							    </div>

							    <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Cantidad Liberados</label>
					        			<input type="text" required class = "form-control" name="cant_liberados" id="cant_liberados" placeholder="Cantidad Liberados"  value="{{$grupo->cantidad_liberados}}" readonly="readonly"/>
									</div>
							    </div>

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
									<div class="form-group">
										<label class="control-label">Código Bloqueo</label>
					        			<input type="text" required class = "form-control" name="cod_bloqueo" id="cod_bloqueo" value="{{$grupo->codigo_bloqueo}}" readonly="readonly" style="text-transform: uppercase"/>
									</div>
								</div> 	
							</div>  

							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
									<div class="form-group">
										<label>Destino</label>
										<input type="text" required class = "form-control" name="destino" id="destino"   value="{{$grupo->destino->desc_destino}}" readonly="readonly" />
									</div>
								</div>

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;">
									<div class="form-group">
										<label>Fecha de emisión</label>						 
										<input type="text" required class = "form-control" name="fecha_emision" id="fecha_emision"   value="{{$fecha_emision}}" readonly="readonly"/>
									</div>							
								</div>

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;">
									<div class="form-group">
										<label>Fecha de salida</label>						 
										<input type="text" required class = "form-control" name="fecha_salida" id="fecha_salida"   value="{{$fecha_salida}}" readonly="readonly"/>
									</div>							
								</div> 	

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px; padding-right: 25px;">
									<div class="form-group">
										<label>Fecha de seña</label>						 
										<input type="text" required class = "form-control" name="fecha_senha" id="fecha_senha"   value="{{$fecha_seña}}" readonly="readonly"/>
									</div>							
								</div> 	 	
							</div>

							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
									<div class="form-group">
										<label>Moneda</label>					            	
										<input type="text" required class = "form-control" name="moneda" id="moneda"   value="{{$grupo->currency->currency_code}}" readonly="readonly"/>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label>Estado</label>					            	
										<input type="text" required class = "form-control" name="estado" id="estado"   value="{{$grupo->estado->denominacion}}" readonly="readonly" style="text-transform: uppercase"/>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label>Aerolínea</label>
											@if(isset($grupo->persona->nombre))
												<input type="text" required class = "form-control" name="aerolinea" id="aerolinea" value="{{$grupo->persona->nombre}}" readonly="	readonly" style="text-transform: uppercase"/>
											@else
												<input type="text" required class = "form-control" name="aerolinea" id="aerolinea" value="" readonly="	readonly" style="text-transform: uppercase"/>
											@endif	
									</div>
								</div>

								<div class="col-sm-3 col-md-3">
									<label>Comisión</label>
									<div class="form-group">
										<input type="text" required class = "form-control" name="comision" id="comision" value="{{ number_format($grupo->comision,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>   
							</div>

							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">PNR</label>
										<input type="text" required class = "form-control" name="PNR" id="PNR" placeholder="PNR"  value="{{ $grupo->pnr }}" readonly="readonly" style="text-transform: uppercase"/>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Importe Facial</label>
										<input type="text" required class = "form-control" name="facial" id="facial" placeholder="Importe Facial"  value="{{ number_format($grupo->importe_facial,2,",",".") }}" readonly="readonly"/>
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Tasas</label>
										<input type="text" required class = "form-control" name="tasa" id="tasa" placeholder="Tasas" value="{{ number_format($grupo->importe_tasas,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
									<div class="form-group">
										<label class="control-label">Tasa YQ</label>
										<input type="text" required class = "form-control" name="tasa_yq" id="tasa_yq" value="{{ number_format($grupo->tasa_YQ,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>
							</div>

							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">IVA</label>
										<input type="text" required class = "form-control" name="iva" id="iva"  value="{{ number_format($grupo->importe_iva,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>  

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Variación Impuesto</label>
										<input type="text" required class = "form-control" name="var_impuesto" id="var_impuesto"  value="{{ number_format($grupo->variacion_impuesto,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>  

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
									<div class="form-group">
										<label class="control-label">Monto seña (por persona)</label>
										<input type="text" required class = "form-control" name="monto_seña" id="monto_seña"  value="{{ number_format($grupo->monto_senha,2,",",".") }}" readonly="readonly"/> 
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
									<div class="form-group">
										<label>Tipo ticket</label>
											@if(isset($grupo->tipoTicket->descripcion))							
												<input type="text" required class = "form-control" name="tipo_ticket" id="tipo_ticket"  value="{{$grupo->tipoTicket->descripcion}}" readonly="readonly" style="text-transform: uppercase"/>
											@else	
												<input type="text" required class = "form-control" name="tipo_ticket" id="tipo_ticket"  value="" readonly="readonly" style="text-transform: uppercase"/>
											@endif
									</div>
								</div> 

								<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
									<div class="form-group">
											<label>Usuario</label>				
										<input type="text" required class = "form-control" name="" id=""  value="{{$grupo->usuario['nombre']}} {{$grupo->usuario['apellido']}}" readonly="readonly" style="text-transform: uppercase"/>
									</div>
								</div> 

							</div>

															
							       	
							<div class="row"  style="margin-bottom: 1px;">
								@if($grupo->imagen_documento !="")
									<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 25px;">
										<div class="form-group">
											<label>Ver Documento</label>
										        <?php
										           $ruta = 'grupos/'.$grupo->imagen_documento;
										        ?>		
											<a href="{{asset($ruta)}}" target="_blank"><i class='fa fa-file-text-o fa-lg' style='color: blue;'></i></a>      
										</div>
									</div> 
								@endif 	
							</div>	
							@php
								if(count($grupo->penalidad) != 0){
							@endphp
							<div class="box-header with-border">
						  		<h3 class="box-title">Datos de Penalidades</h3>
							</div>
								<div class="table-responsive" style="margin-left: 2%;">
					              <table id="listadoPenalidades" class="table" style="width:97%">
					                <thead>
										<tr>
											<th>Fecha</th>
											<th>Monto</th>
							            </tr>
					                </thead>
					                <tbody  style="text-align: center">
					                	@foreach($grupo->penalidad as $penalidad)
						                	<tr>
												<th style="text-align: center;">{{$penalidad->fecha}}</th>
												<th style="text-align: center;">{{$penalidad->monto}}</th>
											</tr>	
										@endforeach	
					                </tbody>
					              </table>
								</div>		
							@php
								}

							if(count($grupo->pagos) != 0){
							@endphp
							<div class="box-header with-border">
						  		<h3 class="box-title">Calendario de Pagos</h3>
							</div>
								<div class="table-responsive" style="margin-left: 2%;">
					              <table id="listadoPenalidades" class="table" style="width:97%">
					                <thead>
										<tr>
											<th>Fecha</th>
											<th>Proveedor</th>
											<th>Monto</th>
							            </tr>
					                </thead>
					                <tbody  style="text-align: center">
					                	@foreach($grupo->pagos as $pagos)
						                	<tr>
												<th style="text-align: center;">{{$pagos->fecha}}</th>
												<th style="text-align: center;">{{$pagos->proveedor_id}}</th>
												<th style="text-align: center;">{{$pagos->monto}}</th>
											</tr>	
										@endforeach	
					                </tbody>
					              </table>
								</div>		
							@php
								}
								if(count($grupo->senha) != 0){
							@endphp
							<div class="box-header with-border">
						  		<h3 class="box-title">Datos de Señas Cliente</h3>
							</div>
								<div class="table-responsive" style="margin-left: 2%;">
					              <table id="listadoPenalidades" class="table" style="width:97%">
					                <thead>
										<tr>
											<th>Fecha</th>
											<th>Monto</th>
							            </tr>
					                </thead>
					                <tbody  style="text-align: center">
					                	@foreach($grupo->senha as $senha)
						                	<tr>
												<th style="text-align: center;">{{$senha->fecha}}</th>
												<th style="text-align: center;">{{$senha->monto}}</th>
											</tr>	
										@endforeach	
					                </tbody>
					              </table>
								</div>		
							@php
								}
         					@endphp
	
							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-xs-12 col-sm-10 col-md-10" style="padding-left: 15px;">
								</div>
								<div class="col-xs-12 col-sm-2 col-md-2" style="padding-left: 15px;margin-bottom: 15px; padding-top: 10px;">
									<a class="btn btn-danger" href="{{ route('indexGrupo') }}" style="width: 122px;padding-bottom: 10px;padding-top: 5px;padding-left: 20px;padding-right: 20px;height: 40px; font-size: 20px;"><b>Volver</b></a>
								</div>
							</div>	
				            <!-- /.box -->
						    <!-- SELECT2 EXAMPLE -->
						   
				          	<!-- <div class="row"  style="margin-bottom: 1px;"> -->
				          		
							<!-- </div>	 -->
				</div>	
		          <!-- /.box -->
			</div>
		        <!-- /.col -->
	    </div>
	</div>
</section>
    <!-- /.content -->


	

@endsection
@section('scripts')
	@include('layouts/mc/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>

	<script>
		
		$(document).ready(function() {
			$('.select2').select2();
			
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

			$( "#fecha_senha_0" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$( "#fecha_salida" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$( "#fecha_emision" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});


		});


		function guardarAdelanto(){
	        dataString = { id_grupo :$('#id_grupo').val(), fecha :$('#fecha_senha_0').val(), formaPago :$('#forma_pago_0').val(), recibo :$('#recibo_0').val(), comprobante :$('#comprobante_0').val(), moneda :$('#moneda_0').val(), monto :$('#monto_0').val()};
			$.ajax({
					type: "GET",
					url: "{{route('getAdelanto')}}",
					dataType: 'json',
					async:false,
					data: dataString,
						error: function(){
								 $.toast({
										    heading: 'Error',
										    text: 'Ingrese algun dato para proceder al guardado',
										    position: 'top-right',
										    showHideTransition: 'fade',
										    icon: 'error'
										});
							},
						success: function(rsp){
							if(rsp.mensaje == 'OK'){
									console.log(rsp);
									if(jQuery.isEmptyObject(rsp.fecha) == false){
											var fechaIn = rsp.fecha;
											var $In = fechaIn.split('-');
											fecha_in = $In[2]+"/"+$In[1]+"/"+$In[0];
									}else{
											fecha_in = "";
									}	
					       		    var nFilas = $("#listadoPagoAdelanto tr").length;
					       		    i = nFilas - 1;
									tablas = '<tr id="rows'+rsp.id+'">';
									tablas += '<td><div class="input-group"><div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;"><i class="fa fa-calendar"></i></div><input type="text"  class="form-control pull-right fecha" name="data['+i+'][fecha]" placeholder="Fecha de Penalidad" value = "'+fecha_in+'" value=""></div></td>';
									tablas += '<td><input type="text" maxlength="2" required value = "'+rsp.forma_pago+'" class = "Requerido form-control input-sm" name="data['+i+'][monto]" style="text-align: -webkit-center;"/></td>';
									tablas += '<td><input type="text" maxlength="2" required value = "'+rsp.nro_recibo+'" class = "Requerido form-control input-sm" name="data['+i+'][monto]"/></td>';
									tablas += '<td><input type="text" maxlength="2" required value = "'+rsp.nro_comprobante+'" class = "Requerido form-control input-sm" name="data['+i+'][monto]"/></td>';
									tablas += '<td><input type="text" maxlength="2" required value = "'+rsp.denominacionMoneda+'" class = "Requerido form-control input-sm" name="data['+i+'][monto]"/></td>';
									tablas += '<td><input type="text" maxlength="2" required value = "'+ formatter.format(parseFloat(rsp.monto))+'" class = "Requerido form-control input-sm" name="data['+i+'][monto]"/></td>';
									tablas += '<td><a onclick="eliminarFilaPagos('+rsp.id+')" title="Eliminar" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-times"></i></a></td>';
									tablas += '</tr>';
									$('#listadoPagoAdelanto tbody').append(tablas);
									$.toast({
										heading: 'Exito',
										text: 'Se ha realizado el pago',
										position: 'top-right',
										showHideTransition: 'slide',
										icon: 'success'
									});  

									total = parseFloat($('#monto_total_0').val().replace('.', '').replace(',', '.'));
									monto = rsp.monto;
									totalFinal = parseFloat(total) + parseFloat(monto.replace(',', '.'));
									console.log(total);
									console.log(monto);

									$('#monto_total_0').val(totalFinal);
									$('#fecha_senha_0').val('');
									$('#recibo_0').val('');
									$('#comprobante_0').val('');
									$('#monto_0').val('');
									$("#forma_pago_0").val(0).trigger('change.select2');
									//$("#moneda_0").val(0).trigger('change.select2');
							}else{
									$.toast({
										heading: 'Error',
										ext: 'Ya se ha ingresado un comprobante con el mismo monto',
										position: 'top-right',
										showHideTransition: 'fade',
										icon: 'error'
											});							
								}		
						}	
				})		

		}	

		function eliminarFilaPagos(idFila){
		 	dataString = { id_fila :idFila};
			$.ajax({
					type: "GET",
					url: "{{route('getEliminarAdelanto')}}",
					dataType: 'json',
					async:false,
					data: dataString,
					error: function(){
								$.toast({
									heading: 'Error',
									ext: 'Ingrese algun dato para proceder al guardado',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
										});
					},
					success: function(rsp){
										$.toast({
											heading: 'Exito',
											text: rsp.mensaje,
											position: 'top-right',
											showHideTransition: 'slide',
										    icon: 'success'
										});  
										$('table#listadoPagoAdelanto tr#rows'+idFila).remove();
										$("#row"+idFila).remove();
										total = parseFloat($('#monto_total_0').val());
										console.log(total);
										console.log(rsp.monto);
										totalFinal = parseFloat(total) - parseFloat(rsp.monto);
										$('#monto_total_0').val(totalFinal);
								}	
				});				


		}

		var options = { 
	                beforeSubmit:  showRequest,
					success: showResponse,
					dataType: 'json' 
	        }; 

	 	$('body').delegate('#image','change', function(){
	 		$('#upload').ajaxForm(options).submit();  		
	 	});

		function showRequest(formData, jqForm, options) { 
			$("#validation-errors").hide().empty();
			$("#output").css('display','none');
		    return true; 
		} 
		function showResponse(response, statusText, xhr, $form){ 

			if(response.success == false)
			{
				var arr = response.errors;
				console
				$.each(arr, function(index, value)
				{
					if (value.length != 0)
					{
						$("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
					}
				});
				$("#validation-errors").show();
			} else {
				 $("#imagen").val(response.archivo);	
				 $("#output").html("<img class='img-responsive' src='"+response.file+"'/>");
				 $("#output").css('display','block');
				 $("#btn-delete").html("<button type='button' id='btn-imagen' data-id='"+response.archivo+"'><i class='glyphicon glyphicon-remove'></i></button>");
				 eliminarImagen();
			}
		}

		function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

		var formatNumber = {
		 separador: ".", // separador para los miles
		 sepDecimal: ',', // separador para los decimales
		 formatear:function (num){
		 num +='';
		 var splitStr = num.split('.');
		 var splitLeft = splitStr[0];
		 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
		 var regx = /(\d+)(\d{3})/;
		 while (regx.test(splitLeft)) {
		 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
		 }
		 return this.simbol + splitLeft +splitRight;
		 },
		 new:function(num, simbol){
		 this.simbol = simbol ||'';
		 return this.formatear(num);
		 }
		}
		formatNumber.new($('#monto_seña').val()) // retorna "123.456.779,18"

		$('.numerico').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

		const formatter = new Intl.NumberFormat('de-DE', {
							  currency: 'USD'
							});
		{{--SE UTILIZA LA BANDERA BD, PORQUE LOS DATOS DE BASE DE DATOS
			NO NECESITAN FORMATEO PARA REALIZAR OPERACIONES ARITMETICAS --}}
		function clean_num(n,bd=false){
			if(n && bd == false){ 
			n = n.replace(/[,.]/g,function (m) {  
					 				 if(m === '.'){
					 				 	return '';
					 				 } 
					 				  if(m === ','){
					 				 	return '.';
					 				 } 
					 			});
			return Number(n);
		}
		if(bd){
			return Number(n);
		}
		return 0;
		}

	</script>
@endsection