@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')

	@include('flash::message')  

    <section class="base-style"> 

		<section class="card">
			<div class="card-header">
				<h4 class="card-title">Editar Ticket</h4>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-content collapse show" aria-expanded="true">
				<div class="card-body pt-0">
			   		<form id="frmGrupo" method="post"  action="{{route('editarTicket')}}" style="margin-top: 2%;">
					   <input type="hidden" class = "form-control numeric" name="id" id="id" placeholder="Importe Facial" value="{{$ticketes->id}}" onkeypress="return justNumbers(event);"/>

							@include('flash::message')     
									@if (count($errors) > 0)
										   <div class = "alert alert-danger">
										      <ul>
										         @foreach ($errors->all() as $error)
										            <li>{{ $error }}</li>
										         @endforeach
										      </ul>
										   </div>
										@endif
										<!-- /.box -->
								@foreach($tickets as $ticket)	
									<div class="row"  style="margin-bottom: 1px;">
										<div class="col-12 col-sm-3 col-md-3" >
											<div class="form-group">
												<label class="control-label">Número de ticket</label>
												<input type="text" required class = "form-control" name="amadeus" id="amadeus" placeholder="Número de ticket" value="{{$ticket->numero_amadeus}}" readonly="readonly" style="text-transform: uppercase"/>
											</div>
										</div>
					
										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label>Aerolínea</label>					            	
												<select class="form-control input-sm select2" name="aerolinea" id="aerolinea" style="padding-left: 0px;width: 100%;">
													<option value=" ">Seleccione aerolínea</option>
													@foreach($aerolineas as $key=>$aerolinea)
														<option value="{{$aerolinea->out_id_persona}}">{{$aerolinea->out_nombre}}</option>
													@endforeach	
												</select>											
											</div>
										</div>

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label class="control-label">Fecha Emisión</label>
												<input type="text" required class = "form-control" name="fecha_emision" id="fecha_emision" value="{{$fecha_emision}}" readonly="readonly" />
											</div>
										</div>

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label>Fecha de pago</label>						 
												<input type="text" required class = "form-control" name="fecha_senha" id="fecha_senha"   value="{{$fecha_pago}}" readonly="readonly"/>
											</div>							
										</div> 
									
										<div class="col-12 col-sm-3 col-md-3" >
											<div class="form-group">
												<label>Origen Ticket</label>					            	
												<input type="text" required class = "form-control" name="tipo_ticket" id="tipo_ticket"   value="{{$ticket->codigo_amadeus}}" readonly="readonly"/>
											</div>
										</div> 


										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label>Origen</label>					            	
												<input type="text" required class = "form-control" name="ticket" id="ticket"  value="{{$ticket->descripcion}}" readonly="readonly" style="text-transform: uppercase"/>
											</div>
										</div> 

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label>Estado</label>					            	
												<input type="text" required class = "form-control" name="estado_ticket" id="estado_ticket"  value="{{$ticket->estado}}" readonly="readonly" style="text-transform: uppercase"/>
											</div>
										</div> 

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label class="control-label">PNR</label>
												<input type="text" required class = "form-control" name="PNR" id="PNR" placeholder="PNR"  value="{{$ticket->pnr}}" readonly="readonly" style="text-transform: uppercase"/>
											</div>
										</div>
									
										<div class="col-12 col-sm-3 col-md-3" >
											<div class="form-group">
												<label>Moneda</label>					            	
												<input type="text" required class = "form-control" name="moneda" id="moneda"   value="{{$ticket->currency_code}}" readonly="readonly"/>
											</div>
										</div> 

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label class="control-label">Facial/Gravada</label>
												<input type="text" class = "form-control numeric" name="facial" id="facial" placeholder="Importe Facial" value="{{ number_format($ticketes->facial,2,",",".") }}" onkeypress="return justNumbers(event);"/>											</div>
										</div> 

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label class="control-label">Tasas/Exenta</label>
												<input type="text" class = "form-control numeric" name="tasa" id="tasa" placeholder="Tasas" value="{{ number_format($ticketes->tasas,2,",",".") }}" onkeypress="return justNumbers(event);"/>											</div>
										</div>

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label class="control-label">Tasa YQ</label>
												<input type="text" required class = "form-control" name="tasa_yq" id="tasa_yq" value="{{ number_format($ticketes->tasas_yq,2,",",".") }}"/>											
											</div>
										</div>
								
										<div class="col-12 col-sm-3 col-md-3" >
											<div class="form-group">
												<label class="control-label">Gravada</label>
												<input type="text" required class = "form-control" name="grabda" id="grabda"  value="{{ number_format($ticket->grabada,2,",",".") }}" readonly="readonly"/>
											</div>
										</div>

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label class="control-label">IVA</label>
												<input type="text" data-toggle="tooltip" data-placement="left" class = "form-control numeric" name="iva" id="iva" placeholder="Tasas" value="{{ number_format($ticket->iva,2,",",".") }}" onkeypress="return justNumbers(event);"/>											</div>
										</div>  

										<div class="col-12 col-sm-3 col-md-3">
												<label>Comisión</label>
											<div class="form-group">
											<input type="text" data-toggle="tooltip" data-placement="left" class = "form-control numeric" name="comision" id="comision" placeholder="Tasas" value="{{ number_format($ticket->comision,2,",",".") }}" onkeypress="return justNumbers(event);"/>											</div>
										</div>

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label class="control-label">IVA Comisión</label>
												<input type="text" data-toggle="tooltip" data-placement="left" class = "form-control numeric" name="iva_comision" id="iva_comision" placeholder="Tasas" value="{{number_format($ticket->iva_comision,2,",",".") }}" onkeypress="return justNumbers(event);"/>											</div>
										</div>
								
										<div class="col-12 col-sm-3 col-md-3" >
											<div class="form-group">
												<label class="control-label">Precio ticket</label>
												<input type="text" data-toggle="tooltip" data-placement="left" class = "form-control numeric" name="total_ticket" id="total_ticket" value="{{ number_format($ticketes->total_ticket,2,",",".") }}" onkeypress="return justNumbers(event);"/>											</div>
										</div>

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label class="control-label">Neto sin comisión</label>
												<input type="text" required class = "form-control" name="precio_sin_comision" id="precio_sin_comision"  value="{{ number_format($ticket->total_costo,2,",",".") }}" readonly="readonly"/>
											</div>
										</div>  

										<div class="col-12 col-sm-3 col-md-3">
												<label>Proforma</label>
											<div class="form-group">
												<input type="text" required class = "form-control" name="proforma_id" id="proforma_id"   value="{{$ticket->id_proforma}}" readonly="readonly"/>
											</div>
										</div>
													
										<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
											<div class="form-group">
												<label class="control-label">Factura</label>
												<input type="text" required class = "form-control" name="factura_nro" id="factura_nro" value="{{$ticket->nro_factura}}" readonly="readonly"/>
											</div>
										</div>
								
										<div class="col-12 col-sm-3 col-md-3" >
											<div class="form-group">
												<label class="control-label">Variación Impuesto</label>
												<input type="text" class = "form-control numeric" name="variacion_impuesto" id="variacion_impuesto" placeholder="Variación Impuesto" value="{{$ticket->variacion_impuesto}}" readonly="readonly" />
											</div>
										</div>  
										<div class="col-12 col-sm-3 col-md-3" >
											<div class="form-group">
												<label class="control-label">Pasajero</label>
												<input type="text" required class = "form-control" name="pasajero" id="pasajero"  value="{{$ticket->pasajero}}" readonly="readonly"/>
											</div>
										</div>

										<div class="col-12 col-sm-3 col-md-3">
											<div class="form-group">
												<label class="control-label">Emitido Por:</label>
												<input type="text" required class = "form-control" name="usuario" id="usuario"  value="{{$ticket->usuario}}" readonly="readonly"/>
											</div>
										</div>  
									</div> 
								@endforeach
										<div class="row"  style="margin-bottom: 1px;margin-top: 20px;">
											<div class="col-12 col-sm-10 col-md-10" style="padding-left: 15px;">
											</div>
									
											<div class="col-12 col-sm-2 col-md-2" style="padding-left: 15px;margin-bottom: 10px;">
												<button type="submit" form="frmGrupo" id="btnGuardar" class="btn btn-success btn-lg" >Guardar</button>
											</div>
										</div> 


						</form> 
				</div>
			</div>
		</section>
	</section>
    <!-- /.content -->

@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script>
	$('.select2').select2();
	$('#aerolinea').val("{{$ticketes->id_proveedor}}").trigger('change.select2');
	$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { 
				$(this).val(0);
			 }
		});



</script>
@endsection