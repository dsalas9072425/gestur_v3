@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Ticket Detalles</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	<form  method="post"  action="{{ route('saveUser', $ticket->id) }}">
					<div class="row"  style="margin-bottom: 1px;">
							<div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 53px;padding-right: 15px;">
								<div class="form-group">
									<label>Vendedor Empresa</label>	
									<select class="form-control input-sm select2" name="id_vendedor" id="id_vendedor" style="padding-left: 0px;width: 100%;">
										@if(isset($ticket->vendedor->id))
											<optgroup label="Vendedor Actual"></optgroup>
											<option value="{{$ticket->vendedor->id}}">{{$ticket->vendedor->nombre}} {{$ticket->vendedor->apellido}}</option>
											<optgroup label="Seleccione Vendedor"></optgroup>
											@foreach($personas as $key=>$persona)
												<option value="{{$persona->id}}">{{$persona->nombre}} {{$persona->apellido}}</option>
											@endforeach
										@else
											<option value=" ">Seleccione vendedor</option>
											@foreach($personas as $key=>$persona)
												<option value="{{$persona->id}}">{{$persona->nombre}} {{$persona->apellido}}</option>
											@endforeach	
										@endif
												
									</select>
								</div>
							</div>
						
						    <div class="col-xs-12 col-sm-3 col-md-3" style="padding-left: 35px;padding-right: 25px;">
						    	<div class="col-md-6">
									<br>
									<button type="submit" class="btn btn-success btn-lg" >Guardar</button>
								</div>
							</div>
						</div> 
        
            	</form>	
            </div>	
        </div>    
    </div>   
</section>
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')	
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script>
		$(document).ready(function() {
			$('.select2').select2();
			
		});

	</script>
	
@endsection
