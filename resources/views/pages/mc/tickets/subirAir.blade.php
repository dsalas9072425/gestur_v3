@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

@endsection
@section('content')

    <!-- Main content -->
<section id="base-style">
   	<div class="card-content">
   		@include('flash::message')
            <div class="card" style="border-radius: 14px;">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="card-title">Subir Archivo Air</h4>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        </ul>
                    </div>
                </div> 
                <div class="card-content collapse show" aria-expanded="true">
                    <div class="card-body rounded-bottom rounded-lg" style="padding-bottom: 10px;padding-top: 10px;">
						    <form method="post" action="{{url('importArchivo')}}" id="frmulario" enctype="multipart/form-data">
						    {{csrf_field()}}
						    <div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<br>
									<label class="control-label">Archivo:</label>
								</div>
							    <div class="col-xs-12 col-sm-6 col-md-6">  	
							    	<br>  
							        <input type="file" name="file">
							    </div> 
						    </div>   
						    <br><br>
						        <input class="btn-primary btn-lg" type="submit" value="Procesar" style="margin-left: 15%;">
						    </form>
                    </div>
                </div>
            </div>
	</div>
</section>		

@endsection
@section('scripts')

	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<!-- wysihtml core javascript with default toolbar functions --> 
	
	<script>
	
		$('.select2').select2();
 		$("#frmulario").bind("submit", function() { 
            $.blockUI({
                                                 centerY: 0,
                                                 message: "<h2>Procesando...</h2>",
                                                 css: {
                                                     color: '#000'
                                                 }
                                            });
       });	
	</script>
@endsection
