
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
	</style>
@endsection
@section('content')

<section id="base-style">
@include('flash::message') 
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Subir Planilla BSP</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">
                <form action="{{route('doSubirBsp')}}" method="post" enctype="multipart/form-data">
					<div class="row">
                        <div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Archivo</label>
                                <input type="file" name="file" class="form-control" id="exampleInputFile">
                            </div>
                        </div>

						<div class="col-9 mb-1"></div>
						<div class="col-2 mb-1">
                            <button type="submit" name="submit" class="btn btn-info btn-lg pull-right mb-1">Migrar</button>
						</div>
					</div>
				</form>
				<div class="row">
					<div class="col-9 mb-1"></div>	
					<div class="col-2 mb-1">
						<a href="https://gestur.git.com.py/formato_bsp.xlsx"target="_blank">
							<button class="btn btn-info btn-lg pull-right mb-2" style="background-color: #ff0080db !important;">Descargar Plantilla</button>
						</a>
					</div>
				</div>
    		</div>
		</div>
	</section> 
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script>


	</script>
@endsection