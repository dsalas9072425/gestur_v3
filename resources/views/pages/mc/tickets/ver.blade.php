@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->

<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Ticket Detalles</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
			   
							@foreach($tickets as $ticket)	
							<input type="hidden" required class = "form-control" name="id_ticket" id="id_ticket"  value="{{$ticket->id}}" readonly="readonly"/>
							<div class="row"  style="margin-bottom: 1px;">
								<div class="col-12 col-sm-3 col-md-3" >
									<div class="form-group">
										<label class="control-label">Número de ticket</label>
										<input type="text" required class = "form-control" name="amadeus" id="amadeus" placeholder="Número de ticket" value="{{$ticket->numero_amadeus}}" readonly="readonly" style="text-transform: uppercase"/>
									</div>
								</div>
	   		
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Aerolínea</label>					            	
										<input type="text" required class = "form-control" name="aerolinea" id="aerolinea"   value="{{$ticket->nombre_proveedor}}" readonly="readonly"/>
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Fecha Emisión</label>
										<input type="text" required class = "form-control" name="fecha_emision" id="fecha_emision" value="{{$fecha_emision}}" readonly="readonly" />
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Fecha de pago</label>						 
										<input type="text" required class = "form-control" name="fecha_senha" id="fecha_senha"   value="{{$fecha_pago}}" readonly="readonly"/>
									</div>							
								</div> 
							
								<div class="col-12 col-sm-3 col-md-3" >
									<div class="form-group">
										<label>Origen Ticket</label>					            	
										<input type="text" required class = "form-control" name="tipo_ticket" id="tipo_ticket"   value="{{$ticket->codigo_amadeus}}" readonly="readonly"/>
									</div>
								</div> 


								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Origen</label>					            	
										<input type="text" required class = "form-control" name="ticket" id="ticket"  value="{{$ticket->descripcion}}" readonly="readonly" style="text-transform: uppercase"/>
									</div>
								</div> 

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>Estado</label>					            	
										<input type="text" required class = "form-control" name="estado_ticket" id="estado_ticket"  value="{{$ticket->estado}}" readonly="readonly" style="text-transform: uppercase"/>
									</div>
								</div> 

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Tipo Tarifa</label>
										<input type="text" required class = "form-control"  value="{{ $ticket->tipo_tarifa }}" readonly="readonly"/>
									</div>
								</div>  

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">PNR</label>
										<input type="text" required class = "form-control" name="PNR" id="PNR" placeholder="PNR"  value="{{$ticket->pnr}}" readonly="readonly" style="text-transform: uppercase"/>
									</div>
								</div>
							
								<div class="col-12 col-sm-3 col-md-3" >
									<div class="form-group">
										<label>Moneda</label>					            	
										<input type="text" required class = "form-control" name="moneda" id="moneda"   value="{{$ticket->currency_code}}" readonly="readonly"/>
									</div>
								</div> 

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Facial/Gravada</label>
										<input type="text" class = "form-control" name="facial" id="facial" placeholder="Importe Facial"  value="{{ number_format($ticket->facial,2,",",".") }}" readonly="readonly"/>
									</div>
								</div> 

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Tasas/Exenta</label>
										<input type="text" required class = "form-control" name="tasa" id="tasa" placeholder="Tasas"  value="{{ number_format($ticket->tasas,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Tasa YQ</label>
										@if($ticket->tasas_yq != 0)
											<input type="text" required class = "form-control" name="tasa_yq" id="tasa_yq" value="{{ number_format($ticket->tasas_yq,2,",",".") }}" readonly="readonly"/>
										@else
											<input type="text" required class = "form-control" name="tasa_yq" id="tasa_yq" value="{{ number_format($ticket->tasasyq,2,",",".") }}" readonly="readonly"/>
										@endif

									</div>
								</div>
						
								<div class="col-12 col-sm-3 col-md-3" >
									<div class="form-group">
										<label class="control-label">Gravada</label>
										<input type="text" required class = "form-control" name="grabda" id="grabda"  value="{{ number_format($ticket->grabada,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">IVA</label>
										<input type="text" required class = "form-control" name="iva" id="iva"  value="{{ number_format($ticket->iva,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>  

								<div class="col-12 col-sm-3 col-md-3">
										<label>Comisión</label>
									<div class="form-group">
										<input type="text" required class = "form-control" name="comision" id="comision" value="{{ number_format($ticket->comision,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">IVA Comisión</label>
										<input type="text" required class = "form-control" name="iva_comision" id="iva_comision" value="{{number_format($ticket->iva_comision,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>
						
								<div class="col-12 col-sm-3 col-md-3" >
									<div class="form-group">
										<label class="control-label">Precio ticket</label>
										<input type="text" required class = "form-control" name="gran_total" id="gran_total"  value="{{ number_format($ticket->total_ticket,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Neto sin comisión</label>
										<input type="text" required class = "form-control" name="precio_sin_comision" id="precio_sin_comision"  value="{{ number_format($ticket->total_costo,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>  

								<div class="col-12 col-sm-3 col-md-3">
										<label>Proforma</label>
									<div class="form-group">
										<input type="text" required class = "form-control" name="proforma_id" id="proforma_id"   value="{{$ticket->id_proforma}}" readonly="readonly"/>
									</div>
								</div>
								      		
								<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
									<div class="form-group">
										<label class="control-label">Factura</label>
										<input type="text" required class = "form-control" name="factura_nro" id="factura_nro" value="{{$ticket->nro_factura}}" readonly="readonly"/>
									</div>
								</div>
						
								<div class="col-12 col-sm-3 col-md-3" >
									<div class="form-group">
										<label class="control-label">Variación Impuesto</label>
										<input type="text" class = "form-control numeric" name="variacion_impuesto" id="variacion_impuesto" placeholder="Variación Impuesto" value="{{$ticket->variacion_impuesto}}" readonly="readonly" />
									</div>
								</div>  
								<div class="col-12 col-sm-3 col-md-3" >
									<div class="form-group">
										<label class="control-label">Pasajero</label>
										<input type="text" required class = "form-control" name="pasajero" id="pasajero"  value="{{$ticket->pasajero}}" readonly="readonly"/>
									</div>
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Emitido Por </label>
										<input type="text" required class = "form-control" name="usuario" id="usuario"  value="{{$ticket->usuario}}" readonly="readonly"/>
									</div>
								</div>  
								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Cotizacion JURCAIP</label>
										@if(empty($ticket->cotizacion_jurcaip))	
											<?php
												$jurcaip = $ticket->cotizacion_jurcaip;
												?>
										@else 
										<?php
												$jurcaip = 0;
												?>
										@endif
										<input type="text" required class = "form-control" name="usuario" id="usuario"  value="{{ number_format($jurcaip,2,",",".") }}" readonly="readonly"/>
									</div>
								</div>  

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Origen de creación</label>
										<input type="text" required class = "form-control"  value="{{ $ticket->origen == 'A' ? 'Automático' : 'Manual' }}" readonly="readonly"/>
									</div>
								</div>  

								<div class="col-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label class="control-label">Office</label>
										<input type="text" required class = "form-control"  value="{{ $ticket->empresa_emisor_ticket }}" readonly="readonly"/>
									</div>
								</div>  

								@if($op)
									
									<div class="col-12 col-sm-3 col-md-3">
										<div class="form-group">
											<label class = "base"><a href="{{route('controlOp',['id'=>$op->id])}}" target="_blank" style="color:black"  class="bgRed">Nro OP - Pago TC  <i class="fa fa-fw fa-search" style="color:red"></i></a></label>
											<input style="color:red" type="text" class = "form-control text-bold" name="proforma_id" style="font-weight: bold;font-size: 15px;" value="{{$op->nro_op}}" readonly="readonly"/>
										</div>
									</div>

									<div class="col-12 col-sm-3 col-md-3">
										<div class="form-group">
											<label class="control-label">Nro TC</label>
											<input type="text" disabled class = "form-control"  value="{{ $ticket->nro_tarjeta }}" />
										</div>
									</div> 
								@endif
								

								<div class="col-12">
									<button onclick="getImpuestos({{$ticket->id}})" type="button" class="btn btn-info pull-right" id="impusto"><b>Impuestos</b></button>
									@if($ticket->estado == "D")
										<?php 
											if(isset($btn[0])){
												echo $btn[0];
											}
											?> 
										@if($permiso == 1)
											<a class="btn btn-success" role="button" href="{{ route('edit',['id'=>$ticket->id])}}"><b>Editar</b></a>
										@endif
									@endif	
									<a class="btn btn-danger" role="button" href="{{ route('indexTicket')}}"><b>Volver</b></a>
								</div>
							</div>
							@endforeach
							
            </div>
        </div> 
    </div>       	
</section>


<div id="requestImpuestoTicket" class="modal fade" role="dialog">
	  		<div class="modal-dialog" style="width: 80%;margin-top: 7%;margin-left: 150px;">
		<!-- Modal content-->
				<div class="modal-content" style="width: 200%;">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Impuestos<i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_c" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">
				  		<input type="hidden" id="idTicketAAsignar" value="">
						<div class="table-responsive">
				              <table id="listadoImpuestos" class="table" style="width:100%"> 
				                <thead>
									<tr>
										<th>Tax Country Code</th>
										<th>Tax Indicador</th>
										<th>Tax Currency</th>
										<th>Tax Nature Code</th>
										<th>Tax Amount</th>
						            </tr>
				                </thead>
				                <tbody style="text-align: center">

						        </tbody>
				              </table>
			            </div>
				    </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>

	<script>
		$(document).ready(function() {
			$('.select2').select2();
			
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});

			$( "#fecha_seña" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$( "#fecha_salida" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$( "#fecha_emision" ).datepicker({ 
								    altFormat: 'dd/mm/yy',
								    dateFormat: 'yy-mm-dd'
									});

			$("#itemList").select2({
					    ajax: {
					            url: "{{route('destinoGrupo')}}",
					            dataType: 'json',
					            placeholder: "Seleccione un destino",
					            delay: 0,
					            data: function (params) {
					                return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					                var results = $.map(data, function (value, key) {
					                    return {
					                        children: $.map(value, function (v) {
					                            return {
					                                id: key,
					                                text: v
					                            };
					                        })
					                    };
					                });
					                return {
					                    results: results,
					                };
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					            return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
			});
		
			


		});

		var options = { 
	                beforeSubmit:  showRequest,
					success: showResponse,
					dataType: 'json' 
	        }; 

	 	$('body').delegate('#image','change', function(){
	 		$('#upload').ajaxForm(options).submit();  		
	 	});

		function showRequest(formData, jqForm, options) { 
			$("#validation-errors").hide().empty();
			$("#output").css('display','none');
		    return true; 
		} 
		function showResponse(response, statusText, xhr, $form){ 

			if(response.success == false)
			{
				var arr = response.errors;
				console
				$.each(arr, function(index, value)
				{
					if (value.length != 0)
					{
						$("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
					}
				});
				$("#validation-errors").show();
			} else {
				 $("#imagen").val(response.archivo);	
				 $("#output").html("<img class='img-responsive' src='"+response.file+"'/>");
				 $("#output").css('display','block');
				 $("#btn-delete").html("<button type='button' id='btn-imagen' data-id='"+response.archivo+"'><i class='glyphicon glyphicon-remove'></i></button>");
				 eliminarImagen();
			}
		}
		
		function getImpuestos(idTicket){
			var dataString = {
							'ticke_asignado': idTicket
							 };

						$.ajax({
								type: "GET",
								url: "{{route('getImpuestos')}}",
								dataType: 'json',
								data: dataString,
								error: function(jqXHR,textStatus,errorThrown){
								  	$('#mensaje_cambio').css('color','red');
								  	$('#mensaje_cambio').html('Ocurrio un error en la comuniación con el servidor');
								  	$('.cargandoImg').hide();
		                        },
								success: function(rsp){
									//if(rsp.length > 0){	
										$.each(rsp, function (key, item){
											console.log(item);
											if(item.tax_nature_code == null){
												nature = '';
											}else{
												nature = item.tax_nature_code;
											}	
											tabla = '';
											tabla +='<tr>';
											tabla +='<td>'+item.tax_country_code+'</td>';
											tabla +='<td>'+item.tax_indicator+'</td>';
											tabla +='<td>'+item.tax_currency+'</td>';
											tabla +='<td>'+nature+'</td>';
											tabla +='<td>'+item.tax_amount+'</td>';

											tabla +='</tr>';
											$('#listadoImpuestos tbody').append(tabla);

										})
										$("#requestImpuestoTicket").modal("show");
									//	$("#impusto").css('display','block');
									/*}else{
										//$("#impusto").css('display','none');
									}	*/



								}
							})	
	}

	$(".anularTicket").click(function(){  
		return swal({
                title: "GESTUR",
                text: "¿Está seguro que desea eliminar el ticket?",
                showCancelButton: true,
                buttons: {
                        cancel: {
                                text: "No",
                                value: null,
                                visible: true,
                                className: "btn-warning",
                                closeModal: false,
                                },
                        confirm: {
                                text: "Sí, anular",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: false
                                }
                        }
                    }).then(isConfirm => {
                                if (isConfirm) {
									$.ajax({
											type: "GET",
											url: "{{route('anularTicket')}}",
											dataType: 'json',
											data: {
													dataTicket: $('#id_ticket').val()
									       			},
											success: function(rsp){
												console.log(rsp);
												if(rsp.status == 'OK'){
                                     				swal("Éxito", rsp.mensaje, "success");

													window.location.replace("{{route('indexTicket')}}");
												}else{
													swal("Cancelado", rsp.mensaje, "error");
												}
											}	
										});                                
									} else {
                                     swal("Cancelado", "", "error");
                                }
					})		

	})
	</script>
@endsection