@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
         <div class="card-header" style="border-radius: 15px;">
		        <h4 class="card-title">Tickets</h4>
		        <div class="heading-elements">
		            <ul class="list-inline mb-0">
		                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
		            </ul>
		        </div>
		    </div>
        <div class="card-content collapse show" aria-expanded="true">
        <div class="card-body pt-0">
					<a href="{{route('create')}}" title="Agregar Ticket" class="btn btn-success pull-right" role="button">
						<div class="fonticon-wrap">
							<i class="ft-plus-circle"></i>
						</div>
					</a>
				</div>		        
            <div class="card-body">
				<form id="frmBusqueda" method="get" action="{{route('getTicket')}}">
					
					<div class="row">
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label class="control-label">Nro Ticket</label>
								<input type="text" class ="form-control"  name="nro_ticket" id="nro_ticket" placeholder="" value="" style="text-transform: uppercase"/>
							</div>
						</div> 
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label class="control-label">PNR</label>
								<input type="text" class = "form-control" name="pnr" id="pnr" placeholder="PNR" value="" style="text-transform: uppercase"/>
							</div>
						</div> 

						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Pasajero</label>					            	
								<select class="form-control input-sm select2"  name="pasajero" id="pasajero" style="    padding-left: 0px;width: 100%;">
									<option value="">Seleccione Pasajero</option>
									@foreach($pasajeros as $pasajero)
										<option value="{{$pasajero->pasajero}}">{{$pasajero->pasajero}}</option>
									@endforeach   
								</select>
							</div>
						</div> 
						<div class="col-12 col-md-3" id="inputCiudad">
							 <div class="form-group">
								<label for="">Estado <span class=""></span></label>
								<select class="form-control select2" name="estado_id" id="estado_id" style="width: 100%;">
									<option value="">Seleccione Estado</option>
									@foreach($estados as $estado)
									    <option value="{{$estado->id}}">{{$estado->denominacion}}</option>
									@endforeach   
								</select>
							</div>
						</div>
				
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Moneda</label>					            	
								<select class="form-control input-sm select2"  name="moneda_id" id="moneda_id" style="width: 100%;">
									<option value="">Seleccione Moneda</option>
									@foreach($monedas as $moneda)
									    <option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
									@endforeach 
								</select>
							</div>
						</div> 

						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Fecha Pago</label>					            	
								<select class="form-control input-sm select2"  name="fecha_pago" id="fecha_pago" style="padding-left: 0px;width: 100%;">
									<option value="">Seleccione Fecha Pago</option>
									@foreach($calendarioBsp as $calendario)
										<option value="{{$calendario->fecha_de_pago}}">{{$calendario->fecha_pago_format}}</option>
									@endforeach 
								</select>
							</div>
						</div>

						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Emisión Desde - Hasta</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text"  class="form-control pull-right fecha" name="fecha_emision_desdeHasta" id="fecha_emision_desdeHasta" value="">
								</div>
							</div>
						</div>

						<div class="col-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>Check In Desde - Hasta</label>						 
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									  </div>
									<input type="text" disabled="disabled" class="form-control pull-right fecha" name="fecha_checkIn_desdeHasta" id="fecha_checkIn_desdeHasta" value="">
								</div>
							</div>							
						</div>
				
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Aerolinea</label>					            	
								<select class="form-control input-sm select2"  name="aerolinea_id" id="aerolinea_id" style="width: 100%;">
									<option value="">Todos</option>
									@foreach($aerolineas as $aerolinea)
									    <option value="{{$aerolinea->id}}">{{$aerolinea->nombre}} {{--$aerolinea->codigo_iata_3d--}}</option>
									@endforeach
								</select>
							</div>
						</div> 

						<div class="col-12 col-md-3">
							<div class="form-group">
								<label class="control-label">Nro Proforma</label>
								<input type="text" class = "form-control" name="id_proforma" id="id_proforma" value=""/>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label class="control-label">Nro Factura</label>
								<input type="text" class = "form-control" name="nro_factura" id="nro_factura"  value=""/>
							</div>
						</div> 
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Origen</label>					            	
								<select class="form-control input-sm select2"  name="tipo_ticket" id="tipo_ticket" style="width: 100%;">
									<option value="">Seleccione Origen</option>
									@foreach($tipoTickets as $tipo_ticket)
									    <option value="{{$tipo_ticket['id']}}">{{$tipo_ticket['descripcion']}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-md-3" style="display: none;" id="grupos_salida" >
							<div class="form-group">
								<label>Grupos</label>                                 
								<select class="form-control input-sm select2"  name="grupo_id" id="grupo_id" style="width: 100%;">
									<option value="">Seleccione Grupo</option>
									@foreach($grupos as $grupo)
										<option value="{{$grupo->id}}">
											{{$grupo->denominacion}} - 
											{{isset($grupo->destino) ? $grupo->destino->desc_destino : ''}}
										</option>
									@endforeach
								</select>
							</div>
						</div>  
						<div class="col-12 col-md-3">
							<div class="form-group">
								<label>Vendedor Empresa</label>					            	
								<select class="form-control input-sm select2"  name="id_vendedor_empresa" id="id_vendedor_empresa" style="width: 100%;">
									<option value="">Seleccione Vendedor</option>
									@foreach ($vendedorProforma as $vendedor)
									    <option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
									@endforeach
								</select>
							</div>
						 </div> 
						 <div class="col-12 col-md-3" id="mostrar_precio" >
							<div class="form-group">
								<label>Mostrar tickets Precio 0 (cero)</label>					            	
								<select class="form-control input-sm select2"  name="mostrar_precio" id="mostrar_precio" style="width: 100%;">
									<option value="N">NO</option>
									<option value="S">SI</option>
								</select>
							</div>
						</div> 
						 <div class="col-12 col-md-3">
							<div class="form-group">
								<label class="control-label">Total Guaranies</label>
								<input type="text" class = "form-control numerico" name="totalGuaranies" id="totalGuaranies" value="0" disabled/>
							</div>
						</div>
						<div class="col-12 col-md-3"> 
							<div class="form-group">
								<label class="control-label">Total Dolares</label>
								<input type="text" class = "form-control numerico" name="totalDolares" id="totalDolares" value="0" disabled/>
							</div>
						</div>
						<div class="col-12 col-md-3" >
							<div class="form-group">
								<label>Asignado</label>					            	
								<select class="form-control input-sm select2"  name="asignado" id="asignado" style="width: 100%;">
									<option value="">Todos</option>
									<option value="0">NO</option>
									<option value="1">SI</option>
								</select>
							</div>
						</div>
				
						<div class="col-12" >
							<button type="button" id="botonExcel" class="pull-right btn btn-success btn-lg mr-1"><b>Excel</b></button>
							<button type="button" id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white mr-1"  ><b>Limpiar</b></button>
							<button type="button" onclick="buscar();" id="btnBuscar" class="btn btn-info btn-lg pull-right mr-1"><b>Buscar</b></button>
						</div>
					</div>	
            	</form>	
				<div class="row">
					<div class="col-12 col-md-1"></div>
					<div class="col-12 col-md-11" style="color:red;  font-weight:bold;">
						Los tickets con el número rojo, son ticket asociados
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-md-1"></div>
					<div class="col-12 col-md-1">
					    <b style="color:red;">Disponible</b>
					</div>
					<div class="col-12 col-md-1">
						<b style="color:yellow;">En Proforma</b>
					</div>
					<div class="col-12 col-md-1">
						<b style="color:blue;">Facturado</b>
					</div>
					<div class="col-12 col-md-1">
						<b style="color:orange;">Pagado TC</b>
					</div>
					<div class="col-12 col-md-1">
						<b style="color:#1ab0c3;">Con Seña</b>
					</div>
					<div class="col-12 col-md-1">
						<b style="color:black;">Voideado</b>
					</div>
					<div class="col-12 col-md-2">
					    <b style="color:green;">Facturado y Cobrado</b>
					</div>
					<div class="col-12 col-md-3"></div>
				</div>	
				<div class="table-responsive">
            	 <br>	
	              <table id="listado" class="table" style="width: 100%;">

	                <thead>
						<tr>
							<th></th>
							<th>Ticket</th>
							<th>Cliente</th>
							<th>Pasajero</th>
							<th>Aerolínea</th>
							<th>Emisión</th>
							<th>Usuario</th>
							<th>Proforma</th>
							<th>Check In-Out</th>
							<th>Usuario Proforma</th>
							<th style="width: 150px;">TC</th>
							<th>Pago BSP</th>
							<th>Tipo </th>
							<th>Origen</th>
							<th>Estado</th>
							<th style="width: 130px;">Factura</th>
							<th>PNR</th>
							<th>Moneda</th>
							<th>Costo</th>
							<th>Venta</th>
							<th>Pedido</th>
							<th>Origen</th>
							<th>Asignado</th>
			            </tr>
	                </thead>
	                
	                <tbody style="text-align: center">
	                </tbody>
	              </table>
            	</div>
            </div>	
        </div>    
    </div>    
</section>

	    <div id="requestTicketEstado" class="modal fade" role="dialog">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">CAMBIAR ESTADO TICKET <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_cambio" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">


								<!--DISPONIBLE -->
				          		<div class="row"  style="" id="opcion_1">
				          			<div class="col-md-12">

							          <div class="form-group">
							            <label>Tipo Factura</label>						 
							            <select class="form-control select2" name="" id="select_1" style="width: 100%;">
												<option value="">Seleccione Estado</option>
												<option value="33">Voideado/Cancelado</option>
												<option value="32">Seña/Deposito</option>
							            </select>
									</div>   
				          		</div>

				          		</div>
								
								<!--A DEFINIR -->
				          		<div class="row"  style="display: none" id="opcion_2">
				          			<div class="col-md-12">
							                <div class="form-group">
							            <label>Tipo Factura</label>						 
							            <select class="form-control select2" name="" id="select_2" style="width: 100%;">
											<option value="">Seleccione Estado</option>
											<option value="33">Voideado/Cancelado</option>
											<option value="25">Disponible</option>
							         
							            </select>
									</div> 
							          </div>
				          		</div>
							
					<button type="button" id="cambio_estado" class="btn btn-success" >Guardar</button>
					<input type="hidden" id="opcion_hidden" value="">
					<input type="hidden" id="id_ticket_hidden" value="">

				  </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>


	<div id="requestAsignarTicket" class="modal fade" role="dialog">
	  		<div class="modal-dialog" style="width: 80%;margin-top: 2%;margin-left: 50px;">
		<!-- Modal content-->
				<div class="modal-content" style="width: 260%;">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Asignar Ticket<i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_c" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">
				  		<input type="hidden" id="idTicketAAsignar" value="">
						<div class="table-responsive">
				              <table id="listadoTickets" class="table" style="width:100%">
				                <thead>
									<tr>
										<th>Nro. Tickets</th>
										<th>Aerolínea</th>
										<th>Fecha Emisión</th>
										<th>Usuario Emisión</th>
										<th>Nro. Proforma</th>
										<th>Usuario Proforma</th>
										<th>Nro Fact.</th>
										<th>PNR</th>
										<th>Moneda</th>
										<th>Costo</th>
										<th>Venta</th>
										<th></th>
						            </tr>
				                </thead>
				                
				                <tbody style="text-align: center">

						        </tbody>
				              </table>
			            </div>
				    </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>

	<div id="requestAsignarFactura" class="modal fade" role="dialog">
	  		<div class="modal-dialog" style="width: 60%;margin-top: 10%;margin-left: 28%;">
		<!-- Modal content-->
				<div class="modal-content" style="width: 230%;">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Asignar Factura<i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						<span id="mensaje_c" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">
				  		<div class="row">
				  			<div class="col-md-1">
				  			</div>
				  			<div class="col-md-2">
								<label class="control-label">Ingrese Nro Factura</label>
							</div>
							<div class="col-md-4">		
								<input type="text" class = "form-control" name="nroFactura" id="nroFactura" placeholder="" value="" style="text-transform: uppercase"/>
							</div>
							<div class="col-md-4"> 
								<a onclick="buscarFactura()" role="button" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;"><i class="fa fa-search"></i>   <b>Buscar</b></a>
							</div>
				  		</div>
				  		<br>
				  		<br>
				  		<input type="hidden" id="idAsignarFactura" value="">

						<div class="table-responsive">
				              <table id="listadoFacturas" class="table" style="width:100%">
				                <thead>
									<tr>
										<th>Número Factura</th>
										<th>Fecha Facturación</th>
										<th>Cliente</th>
										<th>Pasajero</th>
										<th>Número Proforma</th>
										<th>Vendedor</th>
										<th>Asignar</th>
						            </tr>
				                </thead>
				                
				                <tbody style="text-align: center">

						        </tbody>
				              </table>
			            </div>
				    </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>


<div id="requestIvaTicket" class="modal fade" role="dialog">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">CAMBIAR ESTADO TICKET <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_c" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">


					
				          		<div class="row" >
				          			<div class="col-md-12">

							          <div class="form-group">
							            <label>Tipo IVA</label>						 
							            <select class="form-control select2" id="iva_select" style="width: 100%;">
												<option value="10">IVA 10%</option>
												<option value="2.5">IVA 2.5%</option>
										
							         
							            </select>
									</div>   
				          		</div>

				          		</div>
							
					<button type="button" id="btn_cambiar_iva" class="btn btn-danger" >Cambiar</button>
					<input type="hidden" id="id_ticket_iva" value="">

				  </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>

	<div id="requestPagoTC" class="modal fade" role="dialog">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">PAGO TC <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_c" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">
					  	<form id="frmPagoTC" method="POST" enctype="multipart/form-data">
							<div class="row" >
								<div class="col-md-12">
									<div class="form-group">
										<label>Tarjeta Utilizada</label>	
										<select class="form-control select2" id="tarjeta_utilizada" style="width: 100%;">
											@foreach($tarjetas as $tarjeta )
												<option value="{{$tarjeta->id_banco_detalle}}">{{$tarjeta->banco_detalle->numero_cuenta.' - '.$tarjeta->banco_detalle->currency->currency_code}} ({{$tarjeta->banco_detalle->banco_cabecera->nombre}})</option>
											@endforeach
											
										</select>
									</div>   
								</div>
							</div>
							<div class="row" >
								<div class="col-md-12">
									<div class="form-group">
										<label>Nro.de Voucher/Comprobante.</label>						 
										<input type="text" class = "form-control" name="comprante" id="comprante" placeholder="" value="" />								
									</div>   
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="adjunto_modal_tc">Adjunto OP</label>
										<input type="file" class="form-control-file" name="adjunto_tc" id="adjunto_modal_tc" accept=".pdf, image/*" required>
										<small>Solo se acepta imagenes y pdf <span id="msg_error_adjunto" style="colot:red"></span></small>
									  </div>
								</div> 
							</div>
							<input type="hidden" id="id_ticket_pago" name="id_ticket_pago">
						</form>
				  </div> 
				  <div class="modal-footer">
				  	<button type="button" id="btn_pago_tc" class="btn btn-info" >Pagar</button>
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>

    <div id="requestPorcentajeComision" class="modal fade" role="dialog">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">CAMBIAR COMISION <i style="display:none;" class="fa fa-refresh fa-spin cargandoImg"></i></h2>
						
						<span id="mensaje_comision" style="font-weight: bold;"></span>
					</div>
				  	<div class="modal-body">
								<div class="row" >
									<div class="col-md-12">
										<div class="form-group">
											<label> Codigo Over Táctico</label>						 
											<input type="text" class = "form-control" name="over_tactico" id="over_tactico" placeholder="" value="" />								
										</div>   
									</div>
								</div>
				          		<div class="row" >
				          			<div class="col-md-12">
							          <div class="form-group">
							            <label>Comisión</label>						 
							            <select class="form-control select2" id="comision_select" style="width: 100%;">
												<option value="0">IVA 0%</option>
												<option value="1">IVA 1%</option>
												<option value="2">IVA 2%</option>		
												<option value="3">IVA 3%</option>	
												<option value="4">IVA 4%</option>	
												<option value="5">IVA 5%</option>
												<option value="6">IVA 6%</option>
												<option value="7">IVA 7%</option>
												<option value="8">IVA 8%</option>
												<option value="9">IVA 9%</option>
												<option value="10">IVA 10%</option>
												<option value="11">IVA 11%</option>
												<option value="12">IVA 12%</option>
												<option value="13">IVA 13%</option>
												<option value="14">IVA 14%</option>
												<option value="15">IVA 15%</option>
												<option value="16">IVA 16%</option>
												<option value="18">IVA 18%</option>
												<option value="19">IVA 19%</option>
												<option value="20">IVA 20%</option>
												<option value="21">IVA 21%</option>
												<option value="21">IVA 23%</option>
												<option value="26">IVA 26%</option>
										</select>
									</div>   
				          		</div>
			          		</div>

					<button type="button" id="btn_cambiar_comision" class="btn btn-danger" >Cambiar Comisión</button>
					<input type="hidden" id="id_ticket_comision" value="">

				  </div> 
				  <div class="modal-footer">
					<button type="button" id="" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Salir</button>
				  </div>
			</div>
	  	</div>
	</div>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>

	<script>
	

	$(document).ready(function(){

		
		$('.select2').select2();
		// $("#estado_id").select2().val('25').trigger("change");
		searchHomeData();
			$('#grupos_salida').hide();
			var base = "{{$id}}";
			if(base != 0){
				$("#tipo_ticket").select2().val('{{$tipo_grupo}}').trigger("change");
				$('#grupos_salida').show();
				$("#grupo_id").select2().val('{{$id}}').trigger("change");
				//$("#btnBuscar").trigger('click');
			}else{
				//$("#btnBuscar").trigger('click');
			}
			// buscar();
	});


	
		$('#estado_id').change(function(){ 
			origen = $(this).val();
			if(origen == 26 || origen == 27){
				$('#fecha_checkIn_desdeHasta').prop('disabled',false);
			}else{
				$('#fecha_checkIn_desdeHasta').prop('disabled',true);
			}
		});	


			function searchHomeData(){
				let id_tipo_t = ('{{$id_tipo_ticket}}' != '') ? '{{$id_tipo_ticket}}': '';
				let id_estado = ('{{$id_estado}}' != '') ? '{{$id_estado}}' : '25';

					  $("#tipo_ticket").select2().val(id_tipo_t).trigger("change");
					  $("#estado_id").select2().val(id_estado).trigger("change");
	
			};

			$("#listadoTicket").dataTable();

			//ASIGNAR TAB INDEX
			document.getElementById("nro_ticket").tabIndex = "1";
			document.getElementById("pnr").tabIndex = "2";
			document.getElementById("pasajero").tabIndex = "3";
			document.getElementById("fecha_emision_desdeHasta").tabIndex = "4";
			document.getElementById("fecha_pago").tabIndex = "5";
			document.getElementById("moneda_id").tabIndex = "6";
			document.getElementById("aerolinea_id").tabIndex = "7";
			document.getElementById("id_proforma").tabIndex = "8";
			document.getElementById("nro_factura").tabIndex = "9";
			document.getElementById("tipo_ticket").tabIndex = "10";
			document.getElementById("btnBuscar").tabIndex = "11";

				
			$('input[name="fecha_emision_desdeHasta"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													     cancelLabel: 'Limpiar'
													    },
													 
								    			});
			//LIMPIAR CALENDARIO 
			 $('input[name="fecha_emision_desdeHasta"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

			$('#fecha_emision_desdeHasta').val('');


			// $("#btnBuscar").click(function(){
			// 	buscar();
				/*var dataString = {'nro_ticket':$('#nro_ticket').val(),
								  'pnr': $('#pnr').val(),
								  'pasajero': $('#pasajero').val(),
								  'estado_id' : $('#estado_id').val(),
								  'fecha_emision_desdeHasta' :$('#fecha_emision_desdeHasta').val(),
								  'fecha_pago': $('#fecha_pago').val(),
								  'moneda_id' : $('#moneda_id').val(),
								  'aerolinea_id' : $('#aerolinea_id').val(),
								  'id_proforma' : $('#id_proforma').val(),
								  'nro_factura' : $('#nro_factura').val(),
								  'tipo_ticket' : $('#tipo_ticket').val(),
								  'id_vendedor_empresa' : $('#id_vendedor_empresa').val(),
								  'grupo_id' : $('#grupo_id').val()
							   	   };


				buscarTicket(dataString);*/
			// });

	$("#btnLimpiar").click(function(){
		
			$('#pasajero').val('').trigger('change.select2');
			$('#moneda_id').val('').trigger('change.select2');
			$('#estado_id').val('').trigger('change.select2');
			$('#fecha_pago').val('').trigger('change.select2');
			$('#aerolinea_id').val('').trigger('change.select2');
			$('#nro_ticket').val("");
			$('#pnr').val("");
			$('#fecha_emision_desdeHasta').val("");
			$('#id_proforma').val("");
			$('#nro_factura').val("");
			$('#id_vendedor_empresa').val('');
			$('#asignado').val('').trigger('change.select2');
			
	});

	$("#botonExcel").on("click", function(e){ 
			console.log('Inicil');
                e.preventDefault();
                 $('#frmBusqueda').attr('method','post');
               $('#frmBusqueda').attr('action', "{{route('generarExcelReporteTickets')}}").submit();
            });

	$('.crearTicket').on('click', function(e){

		location.href ="{{route('create')}}";
	});

		



function cambiarEstadoTicket(data,idTicket){
	var ok = 0;
	var estado = '';
	$('.cargandoImg').hide();
	$('#mensaje_cambio').html('');
	$('#id_ticket_hidden').val(idTicket);


	if(data == 1){
	$('#opcion_1').show();	
	$('#opcion_2').hide();	
	$('#opcion_hidden').val('1');		
	} else {
	$('#opcion_1').hide();
	$('#opcion_2').show();	
	$('#opcion_hidden').val('2');
	}

	$("#requestTicketEstado").modal("show");


	}//fucntion


	$("#tipo_ticket").change(function(){ 
		origen = $(this).val();
		if(origen == 3 || origen == 2){
			$('#grupos_salida').show();
		}else{
			$('#grupos_salida').hide();
		}


	});
		

			$('input[name="fecha_checkIn_desdeHasta"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													     cancelLabel: 'Limpiar'
													    },
													 
								    			});
			//LIMPIAR CALENDARIO 
			 $('input[name="fecha_checkIn_desdeHasta"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });

			$('#fecha_checkIn_desdeHasta').val('');


	$('#cambio_estado').on("click",function(){

		$('.cargandoImg').show();
		var dato = $('#opcion_hidden').val();
		var idTicket = $('#id_ticket_hidden').val();
		var estado = '';
		var ok = 0;

	if(dato == 1){
	 estado = ($('#select_1').val() != '') ? $('#select_1').val() : ok++;		
	} else {
	 estado = ($('#select_2').val() != '') ? $('#select_2').val() : ok++;
	}

	if(ok == 0){

		$.ajax({
						type: "GET",
						url: "{{route('cambiarEstadoTicket')}}",
						dataType: 'json',
						data: { estado : estado,
								id: idTicket,
								opcion:dato},

						  error: function(jqXHR,textStatus,errorThrown){
						  	$('#mensaje_cambio').css('color','red');
						  	$('#mensaje_cambio').html('Ocurrio un error en la comuniación con el servidor');
						  	$('.cargandoImg').hide();
                        },
						success: function(rsp){

						$('.cargandoImg').hide();

						if(rsp.resp == true){
						$('#mensaje_cambio').css('color','green');
						$('#mensaje_cambio').html('El cambio se realizo con éxito');
						} else {
							$('#mensaje_cambio').css('color','red');
						  	$('#mensaje_cambio').html('Ocurrio un error en la operación');
						}




									}//success
						});	

	} else {
		$('#mensaje_cambio').css('color','red');
		$('#mensaje_cambio').html('Seleccione una opción porfavor');
		$('.cargandoImg').hide();
	}


	});//function



		function cambiarIvaTicket(idTicket,porc_iva){
			console.log(porc_iva);
			
		$('#id_ticket_iva').val(idTicket);
		$('#iva_select').val(String(porc_iva)).trigger('change.select2');
		$('#mensaje_c').html(' ');
		$('.cargandoImg').hide();
		$("#requestIvaTicket").modal("show");

	}


	function cambiarPorcentajeTicket(idTicket,porc_iva,codigo){
		$('#id_ticket_comision').val(idTicket);

		if(codigo !== 'null'){
			$('#over_tactico').prop('disabled',true);
		}else{
			$('#over_tactico').prop('disabled',false);
			codigo = '';
		}
		$('#over_tactico').val(codigo);
		$('#comision_select').val(String(porc_iva)).trigger('change.select2');
		$('#mensaje_comision').html(' ');
		$('.cargandoImgen').hide();
		$("#requestPorcentajeComision").modal("show");

	}

			function formatearFecha(texto)
 		{
	        if(texto != '' && texto != null)
	        {
	          	return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
	        } 
	        else 
	        {
	         	return '';    
         	}
        }


		function asignarFactura(idFactura)
        {
        	var dataString = {
								  'id_ticket':$("#idAsignarFactura").val(),
								  'id_factura':idFactura
							   	   };

			
			return swal({
                        title: "GESTUR",
                        text: "¿Está seguro de que desea asignar la factura al ticket?",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, Asignar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
	                                    $.ajax({
											type: "GET",
											url: "{{route('asignarFactura')}}",
											dataType: 'json',
											data: dataString,
																error: function(jqXHR,textStatus,errorThrown){
																  	$('#mensaje_cambio').css('color','red');
																  	$('#mensaje_cambio').html('Ocurrió un error en la comuniación con el servidor');
																  	$('.cargandoImg').hide();
										                        },
																success: function(rsp){
																					if(rsp.status == 'OK'){
																						$.toast({
																								heading: 'Éxito',
																								text: rsp.mensaje,
																								position: 'top-right',
																								showHideTransition: 'slide',
																				    			icon: 'success'
																							});  
																						swal("Exito", rsp.mensaje, "success");
																						$("#requestAsignarFactura").modal("hide");7
																						buscar();
																					}else{
																						$.toast({
																								heading: 'Error',
																								text: rsp.mensaje,
																								showHideTransition: 'fade',
																								position: 'top-right',
																								icon: 'error'
																							});	
																						swal("Cancelado", rsp.mensaje, "error");
																						//$("#requestAsignarFactura").modal("hide");			
																					}
																		}
														});

                                } else {
                                     swal("Cancelado", "", "error");
									 $("#requestAsignarFactura").modal("hide");
                                }
            	});
	       }
	function mostrarModalFacturas(idTicket)
	{ 
		$("#idAsignarFactura").val(idTicket);
		$('#nroFactura').val('');

		var oSettings = $('#listadoFacturas').dataTable().fnSettings();
		var iTotalRecords = oSettings.fnRecordsTotal();
		for (i=0;i<=iTotalRecords;i++) {
			$('#listadoFacturas').dataTable().fnDeleteRow(0,null,true);
		}

		$("#requestAsignarFactura").modal("show");
	};

	function buscarFactura(idTicket)
	{ 

        var tableFacturas = $("#listadoFacturas").DataTable
			({
				"destroy": true,
				"ajax": 
				{
					"url": "{{route('mostrarFacturas')}}",
					"data": {
			        		"factura": $('#nroFactura').val()
			   				},

					"type": "GET",
					error: function(jqXHR,textStatus,errorThrown)
					{
		                $.toast
		                ({
		                    heading: 'Error',
		                    text: 'Ocurrió un error en la comunicación con el servidor.',
		                    position: 'top-right',
		                    showHideTransition: 'fade',
		                    icon: 'error'
		                });

		                }			    
					},
				"columns": 
				[
					{ "data": "nro_factura" },					

					{ "data": function(x)
						{
							// console.log(x);
							var fecha = x.fecha_hora_facturacion.split(' ');
							var f = formatearFecha(fecha[0]);
							return f+' '+fecha[1];
						} 
					},

					{ "data": function(x)
						{
							if (x.cliente == null) 
							{
								return '';
							}
							else
							{
								return x.cliente.nombre;
							}
							
						} 
					},

					{ "data": function(x)
						{
							if (x.pasajero == null) 
							{
								return '';
							}
							else
							{
								if (x.pasajero.apellido == null) 
								{
									return x.pasajero.nombre+" "+' ';
								}
								else
								{
									return x.pasajero.nombre+" "+x.pasajero.apellido;
								}
							}
						} 
					},

					{ "data": "id_proforma"},

					{ "data": function(x)
						{
							if (x.vendedor_empresa == null) 
							{
								return '';
							}
							else
							{
								if (x.vendedor_empresa.apellido == null) 
								{
									return x.vendedor_empresa.nombre+" "+' ';
								}
								else
								{
									return x.vendedor_empresa.nombre+" "+x.vendedor_empresa.apellido;
								}
							}
							
						} 
					},

					{ "data": function(x)
						{
							var btn = `<a onclick="asignarFactura(${x.id})" class="btn btn-danger" style="background: #e2076a !important;padding-left: 6px;padding-right: 6px;" role="button"><i class="fa fa-fw fa-check-square"></i></a>`;
								return btn;
						} 
					}, 
					


					]
						
				});	
				return tableFacturas;

	}


	function asignarTicket(idTicket){

		$("#idTicketAAsignar").val(idTicket);
	
			var tableTickets = $("#listadoTickets").DataTable
			({
				"destroy": true,
				"ajax": 
				{
					"url": "{{route('ajaxTicket')}}",
					"type": "GET",
					"data": {'tipo_ticket': 2, id_ticket : idTicket},
					error: function(jqXHR,textStatus,errorThrown)
					{
		                $.toast
		                ({
		                    heading: 'Error',
		                    text: 'Ocurrió un error en la comunicación con el servidor.',
		                    position: 'top-right',
		                    showHideTransition: 'fade',
		                    icon: 'error'
		                });

		                }			    
					},
				"columns": 
				[
					{ "data": "numero_amadeus" },			
					{ "data": "nombre_proveedor" },			
					{ "data": "fecha_emision_formateo" },			
					{ "data": "usuario" },
					{ "data": "id_proforma" },			
					{ "data": function(x)
						{
							return x.pro_per_n+' '+x.pro_ape_a;
						}
					},			
					{ "data": "nro_factura" },			
					{ "data": "pnr" },			
					{ "data": "currency_code" },			
					{ "data": "total_costo" },			
					{ "data": "total_venta" },			
					{ "data": function(x)
						{
							var btn = '<a onclick="asignarTickets('+x.id+')" role="button" class="btn btn-danger" style="margin-left:5px;background: #e2076a !important;padding-left: 6px;padding-right: 6px;"><i class="fa fa-edit"></i></a>';
								return btn;
						} 
					}, 
				]	
			});	


		$("#requestAsignarTicket").modal("show");
		return tableTickets;


	}

	function pagoTC(id){
		$("#id_ticket_pago").val(id);
		var dataString = {
						'id_ticket':id,
						};
		$.ajax({
				type: "GET",
				url: "{{route('pagoTicketRealizado')}}",
				dataType: 'json',
				data: dataString,
				error: function(jqXHR,textStatus,errorThrown){
								  	$('#mensaje_cambio').css('color','red');
								  	$('#mensaje_cambio').html('Ocurrio un error en la comuniación con el servidor');
								  	$('.cargandoImg').hide();
		        },
				success: function(rsp){
										console.log(rsp.nro_voucher_comprobante);
										if(jQuery.isEmptyObject(rsp.nro_voucher_comprobante) == false){
											$('#comprante').val(rsp.nro_voucher_comprobante);
											$('#comprante').prop('disabled',true);
											$('#tarjeta_utilizada').val(rsp.id_tarjeta_pago).trigger('change.select2');
											$('#tarjeta_utilizada').prop('disabled',true);
											$('#btn_pago_tc').prop('disabled',true);
										}else{
											$('#comprante').val('');
											$('#comprante').prop('disabled',false);
											$('#tarjeta_utilizada').val(1).trigger('change.select2');
											$('#tarjeta_utilizada').prop('disabled',false);
											$('#btn_pago_tc').prop('disabled',false);
										}
									}
			})	

		$("#requestPagoTC").modal("show");
	}

	
	$('#btnVerificacion').click(function()
			{
					BootstrapDialog.confirm({
				            title: '<b>GESTUR</b>',
				            message: '¿Está seguro de que desea asignar la factura al ticket?',
				            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
				            closable: true, // <-- Default value is false
				            draggable: true, // <-- Default value is false
				            btnOKLabel: 'Sí, asignar', // <-- Default value is 'OK',
				            btnCancelLabel: 'No', // <-- Default value is 'Cancel',
				            btnOKClass: 'btn-error', // <-- If you didn't specify it, dialog type will be used,
				            callback: function(result) 
				            {
				            	if(result)
				            	{
				              		verificarProforma();
				            	}
							}
					});
			});


	function buscar(){ 
		// console.log('BUSCAR!!!');

		 	$.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando...</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });
		 	setTimeout(function (){
			$("#totalGuaranies").val(0);
			$("#totalDolares").val(0);
			$("#listado").dataTable({
								"destroy": true,
								"pageLength": 10,  // Aumenta este número para mostrar más registros por página
								"processing": true,
								"serverSide": true,
								"ajax": {
								"url": "{{route('ajaxTickets')}}",
								 "type": "GET",
								 "data": {
									        "formSearch": $('#frmBusqueda').serializeArray() },
								 error: function(jqXHR,textStatus,errorThrown){

		                           $.toast({
		                            heading: 'Error',
		                            text: 'Ocurrio un error en la comunicación con el servidor.',
		                            position: 'top-right',
		                            showHideTransition: 'fade',
		                            icon: 'error'
		                        });

		                        }
									    
							    },"aaSorting":[[2,"desc"]],

					            "columns": [
									            { "data": function(x){
									            		var n = "";	
														console.log(x.estado_cobro_ticket);
															if(x.id_estado == 27){
																if(x.estado_cobro_ticket == 'SI'){
																	if(x.saldo_factura > 0){
																		var color ="blue";
																	}else{
																		var color ="green";
																	}
																}else{
																	if(x.saldo_factura > 0){
																		var color ="blue";
																	}else{
																		var color ="green";
																	}
																}
															}else if(x.id_estado == 26){
																var color ="yellow";
															}else if(x.id_estado == 25){
																if(jQuery.isEmptyObject(x.id_proforma) == false && x.tipo_pago=='TC'){
																	var color ="orange";
																}else{
																	var color ="red";
																}
															}else if(x.id_estado == 33){
																 var color ="black";
															}
									            		acciones = '<div class="btn-group">';
														acciones += '<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:'+color+' !important"><i class="fa fa-edit"></i><span class="caret" width="100px"></span></button>';
														  
														acciones += '<ul class="dropdown-menu" style=" width: 250px !important;">';
														
														acciones += '<li class="btnHand">'
														acciones += '<a href="verDetalleTicket/'+x.id+'"  class="verificarStyle" title ="Ver Detalle" id=""><i class="fa fa-fw fa-search"></i><b>Ver Detalle</b></a>';
														acciones += '</li>';	
														
														if(x.id_estado == 25 || x.id_estado == 26 || x.id_estado == 27) {
															@if(count($tarjetas))
																acciones += '<li class="btnHand">'
																acciones += '<a onclick="pagoTC('+x.id+')" class="verificarStyle" title ="Pago con TC" id=""><i class="fa fa-credit-card"></i><b>Pago con TC</b></a>';
																acciones += '</li>';
															@endif	
														}
														if(x.btn1 == 1){
											            	if(x.id_proveedor == 520 && x.id_estado == 25)
													          {
													            acciones += '<li class="btnHand">'
																acciones += '<a href="edit/'+x.id+'" class="verificarStyle"  title ="Tasa YQ" id=""><i class="fa fa-fw fa-plane"></i><b>Tasa YQ</b></a>';
																acciones += '</li>';	
													           }	
													        }
														if(x.btn6 == 1){
													            acciones += '<li class="btnHand">'
																acciones += `<a onclick="cambiarPorcentajeTicket(`+x.id+`,`+x.porc_comision+`,'`+x.over_tactico+`')" class="verificarStyle"  title ="Cambiar Comision" id=""><i class="fa fa-edit"></i><b>Cambiar Comision</b></a>`;
																acciones += '</li>';	
														  } 	
														 if(x.btn5 == 1){
													          if(x.id_proveedor == 505 && x.id_estado == 25)
													          {
													            acciones += '<li class="btnHand">'
																acciones += '<a onclick="cambiarIvaTicket('+x.id+','+x.porc_iva+')" class="verificarStyle"  title ="Cambiar IVA" id=""><i class="fa fa-fw fa-plane"></i><b>Cambiar Comision</b></a>';
																acciones += '</li>';	
													          }  
													     } 
															if(x.btn2 == 1){	
																 var numero_amadeus = x.numero_amadeus
														 		 if(x.id_estado == 25 && numero_amadeus.length >= 9){
														          	  acciones += '<li class="btnHand">'
																	  acciones += '<a onclick="asignarTicket('+x.id+')" class="verificarStyle" title ="Asignar Ticket Amadeus" id=""><i class="fa fa-files-o"></i><b>Asignar Ticket Amadeus</b></a>';
																	  acciones += '</li>';	
														          }
															}  

															if(x.btn7 == 1){	
														 		// if(x.id_estado == 25 && x.facial == 0 &&   x.comision == 0 && x.iva_comision == 0 && x.fee == 0 && x.total_ticket == 0*/) {
														          	  acciones += '<li class="btnHand">'
																	  acciones += '<a onclick="mostrarModalFacturas('+x.id+')" class="verificarStyle" title ="Asignar Factura"><i class="fa fa-ticket"></i><b>Asignar Factura</b></a>';
																	  acciones += '</li>';	
														       //   }
															}  

														    if(x.btn3 == 1){
 																acciones += '<li class="btnHand">'
																acciones += '<a href="addUser/'+x.id+'"  class="verificarStyle" title ="Asignar Vendedor" id=""><i class="fa fa-fw fa-search"></i><b>Asignar Vendedor</b></a>';
																acciones += '</li>';	
													 		}	         
													          //DISPONIBLE BTN PARA CAMBIAR ESTADO
													       if(x.btn4 == 1){ 
														        if(x.id_estado == 25){
														            var n = 1;
	 																acciones += '<li class="btnHand">'
																	acciones += '<a onclick="cambiarEstadoTicket('+n+','+x.id+')" class="verificarStyle" title ="Cambiar Estado" id=""><i class="fa fa-edit"></i><b>Cambiar Estado</b></a>';
																	acciones += '</li>';	
														         }
														    }     
														acciones += '</li>';
														acciones += '</ul>';
														acciones += '</div>';

													//	console.log(acciones);

														return acciones;
									            	} 
									        	},
									            { "data": function (x){
													//TODO aqui poner el numero de rojo y en negrita
													// Eliminar o ocultar estado asignado
													if(x.asignado){
														return '<div style="color:red; font-weight:bold;">'+x.numero_amadeus+'</div>';	
													}
													return x.numero_amadeus;
												}},
									            { "data": function(x){
									            	var nombre = x.cliente_n; 
									            	if(jQuery.isEmptyObject(x.cliente_n) == false){
									            		cliente = '<b>'+x.cliente_n+'</b>';
									            	}else{
									            		cliente = "";
									            	}
									            	return cliente;
									            } },
												{ "data": "pasajero" },
												{ "data": function(x){
									            	nombre = x.nombre_proveedor; 
													if(jQuery.isEmptyObject(x.codigo_iata_3d) == false){
														nombre = '<b>'+x.nombre_proveedor+" - "+x.codigo_iata_3d+'</b>';
													}else{
														nombre = '<b>'+nombre+'</b>';
													}
									            	return nombre;
									            } },
									            { "data": "fecha_emision_formateo" }, 
												{ "data": function(x){
													cliente = '<b>'+x.usuario+'</b>';
									            	if(jQuery.isEmptyObject(x.usuario_amadeus_n) == false){
									            		cliente += ' |  <b>'+x.usuario_amadeus_n+' '+x.usuario_amadeus_a+'-'+x.usuario_amadeus+'</b>';
									            	}else{
									            		cliente += "";
									            	}
									            	return cliente;
									            } },
									            { "data": "id_proforma" },
												{ "data": function(x){
													check_in_out = '';
													if(jQuery.isEmptyObject(x.fecha_checkin_formateo) == false){
										            	v_in = x.fecha_checkin_formateo;
										             }else{
										        		v_in ="";
										            }
													if(jQuery.isEmptyObject(x.fecha_checkout_formateo) == false){
										            	v_out = x.fecha_checkout_formateo;
										             }else{
										        		v_out ="";
										            } 
													check_in_out =  v_in+' - '+v_out;
													if(check_in_out == ' - '){
										            	check_in_out = '';
										             }
									            	return check_in_out;
									            }},
									            { "data": function(x){
									            	if(jQuery.isEmptyObject(x.pro_per_n) == false){
										            	proveedor =  x.pro_per_n+' '+x.pro_ape_a;
										             }else{
										        		proveedor ="";
										            }
									            	return proveedor;
									            } },
												{ "data": function(x){
													if(jQuery.isEmptyObject(x.nro_tarjeta) == false){
										            	nro_tarjeta = '<b>'+x.nro_tarjeta+'</b>';
										             }else{
										        		nro_tarjeta ="";
										            }
									            	return nro_tarjeta;
									            }},
									            { "data": "fecha_pago_formateo" },
									            { "data": "codigo_amadeus" }, 
									            { "data": "descripcion" }, 
									            { "data": "estado" }, 
												{ "data": function(x){
													if(jQuery.isEmptyObject(x.nro_factura) == false){
														nro_factura = `<a target= _blank href="{{route('verFactura',['id'=>''])}}/${x.id_factura}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${x.nro_factura}</a><div style="display:none;">${x.id_factura}</div>`;

										             }else{
										        		nro_factura ="";
										            }
									            	return nro_factura;
									            }},

									            { "data": "pnr" },
									            { "data": "currency_code" },
									            { "data": "total_costo" }, 
									            { "data": "total_venta" },
												{ "data": function(x){
												
													var nombre = x.vendedor_nombre; 
									            	if(jQuery.isEmptyObject(x.vendedor_nombre) == false){
									            		vendedor =x.vendedor_nombre+" "+x.vendedor_apellido;
									            	}else{
									            		vendedor = "";
									            	}
									            	return vendedor;
									            } },

												{ "data": function(x){
									            	var origen =''; 
									            	if(jQuery.isEmptyObject(x.origen) == false){
									            		origen ="<b>"+x.origen+"</b>";
									            	}else{
									            		origen = "";
									            	}
									            	return origen;
									            } },
												{ "data": function(x){
									            	if(x.asignado) {
														return 'SI';
													}
													return 'NO';
									            } }
									        ], 
					    "createdRow": function ( row, data, iDataIndex, aData) {
									  //console.log(data.total_venta);
									  //console.log(data.id_currency_costo);
									  if(data.id_currency_costo == 111){	
														totalGs = parseFloat(clean_num($("#totalGuaranies").val()))+ parseFloat(data.total_venta);
														$("#totalGuaranies").val(totalGs);
													
													}if(data.id_currency_costo == 143){		
														totalUs = parseFloat(clean_num($("#totalDolares").val()))+ parseFloat(data.total_venta);
														$("#totalDolares").val(totalUs);
													
													}  
									   if (aData[16])
					                    {
					                       //$('td:eq(0)', row).css('width', '150px');
					                    }
						         }, "fnInitComplete": function(oSettings, json) {
											 $.unblockUI();
																        			 	
											}
					
					});	

				}, 300);

		}


	
	function asignarTickets(idTicket){
		
				var dataString = {
								  'ticket_asignar':$("#idTicketAAsignar").val(),
								  'ticke_asignado': idTicket
							   	   };

						$.ajax({
								type: "GET",
								url: "{{route('guardarAsignacionTicket')}}",
								dataType: 'json',
								data: dataString,
								error: function(jqXHR,textStatus,errorThrown){
								  	$('#mensaje_cambio').css('color','red');
								  	$('#mensaje_cambio').html('Ocurrio un error en la comuniación con el servidor');
								  	$('.cargandoImg').hide();
		                        },
								success: function(rsp){
													if(rsp.status == 'OK'){
														$.toast({
																heading: 'Exito',
																text: rsp.mensaje,
																position: 'top-right',
																showHideTransition: 'slide',
												    			icon: 'success'
															});  
														$("#requestAsignarTicket").modal("hide");
													}else{
														$.toast({
																heading: 'Error',
																text: rsp.mensaje,
																showHideTransition: 'fade',
																position: 'top-right',
																icon: 'error'
															});	
														$("#requestAsignarTicket").modal("hide");			
													}
										}
						});								
						$("#btnBuscar").trigger('click');

	}

		

	$('#btn_cambiar_iva').on("click",function(){
		var ok = 0;
		var idTicket = $('#id_ticket_iva').val();
		var porc_iva = $('#iva_select').val();

		if(idTicket =='')ok++;
		if(porc_iva == '')ok++;

		if(ok == 0){

		

			$.ajax({
						type: "GET",
						url: "{{route('cambiarIva')}}",
						dataType: 'json',
						data: { id: idTicket,
								iva:porc_iva},

						  error: function(jqXHR,textStatus,errorThrown){

						  	$('#mensaje_c').css('color','red');
						  	$('#mensaje_c').html('Ocurrio un error en la comuniación con el servidor');
						  	$('.cargandoImg').hide();
                        },
						success: function(rsp){

						$('.cargandoImg').hide();
						if(rsp.err == true){

							$('#mensaje_c').css('color','green');
							$('#mensaje_c').html('El cambio se realizo con éxito');
					
						} else {
							$('#mensaje_c').css('color','red');
						  	$('#mensaje_c').html('Ocurrio un error en la operación');
			
						}




									}//success
						});	//function

		} else {

			$('#mensaje_c').css('color','red');
			$('#mensaje_c').html('Existe un error con los datos');
			$('.cargandoImg').hide();

		}//function

	});

		
	$('#btn_cambiar_comision').on("click",function(){
		var ok = 0;
		var idTicket = $('#id_ticket_comision').val();
		var porc_comision = $('#comision_select').val();
		var over_tactivo = $('#over_tactico').val();
		if(idTicket =='')ok++;
		if(porc_comision == '')ok++;
		if(over_tactivo == '')ok++;
		if(ok == 0){
			console.log('Inhreso');
			$.ajax({
						type: "GET",
						url: "{{route('cambiarComision')}}",
						dataType: 'json',
						data: {
								id: idTicket,
								comision:porc_comision,
								codigo:over_tactivo
							},
						  error: function(jqXHR,textStatus,errorThrown){

						  	$('#mensaje_comision').css('color','red');
						  	$('#mensaje_comision').html('Ocurrio un error en la comuniación con el servidor');
						  	$('.cargandoImgen').hide();
                        },
						success: function(rsp){

						$('.cargandoImgen').hide();

						console.log(rsp);

						if(rsp.err == true){
							$('#over_tactico').prop('disabled',true);
							$('#mensaje_comision').css('color','green');
							$('#mensaje_comision').html('El cambio se realizo con éxito');
					
						} else {
							$('#mensaje_comision').css('color','red');
						  	$('#mensaje_comision').html('Ocurrio un error en la operación');
			
						}
					}//success
            });	//function

		} else {

			$('#mensaje_comision').css('color','red');
			$('#mensaje_comision').html('Existe un error con los datos');
			$('.cargandoImgen').hide();

		}//function

	});


	$('#btn_pago_tc').on("click",function(){
		
				$('#msg_error_adjunto').html('');
				if ($('#adjunto_modal_tc')[0].files.length == 0) {
					swal("Cancelado", "Debe agregar un adjunto para el pago", "error");
					return false;
				}


				return swal({
                    title: "¿Está seguro de realizar esta Operación?",
                    text: "Gestur va a generar una Orden de Pago de forma automática, este proceso no puede ser revertido",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Realizar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {

                            pagoTc();
                        } else {
                            swal("Cancelado", "", "error");
                        }
            	});

	});

	function pagoTc(){

			

					let formData = new FormData();
						formData.append('tarjeta', $('#tarjeta_utilizada').val());
						formData.append('comprobante', $('#comprante').val());
						formData.append('ticket', $('#id_ticket_pago').val());
						formData.append('adjunto_op', $('#adjunto_modal_tc')[0].files[0]);
		

			$.ajax({
				type: "POST",
				url: "{{route('getPagoTc')}}",
				data: formData,
				contentType: false,
				processData: false,
				enctype: 'multipart/form-data',
				cache: false,
					error: function(){
										swal("Cancelado", 'Ocurrio un error, vuelva a intentarlo.', "error");
									},
					success: function(rsp){
						console.log(rsp);
											if(rsp.status == 'OK'){
												$('#adjunto_modal_tc').val('');
												$('#requestPagoTC').modal("hide");
												swal("Exito!", rsp.mensaje+' y se creó la OP Nº: '+rsp.id_op, "success");
											}else{
												$('#requestPagoTC').modal("hide");			
												swal("Cancelado", rsp.mensaje, "error");
											}
										}	
			});
	}	

	$('.numerico').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});
		
	function clean_num(n,bd=false){
			if(n && bd == false){ 
			n = n.replace(/[,.]/g,function (m) {  
					 				 if(m === '.'){
					 				 	return '';
					 				 } 
					 				  if(m === ','){
					 				 	return '.';
					 				 } 
					 			});
			return Number(n);
		}
		if(bd){
			return Number(n);
		}
		return 0;
		}


	</script>
@endsection