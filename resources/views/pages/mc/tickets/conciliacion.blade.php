
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.style-icon{
			font-size: 2rem;
		}
		.btn-exportar-excel {
			background-color: #4CAF50;
			color: white;
			border: none;
			padding: 10px 20px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 16px;
			margin: 4px 2px;
			cursor: pointer;
			}

	</style>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">

@endsection
@section('content')

<section id="base-style">
	<br>
	<br>
	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Conciliación de BSP</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<!--<form id="frmProforma" method="post">
					<div class="row">
						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Moneda</label>
								<select class="form-control select2" name="divisa_id" id="divisa_id"
									style="width: 100%;" required>
									<option value="">Seleccione Moneda</option>
								</select>
							</div>
						</div>

						<div class="col-12 col-sm-5 col-md-5">
							<div class="form-group">
								<label>Periodo</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1"><i
												class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control pull-right fecha" name="periodo" id="periodo"
										required>
								</div>
							</div>
						</div>

						<div class="col-11 mb-1">
							<button type="button" onclick="buscarCotizacion()" id="btnGuardar" class="btn btn-info btn-lg pull-right mb-1">Buscar</button>
						</div>
					</div>
				</form>-->


				<div class="table-responsive table-bordered">
					<table class="table">
						<tr style="text-align: center;">
							<th style="width: 26%;">BSP</th>
							<th style="background-color: aliceblue;">GESTUR</th>
						</tr>
					</table>
					<table id="listado" class="table">
						<thead>
							<tr style="text-align: center;">
							<th>Nro BSP</th>
							<th>Fecha BSP</th>
							<th>Moneda BSP</th>
							<th>Monto BSP</th>
							<th>Nro Ticket</th>
							<th>Fecha Emision</th>
							<th>Proveedor</th>
							<th>Usuario</th>
							<th>Estado</th>
							<th>Proforma</th>
							<th>Factura</th>
							<th>Moneda</th>
							<th>Total Neto</th>
							<th>Total Bruto</th>
							</tr>
						</thead>
						<tbody id="lista_cotizacion" style="text-align: center">
							<?php
								$totalBspGs = 0;
								$totalBspUsd = 0;
								$totalGUsd = 0;
								$totalGGs = 0;
								$cantidadBsp = 0; 
								$cantidadG = 0; 
							?>
							@foreach($resultado as $key => $datos)
							<?php
									$coma = ($datos['moneda'] == 'PYG') ? 0 : 2;
									$cantidadBsp = $cantidadBsp + 1;
									if($datos['moneda'] == 'PYG'){
										$totalBspGs = $totalBspGs + floatval($datos['monto_neto']);
									}else{
										$totalBspUsd = $totalBspUsd + floatval($datos['monto_neto']);
									}
								?>
							@if(isset($datos['detalles']))
								<?php
									
									$monto_neto = number_format($datos['detalles'][0]->total_neto,$coma,",",".");
									$monto_bruto = number_format($datos['detalles'][0]->total_venta,$coma,",",".");
									if($datos['moneda'] != $datos['detalles'][0]->currency_code){
										if($datos['moneda'] == 'PYG'){
											$moneda = 111;
										}else{
											$moneda = 143;
										}
										$neto_monto = DB::select('SELECT public.get_monto_cotizado('.floatval($monto_neto).','.$moneda.','.$datos['detalles'][0]->id_currency_costo.')');
										$bruto_monto = DB::select('SELECT public.get_monto_cotizado('.floatval($monto_bruto).','.$moneda.','.$datos['detalles'][0]->id_currency_costo.')');
										$monto_neto = number_format(floatval(str_replace(',','.', str_replace('.','',$neto_monto))),$coma,",",".");
										$monto_bruto = number_format(floatval(str_replace(',','.', str_replace('.','',$bruto_monto))),$coma,",",".");
									}

								
									$cantidadG = $cantidadG + 1;
									if($datos['moneda'] == 'PYG'){
										$totalGGs = $totalGGs + floatval(str_replace(',','.', str_replace('.','',$monto_neto)));
									}else{
										$totalGUsd = $totalGUsd + floatval(str_replace(',','.', str_replace('.','',$monto_neto)));
									}

								?>
								<tr>
									<td>{{$datos['numero_tickt']}}</td>
									<td>{{$datos['fecha_emision']}}</td>
									<td>{{$datos['moneda']}}</td>
									<td>{{number_format(floatval($datos['monto_neto']),$coma,",",".")}}</td>
									<td>{{$datos['detalles'][0]->numero_amadeus}}</td>
									<td>{{date('d/m/Y', strtotime($datos['detalles'][0]->fecha_emision))}}</td>
									<td>{{$datos['detalles'][0]->nombre_proveedor}}</td>
									<td>{{$datos['detalles'][0]->usuario}}</td>
									<td>{{$datos['detalles'][0]->estado}}</td>
									<td>{{$datos['detalles'][0]->id_proforma}}</td>
									<td>{{$datos['detalles'][0]->nro_factura}}</td>
									<td>{{$datos['detalles'][0]->currency_code}}</td>
									<td>{{$monto_neto}}</td>
									<td>{{$monto_bruto}}</td>
								</tr>
							@else 
								<tr>
									<td>{{$datos['numero_tickt']}}</td>
									<td>{{$datos['fecha_emision']}}</td>
									<td>{{$datos['moneda']}}</td>  
									<td>{{number_format(floatval($datos['monto_neto']),$coma,",",".")}}</td>
									<td></td>
									<td></td>
									<td>NO SE ENCONTRÓ EL TICKET EN LA BASE DE DATOS </td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							@endif
							@endforeach
						</tbody>
					</table>
				</div>
				<hr>
				<br>
				<h4 class="card-title">RESUMEN</h4>
				<br>
				<table id="listado" class="table">
					<tr style="text-align: center;background-color: #dddddd;">
						<th>Descripcion</th>
						<th>PYG</th>
						<th>USD</th>  
					</tr>
					<tr style="text-align: center;">
						<th>Total Registros BSP ({{$cantidadBsp}})</th>
						<th>{{number_format($totalBspGs,0,",",".")}}</th>
						<th>{{number_format($totalBspUsd,2,",",".")}}</th>
					</tr>
					<tr style="text-align: center;">
						<th>Total Registros Gestur ({{$cantidadG}})</th>
						<th>{{number_format($totalGGs,2,",",".")}}</th>
						<th>{{number_format($totalGUsd,0,",",".")}}</th>
					</tr>
					<?php
						$diferenciaGs = $totalBspGs  - $totalGGs;
						$diferenciaUsd = $totalBspUsd  - $totalGUsd;
					?>
					<tr style="text-align: center;color:red;">
						<th>Diferencia</th>
						<th>{{number_format($diferenciaGs,0,",",".")}}</th>
						<th>{{number_format($diferenciaUsd,2,",",".")}}</th>
					</tr>
				</table>
				<hr>
    		</div>
		</div>
	</section>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>

	<script>
	$(document).ready(function() {
    $("#listado").DataTable({
        "aaSorting": [[0, "desc"]],
        dom: 'Bfrtip',
		buttons: [
                                {
                                        extend: 'excelHtml5',
                                        title: 'Reporte de BSP',
                                        exportOptions: {
                                                stripHtml: false,
                                                columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13],
                                                format: {
                                                        body: function(data, row, column, node) {
															if(column ==1){
																return data;
															}else if(column == 10){
																return data;
															}else if(column == 4){
																return data;
															}else{
                                                            if(!isNaN(parseFloat(data))){
                                                                    if(data.indexOf('/') != -1){
                                                                        return data;
                                                                    }else{
                                                                        return clean_num(data);
                                                                    }  
                                                                }else{
                                                                    return data;
                                                                } 
                                                        	}
														}
                                                    }        
                                            }
                                        
                                    },
			],
    });
});

function clean_num(n, bd = false) {

if (n && bd == false) {
	n = n.replace(/[,.]/g, function (m) {
		if (m === '.') {
			return '';
		}
		if (m === ',') {
			return '.';
		}
	});
	return Number(n);
}
if (bd) {
	return Number(n);
}
return 0;

} //
	</script>
@endsection