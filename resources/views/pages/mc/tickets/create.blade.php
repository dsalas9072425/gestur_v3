@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')

	@include('flash::message')  

    <section class="base-style"> 

		<section class="card">
			<div class="card-header">
				<h4 class="card-title">Nuevo Ticket</h4>
				<div class="heading-elements">
					<ul class="list-inline mb-0">
						<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="card-content collapse show" aria-expanded="true">
				<div class="card-body pt-0">
			   <form id="frmGrupo" method="post"  action="{{route('store')}}" style="margin-top: 2%;">
					@include('flash::message')     
									@if (count($errors) > 0)
										   <div class = "alert alert-danger">
										      <ul>
										         @foreach ($errors->all() as $error)
										            <li>{{ $error }}</li>
										         @endforeach
										      </ul>
										   </div>
										@endif
										<!-- /.box -->
										<div class="row"  style="margin-bottom: 1px;margin-top: 20px;">
											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
													<label class="control-label">Número de ticket</label>
													<input type="text" class = "form-control" name="numero_de_ticket" id="numero_de_ticket" placeholder="Número de ticket" value="{{ old('numero_de_ticket')}}"  pattern="[A-Za-z0-9]+" maxlength="10" required/>
											</div>

											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
													<label>Aerolínea</label>					            	
													<select class="form-control input-sm select2" name="aerolinea" id="aerolinea" style="padding-left: 0px;width: 100%;">
														<option value=" ">Seleccione aerolínea</option>
														@foreach($aerolineas as $key=>$aerolinea)
															<option {{ old('aerolinea') == $aerolinea->out_id_persona ? 'selected' : '' }} value="{{$aerolinea->out_id_persona}}">{{$aerolinea->out_nombre}}</option>
														@endforeach	
													</select>
											</div>

											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
													<label>Origen Ticket</label>					            
													<select class="form-control input-sm select2" name="tipo_ticket" id="tipo_ticket" style="padding-left: 0px;width: 100%;" required>
														<option value=" ">Seleccione tipo de ticket</option>
														@foreach($tipos_tickets as $key=>$tipo_ticket)
														<option {{ old('tipo_ticket') == $tipo_ticket->id ? 'selected' : '' }} value="{{$tipo_ticket->id}}">{{$tipo_ticket->descripcion}}</option>
														@endforeach	
													</select>
											</div> 

											{{-- <div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 25px;">
													<label>Estado</label>					            	
													<select class="form-control input-sm select2" name="estado" id="estado" style=" padding-left: 0px;width: 100%;">
														<option value=" ">Seleccione estado</option>
														@foreach($estados as $key=>$estado)
														<option value="{{$estado->id}}">{{$estado->denominacion}}</option>
														@endforeach	
													</select>
											</div> --}}

											<div class="col-12 col-sm-3 col-md-3">
												<label>Tasa YQ</label> <br>
												 <small>(Solo tiene efecto cuando tarifa es comisionable)</small><br>
												<input type="radio" checked="checked" name="tarifa_comision_yq" value="1"> Comisionable SI<br>
												<input type="radio" name="tarifa_comision_yq" value="0"> Comisionable NO<br>
											</div> 

										</div> 

										<div class="row"  style="margin-bottom: 1px;margin-top: 20px;">
											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 25px;">
													<label>Fecha de emisión</label>						 
													<input type="text" class="form-control pull-right fecha" name="fecha_emision" id="fecha_emision" value="{{ old('fecha_emision')}}" required>
											</div>

											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
													<label class="control-label">PNR</label>
													<input type="text" class = "form-control" name="PNR" id="PNR" placeholder="PNR" value="{{ old('PNR') }}"  style="text-transform: uppercase" required/>
											</div> 

											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
													<label class="control-label">Pasajero</label>
													<input type="text" class = "form-control" name="pasajero" id="pasajero" placeholder="Pasajero" value="{{ old('pasajero') }}" maxlength="50" required/>
											</div>

											<div class="col-12 col-sm-3 col-md-3">
												<label>Tipo Tarifa</label><br>
													<input type="radio" name="tarifa" value="0"> Neto<br>
													<input type="radio" checked="checked" name="tarifa" value="1"> Comisionable<br>
											</div> 
	
										</div>

										<div class="row"  style="margin-bottom: 1px;margin-top: 20px;">
											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
													<label class="control-label">Facial/Gravada</label>
													<input type="text" class = "form-control numeric" name="facial" id="facial" placeholder="Importe Facial" value="{{ old('facial') }}"  onkeypress="return justNumbers(event);"/>
											</div>

											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
													<label class="control-label">Tasa YQ</label>
													<input type="text" data-toggle="tooltip" data-placement="left" title="Este valor será restado al Total de Tasas en la generación del ticket" class = "form-control numeric" name="tasaYq" id="tasaYq" placeholder="Tasas" value="{{ old('tasaYq') }}" onkeypress="return justNumbers(event);"/>
											</div>

											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
													<label class="control-label">Tasas/Exenta</label>
													<input type="text" class = "form-control numeric" name="tasa" id="tasa" placeholder="Tasas" value="{{ old('tasa') }}" onkeypress="return justNumbers(event);"/>
											</div>
												
											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 15px;padding-right: 15px;">
													<label>Tipo Transacción</label>					            	
													<select class="form-control input-sm select2" name="tipo_transaccion" id="tipo_transaccion" style="padding-left: 0px;width: 100%;">
														<option value=" ">Seleccione tipo de transacción</option>
															@foreach($tipo_transaccion_tickets as $key=>$tipo_transaccion_ticket)
																<option {{ old('tipo_transaccion') == $tipo_transaccion_ticket->id ? 'selected' : '' }} value="{{$tipo_transaccion_ticket->id}}">{{$tipo_transaccion_ticket->codigo_amadeus}}</option>
															@endforeach	
													</select>
											</div> 
										</div> 
										<div class="row"  style="margin-bottom: 1px;margin-top: 20px;">
											<div class="col-12 col-sm-3 col-md-3" style="padding-left: 25px;padding-right: 15px;">
													<label>Moneda</label>					            	
													<select class="form-control input-sm select2" name="moneda" id="moneda" style="padding-left: 0px;width: 100%;">
														<option value=" ">Seleccione moneda</option>
															@foreach($currencys as $key=>$currency)
																<option {{ old('moneda') == $currency->currency_id ? 'selected' : '' }} value="{{$currency->currency_id}}">{{$currency->currency_code}}</option>
															@endforeach	
													</select>
											</div> 
										</div>
										
										<br>
										<div class="col-12 col-sm-10 col-md-10" style="padding-left: 15px;">
										</div>
								
										<div class="col-12 col-sm-2 col-md-2" style="padding-left: 15px;margin-bottom: 10px;">
											<button type="submit" form="frmGrupo" id="btnGuardar" class="btn btn-success btn-lg" >Guardar</button>
										</div>
						</form> 
				</div>
			</div>
		</section>
	</section>
    <!-- /.content -->

@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script>
	$(document).ready(function() {
		$('.select2').select2();
$('input[name="periodo"]').daterangepicker({
							        //timePicker: true,
							        timePicker24Hour: true,
							        timePickerIncrement: 30,
							        locale: {
							            format: 'DD/MM/YYYY '//H:mm'
							        }
							    });

		$("#listado").dataTable({
			"aaSorting":[[0,"desc"]]
		});

		$( "#fecha_seña" ).datepicker({ 
			altFormat: 'dd/mm/yy',
			dateFormat: 'yy-mm-dd'
		});

		$( "#fecha_salida" ).datepicker({ 
			altFormat: 'dd/mm/yy',
			dateFormat: 'yy-mm-dd'
		});

		$( "#fecha_emision" ).datepicker({ 
			altFormat: 'dd/mm/yy',
			dateFormat: 'yy-mm-dd'
		});

		$('.numeric').inputmask("numeric", {
        	radixPoint: ",",
        	groupSeparator: ".",
        	digits: 2,
        	autoGroup: true,
        	rightAlign: false,
        	oncleared: function () { self.Value(''); }
		});

		$("#itemList").select2({
			ajax: {
				url: "{{route('destinoGrupo')}}",
				dataType: 'json',
				placeholder: "Seleccione un destino",
				delay: 0,
				data: function (params) {
					return {
					                    q: params.term, // search term
					                    page: params.page
					                };
					            },
					            processResults: function (data, params){
					            	var results = $.map(data, function (value, key) {
					            		return {
					            			children: $.map(value, function (v) {
					            				return {
					            					id: key,
					            					text: v
					            				};
					            			})
					            		};
					            	});
					            	return {
					            		results: results,
					            	};
					            },
					            cache: true
					        },
					        escapeMarkup: function (markup) {
					        	return markup;
					        }, // let our custom formatter work
					        minimumInputLength: 3,
					    });
	});

	function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

	var options = { 
		beforeSubmit:  showRequest,
		success: showResponse,
		dataType: 'json' 
	}; 

	$('body').delegate('#image','change', function(){
		$('#upload').ajaxForm(options).submit();  		
	});

	function showRequest(formData, jqForm, options) { 
		$("#validation-errors").hide().empty();
		$("#output").css('display','none');
		return true; 
	} 
	function showResponse(response, statusText, xhr, $form){ 

		if(response.success == false)
		{
			var arr = response.errors;
			console
			$.each(arr, function(index, value)
			{
				if (value.length != 0)
				{
					$("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
				}
			});
			$("#validation-errors").show();
		} else {
			$("#imagen").val(response.archivo);	
			$("#output").html("<img class='img-responsive' src=''/>");
			$("#output").css('display','block');
			$("#btn-delete").html("<button type='button' id='btn-imagen' data-id='"+response.archivo+"'><i class='glyphicon glyphicon-remove'></i></button>");
			eliminarImagen();
		}
	}

		$("#botonExcel").on("click", function(e){
               e.preventDefault();
              $('#frmBusqueda').attr('action', "{{route('generarExcelVenta')}}").submit();
           });

		$("input[name=tarifa]").click(function () {
			console.log($(this).val());
            if($(this).val() == '1'){
                   $(".habilitarComision").prop("disabled", false);
            }else{
                  	$(".habilitarComision").prop("disabled", true);
               }

        });
		$('#aerolinea').change(function(){
				$.ajax({
						type: "GET",
						url: "{{route('datosComisionAerea')}}",//fileDelDTPlus
						dataType: 'json',
						data: {  
								dataFile:$('#aerolinea').val()
							},
						success: function(rsp){
						
							if(rsp.comision == null ){
								 $.toast({
								 	heading: 'Error',
								 	text:  'Esta aerolína NO tiene comisión configurada',
								 	position: 'top-right',
								 	showHideTransition: 'fade',
								 	icon: 'error'
								 });
							} else if(rsp.comision == 0.00 || rsp.comision == 0){
								$.toast({
								 	heading: 'Atención',
								 	text:  'Esta aerolína tiene configurado comision 0',
								 	position: 'top-right',
								 	showHideTransition: 'fade',
								 	icon: 'warning'
								 });
							}
						}, 
						error: function(){

						}
					})
			});




</script>
@endsection