
@extends('masters')
@section('title', 'Listado de Nota de Credito')
@section('styles')
	@parent
	<style type="text/css">
	
	.bgRed {
			font-size: 15px;
			display: inline-block;
			min-width: 10px;
			padding: 3px 7px;
			font-size: 15px;
			font-weight: 800;
			line-height: 1;
			color: #000;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			border-radius: 10px;
			border: 1px solid black;
			}	

</style>

@endsection
@section('content')

<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Listado Notas de Crédito</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
            	<form id="consultaNota" autocomplete="off">
    		    	<div class = "row">
							<div class="col-12 col-sm-6 col-md-3">
							 	<div class="form-group">
									<label>Cliente</label>
									<select class="form-control select2" name="cliente_id"  id="cliente_id" tabindex="1" style="width: 100%;">
										<option value="">Todos</option>
										@foreach($clientes as $cliente)
											@php
												$ruc = $cliente->documento_identidad;
												if($cliente->dv){
													$ruc = $ruc."-".$cliente->dv;
												}
											@endphp
											<option value="{{$cliente->id}}">{{$ruc}} - {{$cliente->full_data}} </option>
										@endforeach
									</select>
								</div>
							</div>	     
							<div class="col-12 col-sm-6 col-md-3">
								<div class="form-group">
									<label>Moneda</label>
									<select class="form-control select2" name="idMoneda"  id="idMoneda" tabindex="2" style="width: 100%;">
									<option value="">Todos</option>
										@foreach($monedas as $moneda)
											<option value="{{$moneda->currency_id}}">{{$moneda->currency_code}}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-12 col-sm-3 col-12 col-sm-6 col-md-3" style="padding-left: 15px;">
								<div class="form-group">
									<label>Desde - Hasta Nota Crédito</label>
									<div class="input-group">
										<div class="input-group-prepend" style="">
											<span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
										</div>
										<input type="text" class="form-control pull-right fecha" name="periodo_fecha_facturacion" id="periodo_fecha_facturacion" tabindex="3" value=""  maxlength="30">
									</div>
								</div>
							</div>

							<div class="col-12 col-sm-6 col-md-3">
								<div class="form-group">
									<label>Estado</label>
									<select class="form-control select2_estado" name="tipoEstado[]"  id="tipoEstado" tabindex="4" style="width: 100%;">
										<option></option>
										<option value="1">Pendiente</option>
										<option value="2">Aplicado</option>
										<option value="3">Anulado</option>
									</select>
								</div>
							</div>
					</div>
					<div class = "row">		
						<div class="col-12 col-sm-6 col-md-3">
							<div class="form-group">
								<label>Vendedor</label>
								<select class="form-control select2" name="vendedor_id"  id="vendedor_id" tabindex="5" style="width: 100%;">
									<option value="">Todos</option>
									
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-3"> 
							<div class="form-group">
								<label>Nro. Nota Crédito</label>
								<input type="text" class="form-control" id="numNotaCredito" name="numNotaCredito" value="" tabindex="6"  maxlength="30">
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-3"> 
							<div class="form-group">
								<label>Nro. Proforma</label>
								<input type="number" class="form-control" tabindex="7" id="numProforma" name="numProforma" value="" >
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-3"> 
							<div class="form-group">
								<label>Nro. Factura</label>
								<input type="text" class="form-control" tabindex="8" id="numFactura" name="numFactura" value="" maxlength="30">
							</div>
						</div>
			
						<div class="col-12">
							<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg" style=" margin: 0 0 10px 10px;"><b>Excel</b></button>

							<button type="button" onclick="limpiar()" id="btnLimpiar" class="btn btn-light btn-lg pull-right text-white" style="margin: 0 0 10px 10px;"><b>Limpiar</b></button>	
						
							<a  onclick ="consultaFactura()" tabindex="9" class="pull-right btn btn-info btn-lg" style="color:#fff;" role="button"><b>Buscar</b></a>
						</div>
					</div>						    	
		        <!-- /.col -->
	      	
	    </form>  	

           	<div class="table-responsive table-bordered">
	              <table id="listado" class="table" style="width: 100%;">
	                <thead>
					  <tr>
						<th>Nota de Crédito</th>
						<th>Fecha</th>
						<th>Factura</th>
						<th>Proforma</th>
						<th>Monto</th>
						<th>Saldo</th>
						<th>Moneda</th>
						<th>Cliente</th>
						<th>Vendedor</th>
						<th>Estado</th>
						<th>Tipo</th>
		              </tr>
	                </thead>
	                <tbody id="lista_cotizacion" style="text-align: center">
			        </tbody>
	              </table>
	            </div>  
            </div>	
        </div>    
    </div>     
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
<script type="text/javascript">

$(document).ready(function(){

$('.select2').select2();

$('.select2_estado').select2({
									multiple:true,
									placeholder: 'Todos'
								});

//$("#tipoEstado").select2().val(1).trigger("change");
$('.select2_estado').val([1,2,3]).trigger('change.select2');

	var Fecha = new Date();
		var fechaInicial =  Fecha.getMonth() +1 + "/" + Fecha.getFullYear();


			$('input[name="periodo_fecha_facturacion"]').daterangepicker({
												timePicker24Hour: true,
												timePickerIncrement: 30,
												locale: {
													    format: 'DD/MM/YYYY',
													     cancelLabel: 'Limpiar',
													     applyLabel: 'Aplicar',					
										                fromLabel: 'Desde',
										                toLabel: 'Hasta',
										                customRangeLabel: 'Seleccionar rango',
										                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
										                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
										                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
										                    'Diciembre']
													    },
												startDate: "01/"+fechaInicial,
        										endDate: new Date(),
								    			});

});//ready
	
	


		
	

	$('input[name="periodo_fecha_facturacion"]').on('cancel.daterangepicker', function(ev, picker) {
			      $(this).val('');
			  });


	$("#botonExcel").on("click", function(e){ 
			
                e.preventDefault();
                 $('#consultaNota').attr('method','post');
               $('#consultaNota').attr('action', "{{route('generarExcelListadoNotaCredito')}}").submit();
            });



	function validar(d){
		if(d != null){
			return d;
		}
		return '';
	}





		function consultaFactura(){

			$("#listado").dataTable({
				"destroy": true,
				 "aaSorting":[[1,"desc"]]
				});
			var dataString = $('#consultaNota').serialize();
			console.log(dataString);
				$.ajax({
						type: "GET",
						url: "{{route('consultaNota')}}",
						dataType: 'json',
						data: dataString,

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                                });

                            },

						success: function(rsp){
							console.log(rsp);
							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}
					

								$.each(rsp, function (key, item){
									botones = `<a href="{{route('verNota',['id'=>''])}}/${item.id}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${item.nro_nota_credito} (${item.contador})</a><div style="display:none;">${item.id}</div>`;
									if(item.id_lv == null){
										saldo = item.saldo_nota_credito;
									}else{
										saldo = item.nota_credito_saldo;
									}
									opcion_name= '';
									if(item.opcion == 1){
										var opcion_name=  'Devolución';
									}
									if(item.opcion == 2){
										var opcion_name=  'Documento'; 
									}
									if(item.opcion == 3){
										var opcion_name=   'Refacturación';
									}

									var dataTableRow = [
														botones,
														item.fecha_nota_credito,
														item.nro_factura,
														item.id_proforma,	
														item.total_neto_nc_format,
														'<b>'+saldo+'</b>',
														item.currency_code,
														validar(item.cliente_a)+' '+validar(item.cliente_n),
														validar(item.vendedor_a)+' '+validar(item.vendedor_n),
														item.denominacion,
														'<b>'+opcion_name+'</b>'
													]
									var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								    var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
						
								})

									}

							});


		}//fucntion



			function limpiar(){
			$('#cliente_id').val('').trigger('change.select2');
			$('#idMoneda').val('').trigger('change.select2');
			$('#periodo_fecha_facturacion').val('');
			$('#tipoEstado').val('').trigger('change.select2');
			$('#tipo_estado').val('').trigger('change.select2');
			$('#vendedor_id').val('').trigger('change.select2');
			$('#numNotaCredito').val('');
			$('#numProforma').val('');
			$('#numFactura').val('');

		}




</script>

@endsection
