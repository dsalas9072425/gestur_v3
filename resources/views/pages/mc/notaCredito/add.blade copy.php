@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	 <style type="text/css">
	 	.error{
	 		color:red;
	 	}

		.correcto_col {
        height: 74px;
    	}
		.input-sm{
		font-size: 15px !important;
		}	

	 </style>
@endsection
@section('content')
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Generar Nota de Credito</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
				
						@if(!empty($facturas)) 
							@foreach($facturas as $key=>$factura)
							  
					            	<div class="row"> 
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Nro Factura</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$factura->nro_factura}}" readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Proforma N°</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$factura->id_proforma}}" readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Cliente</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$factura->cliente->nombre}}" readonly="readonly"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Vendedor</label>
												@if(isset($factura->vendedorEmpresa->nombre))
													<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$factura->vendedorEmpresa->nombre}}" readonly="readonly"/>
												@else
													<input type="text" class = "form-control input-sm" name="proforma_id" value="" readonly="readonly"/>
												@endif
											</div>
									    </div>
									 </div>
									<div class="row"> 
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Pasajero Principal</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$pasjeroNombre}}" readonly="readonly"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Destino</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$destino}}" readonly="readonly"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Check In - Check Out</label>
												<?php 
													$fecha_periodo1 = date('d/m/Y', strtotime($factura->check_in)); 
													$fecha_periodo2 = date('d/m/Y', strtotime($factura->check_out)); 
													$periodo = $fecha_periodo1." - ".$fecha_periodo2;

												?>

												<input type="text"  class="form-control input-sm"  value="{{$periodo}}"  disabled/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Moneda de Facturación</label>
												@if(isset($factura->currency->currency_code))
													<input type="text" class = "form-control input-sm" value="{{$factura->currency->currency_code}}" readonly="readonly"/>
												@else
													<input type="text" class = "form-control input-sm"  value="" readonly/>
												@endif	
											</div>
									    </div>
									</div>
									<div class="row">    
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Monto Seña</label>
												<input type="text" class = "form-control numerico" name="senha" id="senha" value="{{$factura->senha}}" disabled/>
											</div>
									    </div>
							            <div class="col-12 col-sm-4 col-md-3">
									        <div class="form-group">
									            <label>Expediente</label>
												<input type="text" class = "form-control" name="senha" id="senha" value="{{$factura->proforma_padre}}" disabled/>
									        </div>
							            </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Estado</label>
												 @if($factura->id_estado_factura == "30")
													<input type="text" disabled class = " form-control input-sm"  value="{{$factura->estado->denominacion}}"  style="font-weight: bold;font-size: 18px; color:red;"/>
												@else
													<input type="text" disabled class = " form-control input-sm"  value="{{$factura->estado->denominacion}}"  style="font-weight: bold;font-size: 18px;"/>
												@endif	
											</div>
									    </div>
									    <div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Vencimiento</label>
									            <div class="input-group">
												    <div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">
												        <i class="fa fa-calendar"></i>
												    </div>
												    @if(date('d/m/Y', strtotime($factura->vencimiento)) !== '31/12/1969')
													    <input type="text" value="{{date('d/m/Y', strtotime($factura->vencimiento))}}" class="form-control pull-right fecha"  disabled >
													@else
													    <input type="text" value="" class="form-control pull-right fecha"  disabled />
													@endif    
												</div>
											</div>
									    </div>
					            	 </div> 
					            	<div class="row">  
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Cotización</label>
												<input type="text" disabled class = "form-control input-sm numerico"  value="{{$factura->cotizacion_factura}}" />
											</div>
									    </div>										
									    <div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Usuario de Facturación</label>
												@foreach($personas as $person)
													@if($person->id == $factura->usuario_facturacion_id)
														<input type="text" class = "form-control"  value="{{$person->apellido}}, {{$person->nombre}}" disabled />

													@endif
												@endforeach
											</div>
									    </div>
							            <div class="col-12 col-sm-4 col-md-3">
									        <div class="form-group">
									            <label>Fecha de Facturación</label>
												    @if(date('d/m/Y', strtotime($factura->fecha_hora_facturacion)) !== '31/12/1969')
													    <input type="text" value="{{date('d/m/Y H:m:i', strtotime($factura->fecha_hora_facturacion))}}" class="form-control pull-right fecha" disabled />
													@endif    
									        </div>
							            </div>
							           
									</div> 
	
			<form  method="get" class="validator-form" action="{{route('doAddNotaCredito')}}" id="frmNotaCredito" name="agencias">
				<input type="hidden" id="opcion" name="opcion" value=""/>
				<input type="hidden" name="factura_id" value="{{$factura->id}}"/>
				<input type="hidden" name="total_factura" value="{{$totalFactura[0]->total}}"/>
					<div class="table-responsive">
						<table id="lista_productos" class="table">
								<thead>
									<tr>
										<th>Cant.</th>
										<th>Producto</th>
										<th>Código</th>
										<th>Proveedor</th>
										<th>Prestador</th>
										<th>Detalle</th>
										<th>Moneda</th>
										<th>Costo</th>
										<th>Tasas</th>
										<th>Com.<br>Agencia</th>
										<th>Venta</th>
										<th>%</th>
										<th>Monto<br>Nota</th>
										<th>Crédito<br>Proveedor</th>
									</tr>
								</thead>
								<tbody >
									@foreach($factura->facturaDetalle as $key=>$detalle)
												@if($detalle->activo == true)
													<tr>
														<td>{{$detalle->cantidad}}</td>

														<td>
															@foreach($producto as $product)
																@if($product->id == $detalle->id_producto)
																	<b>{{$product->denominacion}}</b>
																@endif
															@endforeach
														</td>

														<td>{{$detalle->cod_confirmacion}}</td>

														<td>
															@foreach($personas as $person)
															  @if($person->id == $detalle->id_proveedor)
																	<b>{{$person->nombre}}</b>
																@endif
															@endforeach
														</td>

														<td>
															@foreach($personas as $person)
															  @if($person->id == $detalle->id_prestador)
																<b>{{$person->nombre}}</b>
															  @endif
															@endforeach
														</td>

														<td>{{$detalle->descripcion}}</td>

														<td>
															@foreach($currency as $curren)
																@if($curren->currency_id == $detalle->currency_costo_id)
																	<b>{{$curren->currency_code}}</b>
																@endif
															@endforeach
														</td>
														
														<td><b>{{number_format($detalle->costo_proveedor,2,",",".")}}</b></td>

														<td>0</td>
														
														<td>{{$detalle->porcentaje_comision_agencia}}</td>
														<td>
															<?php
																$totalDetalle = DB::select('select get_monto_factura_detalle('.$factura->id.','.$detalle->id.')');
																$totalDetalleF = number_format($detalle->saldo,2,",",".");
																			
															?>
															<b>{{$totalDetalle[0]->get_monto_factura_detalle}}</b>
														</td>
														
														<td><b>{{number_format($detalle->markup,2,",",".")}}</b></td>
														
														<td>
															<?php 
																if($factura->id_tipo_facturacion == 1)
																{
																	$monto = $detalle->exento + $detalle->gravadas_10 ;
																}
																else
																{
																	$monto = $detalle->exento + $detalle->comision_agencia + $detalle->gravadas_10 ;
																}
															?>
															<input type="hidden"  name="data[][factura_detalle_id]" value="{{$detalle->id}}"/>
															
															
															<?php
																$totalDetalleF = number_format($detalle->saldo,2,",",".");
															?>
															<input type="hidden"  class="inputMontoNota_{{$detalle->id}} saldo" name="data[][saldoFd]" value="{{$totalDetalleF}}" />

															<div class="input-group" style="width:230px;">
																<div class="input-group-prepend">
																	<span class="input-group-text">{{$factura->currency['currency_code']}}</span>
																</div>
																<input type="text" class="form-control input-sm numerico sumar inputMontoNota_{{$detalle->id}}" oninput="controlInputMontoNc({{$detalle->id_factura}}, {{$detalle->id}},'inputMontoNota_{{$detalle->id}}',)" name="data[][monto]" data-toggle="tooltip" data-placement="left" title="Saldo Item: {{$totalDetalleF}} {{$factura->currency['currency_code']}}" maxlength="12" value="0" />
															</div>
														
														</td>
														
														<td>
															<div class="input-group" style="width:230px;">
															@if(isset($detalle->libroCompra[0]->currency['currency_code']))
																<div class="input-group-prepend">
																	<span class="input-group-text">{{$detalle->libroCompra[0]->currency['currency_code']}}</span>
																</div>
															@else
																<div class="input-group-prepend">
																	<span class="input-group-text">{{$detalle->currencyVenta->currency_code}}</span>
																</div>
															@endif
      

															@if(isset($detalle->libroCompra[0]->currency['currency_code']) && isset($detalle->libroCompra[0]->saldo))

																<input type="text" class="form-control input-sm numerico inputCreditoProveedor_{{$detalle->id}}" oninput="controlInput(1,'inputCreditoProveedor_{{$detalle->id}}',this.value)" name="data[][credito]" data-toggle="tooltip" data-placement="left" title="Saldo Proveedor : {{number_format($detalle->libroCompra[0]->saldo,2,',','.')}} {{$detalle->libroCompra[0]->currency['currency_code']}}" maxlength="12" value="0" />
															@else
																<input type="text" class="form-control input-sm numerico inputCreditoProveedor_{{$detalle->id}}" oninput="controlInput(1,'inputCreditoProveedor_{{$detalle->id}}',this.value)" name="data[][credito]" data-toggle="tooltip" data-placement="left" title="Saldo Proveedor : {{number_format($detalle->saldo,2,',','.')}} {{$detalle->currencyVenta->currency_code}}" maxlength="12" value="0" />

															@endif
															</div>
															
															<input type="hidden"  name="data[][id_detalle]" value="{{$detalle->id}}" />
															@if(isset($detalle->libroCompra[0]->saldo))

																<input type="hidden" class="inputCreditoProveedor_{{$detalle->id}} saldo" name="data[][saldoLc]"  value="{{$detalle->libroCompra[0]->saldo}}" />
															@else
																<input type="hidden" class="inputCreditoProveedor_{{$detalle->id}} saldo" name="data[][saldoLc]"  value="{{$detalle->saldo}}" />
															@endif
														</td>
													</tr>	
												@endif	
										{{--	@else
												<!--<tr>
													<td colspan="14" style="text-align:center">
														<b>Falta relación de libro compra, póngase en contacto con soporte técnico.</b>
													</td>
												</tr>-->

											@endif	--}}
									@endforeach 
									<tr>
										<td></td>
										<td><b style="font-weight: bold;font-size: 15px;">TOTAL</b></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td>
											<input type="text" class = " form-control input-sm numerico" name="total" id="totalizar" maxlength="14" style="width: 100%; min-width: 130px;" value="0" readonly/></td>
										<td>
										</td>
									</tr> 
								</tbody>	
							</table>
						</div> 
						<br>
						<div class="row">
							<div class="col-12 col-md-8"></div>
							<div class="col-12 col-md-2">
								<button type="button" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="background: #e2076a !important;width: 100%;height: 54px; min-width: 130px;" value="Guardar" id="btnDevolucion"><b>Generar NC Devolución</b></button>
							</div>
							<div class="col-12 col-md-2">	
								<button type="button" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="background: #16d39a !important;width: 100%;height: 54px; min-width: 130px;" value="Guardar" id="btnNCredito"><b>Generar NC Documento</b></button>
							</div>				
						</div> 
						<br>				
					</form>	

									<div class="row mt-1">
						         

						         
						            		<div class="col-12 col-sm-4 col-md-2" >
												<div class="form-group">
													<label style="color: red;">TOTAL_FACTURA</label>
														<input type="text" class = "Requerido form-control" name="total_factura" id="total_factura" style="font-weight: bold;font-size: 15px;" value='{{number_format($totalFactura[0]->total,2,",",".")}}' disabled="disabled"/>
												</div>
										    </div>


											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>TOTAL_BRUTO</label>
														<input type="text" class = "Requerido form-control" name="total_bruto" id="total_bruto" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_bruto_factura,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>TOTAL_NETO</label>		
														<input type="text" class = "Requerido form-control" name="total_neto" id="total_neto" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_neto_factura,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>


									
											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>EXENTAS</label>
														<input type="text" class = "Requerido form-control" name="total_bruto" id="total_exentas" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_exentas,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>GRAVADAS</label>
													<input type="text" class = "Requerido form-control" name="total_iva" id="total_gravadas" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_gravadas,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>IVA</label>
													<input type="text" class = "Requerido form-control" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_iva,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>COSTOS</label>
													<input type="text" class = "Requerido form-control" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_costos,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>


								
											<div class="col-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label>COMISIÓN</label>
													<input type="text" class = "Requerido form-control" name="total_comision" id="total_comision" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->proforma->total_comision,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2" >
												<div class="form-group">
													<label>NO_COMISIONA</label>
													<input type="text" class = "Requerido form-control" name="total_comision" id="total_no_comisiona" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_no_comisiona,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>


											<div class="col-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label>MARKUP</label>
													<input type="text" class = "Requerido form-control" name="markupF" id="markupF" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->markup,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>
										

											<div class="col-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label>% GANANCIA</label>
													<input type="text" class = "Requerido form-control" name="total_costo" id="porcentaje_ganancia" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->porcentaje_ganancia,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>GANANCIA</label>
													<input type="text" class = "Requerido form-control" name="senh" id="ganancia_venta" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->ganancia_venta,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label>INCENTIVO</label>
													<input type="text" class = "Requerido form-control" name="incentivo" id="incentivo" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->incentivo_vendedor_agencia,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>RENTA</label>
													<input type="text" class = "Requerido form-control" name="incentivo" id="renta" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->renta,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

									
									</div>	
							 
							@endforeach	
						@endif	 

				</div>
            </div>
        </div>    	
    </div>      
 </section>


 @endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
 
	<script>

		$(()=>{
			formatNumber();
			revisarDatos();
			$('[data-toggle="tooltip"]').tooltip();
		});

		const operacion = {
			valid: false,
			msjErr: []
		}

		$('#btnDevolucion').click(()=>{
			$('#opcion').val(1);
			return swal({
                        title: "GESTUR",
                        text: " ¿Está seguro que desea generar la Nota de Crédito? Este proceso va a generar una OP a favor del cliente para la devolución del monto abonado.",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, Generar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
										guardar();
								}else {
                            		swal("Cancelado", "", "error");
                       			}
							});
		});

		$('#btnNCredito').click(()=>{
			$('#opcion').val(2);
			mensaje = "¿Está seguro que desea generar la Nota de Crédito?<br> Este proceso va a generar una Nota de Crédito, según saldo de la factura:<br>";
			mensaje += "1- NC de cancelación de factura (factura no pagada) o<br>";
			mensaje += "2- NC con saldo a favor que podrá aplicarse a otra factura del mismo cliente (factura pagada)";

			var span = document.createElement("span");
			span.innerHTML = mensaje;

			return swal({
                        title: "GESTUR",
                        content: span,
						//content: mensaje,
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, Generar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
										guardar();
								}else {
                            		swal("Cancelado", "", "error");
                       			}
							});

		});


		function guardar(){
			if(validarNota()){
				sendData();
			} else {
				$.toast().reset('all');	
				mensaje ="";
				$.each(operacion.msjErr, function (key, item){
					if(key == 0){
						mensaje+= item		
					}else{
						mensaje+= ","+item
					}
				})	
				swal("Cancelado",mensaje, "error");
			/*	$.toast({
						    heading: 'Error',
						    text: operacion.msjErr,
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});*/

			operacion.msjErr = [];			
			}
		}

		function controlInputMontoNc(id_factura, id_factura_detalle, clase)
		{
			
			if(parseInt($('.inputMontoNota_'+id_factura_detalle).val()) != 0){
				$.ajax({
							type: "GET",
							url: "{{route('controlarSaldoDisponibleNc')}}",
							dataType: 'json',
							data: {id_factura: id_factura, id_factura_detalle: id_factura_detalle},
							error: function(jqXHR,textStatus,errorThrown)
							{
	                        },
							success: function(rsp)
							{
								 console.log(rsp);
								var diferencia = parseFloat(rsp.total_detalle) - parseFloat(rsp.monto_nc);

								if ( diferencia <= 0 ) 
								{
									$('.'+clase).css('border','1px solid red');
									$('.'+clase).prop("disabled", true);
									$.toast().reset('all');	
									$.toast({
												    heading: 'Error',
												    text: 'Ya no se puede generar nota de crédito. Ha superado el límite disponible.',
												    position: 'top-right',
												    showHideTransition: 'fade',
												    icon: 'error'
									});
								}
								else
								{
									$('.'+clase).css('border-color','#d2d6de');

								}
							}	
						})
			}else{
				$('.'+clase).css('border','1px solid red');
				$('.'+clase).prop("disabled", true);
			}


		}


		function controlInput(opcion,clase,valor)
		{
			let monto = clean_num($('.'+clase).val());
			let saldo = clean_num($('.'+clase+'.saldo').val(),true);
			if(monto > saldo){
				console.log();
				$('.'+clase).css('border','1px solid red');
				
				$.toast().reset('all');	
				$.toast({
						    heading: 'Error',
						    text: 'El monto ingresado supera el saldo disponible.',
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});
					
			} else {
				$('.'+clase).css('border-color','#D2D6DE');
			}
			
		}

		function clean_num(n,bd=false){

				if(n && bd == false){ 
				n = n.replace(/[,.]/g,function (m) {  
										if(m === '.'){
											return '';
										} 
										if(m === ','){
											return '.';
										} 
								});
				return Number(n);
				}
				if(bd){
				return Number(n);
				}
				return 0;

		}//

	


			$(".sumar").keyup(function(){
			
				var sum = 0;
				let val = 0;
				$('.sumar').each(function(index,value){
					val = clean_num($(value).val());
					if(Boolean(val)){
						sum += val;
					}
					console.log(val);
					
				});

				if(isNaN(sum)){
					$('#totalizar').val(0);
				}else{
					total =sum
					$('#totalizar').val(sum.toFixed(2));
				}	 
			})	
			function validarNota()
			{
					formNotaCredito = $('#frmNotaCredito').serializeJSON();
					let ok = 0;
					$.each(formNotaCredito.data,(index,value) => 
					{	
					
						if(clean_num(value.monto) > clean_num(value.saldoFd,true)){
							operacion.msjErr.push('El monto del ítem factura supera el saldo.');
							ok++	
							return false;	
						} 


					
						if(clean_num(value.credito) > clean_num(value.saldoLc,true)){
							operacion.msjErr.push('El monto  de crédito proveedor supera el saldo disponible.');
							ok++	
							return false;
							
						} 


					});


					if(clean_num(formNotaCredito.total) > clean_num(formNotaCredito.total_factura,true)){
						operacion.msjErr.push('El total de la nota de crédito supera el total de la factura.');
						ok++;
					}

					if(clean_num(formNotaCredito.total) <= 0){
						operacion.msjErr.push('El total de la nota de crédito no puede ser cero.');
						ok++;
						
					}

				if(ok !=0)
				{
					return false;	
				}	
				return true;	
				
			}


			function sendData() {

				let dataString = $('#frmNotaCredito').serializeJSON({ 
                    customTypes: customTypesSerializeJSON
				});
				$('#btnDevolucion').prop("disabled", true);
				$('#btnNCredito').prop("disabled", true);

				$.ajax({
						type: "GET",
						url: "{{route('doAddNotaCredito')}}",
						dataType: 'json',
						data: dataString,
						 error: function(jqXHR,textStatus,errorThrown){

                            },
						success: function(rsp){
								if(rsp.status == 'OK'){
									swal("Éxito", rsp.mensaje, "success");
									$('.numerico').attr('disabled',true);
									$('#Guardar').attr('disabled',true);
									$('#btnDevolucion').prop("disabled", false);
									$('#btnNCredito').prop("disabled", false);
								 	 location.href ="../verNota/"+rsp.id;
								}else{
									swal("Cancelado", rsp.mensaje, "error");
								}
							}	
						})
		    }//




		function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

	function formatNumber(){
		$('.numerico').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			// oncleared: function () { self.Value(''); }
		});
	}	


	function revisarDatos(opcion,clase,valor){
	    var datos = @php echo $factura->facturaDetalle @endphp;
		$.each(datos, function (key, item){
			controlInputMontoNc(item.id_factura, item.id, 'inputMontoNota_'+item.id);
		})
	}



	</script>
@endsection