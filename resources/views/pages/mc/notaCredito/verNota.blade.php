@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	 <style type="text/css">
	 	.error{
	 		color:red;
	 	}
	 	.anularStyle {
	 		padding-top: 10px;
	 	}
	 	.anularLetter {
	 		font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif; 
	 		font-weight: 700;
	 		font-size: 1.33333333em;
    		line-height: .75em;
	 	}


	 </style>
@endsection
@section('content')
<section id="base-style">
  @include('flash::message') 
  @if(!empty($resultados)) 
	@foreach($resultados as $key=>$resultado)
	    <div class="card" style="border-radius: 14px;">
	        <div class="card-header" style="border-radius: 14px;">
	            <h4 class="card-title">Nota de Crédito</h4>
	            <div class="heading-elements">
	                <ul class="list-inline mb-0">
	                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
	                </ul>
	            </div>
	        </div>
	        <div class="card-content collapse show" aria-expanded="true">
	            <div class="card-body">
							<div class="row">
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Nro Nota Crédito Nº</label>
												<input type="text" class = "form-control" name="proforma_id" value="{{$resultado->nro_nota_credito}}" readonly style="font-weight: bold;font-size: 15px;"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Cliente</label>
												<?php
													$documento = $resultado->cliente['documento'];
													if($resultado->cliente['dv']){
														$documento = $resultado->cliente['documento'].'-'.$resultado->cliente['dv']; 
													}
												?>
												<input type="text" class = "form-control" name="proforma_id" value="{{$documento}} - {{$resultado->cliente['nombre']}} {{ $resultado->cliente['apellido'] }} - {{ $resultado->cliente['id'] }}" readonly/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Vendedor</label>
												<?php 
													if(isset($resultado->vendedorEmpresa['nombre'])){
														$vendedor = $resultado->vendedorEmpresa['nombre'];
													}else{
														$vendedor = "";	
													}
													?>
												<input type="text" class = "form-control" name="proforma_id" value="{{$vendedor}}" readonly/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Pasajero Principal</label>
												<?php 
													if(isset($resultado->pasajero->nombre)){
														$pasajero = $resultado->pasajero->nombre;
													}else{
														$pasajero = '';
													}
												?>
												<input type="text" class = "form-control" name="proforma_id" value="{{$pasajero}}" readonly/>
											</div>
									    </div>
								
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Destino</label>
												<input type="text" class = "form-control" name="proforma_id" value="{{$destino}}" readonly/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Check In - Check Out</label>
												<?php 
													$fecha_periodo1 = date('d/m/Y', strtotime($resultado->check_in)); 
													$fecha_periodo2 = date('d/m/Y', strtotime($resultado->check_out)); 
													$periodo = $fecha_periodo1." - ".$fecha_periodo2;
												?>
												<input type="text"  class = "form-control" id="periodo" value="{{$periodo}}" name="periodo" disabled="disabled" tabindex="10"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Moneda de Facturación</label>
												@if(isset($resultado->currency->currency_code))
													<input type="hidden" class = "form-control" id="moneda_id" value="{{$resultado->id_moneda_venta}}" readonly/>
													<input type="text" class = "form-control" name="proforma_id" value="{{$resultado->currency->currency_code}}" readonly/>
												@else
													<input type="text" class = "form-control" name="proforma_id" value="" readonly/>
												@endif	
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Cotización</label>
												<input type="text" disabled="disabled" class = "form-control" name="proforma_id" value="{{number_format($resultado->cotizacion,0,',','.')}}" readonly/>
											</div>
									    </div>										
									</div>
									<div class="row">      
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Monto Seña</label>
												<input type="text" onkeypress="return justNumbers(event);" class = "form-control" name="senha" id="senha" value="{{$resultado->senha}}" disabled/>
											</div>
									    </div>
							            <div class="col-12 col-sm-4 col-md-3">
									        <div class="form-group">
									            <label>Expediente</label>
												<input type="text" onkeypress="return justNumbers(event);" class = "form-control" name="senha" id="senha" value="{{$resultado->proforma_padre}}" disabled/>
									        </div>
							            </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Estado</label>
												 @if($resultado->id_estado_nc == "30")
													<input type="text" disabled="disabled" class = "form-control" name="proforma_id" value="{{$resultado->estado->denominacion}}" readonly style="font-weight: bold;font-size: 18px; color:red;"/>
												@else
													<input type="text" disabled="disabled" class = "form-control" name="proforma_id" readonly value="{{$resultado->estado->denominacion}}" style="font-weight: bold;font-size: 18px;"/>
												@endif	
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Factura N°</label>
												@if(isset($resultado->factura[0]->nro_factura))
													<input type="text" class = "form-control" name="proforma_id" value="{{$resultado->factura[0]->nro_factura}}" readonly style="font-weight: bold;font-size: 15px;"/>
												@else
													@if(isset($resultado->libroVentaSaldo0->nro_documento))
														<input type="text" class = "form-control" name="proforma_id" value="{{$resultado->libroVentaSaldo0->nro_documento}}" readonly style="font-weight: bold;font-size: 15px;"/>
													@else 
														<input type="text" class = "form-control" name="proforma_id" value="0" readonly style="font-weight: bold;font-size: 15px;"/>
													@endif
												@endif	
											</div>
									    </div>
					            	
									    <div class="col-12 col-sm-4 col-md-3">
									    	<div class="form-group">
									    		<label>Vencimiento</label>
									    		<div class="input-group">
									    			<div class="input-group-prepend">
									    				<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									    			</div>
									    			@if(date('d/m/Y', strtotime($resultado->vencimiento)) !== '31/12/1969')
									    			<input type="text" value="{{date('d/m/Y', strtotime($resultado->vencimiento))}}"
									    				class="form-control pull-right fecha" name="vencimiento" id="vencimiento"
									    				disabled="disabled" tabindex="11">
									    			@else
									    			<input type="text" value="" class="form-control pull-right fecha" name="vencimiento"
									    				id="vencimiento" disabled="disabled" tabindex="11">
									    			@endif
									    		</div>
									    	</div>
									    </div>
										
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
											    <label>Fecha de Nota de Crédito</label>
													@if(date('d/m/Y', strtotime($resultado->fecha_hora_nota_credito)) !== '31/12/1969')
														<input type="text" value="{{date('d/m/Y H:m:i', strtotime($resultado->fecha_hora_nota_credito))}}" class="form-control pull-right fecha" name="fecha_hora_nota_credito" id="vencimiento" disabled="disabled" tabindex="11">
													@endif    
											</div>
										</div>

										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Saldo</label>
												<?php 
													if(isset($resultado->libroVenta[0]->saldo)){
														$saldo = $resultado->libroVenta[0]->saldo;	
													}else{
														if($resultado->id_lv != null){
															$saldo = $resultado->saldo_nota_credito;
														}else{
															$saldo = 0;
														}
													}
												?>
												<input type="text" class = "form-control numeric" name="saldo" id="saldo"  value="{{$saldo}}" disabled/>
											</div>
									    </div>

										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>CDC</label>
												<input type="text" class = "form-control" name="inputCdc" id ="inputCdc" value="{{$resultado->cdc}}" style="color:green;font-weight: bold;" readonly/>
											</div>
									    </div>
							            @if($resultado->id_estado_factura == "30")
												<div class="col-12 col-sm-4 col-md-3">
													<div class="form-group">
														<label>Usuario de Anulación</label>
														@foreach($personas as $person)
															@if($person->id == $resultado->usuario_anulacion_id)
																<input type="text" class = "form-control" name="senha" id="senha" value="{{$person->apellido}}, {{$person->nombre}}" disabled/>

															@endif
														@endforeach
													</div>
											    </div>
											  
										
												<div class="col-12 col-sm-4 col-md-3">
													<div class="form-group">
											            <label>Fecha de Anulación</label>
														    @if(date('d/m/Y', strtotime($resultado->fecha_hora_anulacion)) !== '31/12/1969')
															    <input type="text" value="{{date('d/m/Y H:m:i', strtotime($resultado->fecha_hora_anulacion))}}" class="form-control pull-right fecha" name="vencimiento" id="vencimiento" disabled="disabled" tabindex="11">
															@endif    
													</div>
												</div>
						            	@endif	
							   
							            <div class="col-12">
											<div class="row">    
												<br>
												<div class="col-12 col-md-3">
													@if(isset($liquidacion->id))	
														<a href="{{route('verLiquidaciones')}}?datos={{$liquidacion->id}}" style="color:black"  class="bgRed"><label class = "base">Liquidación  <i class="fa fa-fw fa-search" style="color:red;cursor: pointer;"></i></label></a>
														<input style="color:red" type="text" class = "Requerido form-control input-sm text-bold" name="proforma_id" style="font-weight: bold;font-size: 15px;font-weight: bold !important;" value="{{$liquidacion->periodo}}" readonly="readonly"/>
													@endif	
												</div>
												<div class="col-12 col-sm-8 col-md-8" style="margin-top: 20px;">
													@if($resultado->cdc == null)
														<a role='button' href="/generarJsonNCURL/{{$resultado->id}}" class="btn btn-info pull-right text-white" title='Reenviar Nota de Credito para DE'><i class='fa fa-external-link-square'></i><b>Reenviar DE</b></a>
													@endif	
													@foreach ($btn as $boton)
														{{-- CARGA LOS BOTONES POR PERMISO DE PERSONAS --}}
															@if($resultado->id_estado_nc != 30)
																<?php echo $boton; ?>
															@endif
													@endforeach
													<button type="button" onclick="verExtracto({{$resultado->id}})" class="btn btn-success pull-right mr-1" tabindex="24"  id="btnExtracto"><b>Extracto NC</b></button>
													<a href="{{route('notaCreditoImprimir',['id'=>$resultado->id])}}" class="btn btn-success pull-right mr-1" style="background: #E5097F !important;" role="button">
														<i class="fa fa-print fa-lg"></i><b> Imprimir</b>
													</a>
													@if($permiso == 1)
														@if($perfil == 2|| $perfil == 6)
															<button type="button" class="btn btn-success pull-right mr-1" tabindex="24" style="margin-left: 18px;" id="btnActualizar"><b>Generar LV</b></button>
														@endif
													@endif	
												</div>
											</div>
						            	</div> 
					            </div>
	            </div>	
	        </div>	    
		</div>	    
		
	    <div class="card" style="border-radius: 14px;">
	        <div class="card-header" style="border-radius: 14px;">
	            <h4 class="card-title">Detalle de Nota de Crédito</h4>
	            <div class="heading-elements">
	                <ul class="list-inline mb-0">
	                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
	                </ul>
	            </div>
	        </div>
	        <div class="card-content collapse show" aria-expanded="true">
	            <div class="card-body">
								            	<div class="table-responsive">
									              	<table id="lista_productos" class="table">
									                	<thead>
															<tr>
														        <th>Cant.</th>
																<th>Producto</th>
																<th>In/Out</th>
																<th>Cant.<br> Pasajero</th>
														        <th>Código</th>
														        <th>Proveedor</th>
														        <th>Prestador</th>
																<th>Detalle</th>
														        <th>Moneda</th>
																<th>Costo</th>
														        <th>Venta</th>
														        <th>%</th>
														       <!-- <th>F. Gasto</th>-->
																<th></th>
												            </tr>
									                	</thead>
									                	<tbody >	
									                	@foreach($resultado->detalleNotaCredito as $key=>$detalle)
									                		@if($detalle->activo == true)
										                		<tr>
															        <td>{{$detalle->cantidad}}</td>
															        <td>
															        @foreach($producto as $product)
															        	@if($product->id == $detalle->id_producto)
															        		<b>{{$product->denominacion}}</b>
															        	@endif
															        @endforeach
															        </td>
															        <td>
															        @if(date('d/m/Y', strtotime($detalle->fecha_in)) !== '31/12/1969')
															        	{{date('d/m/Y', strtotime($detalle->fecha_in))}} - 
															        @endif

															        @if(date('d/m/Y', strtotime($detalle->fecha_out)) !== '31/12/1969')
															        	{{date('d/m/Y', strtotime($detalle->fecha_out))}}
															        @endif
															        </td>
															        <td>{{$detalle->cantidad_pasajero}}</td>
															        <td>{{$detalle->cod_confirmacion}}</td>
															        <td>
															        @foreach($personas as $person)
															        	@if($person->id == $detalle->id_proveedor)
															        		<b>{{$person->nombre}}</b>
															        	@endif
															        @endforeach
															        </td>
															        <td>
															        @foreach($personas as $person)
															        	@if($person->id == $detalle->id_prestador)
															        		<b>{{$person->nombre}}</b>
															        	@endif
															        @endforeach
															        </td>
															        <td>{{$detalle->descripcion}}</td>
															        <td>
																        @foreach($currency as $curren)
																        	@if($curren->currency_id == $detalle->currency_costo_id)
																        		<b>{{$curren->currency_code}}</b>
																        	@endif
																        @endforeach
																    </td>
															        <td><b>{{$detalle->costo_proveedor}}</b></td>
															        <td><b>{{$detalle->precio_venta}}</b></td>
															        <td><b>{{number_format($detalle->markup,2,",",".")}}</b></td>
															        <td></td>
										                		</tr>	
										                	@endif	
									                	@endforeach
									                	</tbody>	
									                </table>	 
									            </div>    

	            </div>	
	        </div>	    
		</div>
		
		
	    <div class="card" style="border-radius: 14px;">
	        <div class="card-header" style="border-radius: 14px;">
	            <h4 class="card-title">Resumen</h4>
	            <div class="heading-elements">
	                <ul class="list-inline mb-0">
	                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
	                </ul>
	            </div>
	        </div>
	        <div class="card-content collapse show" aria-expanded="true">
	            <div class="card-body">
						            	<div class="row">
											
											<div class="col-12 col-sm-4 col-md-3">
												<div class="form-group">
													<label>TOTAL_NOTA_CREDITO</label>
													@if($resultado->id_tipo_facturacion == 1)
														<input type="text" class = "form-control" name="total_bruto" id="total_bruto" style="font-weight: bold;font-size: 15px;" value='{{number_format($resultado->total_neto_nc,2,",",".")}}' disabled/>
													@else
														<input type="text" class = "form-control" name="total_neto" id="total_neto" style="font-weight: bold;font-size: 15px;" value='{{number_format($resultado->total_bruto_nc,2,",",".")}}' disabled/>
													@endif
												</div>
											</div>
											
											<div class="col-12 col-sm-4 col-md-3">
												<div class="form-group">
													<label>EXENTAS</label>
														<input type="text" class = "form-control" name="total_bruto" id="total_exentas" style="font-weight: bold;font-size: 15px;" value='{{number_format($resultado->total_exentas,2,",",".")}}' disabled/>
												</div>
											</div>
											
											<div class="col-12 col-sm-4 col-md-3">
												<div class="form-group">
													<label>GRAVADAS</label>
													<input type="text" class = "form-control" name="total_iva" id="total_gravadas" style="font-weight: bold;font-size: 15px;" value='{{number_format($resultado->total_gravadas,2,",",".")}}' disabled/>
												</div>
											</div>
											
											<div class="col-12 col-sm-4 col-md-3">
												<div class="form-group">
													<label>IVA</label>
													<input type="text" class = "form-control" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px;" value='{{number_format($resultado->total_iva,2,",",".")}}' disabled/>
												</div>
											</div>
											
										</div>
	            </div>	
	        </div>	    
	    </div>	
	@endforeach    
  @endif	    
    <!-- /.content -->
<!--======================================================
        MODAL DE ADJUNTO IMAGEN
========================================================== -->
    <!-- /.content -->
    <div id="modalFacturas" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 100%;margin-top: 5%;margin-left: 4%;">
        <!-- Modal content-->
            <div class="modal-content" style="width: 160%;">
                <div id="modal-header" class="modal-header">
                    <h2 class="modal-title titlepage" style="font-size: x-large;">Aplicar a factura</h2>
					<button type="button" class="closeModal"  data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
                <div class="modal-body" style="padding-top: 0px;">
					<form id="aplicarFactura">
						<div class="table-responsive table-bordered font-weight-bold">
							<table id="listado" class="table" style="width: 100%;font-size: small;">
								<thead>
									<tr>
										<th style="width: 15%">Factura</th>
										<th>Fecha</th>
										<th>Proforma</th>
										<th>Monto</th>
										<th>Moneda</th>
										<th>Saldo</th>
										<th>Cliente</th>
										<th>Pasajero</th>
										<th></th>
									</tr>
								</thead>
								<tbody style="text-align: center">

								</tbody>
							</table>
						</div>
						<div class="row">
							<input type="text" class = "form-control" name="id_factura" id="id_factura" style="font-weight: bold;font-size: 15px; display:none"/>
							<input type="text" class = "form-control" name="id_nc" id="id_nc" value="{{$resultado->id}}" style="font-weight: bold;font-size: 15px;display:none"/>
							<div class="col-12 col-sm-4 col-md-3"></div>
							<div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
									<label>Saldo Factura</label>
									<input type="text" class = "form-control numeric" name="saldo_factura" id="saldo_factura" style="font-weight: bold;font-size: 15px;" disabled="disabled"/>
								</div>
							</div>	
							<div class="col-12 col-sm-4 col-md-3">
								<div class="form-group">
									<label>Saldo Nota Credito <b>(Monto a aplicar a la factura)</b></label>
									@php
										if(isset($resultado->libroVenta[0]->saldo)){
											$saldo = $resultado->libroVenta[0]->saldo;
										}else{	
											$saldo = 0;
										}		
									@endphp
									<input type="text" class = "form-control numeric" name="saldo_credito" value="{{$saldo}}" id="saldo_credito" style="font-weight: bold;font-size: 15px;"/>
									<input type="hidden" class = "form-control numeric" name="saldo_control" value="{{$saldo}}" id="saldo_control" style="font-weight: bold;font-size: 15px;"/>

								</div>
							</div>	
							<div class="col-12 col-sm-4 col-md-3">
								<br>
								<button  onclick ="guardarNota()" class="pull-right btn btn-info btn-lg mr-1" type="button"><b>Guardar</b></button>
							</div>
						</div>	
					</form>	
                </div>
            </div>  
        </div>  
    </div>  

	{{--==============================================
			VER EXTRACTO
	============================================== --}}
	<div class="modal fade" id="modalExtracto" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document" style="margin-left: 5%;margin-top: 7%;">
			<div class="modal-content" style="width: 250%;">
				<div class="modal-header">
					<h5 class="modal-title">Ver Extracto</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-12" style="margin-bottom: 5px;">
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Total de Nota Crédito:</label>
								@php 
									if($resultado->id_tipo_facturacion == 1){
										$totalFactura = $resultado->total_neto_nc;
									}else{
										$totalFactura = $resultado->total_bruto_nc;
									}	
								@endphp


								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm" style="font-weight: 800; font-size: 20px;" value='{{number_format($totalFactura,2,",",".")}}' id="total_nc" disabled>
								</div>
							</div>	
							<?php  
							   $cobrado = (float)$totalFactura - (float)$saldo;
							?>
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Total Aplicado :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm" style="font-weight: 800; font-size: 20px;" id="monto_cobrado" value='0' disabled>
								</div>
							</div>
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;">Saldo Nota Crédito:</label>
								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm numeric" style="font-weight: 800; font-size: 20px;" id="monto_saldo" value='0' disabled>
								</div>
							</div>
							<div class="form-group row" style="margin-bottom: 0px;">	
								<label for="monto_proforma" class="col-sm-3 col-form-label" style="text-align: right;color:green">Facturas Cobradas con Nota Crédito:</label>
								<div class="col-sm-4">
									<input type="text" class="form-control form-control-sm input-sm numeric" style="font-weight: 800; font-size: 20px;color:green" id="facturaCobrada" value='0' disabled>
								</div>
							</div>

						</div>
						<br>
							<div class="col-12">
								<div class="table-responsive">
									<table id="listadoExtractos" class="table" style="width:100%">
										<thead>
											<tr>
												<th>Recibo/NC</th>
												<th>Fecha</th>
												<th>Importe<br>Recibo/NC</th>
												<th>Moneda<br> Recibo/NC</th>
												<th>Cotizacion</th>
												<th>Importe<br>Cobro</th>
												<th>Moneda<br>Cobro</th>
												<th>Forma<br>Cobro</th>
											</tr>
										</thead>
										<tbody style="text-align: center">
										</tbody>
									</table>
								</div>
							</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><b>Cerrar</b></button>
				</div>
			</div>
		</div>
	</div>
	<!-- ========================================
   			MODAL ANULAR DE 
   	========================================  -->		

	   <div class="modal fade" id="modalAnularNC" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" style="margin-left: 25%;margin-top: 10%;">
			<div class="modal-content" style="width: 150%;">
				<div class="modal-header">
					<h4 class="modal-title" id="" style="font-weight: 800;">Anular Nota de Credito Electrónica</h4>
				</div>
				<div class="modal-body">
					<div class="table-responsive" >
						<form id="formFacturaDetalle"> 
							<input type="hidden" name="id_nc" value="{{$resultado->id}}">
							<div class="col-md-12">
								<div class="form-group">
									<label style="font-weight: 600;font-size: 18px;">Motivo de Anulación</label>
									<textarea class="form-control" rows="5" id="acuerdo_id"></textarea>
								</div>
							</div>
						</from>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success btn-lg" onclick="anularDE()"><i class="fa fa-fw fa-file-o"></i> <b>Guardar</b></button>
					<button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger btn-lg"><b>Salir</b></button>
				</div>
			</div>
		</div>
	</div>



@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>

	<script>

		dato = "{{$saldo}}";
		estado = "{{$resultado->id_estado_nc}}";
		if(dato == 0 || parseInt(estado)== 35){
			$('.aplicarNc').css('display', 'none');

		};		
		if(parseInt(estado)== 35){
			$('.anularStyle').css('display','none');
		}	
		
		$('.modalAnular').on('click',function(){ 
			if($('#inputCdc').val() == ""){	
					swal({
							title: "Anular Nota de Crédito",
							text: "¿Está seguro que desea anular la nota de crédito?",
							icon: "warning",
							showCancelButton: true,
							buttons: {
								cancel: {
									text: "No, Cancelar",
									value: null,
									visible: true,
									className: "btn-warning",
									closeModal: false,
								},
								confirm: {
									text: "Sí, Continuar",
									value: true,
									visible: true,
									className: "",
									closeModal: false
								}
							}
						}).then(isConfirm => {
							if (isConfirm) {
								$.when(anularNota()).then((a)=>{ 
									swal("Éxito",a , "success");


									setTimeout( ()=>{ location.reload(true);},1000);
								
								},(b)=>{
									swal("Cancelado", b, "error");
								});
								
							} else {
								swal("Cancelado", "La operación fue cancelada", "error");
							}
						});
			}else{
				$("#modalAnularNC").modal('show');
			}

		});	


	$("#listado").dataTable();
	$('.aplicarNc').on('click',function(){ 	
		$.ajax({
                type: "GET",
                url: "{{route('getNotaCredito')}}",
				data: {
						datamoneda: $("#moneda_id").val(),
						idCliente: "{{$resultado->cliente_id}}"
					},
                dataType: 'json',
                error: function(jqXHR,textStatus,errorThrown){},
                success: function(rsp){
										console.log(rsp);
										var oSettings = $('#listado').dataTable().fnSettings();
										var iTotalRecords = oSettings.fnRecordsTotal();

										for (i=0;i<=iTotalRecords;i++) {
											$('#listado').dataTable().fnDeleteRow(0,null,true);
										}

										$.each(rsp, function (key, item){
											if(jQuery.isEmptyObject(item.vendedor_n) == false){
												vendedor = item.vendedor_n;
											}else{
												vendedor ="";
											}
											accion = '<button type="button" id="btnAceptarProforma" class="btn btn-danger" onclick="calcular('+item.saldo_factura+','+item.id+')" style="width: 40px;background-color: #e2076a;padding-right: 5px;padding-left: 5px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-plus fa-lg"></i></button>';
											var dataTableRow = [
															item.nro_factura,
															item.fecha_format,
															item.id_proforma,
															formatter.format(item.monto_factura),
															item.currency_code,
															formatter.format(item.saldo_factura),
															item.cliente_n,
															item.pasajero_n,
															accion
															];

										var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
										var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
										})	
									}	
			});	
			$("#modalFacturas").modal("show");
	});	
	const formatter = new Intl.NumberFormat('de-DE', {
					  currency: 'USD'
					});

	function guardarNota(){
		console.log(parseFloat($('#saldo_control').val()));
		console.log(parseFloat(clean_num($('#saldo_factura').val())));
		if(jQuery.isEmptyObject($('#saldo_factura').val()) == false){
			if(parseFloat(clean_num($('#saldo_credito').val())) <= parseFloat(clean_num($('#saldo_factura').val()))){
				$('#saldo_factura').prop('disabled',false);
				var dataString = $('#aplicarFactura').serialize();	
				$.ajax({
						type: "GET",
						url: "{{route('guardarNotaCredito')}}",
						dataType: 'json',
						data: dataString,
						error: function(jqXHR,textStatus,errorThrown){},
						success: function(rsp){
							if(rsp =="OK"){
								$("#modalFacturas").modal('hide');
								$.toast({ 
										heading: 'Exito',
										text: 'Se ha aplicado la nota de credito a la factura.',
										position: 'top-right',
										showHideTransition: 'slide',
										icon: 'success'
									});  
							 	location. reload();	
							}else{
								$('#saldo_factura').prop('disabled',true);
								$.toast({
									heading: 'Error',
									text: rsp,
									showHideTransition: 'fade',
									position: 'top-right',
									icon: 'error'
								});				
							}	
						}	
					});
			}else{	
				$.toast({
						heading: 'Error',
						text: 'El monto a aplicar de la nota de credito tiene que ser mayor o igual al saldo de la fatura',
						showHideTransition: 'fade',
						position: 'top-right',
						icon: 'error'
					});		
					$('#saldo_credito').val(parseFloat($('#saldo_control').val()));		
			}		
		}else{	
			$.toast({
					heading: 'Error',
					text: 'Seleccione una factura',
					showHideTransition: 'fade',
					position: 'top-right',
					icon: 'error'
				});				
		}
	}

	function clean_num(n,bd=false){
			if(n && bd == false){ 
			n = n.replace(/[,.]/g,function (m) {  
					 				 if(m === '.'){
					 				 	return '';
					 				 } 
					 				  if(m === ','){
					 				 	return '.';
					 				 } 
					 			});
			return Number(n);
		}
		if(bd){
			return Number(n);
		}
		return 0;
		}
	$('#btnActualizar').on('click',function(){ 
		return swal({
                    title: "GESTUR",
                    text: "¿Desea generar el Libro de Ventas.?",
                    showCancelButton: true,
                    buttons: {
                            cancel: {
                                    text: "No",
                                    value: null,
                                    visible: true,
                                    className: "btn-warning",
                                    closeModal: false,
                                },
                            confirm: {
                                    text: "Sí, Generar",
                                    value: true,
                                    visible: true,
                                    className: "",
                                    closeModal: false
                                }
                            }
                }).then(isConfirm => {
                        if (isConfirm) {
                            $.ajax({
                                    type: "GET",
                                    url: "{{route('getGenearLibroVenta')}}",
                                    dataType: 'json',
                                    data: {
                                            id_nc:"{{$resultados[0]->id}}",
                                            },
                                    error: function(jqXHR,textStatus,errorThrown)
                                    {
                                    },
                                    success: function(rsp)
                                    {
                                        if(rsp = 'OK'){
                                            swal("Éxito", 'Se ha generado el libro', "success");
                                            location. reload();
                                        }else{
                                            swal("Cancelado", rsp, "error");
                                        }
                                    }   
                                })
                        } else {
                            swal("Cancelado", '', "error");
                        }
                });

	});


	function verExtracto(idFactura){	 
	$("#listadoExtractos").DataTable();
	$("#modalExtracto").modal('show');
	$.ajax({
			type: "GET",
			url: "{{route('getExtractoNC')}}",
			dataType: 'json',
			data: {
					idFactura:idFactura
				},
			error: function(jqXHR,textStatus,errorThrown){
				reject('Ocurrió un error en la comunicación con el servidor.');
			},
			success: function(rsp){
								var oSettings = $('#listadoExtractos').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();

								for (i=0;i<=iTotalRecords;i++) {
									$('#listadoExtractos').dataTable().fnDeleteRow(0,null,true);
								}
								total = 0;
								$.each(rsp.exacto, function (key, item){
											console.log(item);
											/*if(item.tipo == "NC"){
												btn = `<a href="{{route('verNota',['id'=>''])}}/${item.id_recibo_nc}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${item.nro_recibo_nc}</a><div style="display:none;">${item.id_recibo_nc}</div>`;
											} else {*/
												btn = `<a href="{{route('verFactura',['id'=>''])}}/${item.factura_id}"  class="bgRed">
												<i class="fa fa-fw fa-search"></i>${item.factura_aplicada}</a><div style="display:none;">${item.factura_aplicada}</div>`;
										//	} 
											accion = '<button type="button" id="btnAceptarProforma" class="btn btn-danger" onclick="calcular('+item.saldo_factura+','+item.id+')" style="width: 40px;background-color: #e2076a;padding-right: 5px;padding-left: 5px;padding-top: 5px;padding-bottom: 5px;"><i class="fa fa-plus fa-lg"></i></button>';
											if(item.tipo == "NC"){
												var dataTableRow = [
																	btn,
																	'<b style="color:green">'+item.fecha_aplicacion+'</b>',
																	'<b style="color:green">'+item.importe_item_recibo+'</b>',
																	'<b style="color:green">'+item.moneda_recibo+'</b>',
																	'<b style="color:green">'+item.cotizacion+'</b>',
																	'<b style="color:green">'+item.importe_cobrado+'</b>',
																	'<b style="color:green">'+item.moneda_cobro+'</b>',
																	'<b style="color:green">'+item.tipo+'</b>',
																];
											} else {
												var dataTableRow = [
																	btn,
																	item.fecha_aplicacion,
																	item.importe_item_recibo,
																	item.moneda_recibo,
																	item.cotizacion,
																	item.importe_cobrado,
																	item.moneda_cobro,
																	item.tipo,
																];
											}
										var newrow = $('#listadoExtractos').dataTable().fnAddData(dataTableRow);
										var nTr = $('#listadoExtractos').dataTable().fnSettings().aoData[newrow[0]].nTr;
										total = total + parseFloat(item.importe_cobrado);

								});
								total_forma = formatter.format(parseFloat(total))
								$('#monto_cobrado').val(total_forma);
								saldo = parseFloat(clean_num($('#total_nc').val())) - total; 
								saldo_forma = formatter.format(parseFloat(saldo));
								$('#monto_saldo').val(saldo_forma);

								var winSize = {
									wheight : $(window).height(),
									wwidth : $(window).width()
								};
								var modSize = {
									mheight : $('#modalExtracto').height(),
									mwidth : $('#modalExtracto').width()
								};
								$('#modalExtracto').css({
											'padding-left' :  ((winSize.wheight - (modSize.mheight/2))/2),
								});
					}	
				});
}


function calcular(saldo,id){
	$("#id_factura").val(id);
	$("#saldo_factura").val(saldo);
}

function anularNota(){

var id = $('.modalAnular').attr('data-btn');

return new Promise((resolve, reject) => {
	let cotizacion;
	$.ajax({
		async: false,
		type: "GET",
		url: "{{route('anularNota')}}",
		data: {
			idNota:id
		},
		dataType: 'json',
		error: function (jqXHR, textStatus, errorThrown) {
			reject('Ocurrió un error en la comunicación con el servidor.');
		},
		success: function (rsp) {

			if(rsp[0].anular_nota_credito !== 'OK'){
				reject(rsp[0].anular_nota_credito);
				$('.anularStyle').css('display', 'none');
			} else {
				resolve('La nota de crédito fue anulada.');

			}//else

		} //success
	});

}); //promise

}

$('.numeric').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			oncleared: function () { self.Value(''); }
		});

		function anularDE(){
				return swal({
							title: "Anular Nota de Crédito",
							text: "¿Está seguro que desea anular la nota de crédito?",
							icon: "warning",
							showCancelButton: true,
							buttons: {
								cancel: {
									text: "No, Cancelar",
									value: null,
									visible: true,
									className: "btn-warning",
									closeModal: false,
								},
								confirm: {
									text: "Sí, Continuar",
									value: true,
									visible: true,
									className: "",
									closeModal: false
								}
							}
						}).then(isConfirm => {
							if (isConfirm) {
									var id = $('.modalAnular').attr('data-btn');
									$.ajax({
											type: "GET",
											url: "{{route('anularDE')}}",
											dataType: 'json',
											data: {
													idNota:'{{$resultado->id}}',
													cdc: $('#inputCdc').val(),
													motivo: $('#acuerdo_id').val()
													},
											success: function(rsp){
													console.log(rsp);
													if(rsp.status == 'OK'){
														swal("Exito", rsp.mensaje, "success");
														$("#modalAnularNC").modal('hide');
														location.reload();
													}else{
														swal("Cancelado", rsp.mensaje, "error");
													}
												}
										});                            
									} else {
											swal("Cancelado", "", "error");
									}
						});
}

	</script>
@endsection