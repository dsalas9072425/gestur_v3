
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Nota credito</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			/*width: 76%;*/
			width: 90%;
			margin:0 auto;
		    /*position:absolute;*/
    		z-index:1;
	}
	
	table{
		width: 100%;
		/*border:1px solid;*/

	}
	
	.b-buttom {
	border-bottom: 1px solid; 
	}
	.b-top {
	border-top:1px solid;
	}
	
	.b-col {
		border-collapse: collapse;
	}

	.n-1{
		font-weight: 700;
	}

	.f-10 {
		font-size: 10px !important;
	}
	.f-5 {
		font-size: 5px !important;
	}
	.f-6 {
		font-size: 6px !important;
	}
	.f-7 {
		font-size: 7px !important;
	}

	.f-8 {
		font-size: 8px !important;
	}
	.f-9 {
		font-size: 9px !important;
	}
	.f-11 {
		font-size: 11px !important;
	}

	.f-12 {
		font-size: 12px !important;
	}
	.c-text {
		text-align: center;
	}

	.cabecera {

	}

	.espacio-10 {
		margin-top: 20px;
	}


	#background{
	/*margin-top:100px;*/
    position:absolute;
    z-index:0;
    background:white;
    display:block;
    min-height:50%; 
    min-width:50%;
    color:yellow;
	}

	#bg-text
{
    color:lightgrey;
    font-size:120px;
    transform:rotate(300deg);
    -webkit-transform:rotate(300deg);
}

	.hidden {
		display: none;
	}



	</style>
</head>
<body>



@php	 

 	function formatoFecha($date){
		if( $date != ''){

		$date = explode('-', $date);
		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
		 	return $fecha;
		} else {
			return 'N/A';
		}
	}


	function formatoFechaSinHora($date){
		if( $date != ''){

		$date = explode(' ', $date);
		$date = trim($date[0]);
		$date = explode('-', $date);

		 	$fecha = $date[2]."/".$date[1]."/".$date[0];
		 	return $fecha;
		} else {
			return 'N/A';
		}
	}

		function formatMoney($num,$f){

		if($f->id_moneda_venta != 111){
		return number_format($num, 2, ",", ".");	
		} 
		return number_format($num,0,",",".");

	}

	
	$facturaOriginal = array( ""=>"Original - Cliente",
							  "DUPLICADO"=>"Duplicado - Tesoreria",
							  "TRIPLICADO"=>"TRIPLICADO");

	$facturaAnulada = array("ANULADO"=>"Copia - Nota de Crédito no válida");
	$factClase = '';
	$recorrido = '';
	$maxDetalles = 20;
	$contadorRecorrido = 0;


//SE DEFINE EL RECORRIDO DE LAS IMPRESIONES
	//Orignial
if($notaCredito[0]->id_estado_nc == '35'){

	$recorrido = $facturaAnulada;
	$corte = 1;

} else {

	$recorrido  = $facturaOriginal;
	$corte = 3;

}

@endphp




@foreach ($recorrido as $key => $value)

@php
$contadorRecorrido++;


	
	/*-==============================================================
					VALORES DE CABECERA INFO NOTA CREDITO
	==================================================================  */
	$imagenNombre = 'logoEmpresa/'.$empresa->logo;
	$denominacionEmpresa = strtoupper($empresa->denominacion); 
	$emailEmpresa = $empresa->email; 
	$paginaEmpresa = $empresa->pagina_web;
	$telefonoEmpresa = $empresa->telefono;
	$direccionEmpresa = $empresa->direccion;
	$idEmpresa = $empresa->id; 
	$timbradoNum = $notaCredito[0]->timbrado['numero'];
	$timbradoAut = $notaCredito[0]->timbrado['autorizacion'];
	$timbradoFA = date('d/m/Y', strtotime($notaCredito[0]->timbrado['fecha_autorizacion'])); 

	$fechaInicioVigencia = formatoFecha($notaCredito[0]->timbrado['fecha_inicio']);
	$fechaFinVigencia = formatoFecha($notaCredito[0]->timbrado['vencimiento']);
	$rucFactura = $empresa->ruc;
	$numFactura = $notaCredito[0]->nro_nota_credito;
	$contadorDetalles = 0;

	$factClase = $value;

	/*
	 * =================================================================
	 * 				VALORES DE CABECERA DATOS DE FACTURA
	 * ================================================================= 
	 */
	


	if(isset($notaCredito[0]->vendedorEmpresa['nombre'])){
		$vendedorDtp = $notaCredito[0]->vendedorEmpresa['nombre'];
	}else{
		$vendedorDtp = "";
	}
	if(isset($notaCredito[0]->vendedorAgencia['nombre'])){
		$vendedorAgencia = $notaCredito[0]->vendedorAgencia['nombre'];
	}else{
		$vendedorAgencia = "";
	}
	
	$fechaEmisionFactura = formatoFechaSinHora($notaCredito[0]->fecha_hora_nota_credito);
	$fechaVencimientoFactura = formatoFecha($notaCredito[0]->vencimiento);
	$tipoFactura = $notaCredito[0]->tipoFactura['denominacion'];
	$nombreRazonSocial = $notaCredito[0]->cliente['nombre']." ".$notaCredito[0]->cliente['apellido'];
	$rucCliente = $notaCredito[0]->cliente['documento_identidad'];

	if($notaCredito[0]->cliente['dv'] != '' && $notaCredito[0]->cliente['dv'] != null){
		$rucCliente .= '-'.$notaCredito[0]->cliente['dv'];
	}
	$direccionFactura = $notaCredito[0]->cliente['direccion'];
	$telefonoFactura = $notaCredito[0]->cliente['telefono'];
	if(isset($notaCredito[0]->pasajero['nombre'])){
		$pasajeroFactura = $notaCredito[0]->pasajero['nombre'].' '.$notaCredito[0]->pasajero['apellido'];
	}else{
		$pasajeroFactura = "";
	}
	if(isset($notaCredito[0]->check_in)){
		$in = formatoFechaSinHora($notaCredito[0]->check_in);
	}else{
		$in = "";
	}

	if(isset($notaCredito[0]->check_out)){
		$out = formatoFecha($notaCredito[0]->check_out);
	}else{
		$out = "";
	}
	
	if(isset($notaCredito[0]->factura['id_proforma'])){
		$proforma =  $notaCredito[0]->factura['id_proforma'];
	}else{
		$proforma = 0;
	}


	/**
	 * =================================================================
	 * 				PIE DE SERVICIOS Y PRODUCTOS
	 * =================================================================
	 */ 

	

	$exentaTotal =  ($notaCredito[0]->total_exentas != null) ? formatMoney($notaCredito[0]->total_exentas,$notaCredito[0]) : "0,00";
	$iva5Total = "0,00";
	$iva10Total = ($notaCredito[0]->total_gravadas != null) ? formatMoney($notaCredito[0]->total_gravadas,$notaCredito[0]) : "0,00";
	$total = formatMoney($totalFactura,$notaCredito[0]);
	$moneda = $notaCredito[0]->currency['hb_desc'];

	
	$letraTotal = $NumeroALetras;
	$liquidacionIva10 =  formatMoney($notaCredito[0]->total_iva,$notaCredito[0]);
	$espacios = 0;







	/*==============================================================
						INICIO DE FACTURA
	 ==================================================================  */

	
@endphp





	

	
  <div id="background">
  <p id="bg-text" style="margin-top: 300px;"><?php echo $key  ?></p>
	</div>  

<div class="container espacio-10">


	<table>
		<tr>
					<td style="width: 25%;" class="c-text"> 
						<img src='{{$logo}}' alt="{{$empresa->logo}}" width="180">
					</td>


					<td style="width: 35%; padding: 10px;" class="c-text f-9">
						<span class="f-10"><?php echo $denominacionEmpresa;?></span><br>
						@php echo $empresa->head_factura_txt; @endphp

			
						@if($idEmpresa == 4||$idEmpresa == 8||$idEmpresa == 3 ) 
							<span class="f-7">
								E-mail: <?php echo $emailEmpresa;?><br>
							</span>	
						@endif

					</td>

					<td style="width: 40%;">
						<div style="padding: 4px;">
							<div style="border:1px solid; text-align: center; padding: 5px;">
								<span class="n-2">Timbrado Nro:<span id="numTimbrado"><?php echo $timbradoNum; ?></span></span><br>
								<span class="f-6">Autorizado como autoimpresor por la SET.N° Solicitud:<span><?php echo $timbradoAut; ?></span> Fecha: <span><?php echo $timbradoFA; ?></span></span><br>
								<span class="f-8">Fecha Inicio Vigencia: <?php echo $fechaInicioVigencia; ?></span><br>
								<span class="f-9">Fecha Fin Vigencia: <?php echo $fechaFinVigencia; ?></span><br>
								<span class="n-2">R.U.C <?php echo $rucFactura; ?></span><br>
								<span class="n-2">Nota Crédito Nro: <?php echo $numFactura;?></span><br>
								<span class="f-8"><b> <?php echo $factClase; ?> </b></span>	
							</div>

						</div>
					</td>
		</tr>

	</table>

	<table style="margin-top: 5px;"  class="b-buttom b-top">
		<tr>
			<td class="f-10" style="padding: 0 0 0 20px;">
				<br>
				@foreach($sucursalEmpresa as $sucursales)
					<b><?php echo ucwords(strtolower($sucursales->nombre)); ?>:</b> {{$sucursales->direccion}}. Telefonos: {{$sucursales->telefono}}<br>
				@endforeach
				<br>

				<!--<b>Casa Central:</b> Gral Bruguez 353 casi 25 de Mayo, Asunción - Paraguay. Telefonos: 021-221-816  Fax: 449-724<br>
				<b>Sucursal Ciudad del Este:</b> Avda Pioneros del Este entre Adrian Jara y Pa'i Perez, Galeria JyD. Ciudad del Este - Paraguay. Telefono: 061-511-779 al 80<br>
				<b>Sucursal Encarnación:</b> Mcal Estigarriba 1997. Galeria San Jorge, Local 13 y 16. Encarnación - Paraguay. Telefono: 071-200-104-->
			</td>
		</tr>
	</table>


	<table class="f-11" style="margin-top: 20px;">
		<tr>
			
		</tr>
	</table>


	<table class="f-11"  style="border:1px solid">

		<colgroup>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
			<col style="width: 10%"/>
	
		</colgroup>


		<tr>
			<td colspan="3">
				<b>Fecha de Emision:</b> <?php echo $fechaEmisionFactura; ?>
			</td>

			<td colspan="3">
				<b>Vencimiento:</b> <?php echo $fechaVencimientoFactura; ?>
			</td>

			<td colspan="3">
				<b>Tipo Factura:</b> Credito
			</td>

			<td colspan="3">

				@if(isset($notaCredito[0]->factura[0]))
				<b>Nro. Factura:</b> <?php echo $notaCredito[0]->factura[0]->nro_factura;?>
				@else
					@if(isset($notaCredito[0]->libroVentaSaldo0->nro_documento))
						<b>Nro. Factura:</b> <?php echo $notaCredito[0]->libroVentaSaldo0->nro_documento;?>
					@else 
						<b>Nro. Factura:</b> <?php echo '0';?>
					@endif
				@endif
			</td>
		</tr>


		<tr>
			<td colspan="6">
				<b>Nombre / Razon Social:</b> <?php echo $nombreRazonSocial; ?>
			</td>

			<td colspan="3">
				<b>RUC:</b> <?php echo $rucCliente; ?>
			</td>

		</tr>


		<tr>
			<td colspan="7">
				<b>Dirección:</b> <?php echo $direccionFactura; ?>
			</td>

			<td colspan="3">
				<b>Telefono:</b> <?php echo $telefonoFactura; ?>
			</td>

		</tr>

		<tr>
			<td colspan="4">
				<b>Pasajero:</b> <?php echo $pasajeroFactura;?>
			</td>

			<td colspan="2">
				<b>IN:</b> <?php echo $in;?>
			</td>

			<td colspan="2">
				<b>OUT:</b> <?php echo $out;?>
			</td>

			<td colspan="2">
				<b>Proforma N.:</b> <?php echo $notaCredito[0]->id_proforma;?>
			</td>
		</tr>

	</table>


	<table style="margin-top: 10px;">

		<colgroup>
			<col style="width: 50%"/>
			<col style="width: 50%"/>



	
		</colgroup>
		<tr>
			<td>
				Detalles
			</td>
			<td>
				Valor de Ventas
			</td>
		</tr>

	</table>

	<table class="c-text b-col" border="1">

	<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>


	
		</colgroup>


		<tr>
			<td width="5%">
				Cod
			</td>

			<td width="5%">
				Cant
			</td>
			<td width="40%">
				Descripción Producto
			</td>

			<td width="10%">
				Exentos
			</td>

			<td width="10%">
				Iva 5%
			</td>

			<td width="10%">
				Iva 10%
			</td>
		</tr>

		</table>
			
		<table class=""  FRAME="vsides" RULES="cols" style="border-bottom: 1px solid; ">

		<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
		</colgroup>

		

		{{-- ===================================
			AGREGAR SERVICIOS Y ESPACIOS
		=================================== --}}
		
		@foreach($notaCreditoDetalle as $detalles)
		
		{{-- VALIDAR SI EL PRODUCTO SE PUEDE IMPRIMIR EN FACTURA --}}
		@if($contadorDetalles <= 20 && $detalles->producto['imprimir_en_factura'] == 1 && $detalles->activo == true)
		
		<tr valign="top" class="f-11">
		

			<td class="c-text" width="5%">
				{{$detalles->id_producto}}
			</td>

			<td class="c-text" width="5%">
				{{$detalles->cantidad}}
			</td>

			<td width="40%">
				{{$detalles->descripcion}}
			</td>

			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
			
			</td>


		</tr>

			 <?php
			 $contadorDetalles++;
			 ?>


			@endif

		@endforeach
		

		{{-- AGREGAR ESPACIOS EN BLANCO PARA COMPLETAR 15 LINEAS EN TOTAL --}}
		<?php 
		if($contadorDetalles < $maxDetalles){
			$espacios = $maxDetalles - $contadorDetalles;
		}

		for ($i=0; $i < $espacios ; $i++):?>

			<tr valign="top" class="f-11">
		

			<td class="c-text" width="5%">
				&nbsp;
			</td>

			<td class="c-text" width="5%">
				
			</td>

			<td width="40%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>

			<td class="c-text" width="10%">
				
			</td>


		</tr>
			
		

		<?php endfor;?>

		<tr valign="top" class="f-11">
		

			<td class="c-text" width="5%">
				&nbsp;
			</td>

			<td class="c-text" width="5%">
				
			</td>

			<td width="40%">
				
			</td>

			<td class="c-text" width="10%">
				<?php echo $exentaTotal; ?>
			</td>

			<td class="c-text" width="10%">
				<?php echo $iva5Total; ?>
			</td>

			<td class="c-text" width="10%">
					<?php echo $iva10Total; ?>
			</td>


		</tr>
		


		

	</table>


	<table style="border-bottom: 1px solid; margin-top: 10px;">

		<colgroup>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
			<col/>
		</colgroup>


		<tr>

			<td style="width: 20%;" class="f-11" width="10%">
				<b>SUB-TOTALES:</b>
			</td>

			<td class="f-9" width="40%">
					********** Esta Factura será cancelada en <?php echo $moneda;?> **********
			</td>
				
			<td  style="width: 10%;" class="c-text f-10" width="13%">
				<?php echo $exentaTotal; ?>
			</td>

			<td  style="width: 10%;" class="c-text f-10" width="13%">
				<?php echo $iva5Total; ?>
			</td>

			<td  style="width: 10%;" class="c-text f-10" width="13%">
				<?php echo $iva10Total; ?>
			</td>

		</tr>

	
	</table>

	<table  style="border-bottom: 1px solid;">
			<tr>

			<td class="f-12">
				<b>TOTAL DE FACTURA:</b> &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $total."       ".$moneda;?>
			</td>

			
		</tr>
	</table>

	<table  style="border-bottom: 1px solid;">
		<tr>
			<td class="f-11">
				son <?php echo $moneda."    ".$letraTotal;?> con 0/100 ctvos.
			</td>
		</tr>
	</table>

	<table   style="border-bottom: 1px solid;">
		<tr>
			<td class="f-12" >
				<b>LIQUIDACÍÓN IVA 10%:</b> <span> <?php echo $liquidacionIva10; ?></span>
			</td>	
			
			<td class="f-12">
				<b>LIQUIDACÍÓN IVA 5%:</b> <span> 0,00</span>
			</td>
			
			
			<td class="f-12">
				<b>TOTAL IVA:</b> <span> <?php echo $liquidacionIva10; ?></span>
			</td>
			
		</tr>
		
	</table>


<table style="border:1px solid; margin-top: 20px;">
	<tr>
		<td class="f-9" style="padding: 5px;">
* Cantidad que en la fecha de vencimiento arriba señalada pagaré(mos) a <?php echo $denominacionEmpresa;?> <?php if($idEmpresa == 1){ echo 'en la ciudad de Asunción'; }?>; en su domicilio en <?php echo $direccionEmpresa;?> por igual valor recibido de
conformidad en mercaderiasy/o servicios detallados mas arriba. Caso contrario se procederá por la via ejecutiva al reclamo de su pago.<br>
* La falta de pago de esta factura a su vencimiento establecera la mora en forma automatica, por lo que autorizo(amos) de manera irrevocable para que incluyan mi nombre personal o razon social a la que
represento, a la base de datos de Informconf S.A. conforme a lo establecido en la Ley 1682/01 y su modificatoria 1969/02 como asi tambien para que se pueda proceer la informacion.<br>
* A todos los efectos legales y procesales emeregentes de este documento, las partes aceptan la jurisdiccion y competencia de los jueces y tribunales de la ciudad de Asuncion y declaran prorrogada cualquier otra que pudiera corresponder.<br>
* El unico comprobante de cancelacion de la Factura Credito constituye nuestro Recibo de Dinero Oficial.<br>
* En caso de exigir algun tipo de devolucion, debera contar con la Nota de Credito correspondiente.<br>
* Si la presente operacion fuera abonada con cheque, dejo formal y expresa constancia que si el mismo fuera rechazado por algun motivo, el pago sera considerado nulo, tomandose la obligacion como impaga, liquida y exigible a partir de ese momento.<br>
<br>
RECIBI CONFORME (firma y sello).....................................................................Aclaracion de Firma...............................................................C.I.Nro........................



		</td>
	</tr>
</table>

</div>
{{-- CONTAINER --}}
@if($contadorRecorrido < $corte)
<div style="page-break-after:always;"></div>
@endif

	{{--==============================================================
						FIN DE FACTURA
		==================================================================  --}}



@endforeach








</body>
</html>
