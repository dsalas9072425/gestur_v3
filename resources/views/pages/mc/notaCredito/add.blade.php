@extends('masters')
@section('title', 'Panel de Control')
@section('styles')

	@parent

	 <style type="text/css">
	 	.error{
	 		color:red;
	 	}

		.correcto_col {
        height: 74px;
    	}
		.input-sm{
		font-size: 15px !important;
		}	
		input{
		font-weight: bold !important;
		font-size: 18px;
	}

	label{
		font-weight: bold;
	}
	input.form-control:focus ,.select2-container--focus,button:focus {
        border-color: rgba(82,168,236,.8);
        outline: 0;
        outline: thin dotted \9;
        -moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
        box-shadow: 0 0 8px rgba(82,168,236,.6) !important;
    }
    	


	 </style>
@endsection
@section('content')
    <!-- Main content -->
   
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Generar Nota de Credito</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
				
						@if(!empty($facturas)) 
							@foreach($facturas as $key=>$factura)
							  
					            	<div class="row"> 
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Nro Factura</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$factura->nro_factura}}" readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
												{{-- en el controlador hay una funcion para traer la fecha y mostramos para poder validar a la hora de aplicar nc --}}	
												<input type="hidden" class="form-control input-sm" name="fecha" value="{{date('Y-m-d')}}" readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
												<input type="hidden" class="form-control input-sm" name="factura_vieja" id='factura_vieja' value="{{$factura->id}}" readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
												<input type="hidden" class="form-control input-sm" name="id_tipo_factura" id='id_tipo_factura' value="{{$factura->id_tipo_factura}}" readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
												<input type="hidden" class="form-control input-sm" name="parcial_loca" id='parcial_loca' value="{{$factura->parcial}}" readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
												<label id="refacturacionLabel" style="display: none; color: red;">No se puede aplicar refacturación para facturas parciales</label>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Proforma N°</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$factura->id_proforma}}"  readonly="readonly" style="font-weight: bold;font-size: 15px;"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Cliente</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$factura->cliente->nombre}}" id="inputCliente" readonly="readonly"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Vendedor</label>
												@if(isset($factura->vendedorEmpresa->nombre))
													<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$factura->vendedorEmpresa->nombre}}" readonly="readonly"/>
												@else
													<input type="text" class = "form-control input-sm" name="proforma_id" value="" readonly="readonly"/>
												@endif
											</div>
									    </div>
									 </div>
									<div class="row"> 
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Pasajero Principal</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$pasjeroNombre}}" readonly="readonly"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Destino</label>
												<input type="text" class = "form-control input-sm" name="proforma_id" value="{{$factura->destino ? $factura->destino->desc_destino : ''}}" readonly="readonly"/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Check In - Check Out</label>
												<?php 
													$fecha_periodo1 = date('d/m/Y', strtotime($factura->check_in)); 
													$fecha_periodo2 = date('d/m/Y', strtotime($factura->check_out)); 
													$periodo = $fecha_periodo1." - ".$fecha_periodo2;

												?>

												<input type="text"  class="form-control input-sm"  value="{{$periodo}}"  disabled/>
											</div>
									    </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Moneda de Facturación</label>
												@if(isset($factura->currency->currency_code))
													<input type="text" class = "form-control input-sm" value="{{$factura->currency->currency_code}}" readonly="readonly"/>
												@else
													<input type="text" class = "form-control input-sm"  value="" readonly/>
												@endif	
											</div>
									    </div>
									</div>
									<div class="row">    
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Monto Seña</label>
												<input type="text" class = "form-control numerico" name="senha" id="senha" value="{{$factura->senha}}" disabled/>
											</div>
									    </div>
							            <div class="col-12 col-sm-4 col-md-3">
									        <div class="form-group">
									            <label>Expediente</label>
												<input type="text" class = "form-control" name="senha" id="senha" value="{{$factura->proforma_padre}}" disabled/>
									        </div>
							            </div>
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Estado</label>
												 @if($factura->id_estado_factura == "30")
													<input type="text" disabled class = " form-control input-sm"  value="{{$factura->estado->denominacion}}"  style="font-weight: bold;font-size: 18px; color:red;"/>
												@else
													<input type="text" disabled class = " form-control input-sm"  value="{{$factura->estado->denominacion}}"  style="font-weight: bold;font-size: 18px;"/>
												@endif	
											</div>
									    </div>
									    <div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Vencimiento</label>
									            <div class="input-group">
												    <div class="input-group-addon" style="padding-top: 8px;padding-bottom: 10px;height: 32.992188px;">
												        <i class="fa fa-calendar"></i>
												    </div>
												    @if(date('d/m/Y', strtotime($factura->vencimiento)) !== '31/12/1969')
													    <input type="text" value="{{date('d/m/Y', strtotime($factura->vencimiento))}}" class="form-control pull-right fecha"  disabled >
													@else
													    <input type="text" value="" class="form-control pull-right fecha"  disabled />
													@endif    
												</div>
											</div>
									    </div>
					            	 </div> 
					            	<div class="row">  
										<div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Cotización</label>
												<input type="text" disabled class = "form-control input-sm numerico"  value="{{$factura->cotizacion_factura}}" />
											</div>
									    </div>										
									    <div class="col-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label>Usuario de Facturación</label>
												<input type="text" class = "form-control"  value="{{$factura->usuarioFacturacion ? $factura->usuarioFacturacion->apellido : '' }}, {{$factura->usuarioFacturacion ? $factura->usuarioFacturacion->nombre : '' }}" disabled />
											</div>
									    </div>
							            <div class="col-12 col-sm-4 col-md-3">
									        <div class="form-group">
									            <label>Fecha de Facturación</label>
												    @if(date('d/m/Y', strtotime($factura->fecha_hora_facturacion)) !== '31/12/1969')
													    <input type="text" value="{{date('d/m/Y H:m:i', strtotime($factura->fecha_hora_facturacion))}}" class="form-control pull-right fecha" disabled />
													@endif    
									        </div>
							            </div>
							           
									</div> 
	
			<form  method="get" class="validator-form" action="{{route('doAddNotaCredito')}}" id="frmNotaCredito" name="agencias">
				<input type="hidden" id="opcion" name="opcion" value=""/>
				<input type="hidden" name="factura_id" value="{{$factura->id}}"/>
				<input type="hidden" name="total_factura" value="{{$totalFactura[0]->total}}"/>
					<div class="table-responsive">
						<table id="lista_productos" class="table">
								<thead>
									<tr>
										<th>Cant.</th>
										<th>Producto</th>
										<th>Código</th>
										<th>Proveedor</th>
										<th>Prestador</th>
										<th>Detalle</th>
										<th>Moneda</th>
										<th>Costo</th>
										<th>Tasas</th>
										<th>Com.<br>Agencia</th>
										<th>Venta</th>
										<th>%</th>
										<th>Monto<br>Nota</th>
										<th>Crédito<br>Proveedor</th>
									</tr>
								</thead>
								<tbody >
									@foreach($factura->facturaDetalle as $key=>$detalle)
												@if($detalle->activo == true)
													<tr>
														<td>{{$detalle->cantidad}}</td>

														<td>
															@foreach($producto as $product)
																@if($product->id == $detalle->id_producto)
																	<b>{{$product->denominacion}}</b>
																@endif
															@endforeach
														</td>

														<td>{{$detalle->cod_confirmacion}}</td>

														<td>{{ $detalle->proveedor ? $detalle->proveedor->nombre : '' }}</td>

														<td>{{ $detalle->prestador ? $detalle->prestador->nombre : '' }}</td>

														<td>{{$detalle->descripcion}}</td>

														<td>
															@foreach($currency as $curren)
																@if($curren->currency_id == $detalle->currency_costo_id)
																	<b>{{$curren->currency_code}}</b>
																@endif
															@endforeach
														</td>
														
														<td>
															<?php 
																$costo = $detalle->costo_proveedor + $detalle->costo_gravada;
															?>
															<b>{{number_format($costo,2,",",".")}}</b>
														</td>

														<td>0</td>
														
														<td>{{$detalle->porcentaje_comision_agencia}}</td>
														<td>
															<?php
																$totalDetalle = DB::select('select get_monto_factura_detalle('.$factura->id.','.$detalle->id.')');
																$totalDetalleF = number_format($detalle->saldo,2,",",".");
																			
															?>
															<b>{{$totalDetalle[0]->get_monto_factura_detalle}}</b>
														</td>
														
														<td><b>{{number_format($detalle->markup,2,",",".")}}</b></td>
														
														<td>
															<?php 
																if($factura->id_tipo_facturacion == 1)
																{
																	$monto = $detalle->exento + $detalle->gravadas_10 ;
																}
																else
																{
																	$monto = $detalle->exento + $detalle->comision_agencia + $detalle->gravadas_10 ;
																}
															?>
															<input type="hidden"  name="data[][factura_detalle_id]" value="{{$detalle->id}}"/>
															
															{{-- <input type="hidden"  class="inputMontoNota_{{$detalle->id}} saldo" name="data[][saldoFd]"  value="{{$detalle->saldo}}" /> --}}
															<?php
																$totalDetalleF = number_format($detalle->saldo,2,",",".");
															?>
															<input type="hidden"  class="inputMontoNota_{{$detalle->id}} saldo" name="data[][saldoFd]" value="{{$detalle->saldo}}" />

															<div class="input-group" style="width:230px;">
																<div class="input-group-prepend">
																	<span class="input-group-text">{{$factura->currency['currency_code']}}</span>
																</div>
																<input type="text" class="form-control input-sm numerico sumar inputMontoNota_{{$detalle->id}}" oninput="controlInputMontoNc({{$detalle->id_factura}}, {{$detalle->id}},'inputMontoNota_{{$detalle->id}}',)" name="data[][monto]" data-toggle="tooltip" data-placement="left" title="Saldo Item: {{$totalDetalleF}} {{$factura->currency['currency_code']}}" maxlength="12" value="0" />
															</div>
														
														</td>
														
														<td>
															<div class="input-group" style="width:230px;">
															@if(isset($detalle->libroCompra[0]->currency['currency_code']))
																<div class="input-group-prepend">
																	<span class="input-group-text">{{$detalle->libroCompra[0]->currency['currency_code']}}</span>
																</div>
															@else
																<div class="input-group-prepend">
																	<span class="input-group-text">{{$detalle->currencyVenta->currency_code}}</span>
																</div>
															@endif
      

															@if(isset($detalle->libroCompra[0]->currency['currency_code']) && isset($detalle->libroCompra[0]->saldo))

																<input type="text" class="form-control input-sm numerico inputCreditoProveedor_{{$detalle->id}}" oninput="controlInput(1,'inputCreditoProveedor_{{$detalle->id}}',this.value)" name="data[][credito]" data-toggle="tooltip" data-placement="left" title="Saldo Proveedor : {{number_format($detalle->libroCompra[0]->saldo,2,',','.')}} {{$detalle->libroCompra[0]->currency['currency_code']}}" maxlength="12" value="0" />
															@else
																<input type="text" class="form-control input-sm numerico inputCreditoProveedor_{{$detalle->id}}" oninput="controlInput(1,'inputCreditoProveedor_{{$detalle->id}}',this.value)" name="data[][credito]" data-toggle="tooltip" data-placement="left" title="Saldo Proveedor : {{number_format($detalle->saldo,2,',','.')}} {{$detalle->currencyVenta->currency_code}}" maxlength="12" value="0" />

															@endif
															</div>
															
															<input type="hidden"  name="data[][id_detalle]" value="{{$detalle->id}}" />
															@if(isset($detalle->libroCompra[0]->saldo))

																<input type="hidden" class="inputCreditoProveedor_{{$detalle->id}} saldo" name="data[][saldoLc]"  value="{{$detalle->libroCompra[0]->saldo}}" />
															@else
																<input type="hidden" class="inputCreditoProveedor_{{$detalle->id}} saldo" name="data[][saldoLc]"  value="{{$detalle->saldo}}" />
															@endif
														</td>
													</tr>	
												@endif	
										{{--	@else
												<!--<tr>
													<td colspan="14" style="text-align:center">
														<b>Falta relación de libro compra, póngase en contacto con soporte técnico.</b>
													</td>
												</tr>-->

											@endif	--}}
									@endforeach 
									<tr>
										<td></td>
										<td><b style="font-weight: bold;font-size: 15px;">TOTAL</b></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td>
											<input type="text" class = " form-control input-sm numerico" name="total" id="totalizar" maxlength="14" style="width: 100%; min-width: 130px;" value="0" readonly/></td>
										<td>
										</td>
									</tr> 
								</tbody>	
							</table>
						</div> 
						<br>
						<div class="row">
							<div class="col-12 col-md-6"></div>
							<div class="col-12 col-md-2">
							<!--<button type="button" class="btn btn-info text-center btn transaction_normal hide-small normal-button notaCreditoStyleRefacturacion " style="background: #FF8000 !important;width: 100%;height: 54px; min-width: 130px;" value="Guardar" id="btnRefinanciacion" onclick="mostrarModalCliente()"><b>Generar NC Refacturación</b></button>-->
							</div>
							<div class="col-12 col-md-2">
								<button type="button" class="btn btn-info text-center btn transaction_normal hide-small normal-button notaCreditoStyle" style="background: #e2076a !important;width: 100%;height: 54px; min-width: 130px;" value="Guardar" id="btnDevolucion"><b>Generar NC Devolución</b></button>
							</div>
							<div class="col-12 col-md-2">	
								<button type="button" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="background: #16d39a !important;width: 100%;height: 54px; min-width: 130px;" value="Guardar" id="btnNCredito"><b>Generar NC Documento</b></button>
							</div>				
						</div> 
						<br>				
					</form>	

									<div class="row mt-1">
						         

						         
						            		<div class="col-12 col-sm-4 col-md-2" >
												<div class="form-group">
													<label style="color: red;">TOTAL_FACTURA</label>
														<input type="text" class = "Requerido form-control" name="total_factura" id="total_factura" style="font-weight: bold;font-size: 15px;" value='{{number_format($totalFactura[0]->total,2,",",".")}}' disabled="disabled"/>
												</div>
										    </div>


											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>TOTAL_BRUTO</label>
														<input type="text" class = "Requerido form-control" name="total_bruto" id="total_bruto" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_bruto_factura,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>TOTAL_NETO</label>		
														<input type="text" class = "Requerido form-control" name="total_neto" id="total_neto" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_neto_factura,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>


									
											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>EXENTAS</label>
														<input type="text" class = "Requerido form-control" name="total_bruto" id="total_exentas" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_exentas,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>GRAVADAS</label>
													<input type="text" class = "Requerido form-control" name="total_iva" id="total_gravadas" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_gravadas,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>IVA</label>
													<input type="text" class = "Requerido form-control" name="total_iva" id="total_iva" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_iva,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>COSTOS</label>
													<input type="text" class = "Requerido form-control" name="total_costo" id="total_costo" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_costos,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>


								
											<div class="col-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label>COMISIÓN</label>
													<input type="text" class = "Requerido form-control" name="total_comision" id="total_comision" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->proforma->total_comision,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2" >
												<div class="form-group">
													<label>NO_COMISIONA</label>
													<input type="text" class = "Requerido form-control" name="total_comision" id="total_no_comisiona" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->total_no_comisiona,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>


											<div class="col-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label>MARKUP</label>
													<input type="text" class = "Requerido form-control" name="markupF" id="markupF" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->markup,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>
										

											<div class="col-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label>% GANANCIA</label>
													<input type="text" class = "Requerido form-control" name="total_costo" id="porcentaje_ganancia" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->porcentaje_ganancia,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>GANANCIA</label>
													<input type="text" class = "Requerido form-control" name="senh" id="ganancia_venta" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->ganancia_venta,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label>INCENTIVO</label>
													<input type="text" class = "Requerido form-control" name="incentivo" id="incentivo" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->incentivo_vendedor_agencia,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

											<div class="col-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label>RENTA</label>
													<input type="text" class = "Requerido form-control" name="incentivo" id="renta" style="font-weight: bold;font-size: 15px;" value='{{number_format($factura->renta,2,",",".")}}' disabled="disabled" tabindex="15"/>
												</div>
										    </div>

									
									</div>	
							 
							@endforeach	
						@endif	 

				</div>
            </div>
        </div>    	
    </div>      
	<div class="modal fade" id="modalCliente" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		  <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			  <h4 class="modal-title" id="" style="font-weight: 800;">Refacturacion<i style="display:none;" class="fa fa-refresh fa-spin load_err"></i></h4>
			  <span class="msj_err" style="color:#DB6042;"></span>
			  <span class="msj_success" style="color:#489946;"></span>
			</div>
			<div class="modal-body">
			  <form id="valoracion_form" method="post">
	  
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Cliente</label>
							<select class="form-control" name="cliente_id"  id="cliente" tabindex="1" style="width: 100%;" >
										<option value="">Todos</option>
							</select>
						</div>
					</div>	
				</div>
				<input type="hidden" name="id_proforma" id='id_proforma' value="{{$factura->id_proforma}}">
			  </form>
			</div>
			<div class="modal-footer">
			    <button type="button" class="btn btn-success" id="btnConfirmar">Confirmar Refacturacion</button>
				<!-- Agrega el botón de cancelación para cerrar el modal -->
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			</div>
		  </div>
		</div>
	</div>
	  

 </section>


  
 @endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script defer type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script>
		function mostrarModalCliente() {
			$('#modalCliente').modal('show');
		}

$(document).ready(function() {
      	function actualizarMenuDesplegableConEntrada() {
        //var clienteNombre = $("#inputCliente").val();

		var clienteNombre = '{{$factura->id_cliente}}';
        // Borra las opciones anteriores y agrega la nueva con el nombre del cliente
        //$("#cliente").empty().append($("<option>").val(clienteNombre).text(clienteNombre));
		$("#cliente").empty().append($("<option>").val(clienteNombre).text($("#inputCliente").val()));
    }

    $("#btnConfirmar").on("click", function() {
        // Antes de mostrar el modal, actualiza el menú desplegable con el valor del campo de entrada
        actualizarMenuDesplegableConEntrada();

        // Muestra el modal
        $('#modalCliente').modal('show');
    });

	 // Evento que se dispara cuando el modal se muestra por completo
	 $('#modalCliente').on('shown.bs.modal', function () {
        actualizarMenuDesplegableConEntrada(); // Actualiza el menú desplegable con el valor del cliente
    });


});
//arturo
$('#btnRefinanciacion').click(() => {
    $('#opcion').val(3);
    $('#modalCliente').modal('show');
  });

  // Agrega la siguiente función para manejar el clic en el botón de confirmación dentro del modal
  $('#btnConfirmar').click(() => {
    guardar(); // Realiza la función que hacías en la confirmación anterior
    $('#modalCliente').modal('hide'); // Cierra el modal
  });

//control para ver si es contado no puede generar nota de credito solo refacturacion
if($('#id_tipo_factura').val() == '2'){
	$(".notaCreditoStyle").hide();
}
if($('#parcial_loca').val() == '1'){ //se aplica para facturas parciales
	$(".notaCreditoStyleRefacturacion").hide();
}

$(document).ready(function() {//se agrega para un mensaje que si tiene una factura parcial no podra refacturar
    var parcialLocaValue = $('#parcial_loca').val();

    if (parcialLocaValue == '1') {
        $('#refacturacionLabel').show();
    }
});
			// DEFINE EL IDIOMA ESPAÑOL A TODOS LOS SELECT2
			var lang_es_select2 = {
				errorLoading: function () {
					return 'La carga falló';
				},
				inputTooLong: function (args) {
					var overChars = args.input.length - args.maximum;
					var message = 'Por favor elimine ' + overChars + ' car';
					if (overChars == 1) {
						message += 'ácter';
					} else {
						message += 'acteres';
					}
					return message;
				},
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					var message = 'Por favor ingrese ' + remainingChars + ' o más caracteres';
					return message;
				},
				loadingMore: function () {
					return 'Cargando más resultados…';
				},
				maximumSelected: function (args) {
					var message = 'Sólo puede seleccionar ' + args.maximum + ' opción';
					if (args.maximum == 1) {
						message += '';
					} else {
						message += 'es';
					}
					return message;
				},
				noResults: function () {
					return 'No se encontraron resultados';
				},
				searching: function () {
					return 'Buscando…';
				},
				removeAllItems: function () {
					return 'Eliminar todas las opciones';
				}
			};

	
	$(document).ready(function() {
		$(".select2").select2();
    
	$("#cliente").select2({
				language: lang_es_select2,
		        ajax: {
		                url: "{{route('get.clientes')}}",
		                dataType: 'json',
		                placeholder: "TODOS",
		                delay: 0,
		                data: function (params) {
		                            return {
		                                q: params.term, // search term
		                                page: params.page
		                                    };
		                },
		                cache: true
		                },
		                escapeMarkup: function (markup) {
		                                return markup;
		                }, // let our custom formatter work
		                minimumInputLength: 3,
	});

});
   


		$(()=>{
			formatNumber();
			revisarDatos();
			$('[data-toggle="tooltip"]').tooltip();
		});

		const operacion = {
			valid: false,
			msjErr: []
		}

		$('#btnDevolucion').click(()=>{
			$('#opcion').val(1);
			return swal({
                        title: "GESTUR",
                        text: " ¿Está seguro que desea generar la Nota de Crédito? Este proceso va a generar una OP a favor del cliente para la devolución del monto abonado.",
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, Generar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
										guardar();
								}else {
                            		swal("Cancelado", "", "error");
                       			}
							});
		});

		$('#btnNCredito').click(()=>{
			$('#opcion').val(2);
			mensaje = "¿Está seguro que desea generar la Nota de Crédito?<br> Este proceso va a generar una Nota de Crédito, según saldo de la factura:<br>";
			mensaje += "1- NC de cancelación de factura (factura no pagada) o<br>";
			mensaje += "2- NC con saldo a favor que podrá aplicarse a otra factura del mismo cliente (factura pagada)";

			var span = document.createElement("span");
			span.innerHTML = mensaje;
			// Crear el elemento select2 dentro del SweetAlert
			//const selectElement = $('<select id="cliente" class="select2" style="width: 100%;"></select>');
			return swal({
                        title: "GESTUR",
                        content: span,
						//content: mensaje,
                        showCancelButton: true,
                        buttons: {
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, Generar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    }
                                }
                            }).then(isConfirm => {
                                if (isConfirm) {
										guardar();
								}else {
                            		swal("Cancelado", "", "error");
                       			}
							});

		});

	/*	$('#btnRefinanciacion').click(()=>{
			$('#opcion').val(3);
			return swal({
                        title: "GESTUR",
						//content: mensaje,
                        text: " ¿Esta seguro de que esto es una refacturacion?. Los tickets y servicios online serán liberados.",
                        showCancelButton: true,
                        buttons: {
							CambiarCliente:{
										text: "Cambiar Cliente",
										value: "cliente",
										visible: true,
										className: "btn-secondary",
										closeModal: false,
									},	
                                cancel: {
                                        text: "No",
                                        value: null,
                                        visible: true,
                                        className: "btn-warning",
                                        closeModal: false,
                                },
                                confirm: {
                                        text: "Sí, Generar",
                                        value: true,
                                        visible: true,
                                        className: "",
                                        closeModal: false
                                    },
								
                                }
                            }).then(isConfirm => {
								if (isConfirm) {
									guardar();
									} else {
									if (isConfirm === "cliente") { // Verificar si se hizo clic en el botón personalizado "CambiarCliente
											$('#modalCliente').modal('show');
									swal("Haz clic en el botón 'Cambiar Cliente'", "", "info");
									} else {
									swal("Cancelado", "", "error");
									}
								}
							});
		});
*/
		function guardar(){
			if(validarNota()){
				sendData();
			} else {
				$.toast().reset('all');	
				mensaje ="";
				$.each(operacion.msjErr, function (key, item){
					if(key == 0){
						mensaje+= item		
					}else{
						mensaje+= ","+item
					}
				})	
				swal("Cancelado",mensaje, "error");
			/*	$.toast({
						    heading: 'Error',
						    text: operacion.msjErr,
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});*/

			operacion.msjErr = [];			
			}
		}

		function controlInputMontoNc(id_factura, id_factura_detalle, clase)
		{
			if(parseFloat($('.inputMontoNota_'+id_factura_detalle).val()) != 0){
				$.ajax({
							type: "GET",
							url: "{{route('controlarSaldoDisponibleNc')}}",
							dataType: 'json',
							data: {id_factura: id_factura, id_factura_detalle: id_factura_detalle},
							error: function(jqXHR,textStatus,errorThrown)
							{
	                        },
							success: function(rsp)
							{
								var diferencia = parseFloat(rsp.total_detalle) - parseFloat(rsp.monto_nc);

								if ( diferencia <= 0 ) 
								{
									$('.'+clase).css('border','1px solid red');
									$('.'+clase).prop("disabled", true);
									$.toast().reset('all');	
									$.toast({
												    heading: 'Error',
												    text: 'Ya no se puede generar nota de crédito. Ha superado el límite disponible.',
												    position: 'top-right',
												    showHideTransition: 'fade',
												    icon: 'error'
									});
								}
								else
								{
									$('.'+clase).css('border-color','#d2d6de');

								}
							}	
						})
			}else{
				$('.'+clase).css('border','1px solid red');
				$('.'+clase).prop("disabled", true);
			}


		}


		function controlInput(opcion,clase,valor)
		{
			let monto = clean_num($('.'+clase).val());
			let saldo = clean_num($('.'+clase+'.saldo').val(),true);
			if(monto > saldo){
				$('.'+clase).css('border','1px solid red');
				
				$.toast().reset('all');	
				$.toast({
						    heading: 'Error',
						    text: 'El monto ingresado supera el saldo disponible.',
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});
					
			} else {
				$('.'+clase).css('border-color','#D2D6DE');
			}
			
		}

		function clean_num(n,bd=false){

				if(n && bd == false){ 
				n = n.replace(/[,.]/g,function (m) {  
										if(m === '.'){
											return '';
										} 
										if(m === ','){
											return '.';
										} 
								});
				return Number(n);
				}
				if(bd){
				return Number(n);
				}
				return 0;

		}//

	


			$(".sumar").keyup(function(){
			
				var sum = 0;
				let val = 0;
				$('.sumar').each(function(index,value){
					val = clean_num($(value).val());
					if(Boolean(val)){
						sum += val;
					}
					
				});

				if(isNaN(sum)){
					$('#totalizar').val(0);
				}else{
					total =sum
					$('#totalizar').val(sum.toFixed(2));
				}	 
			})	
			function validarNota()
			{

					formNotaCredito = $('#frmNotaCredito').serializeJSON();
					let ok = 0;
					$.each(formNotaCredito.data,(index,value) => 
					{	
						console.log(value);


						if(clean_num(value.monto) > clean_num(value.saldoFd,true)){
							operacion.msjErr.push('El monto del ítem factura supera el saldo.');
							ok++	
							return false;	
						} 

						console.log('credito      .'+clean_num(value.credito));
						console.log('Saldo Lc     .' +clean_num(value.saldoLc,true));
						console.log((clean_num(value.credito) >= clean_num(value.saldoLc,true)));

						if(clean_num(value.credito) > clean_num(value.saldoLc,true)){
							operacion.msjErr.push('El monto de crédito proveedor supera el saldo disponible.');
							ok++	
							return false;
							
						} 
							//arturo 
					if ($('#opcion').val() === '3') {
						if(clean_num(formNotaCredito.total) < clean_num(formNotaCredito.total_factura,true)){
						operacion.msjErr.push('El total de debe de ser igual al monto para la refacturacion');
						ok++;
					}
					if(clean_num(value.saldoFd,true) < clean_num(value.monto)){
							operacion.msjErr.push('El monto del ítem debe de ser igual al saldo .');
							ok++	
							return false;	
						} 
					
					}


					});


					if(clean_num(formNotaCredito.total) > clean_num(formNotaCredito.total_factura,true)){
						operacion.msjErr.push('El total de la nota de crédito supera el total de la factura.');
						ok++;
					}

					if(clean_num(formNotaCredito.total) <= 0){
						operacion.msjErr.push('El total de la nota de crédito no puede ser cero.');
						ok++;
						
					}


				

				if(ok !=0)
				{
					return false;	
				}	
				return true;	
				
			}


			function sendData() {

				let dataString = $('#frmNotaCredito').serializeJSON({ 
                    customTypes: customTypesSerializeJSON
				});
				dataString.cliente_id=$('#cliente').val();
				dataString.id_proforma=$('#id_proforma').val();
				dataString.factura_vieja=$('#factura_vieja').val();

				$('#btnDevolucion').prop("disabled", true);
				$('#btnNCredito').prop("disabled", true);
				procesarndo();
				
				$.ajax({
						type: "GET",
						url: "{{route('doAddNotaCredito')}}",
						dataType: 'json',
						data: dataString,
						 error: function(jqXHR,textStatus,errorThrown){
								$.unblockUI();
								swal("Cancelado", 'Ocurrio un error en la comunicación con el servidor, vuelva a intentarlo ', "error");
                            },
						success: function(rsp) {
							$.unblockUI();
							console.log(rsp);
							if (rsp.status == 'OK') {
								swal({
								title: "Éxito",
								text: rsp.mensaje,
								icon: "success",
								buttons: {
									ok: {
									text: "OK",
									value: true
									}
								}
								}).then((value) => {
									if (value) {
												$.ajax({
														type: "GET",
														url: "{{route('envioNC')}}",
														dataType: 'json',
														data: {
																idNC:rsp.id,
															},
														error: function(jqXHR,textStatus,errorThrown){},
														success: function(rsp){}
													})
													$('.numerico').attr('disabled', true);
													$('#Guardar').attr('disabled', true);
													$('#btnDevolucion').prop("disabled", false);
													$('#btnNCredito').prop("disabled", false);
													setTimeout(function() {
																			redireccionar(rsp.id);
																		}, 10000); // Retardo de 30000 milisegundos (30 segundos)
												}
								});
							} else {
								swal("Cancelado", rsp.mensaje, "error");
							}
}
						})
		    }//


			function procesarndo(){
			$.blockUI({
                    centerY: 0,
                    message: "<h2>Procesando...</h2>",
                    css: {
                        color: '#000'
                        }
                    });
		}

		function redireccionar(idNC){
			location.href = "../verNota/" + idNC;

		}

		function justNumbers(e){
	        var keynum = window.event ? window.event.keyCode : e.which;
	        if ((keynum == 8) || (keynum == 46))
	        return true;
	         
	        return /\d/.test(String.fromCharCode(keynum));
        }

	function formatNumber(){
		$('.numerico').inputmask("numeric", {
		    radixPoint: ",",
			groupSeparator: ".",
			digits: 2,
			autoGroup: true,
			 // prefix: '$', //No Space, this will truncate the first character
			rightAlign: false,
			// oncleared: function () { self.Value(''); }
		});
	}	


	function revisarDatos(opcion,clase,valor){
		@foreach($facturaDetalles as $detalle)
			controlInputMontoNc('{{$detalle->id_factura}}', '{{$detalle->id}}', 'inputMontoNota_'+'{{$detalle->id}}');
		@endforeach	

	}

	


	</script>
@endsection