@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
    <div style="margin-left: 1%; margin-right: 2%;">
    	@include('flash::message')
        <br>
        <div id="warning"></div>
        <h1>Facturas</h1>
        <br>
        <div class="block sectiontop">
            @include('flash::message')
            <div class="tab-pane fade in active" id="mis-reservas">
                <br>
                <form action="" id="frmBusqueda" method="post"> 
                    <div class="row">
                        <div class="form-group col-xs-12 col-sm-10 col-md-10">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label>Desde</label>
                                    <div class="input-group date">
								        <div class="input-group-addon">
								            <i class="fa fa-calendar"></i>
								        </div>
								        <input type="text" name="desde" id="desde" class="form-control pull-right">
								    </div> 	
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label>Hasta</label>
                                    <div class="input-group date">
								        <div class="input-group-addon">
								            <i class="fa fa-calendar"></i>
								        </div>
								        <input type="text" name="hasta" id="hasta" class="form-control pull-right">
								    </div> 	
                                </div>
								<!--<div class="col-xs-12 col-sm-3 col-md-3">
									<label>Agencia</label>
									<select class="form-control select2" id="agencia" style="width: 100%;" name="agencia_id">
										<option value="0">Seleccione Agencia</option>
											@foreach($listadoAgencia as $key=>$agencia)
												<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
											@endforeach
									</select>
                                </div>-->
								<div class="col-xs-12 col-sm-4 col-md-4">
									<label>Vendedor</label>
									<select class="form-control select2" id="usuario" style="width: 100%;" name="vendedor_id">
										<option value="0">Seleccione Usuario</option>
										@foreach($listadoUsuarios as $key=>$usuarios)
											<option value="{{$usuarios['value']}}"><b>{{$usuarios['labelUsuario']}}</b> - {{$usuarios['labelApellidoNombre']}}<br/></option> 
										@endforeach
									</select>
                                </div>
                            </div>
                    	</div>
                    	<div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                            <div class="col-md-12">
                                <button type="button" style="width: 160px; margin-bottom: 10px; background-color: #e2076a; height: 40px;" id= "doConsultaFactura" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Buscarx Facturas</button>
                            </div>
                            <div class="col-md-12">
                                <label class="hidden-xs">&nbsp;</label>
                                <label class="hidden-xs">&nbsp;</label>
                                <button id="limpiarFiltros" style="width: 160px;  background-color: #fdb714; height: 40px;" type="reset" class="btn btn-info"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                            </div>
                        </div>
					</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12" id="divFacturas" >
							<div class="table-responsive">
								<br>
								<table id="facturas" class="table table-bordered table-hover">
									<thead>
										<tr class="bgblue">
											<th>Fecha</th>
											<th>Descargar</th>
											<th>Factura</th>
											<th>Pasajero</th>
											<!--<th style="background-color: #2d3e52;">Cliente</th>-->
											<th>Destino</th>
											<th>Total</th>
											<th>Vendedor</th>
											<th>Estado</th>
										</tr>
									</thead>
										<tbody>
										</tbody>
								</table>
							</div>
                        </div>
                    </div>
					<div class="row" id="totalReservas">
					</div>
                </form>
            </div>
        </div>
    </div>
@endsection 
@section('scripts')
    @parent
	@include('layouts/gestion/scripts')
	<script>
		$("#desde").datepicker();
		$("#hasta").datepicker();
		$(document).ready(function() {
			$('#agencia option[value="{{$agency[0]->idAgencia}}"]').attr("selected", "selected");
			$('#agencia').trigger("chosen:updated");
			
			//$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
			$( ".datepicker" ).datepicker( "option", "dateFormat", 'dd/mm/yy' );
			//$("#desde").datepicker("setDate",$("#desde").datepicker("getDate","+1d"));
			
			$("#hasta").datepicker("setDate", new Date());
            var date2 = $('#hasta').datepicker('getDate');
            date2.setDate(date2.getDate()-31);
            $('#desde').datepicker('setDate', date2);
			
			getButton();
			
			$("#facturas").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
			$("#doConsultaFactura").trigger('click');
			$("#limpiarFiltros").click(function(){
				//$("#desde").val("");
				//$("#desde").datepicker("setDate", new Date());
				
				$("#hasta").val("");
				$("#agencia").val(0);
				$('#agencia').trigger("chosen:updated");
				$("#usuario").val(0);
				$('#usuario').trigger("chosen:updated");
			});	
			$("#botonExcel").on("click", function(e){
				e.preventDefault();
    			$('#frmBusqueda').attr('action', "{{route('mc.generarExcelFactura')}}").submit();
			});
		});	

		function getButton(){
			$("#doConsultaFactura").click(function(){
				var dataString = $("#frmBusqueda").serialize();
				/*$.blockUI({
		                centerY: 0,
		                message: '<div class="loadingC"><img style="width: 160px; height: 160px;" src="images/loading.gif" alt="Loading" /><br><h2>Procesando..... </h2></div>',
		                css: {
		                    color: '#000'
		                    }
		               });*/
				$.ajax({
					type: "GET",
					url: "{{route('mc.doConsultaFactura')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
									//$.unblockUI();
									console.log(rsp);
									var oSettings = $('#facturas').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();

									for (i=0;i<=iTotalRecords;i++) {
										$('#facturas').dataTable().fnDeleteRow(0,null,true);
									}

									$.each(rsp, function (key, item){
										var botonvoucher = "<a href="+item.url_vaucher+" target='_blank'><i class='fa fa-file-pdf-o fa-lg' style='   color: red;'></i>  Voucher</a></br>";
										var botonfactura = "<a href="+item.url_factura+" target='_blank'><i class='fa fa-file-text-o fa-lg' style='   color: blue;'></i>  Factura</a>";
										var accion = botonvoucher +" "+botonfactura;
										var dataTableRow = [
															item.factura_fecha,
															accion,
															item.factura_numero,
															//item.ruc_cliente,
															//item.cliente_id,
															item.nombre_cliente,
															//item.factura_moneda,
															item.factura_destinacion,
															item.factura_total,
															item.factura_vendedor,
															item.estado
															];
										var newrow = $('#facturas').dataTable().fnAddData(dataTableRow);
										// set class attribute for the newly added row 
										var nTr = $('#facturas').dataTable().fnSettings().aoData[newrow[0]].nTr;
										// and parse the row:
										var nTds = $('td', nTr);
									 });
								},
							});	
					});
		}

	</script>
@endsection