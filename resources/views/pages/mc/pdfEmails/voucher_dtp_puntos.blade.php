<!DOCTYPE html>
<html>
<head>
	<title>Maqueta HTML con tablas</title>
	<style>
        * {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		}

        body  {
            border: 2px solid black;
            padding: 10px;
        }

		table {
			border-collapse: collapse;
			width: 100%;
			margin-bottom: 20px;
		}
		th, td {
			text-align: left;
			padding: 8px;
			border-bottom: 1px solid #ddd;
		}
		th {
			background-color: #f2f2f2;
		}
		.header {
			font-weight: bold;
			padding: 10px;
			margin-bottom: 20px;
		}
	</style>
</head>
<body>
	<div class="header">
		<table>
            <tr>
                <td colspan="2" class="r-text">
                    <br>
                    <img src="{{$empresa_logo}}" style="width: 120px;height: 45px;">
                </td>
            </tr>
			<tr>
				<td>Nombre Vendedor:</td>
				<td>{{$liquidacion->nombre}}</td>
			</tr>

			{{-- SI NO ES UN SALDO MOSTRAMOS LOS PUNTOS --}}
			@if(!$saldo)
				<tr>
					<td>Meta Puntos:</td>
					<td>{{  number_format($liquidacion->meta,0,",",".")}}</td>
				</tr>
				<tr>
					<td>Puntos Alcanzados:</td>
					<td>{{ number_format($liquidacion->total_puntos_alcanzados,2,",",".")}}</td>
				</tr>
				
				<tr>
					<td>Total a Cobrar:</td>
					<td>$ {{ number_format($liquidacion->total_cobrar,2,",",".") }}</td>
				</tr>

			@else 
				<tr>
					<td>Total Saldo Anterior a Cobrar:</td>
					<td>$ {{ number_format($liquidacion->total_cobrar,2,",",".") }}</td>
				</tr>

			@endif
			<tr>
				<td>Periodo:</td>
				<td>{{$mes}}-{{ $anho}}</td>
			</tr>
		</table>
	</div>
	<table style="font-size: 10px;">
		<thead>
			<tr>
				<th>Nro Factura</th>
				<th>Tipo</th>
				<th>Pasajero</th>
				<th>Estado</th>
				<th>Promoción</th>
				<th>Puntos</th>
				<th>Periodo Factura</th>
				<th>USD</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($detalles as $item)
				<tr>
					<td>{{ $item->nro_factura }}</td>
					<td>{{ $item->tipo }}</td>
					<td>{{ $item->pasajero_principal }}</td>
					<td>{{ $item->estado }}</td>
					<td>{{ $item->promocion }}</td>
					<td>{{ $item->puntos }}</td>
					<td>{{ $item->mes_anho }}</td>
					<td>$ {{ number_format($item->monto,2,",",".") }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>