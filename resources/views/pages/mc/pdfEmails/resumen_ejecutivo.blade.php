<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Detalle Cierre</title>


	<style type="text/css">
		* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			margin: 0;
			padding: 0;
		}

		.container {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 90%;
			margin: 0 auto;
			z-index: 1;
		}

		table {
			width: 100%;

		}

		.b-buttom {
			border-bottom: 1px solid;
		}

		.b-top {
			border-top: 1px solid;
		}

		.b-col {
			border-collapse: collapse;
		}

		.n-1 {
			font-weight: 700;
		}

		.text {
			overflow: hidden;
			/*text-overflow: ellipsis;*/
			white-space: nowrap;
			display: block;
			width: 100%;
			min-width: 1px;
		}

		.f-10 {
			font-size: 10px !important;
		}

		.f-9 {
			font-size: 9px !important;
		}

		.f-11 {
			font-size: 11px !important;
		}

		.f-12 {
			font-size: 12px !important;
		}

		.f-15 {
			font-size: 15px !important;
		}

		.f-17 {
			font-size: 17px !important;
		}

		.c-text {
			text-align: center;
		}

		.r-text {
			text-align: left;
		}

		.r-text-detalle {
			margin-right: 20px;
		}

		.cabecera {}

		.espacio-10 {
			margin-top: 10px;
		}

		.b-buttom {
			border-bottom: 1px solid;
		}

		#background {
			/*margin-top:100px;*/
			position: absolute;
			z-index: 0;
			background: white;
			display: block;
			min-height: 50%;
			min-width: 50%;
			color: yellow;
		}

		#bg-text {
			color: lightgrey;
			font-size: 120px;
			transform: rotate(300deg);
			-webkit-transform: rotate(300deg);
		}

		.hidden {
			display: none;
		}
	</style>
</head>

<body style="page-break-inside: avoid;">
	<br>
	<br>
	<div class="container espacio-10">
		<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
			<br>
			<table style="page-break-inside: auto;">
				<tr>
					<td colspan="6" class="r-text">
						<br>
						<img src="{{$empresa_logo}}" style="width: 120px;height: 45px;">
					</td>
				</tr>
				<tr>
					<td colspan="6" class="c-text f-17">
						<b>CUENTAS BANCARIAS</b>
					</td>
				</tr>
				</tr>
			</table>
			<br>
			<table border=1 cellspacing=0 cellpadding=2 style="width: 680px;margin-left: 25px;" class="f-11">
				<tr style="background-color: lightgray;">
					<th class="c-text">Cuenta de Fondo</th>
					<th class="c-text">Nro. Cuenta Fondo</th>
					<th class="c-text">Moneda</th>
					<th class="c-text">Saldo</th>
					<th class="c-text">Tipo</th>
				</tr>
				@foreach($bancos_2 as $banco)
					<tr>
						<td class="n-1">{{$banco->banco_n}}</td>
						<td class="n-1 c-text">{{$banco->numero_cuenta}}</td>
						<td class="n-1 c-text">{{$banco->currency_code}}</td>
						<td class="n-1 c-text">{{$banco->currency_code != 'PYG' ? number_format($banco->saldo_detalle, 2, ",", ".") : number_format($banco->saldo_detalle,0,",",".") }}</td>
						<td class="n-1 c-text">{{$banco->tipo_cuenta_banco_n}}</td>
					</tr>
				@endforeach

				<tr>
					<td colspan="5">&nbsp;&nbsp;&nbsp;</td>
				</tr>

				@foreach($bancos_2_1 as $banco)
				<tr>
					<td class="">{{$banco->banco_n}}</td>
					<td class="c-text">{{$banco->numero_cuenta}}</td>
					<td class="c-text">{{$banco->currency_code}}</td>
					<td class="c-text">{{$banco->currency_code != 'PYG' ? number_format($banco->saldo_detalle, 2, ",", ".") : number_format($banco->saldo_detalle,0,",",".") }}</td>
					<td class="c-text">{{$banco->tipo_cuenta_banco_n}}</td>
				</tr>
			
				@endforeach
			</table>
			<br>
			<br>
			<br>

		</div>
	</div>
	<br>
	<br>
	<br>
	<div class="container espacio-10">
		<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
			<br>
			<table>
				<tr>
					<td colspan="6" class="c-text f-17">
						<b>INGRESOS</b>
					</td>
				</tr>
			</table>
			<br>
			<br>
			

			<table border=1 cellspacing=0 cellpadding=2 style="width: 680px;margin-left: 25px;" class="f-11">
				<tr style="background-color: rgb(223, 236, 215);">
					<th class="c-text" colspan="4">
						TOTAL COBRADO EN VALORES &nbsp;&nbsp;&nbsp;&nbsp;

						@foreach($total_ingreso_valores as $data_ingreso_moneda)
							@if($data_ingreso_moneda->currency_code == 'PYG')
							{{$data_ingreso_moneda->currency_code}} : {{number_format($data_ingreso_moneda->total,0,",",".")}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							@else
							{{$data_ingreso_moneda->currency_code}} : {{number_format($data_ingreso_moneda->total,2,",",".")}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							@endif
						@endforeach
					</th>
				</tr>
				<tr style="background-color: lightgray;">
					<th class="c-text">
						Forma de Cobro
					</th>
					<th class="c-text">
						Moneda
					</th>
					<th class="c-text">
						Monto
					</th>
					<th class="c-text">
						Tipo
					</th>
				</tr>
				@foreach($ingresos_valores as $ingreso)
					<tr>
						<td class="n-1">
							{{$ingreso->forma_cobro}}
						</td>
						<td class="n-1 c-text">
							{{$ingreso->currency_code}}
						</td>
						<td class="n-1 c-text">
							{{number_format($ingreso->total,2,",",".")}}
						</td>
						<td class="n-1 c-text">
							{{$ingreso->tipo}}
						</td>
					</tr>
				@endforeach
				<tr>
					<td colspan="4">&nbsp;&nbsp;&nbsp;</td>
				</tr>
				@foreach($ingresos_documento as $ingreso)
					<tr>
						<td class="">
							{{$ingreso->forma_cobro}}
						</td>
						<td class="c-text">
							{{$ingreso->currency_code}}
						</td>
						<td class="c-text">
							{{number_format($ingreso->total,2,",",".")}}
						</td>
						<td class="c-text">
							{{$ingreso->tipo}}
						</td>
					</tr>
				@endforeach
			</table>
			<br>
			<br>

		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="container espacio-10">
		<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
			<br>
			<table style="page-break-inside: auto;">
				<tr>
					<td colspan="6" class="c-text f-17">
						<b>CIERRES DE CAJA</b>
					</td>
				</tr>
			</table>
			<br>
			<br>
			<table border=1 cellspacing=0 cellpadding=2 style="width: 680px;margin-left: 25px;" class="f-11">
				<tr style="background-color: lightgray;">
					<th class="c-text">
						Fecha
					</th>
					<th class="c-text">
						Total GS
					</th>
					<th class="c-text">
						Total US
					</th>
					<th class="c-text">
						Usuario
					</th>
				</tr>
				@foreach($cierreCajas as $cierreCaja)
				<tr>
					<td class="c-text">
						{{date('d/m/Y H:m:i', strtotime($cierreCaja->fecha_creacion))}}
					</td>
					<td class="c-text">
						{{number_format($cierreCaja->total_gs,0,",",".")}}
					</td>
					<td class="c-text">
						{{number_format($cierreCaja->total_us,2,",",".")}}
					</td>
					<td class="c-text">
						{{$cierreCaja->nombre}} {{$cierreCaja->apellido}}
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<table border=1 cellspacing=0 cellpadding=2 class="f-11">
							<tr style="background-color: lightgray;">
								<th class="r-text">
									Forma de Cobro
								</th>
								<th class="c-text">
									Total GS
								</th>
								<th class="c-text">
									Total US
								</th>
							</tr>
							@foreach($cierreCaja->detalles as $cierreCajaDetalle)
							<tr>
								<td class="r-text">
									{{$cierreCajaDetalle->denominacion}}
								</td>
								<td class="c-text">
									{{$cierreCajaDetalle->currency_code}}
								</td>
								<td class="c-text">
									{{ $cierreCajaDetalle->moneda_id != 111 ? number_format($cierreCajaDetalle->total, 2, ",", ".") : number_format($cierreCajaDetalle->total,0,",",".") }}
								</td>
							</tr>
							@endforeach
						</table>

					</td>
				</tr>
				@endforeach
			</table>
			<br>
			<br>

		</div>
	</div>
	<br>
	<br>
	<br>
	<div class="container espacio-10">
		<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
			<br>
			<table>
				<tr>
					<td colspan="6" class="c-text f-17">
						<b>EGRESOS</b>
					</td>
				</tr>
			</table>
			<br>
			<br>
			<table border=1 cellspacing=0 cellpadding=2 style="width: 680px;margin-left: 25px;" class="f-11">
				<tr style="background-color: rgb(241, 208, 204);">
					<th class="c-text" colspan="4">
						TOTAL DE PAGOS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						@foreach ($egresos_totales_moneda as $key =>  $egreso)
							@if($key == 'PYG')
								{{$key}}: {{number_format($egreso,0,",",".")}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							@else 
								{{$key}}: {{number_format($egreso,2,",",".")}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							@endif
						@endforeach
					</th>
				</tr>
				<tr style="background-color: lightgray;">
					<th class="c-text">
						Cuenta
					</th>
					<th class="c-text">
						Nro. Cuenta
					</th>
					<th class="c-text">
						Moneda
					</th>
					<th class="c-text">
						Monto
					</th>
				</tr>
				@foreach($egresos as $egreso)
				<tr>
					<td class="">
						{{$egreso->cuenta}}
					</td>
					<td class="c-text">
						{{$egreso->numero_cuenta}}
					</td>
					<td class="c-text">
						{{$egreso->moneda}}
					</td>
					<td class="c-text">
						{{number_format($egreso->total,2,",",".")}}
					</td>
				</tr>
				@endforeach
			</table>
			<br>
			<br>

		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>	
	<div class="container espacio-10">
		<div class="table-responsive" style="border: solid 2px #000; border-radius:15px;">
			<br>
			<table>
				<tr>
					<td colspan="6" class="c-text f-17">
						<b>CUENTAS NO BANCARIAS</b>
					</td>
				</tr>
				</tr>
			</table>
			<br>
			<br>
			<table border=1 cellspacing=0 cellpadding=2 style="width: 680px;margin-left: 25px;" class="f-11">
				<tr style="background-color: lightgray;">
					<th class="c-text">Cuenta de Fondo</th>
					<th class="c-text">Nro. Cuenta Fondo</th>
					<th class="c-text">Moneda</th>
					<th class="c-text">Saldo</th>
					<th class="c-text">Tipo</th>
				</tr>
				@foreach($bancos_1 as $banco)
				<tr>
					<td class="">{{$banco->banco_n}}</td>
					<td class="c-text">{{$banco->numero_cuenta}}</td>
					<td class="c-text">{{$banco->currency_code}}</td>
					<td class="c-text">{{$banco->currency_code != 'PYG' ? number_format($banco->saldo_detalle, 2, ",", ".") : number_format($banco->saldo_detalle,0,",",".") }}</td>
					<td class="c-text">{{$banco->tipo_cuenta_banco_n}}</td>
				</tr>
			
				@endforeach
			</table>
			<br>
			<br>

		</div>
	</div>


</body>

</html>