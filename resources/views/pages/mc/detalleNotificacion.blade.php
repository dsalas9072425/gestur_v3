@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
		.table>thead:first-child>tr:first-child>td{
			background-color: #FFF;
		}
	</style>	
@endsection
@section('content')
	<div>
        <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
  <!-- Content Header (Page header) -->
			    <section class="content-header">
			      <h1>
			      </h1>
			      <ol class="breadcrumb">
			      </ol>
			    </section>
			    <!-- Main content -->
			    <section class="content">
			      <div class="row">
			        <div class="col-md-3">
			          <div class="box box-solid">
			            <div class="box-header with-border">
			              <h3 class="box-title">Tipos</h3>

			              <div class="box-tools">
			                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
			                </button>
			              </div>
			            </div>
			            	<div class="box-body no-padding">
				              	<ul class="nav nav-pills nav-stacked">
					                <li><a href="{{route('mc.notificacionesIndex')}}"><i class="fa fa-reply"></i> Volver</a></li>
			              		</ul>
			            	</div>
			          </div>
			          <!-- /. box -->
			         <!-- <div class="box box-solid">
			            <div class="box-header with-border">
			              <h3 class="box-title">Labels</h3>

			              <div class="box-tools">
			                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
			                </button>
			              </div>
			            </div>
			            <div class="box-body no-padding">
			              <ul class="nav nav-pills nav-stacked">
			                <li><a href="#"><i class="fa fa-circle-o text-red"></i> Important</a></li>
			                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Promotions</a></li>
			                <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Social</a></li>
			              </ul>
			            </div>
			            <!-- /.box-body 
			          </div>-->
			          <!-- /.box -->
			        </div>
			        <!-- /.col -->
			        <div class="col-md-9">
			          <div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title"><i class="{{$detalleNotificacion['tipos']['clase']}}"></i>{{$detalleNotificacion['tipos']['titulo']}}</h3>

			              <!--<div class="box-tools pull-right">
			                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
			                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
			              </div>-->
			            </div>
			            <!-- /.box-header -->
			            <div class="box-body no-padding">
			              <div class="mailbox-read-info">
			                <h3>{{$detalleNotificacion['asunto']}}</h3>
			                <h5>De: {{$detalleNotificacion['de']}}
			                  <span class="mailbox-read-time pull-right">{{date('d/m/Y h:i', strtotime($detalleNotificacion['fecha_hora_envio']))}}</span></h5>
			              </div>

			              <!-- /.mailbox-controls -->
			              <div class="mailbox-read-message">
			           			<?php echo $detalleNotificacion['contenido'];?>
			              </div>
			              <!-- /.mailbox-read-message -->
			            </div>
			            <!-- /.box-body -->
			            <div class="box-footer">
			            </div>
			            <!-- /.box-footer -->
			            <!--<div class="box-footer">
			              <div class="pull-right">
			                <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
			                <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
			              </div>
			              <button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
			              <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
			            </div>-->
			            <!-- /.box-footer -->
			          </div>
			          <!-- /. box -->
			        </div>
			        <!-- /.col -->
			      </div>
			      <!-- /.row -->
			    </section>
			    <!-- /.content -->
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$(".active").click(function(){
			$(".box-primary").hide()
			$("#divMensaje"+$(this).attr('dataLabel')).show();
		})	
		$('.table').dataTable({
								"aoColumns": [
								    { "width": "10%", "title": "Descripcion", "bSortable": false },
								    { "width": "20%", "title": "Lugar", "bSortable": false },
								    { "width": "40%", "title": "Fecha", "bSortable": false },
								    { "width": "20%", "title": "Cliente (s)", "bSortable": false },
								    { "width": "10%", "title": "Cliente (s)", "bSortable": false},
								  ]
						});
		
	</script>
@endsection