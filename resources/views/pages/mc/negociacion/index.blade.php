
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
	<style>
			.card,.card-header {
					border-radius: 14px !important;
					}
	</style>
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Negociación</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">


				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-6">
						<div class="form-group">
							<label>Nombre / Razon Social - Denominación Comercial de Agencia</label>
							<select class="form-control select2" name="idAgencia" id="selectAgencia"
								style="width: 100%;" required>
								<option value="">Seleccione Agencia</option>

								@foreach($personas as $persona)
								<option value="{{$persona->id}}">{{$persona->nombre}} -
									{{$persona->denominacion_comercial}}</option>
								@endforeach

							</select>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 mb-1">
						<button id="btnEditar" class="btn  btn-lg btn-success">Editar / Agregar Negociación</button>
					</div>
				</div>


				<div class="table-responsive table-bordered">
					<table id="listado" class="table">
						<thead>
							<tr>
								<th>Nombre/Razon Social</th>
								<th>Denominación Comercial</th>
								<th>Comisión Establecida</th>
								<th>Producto</th>
							</tr>
						</thead>
						<tbody id="" style="text-align: center">

							@foreach($negociacion as $negociaciones)
							<tr>
								<td>{{$negociaciones->persona['nombre'] }}</td>
								<td>{{$negociaciones->persona['denominacion_comercial']}}</td>
								<td>{{$negociaciones->comision_pactada}}</td>
								<td>{{$negociaciones->producto['frase_predeterminada']}}</td>
							</tr>

							@endforeach

						</tbody>
					</table>
				</div>




			</div>
		</div>
	</div>
</section>

@endsection
@section('scripts')
	@include('layouts/gestion/scripts')

	<script>
		//$(document).ready(function() {
			// $('#btnEditar').prop('disabled',true);
			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});



			$('#btnEditar').on('click',function(){
				var id = $('#selectAgencia').val();
				if(id != ''){ 
				location.href ="{{route('negociacionEdit')}}?id="+id;
				}
			});

			  $('#selectAgencia').change(function(){

			  		var dataString = {idAgencia : $('#selectAgencia').val()};
			  		
			  		$.blockUI({
                                        centerY: 0,
                                        message: "<h2>Obteniendo Datos. . . </h2>",
                                        css: {
                                            color: '#000'
                                        }
                                    });


			  

			  	$.ajax({
                        type: "POST",
                        url: "{{route('consultaNegociacion')}}",
                        dataType: 'json',
                        data: dataString,

                         error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                           $.unblockUI();

                        },
                        success: function(rsp){
                        	$.unblockUI();

							
                        	// if(rsp.productos.length > 0){
                        	// 	$('#btnEditar').prop('disabled',false);
                        	// } else {
                        	// 	$('#btnEditar').prop('disabled',true);
                        	// }
							var oSettings = $('#listado').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#listado').dataTable().fnDeleteRow(0,null,true);
							}

							$.each(rsp.productos, function (key, item){
				


								var dataTableRow = [
														item.nombre,
														item.denominacion_comercial,
														item.comision_pactada,
														item.frase_predeterminada

													];
								var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
								var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;

							})

						

									

										}//funcion  
                });

			  });












			

	</script>
@endsection