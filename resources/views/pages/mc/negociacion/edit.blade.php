{{-- Funcionamiento --}}

@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
			.card,.card-header {
					border-radius: 14px !important;
					}
	</style>

@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Editar / Agregar Negociación</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

				<form id="frmNegociacion" class="row">


					<!-- /.box -->
					<div class="col-xs-12 col-sm-12 pb-1 ">
						<h3 class="box-title"><b>Nombre:</b> {{$persona->nombre}} - <b>Denominacion Comercial:</b>
							{{$persona->denominacion_comercial}}</h3>
					</div>




					{{-- ==============================================
							BUCLE DE PRODUCTOS
					==============================================	 --}}

					@foreach($productos as $producto)
						@if($producto->tipo_producto == 'P')
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
								<div class="form-group">
									<label for="">{{$producto->denominacion}}</label>
									<input type="number" class="Requerido form-control" name="{{$producto->id}}"
										id="{{$producto->id}}" value="{{$producto->comision_base}}" min="0" max="1000" />
								</div>
							</div>
						@endif
					@endforeach
					<input type="hidden" name="id" id="idPersona" value="{{$persona->id}}">
				</form>
			</div>
		</div>
		<div class="card-footer">
			<button form="" id="btnVolver" class="btn btn-danger btn-lg">Volver</button>
			<button form="" id="btnGuardar" class="btn btn-success btn-lg pull-right">Guardar</button>
		</div>
	</div>
</section>

    @endsection
@section('scripts')
	@include('layouts/gestion/scripts')
<script type="text/javascript">




recargarDatos();

    function recargarDatos(){

    	$.blockUI({
                                        centerY: 0,
                                        message: "<h2>Obteniendo Datos. . . </h2>",
                                        css: {
                                            color: '#000'
                                        }
                                    });

    	var dataString = {'id':$('#idPersona').val()};


    	$.ajax({
						type: "GET",
						url: "{{route('getDatosNegociacion')}}",
						dataType: 'json',
						data: dataString,
						 retries       : 3,     
					    retryInterval : 1000,   

						 error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor, actualice la página por favor.',
                            position: 'top-right',
                            icon: 'error',
                             hideAfter: 'false'
                                });
                            $.unblockUI();
                            $('#btnGuardar').prop('disabled',true);
                            },

						success: function(rsp){
							   $.unblockUI();
						
						$.each(rsp.datos, function(index,item){

							$('#'+item.id).val(item.comision_pactada);

						});


						}//cierreFunc	
				});//Cierre ajax


    }//function


    $('#btnGuardar').on('click',function(e){

    		$.blockUI({
                                        centerY: 0,
                                        message: "<h2>Guardando Datos. . . </h2>",
                                        css: {
                                            color: '#000'
                                        }
                                    });

    	e.preventDefault();
		var dataString =  $('#frmNegociacion').serialize();


			 $.ajax({
                        type: "GET",
                        url: "{{route('actualizarNegociacion')}}",
                        dataType: 'json',
                        data: dataString,

                          error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                             hideAfter: 'false',
                            icon: 'error'
                        });
                             $.unblockUI();

                        },
                        success: function(rsp){
                        	  $.unblockUI();

                        	// console.log(rsp.rsp);

                        	if(rsp.resp == true){

                        		location.href ="{{ route('negociacionIndex')}}";

                        	} else {
                        		 $.toast({
                            heading: 'Error',
                             position: 'top-right',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });

                        	}//else

                        		
                          
                                }//funcion  
                });//ajax

			});//function


			$('#btnVolver').click(()=>{
				window.location.href = "{{route('negociacionIndex')}}";
			});




</script>


	@endsection


