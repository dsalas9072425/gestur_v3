{{-- Funcionamiento --}}

@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

@endsection
@section('content')
    @include('flash::message')

        <!-- Main content -->
    <section class="content"> 
		        <div class="box">

    <form id="frmNegociacion" method="GET"  action="{{route('guardarNegociacion')}}" style="margin-top: 2%;">   
	     
		            <div class="box-header">
		             <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Nueva Negociación</h1>
		            </div> 



		            	<div id="proformaBase">
						    <!-- SELECT2 EXAMPLE -->
						    <div class="box box-default" style="border-top-width: 1px;margin-bottom: 0px;">

						    	<div class="box-body">
							      	<!-- /.box -->

						<div class="row" style="margin-bottom: 1px; padding-left: 15px;padding-right: 15px;" id="datosClienteRow">

						        <div class="box-header with-border">



						       <div class="col-12 col-sm-4 col-md-4" >
						        <div class="form-group">
						            <label for="plazoPago">Agencia</label>
						            <select class="form-control select2" name="" id="id_agencia" style="width: 100%;">
						                <option value="">Seleccione Agencia</option>
						                 @foreach($agencias as $agencia)
						                <option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
						                @endforeach

						               
						            </select>
						        </div>
						    </div>
                        </div>

						        <input type="hidden" name="idPersona" id="idPersona" value="">
						     <div class="box-header with-border" >
								    <h3 class="box-title">Negociación</h3>
								</div>

						        	<div class="row" style="margin-bottom: 1px; padding-left: 15px;padding-right: 15px;" id="datosClienteRow">

									{{-- ==============================================
											BUCLE DE PRODUCTOS
										==============================================	 --}}
										@foreach($productos as $producto)


										 <div class="col-12 col-sm-4 col-md-4"  id="">
									        <div class="form-group">
									            <label for="">{{$producto->denominacion}}</label>
									            <input type="number" class="Requerido form-control" name="{{$producto->id}}" id=""  value="{{$producto->comision_base}}" min="0" max="1000" required="required" />
									        </div>
									    </div>

									                

						                @endforeach

						
							    </div>


							    

						
							    </div>
							</div>


							  <div class="row">
								    <div class="col-12 col-sm-2 col-md-2" style="margin-bottom: 10px;margin-left: 20px">
									<button type="submit" id="btnGuardar" class="btn btn-success btn-lg"  disabled="disabled">Guardar</button>
									</div>
								 </div>  
						</div>	
		          <!-- /.box -->
			    </div>
		        <!-- /.col -->
	    </form>  	
	      	</div>
	      
    </section>
    <!-- /.content -->

    @endsection
@section('scripts')
	@include('layouts/gestion/scripts') 
	<script type="text/javascript">
		$('.select2').select2();
	    $('.select2').on('change', function() {
	    $(this).trigger('blur');
	            }); 

	    $('#id_agencia').change(function(){
	    	var idPersona = $("#id_agencia option:selected").val();

	    	if( idPersona != ''){
	    		$('#idPersona').val(idPersona);
	    		$('#btnGuardar').prop('disabled',false);
	    	} else {
	    		$('#btnGuardar').prop('disabled',true);
	    		$('#idPersona').val('');
	    	}

	    });
	</script>



	@endsection