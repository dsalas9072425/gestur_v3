@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
        	<br>
			<br>
			<br>
			<br>
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop" style="background-color: #fff;">
            	@include('flash::message')
				<div class="booking-information travelo-box">
	         			<form id="frmSucursales" class="contact-form" action="{{route('mc.doEditSucursales')}}" method="post">
			                @include('flash::message')
		                {{ csrf_field() }}
					<div class="booking-information travelo-box">
						<div>
						<h1 class="subtitle hide-medium" style="font-size: x-large;">Editar Sucursal</h1>
							<br>
                            	<input type="hidden" class="form-control" value="{{$sucursales[0]->id_sucursal_agencia}}" name="idSucursalAgencia" id="idSucursalAgencia"/>
	                            <div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Nombre Sucursal (*)</label>
	                                		<input type="text" required class = "Requerido form-control" name="descripcionAgencia" id="descripcionAgencia"  value="{{$sucursales[0]->descripcion_agencia}}"/>
	                                    </div>
	                                   	<div class="col-xs-12 col-sm-6 col-md-6">
					                        <label class="control-label">Agencia (*)</label>
					                        <select name="agencia_id" required class = "select2 form-control" id="agencia_id">
											@foreach($listadoAgencia as $key=>$agencia)
												<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
											@endforeach
											</select>
					                    </div>
	                                </div>
	                            </div>
	                          	<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Email (*)</label>
	                                		<input type="email" required class = "Requerido form-control" name="email" id="email" value="{{$sucursales[0]->email}}"/>
	                                    </div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Telefono (*)</label>
											<input type="text" required class = "Requerido form-control" name="telefono" id="telefono" value="{{$sucursales[0]->telefono}}"/>
										</div>
									</div>
	                            </div>
								<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Activo (*)</label>
											<select name="activo" required class = "Requerido form-control" class="input-text full-width required" id="activo">
												<option value="true">SI</option>
												<option value="false">NO</option>
											</select>
										</div>
	                            		<div class="col-xs-12 col-sm-6 col-md-6">
	                            		</div>
	                                </div>
	                            </div>
	                            <br>
	                                <button type="submit" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style=" background: #e2076a !important;" name="guardar" value="Guardar" >Guardar</button>
								</div>
							</form>
						</div>
                </div>           
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/mc/scripts')
	<script type="text/javascript">
    $(document).ready(function() {
		$('#agencia_id option[value="{{$sucursales[0]->id_agencia}}"]').attr("selected", "selected");
		$('#agencia_id').select2();
		$('#agencia_id').attr("disabled", true);
		var activo = "{{$sucursales[0]->activo}}";
		if(activo =="S"){
			$('#activo option[value="true"]').attr("selected", "selected");
		}else{
			$('#activo option[value="false"]').attr("selected", "selected");
		}

    })	
    </script>
    @endsection
