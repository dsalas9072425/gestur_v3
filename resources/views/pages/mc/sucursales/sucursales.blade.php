@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	<div>
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
            	<form id="frmAgencias" class="contact-form" action="" method="post">
					<div class="booking-information travelo-box">
						<div>
							<h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Sucursales</h1>
							<br>
							@include('flash::message')
							<div class="row">
								<div class="col-sm-10">
									<label class="control-label">Agencia:</label>
									<select name="agencia" required class = "select2" class="input-text full-width required" id="agenciaId">
										<option value="0">Seleccione una Agencia</option>
										@foreach($listadoAgencia as $key=>$agencia)
											<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
										@endforeach
									</select>
								</div>	
								<div class="col-sm-2">
									<a href="{{route('mc.sucursalesAdd')}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px;" role="button">Nueva Sucursal</a>
								</div>
							</div>
							<br>
							<div style= "background-color: #ffffff">
            				<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
	                            <thead>
	                            	<tr>
	                                	<th>ID</th>
	                                  	<th>Sucursal</th>
	                                  	<th>Agencia</th>
				                      	<th>Teléfono</th>
	                                   	<th>Email</th>
	                                   	<th></th>
	                               </tr>
	                            </thead>
	                            <tbody>
	                                @foreach($sucursales as $sucursal)
		                                <tr>
		                            		<td><b>{{$sucursal->id_sucursal_agencia}}</b></td>
		                            	    <td>{{$sucursal->descripcion_agencia}}</td>
		                                  	<td><b>{{$sucursal->agencia->razon_social}}</b></td>
					                      	<td>{{$sucursal->telefono}}</td>
		                                   	<td><b>{{$sucursal->email}}</b></td>
		                                   	<td><a href="{{route('mc.sucursalesEdit', ['id' =>$sucursal->id_sucursal_agencia])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; background: #e2076a !important; width: 130px;" role="button">Editar</a></td>
		                                </tr>   	
	                                @endforeach   	
	                            </tbody>
                        	</table>
	                       </div>	 
                        </div>
                    </div>    			
            </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
		$('#agenciaId').select2();
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
			$('#agenciaId option[value="{{$agenciaId[0]->id_agencia}}"]').attr("selected", "selected");
		});

		$("#agenciaId").change(function(){
			idAgencia = $(this).val();
			$.blockUI({
		            centerY: 0,
		            message: "<h2>Procesando...</h2>",
		                css: {
		                    color: '#000'
		                    }
		                });
			$.ajax({
					type: "GET",
					url: "{{route('getAgenciasSucursales')}}",
					dataType: 'json',
					data: {
						dataAgencia: idAgencia
			       			},
					success: function(rsp){
								$.unblockUI();
								var oSettings = $('#listado').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();

									for (i=0;i<=iTotalRecords;i++) {
										$('#listado').dataTable().fnDeleteRow(0,null,true);
									}

									$.each(rsp, function (key, item){
										var accion = "<a href='./sucursalesEdit/"+item.id_sucursal_agencia+"' class='btn btn-info text-center btn transaction_normal hide-small normal-button' style='padding-top: 10px; background: #e2076a !important;' role='button'>Editar</a>";
								        var dataTableRow = [
															item.id_sucursal_agencia,
															item.descripcion_agencia,
															item.agencia.razon_social,
															item.telefono,
															item.email,
															accion
															];

										var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
										// set class attribute for the newly added row 
										var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
										// and parse the row:
										var nTds = $('td', nTr);
									})	
							}
 					});
		});
	</script>
@endsection
