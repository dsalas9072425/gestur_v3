@extends('masters')

@section('title', 'Detalle de Reserva')
@section('styles')
	@parent
@endsection
@section('content')
<section class="sectiontop">
		<div class="container" style="background-color: white">
                <form action="" method="post">
                    <div class="row"> 
                        <div class="col-sm-12 col-md-12 alert-danger">
							<!-- En caso de error-->
                        </div>
                        @include('flash::message')
                        <div id="main" class="col-sm-12 col-md-12" class="sectiontop">
                            <div class="booking-information travelo-box"> 
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<h2 style="background-color: #2d3e52;color: white;">Información de la Reserva</h2>
										<dl class="term-descriptioncustom">
											<dt>Localizador:</dt><dd><b style="font-size: 15px;color: blue;">{{$reserva->codigo}}</b></dd>
											<dt>Fecha de Reserva:</dt><dd>{{$reserva->fecha_reserva}}</dd>
											<dt>Pasajero:</dt><dd>{{$reserva->pasajero_principal_nombre  . ' ' . $reserva->pasajero_principal_apellido}}</dd>
										</dl>
									</div>
									<div class="col-md-6 col-sm-6">
										<h2 style="background-color: #2d3e52;color: white;">Datos del Alojamiento</h2>
										<dl class="term-descriptioncustom">
											<dt>Alojamiento:</dt><dd>{{$reserva->nombre_hotel}}</dd>
											<dt>Dirección:</dt><dd>{{$reserva->direccion_hotel}}</dd>
											@if(!empty($ocupantesHabitacion))
												<dt>Estado:</dt><dd>@php
														switch ($reserva->id_estado_reserva) {
															//Pendiente
															case config('constants.resPendiente'):
																echo '<span class="label alert-info">PENDIENTE</span>';
																break;
															//Aprobado
															case config('constants.resConfirmada'):
																echo '<span class="label alert-success">CONFIRMADA</span>';
																break;
															//Cancelado    
															case config('constants.resEliminada'):
															echo '<span class="label alert-danger">CANCELADA</span>';
																break;
															//Rechazado	
															case config('constants.resRechazada'):
																echo '<span class="label alert-warning">RECHAZADA</span>';
																break;
															//Error	
															case config('constants.resError'):
																echo '<span class="label alert-error">ERROR</span>';
																break;
														}
														@endphp</dd>
											@endif	
										</dl>
									</div>
								</div>
							</div>
							@if(!empty($ocupantesHabitacion))
	                            <div class="booking-information travelo-box">
									<h2 style="background-color: #2d3e52;color: white;">Habitaciones</h2>
	                                <div class="table-responsive">
										<table class="table">
											<tr>
												<th>Descripción</th>
												<th>Régimen</th>
												<th>Titular</th>
												<th>Precio</th>
											</tr>									
											@foreach($ocupantesHabitacion as $habitacion)
												<tr>
													<td>{{$habitacion->descripcion_habitacion}}</td>
													<td>{{$habitacion->descripcion_regimen}} ({{$habitacion->codigo_regimen}})</td>
													<td>{{$habitacion->nombre}} ({{$habitacion->tipo}})</td>
													<td>{{number_format($habitacion->precio_con_comision,2)." USD"}}</td>											
												</tr>
											@endforeach
										</table>	
									</div>
	                            </div>
                            @else
								<div class="booking-information travelo-box">
									<h2 style="background-color: #2d3e52;color: white;">Detalles</h2>
	                                <div class="table-responsive">
										<table class="table">
											<tr>
												<th>Total Ocupancia</th>
												<th>Cant. Habitacion/es</th>
												<th>Cant. Noches</th>
												<th>Total Reserva</th>
												<th>Agente DTP</th>
												<th>Estado</th>
											</tr>
											<tr>
												<td>{{$reserva->ocupancia}} persona/s</td>
												<td>{{$reserva->cantidad_habitaciones}}</td>
												<td>{{$reserva->cantidad_noches}}</td>
												<td>{{$reserva->monto_cobrado}}</td>
												<td>{{$nombre_agente_dtp}}</td>
												<td>
													@php
														switch ($reserva->id_estado_reserva) {
															//Pendiente
															case config('constants.resPendiente'):
																echo '<span class="label alert-info">PENDIENTE</span>';
																break;
															//Aprobado
															case config('constants.resConfirmada'):
																echo '<span class="label alert-success">CONFIRMADA</span>';
																break;
															//Cancelado    
															case config('constants.resEliminada'):
															echo '<span class="label alert-danger">CANCELADA</span>';
																break;
															//Rechazado	
															case config('constants.resRechazada'):
																echo '<span class="label alert-warning">RECHAZADA</span>';
																break;
															//Error	
															case config('constants.resError'):
																echo '<span class="label alert-error">ERROR</span>';
																break;
														}
													@endphp</td>
											</tr>
										</table>
									</div>
	                            </div>
                            @endif
                            <div class="booking-information travelo-box">
                                <h2 style="background-color: #2d3e52;color: white;">Comentarios</h2>
                                <dl class="term-descriptioncustom">
									<p>
									{{$reserva->info_adicional}}
									</p>
                                </dl>
                            </div>
                            <div class="booking-information travelo-box">
                               	<div class="row">
									<div class="col-md-6 col-sm-6">
										<h2 style="background-color: #2d3e52;color: white;">Datos de Envio de Proforma</h2>
										<dl class="term-descriptioncustom">
											<dt>N° Proforma:</dt><dd>{{$reserva->numero_proforma}}</dd>
											<!--<dt>Descripción Proforma:</dt><dd>{{$reserva->desret_proforma}}</dd>-->
										</dl>
									</div>
									<div class="col-md-6 col-sm-6">
										@if($reserva->state_id == config('constants.resEliminada'))
											<h2 style="background-color: #2d3e52;color: white;">Datos de Cancelación de Proforma</h2>
											<dl class="term-descriptioncustom">
												<dt>Usuario:</dt>
												<dd>
													@if(isset($reserva->usuariocancelacion->nombre_apellido))
														{{$reserva->usuariocancelacion->nombre_apellido}}
													@endif	
												</dd>
												<dt>Descripción de la Cancelación:</dt>
												<dd>
													@if(isset($reserva->desret_proforma_cancelacion))
														{{$reserva->desret_proforma_cancelacion}}
													@endif
												</dd>
											</dl>
										@endif	
									</div>
								</div>	
                            </div>

                            <div class="booking-information travelo-box">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<h2 style="background-color: #2d3e52;color: white;">Política de Cancelación</h2>
											<dl class="term-descriptioncustom">
													<dt>Desde:</dt><dd>{{$reserva->cancelacion_desde}}</dd>
													<dt>Costo:</dt><dd>{{round($reserva->cancelacion_monto, 2)}} {{$reserva->monedaorigen->currency_code}}</dd>
											</dl>
									</div>
									<div class="col-md-6 col-sm-6">
										<h2 style="background-color: #2d3e52;color: white;">Información Importante</h2>
										<dl class="term-descriptioncustom">
											<dt>Check In:</dt><dd>{{$reserva->fecha_checkin}}</dd>
											<dt>Check Out:</dt><dd>{{$reserva->fecha_checkout}}</dd>
										</dl>
										<p><b>A confirmar según política de cada Hotel.</b></p>
									</div>
								</div>							
                            </div>
                        </div>
                    </div>
                </form>
    	</div>
</section>
@endsection

@section('scripts')
    @parent
@endsection
