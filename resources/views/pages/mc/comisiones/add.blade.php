@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
	border-radius: 14px !important;
	}
	</style>
	
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Agregar Comisiones</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">
				<form id="frmComision">
					<div id="outputDetalles" style="display:none;" ></div>
					<div class="row">
						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Vendedor</label>
								<select class="form-control select2" name="id_vendedor" id="id_vendedor" style="width: 100%;"
									required>
									<option value="">Seleccione Vendedor</option>
									@foreach($vendedores as $vendedor)
									@php 
										if($vendedor->documento_identidad != ""){
											$ci_ruc = $vendedor->documento_identidad.' - ';
										}else{
											$ci_ruc = "";
										}
									@endphp
									<option value="{{$vendedor->id}}">{{$ci_ruc}} {{$vendedor->nombre}} {{$vendedor->apellido}} - {{$vendedor->email}} - {{$vendedor->denominacion}}</option>
								@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-4">
							<div class="form-group">
								<label>Moneda</label>
								<select class="form-control select2" name="id_moneda" id="id_moneda" style="width: 100%;"
									required>
									<option value="">Seleccione Divisa</option>
									@foreach($divisas as $divisa)
									<option value="{{$divisa->currency_id}}">{{$divisa->currency_code}} -
										{{$divisa->hb_desc}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-4">
							<label class="control-label">Porcentaje Extra</label>
							<input type="text" name="porcentaje_extra" class="Requerido form-control numeric" id="porcentaje_extra" required="required" />
						</div>
						<div class="col-12 col-sm-4 col-md-4">
							<label class="control-label">Facturación</label>
							<input type="text" name="facturacion" class="Requerido form-control numeric" id="facturacion" required="required" />
						</div>
					</div>
					<br>
					<br>
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12">
							<div class="table-responsive table-bordered">
								<table id="listadoDetalle" class="table">
									<thead>
										<tr style="text-align: center;">
											<th>Desde</th>
											<th>Hasta</th>
											<th>Porcentaje</th>
											<th></th>
										</tr>
									</thead>
									<tbody id="listaCotizacion" style="text-align: center">
										<tr>
											<th>
												<input type="text" class="Requerido form-control numeric" id="desde0" value="0"/>
											</th>
											<th>
												<input type="text" class="Requerido form-control numeric" id="hasta0" value="0"/>
											</th>
											<th>
												<input type="text" class="Requerido form-control numeric" id="porcentaje0" value="0"/>
											</th>
											<th>
												<a onclick="guardarFila()" title="Guardar Fila" class="btn btn-success" style="margin-left:5px;padding-left: 8px;padding-right: 8px;" role="button"><i class="fa fa-fw fa-plus"></i></a>
											</th>
										</tr>
									</tbody>
								</table>
							</div>	
						</div>				
					</div>
					<div class="row">
						<div class="col-12 mb-2">
							<a href="{{ route('cotizacionIndex')}}" type="button" class="btn btn-danger btn-lg pull-right mr-1">Volver</a>
							<button type="button" onclick="guardarComision()" id="btnGuardar" class="btn btn-success btn-lg pull-right mr-1">Guardar</button>
						</div>
					</div>

				</form>

			</div>
		</div>
	</section>
	</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.mask.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>

	<script>


		$(document).ready(function() {
		//	buscarCotizacion();


			$('.select2').select2();
			$('.select2').on('change', function() {
			  $(this).trigger('blur');
			});

			$('.numeric').inputmask("numeric", {
				radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				// prefix: '$', //No Space, this will truncate the first character
				rightAlign: false,
				oncleared: function () { 
					$(this).val(0);
				}
			});

		})

		function guardarFila(){
				console.log(parseInt($("#desde0").val()));  
				console.log(parseInt($("#hasta0").val())); 
				console.log(parseInt($("#porcentaje0").val())); 

       		if($("#desde0").val() != "" ||  parseInt($("#desde0").val()) != 0 && $("#hasta0").val() != "" || parseInt($("#hasta0").val()) != 0 && $("#porcentaje0").val() != ""|| parseInt($("#porcentaje0").val()) != 0){
       		    var nFilas = $("#listaCotizacion tr").length;
       		    i = nFilas - 1;
       		    tablas = '';
				detalle = '';
				detalle += '<div id="row'+i+'"><input type="text" maxlength="2" required value = "'+$("#desde0").val()+'" class = "Requerido form-control input-sm" name="detalles['+i+'][desde]" id="detalles_desde_'+i+'"/>';	
				detalle += '<input type="text" maxlength="2" required value = "'+$("#hasta0").val()+'" class = "Requerido form-control input-sm" name="detalles['+i+'][hasta]" id="detalles_hasta_'+i+'"/>';	
				detalle += '<input type="text" maxlength="2" required value = "'+$("#porcentaje0").val()+'" class = "Requerido form-control input-sm" name="detalles['+i+'][porcentaje]" id="detalles_porcentaje_'+i+'"/></div>';
				tablas += '<tr id="rows'+i+'">';
				tablas += '<td><input type="text" class="Requerido form-control numeric" name="detalle['+i+'][desde]" id="desde_'+i+'" value='+$("#desde0").val()+' disabled/></td>';
				tablas += '<td><input type="text" class="Requerido form-control numeric" name="detalle['+i+'][hasta]" id="hasta_'+i+'" value='+$("#hasta0").val()+' disabled/></td>';
				tablas += '<td><input type="text" class="Requerido form-control numeric" name="detalle['+i+'][porcentaje]" id="porcentaje_'+i+'" value='+$("#porcentaje0").val()+' disabled/></td>';
				tablas += '<td><a onclick="eliminarFila('+i+')" title="Eliminar" class="btn btn-danger" style="background: #e2076a !important;margin-left:5px;padding-left: 8px;padding-right: 8px;width: 36px;" role="button"><i class="fa fa-times"></i></a></td>';
				tablas += '</tr>';

				$("#outputDetalles").append(detalle);	
				$('#listadoDetalle tbody').append(tablas);

				$("#desde0").val(0);
				$("#hasta0").val(0);
				$("#porcentaje0").val(0);

			}else{
				 $.toast({
						    heading: 'Error',
						    text: 'Ingresen los datos para proceder al guardado',
						    position: 'top-right',
						    showHideTransition: 'fade',
						    icon: 'error'
						});
			}	

       	}

		function eliminarFila(idFila){
			$('table#listadoDetalle tr#rows'+idFila).remove();
			$("#row"+idFila).remove();

		}
		   
		function guardarComision(){
			if($("#id_vendedor").val() != ""){
				if($("#id_moneda").val() != ""){
					if($("#tipo").val() != ""){
						if($("#porcentaje_extra").val() != ""){
							if($("#facturacion").val() != ""){
									$('#btnGuardar').prop('disabled', true);
									return swal({
												title: "GESTUR",
												text: "¿Desea agregar las comisiones?",
												showCancelButton: true,
												buttons: {
														cancel: {
																text: "No",
																value: null,
																visible: true,
																className: "btn-warning",
																closeModal: true,
																},
														confirm: {
																text: "Sí, agregar",
																				value: true,
																				visible: true,
																				className: "",
																				closeModal: true
																			}
														}
											}).then(isConfirm => {
															if (isConfirm) {
																	guardar(); 
															} else {
																	$('#btnGuardar').prop('disabled', false);
																	swal("Cancelado", "", "error");
															}
														});
							}else{
								$.toast({
											heading: 'Error',
											text: 'Ingrese el dato de Facturación',
											position: 'top-right',
											showHideTransition: 'fade',
											icon: 'error'
										});
							}	
						}else{
							$.toast({
										heading: 'Error',
										text: 'Ingrese el dato de Porcentaje Extra',
										position: 'top-right',
										showHideTransition: 'fade',
										icon: 'error'
									});
						}
					}else{
						$.toast({
									heading: 'Error',
									text: 'Ingrese el dato del Tipo',
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
								});
					}	
				}else{
					$.toast({
								heading: 'Error',
								text: 'Ingrese el dato de Moneda',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
							});
				}
			}else{
				$.toast({
							heading: 'Error',
							text: 'Ingrese el dato de Vendedor',
							position: 'top-right',
							showHideTransition: 'fade',
							icon: 'error'
						});
			}	

		}	

		function guardar(){
			var dataString = $("#frmComision").serialize();
            $.ajax({
                    type: "GET",
                    url: "{{route('doAgregarComision')}}",
                    data: dataString,
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown) {
							$('#btnGuardar').prop('disabled', false);
                            swal("Cancelado", "Ocurrió un error en la comunicación con el servidor", "error");
                    },
                    success: function (rsp) {
							if(rsp.status == 'OK'){
								swal("Éxito", rsp.mensaje, "success");
								redirectView()
							}else{
								swal("Cancelado", rsp.mensaje, "error");
							}
                    } //success
            }).done(()=>{
                $('#btnGuardar').prop('disabled', false);
            }); //AJAX
		}												

		function redirectView(){

			$.blockUI({
				centerY: 0,
				message: "<h2>Redirigiendo a vista de Listado de Comisiones...</h2>",
				css: {
					color: '#000'
				}
			});

			location.href = "{{ route('reporteComision') }}";
			}


	</script>
@endsection