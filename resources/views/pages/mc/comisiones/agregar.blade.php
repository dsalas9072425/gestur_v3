@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent

	<style>
		.card,.card-header {
			border-radius: 14px !important;
		}

	.loading-dots {
		display: inline-block;
		animation: loading 1s infinite;
	}

	@keyframes loading {
		0% {
			opacity: 0.2;
		}
		20% {
			opacity: 1;
		}
		100% {
			opacity: 0.2;
		}
	}

	.dot {
		display: inline-block;
		width: 10px;
		height: 10px;
		border-radius: 50%;
		background-color: #000;
		margin-right: 3px;
	}
	</style>
	
@endsection
@section('content')

<section id="base-style">

	<section class="card">
		<div class="card-header">
			<h4 class="card-title">Comisión Aerolínea</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body pt-0">

				<form id="frmCotizacion" method="post" action="{{route('doAddCotizacion')}}">
					<div class="row">
						<div class="col-12 col-sm-4 col-md-4" style="padding-left: 20px;">
							<label>Aerolínea</label>					            	
								<select class="form-control input-sm select2" name="aerolinea" id="aerolinea" style="padding-left: 0px;width: 100%;">
									<option value=" ">Seleccione aerolínea</option>
									@foreach($aerolineas as $key=>$aerolinea)
											<option {{ old('aerolinea') == $aerolinea->out_id_persona ? 'selected' : '' }} value="{{$aerolinea->out_id_persona}}">{{$aerolinea->out_id_persona}} - {{$aerolinea->out_nombre}}</option>
									@endforeach	
								</select>
						</div>
						<div class="col-12 col-sm-4 col-md-4" style="padding-left: 20px;">
							<label class="control-label">Comisión</label>
							<span id="esperar" style="display:none;">
								<div class="loading-dots">
									<span class="dot"></span>
									<span class="dot"></span>
									<span class="dot"></span>
								</div>
							</span>
					
							<input class="Requerido form-control numeric" value="" id="comision" name="comision" required="required" disabled />
						</div>

					<div class="col-12 mb-2">
							<button type="button" onclick="guardarComisionAerea()" id="btnGuardar" class="btn btn-success btn-lg pull-right mr-1">Guardar</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</section>
	</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>

	<script>
		$(document).ready(function() {
			$('.select2').select2();
			$('#aerolinea').change(function(){
			$('#esperar').show();
			$('#comision').prop('disabled',true);
				
				$.ajax({
						type: "GET",
						url: "{{route('datosComisionAerea')}}",//fileDelDTPlus
						dataType: 'json',
						data: {  
								dataFile:$('#aerolinea').val()
							},
						success: function(rsp){
							
							if(rsp.comision != null){
								$('#comision').val(rsp.comision);
							}else {
								$('#comision').val('');
							}

							$('#comision').prop('disabled',false);
							$('#esperar').hide();
							
						},
						error: function(){
							$.toast({
								heading: 'Error',
								text: 'Ocurrio un error en la comunicación con el servidor.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
                                });

								$('#comision').prop('disabled',false);
								$('#esperar').hide();
						}
					})
			});

			$('.numeric').inputmask("numeric", {
				radixPoint: ",",
				groupSeparator: ".",
				digits: 2,
				autoGroup: true,
				// prefix: '$', //No Space, this will truncate the first character
				rightAlign: false,
				oncleared: function () { 
					$(this).val(0);
				}
			});

		});	

		function guardarComisionAerea(){

			$.ajax({
						type: "GET",
						url: "{{route('guardarComisionAerea')}}",//fileDelDTPlus
						dataType: 'json',
						data: {  
								dataAerolinea:$('#aerolinea').val(),
								dataComision:$('#comision').val()
							},
						success: function(rsp){
											if(rsp.status == 'OK'){
												$.toast({
														heading: 'Exito',
														text: rsp.mensaje,
														position: 'top-right',
														showHideTransition: 'slide',
														icon: 'success'
													});  
													location.reload();
												}else{
													$.toast({
														heading: 'Error',
														text: rsp.mensaje,
														position: 'top-right',
														showHideTransition: 'fade',
														icon: 'error'
													});
												}
						}
					})





		}


	</script>
@endsection