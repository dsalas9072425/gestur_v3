
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
<style type="text/css">
	.icoRojo{
		color:red;
	}

	.icoVerde{
		color:green;
	}

	.icoAmarillo{
		color:#C7A10A;
	}
	
	input .tachado
	{
		text-decoration: line-through;
		
	}

	.tachado
	{
		text-decoration: line-through;
		
	}

	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}


	#formMovimientoBancario select option, select{
		font-size: 12px;
		font-weight: 800;
	}

	.select2{
		width: 100% !important;
	}

	.no-input{
		border:0 solid white;
		background-color: #fff;
	}

</style>


<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Resumen Comisiónes</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">

			<ul class="nav nav-tabs nav-underline" id="myTabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="tabCuenta" data-toggle="tab" aria-controls="tabIcon21" href="#home" role="tab" aria-selected="true">
                        Comisiones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tabMovimiento" data-toggle="tab" aria-controls="tabIcon22" href="#movimientoBancario" role="tab" aria-selected="false">
                       Detalle de comisiones</a>
                </li>
                
			</ul>
		<div class="tab-content">
		<section id="home"  class="tab-pane active" role="tabpanel">
			<div class="card-body">
				<div class="card-body pt-0">
					<a href="{{route('agregarComision')}}" title="Agregar Escala Comisiones" class="btn btn-success pull-right" role="button"><div class="fonticon-wrap"><i class="ft-plus-circle"></i></div></a>
				</div>
				<form class="row" id="reporteCuentaBanco">
					<div class="col-12 col-sm-3 col-md-3">
						<div class="form-group">
							<label>Vendedor</label>
							<select class="form-control select2" name="id_vendedor" id="id_vendedor">
								<option value="">Seleccione Vendedor</option>
								@foreach($vendedores as $vendedor)
									<option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-12 col-sm-3 col-md-4">
						<div class="form-group">
							<label>Moneda</label>
							<select class="form-control select2" name="id_moneda" id="id_moneda">
								<option value="">Seleccione Divisa</option>
								@foreach($divisas as $divisa)
									<option value="{{$divisa->currency_id}}">{{$divisa->currency_code}} -
										{{$divisa->hb_desc}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-12 col-sm-3 col-md-2">
						<div class="form-group">
							<label>Tipo</label>
							<select class="form-control select2" name="tipo" id="tipo">
								<option value="">Seleccione Tipo</option>
								<option value="R">Por Renta</option>
								<option value="P">Por Porcentaje</option>
							</select>
						</div>
					</div>
					<div class="col-12 col-sm-3 col-md-2">
						<div class="form-group">
							<label>Activo</label>
							<select class="form-control select2" name="activo" id="activo">
								<option value="true">SI</option>
								<option value="false">NO</option>
							</select>
						</div>
					</div>


				</form>

				<div class="row">
					<div class="col-12 mb-1">
						<button type="button" onclick="limpiar()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
						<button type="button" class="btn btn-info pull-right btn-lg mr-1" onclick="consultaCuentaBanco()"><b>Buscar</b></button>

					</div>
				</div>
            	<div class="table-responsive table-bordered">
		            <table id="listadoCuentaBanco" class="table" style="width: 100%;">
						<thead>
							<tr style="text-align: center;">
								<th>Fecha</th>
								<th>Vendedor</th>
								<th>Moneda</th>
								<th>Tipo</th>
								<th>Facturación</th>
								<th>% Extra</th>
								<th></th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
						</tbody>
					</table>
				</div>


			</div>
		</section>

		<section id="movimientoBancario"  class="tab-pane" role="tabpanel">

			<div class="card-body">
				<form class="row" id="reporteMovimientoBancario">

					<div class="col-12 col-sm-4 col-md-4">
						<div class="form-group">
							<label>Vendedor</label>
							<select class="form-control select2" name="id_vendedor_base" id="id_vendedor_base" disabled="disabled" >
								<option value="">Seleccione Vendedor</option>
								@foreach($vendedores as $vendedor)
									<option value="{{$vendedor->id}}">{{$vendedor->nombre}} {{$vendedor->apellido}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<input type="hidden" class="form-control calendario" name="id_cabecera" id="id_cabecera" />

					<div class="col-12">
						<button type="button" id="botonExcel" class="btn btn-success btn-lg pull-right mr-1"><b>Excel</b></button> 
						<button type="button" class="btn btn-info pull-right btn-lg mr-1" onclick="consultaMovimientoBancario()"><b>Buscar</b></button>
					</div>
				</form>
				<div class="table-responsive mt-1">
					<table id="listadoMovimientoBancario" class="table table-hover table-condensed nowrap" style="width: 100%;">
						<thead>
							<tr style="text-align: center;">
								<th>Desde</th>
								<th>Hasta</th>
								<th>Porcentaje</th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
						</tbody>
					</table>
				</div>


			</div>

		</section>



		</div>





		</div>
	</div>
</section>


@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.serializejson.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.inputmask.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/customTypesSerialize.js')}}"></script>
	<script>





		{{--==========================================
					VARIABLES GLOBALES
			==========================================--}}
			var ordenamiento = [];
			var table;

			$(()=>{
				consultaCuentaBanco();
				$('.select2').select2();
			});


		
		{{--==========================================
					BUSQUEDA POR CUENTA DE BANCO
			==========================================--}}


	function consultaCuentaBanco(){

			$.blockUI({
											centerY: 0,
											message: '<div class="loadingC"><img src="images/loading.gif" alt="Procesando..." style="width:100px"/><br><h2>Cargando ...</h2></div>',
											css: {
												color: '#000'
											}
										});

			// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
			setTimeout(function (){
				// `<div class="click_estrella" onclick="mostrar_postVenta(${x.id})">`
						
	 		table =   $('#listadoCuentaBanco').DataTable({
					"destroy": true,
					"ajax": {
							data: $('#reporteCuentaBanco').serializeJSON(),
							url: "{{route('ajaxReporteComision')}}",
							type: "GET",
					error: function(jqXHR,textStatus,errorThrown){

				   $.toast({
					heading: 'Error',
					text: 'Ocurrió un error en la comunicación con el servidor.',
					position: 'top-right',
					showHideTransition: 'fade',
					icon: 'error'
				});
					$.unblockUI();
				}
						// formatter.format(parseFloat(x.total_venta_proforma));
				  },
				 "columns":[
								{data:'fecha_format'},
								{data:'vendedores'},
								{data:'moneda'},
								{data:'tipo'},
								{data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.facturacion}">`;
											}
									 },
								{data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.porcentaje_extra}">`;
											}
									 },
								{data: function(x){
									if(x.activo == true){
										botones = `<button  class='btn btn-danger' onclick='anularComision(${x.id})' style="margin-right: 10px;" type='button'><i class='fa fa-close'></i></button>`;
									}else{
										botones = `<button  class='btn btn-success' onclick='activarComision(${x.id})' style="margin-right: 10px;" type='button'><i class='fa fa-check'></i></button>`;
									}
									botones += `<button  class='btn btn-info' onclick='verMovimiento(${x.id},${x.id_vendedor})' type='button'><i class='fa fa-fw fa-search'></i></button>`;
									return botones;
								}}
							 
							]
				
				}).on('xhr.dt', function ( e, settings, json, xhr ){
						//Ajax event - fired when an Ajax request is completed.;
					 $.unblockUI();
				}).on('draw', function () {
	       		/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

			    $('#listadoCuentaBanco tbody tr .format-number').inputmask("numeric", {
					radixPoint: ",",
					groupSeparator: ".",
					digits: 2,
					autoGroup: true,
					 // prefix: '$', //No Space, this will truncate the first character
					rightAlign: false
				});

    	});
				
	 
					}, 300);

}//function




			{{--==========================================
					MOVIMIENTO BANCARIO
			==========================================--}}



	function consultaMovimientoBancario(){

		$.blockUI({
				                        centerY: 0,
				                        message: "<h2>Procesando....</h2>",
				                        css: {
				                            color: '#000'
				                        }
				                    });

		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		setTimeout(function (){
			// `<div class="click_estrella" onclick="mostrar_postVenta(${x.id})">`
					
			 table2 =   $('#listadoMovimientoBancario').DataTable({
					        "destroy": true,
					        "ajax": {
					        		data: $('#reporteMovimientoBancario').serializeJSON({customTypes: customTypesSerializeJSON}),
						            url: "{{route('reporteDetalleComision')}}",
						            type: "GET",
						    error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrió un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                            $.unblockUI();
                        }
                        		// formatter.format(parseFloat(x.total_venta_proforma));
						  },
						 "columns":[

									{data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.desde}">`;
											}
									 },
									 {data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.hasta}">`;
											}
									 },
									 {data: function(x){
												return `<input type="text" disabled style="text-align:center;" class="no-input format-number" value="${x.porcentaje}">`;
											}
									 },
						 		   ],
						 		   "order": [[ 0, "asc" ]],
							     "createdRow": function ( row, data, index ) {
						     	if ($(row).find('td .activeCheck').val() == 'false') {
                        					$(row).addClass('tachado bg-red bg-lighten-4')
                    			} 
						      
						        				}			
						                    
					    }).on('xhr.dt', function ( e, settings, json, xhr ){
								//Ajax event - fired when an Ajax request is completed.;
							 $.unblockUI();

						}).on('draw', function () {
	       		/*{{-- AL PAGINAR VUELVE A ASIGNAR EL CONTROL DE INPUT--}}*/

			    $('#listadoMovimientoBancario tbody tr .format-number').inputmask("numeric", {
					radixPoint: ",",
					groupSeparator: ".",
					digits: 2,
					autoGroup: true,
					 // prefix: '$', //No Space, this will truncate the first character
					rightAlign: false
				});

    	});
					    
			 
					    	}, 300);

}//function


			{{--==========================================
					INTERACCION DE PESTAÑAS
			==========================================--}}

				function verMovimiento(num, id){
					$('#id_cabecera').val(num);
					$('#id_vendedor_base').val(id).trigger('change.select2');
					consultaMovimientoBancario();
					$('#tabMovimiento').tab('show');
				}

				function activarComision(id){
					return swal({
								title: "GESTUR",
								text: "¿Está seguro que desea activar la escala de comisiones?",
								showCancelButton: true,
								buttons: {
										cancel: {
												text: "No",
												value: null,
												visible: true,
												className: "btn-warning",
												closeModal: false,
											},
										confirm: {
												text: "Sí, Activar",
												value: true,
												visible: true,
												className: "",
												closeModal: false
											}
										}
							}).then(isConfirm => {
									if (isConfirm) {
										$.ajax({
												type: "GET",
												url: "{{route('activarEsalaComision')}}",
												dataType: 'json',
												data: {
													id_comision:id,
												},
												error: function (jqXHR, textStatus, errorThrown) {
													$.toast({
														heading: 'Error',
														text: 'Ocurrió un error al intentar eliminar la línea.',
														position: 'top-right',
														showHideTransition: 'fade',
														icon: 'error'
													});
												},
												success: function (rsp) {
													if(rsp.status == "OK"){
														swal("Éxito", rsp.msj, "success");	
								        				location. reload();
													}else{
														swal("Cancelado", rsp.msj, "error");
													}
												}
											})
										}else{
											swal("Cancelado", "", "error");
										}	
							})	
					}

				function anularComision(id){
					return swal({
								title: "GESTUR",
								text: "¿Está seguro que desea eliminar la escala de comisiones?",
								showCancelButton: true,
								buttons: {
										cancel: {
												text: "No",
												value: null,
												visible: true,
												className: "btn-warning",
												closeModal: false,
											},
										confirm: {
												text: "Sí, Eliminar",
												value: true,
												visible: true,
												className: "",
												closeModal: false
											}
										}
							}).then(isConfirm => {
									if (isConfirm) {
										$.ajax({
												type: "GET",
												url: "{{route('anularEsalaComision')}}",
												dataType: 'json',
												data: {
													id_comision:id,
												},
												error: function (jqXHR, textStatus, errorThrown) {
													$.toast({
														heading: 'Error',
														text: 'Ocurrió un error al intentar eliminar la línea.',
														position: 'top-right',
														showHideTransition: 'fade',
														icon: 'error'
													});
												},
												success: function (rsp) {
													if(rsp.status == "OK"){
														swal("Éxito", rsp.msj, "success");	
								        				location. reload();
													}else{
														swal("Cancelado", rsp.msj, "error");
													}
												}
											})
										}else{
											swal("Cancelado", "", "error");
										}	
							})	
					}

		function limpiar(){
			$('#id_cuenta_detalle').val('').trigger('change.select2');
			$('#id_banco').val('').trigger('change.select2');
			$('#tipo_cuenta').val('').trigger('change.select2');
		}

		function limpiarMovimientoBancario(){
			$('#rm_cuenta_detalle').val('').trigger('change.select2');
			$('#periodo_fecha').val('');

		}

			$("#botonExcel").on("click", function(e){ 
                e.preventDefault();
				$('#reporteMovimientoBancario').attr("method", "post");               
				$('#reporteMovimientoBancario').attr('action', "{{route('generarExcelMovimientoCuenta')}}").submit();
            });


	</script>
@endsection