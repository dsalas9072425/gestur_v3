{{-- Funcionamiento --}}

@extends('master')
@section('title', 'Panel de Control')
@section('styles')
@parent

@endsection
@section('content')


<style type="text/css">
 .col-center{
  float: none;
  margin-left: auto;
  margin-right: auto;
}   

.input-bold{
    /*font:small-caption;*/
    /*font-family: caption*/
     font: bold 100% ,sans-serif;
    font-size:20px;
}

.eye{
    background: #0A77C7 !important;
    color:white;
    cursor: pointer;
}
</style>

<br>
<br>
<br>
<section id="base-style">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><i class="fa fa-pencil"></i> Cambiar Clave</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
                <form id="actualizar-form" action="{{route('actualizar')}}" method="get" action="" autocomplete="off" style="margin-top: 2%;">
                    <div class="row">
                        <div class="col-3 col-center">
                            <div class="form-group">
                                <label>NUEVA CLAVE (*)</label>

                                <div class="input-group">
                                    <div class="input-group-prepend" id="btnVer1">
                                        <button class="btn btn-warning" type="button"><i class="fa fa-eye"></i></button>
                                    </div>
                                    <input type="password" class="form-control input-bold" name="clave_anterior"
                                        id="password1">
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-3 col-center">
                            <div class="form-group">
                                <label>CONFIRMAR NUEVA CLAVE (*)</label>

                                <div class="input-group">
                                    <div class="input-group-prepend" id="btnVer2">
                                        <button class="btn btn-warning" type="button"><i class="fa fa-eye"></i></button>
                                    </div>
                                    <input type="password" class="form-control input-bold" name="clave_nueva"
                                        id="password2">
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                    </div>
					</form>

                    <div class="row" style="margin-bottom: 1px;">
                        <div class="col-3 col-center">
                            <div class="form-group text-center">
                                <button type="submit" form="actualizar-form" id="btnGuardar" class="btn btn-lg btn-success">
                                    <b>CAMBIAR CLAVE</b></button>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</section>


@endsection
@section('scripts')
@include('layouts/gestion/scripts')
<script>
			$(function(){
				$('#actualizar-form').submit(function(e){
					e.preventDefault();
					
					if($('#password1').val() != $('#password2').val()){
						$.toast({
								heading: 'Error',
								text: 'Las contraseñas deben de ser iguales.',
								position: 'top-right',
								showHideTransition: 'fade',
								icon: 'error'
								});
						return;
					}
					var queryString = "password="+$('#password1').val() + "&tokenEmail={{$tokenEmail}}";
					console.log($('#actualizar-form').attr('action') +"?"+queryString);
					var desRetorno = '';
					$.ajax({
						url: $('#actualizar-form').attr('action'),
						type: 'GET',
						dataType: 'json',
						data: queryString,
						
						success: function(jsonRsp){
							if(jsonRsp.status == "OK"){
								$.toast({
                                    heading: jsonRsp.status,
                                    text: jsonRsp.mensaje,
                                    position: 'top-right',
                                    showHideTransition: 'slide',
                                    icon: 'success'
                                });
                                setTimeout(function(){ window.location = "{{route('login')}}"; }, 3000);
                                
							}else{
								$.toast({
									heading: jsonRsp.status,
									text: jsonRsp.mensaje,
									position: 'top-right',
									showHideTransition: 'fade',
									icon: 'error'
								});
							}
						},
						error: function(jsonRsp){
							alert("Hubo un error");
						},
						complete: function(){
							// window.location = "{{route('login')}}";
						}
						
					});
					
				});
				
			});
			function functionBoton() {
				$('#aceptar').on('click',function(){
					window.location = "{{route('login')}}";
				})

			}	
		</script>
	</body>
</html>














