@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
    <!-- Main content -->
<section id="base-style">
	@include('flash::message') 
    <div class="card" style="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Subir Plan de Cuentas</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body">
			
				
				<div class="row mt-1">
					<div class="col-md-12">
						<form  method="post"  action="{{route('contabilidad.guardarExcel')}}" enctype="multipart/form-data" id="formExcel">
							<div class="row">


								<div class="col-12">
									<div class="form-group">
										<label>Empresa</label>					            	
										<select class="form-control input-sm select2"  name="id_empresa" id="" style="padding-left: 0px;width: 100%;" required>
										@foreach($empresas as $empresa)
											<option value="{{$empresa->id}}">{{$empresa->denominacion}}</option>
										@endforeach
										</select>
									</div>
								</div> 
								<div class="col-12">
									<div class="form-group">
										<label for="exampleFormControlFile1">Subir Plantilla</label>
										<input type="file" class="form-control-file" id="exampleFormControlFile1" name="file" required>
									  </div>
								</div>

							</div>
							
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-success btn-lg" type="submit" id="btn_submit">PROCESAR PLANTILLA</button>
									<br><br><br>
									<small>	<a href="{{url('template_excel/plan_cuentas_basico.xls')}}" download="plan_cuentas_basico.xls" ><i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp; Descargar plantilla plan de cuentas basico </a></small>
									<br>
								</div>
							</div>
						</form>	
					</div>
				
				</div>

            </div>	
        </div>    
    </div>   
</section>



    <!-- /.content -->

    <!-- /.content -->
@endsection
@section('scripts')
	@include('layouts/gestion/scripts')	
	<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.form.min.js')}}"></script>
	<script>
		$(document).ready(function() {
			$('.select2').select2();


			document.getElementById('formExcel').addEventListener('submit', function() {
            // Deshabilitar el botón después de enviar el formulario
            document.getElementById('btn_submit').disabled = true;
        });
		});

		
	</script>
	

@endsection
