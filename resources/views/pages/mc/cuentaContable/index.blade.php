
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
<style type="text/css">
	.negrita{
		font-weight: bold;
	/*	cursor:pointer; 
		cursor: hand;	*/
		/*text-align: center;*/
	}
	.negrita:hover { 
		  background-color: #F0F0F0;
		}
	.rojo{
		color:red;
		font-size: 20px;
	}
	.amarillo{
		color:yellow;
		font-size: 20px;
	}
	 .star_yelllow {
	 	color:#E2D532;
	 	font-size: 20px;
	 	
	 }
	 .star{
	 	font-size: 20px;
	 	color:#c1b8b8;
	 }

	.verde {
		color: green;
		font-size: 20px;
	}

	.bgRed {
		/*background-color: #C48433;*/
		font-size: 15px;

	display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 15px;
    font-weight: 800;
    line-height: 1;
    color: #000;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    border-radius: 10px;
    border: 1px solid black;
    /*background: #e2076a*/
	}

	/*.bgRed:hover{
		color: white;
	}*/

	/*background: #e2076a !important;padding-left: 6px;padding-right: 6px;*/


.center-text {
	text-align: center;
}



</style>

	@parent
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Plan de cuentas</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="card-content collapse show" aria-expanded="true">
			<div class="card-body">

				<form id="formPlanCuenta">
					<div class="row">
						<div class="col-12 col-sm-3 col-md-3">
							<label>Numero Cuenta Actual</label>
							<input type="text" class="form-control" readonly>
						</div>


						<div class="col-12">
							<div class="form-group">
								<label>Cuentas</label>
								<select class="form-control select2" name="id_cuenta" id="id_cuenta"
									style="width: 100%;">
									<option value="G">Grupo</option>

								</select>
							</div>
						</div>


						<div class="col-12 col-sm-3 col-md-3">
							<div class="row">

								<div class="col-9" style="padding: 0 0 0 15px">
									<label>Codigo Cuenta</label>
									<input type="text" id="cod_cuenta" class="form-control" readonly value="0">
								</div>
								<div class="col-3" style="padding: 0">
									<label>Codigo</label>
									<input type="text" class="form-control" name="codigo" id="codigo" value="0">
								</div>

							</div>
						</div>

						<div class="col-12 col-sm-3 col-md-3">
							<label class="control-label">Descripción</label>
							<input type="text" class="form-control" name="denominacion" id="denominacion" value="">
						</div>

						<div class="col-6 col-sm-3 col-md-2">
							<div class="form-group">
								<label>Tipo</label>
								<select class="form-control select2" name="tipo" id="tipo" style="width: 100%;">
									<option value="D">Deudor</option>
									<option value="A">Acreedor</option>

								</select>
							</div>
						</div>

						<div class="col-6 col-sm-3 col-md-2">
							<div class="form-group">
								<label>Asentable</label>
								<select class="form-control select2" name="asentable" id="asentable"
									style="width: 100%;">
									<option value="true">SI</option>
									<option value="false">NO</option>
								</select>
							</div>
						</div>
						<div class="col-6 col-sm-3 col-md-2">
							<div class="form-group">
								<label>Cuenta Gasto</label>
								<select class="form-control select2" name="cuenta_gasto" id="cuenta_gasto" style="width: 100%;">
									 <option value="">Seleccione Cuenta Gasto</option>	
									@foreach($cuentasCostos as $key=>$cuentasCosto)
											@php 	
											if(isset($cuentasCosto->cuentasPadre->denominacion)){
												$planCuentas = ' ('.$cuentasCosto->cuentasPadre->denominacion.')';
											 }else{
												$planCuentas = '';
											 }
											@endphp
											<option value="{{$cuentasCosto->id}}">{{$cuentasCosto->denominacion}} {{$planCuentas}}</option>
									@endforeach
								</select>
							</div>
						</div>

					</div>
				</form>
				<div class="row">
					<div class="col-12">
						<button type="button" id="botonExcel" class="pull-right text-center btn btn-success btn-lg mr-1" style=" margin: 0px;"><b>Excel</b></button>
						<button type="button" onclick="limpiar()" class="btn btn-light btn-lg pull-right mr-1 text-white"><b>Limpiar</b></button>
						<button type="button"  onclick="setDataPlan()" class="btn btn-info pull-right btn-lg mr-1"><b>Guardar</b></button>
					</div>
				</div>



				<table id="listado" class="table table-hover table-condensed nowrap mt-1" style="width: 100%;">
					<thead>
						<tr style="text-align: center;">
							<th>Cod_txt</th>
							<th>Descripción</th>
							<th>Tipo</th>
							<th>Asentable</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>





			</div>
		</div>
	</div>
</section>

	

     <!-- ========================================
   					MODAL EDIT CUENTA
   	========================================  -->		

	<div class="modal fade" id="modalEditCuenta" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog">
			<div class="modal-content" style="width: 180%;">
				<div class="modal-header">
					<h4 class="modal-title" id="" style="font-weight: 800;">Editar Cuenta <i style="display:none;"
						class="fa fa-refresh fa-spin load_err"></i></h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					  </button>
					<span class="msj_err" style="color:#DB6042;"></span>
					<span class="msj_success" style="color:#489946;"></span>
				</div>
				<div class="modal-body">
					<div class="content-fluid">
						<form id="formEditCuenta">
							<div class="row">
								<div class="col-12 col-sm-3 col-md-3">
									<label>CodigoBase</label>
									<input type="text" class="form-control" name="base" disabled id="codigo_base"
										value="">
								</div>
								<div class="col-12 col-sm-3 col-md-3">
									<label>Codigo</label>
									<input type="text" class="form-control" name="codigo" id="codigo_edit"
										value="">
								</div>

								<div class="col-12 col-sm-3 col-md-3">
									<label class="control-label">Descripción</label>
									<input type="text" class="form-control" name="denominacion" id="denominacion_edit"
										value="">
								</div>

								<div class="col-6 col-sm-3 col-md-3" style="height: 74px;">
									<div class="form-group">
										<label>Tipo</label>
										<select class="form-control select2" name="tipo" id="tipo_edit"
											style="width: 100%;">
											<option value="D">Deudor</option>
											<option value="A">Acreedor</option>


										</select>
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3" style="height: 74px;">
									<div class="form-group">
										<label>Asentable</label>
										<select class="form-control select2" name="asentable" id="asentable_edit"
											style="width: 100%;">
											<option value="true">SI</option>
											<option value="false">NO</option>
										</select>
									</div>
								</div>

								<div class="col-6 col-sm-3 col-md-3" style="height: 74px;">
									<div class="form-group">
										<label>Activo</label>
										<select class="form-control select2" name="activo" id="activo"
											style="width: 100%;">
											<option value="true">SI</option>
											<option value="false">NO</option>
										</select>
									</div>
								</div>
								<div class="col-6 col-sm-6 col-md-6">
								<div class="form-group">
									<label>Cuenta Gasto</label>
									<select class="form-control select2" name="cuenta_gasto_edit" id="cuenta_gasto_edit" style="width: 100%;">
										<option value="">Seleccione Cuenta Gasto</option>	
										@foreach($cuentasCostos as $key=>$cuentasCosto)
											@php 	
											if(isset($cuentasCosto->cuentasPadre->denominacion)){
												$planCuentas = ' ('.$cuentasCosto->cuentasPadre->denominacion.')';
											 }else{
												$planCuentas = '';
											 }
											@endphp
											<option value="{{$cuentasCosto->id}}">{{$cuentasCosto->denominacion}} {{$planCuentas}}</option>
										@endforeach
									</select>
								</div>
							</div>


							</div>
							<input type="hidden" name="id" id="id_input_cuenta" value="">
						</form>

					</div>

				</div>
				<div class="modal-footer">
					{{-- <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal"><b>Cerrar</b></button> --}}
					<button type="button" class="btn btn-success btn-lg" id="btnGuardarEditCuenta"><b>Guardar</b></button>
				</div>
			</div>
		</div>
	</div>








@endsection
@section('scripts')
	@include('layouts/gestion/scripts')
	<script>
// $('.sidebar-toggle').trigger('click');
$('.select2').select2();




{{--==========================================
		CONFIG INICIAL DE CARGA
 	==========================================--}}
		consultaAjax();	
		getDataPlan();
		// $('#codigo').prop('disabled',true);





{{--==========================================
		LOGICA DE CONTROL
 	==========================================--}}
	$("#id_cuenta").change(function(){

		let option = $(this).val();
		controlCodigo(option);

		// if(option === 'G'){
		// 	$('#codigo').val('');
		// 	$('#codigo').prop('disabled',false);
		// } else {
		// 	$('#codigo').val('');
			
		// }
		
	});



	function limpiar()
	{
		$('#id_cuenta').val('G').trigger('change.select2');
		$('#codigo').val('');
		$('#denominacion').val('');
	}


	$('.select2').on('select2:select', function (e) {
		$(this).focus();
	
		});



	function controlCodigo(id_cuenta){

		$('#codigo').val('');	
		$("#cod_cuenta").val('');

		$.ajax({
					type: "GET",
					url: "{{ route('controlCodigo') }}",
					dataType: 'json',
					data:{id_cuenta: id_cuenta},
					error: function (jqXHR, textStatus, errorThrown) {
						$.toast({	
								heading: '<b>Atención</b>',
								position: 'top-right', 
								text: 'Ocurrio un error en la comunicación con el servidor.',
								hideAfter: false,
								icon: 'error'
							});
			 		},	
							success: function(rsp){

								if(rsp.data.length != 0 ){
									
									if(rsp.data[0].max != 0){
										$('#codigo').val(Number(rsp.data[0].max) + 1);	
									} else {
										$('#codigo').val('1');	
									}
									
                            	}
								
								if(rsp.cod.length != 0 ){
									$("#cod_cuenta").val(rsp.cod[0].cod_txt);
                            	}
						}
				});	

	}



	
		function setDataPlan(){
				$.ajax({
					type: "GET",
					url: "{{ route('setDataPlanCuenta') }}",
					dataType: 'json',
					data: $('#formPlanCuenta').serialize(),
					error: function (jqXHR, textStatus, errorThrown) {
						$.toast({	
								heading: '<b>Atención</b>',
								position: 'top-right', 
								text: 'Ocurrio un error en la comunicación con el servidor.',
								hideAfter: false,
								icon: 'error'
							});
			 		},
					success: function(rsp){
							if(rsp.data > 0 ){
								consultaAjax();	
								getDataPlan();	
                            } else {
								$.toast({	
									heading: '<b>Atención</b>',
									position: 'top-right', 
									text: rsp.msg,
									hideAfter: false,
									icon: 'warning'
								});
							}
							
						}
				});	
		}//function




		function getDataPlan(){
			let optionSelect = $('#id_cuenta').val();

				$.ajax({
					type: "GET",
					url: "{{ route('getDataPlanCuenta') }}",
					dataType: 'json',
					error: function (jqXHR, textStatus, errorThrown) {
						$.toast({	
								heading: '<b>Atención</b>',
								position: 'top-right', 
								text: 'Ocurrio un error en la comunicación con el servidor.',
								hideAfter: false,
								icon: 'error'
							});
			 		},
					success: function(rsp){

						if(rsp.data.length != 0 ){

                        $('#id_cuenta').empty();

                        var newOption = new Option('Grupo', 'G', false, false);
                        $('#id_cuenta').append(newOption);


                        $.each(rsp.data, function (key, item){
                        var newOption = new Option('( '+item.cod_txt+' ) '+item.descripcion, item.id, false, false);
                        $('#id_cuenta').append(newOption)
                            });

                        $('#id_cuenta').val(optionSelect).trigger('change.select2');
                        controlCodigo(optionSelect);
                        $('#denominacion').val('');

                        }
				}
	});	
		}



		function editCuenta(id){
			$('#modalEditCuenta').modal('show');

					$.ajax({
					type: "GET",
					url: "{{ route('getCuentaInfo') }}",
					dataType: 'json',
					data: { id_cuenta : id},
					
					success: function(rsp){
						if(rsp.data.length != 0){

							var descripcion = rsp.data.cod_txt.split('.');
			
							var contador = descripcion.length - 1;
	
							var codigo = '';
							$.each(descripcion, function (key, item){
								
								if(key < contador){ 
									if(key == 0){
										codigo += item;	
									}else{
										codigo += '.'+item;	
									}
								}
							})	
							
						
							$('#codigo_base').val(codigo);
							$('#codigo_edit').val(rsp.data.codigo);
							$('#denominacion_edit').val(rsp.data.descripcion);
							$("#tipo_edit").select2().val(rsp.data.tipo).trigger("change");
							$("#asentable_edit").select2().val(String(rsp.data.asentable)).trigger("change");
							$("#cuenta_gasto_edit").select2().val(rsp.data.id_cuenta_gasto).trigger("change");
							$('#id_input_cuenta').val(rsp.data.id);

                        }//if
				}//success
	});	

		}

		$('#btnGuardarEditCuenta').on('click',function(){
			$('.load_err').show();
			$('.msj_err').val('');
			$('.msj_success').val('');
			$('#codigo_base').prop('disabled',false);
			$.ajax({
					type: "GET",
					url: "{{ route('saveCuentaInfo') }}",
					dataType: 'json',
					data:  $('#formEditCuenta').serialize(),
					error: function(){
									$('.load_err').hide(2000);
									$('.msj_err').html('Ocurrio un error en la comunicación con el servidor.');
								},

					success: function(rsp){
						$('.load_err').hide(2000);

							if(rsp.err == true){
								$('#modalEditCuenta').modal('hide');
								$('.msj_success').val('Los datos fueron almacenados');
								consultaAjax();
							} else {
									$('.msj_err').html('Ocurrio un error en la comunicación con el servidor.');
							}
				}//success
	});	

		})
		




			function consultaAjax(){

		// $.blockUI({
		// 		                        centerY: 0,
		// 		                        message: "<h2>Procesando...</h2>",
		// 		                        css: {
		// 		                            color: '#000'
		// 		                        }
		// 		                    });

		// RETRASO DE EJECUCION PARA MOSTRAR BLOCK UI
		// setTimeout(function (){

					
			 table =   $('#listado').DataTable({
					        "destroy": true,
					        "select": true,
					        "pageLength": 50,
					        "ajax": {
					        		data: { "formSearch": $('#filtroProforma').serializeArray() },
						            url: "{{ route('getDataPlanCuenta') }}",
						            type: "GET",
						    error: function(jqXHR,textStatus,errorThrown){

                           $.toast({
                            heading: 'Error',
                            text: 'Ocurrio un error en la comunicación con el servidor.',
                            position: 'top-right',
                            showHideTransition: 'fade',
                            icon: 'error'
                        });
                            // $.unblockUI();
                        }
                        		// formatter.format(parseFloat(x.total_venta_proforma));
						  },
						 "columns":[
							 		// {data: 'codigo'},
							 		{data: 'cod_txt'},
							        {data: 'descripcion'},
							        {data: function(x){
							        	let tipo ='';
							        	if(x.tipo === 'D'){tipo = 'DEUDOR';}
							        		if(x.tipo === 'A'){tipo = 'ACREEDOR';}
							        			if(x.tipo === 'T'){tipo = 'AMBOS';}
							        	return tipo;			
							        }},
							        {data: function (x){
							        	let data = '';
							        	
							        	if(x.asentable === true){data = 'SI';}
							        		if(x.asentable === false){data = 'NO';}	
							        	return data;	
							        }},
							         {data: function (x){
							        	return `<button class="btn btn-info" title="Editar"  onclick="editCuenta(${x.id})" type="button""><i class="fa fa-edit"></i></button>` ;	
							        	// <i class="fa fa-fw fa-trash"></i>
							        }}
						           ],
						 "columnDefs":[ 
									// {
									// 	targets: 0,
									// 	className: 'hover',
									// 	width: "20%",
									// 	className: "center-text"
									// },
									{
										targets: [2,3,4],
										className: "center-text"
									}
									  ],
										"aaSorting":[[0,"asc"]],
						     "createdRow": function ( row, data, index ) {
						     	
						        	$(row).addClass('negrita');
						        	
						        				}			
						                    
					    }).on('xhr.dt', function ( e, settings, json, xhr ){
								//Ajax event - fired when an Ajax request is completed.;
							 // $.unblockUI();
								});
					    
			 
					    	// }, 300);

		$("#botonExcel").on("click", function(e)
		{ 
			e.preventDefault();
			$('#formPlanCuenta').attr('method','post');
			$('#formPlanCuenta').attr('action', "{{route('generarExcelPlan')}}").submit();
		});

		

}//function


	

		




	</script>
@endsection