@extends('masters')

@section('title', 'Búsqueda de hoteles')
@section('styles')
	@parent
@endsection

@section('content')
<section class="sectiontop">
	<div class="container">
                <form action="" method="GET">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 alert-danger">
							<!-- En caso de error-->
                        </div>
                        <div id="main" class="col-sm-12 col-md-12" class="sectiontop">
                            <div class="booking-information travelo-box"> 
                                <h2>Error de Tiempo de Conexión</h2>
                                <hr />
                                <div class="booking-confirmation clearfix">
                                    <i class="glyphicon glyphicon-warning-sign icon circle"></i>
                                    <div class="message">
                                        <h4 class="main-message">Se a producido un error en el tiempo de conexión con el proveedor.</h4>
                                        <p>Intentelo nuevamente actualizando la página.</p>
                                    </div>
                                </div>
                                </div>
                                <hr />
							</div>
                        </div>
                    </div>
                </form>
    </div>
</section>
@endsection

@section('scripts')
    @parent
@endsection
