@extends('master')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
@endsection
<?php
include("../comAerea.php");
include("../destinationFligth.php");
?>
@section('content')

<section id="content" class="gray-area">
    <div class="container">
        <div id="main">
            @include('flash::message')
            <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large; margin-top: 5%">Próximos Vencimientos</h1>
            <br>
            <div class="block sectiontop">
				<ul class="nav nav-tabs">
					  <li class="active"><a data-toggle="tab" href="#home">Hoteles</a></li>
					 <!--<li><a data-toggle="tab" href="#menu1">Vuelos</a></li>-->
				</ul>
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
							<br>
                            <br>
                            <form action="" id="frmBusqueda" method="post" style="margin-left: 1%; margin-right: 2%;"> 
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                        <input type="hidden" class="input-text full-width" id="cant_noche" name="cant_noche" />
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Proforma</label>
                                                <input type="number" placeholder="N° de Expediente" class="input-text full-width" id="nrodeexpediente" name="idFileReserva" value="0">
                                            </div>
											<div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>N°. Reserva</label>
                                                <input type="number" placeholder="N° de Reserva" class="input-text full-width" id="nroreserva" name="nroreserva" value="0">
                                            </div>
											<div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Localizador</label>
                                                <input type="text" placeholder="Localizador" class="input-text full-width" id="localizador" name="localizador"/>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Desde</label>
                                                <div class="datepicker-wrap">
                                                    <input type="text" value="" name="desde" id="desde" class="input-text full-width required fecha"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Hasta</label>
                                                <div class="datepicker-wrap">
                                                    <input type="text" value="" name="hasta" id="hasta" class="input-text full-width required fecha"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Pasajero Principal</label>
                                                <input type="text" placeholder="Pasajero Principal" class="input-text full-width" id="pasajeroPrincipal" name="pasajeroPrincipal">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                        <div class="col-md-12">
                                            <label class="hidden-xs">&nbsp;</label>
                                            <button type="button" style="width: 160px;  background-color: #e2076a;" id= "buscarReservas" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-search"></i> Buscar Reservas</button>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="hidden-xs">&nbsp;</label>
                                            <button id="limpiarFiltros" style="width: 160px;" type="reset" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                                        </div>
                                        <div class="col-md-12">
                                             <label class="hidden-xs">&nbsp;</label>
                                            <button type="button" style="width: 160px; background-color: #fdb714;" id="botonExcel" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="fa fa-file-excel-o"></i> Exportar a Excel</span></button>

                                        </div>
										
                                    </div>
								</div>
								<div class="row">
										<h3 class="tab-content-title">Referencias</h3>
										<div class="col-xs-12 col-sm-2 col-md-2">
											<span class="cancelarReserva"><i class="fa fa-times fa-lg"></i> Cancelar Reserva</span>
										</div>
										<div class="col-xs-12 col-sm-2 col-md-2">
											<span class="descargarVoucher"><i class="fa fa-file-pdf-o fa-lg"></i> Descargar Voucher</span>
										</div>
										<?php 
											echo '	<div class="col-xs-12 col-sm-2 col-md-2">
														<i class="fa fa-file-text-o fa-lg"></i>Detalle de Reserva
													</div>
                                                	<div class="col-xs-12 col-sm-2 col-md-2">
                                                    	<i class="fa fa-pencil-square-o fa-lg"></i> Modificar Monto Cobrado
                                                	</div>	';
										?>
								</div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas" style= "overflow-x: scroll; overflow-y: hidden;">
										<div class="table-responsive">
											<br>
												<h1 align="center">Listado de Reservas</h1>
											<br>
											<table id="reservas" class="table table-bordered table-hover">
												<thead>
													<tr>
														<th>&nbsp;&nbsp;&nbsp;</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Pago Cancelacion Desde</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Localizador</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Proforma</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Destino</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Fecha Res.</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Pasajero</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Agencia</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Vendedor Agencia</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Vendedor DTP</th>
														<th style="color: #ffffff;background-color: #2d3e52;">Monto Cancelacion</th>
														@if($isDtp)<th style="color: #ffffff;background-color: #2d3e52;">Precio sin Comision</th>@endif	<!-- precio sin comision (solo para dtp)--> 
														<th style="color: #ffffff;background-color: #2d3e52;">Precio Venta</th>	<!-- monto cobrado-->
														<th style="color: #ffffff;background-color: #2d3e52;">Accion</th>
													</tr>
												</thead>
												<tbody>
													@foreach($reservas as $key=>$reserva)
														@php
															switch ($reserva->estado) {
																//Pendiente
																case config('constants.resPendiente'):
																    echo "<tr class='alert-info' style='width:80px'>";
																    break;
																//Aprobado
																case config('constants.resConfirmada'):
																    echo "<tr class='alert-success' style='width:80px'>";
																    break;
																//Cancelado    
																case config('constants.resEliminada'):
																	echo  "<tr class='alert-danger' style='width:80px'>";
																	break;
																//Rechazado	
																case config('constants.resRechazada'):
																	echo "<tr class='alert-warning' style='width:80px'>";
																	break;
																//Error	
																case config('constants.resError'):
																	echo "<tr class='alert-error' style='width:80px'>";
																	break;
																}
														@endphp
														<td>
															@if($reserva->iconoproforma == 0)
																<i class='fa fa-check' style='color:green' title='Proforma enviada satisfactoriamente' aria-hidden='true'></i>
															@else
																<i class="fa fa-exclamation-triangle" title='Proforma no enviada satisfactoriamente' style='color:#fdb714'  aria-hidden="true"></i>
															@endif	
															@if($reserva->iconocancelar == 1)
																<i class="fa fa-exclamation-triangle" title='Proforma no cancelada satisfactoriamente' style='color:#fdb714'  aria-hidden="true"></i>
															@else
																@if($reserva->iconocancelar == 2)
																	<i class='fa fa-check' style='color:green' title='Proforma cancelada satisfactoriamente' aria-hidden='true'></i>
																@endif	
															@endif
															@if($isDtp)
															&nbsp;&nbsp;&nbsp;<img alt="" style="width: 90px;" src="images/proveedores/{{$reserva->id_proveedor}}.png">
															@endif
														</td>
														<td>{{$reserva->cancelacion_desde}}</td>
														<td>{{$reserva->localizador}}</td>
														<td>{{$reserva->proforma}}</td>
														<td>{{$reserva->destino}}</td>
														<td>{{$reserva->fecha}}</td>
														<td><b>{{$reserva->pasajero}}</b></td>
														<td>{{$reserva->agencia}}</td>
														<td>{{$reserva->vendedor_agencia}}</td>
														<td>{{$reserva->vendedor_dtp}}</td>
														<td>{{$reserva->cancelacion_monto}}</td>
														@if($isDtp)<td>{{$reserva->monto_sin_comision}}</td>@endif
														<td>{{$reserva->monto_cobrado}}</td>
														<td>
														@if($reserva->estado == config('constants.resConfirmada')&& $reserva->iconoproforma == 0)
															<a onclick='cancelarBusqueda({{$reserva->id_reserva}})' id="reserva_{{$reserva->id_reserva}}"' class='fontCancelar' title='Cancelar Reserva'><i class='fa fa-times'></i></a>&nbsp;&nbsp;
															<a href="{{route('detallesReserva', ['id' =>$reserva->id_reserva])}}" target="_blank"><i class="fa fa-file-text-o fa-lg"></i></a>
														@else
															<a href="{{route('detallesReserva', ['id' =>$reserva->id_reserva])}}" target="_blank"><i class="fa fa-file-text-o fa-lg"></i></a> 												
														@endif	
														</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
                                    </div>
                                </div>
								<div class="row" id="totalReservas">
								</div>
                            </form>
					</div>
					<div id="menu1" class="tab-pane fade">
						<br>
                        <br>
						<form action="" id="frmBusquedaFlight" method="post" style="margin-left: 1%; margin-right: 2%;">
						    <div class="row">
                                <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                    <input type="hidden" class="input-text full-width" id="cant_noche" name="cant_noche"/>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>N° Control</label>
                                            <input type="number" placeholder="N° de Control" class="input-text full-width" id="nrodeexpediente" name="idFileReserva" >
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Origen</label>
                                            <select data-placeholder="Origen" required class="chosen-select-deselect destination text-center" id="origen" name="origen">
                                            	<option value=""></option>
											</select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Destino</label>
				                             <select data-placeholder="Destino" class="chosen-select-deselect destination text-center" id="destinoF" required name="destino">
				                                 <option value=""></option>
											 </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Compañia</label>
				                             <select data-placeholder="Compañia Aerea" class="chosen-select-deselect destination text-center" id="companhia" required name="companhia">
				                                 <option value=""></option>
				                                 @foreach($listadoComp as $key=>$listado)
				                                 	<option value="{{$key}}">{{$listado['label']}}</option>
				                                 @endforeach
											 </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button type="button" style="width: 160px;  background-color: #e2076a;" id= "buscarReservasFlight" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-search"></i> Buscar Reservas</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button id="limpiarFiltros" style="width: 160px;" type="reset" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button type="button" style="width: 160px; background-color: #fdb714;" id="botonExcel" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="fa fa-file-excel-o"></i> Exportar a Excel</span></button>
                                    </div>
                                </div>
							</div>
						    <div class="row">
	                            <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas" style= "overflow-x: scroll; overflow-y: hidden;">
											<div class="table-responsive">
												<br>
													<h1 align="center">Listado de Reservas</h1>
												<br>
												<table id="reservasFligth" class="table table-bordered table-hover" style="margin-left: 1%; margin-right: 3%;">
													<thead>
														<tr class='bgblue'>
															<th>Id</th>
															<th>Fecha</th>
															<th>N° Control</th>
															<!--<th>Proforma N°</th>-->
															<th>Fecha Origen</th>
															<th>Origen</th>
															<th>Fecha Destino</th>
															<th>Destino</th>
															<th>Agencia</th>
															<th>Compañia de Reserva</th>
															<th>Total</th>
															<th>Accion</th>
														</tr>
													</thead>
												<tbody>
													@foreach($resultsFligth as $key=>$reservaFligth)
													<tr style='width:50px'>
															<td><b>{{$reservaFligth->id}}</b></td>
															<td>{{$reservaFligth->fecha_reserva}} {{date("G:i",strtotime($reservaFligth->hora_reserva))}}</td>
															<td><b>{{$reservaFligth->controlnumber}}</b></td>
															<td>{{$reservaFligth->fecha_origen}}</td>
															<td>{{$destination[$reservaFligth->cod_origen]}}</td>
															<td>{{$reservaFligth->fecha_destino}}</td>
															<td>{{$destination[$reservaFligth->cod_destino]}}</td>
															<td>{{$reservaFligth->agencia->razon_social}}</td>
															<td><img src="https://images.kiwi.com/airlines/64/{{$reservaFligth->marketingcompany}}.png" class="imageHead"> 
															<?php 
																$marketingcompany= explode("|", $comAerea[$reservaFligth->marketingcompany])
															?>
															 {{$marketingcompany[0]}} - {{$marketingcompany[3]}} </td>
															<td>{{$reservaFligth->total_monto_vuelo}}</td>
															<td>
																<a href="{{route('detallesReservaFligth', ['id' =>$reservaFligth->id])}}" target="_blank"><i class="fa fa-file-text-o fa-lg"></i></a>	
															</td>
													</tr>
												@endforeach
											</tbody>
										</table>
									</div>
	                             </div>
	                        </div>
	                    </form>    
					</div>
				</div>
            </div>
        </div>
    </div>
	
</section>
@endsection

@section('scripts')
	@include('layouts/scripts')
		<script>
		$("#desde").datepicker();
		$("#hasta").datepicker();
  		$('#reservas').DataTable({	
					        "aaSorting": [[1, "desc"]]
					    });		startProcess();
		function startProcess(){
			$("#buscarReservas").click(function(){
				var dataString = $("#frmBusqueda").serialize();
				$.ajax({
					type: "GET",
					url: "{{route('getReservasCancelacion')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
									$.unblockUI();
									var oSettings = $('#reservas').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();

									for (i=0;i<=iTotalRecords;i++) {
										$('#reservas').dataTable().fnDeleteRow(0,null,true);
									}
									$.each(rsp, function (key, item){
										if(item.estado == {{config('constants.resConfirmada')}}){
											var accion = `<a onclick='cancelarBusqueda(`+rsp[key].id_reserva+`)' id='reserva_`+item.id_reserva+`' class='fontCancelar' title='Cancelar Reserva'><i class='fa fa-times'></i></a>&nbsp;&nbsp;<a href='{{url("detallesReserva")}}/`+rsp[key].id_reserva+`' target='_blank'><i class='fa fa-file-text-o fa-lg'></i></a>`;
										}else{
											//var accion = "<a href='../public/detallesReserva/"+rsp[key].id_reserva+"' target='_blank'><i class='fa fa-file-text-o fa-lg'></i></a>";
											var accion = `<a href='{{url("detallesReserva")}}/`+rsp[key].id_reserva+`' target='_blank'><i class='fa fa-file-text-o fa-lg'></i></a>`;
										}
										if(item.iconoproforma == 0){
											var iconoproforma = "<i class='fa fa-check' style='color:green' title='Proforma enviada satisfactoriamente' aria-hidden='true'></i>";
										}else{
											var iconoproforma = "<i class='fa fa-exclamation-triangle' title='Proforma no enviada satisfactoriamente' style='color:#fdb714' aria-hidden='true'></i>";
										}

										if(item.iconocancelar == 1){
											var iconocancelar ='<i class="fa fa-exclamation-triangle" title="Proforma no cancelada satisfactoriamente" style="color:#fdb714" aria-hidden="true"></i>';
										}
										else{
											if(item.iconocancelar == 2 ){
												var iconocancelar ="<i class='fa fa-check' style='color:green' title='Proforma cancelada satisfactoriamente' aria-hidden='true'></i>";
											}else{
												var iconocancelar ="";
											}	
										}
										//var totalIconos =  iconoproforma+""+iconocancelar;
										var totalIconos = iconoproforma+" "+iconocancelar+`&nbsp;&nbsp;&nbsp;<img alt="" style="width: 90px;" src="images/proveedores/`+ item.id_proveedor+ `.png">`;

								        var dataTableRow = [
															totalIconos,
															'<b>'+item.cancelacion_desde+'</b>',
															'<b>'+item.localizador+'</b>',
															item.proforma,
															item.destino,
															item.fecha,
															item.pasajero,
															item.agencia,
															item.vendedor_agencia,
															item.vendedor_dtp,
															item.cancelacion_monto,
															//item.monto_cobrado, //es cancelacion	(habia un tal monto por aca tambien)
													@if($isDtp)	item.monto_sin_comision,	@endif
															item.monto_cobrado,
															accion
															];

										var newrow = $('#reservas').dataTable().fnAddData(dataTableRow);

										// set class attribute for the newly added row 
										var nTr = $('#reservas').dataTable().fnSettings().aoData[newrow[0]].nTr;
										$('td',$('#reservas').dataTable().fnSettings().aoData[newrow[0]].anCells[0]).addClass('alert-error');
										// and parse the row:
										var nTds = $('td', nTr);
										//nTds.style('width', .css("background-color"););
										switch (item.estado) {
											//Pendiente
											case 1:
												nTds.addClass('alert-info');
												break;
											//Aprobado
											case 2:
												nTds.addClass('alert-success');
												break;
											//Cancelado    
											case 3:
												nTds.addClass('alert-danger');
												break;
											//Rechazado	
											case 4:
												nTds.addClass('alert-warning');
												break;
											//Error	
											case 5:
												nTds.addClass('alert-error');
												break;
										}
									});
					},
				});	
			});

		}

		function cancelarBusqueda(idReserva){
		        BootstrapDialog.confirm({
		            title: 'DTP',
		            message: 'Seguro desea cancelar la Reserva?',
		            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
		            closable: true, // <-- Default value is false
		            draggable: true, // <-- Default value is false
		            btnCancelLabel: 'No', // <-- Default value is 'Cancel',
		            btnOKLabel: 'Si, cancelar Reserva', // <-- Default value is 'OK',
		            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
		            callback: function(result) {
		                // result will be true if button was click, while it will be false if users close the dialog directly.
		                if(result) 
						{
							
		                    $.blockUI({
		                        centerY: 0,
		                        message: "<h2>Procesando...</h2>",
		                        css: {
		                            color: '#000'
		                        }
		                    });
							
						   	$.ajax({
								type: "GET",
								url: "{{route('getCancelarReservas')}}",
								dataType: 'json',
								data: {
							            dataReserva: idReserva
			       					 },
								success: function(rsp){
										$.unblockUI();
										console.log(rsp);
			                            if(rsp.codRetorno == 0)
			                            {
			                           		startProcess()
			                                $("#buscarReservas").trigger('click');
			                                BootstrapDialog.show({
			                                    title: 'DTPMUNDO',
			                                    message: " La reserva ha sido cancelada con éxito. "
			                                });
			                            }
			                            else
			                            {
			                                BootstrapDialog.show({
			                                    title: 'DTPMUNDO',
			                                    message: rsp.desDetalle,
			                                    type: BootstrapDialog.TYPE_DANGER // <-- Default value is BootstrapDialog.TYPE_PRIMARY
			                                });
			                            }
									}

								})
		                }else {
		                    return false;
		                }
		            }
		        });
		}
		$('.nav-tabs a').on('shown.bs.tab', function(event){
		    var x = $(event.target).text();         // active tab
		    var y = $(event.relatedTarget).text();  // previous tab
		});

	</script>
@endsection