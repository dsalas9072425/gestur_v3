@extends('master')

@section('title', 'Búsqueda de hoteles')
@section('styles')
	<link rel="stylesheet" href="{{ URL::asset('css/select2.min.css') }}" media="screen" >
	@parent
	<style>
	.filtro span {
    	color: #666;
	}
	.sort-by-section li {
      		padding: 0px;
      	}
	.select2-container--default .select2-selection--multiple:before {
						content: ' ';display: block;position: absolute;border-color: #888 transparent transparent transparent;border-style: solid;border-width: 5px 4px 0 4px;height: 0;right: 6px;margin-left: -4px;margin-top: -2px;top: 50%;width: 0;cursor: pointer}
	.case-block .inner-box .overlay-box .overlay-inner .search-box {
			    position: absolute;
			    left: 30%;
			    top: 50%;
			    width: 60%;
			    height: 50px;
			    color: #ffffff;
			    line-height: 48px;
			    font-size: 20px;
			    text-align: center;
			    border-radius: 0%;
			    margin-left: -25px;
			    margin-top: -45px;
			    display: inline-block;
			    background-color: rgba(255,255,255,0.30);
	}	
	.select2-container--default .select2-selection--single{
			    border: 1px solid #aaa;
			    height: 36.992188px;
	}	
	.form-control{
		height: 24px;
	}	
	.picture { 
				border:10px solid #e2076a;
				border-right-width: 0px;
    			border-bottom-width: 0px;
    			border-top-width: 0px;

			}	
	.pictures { 
				border:10px solid #1c2b39;
				border-right-width: 0px;
    			border-bottom-width: 0px;
    			border-top-width: 0px;

			}	
		
	</style>
@endsection

@section('content')
	@include('flash::message')
	<div class="container">
		<!-- Modal Habitaciones -->
		<div class="modal fade" id="Habitaciones" role="dialog">
			<div class="modal-dialog">
			  <!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header" style="padding:15px 30px;">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4><span class="glyphicon glyphicon-bed"></span> Agregar Habitaciones</h4>
					</div>
					<div class="modal-body">
						<div class="clearfix"></div>
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div id="main">
			<div class="row">
				<div class="col-md-12 col-sm-12 nueva-busqueda" style="background-color:transparent;box-shadow: none;">
					<div id="cantidadResultados" style="color: #e2076a;"></div>
				</div>
		        <div class="sh-filtro">
					<a data-toggle="collapse" class="sh-filter" data-target="#filtro-buscador">Modificar Búsqueda <i class="fa fa-angle-down" aria-hidden="true"></i></a>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-3 filtro collapse" id="filtro-buscador" style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
				<br/>
				 <form role="form" id="filtro-formulario">
					<input type="hidden" name="filter" value="yes" />
					<!--<input type="hidden" id="maximoReal"/>
					<input type="hidden" id="minimoReal"/>-->
					<h2 style="font-size: 17px; font-weight: normal;" class="search-results-title filtrotitle"><i class="fa fa-filter" aria-hidden="true"></i> Filtrar búsqueda</h3>
					<div class="row">
						<br>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
							<div class="col-xs-12 col-sm-6 col-md-6 form-group" style="margin-top: 5%; padding-left: 5px;">
								<h5>PROMOCIONES</h5>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 form-group" style="margin-top: 5%;">
								<input type="checkbox" class="form-control filtroCheck" name="promociones">
							</div>
						</div>	
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
							<div class="col-xs-12 col-sm-6 col-md-6 form-group" style="margin-top: 5%; padding-left: 5px;">
								<h5">SUGERIDOS</h5>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 form-group" style="margin-top: 5%;">
								<input type="checkbox" class="form-control filtroCheck" name="sugeridos">
							</div>
    					</div>

						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
						<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Categoria</h5>
							<div id="categorias"></div>
							<input type="hidden" name="filtroCategoriaInput" id="filtroCategoriaInput" class="form-control marca">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
						<br>
						<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Paises</h5>
							<div id="paises"></div>
			                <input type="hidden" name="filtroPaisesInput" id="filtroPaisesInput" class="form-control marca">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
						<br>
						<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Destinos</h5>
							<div id="destinos"></div>
			                <input type="hidden" name="filtroDestinoInput" id="filtroDestinoInput" class="form-control marca">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
						<br>
						<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Periodos</h5>
							<div id="periodos"></div>
			                <input type="hidden" name="filtroPeriodosInput" id="filtroPeriodosInput" class="form-control marca">
						</div>
					</form>
					</br>
					</br>
					</br>
					<div class="row">
					</div>	
						<div class="col-xs-12 col-sm-3 col-md-3 form-group" style="margin-top: 5%;">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 form-group" style="margin-top: 5%;">
							<br>
							<button id="btnVerTodos" class="btn btn-info full-width" style="background-color: #e2076a;" type="button">Ver todos</button>
							<!--<button id="btnFiltro" type="button">Remover Filtro</button>-->
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-8 col-md-9 resultados" style="padding-right: 0px;">
					<div class="hotel-list listing-style3 hotel" id = "resultadosBusqueda" style="height: 100%;">
				            <div class="auto-container" style="margin-top: 2%;">
				                <!--Sortable Gallery-->
				                <div class="mixitup-gallery">
				                    <!--Filter-->
				                    <div class="filter-list row clearfix">
				                        <!--Case Block-->
				                    </div> 
				                </div>
				            </div>
					</div>
				</div>	
				</div>
					<!--<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 text-center">
							<ul class="pagination">
								<li><a href="#" id ="btnAtras">« Anterior</a></li>
								<li><a href="#" id="btnNext">Siguiente »</a></li>
							</ul>
						</div>
					</div>-->
				</div>
			</div>
		</div>
	
	</div>

@endsection

@section('scripts')
    @parent
	<script type="text/javascript" src="{{ URL::asset('js/search/filtro-buscador.js') }}"></script>
 	<script type="text/javascript" src="{{ URL::asset('js/select2.min.js')}}"></script>
 	<script type="text/javascript" src="{{ URL::asset('css/jquery.ui.autocomplete.scroll.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/paquetes/jquery-ui.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/paquetes/jquery.fancybox.pack.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/paquetes/jquery.fancybox-media.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/paquetes/owl.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/paquetes/appear.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/paquetes/mixitup.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/paquetes/wow.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/paquetes/script.js')}}"></script>
	<script>
			inicio();
			function inicio(){
				$.ajax({
						type: "GET",
						url: "{{route('doBuscarPaquete')}}",
						dataType: 'json',
						//data: dataString,
						success: function(rsp){
											$(".mixitup-gallery").html("");
											var selectCategoria = '<select class="form-control select2" name="filtroCategoria" multiple id="filtroCategoria" style="width: 100%;">';
												selectCategoria += '<option value="0"></option>';
												$.each(rsp.filtros.categoria, function(index,categorias){	
													selectCategoria += '<option value="'+categorias.id+'">'+categorias.denominacion+'</option>';
												})	
												selectCategoria += '</select>';
											$("#categorias").html(selectCategoria);	

											var selectDestinos = '<select class="form-control select2" name="filtroCategoria" id="filtroDestino" multiple style="width: 100%;">';
												selectDestinos += '<option value="0"></option>';
												$.each(rsp.filtros.destino, function(index,destinos){	
													selectDestinos += '<option value="'+destinos.id+'">'+destinos.destino+' - '+destinos.pais+'</option>';
												})	
												selectDestinos += '</select>';
											$("#destinos").html(selectDestinos);	

											var selectPaises = '<select class="form-control select2" multiple name="filtroPaises" id="filtroPaises" style="width: 100%;">';
												selectPaises += '<option value="0"></option>';
												$.each(rsp.filtros.pais, function(index,paises){	
													selectPaises += '<option value="'+paises.id+'">'+paises.nombre+'</option>';
												})	
												selectPaises += '</select>';
											$("#paises").html(selectPaises);	

											var selectPeriodos = '<select class="form-control select2" name="filtroPaises" id="filtroPeriodos" style="width: 100%;">';
												selectPeriodos += '<option value="0"></option>';
												$.each(rsp.filtros.periodos, function(index,periodos){	
													selectPeriodos += '<option value="'+periodos.periodo+'">'+periodos.periodo+'</option>';
												})	
												selectPeriodos += '</select>';
											$("#periodos").html(selectPeriodos);

											var cantidad = rsp.paquetes.length;
											$("#cantidadResultados").html('<h1 class="subtitle hide-medium" style="text-align: center;font-size: x-large;color: #e2076a;">'+cantidad+' PAQUETES ENCONTRADOS</h1>');
											var selectPaquetes = '<div class="filter-list row clearfix">';
											$.each(rsp.paquetes, function(index,paquetes){
												selectPaquetes+='<div class="case-block mix all '+paquetes.id+' col-md-4 col-sm-6 col-xs-12">';
				                                selectPaquetes+='<div class="inner-box">';
				                                selectPaquetes+='<div class="image">';
					                            baseImagen = paquetes.imagenes.length;
				                                if(baseImagen != 0){
						                            $.each(paquetes.imagenes, function(index,imagen){
						                            	if(paquetes.promocion == true){
						                            		selectPaquetes+='<div class="pictures">';
						                            	}else{
						                            		selectPaquetes+='<div>';
						                            	}	
						                            	if(paquetes.sugerido == true){
						                            		selectPaquetes+='<img class="picture" src="uploads/'+paquetes.imagenes[0].url+'" style="vspace="16"/></div>';
						                            	}else{
						                            		selectPaquetes+='<img src="uploads/'+paquetes.imagenes[0].url+'" style="vspace="16"/></div>';
						                            	}	
					                            	})	
					                            }else{
						                            images="{{asset('images/noDisponible.png')}}";
					                                selectPaquetes+='<img src="'+images+'"/>';	
					                            }	
				                                selectPaquetes+='<div class="overlay-box">';
				                                selectPaquetes+='<div class="overlay-inner"><br>';
				                                selectPaquetes+='<a href="#" class="search-box lightbox-image"><span>'+number_format(paquetes.precio, 2, ',', '.')+' '+paquetes.moneda+'</span></a>'
				                                selectPaquetes+='<a target="_blank" href="paqueteDetalles/'+paquetes.id+'" class="case-link">VER PAQUETE</a>';
				                                selectPaquetes+='</div>';
				                                selectPaquetes+='</div>';
				                                selectPaquetes+='</div>';
				                                selectPaquetes+='<div class="lower-content">';
				                                selectPaquetes+='<h3><a target="_blank" href="paqueteDetalles/'+paquetes.id+'">'+paquetes.titulo+'</a></h3>';
				                                selectPaquetes+='</div>';
				                                selectPaquetes+='</div>';
				                                selectPaquetes+='</div>';  
											})
											selectPaquetes += '</div>';

	 										$('.mixitup-gallery').append(selectPaquetes);
	                                        $('.filter-list').mixItUp({});
	                                        $('.filter-list').mixItUp('_refresh');
	                                        $('#filtroDestino').select2();
	                                        $('#filtroCategoria').select2();
	                                        $('#filtroPeriodos').select2();
	                                        $('#filtroPaises').select2();
	                                        filtro();

										}
					})
			}

			$('#btnVerTodos').on("click", function(e){
				inicio();
			})	
			function filtro(){

				$(".select2").change(function(){
					sid = $(this).attr("id");
					console.log(sid);
					$('#'+sid+'Input').val($(this).select2("val"));
					var dataString = $("#filtro-formulario").serialize();
					$.ajax({	
							type: "GET",
							url: "{{route('doFiltroPaquete')}}",
							dataType: 'json',
							data: dataString,
							success: function(rsp){
										$(".mixitup-gallery").html("");
										$('.filter-list').mixItUp({});
										var selectPaquetes = '<div class="filter-list row clearfix">';
										var cantidadRes = 0;
										if($.isEmptyObject(rsp) == false){
											if(rsp.paquetes.length != 0){
												$.each(rsp.paquetes, function(index,paquetes){
													cantidadRes  = cantidadRes+1;
													selectPaquetes+='<div class="case-block mix all '+paquetes.id+' col-md-4 col-sm-6 col-xs-12">';
					                                selectPaquetes+='<div class="inner-box">';
					                                selectPaquetes+='<div class="image">';
					                                if(paquetes.imagenes.length !== 0) {
					                                	if(paquetes.promocion == true){
						                            		selectPaquetes+='<div class="pictures">';
						                            	}else{
						                            		selectPaquetes+='<div>';
						                            	}	
						                            	if(paquetes.sugerido == true){
						                            		selectPaquetes+='<img class="picture" src="uploads/'+paquetes.imagenes[0].url+'" style="vspace="16"/></div>';
						                            	}else{
						                            		selectPaquetes+='<img src="uploads/'+paquetes.imagenes[0].url+'" style="vspace="16"/></div>';
						                            	}
					                                }else{
					                                	images="{{asset('images/textura.jpg')}}";
					                                    selectPaquetes+='<img src="'+images+'"/>';
					                                }
					                                selectPaquetes+='<div class="overlay-box">';
					                                selectPaquetes+='<div class="overlay-inner">';
					                                selectPaquetes+='<a href="#" class="search-box lightbox-image"><span>'+number_format(paquetes.precio, 2, ',', '.')+' '+paquetes.moneda+'</span></a>'
					                                selectPaquetes+='<a target="_blank" href="paqueteDetalles/'+paquetes.id+'" class="case-link">VER PAQUETE</a>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='<div class="lower-content">';
					                                selectPaquetes+='<h3><a target="_blank" href="paqueteDetalles/'+paquetes.id+'">'+paquetes.titulo+'</a></h3>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='</div>';  
												})
											}else{
												cantidadRes = 0;
											}	
										}else{
											cantidadRes = 0;
										}	
										selectPaquetes += '</div>';
 										$('.mixitup-gallery').append(selectPaquetes);
                                        $('.filter-list').mixItUp({});
                                        $('.filter-list').mixItUp('_refresh');
                                        if(cantidadRes != 0){
	                                        $("#cantidadResultados").html('<h1 class="subtitle hide-medium" style="text-align: center;font-size: x-large;color: #e2076a;">'+cantidadRes+' PAQUETES ENCONTRADOS</h1>');
	                                    }else{
											$("#cantidadResultados").html('<h1 class="subtitle hide-medium" style="text-align: center;font-size: x-large;color: #e2076a;">NO HAY PAQUETES SOBRE ESTOS FILTROS</h1>');
	                                    }   
							}
						 })	
				})

				$(".filtroCheck").on("click", function(e){
					var dataString = $("#filtro-formulario").serialize();
					$.ajax({	
							type: "GET",
							url: "{{route('doFiltroPaquete')}}",
							dataType: 'json',
							data: dataString,
							success: function(rsp){
										$(".mixitup-gallery").html("");
										$('.filter-list').mixItUp({});
										var selectPaquetes = '<div class="filter-list row clearfix">';
										var cantidadRes = 0;
										if($.isEmptyObject(rsp) == false){
											if(rsp.paquetes.length != 0){
												$.each(rsp.paquetes, function(index,paquetes){
													cantidadRes  = cantidadRes+1;
													selectPaquetes+='<div class="case-block mix all '+paquetes.id+' col-md-4 col-sm-6 col-xs-12">';
					                                selectPaquetes+='<div class="inner-box">';
					                                selectPaquetes+='<div class="image">';
					                                if(paquetes.imagenes.length !== 0) {
					                                	if(paquetes.promocion == true){
						                            		selectPaquetes+='<div class="pictures">';
						                            	}else{
						                            		selectPaquetes+='<div>';
						                            	}	
						                            	if(paquetes.sugerido == true){
						                            		selectPaquetes+='<img class="picture" src="uploads/'+paquetes.imagenes[0].url+'" style="vspace="16"/></div>';
						                            	}else{
						                            		selectPaquetes+='<img src="uploads/'+paquetes.imagenes[0].url+'" style="vspace="16"/></div>';
						                            	}
					                                }else{
					                                	images="{{asset('images/textura.jpg')}}";
					                                    selectPaquetes+='<img src="'+images+'"/>';
					                                }
					                                selectPaquetes+='<div class="overlay-box">';
					                                selectPaquetes+='<div class="overlay-inner">';
					                                selectPaquetes+='<a href="#" class="search-box lightbox-image"><span>'+number_format(paquetes.precio, 2, ',', '.')+' '+paquetes.moneda+'</span></a>'
					                                selectPaquetes+='<a target="_blank" href="paqueteDetalles/'+paquetes.id+'" class="case-link">VER PAQUETE</a>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='<div class="lower-content">';
					                                selectPaquetes+='<h3><a target="_blank" href="paqueteDetalles/'+paquetes.id+'">'+paquetes.titulo+'</a></h3>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='</div>';
					                                selectPaquetes+='</div>';  
												})
											}else{
												cantidadRes = 0;
											}	
										}else{
											cantidadRes = 0;
										}	
										selectPaquetes += '</div>';
 										$('.mixitup-gallery').append(selectPaquetes);
                                        $('.filter-list').mixItUp({});
                                        $('.filter-list').mixItUp('_refresh');
                                        if(cantidadRes != 0){
	                                        $("#cantidadResultados").html('<h1 class="subtitle hide-medium" style="text-align: center;font-size: x-large;color: #e2076a;">'+cantidadRes+' PAQUETES ENCONTRADOS</h1>');
	                                    }else{
											$("#cantidadResultados").html('<h1 class="subtitle hide-medium" style="text-align: center;font-size: x-large;color: #e2076a;">NO HAY PAQUETES SOBRE ESTOS FILTROS</h1>');
	                                    }   
							}
						 })
					})

			}

			number_format = function (number, decimals, dec_point, thousands_sep) {
			        number = number.toFixed(decimals);

			        var nstr = number.toString();
			        nstr += '';
			        x = nstr.split('.');
			        x1 = x[0];
			        x2 = x.length > 1 ? dec_point + x[1] : '';
			        var rgx = /(\d+)(\d{3})/;

			        while (rgx.test(x1))
			            x1 = x1.replace(rgx, '$1' + thousands_sep + '$2');

			        return x1 + x2;
			    }

	</script>
	@endsection
