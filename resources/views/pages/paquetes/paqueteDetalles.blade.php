@extends('master')
@section('title', 'Paquetes Categorias')
@section('styles')
    @parent
    <link rel="stylesheet" href="{{ URL::asset('css/paquetes.css') }}" media="screen" >
    <link rel="stylesheet" href="{{asset('mC/css/bootstrap3-wysihtml5.min.css')}}">  
    <style>
        
    .flotante {
        bottom:350px;
        right:30%;
    }
    ul.wysihtml5-toolbar {
        margin: 0;
        padding: 0;
        display: none;
    }
    textarea {
      width: 100%;
      /* changed from 96 to 100% */}
    </style>
@endsection
@section('content')

<section class="sectiontop">
    <div class="container">
    <div class="sidebar-page-container">
        <div id="mensaje"></div>
    	<div class="auto-container">
        	<div class="row clearfix">
            <?php 
                $imagenNoDisponible = "../images/noDisponible.png";
                foreach($paquetes->paquetes[0]->imagenes as $imagen){
                    if($imagen->tipo == 'Flyer'){
                        $imagens = $imagen->url;
                        $filename2 =  "http://img.dtpmundo.com/imagen.php?base=".$imagens."|".$agenciaId;
                    }
                }   
               if(isset($imagens)){
                    $titulo = $paquetes->paquetes[0]->titulo;
                    $filename="http://img.dtpmundo.com/imagen2.php?base=".$imagens."-".$agenciaId;
                    $filenameWap="http://img.dtpmundo.com/?a=".$imagens."-".$agenciaId."-".$titulo;
                }  
             ?>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-3" style="padding-left: 0px;padding-right: 0px;">
                        <div class="row">
                        </div> 
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-5" style="padding-right: 0px;padding-left: 0px;">
                                <a href='<?php echo $filename2; ?>' title="Descargar" target="_blank">Descargar</a>
                            </div>
                             <div class="col-md-3" style="padding-left: 0px;">
                                <a href='<?php echo $filename2; ?>' title="Descargar" target="_blank"><img src="{{ URL::asset('images/icon/download.png')}}" style="width: 35px; padding-bottom: -10px"/></a>                            
                             </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                           <div class="col-md-4" style="padding-right: 0px;"> 
                            <?php
                               echo '<a href="https://web.whatsapp.com/send?text= '.urlencode($filenameWap).'" target="_blank" data-action="share/whatsapp/share">Compartir</a>';
                            ?>   
                           </div>
                           <div class="col-md-3" style="padding-left: 0px;">
                            <?php
                               echo '<a href="https://web.whatsapp.com/send?text= '.urlencode($filenameWap).'" target="_blank" data-action="share/whatsapp/share"><img  title="Whatsapp" src="'.URL::asset('images/icon/wap.png').'" style="width: 35px;"/></a>';
                            ?>   
                           </div>
                        </div>
                    </div>
                </div>
            	<div class="row"> 
                   <div class="col-md-8" style="padding-right: 0px; padding-left: 0px;">
                        <div style="padding-left: 45%; text-align:center;">
                        </div>     
                       </div> 
                    <div class="col-md-4" style="padding-right: 0px;">
                    </div>    
                </div>
                <!--Content Side / Blog Single-->
                <div class="sidebar-side col-lg-1 col-md-2 col-sm-12 col-xs-12">
                </div> 

                <div class="content-side col-lg-8 col-md-6 col-sm-12 col-xs-12">
					<div class="case-single">
                    	<div class="inner-box">
                        	<div class="image">
                            @if(isset($filename))
                                <?php
                                	echo "<img src='$filename'/>";
                                ?>
                            @else
                                <?php
                                    echo "<img src='".$imagenNoDisponible."' style='margin-right: 0px;padding-right: 250px;'/>";
                                ?>
                            @endif            
                            </div>
                        </div>
                    </div>
                </div>
                <!--Sidebar-->
                <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        <!-- Search Form -->
                        <div style="margin-bottom: 5px;">
                            <a data-toggle="modal" id="{{$paquetes->paquetes[0]->id}}" href="#requestModal" class="btn-mas-request"><img src="{{URL::asset('images/cotizacion.png')}}" style='width: 250px;'/></a> 
                            @if(isset($paquetes->paquetes[0]->cupos))
                                @foreach($paquetes->paquetes[0]->cupos as $key=>$cupos)
                                <br>
                                <div><h1 class="subtitle hide-medium" style="text-align: left;font-size: 22px;color: #fff;background-color: #e2076a;">&nbsp;&nbsp;&nbsp;Salida:&nbsp;&nbsp;&nbsp;&nbsp;{{$cupos->salida}}</h1></div>
                                <div><h1 class="subtitle hide-medium" style="text-align: left;font-size: 20px;color: #e2076a;background-color: #fff;">&nbsp;&nbsp;&nbsp;Cupos Disponibles:&nbsp;&nbsp;{{$cupos->disponibles}}</h1></div>
                                @endforeach
                            @endif    
                        </div>
                        <!--Blog Category Widget-->
                        <div class="sidebar-widget sidebar-blog-category">
                            <div class="sidebar-title" style="background-color: blueviolet;margin-bottom: 0px;height: 40px;background-color: #e2076a;">
                                <h2 style="text-align: center;padding-top: 10px;color:#ffffff;">{{$paquetes->paquetes[0]->titulo}}</h2>
                            </div>
                            <ul class="blog-cat">
                            <li style="margin-bottom: 0px;"><b>Precio:</b> {{number_format($paquetes->paquetes[0]->precio,2,",",".")}} {{$paquetes->paquetes[0]->moneda}}</li>
                            <li style="background-color: #c3c0c0;margin-bottom: 0px;"><b>Validez de Paquete:</b> {{date("d/m/Y",strtotime($paquetes->paquetes[0]->periodoFechaFinal))}}</li>

                            <li style="height: 100%;padding-left: 0px;"><textarea  style="border-color: white" class="textarea" id = "detalle" name = "detalle">@php echo $paquetes->paquetes[0]->detalle; @endphp</textarea></li>
                            @if($paquetes->paquetes[0]->promocion == "true")
                                <li><b>Periodo de Compra:</b><br>{{date("d/m/Y",strtotime($paquetes->paquetes[0]->fechaCompraInicio))}} al {{date("d/m/Y",strtotime($paquetes->paquetes[0]->fechaCompraFinal))}}</li>
                                <li><b>Periodo de  Viaje:</b><br> {{date("d/m/Y",strtotime($paquetes->paquetes[0]->fechaViajeInicio))}} al {{date("d/m/Y",strtotime($paquetes->paquetes[0]->fechaViajeFinal))}}</li>
                            @endif
                            <li style="text-align:-webkit-center;background-color: #e2076a;color:#ffffff;padding-left: 0px;"><a href="{{route('buscarPaquete')}}" role="button" style="padding-top: 5px;padding-left: 0px;text-align: center;align-content: center;"><b style="color:#ffffff;align-content: center;">&nbsp;&nbsp;Ver otros Paquetes</b></a></li>
                            </ul>
                        </div>
					</aside>
                </div>
                
            </div>
        </div>
    </div>
    </div>

    <div id="requestModal" class="modal fade" role="dialog">
        <form id="frmPaquete" method="post" action="" style="margin-top: 20%;">
            <div class="modal-dialog">
        <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title titlepage" style="font-size: x-large;">Seleccione los Datos</h2>
                    </div>
                    <div class="modal-body">
                        <div id="contenido">
                            <div class="row">
                                <div class="col-md-12" style="padding-left: 25px;padding-right: 25px;">
                                @if(isset($paquetes->paquetes[0]->cupos))
                                    <div id="mensajesCupos"></div>
                                    <div class="form-group">
                                        <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Salida</b></label>
                                        <div class="selector">
                                            <select class="full-width" id="cuposId" name="salida" required="">
                                                    <option data="" value="">Seleccione Salida</option>
                                                        @foreach($paquetes->paquetes[0]->cupos as $key=>$cupos)
                                                            @if($cupos->disponibles != 0)
                                                            <option value="{{$cupos->idCupo}}-{{$cupos->disponibles}}-{{$cupos->salida}}">{{$cupos->salida}} - ({{$cupos->disponibles}} CUPOS DISPONIBLES)</option>
                                                            @endif
                                                        @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif   
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <input name="paquete_id" type="hidden" value="{{$paquetes->paquetes[0]->id}}">
                                    @if(isset($paquetes->paquetes[0]->cupos))
                                        <input name="cuposInicio" id="cuposInicio" type="hidden" value="">
                                    @else
                                        <input name="cuposInicio" id="cuposInicio" type="hidden" value="99">
                                    @endif    
                                    <input name="cupo_factour_id" id="cupoFactourId" type="hidden" value=0>
                                    <input name="fecha_salida" id="fechaSalida" type="hidden" value="">
                                    <div id="cupos" style="padding-left: 15px;padding-right: 15px;"></div>
                                    <div class="col-md-4">
                                        <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Adultos (+12 AÑOS)</b></label>
                                        <div class="input-group form-group" style="margin-bottom: 5px;">
                                            <div class="input-group-btn">
                                                <button type="button" id="disminuir" class="btn btn-default botonIzquierdo" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-minus-sign" style="color:white"></i></button>
                                            </div>
                                            <input name="ocupancia_adultos" id="cantidad_adultos" class="form-control contadorClase" readonly="readonly" value="1"/>
                                            <div class="input-group-btn">
                                                <button type="button" id="aumentar" class="btn btn-default botonIzquierdo" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-plus-sign" style="color:white"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Niños (2/11 AÑOS)</b></label>
                                        <div class="input-group form-group" style="margin-bottom: 5px;">
                                            <div class="input-group-btn">
                                                <button type="button" id="disminuir-n" class="btn btn-default botonIzquierdo" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-minus-sign" style="color:white"></i></button>
                                            </div>
                                            <input name="ocupancia_ninhos" readonly="readonly" id="cantidadN" class="form-control contadorClase" value="0"/>
                                            <div class="input-group-btn">
                                                <button type="button" id="aumentarN" class="btn btn-default botonDerecho" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-plus-sign" style="color:white"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Infantes (0/1 AÑOS)</b></label>
                                        <div class="input-group form-group" style="margin-bottom: 5px;">
                                            <div class="input-group-btn">
                                                <button type="button" id="disminuir-i" class="btn btn-default botonIzquierdo" style="background-color: #2d3e52;" ><i class="glyphicon glyphicon-minus-sign" style="color:white"></i></button>
                                            </div>
                                            <input name="ocupancia_infante" id="cantidad_infantes" class="form-control contadorClase" value="0" readonly="readonly"  />
                                            <div class="input-group-btn">
                                                <button type="button" id="aumentar-i" class="btn btn-default botonDerecho" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-plus-sign" style="color:white"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Edad</b></label>                                        
                                        <input name="cupo_factour_id" id="cupoFactourId" type="text">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Edad</b></label>                                        
                                        <input name="cupo_factour_id" id="cupoFactourId" type="text">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Edad</b></label>                                        
                                        <input name="cupo_factour_id" id="cupoFactourId" type="text">
                                    </div>
                                 </div>                                
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="padding-left: 25px;padding-right: 25px;">
                                    <div class="form-group">
                                        <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Vendedor DTP</b></label>
                                        <div class="selector">
                                            <select class="full-width" id="agente" name="agente_dtp">
                                                @foreach($selectAgentes as $key=>$agentes)
                                                    <option value="{{$agentes['value']}}">{{$agentes['label']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" id="btnAceptarProforma" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Aceptar</button>
                    <button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
                  </div>
            </div>
        </div>
     </form>
    </div>

</section>  
@endsection
@section('scripts')
    @include('layouts/scripts')
    @parent
        <!--End Revolution Slider-->
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/jquery-ui.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/jquery.fancybox.pack.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/jquery.fancybox-media.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/owl.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/appear.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/mixitup.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/wow.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/script.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('mC/js/bootstrap3-wysihtml5.all.min.js')}}"></script>
        <script type="text/javascript">
            var id = "{{$paquetes->paquetes[0]->id}}";
            setTextareaHeight($('textarea'));
            function setTextareaHeight(textareas) {
                    textareas.each(function () {
                    var textarea = $(this);
                    if ( !textarea.hasClass('autoHeightDone') ) {
                        textarea.addClass('autoHeightDone');
             
                        var extraHeight = parseInt(textarea.css('padding-top')) + parseInt(textarea.css('padding-bottom')), // to set total height - padding size
                            h = textarea[0].scrollHeight - extraHeight;
             
                        // init height
                        textarea.height('auto').height(h);
             
                        textarea.bind('keyup', function() {
             
                            textarea.removeAttr('style'); // no funciona el height auto
             
                            h = textarea.get(0).scrollHeight - extraHeight;
             
                            textarea.height(h+'px'); // set new height
                        });
                    }
                })
            }
        $('#detalle').wysihtml5();

        $('.btn-mas-request').click(function(){
            console.log('Inicial');
            botones();
         })   
        $("#cuposId").change(function(){
            cupon = $("#cuposId").val();
            var cupos  = cupon.split('-');
            $('#cuposInicio').val(cupos[1]);
            $('#cupoFactourId').val(cupos[0]);
            $('#fechaSalida').val(cupos[2]);
            $('#cantidad_adultos').val(1);
            $('#cantidadN').val(0);
        })    

        function botones(){
            var adulto = $('#cantidad_adultos').val();
            var ninho  = $('#cantidadN').val();
            /* Adultos */
            $('#disminuir').click(function(){
                cantidadTotal =  parseInt($('#cantidad_adultos').val()) + parseInt($('#cantidadN').val());
                //Solo si el valor del campo es diferente de 0
                $cantidad= parseInt($('#cantidad_adultos').val())-1; 
                if (parseInt($('#cantidad_adultos').val()) != 0){
                    //Decrementamos su valor
                    $('#cantidad_adultos').val($cantidad);
                }
            });
            $('#aumentar').click(function(){
                 cantidadTotal =  parseInt($('#cantidad_adultos').val()) + parseInt($('#cantidadN').val());
                //Solo si el valor del campo es diferente de 0
                if (parseInt(cantidadTotal) < $('#cuposInicio').val())
                {
                    $cantidad= parseInt($('#cantidad_adultos').val())+1;
                    $('#cantidad_adultos').val($cantidad);
                }
            });
             /* Niños */
            $('#disminuir-n').click(function(){
                //Solo si el valor del campo es diferente de 0
                cantidadTotal =  parseInt($('#cantidad_adultos').val()) + parseInt($('#cantidadN').val());
                                 console.log(cantidadTotal);
                resultado = parseInt($('#cantidadN').val()) - 1;
                //Solo si el valor del campo es diferente de 0
               if (parseInt($('#cantidadN').val()) != 0){
                    //Decrementamos su valor
                    $('#cantidadN').val(resultado);
                }    
               /* }else{
                    return;
                } */   
            });

            $('#aumentarN').click(function(){
                //Aumentamos el valor del campo
                cantidadTotal =  parseInt($('#cantidad_adultos').val()) + parseInt($('#cantidadN').val());
                //Solo si el valor del campo es diferente de 0                 
                if(parseInt(cantidadTotal) < $('#cuposInicio').val())
                {
                    resultado = parseInt($('#cantidadN').val()) + 1;
                    $('#cantidadN').val(resultado);
                }
            });

            /* Infantes */

            $('#disminuir-i').click(function(){
                //Solo si el valor del campo es diferente de 0
                resultado = parseInt($('#cantidad_infantes').val()) - 1;
                if (parseInt($('#cantidad_infantes').val()) != 0) 
                    //Decrementamos su valor
                    $('#cantidad_infantes').val(resultado);

            });

            $('#aumentar-i').click(function(){
                //Aumentamos el valor del campo
                resultado = parseInt($('#cantidad_infantes').val()) + 1;
                if ($('#cantidad_infantes').val() != 2)
                {
                    $('#cantidad_infantes').val(resultado);
                }
            });
        }

        $("#btnAceptarProforma").click(function(){
            if($("#cuposId").val()!= 0 ){
                var dataString = $("#frmPaquete").serialize();
                $.ajax({
                        type: "GET",
                        url: "{{route('guardarPaqueteSolicitado')}}",
                        dataType: 'json',
                        data: dataString,
                        success: function(rsp){
                                $("#requestModal").modal('hide');
                                $(".modal-backdrop").remove();
                                if(rsp.estado == "OK"){
                                    $(".btn-mas-request").hide();
                                    $("#mensaje").append('<div class="alert alert-success" style="margin-left: 15%;"><strong>'+rsp.descripcion+'</strong><div>');                           
                                }else{
                                    $("#mensaje").append('<div class="alert alert-error" style="margin-left: 15%;"><strong>'+rsp.descripcion+'</strong><div>');                           
                                }   
                            }
                    })
            }else{
                $("#mensajesCupos").html('<div class="alert alert-error" style="margin-left: 1%;margin-bottom: 0px;padding-bottom: 1px;height: 46px;background-color: red;color: white;"><strong>Seleccione Proveedor</strong><div>');
            }   
        })
        </script>
@endsection
