@extends('master')

@section('title', 'Búsqueda de hoteles')
@section('styles')
	@parent
@endsection

@section('content')
<section class="sectiontop">
	<div class="container">
                <form action="" method="GET">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 alert-danger">
							<!-- En caso de error-->
                        </div>
                        <div id="main" class="col-sm-12 col-md-12" class="sectiontop">
                            <div class="booking-information travelo-box"> 
                                <h2>Reserva Realizada</h2>
                                <hr />
                                <div class="booking-confirmation clearfix">
                                    <i class="glyphicon glyphicon-plane icon circle" style="color:#088A29;"></i>
                                    <div class="message">
                                        <h4 class="main-message">Muchas gracias por su reserva.</h4>
                                        <p>Se le ha enviado a su cuenta de mail la información detallada.</p>
                                    </div>
                                </div>

                                </div>
                                <hr />
							</div>
                        </div>
                    </div>
                </form>
    </div>
</section>
@endsection

@section('scripts')
    @parent
@endsection
