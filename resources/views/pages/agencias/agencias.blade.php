@extends('master')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
            	<form id="frmAgencias" class="contact-form" action="" method="post">
            		@include('flash::message')
					<div class="booking-information travelo-box">
						<div>
							<h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Agencias</h1>
							<br>
							<div class="row">
								<div class="form-group col-xs-12 col-sm-6">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<a href="#" title="a Excel"><i class="fa fa-file-excel-o"></i></a>
									<!-- <a href="impresiones/impresion.php" target="_blank" title="Imprimir"><i class="fa fa-print"></i></a>-->
								</div>
							</div>
							<br>
            				<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
	                            <thead>
	                            	<tr>
	                                	<th>ID</th>
	                                  	<th>Razon Social</th>
	                                  	<th>Teléfono</th>
				                      	<th>Email</th>
	                                   	<th>Comisión</th>
	                                   	<th class="oculto">Ver</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                            @foreach($agencias as $agencia)
	                            	<tr>
	                            		<td><b>{{$agencia->id_agencia}}</b></td>
	                            	    <td><b>{{$agencia->razon_social}}</b></td>
	                                  	<td>{{$agencia->telefono}}</td>
				                      	<td>{{$agencia->email}}</td>
	                                   	<td><b>{{$agencia->comision}}</b></td>
	                                   	<td><a href="{{route('agenciasEdit', ['id' =>$agencia->id_agencia])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="background: #e2076a !important;" role="button"><br>Editar</a></td>
	                                </tr>   	
	                            @endforeach       	
	                            </tbody>
                        </table>'
                        </div>
                    </div>    			
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/scripts')
	<script>
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});;
		});
	</script>
@endsection