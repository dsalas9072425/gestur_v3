@extends('master')

@section('title', 'Búsqueda de Vuelos')
@section('styles')
	@parent
@endsection
@section('content')
<div class="container">
	<div id="main">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="nueva-busqueda">
					<div>
						<div class="tab-container">
							<ul class="tabs">
	                            <li class="active"><label data-target="#idavuelta" ><input type="radio" class="tipo_tabs" name="radio-tab" id="idayvuelta" checked="checked">Ida y Vuelta</label>
	                            <li><label data-target="#idavuelta"><input type="radio" name="radio-tab" class="tipo_tabs" id="ida">Sólo Ida</label></li>
	                        </ul>
						</div>
						<div class="tab-content">
                			<div class="tab-pane active in" id="idavuelta">
                			    <input type="hidden" form ="frmBusquedaFligth" id="tipo" name="tipo" value = "idayvuelta"/>
                				<form action="{{route('searchFlight')}}" method="" id="frmBusquedaFligth" style="background-color:  #1c2b39;">
	                				<div class="row">
		                            	<div class="col-xs-12 col-sm-6 col-md-6">
		                                    <div class="form-group">
		                                        <label class="title">Origen</label>
												<input id="origen" name="origen" type="hidden">
												<input id="origen_name" value="{{$origenF}}"  class="Requerido form-control" placeholder="Origen" style="height: 39px;">
		                                    </div>
		                                </div>
		                                <div class="col-xs-12 col-sm-6 col-md-6">
		                                    <div class="form-group">
		                                        <label class="title">Destino</label>
												<input id="destino" name="destino" type="hidden">
												<input id="destinationF_name" value="{{$destinoF}}" placeholder="Destino" class="Requerido form-control" style="height: 39px;">
		                                    </div>
		                                </div>
		                                <div class="col-xs-6 col-sm-3 col-md-3">
		                                    <div class="form-group">
		                                        <label class="title">IDA</label>
 												<div>
		                                            <input type="text" id="fecha_ida_vuelo" name="fecha_ida_vuelo" class="input-text full-width required" value = "" readonly="readonly" />
		                                        </div>		                                        
		                                    </div>
		                                </div>
		                                <input type="hidden" min="1" max="361" value="1" class="input-text full-width no-padding text-center" name="cant_noches" id="cant_nochesFligthF"/>  
		                                <div class="col-xs-6 col-sm-3 col-md-3">
		                                    <div class="form-group vuelta">
		                                        <label class="title">VUELTA</label>
		                                        <div>
		                                            <input type="text" id="fecha_vuelta_vuelo" name="fecha_vuelta_vuelo" class="input-text full-width required" value = "" readonly="readonly" />
		                                        </div>
		                                    </div>
		                                </div>
				                        <div class="col-xs-6 col-sm-2 col-md-2">
				                            <label class="titles cantPasajeros">Adultos</label>            
					                        <div class="input-group form-group">
					                            <div class="input-group-btn">
									                <button type="button" id="disminuir" class="btn btn-default" style="background-color: rgb(43, 67, 145); width: 30px;"><i class="glyphicon glyphicon-minus-sign" style="color:white"></i></button>
									            </div>
									            <input name="cantidad_adultos" id="cantidad_adultos" class="form-control" value="1" readonly="readonly" />
									            <div class="input-group-btn">
										            <button type="button" id="aumentar" class="btn btn-default" style="background-color: rgb(43, 67, 145); width: 30px;"><i class="glyphicon glyphicon-plus-sign" style="color:white"></i></button>
										        </div>
										    </div>
										</div>
										<div class="col-xs-6 col-sm-2 col-md-2">
										    <label class="titles cantPasajeros">Niños (2/11 AÑOS)</label>
										        <div class="input-group form-group">
										            <div class="input-group-btn">
									                    <button type="button" id="disminuir-n" class="btn btn-default" style="background-color: rgb(43, 67, 145); width: 30px;"><i class="glyphicon glyphicon-minus-sign" style="color:white"></i></button>
									                </div>
									                <input name="cantidadN" id="cantidadN" class="form-control" value="0" readonly="readonly"  />
									                <div class="input-group-btn">
										                <button type="button" id="aumentarN" class="btn btn-default" style="background-color: rgb(43, 67, 145); width: 30px;"><i class="glyphicon glyphicon-plus-sign" style="color:white"></i></button>
										            </div>
										        </div>
										</div>
										<div class="col-xs-6 col-sm-2 col-md-2">
										    <label class="titles cantPasajeros">Infantes (0/2 AÑOS)</label>
										    <div class="input-group form-group">
										        <div class="input-group-btn">
									                <button type="button" id="disminuir-i" class="btn btn-default" style="background-color: rgb(43, 67, 145); width: 30px;"><i class="glyphicon glyphicon-minus-sign" style="color:white"></i></button>
									            </div>
									            <input name="cantidad_infantes" id="cantidad_infantes" class="form-control" value="0" readonly="readonly"  />
									            <div class="input-group-btn">
										            <button type="button" id="aumentar-i" class="btn btn-default" style="background-color: rgb(43, 67, 145); width: 30px;"><i class="glyphicon glyphicon-plus-sign" style="color:white"></i></button>
										        </div>
										    </div>
				                        </div>
	                                	<!--<input type= "hidden" id= "objBusqueda" name= "objBusqueda">-->
	                                	<div class="col-xs-11 col-sm-11 col-md-11">
	                                	</div>
		                                <div class="col-xs-6 col-sm-1 col-md-1">
		                                    <button style="background-color:#e2076a;" type="submit" id="btnBuscar" class="btn qsf-search hotels" tabindex="5">
		                                    	<span class="text"><i class="icon-loupe-search"></i>Buscar</span>
											</button>
		                                </div>
		                            </div>
                            	</form>
                			</div>
                	</div>
				</div>
			</div>

		<div class="col-xs-12 col-sm-4 col-md-3 filtro" id="filtro-buscador">
			<form role="form" id= "filtro-formulario">
				<input type="hidden" name="filter" value="yes" />
				<h4 class="search-results-title filtrotitle"><i class="fa fa-filter" aria-hidden="true"></i> Filtrar búsqueda</h4>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 form-group border-bottom">

						<label>PRECIOS</label>

						<div id="price-rang"></div>

                        <br />
						<input type="hidden" class="form-control" id="filtroSlide" name="filtro" value=0>

						<span class="min-price-label pull-left" id="precio_minimo_label">850</span>

                        <span class="max-price-label pull-right" id="precio_maximo_label">3500</span>

                        <div class="clearer"></div>

                        <input type="hidden" class="form-control" id="precio_minimo" name="precio_minimo">

                        <input type="hidden" class="form-control" id="precio_maximo" name="precio_maximo" align="right">

					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 form-group border-bottom">
						<label for="estrellas">ESCALAS</label>
						<div id="filtroescala">
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 form-group border-bottom">
						<label>AEROLÍNEAS</label>
						<div id="filtroaerolinea">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 form-group">
						<button id="btnFiltro" type="button" class="btn btn-info" style="width: 120px;">Remover Filtro</buttom>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 form-group">
						<button id="btnFiltroAplicar" type="button" class="btn btn-success"  style="background-color: #e2076a; width: 120px;">Aplicar Filtro</buttom>
					</div>
				</div>
			</form>		
			</div>
			<div class="col-sm-8 col-md-9 vuelos-resultados" style="padding-right: 0px;">
				<div class="flight-list listing-style3 flight">
					@include('flash::message')
					<div class="hotel-list listing-style3 hotel" id = "resultadosBusqueda" >
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 text-center">
							<ul class="pagination">
								<li><a href="#" id ="btnAtras">« Anterior</a></li>
								<li><a href="#" id="btnNext">Siguiente »</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
    @parent
	<script type="text/javascript" src="{{ URL::asset('js/search/filtro-buscador-vuelo.js') }}"></script>
	<script>
		maxResultsPerPage = 10;
		currentPage = 0;
		first = true;
		wFilter = false;
		$('#price-rang').slider({
					    change: function(event, ui) {
					        if (event.originalEvent) {
					            //manual change
						        $('#filtroSlide').val(1);
					        }
					    }
					});

		$(function(){	
			$('#btnAtras').hide();
			$('#btnNext').hide();
			var inputs = JSON.parse('<?php echo json_encode($inputs); ?>');
			$.each(inputs, function(key,value){
				$('#'+key).val(value);
				if(key=='origen'){
					$('select').trigger("chosen:updated");
				}
				if(key=='destino'){
					$('select').trigger("chosen:updated");
				}
				if(key=='tipo'){
					$('#'+value).attr('checked', true);
				}
			});
			$('#btnSearch, #btnFiltro').click(function(){
				wFilter = isFilterOn();
				//si se hizo click en remover filtro (btnFiltro) hacer busqueda sin filtro y resetear los campos de filtro

				if($(this).attr('id') == 'btnFiltro'){
					$('#filtro-buscador :input').each(function(){
						if($(this).attr('type') == 'checkbox') 
						{
						}
						else 
							
							if($(this).attr('type') == 'number' || $(this).attr('type') == 'text') 
								$(this).val(null);
					});
					wFilter = false;
				}
				if(wFilter){
					$('#btnFiltro').show();
				}else{
					$('#btnFiltro').hide();
				}			
				
				first=true;
				currentPage=1;
				$('#btnAtras').hide();
				$('#btnNext').hide();
				doFlightSearch();
			});
			
			$('#btnNext, #btnAtras').click(function(e){
				wFilter = isFilterOn();
				$('#filtro-buscador :input').each(function(){
					if($(this).attr('type') == 'checkbox'){
						if($(this).is(':checked')){
							$(this).prop('checked',true);	
						}
					}					
				})		
				if($(e.target).attr('id') == 'btnNext') currentPage++;
				else currentPage--;
				if(currentPage>1){
					$('#btnAtras').show();
				}else{
					$('#btnAtras').hide();
				}
				doFlightSearch();
			});
			currentPage=1;
			doFlightSearch();
		});	
	$('input[name="radio-tab"]').click(function () {
		//jQuery handles UI toggling correctly when we apply "data-target" attributes and call .tab('show') 
		//on the <li> elements' immediate children, e.g the <label> elements:
		$(this).closest('label').tab('show');
	});

	$(".tipo_tabs").click(function(event){
		$( "#tipo").val($(this).attr('id'));
	})	


	function isFilterOn(){
		var isOn = false;
		$('#filtro-buscador :input').each(function(){
			if($(this).attr('type') == 'number' || $(this).attr('type') == 'text'){
				if($(this).val()){
					isOn = true;
					return;
				}
			}else if($(this).attr('type') == 'checkbox'){
				if($(this).is(':checked')){
					isOn = true;
					return;
				}
			}
			if($(this).attr('type') == 'hidden' && $(this).attr('id') =='filtroSlide'){
				if($(this).val()){
					isOn = true;
					return;
				}	
			}
		});
		return isOn;
	}


	function doFlightSearch(){
		var dataString = $("#frmBusquedaFligth").serialize();
			if(wFilter)
			{
				$('#filtro-buscador :input').each(function(){
					if($(this).attr('type') == 'number' || $(this).attr('type') == 'text' || $(this).attr('type') == 'hidden'){
						dataString += '&'+$(this).attr('name')+'='+ $(this).val();
					}else if($(this).attr('type') == 'checkbox'){
						if($(this).is(':checked') == true)
						{
							$("#filtro-buscador input[name='"+$(this).attr('name')+"']:checked:enabled");
							var name = $(this).attr('name');
							if(name=='escala')
							{
								dataString += '&escala=' + $(this).val();
							}
							else
							{
								dataString += '&'+$(this).attr('name')+'='+ $(this).is(':checked');
							}
						}
					}
				});
			}
			dataString += '&from='+(currentPage-1)*maxResultsPerPage;
			dataString += '&to='+((currentPage)*maxResultsPerPage-1);
		
		if(first == true){
			dataString +='&first=true';
		}
		procesando();
		$.ajax({
				type: "GET",
				url: "{{route('doFlightSearch')}}",
				dataType: 'json',
				data: dataString,
				success: function(rsp){	
							$.unblockUI();
							if(rsp.codRetorno != 0){
								msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i>'+rsp.desRetorno+'</h2><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
								mostrarMensaje(msg);
								return;
							}
							if(first == true){
								$.ajax({
									type: "GET",
									url:"{{'dataFilterFligth'}}",
									dataType: 'json',
									data: true,
									success: function(rsp){
										$("#filtroaerolinea").empty();
										$("#filtroclase").empty();
										$("#filtroescala").empty();
										$.each(rsp, function (i, item) {
											$.each(item, function (ii, items) {
												if(i == "aerolinea"){
													filtro = '<label class="checkbox-inline"><input name="aerolinea['+ii+']" type="checkbox" value="'+ii+'" id="'+ii+'"><img src="https://images.kiwi.com/airlines/64/'+ii+'.png" style="width: 20px;" class="imageHead">&nbsp;&nbsp;'+items+'</label><br>';	
												}
												if(i == "escala"){
													if(items == 0){
														filtro = '<label class="checkbox-inline"><input name="escala" type="checkbox" value="'+ii+'" id="'+ii+'">Sin Escala(s)</label><br>';	
													}else{
														filtro = '<label class="checkbox-inline"><input name="escala" type="checkbox" value="'+ii+'" id="'+ii+'">'+items+' Escala(s)</label><br>';	
													}
												}
												$('#filtro'+i).append(filtro);	
											})
											$('#filtro'+i).append('<br>');
										})	

									},
								})	
							}

							$("#price-rang" ).slider({
							        range: true,
							        min: rsp.minPrecio,
							        max: rsp.maxPrecio,
							        values: [ rsp.minPrecio, rsp.maxPrecio],
							        slide: function( event, ui ) {
							            $(".min-price-label").html( ui.values[ 0 ]);
							            $(".max-price-label").html( ui.values[ 1 ]);

							            $( "#precio_minimo" ).val( ui.values[ 0 ] );
							            $( "#precio_maximo" ).val( ui.values[ 1 ] );
							        }
							    });
							$( "#precio_minimo" ).val($( "#price-rang" ).slider( "values", 0 ) );

							$( "#precio_maximo" ).val($( "#price-rang" ).slider( "values", 1 ) );

							$('#precio_minimo').val(rsp.minPrecio);
							$('#precio_maximo').val(rsp.maxPrecio);
							$('#precio_minimo_label').html(rsp.minPrecio);
							$('#precio_maximo_label').html(rsp.maxPrecio);

							if(first) first=false;
							if(rsp.fligthViews.length>0){
								$('#resultadosBusqueda').html('');
								$.unblockUI();
								rsp.fligthViews.forEach(function(div){
									//recibo
									$('#resultadosBusqueda').append(div);		
								});
								if(rsp.fligthViews.length != maxResultsPerPage){
									$('#btnNext').hide();
								}else{
									$('#btnNext').show();
								}
							}else{
									//No Results
									$.unblockUI();
									var msg = "";
									$('#btnNext').hide();
									if(currentPage==1){
										msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se han encontrado más resultados</h2><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
											mostrarMensaje(msg);
										$('#resultadosBusqueda').html('');	
										
									}
									else{
										var msg = "";
										msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No hay mas resultados para mostrar</h2><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
											mostrarMensaje(msg);
											
										currentPage--;
										if(currentPage == 1){
											$('#btnAtras').hide();
											$('#btnNext').hide();
										}
									}					
								}

				},	
				error: function(jqXHR,textStatus,errorThrown){
						ajaxGlobalHandler(jqXHR,textStatus,errorThrown);
					}
			});
	}
	$("#btnFiltroAplicar").click(function(event){
		
		var dataString = $("#filtro-formulario").serialize();
		wFilter = isFilterOn();
		dataString += '&from='+0;
		dataString += '&to='+9;
		dataString += '&max='+$('#minimoReal').val();
		dataString += '&min='+$('#maximoReal').val();
		dataString += '&fecha_ida_vuelo='+$('#fecha_ida_vuelo').val();
		dataString += '&fecha_vuelta_vuelo='+$('#fecha_vuelta_vuelo').val();
		dataString += '&remover= 1';
		if(first == true){
			dataString +='&first=true';
		}
		$.ajax({
				type: "GET",
				url: "{{route('doFlightFiltro')}}",
				dataType: 'json',
				data: dataString,
				success: function(rsp){
						if(rsp.fligthViews.length>0){
							$('#resultadosBusqueda').html('');
							$.unblockUI();
							rsp.fligthViews.forEach(function(div){
									//recibo
								$('#resultadosBusqueda').append(div);		
							});
							if(rsp.fligthViews.length != maxResultsPerPage){
								$('#btnNext').hide();
							}else{
								$('#btnNext').show();
							}
						}else{
							//No Results
							$.unblockUI();
							var msg = "";
							$('#btnNext').hide();
								if(currentPage==1){
									msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se han encontrado más resultados</h2><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
											mostrarMensaje(msg);
									$('#resultadosBusqueda').html('');	
								}
								else{
									var msg = "";
									msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No hay mas resultados para mostrar</h2><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
											mostrarMensaje(msg);
									currentPage--;
									if(currentPage == 1){
										$('#btnAtras').hide();
										$('#btnNext').hide();
									}
								}					
							}
				}
			})	
	})

	/*$('#destinationF_name').autocomplete({
		    source: function (request, response) {
	          $.getJSON("{{'destinationsFligth'}}?term=" + request.term, function (data) {
		            response($.map(data, function (value, key) {
		                return {
		                    label: value,
		                    value: value,
		                    valor: key
		                };
		            }));
		        });
		    },
		    minLength: 3,
		    delay: 100,
		    maxShowItems: 7,
		    select: function( event, ui ) {
				$('#destinoF').val(ui.item.valor);
		      }
	});

	$('#origen_name').autocomplete({
		    source: function (request, response) {
	          $.getJSON("{{'destinationsFligth'}}?term=" + request.term, function (data) {
		            response($.map(data, function (value, key) {
		                return {
		                    label: value,
		                    value: value,
		                    valor: key
		                };
		            }));
		        });
		    },
		    minLength: 3,
		    delay: 100,
		    maxShowItems: 7,
		    select: function( event, ui ) {
				$('#origen').val(ui.item.valor);
		    }

	});*/

		var $projects = $('#destinationF_name');
		$projects.autocomplete({
							    minLength: 3,
							    source: function (request, response) {
									          $.getJSON("{{'destinationsFligth'}}?term=" + request.term, function (data) {
										            response($.map(data, function (value, key) {
										            	var values = value.split("||")
										                return {
										                    label: values[0],
										                    value: values[0], 
										                    img: 'http://www.countryflags.io/'+values[1]+'/flat/64.png',
										                    valor: key
										                };
										            }));
										        });
										    },
							    focus: function( event, ui ) {
							      $projects.val( ui.item.label );
							      return false;
							    },
								select: function( event, ui ) {
												$('#destinoF').val(ui.item.valor);
										      }		 					 
						});
		  
		  $projects.data( "ui-autocomplete" )._renderItem = function(ul, item ) {
		    var $li = $('<li>'),
		        $img = $('<img style="height: 22px; width: 22px; margin-right: 2%; padding-top: 1%;">');
		    $img.attr({
		      src: item.img,
		      alt: item.label
		    });

		    $li.attr('data-value', item.label);
		    $li.append('<a href="#">');
		    $li.find('a').append($img).append(item.label);    

		    return $li.appendTo(ul);
		};

		var $project = $('#origen_name');
		$project.autocomplete({
							    minLength: 3,
							    source: function (request, response) {
									          $.getJSON("{{'destinationsFligth'}}?term=" + request.term, function (data) {
										            response($.map(data, function (value, key) {
										            	var values = value.split("||")
										                return {
										                    label: values[0],
										                    value: values[0], 
										                    img: 'http://www.countryflags.io/'+values[1]+'/flat/64.png',
										                    valor: key
										                };
										            }));
										        });
										    },
							    focus: function( event, ui ) {
							      $project.val( ui.item.label );
							      return false;
							    },
								select: function( event, ui ) {
												$('#origen').val(ui.item.valor);
										      }		 					 
						});
		  
		  $project.data( "ui-autocomplete" )._renderItem = function(ul, item ) {
		    var $li = $('<li>'),
		        $img = $('<img style="height: 22px; width: 22px; margin-right: 2%; padding-top: 1%;">');
		    $img.attr({
		      src: item.img,
		      alt: item.label
		    });

		    $li.attr('data-value', item.label);
		    $li.append('<a href="#">');
		    $li.find('a').append($img).append(item.label);    

		    return $li.appendTo(ul);
		  };
	
		
</script>
@endsection
