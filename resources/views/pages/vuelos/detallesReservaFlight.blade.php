@extends('master')

@section('title', 'Booking de Vuelos')
@section('styles')
	@parent
@endsection
<?php
include("../comAerea.php");
include("../destinationFligth.php");
?>
@section('content')
<section class="sectiontop">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="booking-information travelo-box">
					<div class="row row-eq-height">
						<div class="col-sm-4 col-md-4 localizador" style="padding-bottom: 0px; width: 25%;">
							<div>Localizador / <span class="text-italic">Reference number:</span></div>
							<div class="num-localizador" style="color:#fdb714">
								{{$resultado[0]->controlnumber}}
							</div>
						</div>
						<div class="col-sm-8 col-md-8 booking-info-pasajero text-center" style="width: 75%;">
							<?php 
								$total = count($resultado[0]->itinerarios) -1;
							?>
							@foreach($resultado[0]->itinerarios as $key=>$itinerario)
								{{--json_encode($itinerario)--}}
								<div class="row">
		                    		<div class="col-md-4">
		                    			<div><b>{{$destination[$itinerario->origen]}}</b></div>
		                    		</div>
		                    		<div class="col-md-1 salida-fa">
		                    			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		                    		</div>
		                    		<div class="col-md-4">
		                    			<div><b>{{$destination[$itinerario->destino]}}</b></div>
		                    		</div>
								</div>
							@endforeach	
							@if($total > $key)
								<hr style="margin-top: 2%; margin-bottom: 2%; height: 1px;">
							@endif
						</div>
					</div>
					<div class="booking-pasajero">
						<h3 class="textblue">Pasajeros</h3>
						<div>
							@foreach($resultado[0]->pasajeros as $key=>$pasajeros)
								<h1>{{$pasajeros->n_pasajero}}) {{$pasajeros->apellido}}, {{$pasajeros->nombre}}</h1>
							@endforeach	
						</div>
					</div>


					@foreach($resultado[0]->itinerarios as $key=>$itinerario)
					<?php
                              $timezone = new DateTimeZone('UTC'); 
                             // $dateInicio = DateTime::createFromFormat('dmY', $itinerario->fechaInicio, $timezone);
                              $dateArrival = DateTime::createFromFormat('dmY', $itinerario->fecha_destino, $timezone);
                              $dateDepature = DateTime::createFromFormat('dmY', $itinerario->fecha_origen, $timezone);
                              ?>
						<div class="row">
							<div class="col-sm-12 col-md-12 booking-vuelo">
								<b>Vuelo</b> -{{$itinerario->vuelo}} {{$comAerea[$itinerario->carrier_company]}} - {{$dateDepature->format('d F')}}
							</div>
							<div class="col-sm-6 col-md-6 booking-vuelo-detalle-left">
								<div>
									<br>
									<b>Salida:</b> {{$itinerario->origen}}
									<br>
									<b>Fecha:</b> {{$dateDepature->format('d F')}} {{$itinerario->horaIn}}
									<br>
									<b>Duración:</b> {{$itinerario->diff}}
									<br>
									<b>Localizador Aerolínea:</b> {{$itinerario->localizador_aerolínea}}
									<br>
									<b>Reserva confirmada, {{$itinerario->tipo_asiento}}</b>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 booking-vuelo-detalle-right">
								<div>
									<br>
									<b>Llegada:</b> {{$itinerario->destino}}
									<br>
									<b>Fecha:</b> {{$dateArrival->format('d F')}}  {{$itinerario->horaOut}}
									<br>
									<b>Sin Parada</b>
									<br>
									<b>Equipaje Permitido:</b> 1PC
									<br>
									<b></b>
									<br>
									<!--<b>Comida:</b> Desayuno</b>-->
								</div>
							</div>
						</div>
					@endforeach	
					<div class="booking-pasajero">
						<!--<h3 class="textblue">Billetes Aéreos</h3>-->
						<div class="row">
							<div class="col-sm-9 col-md-9">
							</div>
							<div class="col-sm-3 col-md-3 text-center padding-logo">
								<div>
								<img src="{{asset('images/dtpmundo.png')}}" width="200">
							</div>
							</div>
						</div>
						
					</div>

				</div>
				
			</div>
			
		</div>
                
    </div>

</section>
@endsection

@section('scripts')
    @parent
@endsection
