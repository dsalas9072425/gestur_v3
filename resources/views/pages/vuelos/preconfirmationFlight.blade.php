@extends('master')

@section('title', 'Preconfirmacion de Vuelos')
@section('styles')
	@parent
@endsection

@section('content')
<?php
	$contadorInfante = 1;
	$contadorCopa = 0;
?>	
	<div class="container" style="height: 1600px;">
	<div id="main">
			@include('flash::message')
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
                <form id="frmPreConfirmacion" class="contact-form" action="{{route('bookingFlight')}}" method="post">
                	{{ csrf_field() }}					
                	<div class="toggle-container box">
						<div class="panel style1">
							<h4 class="panel-title">
                                <a href="#inforeserva" data-toggle="collapse"><i class="fa fa-info-circle" aria-hidden="true"></i> Información de Reserva</a>
                            </h4>
                            <div class="panel-collapse collapse in" id="inforeserva">
                                <div class="panel-content">
                                	<?php
							            $timezone = new DateTimeZone('UTC'); 
							            $dateArrivals = DateTime::createFromFormat('dmY', $itinerary[0]->arrivalLocalization->arrivalPointDetails->dateOfArrival, $timezone);
							            $dateDepatures = DateTime::createFromFormat('dmY', $itinerary[0]->departureLocalization->departurePoint->dateOfDeparture, $timezone);
			                        ?>
									<div class="row">
				                        <div class="col-md-2 fechas">
				                        	<div class="text-small">DESDE</div>
				                        	{{$dateDepatures->format('d F')}}
				                        </div>
				                        <div class="col-md-3 detalle-cabecera">
					                        {{$itinerary[0]->departureLocalization->departurePoint->datosAeropuerto}}<br>
					                        {{$itinerary[0]->departureLocalization->departurePoint->datosCiudadPais}}
				                        </div>
				                        <div class="col-md-3 detalle-cabecera">
				                        	{{$itinerary[0]->arrivalLocalization->arrivalPointDetails->datosAeropuerto}}<br>
					                        {{$itinerary[0]->arrivalLocalization->arrivalPointDetails->datosCiudadPais}}

				                        </div>
				                        <div class="col-md-2 fechas">
				                        	<div class="text-small">HASTA</div>
				                        	{{$dateArrivals->format('d F')}}
				                        </div>
				                         <div class="col-md-2 text-center">
				                        	<div class="total-vuelo">TOTAL $ {{number_format($monto,2,",",".")}}</div>
				                        	<input type="hidden" name="totalMonto" id="monto" value="{{$monto}}">
				                        </div>
				                        @foreach($itinerary as $key=>$itinerario)
				                        	{{--json_encode($itinerario)--}}
				                             <div class="col-md-12">
					                        	<div class="col-md-12 texto-aerolinea separador" style="margin-top: 0px;">
					                        		<small style="font-size: 12px;">{{$itinerario->flightInformation[0]->aerolinea}}</small>
					                        		
					                        	</div>
					                        	<div class="col-md-2 img-aerolinea">
						                        	<img src="https://images.kiwi.com/airlines/64/{{$itinerary[0]->flightInformation[0]->companyId->marketingCarrier}}.png" class="imageHead"></div>
					                        	<div class="col-md-8">
					                        		<div class="row">
					                        			<div class="col-md-4 salida">
						                        			<span>{{date("G:i",strtotime($itinerario->departureLocalization->departurePoint->timeOfDeparture))}}</span>
						                        			<br>
						                        			@if($key != 0)
							                        			{{$itinerary[0]->arrivalLocalization->arrivalPointDetails->datosciudad}}
						                        			@else
							                        			{{$itinerary[0]->departureLocalization->departurePoint->datosciudad}}
						                        			@endif
						                        		</div>
						                        		<div class="col-md-4 escalas">
															<?php 
						                                      $totalHoras = date("G:i",strtotime($itinerario->totalHora));
						                                      $totalH =  explode(":", $totalHoras);
						                                    ?>
						                                    <span class="cant-horas">{{$totalH[0]."h :".$totalH[1]."m"}}</span>
						                        			<ul class="cant-escalas" style="margin-top: 3px;     margin-bottom: 3px;">
						                        			    @for ($i = 0; $i < $itinerario->cantidad; $i++)
                                                					<li class="stop-dot"></li>
                                         						@endfor     
						                        				<li class="cant-escalas">
						                        					<div class="empty"></div>
						                        				</li>
						                        			</ul>
						                        			<span class="cantidad-escalas">{{$itinerario->cantidad}} Parada(s)</span> 						                        		
						                        		</div>
						                        		<div class="col-md-4 llegada">
						                        			<span>{{date("G:i",strtotime($itinerario->arrivalLocalization->arrivalPointDetails->timeOfArrival))}}</span>
						                        			<br>
						                        			@if($key != 0)
							                        			{{$itinerary[0]->departureLocalization->departurePoint->datosciudad}}
						                        			@else
							                        			{{$itinerary[0]->arrivalLocalization->arrivalPointDetails->datosciudad}}
						                        			@endif
						                        		</div>
					                        		</div>
					                        	</div>
					                        	<div class="col-md-2">
					                        		<a class="btn btn-success" data-toggle="collapse" style="background-color:rgb(43, 67, 145); width: 100%; height: 31px;" data-target="#mas-detalles{{$key}}"><i class="fa fa-angle-down" aria-hidden="true"></i> Detalles</a>
					                        	</div>
					                        	<div class="col-md-12 mas-detalle-vuelos collapse" id="mas-detalles{{$key}}">
					                        		<?php 
					                        			$comparador = count($itinerario->flightInformation);
					                        		?>
					                        		@foreach($itinerario->flightInformation as $key2=>$flightInformation)
					                        			{{json_encode($flightInformation)}}
					                        			<?php
					                        				if($flightInformation->companyId->operatingCarrier == 'CM' || $flightInformation->companyId->marketingCarrier == 'CM'){
					                        					$contadorCopa++;
					                        				}

							                              $timezone = new DateTimeZone('UTC'); 
							                              $dateArrival = DateTime::createFromFormat('dmY', $flightInformation->productDateTime->dateOfArrival, $timezone);
							                              $dateDepature = DateTime::createFromFormat('dmY', $flightInformation->productDateTime->dateOfDeparture, $timezone);
			                             				?>
					                        			<div class="col-md-4">
					                        				<div class="fecha">Salida {{$dateDepature->format('d F')}}</div>
					                        				<div class="ciudad-hora">{{$flightInformation->location[0]->locationId}} {{date("G:i",strtotime($flightInformation->productDateTime->timeOfDeparture))}}</div>
					                        				<div>{{$flightInformation->location[0]->nombre}} - <b>{{$flightInformation->location[0]->cuidad}}, {{$flightInformation->location[0]->pais}}</b></div>
					                        			</div>
					                                    <div class="col-md-1 salida-fa" style="padding-left: 0px;padding-right: 0px;width: 8%;">
										                      <?php 
					                                            $duracions = date("G:i",strtotime($flightInformation->duracion));
					                                            $duracion =  explode(":", $duracions);
					                                          ?>
					                                          <i class="fa fa-chevron-right" aria-hidden="true"></i></br>
					                                          <span class="fec-llegada">{{$duracion[0]."h :".$duracion[1]."m"}}</span> 
					                                    </div>
					                        			<div class="col-md-4">
					                        				<div class="fecha">Llegada {{$dateArrival->format('d F')}}</div>
					                        				<div class="ciudad-hora">{{$flightInformation->location[1]->locationId}} {{date("G:i",strtotime($flightInformation->productDateTime->timeOfArrival))}}</div>
					                        				<div>{{$flightInformation->location[1]->nombre}}  - <b>{{$flightInformation->location[1]->cuidad}}, {{$flightInformation->location[1]->pais}}</b></div>
					                        			</div>
					                        			<div class="col-md-3 detalle-avion">
					                        			@if($flightInformation->companyId->operatingCarrier != null)
					                        				<img src="https://images.kiwi.com/airlines/64/{{$itinerary[0]->flightInformation[0]->companyId->operatingCarrier}}.png" class="imageHead">
					                        			@else
					                        				<img src="https://images.kiwi.com/airlines/64/{{$itinerary[0]->flightInformation[0]->companyId->marketingCarrier}}.png" class="imageHead">
					                        			@endif	
					                        				<div>
					                        					{{$flightInformation->companyId->marketingCarrier}} {{$flightInformation->flightOrtrainNumber}}
					                        					<br>
					                        					<b>{{$flightInformation->tipoAsiento}}</b> {{$flightInformation->aeronave}}
					                        				</div>
					                        			</div>
					                        			@if(($key2+1)!= $comparador)
						                        			<div class="col-md-12 tiempo-espera">
						                        				<i class="fa fa-clock-o" aria-hidden="true"></i> Espera de <b>{{$flightInformation->duracion}}</b> (Cambio de Avión)
						                        			</div>
						                        		@endif	
					                        		 @endforeach	
					                        	</div>
				                        	</div>
				                        @endforeach	
					                </div>
						        </div>
						    </div>
						</div>
					    <div class="panel style1">
                            <h4 class="panel-title">
                                <a href="#infopasajeros" data-toggle="collapse"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Información de Pasajeros</a>
                            </h4>
                            <div class="panel-collapse collapse in" id="infopasajeros">
                                <div class="panel-content">
									@for ($i = 0; $i < $adulto; $i++)
										<h4 class="pre-conf-titulo">Datos Personales del Pasajero {{$i+1}}</h4>
										<div class="row rowPreconfirmacion">
											<div class="col-xs-12 col-sm-3 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Nombres</label>
														<input type="text" required class = "form-control" maxlength="25" name="adt_nombre_{{$i}}" id="adt_nombre_{{$i}}">
												</div>
											</div>
											<div class="col-xs-12 col-sm-3 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Apellidos</label>
													<input type="text" required class = "form-control" name="adt_apellidos_{{$i}}" id="adt_apellidos_{{$i}}" maxlength="25" class="input-text full-width required">
												</div>
											</div>
											<div class="col-xs-12 col-sm-3 col-md-3">
												<div class="form-group">
													<label class="lbl-form">Sexo</label>
													<select name="adt_sexo_{{$i}}" required class = "Requerido form-control" class="input-text full-width" id="adt_sexo_{{$i}}">
														<option value="">Seleccione Sexo</option>
														<option value="F">Femenino</option>
														<option value="M">Masculino</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row rowPreconfirmacion">
											<div class="col-xs-12 col-sm-3 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Tipo de Documento</label>
													<select name="adt_tipoDocuento0_{{$i}}" class = "Requerido form-control selectTipo" class="input-text full-width selectTipo" required id="adt_tipoDocuento0_{{$i}}">
														<option value="">Seleccione Tipo de Documento</option>
														<option value="PP">Pasaporte</option>
														<option value="ID">Documento de Identidad</option>
													</select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-3 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Nro. de Documento</label>
													<input type="text" required class = "form-control" name="adt_nrodoc_{{$i}}" id="adt_nrodoc_{{$i}}" maxlength="10" class="input-text full-width required">
												</div>
											</div>
											<div class="col-xs-12 col-sm-3 col-md-3">
												<div class="form-group">
													<label class="lbl-form">Fecha de Nacimiento</label>
													 <input type="text" indice="adt_fechNac" name="adt_fechNac_{{$i}}" readonly="readonly"  class = "required form-control datepicker" id="adt_fechNac_{{$i}}" required>
												</div>
											</div>
										</div>
										<div class="booking-bg">
											<h4 class="box-title">Información del Contacto</h4>
											<div class="row">
												<div class="col-md-12 col-sm-3 col-md-4 " id="adt_fechaVencimiento_{{$i}}" style="display:none">
													<label class="lbl-form">Fecha de Vencimiento Pasaporte</label>
													 <input type="text" indice="adt_fechVenc" name="adt_fechVenc_{{$i}}" readonly="readonly"  class = "required form-control datepickerV" id="adt_fechVenc_{{$i}}" required>
												</div>

												<div class="col-md-12 col-sm-3 col-md-4">
													<label class="lbl-form">Correo Electrónico</label>
													<input type="mail"  class = "form-control" name="adt_mail_{{$i}}" id="adt_mail_{{$i}}" required class="input-text full-width required">
												</div>
												<div class="col-md-12 col-sm-3 col-md-4">
													<label class="lbl-form">Teléfono</label>
													<input type="number"  class = "form-control" name="adt_telefono_{{$i}}" id="adt_telefono_{{$i}}" required class="input-text full-width required">
												</div>
											</div>
										</div>
										@if($infante != 0)
											@if($infante >= $contadorInfante)
												<br> 
												<h4 class="pre-conf-titulo">Datos Personales del Infante {{$i+1}}</h4>
												<div class="row rowPreconfirmacion">
													<div class="col-xs-12 col-sm-3 col-md-4">
														<div class="form-group">
															<label class="lbl-form">Nombres</label>
															<input type="text"  class = "form-control" required name="inf_nombre_{{$i}}" maxlength="14" id="inf_nombre_{{$i}}">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4">
														<div class="form-group">
															<label class="lbl-form">Apellidos</label>
															<input type="text" required class = "form-control" name="inf_apellidos_{{$i}}" maxlength="14" id="inf_apellidos_{{$i}}" class="input-text full-width required">
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4">
														<div class="form-group">
															<label class="lbl-form">Sexo</label>
															<select name="inf_sexo_{{$i}}" class = "Requerido form-control" class="input-text full-width" required id="inf_sexo_{{$i}}">
																<option value="">Seleccione Sexo</option>
																<option value="FI">Femenino</option>
																<option value="MI">Masculino</option>
															</select>
														</div>
													</div>
												</div>	
												<div class="row rowPreconfirmacion">
													<div class="col-xs-12 col-sm-4 col-md-4">
														<div class="form-group">
															<label class="lbl-form">Tipo de Documento</label>
															<select name="inf_tipoDocuento0_{{$i}}" required class = "Requerido form-control selectTipo" id="inf_tipoDocuento0_{{$i}}">
																<option value="">Seleccione Tipo de Documento</option>
																<option value="PP">Pasaporte</option>
																<option value="ID">Documento de Identidad</option>
															</select>
														</div>
													</div>
													<div class="col-xs-12 col-sm-4 col-md-4">
														<div class="form-group">
															<label class="lbl-form">Nro. de Documento</label>
															<input type="text"  class = "form-control" required name="inf_nrodoc_{{$i}}" maxlength="10" id="inf_nrodoc_{{$i}}" class="input-text full-width required">
														</div>
													</div>
													<div class="col-md-12 col-sm-3 col-md-4" id="inf_fechaVencimiento_{{$i}}" style="display:none">
														<div class="form-group">
															<label class="lbl-form">Fecha de Vencimiento Pasaporte</label>
															 <input type="text" indice="inf_fechVenc" name="inf_fechVenc_{{$i}}" readonly="readonly" required class = "input-text full-width required datepickerV" id="inf_fechVenc_{{$i}}">
														</div>
													</div>												
													<div class="col-xs-12 col-sm-4 col-md-4">
														<div class="form-group">
															<label class="lbl-form">Fecha de Nacimiento</label>
															 <input type="text" indice="inf_fechNac" name="inf_fechNac_{{$i}}" readonly="readonly" required class = "input-text full-width required datepickerInfante" id="inf_fechNac_{{$i}}">
														</div>
													</div>												
												</div>
												<?php 
													$contadorInfante= $contadorInfante+1
												?>
											@endif
										@endif		
										<hr class="booking">
									@endfor

									@for ($i = 0; $i < $ninho; $i++)
										<h4 class="pre-conf-titulo">Datos del Pasajero - Niño {{$i+1}}</h4>
										<div class="row rowPreconfirmacion">
											<div class="col-xs-12 col-sm-3 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Nombres</label>
													<input type="text" maxlength="14" class = "form-control" name="chd_nombre_{{$i}}" required id="chd_nombre_{{$i}}">
												</div>
											</div>
											<div class="col-xs-12 col-sm-3 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Apellidos</label>
													<input type="text"  required class = "form-control" name="chd_apellidos_{{$i}}" id="chd_apellidos_{{$i}}" maxlength="14" class="input-text full-width required">
												</div>
											</div>
											<div class="col-xs-12 col-sm-3 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Sexo</label>
													<select name="chd_sexo_{{$i}}" required class = "Requerido form-control" id="chd_sexo_{{$i}}">
														<option value="">Seleccione Sexo</option>
														<option value="F">Femenino</option>
														<option value="M">Masculino</option>
													</select>
												</div>
											</div>
										</div>	
										<div class="row rowPreconfirmacion">	
											<div class="col-xs-12 col-sm-4 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Tipo de Documento</label>
													<select name="chd_tipoDocuento0_{{$i}}" required class = "Requerido form-control selectTipo" id="chd_tipoDocuento0_{{$i}}">
														<option value="">Seleccione Tipo de Documento</option>
														<option value="PP">Pasaporte</option>
														<option value="ID">Documento de Identidad</option>
													</select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-3 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Nro. de Documento</label>
													<input type="text"  class = "form-control" required maxlength="10" name="chd_nrodoc_{{$i}}" id="chd_nrodoc_{{$i}}" class="input-text full-width required">
												</div>
											</div>
											<div class="col-md-12 col-sm-3 col-md-4" id="chd_fechaVencimiento_{{$i}}" style="display:none">
												<div class="form-group">
													<label class="lbl-form">Fecha de Vencimiento Pasaporte</label>
													<input type="text" indice="chd_fechVenc" name="chd_fechVenc_{{$i}}" class = "input-text full-width required datepickerV" readonly="readonly" id="chd_fechVenc_{{$i}}" required >
												</div>
											</div>
											<div class="col-xs-12 col-sm-3 col-md-4">
												<div class="form-group">
													<label class="lbl-form">Fecha de Nacimiento</label>
													<input type="text" indice="chd_fechNac" name="chd_fechNac_{{$i}}" class = "input-text full-width required datepickerNino" readonly="readonly" id="chd_fechNac_{{$i}}" required >
												</div>
											</div>

										</div>
										<hr class="booking">
									@endfor
								</div>
							</div>
						</div>

						<div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#infoservicios" data-toggle="collapse"><i class="fa fa-plane" aria-hidden="true"></i> Servicios de Aerolíneas y Asientos</a>
                            </h4>
                            <div class="panel-collapse collapse" id="infoservicios">
                                <div class="panel-content">
                					<div class="row">
                					<?php 
		                				$tipo = Session::get('tipo');
		                				?>
                						@for ($x = 0; $x < $tipo; $x++)
		                					<?php 
		                						if($x == 0){
			                						$departurePoint = $itinerary[0]->departureLocalization->departurePoint->datosciudad;
			                						$arrivalPointDetails = $itinerary[0]->arrivalLocalization->arrivalPointDetails->datosciudad;
			                					}else{
			                						$departurePoint = $itinerary[0]->arrivalLocalization->arrivalPointDetails->datosciudad;
			                						$arrivalPointDetails = $itinerary[0]->departureLocalization->departurePoint->datosciudad;
			                					}	
		                					?>
	                    					<div class="col-xs-12 col-sm-6 col-md-6">
	                    						<div class="pre-conf-vuelo-ciudad">
													<div class="row">
								                    	<div class="col-md-5">
								                    		{{$departurePoint}}
								                    	</div>
								                    	<div class="col-md-2">
								                    		<i class="fa fa-chevron-right" aria-hidden="true"></i>
								                    	</div>
								                    	<div class="col-md-5">
								                    		{{$arrivalPointDetails}}
								                    	</div>
							                    	</div>
							                    </div>
							                <div>
							                <div class="row">
							                    <div class="col-md-12">
								                    <div class="toggle-container box">
								                    	<div class="panel style1">
								                            <h4 class="panel-title">
									                            <a class="collapsed" href="#equipaje{{$x}}" data-toggle="collapse">Equipaje</a>
								                            </h4>
								                            <div class="panel-collapse collapse" id="equipaje{{$x}}">
								                                <div class="panel-content">
								                               	 	@foreach($itinerario->flightInformation as $key2=>$flightInformation)
								                               	 		@if($key2 == 0)
										                                    <img src="https://images.kiwi.com/airlines/64/{{$flightInformation->companyId->marketingCarrier}}.png" class="imageHead"> {{$flightInformation->companyId->marketingCarrier}}  {{$flightInformation->productDetail->equipmentType}}  {{$flightInformation->aeronave}}  {{$flightInformation->tipoAsiento}}
										                                @endif    
									                                  @endforeach 
								                                    <div class="tab-container style1">
								                                       <ul class="tabs full-width">
								                                       	@for($a = 0; $a<$adulto; $a++)
												                            <li><a href="#equipaje-ida-pasajero{{$x}}{{$a}}" data-toggle="tab">Pasajero {{$a+1}}</a></li>
												                        @endfor
								                                       	@for($n = 0; $n<$ninho; $n++)
												                            <li><a href="#equipaje-ida-nino{{$x}}{{$n}}" data-toggle="tab">Niño {{$n+1}}</a></li>
												                        @endfor    
												                        </ul>
												                    <div class="tab-content">
												                    	@for($a = 0; $a<$adulto; $a++)
												                    		@if($a ==  0)
														                        <div class="tab-pane fade in active" id="equipaje-ida-pasajero{{$x}}{{$a}}">
														                    @else
														                        <div class="tab-pane" id="equipaje-ida-pasajero{{$x}}{{$a}}">
														                    @endif    	
													                            <div class="row">
													                                <div class="col-md-9">
													                                	<i class="fa fa-briefcase" aria-hidden="true"></i> Total amount for  {{$departurePoint}}  >  {{$arrivalPointDetails}}
													                                </div>
																					<div class="col-md-3 text-right">
													                                	0.00 USD
													                                </div>
																					<div class="col-md-9">
																						ADT {{$a+1}} Equipaje:
																						@if($equipaje)
																						{{$equipaje->freeAllowance}}@if($equipaje->freeAllowance =='N'){{'Unidad(es)'}}@else{{$equipaje->unitQualifier}}@endif
																						@endif
													                                </div>
																					<div class="col-md-3 text-right">
													                                	Free Allowance
													                                </div>
																					<div class="col-md-12">
																						<h3>Servio Stándard</h3>
																							<p>
																							Los Servicios Estándar podrían estar disponibles a petición con la Aerolínea y no están garantizados. Póngase en contacto con la aerolínea con la referencia de reserva para confirmar la disponibilidad del servicio y los cargos aplicables si los hubiere.
																							</p>
																							<!--<p><a href=""><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Servicio</a></p>-->
													                                </div>
													                            </div>
													                        </div>
													                    @endfor  
													                    @for($n = 0; $n<$ninho; $n++) 	
													                    	@if($n ==  0) 
														                        <div class="tab-pane fade" id="equipaje-ida-nino{{$x}}{{$n}}">
														                    @else
														                        <div class="tab-pane" id="equipaje-ida-nino{{$x}}{{$n}}">
														                    @endif 
													                            <div class="row">
													                                <div class="col-md-9">
													                                	<i class="fa fa-briefcase" aria-hidden="true"></i> Total amount for  {{$departurePoint}}  >  {{$arrivalPointDetails}}
													                                </div>
													                                <div class="col-md-3 text-right">
													                                	0.00 USD
													                                </div>
													                                <div class="col-md-9">
																						CHD {{$n+1}} Equipaje
													                                </div>
													                                <div class="col-md-3 text-right">
													                                	Free Allowance
													                                </div>
													                                <div class="col-md-12">
																						<h3>Servio Stándard</h3>
																						<p>
																						Los Servicios Estándar podrían estar disponibles a petición con la Aerolínea y no están garantizados. Póngase en contacto con la aerolínea con la referencia de reserva para confirmar la disponibilidad del servicio y los cargos aplicables si los hubiere.
																						</p>
																						<!--<p><a href=""><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Servicio</a></p>-->
													                                </div>
													                            </div>
													                        </div>
													                    @endfor     
												                    </div>
								                                </div>
								                             </div>
								                        </div>
								                    </div>
													<div class="panel style1">
														<h4 class="panel-title">
															<a href="#comida{{$x}}" data-toggle="collapse">Comidas</a>
														</h4>
														<div class="panel-collapse collapse" id="comida{{$x}}">
															<div class="panel-content">
																<div class="tab-container style1">
																	<ul class="tabs full-width">
																		@for($a = 0; $a<$adulto; $a++)
																			<li><a href="#comida-ida-pasajero{{$x}}{{$a}}" data-toggle="tab">Pasajero {{$a+1}}</a></li>
													                    @endfor  
													                    @for($n = 0; $n<$ninho; $n++) 	 
																			<li><a href="#comida-ida-nino{{$x}}{{$n}}" data-toggle="tab">Niño {{$n+1}}</a></li>
																		@endfor	
																	</ul>
																	<div class="tab-content">
																		@for($a = 0; $a<$adulto; $a++)
																			@if($a ==  0)
																				<div class="tab-pane fade in active" id="comida-ida-pasajero{{$x}}{{$a}}">
																				<?php 
														                        	$nombre = '_comidaIda_';
														                        ?>
																			@else
																				<div class="tab-pane" id="comida-ida-pasajero{{$x}}{{$a}}">
																				<?php 
														                        	$nombre = '_comidaVuelta_';
														                        ?>
																			@endif
																				<h3>Servicio Stándard</h3>
																				<p>
																				ADT {{$a+1}}- Los Servicios Estándar podrían estar disponibles a petición con la Aerolínea y no están garantizados. Póngase en contacto con la aerolínea con la referencia de reserva para confirmar la disponibilidad del servicio y los cargos aplicables si los hubiere.
																				</p>
																				<div class="form-group">
																					<label for="adt_comida_{{$x}}"><i class="fa fa-cutlery" aria-hidden="true"></i> Seleccione tipo de comida:</label>
																					<select class="form-control" id="adt{{$nombre}}{{$a}}" name="adt{{$nombre}}{{$a}}">
																					<option value="">Selecione comida</option>
																					@foreach($meals as $key=>$meal)
											                                            <option value="{{$meal['value']}}">{{$meal['label']}}</option>
						                                            				@endforeach
																					</select>
																				</div>
													                        </div>
													                    @endfor  
													                    @for($n = 0; $n<$ninho; $n++) 
													                    	@if($a ==  0)	 
														                        <div class="tab-pane fade" id="comida-ida-nino{{$x}}{{$n}}">
																				<?php 
														                        	$nombre = '_comidaIda_';
														                        ?>
														                     @else
														                        <div class="tab-pane" id="comida-ida-nino{{$x}}{{$n}}">
																				<?php 
														                        	$nombre = '_comidaVuelta_';
														                        ?>
														                     @endif   
													                            <h3>Servicio Stándard</h3>
																				<p>
																				CHD {{$n+1}}- Los Servicios Estándar podrían estar disponibles a petición con la Aerolínea y no están garantizados. Póngase en contacto con la aerolínea con la referencia de reserva para confirmar la disponibilidad del servicio y los cargos aplicables si los hubiere.
																				</p>
																				<div class="form-group">
																					<label for="chd_comida_{{$x}}"><i class="fa fa-cutlery" aria-hidden="true"></i> Seleccione tipo de comida:</label>
																					<select class="form-control" id="chd{{$nombre}}{{$x}}" name="chd{{$nombre}}{{$x}}">
																					<option value="">Selecione comida</option>
																					@foreach($meals as $key=>$meal)
											                                            <option value="{{$meal['value']}}">{{$meal['label']}}</option>
						                                            				@endforeach
																					</select>
																				</div>
													                        </div>
													                    @endfor      
												                    </div>
								                                </div>  
								                            </div>
								                        </div>
								                    </div>
								                    <div class="panel style1">
								                        <h4 class="panel-title">
								                            <a href="#asiento{{$x}}" data-toggle="collapse">Asientos</a>
								                        </h4>
								                        <div class="panel-collapse collapse" id="asiento{{$x}}">
								                            <div class="panel-content">
								                                <div class="tab-container style1">
								                                    <ul class="tabs full-width">
																		@for($a = 0; $a<$adulto; $a++)
													                        <li><a href="#asiento-ida-pasajero{{$x}}{{$a}}" data-toggle="tab">Pasajero 1</a></li>
													                    @endfor  
													                    @for($n = 0; $n<$ninho; $n++) 	 
													                        <li><a href="#asiento-ida-nino{{$x}}{{$n}}" data-toggle="tab">Niño 1</a></li>
													                    @endfor    
												                    </ul>
												                    <div class="tab-content">
																		@for($a = 0; $a<$adulto; $a++)
																			@if($a ==  0)
														                        <div class="tab-pane fade in active" id="asiento-ida-pasajero{{$x}}{{$a}}">
														                        <?php 
														                        	$nombre = '_asientoIda_';
														                        ?>
														                    @else
														                        <div class="tab-pane fade" id="asiento-ida-pasajero{{$x}}{{$a}}">
																				<?php 
														                        	$nombre = '_asientoVuelta_';
														                        ?>
														                    @endif   
																				<div class="form-group">
																					<label for="sel1"><i class="fa fa-plane" aria-hidden="true"></i>ADT-{{$a+1}} Seleccione lugar:</label>
																					<select class="form-control" name="ad{{$nombre}}{{$a}}" id="sel1">
																					 <option value="">Seleccione un tipo asiento</option>
																						@foreach($asientos as $key=>$asiento)
												                                            <option value="{{$asiento['value']}}">{{$asiento['label']}}</option>
						                                            					@endforeach
																					</select>
																				</div>
													                        </div>
																		@endfor  
													                    @for($n = 0; $n<$ninho; $n++)
													                    	@if($n ==  0)
																				<div class="tab-pane fade" id="asiento-ida-nino{{$x}}{{$n}}">
																				<?php 
														                        	$nombre = '_asientoId_';
														                        ?>
																			@else
																				<div class="tab-pane" id="asiento-ida-nino{{$x}}{{$n}}">
																				<?php 
														                        	$nombre = '_asientoVuelta_';
														                        ?>
																			@endif	
																				<div class="form-group">
																					<label for="sel1"><i class="fa fa-plane" aria-hidden="true"></i> CHD-{{$n+1}} Seleccione lugar:</label>
																					<select class="form-control" name="ch{{$nombre}}{{$a}}" id="sel1">
																					<option value="">Seleccione un tipo asiento</option>
																						@foreach($asientos as $key=>$asiento)
												                                            <option value="{{$asiento['value']}}">{{$asiento['label']}}</option>
						                                            					@endforeach
																					</select>
																				</div>
													                        </div>
													                    @endfor    
												                        </div>
								                                    </div>
								                                </div>
								                            </div>
								                        </div>
								                    </div>
							                    </div>
							                </div>
	                    				</div>
									</div>
									@endfor		
								</div>
							</div>
						</div>

					</div>

					<div class="panel style1">
                       <h4 class="panel-title">
                            <a href="#infoconfirmar" data-toggle="collapse"><i class="fa fa-check-square-o" aria-hidden="true"></i> Confirmar Reserva</a>
                        </h4>
                            <div class="panel-collapse collapse in" id="infoconfirmar">
                                <div class="panel-content">
									<div class="row">	
										<div class="col-sm-12 col-md-12">
											<div class="alert alert-info">
												<div class="row">	
													<div class="col-sm-5 col-md-5">
														<div class="form-group" style="margin-left: 2px;">
															<label>SELECCIONE EXPEDIENTE(*)</label>
															<div class="selector">
																<select required class="full-width" id="expediente" name="expediente">
																	<option value="0">Nueva Proforma</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-sm-5 col-md-5">
														<div class="form-group">
															<label>VENDEDOR DTP MUNDO</label>
															<div class="selector">
																<select class="full-width" id="agente" name="agente">
						                                            @foreach($selectAgentes as $key=>$agentes)
						                                                <option value="{{$agentes['value']}}">{{$agentes['label']}}</option>
						                                            @endforeach
																</select>
															</div>
														</div>
													</div>
												</div>
												<h4><i class="glyphicon glyphicon-info-sign"></i> Importante!</h4> <p>Al hacer click en "<b>Reservar</b>" Ud. acepta nuestros <a href="{{route('terminosycondiciones')}}" target="_blank"><b>Términos y Condiciones</b></a></p>
											</div>
				                        </div>
											
									</div>
							
				                    <div class="form-group row">
				                    	<div class="col-sm-6 col-md-6">
				                            <div class="total-vuelo" style="font-size: 2em;"">TOTAL $ {{number_format($monto,2,",",".")}}</div>
				                        </div>
				                        <div class="col-sm-6 col-md-6 text-right">
				                              <button type="submit" class="btn btn-info text-center btn transaction_normal hide-small normal-button" id="reservaBtn" style="background-color:#e2076a;">Reservar</button>
				                        </div>
				                    </div>

				                </div>

							</div>

						</div>

					</div>
						
                </form>
            
            </div>	
     </div>   
    </div>

    @if($contadorCopa != 0)
    	@include('partials/modalCopa')
    @endif
@endsection

@section('scripts')
    @parent
    <script>

    @if($contadorCopa != 0)    
		$('#Detalles').modal({
	        show: 'true'
	    }); 
	    $('#reservaBtn').prop("disabled",true);
	@endif
	

      $.unblockUI();
	  $( function() {
   		 $( ".datepickerV" ).datepicker({
   		 				dateFormat: 'dd/mm/yy',
   		 		      	changeMonth: true,
     				 	changeYear: true,
     				 	yearRange: '-0:+20'
   		 	});
   		$( ".datepicker" ).datepicker({
   		 				dateFormat: 'dd/mm/yy',
   		 		      	changeMonth: true,
     				 	changeYear: true,
     				 	yearRange: '-90:-0'
   		 	});
 		$( ".datepickerNino" ).datepicker({
   		 				dateFormat: 'dd/mm/yy',
   		 		      	changeMonth: true,
     				 	changeYear: true,
     				 	yearRange: '-12:-2'
   		 	}); 		
		$('.datepickerInfante').datepicker({
				minDate: '-730d',
				maxDate: '{{Session::get("fechaIda")}}',
				changeMonth: true,
     			changeYear: true,
				dateFormat: 'dd/mm/yy',
			});

		$(".selectTipo").change(function(event){
			indicador = $(this).attr('id').split('_');
			if($(this).val() == "PP"){
				//adt_fechaVencimiento_0
				console.log("#"+indicador[0]+"_fechaVencimiento_"+indicador[2]);
				$("#"+indicador[0]+"_fechaVencimiento_"+indicador[2]).show();

			}else{
				$("#"+indicador[0]+"_fechaVencimiento_"+indicador[2]).hide();
			}
		})

	  	$('#frmPreConfirmacion').submit(function(e){
	  		marcador = 0;
	  		//e.preventDefault();
			$(".datepicker" ).each(function(index, element) {
				if($('#adt_fechNac_'+index).val().length == 0) {
			   		$('#adt_fechNac_'+index).focus();
			   		marcador = 1;
			   		e.preventDefault();
			  	}
			})

			$(".datepickerNino" ).each(function(index, element) {
				if($('#chd_fechNac_'+index).val().length == 0) {
			   		$('#chd_fechNac_'+index).focus();
			   		marcador = 1;
			   		e.preventDefault();
			  	}
			})

			$(".datepickerInfante" ).each(function(index, element) {
				if($('#inf_fechNac_'+index).val().length == 0) {
			   		$('#inf_fechNac_'+index).focus();
			   		marcador = 1;
			   		e.preventDefault();
			  	}
			})
			if(marcador != 1){
				procesando();
			}
		})


	  $('#agente option[value="{{Session::get('datos-loggeo')->datos->datosUsuarios->codAgente}}"]').attr("selected", "selected");

		var inputs = JSON.parse('<?php echo json_encode($inputs); ?>');
		$.each(inputs, function(key,value){
			$('#'+key).val(value);
			if(key=='destino'){
				$('select').trigger("chosen:updated");
			}

		});

		});
	</script>
@endsection
