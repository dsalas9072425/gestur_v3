@extends('master')

@section('title', 'Booking de Vuelos')
@section('styles')
	@parent
@endsection
@section('content')
{{--json_encode($resultado)--}}
<section class="sectiontop">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="booking-information travelo-box">
					<div class="row row-eq-height">
						<div class="col-sm-3 col-md-3 localizador" style="padding-bottom: 0px;width: 40%;padding-top: 15%">
							<div>Localizador / <span class="text-italic">Reference number:</span></div>
							<div class="num-localizador" style="color:#fdb714">
								{{$resultado->controlNumber}}
							</div>
							<div>
								<!--<img src="{{asset('images/marviajes.png')}}" max-width="180">-->
							</div>
						</div>
						<div class="col-sm-9 col-md-9 booking-info-pasajero text-center">
							<?php 
								$total = count($resultado->segmentos) -1;
							?>
							@foreach($resultado->segmentos as $key=>$segmentos)
								<div class="row">
		                    		<div class="col-md-4">
		                    			<div>{{$segmentos->boardPointDetails->ciudad}}, <b>{{$segmentos->boardPointDetails->pais}}</b></div>
		                    		</div>
		                    		<div class="col-md-1 salida-fa">
		                    			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		                    		</div>
		                    		<div class="col-md-4">
		                    			<div>{{$segmentos->offpointDetails->ciudad}}, <b>{{$segmentos->offpointDetails->pais}}</b></div>
		                    		</div>
								</div>
								@if($total > $key)
									<hr style="margin-top: 2%; margin-bottom: 2%; height: 1px;">
								@endif
	                    	@endforeach
						</div>
					</div>
					<br>
					<div class="booking-pasajero">
						<h3 class="textblue">Pasajeros</h3>
						<div>
							@foreach($resultado->pasajeros as $key=>$pasajeros)
								<h4>{{$pasajeros}}</h4>
							@endforeach	
						</div>
					</div>
					<br>
					<br>
					@foreach($resultado->itinerario as $key=>$itinerario)
					<?php
                              $timezone = new DateTimeZone('UTC'); 
                              $dateInicio = DateTime::createFromFormat('dmY', $itinerario->fechaInicio, $timezone);
                              $dateArrival = DateTime::createFromFormat('dmY', $itinerario->fechaIn, $timezone);
                              $dateDepature = DateTime::createFromFormat('dmY', $itinerario->fechaOut, $timezone);
                              ?>
						<div class="row">
							<div class="col-sm-12 col-md-12 booking-vuelo">
								Vuelo -{{$itinerario->vuelo}} {{$itinerario->aerolinea}} - {{$dateInicio->format('d F')}}
							</div>
							<div class="col-sm-6 col-md-6 booking-vuelo-detalle-left">
								<div>
									<br>
									<b>Salida:</b> {{$itinerario->origen}}
									<br>
									<b>Fecha:</b> {{$dateDepature->format('d F')}} {{$itinerario->horaIn}}
									<br>
									<b>Duración:</b> {{$itinerario->diff}}
									<br>
									<b>Localizador Aerolínea:</b> {{$itinerario->localizador_aerolínea}}
									<br>
									<b>Reserva confirmada, {{$itinerario->tipo_asiento}}</b>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 booking-vuelo-detalle-right">
								<div>
									<br>
									<b>Llegada:</b> {{$itinerario->destino}}
									<br>
									<b>Fecha:</b> {{$dateArrival->format('d F')}}  {{$itinerario->horaOut}}
									<br>
									<b>Sin Parada</b>
									<br>
									<b>Equipaje Permitido:</b> 1PC
									<br>
									<b></b>
									<br>
									<!--<b>Comida:</b> Desayuno</b>-->
								</div>
							</div>
						</div>
					@endforeach	
					<div class="booking-pasajero">
						<!--<h3 class="textblue">Billetes Aéreos</h3>-->
						<div class="row">
							<div class="col-sm-9 col-md-9">
								<!--<div>
									@foreach($resultado->pasajeros as $key=>$pasajeros)
										BILLETE: PZH/ETKT 792 5280042170 por {{$pasajeros}}
										<br>
									@endforeach	
								</div>-->
							</div>
							<div class="col-sm-3 col-md-3 text-center padding-logo">
								<div>
								<img src="{{asset('images/logo-2.png')}}" width="150">
							</div>
							</div>
						</div>
						
					</div>

				</div>
				
			</div>
			
		</div>
                
    </div>

</section>
@endsection

@section('scripts')
    @parent
@endsection
