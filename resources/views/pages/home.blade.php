@extends('master')

@section('title', 'Desarrollo Turístico Paraguayo')

@section('styles')
	@parent
		<!-- load BXSlider Css // Mobile Tabs -->
		<link rel="stylesheet" href="{{ URL::asset('components/jquery.bxslider/jquery.bxslider.css') }}" media="screen" >
		<link rel="stylesheet" href="{{ URL::asset('css/font-awesome.css') }}" media="screen" >
@endsection

@section('content')
    @include('flash::message')
	<!-- Modal Habitaciones -->
    <div class="modal fade" id="Habitaciones" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="padding:15px 30px; background-color: #1c2b39;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 style="background-color: #1c2b39;"><span class="glyphicon glyphicon-bed"></span> Agregar Habitaciones</h4>
                </div>
                <div class="modal-body">
                    <form role="form" id= "formHabitaciones">
                        <button type="button" class="btn btn-primary pull-left" style="background-color: #e2076a;" id= "agregarHabitacion" onclick= "objBusqueda.habitaciones.agregar()"><span class="glyphicon glyphicon-plus-sign"></span> Agregar más habitación</button>
                        <button type="button" class="btn btn-info pull-right" id= "removerHabitacion" onclick="objBusqueda.habitaciones.remover()"><span class="glyphicon glyphicon-minus-sign"></span> Eliminar habitación</button>
                    </form>
                    <div class="clearfix"></div>
                </div>
				<div class="modal-footer">
                    <button type="button" onclick= "objBusqueda.crear();" class="btn btn-success btn-default pull-right" data-dismiss="modal" style="width: 100px; background-color: #e2076a;"><span class="glyphicon glyphicon-ok"></span> Aceptar</button>
                </div>
            </div>
        </div>
    </div>
	@php
		print_r(json_decode(Session::get('menuJson')));
	@endphp
	<div id="slide">
		<div id="multiQsfFlights" class="" style="opacity: 1;">
			<div id="contentsearch">
	            <div class="mq-header">
					<div class="multi-qsf" id="multiQsf" style="margin-left: 13%; margin-right: 13%;">
						<div class="mq-header">
							<div class="mq-shadow-left" style="opacity: 0;"></div>
							<div class="mq-shadow-right" style="opacity: 0;"></div>
							<div class="mq-tabs-wrapper">
								<div class="mq-tabs">
									<div class="mq-tab active" data-tab-id="Hotels">
										<a class="tablinks mq-tab-icon" onclick="openCity(event, 'hotels-tab')">
											<span class="icon" style="margin-bottom: 0px;"><img src="{{asset('images/icon/bed.png')}}" alt="Map of roads free icon" style="width: 31px;height: 18px;"></span>
											<span class="mq-tab-caption">Hoteles</span>
										</a>
									</div>
									<div class="mq-tab" data-tab-id="Flights">
										<a class="tablinks mq-tab-icon" id="flight" href="{{route('vuelos')}}">
											<span class="icon" style="margin-bottom: 0px;height: 24px;"><img src="{{asset('images/icon/plane.png')}}" alt="Map of roads free icon" style="width: 24px;height: 20px;"></span>
											<span class="mq-tab-caption">Vuelos</span>
										</a>
									</div>
										<div class="mq-tab" data-tab-id="Paquetes">
											<a class="tablinks mq-tab-icon" href="{{route('buscarPaquete')}}">
												<span class="icon" style="margin-bottom: 0px;"><img src="{{asset('images/icon/paquete.png')}}" alt="Map of roads free icon" style="width: 45px;height: 24px;"></span>
												<span class="mq-tab-caption">Paquetes</span>
											</a>
										</div>
									<div class="mq-tab" data-tab-id="Circuitos">
										<a class="tablinks mq-tab-icon" onclick="autoLogIn('{{$user->usuario}}', '{{$user->contrasenha}}')">
											<span class="icon" style="margin-bottom: 0px;"><img src="{{asset('images/icon/circuit.png')}}" alt="Map of roads free icon" style="width: 23px;height: 23px;"></span>
											<span class="mq-tab-caption">Circuitos</span>
										</a>
									</div>
									<div class="mq-tab" data-tab-id="Actividades">
										<a class="tablinks mq-tab-icon" onclick="autoLogIn('{{$user->usuario}}', '{{$user->contrasenha}}')">
											<span class="icon" style="margin-bottom: 0px;"><img src="{{asset('images/icon/maps.png')}}" alt="Map of roads free icon" style="width: 24px;height: 20px;"></span>
											<span class="mq-tab-caption">Actividades</span>
										</a>
									</div>
									<div class="mq-tab" data-tab-id="Traslados">
										<a class="tablinks mq-tab-icon" onclick="autoLogIn('{{$user->usuario}}', '{{$user->contrasenha}}')">
											<span class="icon" style="margin-bottom: 0px;"><img src="{{asset('images/icon/cars.png')}}" alt="Map of roads free icon" style="width: 23px;height: 19px;"></span>
											<span class="mq-tab-caption">Traslados</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="mq-content">
							<div id="hotels-tab" class="tabcontent tab-pane fade active" style="opacity: 1;">
								<section class="qsf-container standalone">
									<form action="{{route('search')}}" method="get" id="frmBusqueda" class="hotels-qsf tx-form qsf-visible" novalidate="novalidate">
										<div class="row">
											<div class="col-sm-4">
												<label for="code" class="required" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Destino</b></label>
												<input id="destino" name="destino" placeholder="¿Adónde vamos?" type="hidden">
												<input id="destinations_name" class="Requerido form-control">
											</div>
											<div class="col-sm-2 check-in-date">
												<label for="code" class="required" style="font-size: 11px; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Entrada</b></label>
													<input type="text" id="fecha_desde" name="fecha_desde" class="date" value = "" readonly="readonly" style="padding-left: 25%;"/>
													<button type="button" class="ui-datepicker-trigger" style="top: 35px;left: 15px;"></button>
											</div>
											<div class="col-sm-1">
												<label for="code" class="required" style="font-size: 11px; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Noches</b></label>
													<input type="number" min="1" max="30" value="1" class="destination text-center" name="cant_noches" id="cant_noches"/>

											</div>
											<div class="col-sm-2 check-out-date">
												<label for="code" class="required" style="font-size: 11px; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Salida</b></label>
												<input type="text" id="fecha_hasta" name="fecha_hasta" class="date required" value = ""  style="padding-left: 25%;" readonly="readonly"/>
											 	<button type="button" class="ui-datepicker-trigger" style="top: 35px;left: 15px;"></button>			
											</div>
											<div class="col-sm-2">
												<label>
										            <a data-toggle="modal" href="#Habitaciones" data-backdrop="static" class="mod-hab" >
										            <span class="glyphicon glyphicon-edit"></span> Modificar Habitación</a>
										        </label>
										        <p class="lblHabitaciones" id= "labelBusqueda"></p>
										    </div>
											<div class="col-sm-1" style="padding-left: 0px;">
												<button type="submit" id="btnBuscar" style="padding-top: 1%;" class="btn qsf-search hotels" tabindex="5">
													<span class="text"><i class="icon-loupe-search"></i></span>
												</button>											
											</div>
										</div>	
									</form>
								</section>
							</div>

							<div id="flights-tab" class="tabcontent tab-pane fade" style="opacity: 1; display:none">
								<section class="qsf-container standalone">
								 <div class="tab-pane fade active in" id="flights-tab" style="margin-left: 2%;margin-right: 1%;">
		                        	<div class="contentforms">
		                                <div class="row">
		                                    <div>
		                                    	<div class="tab-container">
			                                    	<ul class="tabs" style="border-bottom-width: 0px;">
			                                    		<li class="active"><label data-target="#idavuelta" style="font-size: 11px;" ><input type="radio" class="tipo_tabs" name="radio-tab" id="idayvuelta" checked="checked">Ida y Vuelta</label>
			                                    		<li><label data-target="#ida" style="font-size: 11px;"><input type="radio" name="radio-tab" class="tipo_tabs" id="ida">Sólo Ida</label></li>
			                                    	</ul>
			                                    	<div class="tab-content-search">
			                                    		<input type="hidden" form ="frmBusquedaFligth" id="tipo" name="tipo" value = "idayvuelta"/>
		 	                                    		<div class="tab-pane active in" id="idavuelta">
			                                    			<form action="{{route('searchFlight')}}" method="" id="frmBusquedaFligth" style="margin-left: 0px;">
					                                        	<div class="col-xs-12 col-sm-4 col-md-4">
				                                                    <div class="form-group">
				                                                    <label for="code" class="required" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Origen</b></label>
																		<input id="origen" name="origen" type="hidden">
																		<input id="origen_name" class="Requerido form-control" placeholder="Origen" style="height: 39px;">
				                                                    </div>
				                                                </div> 
				                                                <div class="col-xs-12 col-sm-4 col-md-4">
				                                                    <div class="form-group">
				                                                        <label for="code" class="required" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Destino</b></label>
																		<input id="destinoF" name="destino" type="hidden">
																		<input id="destination_name" placeholder="Destino" class="Requerido form-control" style="height: 39px;">
				                                                    </div>
				                                                </div>
				                                                 <div class="col-xs-6 col-sm-2 col-md-2">
				                                                    <div class="form-group">
				                                                        <label for="code" class="required" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Ida</b></label>
				                                                        <div>
				                                                            <input type="text" id="fecha_ida_vuelo" name="fecha_ida_vuelo" class="input-text full-width required" value = "" readonly="readonly" />
				                                                        </div>
				                                                    </div>
				                                                </div> 
				                                                  <input type="hidden" min="1" max="361" value="1" class="input-text full-width no-padding text-center" name="cant_noches" id="cant_nochesFligthF"/>  
		 														<div class="col-xs-6 col-sm-2 col-md-2" id="idaDiv">
				                                                    <div class="form-group">
				                                                        <label for="code" class="required" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Vuelta</b></label>
				                                                        <div>
				                                                            <input type="text" id="fecha_vuelta_vuelo" name="fecha_vuelta_vuelo" class="input-text full-width required" value = "" readonly="readonly" />
				                                                        </div>
				                                                    </div>
				                                                </div>    
				                                                <div class="col-xs-6 col-sm-3 col-md-3">
					                                                <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Adultos</b></label>
					                                                <div class="input-group form-group">
					                                                   	<div class="input-group-btn">
									                    					<button type="button" id="disminuir" class="btn btn-default botonIzquierdo" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-minus-sign" style="color:white"></i></button>
									                    				</div>
									                    				<input name="cantidad_adultos" id="cantidad_adultos" class="form-control contadorClase" value="1" readonly="readonly"/>
									                    				<div class="input-group-btn">
										                        			<button type="button" id="aumentar" class="btn btn-default botonIzquierdo" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-plus-sign" style="color:white"></i></button>
										                        		</div>
										                        	</div>
										                        </div>
										                        <div class="col-xs-6 col-sm-3 col-md-3">
					                                                <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Niños (2/11 AÑOS)</b></label>
										                        	<div class="input-group form-group">
										                        		<div class="input-group-btn">
									                    					<button type="button" id="disminuir-n" class="btn btn-default botonIzquierdo" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-minus-sign" style="color:white"></i></button>
									                    				</div>
									                    				<input name="cantidadN" id="cantidadN" class="form-control contadorClase" value="0" readonly="readonly"  />
									                    				<div class="input-group-btn">
										                        			<button type="button" id="aumentarN" class="btn btn-default botonDerecho" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-plus-sign" style="color:white"></i></button>
										                        		</div>
										                        	</div>
										                        </div>
										                        <div class="col-xs-6 col-sm-3 col-md-3">
					                                                <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;"><b>Infantes (0/2 AÑOS)</b></label>
										                        	<div class="input-group form-group">
										                        		<div class="input-group-btn">
									                    					<button type="button" id="disminuir-i" class="btn btn-default botonIzquierdo" style="background-color: #2d3e52;" ><i class="glyphicon glyphicon-minus-sign" style="color:white"></i></button>
									                    				</div>
									                    				<input name="cantidad_infantes" id="cantidad_infantes" class="form-control contadorClase" value="0" readonly="readonly"  />
									                    				<div class="input-group-btn">
										                        			<button type="button" id="aumentar-i" class="btn btn-default botonDerecho" style="background-color: #2d3e52;"><i class="glyphicon glyphicon-plus-sign" style="color:white"></i></button>
										                        		</div>
										                        	</div>
				                                                </div>
				                                               <input type= "hidden" id= "objBusqueda" name= "objBusqueda">
				                                                <div class="col-xs-6 col-sm-1 col-md-1">
				                                                <label for="code" class="required cantPasajeros" style="font-size: 11px;font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-stretch:condensed; margin-bottom: 0px;">&nbsp;</label>
				                                                    <button type="submit" id="btnFlightBuscar" class="btn qsf-search hotels" tabindex="5">
																		<span class="text"><i class="icon-loupe-search">Buscar</i></span>
																	</button>

				                                                </div>
					                                        </form>
			                                    		</div>
			                                    	</div>
			                                   </div>
			                                </div>
			                            </div>
			                        </div>
		                    	</div>

								</section>
							</div>

							<div id="circuitos-tab" class="tabcontent tab-pane fade" style="opacity: 1; display:none">
								<section class="qsf-container standalone">
										<label for="code" class="required">Nuevo Inicio3</label>
								</section>
							</div>

							<div id="actividades-tab" class="tabcontent tab-pane fade" style="opacity: 1; display:none">
								<section class="qsf-container standalone">
										<label for="code" class="required">Nuevo Inicio4</label>
								</section>
							</div>

							<div id="traslado-tab" class="tabcontent tab-pane fade" style="opacity: 1; display:none">
								<section class="qsf-container standalone">
										<label for="code" class="required">Nuevo Inicio5</label>
								</section>
							</div>
							<div id="paquetes-tab" class="tabcontent tab-pane fade" style="opacity: 1; display:none">
								<section class="qsf-container standalone">
									<form action="#" method="get" id="frmPaquete" class="hotels-qsf tx-form qsf-visible" novalidate="novalidate">
										<div class="row">
										
										</div>
									</form>
								</section>		
							</div>
						</div>
						<div class="mq-footer">
														
						</div>
					</div>
	            </div>
			</div>
		</div>
	</div>
@endsection

@include('partials/modalPromocion')

@section('scripts')
    @parent



@endsection