@extends('master')
@section('title', 'Perfil de Usuario')
@section('styles')
	@parent
@endsection

@section('content')

<section id="content" class="gray-area">
    <div class="container">
        <div id="main">
           @include('flash::message')
            <div class="block">
                <h2></h2>
                <br>
                <br>
                <div class="tab-container full-width-style white-bg">
                    <ul class="tabs">
                        <li class="active"><a href="#one-tab" data-toggle="tab"><i class="soap-icon-user circle"></i><b>Actualizar <br>Datos</b></a></li>
                        <li><a href="#two-tab" data-toggle="tab"><i class="soap-icon-key circle"></i><b>Actualizar<br>Password</b></a></li>
                         @if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia != 1)<li><a href="#third-tab" data-toggle="tab"><i class="soap-icon-user circle"></i>Actualizar<br>Agente</a></li>@endif
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="one-tab">
                            <h2 class="tab-content-title">Actualizar Datos</h2>
                            <form id="editDateForm" action="{{route('doEditPerfil')}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" value="{{ config('constants.tipoDatos')}}" class="input-text full-width" name="tipo">
                                <input type="hidden" value="{{$datosUsuario->datos->datosUsuarios->idUsuario}}" class="input-text full-width" name="id_usuario">
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label>Usuario</label>
                                        <input value="{{$datosUsuario->datos->datosUsuarios->usuario}}" class="input-text full-width" name="usuario" disabled="true">
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label>Nombre y Apellido</label>
                                        <input value="{{ $usuario->nombre_apellido}}" required class = "Requerido form-control" name="nombre_apellido">
                                    </div>
                                </div>
                            </form>
                            <input type="submit" class="btn btn-primary btn-block" value="ACTUALIZAR" form="editDateForm" style="height: 40px; background-color: #e2076a">
                        </div>
                        <div class="tab-pane" id="two-tab">
                            <h2 class="tab-content-title">Actualizar Password</h2>
                            <form id="editProfileForm" action="{{route('doEditPerfil')}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" value="{{config('constants.tipoContrasena')}}" class="input-text full-width" name="tipo">
                                <input type="hidden" value="{{$datosUsuario->datos->datosUsuarios->idUsuario}}" class="input-text full-width" name="id_usuario">
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label>Nueva Contraseña</label>
                                        <input type="password" value="" minlength="7" maxlength="10" id= "password" required class = "Requerido form-control" name="pass">
                                    </div>    
                                    <div class="form-group col-xs-12 col-sm-6">
                                        <label>Confirmar Contraseña</label>
                                        <input type="password" value="" minlength="7" maxlength="10" required id= "confirm_password"  class = "Requerido form-control" name="confirm_pass">
                                    </div>
                                </div>
                            </form>
                            <input type="submit" class="btn btn-primary btn-block" style="height: 40px; background-color: #e2076a" value="ACTUALIZAR" form="editProfileForm">
                        </div>
                        {{-- @if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil != config('constants.adminAgencia')) --}}
						@if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia != 1)
                           <div class="tab-pane" id="third-tab">
                                <h2 class="tab-content-title">Actualizar Agente</h2>
                                <form id="agenteProfileForm" action="{{route('doEditPerfil')}}" method="post">
                                    {{csrf_field()}}
                                <input type="hidden" value="{{config('constants.tipoAgente')}}" class="input-text full-width" name="tipo">
                                <input type="hidden" value="{{$datosUsuario->datos->datosUsuarios->idUsuario}}" class="input-text full-width" name="id_usuario">
                                    <div class="row">
										<!--
                                        <div class="form-group col-xs-12 col-sm-6">
                                            <label>Usuario</label>
                                            <input value="{{$datosUsuario->datos->datosUsuarios->usuario}}" disabled="true" class="input-text full-width" name="usuario">
                                        </div>
										-->
                                        <div class="form-group col-xs-12 col-sm-6">
                                            <label>Agentes</label>
                                            <select name="codigo_agente" required class = "Requerido form-control" class="input-text full-width required" id="perfil_id">
                                                @foreach($selectAgentes as $key=>$agentes)
                                                    <option value="{{$agentes['value']}}"  @if($agentes['value'] == Session::get('datos-loggeo')->datos->datosUsuarios->codAgente )  selected  @endif">{{$agentes['label']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>                                   
                                </form>
                                <input type="submit" class="btn btn-primary btn-block" value="ACTUALIZAR" form="agenteProfileForm" style="height: 40px; background-color: #e2076a">
                           </div>
                        @endif    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
    @include('layouts/scripts')
    <script>
        var password = document.getElementById("password");
        var confirm_password = document.getElementById("confirm_password");

        function validatePassword(){
            if(password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Las contraseñas no coinciden");
            }else{
                confirm_password.setCustomValidity('');
            }
        }
        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    </script>

@endsection
