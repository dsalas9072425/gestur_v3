@extends('master')

@section('title', 'Booking de Vuelos')
@section('styles')
	@parent
	<style>
		.localizador {
    		background-color: #fff;
    	}	
	</style>
@endsection
@section('content')
<section class="sectiontop">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="booking-information travelo-box" style="padding-top: 30px;">
					<div class="row row-eq-height">
						<div class="col-sm-3 col-md-3 localizador" style="padding-bottom: 0px;width: 40%;padding-top: 1%">
							<div><span class="text-italic"></span></div>
							<div class="num-localizador" style="color:#fdb714">
								<img src="{{asset('images/logo-4.png')}}" width="220">
							</div>
						</div>
						<div class="col-sm-6 col-md-6 booking-info-pasajero text-center" style="background-color: #fff;">
							<br>
							<br>
							<h1 style="font-size: 30px;"><b>TODOS GANAN CON DTP!!!</b></h1>
						</div>
						<div class="col-sm-3 col-md-3 localizador" style="padding-bottom: 0px;width: 40%;padding-top: 1%">
							<div><span class="text-italic"></span></div>
							<div class="num-localizador" style="color:#fdb714">
								<img src="{{$imagen}}" width="90">
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-sm-12 col-md-12 booking-vuelo" style="padding-top: 20px;padding-bottom: 20px;padding-left: 30px; font-size: 17px;">
							<div class="row">
		                    	<div class="col-md-8">
		                    		<spam><b>Agencia: </b></spam><spam>{{$resultado->agencia}}</spam><br>
		                    		<spam><b>Vendedor: </b></spam><spam>{{$resultado->nombre_apellido}}</spam><br>
		                    		<spam><b>Fecha: </b></spam><spam>{{date('d/m/Y')}}</spam><br>
		                    	</div>
		                    	<div class="col-md-4">
		                    		<spam>VOUCHER #</spam><spam style="font-size: 20px;"><b>{{$indice}}</b></spam><br>
		                    		<spam>USD: </spam><spam>{{$resultado->monto}}</spam><br>
		                    		<spam><b>Concepto:</b> </spam><spam> Promo/Incentivo</spam>
		                    	</div>
							</div>
						</div>
							<div class="col-sm-12 col-md-12 booking-vuelo-detalle-right" style="border-left: 1px solid #666;margin-bottom: 5px;">
								<div style="margin-top: 15px;margin-left: 15px;">
									<b style="font-size: 18px;">Facturas Canjeadas:</b> 
									<br>
									<?php $contador = 0;?>
									@foreach($resultado->facturas as $factura)
										@if($contador ==0)
											<div clas="row">
											<div class="col-md-3"></div>
										@endif
										<div class="col-md-3" style="font-size: 16px;">{{$factura}}</div>
										<?php 
											$contador++
										?>
										@if($contador == 3)
											</div>
											<?php 
												$contador = 0;
											?>	
										@endif
									@endforeach
								</div>
							</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<small style="font-size:  x-small;">
								1- El monto de los canjes en efectivo es IVA incluido y se requiere la presentación de una factura legal. El canje puede realizarse en GS o USD tomando el cambio del día DTP.<br>
								2- Una vez generada esta boleta, se deberá descargar y enviar un correo a <b>administracion@dtp.com.py</b> adjuntando el voucher.<br>
								3. El canje generado con empresas asociadas a DTP no requiere una factura legal por parte del vendedor de la agencia. DTP entregará el vale legal en GS tomando el cambio del día DTP.
								4. Desarrollo Turístico Parsguayo es agente de retención y por los montos imponibles se expedirá el documento legal correspondiente. Valor imponible para retención: 812.524 gs
							</small>	
						</div>	
					</div>
					<div class="booking-pasajero">
						<a style="margin-left: 85%;height: 45px;width: 160px;padding-top: 10px; background-color: #f80a84" class="btn" href="{{route('voucherpdf')}}">Descargar<i class="fa fa-file-pdf-o"></i></a>
					</div>
				</div>
			</div>
		</div>
    </div>
</section>
@endsection

@section('scripts')
    @parent
@endsection
