@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
    <style type="text/css">
		.error{
			color:red;
		}
	</style>
@endsection
@section('content')
<style>
	.card,.card-header {
        border-radius: 14px !important;
        }
</style>

<section id="base-style">
	
    <div class="card" sltyle="border-radius: 14px;">
        <div class="card-header" style="border-radius: 14px;">
            <h4 class="card-title">Editar Tipo Ticket</h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-content collapse show" aria-expanded="true">
            <div class="card-body"> 
<!--funciona para todas las validacio-->

                    <Form action="{{route('tipoticket.update', $tipoTicket->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
	            	<div class="row"  style="margin-bottom: 1px;">
						<div class="col-xs-12 col-sm-3 col-md-4" style="padding-left: 15px;padding-right: 25px;">
							<div class="form-group">
								<label for="descripcion">Descripcion<label class="error"></label></label>
                                <input type="text" id="descripcion"  required name="descripcion"  class="form-control" value="{{ $tipoTicket->descripcion }}">
                         
                            </div>
						</div>	
                        <div class="col-12 col-sm-3 col-md-4" >
                            <div class="form-group">
                                <label>Empresa</label>
                                <select class="form-control" name="id_empresa"  id="id_empresa" tabindex="1" style="width: 100%;">
                                    <option value="">Todos</option>
                                    @foreach ($empresas as $empresa)
                                    <option value="{{ $empresa->id }}" {{ $empresa->id == $tipoTicket->empresa->id ? 'selected' : '' }}>
                                        {{ $empresa->denominacion }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
					</div>
					
			</div>	
	            	
            <div class="col-12">
                <button type="submit" id="guardarEdicion"  class="btn btn-success btn-lg pull-right mb-1" tabindex="17"><b>GUARDAR</b></button>
            </div>
				
				</form>	
				
            </div>
        </div> 
    </div>       	
</section>


@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('gestion/app-assets/js/jquery.validate.min.js')}}"></script>
<script>
$("#guardarEdicion").validate({  // initialize plugin on the form
    debug: false,
    rules: {
        "deescripcion": {
            required: true
        },

    },
    messages: {
        "descripcion": {
            required: "<br>No puede quedar vacio el campo"
        },

    }
});

</script>
@endsection