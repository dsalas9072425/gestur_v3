
@extends('masters')
@section('title', 'Panel de Control')
@section('styles')
	@parent
    <style>
        .pagination .page-item {
            border-radius: 50%;
        }
    </style>
@endsection
@section('content')

<section id="base-style">

	<div class="card">
		<div class="card-header">
			<h4 class="card-title">Listado Parametros</h4>
			<div class="heading-elements">
				<ul class="list-inline mb-0">
					<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
				</ul>
			</div>
		</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-md-9">
                    
                        <a href="{{ route('tipoticket.create') }}" class="btn btn-info">Crear Parametro</a>

                    </div>




                    
                    <div class="col-md-3">
                        <form action="{{ route('tipoticket.index') }}">
                            <LAbel>Buscar:</LAbel>
                            <input type="search" name="buscar" class="form-control">                            
                        </form>
                    </div>
                </div>


                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>DESCRIPCION</th>
                            <th>EMPRESA</th>
                            <th>ACCION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tipoTickets  as $ticket)

                        <tr>
                            <td>{{ $ticket->id }}</td>
                            <td>{{ $ticket->descripcion }}</td>
                            <td>{{ $ticket->empresa ? $ticket->empresa->denominacion : '' }}</td>
                             
                            <td>
                                <a href="{{route('tipoticket.edit', $ticket->id)}}" class="btn btn-warning">
                                    <span class="fa fa-edit"></span>
                                </a>
                        
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $tipoTickets->links('pagination::bootstrap-4') }}
                TOTAL DATOS: {{ $tipoTickets->total() }}
  

            </div>
        </div>

    </div>
</div>

@endsection