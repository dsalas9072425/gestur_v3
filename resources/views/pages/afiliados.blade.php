@extends('master')

@section('title', 'Detalle de Reserva')
@section('styles')
	@parent
	<style type="text/css">
            #container-main h1{
                font-size: 40px;
                text-shadow:4px 4px 5px #16a085;
            }

            .accordion-container {
                width: 100%;
                margin: 0 0 20px;
                clear:both;
            }

            .accordion-titulo {
                position: relative;
                display: block;
                padding: 20px;
                font-size: 24px;
                font-weight: 300;
                background: #fff;
                color: #2c3e50;
                text-decoration: none;
                text-align: center;
            }
            .accordion-titulo.open {
                background: #fff;
                color: #2c3e50;
                text-align: center;
            }
            .accordion-titulo:hover {
                background: #fff;
                color: #2c3e50;
                text-align: center;
            }

            .accordion-titulo span.toggle-icon:before {
                content:"+";
            }

            .accordion-titulo.open span.toggle-icon:before {
                content:"-";
            }

            .accordion-titulo span.toggle-icon {
                position: absolute;
                top: 10px;
                right: 20px;
                font-size: 38px;
                font-weight:bold;
            }
            .tooltip{
                  display: inline;
                  position: relative;
              }
              
            .tooltip:hover:after{
                  background: #333;
                  background: rgba(0,0,0,.8);
                  border-radius: 5px;
                  bottom: 26px;
                  color: #fff;
                  content: attr(title);
                  left: 20%;
                  padding: 5px 15px;
                  position: absolute;
                  z-index: 98;
                  width: 220px;
              }
              
            .tooltip:hover:before{
                  border: solid;
                  border-color: #333 transparent;
                  border-width: 6px 6px 0 6px;
                  bottom: 20px;
                  content: "";
                  left: 50%;
                  position: absolute;
                  z-index: 99;
              }
 
    </style>
@endsection
@section('content')
<section class="sectiontop">
	<div class="container">
    <div class="accordion-container">
        <a href="#" title="Las tarifa, acuerdos comerciales se encuentran disponibles a la hora de cotizar por DTPMundo" class="accordion-titulo">CONTRATOS ONLINE&nbsp;&nbsp;<span class="toggle-icon"></span></a>
        <div class="accordion-content">
            <div class="row bm-portfolio onepixel portfolio-filter isotope" style="position: relative; overflow: hidden; height: 2560px;">
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_best western.jpg')}}" style="height: 160px;width: 360px; margin-top: 20px;border-radius: 5px;background-color: #ffffff;" title="Hotelbeds" alt="Hotelbeds">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_days inn.jpg')}}" style="height: 160px;width: 360px; margin-top: 20px;border-radius: 5px;background-color: #ffffff;" title="Abreu" alt="Abreu">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_solaris-15.jpg')}}" style="height: 160px;width: 360px; margin-top: 20px;border-radius: 5px; background-color: #ffffff;" title="Action Travel" alt="Action Travel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_solaris-16.jpg')}}" style="height: 160px;width: 360px; margin-top: 20px;border-radius: 5px;background-color: #ffffff;" title="Amadeus" alt="Amadeus">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                       
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_Hyatt.jpg')}}" style="height: 160px;width: 360px; margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Bahía Principe" alt="Bahía Principe">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_NOBILE.jpg')}}" style="height: 160px;width: 360px; margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Booköhotel" alt="Booköhotel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_nobu hotel.jpg')}}" style="height: 160px;width: 360px; margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Bedsonline" alt="Bedsonline">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/panama_jack.png')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/royal_playa_del_carmen.png')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_park royal.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_solaris-17.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliados/sanctuary.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/UNICO.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>

               <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_hard rock.jpg')}}" style="height: 160px;width: 360px;margin-bottom: 15px;margin-top: 15px;border-radius: 5px;padding-top: 10px;padding-bottom: 10px;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_oasis.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/costao.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>
            </div>        
        </div>  
    </div>
    
    <div class="accordion-container">
        <a href="#" class="accordion-titulo">CONTRATOS OFFLINE<span class="toggle-icon"></span></a>
        <div class="accordion-content">
            <div class="row bm-portfolio onepixel portfolio-filter isotope" style="position: relative; overflow: hidden; height: 2560px;">
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(236px, 0px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_bahia principe.jpg')}}" style="height: 150px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Flight Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 0px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/blue.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                       
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_crown paradise.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(236px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/amresorts.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(472px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/decameron.png')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_divi resorts.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div> 
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_iberostar.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div> 
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_melia.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div> 
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/palace_resorts.jpeg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div> 
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_palladium.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div> 
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_riu.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div> 
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_royalton.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div> 
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_sandos.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div> 
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Tours isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(708px, 160px, 0px);">
                    <div class="portfolio-item">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <img class="img-responsive" src="{{asset('images/afiliado/Sin título-13_vsita sol.jpg')}}" style="height: 160px;width: 360px;  margin-bottom: 15px; margin-top: 15px; border-radius: 5px;background-color: #ffffff;" title="Carrusel" alt="Carrusel">
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>

</section>		

@endsection
@section('scripts')
    @parent
	<script type="text/javascript" src="{{ URL::asset('js/api.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/countto.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/hoverIntent.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/analytics.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/easing.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/fitvids.js')}}"></script> 
    <script type="text/javascript" src="{{ URL::asset('js/functions.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/superfish.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/mmenu.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/magnificpopup.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/isotope.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/jquery.themepunch.revolution.min.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/jquery.themepunch.plugins.min.js')}}"></script> 
    <script type="text/javascript">
        $(".accordion-titulo").click(function(){
           var contenido=$(this).next(".accordion-content");
                    
           if(contenido.css("display")=="none"){ //open     
              contenido.slideDown(250);         
              $(this).addClass("open");
           }
           else{ //close        
              contenido.slideUp(250);
              $(this).removeClass("open");  
          }
                                    
        });
        $(".accordion-content").collapse();
    </script>
@endsection
