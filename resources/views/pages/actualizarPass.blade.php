<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8">
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9">
<![endif]-->
<!--[if gt IE 9]><!--> 
<html> <!--<![endif]-->
<head>
    <!-- Page Title -->
     <title>DTPMundo - Actualizar Contraseña</title>
	@include('layouts/meta')
	@include('layouts/styles2')
</head>
<body class="soap-login-page style1 body-blank" style="margin:0;background-image:url({{asset('images/textura.jpg')}});	background-repeat:repeat;">
    <div id="page-wrapper" class="wrapper-blank">
        <section id="content">
            <div class="container">
                <div id="main">
					<h1 class="logo block text-center">
	                    <a href="{{route('home')}}" title="DTP Mundo - Home">
                            <img src="{{asset('images/logo-2.png')}}" style="width: 18%;"/>
                        </a>
	                </h1>
	                {{--@include('flash::message')--}}
	                <div class="text-center yellow-color" style="font-size: 4em; font-weight: 450; line-height: 1em; margin-top: 20px; color:#1c2b39 !important;"></div>
	                <br/>
	                <div class="row">
						<div class="col-md-4">
						</div>
						<div class="col-md-3 divContainer">
							<br>
							<br>
							<div style="border-top-width: 1px; border-color: #111">
	                            <form  id="actualizar-form" action="{{route('actualizar')}}" method="get">
	                                <div class="form-group">
	                                    <!--<input type="text" class="input-text form-control logForm" id="usuario" name="usuario" placeholder="Ingrese su E-mail">-->
										<p><label>
											<input id="password1" minlength=5 type="password" required class="input-text form-control logForm" placeholder="Ingrese su nuevo password">
											</label></p>

	                                </div>
	                                <div class="form-group">
	                                    <!--<input type="password" class="input-text form-control logForm" id="password" name="password" placeholder="Ingrese su Password">-->
										<p><label>
											<input id="password2" class="input-text form-control logForm" minlength=5 type="password" required placeholder="Ingrese su nuevo password de nuevo" ></label></p>

	                                </div>
	                                <div class="form-group">

	                                </div>
	                                <!--<button type="submit" id="ingresarCuenta" class="btn full-width yellow" style="background-color: #e2076a" style="color:#ffffff;">INGRESAR A SU CUENTA</button> -->
									<button type="submit" class="btn full-width yellow" style="background-color: #e2076a" style="color:#ffffff;" >ACTUALIZAR</button>		

	                                <br /><br />	
									
                            	</form>
	                        </div>						
	                    </div>
						<div class="col-md-4">
						</div>
	                </div>    
                </div>
            </div>
        </section>
			
			
			<footer id="footer">
				<div class="footer-wrapper">
					<div class="container">
						<div class="copyright">
							
						</div>
					</div>
				</div>
			</footer>
    </div>
	
		@include('layouts/scripts')
		<script type="text/javascript">
			$(function(){
				$('#actualizar-form').submit(function(e){
					e.preventDefault();
					
					if($('#password1').val() != $('#password2').val()){
						alert('los password deben ser iguales');
						return;
					}
					var queryString = "password="+$('#password1').val() + "&tokenEmail={{$tokenEmail}}";
					console.log($('#actualizar-form').attr('action') +"?"+queryString);
					var desRetorno = '';
					$.ajax({
						url: $('#actualizar-form').attr('action'),
						type: 'GET',
						dataType: 'json',
						data: queryString,
						
						success: function(jsonRsp){
							console.log(jsonRsp);
							if(jsonRsp.codRetorno ==0){
								msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i>     Recuperación de Contraseña</h2><p>'+jsonRsp.desRetorno+'.</p><p><br><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
								mostrarMensaje(msg);
								functionBoton();
							}else{
							 alert(jsonRsp.desRetorno);
							} 
						},
						error: function(jsonRsp){
							alert("Hubo un error");
						},
						complete: function(){
							 window.location = "{{route('login')}}";
						}
						
					});
					
				});
				
			});
			function functionBoton() {
				$('#aceptar').on('click',function(){
					window.location = "{{route('login')}}";
				})

			}	
		</script>
	</body>
</html>














