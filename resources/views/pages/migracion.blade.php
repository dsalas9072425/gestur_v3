@extends('master')
@section('title', 'Migración de Tablas')
@section('styles')
	@parent
@endsection

@section('content')
<section id="content" class="gray-area">
    	<div class="container">
		<h2 class="entry-title">Migración de Datos</h2>
			<div class="col-md-3">
			</div>	
			<div id="main" class="col-md-9">
	        	<form id="form-convertir" method="get" action="">
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h3>Tabla</h3>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h3>Cant. Campos</h3>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h3>Acción</h3>
        			</div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Monedas(Currency)</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>10</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="currency" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_moneda" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Destination DTP</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>7</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="destinationDTP" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_destinationdtp" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Destination</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>22</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="destination" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_destination" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Infoprovider</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>5</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="infoprovider" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_infoprovider" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Paymentmethod</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>3</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="paymentmethod" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_paymentmethod" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Perfiles</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>2</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="perfiles" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_perfiles" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Perfil_Paymentmethod</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>2</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="perfilPaymentmethod" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_perfil_paymentmethod" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Estados (State)</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>4</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="estados" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_state" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Accesos_Token</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>12</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="accesosToken" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_accesosToken" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Agencias</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>14</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="agencias" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_agencias" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Sucursales</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>9</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="sucursales" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_sucursales" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Usuarios</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>17</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="usuarios" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_usuarios" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>Reserva</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>61</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="reserva" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_reserva" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	
        		<div class="row">
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>FileReserva</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<h4>4</h4>
        			</div>
        			<div class="col-xs-12 col-sm-4 col-md-4">
        				<strong><button type="button" id="fileReserva" class="btn btn-primary migrar" data-dismiss="modal">Migrar</button></strong>
        			</div>
        			<div id="div_fileReserva" class="col-xs-12 col-sm-12 col-md-12"></div>
        		</div>	

        		<!--<button type="button" id="convertir" class="btn btn-primary" data-dismiss="modal">Migrar</button>-->
    		</form>
    	</div>
    </div>		
</section>
@endsection

@section('scripts')
	@include('layouts/scripts')
	<script>
		$(document).ready(function() {
			//Inicio de los Filtros
			$(".migrar").click(function(){
				var dataDetail = 'codTabla='+$(this).attr('id');
				loading();
				$.ajax({
					type: "GET",
					url: "{{route('getMigrations')}}",
					dataType: 'json',
					data: dataDetail,
					success: function(rsp){
									$.unblockUI();
									console.log("#div_"+rsp[0].indice);
									$("#div_"+rsp[0].indice).html("RESUMEN <br> <strong color='red'>Total de Registros:<strong>"+rsp[0].total_registros+"<br>"+"<strong color='red'>Total de Registros Migrados : <strong>"+rsp[0].total_migrados);
									},
					});		
			})
		})		
	</script>			
@endsection