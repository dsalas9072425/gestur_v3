@extends('master')

@section('title', 'Búsqueda de hoteles')
@section('styles')
	@parent
@endsection

@section('content')
<section class="sectiontop">

	<div class="container">

		<div class="row">

			<div class="col-sm-12 col-md-12">

				<div class="booking-information travelo-box">
					<div class="row">
						<div class="col-sm-3 col-md-9">
							<h2 class="text-center">Bono / <span class="text-italic">Voucher</span> / Traslado / <span class="text-italic">Transfers</span></h2>
							<p class="text-center">Reserva Confirmada y Garantizada - Bono - Traslado / <span class="text-italic">Booking confirmed and guaranteed - Voucher - Transfer</span></p>
						</div>
						<div class="col-sm-3 col-md-3">
							<img src="{{asset('images/marviajes.png')}}" width="180">
						</div>
					</div>

					<div class="row row-eq-height">
						<div class="col-sm-3 cold-md-3 localizador">
							<div>Localizador / <span class="text-italic">Reference number:</span></div>
							<div class="num-localizador">66-1391527</div>
						</div>
						<div class="col-sm-9 col-md-9 booking-info-pasajero">
							<div class="row">
								<div class="col-sm-4 col-md-4">
									<div><b>Expediente TO</b> / <span class="text-italic">File TO:</span></div>
									<div>74919</div>
								</div>
								<div class="col-sm-8 col-md-8">
									<div><b>Nombre del Pasajero</b> / <span class="text-italic">Passenger name:</span></div>
									<div class="nombre-pasajero">José Carlos Galeano</div>
								</div>
							</div>
							
						</div>
					</div>

					<div class="fecha-reserva">
					<p></p>
					</div>

					<div class="row">
						<div class="col-sm-3 col-md-3 booking-traslado-title">
							Tipo de Producto / <span class="text-italic">Product Type</span>
						</div>
						<div class="col-sm-3 col-md-3 booking-traslado-title">
							Desde / <span class="text-italic">From</span>
						</div>
						<div class="col-sm-3 col-md-3 booking-traslado-title">
							Hasta / <span class="text-italic">To</span>
						</div>
						<div class="col-sm-3 col-md-3 booking-traslado-title">
							Pasajeros / <span class="text-italic">Passengers</span>
						</div>
					</div>

					<div class="row row-eq-height">
						<div class="col-sm-3 col-md-3 booking-info-detalle booking-info-detalle-left big-font">
							<div>Compartido Premium (Minibús)</div>
						</div>
						<div class="col-sm-3 col-md-3 booking-info-detalle big-font">
							<div>Bristol Hotel</div>
						</div>
						<div class="col-sm-3 col-md-3 booking-info-detalle big-font">
							<div>Asunción, Aeropuerto Int. Silvio Pettirossi</div>
						</div>
						<div class="col-sm-3 col-md-3 booking-info-detalle booking-info-detalle-right">
							<div>2 Adultos / <span class="text-italic">Adults</span></div>
						</div>
					</div>

					<div class="fecha-reserva">
					<p></p>
					</div>

					<div class="row">
						<div class="col-sm-3 col-md-3 booking-traslado-title">
							Fecha del Servicio / <span class="text-italic">Service Date</span>
						</div>
						<div class="col-sm-3 col-md-3 booking-traslado-title">
							Hora de Recogida / <span class="text-italic">Pick-up time</span>
						</div>
						<div class="col-sm-6 col-md-6 booking-traslado-title">
							Punto de Recogida / <span class="text-italic">Punto de recogida</span>
						</div>
					</div>

					<div class="row row-eq-height">
						<div class="col-sm-3 col-md-3 booking-info-detalle booking-info-detalle-left">
							<div>28/05/2017</div>
						</div>
						<div class="col-sm-3 col-md-3 booking-info-detalle">
							<div>--:--</div>
						</div>
						<div class="col-sm-6 col-md-6 booking-info-detalle booking-info-detalle-right">
							<div>Cuando hayas recogido tu equipaje, dirígete a la puerta de salida de los touroperadores y acércate al mostrador de Actividades/Hotelbeds. -Si no puedes localizar al conductor/agente, por favor llama a DESTINATION SERVICES al +809 796 8324; +809 854 0331 .Idiomas de atención teléfonica:inglés, español.</div>
						</div>
					</div>

					<div class="fecha-reserva">
					<p></p>
					</div>

					<div class="row">
						<div class="col-sm-3 col-md-3 booking-traslado-title">
							Datos del Viaje / <span class="text-italic">Travel info</span>
						</div>
						<div class="col-sm-9 col-md-9 booking-traslado-title">
							Servicios Incluidos / <span class="text-italic">Included Services</span>
						</div>
					</div>

					<div class="row row-eq-height">
						<div class="col-sm-3 col-md-3 booking-info-detalle booking-info-detalle-left">
							<div><b>Vuelo de llegada</b> / <span class="text-italic">Arrival Flight</span></div>
							<div>CM110</div>
							<div><b>Hora de llegada</b> / <span class="text-italic">Arrival time</span></div>
							<div>11:30 h.</div>
						</div>
						<div class="col-sm-9 col-md-9 car-descripcion-modal booking-info-detalle booking-info-detalle-right">
							<ul>
								<li>Máximo tiempo de espera del cliente: 60 minutos</li>
								<li>Máximo tiempo de espera del conductor en llegadas doméstico: 60 minutos</li>
								<li>Máximo tiempo de espera del conductor en llegadas internacional: 90 minutos</li>
								<li>En caso de que no localice a ningún miembro de nuestro personal, llame por favor al teléfono de
								emergencias indicado en este bono.</li>
								<li>En caso de que tenga algún problema con su equipaje, llame por favor al teléfono de emergencias
								para informar del retraso y adoptar las medidas oportunas.</li>
								<li>Recuerde que deberá presentar su bono de ida y/o vuelta al representante/guía/conductor junto con
								su DNI o pasaporte. Por favor, no olvide su documentación.</li>
								<li>Por favor, tenga en cuenta que si viaja con niños pequeños es obligatorio el uso de una silla infantil
								homologada. Los padres o tutores del menor serán responsables de proporcionar dicho dispositivo
								de retención. En caso de no hacerlo, no podremos garantizar el servicio.</li>
								<li>1 bolso de mano permitido por persona</li>
								<li>1 maleta permitida por persona (max.dimensiones 158cm) largo+ancho+alto=158cm</li>
							</ul>
						</div>
					</div>


					<div class="fecha-reserva">
					<p></p>
					</div>

					<div class="row">
						<div class="cols-xs-12 col-sm-12 col-md-12 booking-traslado-title">
							Teléfono / <span class="text-italic">Telephone</span>
						</div>
					</div>

					<div class="row row-eq-height">
						<div class="col-sm-12 col-md-12">
							<div class="row">
								<div class="col-sm-6 col-md-6 booking-info-detalle booking-info-detalle-left">
									<div>Asistencia en origen / <span class="text-italic">In Origin telephone support line</span></div>
									<div><b>+529981931333</b></div>
								</div>
								<div class="col-sm-6 col-md-6 booking-info-detalle booking-info-detalle-right">
									<div>Asistencia en destino / <span class="text-italic">In Destination telephone support line</span></div>
									<div><b>+18299548261</b></div>
								</div>
							</div>
						</div>
					</div>

					<div class="fecha-reserva">
					<p></p>
					</div>

					<div class="row">
						<div class="col-sm-9 col-md-9">
							<div>
								<b>Reservado y pagadero por</b> / <span class="text-italic">Booked and payable by</span> HOTELBEDS DOMINICANA S.A. NIF/CIF/Tax ID Number: 1-01-13759-2
							</div>
						</div>
						<div class="col-sm-3 col-md-3 text-center padding-logo">
							<div>
								<img src="{{asset('images/dtpmundo.png')}}" width="200">
							</div>
						</div>
					</div>

					

				</div>
				
			</div>
			
		</div>
                
    </div>

</section>
@endsection

@section('scripts')
    @parent
@endsection
