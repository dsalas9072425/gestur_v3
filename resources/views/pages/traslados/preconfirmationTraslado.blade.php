@extends('master')

@section('title', 'Pre Confirmación Traslados')
@section('styles')
	@parent
@endsection

@section('content')
	
	<div class="container">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-12 car-preconfirmation sectiontop">
                @include('flash::message')         
                <form id="frmConfirmacion" class="contact-form" action="" method="post">
					
					<div class="toggle-container box">

						<div class="panel style1">

							<h4 class="panel-title">

                                <a href="#inforeserva" data-toggle="collapse"><i class="fa fa-info-circle" aria-hidden="true"></i> Información de Reserva</a>

                            </h4>

                            <div class="panel-collapse collapse in" id="inforeserva">

                                <div class="panel-content car-info">

									<div class="row">

										<div class="col-sm-3 col-md-3">
											<figure>
												<img src="images/traslado/minibus.png">
											</figure>
										</div>

										<div class="col-sm-3 col-md-3 car-info-detalle">
											<h4>Compartido Premium (Minibús)</h4>
											<p>2 Adultos</p>
											<p>1 Niño (15 años)</p>
										</div>

										<div class="col-sm-6 col-md-6 car-info-detalle">
											<p><b>De:</b> Bristol Hotel</p>
											<p><b>A:</b> Asunción, Aeropuerto Int. Silvio Pettirossi</p>
										</div>

									</div>

									<div class="row">

										<div class="col-sm-10 col-md-10">

											<p>
												<b>Regreso 07/07/2017, Hora de Salida 21:50, Número de Vuelo BZH415</b>
											</p>

										</div>

										<div class="col-sm-2 col-md-2 text-right">

											<p>
												<b>28,88 US$</b>
											</p>

										</div>

									</div>

									<div class="row">
										<div class="col-sm-12 col-md-12">
											<div class="alert alert-warning">
												 <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> En caso de que los datos introducidos no sean correctos, el proveedor no se hace responsable de la correcta realización del servicio.
											</div>
										</div>
									</div>

									<h3>Entrada en gastos - Regreso</h3>

									<div class="row">
										
										<div class="col-sm-4 col-md-4">
											<div class="alert alert-success">
												<p>Hasta las 23:59 PM el 05/07/2017 - <span class="font-big">Gratis</span></p>
											</div>
										</div>
										<div class="col-sm-4 col-md-4">
											<div class="alert alert-danger">
												<p>Después de las 23:59 PM el 05/07/2017 - <span class="font-big">73.33 US$</span></p>	
											</div>
										</div>
										<div class="col-sm-4 col-md-4 text-right">
											<div class="alert alert-info">
												<p><span class="font-big">Total 28.88 US$</span></p>
											</div>
											</p>
										</div>
									</div>

						        </div>

						    </div>

						</div>

					    <div class="panel style1">

                            <h4 class="panel-title">

                                <a href="#infopasajeros" data-toggle="collapse"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Información de Pasajeros</a>

                            </h4>

                            <div class="panel-collapse collapse in" id="infopasajeros">

                                <div class="panel-content">

                                	<h3>Datos de los Pasajeros</h3>

									<div class="row">

										<div class="col-sm-12 col-md-12">

											<label class="radio-inline"><input type="radio" name="cant-pasajeros" checked="checked">Incluir solo los datos del Pasajero principal</label>
											<label class="radio-inline"><input type="radio" name="cant-pasajeros">Incluir los datos de todos los pasajeros</label>

										</div>

									</div>

									<h4>Pasajero Principal</h4>

									<div class="row">
										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<label>Nombre</label>
												<input type="text" class="form-control" name="nombre">
											</div>
										</div>

										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<label>Apellido</label>
												<input type="text" class="form-control" name="apellido">
											</div>
										</div>
									</div>

									<h4>Datos de Contacto</h4>

									<div class="row">
										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<label>Teléfono móvil</label>
												<div class="form-inline">
													<div class="form-group">
														<select class="form-control">
															<option>Cód. Pais</option>
															<option>Paraguay +595</option>
														</select>
													</div>
													<div class="form-group">
														<input type="number" class="form-control" name="telefono">
													</div>
												</div>
											</div>
										</div>

										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<label>Email</label>
												<input type="text" class="form-control" name="email">
											</div>
										</div>
									</div>

									<h4>Datos de la Reserva</h4>

									<div class="row">
										<div class="col-ms-4 col-md-4">
											<div class="form-group">
												<label>Expediente TO</label>
												<input type="text" class="form-control" name="expediente-to">
											</div>
										</div>
									</div>

						        </div>

							</div>

						</div>

						<div class="panel style1">

                            <h4 class="panel-title">

                                <a href="#infoconfirmar" data-toggle="collapse"><i class="fa fa-check-square-o" aria-hidden="true"></i> Confirmar Reserva</a>

                            </h4>

                            <div class="panel-collapse collapse in" id="infoconfirmar">

                            	<div class="table-responsive">

                            		<table class="table">
                            			<thead>
                            				<tr>
	                            				<th>Servicios</th>
	                            				<th class="text-right">Precio Neto</th>
	                            			</tr>
                            			</thead>
                            			<tbody>
                            				<tr>
                            					<td>Compartido Premium (Minibús)</td>
                            					<td class="text-right">28.88 US$</td>
                            				</tr>
                            				<tr>
                            					<td>Total</td>
                            					<td class="text-right">28.88 US$</td>
                            				</tr>
                            			</tbody>
                            		</table>
                            		
                            	</div>

                                <div class="panel-content">

									<div class="row">	

										<div class="col-sm-12 col-md-12">
											<div class="alert alert-info">
												<h4><i class="glyphicon glyphicon-info-sign"></i> Importante!</h4> <p>Al hacer click en "<b>Reservar</b>" Ud. acepta nuestros <a href="{{route('terminosycondiciones')}}" target="_blank"><b>Términos y Condiciones</b></a></p>
											</div>
				                        </div>
											
									</div>

				                    <div class="form-group row">
				                    	<div class="col-sm-6 col-md-6">
				                            <div class="total-vuelo">TOTAL 28.88 US$</div>
				                        </div>
				                        <div class="col-sm-6 col-md-6 text-right">
				                            <a href="{{route('bookingTraslado')}}" class="btn btn-primary uppercase">Reservar</a>
				                        </div>
				                    </div>

				                </div>

							</div>

						</div>

					</div>
					
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
@endsection
