@extends('master')

@section('title', 'Búsqueda de Traslados')
@section('styles')
	@parent
	<style>
	    .floating-panel {
				        position: absolute;
				        top: 14%;
				        left: 14%;
				        z-index: 5;
				        padding: 5px;
				        text-align: center;
				        font-family: 'Roboto','sans-serif';
				        line-height: 30px;
				        padding-left: 10px;
      					}
    </style>
@endsection

@section('content')

<div class="container">
	<div id="main">
		@include('partials/traslados/modalVuelo')
		@include('partials/traslados/modalDetalles')
		@include('partials/traslados/modalDesdeHasta')
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="nueva-busqueda">
					<div>
						<div class="tab-container">
							<ul class="tabs">
	                            <li class="active"><label data-target="#ida-vuelta-traslado" ><input type="radio" name="radio-tab-traslado" checked="checked">Ida y Vuelta</label></li>
	                            <li><label data-target="#un-trayecto-traslado"><input type="radio" name="radio-tab-traslado" >Sólo 1 Trayecto</label></li>
							</ul>
						</div>
						<div class="tab-content">
                			<div class="tab-pane active in" id="ida-vuelta-traslado">
                    			<form action="" method="" id="frmBusquedaTraslade">
                    				<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                        <div class="form-group">
	                                            <label class="title">Desde</label>
			                                    <div class="input-group form-group">
           									        <select data-placeholder="Origen" class="chosen-select-deselect" id="Aeropuerto" name="aeropuerto">
				                                        <optgroup label="Aeropuerto">
					                                        <option value=""></option>
															<option value="asuncion">Aeropuerto Jorge Newbery - AEP</option>
															<option value="buenos-aires">Aeropuerto Internacional Silvio Pettirossi - ASU</option>
															<option value="brasilia">Aeropuerto Internacional de Maiquetía Simón Bolívar  - CCS</option>
															<option value="sao-paulo">Aeropuerto Internacional de Dubái - DXB</option>
														</optgroup>    
														<optgroup label="Hotel">    
															<option value="milan">Aeropuerto Internacional Ministro Pistarini - EZE</option>
															<option value="roma">Aeropuerto de Roma - Fiumicino - FCO</option>
															<option value="miami">Aeropuerto de Hamburgo - HAM</option>
															<option value="cancun">Aeropuerto Internacional John F. Kennedy - JFK</option>
															<option value="montevideo">Aeropuerto Adolfo Suárez Madrid - Barajas - MAD</option>
															<option value="londres">Aeropuero de Estrasburgo - SXB</option>
														</optgroup>    
													</select>
								                    <div class="input-group-btn">
									                    <a  id="modalCoordenadasDesde" onclick= "recrearMapaDesde();" href="#" class="btn btn-default"><i class="glyphicon fa fa-map-marker"></i></a>
									                    <input type="hidden" class ="input-text coordenadaDesde"/>
														<input type="hidden" class ="input-text direccionDesde"/>
									                </div>
									            </div>	
	                                        </div>
	                                    </div>
	                                    <div class="col-xs-12 col-sm-6 col-md-6">
	                                        <div class="form-group">
	                                            <label class="title">Hasta</label>
				                                <div class="input-group form-group">
			                                        <select data-placeholder="Destino" class="chosen-select-deselect" id="destino" name="destino">
			                                            <optgroup label="Aeropuerto">   
				                                            <option value=""></option>
															<option value="asuncion">Asunción</option>
															<option value="buenos-aires">Buenos Aires</option>
															<option value="brasilia">Brasilia</option>
															<option value="sao-paulo">Sao Paulo</option>
														</optgroup>    
			                                            <optgroup class="" label="Hotel">   
															<option value="milan">Milán</option>
															<option value="roma">Roma</option>
															<option value="miami">Miami</option>
															<option value="cancun">Cancún</option>
															<option value="montevideo">Montevideo</option>
															<option value="londres">Londres</option>
														</optgroup>    
													</select>
								                    <div class="input-group-btn">
									                    <a id="modalCoordenadasHasta"  onclick= "recrearMapaHasta();" href="#" class="btn btn-default"><i class="glyphicon fa fa-map-marker"></i></a>
									                    <input type="hidden" class ="input-text coordenadaHasta"/>
														<input type="hidden" class ="input-text direccionHasta"/>
									                </div>
									            </div>	                                        
									        </div>
	                                    </div>
	                                    <div class="col-xs-5 col-sm-3 col-md-3">
	                                        <div class="form-group">
	                                            <label class="title">FECHA IDA</label>
	                                            <div class="datepicker-wrap">
	                                                <input type="text" id="fecha_ida" name="fecha_ida" class="input-text full-width required" value = ""/>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="col-xs-5 col-sm-3 col-md-3">
	                                        <div class="form-group fecha-vuelta">
	                                            <label class="title">FECHA VUELTA</label>
	                                            <div class="datepicker-wrap">
	                                                <input type="text" id="fecha_vuelta" name="fecha_vuelta" class="input-text full-width required" value = ""/>
	                                            </div>
	                                        </div>
	                                    </div>
	     
	                                    <div class="col-xs-6 col-sm-2 col-md-2">

	                                    	<label class="title cantPasajeros">Adultos</label>            

	                                        <div class="input-group form-group">

	                                           	<div class="input-group-btn">

			                    					<button type="button" id="disminuir" class="btn btn-default"><i class="glyphicon glyphicon-minus-sign"></i></button>

			                    				</div>

			                    				<input name="cantidad_adultos" id="cantidad_adultos" class="form-control" value="2" readonly="readonly" />

			                    				<div class="input-group-btn">

				                        			<button type="button" id="aumentar" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i></button>

				                        		</div>

				                        	</div>

				                        </div>

				                        <div class="col-xs-6 col-sm-2 col-md-2">

				                        	<label class="title cantPasajeros">Niños (1/17 AÑOS)</label>

				                        	<div class="input-group form-group">

				                        		<div class="input-group-btn">

			                    					<button type="button" id="disminuir-n" class="btn btn-default"><i class="glyphicon glyphicon-minus-sign"></i></button>

			                    				</div>
			                    				<input name="cantidad_ninhos" id="cantidad_ninhos" class="form-control" value="0" readonly="readonly"  />
			                    				<div class="input-group-btn">
				                        			<button type="button" id="aumentar-n" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i></button>
				                        		</div>
				                        	</div>
				                        </div>
	                                                
	                                    <div class="col-xs-6 col-sm-2 col-md-2">
	                                        <label class="title">&nbsp;</label>
	                                        <!-- <button type="submit" id="btnBuscar" class="btn btn-primary">Buscar</button> -->
											<a href="{{route('searchTraslado')}}" class="btn btn-primary">Buscar</a>
	                                    </div>
	                                </div>
                                </form>
                    		</div>
                    		<div class="tab-pane fade" id="un-trayecto-traslado">
                    		    <form action="" method="" id="frmBusquedaTraslade">
                    				<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
	                                        <div class="form-group">
	                                            <label class="title">Desde</label>
			                                    <div class="input-group form-group">
           									        <select data-placeholder="Origen" class="chosen-select-deselect" id="Aeropuerto" name="aeropuerto">
				                                        <optgroup label="Aeropuerto">
					                                        <option value=""></option>
															<option value="asuncion">Aeropuerto Jorge Newbery - AEP</option>
															<option value="buenos-aires">Aeropuerto Internacional Silvio Pettirossi - ASU</option>
															<option value="brasilia">Aeropuerto Internacional de Maiquetía Simón Bolívar  - CCS</option>
															<option value="sao-paulo">Aeropuerto Internacional de Dubái - DXB</option>
														</optgroup>    
														<optgroup label="Hotel">    
															<option value="milan">Aeropuerto Internacional Ministro Pistarini - EZE</option>
															<option value="roma">Aeropuerto de Roma - Fiumicino - FCO</option>
															<option value="miami">Aeropuerto de Hamburgo - HAM</option>
															<option value="cancun">Aeropuerto Internacional John F. Kennedy - JFK</option>
															<option value="montevideo">Aeropuerto Adolfo Suárez Madrid - Barajas - MAD</option>
															<option value="londres">Aeropuero de Estrasburgo - SXB</option>
														</optgroup>    
													</select>
								                    <div class="input-group-btn">
									                    <a  id="modalCoordenadasDesde" onclick= "recrearMapaDesde();" href="#" class="btn btn-default"><i class="glyphicon fa fa-map-marker"></i></a>
									                    <input type="hidden" class ="input-text coordenadaDesde"/>
														<input type="hidden" class ="input-text direccionDesde"/>
									                </div>
									            </div>	
	                                        </div>
	                                    </div>
	                                                    
	                                    <div class="col-xs-12 col-sm-6 col-md-6">
	                                        <div class="form-group">
	                                            <label class="title">Hasta</label>
				                                <div class="input-group form-group">
			                                        <select data-placeholder="Destino" class="chosen-select-deselect" id="destino" name="destino">
			                                            <optgroup label="Aeropuerto">   
				                                            <option value=""></option>
															<option value="asuncion">Asunción</option>
															<option value="buenos-aires">Buenos Aires</option>
															<option value="brasilia">Brasilia</option>
															<option value="sao-paulo">Sao Paulo</option>
														</optgroup>    
			                                            <optgroup class="" label="Hotel">   
															<option value="milan">Milán</option>
															<option value="roma">Roma</option>
															<option value="miami">Miami</option>
															<option value="cancun">Cancún</option>
															<option value="montevideo">Montevideo</option>
															<option value="londres">Londres</option>
														</optgroup>    
													</select>
								                    <div class="input-group-btn">
									                    <a id="modalCoordenadasHasta"  onclick= "recrearMapaHasta();" href="#" class="btn btn-default"><i class="glyphicon fa fa-map-marker"></i></a>
									                    <input type="hidden" class ="input-text coordenadaHasta"/>
														<input type="hidden" class ="input-text direccionHasta"/>
									                </div>
									            </div>	                                        
	                                        </div>
	                                    </div>
	                                    <div class="col-xs-5 col-sm-3 col-md-3">
	                                        <div class="form-group">
	                                            <label class="title">FECHA IDA</label>
	                                            <div class="datepicker-wrap">
	                                                <input type="text" id="fecha_ida" name="fecha_ida" class="input-text full-width required" value = ""/>
	                                            </div>
	                                        </div>
	                                    </div>

	                                    <div class="col-xs-6 col-sm-3 col-md-3">

	                                    	<label class="title cantPasajeros">Adultos</label>            

	                                        <div class="input-group form-group">

	                                           	<div class="input-group-btn">

			                    					<button type="button" id="disminuir" class="btn btn-default"><i class="glyphicon glyphicon-minus-sign"></i></button>

			                    				</div>

			                    				<input name="cantidad_adultos" id="cantidad_adultos" class="form-control" value="2" readonly="readonly" />

			                    				<div class="input-group-btn">

				                        			<button type="button" id="aumentar" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i></button>

				                        		</div>

				                        	</div>

				                        </div>

				                        <div class="col-xs-6 col-sm-3 col-md-3">

				                        	<label class="title cantPasajeros">Niños (1/17 AÑOS)</label>

				                        	<div class="input-group form-group">

				                        		<div class="input-group-btn">

			                    					<button type="button" id="disminuir-n" class="btn btn-default"><i class="glyphicon glyphicon-minus-sign"></i></button>

			                    				</div>
			                    				<input name="cantidad_ninhos" id="cantidad_ninhos" class="form-control" value="0" readonly="readonly"  />
			                    				<div class="input-group-btn">
				                        			<button type="button" id="aumentar-n" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i></button>
				                        		</div>
				                        	</div>
				                        </div>
	                                    <div class="col-xs-6 col-sm-3 col-md-3">
	                                        <label class="title">&nbsp;</label>
	                                        <!-- <button type="submit" id="btnBuscar" class="btn btn-primary">Buscar</button> -->
											<a href="{{route('searchTraslado')}}" class="btn btn-primary">Buscar</a>
	                                    </div>
	                                </div>
                                </form>
                    		</div>
                		</div>
                	</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-12 traslado-resultados">
				@include('partials/traslados/singleResult')
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
    @parent
	<script type="text/javascript" src="{{ URL::asset('js/search/filtro-buscador-vuelo.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/search/scriptBuscador.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/search/fechasSearch.js') }}"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCu2ZW8F0SdXXa2bLgnIu-vIInMdg_IMQw&libraries=places&callback=initialize" type="text/javascript"></script>
	<script type="text/javascript" src="{{ URL::asset('js/searchTraslado.js') }}"></script>

@endsection
