@extends('master')
@section('title', 'Búsqueda de Vuelos')
@section('styles')
	@parent
    <style>
        #imagen{ position:absolute; z-index:1; } 
        #vueloFrame{ position:absolute; z-index:0; } 
    </style>
@endsection

@section('content')
<section class="sectiontop">
	<div class="container" style="height:780"> 
        <div id="imagen" style="text-align:Left;height: 15px;">
            <img src="{{asset('images/blanco.png')}}" width="220">
        </div>                                                
        <iframe id= "vueloFr" frameborder="0" height="780" src="https://www-amer.epower.amadeus.com/dtp/" width="100%"></iframe>
        <div class="row">
            <div class="col-md-11">
            </div>
            <div class="col-md-1">
                <a href="javascript:history.back(1);">Volver Atrás</a>
            </div>     
        </div>
	</div>
</section>	
@endsection

@section('scripts')
	@include('layouts/scripts')
    @parent
    <script type="text/javascript">

    </script>
@endsection
