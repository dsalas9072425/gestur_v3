@extends('master')

@section('title', 'Detalle de Reserva')
@section('styles')
	@parent
	<style type="text/css">
            #container-main h1{
                font-size: 40px;
                text-shadow:4px 4px 5px #16a085;
            }

            .accordion-container {
                width: 100%;
                margin: 0 0 20px;
                clear:both;
            }

            .accordion-titulo {
                position: relative;
                display: block;
                padding: 20px;
                font-size: 24px;
                font-weight: 300;
                background: #fff;
                color: #2c3e50;
                text-decoration: none;
                text-align: center;
            }
            .accordion-titulo.open {
                background: #fff;
                color: #2c3e50;
                text-align: center;
            }
            .accordion-titulo:hover {
                background: #fff;
                color: #2c3e50;
                text-align: center;
            }

            .accordion-titulo span.toggle-icon:before {
                content:"+";
            }

            .accordion-titulo.open span.toggle-icon:before {
                content:"-";
            }

            .accordion-titulo span.toggle-icon {
                position: absolute;
                top: 10px;
                right: 20px;
                font-size: 38px;
                font-weight:bold;
            }
            .tooltip{
                  display: inline;
                  position: relative;
              }
              
            .tooltip:hover:after{
                  background: #333;
                  background: rgba(0,0,0,.8);
                  border-radius: 5px;
                  bottom: 26px;
                  color: #fff;
                  content: attr(title);
                  left: 20%;
                  padding: 5px 15px;
                  position: absolute;
                  z-index: 98;
                  width: 220px;
              }
              
            .tooltip:hover:before{
                  border: solid;
                  border-color: #333 transparent;
                  border-width: 6px 6px 0 6px;
                  bottom: 20px;
                  content: "";
                  left: 50%;
                  position: absolute;
                  z-index: 99;
              }
 
    </style>
@endsection
@section('content')
<section class="sectiontop">
	<div class="container">
    <div class="accordion-container">
        <a href="#" title="Las tarifa, acuerdos comerciales se encuentran disponibles a la hora de cotizar por DTPMundo" class="accordion-titulo">FORMAS DE CANJEAR&nbsp;&nbsp;<span class="toggle-icon"></span></a>
        <div class="accordion-content">
            <div class="row bm-portfolio onepixel portfolio-filter isotope" style="position: relative; overflow: hidden; height: 2560px;">
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                  <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <!--<a href="{{route('detalleCobro', ['id' =>'copetrol'])}}">--><img class="img-responsive" src="{{asset('images/promociones/copetrol.png')}}" id = "copetrol"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="Copetrol" alt="Copetrol"><!--</a>-->
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <!--<a href="{{route('detalleCobro', ['id' =>'copetrol'])}}">--><img class="img-responsive" src="{{asset('images/promociones/stock.png')}}" id = "stock"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="Stock" alt="Stock"><!--</a>-->
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <!--<a href="{{route('detalleCobro', ['id' =>'copetrol'])}}">--><img class="img-responsive" src="{{asset('images/promociones/seis.png')}}" id = "seis"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="SuperSeis" alt="SuperSeis"><!--</a>-->
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <!--<a href="{{route('detalleCobro', ['id' =>'copetrol'])}}">--><img class="img-responsive" src="{{asset('images/promociones/ggcia.png')}}" id = "ggcia"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="Gonzalez Gimenez y Cia" alt="Gonzalez Gimenez y Cia"><!--</a>-->
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <!--<a href="{{route('detalleCobro', ['id' =>'copetrol'])}}">--><img class="img-responsive" src="{{asset('images/promociones/bolsa.png')}}" id = "bolsa"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="Efectivo" alt="Efectivo"><!--</a>-->
                    </div>
                </div>

            </div>        
        </div>  
    </div>
</div>

</section>		

@endsection
@section('scripts')
    @parent
	<script type="text/javascript" src="{{ URL::asset('js/api.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/countto.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/hoverIntent.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/analytics.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/easing.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/fitvids.js')}}"></script> 
    <script type="text/javascript" src="{{ URL::asset('js/functions.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/superfish.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/mmenu.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/magnificpopup.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/isotope.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/jquery.themepunch.revolution.min.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/jquery.themepunch.plugins.min.js')}}"></script> 
    <script type="text/javascript">
        $(".accordion-titulo").click(function(){
           var contenido=$(this).next(".accordion-content");
                    
           if(contenido.css("display")=="none"){ //open     
              contenido.slideDown(250);         
              $(this).addClass("open");
           }
           else{ //close        
              contenido.slideUp(250);
              $(this).removeClass("open");  
          }
                                    
        });

         $(".img-responsive").click(function(){
            var id = $(this).attr('id');
             BootstrapDialog.confirm({
                    title: '<b>DTP</b>',
                    message: 'Seguro desea canjear su incentivo?',
                    type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    btnCancelLabel: 'No', // <-- Default value is 'Cancel',
                    btnOKLabel: 'Si, Canjear', // <-- Default value is 'OK',
                    btnOKClass: 'btn-error', // <-- If you didn't specify it, dialog type will be used,
                    callback: function(result) {
                        // result will be true if button was click, while it will be false if users close the dialog directly.
                        if(result) 
                        {
                          window.location.replace("detalleCobro/"+id);     
                        }   
                    }
                }); 





          }) 


    </script>
@endsection
