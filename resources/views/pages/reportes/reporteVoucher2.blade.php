<!DOCTYPE html>
<html>
<head>
	<title>Voucher Nuevo modelo</title>

	<style type="text/css">

	* {
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			 margin: 0;
			 padding: 0;
		}

	.container{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			/*width: 595.28px;*/
			width: 90%;
			margin:0 auto;
	}
	
	table{
		width: 100%;
		border-collapse: collapse;
	}

	.center-text{
		text-align: center;
	}

	.m-0{
		margin:0;
	}

	.cabecera{
		background: #f3f5f5;
	}

	.cabeceraDetalle{

	}
	.localizador{
		background: #b52555;
	}
	.text-bold{
		font-weight: bold;
	}
	.text-italic{
		font-style: italic;
	}
	
	.flexbox-container{
		display: -ms-flex;
		display: -webkit-flex;
		display: flex;
	}

	.flexbox-container > div{
		width: 50%;
		padding: 10px;
	}

	.flexbox-container > div:first-child{
		margin-right: 20px;
	}
	.border-bottom-red-td td{
		border-bottom: 1px solid red;
	}
	.border-section{
		border-left: 1px solid #c4cecb;
		border-right: 1px solid #c4cecb;

	}

	.border-comentarios{
	border-left: 1px solid #c4cecb;
	border-right: 1px solid #c4cecb;
	border-bottom: 1px solid #c4cecb;
	}

	.footer{
		border:1px solid #c4cecb;
	}
	.text-cabecera-info{
		font-size: 13px;
	}

	.padding-td td{
		padding: 10px;
	}

	.text-footer{
		font-size: 12px;
	}
		
	</style>
	
	
	
	
	
	
	
	
	
	
	


</head>
<body>






		@php
				$totalProformaDetalle = count($proformasDetalle);
				$contadorRecorrido = 0;
				
				$imagenAgencia = '';
				$pais='';
			    $ciud='';
			    $contadorVaucher = 0;

			
		@endphp
		
	


@foreach($voucherArray as $vou)


	@php
		$imagenEmpresa = 'logoEmpresa/'.$vou->logoEmpresa;
		$imagenAgencia = 'pesonasLogo/'.$vou->logoAgencia;
	@endphp





	{{-- ============================================
				INICIO DE VOUCHER 
		=============================================--}}


		
	<div class="container">
		<table>
			<tr>
				<td style="width: 30%;">
					<img src="{{asset($imagenAgencia)}}" width="100" height="100">
				</td>
				<td style="width: 70%; height: 140px;"></td>
			</tr>
		</table>


		<table style="margin-bottom: 10px;">
			<tr>
				<td class="center-text cabecera" style="height: 50px;">
					<p class="m-0" style="font-size:18px; ">
						<?php echo $vou->titulo; ?>
					</p>
			<p class="m-0" style="font-size: 11px;">
			<b>Reserva Confirmada y Garantizada - Bono - Hotel</b> /<i> Booking confirmed and guaranteed - Voucher - Hotel</i></p>
				</td>
			</tr>
		</table>

		<table style="background-color:#e5f7f4; ">
			<tr>
				<td class="localizador" style="width: 30%; height: 150px; text-align: center; color:white;">
					<p style="font-size: 12px;"><b>Localizador</b> / Reference number :</p>
							<p style="font-size: 25px; margin-top: 10px;"><b>{{$vou->codigoConfirmacion}}</b></p>
							<p style="font-weight:bold;font-size: 12px; margin-top: 10px;">Válido para el hotel</p>
				</td>


				<td  style="width: 70%; padding: 5px;" valign="top">
				
				<div style="font-weight: bold; font-size: 20px; margin:10px 0 0 0; display:inline-block;"></div>

						<br>
						<br>
				
					<table>
					<tr>
						<td style="width: 50%;">	
						<span style="" class="text-bold text-cabecera-info">Nombre de pasajero</span> / <span class="text-cabecera-info" style="font-style: italic; font-weight: 400;">Passenger name : </span>
						</td>

						<td style="text-align: left;">
						<span class="text-cabecera-info" style="font-size: 11px;"> <?php echo $vou->nombre; ?> </span>
						</td>
							
					</tr>
					
				</table>
				<div style="margin-top: 8px;"></div>
				
					<span class="text-bold text-cabecera-info">Fecha confirmación reserva</span> 
					<span class="text-cabecera-info text-italic">/ Booking date: </span>
					<span class="text-cabecera-info">@php echo date('Y-m-d');@endphp</span><br>
					<br>
					<span class="text-bold text-cabecera-info">Proforma TO</span>  
					<span class="text-cabecera-info text-italic">/ Proforma TO :</span> 
					<span class="text-cabecera-info"> <?php echo $vou->id_proforma;?></span>

					
				</td>
			</tr>
		</table>

		<table class="border-section">
			<tr class="border-bottom-red-td">
				<td colspan="6" style="font-size: 13px; padding: 10px;">
					<span class="text-bold">Desde</span>
					<span class="text-italic"> / From :</span>&nbsp; 20/03/2019 &nbsp;
					<span class="text-bold">Hasta</span>
					<span class="text-italic"> / To :</span>&nbsp; 28/03/2019
				</td>
			</tr>

			<tr class="padding-td border-bottom-red-td" style="font-size: 12px;">
				<td>
					<span class="text-bold">Unidades</span><br>
					<span class="text-italic">Units</span>
				</td>
				<td>
					<span class="text-bold">Tipo de Servicio</span><br>
					<span class="text-italic">Service Type</span>
				</td>
				<td>
					<span class="text-bold">Adultos</span><br>
					<span class="text-italic">Adults</span>
				</td>
				<td>
					<span class="text-bold">Niños</span><br>
					<span class="text-italic">Children</span>
				</td>
				<td>
					<span class="text-bold">Noches</span><br>
					<span class="text-italic">Nights</span>
				</td>
			
			</tr>
			
			
			 <?php echo $vou->servicio; ?>
		
			
				

		</table>

		<table class="border-comentarios padding-td" >
			<tr>
				<td>
					<b>Obervaciones</b>
					<i>/ Remarks</i>
				</td>
			</tr>
			<tr>
				<td style="font-size: 13px;height: 200px;" valign="top">
					{{-- COMENTARIOS --}}
					<?php echo $vou->comentario; ?>
				
				</td>
			</tr>
		</table>

		<table class="footer padding-td" style="margin-top: 10px;">
			<tr>
				<td>
					<div style="font-weight: bold; font-size: 20px; display:inline-block;">{{$vou->nombreProveedor}}</div>
			
					<br>

					<span class="text-footer"><b>{{$vou->direccionProveedor}} - {{$vou->ciudadProveedor}} ,  {{$vou->paisProveedor}} </b></span><br>
					<span class="text-footer"><b>Teléfono</b> / <span class="text-italic">Telephone :</span> {{$vou->telefonoProveedor}}   <b>Email</b> / <span class="text-italic">Email :</span> {{$vou->correoProveedor}}</span><br>
					<br>
					<div style="margin-top: 5px;"></div>
					

				</td>
			</tr>
		</table>

		<table class="padding-td" style="margin-top: 15px; font-size: 12px;">
			<tr>
				<td style="text-align: center;">
					<span><b>Reservado y pagadero por</b></span> <i>/  Booked and payable by</i> <?php echo $vou->nombrePrestador; ?>
				</td>
			</tr>
		</table>

		<table >
			<tr>
				<td style="width:75%; ">
				
				</td>
				<td>
					<table style="text-align: center;">
						<tr>
							<td>
								<img src="{{ asset($imagenEmpresa) }}" width="50">
							</td>
						</tr>
						<tr>
							<td>
								<div style="font-size: 12px; background-color:#e5f7f4; border-radius: 2px;padding: 5px; ">
									
								<p>Bono generado el <b> <?php echo date('Y-m-d'); ?> </span></b></p>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
 



	</div>
	<!--container-->



@endforeach


			<div style="page-break-after:always;"></div>';
	





</body>
</html>