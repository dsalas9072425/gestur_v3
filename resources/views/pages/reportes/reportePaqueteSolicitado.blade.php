@extends('master')
@section('title', 'Paquetes Categorias')
@section('styles')
    @parent
@endsection
@section('content')

<section class="sectiontop">
    <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
        <form id="frmAgencias" class="contact-form" action="" method="post">
            @include('flash::message')
            <div class="booking-information travelo-box" style= "background-color: #ffffff">
                <div>
                    <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Paquetes Solicitados</h1>
                    <br>
                     <br>
                  <div style= "overflow-x: scroll; overflow-y: hidden; background-color: #ffffff">
                    <br>
                    <br>
                    <table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
                        <thead>
                            <tr>
                                <th>Fecha <br>Solicitud</th>
                                <th>Usuario</th>
                                <th>Paquete</th>
                                <th>Precio</th>
                                <th>Adultos</th>
                                <th>Niños</th>
                                <th>Infante</th>
                                <th>Proforma</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($paquetes as $paquete)
                                <tr>
                                    <td>{{$paquete['fecha']}}</td>
                                    <td>{{$paquete['user']['nombre_apellido']}}({{$paquetes[0]['user']['usuario']}})</td>
                                    <td><b>{{$paquete['paquete']['titulo']}}</b></td>
                                    <td><b>{{$paquete['paquete']['precio']}}</b></td>
                                    <td>{{$paquete['ocupancia_adultos']}}</td>
                                    <td>{{$paquete['ocupancia_ninhos']}}</td>
                                    <td>{{$paquete['ocupancia_infante']}}</td>
                                    <td><div id="proforma{{$paquete['id']}}">
                                            @if($paquete['proforma_id'] != 0)
                                                <b>{{$paquete['proforma_id']}}</b>  
                                            @endif
                                        </div></td>
                                    <td>
                                    @if($paquete['proforma_id'] == 0)
                                            <div class="selector">
                                                <select class="full-width" id="{{$paquete['id']}}" name="agente_dtp">
                                                    @foreach($estados as $estado)
                                                        @if($estado['value'] == $paquete['estado'])
                                                            <option value="{{$estado['value']}}" selected="selected">{{$estado['detalle']}}</option> 
                                                        @else
                                                            <option value="{{$estado['value']}}">{{$estado['detalle']}}</option> 
                                                        @endif  
                                                    @endforeach    
                                                </select>
                                            </div>
                                    @endif        
                                    </td>
                                </tr>
                            @endforeach     
                        </tbody>
                    </table>
                </div>   
               </div>
            </div> 
        </form>             
    </div>

    <div id="requestModal" class="modal fade" role="dialog">
        <form id="frmPaquete" method="post" action="" style="margin-top: 20%;">
            <div class="modal-dialog">
        <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title titlepage" style="font-size: x-large;">Ingrese N° Proforma</h2>
                    </div>
                    <div class="modal-body">
                        <div id="contenido">
                            <div class="row">
                                <div class="col-md-12" style="padding-left: 25px;padding-right: 25px;">
                                    <div class="form-group">
                                        <input type="hidden" required class = "Requerido form-control" name="paquete_id" id="paquete_id" value=0>
                                        <input type="number" required class = "Requerido form-control" name="proforma_id" id="proforma_id" value=0>
                                    </div>
                                </div>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" id="btnAceptarProforma" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Aceptar</button>
                    <button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
                  </div>
            </div>
        </div>
     </form>
    </div>

</section>  
@endsection
@section('scripts')
    @include('layouts/scripts')
    @parent
        <script type="text/javascript">

             $('#listado').dataTable({
                                        "order": [[0, "desc" ]]
                                    });

            $(".full-width").change(function(){
                if($(this).val() == "S"){
                    $('#requestModal').modal({
                        show: 'true'
                    }); 
                    $('#paquete_id').val($(this).attr('id'));
                }

                $.ajax({
                        type: "GET",
                        url: "{{route('getPaqueteSolicitadoEstado')}}",
                        dataType: 'json',
                        data: {
                            estado: $(this).val(),
                            paquete_id: $(this).attr('id')    
                        },
                        success: function(rsp){

                            }
                    })
            }) 

            $("#btnAceptarProforma").click(function(){
                var dataString = $("#frmPaquete").serialize();
                $.ajax({
                        type: "GET",
                        url: "{{route('getPaqueteSolicitado')}}",
                        dataType: 'json',
                        data: dataString,
                        success: function(rsp){
                                $("#requestModal").modal('hide');
                                $(".modal-backdrop").remove();
                                if(rsp.estado == "OK"){
                                    $("#proforma"+rsp.paquete).html('<b>'+rsp.proforma+'</b>')
                                    $("#mensaje").append('<div class="alert alert-success" style="margin-left: 15%;"><strong>'+rsp.descripcion+'</strong><div>');                           
                                }else{
                                    $("#mensaje").append('<div class="alert alert-error" style="margin-left: 15%;"><strong>'+rsp.descripcion+'</strong><div>');                           
                                }   
                            }
                    })
            })



        </script>
@endsection
