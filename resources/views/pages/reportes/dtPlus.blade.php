@extends('master')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
	<style>
		/* Círculos de colores numerados */
		span.green {
			background: #5EA226;
		   /* -webkit-border-radius: 98em;*/
		    color: #ffffff;
		    display: inline-block;
		    font-weight: bold;
		    line-height: 1.6em;
		    margin-right: 0px;
		    text-align: center;
		    font-size: 60px;
		    width: 90%;
		    height: 150px;		    
		    cursor: pointer;
		    padding-top: 30px;
		}

		span.blue {
		    background: #5178D0;
		    /*-webkit-border-radius: 98em;*/
		    color: #ffffff;
		    display: inline-block;
		    font-weight: bold;
		    line-height: 1.6em;
		    margin-right: 0px;
		    text-align: center;
		    font-size: 60px;
		    width: 90%;
		    height: 150px;		    
		    cursor: pointer;
		    padding-top: 30px;
		}
	</style>
@endsection
@section('content')
<section id="content" class="gray-area">
	<div id="main">
	  <div class="container">
		 <div class="block sectiontop">	
			<ul class="nav nav-tabs">
				<li class="active" id="homeLi"><a data-toggle="tab" href="#home">¿Como estoy?</a></li>
				<li id="menu1Li" ><a data-toggle="tab" href="#menu1">Detalle</a></li>
			</ul>
			<div class="tab-content">
				<div id="home" class="tab-pane fade in active">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12" style="padding:5px;">
								<div style="height: 50%;border: 1px solid #999999;border-radius: 5px;margin-right: 15px;padding-left: 5px;padding-right: 5px;background-color: white;margin-left: 15px;">
									<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 3%;margin-bottom: 1%;font-weight: bold; color: #072235">RESUMEN DE DTPlus</h1>
									<div class="table-responsive">
										<div class="row" style="margin-top: 3%; margin-bottom: 6%">
											<div class="col-md-2">
											</div>
											<div class="col-md-4">
												<span class="green"></span><br>
												<span style="text-align=center"><b>&nbsp;&nbsp;&nbsp;Incentivo a canjear por Facturas cobradas</b></span>
											</div>  
											<div class="col-md-1">
											</div>
											<div class="col-md-4">
												<span class="blue"></span><br>
												<span style="text-align:center"><b>&nbsp;&nbsp;Incentivos pendientes de cobro por Facturas<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a pagar</b></span>
											</div> 
											<div class="col-md-1">
											</div>  
										</div>	
									</div>		
								</div>
							</div>
						</div>
						<div id="aCobrar" class="row" style="display: none; height: 50%;border: 1px solid #999999;border-radius: 5px;margin-right: 10px;padding-left: 5px;padding-right: 5px;background-color: white;margin-left: 10px;">
							<div style="margin-left: 15px;margin-right: 15px;">
								<div class="row">
									<div class="col-md-10">
										<h5 class="subtitle hide-medium" style="text-align: left; font-size: xx-large; margin-top: 3%;margin-bottom: 3%; margin-left: 30px;">Detalles de Facturas a Cobrar</h5>
									</div>
									<div class="col-md-2">
										<br/>
										<a id="anchorMapa" href="{{route('tipoCambioDtPlus')}}" style="border-bottom-width: 0px;margin-top: 2px;margin-bottom: : 10px;margin-bottom: 20px;width: 100%;height: 40px;padding-top: 5px;font-size: 18px;" class="btn function small show-full-map map-animation animate">CANJEAR</a>
										<br/>
									</div>
								</div>
								<table id="reservasACobrar" class="table table-bordered table-hover">
									<thead>
										<tr class='bgblue'>
											<th style="background-color: #072235">Fecha</th>
											<th style="background-color: #072235">N° Factura</th>
											<th style="background-color: #072235">Nombre Titular</th>
											<th style="background-color: #072235">DTPlus</th>
											<th style="background-color: #072235">Beneficio <br>Promo</th>
											<th style="background-color: #072235">Total Beneficio <br>Acumulado</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5" style="border-top: 1px solid #111;"><span class="price">TOTAL A CANJEAR POR PROMO LLEGANDO A LA META</span></td>
											<td style=" border-top: 1px solid #111;"><span id ="totalIncentivoACobrar" class="price" style="float: left; text-align: left; color:#e2076a;"></span></td>
										</tr>
									</tfoot>
								</table>
							</div>	
						</div>
						<div id="pendiente" class="row" style="display: none; height: 50%;border: 1px solid #999999;border-radius: 5px;margin-right: 10px;padding-left: 5px;padding-right: 5px;background-color: white;margin-left: 10px;">
							<div style="margin-left: 15px;margin-right: 15px;">
								<h5 class="subtitle hide-medium" style="text-align: left; font-size: xx-large; margin-top: 3%;margin-bottom: 3%; margin-left: 30px;">Detalles de Facturas Pendientes</h5>
								<table id="reservasPendiente" class="table table-bordered table-hover">
									<thead>
										<tr class='bgblue'>
											<th style="background-color: #072235">Fecha</th>
											<th style="background-color: #072235">N° Factura</th>
											<th style="background-color: #072235">Nombre Titular</th>
											<th style="background-color: #072235">DTPlus</th>
											<th style="background-color: #072235">Beneficio <br>Promo</th>
											<th style="background-color: #072235">Total Beneficio <br>Acumulado</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5" style="border-top: 1px solid #111;"><span class="price">TOTAL A CANJEAR POR PROMO LLEGANDO A LA META</span></td>
											<td style=" border-top: 1px solid #111;"><span id ="totalIncentivoPendiente" class="price" style="float: left; text-align: left; color:#e2076a;"></span></td>
										</tr>
									</tfoot>
								</table>
							</div>	
						</div>
						<br/>
				</div>
				<div id="menu1" class="tab-pane fade">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12" style="padding:5px;">
							<div style="height: 50%;border: 1px solid #999999;border-radius: 5px;margin-right: 15px;padding-left: 5px;padding-right: 5px;background-color: white;margin-left: 15px;">
								<h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 3%;margin-bottom: 1%;font-weight: bold; color: #072235">VOUCHER</h1>
								<div style= "background-color: #ffffff;padding-top: 20px;padding-left: 10px;padding-right: 10px;">
									<div class="col-xs-12 col-sm-12 col-md-12">
							            <table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
								            <thead>
								                <tr>
								                    <th style="background-color: #072235;color: white;">Fecha</th>
								                    <th style="background-color: #072235;color: white;">Opcion</th>
								                    <th style="background-color: #072235;color: white;">Lista Facturas</th>
								                    <th style="background-color: #072235;color: white;">Estado</th>
											        <th style="background-color: #072235;color: white;">Monto</th>
											        <th style="background-color: #072235;color: white;"></th>
								                </tr>
								            </thead>
								            <tbody>
								            </tbody>
							            </table>
							        </div>	
						            <div class="row">
						                <div class="col-md-10"><h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 1%;margin-bottom: 1%;font-weight: bold; color: #072235">Monto Cobrado</h1></div>
						                <div class="col-md-2">
							                <div id="totalCanjeado"></div>
							            </div>	
							        </div>	
							        <div class="row">	
							            <div class="col-md-10"><h1 class="subtitle hide-medium" style="font-size: x-large;text-align: center;margin-top: 1%;margin-bottom: 1%;font-weight: bold; color: #072235">Monto Pendiente</h1></div>
						                <div class="col-md-2">
							                <div id="totalPendiente"></div>
							            </div>
							            <hr>	
						            </div>
						            <br>
						        </div>	 
							</div>		
						</div>
					</div>	
				  </div>	
				</div>		
			</div>	
		</div>
	</div>		
</section>
@endsection
@section('scripts')
	@include('layouts/scripts')
	<script type="text/javascript">
		$('.green').click(function(){
			$('#aCobrar').show();
			$('#pendiente').css("display", "none");
		})

		$('.blue').click(function(){
			$('#pendiente').show();
			$('#aCobrar').css("display", "none");
		})

		$.ajax({
				type: "GET",
				url: "{{route('getdtPlus')}}",
				dataType: 'json',
				data: {
						dataTipo: "C"
				},
				success: function(rsp){
							console.log(rsp);
							if(rsp.estado == "OK"){
								var total = 0;
								$.each(rsp.resultadoCobrado, function(key,item){
									total = parseFloat(total) + parseFloat(item.incentivo);
									var dataTableRow = [
											item.fecha,
											'<b>'+item.numeroFactura+'</b>',
											item.pasajero,
											MASK(this,item.dtplus,'-$##,###,##0.00',1),
											'<b>'+MASK(this,item.incentivo,'-$##,###,##0.00',1)+'</b>',
											'<b>'+MASK(this,item.total,'-$##,###,##0.00',1)+'</b>',
											];
											var newrow = $('#reservasACobrar').dataTable().fnAddData(dataTableRow);
											// set class attribute for the newly added row 
											var nTr = $('#reservasACobrar').dataTable().fnSettings().aoData[newrow[0]].nTr;
											$('#reservasACobrar').dataTable().fnSort( [ [0,'desc'] ] );	
								})
								if(total !== 0){
									$('.green').html(MASK(this,total,'-$##,###,##0.00',0));
								}else{
									$('.green').html(0);
									$("#anchorMapa").attr("disabled",true);
								}

								$('#totalIncentivoACobrar').html(MASK(this,total,'-$##,###,##0.00',1));
								var totals = 0;
								$.each(rsp.resultadoPendiente, function(key,item){
									totals = totals + parseFloat(item.incentivo);
									var dataTableRow = [
											item.fecha,
											'<b>'+item.numeroFactura+'</b>',
											item.pasajero,
											MASK(this,item.dtplus,'-$##,###,##0.00',1),
											'<b>'+MASK(this,item.incentivo,'-$##,###,##0.00',1)+'</b>',
											'<b>'+MASK(this,item.total,'-$##,###,##0.00',1)+'</b>',
											];
											var newrow = $('#reservasPendiente').dataTable().fnAddData(dataTableRow);
											// set class attribute for the newly added row 
											var nTr = $('#reservasPendiente').dataTable().fnSettings().aoData[newrow[0]].nTr;
											$('#reservasPendiente').dataTable().fnSort( [ [0,'desc'] ] );	
								})
								if(totals !== 0){
									$('.blue').html(MASK(this,totals,'-$##,###,##0.00',0));
								}else{
									$('.blue').html(0);
								}
								$('#totalIncentivoPendiente').html(MASK(this,totals,'-$##,###,##0.00',1));
							}else{
								 $('#menu4').attr('style', 'display:none');
								//$('#menu4').addClass('in active');
								$('#menu4Li').attr('style', 'display:none');
								$("#menu5").attr('style', 'display:none');	
								$('#menu5Li').attr('style', 'display:none');   
								//$("#validation-errors").append('<div class="alert alert-error" style="    background-color: indianred;color: #ffff;"><strong>NO HAS LLEGADO A TU META</strong><div>');
							}	
					}
			})
		$('.green').trigger('click');

		$.ajax({
				type: "GET",
				url: "{{route('getResultadosparaPromoUser')}}",
				dataType: 'json',
				success: function(rsp){
					totalCanjeado = 0;
					totalPendiente = 0;
					$.each(rsp, function(key,item){
						console.log(item);
						if(item.estado_pedido=="P"){
							totalPendiente = totalPendiente + parseInt(item.monto);
							estado = "<b>Pendiente</b>";
							voucher= '<a style="height: 45px; width: 35px;padding-top: 10px; background-color: #f80a84" class="btn" href="voucherpdfUser/'+item.id+'"><i class="fa fa-file-pdf-o"></i></a>';						
						}else{
							totalCanjeado = totalPendiente +  parseInt(item.monto);
							estado = "<b>Cobrado</b>";
							voucher= '';
						}

						var dataTableRow = [
											'<b>'+item.fecha_pedido+'</b>',
											'<img class="img-responsive" src="./images/promociones/'+item.opcion_cambio+'.png" style="width: 50px;">',
											'<b>'+item.facturas+'</b>',
											estado,
											'<b>'+MASK(this,item.monto,'-$##,###,##0.00',1)+'</b>',
											voucher
											];
						var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
											// set class attribute for the newly added row 
						var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
						$('#listado').dataTable().fnSort( [ [0,'desc'] ] );	
					})	
					
					$('#totalCanjeado').html('<h1 class="subtitle" style="font-size: x-large;margin-top: 1%;margin-bottom: 1%;font-weight: bold; color: #072235">'+MASK(this,totalCanjeado,'-$##,###,##0.00',1)+'</h>');
					$('#totalPendiente').html('<h1 class="subtitle" style="font-size: x-large;margin-top: 1%;margin-bottom: 1%;font-weight: bold; color: #072235">'+MASK(this,totalPendiente,'-$##,###,##0.00',1)+'</h1>');					
				}
			})	

		function MASK(form, n, mask, format) {
		  if (format == "undefined") format = false;
		  if (format || NUM(n)) {
		    dec = 0, point = 0;
		    x = mask.indexOf(".")+1;
		    if (x) { dec = mask.length - x; }

		    if (dec) {
		      n = NUM(n, dec)+"";
		      x = n.indexOf(".")+1;
		      if (x) { point = n.length - x; } else { n += "."; }
		    } else {
		      n = NUM(n, 0)+"";
		    } 
		    for (var x = point; x < dec ; x++) {
		      n += "0";
		    }
		    x = n.length, y = mask.length, XMASK = "";
		    while ( x || y ) {
		      if ( x ) {
		        while ( y && "#0.".indexOf(mask.charAt(y-1)) == -1 ) {
		          if ( n.charAt(x-1) != "-")
		            XMASK = mask.charAt(y-1) + XMASK;
		          y--;
		        }
		        XMASK = n.charAt(x-1) + XMASK, x--;
		      } else if ( y && "$0".indexOf(mask.charAt(y-1))+1 ) {
		        XMASK = mask.charAt(y-1) + XMASK;
		      }
		      if ( y ) { y-- }
		    }
		  } else {
		     XMASK="";
		  }
		  return XMASK;
		}

		// Convierte una cadena alfanumérica a numérica (incluyendo formulas aritméticas)
		//
		// s   = cadena a ser convertida a numérica
		// dec = numero de decimales a redondear
		//
		// La función devuelve el numero redondeado

		function NUM(s, dec) {
		  for (var s = s+"", num = "", x = 0 ; x < s.length ; x++) {
		    c = s.charAt(x);
		    if (".-+/*".indexOf(c)+1 || c != " " && !isNaN(c)) { num+=c; }
		  }
		  if (isNaN(num)) { num = eval(num); }
		  if (num == "")  { num=0; } else { num = parseFloat(num); }
		  if (dec != undefined) {
		    r=.5; if (num<0) r=-r;
		    e=Math.pow(10, (dec>0) ? dec : 0 );
		    return parseInt(num*e+r) / e;
		  } else {
		    return num;
		  }
		}

	</script>

@endsection