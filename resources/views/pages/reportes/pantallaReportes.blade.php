<script type="text/javascript" src="{{ asset ('js/jquery-1.11.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset ('js/jquery-ui.1.10.4.min.js')}}"></script>
<script type="text/javascript" src="{{ asset ('js/bootstrap.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{asset('css/app-custom.css') }}">
<div>	
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 distancia">
			<div class="superior-derecho">
				<h1 class="subtitle hide-medium titulo-pantalla">TOP DE RESERVAS</h1>
				<table class="table table-bordered">
					<thead>
						<tr style="color:#e2076a">
							<th style="height: 50px;">Vendedor</th>
							<th>Agencia</th>
							<th>Cantidad</th>
							<th>Monto</th>
						</tr>
					</thead>
					<tbody id="top_reserva">
					</tbody>	
				</table>	
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 distancia">
			<div class="superior-izquierdo">
				<h1 class="subtitle hide-medium titulo-pantalla">TOP DE AGENCIAS</h1>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr style="color:#e2076a">
								<th style="width: 55%;">Agencia</th>
								<th>Total Usuarios</th>
								<th>Usuario Usaron</th>
								<th>Cant. Busquedas</th>
							</tr>
						</thead>
						<tbody id="top_agencias">
						</tbody>	
					</table>	
				</div>	
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 distancia">
			<div class="inferior-derecho">
				<h1 class="subtitle hide-medium titulo-pantalla">PROMOCION: TODOS GANAN CON DTP!!!</h1>
				<div class="row">
					<div class="col-md-4">
						<div class="row" style="margin-top: 15%;">
						 	<div class="col-md-9">
						 		<h5 class="subtitle hide-medium label-reporte-large">Total:</h5>
						 	</div>
						 	<div class="col-md-3">
						 		<h5 class="subtitle hide-medium label-reporte-resultado" id="usuario_total"></h5>
						 	</div>
						 	<div class="col-md-9">
						 		<h5 class="subtitle hide-medium label-reporte-large" style="color:#e2076a">Promoción:</h5>
						 	</div>
						 	<div class="col-md-3"> 
								<h5 class="subtitle hide-medium label-reporte-resultado" id="usuario_promocion" style="color:#e2076a"></h5>	
							</div>
						 	<div class="col-md-9">
						 		<h5 class="subtitle hide-medium label-reporte-large" style="color:#00007c">Restantes:</h5>
						 	</div>
						 	<div class="col-md-3">
								<h5 class="subtitle hide-medium label-reporte-resultado" id="usuario_no_promocion" style="color:#00007c"></h5>	
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div id="actual" class="piePromo"></div>
					</div>	
				</div> 
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6 distancia">
			<div class="inferior-izquierdo">
				<h1 class="subtitle hide-medium titulo-pantalla" >ACCESOS AL SISTEMA</h1>
				<div id="cuadro" class="lineAccesos"></div>
			</div>
		</div>
	</div>
</div>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script>
	cargaDatos();
	setInterval(cargaDatos, 10000);
	function cargaDatos(){	
		$.ajax({
				type: "GET",
				url: "{{route('getReservasReporte')}}",
				dataType: 'json',
				success: function(rsp){
									$("#top_agencias").html("");
									$.each(rsp, function(keys,values){
											var fila = "";
											fila += "<tr><td>"+values.razon_social+"</td><td>"+values.total_agencia+"</td><td>"+values.usuario_usaron+"</td><td><b>"+values.total_busquedas+"</b></td></tr>"
											$("#top_agencias").append(fila);
									});	
								}
				});

		$.ajax({
				type: "GET",
				url: "{{route('getReservasTotal')}}",
				dataType: 'json',
				success: function(rsp){
									$("#top_reserva").html("");
									$.each(rsp, function(keys,values){
											var fila = "";
											fila += "<tr><td><b>"+values.nombre_apellido+"</b></td><td>"+values.razon_social+"</td><td>"+values.totalusuario+"</td><td><b>"+formatNumber.new(values.totalcobrado, "$") +"</b></td></tr>"
											$("#top_reserva").append(fila);
									});	
								}
				});


		$.ajax({
				type: "GET",
				url: "{{route('getUsuariosPromo')}}",
				dataType: 'json',
				success: function(rsp){
					$('#usuario_total').html(rsp.total);
					$('#usuario_promocion').html(rsp.promo); 
					$('#usuario_no_promocion').html(rsp.restante);
					$('#actual').html("");
					$('#actual').highcharts({
					                    chart: {
					                        type: "pie"
					                    },
					                    title: {
					                        text: ""
					                    },
					                    colors: ['#FF0080', '#00007c'],
					                    xAxis: {
					                        type: 'category',
					                        allowDecimals: false,
					                        title: {
					                            text: ""
					                        }
					                    },
					                    yAxis: {
					                        title: {
					                            text: "Scores"
					                        }
					                    },
					                    series: [{
									        name: 'Detalle',
									        colorByPoint: true,
									        data: [
									            { name: 'Registrados', y: rsp.promo },
									            { name: 'Pendiente', y:  rsp.restante },
									        ]
									    }]
					        		});
					}
				});

		$.ajax({
				type: "GET",
				url: "{{route('getAccesosSemanales')}}",
				dataType: 'json',
				success: function(rsp){
						var categoria_json = new Array(); 
						var data_json = new Array();
						$.each(rsp, function(keys,values){
							var fecha = values.fecha.split('-');
							data_json.push([values.count]);
							categoria_json.push([fecha[2]+"/"+[fecha[0]]+"/"+fecha[1]]);
						})	

						$('#cuadro').highcharts({
							chart: {
									type: 'line'
									},
							title: {
									text: ''
									},
							colors: ['#00007c'],
							subtitle: {
									text: ''
									},
							xAxis: {
									categories: categoria_json
									},
							yAxis: {
									title: {
											text: ''
											},
									labels: {
											formatter: function () {
											                return this.value ;
											            }
											}
									},
							tooltip: {
									crosshairs: true,
									shared: true
									},
							plotOptions: {
										line: {
											   dataLabels: {
											                enabled: true
											            },
											            enableMouseTracking: false
											    }
										},
										series: [
												{
												name: 'Ingresados',
												marker: {
												            symbol: 'circle'
												        },
												data: data_json
												}
										]
									});
								}
							})	

	}	


	var formatNumber = {
		 separador: ".", // separador para los miles
		 sepDecimal: ',', // separador para los decimales
		 formatear:function (num){
		 num +='';
		 var splitStr = num.split('.');
		 var splitLeft = splitStr[0];
		 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
		 var regx = /(\d+)(\d{3})/;
		 while (regx.test(splitLeft)) {
		 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
		 }
		 return this.simbol + splitLeft +splitRight;
		 },
		 new:function(num, simbol){
		 this.simbol = simbol ||'';
		 return this.formatear(num);
		 }
		}


</script>
