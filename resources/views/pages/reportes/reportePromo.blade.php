@extends('master')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
@endsection
<?php
include("../comAerea.php");
include("../destinationFligth.php");

?>
@section('content')

<section id="content" class="gray-area" style="background: url('../public/images/fondo2.png')">
    <div class="container">
        <div id="main">
            @include('flash::message')
            <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large; margin-top: 5%">TODOS GANAN CON DTP !!!!</h1>
            <div class="block sectiontop">
				<ul class="nav nav-tabs">
					  <li class="active"><a data-toggle="tab" href="#home">Usuarios</a></li>
					  <li><a data-toggle="tab" href="#menu1">Detalle</a></li>
				</ul>
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
					 <form id="frmEstoy" class="contact-form" action="" method="post" autocomplete="nope"> 
					 <br>
					 	<div class="row">
					 		<div class="col-md-4">
					 			<div class="row" style="margin-top: 15%;">
					 				<div class="col-md-8">
					 		 			<h5 class="subtitle hide-medium" style="text-align: left; font-size: x-large; margin-top: 10%; margin-left: 20px;">Total:</h5>
					 				</div>
					 				<div class="col-md-4">
					 		 			<h5 class="subtitle hide-medium" id="usuario_total" style="text-align: left; font-size: x-large; margin-top: 10%;"></h5>
					 				</div>
					 				<div class="col-md-8">
					 					<h5 class="subtitle hide-medium" style="text-align: left; font-size: x-large; margin-top: 5%;  margin-left: 20px;">En Promoción:</h5>
					 				</div>
					 				<div class="col-md-4"> 
										<h5 class="subtitle hide-medium" id="usuario_promocion" style="text-align: left; font-size: x-large; margin-top: 5%;"></h5>	
									</div>
					 				<div class="col-md-8">
					 					<h5 class="subtitle hide-medium" style="text-align: left; font-size: x-large; margin-top: 5%;  margin-left: 20px;">Restantes:</h5>
					 				</div>
					 				<div class="col-md-4">
										<h5 class="subtitle hide-medium" id="usuario_no_promocion" style="text-align: left; font-size: x-large; margin-top: 5%;"></h5>	
									</div>
					 			</div>
					 		</div>
					 	 	<div class="col-md-8">
						 		<div id="actual" style="min-width: 210px; height: 300px; max-width: 500px; margin: 0 auto"></div>
						 	</div>	
						</div> 
					 </form>
					</div>
					<div id="menu1" class="tab-pane fade" style="padding:15px">
					 	<h5 class="subtitle hide-medium" style="text-align: left; font-size: xx-large; margin-top: 3%;margin-bottom: 3%; margin-left: 30px;">Detalles de Usuarios</h5>
						<table id="reservasPromocion" class="table table-bordered table-hover">
							<thead>
								<tr class='bgblue'>
									<th style="background-color: #072235">Fecha</th>
									<th style="background-color: #072235">Usuario</th>
									<th style="background-color: #072235">Email</th>
									<th style="background-color: #072235">Agencia</th>
									<th style="background-color: #072235">Sucursal</th>
									<th style="background-color: #072235">Telefono</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
            	</div>
        	</div>
        </form>	
    </div>
</section>
@endsection

@section('scripts')
	@include('layouts/scripts')
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script>
		$('#reservasPromocion').dataTable();	
		$.ajax({
				type: "GET",
				url: "{{route('getUsuariosPromo')}}",
				dataType: 'json',
				success: function(rsp){
					$('#usuario_total').html(rsp.total);
					$('#usuario_promocion').html(rsp.promo); 
					$('#usuario_no_promocion').html(rsp.restante);
					$('#actual').html("");
					$('#actual').highcharts({
					                    chart: {
					                        type: "pie"
					                    },
					                    title: {
					                        text: "USUARIOS"
					                    },
					                    colors: ['#FF0080', '#00007c'],
					                    xAxis: {
					                        type: 'category',
					                        allowDecimals: false,
					                        title: {
					                            text: ""
					                        }
					                    },
					                    yAxis: {
					                        title: {
					                            text: "Scores"
					                        }
					                    },
					                    series: [{
									        name: 'Detalle',
									        colorByPoint: true,
									        data: [
									            { name: 'Registrados', y: rsp.promo },
									            { name: 'Pendiente', y:  rsp.restante },
									        ]
									    }]
					        		});
					}
				});	

		$.ajax({
				type: "GET",
				url: "{{route('getUsuariosDetallePromo')}}",
				dataType: 'json',
				success: function(rsp){
									var oSettings = $('#reservasPromocion').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();

									for (i=0;i<=iTotalRecords;i++) {
										$('#reservasPromocion').dataTable().fnDeleteRow(0,null,true);
									}
									$.each(rsp, function (key, item){
										        	var dataTableRow = [
																	item.fecha_ingreso_promo,
																	item.nombre_apellido,
																	item.email,
																	item.agencia,
																	item.sucursal,
																	item.celular,
																	];
													var newrow = $('#reservasPromocion').dataTable().fnAddData(dataTableRow);
													// set class attribute for the newly added row 
													var nTr = $('#reservasPromocion').dataTable().fnSettings().aoData[newrow[0]].nTr;
																})	
													$('#reservasPromocion').dataTable().fnSort([ [0,'desc'] ] );	
											}
				})							
	</script>
@endsection