@extends('master')

@section('title', 'Detalle de Reserva')
@section('styles')
	@parent
	<style type="text/css">
            #container-main h1{
                font-size: 40px;
                text-shadow:4px 4px 5px #16a085;
            }

            .accordion-container {
                width: 100%;
                margin: 0 0 20px;
                clear:both;
            }

            .accordion-titulo {
                position: relative;
                display: block;
                padding: 20px;
                font-size: 24px;
                font-weight: 300;
                background: #fff;
                color: #2c3e50;
                text-decoration: none;
                text-align: center;
            }
            .accordion-titulo.open {
                background: #fff;
                color: #2c3e50;
                text-align: center;
            }
            .accordion-titulo:hover {
                background: #fff;
                color: #2c3e50;
                text-align: center;
            }

            .accordion-titulo span.toggle-icon:before {
                content:"+";
            }

            .accordion-titulo.open span.toggle-icon:before {
                content:"-";
            }

            .accordion-titulo span.toggle-icon {
                position: absolute;
                top: 10px;
                right: 20px;
                font-size: 38px;
                font-weight:bold;
            }
            .tooltip{
                  display: inline;
                  position: relative;
              }
              
            .tooltip:hover:after{
                  background: #333;
                  background: rgba(0,0,0,.8);
                  border-radius: 5px;
                  bottom: 26px;
                  color: #fff;
                  content: attr(title);
                  left: 20%;
                  padding: 5px 15px;
                  position: absolute;
                  z-index: 98;
                  width: 220px;
              }
              
            .tooltip:hover:before{
                  border: solid;
                  border-color: #333 transparent;
                  border-width: 6px 6px 0 6px;
                  bottom: 20px;
                  content: "";
                  left: 50%;
                  position: absolute;
                  z-index: 99;
              }
 
    </style>
@endsection
@section('content')
<section class="sectiontop">
	<div class="container">
    <div class="accordion-container">
        <a href="#" title="Formas de Cajear tu DTPlus" class="accordion-titulo">FORMAS DE CANJEAR&nbsp;&nbsp;<span class="toggle-icon"></span></a>
        <div class="accordion-content">
            <div class="row bm-portfolio onepixel portfolio-filter isotope" style="position: relative; overflow: hidden; height: 2560px;">
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                  <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                         <a id="copetrol" href="{{route('detalleCanje', ['id' =>'copetrol'])}}"><img class="img-responsive detalle" src="{{asset('images/promociones/copetrol.png')}}" id = "copetrol"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="Copetrol" alt="Copetrol"></a>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                         <a id="stock" href="{{route('detalleCanje', ['id' =>'stock'])}}"><img class="img-responsive detalle" src="{{asset('images/promociones/stock.png')}}" id = "stock"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="Stock" alt="Stock"></a>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                         <a id="seis" href="{{route('detalleCanje', ['id' =>'seis'])}}"><img class="img-responsive detalle" src="{{asset('images/promociones/seis.png')}}" id = "seis"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="SuperSeis" alt="SuperSeis"></a>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                         <a id="ggcia" href="{{route('detalleCanje', ['id' =>'ggcia'])}}"><img class="img-responsive detalle" src="{{asset('images/promociones/ggcia.png')}}" style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" id="ggcia" title="Gonzalez Gimenez y Cia" alt="Gonzalez Gimenez y Cia"></a>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                       <a data-toggle="modal" id="bolsaUs" href="#requestModal"><img class="img-responsive" src="{{asset('images/promociones/dolares.png')}}" id = "dolares"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="Efectivo Dolares" alt="Efectivo Dolares"></a>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 bottom-onepixel element Hotels Transfers Activities isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                    <div class="portfolio-item" style="border-radius: 5px; background-color: #ffffff;">
                        <div class="portfolio-mark">
                            <div class="portfolio-link">
                            </div>
                            <div class="portfolio-mark-content">
                                <div class="mark-content">
                                    <h4 class="bottom-0 portfolio-title">
                                    </h4>
                                </div>
                            </div>
                        </div>
                       <a data-toggle="modal" id="bolsaGs" href="#requestModal"><img class="img-responsive" src="{{asset('images/promociones/guaranies.png')}}" id = "guaranies"  style="height: 160px;width: 360px;margin-top: 20px;border-radius: 5px;background-color: #ffffff;padding-left: 50px;padding-right: 50px; cursor:pointer;" title="Efectivo Guaranies" alt="Efectivo Guaranies"></a>
                    </div>
                </div>

            </div>        
        </div>  
    </div>
</div>
    <div id="requestModal" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-top: 200px;">
        <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title titlepage" style="font-size: x-large;">GENERAR FACTURA</h2>
                    </div>
                    <div class="modal-body">
                        <div id="contenido">
                          <div class ="row">
                              <div class="col-xs-12 col-sm-3 col-md-3">
                                Nombre
                              </div>
                              <div class="col-xs-12 col-sm-3 col-md-3">
                              </div>
                              <div class="col-xs-12 col-sm-3 col-md-3">
                                Ruc
                              </div>
                              <div class="col-xs-12 col-sm-3 col-md-3">
                              </div>
                          </div>  
                          <div class ="row">
                              <div class="col-xs-12 col-sm-3 col-md-3">
                                Monto
                              </div>
                              <div class="col-xs-12 col-sm-3 col-md-3">
                              </div>
                              <div class="col-xs-12 col-sm-3 col-md-3">
                              </div>
                              <div class="col-xs-12 col-sm-3 col-md-3">
                              </div>
                          </div>  
                          <form class="validator-form" id="upload" enctype="multipart/form-data" method="post" action="{{ url('uploadDTPlus') }}" autocomplete="off">
                            <div class="form-group">
                              <div class="row">
                                <div id="validation-errors"></div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                  <label class="control-label" style="margin-left: 3%; width: 96%">Imagen</label>
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                  <input type="file" class="form-control" name="image" id="image" style="margin-left: 3%; width: 96%"/> 
                                  </div> 
                                  <div class="col-xs-12 col-sm-2 col-md-2">
                                    <div id="output" style="width: 100px;height: 100px;">
                                    </div>
                                  </div> 
                                  <div class="col-xs-12 col-sm-1 col-md-1">
                                    <div id="btn-delete">
                                    </div>
                                  </div>

                                </div>  
                              </div>  
                          </form>
                          <form class="validator-form" id="uploadPost" method="get" action="{{ url('detalleCanje')}}">
                              <input type="hidden" name="imagen" id="imagen"/>
                              <input type="hidden" name="id" id="id"/>
                           </form>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" id="btnAceptarCanje" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Aceptar</button>
                    <button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
                  </div>
            </div>
        </div>
    </div>
</section>		

@endsection
@section('scripts')
    @parent
  <script type="text/javascript" src="{{asset('mC/js/jquery.form.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/api.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/countto.js')}}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/hoverIntent.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/analytics.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/easing.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/fitvids.js')}}"></script> 
  <script type="text/javascript" src="{{ URL::asset('js/functions.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/superfish.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/mmenu.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/magnificpopup.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/isotope.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/jquery.themepunch.revolution.min.js')}}"></script> 
	<script type="text/javascript" src="{{ URL::asset('js/jquery.themepunch.plugins.min.js')}}"></script> 
    <script type="text/javascript">
        $(".accordion-titulo").click(function(){
           var contenido=$(this).next(".accordion-content");
           if(contenido.css("display")=="none"){ //open     
              contenido.slideDown(250);         
              $(this).addClass("open");
           }
           else{ //close        
              contenido.slideUp(250);
              $(this).removeClass("open");  
          }
        });

      $(document).ready(function(){
          var options = { 
                        beforeSubmit:  showRequest,
                success: showResponse,

            dataType: 'json' 
                }; 
          $('body').delegate('#image','change', function(){
            $('#upload').ajaxForm(options).submit();      
          });

      })

       $(".img-responsive").click(function(){
          var id = $(this).attr('id');
           $("#id").val(id);
       })

      function showRequest(formData, jqForm, options) { 
        $("#validation-errors").hide().empty();
        $("#output").css('display','none');
        return true; 
      } 
      function showResponse(response, statusText, xhr, $form)  { 
            if(response.success == false)
            {
              var arr = response.errors;
              console
              $.each(arr, function(index, value)
              {
                if (value.length != 0)
                {
                  $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                }
              });
              $("#validation-errors").show();
            } else {
               $("#imagen").val(response.archivo);  
               $("#output").html("<img class='img-responsive' src='"+response.file+"'/>");
               $("#output").css('display','block');
               $("#btn-delete").html("<button type='button' id='btn-imagen' data-id='"+response.archivo+"'><i class='glyphicon glyphicon-remove'></i></button>");
               eliminarImagen();
            }
       }

      function eliminarImagen(){
        $("#btn-imagen").on("click", function(e){
          dataString = "file="+$(this).attr('data-id');
          $.ajax({
              type: "GET",
              url: "{{route('fileDelDTPlus')}}",//fileDelDTPlus
              dataType: 'json',
              data: dataString,
              success: function(rsp){
                $("#imagen").val("");
                $("#output").html("");
                $("#btn-delete").html("");
              }
          })      

        });
      } 



      $("#btnAceptarCanje").click(function(){
       $('#uploadPost').submit();       
      }) 
       
    </script>
@endsection
