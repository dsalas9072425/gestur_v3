@extends('master')
@section('title', 'Búsqueda de Reserva')
@section('styles')
	@parent
	<style>
		.btn{
			width: 20%;
		}
		.btn-error{
			background-color: #FF0080;
		}
		.dt-body-right{
			cursor:pointer;
		}
		.colum1{
			min-width: 250px;
		}
		.colum2{
			min-width: 150px;
		}
		.colum3{
			min-width: 250px;
		}
	</style>
@endsection
<?php
include("../comAerea.php");
include("../destinationFligth.php");
?>
@section('content')
	<section id="content" class="gray-area">
	    @include('flash::message')
   		<div class="container">
        	<div id="main">
				<h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large; margin-top: 5%">Reservas</h1>
            <div class="block sectiontop">
				<ul class="nav nav-tabs">
					  <li class="active"><a data-toggle="tab" href="#home">Hoteles</a></li>
					  <li><a data-toggle="tab" href="#menu1">Vuelos</a></li>
					  <li><a data-toggle="tab" href="#menu2">Circuitos</a></li>
					  <li><a data-toggle="tab" href="#menu3">Actividades</a></li>
					  <li><a data-toggle="tab" href="#menu4">Traslado</a></li>
				</ul>
				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
							<br>
                            <br>
                            <form action="" id="frmBusqueda" method="post" style="margin-left: 1%; margin-right: 2%;"> 
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                        <input type="hidden" class="input-text full-width" id="cant_noche" name="cant_noche"/>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Proforma</label>
                                                <input type="number" placeholder="N° de Expediente" class="input-text full-width" id="nrodeexpediente" name="idFileReserva" value="0">
                                            </div>
											<div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Localizador</label>
                                                <input type="text" placeholder="Localizador" class="input-text full-width" id="localizador" name="localizador"/>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Pasajero Principal</label>
                                                <input type="text" placeholder="Pasajero Principal" class="input-text full-width" id="pasajeroPrincipal" name="pasajeroPrincipal">
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Desde</label>
                                                <div class="datepicker-wrap">
                                                    <input type="text" value="" name="desde" id="desde" class="input-text full-width required fecha datepicker"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <label>Hasta</label>
                                                <div class="datepicker-wrap">
                                                    <input type="text" value="" name="hasta" id="hasta" class="input-text full-width required fecha datepicker"/>
                                                </div>
                                            </div>
											<div class="col-xs-12 col-sm-4 col-md-4">
												<div class="form-group">
													<label>Estado</label>
													<select name="estadoreserva" class = "Requerido form-control" class="input-text full-width" id="estadoreserva">
														<option value="0">Todos</option>
														@foreach($listadoState as $state)
														  <option value="{{$state['value']}}">{{$state['label']}}</option>
														@endforeach
													</select>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                   		<div class="col-md-12">
                                   		</div>
                                        <div class="col-md-12">
                                            <label class="hidden-xs">&nbsp;</label>
                                            <button type="button" style="width: 160px;  background-color: #e2076a;" id= "buscarReservas" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-search"></i> Buscar Reservas</button>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="hidden-xs">&nbsp;</label>
                                            <button id="limpiarFiltros" style="width: 160px;" type="reset" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                                        </div>
                                        <div class="col-md-12">
                                             <label class="hidden-xs">&nbsp;</label>
                                            <!--<button type="button" style="width: 160px; background-color: #fdb714;" id="botonExcel" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="fa fa-file-excel-o"></i> Exportar a Excel</span></button>-->

                                        </div>
                                    </div>
								</div>
								<div class="row">
										<h3 class="tab-content-title">Referencias</h3>
										<div class="col-xs-12 col-sm-1 col-md-1">
										</div>
										<div class="col-xs-12 col-sm-2 col-md-3">
											<img alt="" style="width: 20px; margin-left: 25%;" src="images/icon/2.png"><b>Confirmado</b>
										</div>
										<div class="col-xs-12 col-sm-2 col-md-3">
											<img alt="" style="width: 20px; margin-left: 25%;" src="images/icon/3.png"><b>Cancelado</b>
										</div>
										<div class="col-xs-12 col-sm-2 col-md-3">
											<img alt="" style="width: 20px; margin-left: 25%;" src="images/icon/1.png"><b>Rechazado</b>
										</div>
								</div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas" style= "overflow-x: scroll; overflow-y: hidden;">
										<div class="table-responsive">
											<br>
											<table id="reservas" class="table table-bordered table-hover">
												<thead>
													<tr class='bgblue'>
														<th></th>
														<th>Fecha</th>
														<th>Proforma</th>
														<th>Localizador</th>
														<th>Pasajero</th>
														<th>Vendedor<br>Agencia</th>
													</tr>
												</thead>
												<tbody>

												</tbody>
											</table>
										</div>
                                    </div>
                                </div>
                            </form>
					</div>
					<div id="menu1" class="tab-pane fade">
						<br>
                        <br>
						<form action="" id="frmBusquedaFlight" method="post" style="margin-left: 1%; margin-right: 2%;">
						    <div class="row">
                                <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                    <input type="hidden" class="form-control" id="cant_noche" name="cant_noche"/>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Localizador</label>
                                            <input placeholder="Localizador" class="form-control" id="nrodeexpediente" name="idFileReserva" >
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Origen</label>
											<input id="origen" name="origen" type="hidden">
											<input id="origen_name" class="Requerido form-control" placeholder="Origen" style="height: 39px;">
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Destino</label>
											<input id="destinoF" name="destino" type="hidden">
											<input id="destinationF_name" placeholder="Destino" class="Requerido form-control" style="height: 39px;">
                                        </div>
                                    </div>    
                                    <div class="row">    
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Compañia</label>
				                             <select data-placeholder="Compañia Aerea" class="chosen-select-deselect destination text-center" id="companhia" required name="companhia">
				                                 <option value=""></option>
				                                 @foreach($listadoComp as $key=>$listado)
				                                 	<option value="{{$key}}">{{$listado['label']}}</option>
				                                 @endforeach
											 </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button type="button" style="width: 160px;  background-color: #e2076a;" id= "buscarReservasFlight" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-search"></i> Buscar Reservas</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button id="limpiarFiltros" style="width: 160px;" type="reset" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <!--<button type="button" style="width: 160px; background-color: #fdb714;" id="botonExcel" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="fa fa-file-excel-o"></i> Exportar a Excel</span></button>-->
                                    </div>
                                </div>
							</div>
						    <div class="row">
	                            <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas" style= "overflow-x: scroll; overflow-y: hidden;">
											<div class="table-responsive">
												<br>
												<table id="reservasFligth" class="table table-bordered table-hover" style="margin-left: 1%; margin-right: 3%;">
													<thead>
														<tr class='bgblue'>
															<th>Id</th>
															<th>Fecha</th>
															<th>Localizador</th>
															<!--<th>Proforma N°</th>-->
															<th>Fecha<br> Origen</th>
															<th>Origen</th>
															<th>Fecha<br>Destino</th>
															<th>Destino</th>
															<th>Agencia</th>
															<th>Compañia de<br>Reserva</th>
															<th>Total</th>
															<th>Accion</th>
														</tr>
													</thead>
												<tbody>
												@if(isset($resultsFligth))
													@foreach($resultsFligth as $key=>$reservaFligth)
														@if(isset($reservaFligth))
														<tr style='width:50px'>
																<td><b>{{$reservaFligth->id}}</b></td>
																<td>{{$reservaFligth->fecha_reserva}} {{date("G:i",strtotime($reservaFligth->hora_reserva))}}</td>
																<td><b>{{$reservaFligth->controlnumber}}</b></td>
																<td>{{$reservaFligth->fecha_origen}}</td>
																<td>{{$destination[$reservaFligth->cod_origen]}}</td>
																<td>{{$reservaFligth->fecha_destino}}</td>
																<td>{{$destination[$reservaFligth->cod_destino]}}</td>
																<td>{{$reservaFligth->agencia->razon_social}}</td>
																<td><img src="https://images.kiwi.com/airlines/64/{{$reservaFligth->marketingcompany}}.png" class="imageHead" style="width: 40px;"> 
																<?php 
																	$marketingcompany= explode("|", $comAerea[$reservaFligth->marketingcompany])
																?>
																 {{$marketingcompany[0]}} - {{$marketingcompany[3]}} </td>
																<td>{{$reservaFligth->total_monto_vuelo}}</td>
																<td>
																	<a href="{{route('detallesReservaFligth', ['id' =>$reservaFligth->id])}}" target="_blank"><i class="fa fa-file-text-o fa-lg"></i></a>	
																</td>
														</tr>
														@endif
												@endforeach
											@endif
											</tbody>
										</table>
									</div>
	                             </div>
	                        </div>
	                    </form>    
					</div>
					<div id="menu2" class="tab-pane fade">
						<br>
                        <br>						
                        <form action="" id="frmBusquedaTour" method="post" style="margin-left: 1%; margin-right: 2%;">
						    <div class="row">
                                <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Localizador</label>
                                            <input placeholder="Localizador" class="form-control" id="localizadorBusqueda" name="localizadorBusqueda" >
                                        </div>
                                       <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Desde</label>
                                            <div class="datepicker-wrap">
                                                <input type="text" value="" name="desdeBusqueda" id="desdeBusquedaTour" class="form-control required fecha"/>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Hasta</label>
                                            <div class="datepicker-wrap">
                                                <input type="text" value="" name="hastaBusqueda" id="hastaBusquedaTour" class="form-control required fecha"/>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Pasajero</label>
                                            <input type="number" placeholder="Pasajero" class="form-control" id="pasajeroBusqueda" name="pasajeroBusqueda" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button type="button" style="width: 160px;  background-color: #e2076a;" id= "buscarReservasFlight" data="Tour" class="btn btn-info text-center btn transaction_normal hide-small normal-button buscarReserva"><i class="glyphicon glyphicon-search"></i> Buscar Reservas</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button id="limpiarFiltros" style="width: 160px;" type="reset" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <!--<button type="button" style="width: 160px; background-color: #fdb714;" id="botonExcel" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="fa fa-file-excel-o"></i> Exportar a Excel</span></button>-->
                                    </div>
                                </div>
							</div>
							<div class="row">
		                        <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas" style= "overflow-x: scroll; overflow-y: hidden;">
									<div class="table-responsive">
										<br>
											<table id="reservasTour" class="table table-bordered table-hover" style="margin-left: 1%; margin-right: 3%;">
												<thead>
													<tr class='bgblue'>
														<th></th>
														<th>Fecha</th>
														<th>Localizador</th>
														<th>Proforma</th>
														<th>Pasajero</th>
														<th>Agencia</th>
														<th>Usuario</th>
														<th>Vendedor DTP</th>
														<th>Fecha de Gasto</th>
														<th>Monto<br>Cancelación</th>
														<th>Precio<br>Venta</th>
														<th>Asignar<br>Proforma</th>
													</tr>
												</thead>
												<tbody>
													@foreach($reservasCiruito as $key=>$reservaCiruito)
														@php
														switch ($reservaCiruito->estado) {
															//Pendiente
															case config('constants.resPendiente'):
																echo "<tr class='alert-info' style='width:80px'>";
																break;
															//Aprobado
															case config('constants.resConfirmada'):
																echo "<tr class='alert-success' style='width:80px'>";
																break;
															//Cancelado    
															case config('constants.resEliminada'):
																echo  "<tr class='alert-danger' style='width:80px'>";
																break;
															//Rechazado	
															case config('constants.resRechazada'):
																echo "<tr class='alert-warning' style='width:80px'>";
																break;
															//Error	
															case config('constants.resError'):
																echo "<tr class='alert-error' style='width:80px'>";
																break;
															}
														@endphp
															@if($isDtp)	
																<td>
																	<img alt="" style="width: 90px;" src="images/proveedores/{{$reservaCiruito->id_proveedor}}.png">
																</td>
															@endif
															<td>{{$reservaCiruito->fecha}}</th>
															<td><b>{{$reservaCiruito->localizador}}</b></td>
															<td>{{$reservaCiruito->proforma}}</td>
															<td><b>{{$reservaCiruito->pasajero}}</b></td>
															<td>{{$reservaCiruito->agencia}}</td>
															<td>{{$reservaCiruito->vendedor_agencia}}</td>
															<td>{{$reservaCiruito->vendedor_dtp}}</td>
															<td>{{$reservaCiruito->cancelacion_desde}}</td>
															<td>{{$reservaCiruito->cancelacion_monto}}</td>
															<td>{{$reservaCiruito->monto}}</td>
															<td>
															@if($reservaCiruito->estado == 2 && empty($reservaCiruito->proforma))
																<a onclick='cargar({{$reservaCiruito->id_reserva}})' data-toggle="modal" id="{{$reservaCiruito->id_reserva}}" href="#requestModal" class="btn-mas-request"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;&nbsp;
																<a onclick='cancelarReserva({{$reservaCiruito->id_reserva}})' id="reserva_{{$reservaCiruito->id_reserva}}"' class='fontCancelar' title='Cancelar Reserva'><i class='fa fa-times'></i></a>
															@endif

															</td>
														</tr>
													@endforeach
												</tbody>
											</table>
									</div>
		                        </div>
		                    </div>
		               </form>	
					</div>
					<div id="menu3" class="tab-pane fade">
						<br>
                        <br>						
                        <form action="" id="frmBusquedaActivity" method="post" style="margin-left: 1%; margin-right: 2%;">
						    <div class="row">
                                <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Localizador</label>
                                            <input type="number" placeholder="Localizador" class="form-control" id="localizadorBusqueda" name="localizadorBusqueda" >
                                        </div>
                                       <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Desde</label>
                                            <div class="datepicker-wrap">
                                                <input type="text" value="" name="desdeBusqueda" id="desdeBusquedaActivity" class="form-control required fecha"/>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Hasta</label>
                                            <div class="datepicker-wrap">
                                                <input type="text" value="" name="hastaBusqueda" id="hastaBusquedaActivity" class="form-control required fecha"/>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Pasajero</label>
                                            <input type="number" placeholder="Pasajero" class="form-control" id="pasajeroBusqueda" name="pasajeroBusqueda" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button type="button" style="width: 160px;  background-color: #e2076a;" id= "buscarReservasFlight" data="Activity" class="btn btn-info text-center btn transaction_normal hide-small normal-button buscarReserva"><i class="glyphicon glyphicon-search"></i> Buscar Reservas</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button id="limpiarFiltros" style="width: 160px;" type="reset" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <!--<button type="button" style="width: 160px; background-color: #fdb714;" id="botonExcel" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="fa fa-file-excel-o"></i> Exportar a Excel</span></button>-->
                                    </div>
                                </div>
							</div>
							<div class="row">
		                        <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas" style= "overflow-x: scroll; overflow-y: hidden;">
									<div class="table-responsive">
											<table id="reservasActivity" class="table table-bordered table-hover" style="margin-left: 1%; margin-right: 3%;">
												<thead>
													<tr class='bgblue'>
														@if($isDtp)
															<th></th>
														@endif	
														<th>Fecha</th>
														<th>Localizador</th>
														<th>Proforma</th>
														<th>Pasajero</th>
														<th>Agencia</th>
														<th>Usuario</th>
														<th>Vendedor DTP</th>
														<th>Fecha de Gasto</th>
														<th>Monto<br>Cancelación</th>
														<th>Precio<br>Venta</th>
														<th>Asignar<br>Proforma</th>
													</tr>
												</thead>
												<tbody>
													@foreach($reservasActividad as $key=>$reservaActividad)
															@php
															switch ($reservaActividad->estado) {
																//Pendiente
																case config('constants.resPendiente'):
																	echo "<tr class='alert-info' style='width:80px'>";
																	break;
																//Aprobado
																case config('constants.resConfirmada'):
																	echo "<tr class='alert-success' style='width:80px'>";
																	break;
																//Cancelado    
																case config('constants.resEliminada'):
																	echo  "<tr class='alert-danger' style='width:80px'>";
																	break;
																//Rechazado	
																case config('constants.resRechazada'):
																	echo "<tr class='alert-warning' style='width:80px'>";
																	break;
																//Error	
																case config('constants.resError'):
																	echo "<tr class='alert-error' style='width:80px'>";
																	break;
																}
															@endphp
															@if($isDtp)	
																<td>
																	<img alt="" style="width: 90px;" src="images/proveedores/{{$reservaActividad->id_proveedor}}.png">
																</td>
															@endif
															<td>{{$reservaActividad->fecha}}</th>
															<td><b>{{$reservaActividad->localizador}}</b></td>
															<td>{{$reservaActividad->proforma}}</td>
															<td><b>{{$reservaActividad->pasajero}}</b></td>
															<td>{{$reservaActividad->agencia}}</td>
															<td>{{$reservaActividad->vendedor_agencia}}</td>
															<td>{{$reservaActividad->vendedor_dtp}}</td>
															<td>{{$reservaActividad->cancelacion_desde}}</td>
															<td>{{$reservaActividad->cancelacion_monto}}</td>
															<td>{{$reservaActividad->monto}}</td>
															<td>
															@if($reservaActividad->estado == 2 && empty($reservaActividad->proforma))
																<a data-toggle="modal" onclick='cargar({{$reservaActividad->id_reserva}})' id="{{$reservaActividad->id_reserva}}" href="#requestModal" class="btn-mas-request"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;&nbsp;
																<a onclick='cancelarReserva({{$reservaActividad->id_reserva}})' id="reserva_{{$reservaActividad->id_reserva}}"' class='fontCancelar' title='Cancelar Reserva'><i class='fa fa-times'></i></a>
															@endif	
															</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
		                        	</div>
		                    	</div>
	                    </form>	
					</div>
					<div id="menu4" class="tab-pane fade">
						<br>
						<form action="" id="frmBusquedaTransfer" method="post" style="margin-left: 1%; margin-right: 2%;">
						    <div class="row">
                                <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Localizador</label>
                                            <input type="number" placeholder="Localizador" class="form-control" id="localizadorBusqueda" name="localizadorBusqueda" >
                                        </div>
                                       <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Desde</label>
                                            <div class="datepicker-wrap">
                                                <input type="text" value="" name="desdeBusqueda" id="desdeBusquedaTransfer" class="form-control required fecha"/>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Hasta</label>
                                            <div class="datepicker-wrap">
                                                <input type="text" value="" name="hastaBusqueda" id="hastaBusquedaTransfer" class="form-control required fecha"/>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                            <label>Pasajero</label>
                                            <input type="number" placeholder="Pasajero" class="form-control" id="pasajeroBusqueda" name="pasajeroBusqueda" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button type="button" style="width: 160px;  background-color: #e2076a;" id= "buscarReservasFlight" data="Transfer" class="btn btn-info text-center btn transaction_normal hide-small normal-button buscarReserva"><i class="glyphicon glyphicon-search"></i> Buscar Reservas</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button id="limpiarFiltros" style="width: 160px;" type="reset" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <!--<button type="button" style="width: 160px; background-color: #fdb714;" id="botonExcel" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="fa fa-file-excel-o"></i> Exportar a Excel</span></button>-->
                                    </div>
                                </div>
							</div>
							<div class="row">
		                        <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas" style= "overflow-x: scroll; overflow-y: hidden;">
									<div class="table-responsive">
											<table id="reservasTransfer" class="table table-bordered table-hover" style="margin-left: 1%; margin-right: 3%;">
												<thead>
													<tr class='bgblue'>
														@if($isDtp)
															<th></th>
														@endif	
														<th>Fecha</th>
														<th>Localizador</th>
														<th>Proforma</th>
														<th>Pasajero</th>
														<th>Agencia</th>
														<th>Usuario</th>
														<!--<th>Vendedor DTP</th>-->
														<th>Fecha de Gasto</th>
														<th>Monto<br>Cancelación</th>
														<th>Precio<br>Venta</th>
														<th>Asignar<br>Proforma</th>
													</tr>
												</thead>
												<tbody>
													@foreach($reservasTraslado as $key=>$reservaTraslado)
															@php
															switch ($reservaTraslado->estado) {
																//Pendiente
																case config('constants.resPendiente'):
																	echo "<tr class='alert-info' style='width:80px'>";
																	break;
																//Aprobado
																case config('constants.resConfirmada'):
																	echo "<tr class='alert-success' style='width:80px'>";
																	break;
																//Cancelado    
																case config('constants.resEliminada'):
																	echo  "<tr class='alert-danger' style='width:80px'>";
																	break;
																//Rechazado	
																case config('constants.resRechazada'):
																	echo "<tr class='alert-warning' style='width:80px'>";
																	break;
																//Error	
																case config('constants.resError'):
																	echo "<tr class='alert-error' style='width:80px'>";
																	break;
																}
															@endphp
															@if($isDtp)	
																<td>
																	<img alt="" style="width: 90px;" src="images/proveedores/{{$reservaTraslado->id_proveedor}}.png">
																</td>
															@endif
															<td>{{$reservaTraslado->fecha}}</th>
															<td><b>{{$reservaTraslado->localizador}}</b></td>
															<td>{{$reservaTraslado->proforma}}</td>
															<td><b>{{$reservaTraslado->pasajero}}</b></td>
															<td>{{$reservaTraslado->agencia}}</td>
															<td>{{$reservaTraslado->vendedor_agencia}}</td>
															<!--<td>{{--$reservaTraslado->vendedor_dtp--}}</td>-->
															<td>{{$reservaTraslado->cancelacion_desde}}</td>
															<td>{{$reservaTraslado->cancelacion_monto}}</td>
															<td>{{$reservaTraslado->monto}}</td>
															<td>
																@if($reservaTraslado->estado == 2 && empty($reservaTraslado->proforma))
																<a data-toggle="modal" onclick='cargar({{$reservaTraslado->id_reserva}})' id="{{$reservaTraslado->id_reserva}}" href="#requestModal" class="btn-mas-request"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;&nbsp;
																<a onclick='cancelarReserva({{$reservaTraslado->id_reserva}})' id="reserva_{{$reservaTraslado->id_reserva}}"' class='fontCancelar' title='Cancelar Reserva'><i class='fa fa-times'></i></a>
																@endif
																</td>
														</tr>
													@endforeach
												</tbody>
											</table>
									</div>
		                        </div>
		                    </div>
						</div>
						</form>	
					</div>            	
            	</div>
        	</div>
       	</div>
        </div>
	<!-- Modal Request-->
	<div id="requestModal" class="modal fade" role="dialog">
		<form id="frmProforma" method="post" action="" style="margin-top: 20%;">
	  		<div class="modal-dialog">
		<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h2 class="modal-title titlepage" style="font-size: x-large;">Seleccionar Proforma</h2>
					</div>
				  	<div class="modal-body">
				  		<div id="contenido">
							<div class="row">
								<br>	
								<input type="hidden" class="input-text full-width" id="id_reserva" name="id_reserva" >
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<h5 style="font-size: 16px; margin-bottom: 10px;" class="filter-name has-expander expanded">Seleccione Expediente (*)</h5>
										<div class="selector">
											<select required class="full-width" id="expediente" name="expediente">
													<option value="">Seleccione Proforma </option>
													<option value="0">Nueva Proforma</option>
												@if(isset($selectProforma))
													@foreach($selectProforma as $key=>$proforma)
		                                                <option value="{{$proforma['value']}}">{{$proforma['label']}}</option>
													@endforeach
												@endif	
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<h5 style="font-size: 16px; margin-bottom: 10px;" class="filter-name has-expander expanded">Vendedor DTP Mundo</h5>
										<div class="selector">
											<select class="full-width" id="agente" name="agente">
		                                        @foreach($selectAgentes as $key=>$agentes)
		                                            <option value="{{$agentes['value']}}">{{$agentes['label']}}</option>
		                                        @endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
				  		</div>
				  </div>
				  <div class="modal-footer">
					<button type="button" id="btnAceptarProforma" class="btn btn-danger" style="width: 90px; background-color: #e2076a;">Aceptar</button>
					<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 90px; background-color: #969dac;" data-dismiss="modal">Cerrar</button>
				  </div>
				</div>
	  		</div>
		 </form>
	</div>
    </section>
@endsection

@section('scripts')
	@include('layouts/scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/sorting/date-dd-MMM-yyyy.js"></script>
	<script>
/* Formatting function for row details - modify as you need */
		$("#desdeBusquedaTransfer").datepicker({dateFormat: 'dd/mm/yy'});
		$("#hastaBusquedaTransfer").datepicker({dateFormat: 'dd/mm/yy'});
		$("#desdeBusquedaActivity").datepicker({dateFormat: 'dd/mm/yy'});
		$("#hastaBusquedaActivity").datepicker({dateFormat: 'dd/mm/yy'});
		$("#desdeBusquedaTour").datepicker({dateFormat: 'dd/mm/yy'});
		$("#hastaBusquedaTour").datepicker({dateFormat: 'dd/mm/yy'});
		$('#reservasActivity').dataTable();
		$('#reservasTour').dataTable();
		$('#reservasTransfer').dataTable();
		$('#reservasFligth').dataTable();
		function startProcess(){
			$(".buscarReserva").click(function(){
				var encabezado = $(this).attr('data');
				var dataString = $("#frmBusqueda"+$(this).attr('data')).serialize();
				dataString += '&type='+encabezado;
				$.ajax({
					type: "GET",
					url: "{{route('getReservasServicios')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
						console.log(rsp);
						var oSettings = $("#reservas"+encabezado).dataTable().fnSettings();
						var iTotalRecords = oSettings.fnRecordsTotal();
						for (i=0;i<=iTotalRecords;i++) {
							$("#reservas"+encabezado).dataTable().fnDeleteRow(0,null,true);
						}
						$.each(rsp, function (key, item){
						var totalIconos = `<img alt="" style="width: 90px;" src="images/proveedores/`+ item.id_proveedor+ `.png">`;

						var iconocancelar ='<a data-toggle="modal" id="'+item.id+'" href="#requestModal" class="btn-mas-request"><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;&nbsp;<a onclick="cancelarReserva('+item.id+')" id="reserva_'+item.id_reserva+'" class="fontCancelar" title="Cancelar Reserva"><i class="fa fa-times"></i></a>';

						var pasajero = '<b>'+item.pasajero+'</b>';
						var dataTableRow = [
											totalIconos,
											item.fecha,
											item.localizador,
											item.proforma,
											pasajero,
											item.agencia,
											item.vendedor_agencia,
											item.vendedor_dtp,
											item.cancelacion_desde,
											item.cancelacion_monto,
											item.monto,
											iconocancelar
										];
						var newrow = $("#reservas"+encabezado).dataTable().fnAddData(dataTableRow);

						// set class attribute for the newly added row 
						var nTr = $("#reservas"+encabezado).dataTable().fnSettings().aoData[newrow[0]].nTr;
						$('td',$("#reservas"+encabezado).dataTable().fnSettings().aoData[newrow[0]].anCells[0]).addClass('alert-error');
						// and parse the row:
						var nTds = $('td', nTr);
						//nTds.style('width', .css("background-color"););
						switch (item.estado) {
							//Pendiente
							case 1:
								nTds.addClass('alert-info');
								break;
							//Aprobado
							case 2:
								nTds.addClass('alert-success');
								break;
							//Cancelado    
							case 3:
								nTds.addClass('alert-danger');
								break;
								//Rechazado	
							case 4:
								nTds.addClass('alert-warning');
								break;
							//Error	
							case 5:
								nTds.addClass('alert-error');
								break;
						}

						})
					}
				})		
			})
			$("#buscarReservasFlight").click(function(){
				var dataString = $("#frmBusquedaFlight").serialize();
				$.ajax({
					type: "GET",
					url: "{{route('getReservasFlight')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
							var oSettings = $('#reservasFligth').dataTable().fnSettings();
							var iTotalRecords = oSettings.fnRecordsTotal();
							for (i=0;i<=iTotalRecords;i++) {
								$('#reservasFligth').dataTable().fnDeleteRow(0,null,true);
							}

							$.each(rsp, function (key, item){
								var iconocancelar ='<a href="../public/detallesReservaFligth/'+item.id+'" target="_blank"><i class="fa fa-file-text-o fa-lg"></i>';

								var fechaHora = item.fecha_reserva+" "+item.hora_reserva;
								var dataTableRow = [
														item.id,
														fechaHora,
														item.controlnumber,
														item.fecha_origen,
														item.cod_origen,
														item.fecha_destino,
														item.cod_destino,
														item.agencia.razon_social,
														item.marketingcompany,
														item.total_monto_vuelo,
														iconocancelar
													];
								var newrow = $('#reservasFligth').dataTable().fnAddData(dataTableRow);
								var nTr = $('#reservasFligth').dataTable().fnSettings().aoData[newrow[0]].nTr;
								$('td',$('#reservasFligth').dataTable().fnSettings().aoData[newrow[0]].anCells[0]).addClass('alert-error');
										// and parse the row:
										var nTds = $('td', nTr);
										//nTds.style('width', .css("background-color"););
										switch (item.estado) {
											//Pendiente
											case 1:
												nTds.addClass('alert-info');
												break;
											//Aprobado
											case 2:
												nTds.addClass('alert-success');
												break;
											//Cancelado    
											case 3:
												nTds.addClass('alert-danger');
												break;
											//Rechazado	
											case 4:
												nTds.addClass('alert-warning');
												break;
											//Error	
											case 5:
												nTds.addClass('alert-error');
												break;
										}


								})
							}
						})	
					});	
			}

		function cargar(idReserva){
			$('#id_reserva').val(idReserva);
		}

		function cancelarReserva(idReserva){
			console.log(idReserva);
			 BootstrapDialog.confirm({
		            title: '<b>DTP</b>',
		            message: 'Seguro desea cancelar la Reserva?',
		            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
		            closable: true, // <-- Default value is false
		            draggable: true, // <-- Default value is false
		            btnCancelLabel: 'No', // <-- Default value is 'Cancel',
		            btnOKLabel: 'Si, Cancelar', // <-- Default value is 'OK',
		            btnOKClass: 'btn-error', // <-- If you didn't specify it, dialog type will be used,
		            callback: function(result) {
		                // result will be true if button was click, while it will be false if users close the dialog directly.
		                if(result) 
						{
		                    $.blockUI({
		                        centerY: 0,
		                        message: "<h2>Procesando...</h2>",
		                        css: {
		                            color: '#000'
		                        }
		                    });
							
							console.log("{{route('getCancelarReservas')}}?idReserva="+idReserva);
						}	
					}
				});	
		}	

		function cancelarBusqueda(idReserva){
		        BootstrapDialog.confirm({
		            title: 'DTP',
		            message: 'Seguro desea cancelar la Reserva?',
		            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
		            closable: true, // <-- Default value is false
		            draggable: true, // <-- Default value is false
		            btnCancelLabel: 'No', // <-- Default value is 'Cancel',
		            btnOKLabel: 'Si, cancelar Reserva', // <-- Default value is 'OK',
		            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
		            callback: function(result) {
		                // result will be true if button was click, while it will be false if users close the dialog directly.
		                if(result) 
						{
		                    $.blockUI({
		                        centerY: 0,
		                        message: "<h2>Procesando...</h2>",
		                        css: {
		                            color: '#000'
		                        }
		                    });
							
							//alert("{{route('getCancelarReservas')}}?idReserva="+idReserva);
						   	$.ajax({
								type: "GET",
								url: "{{route('getCancelarReservas')}}?idReserva="+idReserva,
								//dataType: 'json',
								data: idReserva,
								success: function(rsp){
										$.unblockUI();
										console.log(rsp);
			                            if(rsp.codRetorno == 0)
			                            {
			                           		startProcess()
			                                $("#buscarReservas").trigger('click');
			                                BootstrapDialog.show({
			                                    title: 'DTPMUNDO',
			                                    message: " La reserva ha sido cancelada con éxito. "
			                                });
			                            }
			                            else
			                            {
			                                BootstrapDialog.show({
			                                    title: 'DTPMUNDO',
			                                    message: rsp.desRetorno,
			                                    type: BootstrapDialog.TYPE_DANGER // <-- Default value is BootstrapDialog.TYPE_PRIMARY
			                                });
			                            }
									}

								})
		                }else {
		                    return false;
		                }
		            }
		        });
		}


		$('.nav-tabs a').on('shown.bs.tab', function(event){
		    var x = $(event.target).text();         // active tab
		    var y = $(event.relatedTarget).text();  // previous tab
		});

		$("#companhia").chosen();

		$('#destinationF_name').autocomplete({
		    source: function (request, response) {
	          $.getJSON("{{'destinationsFligth'}}?term=" + request.term, function (data) {
		            response($.map(data, function (value, key) {
		            	console.log(key);
		            	console.log(value);
		                return {
		                    label: value,
		                    value: value,
		                    valor: key
		                };
		            }));
		        });
		    },
		    minLength: 3,
		    delay: 100,
		    maxShowItems: 7,
		    select: function( event, ui ) {
				$('#destinoF').val(ui.item.valor);
		      }
		});

		$('#origen_name').autocomplete({
		    source: function (request, response) {
	          $.getJSON("{{'destinationsFligth'}}?term=" + request.term, function (data) {
		            response($.map(data, function (value, key) {
		                return {
		                    label: value,
		                    value: value,
		                    valor: key
		                };
		            }));
		        });
		    },
		    minLength: 3,
		    delay: 100,
		    maxShowItems: 7,
		    select: function( event, ui ) {
				$('#origen').val(ui.item.valor);
		      }
		});

		$("#btnAceptarProforma").click(function(){
			var dataString = $("#frmProforma").serialize();
			console.log(dataString);
			$.ajax({
					type: "GET",
					url: "{{route('guardarReservaRegistro')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
							$("#requestModal").modal('hide');
							if(rsp.RestVentasOut.codRetorno == 0){
								BootstrapDialog.show({
				                                    title: 'DTPMUNDO',
				                                    message: rsp.RestVentasOut.descRetorno
				                                });
							}else{
			                    BootstrapDialog.show({
			                                    title: 'DTPMUNDO',
			                                    message: rsp.RestVentasOut.descRetorno,
			                                    type: BootstrapDialog.TYPE_DANGER // <-- Default value is BootstrapDialog.TYPE_PRIMARY
			                                });

							}	
							$(".buscarReservas").trigger('click');
						}
				})
		})

		$(document).ready(function() {
			$( ".datepicker" ).datepicker( "option", "dateFormat", 'dd/mm/yy' );
			$("#hasta").datepicker("setDate", new Date());
			var date2 = $('#hasta').datepicker('getDate');
			date2.setDate(date2.getDate()-31);
			$('#desde').datepicker('setDate', date2);
			$('#reservas').DataTable();
		    $("#buscarReservas").trigger('click');
		})

		$("#buscarReservas").click(function(){
				console.log('inico');
				var dataString = $("#frmBusqueda").serialize();
				$('#reservas').DataTable().clear();
				$('#reservas').DataTable().destroy();
				$('#reservas').DataTable({
				        "ajax": {
				        	"serverSide": true,
				            "url": "{{route('getReservas')}}",
				            "data": {
        								"localizador": $('#localizador').val(),
        								"pasajeroPrincipal": $('#pasajeroPrincipal').val(),
        								"desde": $('#desde').val(),
        								"hasta": $('#hasta').val(),
        								"estadoreserva": $('#estadoreserva').val(),
										"nrodeexpediente": $('#nrodeexpediente').val()   							
									},
    						},
    						"processing": true,
    						"deferLoading": 57,
				            "columns": [
								            {
								                "className":      'details-control',
								                "orderable":      false,
								                "data":           "estado",
								                "defaultContent": "",
								                "className": 'dt-body-right'
								            },
								            { 
								            	"data": "fecha",  
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "proforma",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "localizador",
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "pasajero", 
								            	"className": 'dt-body-right'
								            },
								            { 
								            	"data": "vendedor_agencia",
								            	"className": 'dt-body-right'
								            }
										],
										"columnDefs": [
									       { "type": 'date-dd-mmm-yyyy', targets: 1 }
									     ],
										"order": [[1, 'asc']]	
    					})	
				})

		$('#reservas tbody').on('click', 'td.dt-body-right', function () {
					var table = $('#reservas').DataTable();
				    var tr = $(this).closest('tr');
				    var row = table.row(tr);
				     if (row.child.isShown()) {
				            // This row is already open - close it
				            row.child.hide();
				            tr.removeClass('shown');
				        }
				        else {

				            // Open this row
				            row.child(format(row.data()) ).show();
				            tr.addClass('shown');
				        }
				} )
		function format(d) {
				console.log(d);
				   if(d.estados == {{config('constants.resConfirmada')}}){
						var accion = '<button type="button" onclick="cancelarBusqueda('+d.id_reserva+')" style="width: 120px;  background-color: #e2076a;" id= "reserva_'+d.id_reserva+'" class="btn btn-info text-center btn transaction_normal hide-small normal-button"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>&nbsp;&nbsp;<a href="{{url("detallesReserva")}}/'+d.id_reserva+'" class="btn btn-info text-center btn transaction_normal hide-small normal-button" target="_blank" style="width: 120px; background-color: #1c2b39; height: 41px;"><i class="glyphicon glyphicon-list-alt" style="padding-top: 10px;"></i>&nbsp;&nbsp;Ver Detalles</a>';
					}else{
						var accion = '<a href="{{url("detallesReserva")}}/'+d.id_reserva+'" class="btn btn-info text-center btn transaction_normal hide-small normal-button" target="_blank" style="width: 120px; background-color: #1c2b39; height: 41px;"><i class="glyphicon glyphicon-list-alt" style="padding-top: 10px;"></i>&nbsp;&nbsp;Ver Detalles</a>';
					}

				cancelacion_desde = d.cancelacion_desde;
				if(cancelacion_desde != null){
					cancelacion_desde = d.cancelacion_desde;
				}else{
					cancelacion_desde = "";
				}

				monto_sin_comision = d.monto_sin_comision;
				if(monto_sin_comision != "1.00"){
					monto_sin_comision = d.monto_sin_comision+' US';
				}else{
					monto_sin_comision = "";
				}

				cancelacion_monto = d.cancelacion_monto;
				if(cancelacion_monto != null){
					cancelacion_monto = cancelacion_monto+' US';
				}else{
					cancelacion_monto = "";
				}


				var tabla = '<table cellpadding="6" cellspacing="0" border="0" style="padding-left:50px;background-color: #eaebee; width: 100%"> ';
					tabla+= '<tr>';
					tabla+= '<td><b>Destino:</b></td><td>'+d.destino+'</td><td></td>';
					tabla+= '<td><b>Fecha Gasto:</b></td><td><b>'+cancelacion_desde+'</b></td><td></td>';
					tabla+= '</tr>';
					tabla+= '<tr>';	
					tabla+= '<td><b>Precio Venta:</b></td><td><b>'+d.monto_cobrado+' US</b></td><td></td>';
					tabla+= '<td><b>Monto Cancelación:</b></td><td><b>'+cancelacion_monto+'</b></td><td></td>';
					tabla+= '</tr>';
					tabla+= '<tr>';	
					tabla+= '<td><b>Agencia:</b></td><td>'+d.agencia+'</td><td></td>';
				@if($isDtp)
					tabla+= '<td><b>Precio sin Comisión:</b></td><td>'+monto_sin_comision+'</td><td></td>';
					tabla+= '</tr>';
					tabla+= '<tr>';	
					tabla+= '<td><b>Vendedor:</b></td><td>'+d.vendedor_dtp+'</td><td></td>';
					tabla+= '<td><b>Proveedor:</b></td><td><img alt="" style="width: 90px;" src="images/proveedores/'+d.id_proveedor+'.png"></td><td>'+accion+'</td>';
					tabla+= '</tr>';
				@else
					tabla+= '<td></td><td></td><td></td>';
					tabla+= '</tr>';
					tabla+= '<tr>';	
					tabla+= '<td></td><td></td><td></td>';
					tabla+= '<td></td><td></td><td>'+accion+'</td>';
					tabla+= '</tr>';
				@endif
					tabla+= '</table>';
				    return tabla;  
				}
				startProcess();
	</script>
@endsection