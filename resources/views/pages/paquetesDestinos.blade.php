@extends('master')
@section('title', 'Paquetes Categorias')
@section('styles')
	@parent
@endsection

@section('content')
<section class="sectiontop">
	<div class="container">
            <div class="auto-container" style="margin-top: 2%;">
                <!--Sortable Gallery-->
                <div class="mixitup-gallery">
                    <!--Filter-->
                    <div class="filters clearfix">
                        <ul class="filter-tabs filter-btns">
                            <li class="filter active" data-role="button" data-filter="all">Ver todos</li>
                            @foreach($categorias as $key=>$categoria)
                                <li class="filter" data-role="button" data-filter=".{{$key}}">{{utf8_decode ($categoria['nombre'])}}</li>
                            @endforeach
                        </ul>
                    </div>
                    
                    <div class="filter-list row clearfix">
                        <!--Case Block-->
                        @foreach($paquetesDestinos as $keys=>$category)
                        <div class="case-block mix all {{$category->destino}} col-md-4 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <?
                                            $filename='http://www.dtp.com.py/emisivo/ADMIN_PRO2/todas_las_img2/'.$category->fotos; 
                                            if(isset($filename)) {
                                                echo "<img src='$filename'/>";
                                            }else{ ?>
                                                <img src="{{asset('images/dtpmundo-list.jpg')}}"/>;
                                        <?php        
                                            }
                                        ?>                                        
                                        <div class="overlay-box">
                                            <div class="overlay-inner">
                                                <a href="{{url('paqueteDetalle', ['id' =>$category->id, 'agencia'=>$agencia])}}" class="case-link">VER PAQUETE</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="{{url('paqueteDetalle', ['id' =>$category->id, 'agencia'=>$agencia])}}">{{utf8_decode ($category->titulo)}}</a></h3>
                                    </div>
                                </div>
                            </div>
                        @endforeach    
                    </div> 
                </div>
            </div>
	</div>
        @include('partials/modalPaquetes')

</section>	
@endsection

@section('scripts')
	@include('layouts/scripts')
    @parent
        <!--End Revolution Slider-->
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/jquery-ui.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/jquery.fancybox.pack.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/jquery.fancybox-media.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/owl.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/appear.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/mixitup.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/wow.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/paquetes/script.js')}}"></script>
        <script type="text/javascript">
            @if(Session::get('datos-loggeo')->datos->datosUsuarios->logoAgencia == "SIN_LOGO")    
                $('#Detalles').modal({
                    show: 'true'
                }); 
                //document.getElementById('DetallesLoca').click(function(){
                $('#Detalles').click(function(){
                    $('#Detalles').modal('hide');   
                })
            @endif
        </script>
@endsection
