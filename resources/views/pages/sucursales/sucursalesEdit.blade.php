@extends('master')
@section('title', 'Panel de Control')
@section('styles')
<style>
	.form-control{
			border: 1px solid #111 !important;
	}		
</style>

@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
                <form id="frmSucursales" class="contact-form" action="{{route('doEditSucursales')}}" method="post">
                @include('flash::message')
                {{ csrf_field() }}
					<div class="booking-information travelo-box">
						<div>
						<h1 class="subtitle hide-medium" style="font-size: x-large;">Editar Sucursal</h1>
							<br>
                            	<input type="hidden" class="form-control" value="{{$sucursales[0]->id_sucursal_agencia}}" name="idSucursalAgencia" id="idSucursalAgencia"/>
	                            <div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Nombre Sucursal (*)</label>
	                                		<input type="text" required class = "Requerido form-control" name="descripcionAgencia" id="descripcionAgencia"  value="{{$sucursales[0]->descripcion_agencia}}"/>
	                                    </div>
	                                   	<div class="col-xs-12 col-sm-6 col-md-6">
					                        <label class="control-label">Agencia (*)</label>
					                        <select name="agencia_id" required class = "chosen-select-deselect" class="input-text full-width required" id="agencia_id">
											@foreach($listadoAgencia as $key=>$agencia)
												<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
											@endforeach
											</select>
					                    </div>
	                                </div>
	                            </div>
	                          	<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Email (*)</label>
	                                		<input type="email" required class = "Requerido form-control" name="email" id="email" value="{{$sucursales[0]->email}}"/>
	                                    </div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Telefono (*)</label>
											<input type="text" required class = "Requerido form-control" name="telefono" id="telefono" value="{{$sucursales[0]->telefono}}"/>
										</div>
									</div>
	                            </div>
								<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Activo (*)</label>
											<select name="activo" required class = "Requerido form-control" class="input-text full-width required" id="activo">
												<option value="true">SI</option>
												<option value="false">NO</option>
											</select>
										</div>
	                            		<div class="col-xs-12 col-sm-6 col-md-6">
	                            		</div>
	                                </div>
	                            </div>
							</form>
								<div class="form-group">
									<br>
	                                <button type="submit" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style=" background: #e2076a !important;" name="guardar" value="Guardar" >Guardar</button>
	                                <br>
	                            </div>
	                        <br>   
							<!--<a id="notPass" target="_blank" href="#">Notificar Passwords a Usuarios de Sucursal</a>-->
						</div>
					</div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/scripts')
	<script type="text/javascript">
    $(document).ready(function() {
		$('#agencia_id option[value="{{$sucursales[0]->id_agencia}}"]').attr("selected", "selected");
		$('#agencia_id').trigger("chosen:updated");
		var activo = "{{$sucursales[0]->activo}}";
		if(activo =="S"){
			$('#activo option[value="true"]').attr("selected", "selected");
		}else{
			$('#activo option[value="false"]').attr("selected", "selected");
		}

        $(".chosen").chosen();
        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Sin resultados!'},
            '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

    })	
    </script>
@endsection