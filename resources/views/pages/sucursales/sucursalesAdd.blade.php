@extends('master')
@section('title', 'Panel de Control')
@section('styles')
@parent
<style>
	.form-control{
			border: 1px solid #111 !important;
	}		
</style>
@endsection
@section('content')
	<div class="container">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
                <form id="frmSucursales" class="contact-form" action="{{route('doAddSucursales')}}" method="post">
                	{{ csrf_field() }}
                	@include('flash::message')
					<div class="booking-information travelo-box">
						<div>
							<h2 class="box-title">Nueva Sucursal</h2>
							<br>
	                            <div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Nombre Sucursal (*)</label>
	                                		<input type="text" required class = "Requerido form-control" name="descripcionAgencia" id="descripcionAgencia"  value=""/>
	                                    </div>
										<div class="col-xs-12 col-sm-6 col-md-6">
					                        <label class="control-label">Agencia (*)</label>
					                        <select name="agencia_id" required class = "chosen-select-deselect" class="input-text full-width required" id="agencia_id">
											@foreach($listadoAgencia as $key=>$agencia)
												<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
											@endforeach
											</select>
					                    </div>
	                                </div>
	                            </div>
	                          	<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Email (*)</label>
	                                		<input type="email" required class="Requerido form-control" name="email" id="email"  value=""/>
	                                    </div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Telefono (*)</label>
											<input type="text" required class="Requerido form-control" name="telefono" id="telefono" value=""/>
										</div>
									</div>
	                            </div>
								<div class="form-group">
	                            	<div class="row">
	                                	<div class="col-xs-12 col-sm-6 col-md-6">
	                                    	<label class="control-label">Activo (*)</label>
												<select name="activo" required class = "Requerido form-control" class="input-text full-width required" id="activo">
													<option value="true">SI</option>
													<option value="false">NO</option>
												</select>
										</div>
	                                </div>
	                            </div>
	                           	<div class="form-group">
									<br>
	                                <button type="submit" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; background: #e2076a !important;" name="guardar" value="Guardar" >Guardar</button>
	                                <br>
	                            </div>
	                        <br>   
							<!--<a id="notPass" target="_blank" href="#">Notificar Passwords a Usuarios de Sucursal</a>-->
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/scripts')
	<script>
        $(".chosen").chosen();
        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Sin resultados!'},
            '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
	</script>
@endsection