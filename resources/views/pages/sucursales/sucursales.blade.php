@extends('master')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
            	<form id="frmAgencias" class="contact-form" action="" method="post">
            	    @include('flash::message')
					<div class="booking-information travelo-box">
						<div>
							<h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Sucursales</h1>
							<br>
							<div class="row">
								<div class="form-group col-xs-12 col-sm-12">
									<label class="control-label">Agencia:</label>
									<select name="agencia" required class = "chosen-select-deselect" class="input-text full-width required" id="agenciaId">
										<option value="0">Seleccione una Agencia</option>
										@foreach($listadoAgencia as $key=>$agencia)
											<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
										@endforeach
									</select>
								</div>	
							</div>
							<br>
							<div class="row">
							</div>
							<br>
            				<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
	                            <thead>
	                            	<tr>
	                                	<th>ID</th>
	                                  	<th>Sucursal</th>
	                                  	<th>Agencia</th>
				                      	<th>Teléfono</th>
	                                   	<th>Email</th>
	                               </tr>
	                            </thead>
	                            <tbody>
	                                @foreach($sucursales as $sucursal)
		                                <tr>
		                            		<td><b>{{$sucursal->id_sucursal_agencia}}</b></td>
		                            	    <td>{{$sucursal->descripcion_agencia}}</td>
		                                  	<td><b>{{$sucursal->agencia->razon_social}}</b></td>
					                      	<td>{{$sucursal->telefono}}</td>
		                                   	<td><b>{{$sucursal->email}}</b></td>
		                                </tr>   	
	                                @endforeach   	
	                            </tbody>
                        </table>'
                        </div>
                    </div>    			
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/scripts')
		
	<script>
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
			$('#agenciaId option[value="{{$agenciaId[0]->id_agencia}}"]').attr("selected", "selected");
			$('#agenciaId').trigger("chosen:updated");
		});

        $(".chosen").chosen();
        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Sin resultados!'},
            '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

		$("#agenciaId").change(function(){
			idAgencia = $(this).val();
			$.blockUI({
		            centerY: 0,
		            message: "<h2>Procesando...</h2>",
		                css: {
		                    color: '#000'
		                    }
		                });
			$.ajax({
					type: "GET",
					url: "{{route('getAgenciasSucursales')}}",
					dataType: 'json',
					data: {
						dataAgencia: idAgencia
			       			},
					success: function(rsp){
								$.unblockUI();
								var oSettings = $('#listado').dataTable().fnSettings();
								var iTotalRecords = oSettings.fnRecordsTotal();

									for (i=0;i<=iTotalRecords;i++) {
										$('#listado').dataTable().fnDeleteRow(0,null,true);
									}

									$.each(rsp, function (key, item){
										var accion = "<a href='./sucursalesEdit/"+item.id_sucursal_agencia+"' class='btn btn-info text-center btn transaction_normal hide-small normal-button' style='padding-top: 10px; background: #e2076a !important;' role='button'>Editar</a>";
								        var dataTableRow = [
															item.id_sucursal_agencia,
															item.descripcion_agencia,
															item.agencia.razon_social,
															item.telefono,
															item.email,
															accion
															];

										var newrow = $('#listado').dataTable().fnAddData(dataTableRow);
										// set class attribute for the newly added row 
										var nTr = $('#listado').dataTable().fnSettings().aoData[newrow[0]].nTr;
										// and parse the row:
										var nTds = $('td', nTr);
									})	
							}
 					});
		});
	</script>
@endsection