@extends('master')
@section('title', 'Formulario Promoción')
@section('styles')
    @parent
    <style>
        .contact-form label {
            font-size: 1em;
            margin-bottom: 6px;
            margin-top: 6px;
            font-weight: bold;
        }
        section#content {
            text-align: center;
        }
        .content {
            text-align: left;
        }
        @font-face {
            font-family: 'bebas';
            src: url('fonts/bebas.eot?') format('eot'), 
                 url('fonts/bebas.woff') format('woff'), 
                 url('fonts/bebas.ttf')  format('truetype'),
                 url('fonts/bebas.svg#bebas') format('svg');
        }

         .panel-tit-label{
        font-family: 'Bebas Neue', arial;
        color: #fff;
        }

    </style>
@endsection

@section('content')

<section id="content" class="gray-area" style="background: url('images/tapiz.jpg')">
    <div class="container">
        <div id="main">
           @include('flash::message')
            <div class="block">
                  <h1 class="subtitle hide-medium panel-tit-label" style="font-size: xx-large;padding-left: 5%;margin-top: 5%; color: #fff">FORMULARIO DE PROMOCIÓN</h1>
                </br>
                <form id="frmUsuarios" class="contact-form" action="{{route('postPromocion')}}" method="post" autocomplete="nope"> 
                    {{ csrf_field()}}
                    <div class="col-xs-12 col-sm-2 col-md-2">
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <input type="hidden" required class = "Requerido form-control" name="id_usuario" id="id_usuario" value="{{$usuario[0]->id_usuario}}"/>

                                <label class="control-label panel-tit-label">Nombre(*)</label>
                                <input type="text" readonly="readonly" class = "Requerido form-control" name="nombreApellido" id="nombreApellido" placeholder="Nombres y Apellido" value="{{$usuario[0]->nombre_apellido}}"/>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label class="control-label panel-tit-label">Email (*)</label>
                                    <input type="email" readonly="readonly" class = "Requerido form-control" name="email" id="email" placeholder="Email" value="{{$usuario[0]->email}}" autocapitalize="off"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label class="control-label panel-tit-label">Cédula de Identidad</label>
                                <input type="number" required class = "Requerido form-control" name="documento_identidad" id="cedula" placeholder="Cédula de identidad" value=""/>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label class="control-label panel-tit-label">Fecha de Nacimiento (*)</label>
                                <div clas="row">
                                    <div class="col-md-4" style="padding-left: 0px;">
                                        <select required class = "Requerido form-control" name= "dia">
                                            <option value ="">Dia</option>
                                            @for ($i = 1; $i < 32; $i++)
                                                <option value ="{{$i}}">{{$i}}</option>
                                            @endfor    
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="padding-left: 0px;padding-right: 0px;">
                                        <select required class = "Requerido form-control" name= "mes">
                                                <option value ="">Mes</option>
                                                <option value ="01">Enero</option>
                                                <option value ="02">Febrero</option>
                                                <option value ="03">Marzo</option>
                                                <option value ="04">Abril</option>
                                                <option value ="05">Mayo</option>
                                                <option value ="06">Junio</option>
                                                <option value ="07">Julio</option>
                                                <option value ="08">Agosto</option>
                                                <option value ="09">Septiembre</option>
                                                <option value ="10">Octubre</option>
                                                <option value ="11">Noviembre</option>
                                                <option value ="12">Diciembre</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4" style="padding-right: 0px;">
                                        <select required class = "Requerido form-control" name= "anho">
                                                <option value ="">Año</option>
                                            @for ($x = 1940; $x < 2001; $x++)
                                                <option value ="{{$x}}">{{$x}}</option>
                                            @endfor    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="row"> 
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label class="control-label panel-tit-label">Celular (*)</label>
                                <input type="number" required class = "Requerido form-control" name="celular" id="celular" placeholder="Celular" value=""/>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label class="control-label panel-tit-label">¿Color favorito?</label>
                                    <select name="color" required class = "Requerido form-control" id="perfil_id">
                                            <option value=""><b>Seleccione un color</b></option>
                                            <option style="background-color:#FF0000; font-weight: bold; color:white" value="#FF0000"><b>Rojo</b></option>
                                            <option style="background-color:#FFFF00; font-weight: bold;" value="#FFFF00"><b>Amarillo</b></option>
                                            <option style="background-color:#008000; font-weight: bold; color:white" value="#008000"><b>Verde</b></option>
                                            <option style="background-color:#808080; font-weight: bold; color:white" value="#808080"><b>Gris</b></option>
                                            <option style="background-color:#FF00FF; font-weight: bold; color:white" value="#FF00FF"><b>Fucsia</b></option>
                                            <option style="background-color:#0000FF; font-weight: bold; color:white" value="#0000FF"><b>Azul</b></option>
                                            <option style="background-color:#00FFFF; font-weight: bold;" value="#00FFFF"><b>Cian</b></option>
                                            <option style="background-color:#FFD700; font-weight: bold;" value="#FFD700"><b>Dorado</b></option>
                                            <option style="background-color:#4B0082; font-weight: bold; color:white" value="#4B0082"><b>Indigo</b></option>
                                            <option style="background-color:#FF00FF; font-weight: bold; color:white" value="#FF00FF"><b>Magenta</b></option>
                                            <option style="background-color:#800080; font-weight: bold; color:white" value="#800080"><b>Purpura</b></option>
                                            <option style="background-color:#FFC0CB; font-weight: bold;" value="#FFC0CB"><b>Rosado</b></option>
                                            <option style="background-color:#FFFFFF; font-weight: bold;" value="#FFFFFF"><b>Blanco</b></option>
                                            <option style="background-color:#FFA500; font-weight: bold;" value="#FFA500"><b>Naranja</b></option>
                                            <option style="background-color:#000080; font-weight: bold; color:white" value="#000080"><b>Azul Naval</b></option>
                                            <option style="background-color:#32CD32; font-weight: bold;" value="#32CD32"><b>Verde Limón</b></option>
                                            <option style="background-color:#E6E6FA; font-weight: bold;" value="#E6E6FA"><b>Lavanda</b></option>
                                            <option style="background-color:#8B0000; font-weight: bold;  color:white" value="#8B0000"><b>Granate</b></option>
                                            <option style="background-color:#000000; font-weight: bold; color:white" value="#000000"><b>Negro</b></option>
                                            <option style="background-color:#FFFFE0; font-weight: bold;" value="#FFFFE0"><b>Amarillo Claro</b></option>
                                            <option style="background-color:#A52A2A; font-weight: bold; color:white" value="#A52A2A"><b>Marron</b></option>
                                    </select>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <label class="control-label panel-tit-label">Destinos sonados que te gustaria conocer</label>
                                <small style="color: #fff;">(en orden de prioridad)</small>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <label class="control-label panel-tit-label">¿Destinos que mas vendes?</label>
                                <small style="color: #fff;">(en orden de prioridad)</small>
                            </div>

                        </div>  
                       @for ($i = 0; $i < 3; $i++)
                            <?php 
                                $y = $i + 1;
                            ?>
                            <div class="row">
                                <div class="col-sm-1">
                                    <label class="control-label" style="color: #fff;">{{$y}})</label>
                                </div>
                                <div class="col-sm-5" style="padding-left: 0px;">
                                    <input id="destino_conocer{{$y}}" name="destino_conocer{{$y}}" type="hidden">
                                    <input id="destinations_name_conocer{{$y}}" required class="Requerido form-control">
                                </div>
                                <div class="col-sm-1">
                                    <label class="control-label" style="color: #fff;">{{$y}})</label>
                                </div>
                                <div class="col-sm-5" style="padding-left: 0px;">
                                    <input id="destino_venta{{$y}}" name="destino_venta{{$y}}" type="hidden">
                                    <input id="destinations_name_venta{{$y}}" required class="Requerido form-control">
                                </div>
                            </div> 
                            </br>
                        @endfor

                         <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label class="control-label panel-tit-label">Actividad o Hobbie </label>
                                <div class="col-sm-2 content" style="padding-left: 0px;"></div>
                                <div class="col-sm-4 content" style="padding-left: 0px; color: #fff;">
                                    <input type="checkbox" color="#fff" name="actividades[]" value="autos"> Autos<br>
                                    <input type="checkbox" color="#fff" name="actividades[]" value="deportes"> Deportes<br>
                                    <input type="checkbox" color="#fff" name="actividades[]" value="videojuegos"> Videojuegos
                                </div>
                                <div class="col-sm-2 content" style="padding-left: 0px;"></div>
                                <div class="col-sm-4 content" style="padding-left: 0px; color: #fff;">
                                    <input type="checkbox" color="#fff" name="actividades[]" value="viajes"> Viajes<br>
                                    <input type="checkbox" color="#fff" name="actividades[]" value="cine"> Cine<br>
                                    <input type="checkbox" color="#fff" id="varios" name="actividades[]" value="otros"> Otros
                                </div>
                                </br>
                                </br>
                                <input type="text" class = "Requerido form-control" name="descripcion_varios" id="descripcionVarios" placeholder="Actividades Varias"  style="margin-top: 15px; display: none"/> 
                            </div>
                        </div>
                        <div class="row">
                            <br>
                            <button type="submit" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style=" background: #fff204 !important; margin-bottom: 15px; color: #111;" name="guardar" value="Guardar">Registrarse</button>
                        </div>
                    </div>  
                    <div class="col-xs-12 col-sm-2 col-md-2">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
    @include('layouts/scripts')
     <script type="text/javascript" src="{{ URL::asset('css/jquery.ui.autocomplete.scroll.js') }}"></script>
     <script type="text/javascript" src="{{ URL::asset('js/listados.js') }}"></script>
     <script type="text/javascript" src="{{ URL::asset('js/jquery.maskedinput.js') }}"></script>
    <script>
       $("#fechaNacimiento" ).datepicker({
                        dateFormat: 'dd/mm/yy',
                        changeMonth: true,
                        changeYear: true,
                        yearRange: '-90:-0'
            });

       /* $("#fechaNacimiento").mask("99/99/9999");

         $("#fechaNacimiento").focusout(function() {*/



        $("#varios").click(function(){
            if( $(this).is(':checked') ){
                $("#descripcionVarios").show('slow');   
            }else{
                $("#descripcionVarios").hide('slow');     
            }    
          })  

         $('#destinations_name_conocer1').autocomplete({
                        source: function (request, response) {
                          $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                                response($.map(data, function (value, key) {
                                    return {
                                        label: value.desDestino,
                                        value: value.desDestino,
                                        valor: value.idDestino
                                    };
                                }));
                            });
                        },
                        minLength: 3,
                        delay: 100,
                        maxShowItems: 7,
                        select: function( event, ui ) {
                            $('#destino_conocer1').val(ui.item.valor);
                          }
        });

        $('#destinations_name_conocer2').autocomplete({
                        source: function (request, response) {
                          $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                                response($.map(data, function (value, key) {
                                    return {
                                        label: value.desDestino,
                                        value: value.desDestino,
                                        valor: value.idDestino
                                    };
                                }));
                            });
                        },
                        minLength: 3,
                        delay: 100,
                        maxShowItems: 7,
                        select: function( event, ui ) {
                            $('#destino_conocer2').val(ui.item.valor);
                          }
        });

        $('#destinations_name_conocer3').autocomplete({
                        source: function (request, response) {
                          $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                                response($.map(data, function (value, key) {
                                    return {
                                        label: value.desDestino,
                                        value: value.desDestino,
                                        valor: value.idDestino
                                    };
                                }));
                            });
                        },
                        minLength: 3,
                        delay: 100,
                        maxShowItems: 7,
                        select: function( event, ui ) {
                            $('#destino_conocer3').val(ui.item.valor);
                          }
        });


        $('#destinations_name_venta1').autocomplete({
                        source: function (request, response) {
                          $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                                response($.map(data, function (value, key) {
                                    return {
                                        label: value.desDestino,
                                        value: value.desDestino,
                                        valor: value.idDestino
                                    };
                                }));
                            });
                        },
                        minLength: 3,
                        delay: 100,
                        maxShowItems: 7,
                        select: function( event, ui ) {
                            $('#destino_venta1').val(ui.item.valor);
                          }
        });

        $('#destinations_name_venta2').autocomplete({
                        source: function (request, response) {
                          $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                                response($.map(data, function (value, key) {
                                    return {
                                        label: value.desDestino,
                                        value: value.desDestino,
                                        valor: value.idDestino
                                    };
                                }));
                            });
                        },
                        minLength: 3,
                        delay: 100,
                        maxShowItems: 7,
                        select: function( event, ui ) {
                            $('#destino_venta2').val(ui.item.valor);
                          }
        });

        $('#destinations_name_venta3').autocomplete({
                        source: function (request, response) {
                          $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
                                response($.map(data, function (value, key) {
                                    return {
                                        label: value.desDestino,
                                        value: value.desDestino,
                                        valor: value.idDestino
                                    };
                                }));
                            });
                        },
                        minLength: 3,
                        delay: 100,
                        maxShowItems: 7,
                        select: function( event, ui ) {
                            $('#destino_venta3').val(ui.item.valor);
                          }
        });

    </script>
@endsection
