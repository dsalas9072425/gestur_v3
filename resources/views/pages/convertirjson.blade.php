@extends('master')
@section('title', 'Búsqueda de Vuelos')
@section('styles')
	@parent
@endsection

@section('content')
<section id="content" class="gray-area">
    <div class="container">
        <div id="main">
        	<form id="form-convertir" method="get" action="outpdf2.php">
        		<textarea id="in" rows="10" cols="70" name="in"></textarea>
        		<textarea id="out" rows="10" cols="70" name="out"></textarea>
        		<button type="button" id="convertir" class="btn btn-primary" data-dismiss="modal">Guardar</button>
    		</form>
    	</div>
    </div>		
</section>
@endsection

@section('scripts') 
	@include('layouts/scripts')
	<script>
		$(document).ready(function() {
			//Inicio de los Filtros
			$("#convertir").click(function(){
				var dataString = $("#form-convertir").serialize();
				$.ajax({
					type: "POST",
					url: "{{route('doConvertirjson')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){

					}	
				})		
			})
		})		
	</script>			
@endsection