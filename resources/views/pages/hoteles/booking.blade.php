@extends('master')

@section('title', 'Confirmación de Reserva')
@section('styles')
	@parent
@endsection

<?php 
?>
@section('content')
<section class="sectiontop">
		<div class="container">
                <form action="" method="post">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 alert-danger">
							<!-- En caso de error-->
                        </div>
                        @include('flash::message')
                        <div id="main" class="col-sm-12 col-md-12" class="sectiontop">
                            <div class="booking-information travelo-box"> 
                                <h2>Reserva Realizada</h2>
                                <hr />
                                <div class="booking-confirmation clearfix">
                                    <i class="glyphicon glyphicon-thumbs-up icon circle"></i>
                                    <div class="message">
                                        <h4 class="main-message">Muchas gracias por su reserva.</h4>
                                        <p>Se le ha enviado a su cuenta de mail la información detallada.</p>
                                    </div>
                                </div>
                                <hr />
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<h2>Información de la Reserva</h2>
										<dl class="term-descriptioncustom">
											<dt>Codigo de Reserva:</dt><dd>{{$confRsp->codReserva}}</dd>
											<dt>Fecha de Reserva:</dt><dd>{{$confRsp->fechaReserva}}</dd>
											<dt>Pasajero:</dt><dd>{{$confRsp->titular->nombre  . ' ' . $confRsp->titular->apellido }}</dd>
											@if(property_exists($confRsp,'codRetorno'))
												<dt>Estado:</dt><dd>
													@php
													if($confRsp->codRetorno == 0){
														echo '<span class="label alert-success">CONFIRMADA</span>';
													}else{
														echo '<span class="label alert-danger">RECHAZADA</span>';
													}
													@endphp</dd>
											@endif
											</dt><dd></dd>
										</dl>
									</div>
									<div class="col-md-6 col-sm-6">
										<h2>Datos del Alojamiento</h2>
										<dl class="term-descriptioncustom">
											<dt>Alojamiento:</dt><dd>{{$confRsp->nomHotel}}</dd>
											<dt>Dirección:</dt><dd>{{$confRsp->dirHotel}}</dd>
										</dl>
									</div>
								</div>
							</div>
                            <div class="booking-information travelo-box">
								<h2>Servicios</h2>
                                <div class="table-responsive">
									<table class="table">
										<tr>
											<!--<th>Desde</th>
											<th>Hasta</th>-->
											<th>Tipo de habitación</th>
											<th>Adultos</th>
											<th>Niños</th>
											<th>Edades de Niños</th>
											<th>Comentarios de Habitación</th>
											<!--<th>Régimen</th>-->
										</tr>
									@foreach($confRsp->habitaciones as $habitacion)
										@foreach($habitacion->tarifas as $tarifa)
											<tr>
												<!--<td>{{--$confRsp->fechaDesde--}}</td>
												<td>{{--$confRsp->fechaHasta--}}</td>-->
												<td>{{$habitacion->desHabitacion}} - {{$tarifa->desConsumicion}}</td>
												<td>{{$tarifa->ocupanciaDtp->adultos}}</td>
												<td>{{$tarifa->ocupanciaDtp->edadesNinhos == null ? 0 : sizeof($tarifa->ocupanciaDtp->edadesNinhos)}}</td>
												@if($tarifa->ocupanciaDtp->edadesNinhos != null )
													<td>
													@foreach($tarifa->ocupanciaDtp->edadesNinhos as $edad){{$loop->first ? $edad : ', '.$edad}}@endforeach
													</td>
												@else
													<td>0</td>
												@endif
												@if($tarifa->comentarioPasajero != null)
													<td>
													{{$tarifa->comentarioPasajero}}
													</td>
												@else
													<td></td>
												@endif
												
											
												<!--<td>{{$tarifa->codConsumicion}} ({{$tarifa->desConsumicion}})</td>-->
											</tr>
										@endforeach
									@endforeach
									</table>
								</div>
                            </div>
                            <div class="booking-information travelo-box">
                                <h2>Comentarios</h2>
                                <dl class="term-descriptioncustom">
									<p>
									{{$confRsp->infoAdicional}}
									</p>
                                </dl>
                            </div>
                            <div class="booking-information travelo-box">
								<div class="row">
									<div class="col-md-4 col-sm-4">
										<h2>Política de Cancelación</h2>
											<dl class="term-descriptioncustom">
												<dt>Desde:</dt><dd>{{(\DateTime::createFromFormat("Y-m-d" , $confRsp->politicaDtp->desde))->format('d/m/Y')}}</dd>
												<dt>Costo:</dt><dd>{{$confRsp->politicaDtp->monto}} USD</dd>
											</dl>
									</div>
									<div class="col-md-4 col-sm-4">
										<h2>Información Importante</h2>
										<dl class="term-descriptioncustom">
											<!--<dt>Check In:</dt><dd>{{--Session::get('searchData')->datosBusqueda->fechaDesde--}}</dd>
											<dt>Check Out:</dt><dd>{{--Session::get('searchData')->datosBusqueda->fechaHasta--}}</dd>-->
											<dt>Check In:</dt><dd>{{(\DateTime::createFromFormat("Y-m-d" , $fechaDesde))->format('d/m/Y')}}</dd>
											<dt>Check Out:</dt><dd>{{(\DateTime::createFromFormat("Y-m-d" , $fechaHasta))->format('d/m/Y')}}</dd>

										</dl>
										<p>A confirmar según política de cada Hotel.</p>
									</div>
									@if($confRsp->rspFactour)
									<div class="col-md-4 col-sm-4">
										<h2>Liquidación</h2>
											<dl class="term-descriptioncustom">
												<dt>Comisión Agencia:</dt><dd>{{$confRsp->factourComAg}}</dd>
												<dt>Neto:</dt><dd>{{$confRsp->factourNeto}} USD</dd>
												<dt>Bruto:</dt><dd>{{$confRsp->factourBruto}} USD</dd>
												@if($confRsp->nroFactura != null && $confRsp->nroFactura !='')
												<dt>Nro Factura:</dt><dd>{{$confRsp->nroFactura}}</dd>
												@endif
											</dl>
									</div>
									@endif
								</div>							
                            </div>
                        </div>
                    </div>
                </form>
    	</div>
</section>
@endsection

@section('scripts')
    @parent
    <script>
    	$.unblockUI();
    </script>
@endsection
