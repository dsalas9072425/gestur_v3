<html>
	<head>
		<title>dtpmundo.com - Voucher.pdf</title>
		<!-- Main Style -->
		<!--<link rel="stylesheet" type="text/css" href="{{ asset ('css/style.css')}}" >-->
		<style type="text/css">
			body {
			  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			  font-size: 10px;
			  line-height: 1.42857143;
			  color: #555555;
			  background-color: #ffffff;
			}
			#headerPage
			{
				position:fixed;
				top:0;
				left:0;
				padding-bottom:3px;
			}
			.contentLeft
			{
				display:inline-block;
				vertical-align:top;
				width:45%;
			}
			#headerPage .contentLeft img
			{
				max-width:160px;
				max-height:50px;
			}
			#headerPage .contentRight img
			{
				max-width:225px;
				max-height:65px;
			}
			.contentRight
			{
				text-align:right;
				display:inline-block;
				vertical-align:top;
				width:52%;
			}
			#bodyPage
			{
				margin-top:5px;
				/*page-break-before: always;*/
			}
			b{
				color:#1169B0;
			}
			h2{
				font-weight:400;
				color:#2d3e52;
			}
			h3{
				font-weight:500;
				color: #555555;
			}

			.booking-information
			{
				padding:0 10px;
				margin:10px;
			}

			.booking-information hr {
			  margin-top: 20px;
			  margin-bottom: 20px;
			  border-color: #a9a9a9; }

			.table-information table
			{
				width: 100%;
				border-collapse: collapse;
			}

			.table-information table th
			{
				color:#ffffff;
				background-color: #1169B0;
				border:1px solid #1169B0;
				text-transform: uppercase;
				text-align: center;
				padding: 5px;
				font-weight:500;
			}

			.table-information table td
			{
				border:1px solid #a9a9a9;
				padding: 5px;
				text-align: center;
				background-color: #072235;
			}
			b{
				font-weight:400;
			}
			#footerPage
			{
				border-top:1px solid #666;
				padding-top:3px;
				position:fixed;
				bottom:8px;
				left:0;
				/*max-height:60px;*/
			}
			#intermedio{
				width: 350px;
				height: 133px;
			}

			.tablaTitulo{
				font-size: 10px;
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			}

			.intermedio{
				width: 70px;
				padding-left: 15px;
				font-size: 13px;
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				text-align: left;
				font-weight: bold;
			}
			.secundario{
				width: 125px;
				font-size: 13px;
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				text-align: left;
			}
			.tercero{
				width:242px;
				padding-left: 5px;
				font-size: 13px;
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				text-align: left;
			}
			.factura {
				width: 150px;
				padding-left: 15px;
				font-size: 13px;

			}
			.facturasCell{
				width: 115px;
				padding-left: 15px;	
				font-size: 13px;
			}

        </style>
	</head>
<body>
	<header>
		<div class="container">
			<section class="sectiontop">
				<div class="container"> 
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="booking-information travelo-box" style="padding-top: 30px;">
								<table>
									<tr>
										<td><img src="{{asset('images/logo-4.png')}}" width="200"></td>
										<td id="intermedio"><h3  style="font-size: 20px;font-weight: bold; color:#555555;"><b style="font-size: 20px;font-weight: bold; color:#555555;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TODOS GANAN CON DTP!!!</b></h3></td>
										<td><img src="http://img.dtpmundo.com/promocion/{{$datosVoucher[0]->opcion_cambio}}.png" width="80"></td>
									</tr>
								</table>

								<br>
								<div class="row">
									<table class="table-information" style="background-color: #072235; color: #FFF;">
										<tr>
											<td class="intermedio">Agencia: </td>
											<td class="secundario">{{$datosVoucher[0]->razon_social}}</td>
											<td class="tercero">&nbsp;</td>
											<td class="intermedio">VOUCHER </td>
											<td class="secundario"># {{$datosVoucher[0]->id}}</td>
										</tr>
										<tr>
											<td class="intermedio">Vendedor:</td>
											<td class="secundario">{{$datosVoucher[0]->nombre_apellido}}</td>
											<td class="tercero">&nbsp;</td>
											<td class="intermedio">USD: </td>
											<td class="secundario">{{$datosVoucher[0]->monto}}</td>
										</tr>
										<tr>
											<td class="intermedio">Fecha:</td>
											<td class="secundario">{{$datosVoucher[0]->fecha_pedido}}</td>
											<td class="tercero">&nbsp;</td>
											<td class="intermedio">Concepto:</td>
											<td class="secundario">Promo/Incentivo</td>
										</tr>
									</table>
 									<div class="col-sm-12 col-md-12 booking-vuelo-detalle-right" style="border: 1px solid #666;margin-bottom: 5px;">
										<table>
											<tr>
												<td class="factura" style="font-size: 15px;font-weight: bold;">Facturas Canjeadas: </td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<?php
											$contador = 0;
											$facturasArray = explode(",",$datosVoucher[0]->facturas);
											?>
												@foreach($facturasArray as $factura)
													@if($contador ==0)
														<tr><td></td>
													@endif	
														<td class="facturasCell">{{$factura}}</td>
													<?php 
														$contador++
													?>
													@if($contador == 3)
														</tr>
														<?php 
															$contador = 0;
														?>	
													@endif
												@endforeach
										</table>	
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<small style="font-size: 8px;">
											1- El monto de los canjes en efectivo es IVA incluido y se requiere la presentación de una factura legal. El canje puede realizarse en GS o USD tomando el cambio del día DTP.<br>
											2- Una vez generada esta boleta, se deberá descargar y enviar un correo a <b>administracion@dtp.com.py</b> adjuntando el voucher.<br>
											3. El canje generado con empresas asociadas a DTP no requiere una factura legal por parte del vendedor de la agencia. DTP entregará el vale legal en GS tomando el cambio del día DTP.
											4. Desarrollo Turístico Parsguayo es agente de retención y por los montos imponibles se expedirá el documento legal correspondiente. Valor imponible para retención: 812.524 gs
										</small>	
									</div>	
								</div>
								<div class="booking-pasajero">
								</div>

							</div>
							
						</div>
						
					</div>
			                
			    </div>
			</section>
		</div>
	</header>
</body>
</html>