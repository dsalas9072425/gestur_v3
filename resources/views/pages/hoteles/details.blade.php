@extends('master')

@section('title', 'Detalle de Hotel')
@section('styles')
	@parent
@endsection

@section('content')
	<div class="container" style="margin-left: 10%; margin-right: 10%;">
	<br><br><br>
		<h2 class="entry-title">{{--{{$hotel->titulo->nomHotel}} - {{$hotel->zona}}, {{$hotel->ciudad}} --}}</h2>
            <div class="row">

                <div id="main" class="col-md-9">

					<!-- Tabs de Fotos y Mapas -->
                    <div class="tab-container style1 detalle-hotel" id="hotel-main-content">

                        <ul class="tabs">

                            <li class="active"><a data-toggle="tab" href="#photos-tab">fotos</a></li>

                            <li><a data-toggle="tab" href="#map-tab">mapa</a></li>

                        </ul>

                        <div class="tab-content">

                            <div id="photos-tab" class="tab-pane fade in active">

                                <div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">

                                    <ul class="slides">
										
										@if(isset($hotel->imagenHotel))
												
											@foreach($hotel->imagenHotel as $imagen)
												<li><img src="{{$imagen}}"/></li>
											@endforeach
												
										@endif
                                    </ul>
                                </div>

                                <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                                    <ul class="slides">
										@if(isset($hotel->imagenHotel))
											@foreach($hotel->imagenHotel as $imagen)
												<li><img src="{{$imagen}}"/></li>
											@endforeach
										@endif
									</ul>
                                </div>
                            </div>
                            <div id="map-tab" class="tab-pane fade">
                            		<div id="map-tab" style="width:80%;height:300px"></div>
                            </div>
                        </div>
					</div>
					<!-- Tabs de Habitaciones, Descripcion, Comodidades -->
                    <div id="hotel-features" class="tab-container">
                        <ul class="tabs">
							<li @if(true) class="active" @endif><a href="#hotel-description" data-toggle="tab">Descripción</a></li>
                            <li><a href="#hotel-amenities" data-toggle="tab">Comodidades</a></li>
                        </ul>
                        <div class="tab-content detalle-hotel">
							<div @if(true) class="tab-pane fade in active" @endif id="hotel-description">
                                <h2>Sobre {{$hotel->titulo->nomHotel}}</h2>
                                <p>
                                <?php
									echo $hotel->desHotel;
								?>	
                                </p>
								<!--<ul>
									@if(isset($hotel->detalleExtraHotel))
										@foreach($hotel->detalleExtraHotel as $detalle)
											<li class="col-md-4 col-sm-6">{{$detalle->nombre}}</li>
										@endforeach
									@endif
								</ul>-->
                            </div>
                            <div class="tab-pane fade" id="hotel-amenities">
                                <h2>Comodidades / Servicios</h2>
                                <p>Servicios y comodidades incluidos en su alojamiento:</p>
                                <ul class="amenities clearfix style2">
                                	@if(isset($listadoComodidades))
										@foreach($listadoComodidades as $key=>$detalle)
											<li class="col-md-4 col-sm-6"> <div class="icon-box style2"><i class="glyphicon glyphicon-ok-circle"></i>{{$detalle}}</div></li>
										@endforeach
									@endif	
                                </ul>
                            </div>

                        </div>

                    </div>
					
				</div>
				<!--Imagen y datos a la derecha y porque elegir dtp mundo -->
				<div class="sidebar col-md-3">
					<article class="detailed-logo">
						<figure>
							<img width="114" height="85" src="@if(isset($hotel->imagenHotel)) {{$hotel->imagenHotel[0]}}  @endif" alt="">
						</figure>
						<div class="details">
							<h2 class="box-title">
								{{$hotel->titulo->nomHotel}}
								<small>
									<i class="soap-icon-departure yellow-color"></i><span class="fourty-space">{{$hotel->dirHotel[0] or 'dir'}}</span>
									<br>
								</small>
							</h2>

							<div class="feedback clearfix">

								<div data-toggle="tooltip" data-placement="bottom"><span class="review" style="width: 80%; font-size: 1.3em;">{{$hotelInfo->titulo->catHotel or ''}}</span></div>

							</div>
							<p class="description">
								@if(isset($hotel->checkOutTime))
									<strong>Check-in desde</strong><span class="fourty-space">{{$checkOutTime}}</span><br>
								@endif
								@if(isset($hotel->checkInTime))
									<strong>Check-out hasta</strong><span class="fourty-space">{{$checkInTime}}</span><br>
								@endif
							</p>
						</div>
					</article>
					<div class="travelo-box book-with-us-box">
						<h4>Porque Elegir DTPMundo?</h4>
						<ul>
							<li><i class="soap-icon-support circle"></i> <h5 class="title"><a href="#">Hecho en Paraguay</a></h5>
								<p>DTPMundo está desarrollada 100% por un equipo <b>Paraguayo</b>. Como está desarrollado por nosotros, es el sitio mas personalizado y flexible que encontrará en el mercado.</p>
							</li>
							<li>
								<i class="soap-icon-hotel-1 circle"></i> <h5 class="title"><a href="#">200.000+ Hoteles</a></h5>
								<p>DTPMundo actualmente tiene mas de 200.000 hoteles y es el sitio que incorpora hoteles más rapidamente.</p>
							</li>
							<li>
								<i class="soap-icon-savings circle"></i> <h5 class="title"><a href="#">Bajos Precios</a></h5>
								<p>DTPMundo opera directo con los proveedores, esto nos vuelve muy flexibles y sobre todo garantiza <i><b>el mejor precio del mercado.</b></i></p>
							</li>
						</ul>
					</div>
				</div>
            </div>
	</div>
@endsection

@section('scripts')
    @section('scripts')
    @parent
	<!-- load FlexSlider scripts -->
	<script type="text/javascript" src="{{ URL::asset('components/flexslider/jquery.flexslider-min.js') }}"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCu2ZW8F0SdXXa2bLgnIu-vIInMdg_IMQw&libraries=places&callback=initialize" type="text/javascript"></script>
   <script type="text/javascript">	
		var map;
		var panorama;
		var fenway;
		var mapOptions;
		var panoramaOptions;
		var lat = {{ $hotel->coordenadasHotel->latitud}};
		var lon = {{ $hotel->coordenadasHotel->longitud}};

        $(document).ready(function() {
												
			$('img').on("error", function () {
				$(this).parent().remove();		
			});
					
			$("#map-tab").height($("#hotel-main-content").width() * 0.6);
		
			$('a[href="#map-tab"]').on('shown.bs.tab', function (e) {
				
				google.maps.event.trigger(map, "resize");

				map.setCenter(new google.maps.LatLng(lat, lon));
				
			});
				

        });
		
		function initialize(){		
			map = new google.maps.Map(document.getElementById('map-tab'), {
				center: {lat: lat, lng: lon},
				zoom: 15
			});
			var marker = new google.maps.Marker({
			  position: map.getCenter(),
			  map: map,
			  title: <?php echo "'{{$hotel->titulo->nomHotel}}'"; ?>
			});
			var descripcion = '';
			descripcion = '<b>{{$hotel->titulo->nomHotel}}</b><br>{{$hotel->dirHotel[0]}} {{$hotel->ciudad}}<br>Precio desde: <b>{{$moneda}}'

			var infowindow = new google.maps.InfoWindow({
				content: descripcion
			});

			marker.addListener('click', function() {
				infowindow.open(map, marker);
			});				
		}
    </script>

@endsection


