@extends('master')

@section('title', 'Vista en mapas')
@section('styles')
	@parent
@endsection

@section('content')
	<body>
		@include('flash::message')
		<div id="map" style="width:100%;height:600px"></div>
	</body>
@endsection

@section('scripts')
    @parent
	<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCu2ZW8F0SdXXa2bLgnIu-vIInMdg_IMQw&libraries=places&callback=myMap"></script>
	<script>
		var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
	    console.log({!!json_encode($hotels)!!});
		hotels = {!!json_encode($hotels)!!};
		console.log(hotels);
		
		codHotel = '{{$codHotel}}';
		codProveedor = '{{$codProveedor}}';
		//auxCenter = hotelIndex;
		indexSelectedHotel = 0;
		hotels.forEach(function(hotel, index){
			if(hotel.codHotel == codHotel && hotel.codProveedor == codProveedor) indexSelectedHotel = index;
		});
		function myMap(){
			var mapCanvas = $('#map')[0];
			
			var mapOptions = {
				center: {lat: parseFloat(hotels[indexSelectedHotel].latitud), lng: parseFloat(hotels[indexSelectedHotel].longitud)},
				zoom: 17
			}
			
			var map = new google.maps.Map(mapCanvas, mapOptions);
			var prev_infowindow = false;
			var prev_marker = false;
			hotels.forEach(function(hotel,index){

				var marker = new google.maps.Marker({
					position: {lat: parseFloat(hotel.latitud), lng: parseFloat(hotel.longitud)},
					map: map,
					title: hotel.nomHotel,
					icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png' 
				});

				//if(index == hotelIndex){
				if(index == indexSelectedHotel){
					marker.setIcon('./images/puntero.png');
					prev_marker = marker;
				}
				var precioEconomico = parseFloat(hotel.comMasEconomicaHotel);
				var infowindow = new google.maps.InfoWindow({
					content: 
							"<form method='GET' action='{{route('details')}}' target='_blank' class='mapa-resultados'>"+
								"<input type='hidden' name='codHotel' value = '" + hotel.codHotel + "'/>"+
								"<input type='hidden' name='codProveedor' value = '" + hotel.codProveedor + "'/>"+
								"<div onclick= ' $(this).closest(`form`).submit();'><b>"+hotel.nomHotel + ' (' +hotel.catHotel + ")</b><br>" + hotel.dirHotel +"<br><b color='#7fb231'>Precio desde: "+hotel.comMasEconomicaHotel+" "+hotel.moneda+"</b></div></form>"
				});
				marker.addListener('click',function(){
					//cambiar de color el marker anterior (menos la primera vez)
					if(prev_marker){
						prev_marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
					}
					marker.setIcon('./images/puntero.png');
					//cerrar el window anterior (menos la primera vez)
					if(prev_infowindow){
						prev_infowindow.close();
					}
					infowindow.open(map, marker);
					prev_infowindow = infowindow;	
					prev_marker = marker;
					//abrir nuevo infowindow
					
				});
				
				/*
				marker.addListener('click',function(){
					if(prev_marker != marker){
						prev_marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
					}
					if(prev_infowindow){
						prev_infowindow.close();
					}
					prev_infowindow = infowindow;
					marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
					prev_marker = marker;
					infowindow.open(map, marker);
				});
				
				*/
			});
		}
		
		

	</script>
@endsection