@extends('master')

@section('title', 'Búsqueda de hoteles')
@section('styles')
	<link rel="stylesheet" href="{{ URL::asset('css/select2.min.css') }}" media="screen" >
	@parent
	<style>
	.filtro span {
    	color: #666;
	}
	.sort-by-section li {
      		padding: 0px;
      	}
	.select2-container--default .select2-selection--multiple:before {
						content: ' ';display: block;position: absolute;border-color: #888 transparent transparent transparent;border-style: solid;border-width: 5px 4px 0 4px;height: 0;right: 6px;margin-left: -4px;margin-top: -2px;top: 50%;width: 0;cursor: pointer}
	.ui-dialog-titlebar-close{
		    background-color: #00800005;
	}

	</style>
@endsection

@section('content')
	@include('flash::message')
	<div class="container">
		<!-- Modal Habitaciones -->
		<div class="modal fade" id="Habitaciones" role="dialog">
			<div class="modal-dialog">
			  <!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header" style="padding:15px 30px;">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4><span class="glyphicon glyphicon-bed"></span> Agregar Habitaciones</h4>
					</div>
					<div class="modal-body">
						<form role="form" id= "formHabitaciones">
							<button type="button" class="btn btn-primary pull-left" id= "agregarHabitacion" onclick= "objBusqueda.habitaciones.agregar()"><span class="glyphicon glyphicon-plus-sign"></span> Agregar más habitación</button>
							<button type="button" class="btn btn-info pull-right" id= "removerHabitacion" onclick= "objBusqueda.habitaciones.remover()"><span class="glyphicon glyphicon-minus-sign"></span> Eliminar habitación</button>
						</form>
						<div class="clearfix"></div>
					</div>
					<div class="modal-footer">
					  <button type="button"  style="width: 100px; background-color: #e2076a;" onclick= "objBusqueda.crear();" class="btn btn-success btn-default pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Aceptar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div id="main">
			<div class="row">
				<div class="col-md-12 col-sm-12 nueva-busqueda">
					<div class="row">
						<form action="{{route('search')}}" method="get" id="frmBusqueda">
							<div class="col-xs-12 col-sm-4 col-md-4 form-group">
								<label for="destino" >Destino</label>
								<input id="sid" type="hidden" value="{{$sid}}">
								<input id="ocupanciaActual" type="hidden" value="0_0">
								<input id="indicadorOcupancias" type="hidden" value="0">
								<input id="destino" name="destino" type="hidden">
								<input id="destinations_name" class="Requerido form-control" value="{{$destino}}">

							</div>
							<div class="col-xs-6 col-sm-2 col-md-2 form-group">
								<label for="fecha_desde" class = "title">Check In</label>
								<div>
									<input type="text" id="fecha_desde" name="fecha_desde" class="input-text full-width required" value = "" readonly="readonly" />
								</div>
							</div>
							
							<div class="col-xs-6 col-sm-1 col-md-1 form-group">
								<label for="cant_noche" class = "title">Noches</label>
								<div>
									<input type="text" id="cant_noches" name="cant_noches" class="input-text full-width required" value = "" />
								</div>
							</div>
							
							<div class="col-xs-6 col-sm-2 col-md-2 form-group">
								<label for="fecha_hasta" class = "title">Check Out</label>
								<div>
									<input type="text" id="fecha_hasta" name="fecha_hasta" class="input-text full-width required" value = "" readonly="readonly" />
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-2 col-md-2">
								<div class="mod-hab-search" >
									<label><a data-toggle="modal" href="#Habitaciones" data-backdrop="static" class="mod-hab" ><span class="glyphicon glyphicon-edit"></span> Modificar Habitación</a></label>
									<p class="lblHabitaciones" id= "labelBusqueda"></p>
								</div>
							</div>
							<div class="col-xs-12 col-sm-1 col-md-1">
								<br>
								<button type="submit" id="btnSearch" class="btn qsf-search hotels" style="background-color: #e2076a;" tabindex="5">
									<span class="text"><i class="icon-loupe-search"></i>Buscar</span>
								</button>
							</div>
						</form>
					</div>
				</div>
	
		<div class="sh-filtro">
					<a data-toggle="collapse" class="sh-filter" data-target="#filtro-buscador">Modificar Búsqueda <i class="fa fa-angle-down" aria-hidden="true"></i></a>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-3 filtro collapse" id="filtro-buscador" style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
				<br/>
				<div style="background: url({{asset('images/icon/thumbnail.jpeg')}}) no-repeat center center;">
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 form-group">
						</div>
						<div class="col-xs-12 col-sm-10 col-md-8 form-group">
							<br/>
							<span class="count" id="cantResultados" style="font-size: 22px; color: #111"></span>
							<br/>
						</div>	
						<div class="col-xs-12 col-sm-2 col-md-2 form-group">
						</div>

					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 form-group">
						</div>
						<div class="col-xs-12 col-sm-10 col-md-8 form-group">
							<a id="anchorMapa" href="{{route('mapa')}}" target="_blank" style="order-bottom-width: 0px; margin-top: 2px;" class="btn function small show-full-map map-animation animate" >
						<i class="fa fa-map-marker" aria-hidden="true">
						</i> Ver resultados</a>

						</div>	
						<div class="col-xs-12 col-sm-2 col-md-2 form-group">
						</div>
					</div>
				</div>		
				 <br>
				 <form role="form" id= "filtro-formulario">
					<input type="hidden" name="filter" value="yes" />
					<!--<input type="hidden" id="maximoReal"/>
					<input type="hidden" id="minimoReal"/>-->
					<h2 style="font-size: 17px; font-weight: normal;" class="search-results-title filtrotitle"><i class="fa fa-filter" aria-hidden="true"></i> Filtrar búsqueda</h3>
					<div class="row">
						<br>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
						<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Hotel</h5>
							<!--<select data-placeholder="Hotel" class="chosen-select-deselect ui-autocomplete-input" id="hotels" name="hotel">
								<option value="">  <br/></option> 
							</select>-->
							<input class="form-control" id="busquedaHotel" name="hotel">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
						<br>
						<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Precios</h5>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group" style="margin-bottom: 15px;">
							<div id="price-ranges"></div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-5 rangoPrecio">
							<input type="number" class="form-control" id="precio_minimo" name="precio_minimo">
						</div>
						<div class="col-xs-6 col-sm-6 col-md-2 rangoPrecio"></div>
						<div class="col-xs-6 col-sm-6 col-md-5 rangoPrecio">
							<input type="number" class="form-control" id="precio_maximo" name="precio_maximo" align="right">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
							<br>
							<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Estrellas</h5>
							<div id= "estrellasContainer">
								<div class="selectBox" onclick="showCheckboxes()">
									<select class="form-control" data-placeholder="Estrellas" id="estrellas">
										<option>Estrellas</option>
									</select>
									<div class="overSelect">Estrellas</div>
								</div>
								<div id="checkboxes">
									<label><input type="checkbox" name="estrellas[]" value ="-1">Otros</input></label>
									<label><input type="checkbox" name="estrellas[]" value="1" data-cant-estrellas= "1 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i></label>
									<label><input type="checkbox" name="estrellas[]" value="2" data-cant-estrellas= "2 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
									<label><input type="checkbox" name="estrellas[]" value="3" data-cant-estrellas= "3 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
									<label><input type="checkbox" name="estrellas[]" value="4" data-cant-estrellas= "4 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
									<label><input type="checkbox" name="estrellas[]" value="5" data-cant-estrellas= "5 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
								</div>
							</div>
						</div>
						<!-- -->
						<label style="margin-left: 5%;" for="estrellas">Régimen</label>
						<div class="col-xs-12 col-sm-11 col-md-11 form-group" style="margin-bottom: 20px; margin-left: 5%; background-color: white;">
								<div id="checkBoxConsumicion">
								</div>
						</div>
						<!-- -->
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
							<br>
							<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Promociones</h5>
							<div>
								<label class="checkbox-inline">
								  <input class="checkbox-group" name='promociones' type="checkbox" value="true"> Promociones
								</label>
								<br>
								<label class="checkbox-inline">
								  <input class="checkbox-group" name="sugeridos" type="checkbox" value="true"> Promoción Nespresso </input>
								</label>
							</div>
						</div>
						@if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia == config('constants.adminDtp'))
						<label style="margin-left: 5%;" for="estrellas">PROVEEDORES</label>
						<div class="col-xs-12 col-sm-11 col-md-11 form-group" style="margin-bottom: 20px; margin-left: 5%; background-color: white;">
								<div id="checkBoxProveedor">
								</div>
						</div>	
						@endif
					</form>

					<div class="row">
						</br>
					</div>	
					<div class="col-xs-12 col-sm-6 col-md-6 form-group">
							<button id="btnFiltrar" class="btn btn-info full-width" style="background-color: rgb(17, 105, 176);" type="button">Filtrar</button>
							<!--<button id="btnFiltro" type="button">Remover Filtro</button>-->
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 form-group">
							<button id="btnRemover" class="btn btn-info full-width" style="background-color: rgb(17, 105, 176);" type="button">Remover</button>
							<!--<button id="btnFiltro" type="button">Remover Filtro</button>-->
						</div>

					</div>
				</div>
				
				
				<div class="col-xs-12 col-sm-8 col-md-9 resultados" style="padding-right: 0px;">
				
					<div class="progress progress-striped active">
						<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					
					<div id="div_msg">
						
					</div>
					
					<div class="hotel-list listing-style3 hotel" id = "resultadosBusqueda">
							@if(@codRetorno != 0)
								@include('partials/conexionError')
							@endif	
					</div>
					
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 text-center">
							<ul class="pagination">
								<li><a href="#" id ="btnAtras">« Anterior</a></li>
								<li><a href="#" id="btnNext">Siguiente »</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div id="dialog-confirm" title="DTPMundo">
	<br>
	<b style="font-size: x-large;color: #e2076a;" >{{Session::get('desRetorno')}}</b>
</div>
    <div id="requestModal" class="modal fade" role="dialog">
        <!--<form id="frmPaquete" method="post" action="" style="margin-top: 10%;">-->
            <div class="modal-dialog">
        <!-- Modal content-->
                <div class="modal-content" style="height: 20%;width: 90%;">
                    <div class="modal-body" style="padding-bottom: 0px;">
                        <div id="contenido">
				            <div class="booking-details travelo-box detalle-hotel" style="padding-left: 10px;padding-right: 10px;padding-top: 0px;padding-bottom: 0px;">
				                <div class="col-sm-12 col-md-12" id ="nombreHotel" style="background-color: #2d3e52;text-align: center;padding-left: 15px;width: 96%;margin-left: 8px;color:#fff;margin-top: 10px;height: 50%;margin-bottom: 20px;padding-top: 15px;">
				                </div>
				                <article class="image-box hotel listing-style1">
					                    <div class="row">
											<div class="col-sm-12 col-md-12" id="detallesHabitacion" >
											</div>
										</div>	
				                        <div class="row">
				                            	<div class="col-sm-4 col-md-4">
				                                    <label style="text-align: center;"><b>Check in</b></label>
				                                    <span id="desde" style="text-align: center;margin-left: 5%;"></span>
				                                </div>
												<div class="col-sm-4 col-md-4" style="text-align: center;">
				                                    <label><b>Noches</b></label><br>
				                                    <span id="noches"></span>
				                                </div>
				                                <div  class="col-sm-4 col-md-4" style="padding-left: 0px;">
				                                    <label style="text-align: center;"><b>Check out</b></label>
				                                    <span id="hasta" style="text-align: center;margin-left: 5%;"></span>
				                                </div>
				                        </div>
					                    <dl class="other-details">
					                    	<hr style="height: 2px;margin-top: 10px; margin-bottom: 5px; border-color: #111;">
											<dt class="total-price"  style="color:#2d3e52;"><b>Precio Actualizado: </b></dt><dd class="total-price-value" style="color:#e2076a"><b style="font-size: 22px;"> <div id="totalPrecio"></div></b></dd>
					                    </dl>
										<div class="row">
											<div class="col-sm-12 col-md-12" style="background-color: #2d3e52;text-align: center;padding-left: 15px;width: 96%;margin-left: 8px;color:#fff;margin-top: 15px;">
												<b>POLITICA DE CANCELACIÓN</b>
											</div>
										</div>
										<div id="politicaCancelacion"></div>
										<!--<textarea id="filtros" name="filtros"></textarea>-->
				                </article>
				            </div>
                        </div>
                  </div>
                  <div class="modal-footer" style="margin-top: 0px;padding-bottom: 20px;padding-right: 40px;">
                 	<div class="row">
                 		<div class="col-sm-4 col-md-4">
                 		</div>
                 		<div class="col-sm-4 col-md-4">
	                 		<form class="frmConfirm" method="GET" action="{{route('preconfirmation')}}" style="background-color: white;">
								<!--Se le asignan via atributo form los inputs de los checkboxes--> 
								<input type="submit" style="padding-top: 5px; padding-left: 5%;" class="btn btn-info text-center reservarBtn btn transaction normal hide-small normal-button" value="RESERVAR" >
							</form>
                 		</div>
                 		<div class="col-sm-4 col-md-4">
		                    <button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 130px;height: 45px;" data-dismiss="modal">SALIR</button>
                 		</div>
                 	</div>
					
                  </div>
            </div>
        </div>
     <!--</form>-->
    </div>

@endsection

@section('scripts')
    @parent
	<script type="text/javascript" src="{{ URL::asset('js/search/scriptBuscador.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/search/filtro-buscador.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/search/fechasSearch.js') }}"></script>
 	<script type="text/javascript" src="{{ URL::asset('js/select2.min.js')}}"></script>
 	<script type="text/javascript" src="{{ URL::asset('css/jquery.ui.autocomplete.scroll.js') }}"></script>

	<script>
	@if(Session::has('codRetorno') && Session::get('codRetorno') != 0)	
	    $( "#dialog-confirm" ).dialog({
	      resizable: false,
	      height: 250,
	      width: 500,
	      modal: true,
	      buttons: {
	        Ok: function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
	@endif    

	var mapConsumicion = new Object();
	mapConsumicion['RO'] = "Solo Habitación";
	mapConsumicion['BF'] = "Desayuno";
	mapConsumicion['HB'] = "Media pensión";
	mapConsumicion['FB'] = "Pensión completa";
	mapConsumicion['AI'] = "Todo incluido";
	mapConsumicion['OT'] = "Otros";
	var d = new Date();

	var indice = "{{$sid}}";
	if(indice == ""){
		sid = d.getDate()+""+d.getMonth()+""+d.getFullYear()+""+d.getHours()+""+d.getMinutes()+""+d.getSeconds()+""+d.getMilliseconds()+"_"+Math.random().toString(36).substring(5);
		tipo = 1;
	}else{
		sid = indice;
		tipo = 2;
	}

	wFilter = false;
	filterRemoved = false;
	
	acabo = false;
	segurola = false;
	
	maxResultsPerPage = 10;
	currentPage = 0;
	function makeProgress(){
		if(i < 100){
			i = i + 1;
			$(".progress-bar").css("width", i + "%").text(i + " %");
		}
		// Wait for sometime before running this script again
		setTimeout("makeProgress()", 1200);
	}

	$(function(){
		var i = 0;
		makeProgress();	
		$('#anchorMapa').attr("href", $('#anchorMapa').attr("href") + '?sid=' + sid);
		$("#frmBusqueda").submit(function(e){
			e.preventDefault();
			var dataString = $("#frmBusqueda").serialize();
			objBusqueda.habitaciones.storeRoomsContainer();
			dataString += '&ocupancia='+encodeURIComponent(objBusqueda.getHabitaciones());
			window.location = "{{route('search')}}?"+dataString;
		});

		
		$('#btnAtras').hide();
		$('#btnNext').hide();
		

		$('#btnNext, #btnAtras').click(function(e){
			if($(e.target).attr('id') == 'btnNext') currentPage++;
			else currentPage--;
			paginar();
		});
		
		var inputs = {!!  json_encode($inputs) !!};
		ocupancia = {!! $ocupancia !!};
		//console.log(ocupancia);
		adultos = 0;
		contar = 0;
		$.each(ocupancia, function(key,value){
			//console.log(value.adultos);	
			adultos = adultos + value.adultos;
			$.each(value.edadesNinhos, function(key,value){
				contar = contar +1;
			})	
		})
			
		$('#indicadorOcupancias').val(adultos+"_"+contar);

		objBusqueda.habitaciones.setRoomsContainer();
		objBusqueda.crear();
		$.each(inputs, function(key,value){
			if(key=='objBusqueda') return;
			$('#'+key).val(value);
		});
		currentPage=1;
		doSearchs(indice);
		
		$('.checkbox-group').click(function(){
			$(this).prop('value', true);
		});
		
		//al hacer click en filtrar le llama a DoFiltro
		$('#btnFiltrar').click(function(){
			console.log('Iniciar');
			$('#btnAtras').hide();
			wFilter=true;
			currentPage =1;
			doFiltro();
		});

		if($('#busquedaHotel').val() !== ""){
			//console.log('Ingreso');
			wFilter=true;
			currentPage =1;
			doFiltro();

		}

		$('#busquedaHotel').on('keyup', function(){
			$('#btnAtras').hide();
			wFilter=true;
			currentPage =1;
			doFiltro();
		});

		//al hacer click en boton remover, resetea los filtros y realiza una primera busqueda
		$('#btnRemover').click(function(){
			$('#div_msg').html('');
			wFilter = false;
			filterRemoved = true;
			$('#filtro-buscador :input').each(function(){
				if($(this).attr('type') == 'checkbox') 
				{
					if($(this).prop('name') == 'proveedores[]' || $(this).prop('name') == 'consumiciones[]' ) $(this).prop('checked',true);
					else $(this).prop('checked',false);
					
				}
				else if($(this).attr('type') == 'text' || $(this).attr('name') == 'hotel'){ 
					$(this).val(null);
				}	
				
				sliceUpdate($( "#price-ranges" ).slider('option','min'),$( "#price-ranges" ).slider('option','max'));
				
			});
			currentPage = 1;
			$('#btnAtras').hide();			
			paginar();
			
		});
	});

	//inicializa el Proceso de Busquedas
	function doSearchs(indice){
		var dataString = genSearchString();
		if(indice == ""){
			loading();
		}
		
		$.ajax({
			async: true,
			type: "GET",
			url: "{{route('doTotalSearch')}}",
			data: dataString,
			timeout: 70000,
			success: function(rspTotal){
			},
			complete: function(){
				acabo = true;
			}
		});
		getCurrentHotels(indice);
		
	}
	
	function getCurrentHotels(indice){
		$.ajax({
			type: "GET",
			url: "{{route('getCurrentHotels')}}",
			dataType: 'json',
			data: 'sid=' + sid + (segurola ? '&segurola=true' : ''),
			timeout: 90000,
			success: handleSearch,
			error: function(jqXHR,textStatus,errorThrown){
			},
			complete: function(){
				setTimeout($.unblockUI, 7000); 
				if($.active <=1 && $('#resultadosBusqueda').children().length > 0){
					$('.btnPreReserva').removeAttr('disabled');			
				}		
				if(acabo){
					//si ya se hizo el pedido para asegurar segurola = true, sino false
					if(segurola){
						$(".progress").remove();
						
						if($('#resultadosBusqueda').children().length ==0 ){
							}
					}
					else{
						segurola = true;
						getCurrentHotels();
					}
				}
				else{
					getCurrentHotels();
				}
			}
		});
		var url = "{{Session::get('urlFull')}}&sid="+sid;
        
       ;

		history.pushState(null, "", url.replace(/amp;/g,""));
	}

	function handleSearch(rsp){

			currentPage=1;
			updateHotelViews(rsp.hotelViews);
			updateButtons(rsp);
			updateFilter(rsp);
			updateNumberOfResults(rsp.cantResults);
		//}
	}
	
	function paginar(){
		var dataString = genSearchString();
		$.ajax({
			//async: false,
			type: "GET",
			url: "{{route('paginar')}}",
			dataType: 'json',
			data: dataString,
			success: handlePaginar,
			error: function(jqXHR,textStatus,errorThrown){
				//ajaxGlobalHandler(jqXHR,textStatus,errorThrown);
			},
			complete: function(){
				//if($.active <= 1) $.unblockUI();
				//$.unblockUI();
				
				if(filterRemoved) filterRemoved = false;
				
			}
		});
	}
	
	
	function handlePaginar(rsp){
		if(rsp.codRetorno != 0){
			//error
		}else if(rsp.hotelViews.length <= 0){
			//no hay resultados en session para busqueda
		}
		else{
			updateHotelViews(rsp.hotelViews);
			updateButtons(rsp);
			if(filterRemoved){
				updateNumberOfResults(rsp.cantResults);
			}
			//
		}
	}
	
	function doFiltro(){
		$("#ocupanciaActual").val('0_0');
		wFilter=true;
		var dataString = $("#filtro-formulario").serialize();
		dataString += '&' + genSearchString();
		//console.log("{{route('doFiltro')}}?"+dataString);
		$.ajax({
			type: "GET",
			url: "{{route('doFiltro')}}",
			dataType: 'json',
			data: dataString,
			success: handleFiltro,
			error: function(jqXHR,textStatus,errorThrown){
				//console.log(errorThrown);
				//ajaxGlobalHandler(jqXHR,textStatus,errorThrown);
			},
			complete: function(){
				$.unblockUI();
			}
		});
	}
	
	function handleFiltro(rsp){
		if(rsp.codRetorno != 0){
			msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i> No existen resultados con esos criterios</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
			mostrarMensaje(msg);			
		}else if(rsp.hotelViews.length <= 0){
			//msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i> No existen resultados con esos criterios</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
			//mostrarMensaje(msg);
			//alert('No existen resultados con esos criterios');
			//console.log('No existen resultados con esos criterios');
			$('#div_msg').html("<h1 style='color:#e2076a; background-color:white; width: 400px; margin-left:240px;'>NO SE ENCONTRARON RESULTADOS CON ESOS CRITERIOS</h1>");
			$('#div_msg').css('margin-top','15px');
		}
		else{
			$('#div_msg').html("");
			updateHotelViews(rsp.hotelViews);
			updateButtons(rsp);
			updateNumberOfResults(rsp.cantResults);
		}
	}
	
	function handleResponse(rsp){
		//en caso de error en alguna respuesta, se cancela todo
		//console.log(rsp.hotelViews);
		return;
		if(rsp.codRetorno != 0){	
			msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i>'+rsp.desRetorno+'</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
			mostrarMensaje(msg);
			return;
		}
		else if(rsp.checkedForMore){
			currentPage =1;
			//$('#btnAtras').hide();
			if(rsp.newResults) console.log('#CHECK FOR MORE TRAJO NEW RESULTS');
			else console.log('#CHECK FOR MORE FUNCIONO PERO NO TRAJO RESULTS');
		}
		if(rsp.hotelViews.length>0){
			$('#resultadosBusqueda').html('');
			appendHotelViews(rsp.hotelViews);
			updateElements(rsp);
			
		}else{
			updateNumberOfResults(rsp.cantResults);
			//No Results
			var msg = "";
			$('#btnNext').hide();
			if(currentPage==1){
				$.unblockUI();
				msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se han encontrado resultados</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
				mostrarMensaje(msg);
				
				$('#resultadosBusqueda').html('');
			}
			else{
				var msg = "";
				msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No hay mas resultados para mostrar</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
				mostrarMensaje(msg);			
				currentPage--;
				if(currentPage == 1){
					$('#btnAtras').hide();
					$('#btnNext').hide();
				}
			}					
		}
	}
	
	function genSearchString(){
		var dataString = $("#frmBusqueda").serialize();
		dataString += '&ocupancia='+encodeURIComponent(objBusqueda.getHabitaciones());
		tOccupancy = objBusqueda.getFullOccupancy();
		dataString += '&trooms='+tOccupancy.habitaciones;
		dataString += '&tadults='+tOccupancy.adultos;
		dataString += '&tchild='+tOccupancy.ninhos;
		dataString += '&from='+(currentPage-1)*maxResultsPerPage;
		dataString += '&to='+((currentPage)*maxResultsPerPage-1);
		dataString += '&sid='+sid;
		dataString += '&recarga='+tipo
		if(wFilter){
			dataString+='&filter=true';
		}
		
		codDestino = $('#destino').val();
		fechaDesde = $('#fecha_desde').val();
		fechaHasta = $('#fecha_hasta').val();
		cantAdultos = objBusqueda.habitaciones.occupancy.adultos;
		cantHabitaciones = ocupancia.length;
		
		return dataString;
		
	}
	
	//checkea por mas y adicionalmente appendea si hace falta
	function checkForMore(){
		$.ajax({
			type: "GET",
			url:"{{'checkForMore'}}",
			dataType: 'json',
			data: true,
			success: handleResponse,
			error: function(jqXHR,textStatus,errorThrown){
			},
			complete: function(){
				$(".progress").remove();
			}
		});
	}

	function updateButtons(rsp){
		if(rsp.hotelViews.length != maxResultsPerPage){
			$('#btnNext').hide();
		}else{
			$('#btnNext').show();
		}
		if(currentPage == 1){
			$('#btnAtras').hide();
		}else{
			$('#btnAtras').show();
		}
		
	}
	
	function updateFilter(rsp){
		//console.log(rsp);
		if(rsp.minimo != undefined){
			sliceUpdate(rsp.minimo, rsp.maximo);
		}
		//$('#chechboxProveedor').empty();
		
		//$.each(rsp.webProviders,function(index,webProvider){
		$.each(rsp.providers,function(idProvider,results){
			if(results == -1) results = 0;
			
			if($('#checkBoxProveedor').children('label[id="lblProv' + idProvider + '"]').length ==1){
				$('#checkBoxProveedor').find('label[id="lblProv' + idProvider + '"]').each(function(){
					//disablear/enablear en update
					if(results != 0){
						$(this).children('span').html(`&nbsp&nbsp&nbsp&nbsp(` + results + `)`);
						$(this).children('input').prop('disabled',false);
						$(this).children('input').prop('checked',true);
					}
					else{
						$(this).children('input').prop('disabled',true);
						$(this).children('input').prop('checked',false);
					}
				});
			}else{
				$('#checkBoxProveedor').append(`<label style="color: #1169B0;" id="lblProv` + idProvider+  `"class="checkbox-inline"><input name="proveedores[]" type="checkbox" value="`
				+idProvider+`"` + (results == 0 ? `disabled` : `checked`) +  `><img alt="" style="width: 80px; height:19px; background-color: white;" src="images/proveedores/`+idProvider+
				`.png"></img><span>&nbsp&nbsp&nbsp&nbsp(`+results+`)</span></br></label>`);
			}
			
		});
		
		$.each(rsp.codConsumiciones, function(index,codConsumicion){
			//console.log(mapConsumicion);
			//console.log(codConsumicion);
			if(!$('#checkBoxConsumicion').children('label[id="lblCons' + codConsumicion + '"]').length == 1){
				$('#checkBoxConsumicion').append(`<label id="lblCons` + codConsumicion + `"style="color: #1169B0;" class="checkbox-inline"><input name="consumiciones[]" type="checkbox" checked value="` + codConsumicion +`"><span>&nbsp&nbsp&nbsp` + mapConsumicion[codConsumicion] + `</span></input></label><br>`);		
			}
		});
		
		/*
		$.each(rsp.resultsPerProvider,function(provId, results){
			$('#chechboxProveedor').append('<label style="color: #1169B0;" class="checkbox-inline"><input name="proveedor[]" type="checkbox" checked value="'+provId+'"><img alt="" style="width: 80px; height:19px; background-color: white;" src="images/proveedores/'+provId+'.png">&nbsp&nbsp&nbsp&nbsp('+results+')</br></label>');
		});
		*/
	}
	
	function updateNumberOfResults(n){
		if(n != undefined){
			$("#cantResultados").html('<b>'+n+" resultados encontrados<b>");
		}
	}
	
	function updateHotelViews(hotelViews){
		/*//viejo
		for(k=$("#resultadosBusqueda > div").length; k<hotelViews.length ; k++){
			$('#resultadosBusqueda').append(hotelViews[k]);
		}
		*/
		$('#resultadosBusqueda').html('');
		for(k=0; k<hotelViews.length ; k++){
			$('#resultadosBusqueda').append(hotelViews[k]);
		}
		//falta llamar esto solo con los nuevos
		$('.frmConfirm').each(function(){
			$(this).submit(function(e){
				var domForm = this;
				var submitedForm = $(domForm);
				procesando();
				var submitForm = false;
				var redirect = false;
				//submitForm = true;
				e.preventDefault();
				$.each(submitedForm.serializeArray(), function(i,field) {
					if(field.name == 'rates[]'){
						submitForm = true;
						return;
					}
				});
				if(!submitForm){
					msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i> Seleccione al menos una habitacion</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
					mostrarMensaje(msg);
					$.unblockUI();
					e.preventDefault();
					return;
				}else{
					var serializedForm = submitedForm.serialize()+'&sid='+sid;
					$.ajax({
						type: "GET",
						url: "{{route('checkForPreReserva')}}",
						async: false,
						//dataType: 'json',
						data: serializedForm,
						success: function(allow){
							//alert(allow);
							redirect = allow;
							//console.log(allow);
							//alert(allow);
						},
						error: function(jqXHR,textStatus,errorThrown){
							//console.log(errorThrown);
							//ajaxGlobalHandler(jqXHR,textStatus,errorThrown);
						},
						complete: function(){
							if(redirect=='true'){
								submitedForm.append('<input type="hidden" name="sid" value="'+  sid + '">');
								domForm.submit();
							}
							$.unblockUI();
						}
					});
				}			
			});
		});

		$('.rate').click(function(){
			if($(this).is(':checked') ) {
    				//console.log('Seleccionado');
					anterior =  $('#ocupanciaActual').val();
					indicador = anterior.split("_");
					adultoAnterior = indicador[0];
					ninhoAnterior = indicador[1];
					actual = $(this).attr('data');
					indicadorActual = actual.split("-");
					adultoActual = indicadorActual[0];
					ninhoActual = indicadorActual[1];
					adutloTotal = parseInt(adultoAnterior) + parseInt(adultoActual);
					ninhoTotal = parseInt(ninhoAnterior) + parseInt(ninhoActual);
					total = $('#indicadorOcupancias').val();
					totales = total.split("_");
					totalAdulto = parseInt(totales[0]);
					totalNinho =parseInt(totales[1]);
					if(totalAdulto >= adutloTotal){
						if(totalNinho >= ninhoTotal){
							$('#ocupanciaActual').val(adutloTotal+"_"+ninhoTotal);
						}else{
							$("#dialog-confirm").html('La cantidad de niños es mayor a la ocupancia');
							$("#dialog-confirm" ).dialog({
														resizable: false,
														height: "auto",
														width: 400,
														modal: true,
														buttons: {
																Ok: function() {
																	          $( this ).dialog( "close" );
																	        }
																	      }
														});

							$(this).attr('checked', false);
						}	
					}else{
						$("#dialog-confirm").html('La cantidad de adultos es mayor a la ocupancia');
						$("#dialog-confirm" ).dialog({
													resizable: false,
													height: "auto",
													width: 400,
													modal: true,
													buttons: {
															Ok: function() {
																          $( this ).dialog( "close" );
																        }
																      }
													});
						$(this).attr('checked', false);
					}
			}else{
    				//console.log('DeSeleccionado');
					anterior =  $('#ocupanciaActual').val();
					indicador = anterior.split("_");
					adultoAnterior = indicador[0];
					ninhoAnterior = indicador[1];
					actual = $(this).attr('data');
					indicadorActual = actual.split("-");
					adultoActual = indicadorActual[0];
					ninhoActual = indicadorActual[1];
					adutloTotal = parseInt(adultoAnterior) - parseInt(adultoActual);
					ninhoTotal = parseInt(ninhoAnterior) - parseInt(ninhoActual);
					$('#ocupanciaActual').val(adutloTotal+"_"+ninhoTotal);
			}
		})
			
		$('.btnPreReserva').click(function(){
			procesar();
			ocupancia = {!! $ocupancia !!};
			//console.log(ocupancia);
			dataString = "";
			var values =  new Array();
			contador = 0;
			$.each($("."+$(this).attr('id')+" .rate"), function(){
				if($(this).prop('checked')){
					contador = contador+1;
					values.push([$(this).val()]);
				}
			});

			//console.log(values);
			if(contador > 0){
				dataString += '&habitaciones='+values;
				dataString += '&sid='+sid;
				$.ajax({
						type: "GET",
						url: "{{route('getPreconfirmation')}}",
						dataType: 'json',
						data: dataString,
						success: function(data){
							//console.log(data);
							if(data.codRetorno == 0){
								$('#filtros').val();
								$(".frmConfirm").attr("id", "frm"+data.hotel.identifier);
								$('#nombreHotel').html('<b style="font-size: large;">'+data.hotel.nomHotel+'</b>');
								var habitaciones = "";
								$.each(data.hotel.habitaciones, function (key, item){
									//console.log(item);
									key = key+1;
										var contadorNinhos = 0;
										edad = "";
										$.each(item.tarifas[0].ocupanciaDtp.edadesNinhos, function (key, item){
											if(contadorNinhos == 0){
												edad += item;	
											}else{
												edad += ", "+item;
											}
											contadorNinhos =  contadorNinhos+1;
										})
										
									habitaciones += "<small><b>Habitación "+key+": </b></small>";
									habitaciones += "<small>"+item.desHabitacion+"  - "+item.tarifas[0].codConsumicion+" ("+item.tarifas[0].desConsumicion+") </small>";
									habitaciones += "<br>";
									habitaciones += "<small><b>Ocupancia: </b></small>";
									habitaciones += "<small>"+item.tarifas[0].ocupanciaDtp.adultos+" Adultos";
									if(contadorNinhos != 0){
									 	habitaciones += "- "+contadorNinhos+" Niños de: "+edad+" años";
									}
									habitaciones += "</small>";
									habitaciones += "<br>";
									habitaciones += "<small><b>Precio Actualizado: </b></small>";
									habitaciones += "<small>"+item.tarifas[0].precio+" USD</small>";
									habitaciones += "<br>";
									habitaciones += "<hr style='height: 2px;margin-top: 10px; margin-bottom: 5px; border-color: #ccc;'>";
								})
								$('#detallesHabitacion').html(habitaciones);
								$('#totalPrecio').html(data.totalReserva+"  USD");

								fechaDesde = data.fechaDesde.split("/");
								fechaHasta = data.fechaHasta.split("/");
								fecDesde = fechaDesde[1]+"/"+fechaDesde[0]+"/"+fechaDesde[2];
								fecHasta = fechaHasta[1]+"/"+fechaHasta[0]+"/"+fechaHasta[2];

								$('#desde').html(fecDesde);
								$('#hasta').html(fecHasta);
								nombres = restaFechas(fecDesde,fecHasta);
								$('#noches').html('<b>'+nombres+'</b>');

								var politicaCancelacion = "";

								fechaPolitica = data.politicaDtp.desde.split("-");

								var f = new Date();
								fechaCompleta = f.getFullYear()+"-"+(f.getMonth() +1) + "-"+f.getDate();

								fecPolitica = fechaPolitica[2]+"/"+fechaPolitica[1]+"/"+fechaPolitica[0];

								console.log(data.politicaDtp.desde);
								console.log(Date.parse(data.politicaDtp.desde));
								console.log(fechaCompleta);
								console.log(Date.parse(fechaCompleta));

								if((Date.parse(data.politicaDtp.desde)) <= (Date.parse(fechaCompleta))){
									politicaCancelacion += '<div class="row alert alert-danger" style="width: 96%;margin-left: 8px;margin-bottom: 0px;margin-top: 10px; padding-left: 10px;padding-right: 10px;">';
						        }else{
									politicaCancelacion += '<div class="row alert alert-info" style="width: 96%;margin-left: 8px;margin-bottom: 0px;margin-top: 10px; padding-left: 10px;padding-right: 10px;">';
						        }
						        politicaCancelacion += '<div class="col-sm-12 col-md-12" style="height: 40px; text-align: center;">';
						        
						        politicaCancelacion += '<i class="fa fa-exclamation-triangle fa-5 fa-lg" aria-hidden="true"></i></div>';
						                        	
						        politicaCancelacion += '<div class="col-sm-12 col-md-6" style="padding-left: 0px;padding-right: 0px; margin-bottom: 0px;">';

						        politicaCancelacion += '<small><b>A Partir de: </b></small></div>';
						                                
						        politicaCancelacion += '<div class="col-sm-12 col-md-6" style="text-align: center;">';    
						        politicaCancelacion += '<small>'+fecPolitica+'</small></div>';
						                            
						        politicaCancelacion += '<div class="col-sm-12 col-md-6"  style="padding-left: 0px;padding-right: 0px;">';
						        
						        politicaCancelacion += '<small"><b>Monto</b></small></div>';
						                                
						        politicaCancelacion += '<div class="col-sm-12 col-md-6" style="text-align: center;">';
						                                
						        politicaCancelacion += '<small>'+data.politicaDtp.monto+' USD</small></div></div>';

						        $('#politicaCancelacion').html(politicaCancelacion);
						        $.unblockUI();
						        $('#requestModal').modal({
												        show: 'true'
												    }); 

						    }else{
						    	$.unblockUI();
						    	$('#btnCancelarVSTour').trigger("click");
						    	$("#dialog-confirm").html('LO SENTIMOS, LA ÚLTIMA HABITACIÓN FUE TOMADA. INTENTE CON OTRA OPCIÓN.');
						    	$("#dialog-confirm" ).dialog({
														      resizable: false,
														      height: "auto",
														      width: 400,
														      modal: true,
														      buttons: {
														        Ok: function() {
														          $( this ).dialog( "close" );
														        }
														      }
													    	});
							    }
						}
				});			
			}else{
				$.unblockUI();
				$("#dialog-confirm").html('Seleccione una Habitacion');
				$("#dialog-confirm" ).dialog({
											resizable: false,
											height: "auto",
											width: 400,
											modal: true,
											buttons: {
													Ok: function() {
														          $( this ).dialog( "close" );
														        }
														      }
											});
			}
		});	

		$('.detalle').each(function(){
			$(this).attr('data-target',jqid($(this).data('target')));
		});

		if($.active <=1 && $('#resultadosBusqueda').children().length > 0){
			$('.btnPreReserva').removeAttr('disabled');			
		}

		$('.detalle').click(function(){
			id = $(this).attr('data-target')
			//console.log(id);
			$.each($(".rate"), function(){
					if($(this).prop('checked')){
						anterior =  $('#ocupanciaActual').val();
						indicador = anterior.split("_");
						adultoAnterior = indicador[0];
						ninhoAnterior = indicador[1];
						actual = $(this).attr('data');
						indicadorActual = actual.split("-");
						adultoActual = indicadorActual[0];
						ninhoActual = indicadorActual[1];
						adutloTotal = parseInt(adultoAnterior) - parseInt(adultoActual);
						ninhoTotal = parseInt(ninhoAnterior) - parseInt(ninhoActual);
						$('#ocupanciaActual').val(adutloTotal+"_"+ninhoTotal);
						$(this).attr('checked', false);
					}
			});
			$.each($(".listaresultado"), function(){
				var comparador = "";
				var comparador = "#"+$(this).attr('id');
				if(comparador == id){
				}else{
					$(this).removeClass("in")
					}
			})
			$(id).focus();
		});
		
	}


	function sliceUpdate(minimo, maximo){
		$( "#price-ranges" ).slider({
			range: true,
			min: minimo,
			max: maximo,
			values: [ minimo, maximo], 
			slide: function( event, ui ) {
				$( "#precio_minimo" ).val( ui.values[ 0 ] );
				$( "#precio_maximo" ).val( ui.values[ 1 ] );
			}
		});
	    $( "#precio_minimo" ).val($( "#price-ranges" ).slider( "values", 0 ) );
	    $( "#precio_maximo" ).val($( "#price-ranges" ).slider( "values", 1 ) );
	}

	function cantidad(id){
		$("#cantResultados").empty();
		$("#cantResultados").html('<b>'+id+" resultados encontrados<b>");
	}	

	function jqid (id) {
		return (!id) ? null : '#' + id.replace(/(:|\.|\[|\]|,|=|@)/g, '\\$1');
	}
	function getParameterByName(name,url) {
		var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
		return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
	}

	$('#destinations_name').autocomplete({
		    source: function (request, response) {
	          $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
		            response($.map(data, function (value, key) {
		                return {
		                    label: value.desDestino,
		                    value: value.desDestino,
		                    valor: value.idDestino
		                };
		            }));
		        });
		    },
		    minLength: 3,
		    delay: 100,
		    maxShowItems: 7,
		    select: function( event, ui ) {
				$('#destino').val(ui.item.valor);
		      }
		});

	function randomNumber() {
		var d = new Date();
    	return d.getDate()+""+d.getMonth()+""+d.getFullYear()+""+d.getHours()+""+d.getMinutes()+""+d.getSeconds()+""+d.getMilliseconds();
	}
	
	function mensaje(event){
		$mensaje  = "{{Session::get('desRetorno')}}";
		$.blockUI({
		        centerY: 0,
		        message: '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i>'+$mensaje+'</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>',
		        css: {
		            	color: '#000'
		             }
		        });
			}

	// Función para calcular los días transcurridos entre dos fechas
	restaFechas = function(f1,f2)
	 {
		 var aFecha1 = f1.split('/');
		 var aFecha2 = f2.split('/');
		 var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
		 var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);
		 var dif = fFecha2 - fFecha1;
		 var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
		 return dias;
	 }
	</script>
	@endsection
