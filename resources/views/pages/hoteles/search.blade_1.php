@extends('master')

@section('title', 'Búsqueda de hoteles')
@section('styles')
	<link rel="stylesheet" href="{{ URL::asset('css/select2.min.css') }}" media="screen" >
	@parent
	<style>
	.filtro span {
    	color: #666;
	}
	.sort-by-section li {
      		padding: 0px;
      	}
	.select2-container--default .select2-selection--multiple:before {
						content: ' ';display: block;position: absolute;border-color: #888 transparent transparent transparent;border-style: solid;border-width: 5px 4px 0 4px;height: 0;right: 6px;margin-left: -4px;margin-top: -2px;top: 50%;width: 0;cursor: pointer}

	</style>
@endsection

@section('content')
	@include('flash::message')
	<div class="container">
		<!-- Modal Habitaciones -->
		<div class="modal fade" id="Habitaciones" role="dialog">
			<div class="modal-dialog">
			  <!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header" style="padding:15px 30px;">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4><span class="glyphicon glyphicon-bed"></span> Agregar Habitaciones</h4>
					</div>
					<div class="modal-body">
						<form role="form" id= "formHabitaciones">
							<button type="button" class="btn btn-primary pull-left" id= "agregarHabitacion" onclick= "objBusqueda.habitaciones.agregar()"><span class="glyphicon glyphicon-plus-sign"></span> Agregar más habitación</button>
							<button type="button" class="btn btn-info pull-right" id= "removerHabitacion" onclick= "objBusqueda.habitaciones.remover()"><span class="glyphicon glyphicon-minus-sign"></span> Eliminar habitación</button>
						</form>
						<div class="clearfix"></div>
					</div>
					<div class="modal-footer">
					  <button type="button"  style="width: 100px; background-color: #e2076a;" onclick= "objBusqueda.crear();" class="btn btn-success btn-default pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Aceptar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div id="main">
			<div class="row">
				<div class="col-md-12 col-sm-12 nueva-busqueda">
					<div class="row">
						<form action="{{route('search')}}" method="get" id="frmBusqueda">
							<div class="col-xs-12 col-sm-4 col-md-4 form-group">
								<label for="destino" >Destino</label>
								<!--<input id="proveedor" type="hidden" value="{{--Session::get('codProveedor')--}}">-->
								<input id="ocupanciaActual" type="hidden" value="0_0">
								<input id="indicadorOcupancias" type="hidden" value="0">
								<input id="destino" name="destino" type="hidden">
								<input id="destinations_name" class="Requerido form-control" value="{{$destino}}">

							</div>
							<div class="col-xs-6 col-sm-2 col-md-2 form-group">
								<label for="fecha_desde" class = "title">Check In</label>
								<div>
									<input type="text" id="fecha_desde" name="fecha_desde" class="input-text full-width required" value = "" readonly="readonly" />
								</div>
							</div>
							
							<div class="col-xs-6 col-sm-1 col-md-1 form-group">
								<label for="cant_noche" class = "title">Noches</label>
								<div>
									<input type="text" id="cant_noches" name="cant_noches" class="input-text full-width required" value = "" />
								</div>
							</div>
							
							<div class="col-xs-6 col-sm-2 col-md-2 form-group">
								<label for="fecha_hasta" class = "title">Check Out</label>
								<div>
									<input type="text" id="fecha_hasta" name="fecha_hasta" class="input-text full-width required" value = "" readonly="readonly" />
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-2 col-md-2">
								<div class="mod-hab-search" >
									<label><a data-toggle="modal" href="#Habitaciones" data-backdrop="static" class="mod-hab" ><span class="glyphicon glyphicon-edit"></span> Modificar Habitación</a></label>
									<p class="lblHabitaciones" id= "labelBusqueda"></p>
								</div>
							</div>
							<div class="col-xs-12 col-sm-1 col-md-1">
								<br>
								<button type="submit" id="btnSearch" class="btn qsf-search hotels" style="background-color: #e2076a;" tabindex="5">
									<span class="text"><i class="icon-loupe-search"></i>Buscar</span>
								</button>
							</div>
						</form>
					</div>
				</div>
	
		<div class="sh-filtro">
					<a data-toggle="collapse" class="sh-filter" data-target="#filtro-buscador">Modificar Búsqueda <i class="fa fa-angle-down" aria-hidden="true"></i></a>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-3 filtro collapse" id="filtro-buscador" style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
				<br/>
				<div style="background: url({{asset('images/icon/thumbnail.jpeg')}}) no-repeat center center;">
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 form-group">
						</div>
						<div class="col-xs-12 col-sm-10 col-md-8 form-group">
							<br/>
							<span class="count" id="cantResultados" style="font-size: 22px; color: #111"></span>
							<br/>
						</div>	
						<div class="col-xs-12 col-sm-2 col-md-2 form-group">
						</div>

					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-2 col-md-2 form-group">
						</div>
						<div class="col-xs-12 col-sm-10 col-md-8 form-group">
							<a id="anchorMapa" href="{{route('mapa')}}" target="_blank" style="border-bottom-width: 0px; margin-top: 2px;
" class="btn function small show-full-map map-animation animate" >
						<i class="fa fa-map-marker" aria-hidden="true">
						</i> Ver resultados</a>

						</div>	
						<div class="col-xs-12 col-sm-2 col-md-2 form-group">
						</div>
					</div>
				</div>		
				 <br>
				 <form role="form" id= "filtro-formulario">
					<input type="hidden" name="filter" value="yes" />
					<!--<input type="hidden" id="maximoReal"/>
					<input type="hidden" id="minimoReal"/>-->
					<h2 style="font-size: 17px; font-weight: normal;" class="search-results-title filtrotitle"><i class="fa fa-filter" aria-hidden="true"></i> Filtrar búsqueda</h3>
					<div class="row">
						<br>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
						<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Hotel</h5>
							<!--<select data-placeholder="Hotel" class="chosen-select-deselect ui-autocomplete-input" id="hotels" name="hotel">
								<option value="">  <br/></option> 
							</select>-->
							<input class="form-control" id="busquedaHotel" name="hotel">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
						<br>
						<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Precios</h5>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group" style="margin-bottom: 15px;">
							<div id="price-ranges"></div>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-5 rangoPrecio">
							<input type="number" class="form-control" id="precio_minimo" name="precio_minimo">
						</div>
						<div class="col-xs-6 col-sm-6 col-md-2 rangoPrecio"></div>
						<div class="col-xs-6 col-sm-6 col-md-5 rangoPrecio">
							<input type="number" class="form-control" id="precio_maximo" name="precio_maximo" align="right">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
							<br>
							<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Estrellas</h5>
							<div id= "estrellasContainer">
								<div class="selectBox" onclick="showCheckboxes()">
									<select class="form-control" data-placeholder="Estrellas" id="estrellas">
										<option>Estrellas</option>
									</select>
									<div class="overSelect">Estrellas</div>
								</div>
								<div id="checkboxes">
									<label><input type="checkbox" name="estrellas[]" value ="-1">Otros</input></label>
									<label><input type="checkbox" name="estrellas[]" value="1" data-cant-estrellas= "1 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i></label>
									<label><input type="checkbox" name="estrellas[]" value="2" data-cant-estrellas= "2 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
									<label><input type="checkbox" name="estrellas[]" value="3" data-cant-estrellas= "3 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
									<label><input type="checkbox" name="estrellas[]" value="4" data-cant-estrellas= "4 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
									<label><input type="checkbox" name="estrellas[]" value="5" data-cant-estrellas= "5 ESTRELLAS" class= "cantidadEstrellas"/> <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></label>
								</div>
							</div>
						</div>
						<!-- -->
						<label style="margin-left: 5%;" for="estrellas">Régimen</label>
						<div class="col-xs-12 col-sm-11 col-md-11 form-group" style="margin-bottom: 20px; margin-left: 5%; background-color: white;">
								<div id="checkBoxConsumicion">
								<!--
									<label style="color: #1169B0;" class="checkbox-inline"><input name="consumiciones[]" type="checkbox" checked value="RO"><span>&nbsp&nbsp&nbsp Room Only</span></input></label>
									<label style="color: #1169B0;" class="checkbox-inline"><input name="consumiciones[]" type="checkbox" checked value="BF"><span>&nbsp&nbsp&nbsp Breakfast</span></input></label>
									<label style="color: #1169B0;" class="checkbox-inline"><input name="consumiciones[]" type="checkbox" checked value="HB"><span>&nbsp&nbsp&nbsp Half Board</span></input></label>
									<label style="color: #1169B0;" class="checkbox-inline"><input name="consumiciones[]" type="checkbox" checked value="FB"><span>&nbsp&nbsp&nbsp Full Board</span></input></label>
									<label style="color: #1169B0;" class="checkbox-inline"><input name="consumiciones[]" type="checkbox" checked value="AI"><span>&nbsp&nbsp&nbsp All Inclusive</span></input></label>
									<label style="color: #1169B0;" class="checkbox-inline"><input name="consumiciones[]" type="checkbox" checked value="OT"><span>&nbsp&nbsp&nbsp Otros</span></input></label>	
								-->
								</div>
						</div>
						<!-- -->
						<div class="col-xs-12 col-sm-12 col-md-12 form-group">
							<br>
							<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Promociones</h5>
							<div>
								<label class="checkbox-inline">
								  <input class="checkbox-group" name='promociones' type="checkbox" value="true"> Promociones
								</label>
								<br>
								<label class="checkbox-inline">
								  <!--<input class="checkbox-group" name='descuentos' type="checkbox" value=""> Descuentos-->
								  <input class="checkbox-group" name="sugeridos" type="checkbox" value="true"> Sugeridos </input>
								</label>
							</div>
						</div>
						@if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia == config('constants.adminDtp'))
						<label style="margin-left: 5%;" for="estrellas">PROVEEDORES</label>
						<div class="col-xs-12 col-sm-11 col-md-11 form-group" style="margin-bottom: 20px; margin-left: 5%; background-color: white;">
								<div id="checkBoxProveedor">
								</div>
						</div>	
						@endif
					</form>

					<div class="row">
						</br>
					</div>	
					<div class="col-xs-12 col-sm-6 col-md-6 form-group">
							<button id="btnFiltrar" class="btn btn-info full-width" style="background-color: rgb(17, 105, 176);" type="button">Filtrar</button>
							<!--<button id="btnFiltro" type="button">Remover Filtro</button>-->
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 form-group">
							<button id="btnRemover" class="btn btn-info full-width" style="background-color: rgb(17, 105, 176);" type="button">Remover</button>
							<!--<button id="btnFiltro" type="button">Remover Filtro</button>-->
						</div>

					</div>
				</div>
				
				
				<div class="col-xs-12 col-sm-8 col-md-9 resultados" style="padding-right: 0px;">
					{{$sid}}
					<div class="progress progress-striped active">
						<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					
					<div id="div_msg">
						
					</div>
					
					<div class="hotel-list listing-style3 hotel" id = "resultadosBusqueda">
							@if(@codRetorno != 0)
								@include('partials/conexionError')
							@endif	
					</div>
					
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 text-center">
							<ul class="pagination">
								<li><a href="#" id ="btnAtras">« Anterior</a></li>
								<li><a href="#" id="btnNext">Siguiente »</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</div>

@endsection

@section('scripts')
    @parent
	<script type="text/javascript" src="{{ URL::asset('js/search/scriptBuscador.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/search/filtro-buscador.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/search/fechasSearch.js') }}"></script>
 	<script type="text/javascript" src="{{ URL::asset('js/select2.min.js')}}"></script>
 	<script type="text/javascript" src="{{ URL::asset('css/jquery.ui.autocomplete.scroll.js') }}"></script>
	<script>
			//location.reload();
			@if(Session::has('codRetorno') && Session::get('codRetorno') != 0)
				mensaje();
			@endif

			@if($sid)
				alert('Ingreso');
			@endif	
			wFilter = false;
			filterRemoved = false;
			acabo = false;
			segurola = false;
			maxResultsPerPage = 10;
			currentPage = 0;

			function jqid (id) {
					return (!id) ? null : '#' + id.replace(/(:|\.|\[|\]|,|=|@)/g, '\\$1');
				}

			function sliceUpdate(minimo, maximo){
					$( "#price-ranges" ).slider({
							range: true,
							min: minimo,
							max: maximo,
							values: [ minimo, maximo], 
							slide: function( event, ui ) {
								$( "#precio_minimo" ).val( ui.values[ 0 ] );
								$( "#precio_maximo" ).val( ui.values[ 1 ] );
							}
					});
					$( "#precio_minimo" ).val($( "#price-ranges" ).slider( "values", 0 ) );
					$( "#precio_maximo" ).val($( "#price-ranges" ).slider( "values", 1 ) );
				}

			$('#destinations_name').autocomplete({
						    source: function (request, response) {
					          $.getJSON("{{'destinations'}}?term=" + request.term, function (data) {
						            response($.map(data, function (value, key) {
						                return {
						                    label: value.desDestino,
						                    value: value.desDestino,
						                    valor: value.idDestino
						                };
						            }));
						        });
						    },
						    minLength: 3,
						    delay: 100,
						    maxShowItems: 7,
						    select: function( event, ui ) {
								$('#destino').val(ui.item.valor);
						    }
				});
	
			var mapConsumicion = new Object();
				mapConsumicion['RO'] = "Solo Habitación";
				mapConsumicion['BF'] = "Desayuno";
				mapConsumicion['HB'] = "Media pensión";
				mapConsumicion['FB'] = "Pensión completa";
				mapConsumicion['AI'] = "Todo incluido";
				mapConsumicion['OT'] = "Otros";

			alert("{{Session::get('marcador')}}")

			@if(Session::has('marcador') || Session::has('sid'))

				$(".progress").remove();
				sid="{{$sid}}";
				alert('2')
				$('#anchorMapa').attr("href", $('#anchorMapa').attr("href") + '?sid=' + sid);

				$(function(){
					$("#frmBusqueda").submit(function(e){
						e.preventDefault();
						cerrar1="{{Session::forget('marcador')}}";
						cerrar2="{{Session::forget('sid')}}";
						cerrar3="{{Session::forget('urlFull')}}";
						var dataString = $("#frmBusqueda").serialize();
						objBusqueda.habitaciones.storeRoomsContainer();
						dataString += '&ocupancia='+encodeURIComponent(objBusqueda.getHabitaciones());
						window.location = "{{route('search')}}?"+dataString;
					});

					var inputs = {!!  json_encode($inputs) !!};
					ocupancia = {!! $ocupancia !!};
					
					objBusqueda.habitaciones.setRoomsContainer();
					objBusqueda.crear();
					$.each(inputs, function(key,value){
						if(key=='objBusqueda') return;
						$('#'+key).val(value);
					});
					currentPage=1;
				});

				$('#busquedaHotel').on('keyup', function(){
						$('#btnAtras').hide();
						wFilter=true;
						currentPage =1;
						doFiltros();
					});


				function genSearchString(){
					var dataString = $("#frmBusqueda").serialize();
					dataString+="&sid={{Session::get('sid')}}"; 
					if(wFilter){
						dataString+='&filter=true';
					}
					dataString += '&from='+(currentPage-1)*maxResultsPerPage;
					dataString += '&to='+((currentPage)*maxResultsPerPage-1);
					console.log((currentPage-1)*maxResultsPerPage);
					console.log((currentPage)*maxResultsPerPage-1);
					return dataString;
				}
				
				$("#frmBusqueda").submit(function(e){
						e.preventDefault();
						var dataString = $("#frmBusqueda").serialize();
						objBusqueda.habitaciones.storeRoomsContainer();
						dataString += '&ocupancia='+encodeURIComponent(objBusqueda.getHabitaciones());
						window.location = "{{route('search')}}?"+dataString;
					});

				//function doBack(){
					var dataString = genSearchString();
					$.ajax({
						type: "GET",
						url: "{{route('doBack')}}",
						dataType: 'json',
						data: dataString,
						success: handleFiltros,
						error: function(jqXHR,textStatus,errorThrown){
						},
						complete: function(){
							$.unblockUI();
						}
					});
				//}

				function handleFiltros(rsp){
					if(rsp.codRetorno != 0){
						msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i> No existen resultados con esos criterios</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
						mostrarMensaje(msg);			
					}else if(rsp.hotelViews.length <= 0){
						$('#div_msg').html("<h1 style='color:#e2076a; background-color:white; width: 400px; margin-left:240px;'>NO SE ENCONTRARON RESULTADOS CON ESOS CRITERIOS</h1>");
						$('#div_msg').css('margin-top','15px');
					}
					else{
						$('#div_msg').html("");
						updateHotelView(rsp.hotelViews);
						updateButton(rsp);
						updateNumberOfResults(rsp.cantResults);
					}
					updateFilters(rsp);
				}

				function updateNumberOfResults(n){
					if(n != undefined){
						$("#cantResultados").html('<b>'+n+" resultados encontrados<b>");
					}
				}

				function updateButton(rsp){
					if(rsp.hotelViews.length != maxResultsPerPage){
						$('#btnNext').hide();
					}else{
						$('#btnNext').show();
					}
					if(currentPage == 1){
						$('#btnAtras').hide();
					}else{
						$('#btnAtras').show();
					}
				}

				$('#btnAtras').hide();
				$('#btnNext').hide();

				$('#btnNext, #btnAtras').click(function(e){
					if($(e.target).attr('id') == 'btnNext') currentPage++;
						else currentPage--;
						paginacion();
				});

				function paginacion(){
					var dataString = genSearchString();
					$.ajax({
						type: "GET",
						url: "{{route('paginar')}}",
						dataType: 'json',
						data: dataString,
						success: handlePaginacion,
						error: function(jqXHR,textStatus,errorThrown){
						},
						complete: function(){
							
						}
					});
				}
				
				function handlePaginacion(rsp){
					if(rsp.codRetorno != 0){
						//error
					}else if(rsp.hotelViews.length <= 0){
						//no hay resultados en session para busqueda
					}
					else{
						updateHotelView(rsp.hotelViews);
						updateButton(rsp);
						updateNumberOfResults(rsp.cantResults);
					}
				}

				function updateFilters(rsp){
					if(rsp.minimo != undefined){
						sliceUpdate(rsp.minimo, rsp.maximo);
					}
					$.each(rsp.providers,function(idProvider,results){
						if(results == -1) results = 0;
						
						if($('#checkBoxProveedor').children('label[id="lblProv' + idProvider + '"]').length ==1){
							$('#checkBoxProveedor').find('label[id="lblProv' + idProvider + '"]').each(function(){
								//disablear/enablear en update
								if(results != 0){
									$(this).children('span').html(`&nbsp&nbsp&nbsp&nbsp(` + results + `)`);
									$(this).children('input').prop('disabled',false);
									$(this).children('input').prop('checked',true);
								}
								else{
									$(this).children('input').prop('disabled',true);
									$(this).children('input').prop('checked',false);
								}
							});
						}else{
							$('#checkBoxProveedor').append(`<label style="color: #1169B0;" id="lblProv` + idProvider+  `"class="checkbox-inline"><input name="proveedores[]" type="checkbox" value="`
							+idProvider+`"` + (results == 0 ? `disabled` : `checked`) +  `><img alt="" style="width: 80px; height:19px; background-color: white;" src="images/proveedores/`+idProvider+
							`.png"></img><span>&nbsp&nbsp&nbsp&nbsp(`+results+`)</span></br></label>`);
						}
					});
					
					$.each(rsp.codConsumiciones, function(index,codConsumicion){
						if(!$('#checkBoxConsumicion').children('label[id="lblCons' + codConsumicion + '"]').length == 1){
							$('#checkBoxConsumicion').append(`<label id="lblCons` + codConsumicion + `"style="color: #1169B0;" class="checkbox-inline"><input name="consumiciones[]" type="checkbox" checked value="` + codConsumicion +`"><span>&nbsp&nbsp&nbsp` + mapConsumicion[codConsumicion] + `</span></input></label><br>`);		
						}
					});
				}

				function updateHotelView(hotelViews){
					$('#resultadosBusqueda').html('');
					for(k=0; k<hotelViews.length ; k++){
						$('#resultadosBusqueda').append(hotelViews[k]);
					}
					//falta llamar esto solo con los nuevos
					$('.frmConfirm').each(function(){
						$(this).submit(function(e){
							var domForm = this;
							var submitedForm = $(domForm);
							procesando();
							var submitForm = false;
							var redirect = false;
							e.preventDefault();
							$.each(submitedForm.serializeArray(), function(i,field) {
								if(field.name == 'rates[]'){
									submitForm = true;
									return;
								}
							});
							if(!submitForm){
								msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i> Seleccione al menos una habitacion</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
								mostrarMensaje(msg);
								$.unblockUI();
								e.preventDefault();
								return;
							}else{
								var serializedForm = submitedForm.serialize()+'&sid='+sid;
								$.ajax({
									type: "GET",
									url: "{{route('checkForPreReserva')}}",
									async: false,
									data: serializedForm,
									success: function(allow){
										redirect = allow;
									},
									error: function(jqXHR,textStatus,errorThrown){
										console.log(errorThrown);
									},
									complete: function(){
										if(redirect=='true'){
											submitedForm.append('<input type="hidden" name="sid" value="'+  sid + '">');
											domForm.submit();
										}
										$.unblockUI();
									}
								});
							}			
						});
						$('.reservarBtn').removeAttr('disabled');
					});
					$('.detalle').each(function(){
						$(this).attr('data-target',jqid($(this).data('target')));
					});
					//maneja todas las paginaciones
					if($.active <=1 && $('#resultadosBusqueda').children().length > 0){
						$('.reservarBtn').removeAttr('disabled');			
					}
				}

//////////////////////////////////////////////////////////////////////////////////////////////////////
			@else
//////////////////////////////////////////////////////////////////////////////////////////////////////	
				alert('Inicial');
				sid = Math.random().toString(36).substring(5);
				$(function(){
					var i = 0;
					makeProgress();	
					$('#anchorMapa').attr("href", $('#anchorMapa').attr("href") + '?sid=' + sid);
					$("#frmBusqueda").submit(function(e){
						e.preventDefault();
						var dataString = $("#frmBusqueda").serialize();
						objBusqueda.habitaciones.storeRoomsContainer();
						dataString += '&ocupancia='+encodeURIComponent(objBusqueda.getHabitaciones());
						window.location = "{{route('search')}}?"+dataString;
					});

					$('#btnAtras').hide();
					$('#btnNext').hide();

					$('#btnNext, #btnAtras').click(function(e){
						if($(e.target).attr('id') == 'btnNext') currentPage++;
						else currentPage--;
						paginar();
					});
					
					var inputs = {!!  json_encode($inputs) !!};
					ocupancia = {!! $ocupancia !!};
					
					objBusqueda.habitaciones.setRoomsContainer();
					objBusqueda.crear();
					$.each(inputs, function(key,value){
						if(key=='objBusqueda') return;
						$('#'+key).val(value);
					});
					currentPage=1;
					doSearchs();
					
					$('.checkbox-group').click(function(){
						$(this).prop('value', true);
					});
					
					//al hacer click en filtrar le llama a DoFiltro
					$('#btnFiltrar').click(function(){
						$('#btnAtras').hide();
						wFilter=true;
						currentPage =1;
						doFiltro();
						
					});

					$('#busquedaHotel').on('keyup', function(){
						$('#btnAtras').hide();
						wFilter=true;
						currentPage =1;
						doFiltro();
					});

					$('#btnRemover').click(function(){
						$('#div_msg').html('');
						wFilter = false;
						filterRemoved = true;
						$('#filtro-buscador :input').each(function(){
							if($(this).attr('type') == 'checkbox') 
							{
								if($(this).prop('name') == 'proveedores[]' || $(this).prop('name') == 'consumiciones[]' ) $(this).prop('checked',true);
								else $(this).prop('checked',false);
								
							}
							else if($(this).attr('type') == 'text' || $(this).attr('name') == 'hotel'){ 
								$(this).val(null);
							}	
							sliceUpdate($( "#price-ranges" ).slider('option','min'),$( "#price-ranges" ).slider('option','max'));
						});
						currentPage = 1;
						$('#btnAtras').hide();			
						paginar();
						
					});
				});

				function makeProgress(){
					if(i < 100){
						i = i + 1;
						$(".progress-bar").css("width", i + "%").text(i + " %");
					}
					// Wait for sometime before running this script again
					setTimeout("makeProgress()", 1200);
				}

				function paginar(){
						var dataString = genSearchString();
						$.ajax({
							type: "GET",
							url: "{{route('paginar')}}",
							dataType: 'json',
							data: dataString,
							success: handlePaginar,
							error: function(jqXHR,textStatus,errorThrown){
							},
							complete: function(){
								if(filterRemoved) filterRemoved = false;
								
							}
						});
				}
					
				function handlePaginar(rsp){
						if(rsp.codRetorno != 0){
							//error
						}else if(rsp.hotelViews.length <= 0){
							//no hay resultados en session para busqueda
						}
						else{
							updateHotelViews(rsp.hotelViews);
							updateButtons(rsp);
							if(filterRemoved){
								updateNumberOfResults(rsp.cantResults);
							}
							//
						}
				}
				//inicializa el Proceso de Busquedas
				function doSearchs(){
				@if(Session::get('marcador') == null)
						var dataString = genSearchString();
						loading();
						$.ajax({
							async: true,
							type: "GET",
							url: "{{route('doTotalSearch')}}",
							data: dataString,
							timeout: 70000,
							success: function(rspTotal){
							},
							complete: function(){
								acabo = true;
							}
						});
						getCurrentHotels();
					@endif
				}
				function getCurrentHotels(){
					$.ajax({
						type: "GET",
						url: "{{route('getCurrentHotels')}}",
						dataType: 'json',
						data: 'sid=' + sid + (segurola ? '&segurola=true' : ''),
						timeout: 90000,
						success: handleSearch,
						error: function(jqXHR,textStatus,errorThrown){
						},
						complete: function(){
							$.unblockUI();
							if($.active <=1 && $('#resultadosBusqueda').children().length > 0){
								$('.reservarBtn').removeAttr('disabled');			
							}		
							if(acabo){
								//si ya se hizo el pedido para asegurar segurola = true, sino false
								if(segurola){
									$(".progress").remove();
									
									if($('#resultadosBusqueda').children().length ==0 ){
											console.log('no se encontraron resultados');
										}
								}
								else{
									segurola = true;
									getCurrentHotels();
								}
							}
							else{
								getCurrentHotels();
							}
						}
					});
				}

				function handleSearch(rsp){
						currentPage=1;
						updateHotelViews(rsp.hotelViews);
						updateButtons(rsp);
						updateFilter(rsp);
						updateNumberOfResults(rsp.cantResults);
				}
				
				function doFiltro(){
					wFilter=true;
					var dataString = $("#filtro-formulario").serialize();
					dataString += '&' + genSearchString();
					$.ajax({
						type: "GET",
						url: "{{route('doFiltro')}}",
						dataType: 'json',
						data: dataString,
						success: handleFiltro,
						error: function(jqXHR,textStatus,errorThrown){
						},
						complete: function(){
							$.unblockUI();
						}
					});
				}
				
				function handleFiltro(rsp){
					if(rsp.codRetorno != 0){
						msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i> No existen resultados con esos criterios</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
						mostrarMensaje(msg);			
					}else if(rsp.hotelViews.length <= 0){
						$('#div_msg').html("<h1 style='color:#e2076a; background-color:white; width: 400px; margin-left:240px;'>NO SE ENCONTRARON RESULTADOS CON ESOS CRITERIOS</h1>");
						$('#div_msg').css('margin-top','15px');
					}
					else{
						$('#div_msg').html("");
						updateHotelViews(rsp.hotelViews);
						updateButtons(rsp);
						updateNumberOfResults(rsp.cantResults);
					}
				}
				
				function handleResponse(rsp){
					//en caso de error en alguna respuesta, se cancela todo
					return;
					if(rsp.codRetorno != 0){	
						msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i>'+rsp.desRetorno+'</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
						mostrarMensaje(msg);
						return;
					}
					else if(rsp.checkedForMore){
						currentPage =1;
						if(rsp.newResults) console.log('#CHECK FOR MORE TRAJO NEW RESULTS');
						else console.log('#CHECK FOR MORE FUNCIONO PERO NO TRAJO RESULTS');
					}
					if(rsp.hotelViews.length>0){
						$('#resultadosBusqueda').html('');
						appendHotelViews(rsp.hotelViews);
						updateElements(rsp);
						
					}else{
						updateNumberOfResults(rsp.cantResults);
						//No Results
						var msg = "";
						$('#btnNext').hide();
						if(currentPage==1){
							$.unblockUI();
							msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se han encontrado resultados</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
							mostrarMensaje(msg);
							
							$('#resultadosBusqueda').html('');
						}
						else{
							var msg = "";
							msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No hay mas resultados para mostrar</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
							mostrarMensaje(msg);			
							currentPage--;
							if(currentPage == 1){
								$('#btnAtras').hide();
								$('#btnNext').hide();
							}
						}					
					}
				}
				
				function genSearchString(){
					var dataString = $("#frmBusqueda").serialize();
					dataString += '&ocupancia='+encodeURIComponent(objBusqueda.getHabitaciones());
					tOccupancy = objBusqueda.getFullOccupancy();
					dataString += '&trooms='+tOccupancy.habitaciones;
					dataString += '&tadults='+tOccupancy.adultos;
					dataString += '&tchild='+tOccupancy.ninhos;
					dataString += '&from='+(currentPage-1)*maxResultsPerPage;
					dataString += '&to='+((currentPage)*maxResultsPerPage-1);
					dataString+='&sid='+sid;
					if(wFilter){
						dataString+='&filter=true';
					}
					
					codDestino = $('#destino').val();
					fechaDesde = $('#fecha_desde').val();
					fechaHasta = $('#fecha_hasta').val();
					cantAdultos = objBusqueda.habitaciones.occupancy.adultos;
					cantHabitaciones = ocupancia.length;
					return dataString;
				}

				function checkForMore(){
					$.ajax({
						type: "GET",
						url:"{{'checkForMore'}}",
						dataType: 'json',
						data: true,
						success: handleResponse,
						error: function(jqXHR,textStatus,errorThrown){
						},
						complete: function(){
							$(".progress").remove();
						}
					});
				}

				function updateButtons(rsp){
					if(rsp.hotelViews.length != maxResultsPerPage){
						$('#btnNext').hide();
					}else{
						$('#btnNext').show();
					}
					if(currentPage == 1){
						$('#btnAtras').hide();
					}else{
						$('#btnAtras').show();
					}
				}
				
				function updateFilter(rsp){
					if(rsp.minimo != undefined){
						sliceUpdate(rsp.minimo, rsp.maximo);
					}
					$.each(rsp.providers,function(idProvider,results){
						if(results == -1) results = 0;
						
						if($('#checkBoxProveedor').children('label[id="lblProv' + idProvider + '"]').length ==1){
							$('#checkBoxProveedor').find('label[id="lblProv' + idProvider + '"]').each(function(){
								//disablear/enablear en update
								if(results != 0){
									$(this).children('span').html(`&nbsp&nbsp&nbsp&nbsp(` + results + `)`);
									$(this).children('input').prop('disabled',false);
									$(this).children('input').prop('checked',true);
								}
								else{
									$(this).children('input').prop('disabled',true);
									$(this).children('input').prop('checked',false);
								}
							});
						}else{
							$('#checkBoxProveedor').append(`<label style="color: #1169B0;" id="lblProv` + idProvider+  `"class="checkbox-inline"><input name="proveedores[]" type="checkbox" value="`
							+idProvider+`"` + (results == 0 ? `disabled` : `checked`) +  `><img alt="" style="width: 80px; height:19px; background-color: white;" src="images/proveedores/`+idProvider+
							`.png"></img><span>&nbsp&nbsp&nbsp&nbsp(`+results+`)</span></br></label>`);
						}
						
					});
					
					$.each(rsp.codConsumiciones, function(index,codConsumicion){
						if(!$('#checkBoxConsumicion').children('label[id="lblCons' + codConsumicion + '"]').length == 1){
							$('#checkBoxConsumicion').append(`<label id="lblCons` + codConsumicion + `"style="color: #1169B0;" class="checkbox-inline"><input name="consumiciones[]" type="checkbox" checked value="` + codConsumicion +`"><span>&nbsp&nbsp&nbsp` + mapConsumicion[codConsumicion] + `</span></input></label><br>`);		
						}
					});
				}
				
				function updateNumberOfResults(n){
					if(n != undefined){
						$("#cantResultados").html('<b>'+n+" resultados encontrados<b>");
					}
				}
				
				function updateHotelViews(hotelViews){
					$('#resultadosBusqueda').html('');
					for(k=0; k<hotelViews.length ; k++){
						$('#resultadosBusqueda').append(hotelViews[k]);
					}
					//falta llamar esto solo con los nuevos
					$('.frmConfirm').each(function(){
						$(this).submit(function(e){
							var domForm = this;
							var submitedForm = $(domForm);
							procesando();
							var submitForm = false;
							var redirect = false;
							e.preventDefault();
							$.each(submitedForm.serializeArray(), function(i,field) {
								if(field.name == 'rates[]'){
									submitForm = true;
									return;
								}
							});
							if(!submitForm){
								msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i> Seleccione al menos una habitacion</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>';
								mostrarMensaje(msg);
								$.unblockUI();
								e.preventDefault();
								return;
							}else{
								var serializedForm = submitedForm.serialize()+'&sid='+sid;
								$.ajax({
									type: "GET",
									url: "{{route('checkForPreReserva')}}",
									async: false,
									data: serializedForm,
									success: function(allow){
										redirect = allow;
									},
									error: function(jqXHR,textStatus,errorThrown){
										console.log(errorThrown);
									},
									complete: function(){
										if(redirect=='true'){
											submitedForm.append('<input type="hidden" name="sid" value="'+  sid + '">');
											domForm.submit();
										}
										$.unblockUI();
									}
								});
							}			
						});
					});
					$('.detalle').each(function(){
						$(this).attr('data-target',jqid($(this).data('target')));
					});
					//maneja todas las paginaciones
					if($.active <=1 && $('#resultadosBusqueda').children().length > 0){
						$('.reservarBtn').removeAttr('disabled');			
					}
				}

				function cantidad(id){
					$("#cantResultados").empty();
					$("#cantResultados").html('<b>'+id+" resultados encontrados<b>");
				}	

				function getParameterByName(name,url) {
					var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
					return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
				}

			@endif
			function mensaje(event){
				$mensaje  = "{{Session::get('desRetorno')}}";
				$.blockUI({
		                centerY: 0,
		                message: '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i>'+$mensaje+'</h2><p><br><button id="aceptar" class="btn btn-info text-center btn transaction normal hide-small normal-button" style="height: 45px;">Aceptar</button></p></div>',
		                css: {
		                    color: '#000'
		                    }
		               });
			}
			
			function doFiltros(){
					wFilter=true;
					var dataString = $("#filtro-formulario").serialize();
					dataString += '&' + genSearchString();
					$.ajax({
						type: "GET",
						url: "{{route('doFiltro')}}",
						dataType: 'json',
						data: dataString,
						success: handleFiltros,
						error: function(jqXHR,textStatus,errorThrown){
						},
						complete: function(){
							$.unblockUI();
						}
					});
				}
	</script>
	@endsection
