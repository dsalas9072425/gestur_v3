@extends('master')

@section('title', 'Confirmación de Reserva')
@section('styles')
	@parent
@endsection

<?php 
?>
@section('content')
<section class="sectiontop">
		<div class="container">
                <form action="" method="post">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 alert-danger">
							<!-- En caso de error-->
                        </div>
                        @include('flash::message')
                        <div id="main" class="col-sm-12 col-md-12" class="sectiontop">
                            <div class="booking-information travelo-box"> 
                                <h2>Reserva Rechazada</h2>
                                <hr />
                                <div class="booking-confirmation clearfix">
                                    <i class="glyphicon glyphicon-folder-open icon circle"></i>
                                    <div class="message">
                                        <h4 class="main-message">Muchas gracias por su reserva.</h4>
                                        <p>Se debe de hacer la revision del estado de las Reserva.</p>
                                    </div>
                                </div>
                                <hr />
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<h2>Información de la Reserva</h2>
										<dl class="term-descriptioncustom">
											<dt>Codigo de Reserva:</dt><dd></dd>
											<dt>Fecha de Reserva:</dt><dd></dd>
											<dt>Pasajero:</dt><dd></dd>
												<dt>Estado:</dt><dd>
													@php
														echo '<span class="label alert-info">PENDIENTE</span>';
													@endphp</dd>
											</dt><dd></dd>
										</dl>
									</div>
									<div class="col-md-6 col-sm-6">
										<h2>Datos del Alojamiento</h2>
										<dl class="term-descriptioncustom">
											<dt>Alojamiento:</dt><dd></dd>
											<dt>Dirección:</dt><dd></dd>
										</dl>
									</div>
								</div>
							</div>
                            <div class="booking-information travelo-box">
								<h2>Servicios</h2>
                                <div class="table-responsive">
									<table class="table">
										<tr>
											<th>Tipo de habitación</th>
											<th>Adultos</th>
											<th>Niños</th>
											<th>Edades de Niños</th>
											<th>Comentarios de Habitación</th>
										</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
									</table>
								</div>
                            </div>
                            <div class="booking-information travelo-box">
                                <h2>Comentarios</h2>
                                <dl class="term-descriptioncustom">
									<p>
									</p>
                                </dl>
                            </div>
                            <div class="booking-information travelo-box">
								<div class="row">
									<div class="col-md-4 col-sm-4">
										<h2>Política de Cancelación</h2>
											<dl class="term-descriptioncustom">
												<dt>Desde:</dt><dd></dd>
												<dt>Costo:</dt><dd></dd>
											</dl>
									</div>
									<div class="col-md-4 col-sm-4">
										<h2>Información Importante</h2>
										<dl class="term-descriptioncustom">
											<dt>Check In:</dt><dd></dd>
											<dt>Check Out:</dt><dd></dd>

										</dl>
										<p>A confirmar según política de cada Hotel.</p>
									</div>
								</div>							
                            </div>
                        </div>
                    </div>
                </form>
    	</div>
</section>
<div id="dialog-confirm" title="DTPMundo">
	<br>
	<b style="font-size: x-large;color: #e2076a;" >{{$respuesta->mensaje}}</b>
</div>

@endsection

@section('scripts')
    @parent
    <script>
    	$.unblockUI();
    		@if($respuesta->codigo != 0)	
			    $( "#dialog-confirm" ).dialog({
			      resizable: false,
			      height: 250,
			      width: 500,
			      modal: true,
			      buttons: {
			        Ok: function() {
			          $( this ).dialog( "close" );
			          window.location.assign("{{route('home')}}")
			        }
			      }
			    });
			@endif    

    </script>
@endsection
