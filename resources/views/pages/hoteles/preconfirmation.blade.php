@extends('maste')

@section('title', 'PreReserva de hoteles')
@section('styles')
	@parent
@endsection
@section('content')
	<div class="container" style="margin-left: 13%; margin-right: 10%;">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-9 info-reserva sectiontop">
                <form id="frmConfirmacion" class="contact-form" action="{{route('confirmation')}}" method="post">
				<input type="hidden" name="fechaDesde" value="{{$fechaDesde}}"/>
				<input type="hidden" name="fechaHasta" value="{{$fechaHasta}}"/>
				<input type="hidden" name="idBusqueda" value="{{$idBusqueda}}"/>
				<input id="input-facturar" type="hidden" name="facturar" value="false" />
                	{{ csrf_field() }}
                	<div class="row">

                	</div>
					<div class="booking-information travelo-box" style="border-top-left-radius: 5px; border-top-right-radius: 5px; display: block;">
						@include('flash::message')
						<div>
							<span class="count" id="cantResultados" style="font-size: 22px;">
								<b style="text-align: center;">Información de Reserva</b>
								<br/>
							</span>
							<br>
							<?php 
								$indice = 0;
							?>
							<!--<h2 class="box-title">Información de Reserva</h2>-->
							@foreach ($selectedHotel->habitaciones as $habitacion)
								@foreach($habitacion->tarifas as $tarifa)
									<?php 
										$indice++;
									?>
									<h4 class="box-title">Contacto para Habitación {{$indice}}</h4>
									<div class="row">
										<?php 
											$contadorA = 0;	
											$contadorN = 0;	
										?>
										@foreach($tarifa->ocupanciaDtp->descPasajeros as $pasajeros)
											<?php 
												$tipoBajo =  strtolower($pasajeros->tipo);
												if($pasajeros->tipo == "AD"){
													$contadorA++;
													$tipo = "Adulto ".$contadorA;
													$contadors = $contadorA;
												}else{
													$contadorN++;
													$tipo = "Niño ".$contadorN;
													$contadors = $contadorN;
												}
											?>
											<h4 class="box-title" style="color: #e2076a;margin-bottom: 7px;text-align: center; margin-top: 10px;">{{$tipo}}</h4>
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="form-group">
														<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Nombres(*)</h5>
														<input type="text" required class = "Requerido form-control" name="habitacion[{{$indice}}][{{$tipoBajo}}][{{$contadors}}][nombre]" id="[habitacion][{{$indice}}][nombres_{{$tipoBajo}}_{{$contadors}}]">
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="form-group">
														<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Apellidos(*)</h5>
														<input type="text" required class = "Requerido form-control" name="habitacion[{{$indice}}][{{$tipoBajo}}][{{$contadors}}][apellido]" class="input-text full-width required">
													</div>
												</div>
											</div>	
											<div class="row">	
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="form-group">
														<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Edad(*)</h5>
														@if($tipoBajo == "ad")	
															<input type="number" min="18" max="100" required class = "Requerido form-control" name="habitacion[{{$indice}}][{{$tipoBajo}}][{{$contadors}}][edad]" class="input-text full-width required">
														@else
															<input type="number" min="1" max="17" required class = "Requerido form-control" name="habitacion[{{$indice}}][{{$tipoBajo}}][{{$contadors}}][edad]"  class="input-text full-width required">
														@endif	
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="form-group">
													</div>
												</div>
											</div>
											<br>	
										@endforeach	
										<div class="col-xs-12 col-sm-12 col-md-12">
											<div class="form-group">
												<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Servicios Adicionales</h5>
												<input type="text"  name="comentario_{{$indice}}" id="comentario_{{$indice}}" class="input-text full-width">
											</div>
										</div>
									</div>
									<input type= "hidden" name = "rate_key_{{$indice}}" value = "{{$tarifa->codReserva}}">
									<input type= "hidden" name = "n_adultos_{{$indice}}" value="{{$tarifa->ocupanciaDtp->adultos}}">
									<br>
									@if($tarifa->ocupanciaDtp->edadesNinhos != null)
									<input type="hidden" name = "children_ages_{{$indice}}" value="{{implode($tarifa->ocupanciaDtp->edadesNinhos,',')}}">
									@endif
										
									<div class="row">
										<div class="col-sm-4 col-md-4">
											<div class="form-group">
												<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Ocupación</h5>
												<p>{{$tarifa->ocupanciaDtp->adultos}} Adultos 
													@if($tarifa->ocupanciaDtp->edadesNinhos != null ) - Niños de:
														@foreach($tarifa->ocupanciaDtp->edadesNinhos as $edad){{$loop->first ? $edad : ', '.$edad}}@endforeach
														años
													@endif
												</p>
											</div>
										</div>
										<div class="col-sm-4 col-md-4">
											<div class="form-group">
												<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Habitación</h5>
												<p>{{ $habitacion->desHabitacion }} @if($selectedHotel->codProveedor != 4) - {{$tarifa->codConsumicion}} ({{$tarifa->desConsumicion}}) @endif</p>
												<input disabled type="hidden" name="habitacion_{{$indice}}" id="habitacion_{{$indice}}" value="" class="input-text full-width">
											</div>
										</div>
										<div class="col-sm-4 col-md-4">
											<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Costo</h5>
											<p class="costo">{{round($tarifa->precio, 2)}} USD</p>
										</div>
									</div>
													
									<div class="row">
										<div class="col-md-12">
											<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Comentario</h5>
											<p name="comentario" class="input-text full-width">{{ $tarifa->infoAdicional or '' }}</p>
										</div>
									</div>
									<hr class="booking" style="margin-top: 30px;">
									<div class="row">
										<div class="col-md-12">
										</div>
									</div>
								@endforeach
							@endforeach
									
							<input type="hidden" id="clienteReferencia" name="clienteReferencia" class="input-text full-width" value="AG_{{Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia}}_{{Session::get('datos-loggeo')->datos->datosUsuarios->idSucursalAgencia}}" placeholder="" />
						</div>
					</div>
							
					<div class="booking-information travelo-box" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
	                        
	                    <h2 class="box-title">Confirmar Reserva</h2>
	                    <div class="row">
							<div class="col-sm-4 col-md-4">
							<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Total Reserva</h5>
								<p class="costo">{{round($totalReserva,2)}} USD</p>
							</div>
												
							<div class="col-sm-4 col-md-4">
								<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Total Cancelacíon</h5>
								<p class="cancelacion">{{number_format($precioCan,2)." USD"}}</p>
							</div>
								@if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia == config('constants.adminDtp'))
										<div class="col-sm-4 col-md-4">
											<div class="form-group">
											<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Monto a Cobrar (en USD)</h5>
												<input type="number" id="monto" name="monto" min="{{round($totalReserva,2)}}" class="Requerido input-text full-width" data-warned="false" placeholder = "Monto Agencia DTP" required step="0.01" value="{{ round($totalReserva,2) }}" />
											</div>
										</div>

								@else
								@endif
							<div class="col-sm-4 col-md-4">
							</div>
						</div>
								
						<div class="row">			
							<div class="col-sm-5 col-md-5">
								<div class="form-group">
									<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Seleccione Expediente (*)</h5>
									<div class="selector">
										<select required class="full-width" id="expediente" name="expediente" >
											<option value="">Seleccione Proforma </option>
											<option value="0">Nueva Proforma</option>
											@if(isset($selectProforma))
												@foreach($selectProforma as $key=>$proforma)
						                            <option value="{{$proforma['value']}}">{{$proforma['label']}}</option>
												@endforeach
											@endif	
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-5 col-md-5">
								<div class="form-group">
									<h5 style="font-size: 16px;" class="filter-name has-expander expanded">Vendedor DTP Mundo</h5>
									<div class="selector">
										<select class="full-width" id="agente" name="agente">
                                            @foreach($selectAgentes as $key=>$agentes)
                                                <option value="{{$agentes['value']}}"   @if($agentes['value'] == Session::get('datos-loggeo')->datos->datosUsuarios->codAgente )  selected  @endif    >{{$agentes['label']}}</option>
                                            @endforeach
										</select>
									</div>
								</div>
							</div>
						</div>
						<br>
						<div class="row">	
							<div class="col-sm-12 col-md-12">
								<div class="alert alert-{{(   (\DateTime::createFromFormat('d/m/Y' , $fechaCan))  <= new \DateTime()   ? 'danger' : 'info' )}}">
									<div class="row">
										<div class="col-sm-12 col-md-6">		
											<h4><i class="fa fa-exclamation-triangle"></i> Advertencia!</h4> <p>A partir de <b>{{$fechaCan}}</b> se incurrirá en gastos por cancelación</p>
										</div>
										<div class="col-sm-12 col-md-6 text-center">
											<h4><i class="glyphicon glyphicon-thumbs-up"></i> Asegurá tu reserva con</h4>
											<img src="{{asset('images/assist-card.jpg')}}" width="100" />
										</div>
									</div>
								</div>
							</div>
							<div>
							</div>
						</div>
								
	                    <div class="row">
							<div class="col-md-12">
								<div class="alert alert-info">
									<h4><i class="glyphicon glyphicon-info-sign"></i> Importante!</h4> <p>Al hacer click en "<b>Reservar</b>" Ud. acepta nuestros <a href="{{route('terminosycondiciones')}}" target="_blank"><b>Términos y Condiciones</b></a></p>
								</div>
	                        </div>
	                    </div>
	                    <div class="form-group row">
	                        <div class="col-sm-3 col-md-4">
	                            <!--<button type="submit" id="btnReserva" style="padding-top: 5px; height: 45px; display: none;" class="btn btn-info text-center reservarBtn btn transaction normal hide-small normal-button">Reservar</button>-->
	                            <button type="submit" id="btnReserva" style="padding-top: 5px; height: 45px;" class="btn btn-info text-center reservarBtn btn transaction normal hide-small normal-button">Reservar</button>
	                        </div>
	                        <div class="col-sm-3 col-md-4">
	                            <!--<button type="submit" id="btnReserva" style="padding-top: 5px; height: 45px; display: none;" class="btn btn-info text-center reservarBtn btn transaction normal hide-small normal-button">Reservar</button>-->
	                            <!--<button id="btnReservaF" style="padding-top: 5px; height: 45px;" class="btn btn-info text-center reservarBtn btn transaction normal hide-small normal-button">Reservar y Facturar</button>-->
	                        </div>
	                    </div>    
					</div>

                </form>
            </div>
            <div class="sidebar col-sm-12 col-md-3 info-hotel sectiontop" style="padding-left: 0px;padding-right: 0px; border-width: 1px;">
		                <div class="booking-details travelo-box detalle-hotel" style="padding-left: 10px; padding-right: 10px; padding-top: 5px;"">
		                  <div class="col-sm-12 col-md-12" style="background-color: #2d3e52;text-align: center;padding-left: 15px;width: 96%;margin-left: 8px;color:#fff;margin-top: 10px;">
		                  		<b style="font-size: x-large;">RESUMEN DETALLE</b>
		                  </div>
		                    <article class="image-box hotel listing-style1">
		                    	<h2 style="width: 99%;margin-left: 0px;background-color: #2d3e52;color: #fff;text-align: center;margin-bottom: 10px;">Hotel: {{$selectedHotel->nomHotel}}</h2>
		                    	<?php $contador = 0;?>
		                    	@foreach ($selectedHotel->habitaciones as $habitacion)
		                    		<?php $contador++;?>	
			                    	<div class="row">
										<div class="col-sm-12 col-md-12">
											<small><b>Habitación {{$contador}}: </b></small><small>{{ $habitacion->desHabitacion }} @if($selectedHotel->codProveedor != 4) - {{$tarifa->codConsumicion}} ({{$tarifa->desConsumicion}}) @endif</small><br>
											<small><b>Ocupancia: </b></small><small>{{$habitacion->tarifas[0]->ocupanciaDtp->adultos}} Adultos 
											@if($habitacion->tarifas[0]->ocupanciaDtp->edadesNinhos != null ) - Niños de:
												@foreach($habitacion->tarifas[0]->ocupanciaDtp->edadesNinhos as $edad){{$loop->first ? $edad : ', '.$edad}}@endforeach
												años
											@endif
											</small><br>
											<small><b>Monto: </b></small><small>{{round($habitacion->tarifas[0]->precio, 2)}} USD</small>
										</div>
									</div>	
									<hr style="height: 2px;margin-top: 10px; margin-bottom: 5px; border-color: #ccc;">
								@endforeach	
		                        <div class="row">
		                            	<div class="col-sm-4 col-md-4">
		                                    <label style="text-align: center;"><b>Check in</b></label>
		                                    <span style="text-align: center;">{{$fechaDesde}}</span>
		                                </div>
										<div class="col-sm-4 col-md-4" style="text-align: center;">
		                                    <label><b>Noches</b></label><br>
		                                    <span><b>{{$noches}}</b></span>
		                                </div>
		                                <div  class="col-sm-4 col-md-4" style="padding-left: 0px;">
		                                    <label style="text-align: center;"><b>Check out</b></label>
		                                    <span style="text-align: center;">{{$fechaHasta}}</span>
		                                </div>
		                        </div>
			                    <dl class="other-details">
			                    	<hr style="height: 2px;margin-top: 10px; margin-bottom: 5px; border-color: #111;">
									<dt class="total-price"  style="color:#2d3e52;"><b>Total a Pagar</b></dt><dd class="total-price-value" style="color:#e2076a"><b style="font-size: 22px;">{{round($totalReserva,2)}} USD</b></dd>
			                    </dl>
								<div class="row">
									<div class="col-sm-12 col-md-12" style="background-color: #2d3e52;text-align: center;padding-left: 15px;width: 96%;margin-left: 8px;color:#fff;margin-top: 15px;">
										<b>POLITICA DE CANCELACIÓN</b>
									</div>
								</div>
		                        <div class="row alert alert-{{(   (\DateTime::createFromFormat('d/m/Y' , $fechaCan))  <= new \DateTime()   ? 'danger' : 'info' )}}" style="width: 96%;margin-left: 8px;margin-bottom: 0px;margin-top: 10px; padding-left: 10px;padding-right: 10px;">
		                        	<div class="col-sm-12 col-md-12" style="height: 40px; text-align: center;">
		                        	@if((\DateTime::createFromFormat('d/m/Y' , $fechaCan)) <= new \DateTime())
		                        		<i class="fa fa-exclamation-triangle fa-5 fa-lg" aria-hidden="true"></i>
		                        	@else
		                        		<i class="fa fa-check-circle fa-5 fa-lg" aria-hidden="true"></i>
		                        	@endif
		                        	</div>
		                            <div class="col-sm-12 col-md-6" style="padding-left: 0px;padding-right: 0px; margin-bottom: 0px;">
		                                <small><b>A Partir de: </b></small>
		                            </div>    
		                            <div class="col-sm-12 col-md-6" style="text-align: center;">    
		                               <small>{{$fechaCan}}</small>
		                            </div>
		                            <div class="col-sm-12 col-md-6"  style="padding-left: 0px;padding-right: 0px;">
		                                <small"><b>Monto</b></small>
		                            </div>    
		                            <div class="col-sm-12 col-md-6" style="text-align: center;">
		                                <small>{{$precioCan}} USD</small>
		                            </div>
		                        </div>
		                    </article>
		                     <!--<a href="{{Session::get('urlTotal')}}" style="margin-top: 15px; margin-left: 20%;" class="btn btn-info text-center reservarBtn btn transaction normal hide-small normal-button"><b>Ver otras ofertas</b></a>-->
		                </div>
		            </div>
	       </div>
    </div>
	<div id="dialog-confirm" title="DTPMundo">
	<br>
		<b style="font-size: x-large;color: #e2076a;" >{{Session::get('desRetorno')}}</b>
	</div>

@endsection
@section('scripts')
    @parent
	<script>
	
		@if(Session::has('codRetorno') && Session::get('codRetorno') != 0)	
		    $( "#dialog-confirm" ).dialog({
		      resizable: false,
		      height: 250,
		      width: 500,
		      modal: true,
		      buttons: {
		        Ok: function() {
		          $( this ).dialog( "close" );
		        }
		      }
		    });
		@endif    

		 $('#frmConfirmacion').submit(function(e){
		 	$('#btnReserva').attr("disabled", "disabled");
			$form = $(this);
			if($(document.activeElement).attr('id') == 'btnReservaF'){
				e.preventDefault();
				$('#input-facturar').val('true');
				BootstrapDialog.confirm({
			            title: '<b>DTP</b>',
			            message: 'Si elige Reservar y Facturar se facturarán todas las reservas asociadas a esta proforma. Desea Continuar?' ,
			            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
			            closable: true, // <-- Default value is false
			            draggable: true, // <-- Default value is false
			            btnCancelLabel: 'No', // <-- Default value is 'Cancel',
			            btnOKLabel: 'Si, Reservar y Facturar', // <-- Default value is 'OK',
			            btnOKClass: 'btn-res-fac', // <-- If you didn't specify it, dialog type will be used,
			            callback: function(result) {
			                // result will be true if button was click, while it will be false if users close the dialog directly.
			                if(result) 
							{
			                    $.blockUI({
			                        centerY: 0,
			                        message: "<h2>Procesando...</h2>",
			                        css: {
			                            color: '#000'
			                        }
			                    });
								//ryf=true;
								$form.submit();
							}
							else{
								$('#input-facturar').val('false');
							}
						}
					});
				//console.log('opa');
			}else{
				
			}
			
			//procesando();
			//e.preventDefault();
		});
		$('.fecha').each(function() {
			$('#'+$(this).attr('id')).datepicker({
										 		changeMonth: true,
						           				changeYear: true,
						            			yearRange: "-50:+0",
												});
		});




	</script>
	
@endsection
