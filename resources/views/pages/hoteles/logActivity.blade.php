@extends('master')
@section('title', 'Log Activity')
@section('styles')
	@parent
@endsection

@section('content')
<section id="content" class="gray-area">
    <div class="container">
        <div id="main">
            @include('flash::message')
            <div class="block sectiontop">
                <div class="tab-container style1 travelo-policies">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="mis-reservas">
                            <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large; margin-top: 5%">Notificaciones</h1>

                            <form action="" id="frmBusqueda" method="post"> 
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12" id="divReservas" style= "overflow-x: scroll; overflow-y: hidden;">
										<div class="table-responsive">
											<br>
												<!--<h1 align="center">Listado de Actividades</h1>-->
											<br>
										
											 <table id="notificacionTable" class="table table-bordered table-hover">
												<thead>
													<tr class='bgblue'>
														<th>Fecha</th>
														<th>Contenido</th>
														<th>Usuario</th>
														<th>Agencia</th>
														<!--<th>De</th>-->
														<th>Para</th>
														<!--<th>CC</th>-->
														<th>Asunto</th>
													</tr>
												</thead>
												<tbody>
												@foreach($notificaciones as $notificacion)
														<tr>
															<td>{{$notificacion->fecha_hora_creacion}}</td>
															<td><a data-toggle="modal" id="" href="#requestModal" onclick="testFunction({{$notificacion->id}});"class="btn-mas-request"><i class="fa fa-file-text-o fa-lg"></i></a></td>									@if(isset($notificacion->usuario->nombre_apellido))
																<td><b>{{$notificacion->usuario->nombre_apellido}}</b></td>
															@else
																<td><b></b></td>
															@endif	
															<td>{{$notificacion->agencia->razon_social}}</td>
															<td>{{$notificacion->para}}</td>
															 <td><b>{{$notificacion->asunto}}</b></td>
														</tr>
												@endforeach		
												</tbody>
											</table>



										</div>
                                    </div>
                                </div>
								<div class="row" id="totalReservas">
								</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   	<!-- Modal Request-->
	<div id="requestModal" class="modal fade" role="dialog">
	<!--<form id="form-asignar-expediente" method="post" action="">-->
	  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h2 class="modal-title titlepage" style="font-size: x-large;">Contenido</h2>
			</div>
		  <div class="modal-body">
		  		<div id="contenido"></div>
		  </div>
		  <div class="modal-footer">
			<button type="button" id="btnCancelarVSTour" class="btn btn-danger" style="width: 80px; background-color: #e2076a;" data-dismiss="modal">Cerrar</button>
		  </div>
		</div>
	  </div>
	 <!--</form>-->
	</div>
</section>
@endsection

@section('scripts')
	@include('layouts/scripts')
	<script>
	$('#notificacionTable').dataTable({
										"order": [[ 0, 'desc' ]]
									});

	/*$(".btn-mas-request").each( function() {
   			cargando();
	});*/

	function testFunction(idNotificacion){
			console.log("{{route('getDetalleNotificacion')}}?id="+idNotificacion);
			$.ajax({
					type: "GET",
					url: "{{route('getDetalleNotificacion')}}",
					data: "id="+idNotificacion,
					dataType: 'json',
					success: function(rsp){
									$("#contenido").html(rsp.contenido);
									console.log(rsp);
								}	
					})

	}		


	</script>

@endsection