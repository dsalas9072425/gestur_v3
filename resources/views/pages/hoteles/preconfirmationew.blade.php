@extends('master')

@section('title', 'PreReserva de hoteles')
@section('styles')
	@parent
@endsection

<?php 
$hotelDetails = "";
$montoUsdCC = $hotelInfo->selectedRates[0]->precioConComision;
?>
@section('content')
	<div class="container">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-9 info-reserva sectiontop">
                <form id="frmConfirmacion" class="contact-form" action="{{route('confirmation')}}" method="post">
                	{{ csrf_field() }}
					<input type="hidden" name="fechaCan" value= "{{$hotelInfo->fechaCan}}" >
					<input type="hidden" name ="precioCan" value="{{$hotelInfo->precioCan}}">
					<input type="hidden" name ="id_request" value="{{Session::get('token')}}">
					
					<input type="text" name ="totalPrecioNetoConvertido" value="{{Session::get('totalPrecioNetoConvertido')}}">
					<input type="text" name ="totalReservaConComision" value="{{Session::get('totalReservaConComision')}}">
					<input type="text" name ="totalReservaSinImpuestos" value="{{Session::get('totalReservaSinImpuestos')}}">
					<input type="text" name ="totalReservaPrecioNeto" value="{{Session::get('totalReservaPrecioNeto')}}">
					<div>
						@include('flash::message')
						<div class="toggle-container box">
							<div class="panel style1">
								<h4 class="panel-title">
	                                <a href="#inforeserva" data-toggle="collapse"><i class="fa fa-info-circle" aria-hidden="true"></i> Información de Reserva</a>
	                            </h4>
	                            <div class="panel-collapse collapse in" id="inforeserva">
	                            	<div class="panel-content">
										@foreach ($hotelInfo->selectedRates as $rate)
											<h4 class="box-title">Contacto Principal para Habitación {{$loop->iteration}}</h4>
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="form-group">
														<label>Nombres(*)</label>
														<input type="text" required class = "Requerido form-control" name="nombres_{{$loop->iteration}}" id="nombres_{{$loop->iteration}}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="form-group">
														<label>Apellidos(*)</label>
														<input type="text" required class = "Requerido form-control" name="apellidos_{{$loop->iteration}}" id="apellidos_{{$loop->iteration}}" class="input-text full-width required">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="form-group">
														<label>Tipo de Documento(*)</label>
														<select name="tipoDocumento_{{$loop->iteration}}" required class = "Requerido form-control" class="input-text full-width required" id="tipoDocumento_{{$loop->iteration}}">
															  <option value="">Seleccione Tipo de Documento</option>
															  <option value="PAS">Pasaporte</option>
															  <option value="CIP">Cédula de Identidad Policial</option>
															  <option value="DNI">Documento Nacional de Identidad</option>
															  <option value="LIC">Licencia de Conductor</option>
														</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-6 col-md-6">
													<div class="form-group">
														<label>N° de Documento(*)</label>
														<input type="text" required class = "Requerido form-control" name="numeroDocumento_{{$loop->iteration}}" id="numeroDocumento_{{$loop->iteration}}" class="input-text full-width required">
													</div>
												</div>
											</div>	
											<input type= "hidden" name = "rate_key_{{$loop->iteration}}" value = "{{$rate->codReserva}}">
											<input type= "hidden" name = "n_adultos_{{$loop->iteration}}" value="{{$rate->adultos}}">
											<br>
											@if(isset($rate->edadNinos) && $rate->edadNinos != null)
											<input type="hidden" name = "children_ages_{{$loop->iteration}}" value="{{implode($rate->edadNinos,',')}}">
											@endif
											<div class="row">
												<div class="col-sm-4 col-md-4">
													<div class="form-group">
														<label>Ocupación</label>
														<p>{{$rate->adultos}} Adultos {{$rate->children or '0'}} Niño/s</p>
														{{--<input readonly type="hidden" name="ocupacion_{{$loop->iteration}}" id="ocupacion_{{$loop->iteration}}" value="" class="input-text full-width">--}}
													</div>
												</div>
												<div class="col-sm-4 col-md-4">
													<div class="form-group">
														<label>Regimen</label>
														<p>{{$rate->desConsumicion or 'Solo habitacion'}}</p>
														<input disabled type="hidden" name="habitacion_{{$loop->iteration}}" id="habitacion_{{$loop->iteration}}" value="" class="input-text full-width">
													</div>
												</div>
												<div class="col-sm-4 col-md-4">
													<label>Costo</label>
													<p class="costo">{{round($rate->precioConComision
												, 2)}} USD</p>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
												{{--
													<input type="hidden" readonly name="rate_key_{{$cont}}" id="rate_key_{{$cont}}" value="{{$rateKey}}" class="input-text full-width">
													<input type="hidden" readonly name="adultos_{{$cont}}" id="adultos_{{$cont}}" value="{{$adults}}" class="input-text full-width">
													<input type="hidden" readonly name="ninhos_{{$cont}}" id="ninhos_{{$cont}}" value="{{$children}}" class="input-text full-width">
													<input type="hidden" readonly name="edades_ninhos_{{$cont}}" id="edades_ninhos_{{$cont}}" value="{{ $childrenAges}}" class="input-text full-width">
												--}}
												</div>
											</div>
										@endforeach
	                                </div>
							    </div>
							</div>
						    <div class="panel style1">
	                            <h4 class="panel-title">
	                                <a href="#infopasajeros" data-toggle="collapse"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Servicios Adicionales</a>
	                            </h4>
	                            <div class="panel-collapse collapse in" id="infopasajeros">
	                                <div class="panel-content">
										<div class="form-group">
											<label>SOLICITAR SERVICIOS ADICIONALES (Translados, excursiones, admisiones, cruceros, etc.)<br />
											<span class="alert-service">* sujeto a disponibilidad</span></label>
											<textarea name="comentario" class="input-text full-width" placeholder="Servicios Adicionales"></textarea>
										</div>
							        </div>
								</div>
							</div>
							<div class="panel style1">
	                            <h4 class="panel-title">
	                                <a href="#infoconfirmar" data-toggle="collapse"><i class="fa fa-check-square-o" aria-hidden="true"></i> Confirmar Reserva</a>
	                            </h4>
	                            <div class="panel-collapse collapse in" id="infoconfirmar">
	                                <div class="panel-content">
						                    <div class="row">
												<div class="col-sm-4 col-md-4">
													<label>TOTAL RESERVA</label>
													<p class="costo">{{round($hotelInfo->totalReserva,2)}} USD</p>
												</div>
																	
												<div class="col-sm-4 col-md-4">
													<label>TOTAL CANCELACION</label>
													<p class="cancelacion">{{number_format($hotelInfo->precioCan,2)." USD"}}</p>
												</div>
													@if(Session::get('datos-loggeo')->datos->datosUsuarios->idPerfil == config('constants.adminDtp'))
															<div class="col-sm-4 col-md-4">
																<div class="form-group">
																	<label>MONTO A COBRAR (en USD)</label>
																	<input type="number" id="monto" name="monto" class="Requerido input-text full-width" data-warned="false" placeholder = "Monto Agencia DTP" required step="0.01" value="{{ round($montoUsdCC,2) }}" />
																</div>
															</div>

													@else
															<div class="col-sm-4 col-md-4">
																<div class="form-group">
																	<label>MONTO A COBRAR (en USD)</label>
																	<input type="number" id="monto" name="monto" class="Requerido input-text full-width" data-warned="false" placeholder = "Monto Agencia DTP" required step="0.01" value="{{ round($montoUsdCC,2) }}" readonly="readonly"/>
																</div>
															</div>
													@endif
												<div class="col-sm-4 col-md-4">
												{{--
													<div class="form-group">
														<label>MEDIO DE PAGO</label>
														<div class="selector">
															<select class="full-width" id="medioPago" name="medioPago">
																{{$mediosPago or ''}}
															</select>
														</div>
													</div>
												--}}
												</div>
											</div>
													
											<div class="row">			
												<div class="col-sm-4 col-md-4">
													<div class="form-group">
														<label>SELECCIONE EXPEDIENTE</label>
														<div class="selector">
															<select class="full-width" id="expediente" name="expediente">
																<option value="0">Nuevo Expediente </option>
																{{$expedientesDtpMundo or ''}}
															</select>
														</div>
													</div>
												</div>
												<div class="col-sm-4 col-md-4">
													<div class="form-group">
														<label>VENDEDOR DTP MUNDO</label>
														<div class="selector">
															<select class="full-width" id="agente" name="agente">
					                                            @foreach($selectAgentes as $key=>$agentes)
					                                                <option value="{{$agentes['value']}}">{{$agentes['label']}}</option>
					                                            @endforeach
															</select>
														</div>
													</div>
												</div>
											</div>
											<br>
											<div class="row">	
												<div class="col-sm-12 col-md-12">
													<div class="alert alert-danger">
														<div class="row">
															<div class="col-sm-12 col-md-6">
																<h4><i class="fa fa-exclamation-triangle"></i> Advertencia!</h4> <p>A partir de <b>{{$hotelInfo->fechaCan}}</b> se incurrirá en gastos por cancelación</p>
															</div>
															<div class="col-sm-12 col-md-6 text-center">
																<h4><i class="glyphicon glyphicon-thumbs-up"></i> Asegurá tu reserva con</h4>
																<img src="{{asset('images/assist-card.jpg')}}" width="200" />
															</div>
														</div>
													</div>
												</div>
												<div>
												</div>
											</div>
						                    <div class="row">
												<div class="col-md-12">
													<div class="alert alert-info">
														<h4><i class="glyphicon glyphicon-info-sign"></i> Importante!</h4> <p>Al hacer click en "<b>Reservar</b>" Ud. acepta nuestros <a href="{{route('terminosycondiciones')}}" target="_blank"><b>Términos y Condiciones</b></a></p>
													</div>
						                        </div>
						                    </div>
						                    <div class="form-group row">
						                    <div class="col-sm-3 col-md-4">
						                        <button type="submit" class="btn btn-primary uppercase">Reservar</button>
						                    </div>
						                </div>    
					                </div>
								</div>
							</div>
						</div>
					</div>
				</form>
            </div>
            <div class="sidebar col-sm-12 col-md-3 info-hotel sectiontops">
                <div class="booking-details travelo-box detalle-hotel">
                    <h4 class="box-title">Resumen Detalles</h4>
                    <article class="image-box hotel listing-style1">
                    	<h2 style="color:#111;text-align: center;">{{$hotelInfo->titulo->nomHotel}}</h2>
						<form action="{{route('details')}}" method="GET" id="detallesForm" target = "_blank">
							<figure class="clearfix">
								<a href="#" class="hover-effect middle-block"><img class="middle-item" width="270" height="160" alt="" src="{{$hotelInfo->imagenHotel or ''}}"></a>
								<div class="travel-title">
									<h5 class="box-title"><small>
									
										{{$hotelInfo->dirHotel}}
										{{--
										{{$hotelDetails->location->city or 'ciudad'}}
										{{$hotelDetails->location->country or 'pais'}}
									--}}
											
									</small></h5>
									<input type="hidden" name="codHotel" value="{{$hotelInfo->titulo->codHotel}}" />
									<input type="hidden" name="codProveedor" value="{{$hotelInfo->titulo->codProveedor}}" />
									<input type="hidden" name="longitud" value="{{$hotelInfo->coordenadasHotel->longitud }}" />
									<input type="hidden" name="latitud" value="{{$hotelInfo->coordenadasHotel->latitud}} " />
									<button type="submit" form="detallesForm" class="btn-medium uppercase" >DETALLES</button>
								</div>
							</figure>
						</form>
							
                        <div class="details">
                            <div class="constant-column-3 timing clearfix">
                                <div class="check-in">
                                    <label>Check in</label>
                                    <span>{{$hotelInfo->fechaDesde}}</span>
                                </div>
								
                                <div class="duration text-center">
                                    <i class="soap-icon-clock"></i>
                                    <span>{{$hotelInfo->noches}} noches</span>
                                </div>
								
                                <div class="check-out">
                                    <label>Check out</label>
                                    <span>{{$hotelInfo->fechaHasta}}</span>
                                </div>
                            </div>
                        </div>
                    </article>
                    <dl class="other-details">
						<dt class="total-price">Total a Pagar</dt><dd class="total-price-value"><b>{{round($hotelInfo->totalReserva, 2)}} USD</b></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
	<script>
		$('#frmConfirmacion').submit(function(e){
		});
		$('.fecha').each(function() {
			$('#'+$(this).attr('id')).datepicker({
										 		changeMonth: true,
						           				changeYear: true,
						            			yearRange: "-50:+0",
												});
		});
 		$(window).unload(function() {
		  		//console.log("Bye now!");
		});

		$('#agente option[value="{{Session::get('datos-loggeo')->datos->datosUsuarios->codAgente}}"]').attr("selected", "selected");

	</script>
	
@endsection
