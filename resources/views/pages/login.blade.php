<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="GESTUR">
    <title>GESTUR</title>
    <link rel="apple-touch-icon" href="{{asset('gestion/app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('gestion/app-assets/images/logo/git.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    @include('layouts/meta')
    @include('layouts/gestion/styles')
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column   blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center" style="height: 90px;">
                                        <div class="p-1">
                                            <img src="{{asset('gestion/app-assets/images/logo/git.png')}}" alt="branding logo">
                                        </div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Ingresar al Sistema</span></h6>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form  id="login-form" class="login-form" action="{{route('doLogin')}}" method="get">
											<fieldset class="form-group position-relative has-icon-left mb-0">
                                                <input type="text" class="form-control form-control-lg" id="usuario" placeholder="Usuario" name="usuario" required>
                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                            </fieldset>  
                                            <br>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Contraseña" required>
                                                <div class="form-control-position">
                                                    <i class="fa fa-key"></i>
                                                </div>
                                            </fieldset>
                                            <div class="form-group">

                                            </div>
                                            <button type="submit" id="ingresarCuenta" class="btn btn-primary btn-lg btn-block" style="background-color: #002b71" style="color:#ffffff;"><i class="ft-unlock"></i> Ingresar</button> 
                                            <br /><br />
                                             <!--<a href="#myModal" data-toggle="modal" style="color:#ffffff;">Ha olvidado su contraseña?</a>--> 
                                        </form>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="">
                                       <p class="float-sm-left text-center m-0"><a href="{{route('recuperarContrasenha')}}" class="card-link">Recuperar contraseña</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
        @include('layouts/scripts')
        <script type="text/javascript">
            $(function(){

                modal = $('#recuperar-modal');
                var anchor_modal = $('#recuperar-anchor');
                var modalX = $('#recuperar-close');
                
                anchor_modal.click(function(){
                    modal.css('display','block');
                });
                
                modalX.click(function(){
                    modal.css('display','none');
                });
                $(window).click(function(e){
                    if( $(e.target).attr('id') == 'recuperar-modal') modal.css('display','none');
                });

                $('#login-form').submit(function(e){
                    var msg = "";
                    e.preventDefault();
                    //alert('yolo');
                    //return;
                    $.ajax({
                        url: $('#login-form').attr( 'action' ),
                        type: 'GET',
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        data: $('#login-form').serialize(),
                        success: function(jsonRsp){
                            if(jsonRsp.codRetorno==0){
                                var ruta = "{{asset('images/loading.gif')}}";
                                $.blockUI({
                                        centerY: 0,
                                        message: "<br><br><br><img src='"+ruta+"' class='img-circle' style='border-radius: 15%;width: 50px;height: 40px;'' alt='User Image'><h1>Ingresando...</h1><br><br>",
                                        css: {
                                            color: '#000'
                                            }
                                        });
                                        window.location.replace("{{route('home')}}");
                            }else{
                                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se puede iniciar sesión</h2><p>'+ jsonRsp.desRetorno +'.<br>Favor vuelva a intentar</p><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                                mostrarMensaje(msg);
                            }
                        },
                        error: function(){
                                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se puede acceder</h2><p><br>Favor vuelva a intentar</p><p><br><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                                mostrarMensaje(msg);
                        }
                    });
                });

                $('#recuperar-form').submit(function(e){
                    $("#myModal").modal("hide");
                    $.blockUI({
                        centerY: 0,
                        message: '<img src="images/loading.gif" alt="Procesando.." />',
                        css: {
                            color: '#000'
                            }
                    });
                    var msg = "";
                    var queryString = $('#recuperar-form').serialize();
                    console.log($('#recuperar-form').attr('action') +"?"+queryString);
                    e.preventDefault();
                    var desRetorno = '';
                    $.ajax({
                        url: $('#recuperar-form').attr('action'),
                        type: 'GET',
                        dataType: 'json',
                        data: queryString,
                        
                        success: function(jsonRsp){
                                $.unblockUI();
                                msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i>     Recuperación de Contraseña</h2><p>'+jsonRsp.desRetorno+'.</p><p><br><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                                mostrarMensaje(msg);
                                
                        },
                        error: function(jsonRsp){
                                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se ha podido realizar la solicitud</h2><p>Favor vuelva a intentar</p><p><br><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                                mostrarMensaje(msg);
                        }
                    });
                    
                });
                
                /* Mensajes de Envio y Error */
                function cerrarMensajeError() {
                  // Run the effect
                  $( "#mensaje-error" ).show( 500, callback );
                };
             
                //callback function to bring a hidden box back
                function callback() {
                  setTimeout(function() {
                    $( "#mensaje-error:visible" ).fadeOut();
                  }, 3000 );
                  $("input[name$='recuperar-email']").val("");
                };
                
                function cerrarMensajeSuccess() {
                  // Run the effect
                  $( "#recuperar-mensaje" ).show( 500, callback2 );
                };
             
                //callback function to bring a hidden box back
                function callback2() {
                  setTimeout(function() {
                    $( "#recuperar-mensaje:visible" ).fadeOut();
                  }, 3000 );
                  $("input[name$='recuperar-email']").val("");
                };
                $( "#mensaje-error" ).hide();
                $( "#recuperar-mensaje" ).hide();
            });
        </script>

</body>
<!-- END: Body-->

</html>