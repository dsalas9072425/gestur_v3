@extends('master')
@section('title', 'Circuitos Turisticos')
@section('styles')
	@parent
@endsection

@section('content')
<section class="sectiontop">
	<div class="container">
        <h2 class="title-eropamundo" style="background-color: white;margin-bottom: 0px;"><img src="{{asset('images/europamundo.png')}}" alt="Europamundo" /></h2>
        <iframe id="frame_cmp" src="http://www.europamundo.com/embed/multibuscador.aspx?opeIP=127" frameborder="0" scrolling="auto" style="width:100%; height:1200px; overflow-x: hidden;"></iframe>
        <p><a href="javascript:history.back(1);">Volver Atrás</a></p>
	</div>
</section>	
@endsection

@section('scripts')
	@include('layouts/scripts')
    @parent
    <script type="text/javascript">
    	$('#vueloFram').contents().find("LogoPanel").remove();
    </script>
@endsection
