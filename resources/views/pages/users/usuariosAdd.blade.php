@extends('master')
@section('title', 'Panel de Control')
@section('styles')
<style>
	.form-control{
			border: 1px solid #111 !important;
	}		
</style>
@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
            	@include('flash::message')
				<div class="booking-information travelo-box">
					<div>
						<h1 class="subtitle hide-medium" style="font-size: x-large;">Nuevo Usuario</h1>
						<br>
			               <form id="frmUsuarios" class="contact-form" action="{{route('doAddUser')}}" method="get" autocomplete="nope"> 
			               		{{ csrf_field() }}
					            <div class="form-group">
					                <div class="row">
					                    <div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Perfil(*)</label>
											<select name="perfil_id" required class = "form-control" class="input-text full-width required" id="perfil_id">
												@foreach($listadoPerfil as $key=>$perfil)
													<option value="{{$perfil['value']}}">{{$perfil['label']}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
					                        <label class="control-label">Agencia (*)</label>
					                            <select name="agencia_id" required class="chosen-select-deselect" id="agencia_id">
													@foreach($listadoAgencia as $key=>$agencia)
														<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
													@endforeach
												</select>
					                    </div>
					                </div>
					            </div>
					            <div class="form-group">
					                <div class="row">
					                    <div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Sucursal (*)</label>
					                            <select name="sucursal_id" required class="chosen-select-deselect" id="sucursal_id">
													@foreach($listadoSucursal as $key=>$sucursal)
														<option value="{{$sucursal['value']}}">{{$sucursal['label']}}</option>
													@endforeach
												</select>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Nombres y Apellidos (*)</label>
					                            <input type="text" required class = "Requerido form-control" name="nombreApellido" id="nombreApellido" placeholder="Nombres y Apellido" value=""/>
					                    </div>
					                </div>
					            </div>
								<div class="form-group">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Email (*)</label>
					                        <input type="email" required class = "Requerido form-control" name="email" id="email" placeholder="Email" value="" autocapitalize="off"/>										
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6" style="display:none">
											<label class="control-label">Password (*)</label>
											<input type="hidden" required class = "Requerido form-control" name="password" placeholder="Password" value=""/>	
										</div>
									</div>	
									<div class="row">	
										<div class="col-xs-12 col-sm-6 col-md-6">
					                        <label class="control-label">Agente DTP(*)</label>
					                        <select name="codigo_agente" class = "chosen-select-deselect" class="input-text full-width" id="codigo_agente">
												@foreach($listAgenteDtp as $key=>$agenteDTP)
													<option value="{{$agenteDTP['value']}}">{{$agenteDTP['label']}}</option>
												@endforeach
											</select>
					                    </div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<label class="control-label">Activo (*)</label>
											<select name="activo" required class = "form-control" class="input-text full-width required" id="activo">
												<option value="true">SI</option>
												<option value="false">NO</option>
											</select>
										</div>
					                 </div>
					            </div>
								<div class="form-group">
									<br>
									<button type="submit" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style=" background: #e2076a !important;" name="guardar" value="Guardar">Guardar</button>
								</div>
                            </form>
                    </div> 
                </div>           
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/scripts')
	<script>
		$('#agencia_id').change(function(){
				$.ajax({
					type: "GET",
					url: "{{route('getSucursales')}}",
					dataType: 'json',
					data: {
						dataSucursal: $(this).val()
			       		},
					success: function(rsp){
								$('#sucursal_id').empty()
								$('#sucursal_id').trigger("chosen:updated");
							$.each(rsp, function(key,value){
								$('#sucursal_id').append('<option value="'+value.value+'">'+value.label+'</option>');
								$('#sucursal_id').trigger("chosen:updated");
							})	
					}	
				})
			})
		$('#agencia_id').trigger('change');

		$(document).ready(function() {			
			$("#password").val('');
			
			$("#frmUsuarios").submit(function(e){
				//e.preventDefault();
				var $form = $('#frmUsuarios');
				console.log($form.attr('action') + "?" +$form.serialize());
				
				//$form.unbind('submit').submit();
				//alert('yolo');
				
			});
			
		})

		$(".chosen").chosen();
		 var config = {
		            '.chosen-select'           : {},
		            '.chosen-select-deselect'  : {allow_single_deselect:true},
		            '.chosen-select-no-single' : {disable_search_threshold:10},
		            '.chosen-select-no-results': {no_results_text:'Sin resultados!'},
		            '.chosen-select-width'     : {width:"95%"}
		        }
		for (var selector in config) {
		    $(selector).chosen(config[selector]);
		        }
	</script>
@endsection