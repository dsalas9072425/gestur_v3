@extends('master')
@section('title', 'Panel de Control')
@section('styles')
	@parent
@endsection
@section('content')
	<div class="container">
        <div class="row">
        <br>
            <div id="main" class="col-sm-12 col-md-12 info-reserva sectiontop">
            	<form id="frmAgencias" class="contact-form" action="" method="post">
            	@include('flash::message')
					<div class="booking-information travelo-box" style= "background-color: #ffffff">
						<div>
							<h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Usuarios</h1>
							<br>
							<br>
							<div class="row">
								<div class="form-group col-xs-12 col-sm-6">
									<a href="{{route('usuariosAdd')}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px;" role="button">Nuevo Usuario</a>
								</div>
							</div>
							<br>
							<div style= "overflow-x: scroll; overflow-y: hidden; background-color: #ffffff">
								<br>
								<br>
	            				<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-hover" id="listado">
		                            <thead>
		                            	<tr>
		                                	<th>ID_PORTAL</th>
											@if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia == 1)<th>BKM</th>@endif
		                                  	<th>Email</th>
											<th>Nombre</th>
		                                  	<th>Activo</th>
					                      	<th>Perfil</th>
		                                   	<th>Caducidad</th>
										 	<th>Agencia</th>
											<th>Sucursal</th>
		                                   	<th class="oculto">Ver</th>
	                              		 </tr>
		                            </thead>
		                            <tbody>
		                                @foreach($usuarios as $usuario)
			                                <tr>
			                            		<td><b>{{$usuario->id_usuario}}</b></td>
												@if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia == 1)<td>{{md5($usuario->email)}}</td>@endif
			                            	    <td>{{$usuario->email}}</td>
												<td>{{$usuario->nombre_apellido}}</td>
			                                  	<td>
			                                  	@if($usuario->activo =="S")
			                                  		<span class="label label-success">Activo</span>
			                                  	@else
			                                  		<span class="label label-danger">No Activo</span>
			                                  	@endif
			                                  	</td>
						                      	<td><b>{{ $usuario['perfil']['des_perfil']}}</b></td>
			                                   	<td>{{$usuario->fecha_validez}}</td>
			                                   	<td><b>{{$usuario->agencia->razon_social}}</b></td>
			                                   	<td>{{$usuario->sucursal->descripcion_agencia}}</td>
			                                   	<td><a href="{{route('usuariosEdit', ['id' =>$usuario->id_usuario])}}" class="btn btn-info text-center btn transaction_normal hide-small normal-button" style="padding-top: 10px; background: #e2076a !important;" role="button">Editar</a></td>
			                                </tr>	
		                                @endforeach   	
		                            </tbody>
	                       		 </table>'
	                       	</div>	 
                        </div>
                    </div>    			
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	@include('layouts/scripts')
	<script>
		$(document).ready(function() {
			$("#listado").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
		});
	</script>
@endsection