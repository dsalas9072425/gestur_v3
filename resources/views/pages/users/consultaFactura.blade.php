@extends('master')
@section('title', 'Panel de Control')
@section('styles')
@parent
@endsection
@section('content')
	<section id="content" class="gray-area">
    <div class="container">
        <div id="main">
            <div class="block sectiontop">
                <div class="tab-container style1 travelo-policies">
                   <!-- <ul class="tabs">
                       <li class="active"><a data-toggle="tab" href="#mis-reservas"><span class="glyphicon glyphicon-list"></span> Mis Facturas</a></li>
                    </ul>-->
                    <div class="tab-content">
                        @include('flash::message')
                        <div class="tab-pane fade in active" id="mis-reservas">
                            <h1 class="subtitle hide-medium" style="text-align: center; font-size: xx-large;">Mis Facturas</h1>
                            <br>
                            <br>
                            <form action="" id="frmBusqueda" method="post"> 
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm-10 col-md-10">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                <label>Desde</label>
                                                <div class="datepicker-wrap">
                                                    <input type="text" value="" name="desde" id="desde" class="input-text full-width required fecha datepicker"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-6">
                                                <label>Hasta</label>
                                                <div class="datepicker-wrap">
                                                    <input type="text" value="" name="hasta" id="hasta" class="input-text full-width required fecha datepicker"/>
                                                </div>
                                            </div>
											<div class="col-xs-12 col-sm-4 col-md-6">
												<div class="form-group">
													<label>Agencia</label>
													<select name="agencia_id" class = "chosen-select-deselect" class="input-text full-width" id="agencia">
														<option value="0">Seleccione Agencia</option>
														@foreach($listadoAgencia as $key=>$agencia)
															<option value="{{$agencia['value']}}">{{$agencia['label']}}</option>
														@endforeach
													</select>
												</div>
                                            </div>
											<div class="col-xs-12 col-sm-4 col-md-6">
												<div class="form-group">
													<label>Vendedor</label>
													<select data-placeholder="Seleccione Usuario" class="chosen-select-deselect" id="usuario" name="vendedor_id">
														<option value="0">Seleccione Usuario</option>
														@foreach($listadoUsuarios as $key=>$usuarios)
															<option value="{{$usuarios['value']}}"><b>{{$usuarios['labelUsuario']}}</b> - {{$usuarios['labelApellidoNombre']}}<br/></option> 
														@endforeach
													</select>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-2 col-md-2 fixheight">
                                        <div class="col-md-12">
                                            <label class="hidden-xs">&nbsp;</label>
                                            <button type="button" style="width: 160px;  background-color: #e2076a; height: 40px;" id= "doConsultaFactura" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> Buscar Reservas</button>
                                        </div>
                                        <div class="col-md-12">
                                            <label class="hidden-xs">&nbsp;</label>
                                            <button id="limpiarFiltros" style="width: 160px;  background-color: #fdb714; height: 40px;" type="reset" class="btn btn-info"><i class="glyphicon glyphicon-refresh"></i> Limpiar Campos</button>
                                        </div>
                                        <!--<div class="col-md-12">
                                             <label class="hidden-xs">&nbsp;</label>
                                            <button type="button" id="botonExcel" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Exportar a Excel</span></button>
                                        </div>-->
                                    </div>
								</div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12" id="divFacturas" style= "overflow-x: scroll; overflow-y: hidden;">
										<div class="table-responsive">
											<br>
											<table id="facturas" class="table table-bordered table-hover">
												<thead>
													<tr class="bgblue">
														<th style="background-color: #2d3e52;">Fecha</th>
														<th style="background-color: #2d3e52;">Descargar</th>
														<th style="background-color: #2d3e52;">Factura</th>
														<th style="background-color: #2d3e52;">Pasajero</th>
														<!--<th style="background-color: #2d3e52;">Cliente</th>-->
														<th style="background-color: #2d3e52;">Destino</th>
														<th style="background-color: #2d3e52;">Total</th>
														<!--<th style="background-color: #2d3e52;">FacturasVendedorId</th>-->
														<th style="background-color: #2d3e52;">Vendedor</th>
														
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
                                    </div>
                                </div>
								<div class="row" id="totalReservas">
								</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
    @parent
	@include('layouts/scripts')
	<script>
		$("#desde").datepicker();
		$("#hasta").datepicker();
		$(document).ready(function() {
			$('#agencia option[value="{{$agency[0]->idAgencia}}"]').attr("selected", "selected");
			$('#agencia').trigger("chosen:updated");
			
			//$(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
			$( ".datepicker" ).datepicker( "option", "dateFormat", 'dd/mm/yy' );
			//$("#desde").datepicker("setDate",$("#desde").datepicker("getDate","+1d"));
			
			$("#hasta").datepicker("setDate", new Date());
            var date2 = $('#hasta').datepicker('getDate');
            date2.setDate(date2.getDate()-31);
            $('#desde').datepicker('setDate', date2);
			
			getButton();
			
			$("#facturas").dataTable({
				 "aaSorting":[[0,"desc"]]
				});
			$("#doConsultaFactura").trigger('click');
			$("#limpiarFiltros").click(function(){
				//$("#desde").val("");
				//$("#desde").datepicker("setDate", new Date());
				
				$("#hasta").val("");
				$("#agencia").val(0);
				$('#agencia').trigger("chosen:updated");
				$("#usuario").val(0);
				$('#usuario').trigger("chosen:updated");
			});	
			$("#botonExcel").on("click", function(e){
				e.preventDefault();
    			$('#frmBusqueda').attr('action', "{{route('generarExcelFactura')}}").submit();
			});
		});	

		function getButton(){
			$("#doConsultaFactura").click(function(){
				var dataString = $("#frmBusqueda").serialize();
				$.blockUI({
		                centerY: 0,
		                message: '<div class="loadingC"><img style="width: 160px; height: 160px;" src="images/loading.gif" alt="Loading" /><br><h2>Procesando..... </h2></div>',
		                css: {
		                    color: '#000'
		                    }
		               });
				console.log("{{route('doConsultaFactura')}}"+'?'+dataString);
				$.ajax({
					type: "GET",
					url: "{{route('doConsultaFactura')}}",
					dataType: 'json',
					data: dataString,
					success: function(rsp){
									$.unblockUI();
									console.log(rsp);
									var oSettings = $('#facturas').dataTable().fnSettings();
									var iTotalRecords = oSettings.fnRecordsTotal();

									for (i=0;i<=iTotalRecords;i++) {
										$('#facturas').dataTable().fnDeleteRow(0,null,true);
									}

									$.each(rsp, function (key, item){
										var botonvoucher = "<a href="+item.url_vaucher+" download="+item.url_voucher+" target='_blank'><i class='fa fa-file-pdf-o fa-lg' style='   color: red;'></i>  Voucher</a></br>";
										var botonfactura = "<a href="+item.url_factura+"  download="+item.url_voucher+" target='_blank'><i class='fa fa-file-text-o fa-lg' style='   color: blue;'></i>  Factura</a>";
										var accion = botonvoucher +" "+botonfactura;
										var dataTableRow = [
															item.factura_fecha,
															accion,
															item.factura_numero,
															//item.ruc_cliente,
															//item.cliente_id,
															item.nombre_cliente,
															//item.factura_moneda,
															item.factura_destinacion,
															item.factura_total,
															//item.factura_vendedor_id,
															item.factura_vendedor
															];
										var newrow = $('#facturas').dataTable().fnAddData(dataTableRow);
										// set class attribute for the newly added row 
										var nTr = $('#facturas').dataTable().fnSettings().aoData[newrow[0]].nTr;
										// and parse the row:
										var nTds = $('td', nTr);
									 });
								},
							});	
					});
		}

        $(".chosen").chosen();
        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Sin resultados!'},
            '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
		
	</script>
@endsection