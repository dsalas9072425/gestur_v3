<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8">
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9">
<![endif]-->
<!--[if gt IE 9]><!--> 
<html> <!--<![endif]-->
<head>
    <!-- Page Title -->
     <title>GITSA</title>
    @include('layouts/meta')
    @include('layouts/styles2')
</head>
<body class="soap-login-page style1 body-blank" style="margin:0;/*background-image:url({{asset('images/fondo_factour.jpg')}})*/;background-repeat: no-repeat; background-position: center;background-size: cover;">
   
    <div id="page-wrapper" class="wrapper-blank">
        <section id="content">
            <div class="container">
                <div id="main" style="background-color: white; overflow: hidden;">
                    <h1 class="logo block text-center" style=" margin-top: 9%;">
                        <a href="{{route('home')}}" title="GESTUR">
                            <img src="{{asset('gestion/app-assets/images/logo/git.png')}}" style="width: 10%;"/>
                        </a>
                    </h1>
                    <div class="text-center yellow-color" style="font-size: 4em; font-weight: 450; line-height: 1em; margin-top: 20px; color:#404e67 !important;"></div>
                    <br/>
                    <div class="row">
                        <section class="content">
                              <div class="error-page">
                                <h2 class="headline text-yellow" style="color:#404e67 !important;font-size: -webkit-xxx-large;"> Hubo un inconveniente</h2>

                                <div class="error-content">
                                  <h3><i class="fa fa-warning text-yellow" style="color:yellow; font-size: -webkit-xxx-large"></i><p style="color:#404e67">"Se ha producido un problema con su petición.</p></h3>

                                  <p style="color:#404e67">
                                   
                                    Intentelo nuevamente.  <a href="../..">Volviendo al Inicio</a>
                                  </p>
                                </div>
                                <!-- /.error-content -->
                              </div>
                              <!-- /.error-page -->
                          </section>                   
                     </div>    
                </div>
            </div>
        </section>
            
            <footer id="footer">
                <div class="footer-wrapper">
                    <div class="container">
                        <div class="copyright">
                            
                        </div>
                    </div>
                </div>
            </footer>
    </div>
    
        @include('layouts/scripts')
        <script type="text/javascript">
            $(function(){

                modal = $('#recuperar-modal');
                var anchor_modal = $('#recuperar-anchor');
                var modalX = $('#recuperar-close');
                
                anchor_modal.click(function(){
                    modal.css('display','block');
                });
                
                modalX.click(function(){
                    modal.css('display','none');
                });
                $(window).click(function(e){
                    if( $(e.target).attr('id') == 'recuperar-modal') modal.css('display','none');
                });

                $('#login-form').submit(function(e){
                    var msg = "";
                    e.preventDefault();
                    //alert('yolo');
                    //return;
                    $.ajax({
                        url: $('#login-form').attr( 'action' ),
                        type: 'GET',
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        data: $('#login-form').serialize(),
                        success: function(jsonRsp){
                            if(jsonRsp.codRetorno==0){
                                $.blockUI({
                                        centerY: 0,
                                        message: '<img src="images/loading.gif" alt="Procesando.." />',
                                        css: {
                                            color: '#000'
                                            }
                                    });
                                        window.location.replace("{{route('home')}}");
                            }else{
                                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se puede iniciar sesión</h2><p>'+ jsonRsp.desRetorno +'.<br>Favor vuelva a intentar</p><p><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                                mostrarMensaje(msg);
                            }
                        },
                        error: function(){
                                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se puede acceder</h2><p><br>Favor vuelva a intentar</p><p><br><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                                mostrarMensaje(msg);
                        }
                    });
                });

                $('#recuperar-form').submit(function(e){
                    $("#myModal").modal("hide");
                    $.blockUI({
                        centerY: 0,
                        message: '<img src="images/factour.gif" alt="Procesando.." />',
                        css: {
                            color: '#000'
                            }
                    });
                    var msg = "";
                    var queryString = $('#recuperar-form').serialize();
                    console.log($('#recuperar-form').attr('action') +"?"+queryString);
                    e.preventDefault();
                    var desRetorno = '';
                    $.ajax({
                        url: $('#recuperar-form').attr('action'),
                        type: 'GET',
                        dataType: 'json',
                        data: queryString,
                        
                        success: function(jsonRsp){
                                $.unblockUI();
                                msg = '<div class="loadingC"><h2 class="textInfo"><i class="fa fa-info-circle" aria-hidden="true"></i>     Recuperación de Contraseña</h2><p>'+jsonRsp.desRetorno+'.</p><p><br><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                                mostrarMensaje(msg);
                                
                        },
                        error: function(jsonRsp){
                                msg = '<div class="loadingC"><h2 class="textRed"><i class="fa fa-times-circle" aria-hidden="true"></i> No se ha podido realizar la solicitud</h2><p>Favor vuelva a intentar</p><p><br><button id="aceptar" class="btn btn-primary">Aceptar</button></p></div>';
                                mostrarMensaje(msg);
                        }
                    });
                    
                });
                
                /* Mensajes de Envio y Error */
                function cerrarMensajeError() {
                  // Run the effect
                  $( "#mensaje-error" ).show( 500, callback );
                };
             
                //callback function to bring a hidden box back
                function callback() {
                  setTimeout(function() {
                    $( "#mensaje-error:visible" ).fadeOut();
                  }, 3000 );
                  $("input[name$='recuperar-email']").val("");
                };
                
                function cerrarMensajeSuccess() {
                  // Run the effect
                  $( "#recuperar-mensaje" ).show( 500, callback2 );
                };
             
                //callback function to bring a hidden box back
                function callback2() {
                  setTimeout(function() {
                    $( "#recuperar-mensaje:visible" ).fadeOut();
                  }, 3000 );
                  $("input[name$='recuperar-email']").val("");
                };
                $( "#mensaje-error" ).hide();
                $( "#recuperar-mensaje" ).hide();
            });
        </script>
    </body>
</html>

