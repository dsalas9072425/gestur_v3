@extends('master')

@section('title', '')
@section('styles')
    @parent
@endsection

@section('content')
<section class="sectiontop">
    <div class="container">
                <form action="" method="GET">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 alert-danger">
                            <!-- En caso de error-->
                        </div>
                        <div id="main" class="col-sm-12 col-md-12" class="sectiontop">
                            <div class="booking-information travelo-box"> 
                                <h2>Hubo un problema</h2>
                                <hr />
                                <div class="booking-confirmation clearfix">
                                    <i class="glyphicon glyphicon-warning-sign icon circle"></i>
                                    <div class="message">
                                        <h4 class="main-message">Se ha producido un problema con su petición.</h4>
                                        <p>Intentelo nuevamente.</p>
                                    </div>
                                </div>
                                </div>
                                <hr />
                            </div>
                        </div>
                    </div>
                </form>
    </div>
</section>
@endsection

@section('scripts')
    @parent
@endsection
