
<div class = "detailed-div" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">
	<div class="lista-habitacion" style="margin-left: 2px;margin-top: 0px;">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>
							Habitación
						</th>
						<!--
						<th style="min-width: 120px;">
							Régimen
						</th>
						-->
						<!--<th style="min-width: 130px;">
							Promo
						</th>-->
						<th>
							Ocupancia
						</th>
						<th>
							Precio
						</th>
						<th>
							Seleccionar
						</th>

					</tr>
				</thead>
	
				<tbody>
					@foreach($hotel->habitaciones as $habitacion)
						@foreach($habitacion->tarifas as $tarifa)
							<tr>
						<td>
						
							{{ $habitacion->desHabitacion or 'desHabitacion' }} @if($hotel->codProveedor != 4) - {{$tarifa->codConsumicion }} ({{$tarifa->desConsumicion}}) @endif
						</td>
						<!--
						<td style="min-width: 120px;">
							{{$tarifa->codConsumicion }}({{$tarifa->desConsumicion}})
						</td>
						-->

						<td>
						<?
							$var = sizeof($tarifa->ocupanciaDtp->edadesNinhos);
						?>
						{{$tarifa->ocupanciaDtp->adultos}} adultos 
						@if(sizeof($tarifa->ocupanciaDtp->edadesNinhos) != 0 ) - Niños de:
							@foreach($tarifa->ocupanciaDtp->edadesNinhos as $edad){{$loop->first ? $edad : ', '.$edad}}@endforeach
							años
						@endif
						</td>
						{{-- 
						<td>
							$habitacion->disponibilidadTotal
						</td>
						<td>
							<select data-placeholder="Cantidad de habitaciones" class="form-control">
								@for($k=1; $k<= $habitacion->disponibilidadTotal; $k++)
									<option>{{$k}}</option>
								@endfor
							</select>
						</td>
						--}}
						<td class="price-hab">
							{{round($tarifa->precio, 2)}} USD
						</td>
						<td>
							<input type="checkbox" form="frm{{$hotel->identifier}}" data="{{$tarifa->ocupanciaDtp->adultos}}-{{$var}}" name="rates[]" class="rate" value="{{$hotel->identifier}}:{{$tarifa->codReserva}}"></input>
						</td>
					</tr>
						@endforeach
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>