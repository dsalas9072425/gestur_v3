<article class="listaresultado box">
	<figure class="col-sm-5 col-md-3">
		<a title="" href="{{route('mapa')}}" class="hover-effect"><img alt="" src="{{$hotel->imagenHotel}}"></a>
	</figure>
	<div class="details col-sm-7 col-md-9">
		<div>
			<div>
				<h4 class="box-title title-list">{{$hotel->titulo->nomHotel}}<small><i class="soap-icon-departure yellow-color"></i> {{$hotel->zona}}, {{$hotel->ciudad}}</small></h4>
				<div class="amenities">
					<form method="POST" class="frmDetails" action="{{route('getDetails')}}">
						<input type="hidden" name="codHotel" value="{{$hotel->titulo->codHotel}}" />
						<input type="hidden" name="cadenaHoteles" value="{{$hotel->titulo->cadenaHotel}}" />
						{{ !empty($timeline->custom_title) ? $timeline->custom_title : "Default title" }}
						<input type="hidden" name="codProveedor" value="{{ !empty($hotel->titulo->codProveedor) ? $hotel->titulo->codProveedor : "Default Codee"}}" />
						<button type="submit" class="btn btn-small btn-success"><i class="fa fa-bars" aria-hidden="true"></i> Detalles</button>
					</form>
				</div>

			</div>
			<div>
				<div class="five-stars-container">
					<!--<span class="five-stars" style="width: {{-- $hotel->catHotel *20 --}}%;"></span>-->
					<span class="five-stars" style="width: 20%"></span>
					<!--<span>{{$hotel->catHotel}}</span>-->
				</div>
				<a href="{{route('mapa')}}"><i class="fa fa-map-marker" aria-hidden="true"></i> Ver en mapa</a>
			</div>
		</div>
		<div>
			<p>
				<span class="lista-promo">
					@if($hotel->tienePromociones)
						<i class="fa fa-gift" aria-hidden="true" data-toggle="tooltip" title="Promoción!"></i>	
					@endif
					@if($hotel->tieneOfertas)
						<i class="fa fa-percent" data-toggle="tooltip" title="Descuento!" aria-hidden="true"></i>
					@endif
				</span>
				<span class="amenities">
				@foreach($hotel->comodidadesHotel as $icono => $comodidad)
					<i class="{{$icono}}" data-toggle="tooltip" title="{{$comodidad}}"></i>
				@endforeach
				</span>
				<!--
				<span class="amenities">
					<i class="soap-icon-wifi circle"></i>
					<i class="soap-icon-fitnessfacility circle"></i>
					<i class="soap-icon-fork circle"></i>
					<i class="soap-icon-television circle"></i>
				</span>-->

			</p>
			<div>
				<span class="price"><small>DESDE</small>{{$hotel->combMasEconomica}}</span>
				<button type="submit" class="btn btn-small btn-info text-center" title="" href="#">Reservar</button>
			</div>
		</div>
	</div>
</article>
	
<div class="mostrar-ocultar-habitacion">
				
	<a class="precio-habitacion" data-toggle="collapse" data-target="mostrar-habitaciones{{$hotel->titulo->codHotel}}{{$hotel->titulo->cadenaHotel}}"> Mostrar Habitaciones Disponibles <i class="fa fa-angle-down" aria-hidden="true"></i></a>
</div>
<div id="mostrar-habitaciones{{ $hotel->titulo->codHotel }}{{$hotel->titulo->cadenaHotel}}" class="collapse lista-habitacion">
	
	@for ($i = 1; $i < 4; $i++)
		
	<h5>Habitación {{ $i }}</h5>
	
	<div class="table-responsive">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>
						Habitación
					</th>
					<th>
						Régimen
					</th>
					<th>
						Promo
					</th>
					<!-- <th>
						Habitaciones Disponible
					</th>
					<th>
						Cantidad Habitaciones
					</th> -->
					<th>
						Precio
					</th>
					<th>
						Seleccionar
					</th>

				</tr>
			</thead>
			<tbody>
			@foreach($hotel->habitaciones as $habitacion)
				<tr>
					<td>
						{{$habitacion->desHabitacion}}
					</td>
					<td>
						<span data-toggle="tooltip" title="Sólo Habitación">SH</span>
					</td>
					<td>
						<i class="fa fa-gift" aria-hidden="true" data-toggle="tooltip" title="Promoción!"></i> <i class="fa fa-percent" data-toggle="tooltip" title="Descuento!" aria-hidden="true"></i>
					</td>
					<!-- <td>
						{{$habitacion->disponibilidadTotal}}
					</td>
					<td>
						<select data-placeholder="Cantidad de habitaciones" class="form-control">
							@for($k=1; $k<= $habitacion->disponibilidadTotal; $k++)
								<option>{{$k}}</option>
							@endfor
						</select>
					</td> -->
					<td class="price-hab">
						USD 1.360
					</td>
					<td>
						<input type="radio" name="habitacion{{ $i }}" value="">
					</td>

				</tr>
			@endforeach
						
					 
			</tbody>
		</table>
	</div>
	@endfor
</div>
	
	
	