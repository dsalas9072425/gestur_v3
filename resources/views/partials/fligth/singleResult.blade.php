<article class="listaresultado box" style="padding-bottom: 2px;padding-top: 0px;border-top-left-radius: 5px;border-top-right-radius: 5px;padding-right: 0px;padding-left: 0px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px;">
   <form id="frm{{$fligth->itemNumber->itemNumberId->number}}" class="frmConfirm" method="POST" action="{{route('preconfirmationFlight')}}">
      {{--json_encode($fligth)--}}
      <div class="col-md-2 fechas" style="background-color: #d8d8d8; margin-bottom: 0px; border-top-left-radius: 5px;">
            <div class="text-small" style="text-transform: lowercase;">Desde</div>
                  {{$fligth->dateIn}}
            </div>
             <div class="col-md-4 detalle-cabecera" style="background-color: #d8d8d8; margin-bottom: 0px;">
                  <br>
                  <p style="color: #0775e2; font-size:19px; font-family: Ubuntu,sans-serif;">{{$fligth->origen_localidad}}, {{$fligth->pais_origen}}</p>
            </div>
            <div class="col-md-4 detalle-cabecera" style="background-color: #d8d8d8; margin-bottom: 0px;">
                    <br>
                    <p style="color: #0775e2; font-size:19px; font-family: Ubuntu,sans-serif;">{{$fligth->destino_localidad}}, {{$fligth->pais_destino}}</p>
            </div>
            <div class="col-md-2 fechas" style="background-color: #d8d8d8; margin-bottom: 0px; border-top-right-radius: 5px;">
                  <div class="text-small" style="text-transform: lowercase;">Hasta</div>
                  {{$fligth->dateOut}}
            </div>
            <div class="col-md-10 vuelos-detalle" style="padding-left: 0px;padding-right: 0px; border-top-width: 0px;">
                  @foreach($fligth->detalleBtn as $indice=>$detalles)
                        {{--json_encode($detalles)--}}
                        <?php
                              $timezone = new DateTimeZone('UTC');  
                              $depature = DateTime::createFromFormat('dmY', $detalles->timeDeparture);
                              $dateArrival = DateTime::createFromFormat('dmY',$detalles->timeArrival);
                        ?>
                        @if($indice == 0)
                              <div class="col-md-12 texto-aerolinea" style="font-size:11px">
                        @else
                              <div class="col-md-12 texto-aerolinea separador" style="font-size:11px">
                        @endif
                        {{$detalles->flightDetails[0]->flightInformation->aerolinea}}
                  </div>
                  <div class="col-md-2 img-aerolinea">
                        <img src="https://images.kiwi.com/airlines/64/{{$detalles->flightDetails[0]->flightInformation->companyId->marketingCarrier}}.png" class="imageHead"> 
                        <img src="https://images.kiwi.com/airlines/64/{{$detalles->flightDetails[0]->flightInformation->companyId->operatingCarrier}}.png" class="imageHead"> 
                  </div>
                  <div class="col-md-8">
                        <div class="row">
                              <div class="col-md-4 salida">
                                    <span>{{date("G:i",strtotime($detalles->timeDeparture))}}</span>
                                    <br>
                                    @if($indice == 0)
                                          <p>{{$fligth->origen}}</p>
                                    @else
                                          <p>{{$fligth->destino}}</p>
                                    @endif      
                              </div>
                              <div class="col-md-4 escalas">
                                    <?php 
                                      $totalHoras = date("G:i",strtotime($detalles->totalHoras));
                                      $totalH =  explode(":", $totalHoras);
                                    ?>
                                    <span class="cant-horas">{{$totalH[0]."h :".$totalH[1]."m"}}</span>
                                    <ul class="cant-escalas" style="height: 2px;margin-top: 3px;">
                                          @for ($i = 0; $i < $detalles->cantidad; $i++)
                                                <li class="stop-dot"></li>
                                          @endfor     
                                          <li class="cant-escalas">
                                          </li>
                                    </ul>
                                    <span class="cantidad-escalas">{{$detalles->cantidad}} Parada(s)</span><br>
                                    <!--<span class="fec-llegada">Llegada al día siguiente!</span>-->
                              </div>
                              <div class="col-md-4 llegada">
                                    <span>{{date("G:i",strtotime($detalles->timeArrival))}}</span> 
                                    <br>
                                    @if($indice == 0)
                                          {{$fligth->destino}}
                                    @else
                                          {{$fligth->origen}}
                                    @endif      

                              </div>
                        </div>
                  </div>
                  <div class="col-md-2" style="padding-left: 0px; margin-right: 0px; height: 25px;  width: 105px;">
                        <a class="btn btn-detalle" style="background-color:rgb(43, 67, 145); width: 130%; height: 31px;" data-toggle="collapse" data-target="#mas-detalles-ida{{$fligth->fligthIndex}}{{$indice}}"><i class="fa fa-angle-down" aria-hidden="true"></i> Detalles</a>
                  </div>
                  <div class="col-md-12 mas-detalle-vuelos collapse" id="mas-detalles-ida{{$fligth->fligthIndex}}{{$indice}}" >
                        @foreach($detalles->flightDetails as $indices=>$detalleVuelo)
                        {{--json_encode($detalles)--}}
                        <?php
                              $marcador = $indices + 1;
                              $timezone = new DateTimeZone('UTC'); 
                              $dateArrival = DateTime::createFromFormat('dmY', $detalleVuelo->flightInformation->productDateTime->dateOfArrival, $timezone);
                              $dateDepature = DateTime::createFromFormat('dmY', $detalleVuelo->flightInformation->productDateTime->dateOfDeparture, $timezone);
                              ?>
                              <div class="row">
                                    <div class="col-md-4">
                                          <div class="fecha">Salida {{$dateDepature->format('d F')}}</div>
                                          <div class="ciudad-hora">{{$detalleVuelo->flightInformation->location[0]->locationId}}   {{date("G:i",strtotime($detalleVuelo->flightInformation->productDateTime->timeOfDeparture))}}</div>
                                          <div>{{$detalleVuelo->flightInformation->location[0]->nombre}} - <b>{{$detalleVuelo->flightInformation->location[0]->cuidad}}, {{$detalleVuelo->flightInformation->location[0]->pais}}</b></div>
                                    </div>
                                    <div class="col-md-1 salida-fa" style="padding-left: 0px;padding-right: 0px;width: 8%;">
                                          <?php 
                                            $duracions = date("G:i",strtotime($detalleVuelo->flightInformation->duracion));
                                            $duracion =  explode(":", $duracions);
                                          ?>
                                          <i class="fa fa-chevron-right" aria-hidden="true"></i></br>
                                          <span class="fec-llegada">{{$duracion[0]."h :".$duracion[1]."m"}}</span> 
                                    </div>
                                    <div class="col-md-4">
                                          <div class="fecha">Llegada {{$dateArrival->format('d F')}}</div>
                                          <div class="ciudad-hora">{{$detalleVuelo->flightInformation->location[1]->locationId}} {{date("G:i",strtotime($detalleVuelo->flightInformation->productDateTime->timeOfArrival))}}</div>
                                          <div>{{$detalleVuelo->flightInformation->location[1]->nombre}} - <b>{{$detalleVuelo->flightInformation->location[1]->cuidad}}, {{$detalleVuelo->flightInformation->location[1]->pais}}</b></div>
                                    </div>
                                    <div class="col-md-3 detalle-avion">
                                          <img src="https://images.kiwi.com/airlines/64/{{$detalles->flightDetails[0]->flightInformation->companyId->marketingCarrier}}.png" class="imageHead"> 
                                          <div>
                                                {{$detalleVuelo->flightInformation->companyId->marketingCarrier}} {{$detalleVuelo->flightInformation->flightOrtrainNumber}}
                                                <br>
                                                <small>{{$detalleVuelo->flightInformation->tipoAsiento}}</small><br><small>{{$detalleVuelo->flightInformation->aeronave}}</small><br>
                                          </div>
                                    </div>
                                    @if($marcador < $detalles->cantidad)
                                          <div class="col-md-12 tiempo-espera">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i> Espera de <b>{{$detalles->flightDetails[$marcador]->flightInformation->horaEspera}}</b> (Cambio de Avión)
                                          </div>
                                    @endif      
                              </div>
                        @endforeach      
                  </div>                 
                  @endforeach
            </div>
            <div class="col-md-2 vuelos-precio" style="border-top-width: 0px;border-top-width: 0px;border-left-width: 0px; padding-left: 10px;">
                  <span class="price" style="padding-left: 0px; padding-right: 0px; padding-top: 30%;"><small style="font-size:11px"><b>TOTAL</b></small><br>USD {{number_format($fligth->recPriceInfo->monetaryDetail[0]->amount,2,",",".")}}</span>
                  <br>
                  <!--
                        CANAD: Cantidad de Adultos
                        CANNIN: Cantidad Niños
                        CANINF: Cantidad de Infantes
                        SEGTRA: Segmento de Tramo
                        IND: Muestra el orden en la busqueda
                        MON: Monto
                        BAG: Equipaje
                  -->
                  <button type="submit" value = "CANADU:{{$fligth->cantAdultos}}_CANNIN:{{$fligth->cantNinhos}}_CANINF:{{$fligth->cantInfante}}_IND:{{$fligth->itemNumber->itemNumberId->number}}_SEGTRA:{{$fligth->segmentos}}_MON:{{$fligth->recPriceInfo->monetaryDetail[0]->amount}}_BAG:{{$fligth->bagage}}" name= "datosVuelo" class="btn btn-info text-center btn transaction_normal hide-small normal-button vuelos" style="background-color:#e2076a;">Reservar</button>
            </div>
        </form>   
    </article>
 <br>   
</div>