	<!-- Modal Detalles -->
    <div class="modal fade" id="Detalles" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body bg-gris">
                    
                    <div class="car-descripcion-modal">

                    	<h3>Información general</h3>

	                    <ul>

		                    <li>Servicio superior</li>
							<li>1 bolso de mano permitido por persona</li>
							<li>1 maleta permitida por persona (max.dimensiones 158cm) largo+ancho+alto=158cm</li>
							<li>Recuerde que deberá presentar su bono de ida y/o vuelta al representante/guía/conductor junto con su DNI o pasaporte. Por favor, no olvide su documentación.</li>
							<li>En caso de que cambie de alojamiento durante sus vacaciones, deberá informarnos como mínimo 48 horas antes de la salida de su vuelo, de manera que podamos actualizar los datos de su traslado.</li>
							<li>En caso de que cambie su vuelo de regreso durante sus vacaciones, deberá informarnos como mínimo 48 horas antes de la salida de su vuelo, de manera que podamos actualizar los datos de su traslado.</li>
							<li>En caso de que llegue a su destino con exceso de equipaje, deberá abonar un importe adicional en concepto del peso extra no declarado.</li>
							<li>En caso de que no localice a ningún miembro de nuestro personal, llame por favor al teléfono de emergencias indicado en este bono.</li>
							<li>En caso de que no localice el punto de encuentro, llame por favor al teléfono de emergencias indicado en este bono.</li>
							<li>Los asientos para niños y elevadores no están incluidos a menos que así lo especifique tu reserva y puede conllevar un cargo adicional. Si necesitas reservarlos, por favor ponte en contacto con tu punto de venta antes de viajar.</li>

	                    </ul>

	                    <h3>Información específica</h3>

	                    <ul>

	                    	<li>Servicio con conductor de habla inglesa</li>

	                    </ul>

	                    <h3>Tiempo de espera</h3>

	                    <ul>

	                    	<li>Máxima espera del cliente: 30 minutos</li>

	                    </ul>
                    	
                    </div>
                    
                </div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-success btn-default pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Aceptar</button>
                </div>
            </div>
        </div>
    </div>