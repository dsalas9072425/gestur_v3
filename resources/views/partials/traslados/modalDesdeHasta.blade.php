	<!--Modal de -->
	<div class="modal fade" id="mapaCoordenadasDesde">
        <div id="modalMapaCoordenadas" class="modal-dialog mapaBusquedaCoordinadas">
            <div class="modal-content bg-gris">
                <div class="modal-body">
                    <div class="modal-header">
                		<h3>Traslado de salida</h3>
            		</div>
                	<div>
                	    <div class="floating-panel">
                	    	<button onclick="deleteMarkers();" type="button" id="aumentar-n" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i></button>
    					</div>
						<div id="map_canvas" class="map-tab" style="height: 500px; overflow:auto;"></div>
	                </div>
					<div class="modal-footer">
	                    <!--<a href="{{route('preconfirmationTraslado')}}" class="btn btn-primary pull-right"> Continuar</a>-->
	                </div>
                </div>
            </div>
        </div>
    </div>
	<!--Fin de Modal-->
	<!--Modal de -->
	<div class="modal fade" id="mapaCoordenadasHasta">
        <div id="modalMapaCoordenadas" class="modal-dialog mapaBusquedaCoordinadas">
            <div class="modal-content bg-gris">
                <div class="modal-body">
                    <div class="modal-header">
                		<h3>Traslado de salida</h3>
            		</div>
                	<div>
                	    <div class="floating-panel">
                	    	<button onclick="deleteMarker();" type="button" id="aumentar-n" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i></button>
    					</div>
						<div id="map_canvas2" class="map-tab" style="height: 500px; overflow:auto;"></div>
	                </div>
					<div class="modal-footer">
	                    <!--<a href="{{route('preconfirmationTraslado')}}" class="btn btn-primary pull-right"> Continuar</a>-->
	                </div>
                </div>
            </div>
        </div>
    </div>
	<!--Fin de Modal-->
