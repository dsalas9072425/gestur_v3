   <!-- Modal Info Vuelos -->
    <div class="modal fade" id="info-vuelo" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content bg-gris">
                <div class="modal-body">

                	<div class="car-descripcion-modal">
                    
	                    <p>
	                    	Por favor, introduzca los siguientes detalles para que podamos operar sus traslados correctamente.
	                    	<br>
							Si los datos introducidos no son correctos, el proveedor no se hace responsable de la correcta prestación del servicio 
	                    </p>

	                    <h3>Traslado de salida</h3>

	                    <form>

	                    	<div class="row">

	                    		<div class="col-md-6">
	                    	
			                    	<div class="form-group">
			                    		<label>Código de Vuelos</label>
			                    		<input type="text" name="cod-vuelo" class="form-control" placeholder="BA2760">
			                    	</div>

		                    	</div>

		                    	<div class="col-md-6">
		                    		<div class="form-group">
			                    		<label>Hora de llegada</label>

			                    		<div class="form-inline">
			                    		<div class="form-group">
			                    			<label class="sr-only" for="horal-vuelo">Hora del Vuelo:</label>
				                    		<select class="form-control" id="hora-vuelo">
											    <option>00</option>
												<option>01</option>
											    <option>02</option>
											    <option>03</option>
											    <option>04</option>
												<option>05</option>
											    <option>06</option>
											    <option>07</option>
											    <option>08</option>
												<option>09</option>
											    <option>10</option>
											    <option>11</option>
											    <option>12</option>
												<option>13</option>
											    <option>14</option>
											    <option>15</option>
											    <option>16</option>
												<option>17</option>
											    <option>18</option>
											    <option>19</option>
											    <option>20</option>
												<option>21</option>
											    <option>22</option>
											    <option>23</option>
											</select>
										</div>
										<div class="form-group">
			                    			<label class="sr-only" for="horal-vuelo">Minuto del Vuelo:</label>
				                    		<select class="form-control" id="minuto-vuelo">
											    <option>00</option>
												<option>05</option>
											    <option>10</option>
											    <option>05</option>
											    <option>20</option>
												<option>25</option>
											    <option>30</option>
											    <option>35</option>
											    <option>40</option>
												<option>45</option>
											    <option>50</option>
											    <option>55</option>
											</select>
										</div>
			                    	</div>

			                    	</div>
			                    	
		                    
		                    	</div>

	                    	</div>

	                    </form>

	                </div>
                    
                </div>
				<div class="modal-footer">
                    <a href="{{route('preconfirmationTraslado')}}" class="btn btn-primary pull-right"> Continuar</a>
                </div>
            </div>
        </div>
    </div>
