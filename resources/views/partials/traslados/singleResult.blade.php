<div class="car-list listing-style3 car">
	    <article class="box listaresultado">
	        <figure class="col-xs-3">
	            <span><img alt="" src="images/traslado/coche.png"></span>
	        </figure>
	        <div class="details col-xs-9 clearfix">
	            <div class="col-sm-5">
	                <div class="clearfix">
	                    <h4 class="box-title">Privado - Premium (Coche)</h4>
	                </div>
	                <div class="car-detalle">
	                    <ul>
	                        <li><i class="fa fa-car" aria-hidden="true"></i> Servicio puerta a puerta</li>
	                        <li><i class="fa fa-calendar" aria-hidden="true"></i> Disponible 24/7</li>
	                        <li><i class="fa fa-clock-o" aria-hidden="true"></i> Máxima espera del cliente : <b>30 minutos</b></li>
	                    </ul>
	                </div>
	            </div>
	            <div class="col-xs-6 col-sm-5 car-descripcion">
	                <ul>
	                    <li>Servicio con conductor de habla inglesa </li>
	                    <li>Transporte exclusivo </li>
	                    <li>Servicio Meet & Greet </li>
	                    <li>1 bolso de mano permitido por persona</li>
	                </ul>
	                <a data-toggle="modal" href="#Detalles" class="btn-mas-detalle"><i class="fa fa-angle-down" aria-hidden="true"></i> Más Detalles</a>
	            </div>
	            <div class="action col-xs-6 col-sm-2 car-precio">
	                <span class="price">73.33 US$</span>
	                <br>
	                <a data-toggle="modal" href="#info-vuelo" class="btn btn-primary">Seleccionar</a>
	            </div>
	        </div>
	    </article>
</div>
