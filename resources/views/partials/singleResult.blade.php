<article class="lista-principal box" style="padding-bottom: 2px;padding-top: 5px; border-top-left-radius: 5px; border-top-right-radius: 5px; display: block;">
	<input type="hidden" name="hotelIndex" value="{{$hotel->hotelIndex}}" />
	<div class="row" style="padding-top: 2px;">
		<div class="details col-sm-12 col-md-3" style="padding-left: 0px; padding-right: 0px; padding-top: 0px; width: 96%;max-width: 210px; margin: 0 auto;">
			<form method="GET" class="frmDetails" action="{{route('details')}}" target="_blank">
				<input type="hidden" name="codHotel" value="{{$hotel->codHotel}}" />
				<!--em: p/ det-->
				<input type="hidden" name="sid" value= "{{$sid}}" />
				<input type="hidden" name="codProveedor" value="{{$hotel->codProveedor}}" />
				<!--<input type="hidden" name="nomHotel" value="{{$hotel->nomHotel}}" />-->
				<!--<input type="hidden" name="monCod" value="{{round($hotel->comMasEconomicaHotel, 2)}} {{$hotel->moneda}}" />-->
				<img style="height: 140px;width: 210px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px; cursor: pointer;" onclick="$(this).closest('form').submit()" onerror="this.src='images/dtpmundo-list.jpg'" alt="" 
					@if(!isset($hotel->imagenesHotel[0]))

						src = "images/dtpmundo-list.jpg"
					@else
						src = "{{$hotel->imagenesHotel[0]}}"
					@endif
					/>
			</form>
			<!--<img style="height: 10%;  width: 95%;" onerror="this.src='images/dtpmundo-list.jpg'" alt="" src="https://cdn.hotelbeds.com/giata/00/005273/005273a_hb_ba_004.jpg">-->
		</div>		
		<div class="details col-sm-12 col-md-7" style="padding-right: 0px;padding-left: 0px;padding-top: 0px;">
				<br>
				<h3 class="box-title title-list details col-sm-12 col-md-9" style="padding-top: 0px;">
					<a href="{{route('details')}}?codHotel={{$hotel->codHotel}}&codProveedor={{$hotel->codProveedor}}" style="width: 100%; padding-top: 0px;" target="_blank"><span style="width: 100%;padding-top: 0px;padding-bottom: 0px;padding-right: 0px;">
						{{$hotel->nomHotel}} 
						
						@if(is_numeric($hotel->catHotel))
							@for($k=1 ; $k<= $hotel->catHotel ; $k++)
								<i class='glyphicon glyphicon-star' style='color:#fdb714;'></i>
							@endfor
						@else
						@endif
					</span></a>
					<br>
					<a href="{{route('mapa')}}?codHotel={{$hotel->codHotel}}&codProveedor={{$hotel->codProveedor}}&sid={{$sid}}" style="width: 100%; padding-top: 0px;" target="_blank"><i class="fa fa-map-marker" style='color:#fdb714; font-size: 16px'></i><small style="color:#e2076a;padding-bottom: 0px; font-size: 11px; letter-spacing: 0.00em;padding-left: 0px;">{{$hotel->dirHotel or ''}}</small>
					<div class="col-md-5 divPromo" style="padding-top: 0px; background-color: transparent; width: 90%; padding-left: 0px; border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-left-radius: 3px; border-bottom-right-radius: 3px;display: block;">
						<div class="row" style="padding-top: 5px;">
							@if ($hotel->promocion==true)
							<div class="col-md-4" style="padding-left: 10px;">
							  <img alt="promocion" style="width: 40px;padding-right: 7px;" src="images/promocion.png" title="En Promoción">
							</div>  
							@endif
							{{--@if ($hotel->sugerido==true)--}}
							<!--<div class="col-md-4" style="padding-left: 10px;">
							  <img alt="sugerido" style="width: 29px;padding-right: 7px;" src="images/sugerido.png" title="Sugerido por DTP Mundo">
							</div> --> 
							{{--@endif--}}
							<div class="col-md-4" style="padding-left: 10px;">
								<br>
								<br>
							</div>  
						</div>
					</div></a>	
				</h3>
				@if(Session::get('datos-loggeo')->datos->datosUsuarios->idAgencia == config('constants.adminDtp'))
					<img alt="" style="width: 90px; background-color: white;" src="images/proveedores/{{$hotel->codProveedor}}.png">
				@endif
				@if ($hotel->sugerido==true)
					<img alt="sugerido" style="width: 47px;padding-right: 7px;margin-top: 5%;height: 50px;" src="images/sugerido.png" title="Sugerido por DTP Mundo">
				@endif	
			</div>
			<div class="details col-sm-12 col-md-2" style="padding-top: 0px; padding-left: 0px; padding-right: 0px;">
				<div class="amenities" style="padding-left: 5px; padding-right: 5px;">
					<small>&nbsp;</small>
					<span class="price" style="padding-left: 0px; padding-right: 0px; padding-top: 15%;"><small>DESDE<br></small>USD {{round($hotel->comMasEconomicaHotel, 2)}}<small><br>Por Habitación</small></span>
				</div>
				<form id="frmPreReserva{{$hotel->codHotel}}" class="frmPreReserva" method="GET" action="{{route('getPreconfirmation')}}" style="background-color: white;">
					<a id="{{$hotel->codHotel}}" class="btn btn-info text-center btn transaction normal hide-small normal-button btnPreReserva btn-mas-request" disabled="disabled" style="padding-left: 30px;font-size: large; padding-top: 7px; padding-bottom: 6px;">VERIFICAR</a> 
				</form>			
			</div>
		</div>
	</div>
	<!--<div>
	</div>-->
</article>
<div class="mostrar-ocultar-detalle" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;">	
	<div style="width: 180px;"><a class="detalle" data-toggle="collapse" data-target="mostrar-habitaciones{{$hotel->identifier}}">Seleccionar Habitación <i class="fa fa-angle-down" aria-hidden="true"></i></a></div>
</div>
<article class="listaresultado box collapse detailedArt {{$hotel->codHotel}}" id="mostrar-habitaciones{{ $hotel->identifier}}"  style="height: auto;border-left-width: 0px;border-right-width: 0px;border-bottom-width: 0px;" >
	{{--@if($hotel->nombreProveedor != 'travelport')--}}
		@include('partials/singleResultDetails')
	{{--@endif--}}
</article>

