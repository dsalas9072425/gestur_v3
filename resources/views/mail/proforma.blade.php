<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reporte de Reservas a Pagar</title>
</head>
<body>
    <div class="content">			
        <table>				
            <tr>					
                <td><h3>Gestur - Proforma <?php echo $estado; ?></h3>
                    <p class="lead">Estimado/a <?php echo $nombre; ?>.</p>
                    @if($estado == 'a Facturar')
                        <p class="lead"> La proforma Nº <?php echo $proforma; ?> debe de ser facturada. </p>
                    @endif
                    @if($estado == 'Facturada')
                        <p class="lead"> La proforma Nº <?php echo $proforma; ?> fue facturada. </p>
                    @endif
                    @if($estado == 'Rechazada')
                        <p class="lead"> La proforma Nº <?php echo $proforma; ?> ha sido rechazada. </p>
                    @endif
                    <p class="lead"> Usuario:  <?php echo $usuario; ?></p>
                    <p class="lead"> Asistente:  <?php echo $asistente; ?></p>
                    <p class="lead"> Vendedor:  <?php echo $vendedor; ?></p>
                    <p class="lead"> Cliente:  <?php echo $cliente; ?></p>					
                    <table class="social" width="100%">							
                        <tr>								
                            <td>									
                                <table align="left" class="column">										
                                    <tr>											
                                        <td style="width: 140px;">																	
                                            <p class=""><a href="https://gestur.git.com.py" target="_blank"><img src="<?php echo $logo_empresa;?>"/></a></p>											
                                        </td>
                                    </tr>									
                                </table>
                                <table align="left" class="column">										
                                    <tr>											
                                        <td>	 	
                                            <h4 class=""><?php echo $empresa;?></h4>													
                                        </td>										
                                    </tr>									
                                </table>									
                                <span class="clear"></span>									
                            </td>							
                        </tr>						
                    </table>					
                </td>				
            </tr>			
        </table>			
    </div>	
</body>
</html>