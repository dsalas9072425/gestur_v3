<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>[Gestur] Alerta Error Job</title>
</head>
<body>
    <div class="content">			
        <table>				
            <tr>					
                <td><h3>Gestur -  Alerta Error Job</h3>
                    <p class="lead">Ocurrio un error en el proceso: {{$proceso}}, con el sgte mensaje : {{$mensaje}}</p>
                    <p><a href="https://gestur.git.com.py/log-viewer/logs">Revisar Logs</a></p>						
                    <table class="social" width="100%">							
                        <tr>								
                            <td>									
                                <table align="left" class="column">										
                                    <tr>											
                                        <td style="width: 140px;">																
                                            <p class=""><a href="https://gestur.git.com.py" target="_blank"><img src="https://gestur.git.com.py/gestion/app-assets/images/logo/git.png"/></a></p>											
                                        </td>
                                    </tr>									
                                </table>
                                <table align="left" class="column">										
                                    <tr>											
                                        <td>	 	
                                            <h4 class="">GIT</h4>													
                                        </td>										
                                    </tr>									
                                </table>									
                                <span class="clear"></span>									
                            </td>							
                        </tr>						
                    </table>					
                </td>				
            </tr>			
        </table>			
    </div>	
</body>
</html>