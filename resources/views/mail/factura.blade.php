<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reporte de Reservas a Pagar</title>
</head>
<body>
    <div class="content">			
        <table>				
            <tr>					
                <td><h3>Gestur - Factura</h3>
                  
                    <p class="lead">Estimado/a <?php echo $nombre;?>.</p>
                    <p class="lead"> La factura Nº<?php echo $factura;?>, de fecha <?php echo $fecha_emision;?> y monto <?php echo $total_factura;?> <?php echo $moneda;?>; ha sido generada. </p>
                    
                    <table class="social" width="100%">							
                        <tr>								
                            <td>									
                                <table align="left" class="column">										
                                    <tr>											
                                        <td style="width: 140px;">																	
                                            <p class=""><a href="https://gestur.git.com.py" target="_blank"><img src="<?php echo $logo_empresa;?>"/></a></p>											
                                        </td>
                                    </tr>									
                                </table>
                                <table align="left" class="column">										
                                    <tr>											
                                        <td>	 	
                                            <h4 class=""><?php echo $empresa;?></h4>													
                                        </td>										
                                    </tr>									
                                </table>									
                                <span class="clear"></span>									
                            </td>							
                        </tr>						
                    </table>					
                </td>				
            </tr>			
        </table>			
    </div>	
</body>
</html>