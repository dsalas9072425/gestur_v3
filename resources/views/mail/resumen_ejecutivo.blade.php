<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Resumen Ejecutivo</title>
	<style>
        body {
    font-family: Arial, sans-serif;
    font-size: 16px;
    line-height: 24px;
    color: #333;
    background-color: #f7f7f7;
    margin: 0;
    padding: 0;
}

.container {
    width: 600px;
    margin: 0 auto;
    padding: 30px;
    background-color: #fff;
    box-shadow: 0 3px 5px rgba(0,0,0,0.1);
}

.logo {
	display: block;
	margin: 0 auto 20px;
	max-width: 200px;
	height: auto;
    text-align: center;
	}

h1 {
    font-size: 28px;
    line-height: 36px;
    margin: 0 0 20px;
    color: #333;
}

p {
    margin: 0 0 20px;
}

.footer {
    margin-top: 30px;
    padding-top: 20px;
    border-top: 1px solid #ccc;
    text-align: center;
    font-size: 12px;
    color: #666;
}

.footer img {
    max-width: 100px;
    height: auto;
    margin-right: 10px;
    vertical-align: middle;
}

.text-center{
    text-align: center;
}

.mt{
    margin-top: 20px;
}
    </style>
</head>
<body>
	<div class="container">
        <img src="{{$empresa_logo}}" alt="Logo grande" class="logo">
		<h1 class="text-center mt">Resumen Ejecutivo</h1>
		<p>Se adjunta el resumen de la fecha {{date('d/m/Y')}} en el correo para la empresa {{$denominacion_empresa}}</p>
		<div class="footer">
			<img src="https://gestur.git.com.py/gestion/app-assets/images/logo/git.png" alt="Logo"><br>
			<span>GIT</span>
		</div>
	</div>
</body>
</html>
