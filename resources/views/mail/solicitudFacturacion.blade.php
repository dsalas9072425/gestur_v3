<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Solicitud de Facturacion Parcial</title>
</head>
<body>
    <div class="content">			
        <table>				
            <tr>					
                <td><h3>Gestur </h3>
                    <p class="lead">Estimado/a <?php echo $nombre; ?>.</p>
                    <p class="lead"> La proforma Nº <?php echo $proforma; ?> debe de ser facturada de forma parcial. </p>
                    <p class="lead"> Fecha de Solicitud:  <?php echo $fecha_pedido; ?></p>					
                    <p class="lead"> Usuario que solicito la facturación:  <?php echo $usuario_nombre; ?></p>
                    <p class="lead"> Nombre Cliente:  <?php echo $cliente_nombre; ?></p>
                    <p class="lead"> Ruc. Cliente:  <?php echo $cliente_ruc; ?></p>
                    <p class="lead"> Monto a Facturar (<?php echo $moneda; ?>):  <?php echo $total_factura_parcial; ?></p>					
                    <table class="social" width="100%">							
                        <tr>								
                            <td>									
                                <table align="left" class="column">										
                                    <tr>											
                                        <td style="width: 140px;">																
                                            <p class=""><a href="https://gestur.git.com.py" target="_blank"><img src="http://notificaciones.dtpmundo.com/Assets/images/dtp.png"/></a></p>											
                                        </td>
                                    </tr>									
                                </table>
                                <table align="left" class="column">										
                                    <tr>											
                                        <td>	
                                            <h4 class="">GIT</h4>														
                                        </td>										
                                    </tr>									
                                </table>									
                                <span class="clear"></span>									
                            </td>							
                        </tr>						
                    </table>					
                </td>				
            </tr>			
        </table>			
    </div>	
</body>
</html>