<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reporte de Reservas a Pagar</title>
</head>
<body>
    <div class="content">			
        <table>				
            <tr>					
                <td><h3>Gestur - Reserva facturada y cancelada.</h3>
                  
                    <p class="lead">Estimado/a <b><?php echo $nombre;?></b>.</p>
                    <p class="lead"> La Reserva <?php echo $reserva_reference;?> ha sido cancelada que forma parte de la factura N° <?php echo $factura;?>, abajo se especifican los detalles: </p>
                    <p><strong>Identificador de Reserva : </strong> <?php echo $reserva_reference; ?></p>		
                    <p><strong>Ejecutivo: </strong><?php echo $ejecutivo; ?></p>		
                    <p><strong>Nombre del Cliente: </strong> <?php echo $cliente; ?></p>	
                    <p><strong>Nombre del Proveedor: </strong><?php echo $proveedor; ?></p>	
                    <p><strong>Fecha de la Reserva: </strong> <?php echo date('d/m/Y', strtotime($fecha_creacion)); ?></p>		
                    <p><strong>Destino de Reserva: </strong><?php echo $destino; ?></p>
                    <p><strong>Check-in </strong><?php echo date('d/m/Y', strtotime($checkin)); ?> <strong>Check-out </strong><?php echo date('d/m/Y', strtotime($checkout)); ?></p>	
                    <p><strong>Total Factura: </strong><?php echo $total_factura; ?> <?php echo $moneda; ?></p>
                    <table class="social" width="100%">							
                        <tr>								
                            <td>									
                                <table align="left" class="column">										
                                    <tr>											
                                        <td style="width: 140px;">																	
                                            <p class=""><a href="https://gestur.git.com.py" target="_blank"><img src="<?php echo $logo_empresa;?>"/></a></p>											
                                        </td>
                                    </tr>									
                                </table>
                                <table align="left" class="column">										
                                    <tr>											
                                        <td>	 	
                                            <h4 class=""><?php echo $empresa;?></h4>													
                                        </td>										
                                    </tr>									
                                </table>									
                                <span class="clear"></span>									
                            </td>							
                        </tr>						
                    </table>					
                </td>				
            </tr>			
        </table>			
    </div>	
</body>
</html>