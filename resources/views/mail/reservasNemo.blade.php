<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reporte de Reservas a Pagar</title>
</head>
<body>
    <div class="content">			
        <table>				
            <tr>					
                <td><h3>Gestur <?php 
                                if(isset($titulo)){
                                     echo '-'.$titulo; 
                                }
                            ?></h3>
                    <p class="lead">Estimado/a <?php echo $nombre; ?>;</p>
                    <p class="lead">Se ha realizado una reserva, abajo se especifican los detalles:</p>
                    <br/>	
                    <dl>		
                    <p><strong>Identificador de Reserva : </strong> <?php echo $reserva_reference; ?></p>		
                    <p><strong>Ejecutivo DTP: </strong><a href="mailto:<?php echo $email; ?>"><?php echo $nombre; ?></a> </p>		
                    <p><strong>Nombre del Cliente: </strong> <?php echo $cliente; ?></p>	
                    <p><strong>Nombre del Proveedor: </strong><?php echo $proveedor; ?></p>	
                    <p><strong>Fecha de la Reserva: </strong> <?php echo date('d/m/Y', strtotime($fecha_creacion)); ?></p>		
                    <p><strong>Destino de Reserva: </strong><?php echo $destino; ?></p>
                    <p><strong>Check-in </strong><?php echo date('d/m/Y', strtotime($checkin)); ?> <strong>Check-out </strong><?php echo date('d/m/Y', strtotime($checkout)); ?></p>	
                    </dl><br/><br/>	</p>					
                    <table class="social" width="100%">							
                        <tr>								
                            <td>									
                                <table align="left" class="column">										
                                    <tr>											
                                        <td style="width: 140px;">																	
                                            <p class=""><a href="https://gestur.git.com.py" target="_blank"><img src="http://notificaciones.dtpmundo.com/Assets/images/dtp.png"/></a></p>											
                                        </td>
                                    </tr>									
                                </table>
                                <table align="left" class="column">										
                                    <tr>											
                                        <td>															
                                        <h4 class="">DTP</h4>													
                                        </td>										
                                    </tr>									
                                </table>									
                                <span class="clear"></span>									
                            </td>							
                        </tr>						
                    </table>					
                </td>				
            </tr>			
        </table>			
    </div>	
</body>
</html>