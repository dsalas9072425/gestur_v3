<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reporte de Reservas a Pagar</title>
</head>
<body>
    <div class="content">			
        <table>				
            <tr>					
                <td><h3>Gestur - Reporte de Reservas a Pagar</h3>
                    <?php 
                        $fecha = date_create(date('Y-m-d'));
                        date_add($fecha, date_interval_create_from_date_string("10 day"));
                        $fecha_validaz = date_format($fecha,"d/m/Y");
                    ?>
                    <p class="lead">Estimado/a <?php echo $nombre; ?>;</p>
                    <p class="lead"> Este es el reporte de reservas a pagar a la fecha <?php echo $fecha_validaz;?>.</p>
                    <p class="lead"> Adjunto el reporte de reservas en gastos de los próximos 10 días.</p>					
                    <table class="social" width="100%">							
                        <tr>								
                            <td>									
                                <table align="left" class="column">										
                                    <tr>											
                                        <td style="width: 140px;">																
                                            <p class=""><a href="https://gestur.git.com.py" target="_blank"><img src="http://notificaciones.dtpmundo.com/Assets/images/dtp.png"/></a></p>											
                                        </td>
                                    </tr>									
                                </table>
                                <table align="left" class="column">										
                                    <tr>											
                                        <td>
                                            <h5 class="">GIT</h5>															
                                        </td>										
                                    </tr>									
                                </table>									
                                <span class="clear"></span>									
                            </td>							
                        </tr>						
                    </table>					
                </td>				
            </tr>			
        </table>			
    </div>	
</body>
</html>