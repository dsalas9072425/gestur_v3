<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reporte de Procesos</title>
</head>
<body>
    <div class="content">			
        <table>				
            <tr>					
                <td><h3>Gestur - Asiento Nro <?php echo $id_asiento; ?></h3>
                  
                    <p class="lead">Estimado/a <?php echo $nombre;?>.</p>
                    <p class="lead">El asiento generado por este proceso no está balanceado, favor pongase en contacto con Administración/Contabilidad para su verificación.. </p>
                    <p class="lead"> Gracias por operar con DTP</p>
                    <table class="social" width="100%">							
                        <tr>								
                            <td>									
                                <table align="left" class="column">										
                                    <tr>											
                                        <td style="width: 140px;">																	
                                            <p class=""><a href="https://gestur.git.com.py" target="_blank"><img src="https://gestur.git.com.py/gestion/app-assets/images/logo/git.png"/></a></p>											
                                        </td>
                                    </tr>									
                                </table>
                                <table align="left" class="column">										
                                    <tr>											
                                        <td>	 	
                                            <h4 class="">GIT</h4>													
                                        </td>										
                                    </tr>									
                                </table>									
                                <span class="clear"></span>									
                            </td>							
                        </tr>						
                    </table>					
                </td>				
            </tr>			
        </table>			
    </div>	
</body>
</html>