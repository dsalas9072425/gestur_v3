<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>[Gestur] Alerta Temprana Job</title>


</head>
<body>
    <div class="content">			
        <table>				
            <tr>					
                <td>
                    <h3>Gestur - Alerta Temprana Job</h3>
                    <p class="lead"></p>
                    <p><a href="https://gestur.git.com.py/log-viewer/logs">Revisar Logs</a></p>

                    @if(count($tickets_oasis))
                        <hr>
                        <p>Tickets de OASIS con IVA</p>
                        <table style="border-collapse: collapse;">
                            <thead>
                                <tr>
                                <th style="border: 1px solid #ddd;">Nro Ticket</th>
                                <th style="border: 1px solid #ddd;">PNR</th>
                                <th style="border: 1px solid #ddd;">Moneda</th>
                                <th style="border: 1px solid #ddd;">Facial</th>
                                <th style="border: 1px solid #ddd;">Total</th>
                                <th style="border: 1px solid #ddd;">IVA</th>
                                <th style="border: 1px solid #ddd;">Tasas</th>
                                <th style="border: 1px solid #ddd;">Fecha Emision</th>
                                <th style="border: 1px solid #ddd;">Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tickets_oasis as $ticket)
                                <tr>
                                <td style="border: 1px solid #ddd;">{{ $ticket->numero_amadeus }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->pnr }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->currency_code }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->facial }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->total_ticket }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->iva }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->tasas }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->fecha_emision }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->estado }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif

                    @if(count($tickets_copa_sin_iva))
                        <hr>
                        <p>Tickets COPA DTP sin IVA</p>
                        <table style="border-collapse: collapse;">
                            <thead>
                                <tr>
                                <th style="border: 1px solid #ddd;">Nro Ticket</th>
                                <th style="border: 1px solid #ddd;">PNR</th>
                                <th style="border: 1px solid #ddd;">Moneda</th>
                                <th style="border: 1px solid #ddd;">Facial</th>
                                <th style="border: 1px solid #ddd;">Total</th>
                                <th style="border: 1px solid #ddd;">IVA</th>
                                <th style="border: 1px solid #ddd;">Tasas</th>
                                <th style="border: 1px solid #ddd;">Fecha Emision</th>
                                <th style="border: 1px solid #ddd;">Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tickets_copa_sin_iva as $ticket)
                                <tr>
                                <td style="border: 1px solid #ddd;">{{ $ticket->numero_amadeus }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->pnr }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->currency_code }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->facial }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->total_ticket }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->iva }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->tasas }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->fecha_emision }}</td>
                                <td style="border: 1px solid #ddd;">{{ $ticket->estado }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif

                    @if(count($tickets_creados_hoy))
                    <hr>
                    <p>Tickets que cayeron HOY {{date('d/m/Y')}}</p>
                    <table style="border-collapse: collapse;">
                        <thead>
                            <tr>
                            <th style="border: 1px solid #ddd;">Nro Ticket</th>
                            <th style="border: 1px solid #ddd;">PNR</th>
                            <th style="border: 1px solid #ddd;">Moneda</th>
                            <th style="border: 1px solid #ddd;">Facial</th>
                            <th style="border: 1px solid #ddd;">Total</th>
                            <th style="border: 1px solid #ddd;">IVA</th>
                            <th style="border: 1px solid #ddd;">Tasas</th>
                            <th style="border: 1px solid #ddd;">Fecha Emision</th>
                            <th style="border: 1px solid #ddd;">Empresa</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tickets_creados_hoy as $ticket)
                            <tr>
                            <td style="border: 1px solid #ddd;">{{ $ticket->numero_amadeus }}</td>
                            <td style="border: 1px solid #ddd;">{{ $ticket->pnr }}</td>
                            <td style="border: 1px solid #ddd;">{{ $ticket->currency_code }}</td>
                            <td style="border: 1px solid #ddd;">{{ $ticket->facial }}</td>
                            <td style="border: 1px solid #ddd;">{{ $ticket->total_ticket }}</td>
                            <td style="border: 1px solid #ddd;">{{ $ticket->iva }}</td>
                            <td style="border: 1px solid #ddd;">{{ $ticket->tasas }}</td>
                            <td style="border: 1px solid #ddd;">{{ $ticket->fecha_emision }}</td>
                            <td style="border: 1px solid #ddd;">{{ $ticket->empresa }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

                    <table class="social" width="100%">							
                        <tr>								
                            <td>									
                                <table align="left" class="column">										
                                    <tr>											
                                        <td style="width: 140px;">																
                                            <p class=""><a href="https://gestur.git.com.py" target="_blank"><img src="https://gestur.git.com.py/gestion/app-assets/images/logo/git.png"/></a></p>											
                                        </td>
                                    </tr>									
                                </table>
                                <table align="left" class="column">										
                                    <tr>											
                                        <td>	 	
                                            <h4 class="">GIT</h4>													
                                        </td>										
                                    </tr>									
                                </table>									
                                <span class="clear"></span>									
                            </td>							
                        </tr>						
                    </table>					
                </td>				
            </tr>			
        </table>			
    </div>	
</body>
</html>