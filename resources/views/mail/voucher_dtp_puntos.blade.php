<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Office</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta  name="viewport" content="width=display-width, initial-scale=1.0, maximum-scale=1.0," />
        
        <link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900,100italic,300italic,400italic,500italic,700italic,900italic' rel='stylesheet' type='text/css' />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,300,400,600,700,800' rel='stylesheet' type='text/css' />
        
        <style type="text/css">		
            html { width: 100%; }
            body {margin:0; padding:0; width:100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
            img { display: block !important; border:0; -ms-interpolation-mode:bicubic;}

            .ReadMsgBody { width: 100%;}
            .ExternalClass {width: 100%;}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
            .images {display:block !important; width:100% !important;}
                
            .Heading {font-family:'Roboto', Arial, Helvetica Neue, Helvetica, sans-serif !important;}
            .MsoNormal {font-family:'Open Sans', Arial, Helvetica Neue, Helvetica, sans-serif !important;}
            p {margin:0 !important; padding:0 !important;}					
            .display-button td, .display-button a {font-family:'Open Sans', Arial, Helvetica Neue, Helvetica, sans-serif !important;}
            .display-button a:hover {text-decoration:none !important;}
            
            /* MEDIA QUIRES */
             @media only screen and (min-width:799px)
            {
                .saf-table {display:table !important;}
                .width-auto {width: auto !important;}
                .width600 {width:600px;}	
                .width800 {width:800px !important; max-width:800px !important;}
            }
            
             @media only screen and (max-width:799px)
            {
                 body {width:auto !important;}
                .display-width {width:100% !important;}	
                .res-padding {padding:0 20px !important;}	
                .display-width-inner {width:600px !important;}
                .res-center {text-align:center !important; width:100% !important; }
                .width800 {width:100% !important; max-width:100% !important;}
                .width50percent {width:50% !important; max-width:50% !important;}
           }
           
           @media only screen and (max-width:768px)
        {	
            .width768{
                max-width:684px !important;
            }
            .child1-width{
                width:56% !important;
                max-width:56% !important;
            }
            .child2-width{
                width:44% !important;
                max-width:44% !important;
            }
            .full-width-height
            { 	
                padding-top:50px !important;
                padding-bottom:50px !important;
            }
        }

            @media only screen and (max-width:680px)
        {	
            .child1-width{
                width:50% !important;
                max-width:50% !important;
            }
            .child2-width{
                width:50% !important;
                max-width:50% !important;
            }
            .res-padding-left{
                padding-left: 40px !important;
            }
            .padding-hide
            {	
                padding-bottom:0px !important;
            }
            .res-attract-height {
                padding: 20px 10px 0 10px !important;
            }
            .full-width-height
            { 	
                padding-top:35px !important;
                padding-bottom:25px !important;
            }
        }
        @media only screen and (max-width:660px)
        {
            .child1-width{
                width:50% !important;
                max-width:50% !important;
            }
            .child2-width{
                width:50% !important;
                max-width:50% !important;
            }
            .res-padding-left{
                padding-left: 30px !important;
            }
            .res-attract-height {
                padding: 20px 10px 0 10px !important;
            }
            .full-width-height
            { 	
                padding-top:30px !important;
                padding-bottom:20px !important;
            }
        }
        @media only screen and (max-width:640px)
        {
            .res-attract-height {
                padding: 20px 10px 0 10px !important;
            }
            .child1-width, .child2-width{
                width:50% !important;
                max-width:50% !important;
            }
            .res-padding-left{
                padding-left: 20px !important;
            }
            .full-width-height
            { 	
                padding-top:25px !important;
                padding-bottom:15px !important;
            }
        }
            
            @media only screen and (max-width:639px)
            {
                .display-width-inner, .display-width-child {width:100% !important;}
                .hide-bar {display:none !important;}
                td[class="height-hidden"] {display:none !important;}
                span.unsub-width {width:100% !important;
                display:block !important;}
                span.txt-copyright{ padding-bottom:10px !important;}
                .about-padding-left {padding: 0 10px !important;}
                .txt-center {text-align:center !important;}
                .image-center{margin:0 auto !important; display:table !important;}
                .butn-center{margin:0 auto; display:table;}
                .butn-left{margin:0 auto 0 0; display:table;}
                .footer-2-width {width:170px !important;}
                .feature-width {width:260px !important;}
                .full-width-height { padding-bottom:60px !important;}
                .fun-height { padding-bottom:60px !important;}
                .height10 {height:10px !important; line-height:10px !important;}
                .height15 {height:15px !important; line-height:15px !important;}
                .height20 {height:20px !important; line-height:20px !important;}
                .height30 {height:30px !important; line-height:30px !important;}
                .height60 {height:60px !important; line-height:60px !important;}
                .width272 {max-width:272px !important;}
                .width252 {max-width:252px !important;}
                .news-padding-left { padding-left:10px !important; }	
                .about-padding-right { padding-right:10px !important; }
                .div-width {display: block !important; width: 100% !important; max-width: 100% !important;}
                .saf-table {display:block !important;}
                .width50percent {width:100% !important; max-width:100% !important;}
            }
            
            @media only screen and (max-width:480px) 
            {
               .button-width .display-button {width:auto !important;}
               .div-width {display: block !important; width: 100% !important; max-width: 100% !important;}	
               .display-width-child .footer-2-width {width:170px !important;}
               .display-width-child .feature-width {width:260px !important;}
               .width272 {max-width:272px !important;}
               .width252 {max-width:252px !important;}
            
            }	
            
            @media only screen and (max-width:380px)
            {
                .display-width-child .width252 { width:100% !important; max-width:252px !important;}
                
            }
            
            @media only screen and (max-width:360px)
            {
                .trial-font {font-size: 13px !important;}
                .display-width .header {max-width:255px !important;}
                .display-width-child .width272 { width:100% !important; max-width:100% !important;}
                .display-width-child .width252 { width:100% !important; max-width:252px !important;}
                .display-width-child .feature-width {width:100% !important;}
                .display-width .butn-header {max-width:260px !important;}
                
            }
            
            @media only screen and (max-width:320px)
            {
               .butn-header {max-width:100% !important; width:100% !important;}
                .cta-font {font-size: 32px !important;}
            }
            
            
        </style>
            
    </head>
    <body>
        <!--[if (gte mso 9)|(IE)]>
            <style >
                .Heading {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
                .MsoNormal {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
                .display-button td, .display-button a, a {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}			
            </style>
        <![endif]-->
            
            <!-- MENU STARTS -->
            <table align="center" bgcolor="#495A62" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <!--[if (gte mso 9)|(IE)]>
                                <table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="800" style="width: 800px;">
                                    <tr>
                                        <td align="center" valign="top" width="100%" style="max-width:800px;">
                                            <![endif]-->
                                                <div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">
                                                    <!-- ID:BG MENU OPTIONAL -->
                                                    <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
                                                        <tbody>	
                                                            <tr>
                                                                <td align="center" class="res-padding">
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="600" style="width:600px;">
                                                                        <tr>
                                                                            <td align="center" valign="top" width="600">
                                                                                <![endif]-->
                                                                                <div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="width600">
                                                                                    <table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;"> 
                                                                                        <tr>
                                                                                            <td height="15" class="height30" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="center" style="width:100%; max-width:100%; font-size:0;">
                                                                                                <!--[if (gte mso 9)|(IE)]>
                                                                                                <table  aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="width:100%;">
                                                                                                    <tr>
                                                                                                        <td align="center" valign="top" width="150">
                                                                                                            <![endif]-->
                                                                                                            <div style="display:inline-block; max-width:150px; width:100%; vertical-align:top;" class="div-width">
                                                                                                                <!--TABLE LEFT-->
                                                                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
                                                                                                                    <tr>
                                                                                                                        <td align="center">
                                                                                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:auto !important;">
                                                                                                                                <tr>
                                                                                                                                    <!-- ID:TXT MENU -->
                                                                                                                                    <td align="center" style="color:#333333;">
                                                                                                                                        <a href="#" style="color:#333333; text-decoration:none;"><img src="https://gestur.git.com.py/logoEmpresa/logoDtp.png"  width="150" style="margin:0; border:0; padding:0; display:block;"/></a>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </div>
                                                                                                            <!--[if (gte mso 9)|(IE)]>
                                                                                                        </td>
                                                                                                        <td align="center" valign="top" width="445">
                                                                                                        <![endif]-->
                                                                                                            <div style="display:inline-block; width:100%; max-width:445px; vertical-align:top;" class="div-width">
                                                                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
                                                                                                                    <tr>
                                                                                                                        <td align="center" style="font-size:0;">		
                                                                                                                        <!--[if (gte mso 9)|(IE)]>
                                                                                                                        <table  aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="width:100%;">
                                                                                                                        <tr>
                                                                                                                        <td width="293">
                                                                                                                        <![endif]-->
                                                                                                                            <div style="display:inline-block; width:100%; max-width:293px; vertical-align:top;" class="div-width">
                                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:230px;">
                                                                                                                                    <tr>
                                                                                                                                        <td width="100%">&nbsp;</td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </div>
                                                                                                                        <!--[if (gte mso 9)|(IE)]>
                                                                                                                        </td>
                                                                                                                        <td width="140">
                                                                                                                        <![endif]-->
                                                                                                                        <div style="display:inline-block; width:100%; max-width:140px; vertical-align:top;">
                                                                                                                            <table align="right" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
                                                                                                                                <tr>
                                                                                                                                    <td align="center" style="font-size:0;">
                                                                                                                                        <!--TABLE RIGHT-->
                                                                                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
                                                                                                                                            <tr>
                                                                                                                                                <td align="center">
                                                                                                                                                    <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0">
                                                                                                                                                        <tr>
                                                                                                                                                            <td height="15" class="height20" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;"></td>
                                                                                                                                                        </tr>
                                                                                                                                                
                                                                                                                                                        <tr>
                                                                                                                                                            <td height="9" class="height-hidden" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;"></td>
                                                                                                                                                        </tr>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </div>
                                                                                                                        <!--[if (gte mso 9)|(IE)]>
                                                                                                                        </td>
                                                                                                                        </tr>
                                                                                                                        </table>
                                                                                                                        <![endif]-->
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </div>
                                                                                                            <!--[if (gte mso 9)|(IE)]>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <![endif]-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="15" class="height30" style="mso-line-height-rule:exactly; line-height:15px; font-size:0;">&nbsp;</td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <!--[if (gte mso 9)|(IE)]>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <![endif]-->
                                                                </td>
                                                            </tr>
                                                        </tbody>	
                                                    </table>
                                                </div>
                                            <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                    </tr>
                                </table>
                            <![endif]-->
                        </td>
                    </tr>					
                </tbody>	
            </table>
            <!-- MENU ENDS -->
            

            
            <!-- HEADER-4 STARTS -->
            <table align="center" bgcolor="#495A62" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <!--[if (gte mso 9)|(IE)]>
                        <table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="800" style="width: 800px;">
                            <tr>
                                <td align="center" valign="top" width="800">
                                    <![endif]-->
                                    <div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">
                                        <!-- ID:BG HEADER-4 OPTIONAL -->
                                        <table align="center" border="0" bgcolor="#cccccc" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
                                            <tr>
                                                <td align="center">
                                                    <!--[if gte mso 9]>
                                                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:800px; height:523px; margin:auto;">
                                                    <v:fill type="tile" src="http://www.pennyblacktemplates.com/demo/office/images/800x660.jpg" color="#f6f8f7" />
                                                    <v:textbox inset="0,0,0,0">
                                                    <![endif]-->
                                                    <div style="margin:auto;">
                                                        <table align="center" border="0" 
                                                        cellpadding="0" cellspacing="0" class="display-width" width="100%" 
                                                        >
                                                            <tr>
                                                                <td align="center" class="padding">
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="600" style="width: 600px;">
                                                                        <tr>
                                                                            <td align="center" valign="top" width="600">
                                                                                <![endif]-->
                                                                                <div style="display:inline-block;width:100%; max-width:600px; vertical-align:top;" class="main-width">
                                                                                    <table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                                                        <tr>
                                                                                            <td height="100" style="mso-line-height-rule: exactly; font-size:0;">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="center">
                                                                                                <table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="90%" style="width:90% !important; max-width:90% !important;">
                                                                                                    <tr>
                                                                                                        <td align="center" style="line-height:0; font-size:0;">
                                                                                                            <img src="{{url('email/todos_ganan.jpeg')}}" alt="64x64x4" width="300"  style="color:#ffffff; margin:0; border:0; padding:0; display:block;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height="15" style="line-height:15px; mso-line-height-rule:exactly; font-size:0;">&nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <!-- ID:TXT HEADER-4 HEADING -->
                                                                                                        <td align="center" class="Heading" style="color:#333333; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:38px; line-height:45px; font-weight:500; letter-spacing:1px;">
                                                                                                            {{ $nombre }}
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height="10" style="mso-line-height-rule: exactly; line-height:10px; font-size:0;">&nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <!-- ID:TXT HEADER-4 CONTENT -->
                                                                                                        <td align="center" class="MsoNormal" style="color:#000000 !improtant; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:14px; font-weight:400 !important; letter-spacing:1px; line-height:24px;">
                                                                                                            ¡FELICIDADES! En DTP celebramos tu objetivo!! 

                                                                                                            TODOS GANAN CON DTP te premia! 
                                                                                                            
                                                                                                            Para canjear tus puntos, favor emitinos tu factura a nombre de DESARROLLO TURÍSTICO PARAGUAYO S.A
                                                                                                            RUC 80001813-3 
                                                                                                            Y envía la foto de la misma a dtplus@dtp.com.py 
                                                                                                            
                                                                                                            Seguí sumando puntos!
                                                                                                            <br>
                                                                                                            <br>
                                                                                                            <span style="font-weight: 700;">Al correo se adjunta el pdf con los detalles y el monto a cobrar.</span>	
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height="20" style="mso-line-height-rule: exactly; line-height:20px; font-size:0;">&nbsp;</td>
                                                                                                    </tr>
                                                                                                    
                                                                                                </table>		
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="60" style="mso-line-height-rule: exactly; line-height: 60px; font-size:0;">&nbsp;</td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <!--[if (gte mso 9)|(IE)]>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <![endif]-->
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <!--[if gte mso 9]> </v:textbox> </v:rect> <![endif]-->	
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]>
                                </td>
                            </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </table>	
            <!-- HEADER-4 ENDS -->
        
            
            <!-- FOOTER-1 STARTS -->
            <table align="center" bgcolor="#495A62" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <!--[if (gte mso 9)|(IE)]>
                                <table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="800" style="width: 800px;">
                                    <tr>
                                        <td align="center" valign="top" width="100%" style="max-width:800px;">
                                            <![endif]-->
                                                <div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">
                                                    <!-- ID:BG FOOTER-1 OPTIONAL -->
                                                    <table align="center" bgcolor="#f7fafb" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
                                                        <tbody>	
                                                            <tr>
                                                                <td align="center" class="res-padding">
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="600" style="width:600px;">
                                                                        <tr>
                                                                            <td align="center">
                                                                                    <![endif]-->
                                                                                        <div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="width600">
                                                                                            <table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                                                                <tr>
                                                                                                    <td height="45" style="font-size:0; mso-line-height-rule:exactly; line-height:45px;">&nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td align="center" style="font-size:0;">
                                                                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                                                                    <table  aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="width:100%;">
                                                                                                                        <tr>
                                                                                                                            <td align="center" valign="top" width="292">
                                                                                                                                <![endif]-->
                                                                                                                                <div style="display:inline-block; max-width:292px; vertical-align:top; width:100%;" class="div-width">
                                                                                                                                    <!-- TABLE LEFT -->	
                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
                                                                                                                                        <tr>
                                                                                                                                            <td align="left" style="padding:15px 10px;"> 
                                                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                                                                                                    <tr>
                                                                                                                                                        <td align="left"> 
                                                                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
                                                                                                                                                                <tbody>	
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td align="center">
                                                                                                                                                                            <table align="center" border="0" cellspacing="0" cellpadding="0" style="width:auto !important;">
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <td align="left" width="50" valign="top" style="color:#666666;">
                                                                                                                                                                                        <img src="{{url('email/correo_icono.png')}}" alt="32x32x1" width="32" height="32" style="margin:0; padding:0; border:0;" />
                                                                                                                                                                                    </td>
                                                                                                                                                                                    <td>
                                                                                                                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                                                                            <tr>
                                                                                                                                                                                                <!-- ID:TXT FOOTER-1 MAIL -->
                                                                                                                                                                                                <td class="Heading txt-center" style="color:#333333; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-weight:500; font-size:18px; letter-spacing:1px; line-height:25px; mso-line-height-rule: exactly;" align="left"><a href="#" style="color:#333333;text-decoration:none;">
                                                                                                                                                                                                    <a style="color: black; text-decoration: none;" href="mailto:dtplus@dtp.com.py">dtplus@dtp.com.py</a>	</a>
                                                                                                                                                                                                    </td>
                                                                                                                                                                                            </tr>
                                                                                                                                                                                        </table>
                                                                                                                                                                                    </td>
                                                                                                                                                                                </tr>
                                                                                                                                                                            </table>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>		
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>		
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </div>	
                                                                                                                                
                                                                                                                                <!--[if (gte mso 9)|(IE)]>
                                                                                                                            </td>
                                                                                                                            <td align="center" valign="top" width="292">
                                                                                                                                <![endif]-->
                                                                                                                                <div style="display:inline-block; max-width:292px; vertical-align:top; width:100%;" class="div-width">
                                                                                                                                    <!-- TABLE RIGHT -->	
                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
                                                                                                                                        <tr>
                                                                                                                                            <td align="left" style="padding:15px 10px;"> 
                                                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                                                                                                    <tr>
                                                                                                                                                        <td align="left"> 
                                                                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
                                                                                                                                                                <tbody>	
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td align="center">
                                                                                                                                                                            <table align="center" border="0" cellspacing="0" cellpadding="0" style="width:auto !important;">
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <td align="left" width="50" valign="top" style="color:#666666;">
                                                                                                                                                                                        <img src="{{url('email/telefono_icono.png')}}" alt="32x32x2" width="32" height="32" style="margin:0; padding:0; border:0;" />
                                                                                                                                                                                    </td>
                                                                                                                                                                                    <td>
                                                                                                                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                                                                            <tr>
                                                                                                                                                                                                <!-- ID:TXT FOOTER-1 PH -->
                                                                                                                                                                                                <td class="Heading txt-center" style="color:#333333; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-weight:500; font-size:18px; letter-spacing:1px; line-height:25px; mso-line-height-rule: exactly;" align="left">	(021) 729-7070
                                                                                                                                                                                                    </td>
                                                                                                                                                                                            </tr>
                                                                                                                                                                                        </table>
                                                                                                                                                                                    </td>
                                                                                                                                                                                </tr>
                                                                                                                                                                            </table>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>		
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>		
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </div>
                                                                                                                                <!--[if (gte mso 9)|(IE)]>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                    <![endif]-->
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            
                                                                                            
                                                                                                
                                                                                                
                                                                                                <tr>
                                                                                                    <td height="55" style="mso-line-height-rule:exactly; line-height:55px; font-size:0;">&nbsp;</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    <![endif]-->
                                                                </td>
                                                            </tr>
                                                        </tbody>	
                                                    </table>
                                                </div>
                                            <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                    </tr>
                                </table>
                            <![endif]-->
                        </td>
                    </tr>					
                </tbody>	
            </table>
            <!-- FOOTER-1 ENDS -->
            
            <!-- FOOTER-2 STARTS -->
            <table align="center" bgcolor="#495A62" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td align="center">
                            <!--[if (gte mso 9)|(IE)]>
                                <table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="800" style="width: 800px;">
                                    <tr>
                                        <td align="center" valign="top" width="100%" style="max-width:800px;">
                                            <![endif]-->
                                                <div style="display:inline-block; width:100%; max-width:800px; vertical-align:top;" class="width800">
                                                    <!-- ID:BG FOOTER-2 OPTIONAL -->
                                                    <table align="center" bgcolor="#495A62" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="max-width:800px;">
                                                        <tbody>	
                                                            <tr>
                                                                <td align="center" class="res-padding">
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <table aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="600" style="width:600px;">
                                                                        <tr>
                                                                            <td align="center">
                                                                                    <![endif]-->
                                                                                        <div style="display:inline-block; width:100%; max-width:600px; vertical-align:top;" class="width600">
                                                                                            <table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                                                                                                <tr>
                                                                                                    <td height="45" style="font-size:0; mso-line-height-rule:exactly; line-height:45px;">&nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table align="center" border="0" class="display-width" cellpadding="0" cellspacing="0" width="100%">
                                                                                                            <tr>
                                                                                                                <td align="center" style="font-size:0;">
                                                                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                                                                    <table  aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="width:100%;">
                                                                                                                        <tr>
                                                                                                                            <td align="center" valign="top" width="292">
                                                                                                                                <![endif]-->
                                                                                                                                <div style="display:inline-block; max-width:292px; vertical-align:top; width:100%;" class="div-width">
                                                                                                                                    <!-- TABLE LEFT -->	
                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
                                                                                                                                        <tr>
                                                                                                                                            <td align="left" style="padding:15px 10px;"> 
                                                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                                                                                                    <tr>
                                                                                                                                                        <td align="left"> 
                                                                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
                                                                                                                                                                <tbody>	
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td align="center">
                                                                                                                                                                            <table align="center" border="0" cellspacing="0" cellpadding="0" style="width:auto !important;">
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <td align="left" width="50" valign="top" style="color:#666666;">
                                                                                                                                                                                        <img src="{{url('email/web_icono.png')}}" alt="32x32x1" width="32" height="32" style="margin:0; padding:0; border:0;" />
                                                                                                                                                                                    </td>
                                                                                                                                                                                    <td>
                                                                                                                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                                                                            <tr>
                                                                                                                                                                                                <!-- ID:TXT FOOTER-2 MAIL -->
                                                                                                                                                                                                <td class="Heading txt-center" style="color:#ffffff; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-weight:500; font-size:18px; letter-spacing:1px; line-height:25px; mso-line-height-rule: exactly;" align="left"><a style="color:#ffffff; text-decoration: none;" href="https://dtpmundo.com" >https://dtpmundo.com</a>
                                                                                                                                                                                                    </td>
                                                                                                                                                                                            </tr>
                                                                                                                                                                                        </table>
                                                                                                                                                                                    </td>
                                                                                                                                                                                </tr>
                                                                                                                                                                            </table>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>		
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>		
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </div>	
                                                                                                                                
                                                                                                                                <!--[if (gte mso 9)|(IE)]>
                                                                                                                            </td>
                                                                                                                            <td align="center" valign="top" width="292">
                                                                                                                                <![endif]-->
                                                                                                                                <div style="display:inline-block; max-width:292px; vertical-align:top; width:100%;" class="div-width">
                                                                                                                                    <!-- TABLE RIGHT -->	
                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
                                                                                                                                        <tr>
                                                                                                                                            <td align="left" style="padding:15px 10px;"> 
                                                                                                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                                                                                                    <tr>
                                                                                                                                                        <td align="left"> 
                                                                                                                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; max-width:100%; width:100%;">
                                                                                                                                                                <tbody>	
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td align="center">
                                                                                                                                                                            <table align="center" border="0" cellspacing="0" cellpadding="0" style="width:auto !important;">
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <td align="left" width="50" valign="top" style="color:#666666;">
                                                                                                                                                                                        <img src="{{url('email/compra_icono.png')}}" alt="32x32x2" width="32" height="32" style="margin:0; padding:0; border:0;" />
                                                                                                                                                                                    </td>
                                                                                                                                                                                    <td>
                                                                                                                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                                                                            <tr>
                                                                                                                                                                                                <!-- ID:TXT FOOTER-2 PH -->
                                                                                                                                                                                                <td class="Heading txt-center" style="color:#ffffff; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-weight:500; font-size:18px; letter-spacing:1px; line-height:25px; mso-line-height-rule: exactly;" align="left">	Reserva las mejores tarifas online
                                                                                                                                                                                                    </td>
                                                                                                                                                                                            </tr>
                                                                                                                                                                                        </table>
                                                                                                                                                                                    </td>
                                                                                                                                                                                </tr>
                                                                                                                                                                            </table>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </tbody>
                                                                                                                                                            </table>		
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </table>		
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </div>
                                                                                                                                <!--[if (gte mso 9)|(IE)]>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                    <![endif]-->
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <!-- ID:BR FOOTER-2 BORDER -->
                                                                                                    <td height="15" style="border-bottom:1px solid #dddddd; mso-line-height-rule: exactly; line-height: 15px;"></td>
                                                                                                </tr>
                                                                                            
                                                                                                <tr>
                                                                                                    <td height="25" style="font-size:0; mso-line-height-rule:exactly; line-height:25px;">&nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="center">
                                                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%; max-width:100%;">
                                                                                                            <tr>
                                                                                                                <td align="center" style="width:100%; max-width:100%; font-size:0;">
                                                                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                                                                    <table  aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="width:100%;">
                                                                                                                        <tr>
                                                                                                                            <td align="center" valign="top" width="250">
                                                                                                                                <![endif]-->
                                                                                                                                <div style="display:inline-block; width:100%; vertical-align:top;" class="div-width">
                                                                                                                                    <!--TABLE LEFT-->
                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
                                                                                                                                        <tbody>													
                                                                                                                                            <tr>
                                                                                                                                                <td align="center" style="padding:5px 10px;">
                                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                                        <tr>
                                                                                                                                                            <!-- ID:TXT FOOTER-2 COPY RIGHTS -->
                                                                                                                                                            <td align="center" class="MsoNormal" style="color:#ffffff; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size:13px; line-height:23px; letter-spacing:1px;">
                                                                                                                                                                &copy; GIT
                                                                                                                                                            </td>
                                                                                                                                                        </tr>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>										
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </div>
                                                                                                                                <!--[if (gte mso 9)|(IE)]>
                                                                                                                            </td>
                                                                                                                            <td align="center" valign="top" width="340">
                                                                                                                            <![endif]-->
                                                                                                                                <div style="display:inline-block; width:100%; max-width:340px; vertical-align:top;" class="div-width">
                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:100%;">
                                                                                                                                        <tr>
                                                                                                                                            <td align="center" style="font-size:0;">		
                                                                                                                                            <!--[if (gte mso 9)|(IE)]>
                                                                                                                                            <table  aria-hidden="true" border="0" cellspacing="0" cellpadding="0" align="center" width="100%" style="width:100%;">
                                                                                                                                            <tr>
                                                                                                                                            <td width="230">
                                                                                                                                            <![endif]-->
                                                                                                                                                <div style="display:inline-block; width:100%; max-width:200px; vertical-align:top;" class="div-width">
                                                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width-child" width="100%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%; max-width:210px;">
                                                                                                                                                        <tr>
                                                                                                                                                            <td width="100%" style="font-size:0;">&nbsp;</td>
                                                                                                                                                        </tr>
                                                                                                                                                    </table>
                                                                                                                                                </div>
                                                                                                                                            <!--[if (gte mso 9)|(IE)]>
                                                                                                                                            </td>
                                                                                                                                            <td width="210">
                                                                                                                                            <![endif]-->
                                                                                                                                        
                                                                                                                                            <!--[if (gte mso 9)|(IE)]>
                                                                                                                                            </td>
                                                                                                                                            </tr>
                                                                                                                                            </table>
                                                                                                                                            <![endif]-->
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </div>
                                                                                                                                <!--[if (gte mso 9)|(IE)]>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                    <![endif]-->
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>			
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td height="55" style="mso-line-height-rule:exactly; line-height:55px; font-size:0;">&nbsp;</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    <![endif]-->
                                                                </td>
                                                            </tr>
                                                        </tbody>	
                                                    </table>
                                                </div>
                                            <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                    </tr>
                                </table>
                            <![endif]-->
                        </td>
                    </tr>					
                </tbody>	
            </table>
            <!-- FOOTER-2 ENDS -->
    </body>
</html>	