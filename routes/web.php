<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
| 
*/


Route::get('testing', 'HomeController@testing');
Route::get('rtest', 'HomeController@rtest');
Route::get('delete', 'HomeController@delete'); 

Route::get('respuesta', 'FacturarController@respuesta'); 

//FACTURAR POR DTPMUNDO EJEMPLO
Route::get('facturaDtp/{hash}','FacturarController@facturaDtp', function ($hash) { 
																return $hash;	})->name('facturaDtp');
//VOUCHER POR DTPMUNDO EJEMPLO
Route::get('voucherDtp/{hash}','VoucherPdfController@voucherDtp', function ($hash) { 
																return $hash;	})->name('voucherDtp');

Route::get('facturaDtp/{id?}', 'FacturarController@facturarDtp',  function ($id) { 
																return $id;	})->name('facturaDtp');

Route::get('voucherDtp/{id?}', 'VoucherPdfController@voucherpdfDtp', function ($id) {
																return $id;	})->name('voucherDtp');

Route::group(['middlewareGroups'=>'redirectToLogin'], function(){
	// [redirect, autorizacion]
	/**********************************************************
		HOTELES
	***********************************************************/	
	/*Pagina de Inicio por Defecto*/
    Route::get('/','HomeController@indexMc')->name('home');

});

/*Direccion para subir imagenes por Ajax*/
Route::post('upload', ['as' => 'upload', 'uses' => 'UploadController@indexDtplus']);

Route::post('uploadDocumentosBancario', ['as' => 'uploadDocumentosBancario', 'uses' => 'UploadController@uploadDocumentosBancario']);

Route::post('uploadDTPlus', ['as' => 'uploadDTPlus', 'uses' => 'UploadController@indexDtp_lus']);

Route::get('fileDelDTPlus','UploadController@fileDelDTPlus')->name('fileDelDTPlus');

Route::get('notificacionesPendientes','NotificacionController@index')->name('notificacionesPendientes');

/**==================================================
				VOUCHER 
=====================================================*/

/*Direccion para el voucher*/
Route::get('voucherpdf', 'VoucherPdfController@generarVoucherPdf')->name('voucherpdf');
Route::get('voucherpdfUser/{id?}','VoucherPdfController@voucherpdfUser', function ($id) {
													return $id;	
														})->name('voucherpdfUser');
Route::get('listadopdf', 'VoucherPdfController@listadopdf')->name('listadopdf');

/*Direcciones para Login*/ 
Route::get('login','LoginController@index')->name('login');
Route::get('doLogin','LoginController@doLogin')->name('doLogin');
Route::get('procesoNemo','LoginController@loginNemo')->name('procesoNemo');		
Route::get('procesoPagoNemo','PagoProveedorController@procesoPagoNemo')->name('procesoPagoNemo');	

Route::get('comprobandoNemo','LoginController@comprobandoNemo')->name('comprobandoNemo');		


/*Página de Salida*/
Route::get('logout','LoginController@logout')->name('logout');
Route::get('recuperarContrasenha','LoginController@contrasenha')->name('recuperarContrasenha');
Route::get('doRecuperar','LoginController@doRecuperar')->name('doRecuperar');
Route::get('enviarFactura/{id?}','FacturarController@enviarFactura', function ($id) {
																						return $id;	
																					})->name('enviarFactura');
/**********************************************************************/
/*			MIDDLEWARE PARA REDIRIGIR AL LOGIN 
/**********************************************************************/


Route::group( [ 'middleware'=>'redirectToLogin'], function()
	{
		Route::get('/', 'HomeController@indexMc')->name('homem');    
		Route::get('home','HomeController@indexMc')->name('home');	
		/**********************************************************************/
		/*	CONTROL DE VISTAS Y ACCESO A MENU Y SUBMENU SEGUN TIPO PERSONA EN BD
		/**********************************************************************/
		// Route::middleware(['autorizacionVistas'])->group(function () {
		Route::group(['middleware'=> 'autorizacionVistas'], function() {

//ABM tipoticket NUEVO RESOURCE



	Route::get('tipoticket', 'TipoTicketController@index')->name('tipoticket.index');
	Route::get('tipoticket/create', 'TipoTicketController@create')->name('tipoticket.create');
	Route::get('tipoticket/{id?}/edit', 'TipoTicketController@edit')->name('tipoticket.edit');
	Route::post('tipoticket', 'TipoTicketController@store')->name('tipoticket.store');
	Route::PUT('tipoticket/{id?}', 'TipoTicketController@update')->name('tipoticket.update');


	//ABM PARAMETROS NUEVO RESOURCE
	Route::get('parametros', 'ParametroController@index')->name('parametros.index');
	Route::get('parametros/create', 'ParametroController@create')->name('parametros.create');
	Route::get('parametros/{id?}/edit', 'ParametroController@edit')->name('parametros.edit');
	Route::post('parametros', 'ParametroController@store')->name('parametros.store');
	Route::PUT('parametros/{id?}', 'ParametroController@update')->name('parametros.update');


		//REPORTE
		Route::get('reporteDetalleProveedor','FacturarController@reporteDetalleProveedor')->name('reporteDetalleProveedor');
		Route::get('reporteTicketsPendientes','ReporteController@reporteTicketsPendientes')->name('reporteTicketsPendientes');
		//FACTURAR
		Route::get('facturaIndex','FacturarController@index')->name('facturaIndex');
		Route::get('facturasAutorizadas','FacturarController@autorizadas')->name('facturasAutorizadas');

		Route::get('reporteFactura','FacturarController@reporteFactura')->name('reporteFactura');
		Route::get('reporteFacturasPendientes','FacturarController@reporteFacturasPendientes')->name('reporteFacturasPendientes');
		//REPORTE BSP
		Route::get('reporteBspIndex','ReporteBspController@index')->name('reporteBspIndex');	
		Route::get('reporteBspCobranzas','ReporteBspController@reporteBspCobranzas')->name('reporteBspCobranzas');
		Route::get('personaIndex','PersonaController@index')->name('personaIndex');
		Route::get('listadoCliente','PersonaController@listadoCliente')->name('listadoCliente');

		//ALTA DE MANUAL VISTA PRINCIPAL EN PDF
		Route::get('verActualizacionesManual','ManualController@verActualizacionesManual')->name('verActualizacionesManual');

		//Cotizacion
		Route::get('cotizacionIndex','CotizacionController@index')->name('cotizacionIndex');
		Route::get('negociacionIndex','NegociacionController@index')->name('negociacionIndex');
		//Operativo
		Route::get('controlOperativo/{id?}','ControlOperacionesController@index', function ($id) { return $id;	})->name('controlOperativo');
		//Linea Credito
		Route::get('lineaCreditoIndex','LineaCreditoController@index')->name('lineaCreditoIndex');
		// Empresas
		Route::get('empresas','EmpresaController@index')->name('empresas');
		// Pais
		Route::get('paises','PaisController@index')->name('paises');
		// TipoPersona
		Route::get('tipoPersonas','TipoPersonaController@index')->name('tipoPersonas');
		// TipoPersoneria
		Route::get('tipoPersonerias','TipoPersoneriaController@index')->name('tipoPersonerias');
		// TipoPersoneria
		Route::get('tipoIdentidads','TipoIdentidadController@index')->name('tipoIdentidads');
		// TipoTimbrado
		Route::get('tipoTimbrados','TipoTimbradoController@index')->name('tipoTimbrados');
		// SucursalEmpresa
		Route::get('sucursalEmpresas','SucursalEmpresaController@index')->name('sucursalEmpresas');
		//Proformas
		Route::get('listadosProformas','ProformaController@index')->name('listadosProformas');
		// Route::get('listadosProformas',[ 'middleware'=> ['autorizacionVistas'],'uses'=>'ProformaController@index'])->name('listadosProformas');
		//Grupos
		Route::get('indexGrupo','GruposController@index')->name('indexGrupo');
		//Tickets
		// Route::resource('factour.tickets', 'TicketsController');
		Route::get('indexTicket','TicketsController@indexTicket')->name('indexTicket');
		//Estado de cuenta
		Route::get('estadoCuentaindex','EstadoCuentasController@index')->name('estadoCuentaindex');
		// Route::get('indexDetallado','EstadoCuentasController@indexDetallado')->name('indexDetallado');
		//Migración compras y ventas
		Route::get('indexReporteCompra','MigracionCompraController@indexReporteCompra')->name('indexReporteCompra');
		//PRODUCTOS
		Route::get('indexProducto','ProductosController@index')->name('indexProducto');
		Route::get('listadoNotaCredito','PedidoNotaCreditoController@index')->name('listadoNotaCredito');
		//PERMISOS
		Route::get('indexPermisos','PermisosController@index')->name('indexPermisos');
		//Operativo
		Route::get('controlOperativo/{id?}','ControlOperacionesController@index', function ($id) { return $id;	})->name('controlOperativo');
		//Auditoría
		Route::get('indexAuditoriaPersonas','AuditoriaController@indexAuditoriaPersonas')->name('indexAuditoriaPersonas');
		Route::get('auditoria-productos','AuditoriaController@indexAuditoriaProductos')->name('indexAuditoriaProductos');
		Route::get('auditoria-productos-listado','AuditoriaController@getProductoListado')->name('getProductoListado');
		
		//Reporte de riesgo
		Route::get('reporteRiesgo','FacturarController@reporteRiesgo')->name('reporteRiesgo');
		//Liquidación de comisiones
		Route::get('indexLiquidacionComisiones','ReporteController@indexLiquidacionComisiones')->name('indexLiquidacionComisiones');
		
		Route::get('hechaukaLc','MigracionCompraController@hechaukaLc')->name('hechaukaLc');
		Route::get('hechaukaLv','MigracionVentaController@hechaukaLv')->name('hechaukaLv');

		Route::get('migracionDatos','UploadController@migracionDatos')->name('migracionDatos');

		//Reporte de Venta Sucursal
		Route::get('reporteVentaSucursal','ReporteController@reporteVentaSucursal')->name('reporteVentaSucursal');
		
		 

		//REPORTE PARA BUSQUEDA
		Route::get('buscarReserva','ProformaController@buscarReserva')->name('buscarReserva');
		//REPORTE PARA BUSQUEDA REPRICE
		
		//REPORTE INCENTIVO
		Route::get('incentivoReporte','ReporteController@incentivoReporte')->name('incentivoReporte');

		//FALLO DE CAJA
		Route::get('falloCaja','ReporteController@falloCaja')->name('falloCaja');
		//EXTRACTO AGENCIA
		Route::get('extractoAgencias','ReporteController@extractoAgencias')->name('extractoAgencias');

		Route::get('doEnvioCorreo','ReporteController@doEnvioCorreo')->name('doEnvioCorreo');

		Route::get('servicioDtp','ProformaController@servicioDtp')->name('servicioDtp');

			//reporte de proformas															
		Route::get('reporteProformas','ProformaController@reporteProformas')->name('reporteProformas');		
		Route::post('generarExcelReporteProformasEstado', ['as' => 'generarExcelReporteProformasEstado', 'uses' => 'ExcelController@generarExcelReporteProformasEstado']);
		Route::get('verDatosProformas','ProformaController@verDatosProformas')->name('verDatosProformas');

		//Ventas
		Route::get('createMigracionVentas','MigracionVentaController@createMigracionVentas')->name('createMigracionVentas');

		//Libros Compras
		Route::get('indexLibrosCompras','ContabilidadController@indexLibrosCompras')->name('indexLibrosCompras');
 
		//Costo de Ventas Pendientes
		Route::get('indexVentaPendiente','ContabilidadController@indexVentaPendiente')->name('indexVentaPendiente');

		//Libros Compras FondoFijo
		Route::get('indexLibrosFondoFijo','ContabilidadController@indexLibrosFondoFijo')->name('indexLibrosFondoFijo');

		//PAGO PROVEEDOR
    	Route::get('indexPagoProveedor','PagoProveedorController@indexPagoProveedor')->name('indexPagoProveedor');
		
		Route::get('reposiocionFondo','PagoProveedorController@reposiocionFondo')->name('reposiocionFondo');
		//PLAN CUENTAS
    	Route::get('indexCuentaContable','CuentaContableController@index')->name('indexCuentaContable');

    	//BANCOS
		Route::get('indexBancos','BancosController@index')->name('indexBancos');


		//REPORTE TRANSFERENCIA
		Route::get('reporteTransferencia','BancosController@reporteTransferencia')->name('reporteTransferencia');
		
		//REPORTE DE ASIENTOS CONTABLES
		Route::get('reporteAsiento','ReporteController@reporteAsiento')->name('reporteAsiento');
    	//REPORTE DE OP
		Route::get('indexOp','PagoProveedorController@indexOp')->name('indexOp');
			
		//ANTICIPO
		Route::get('indexAnticipo','PagoProveedorController@indexAnticipo')->name('indexAnticipo');

		
    	//Libros Ventas
		Route::get('indexLibrosVentas','ContabilidadController@indexLibrosVentas')->name('indexLibrosVentas');

		//COBRANZA
		Route::get('listadoCobro','CobranzaController@listadoCobro')->name('listadoCobro');


		//CUENTA CORRIENTE CLIENTE
		Route::get('indexCttaCorrienteCliente','CobranzaController@indexCttaCorrienteCliente')->name('indexCttaCorrienteCliente');

		//CUENTA CORRIENTE PROVEEDOR
		Route::get('indexCttaCorrienteProveedor','PagoProveedorController@indexCttaCorrienteProveedor')->name('indexCttaCorrienteProveedor');

    	Route::get('indexCttaCorrientes','CuentaCorrienteController@indexCttaCorriente')->name('indexCttaCorrientes');

    	Route::get('indexRecibo','CobranzaController@index')->name('indexRecibo');

     	Route::get('indexPago','ReporteController@indexPago')->name('indexPago');

    	Route::get('reporteCobros','ReporteController@reporteCobros')->name('reporteCobros');
		 
		//REPORTE GRUPO/ANTICIPO
		Route::get('grupoAnticipoReporte','ReporteController@grupoAnticipoReporte')->name('grupoAnticipoReporte');

     	Route::get('pagosAgencia','ReporteController@pagosAgencia')->name('pagosAgencia');

	    //CIERRE
	    Route::get('cierre','ReporteController@cierre')->name('cierre');
		Route::get('generarCierre','ReporteController@generarCierre')->name('generarCierre');
		
		//ABM CUENTAS															
		Route::get('listadoCuenta','BancosController@listadoCuenta')->name('listadoCuenta');

		//PLAN CUENTAS	
		Route::get('planCuentas','ReporteController@planCuentas')->name('planCuentas');

		//ABM TIPO CUENTA
		Route::get('listadoTipoCuenta','BancosController@listadoTipoCuenta')->name('listadoTipoCuenta');
 		
 		//TIMBRADO
	     Route::get('indexTimbrado','TimbradoController@index')->name('indexTimbrado');

 		//FORMA COBRO CLIENTE
		 Route::get('indexFormaCobro','FormaCobroClienteController@index')->name('indexFormaCobro');

		 //REPORTE PEDIDOS
		Route::get('indexPedido','VentasRapidasController@indexPedido')->name('indexPedido');		
		// LISTADO VENTAS RÁPIDAS
		Route::get('indexVentasRapidas','VentasRapidasController@indexVentasRapidas')->name('indexVentasRapidas');

		Route::get('actualizarProductos','ExcelController@actualizarProductos')->name('actualizarProductos');
		

		//RESERVAS NEMO LISTADO
		Route::get('verReservasNemo','ReservasNemoController@index')->name('verReservasNemo');
		Route::get('verDatosReservasNemo','ReservasNemoController@verDatos')->name('verDatosReservasNemo');
		Route::get('getRespuestaGDS','ReservasNemoController@getRespuestaGDS')->name('getRespuestaGDS');
		Route::get('editarReservaNemo','ReservasNemoController@editarReservaNemo')->name('editarReservaNemo');
		Route::get('guardarEditarNemo','ReservasNemoController@guardarEditarNemo')->name('guardarEditarNemo');
		Route::get('getPrestadoresListado','ReservasNemoController@getPrestadoresListado')->name('getPrestadoresListado');

		// TESAKA
		Route::get('tesaka','ContabilidadController@indexTesaka')->name('tesaka');

		Route::get('indexLibrosFondoFijo','ContabilidadController@indexLibrosFondoFijo')->name('indexLibrosFondoFijo');

		// MIGRACIÓN DE ASIENTOS
		Route::get('migracionAsientos','ContabilidadController@indexMigracionAsientos')->name('migracionAsientos');

		Route::get('buscarTimbrado','TimbradoController@buscarTimbrado')->name('buscarTimbrado');
		
		Route::get('indexMayorCuentas','ContabilidadController@indexMayorCuentas')->name('indexMayorCuentas');
		
		Route::get('configuracionEmpresa','ConfiguracionController@configuracionEmpresa')->name('configuracionEmpresa');
		Route::get('permisoEmpresa','ConfiguracionController@permisoEmpresa')->name('permisoEmpresa');
		Route::get('menuEmpresa','ConfiguracionController@menuEmpresa')->name('menuEmpresa');
		Route::get('cuentaContableEmpresa','ConfiguracionController@cuentaContableEmpresa')->name('cuentaContableEmpresa');
		Route::get('cuentaEmpresa','ConfiguracionController@cuentaEmpresa')->name('cuentaEmpresa');
		Route::get('negocioIndex','ConfiguracionController@negocioIndex')->name('negocioIndex');


		Route::get('saldo-fecha-cuenta','BancosController@saldoFechaCuenta')->name('saldo_fecha_cuenta.index');

		//Forma de pago Cliente
		Route::get('forma-pago-cliente','FormaPagoClienteController@index')->name('forma_pago.index');

		Route::get('verLiquidaciones','ReporteController@verLiquidaciones')->name('verLiquidaciones');
		Route::get('addLiquidaciones','LiquidacionesController@addLiquidaciones')->name('addLiquidaciones');
		Route::get('agregarLiquidacion','PagoProveedorController@reporteOp')->name('agregarLiquidacion');
	});
	Route::get('buscarReservaReprice','ProformaController@buscarReservaReprice')->name('buscarReservaReprice');
	//PARA BUSQUEDA RESERVA REPRICE
	Route::get('ajaxBuscarReservaReprice','ProformaController@ajaxBuscarReservaReprice')->name('ajaxBuscarReservaReprice');
	
	Route::post('saldo-fecha-cuenta/form','BancosController@setSaldoFechaCuenta')->name('saldo_fecha_cuenta.form');

	//Numeracion de recibo
	Route::get('numeracion-recibo','NumeracionReciboController@index')->name('numeracion_recibo.index');
	Route::get('numeracion-recibo/crear','NumeracionReciboController@create')->name('numeracion_recibo.create');
	Route::post('numeracion-recibo/store','NumeracionReciboController@store')->name('numeracion_recibo.store');
	Route::get('numeracion-recibo/get-numeraciones','NumeracionReciboController@getNumeracionesRecibo')->name('numeracion_recibo.ajax.index');
	

	//Ajax Globales en multiples vistas
	//Select2 server-side y paginacion
	 Route::get('get-clientes','AjaxGlobalesController@getClientes')->name('get.clientes');	
	 Route::get('get-destinos','AjaxGlobalesController@getDestinos')->name('get.destinos');	
	 Route::get('get-vendedores-agencia','AjaxGlobalesController@getVendedorAgencia')->name('get.vendedores.agencia');	
	 Route::get('get-personas','AjaxGlobalesController@getPersonas')->name('get.personas');	
	 Route::get('get-usuariosProformas','AjaxGlobalesController@getUsuariosProformas')->name('get.usuariosProformas');	
	 Route::get('get-id-proformas','AjaxGlobalesController@getProforma')->name('get.getProforma');	

	///////////////////////////////////////////////////////////////////////////////////



	Route::get('estadoReserva','PagoProveedorController@estadoReserva')->name('estadoReserva');

	Route::get('subirAir','TicketsController@subir')->name('subirAir');
	Route::post('importArchivo', 'TicketsController@importArchivo')->name('importArchivo');

	//Ingresos
	Route::get('ingresos','ReporteController@ingresos')->name('ingresos.index');
	Route::get('ingresos/ajax/index','ReporteController@ajaxIngresos')->name('ingresos.ajax.index');

	//Egresos
	Route::get('egresos','ReporteController@egresos')->name('egresos.index');
	Route::get('egresos/ajax/cuentas','ReporteController@ajaxEgresoCuentas')->name('egresos.ajax.cuentas');
	Route::get('egresos/ajax/index','ReporteController@ajaxEgresos')->name('egresos.ajax.index');
	Route::get('egresos/ajax/detalle','ReporteController@ajaxDetallesOp')->name('egresos.ajax.detalle');
	
	//Direcciones para manejo de la edicio de manual
	//saldo-fecha-cuenta
	Route::post('saldo-fecha-cuenta/form','BancosController@setSaldoFechaCuenta')->name('saldo_fecha_cuenta.form');

	//LIQUIDACIONES DE COMISIONES
	Route::get('mostrarLiquidaciones','ReporteController@mostrarLiquidaciones')->name('mostrarLiquidaciones');
	Route::get('consultarLiquidaciones','ReporteController@consultarLiquidaciones')->name('consultarLiquidaciones');

	/*
	================================================================================================
		AJAX
	================================================================================================
	*/


	Route::get('addManual','ManualController@addManual')->name('addManual');
	Route::post('subirManual','ManualController@subirManual')->name('subirManual');
	Route::post('verManualSubido','ManualController@verManualSubido')->name('verManualSubido');
	Route::get('verDatosManualesSubidos','ManualController@verDatosManualesSubidos')->name('verDatosManualesSubidos');

	Route::post('modificarDatos', ['as' => 'modificarDatos', 'uses' => 'LoginController@modificarDatos']);
	
	Route::get('addTimbrado','TimbradoController@add')->name('addTimbrado');
	
	Route::get('editTimbrado/{id?}','TimbradoController@edit', function ($id) { return $id;	})->name('editTimbrado');

	
	Route::get('editarFormaCobro/{id?}','FormaCobroClienteController@edit', function ($id) { return $id;	})->name('editarFormaCobro');
	
	Route::get('venta','ProformaController@addVenta')->name('venta');

	Route::get('calcularPrecioCosto','ProformaController@calcularPrecioCosto')->name('calcularPrecioCosto');

	Route::get('editVenta/{id?}','ProformaController@editVenta', function ($id) {return $id;})->name('editVenta');

	Route::get('verVenta/{id?}','ProformaController@verVenta', function ($id) {return $id;})->name('verVenta');

	Route::get('getSolicitudesFacturacion','ProformaController@getSolicitudesFacturacion')->name('getSolicitudesFacturacion');

	Route::post('doEditVenta', ['as' => 'doEditVenta', 'uses' => 'ProformaController@doEditVenta']);

	Route::get('modificarRetenciones/{id?}','CobranzaController@modificarRetenciones', function ($id) {return $id;})->name('modificarRetenciones');
    
	Route::get('getGuardarCliente','PersonaController@getGuardarCliente')->name('getGuardarCliente');
	
	Route::get('getIvaProcudcto','ProformaController@getIvaProcudcto')->name('getIvaProcudcto');
	
	Route::post('doAddVenta', ['as' => 'doAddVenta', 'uses' => 'ProformaController@doAddVenta']);

	Route::get('anularLiquidacion','LiquidacionesController@anularLiquidacion')->name('anularLiquidacion');


	//REPORTE BSP
	Route::get('reporteBspCobranzasAjax','ReporteBspController@reporteBspCobranzasAjax')->name('reporteBspCobranzasAjax');

		//COTIZACIÓN
	Route::get('doCotizacion','HomeController@doCotizacion')->name('doCotizacion');

	Route::get('guardarCotizacionEspecial','CobranzaController@guardarCotizacionEspecial')->name('guardarCotizacionEspecial');

	Route::get('guardarCotizacionEspecialOp','PagoProveedorController@guardarCotizacionEspecial')->name('guardarCotizacionEspecialOp');

	Route::get('ajaxProforma','ProformaController@ajaxProforma')->name('ajaxProforma');

	/*Direccion para subir imagenes por Ajax*/

	Route::get('controlarNC','ProformaController@controlarNC')->name('controlarNC');

	Route::get('doGenerarNC','ContabilidadController@doGenerarNC')->name('doGenerarNC');

	Route::get('indexDetallado/{id?}', ['as' => 'indexDetallado', 'uses' => 'EstadoCuentasController@indexDetallado']);

	Route::post('importExcel', 'ExcelController@importUsers')->name('importExcel');

	Route::get('listadoGrupo','ProformaController@listadoGrupo')->name('listadoGrupo');

	Route::get('listadoDestinos','ProformaController@listadoDestinos')->name('listadoDestinos');

	Route::get('fileDeletePersona','UploadController@fileDeletePersona')->name('fileDeletePersona');
	Route::get('obtenerCiudad','PersonaController@obtenerCiudad')->name('obtenerCiudad');
	Route::get('ajaxDetalleProveedor','FacturarController@ajaxDetalleProveedor')->name('ajaxDetalleProveedor');
	Route::get('ajaxDetalleProveedores','FacturarController@ajaxDetalleProveedores')->name('ajaxDetalleProveedores');
	//TICKET PENDIENTES
	Route::get('ajaxReporteTicketsPendientes','ReporteController@ajaxReporteTicketsPendientes')->name('ajaxReporteTicketsPendientes');
	//PERMISOS
	Route::get('editPermisos','PermisosController@edit')->name('editPermisos');
	Route::get('obtenerPermisos','PermisosController@doEdit')->name('obtenerPermisos');
	Route::get('doGuardarPermiso','PermisosController@doGuardarPermiso')->name('doGuardarPermiso');
	Route::get('getPermisosPersona','PermisosController@getPermisosPersona')->name('getPermisosPersona');
	//FACTURAR
	Route::get('consultaFacturaServerSide','FacturarController@consultaFacturaServerSide')->name('consultaFacturaServerSide');
	Route::get('consultaFacturaServerSideAutorizado','FacturarController@consultaFacturaServerSideAutorizado')->name('consultaFacturaServerSideAutorizado');
	Route::get('ajaxTable','HomeController@ajaxTable')->name('ajaxTable');	
	Route::get('getDatosComentarios','FacturarController@comentarios')->name('getDatosComentarios');
	Route::get('clientesCobro','CobranzaController@clientesCobro')->name('clientesCobro');


    //Vista de Datos en Home 
    Route::get('indexGraphAjax','HomeController@indexGraphAjax')->name('indexGraphAjax');
    Route::get('indexHighchartsAjax','HomeController@indexHighchartsAjax')->name('indexHighchartsAjax');
    Route::get('cancelarOperacion','FacturarController@cancelarOperacion')->name('cancelarOperacion');
	Route::get('comprobarOP','FacturarController@comprobarOP')->name('comprobarOP');
	Route::get('errorFactura','FacturarController@errorFactura')->name('errorFactura');
	Route::get('consultaFactura','FacturarController@consultaFactura')->name('consultaFactura');
	Route::get('anularFactura','FacturarController@anularFactura')->name('anularFactura');
	Route::get('facturarOperativo','FacturarController@facturarOperativo')->name('facturarOperativo');
	Route::get('consultaFacturasPendientes','FacturarController@consultaFacturasPendientes')->name('consultaFacturasPendientes');
	Route::get('consultaReporteFactura','FacturarController@consultaReporteFactura')->name('consultaReporteFactura');
	Route::get('anularDE', 'FacturarController@anularDE')->name('anularDE');

	Route::get('imprimirFactura/{id?}', 'FacturarController@imprimirFactura',  function ($id) { 
				return $id;	})->name('imprimirFactura');
	Route::get('imprimirFacturaOriginal/{id?}', 'FacturarController@imprimirFacturaOriginal',  function ($id) { 
					return $id;	})->name('imprimirFacturaOriginal');
	Route::get('imprimirFacturaPrueba/{id?}', 'FacturarController@imprimirFacturaPrueba',  function ($id) { 
		return $id;	})->name('imprimirFacturaPrueba');
	
	Route::get('agregarLibroVenta','ContabilidadController@addLibroVenta')->name('agregarLibroVenta');

	Route::get('facturar/{id?}', 'FacturarController@facturar',  function ($id) { 
																return $id;	})->name('facturar');

	//VOUCHER
	Route::get('voucher', 'VoucherPdfController@voucherpdfImprimir')->name('voucher');
	//NOTA CREDITO
	Route::get('notaCreditoImprimir/{id?}', 'NotaCreditoController@notaCreditoImprimir', function ($id) {return $id;	})->name('notaCreditoImprimir');
	//REPORTE BSP
	Route::get('consultaBsp','ReporteBspController@consultaBsp')->name('consultaBsp');
	Route::get('guardarAsignacionTicket','TicketsController@guardarAsignacionTicket')->name('guardarAsignacionTicket');
	Route::get('saveLibroVenta','ContabilidadController@saveLibroVenta')->name('saveLibroVenta');

	Route::get('agregarPromocion','PromocionController@add')->name('agregarPromocion');
	Route::post('doAddPromocion', ['as' => 'doAddPromocion', 'uses' => 'PromocionController@doAdd']);

	Route::get('editarPromocion/{id?}','PromocionController@edit', function ($id) {
																				return $id;	
																					})->name('editarPromocion');
	Route::post('doEditPromocion', ['as' => 'doEditPromocion', 'uses' => 'PromocionController@doEdit']);
	Route::get('reportePromocion','PromocionController@index')->name('reportePromocion');
	Route::get('consultaPromocion','PromocionController@doIndex')->name('consultaPromocion');
	

	//Cotizacion
	Route::get('cotizacionAdd','CotizacionController@add')->name('cotizacionAdd');
	Route::get('indice-cotizacion/add','CotizacionController@addIndice')->name('indice_cotizacion.add');
	Route::get('indice-cotizacion/save','CotizacionController@saveIndice')->name('indice_cotizacion.save');
	Route::get('cotizacionEdit','CotizacionController@edit')->name('cotizacionEdit');
	//Cotizacion-Ajax
	Route::get('consultaCot','CotizacionController@consultaCotizacion')->name('consultaCot');
	Route::get('consultaIndiceCot','CotizacionController@consultaIndiceCot')->name('consultaIndiceCot');
	Route::get('doAddCotizacion','CotizacionController@doAddCotizacion')->name('doAddCotizacion');
	Route::get('doInfoCotizacion','CotizacionController@doInfoCotizacion')->name('doInfoCotizacion');
	Route::get('saveTimbradoVenta','ContabilidadController@saveTimbradoVenta')->name('saveTimbradoVenta');
	Route::get('agregar-cotizacion-indice','CotizacionController@addCotizacionIndice')->name('cotizacion_indice.add');

	//PERSONAS
	Route::get('personaAdd','PersonaController@getDatosPersona')->name('personaAdd');
	Route::get('cambiarClave','PersonaController@cambiarClave')->name('cambiarClave');
	Route::get('obtenerEmpresaAgencia','PersonaController@obtenerEmpresaAgencia')->name('obtenerEmpresaAgencia');
	Route::get('actualizarPersona','PersonaController@actualizarPersona')->name('actualizarPersona');
	Route::get('consultaPersona','PersonaController@consultaPersona')->name('consultaPersona');
	Route::get('getDatosAgencia','PersonaController@getDatosAgencia')->name('getDatosAgencia');
	Route::get('getDatosVendedores','PersonaController@getDatosVendedores')->name('getDatosVendedores');
	Route::get('getDatoPersonaEstado','PersonaController@getDatoPersonaEstado')->name('getDatoPersonaEstado');
	Route::get('validarCorreo','PersonaController@validarCorreo')->name('validarCorreo');
	Route::get('generarHash','PersonaController@generarHash')->name('generarHash');
	Route::get('generarHashPrueba','PersonaController@generarHashPrueba')->name('generarHashPrueba');

	Route::get('crearCiudadPais','PersonaController@crearCiudadPais')->name('crearCiudadPais');
	Route::get('personaEdit/{id?}','PersonaController@edit',  function ($id) { 
																return $id;	})->name('personaEdit');
	Route::get('personaDelete/{id?}','PersonaController@delete',  function ($id) { 
																return $id;	})->name('personaDelete');
	Route::get('guardarPersona','PersonaController@add')->name('guardarPersona');
	Route::get('getAjaxAcuerdo','PersonaController@getAjaxAcuerdo')->name('getAjaxAcuerdo');
	Route::post('personaUpload', ['as' => 'personaUpload', 'uses' => 'UploadController@addPersonaImagen']);
	Route::post('lcUpload', ['as' => 'lcUpload', 'uses' => 'UploadController@lcUpload']);
	Route::post('lcUploadItem', ['as' => 'lcUploadItem', 'uses' => 'UploadController@lcUploadItem']);
	Route::get('ajaxReserva','HomeController@ajaxReserva')->name('ajaxReserva');

	// Route::post('guardarPersona', ['as' => 'guardarPersona', 'uses' => 'PersonaController@add']);
	Route::get('obtenerSucursalAgencia','PersonaController@obtenerSucursalAgencia')->name('obtenerSucursalAgencia');
	/*Direccion para subir imagenes por Ajax*/
	Route::get('incentivoVendedor','PersonaController@incentivoVendedor')->name('incentivoVendedor');
	Route::get('modificarIncentivo','PersonaController@modificarIncentivo')->name('modificarIncentivo');
	Route::get('obtenerIncentivo','PersonaController@obtenerIncentivo')->name('obtenerIncentivo');
	Route::get('markupAgencia','PersonaController@markupAgencia')->name('markupAgencia');
	Route::post('modificarMarkupAgencia', ['as' => 'modificarMarkupAgencia', 'uses' => 'PersonaController@modificarMarkupAgencia']);
	Route::get('getSucursalEmpresaPersona','PersonaController@getSucursalEmpresaPersona')->name('getSucursalEmpresaPersona');
	Route::get('obtenerIncentivoVenta','ProformaController@obtenerIncentivoVenta')->name('obtenerIncentivoVenta');
	Route::get('getUsuarioListado','ProformaController@getUsuarioListado')->name('getUsuarioListado');

	Route::get('asignarCuentaContable/{id?}','FormaCobroClienteController@asignarCuentaContable', function ($id) { return $id;	})->name('asignarCuentaContable');
	Route::get('mostrarVentasPendientes','ContabilidadController@mostrarVentasPendientes')->name('mostrarVentasPendientes');
		
	//ABM CENTRO COSTO
	Route::get('listadoCentroCosto','PersonaController@listadoCentroCosto')->name('listadoCentroCosto');
	Route::get('ajaxListadoCentroCosto','PersonaController@ajaxListadoCentroCosto')->name('ajaxListadoCentroCosto');
	Route::get('getDataCentroCosto','PersonaController@getDataCentroCosto')->name('getDataCentroCosto');
	Route::get('setDataCentroCosto','PersonaController@setDataCentroCosto')->name('setDataCentroCosto');

	//ABM DATOS DE PARAMETROS
	Route::get('abmParametros','ConfiguracionController@abmParametros')->name('abmParametros');
	Route::get('setDataParametro','ConfiguracionController@setDataParametro')->name('setDataParametro');

	Route::get('getConfiguracionEmpresa','ConfiguracionController@getConfiguracionEmpresa')->name('getConfiguracionEmpresa');
	Route::get('guardarConfiguracion','ConfiguracionController@guardarConfiguracion')->name('guardarConfiguracion');
	Route::get('gesturCuentaContable','ConfiguracionController@gesturCuentaContable')->name('gesturCuentaContable');
	Route::get('guardarCuentaEmpresa','ConfiguracionController@guardarCuentaEmpresa')->name('guardarCuentaEmpresa');
	Route::get('obtenerPlanCuentas','ConfiguracionController@obtenerPlanCuentas')->name('obtenerPlanCuentas');
	Route::get('anularCuentaEmpresa','ConfiguracionController@anularCuentaEmpresa')->name('anularCuentaEmpresa');
	Route::get('doAddNegocio','ConfiguracionController@doAddNegocio')->name('doAddNegocio');
	Route::get('obtenerCuentas','ConfiguracionController@obtenerCuentas')->name('obtenerCuentas');
	Route::get('negocioEdit/{id?}','ConfiguracionController@negocioEdit', function ($id) { 
																			return $id;	
																						})->name('negocioEdit');
	Route::get('doDeleteNegocio/{id?}','ConfiguracionController@doDeleteNegocio', function ($id) { 
																			return $id;	
																						})->name('doDeleteNegocio');
																
	Route::get('doEditNegocio','ConfiguracionController@doEditNegocio')->name('doEditNegocio');
	
	//Negociacion
	Route::get('negociacionAdd','NegociacionController@add')->name('negociacionAdd');
	Route::get('negociacionEdit','NegociacionController@edit')->name('negociacionEdit');
	Route::get('guardarNegociacion','NegociacionController@guardarNegociacion')->name('guardarNegociacion');
	Route::post('consultaNegociacion', ['as' => 'consultaNegociacion', 'uses' => 'NegociacionController@consultaNegociacion']);
	Route::get('actualizarNegociacion','NegociacionController@actualizarNegociacion')->name('actualizarNegociacion');
	Route::get('getDatosNegociacion','NegociacionController@getDatosNegociacion')->name('getDatosNegociacion');
	//Operativo-Ajax
	Route::get('getDetallesAjax','ControlOperacionesController@getDetallesAjax')->name('getDetallesAjax');
	Route::get('guardarEditDetalleProforma','ControlOperacionesController@add')->name('guardarEditDetalleProforma');
	Route::get('guardarEstado','ControlOperacionesController@guardarEstado')->name('guardarEstado');
	Route::get('guardarVoucherOperaciones','ControlOperacionesController@guardarVoucher')->name('guardarVoucherOperaciones');
	Route::get('guardarComentario','ControlOperacionesController@guardarComentario')->name('guardarComentario');
	Route::get('autorizarProforma','ControlOperacionesController@autorizarProforma')->name('autorizarProforma');
	Route::get('infoProforma','ControlOperacionesController@infoProforma')->name('infoProforma');
	Route::get('obtenerMarkup/{id?}', function($id){
		return $personas = Proforma::where('id',$id)->get(['markup']);
	})->name('obtenerMarkup');
	Route::get('guardarCredito','LineaCreditoController@guardarCredito')->name('guardarCredito');
	Route::post('consultaCredito', ['as' => 'consultaCredito', 'uses' => 'LineaCreditoController@consultaCredito']);
	Route::get('lineaCreditoAdd/{id?}','LineaCreditoController@add', function ($id) { 
																return $id;	})->name('lineaCreditoAdd');

	Route::get('paisAdd','PaisController@add')->name('paisAdd');
	Route::get('doAddPais','PaisController@doAddPais')->name('doAddPais');
	Route::get('paisEdit','PaisController@edit')->name('paisEdit');
	Route::get('doEditPais','PaisController@doEditPais')->name('doEditPais');
	Route::get('paisDelete','PaisController@delete')->name('paisDelete');
	Route::get('tipoPersonaAdd','TipoPersonaController@add')->name('tipoPersonaAdd');
	Route::get('doAddTipoPersona','TipoPersonaController@doAddTipoPersona')->name('doAddTipoPersona');
	Route::get('tipoPersonaEdit','TipoPersonaController@edit')->name('tipoPersonaEdit');
	Route::get('doEditTipoPersona','TipoPersonaController@doEditTipoPersona')->name('doEditTipoPersona');
	Route::get('tipoPersonaDelete','TipoPersonaController@delete')->name('tipoPersonaDelete');
	Route::get('tipoPersoneriaAdd','TipoPersoneriaController@add')->name('tipoPersoneriaAdd');
	Route::get('doAddTipoPersoneria','TipoPersoneriaController@doAddTipoPersoneria')->name('doAddTipoPersoneria');
	Route::get('tipoPersoneriaEdit','TipoPersoneriaController@edit')->name('tipoPersoneriaEdit');
	Route::get('doEditTipoPersoneria','TipoPersoneriaController@doEditTipoPersoneria')->name('doEditTipoPersoneria');
	Route::get('tipoPersoneriaDelete','TipoPersoneriaController@delete')->name('tipoPersoneriaDelete');
	Route::get('tipoIdentidadAdd','TipoIdentidadController@add')->name('tipoIdentidadAdd');
	Route::get('doAddTipoIdentidad','TipoIdentidadController@doAddTipoIdentidad')->name('doAddTipoIdentidad');
	Route::get('tipoIdentidadEdit','TipoIdentidadController@edit')->name('tipoIdentidadEdit');
	Route::get('doEditTipoIdentidad','TipoIdentidadController@doEditTipoIdentidad')->name('doEditTipoIdentidad');
	Route::get('tipoIdentidadDelete','TipoIdentidadController@delete')->name('tipoIdentidadDelete');
	Route::get('tipoTimbradoAdd','TipoTimbradoController@add')->name('tipoTimbradoAdd');
	Route::get('doAddTipoTimbrado','TipoTimbradoController@doAddTipoTimbrado')->name('doAddTipoTimbrado');
	Route::get('tipoTimbradoEdit','TipoTimbradoController@edit')->name('tipoTimbradoEdit');
	Route::get('doEditTipoTimbrado','TipoTimbradoController@doEditTipoTimbrado')->name('doEditTipoTimbrado');
	Route::get('tipoTimbradoDelete','TipoTimbradoController@delete')->name('tipoTimbradoDelete');
	Route::get('sucursalEmpresaAdd','SucursalEmpresaController@add')->name('sucursalEmpresaAdd');
	Route::get('doAddSucursalEmpresa','SucursalEmpresaController@doAddSucursalEmpresa')->name('doAddSucursalEmpresa');
	Route::get('sucursalEmpresaEdit','SucursalEmpresaController@edit')->name('sucursalEmpresaEdit');
	Route::get('doEditSucursalEmpresa','SucursalEmpresaController@doEditSucursalEmpresa')->name('doEditSucursalEmpresa');
	Route::get('sucursalEmpresaDelete','SucursalEmpresaController@delete')->name('sucursalEmpresaDelete');
	//Grupos
	Route::get('grupoAdd','GruposController@addGrupo')->name('grupoAdd');
	Route::post('storeGrupo', ['as' => 'storeGrupo', 'uses' => 'GruposController@storeGrupo']);
	Route::put('updateGrupo/{id?}','GruposController@updateGrupo')->name('updateGrupo');
	Route::get('editGrupo/{id?}','GruposController@editGrupo', function ($id) {return $id;})->name('editGrupo');
	Route::get('verDetalle/{id?}', 'GruposController@verDetalle')->name('verDetalle');
	Route::get('destinoGrupo','GruposController@destinos')->name('destinoGrupo');
	Route::post('uploadDocument', ['as' => 'uploadDocument', 'uses' => 'GruposController@uploadDocument']);
	Route::get('getGrupo', 'GruposController@getGrupo')->name('getGrupo');
	Route::get('ticketAjaxServerSide','TicketsController@ticketAjaxServerSide')->name('ticketAjaxServerSide');
	Route::post('store', ['as' => 'store', 'uses' => 'TicketsController@store']);
		Route::get('create','TicketsController@create')->name('create');
		Route::get('edit/{id?}', ['as' => 'edit', 'uses' => 'TicketsController@edit']);
		Route::post('editarTicket', ['as' => 'editarTicket', 'uses' => 'TicketsController@editarTicket']);

		Route::put('update/{id?}','TicketsController@update')->name('update');
	Route::get('verDetalleTicket/{id?}', 'TicketsController@verDetalleTicket')->name('verDetalleTicket');
	Route::get('addUser/{id?}', ['as' => 'addUser', 'uses' => 'TicketsController@addUser']);
	Route::post('saveUser/{id?}', ['as' => 'saveUser', 'uses' => 'TicketsController@saveUser']);
	Route::get('getTicket', 'TicketsController@getTicket')->name('getTicket');
	Route::get('cambiarIva', 'TicketsController@cambiarIva')->name('cambiarIva');
	Route::get('cambiarComision', 'TicketsController@cambiarComision')->name('cambiarComision');

	Route::get('cambiarEstadoTicket', 'TicketsController@cambiarEstadoTicket')->name('cambiarEstadoTicket');
		Route::get('getCuenta', 'EstadoCuentasController@getCuenta')->name('getCuenta');
	Route::get('getAgencia', 'EstadoCuentasController@getAgencia')->name('getAgencia');
	Route::get('getDetallesCuenta', 'EstadoCuentasController@getDetallesCuenta')->name('getDetallesCuenta');
	Route::get('getVoucherUpdate','ProformaController@getVoucherUpdate')->name('getVoucherUpdate');
	Route::get('getTarjetaPost','ProformaController@getTarjetaPost')->name('getTarjetaPost');
	Route::get('getProveedor','ProformaController@getProveedor')->name('getProveedor');
	Route::get('getProveedorProfoma','ProformaController@getProveedorProfoma')->name('getProveedorProfoma');

	Route::get('getPrestador','ProformaController@getPrestador')->name('getPrestador');
	Route::get('filtrarDatos','ProformaController@filtrarDatos')->name('filtrarDatos');
	Route::get('procesarTicket','ProformaController@procesarTicket')->name('procesarTicket');
	Route::get('doItem','ProformaController@doItem')->name('doItem');

	Route::get('getTicketEditar','ProformaController@getTicketEditar')->name('getTicketEditar');
	Route::get('getProveedorPrestador','ProformaController@getProveedorPrestador')->name('getProveedorPrestador');
	Route::get('getRestaurarTicket','ProformaController@getRestaurarTicket')->name('getRestaurarTicket');
	Route::get('filtrarDatosLista','ProformaController@filtrarDatosLista')->name('filtrarDatosLista');
	Route::get('fileAdjuntoEliminar','UploadController@fileDelDTPlus')->name('fileAdjuntoEliminar');
	Route::post('uploadAdjunto', ['as' => 'uploadAdjunto', 'uses' => 'UploadController@uploadAdjunto']);
	Route::post('uploadDocumentosRecibos', ['as' => 'uploadDocumentosRecibos', 'uses' => 'UploadController@uploadDocumentosRecibos']);
	Route::get('fileEliminar','UploadController@fileEliminar')->name('fileEliminar');
	Route::get('getDatosFecha','ProformaController@getDatosFecha')->name('getDatosFecha');
	Route::get('mostrarDatosDetalle','ProformaController@mostrarDatosDetalle')->name('mostrarDatosDetalle');
	Route::get('fileAdjuntoEliminarDocumento','UploadController@fileAdjuntoEliminarDocumento')->name('fileAdjuntoEliminarDocumento');
	Route::get('fileAdjuntoEliminarLC','UploadController@fileAdjuntoEliminarLC')->name('fileAdjuntoEliminarLC');
	Route::get('fileAdjuntoRecibo','UploadController@fileAdjuntoRecibo')->name('fileAdjuntoRecibo');

	Route::get('getConfirmacion','ProformaController@getConfirmacion')->name('getConfirmacion');
	Route::get('getTrasladorEditar','ProformaController@getTrasladorEditar')->name('getTrasladorEditar');
	Route::get('actualizarCabecera','ProformaController@actualizarCabecera')->name('actualizarCabecera');
	Route::get('anularProforma','ProformaController@anularProforma')->name('anularProforma');
	Route::get('getDestinosProforma','ProformaController@nombreDestino')->name('getDestinosProforma');
	Route::get('destinoProformaGrupo','ProformaController@destinoProformaGrupo')->name('destinoProformaGrupo');

	Route::get('beneficiarioCheque','PagoProveedorController@beneficiarioCheque')->name('beneficiarioCheque');
	Route::get('cuentaCliente','CuentaCorrienteController@cuentaCliente')->name('cuentaCliente');
	Route::get('cuentaProveedor','CuentaCorrienteController@cuentaProveedor')->name('cuentaProveedor');


	
	//Migración 
	//Compras
	Route::get('createMigracionCompras','MigracionCompraController@createMigracionCompras')->name('createMigracionCompras');
	Route::get('getMigracionCompra','MigracionCompraController@getMigracionCompra')->name('getMigracionCompra');
	Route::get('getMigracionVenta','MigracionVentaController@getMigracionVenta')->name('getMigracionVenta');
	Route::get('guardarAsistencia','ProformaController@guardarAsistencia')->name('guardarAsistencia');
 	Route::get('guardarAsistenciaAU','ProformaController@guardarAsistenciaAU')->name('guardarAsistenciaAU');
	Route::get('getAsistencia','ProformaController@getAsistencia')->name('getAsistencia');
	Route::get('getAsistenciaUA','ProformaController@getAsistenciaUA')->name('getAsistenciaUA');
	Route::get('getDetalleProducto','ProformaController@getDetalleProducto')->name('getDetalleProducto');
	Route::post('generarExcelCompra', ['as' => 'generarExcelCompra', 'uses' => 'ExcelController@generarExcelCompra']);
	
	Route::post('generarExcelPersona', ['as' => 'generarExcelPersona', 'uses' => 'ExcelController@generarExcelPersona']);
	
	Route::post('generarExcelMayorCuenta', ['as' => 'generarExcelMayorCuenta', 'uses' => 'ExcelController@generarExcelMayorCuenta']);
	
	Route::post('generarExcelPlan', ['as' => 'generarExcelPlan', 'uses' => 'ExcelController@generarExcelPlan']);

	Route::post('generarExcelResumenCuenta', ['as' => 'generarExcelResumenCuenta', 'uses' => 'ExcelController@generarExcelResumenCuenta']);
	Route::post('generarExcelResumenCuentaProveedors', ['as' => 'generarExcelResumenCuentaProveedors', 'uses' => 'ExcelController@generarExcelResumenCuentaProveedors']);

	Route::post('generarExcelDetalleCuenta', ['as' => 'generarExcelDetalleCuenta', 'uses' => 'ExcelController@generarExcelDetalleCuenta']);
	Route::post('generarExcelDetalleSinSubtotal', ['as' => 'generarExcelDetalleSinSubtotal', 'uses' => 'ExcelController@generarExcelDetalleSinSubtotal']);

	Route::post('generarExcelMovimientoCuenta', ['as' => 'generarExcelMovimientoCuenta', 'uses' => 'ExcelController@generarExcelMovimientoCuenta']);

	Route::post('generarExcelLibroVenta', ['as' => 'generarExcelLibroVenta', 'uses' => 'ExcelController@generarExcelLVenta']);

	Route::post('generarExcelHechaukaLc', ['as' => 'generarExcelHechaukaLc', 'uses' => 'ExcelController@generarExcelHechaukaLc']);

	Route::post('generarExcelHechaukaLv', ['as' => 'generarExcelHechaukaLv', 'uses' => 'ExcelController@generarExcelHechaukaLv']);

	Route::post('generarExcelRecibos', ['as' => 'generarExcelRecibos', 'uses' => 'ExcelController@generarExcelRecibos']);

	Route::post('generarExcelReporteCierre', ['as' => 'generarExcelReporteCierre', 'uses' => 'ExcelController@generarExcelReporteCierre']);
	
	Route::post('generarExcelAnticipo', ['as' => 'generarExcelAnticipo', 'uses' => 'ExcelController@generarExcelAnticipo']);

	Route::post('generarExcelProducto', ['as' => 'generarExcelProducto', 'uses' => 'ExcelController@generarExcelProducto']);
	
	Route::post('generarExcelListAnticipo', ['as' => 'generarExcelListAnticipo', 'uses' => 'ExcelController@generarExcelListAnticipo']);

	Route::post('generarExcelDetalleCuentaProveedor', ['as' => 'generarExcelDetalleCuentaProveedor', 'uses' => 'ExcelController@generarExcelDetalleCuentaProveedor']);
	Route::post('generarExcelDetalleSinSubtotalProveedor', ['as' => 'generarExcelDetalleSinSubtotalProveedor', 'uses' => 'ExcelController@generarExcelDetalleSinSubtotalProveedor']);

	Route::post('generarExcelPagoProv', ['as' => 'generarExcelPagoProv', 'uses' => 'ExcelController@generarExcelPagoProv']);

	Route::post('generarExcelCostoProveedor', ['as' => 'generarExcelCostoProveedor', 'uses' => 'ExcelController@generarExcelCostoProveedor']);
	
	Route::post('generarExcelTicketsPendientes', ['as' => 'generarExcelTicketsPendientes', 'uses' => 'ExcelController@generarExcelTicketsPendientes']);
	Route::post('generarExcelLibroCompra', ['as' => 'generarExcelLibroCompra', 'uses' => 'ExcelController@generarExcelLibroCompra']);
	Route::post('generarExcelLibroCompraFondoFijo', ['as' => 'generarExcelLibroCompraFondoFijo', 'uses' => 'ExcelController@generarExcelLibroCompraFondoFijo']);
	Route::post('generarExcelMigracionAsiento', ['as' => 'generarExcelMigracionAsiento', 'uses' => 'ExcelController@generarExcelMigracionAsiento']);
	Route::post('generarExcelLiquidaciones', ['as' => 'generarExcelLiquidaciones', 'uses' => 'ExcelController@generarExcelLiquidaciones']);

	Route::post('editarVoucher', ['as' => 'editarVoucher', 'uses' => 'ProformaController@editarVoucher']);

	Route::post('generarExcelVenta', ['as' => 'generarExcelVenta', 'uses' => 'ExcelController@generarExcelVenta']);
	Route::get('actualizarPrecioTicket','ProformaController@actualizarPrecioTicket')->name('actualizarPrecioTicket');
	Route::get('verFactura/{id?}', 'FacturarController@verFactura',  function ($id) { 
																return $id;	})->name('verFactura');
	Route::get('verFacturas/{id?}', 'FacturarController@verVentaFactura',  function ($id) { 
																return $id;	})->name('verFacturas');

	Route::get('notaCredito/{id?}', 'PedidoNotaCreditoController@add',  function ($id) { 
																return $id;	})->name('notaCredito');
	Route::get('notaCreditoVenta/{id?}', 'PedidoNotaCreditoController@addVenta',  function ($id) { 
																	return $id;	})->name('notaCreditoVenta');
	

	Route::get('doAddNotaCredito','PedidoNotaCreditoController@doAdd')->name('doAddNotaCredito');
	Route::get('controlarSaldoDisponibleNc','PedidoNotaCreditoController@controlarSaldoDisponibleNc')->name('controlarSaldoDisponibleNc');
	
	Route::get('consultaNota','PedidoNotaCreditoController@consultaNota')->name('consultaNota');
	Route::get('verNota/{id?}', 'PedidoNotaCreditoController@verNota',  function ($id) { 
																return $id;	})->name('verNota');
	Route::get('verNotaVenta/{id?}', 'PedidoNotaCreditoController@verNotaVenta',  function ($id) { 
		return $id;	})->name('verNotaVenta');
	Route::get('proformaRefacturacion','PedidoNotaCreditoController@proformaRefacturacion')->name('proformaRefacturacion');//arturo para refacturacion
	Route::get('getGenearLibroVenta','PedidoNotaCreditoController@getGenearLibroVenta')->name('getGenearLibroVenta');														
	Route::get('anularNota','PedidoNotaCreditoController@anularNota')->name('anularNota');
	Route::get('getMontoDetalle','ProformaController@getMontoDetalle')->name('getMontoDetalle');
	Route::get('sincronizarSaldo','HomeController@sincronizarSaldo')->name('sincronizarSaldo');
	Route::get('doSincronizarSaldo','HomeController@doSincronizarSaldo')->name('doSincronizarSaldo');	
	Route::get('sincronizarSaldoG','HomeController@sincronizarSaldoG')->name('sincronizarSaldoG');
	Route::get('doSincronizarSaldoG','HomeController@doSincronizarSaldoG')->name('doSincronizarSaldoG');	
	Route::get('calcularVenta','ProformaController@calcularVenta')->name('calcularVenta');
	Route::get('calcularMarkup','ProformaController@calcularMarkup')->name('calcularMarkup');
	Route::get('getDescripcion','ProformaController@getDescripcion')->name('getDescripcion');
	Route::get('buscarPrestador','ProformaController@buscarPrestador')->name('buscarPrestador');
	Route::get('doPrestadorProforma','ProformaController@doPrestadorProforma')->name('doPrestadorProforma');
	Route::get('guardarPrestador','ProformaController@guardarPrestador')->name('guardarPrestador');
	Route::get('getAdjuntosVoucher','FacturarController@getAdjuntosVoucher')->name('getAdjuntosVoucher');	
	Route::get('ajaxTickets','TicketsController@ajaxTickets')->name('ajaxTickets');
	Route::get('ajaxTicket','TicketsController@ajaxTicket')->name('ajaxTicket');
	Route::get('mostrarFacturas','TicketsController@mostrarFacturas')->name('mostrarFacturas');
	Route::get('asignarFactura','TicketsController@asignarFactura')->name('asignarFactura');

	Route::get('agregarLiquidacion','LiquidacionesController@addLiqManual')->name('agregarLiquidacion');
	Route::get('doAddLiqMan','LiquidacionesController@doAddLiquidacion')->name('doAddLiqMan');

	Route::get('asignar_tc','TicketsController@asignar_tc')->name('asignar_tc');

	Route::post('generarExcelFactura', ['as' => 'generarExcelFactura', 'uses' => 'ExcelController@generarExcelFactura']);
	Route::post('generarExcelTickets', ['as' => 'generarExcelTickets', 'uses' => 'ExcelController@generarExcelTickets']);
	Route::post('generarExcelPagoProveedor', ['as' => 'generarExcelPagoProveedor', 'uses' => 'ExcelController@generarExcelPagoProveedor']);
	Route::post('generarExcelReporteTickets', ['as' => 'generarExcelReporteTickets', 'uses' => 'ExcelController@generarExcelReporteTickets']);
	Route::post('generarExcelReporteFactura', ['as' => 'generarExcelReporteFactura', 'uses' => 'ExcelController@generarExcelReporteFactura']);
	Route::post('generarExcelReportePendiente', ['as' => 'generarExcelReportePendiente', 'uses' => 'ExcelController@generarExcelReportePendiente']);
	Route::post('generarExcelReporteBSP', ['as' => 'generarExcelReporteBSP', 'uses' => 'ExcelController@generarExcelReporteBSP']);
	Route::post('generarExcelReporteBSPCobranza', ['as' => 'generarExcelReporteBSPCobranza', 'uses' => 'ExcelController@generarExcelReporteBSPCobranza']);

	//Productos
	Route::get('addProducto','ProductosController@add')->name('addProducto');
	Route::get('guardarProducto','ProductosController@guardarProducto')->name('guardarProducto');
	Route::get('actualizarProducto','ProductosController@actualizarProducto')->name('actualizarProducto');
	Route::get('getDatosProductos','ProductosController@getDatosProductos')->name('getDatosProductos');
	Route::get('deleteIconProducto','UploadController@deleteIconProducto')->name('deleteIconProducto');
	Route::post('productoIconUpload', ['as' => 'productoIconUpload', 'uses' => 'UploadController@productoIconUpload']);
	Route::get('editProducto/{id?}','ProductosController@edit', function ($id) { 
														return $id;	})->name('editProducto');
	//Proformas
	Route::get('proformaAdd','ProformaController@add')->name('proformaAdd');
	Route::get('destinoProforma','ProformaController@destinos')->name('destinoProforma');
	Route::get('getDatosCliente','ProformaController@getDatosCliente')->name('getDatosCliente');
	Route::get('getCotizacion','ProformaController@getCotizacion')->name('getCotizacion');
	Route::post('doAddProforme', ['as' => 'doAddProforme', 'uses' => 'ProformaController@doAddProforme']);
	Route::get('doPasajeroProforma','ProformaController@doPasajeroProforma')->name('doPasajeroProforma');
	Route::get('datosProforma','ProformaController@datosProforma')->name('datosProforma');
	Route::get('getDatosProforma','ProformaController@getDatosProforma')->name('getDatosProforma');
	Route::get('getCosto','ProformaController@getCosto')->name('getCosto');
	Route::get('actualizarProforma','ProformaController@actualizarProforma')->name('actualizarProforma');
	Route::get('guardarFila','ProformaController@guardarFila')->name('guardarFila');
	Route::get('getPasajero','ProformaController@getPasajero')->name('getPasajero');
	Route::get('getDetallesPasajero','ProformaController@getDetallesPasajero')->name('getDetallesPasajero');
	Route::get('doGuardarProforma','ProformaController@doGuardarProforma')->name('doGuardarProforma');
	Route::get('getCargaCliente','ProformaController@getCargaCliente')->name('getCargaCliente');
	Route::get('getSaldoProforma','ProformaController@getSaldoProforma')->name('getSaldoProforma');
	Route::get('getCargaPasajero','ProformaController@getCargaPasajero')->name('getCargaPasajero');
	Route::get('getDetalleProforma','ProformaController@getDetalleProforma')->name('getDetalleProforma');
	Route::get('prestadorProforma','ProformaController@prestadorProforma')->name('prestadorProforma');
	Route::get('proveedorProforma','ProformaController@proveedorProforma')->name('proveedorProforma');

	Route::post('guardarVoucher', ['as' => 'guardarVoucher', 'uses' => 'ProformaController@doGuardarVoucher']);
		
	Route::get('getComisionAgencia','ProformaController@getComisionAgencia')->name('getComisionAgencia');
	Route::get('guardarTarjeta','ProformaController@guardarTarjeta')->name('guardarTarjeta');
	Route::get('getProducto','ProformaController@getProducto')->name('getProducto');
	Route::get('deleteLinea','ProformaController@deleteLinea')->name('deleteLinea');
	Route::get('updateDetalle','ProformaController@updateDetalle')->name('updateDetalle');
	Route::get('verificarProforma','ProformaController@verificarProforma')->name('verificarProforma');
	Route::get('solicitarVerificacionProforma','ProformaController@solicitarVerificacionProforma')->name('solicitarVerificacionProforma');
	Route::get('previewFactura/{id?}','FacturarController@previewFactura', function ($id) {
															return $id;	
																})->name('previewFactura');
	Route::get('detallesProforma/{id?}','ProformaController@detalles', function ($id) {
															return $id;	
																})->name('detallesProforma');
	Route::get('detalleCopy/{id?}','ProformaController@detalleCopy', function ($id) {
															return $id;	
																})->name('detalleCopy');
	//reporte de proformas															
	Route::get('reporteProformas','ProformaController@reporteProformas')->name('reporteProformas');		
	Route::post('generarExcelReporteProformasEstado', ['as' => 'generarExcelReporteProformasEstado', 'uses' => 'ExcelController@generarExcelReporteProformasEstado']);


	Route::get('verDatosProformas','ProformaController@verDatosProformas')->name('verDatosProformas');
	Route::get('resumenOp/{id?}','PagoProveedorController@pdfOp', function ($id) {
															return $id;	
														});

	Route::get('reporteProformas','ProformaController@reporteProformas')->name('reporteProformas');		
	Route::post('generarExcelReporteProformasEstado', ['as' => 'generarExcelReporteProformasEstado', 'uses' => 'ExcelController@generarExcelReporteProformasEstado']);

	Route::get('fileAdjuntoRecibo','UploadController@fileAdjuntoRecibo')->name('fileAdjuntoRecibo');
	
	Route::post('uploadDocumentosRecibos', ['as' => 'uploadDocumentosRecibos', 'uses' => 'UploadController@uploadDocumentosRecibos']);

	Route::get('cuentaClientePdf','CuentaCorrienteController@cuentaClientePdf')->name('cuentaClientePdf');
	Route::get('cuentaProveedorPdf','CuentaCorrienteController@cuentaProveedorPdf')->name('cuentaProveedorPdf');

	Route::post('generarExcelListadoNotaCredito', ['as' => 'generarExcelListadoNotaCredito', 'uses' => 'ExcelController@generarExcelListadoNotaCredito']); 

	Route::post('generarExcelListadoVentas', ['as' => 'generarExcelListadoVentas', 'uses' => 'ExcelController@generarExcelListadoVentas']);
	Route::post('generarExcelFormaCobro', ['as' => 'generarExcelFormaCobro', 'uses' => 'ExcelController@generarExcelFormaCobro']);
	Route::post('generarExcelProforma', ['as' => 'generarExcelProforma', 'uses' => 'ExcelController@generarExcelProforma']);

	Route::get('actualizarProveedor','HomeController@actualizarProveedor')->name('actualizarProveedor');
	Route::get('listarProveedor','HomeController@listarProveedor')->name('listarProveedor');
	Route::get('pagosReservaNemo','PagoProveedorController@pagosReservaNemo')->name('pagosReservaNemo');

	//EXCEL REPORTE PEDIDO DE JOAQUIN
    Route::get('generarExcelClienteReportDos','ExcelController@generarExcelClienteReportDos')->name('generarExcelClienteReportDos');
    Route::get('generarExcelClienteReportUno','ExcelController@generarExcelClienteReportUno')->name('generarExcelClienteReportUno');
    //Auditoría
    Route::get('getAuditoriaPersonas', 'AuditoriaController@getAuditoriaPersonas')->name('getAuditoriaPersonas');
    Route::get('getAuditoriaProductos', 'AuditoriaController@getAuditoriaProductos')->name('getAuditoriaProductos');
	//REPORTE DE RIESGO    
	Route::get('ajaxReporteRiesgo','FacturarController@ajaxReporteRiesgo')->name('ajaxReporteRiesgo');
	Route::get('generarExcelReporteRiesgo','ExcelController@generarExcelReporteRiesgo')->name('generarExcelReporteRiesgo');
	
	Route::post('generarExcelAsiento', ['as' => 'generarExcelAsiento', 'uses' => 'ExcelController@generarExcelAsiento']);


	Route::get('consultarDetalleLiquidaciones','ReporteController@consultarDetalleLiquidaciones')->name('consultarDetalleLiquidaciones');

	//REPORTE VENTA POR SUCURSAL
	Route::get('ajaxReporteSucursal','ReporteController@ajaxReporteSucursal')->name('ajaxReporteSucursal');
	Route::get('ajaxReporteVenta','ReporteController@ajaxReporteVenta')->name('ajaxReporteVenta');
	Route::get('negocioAdd','ConfiguracionController@negocioAdd')->name('negocioAdd');

	//Dtp Puntos
	Route::get('dtp-puntos','DtpPuntosController@listado')->name('liquidar_dtp_puntos.index');
	Route::get('dtp-puntos/reporte-puntos','DtpPuntosController@reportePuntos')->name('liquidar_dtp_puntos.reporte_puntos');
	Route::get('dtp-puntos/excel-puntos','DtpPuntosController@excel')->name('liquidar_dtp_puntos.excel');
	Route::get('dtp-puntos/liquidar','DtpPuntosController@index')->name('liquidar_dtp_puntos.ver_liquidacion');
	
	Route::post('dtp-puntos/liquidar-ajax','DtpPuntosController@generarLiquidaciones')->name('liquidar_dtp_puntos.liquidar');
	Route::get('dtp-puntos/consultar-liquidaciones','DtpPuntosController@consultarLiquidaciones')->name('liquidar_dtp_puntos.cabecera');
	Route::get('dtp-puntos/consultar-detalles-liquidaciones','DtpPuntosController@consultarDetalleLiquidaciones')->name('liquidar_dtp_puntos.detalle');
	Route::get('dtp-puntos/generar-plantilla','DtpPuntosController@generar_excel_template')->name('liquidar_dtp_puntos.generar_excel_template');
	Route::post('dtp-puntos/cargar-excel-comision','DtpPuntosController@cargar_excel_comision')->name('liquidar_dtp_puntos.cargar_excel_comision');
	Route::get('dtp-puntos/get-factura-aplicar','DtpPuntosController@getFacturasAplicar')->name('liquidar_dtp_puntos.get_facturas_aplicar');
	Route::get('dtp-puntos/aplicar-factura','DtpPuntosController@pagarComision')->name('liquidar_dtp_puntos.asignarFacturaDtpPuntos');
	Route::get('dtp-puntos/reporte_puntos/cabecera/ajax','DtpPuntosController@reportePuntosCabecera')->name('liquidar_dtp_puntos.reporte_puntos.cabecera');
	Route::get('dtp-puntos/reporte_puntos/detalle/ajax','DtpPuntosController@reportePuntosDetalle')->name('liquidar_dtp_puntos.reporte_puntos.detalle');
	Route::get('dtp-puntos/reporte_puntos/voucher-pdf', 'DtpPuntosController@generarVoucherPdf')->name('liquidar_dtp_puntos.reporte_puntos.voucherpdf');
	
	Route::get('VentaNetoSucursal','HomeController@VentaNetoSucursal')->name('VentaNetoSucursal');

	//REPORTE PARA BUSQUEDA
	Route::get('ajaxBuscarReserva','ProformaController@ajaxBuscarReserva')->name('ajaxBuscarReserva');
	
	//REPORTE INCENTIVO
	Route::get('getdatosDTPLus','ReporteController@getdatosDTPLus')->name('getdatosDTPLus');

	//Liquidar comisiones
	Route::post('generarLiquidaciones', ['as' => 'generarLiquidaciones', 'uses' => 'LiquidacionesController@generarLiquidaciones']);


	//FALLO DE CAJA
	Route::get('ajaxFalloCaja','ReporteController@ajaxFalloCaja')->name('ajaxFalloCaja');

	//Verificar Detalle Proforma
	Route::get('verificarDetalle','ControlOperacionesController@verificarDetalle')->name('verificarDetalle');
	Route::get('verificarAdjunto','ControlOperacionesController@verificarAdjunto')->name('verificarAdjunto');

	//OBTENER DESTINO PARA PROFORMA EN OPERATIVO
	Route::get('obtenerDestino','ControlOperacionesController@obtenerDestino')->name('obtenerDestino');

	//ALMACENAR CALIFICACION POST VENTA
	Route::get('almacenarCalificacion','ProformaController@almacenarCalificacion')->name('almacenarCalificacion');
	Route::get('getpostventa','ProformaController@getpostventa')->name('getpostventa');

	Route::get('getVencimiento','ProformaController@getVencimiento')->name('getVencimiento');


	//NOTIFICACIONES
	Route::get('getNotificacionTicket','NotificacionController@getNotificacionTicket')->name('getNotificacionTicket');
	Route::get('getNotificacionProceso','NotificacionController@getNotificacionProceso')->name('getNotificacionProceso');

	//SOLICITUD EMISION TICKET
	Route::post('solicitarEmisionTicket', ['as' => 'solicitarEmisionTicket', 'uses' => 'ProformaController@solicitarEmisionTicket']);
	Route::post('getInfoEmisionTicket', ['as' => 'getInfoEmisionTicket', 'uses' => 'ProformaController@getInfoEmisionTicket']);

	//FORMA PAGO
	Route::get('setformaPago','ProformaController@setformaPago')->name('setformaPago');
	Route::get('getformaPago','ProformaController@getformaPago')->name('getformaPago');

	Route::get('getFormaCobro','FormaCobroClienteController@getFormaCobro')->name('getFormaCobro');
	Route::get('cuentasFactura','ProformaController@cuentasFactura')->name('cuentasFactura');
	Route::get('getResumenVendedor','ProformaController@getResumenCliente')->name('getResumenVendedor');
	Route::get('getCuentaCorrienteVendedor','ProformaController@getCuentaCorrienteCliente')->name('getCuentaCorrienteVendedor');



	//VALIDAR CEDULA - PERSONAS
	Route::get('validarCedula','PersonaController@validarCedula')->name('validarCedula');

	//SERVICIOS DTP
	Route::get('servicioDtp','ProformaController@servicioDtp')->name('servicioDtp');
	Route::get('asignarProformaServicios','ProformaController@asignarProformaServicios')->name('asignarProformaServicios');
	Route::get('asignarProformaReserva','ProformaController@asignarProformaReserva')->name('asignarProformaReserva');

	Route::get('buscarServicio','ProformaController@buscarServicio')->name('buscarServicio');
    Route::get('getCancelarReservas','ProformaController@getCancelarReservas')->name('getCancelarReservas');

	Route::get('procesarPago','ProformaController@procesarPago')->name('procesarPago');
	Route::get('eliminarItemsPago','ProformaController@eliminarItemsPago')->name('eliminarItemsPago');
	Route::get('getAdelanto','GruposController@getAdelanto')->name('getAdelanto');
	Route::get('getEliminarAdelanto','GruposController@getEliminarAdelanto')->name('getEliminarAdelanto');
	Route::get('doGrupoCom','ProformaController@doGrupoCom')->name('doGrupoCom');

	//REPORTE GRUPO/ANTICIPO
	Route::get('ajaxGrupoAnticipo','ReporteController@ajaxGrupoAnticipo')->name('ajaxGrupoAnticipo');
	Route::get('getDetallesGrupoAnticipo','ReporteController@getDetallesGrupoAnticipo')->name('getDetallesGrupoAnticipo');
	

    //CUENTAS CONTABLE
    Route::get('setDataPlanCuenta','CuentaContableController@setDataPlanCuenta')->name('setDataPlanCuenta');
    Route::get('getDataPlanCuenta','CuentaContableController@getDataPlanCuenta')->name('getDataPlanCuenta');
    Route::get('controlCodigo','CuentaContableController@controlCodigo')->name('controlCodigo');
    Route::get('getCuentaInfo','CuentaContableController@getCuentaInfo')->name('getCuentaInfo');
    Route::get('saveCuentaInfo','CuentaContableController@saveCuentaInfo')->name('saveCuentaInfo');
	Route::get('adjuntoLibroCompras','ContabilidadController@adjuntoLibroCompras')->name('adjuntoLibroCompras');
	Route::get('getRevisarCotizacion','ProformaController@getRevisarCotizacion')->name('getRevisarCotizacion');
    Route::get('getPersonaNegociacion','ContabilidadController@getPersonaNegociacion')->name('getPersonaNegociacion');
	Route::get('getRevisarCotizacion','ProformaController@getRevisarCotizacion')->name('getRevisarCotizacion');
    //Libros Compras
    Route::get('mostrarLibroCompra','ContabilidadController@mostrarLibroCompra')->name('mostrarLibroCompra');

    Route::get('mostrarLibroCompraFijo','ContabilidadController@mostrarLibroCompraFijo')->name('mostrarLibroCompraFijo');

    Route::get('verDetalle','ContabilidadController@verDetalle')->name('verDetalle');
    Route::get('verDetalleLc','ContabilidadController@verDetalleLc')->name('verDetalleLc');

    Route::get('verAsiento','ContabilidadController@verAsiento')->name('verAsiento');
	Route::get('verAsientoCompra','ContabilidadController@verAsientoCompra')->name('verAsientoCompra');
	Route::get('addLibroCompra','ContabilidadController@addLibroCompra')->name('addLibroCompra');
	Route::get('addLibroCompraFijo','ContabilidadController@addLibroCompraFijo')->name('addLibroCompraFijo');

	Route::get('generarFacturaParcial','FacturarController@generarFacturaParcial')->name('generarFacturaParcial');

	Route::get('saveLibroCompra','ContabilidadController@saveLibroCompra')->name('saveLibroCompra');

	Route::get('saveLibroCompraFijo','ContabilidadController@saveLibroCompraFijo')->name('saveLibroCompraFijo');
	Route::get('guardarRepriceLibro','ContabilidadController@guardarRepriceLibro')->name('guardarRepriceLibro');
	Route::get('modificarFechaLibro/{id?}','ContabilidadController@modificarFechaLibro')->name('modificarFechaLibro');
	Route::get('modificarReprice/{id?}','ContabilidadController@modificarReprice')->name('modificarReprice');
	Route::get('obtenerDataCliente','ContabilidadController@obtenerDataCliente')->name('obtenerDataCliente');
	Route::get('obtenerDataCliente/libro-compra','ContabilidadController@obtenerDataClienteLc')->name('obtenerDataCliente.lc');
	Route::get('saveTimbrado','ContabilidadController@saveTimbrado')->name('saveTimbrado');
	Route::get('obtenerFechaVencimiento','ContabilidadController@obtenerFechaVencimiento')->name('obtenerFechaVencimiento');
	Route::get('verLibroCompra/{id?}','ContabilidadController@verLibroCompra', function ($id) {return $id;})->name('verLibroCompra');

	Route::get('verLibroVenta/{id?}','ContabilidadController@verLibroVenta', function ($id) {return $id;})->name('verLibroVenta');

	Route::get('anularLibroCompra','ContabilidadController@anularLibroCompra')->name('anularLibroCompra');
	Route::post('doAddTimbrado', ['as' => 'doAddTimbrado', 'uses' => 'TimbradoController@doAddTimbrado']);
	Route::post('doEditTimbrado', ['as' => 'doEditTimbrado', 'uses' => 'TimbradoController@doEditTimbrado']);
	Route::get('aplicacionAnticipos','CobranzaController@aplicacionAnticipos')->name('aplicacionAnticipos');
	Route::get('anularLibroVenta','ContabilidadController@anularLibroVenta')->name('anularLibroVenta');


	Route::get('getAplicacionAnticipos','CobranzaController@getAplicacionAnticipos')->name('getAplicacionAnticipos');
	Route::get('aplicacionAnticipoProveedor','PagoProveedorController@aplicacionAnticipoProveedor')->name('aplicacionAnticipoProveedor');
	Route::get('getAplicacionAnticiposProveedor','PagoProveedorController@getAplicacionAnticiposProveedor')->name('getAplicacionAnticiposProveedor');
	Route::post('generarExcelAplicacionAnticipos', ['as' => 'generarExcelAplicacionAnticipos', 'uses' => 'ExcelController@generarExcelAplicacionAnticipos']);
	Route::post('generarExcelAplicacionAnticiposProveedor', ['as' => 'generarExcelAplicacionAnticiposProveedor', 'uses' => 'ExcelController@generarExcelAplicacionAnticiposProveedor']);

	Route::get('aplicacionAnticiposPdf','CobranzaController@aplicacionAnticiposPdf')->name('aplicacionAnticiposPdf');
	Route::get('aplicacionAnticiposProveedorPdf','PagoProveedorController@aplicacionAnticiposProveedorPdf')->name('aplicacionAnticiposProveedorPdf');

	Route::post('doEditFormaCobro', ['as' => 'doEditFormaCobro', 'uses' => 'FormaCobroClienteController@doEditFormaCobro']);
	Route::get('ventaCotizado','ProformaController@ventaCotizado')->name('ventaCotizado');
	Route::get('mostrarListadoFacturas','ContabilidadController@mostrarListadoFacturas')->name('mostrarListadoFacturas');
	Route::get('mostrarListadoFacturasFF','ContabilidadController@mostrarListadoFacturasFF')->name('mostrarListadoFacturasFF');

	Route::get('getCotizacionLc','ContabilidadController@getCotizacionLc')->name('getCotizacionLc');
	Route::get('getSolicitarFactura','ProformaController@getSolicitarFactura')->name('getSolicitarFactura');
	Route::get('getDesposito','CobranzaController@getDesposito')->name('getDesposito');
	Route::get('aplicacionAnticipos','CobranzaController@aplicacionAnticipos')->name('aplicacionAnticipos');
	Route::get('getAplicacionAnticipos','CobranzaController@getAplicacionAnticipos')->name('getAplicacionAnticipos');
	Route::get('aplicacionAnticipoProveedor','PagoProveedorController@aplicacionAnticipoProveedor')->name('aplicacionAnticipoProveedor');
	Route::get('getAplicacionAnticiposProveedor','PagoProveedorController@getAplicacionAnticiposProveedor')->name('getAplicacionAnticiposProveedor');
	Route::post('generarExcelAplicacionAnticipos', ['as' => 'generarExcelAplicacionAnticipos', 'uses' => 'ExcelController@generarExcelAplicacionAnticipos']);
	Route::post('generarExcelAplicacionAnticiposProveedor', ['as' => 'generarExcelAplicacionAnticiposProveedor', 'uses' => 'ExcelController@generarExcelAplicacionAnticiposProveedor']);

	Route::get('aplicacionAnticiposPdf','CobranzaController@aplicacionAnticiposPdf')->name('aplicacionAnticiposPdf');
	Route::get('aplicacionAnticiposProveedorPdf','PagoProveedorController@aplicacionAnticiposProveedorPdf')->name('aplicacionAnticiposProveedorPdf');
	//CUENTAS GASTOS
	Route::get('agregarCuentasGasto','ContabilidadController@cuentaGasto')->name('agregarCuentasGasto');
	Route::get('doGuardarCuenta','ContabilidadController@saveCuentaGastos')->name('doGuardarCuenta');
	Route::get('reporteCuentasGasto','ContabilidadController@indexCuentaGasto')->name('reporteCuentasGasto');
	Route::get('consultaCuentaGasto','ContabilidadController@consultaCuentaGasto')->name('consultaCuentaGasto');
	
	 	//Ingresos
	 	Route::get('ingresos','ReporteController@ingresos')->name('ingresos.index');
		Route::get('ingresos/ajax/index','ReporteController@ajaxIngresos')->name('ingresos.ajax.index');
	

	//PAGO PROVEEDOR
	Route::get('getListProveedor','PagoProveedorController@getListProveedor')->name('getListProveedor');
	Route::get('getListFondoFijo','PagoProveedorController@getListFondoFijo')->name('getListFondoFijo');
    Route::get('getCotizacionFP','PagoProveedorController@getCotizacionFP')->name('getCotizacionFP');
    Route::get('getCotizacionFPO','PagoProveedorController@getCotizacionFPO')->name('getCotizacionFPO');
	Route::get('getCotizacionFPR','CobranzaController@getCotizacionFPR')->name('getCotizacionFPR');

    Route::get('cotizarMontoFP','PagoProveedorController@cotizarMontoFP')->name('cotizarMontoFP');
    Route::get('getListCuentas','PagoProveedorController@getListCuentas')->name('getListCuentas');
	Route::post('setPagoProveedor', ['as' => 'setPagoProveedor', 'uses' => 'PagoProveedorController@setPagoProveedor']);

	Route::post('setPagoProveedorFondoFijo', ['as' => 'setPagoProveedorFondoFijo', 'uses' => 'PagoProveedorController@setPagoProveedorFondoFijo']);

	Route::get('cotizarMontosProveedor','PagoProveedorController@cotizarMontosProveedor')->name('cotizarMontosProveedor');
	Route::get('getRetencion','PagoProveedorController@getRetencion')->name('getRetencion');
	Route::get('getRetencionRecibo','PagoProveedorController@getRetencion')->name('getRetencionRecibo');
	Route::get('agregarComision','ComisionesController@add')->name('agregarComision');
	Route::get('doAgregarComision','ComisionesController@doAdd')->name('doAgregarComision');
	Route::get('reporteComision','ComisionesController@index')->name('reporteComision');
	Route::get('ajaxReporteComision','ComisionesController@getComision')->name('ajaxReporteComision');
	Route::get('reporteDetalleComision','ComisionesController@getComisionDetalle')->name('reporteDetalleComision');
	Route::get('anularEsalaComision','ComisionesController@delete')->name('anularEsalaComision');
	Route::get('activarEsalaComision','ComisionesController@activar')->name('activarEsalaComision');

		/////////////comisiones aereas //////////////////////////////////////////////////////////////////
		Route::get('agregarComisionAerea','ComisionesController@agregar')->name('agregarComisionAerea');
		Route::get('datosComisionAerea','ComisionesController@getDatos')->name('datosComisionAerea');
		Route::get('guardarComisionAerea','ComisionesController@guardarDatos')->name('guardarComisionAerea');
	
	

	Route::get('numMaxCheque','PagoProveedorController@numMaxCheque')->name('numMaxCheque');
	Route::post('personaUpload', ['as' => 'personaUpload', 'uses' => 'UploadController@addPersonaImagen']);
	Route::post('adjuntoDocumentosOp', ['as' => 'adjuntoDocumentosOp', 'uses' => 'UploadController@adjuntoDocumentosOp']);
	Route::post('adjuntoDocumentosOpCopia', ['as' => 'adjuntoDocumentosOpCopia', 'uses' => 'UploadController@adjuntoDocumentosOpCopia']);//para multiples adjuntos en op
	Route::get('fileDeleteOp','UploadController@fileDeleteOp')->name('fileDeleteOp');
	Route::get('fileDeleteOpCopia','UploadController@fileDeleteOpCopia')->name('fileDeleteOpCopia');
	Route::get('aplicarOp/{id?}','PagoProveedorController@aplicarOp',function ($id) {return $id;	})->name('aplicarOp');
	Route::get('reporteOp','PagoProveedorController@reporteOp')->name('reporteOp');
	
	    //VERIFICAR-OP
		Route::get('controlOp/{id?}','PagoProveedorController@controlOp',function ($id) {return $id;	})->name('controlOp');
		Route::get('estadoOp','PagoProveedorController@estadoOp')->name('estadoOp');
		Route::get('guardarGastoOp','PagoProveedorController@guardarGastoOp')->name('guardarGastoOp');
		Route::get('cotrolSeleccionOp','PagoProveedorController@cotrolSeleccionOp')->name('cotrolSeleccionOp');
		Route::get('listPagoOp','PagoProveedorController@listPagoOp')->name('listPagoOp');
		Route::get('insertGasto','PagoProveedorController@insertGasto')->name('insertGasto');
		Route::get('deleteGasto','PagoProveedorController@deleteGasto')->name('deleteGasto');
		Route::get('getGastos','PagoProveedorController@getGastos')->name('getGastos');
		Route::get('insertFp','PagoProveedorController@insertFp')->name('insertFp');
		Route::get('getFp','PagoProveedorController@getFp')->name('getFp');
		Route::get('deleteFp','PagoProveedorController@deleteFp')->name('deleteFp');
		Route::get('procesarOp','PagoProveedorController@procesarOp')->name('procesarOp');
		Route::get('op/guardarNumeroComprobantes','PagoProveedorController@guardarNumeroComprobantes')->name('op.guardarNumeroComprobantes');

	Route::get('getAsientoDetalles','BancosController@getAsientoDetalles')->name('getAsientoDetalles');
	Route::get('getEliminarCtaCtte','FormaCobroClienteController@getEliminarCtaCtte')->name('getEliminarCtaCtte');
	Route::get('getAsignarCtaCtte','FormaCobroClienteController@getAsignarCtaCtte')->name('getAsignarCtaCtte');
    //BANCOS
    Route::get('reporteBanco','BancosController@reporteBanco')->name('reporteBanco');
	Route::get('registrarMovimiento','BancosController@registrarMovimiento')->name('registrarMovimiento');
	Route::get('ajaxReporteCuentaBanco','BancosController@ajaxReporteCuentaBanco')->name('ajaxReporteCuentaBanco');
	Route::get('transferenciaCuentas','BancosController@transferenciaCuenta')->name('transferenciaCuentas');
	Route::get('cotizarMontoTransferencia','BancosController@cotizarMontoTransferencia')->name('cotizarMontoTransferencia');
	Route::get('setTransferencia','BancosController@setTransferencia')->name('setTransferencia');
	Route::get('ajaxReporteTransferenciaCuenta','BancosController@ajaxReporteTransferenciaCuenta')->name('ajaxReporteTransferenciaCuenta');
	Route::get('anularTransferencia','BancosController@anularTransferencia')->name('anularTransferencia');


	//Extracción
	Route::get('movimientoBancario','BancosController@indexMovimientoBancario')->name('movimientoBancario');
	Route::get('generarMovimientoBancario','BancosController@generarMovimientoBancario')->name('generarMovimientoBancario');
	Route::get('imp_fact_pedido_original/{id?}','VentasRapidasController@imp_fact_pedido_original', function ($id) {return $id;})->name('imp_fact_pedido_original');

	Route::get('imp_fact_pedido_original/{id?}','VentasRapidasController@imp_fact_pedido_original', function ($id) {return $id;})->name('imp_fact_pedido_original');

	//ABM CUENTAS															
	Route::get('ajaxListadoCuenta','BancosController@ajaxListadoCuenta')->name('ajaxListadoCuenta');
	Route::get('getDataCuenta','BancosController@getDataCuenta')->name('getDataCuenta');
	Route::get('setDataCuenta','BancosController@setDataCuenta')->name('setDataCuenta');

	//ABM  CUENTA DETALLE
	Route::get('ajaxListadoCuentaDetalle','BancosController@ajaxListadoCuentaDetalle')->name('ajaxListadoCuentaDetalle');
	Route::get('getDataCuentaDetalle','BancosController@getDataCuentaDetalle')->name('getDataCuentaDetalle');
	Route::get('setDataCuentaDetalle','BancosController@setDataCuentaDetalle')->name('setDataCuentaDetalle');
	
	//ABM TIPO CUENTA
	Route::get('ajaxListadoTipoCuenta','BancosController@ajaxListadoTipoCuenta')->name('ajaxListadoTipoCuenta');
	Route::get('getDataTipoCuenta','BancosController@getDataTipoCuenta')->name('getDataTipoCuenta');
	Route::get('setDataTipoCuenta','BancosController@setDataTipoCuenta')->name('setDataTipoCuenta');
	
	//TIMBRADO 
	Route::get('indexTimbrado','TimbradoController@index')->name('indexTimbrado');
	Route::get('addTimbrado','TimbradoController@add')->name('addTimbrado');
	Route::get('editTimbrado','TimbradoController@edit')->name('editTimbrado');
	Route::get('buscarTimbrado','TimbradoController@buscarTimbrado')->name('buscarTimbrado');



    //REPORTE DE OP
    Route::get('reporteOp','PagoProveedorController@reporteOp')->name('reporteOp');

	//REPORTE DE ASIENTOS CONTABLES
	Route::get('ajaxReporteAsiento','ReporteController@ajaxReporteAsiento')->name('ajaxReporteAsiento');
	Route::get('ajaxReporteAsientoDetalle','ReporteController@ajaxReporteAsientoDetalle')->name('ajaxReporteAsientoDetalle');
	
	//CUENTA CORRIENTE
	Route::get('indexCttaCorriente','ReporteController@ajaxReporteAsientoDetalle')->name('indexCttaCorriente');
	Route::get('getCuentaCorriente','CuentaCorrienteController@getCuentaCorriente')->name('getCuentaCorriente');
	Route::get('consultarDetalleCuentas','CuentaCorrienteController@consultarDetalleCuentas')->name('consultarDetalleCuentas');
	Route::get('getTipoPersona','CuentaCorrienteController@getTipoPersona')->name('getTipoPersona');

	//Libros Ventas
	Route::get('indexLibrosVentas','ContabilidadController@indexLibrosVentas')->name('indexLibrosVentas');

	Route::get('mostrarLibroVenta','ContabilidadController@mostrarLibroVenta')->name('mostrarLibroVenta');

	//COBRANZA
	Route::get('listadoCobroAjax','CobranzaController@listadoCobroAjax')->name('listadoCobroAjax');
	Route::get('setCalcRecibo','CobranzaController@setCalcRecibo')->name('setCalcRecibo');
	Route::get('setCrearRecibo','CobranzaController@setCrearRecibo')->name('setCrearRecibo');
	Route::get('imprimirRecibo/{id?}','CobranzaController@imprimirRecibo', function ($id) {return $id;})->name('imprimirRecibo');
	Route::get('controlRecibo/{id?}','CobranzaController@controlRecibo', function ($id) {return $id;})->name('controlRecibo');
	Route::get('aplicarRecibo/{id?}','CobranzaController@aplicarRecibo', function ($id) {return $id;})->name('aplicarRecibo');
	Route::get('ajaxTableRecibo','CobranzaController@ajaxTableRecibo')->name('ajaxTableRecibo');
	Route::get('ajaxTableReciboFormaPago','CobranzaController@ajaxTableReciboFormaPago')->name('ajaxTableReciboFormaPago');
	Route::get('insertFpRecibo','CobranzaController@insertFpRecibo')->name('insertFpRecibo');
	Route::get('getFpRecibo','CobranzaController@getFpRecibo')->name('getFpRecibo');
	Route::get('deleteFpRecibo','CobranzaController@deleteFpRecibo')->name('deleteFpRecibo');
	Route::get('setMontoCotizado','CobranzaController@setMontoCotizado')->name('setMontoCotizado');
	Route::get('estadoRecibo','CobranzaController@estadoRecibo')->name('estadoRecibo');
	Route::get('correoRecibo/{id?}','CobranzaController@correoRecibo', function ($id) {return $id;})->name('correoRecibo');
	Route::get('cotizarMontosRecibo','CobranzaController@cotizarMontosRecibo')->name('cotizarMontosRecibo');
	Route::get('imprimirReciboDuplicadoLista','CobranzaController@imprimirReciboDuplicadoLista')->name('imprimirReciboDuplicadoLista');
	Route::get('getCtaCtb','CobranzaController@getCtaCtb')->name('getCtaCtb');
	Route::get('getListFormaCobro','CobranzaController@getListFormaCobro')->name('getListFormaCobro');
	Route::get('getResumenFormaCobro','CobranzaController@getResumenFormaCobro')->name('getResumenFormaCobro');
	Route::post('saveAplicarDeposito','CobranzaController@saveAplicarDeposito')->name('saveAplicarDeposito');
	Route::post('depositoUploadComprobante','UploadController@depositoUploadComprobante')->name('depositoUploadComprobante');
	Route::get('depositoUploadComprobante/delete','UploadController@depositoUploadComprobanteDelete')->name('depositoUploadComprobante.delete');
	

	Route::get('depositoRecaudaciones','CobranzaController@depositoBancario')->name('depositoRecaudaciones');
	Route::get('reporteDepositoRecaudaciones','CobranzaController@reporteDepositoRecaudaciones')->name('reporteDepositoRecaudaciones');
	Route::get('ajaxreporteDepositoDetalle','CobranzaController@ajaxreporteDepositoDetalle')->name('ajaxreporteDepositoDetalle');
	Route::get('ajaxreporteDepositoCabecera','CobranzaController@ajaxreporteDepositoCabecera')->name('ajaxreporteDepositoCabecera');
	Route::get('getCargarGrupos','ProformaController@getCargarGrupos')->name('getCargarGrupos');
	Route::get('getCargarExpedientes','ProformaController@getCargarExpedientes')->name('getCargarExpedientes');	
	Route::get('validarFormaPago','CobranzaController@validarFormaPago')->name('validarFormaPago');
	Route::get('getFpReciboControl','CobranzaController@getFpReciboControl')->name('getFpReciboControl');
	Route::get('indexCanje','CobranzaController@indexCanje')->name('indexCanje');
	Route::get('canjeTable','CobranzaController@canjeTable')->name('canjeTable');
	Route::get('getFactRetencionRecibo','CobranzaController@getFactRetencionRecibo')->name('getFactRetencionRecibo');
	Route::get('verificarRetenciones','CobranzaController@verificarRetenciones')->name('verificarRetenciones');
	Route::get('getAnticipoList','CobranzaController@getAnticipoList')->name('getAnticipoList');
	Route::get('saveFpRecibo','CobranzaController@saveFpRecibo')->name('saveFpRecibo');
	Route::get('getEditFpRecibo','CobranzaController@getEditFpRecibo')->name('getEditFpRecibo');
	Route::get('modificarRetenciones/{id?}','CobranzaController@modificarRetenciones', function ($id) {return $id;})->name('modificarRetenciones');
	Route::get('getCanjeRetencion','CobranzaController@getCanjeRetencion')->name('getCanjeRetencion');
	Route::get('getDataCanjeRetencion','CobranzaController@getDataCanjeRetencion')->name('getDataCanjeRetencion');
	Route::get('vendedorRentabilidad','ReporteController@vendedorRentabilidad')->name('vendedorRentabilidad');
	Route::get('getResumenVendedorIncentivo','ReporteController@getResumenVendedorIncentivo')->name('getResumenVendedorIncentivo');
	Route::get('getDetalleVendedorIncentivo','ReporteController@getDetalleVendedorIncentivo')->name('getDetalleVendedorIncentivo');
	Route::post('generarExcelIncentivoVendedor', ['as' => 'generarExcelIncentivoVendedor', 'uses' => 'ExcelController@generarExcelIncentivoVendedor']);
	Route::post('generarExcelIncentivoVendedorDetalle', ['as' => 'generarExcelIncentivoVendedorDetalle', 'uses' => 'ExcelController@generarExcelIncentivoVendedorDetalle']);
	Route::get('agregarEquipo','EquipoController@add')->name('agregarEquipo');
	Route::post('doAddEquipo', ['as' => 'doAddEquipo', 'uses' => 'EquipoController@doAdd']);
	Route::get('editarEquipo/{id?}','EquipoController@edit', function ($id) {
																			return $id;	
																			})->name('editarEquipo');
	Route::post('doEditEquipo', ['as' => 'doEditEquipo', 'uses' => 'EquipoController@doEdit']);
	Route::get('reporteEquipo','EquipoController@index')->name('reporteEquipo');
	Route::get('getEquipos','EquipoController@getEquipos')->name('getEquipos');
	Route::get('deleteEquipo','EquipoController@delete')->name('deleteEquipo');


	//LISTADO RETENCION
	Route::get('listadoRetencion','CobranzaController@listadoRetencion')->name('listadoRetencion');
	Route::get('ajaxListadoRetencion','CobranzaController@ajaxListadoRetencion')->name('ajaxListadoRetencion');
	Route::get('getActualizarSaldo','ContabilidadController@getActualizarSaldo')->name('getActualizarSaldo');
	

	//CUENTA CORRIENTE CLIENTE
	Route::get('getCuentaCorrienteCliente','CobranzaController@getCuentaCorrienteCliente')->name('getCuentaCorrienteCliente');
	Route::get('consultarDetalleCuentasCliente','CobranzaController@consultarDetalleCuentasCliente')->name('consultarDetalleCuentasCliente');

	//CUENTA CORRIENTE PROVEEDOR
	// Route::get('getCuentaCorrienteProveedor','PagoProveedorController@getCuentaCorrienteProveedor')->name('getCuentaCorrienteProveedor');
	Route::get('consultarDetalleCuentasProveedor','PagoProveedorController@consultarDetalleCuentasProveedor')->name('consultarDetalleCuentasProveedor');
	Route::get('getEmailCliente','ProformaController@getEmailCliente')->name('getEmailCliente');
	
	Route::get('getDocumentoCliente','ProformaController@getDocumentoCliente')->name('getDocumentoCliente');
	
	//REPORTE GES-996
	Route::post('generarExcelresumencobro','ExcelController@generarExcelresumencobro')->name('generarExcelresumencobro');
	Route::get('resumenCobros','ReporteController@resumenCobros')->name('resumenCobros');
	Route::get('resumenCobrosAjax', 'ReporteController@resumenCobrosAjax')->name('resumenCobrosAjax');
	//FIN GES-966
//MASIVOS PERSONAS
Route::get('personasMasivo/create', 'PersonaController@masivoCreate')->name('personasMasivo.create');
Route::post('personasMasivo', 'PersonaController@masivoStore')->name('personasMasivo.store');
Route::post('generarExcelMasivoPersonas','ExcelController@generarExcelMasivoPersonas')->name('generarExcelMasivoPersonas');

	//REPORTE GES-899

	Route::post('generarExcelVentaPrestador','ExcelController@generarExcelVentaPrestador')->name('generarExcelVentaPrestador');
	Route::get('ventaPrestador','ReporteController@indexVtaPrestador')->name('ventaPrestador');
	Route::get('ventaPrestadorAjax', 'ReporteController@ventaPrestadorAjax')->name('ventaPrestadorAjax');
	Route::get('personasDuplicada', 'PersonaController@personasDuplicada')->name('personasDuplicada');//GES-953
	Route::get('personasDuplicadaAjax', 'PersonaController@personasDuplicadaAjax')->name('personasDuplicadaAjax');//GES-953
	//Route::put('personasDuplicadaEdit/{id?}', 'PersonaController@personasDuplicadaEdit')->name('personasDuplicadaEdit.edit');
	Route::get('personasDuplicadaEdit/{id?}', 'PersonaController@personasDuplicadaEdit')->name('personasDuplicadaEdit.edit');
	Route::get('/ventaPrestador/exportar-excel','ReporteController@exportarExcel')->name('facturapre_excel');
//METAS AGENCIAS GES-972
	Route::get('indexMetrica', 'MetaAgenciaController@index')->name('indexMetrica');
	Route::get('indexMetricaAjax', 'MetaAgenciaController@indexAjax')->name('indexMetricaAjax');
	Route::get('metaAgencias', 'MetaAgenciaController@metaAgencias')->name('metaAgencias');
	Route::get('metaAgenciasAjax', 'MetaAgenciaController@metaAgenciasAjax')->name('metaAgenciasAjax');
	Route::get('metaAgencias/create', 'MetaAgenciaController@create')->name('metaAgencias.create');
	Route::post('metaAgencias', 'MetaAgenciaController@store')->name('metaAgencias.store');
	Route::get('metaAgencias/{id?}/edit', 'MetaAgenciaController@edit')->name('metaAgencias.edit');
	Route::PUT('metaAgencias/{id?}', 'MetaAgenciaController@update')->name('metaAgencias.update');
	Route::post('generarExcelMetaAgencia','ExcelController@generarExcelMetaAgencia')->name('generarExcelMetaAgencia');
	Route::post('Meta-agencia-masivo', 'MetaAgenciaController@AgenciasMasivo')->name('Meta-agencia-masivo');//masivo excel
	Route::post('generarExcelMetasAgencias','ExcelController@generarExcelMetasAgencias')->name('generarExcelMetasAgencias');
	Route::get('historicoAgencias', 'MetaAgenciaController@historicoAgencias')->name('historicoAgencias');
	Route::get('historicoAgenciasAjax', 'MetaAgenciaController@historicoAgenciasAjax')->name('historicoAgenciasAjax');
	Route::get('indexhistorico', 'MetaAgenciaController@indexhistorico')->name('indexhistorico');
	Route::get('indexhistoricoAjax', 'MetaAgenciaController@indexhistoricoAjax')->name('indexhistoricoAjax');
	//SUMA SALDOS PLAN DE CUENTAS 
	Route::get('sumaSaldos', 'ReporteController@sumaSaldos')->name('sumaSaldos');
	Route::get('sumaSaldosAjax', 'ReporteController@sumaSaldosAjax')->name('sumaSaldosAjax');
	Route::post('generarExcelSaldoPlan','ExcelController@generarExcelSaldoPlan')->name('generarExcelSaldoPlan');
	//VENTAS VS NOTA DE CREDITO
	Route::get('ventaNc', 'ReporteController@ventaNc')->name('ventaNc');
	Route::get('ventaNcAjax', 'ReporteController@ventaNcAjax')->name('ventaNcAjax');
	Route::post('generarExcelventaNc','ExcelController@generarExcelventaNc')->name('generarExcelventaNc');
	//REPORTE DETALLES CIERRE DE CAJA
	Route::get('detallesCierreCaja', 'ReporteController@detallesCierreCaja')->name('detallesCierreCaja');
	Route::get('detallesCierreCajaAjax', 'ReporteController@detallesCierreCajaAjax')->name('detallesCierreCajaAjax');
	Route::post('generarExceldetallesCierreCaja','ExcelController@generarExceldetallesCierreCaja')->name('generarExceldetallesCierreCaja');
	//ATK
	Route::get('generarExcelventaRapida','ExcelController@generarExcelventaRapida')->name('generarExcelventaRapida');
	Route::get('ventaRapida','ReporteController@ventaRapida')->name('ventaRapida');
	Route::get('/ventaRapida/exportar-excel','ReporteController@exportarExcel')->name('facturapre_excel');
	//Route::get('generarExcelVentaPrestador', ['as' => 'generarExcelVentaPrestador', 'uses' => 'ExcelController@generarExcelVentaPrestador']);
	//Route::get('generarExcelVentaPrestador', ['as' => 'generarExcelVentaPrestador', 'uses' => 'ExcelController@generarExcelVentaPrestador']);
	//ANTICIPO CLIENTE
	Route::get('indexAnticipoCliente','CobranzaController@indexAnticipoCliente')->name('indexAnticipoCliente');
	Route::get('anticipoAddCliente','CobranzaController@anticipoAddCliente')->name('anticipoAddCliente');
	Route::get('getListAnticipoCliente','CobranzaController@getListAnticipoCliente')->name('getListAnticipoCliente');
	Route::get('saveAnticipoCliente','CobranzaController@saveAnticipoCliente')->name('saveAnticipoCliente');
	Route::get('anticipoAddCobranza','CobranzaController@anticipoAddCobranza')->name('anticipoAddCobranza');
	Route::get('getLisAddAnticipo','CobranzaController@getLisAddAnticipo')->name('getLisAddAnticipo');
	Route::get('getFacturasAplicar','CobranzaController@getFacturasAplicar')->name('getFacturasAplicar');
	Route::get('getFacturasAplicarProveedor','PagoProveedorController@getFacturasAplicar')->name('getFacturasAplicarProveedor');

	Route::get('getLisAddAnticipoProveedor','CobranzaController@getLisAddAnticipoProveedor')->name('getLisAddAnticipoProveedor');

	Route::get('anticipoDetalleCliente/{id?}','CobranzaController@anticipoDetalle', function ($id) {return $id;})->name('anticipoDetalleCliente');
	
	Route::post('generarExcelOp', 'ExcelController@generarExcelOp')->name('generarExcelOp');
	Route::get('getGuardarDetalleFactura','ProformaController@getGuardarDetalleFactura')->name('getGuardarDetalleFactura');
	Route::get('getDetalleFactura','ProformaController@getDetalleFactura')->name('getDetalleFactura');
	
	Route::get('generarJsonFacturaURL/{id?}','FacturarController@generarJsonFacturaURL', function ($id) {
		return $id;	
	});

	Route::get('generarJsonNCURL/{id?}','FacturarController@generarJsonNCURL', function ($id) {
		return $id;	
	});

	//ANTICIPO - PROVEEDOR
	Route::get('getListAnticipo','PagoProveedorController@getListAnticipo')->name('getListAnticipo');
	Route::get('saveAnticipo','PagoProveedorController@saveAnticipo')->name('saveAnticipo');
	Route::get('anticipoAdd','PagoProveedorController@anticipoAdd')->name('anticipoAdd');
	Route::get('anticipoDetalle/{id?}','PagoProveedorController@anticipoDetalle', function ($id) {return $id;})->name('anticipoDetalle');
	Route::get('anularAnticipo','PagoProveedorController@anularAnticipo')->name('anularAnticipo');
	
	//ANTICIPO ANTERIOR !!!!
	Route::get('getListadoPago','CobranzaController@getListadoPago')->name('getListadoPago');
	Route::get('getContContable','CobranzaController@getContContable')->name('getContContable');
	Route::get('addPago','CobranzaController@add')->name('addPago');
	
	Route::get('auditoria-productos','AuditoriaController@indexAuditoriaProductos')->name('indexAuditoriaProductos');
	Route::get('auditoria-productos-listado','AuditoriaController@getProductoListado')->name('getProductoListado');

	//RETENCIONES
	Route::get('reporteRetenciones','ReporteController@reporteRetenciones')->name('reporteRetenciones');											
	Route::get('deletePago','CobranzaController@deletePago')->name('deletePago');

	//Prueba imprimir cheque
	Route::get('indexChequePrueba','ChequesController@indexChequePrueba')->name('indexChequePrueba');
	// Route::post('imprimirCheque', ['as' => 'imprimirCheque', 'uses' => 'ChequesController@imprimirCheque']);

	Route::get('imprimirCheque','ChequesController@imprimirCheque')->name('imprimirCheque');
	
	Route::get('imprimirChequeTransaccion','ChequesController@imprimirChequeTransaccion')->name('imprimirChequeTransaccion');
	// Route::get('imprimirCheque/{id?}','ChequesController@imprimirCheque', function ($id) {return $id;})->name('imprimirCheque');


	Route::get('getDatosPagosDetalle','ReporteController@getDatosPagosDetalle')->name('getDatosPagosDetalle');

	Route::get('getDatosPagos','ReporteController@getDatosPagos')->name('getDatosPagos');

	Route::get('consultaFacturasCobro','ReporteController@consultaFacturasPendientes')->name('consultaFacturasCobro');

	Route::get('consultaGrupoProducto','ProformaController@grupoProducto')->name('consultaGrupoProducto');

	Route::get('consultaPagosServerSide','ReporteController@consultaPagosServerSide')->name('consultaPagosServerSide');

	Route::get('getConfirmarPago','ReporteController@getConfirmarPago')->name('getConfirmarPago');

	Route::get('comentarioPago','ReporteController@comentarioPago')->name('comentarioPago');

	Route::get('corteProveedor','ReporteController@corteProveedor')->name('corteProveedor');
	Route::get('getCorteProveedor','ReporteController@getCorteProveedor')->name('getCorteProveedor');

	Route::get('agregarComentarioPago','ReporteController@agregarComentarioPago')->name('agregarComentarioPago');

	Route::get('ajaxEditClienteComision','ComisionController@ajaxEditClienteComision')->name('ajaxEditClienteComision');
	
	Route::get('getAdjuntosVouchers','ReporteController@getAdjuntosVoucher')->name('getAdjuntosVouchers');
	Route::get('editCliente','ComisionController@editCliente')->name('editCliente');

	Route::get('anularCliente','ComisionController@anularCliente')->name('anularCliente');

	Route::get('getDatoscierre','ReporteController@getDatoscierre')->name('getDatoscierre');
	Route::get('getCierre','ReporteController@getCierre')->name('getCierre');
	Route::get('getResumenCierre','ReporteController@getResumenCierre')->name('getResumenCierre');

	Route::get('getResumenCierreGenerar','ReporteController@getResumenCierreGenerar')->name('getResumenCierreGenerar');
	Route::get('getDatosCierreGenerar','ReporteController@getDatosCierreGenerar')->name('getDatosCierreGenerar');	
	Route::get('getCerrarCaja','ReporteController@getCerrarCaja')->name('getCerrarCaja');

	Route::get('anularCierreCaja','ReporteController@anularCierreCaja')->name('anularCierreCaja');
	Route::get('filtrarTicketTabla','ProformaController@filtrarTicketTabla')->name('filtrarTicketTabla');
	Route::get('datosProformaDetalle','ProformaController@datosProformaDetalle')->name('datosProformaDetalle');
	Route::get('getDatosNemo','ProformaController@getDatosNemo')->name('getDatosNemo');
	Route::get('guardarReservaNemo','ProformaController@guardarReservaNemo')->name('guardarReservaNemo');
	Route::get('getUpdateRetencion','CobranzaController@doEditar')->name('getUpdateRetencion');
	Route::get('getPersonaListado','PersonaController@getPersonaListado')->name('getPersonaListado');
	Route::get('agregarDv','PersonaController@agregarDv')->name('agregarDv');
	Route::get('getClienteListado','ProformaController@getClienteListado')->name('getClienteListado');

	//ABM ASIENTOS
	Route::post('actualizarAsiento', ['as' => 'actualizarAsiento', 'uses' => 'ReporteController@actualizarAsiento']);
	//Route::get('actualizarAsiento','ReporteController@actualizarAsiento')->name('actualizarAsiento');
	Route::get('getCotizacionContable','ReporteController@getCotizacionContable')->name('getCotizacionContable');
	Route::get('getAsientoDetalle','ReporteController@getAsientoDetalle')->name('getAsientoDetalle');
	Route::get('getImporteCotizado','ReporteController@getImporteCotizado')->name('getImporteCotizado');
	Route::get('getPasajeroPrincipal','ProformaController@getPasajeroPrincipal')->name('getPasajeroPrincipal');

	Route::get('getImpuestos','TicketsController@getImpuestos')->name('getImpuestos');    
    Route::get('getPrecioAsistencia','ProformaController@getPrecioAsistencia')->name('getPrecioAsistencia');
	Route::get('getPrecioAsistenciaUA','ProformaController@getPrecioAsistenciaUA')->name('getPrecioAsistenciaUA');
	Route::get('getAsignarZona','ProformaController@getAsignarZona')->name('getAsignarZona');
	//CUENTA CONTABLES PREDETERMINADAS
	Route::get('getCuentaPredeterminada','ContabilidadController@getCuentaPredeterminada')->name('getCuentaPredeterminada');

	Route::get('getAsignarVenta','ProformaController@getAsignarVenta')->name('getAsignarVenta');
	
	Route::get('getReactivarVenta','ProformaController@getReactivarVenta')->name('getReactivarVenta');
	//VENTAS RAPIDAS		
	Route::get('nota_pedido_pdf/{id?}','VentasRapidasController@nota_pedido_pdf', function ($id) {return $id;})->name('nota_pedido_pdf');
	Route::get('getAuditoriaProductos', 'AuditoriaController@getAuditoriaProductos')->name('getAuditoriaProductos');
	Route::get('imp_fact_pedido/{id?}','VentasRapidasController@imp_fact_pedido', function ($id) {return $id;})->name('imp_fact_pedido');

	//::get('imp_fact_pedido','VentasRapidasController@imp_fact_pedido')->name('imp_fact_pedido');

	Route::get('anularFacturaVentaRapida','VentasRapidasController@anularFacturaVentaRapida')->name('anularFacturaVentaRapida');
	Route::get('anularVentaRapida','VentasRapidasController@anularVentaRapida')->name('anularVentaRapida');

	//REPORTE PEDIDOS		
	Route::get('ajaxListadoPedido','VentasRapidasController@ajaxListadoPedido')->name('ajaxListadoPedido');		
	Route::get('subirBsp','UploadController@subirBsp')->name('subirBsp');	
	Route::post('doSubirBsp', ['as' => 'doSubirBsp', 'uses' => 'UploadController@doSubirBsp']);

	
	//COMISION CLIENTE
	Route::get('altaClienteComision','ComisionController@altaClienteComision')->name('altaClienteComision');	
	Route::get('ajaxAltaClienteComision','ComisionController@ajaxAltaClienteComision')->name('ajaxAltaClienteComision');
	Route::get('getAjaxClienteComision','ComisionController@getAjaxClienteComision')->name('getAjaxClienteComision');
	Route::get('getVerCliente','PersonaController@getVerCliente')->name('getVerCliente');

	Route::get('anularOpDetalle','PagoProveedorController@anularOpDetalle')->name('anularOpDetalle');
	Route::get('comprobarFactura','ContabilidadController@comprobarFactura')->name('comprobarFactura');
	Route::get('comprobar-factura-venta','ContabilidadController@comprobarFacturaVenta')->name('comprobarFacturaVenta');

  /*================================================================================================
								PONER AQUI LAS RUTAS TEMPORALES
	================================================================================================*/
	Route::get('venta','ProformaController@addVenta')->name('venta');
	Route::get('getVentasRapidas','VentasRapidasController@getVentasRapidas')->name('getVentasRapidas');
	Route::get('getLatLong','ProformaController@getLatLong')->name('getLatLong');
	Route::get('guardarLatLong','ProformaController@guardarLatLong')->name('guardarLatLong');
															
	/*============================================FIN TEMP===========================================*/
	Route::get('getImprimirNota','ProformaController@getImprimirNota')->name('getImprimirNota');
	Route::get('getImprimirFactura','ProformaController@getImprimirFactura')->name('getImprimirFactura');
	Route::get('getTimbrados','TimbradoController@getTimbrados')->name('getTimbrados');
	Route::get('getEmergencia','ProformaController@getEmergencia')->name('getEmergencia');
	Route::get('getCliente','ProformaController@getCliente')->name('getCliente');
	Route::get('getGuardarProveedor','ProformaController@getGuardarProveedor')->name('getGuardarProveedor');
	Route::get('getProveedorVer','ProformaController@getProveedorVer')->name('getProveedorVer');
	Route::get('getMontoCotizado','ProformaController@getMontoCotizado')->name('getMontoCotizado');
	Route::get('datosPrestadores','PersonaController@datosPrestadores')->name('datosPrestadores');
	
	//ABM MENU NAVEGACION 
	Route::get('addMenu','MenuController@addMenu')->name('addMenu');
	Route::get('agregarNuevoMenu','MenuController@agregarNuevoMenu')->name('agregarNuevoMenu');
	Route::get('indexMenu','MenuController@indexMenu')->name('indexMenu');
	Route::get('verDatosMenu','MenuController@verDatosMenu')->name('verDatosMenu');
	Route::get('guardarEditar','MenuController@guardarEditar')->name('guardarEditar');
	Route::get('editarMenu/{id?}', 'MenuController@editarMenu',  function ($id) { 
																				return $id; })->name('editarMenu');

	//Exportar excel desde tabla asistencias
	Route::post('generarExcelAsistencia', ['as' => 'generarExcelAsistencia', 'uses' => 'ExcelController@generarExcelAsistencia']);

	//ABM ASISTENCIA AL VIAJERO
	Route::get('indexAsistenciaViajero','AsistenciaViajeroController@index')->name('indexAsistenciaViajero');
	Route::get('editarAsistencia/{id?}', 'AsistenciaViajeroController@editarAsistencia',  function ($id) { 
		return $id;	})->name('editarAsistencia');
	Route::get('addAsistencia','AsistenciaViajeroController@addAsistencia')->name('addAsistencia');
	Route::get('agregarNuevoServicio','AsistenciaViajeroController@addSolicitudAsistencia')->name('agregarNuevoServicio');
	Route::get('asistenciasConsulta','AsistenciaViajeroController@verAsistencias')->name('asistenciasConsulta');
	Route::get('guardarEditarAsistencia','AsistenciaViajeroController@guardarEditarAsistencia')->name('guardarEditarAsistencia');

	//Route::get('getDownload','UploadController@getDownload')->name('getDownload');
	Route::post('getDownloadLc', ['as' => 'getDownloadLc', 'uses' => 'UploadController@getDownloadLc']);
	Route::post('getDownloadTesaka', ['as' => 'getDownloadTesaka', 'uses' => 'UploadController@getDownloadTesaka']);
	Route::post('getDownloadLv', ['as' => 'getDownloadLv', 'uses' => 'UploadController@getDownloadLv']);
	Route::get('getMontoCotizado','ProformaController@getMontoCotizado')->name('getMontoCotizado');
	Route::get('cabecerahechaukaLv','MigracionVentaController@cabecerahechaukaLv')->name('cabecerahechaukaLv');
	Route::get('detallehechaukaLv','MigracionVentaController@detallehechaukaLv')->name('detallehechaukaLv');
	Route::get('cabecerahechaukaLc','MigracionCompraController@cabecerahechaukaLc')->name('cabecerahechaukaLc');
	Route::get('detallehechaukaLc','MigracionCompraController@detallehechaukaLc')->name('detallehechaukaLc');
	Route::get('getModificacionesTransferencias','BancosController@getModificacionesTransferencias')->name('getModificacionesTransferencias');
	// TESAKA
	Route::get('consultarTesaka','ContabilidadController@consultarTesaka')->name('consultarTesaka');

	Route::get('getConsultaMigracionAsiento','ContabilidadController@getConsultaMigracionAsiento')->name('getConsultaMigracionAsiento');
	Route::get('getConsultaMayorCuenta','ContabilidadController@getConsultaMayorCuenta')->name('getConsultaMayorCuenta');
	Route::get('getSaldoCuenta','ContabilidadController@getSaldoCuenta')->name('getSaldoCuenta');
	Route::get('getConceptos','BancosController@getConceptos')->name('getConceptos');
	Route::get('getModificarTimbrados','TimbradoController@getModificarTimbrados')->name('getModificarTimbrados');
	Route::post('editarMovimiento','BancosController@editarMovimiento')->name('editarMovimiento');
	Route::get('modificar-transferencia','BancosController@modificarTransferencia')->name('modificarTransferencia');
	
	Route::get('anularMovimientoBancario','BancosController@anularMovimientoBancario')->name('anularMovimientoBancario');

	Route::get('movimientoBancarios','BancosController@movimientoBancarios')->name('movimientoBancarios');
	Route::get('getTimbradosCargados','TimbradoController@getTimbradosCargados')->name('getTimbradosCargados');
	
	Route::get('doUpdateTimbrado','TimbradoController@doUpdateTimbrado')->name('doUpdateTimbrado');
	Route::get('getClienteAuxiliar','ProformaController@getClienteAuxiliar')->name('getClienteAuxiliar');

	Route::get('doPasajeroCliente','ProformaController@doPasajeroCliente')->name('doPasajeroCliente');
	Route::get('doClienteProforma','ProformaController@doClienteProforma')->name('doClienteProforma');
	Route::get('doClienteProformas','ProformaController@doClienteProformas')->name('doClienteProformas');
	Route::get('getEliminrClienteAux','ProformaController@getEliminrClienteAux')->name('getEliminrClienteAux');
	Route::get('getDatosClienteAux','ProformaController@getDatosClienteAux')->name('getDatosClienteAux');

	Route::get('getDataListado','BancosController@getDataListado')->name('getDataListado');
	Route::get('getUsuario','ProformaController@getUsuario')->name('getUsuario');
	Route::get('getDetalleSolicitud','ProformaController@getDetalleSolicitud')->name('getDetalleSolicitud');
	Route::get('getEliminrSolicitud','ProformaController@getEliminrSolicitud')->name('getEliminrSolicitud');

	Route::get('getNotaCredito','PedidoNotaCreditoController@getNotaCredito')->name('getNotaCredito');
	Route::get('guardarNotaCredito','PedidoNotaCreditoController@guardarNotaCredito')->name('guardarNotaCredito');
	Route::get('getExtracto','FacturarController@getExtracto')->name('getExtracto');
	Route::get('anularAplicacionAnticipo','FacturarController@anularAnticipo')->name('anularAplicacionAnticipo');
	Route::get('generarKudePhp','FacturarController@generarKudePhp')->name('generarKudePhp');

	Route::get('getExtractoNC','PedidoNotaCreditoController@getExtractoNC')->name('getExtractoNC');
	Route::get('getCuentaCorrienteCliente','CuentaCorrienteController@getCuentaCorrienteCliente')->name('getCuentaCorrienteCliente');
	Route::get('getResumenCliente','CuentaCorrienteController@getResumenCliente')->name('getResumenCliente');
	Route::get('getTotalesResumenCliente','CuentaCorrienteController@getTotalesResumenCliente')->name('getTotalesResumenCliente');
	Route::get('pagoTicketRealizado','TicketsController@pagoTicketRealizado')->name('pagoTicketRealizado');
	Route::post('getPagoTc','TicketsController@getPagoTc')->name('getPagoTc');
	Route::get('getGuardarClienteAuxiliar','ProformaController@getGuardarClienteAuxiliar')->name('getGuardarClienteAuxiliar');

	Route::get('envioFactura','FacturarController@envioFactura')->name('envioFactura');
	Route::get('envioNC','FacturarController@envioNC')->name('envioNC');

	Route::get('comprobarAsiento','ContabilidadController@comprobarAsiento')->name('comprobarAsiento');

	Route::get('getDetalleSolicitud','ProformaController@getDetalleSolicitud')->name('getDetalleSolicitud');
	Route::get('getEliminrSolicitud','ProformaController@getEliminrSolicitud')->name('getEliminrSolicitud');
	
	Route::get('getResumenProveedor','CuentaCorrienteController@getResumenProveedor')->name('getResumenProveedor');
	Route::get('getCuentaCorrienteProveedor','CuentaCorrienteController@getCuentaCorrienteProveedor')->name('getCuentaCorrienteProveedor');
	Route::get('doControlNombre','ProformaController@doControlNombre')->name('doControlNombre');

	Route::get('guardarAnticipoFactura','CuentaCorrienteController@guardarAnticipoFactura')->name('guardarAnticipoFactura');
	Route::get('getExtractoAnticipo','CobranzaController@getExtractoAnticipo')->name('getExtractoAnticipo');
	Route::get('addFormaCobro','FormaCobroClienteController@add')->name('addFormaCobro');
	Route::get('anularTicket','TicketsController@anularTicket')->name('anularTicket');
	Route::post('doAddFormaCobro', ['as' => 'doAddFormaCobro', 'uses' => 'FormaCobroClienteController@doAddFormaCobro']);
	Route::get('reporteCierrePdf/{id?}','UploadController@reporteCierrePdf', function ($id) {
		return $id;	
	});
	Route::post('doASubirArchivo', ['as' => 'doASubirArchivo', 'uses' => 'UploadController@doASubir']);

	Route::get('getInfoProforma','ProformaController@getInfoProforma')->name('getInfoProforma');

	Route::get('getProductos','ProformaController@getProductos')->name('getProductos');
	Route::get('datosFacturaDetalles','ContabilidadController@facturaDetalles')->name('datosFacturaDetalles');
	Route::get('resumenTrasnferencia/{id?}','BancosController@pdfTransferencia', function ($id) {
		return $id;	
	});

	Route::get('getClienteVenta','ProformaController@getClienteVenta')->name('getClienteVenta');
	Route::get('getProductoVenta','ProformaController@getProductoVenta')->name('getProductoVenta');
	Route::get('getProveedorVenta','ProformaController@getProveedorVenta')->name('getProveedorVenta');
	Route::get('reservasNemo','ProformaController@reservasNemo')->name('reservasNemo');
	Route::get('reservas/nemo/get-datos','ProformaController@reservasNemoDatos')->name('reservasNemoDatos');
	Route::get('reservas/cangoroo/get-datos','ProformaController@reservasCangoroDatos')->name('reservasCangoroDatos');
	Route::post('generarExcelDetalleCuentaVendedor', ['as' => 'generarExcelDetalleCuentaVendedor', 'uses' => 'ExcelController@generarExcelDetalleCuentaVendedor']);
	Route::post('generarExcelDetalleSinSubtotalVendedor', ['as' => 'generarExcelDetalleSinSubtotalVendedor', 'uses' => 'ExcelController@generarExcelDetalleSinSubtotalVendedor']);
	Route::post('generarExcelResumenCuentaVendedor', ['as' => 'generarExcelResumenCuentaVendedor', 'uses' => 'ExcelController@generarExcelResumenCuentaVendedor']);
	Route::get('pagoTicketRealizado','TicketsController@pagoTicketRealizado')->name('pagoTicketRealizado');
	Route::post('generarExcelVentaPendiente','ExcelController@generarExcelVentaPendiente')->name('generarExcelVentaPendiente');
	
	Route::get('generarJsonFacturaLote','FacturarController@generarJsonFacturaLote')->name('generarJsonFacturaLote');

	Route::get('generarJsonNCFactura/{id?}','FacturarController@generarJsonNCFactura', function ($id) {
		return $id;	
	});

	Route::get('generarJsonNCActualizacion','FacturarController@generarJsonNCActualizacion')->name('generarJsonNCActualizacion');

	//Reporte de cuentas
	Route::get('historico-cuenta-banco-ajax','HistoricoController@historicoCuentaBanco')->name('banco.index.historico.ajax');
	Route::get('historico-cierre-caja','HistoricoController@cierre')->name('historico.cierra.caja');
	Route::get('historico-cierre-caja/cabecera-ajax','HistoricoController@getCierre')->name('historico.cierra.caja.cabaera.ajax');
	Route::get('historico-cierre-caja/detalle-ajax','HistoricoController@getResumenCierre')->name('historico.cierra.caja.detalle.ajax');
	Route::get('historico-cierre-caja/pdf/{id?}','HistoricoController@reporteCierrePdf', function ($id) {
		return $id;	
	});


	//Forma de pago Cliente
	Route::get('forma-pago-cliente/cuenta-contabe/{id?}','FormaPagoClienteController@asignarCuentaContable')->name('forma_pago.asignarCuentaContable');
	Route::get('forma-pago-cliente/asignar-cuenta','FormaPagoClienteController@getAsignarCtaCtte')->name('forma_pago.getAsignarCtaCtbPago');
	Route::get('forma-pago-cliente/eliminar-cuenta','FormaPagoClienteController@eliminarCuenta')->name('forma_pago.eliminarCuenta');
	
	//regenerar asiento 
	Route::get('regenerar_asiento_aplicacion/{id_empresa}','CuentaCorrienteController@regenerarAsientoAnticipoFactura')->name('regenerar_asiento_aplicacion');
	Route::get('regenerar_asiento_recibo/{id_empresa}','CobranzaController@regenerarAsientoRecibo');
	Route::post('pagar-reserva-detalle','ProformaController@pagarReservaDetalle')->name('pagarReservaDetalle');

	//Permiso TC 
	Route::get('asignar-tc-pago','BancosController@permisoTc')->name('asignar_tc');
	Route::post('asignar-tc-pago','BancosController@setTcPersona')->name('asignar_tc.save');
	
	Route::get('datosSolicitud','ControlOperacionesController@datosSolicitud')->name('datosSolicitud');

	//REPORTE PARA BUSQUEDA CANGOROO
	Route::get('buscar-reserva-cangoroo','ReservaCangorooController@buscarReserva')->name('buscarReservaCangoroo');
	Route::get('buscar-reserva-cangoroo/ajax','ReservaCangorooController@buscarReservaAjax')->name('buscarReservaCangoroo.ajax');

	//Alta plan cuentas empresas
	Route::get('contabilidad/alta-plan-cuenta','CuentaContableController@altaPlan')->name('contabilidad.altaPlan');
	Route::post('contabilidad/alta-plan-cuenta/alta','CuentaContableController@guardarPlanCuenta')->name('contabilidad.guardarExcel');

	//Dtplus Reporte
	Route::get('dtplus','DtplusController@index')->name('dtplus.index');
	Route::get('dtplus/listado','DtplusController@listado')->name('dtplus.listado.ajax');
	Route::get('dtplus/detalle','DtplusController@detalle')->name('dtplus.detalle.ajax');
	Route::get('dtplus/pdf','DtplusController@voucherPdf')->name('dtplus.pdf.ajax');
	Route::get('dtplus/aplicar-factura','DtplusController@pagarComision')->name('dtplus.pagar.ajax');
	

	//RESOURCE PARA EMPRESA
	Route::get('empresa', 'EmpresaController@index')->name('empresa.index');
	Route::get('empresa/create', 'EmpresaController@create')->name('empresa.create');
	Route::get('empresa/{id?}/edit', 'EmpresaController@edit')->name('empresa.edit');
	Route::post('empresa', 'EmpresaController@store')->name('empresa.store');
	Route::put('empresa/{id?}', 'EmpresaController@update')->name('empresa.update');

	//Reparar detalles de proforma cotizacion
	// Route::get('repararCotizacionProforma', 'ProformaController@repararCotizacionProforma');
	
}); 

	Route::post('recibirAir', ['as' => 'recibirAir', 'uses' => 'TicketsController@recibirAir']);

	Route::get('apiNGO','ExcelController@actualizarStockNgo')->name('apiNGO');

	Route::get('getListaProdMaestro','ExcelController@getListaProdMaestro')->name('getListaProdMaestro');

	Route::get('enviarCorreo','MailController@enviarCorreo')->name('enviarCorreo');
	Route::get('generarMenu','AutorizacionController@generarMenu')->name('generarMenu');

	Route::get('generarProducto','HomeController@generarProducto')->name('generarProducto');

	 /*================================================================================================
										API WOOCOMMERCE VENTAS RAPIDAS
	================================================================================================*/
	Route::post('end_point_webhooks', ['as' => 'end_point_webhooks', 'uses' => 'ApiWooCommerceController@end_point_webhooks']);
	Route::get('migrar_productos','ApiWooCommerceController@migrar_productos')->name('migrar_productos');
	
	Route::get('notificar','MailController@notificar')->name('notificar');

	Route::get('actualizarPass','LoginController@actualizarPass')->name('actualizarPass');

	Route::get('actualizar','LoginController@activarPassword')->name('actualizar');

//	Route::get('reservas_pagar','PagoProveedorController@reservas_pagar')->name('reservas_pagar');

	Route::get('reservas_pagar/{id?}','PagoProveedorController@reservas_pagar', function ($id) {
		return $id;	
	});

	// Route::get('test-correccion','HomeController@ajusteErrorAsiento')->name('trest');
	
	Route::post('sale-info', ['as' => 'sale-info', 'uses' => 'TicketsController@guardarTicket']);


	//Route::get('facturaEca','CertificateController@extractElements')->name('facturaEca');
	Route::get('facturaEca','FacturarController@facturaEca')->name('facturaEca');
	Route::get('envioFeca','CertificateController@envioFeca')->name('envioFeca');

	Route::get('generateSignature','CertificateController@generateSignature')->name('generateSignature');

	Route::get('downloadCangoroo','ReservaCangorooController@download')->name('downloadCangoroo');

	Route::get('generarJsonFactura/{id?}','FacturarController@generarJsonFactura', function ($id) {
		return $id;	
	});

	Route::get('generarJsonNC/{id?}','FacturarController@generarJsonNC', function ($id) {
		return $id;	
	});

	Route::get('documentosElectronicos','FacturarController@documentosElectronicos')->name('documentosElectronicos');


	Route::get('consultaDocumentosEca','FacturarController@consultaDocumentosEca')->name('consultaDocumentosEca');

	Route::get('reservasCangoroo','ProformaController@reservasCangoroo')->name('reservasCangoroo');